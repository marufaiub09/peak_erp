-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppl_planning_info_machine_dtls`
--

DROP TABLE IF EXISTS `ppl_planning_info_machine_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppl_planning_info_machine_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `dtls_id` int(11) NOT NULL DEFAULT '0',
  `machine_id` int(11) NOT NULL DEFAULT '0',
  `dia` varchar(20) NOT NULL,
  `capacity` double NOT NULL DEFAULT '0',
  `distribution_qnty` double NOT NULL DEFAULT '0',
  `no_of_days` double NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Yes 0;No',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Yes 0:No ',
  PRIMARY KEY (`id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppl_planning_info_machine_dtls`
--

LOCK TABLES `ppl_planning_info_machine_dtls` WRITE;
/*!40000 ALTER TABLE `ppl_planning_info_machine_dtls` DISABLE KEYS */;
INSERT INTO `ppl_planning_info_machine_dtls` VALUES (1,1,1,3,'3',300,291,0.97,'2015-04-27','2015-04-27',1,'2015-04-27 10:55:28',0,'0000-00-00 00:00:00',1,0),(5,4,4,1,'1',350,5,0.01,'2015-05-04','2015-05-04',2,'2015-05-04 15:39:26',0,'0000-00-00 00:00:00',1,0),(7,3,3,4,'4',350,2,0.01,'2015-05-04','2015-05-04',2,'2015-05-04 16:40:26',0,'0000-00-00 00:00:00',1,0),(8,3,5,1,'1',350,7,0.02,'2015-05-01','2015-05-01',2,'2015-05-04 16:51:14',0,'0000-00-00 00:00:00',1,0),(9,5,6,2,'2',350,33,0.09,'0000-00-00','0000-00-00',2,'2015-05-09 12:03:10',0,'0000-00-00 00:00:00',1,0),(10,6,7,8,'8',350,652,1.86,'2015-05-12','2015-05-13',17,'2015-05-12 17:56:38',0,'0000-00-00 00:00:00',1,0),(11,7,8,9,'9',300,10,0.03,'2015-05-26','2015-05-26',2,'2015-05-26 09:49:06',0,'0000-00-00 00:00:00',1,0),(12,8,9,3,'3',320,147.48,0.46,'2015-05-27','2015-05-27',17,'2015-05-27 16:47:23',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `ppl_planning_info_machine_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
