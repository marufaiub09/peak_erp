-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_non_ord_samp_booking_mst`
--

DROP TABLE IF EXISTS `wo_non_ord_samp_booking_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_non_ord_samp_booking_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_type` int(11) NOT NULL COMMENT '1=FB,2=TB,3=SB',
  `is_short` int(11) NOT NULL COMMENT '1=yes,2=no',
  `booking_no_prefix` varchar(12) NOT NULL,
  `booking_no_prefix_num` int(11) NOT NULL,
  `booking_no` varchar(25) NOT NULL,
  `company_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `job_no` varchar(25) NOT NULL,
  `po_break_down_id` varchar(150) NOT NULL,
  `item_category` int(11) NOT NULL,
  `fabric_source` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `exchange_rate` double NOT NULL,
  `pay_mode` int(11) NOT NULL,
  `source` int(11) NOT NULL,
  `booking_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `booking_month` int(11) NOT NULL,
  `booking_year` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `attention` varchar(200) NOT NULL,
  `is_approved` int(11) NOT NULL,
  `ready_to_approved` int(11) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `team_leader` int(11) NOT NULL,
  `dealing_marchant` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_non_ord_samp_booking_mst`
--

LOCK TABLES `wo_non_ord_samp_booking_mst` WRITE;
/*!40000 ALTER TABLE `wo_non_ord_samp_booking_mst` DISABLE KEYS */;
INSERT INTO `wo_non_ord_samp_booking_mst` VALUES (1,4,0,'PAL-SMN-15-',1,'PAL-SMN-15-00001',1,42,'','',2,1,2,0,3,3,'2015-06-17','2015-06-22',0,0,0,'Mr. Hasa',0,2,0,1,28,'2015-06-17 14:51:57',0,'0000-00-00 00:00:00',1,10),(2,4,0,'PAL-SMN-15-',2,'PAL-SMN-15-00002',1,42,'','',2,1,2,0,3,3,'2015-06-17','2015-06-22',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-06-17 21:12:59',28,'2015-06-17 21:33:02',1,10),(3,4,0,'PAL-SMN-15-',3,'PAL-SMN-15-00003',1,42,'','',2,1,2,0,3,3,'2015-06-23','2015-06-25',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-06-23 14:01:58',0,'0000-00-00 00:00:00',1,10),(4,5,0,'PAL-TSN-15-',1,'PAL-TSN-15-00001',1,42,'','',4,0,2,75,3,3,'2015-06-30','2015-06-02',0,0,10,'Mr. Ratan',0,1,0,1,28,'2015-06-30 16:37:53',0,'0000-00-00 00:00:00',0,0),(5,4,0,'PAL-SMN-15-',4,'PAL-SMN-15-00004',1,42,'','',2,1,1,0,1,3,'2015-07-01','2015-07-05',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-07-01 12:08:00',0,'0000-00-00 00:00:00',1,10),(6,4,0,'PAL-SMN-15-',5,'PAL-SMN-15-00005',1,42,'','',2,1,1,0,3,3,'2015-07-01','2015-07-05',0,0,0,'Mr. Hasan',0,1,0,1,28,'2015-07-01 15:11:30',0,'0000-00-00 00:00:00',1,10),(7,4,0,'PAL-SMN-15-',6,'PAL-SMN-15-00006',1,42,'','',2,1,2,0,3,3,'2015-07-11','2015-07-30',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-07-11 07:36:55',0,'0000-00-00 00:00:00',1,10),(8,4,0,'PAL-SMN-15-',7,'PAL-SMN-15-00007',1,42,'','',2,1,2,0,3,3,'2015-07-15','2015-07-29',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-07-15 13:51:40',0,'0000-00-00 00:00:00',1,10),(9,4,0,'PAL-SMN-15-',8,'PAL-SMN-15-00008',1,42,'','',2,1,2,0,3,3,'2015-08-03','2015-08-09',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-08-03 14:22:08',0,'0000-00-00 00:00:00',1,10),(10,4,0,'PAL-SMN-15-',9,'PAL-SMN-15-00009',1,42,'','',2,1,2,0,3,3,'2015-08-14','2015-08-20',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-08-14 13:24:56',0,'0000-00-00 00:00:00',1,10),(11,4,0,'PAL-SMN-15-',10,'PAL-SMN-15-00010',1,14,'','',2,1,2,0,3,3,'2015-08-22','2015-08-31',0,0,0,'',0,2,0,1,4,'2015-08-22 14:48:53',0,'0000-00-00 00:00:00',1,2),(12,4,0,'PAL-SMN-15-',11,'PAL-SMN-15-00011',1,14,'','',2,1,2,0,3,3,'2015-08-22','0000-00-00',0,0,0,'',0,2,0,1,4,'2015-08-22 15:22:36',0,'0000-00-00 00:00:00',1,2),(13,4,0,'PAL-SMN-15-',12,'PAL-SMN-15-00012',1,57,'','',2,1,2,74,3,3,'2015-08-26','2015-08-26',0,0,0,'',0,2,0,1,4,'2015-08-26 15:04:20',0,'0000-00-00 00:00:00',1,0),(14,4,0,'PAL-SMN-15-',13,'PAL-SMN-15-00013',1,42,'','',2,1,2,0,3,3,'2015-08-29','2015-08-31',0,0,0,'Mr. Hasan',0,2,0,1,28,'2015-08-29 15:38:08',0,'0000-00-00 00:00:00',1,10),(15,5,0,'PAL-TSN-15-',2,'PAL-TSN-15-00002',1,42,'','',4,0,2,75,1,3,'2015-08-30','2015-09-01',0,0,10,'Mr. Ronton',0,2,0,1,28,'2015-08-30 08:53:52',0,'0000-00-00 00:00:00',0,0),(16,4,0,'PAL-SMN-15-',14,'PAL-SMN-15-00014',1,14,'','',2,1,2,74,3,3,'2015-09-09','0000-00-00',0,0,44,'Mr.Hasan',0,2,0,1,4,'2015-09-09 09:03:27',0,'0000-00-00 00:00:00',1,2),(17,4,0,'PAL-SMN-15-',15,'PAL-SMN-15-00015',1,14,'','',2,1,2,74,3,3,'2015-09-09','2015-09-16',0,0,44,'Mr.Hasan',0,2,0,1,4,'2015-09-09 09:34:02',0,'0000-00-00 00:00:00',1,2),(18,4,0,'PAL-SMN-15-',16,'PAL-SMN-15-00016',1,14,'','',2,1,2,74,3,3,'2015-09-09','2015-09-12',0,0,44,'',0,2,0,1,4,'2015-09-09 17:33:23',0,'0000-00-00 00:00:00',1,2),(19,4,0,'PAL-SMN-15-',17,'PAL-SMN-15-00017',1,14,'','',2,1,2,74,3,3,'2015-09-09','0000-00-00',0,0,44,'',0,2,0,1,4,'2015-09-09 18:27:46',4,'2015-09-09 18:27:52',1,2),(20,4,0,'PAL-SMN-15-',18,'PAL-SMN-15-00018',1,42,'','',2,1,2,0,3,0,'2015-09-30','2015-10-03',0,0,0,'',0,2,0,1,2,'2015-09-30 11:07:35',2,'2015-09-30 11:07:54',1,9),(21,4,0,'PAL-SMN-15-',19,'PAL-SMN-15-00019',1,3,'','',2,1,2,0,3,3,'2015-09-30','2015-10-15',0,0,44,'',0,2,0,1,7,'2015-09-30 15:43:22',0,'0000-00-00 00:00:00',1,7);
/*!40000 ALTER TABLE `wo_non_ord_samp_booking_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
