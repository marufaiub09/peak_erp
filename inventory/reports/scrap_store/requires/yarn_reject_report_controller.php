<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	function check_all_data()
	{
		var tbl_row_count = document.getElementById('list_view' ).rows.length;
		tbl_row_count = tbl_row_count - 1;

		for( var i = 1; i <= tbl_row_count; i++ )
		{
			$('#tr_'+i).trigger('click'); 
		}
	}
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		//alert (id)
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_account_id').val( id );
		$('#item_account_val').val( ddd );
	} 
		  
	</script>
     <input type="hidden" id="item_account_id" />
     <input type="hidden" id="item_account_val" />
 <?php
	
	$sql="SELECT id, item_category_id, product_name_details from product_details_master where item_category_id=1 and status_active=1 and is_deleted=0"; 
	$arr=array(0=>$item_category);

	echo  create_list_view("list_view", "Item Category,Fabric Description,Product ID", "120,250,60","490","300",0, $sql , "js_set_value", "id,product_name_details", "", 1, "item_category_id,0,0", $arr, "item_category_id,product_name_details,id", "",'setFilterGrid("list_view",-1);','0,0,0','',1) ;
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if ($cbo_company_id==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_id'";
	if ($txt_product_id=="") 
	{
		$prod_id_cond=""; 
	}
	else 
	{
		$prod_id_cond=" and c.prod_id in ($txt_product_id)";
	}
	
	//if ($cbo_store_id==0){ $store_id="";}else{$store_id=" and a.store_id='$cbo_store_id'";}
	
	if($db_type==0) 
	{
		$from_date=change_date_format($from_date,'yyyy-mm-dd');
		$to_date=change_date_format($to_date,'yyyy-mm-dd');
	}
	else if($db_type==2) 
	{
		$from_date=change_date_format($from_date,'','',1);
		$to_date=change_date_format($to_date,'','',1);
	}
	
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$buyerArr = return_library_array("select id,short_name from lib_buyer where status_active=1 and is_deleted=0","id","short_name"); 
	$brandArr = return_library_array("select id,brand_name from lib_brand where status_active=1 and is_deleted=0","id","brand_name");
	$supplierArr = return_library_array("select id, supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name"); 
	$determinaArr = return_library_array("select id,construction from lib_yarn_count_determina_mst where status_active=1 and is_deleted=0","id","construction");
	
	$data_array=array();
		
	$sql_scrap="select b.prod_id, sum(b.sales_qty) as scrap_qty, sum(case when a.selling_date<'".$from_date."' then b.sales_qty else 0 end) as sales_total_opening, sum(case when a.selling_date between '".$from_date."' and '".$to_date."' then b.sales_qty else 0 end) as sales_qty from  inv_scrap_sales_mst a, inv_scrap_sales_dtls b where a.id=b.mst_id and a.item_category=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.prod_id";
	$sql_scrap_result=sql_select($sql_scrap);
	foreach($sql_scrap_result as $row)
	{
		$data_array[$row[csf('prod_id')]]['opening']=$row[csf('sales_total_opening')];
		$data_array[$row[csf('prod_id')]]['scrip']=$row[csf('scrap_qty')];
	}
	
	$product_array=array();
		
	$sql_prod="select id, product_name_details, lot, yarn_count_id, yarn_type, brand, supplier_id from product_details_master where company_id='$cbo_company_id' and item_category_id=1 and status_active=1 and is_deleted=0";
	$sql_prod_result=sql_select($sql_prod);
	foreach($sql_prod_result as $row)
	{
		$product_array[$row[csf('id')]]['dtls']=$row[csf('product_name_details')];
		$product_array[$row[csf('id')]]['lot']=$row[csf('lot')];
		$product_array[$row[csf('id')]]['yarn_count_id']=$row[csf('yarn_count_id')];
		$product_array[$row[csf('id')]]['yarn_type']=$row[csf('yarn_type')];
		$product_array[$row[csf('id')]]['brand']=$row[csf('brand')];
		$product_array[$row[csf('id')]]['supplier_id']=$row[csf('supplier_id')];
	}
	ob_start();	
	
	if($type==1)
	{
		?>
		<div>
			<table style="width:1600px" border="1" cellpadding="2" cellspacing="0"  id="caption" rules="all"> 
				<thead>
					<tr class="form_caption" style="border:none;">
						<td colspan="15" align="center" style="border:none;font-size:16px; font-weight:bold" ><?php echo $report_title; ?></td> 
					</tr>
					<tr class="form_caption" style="border:none;">
						<td colspan="15" align="center" style="border:none; font-size:14px;">
						   <b>Company Name : <?php echo $companyArr[$cbo_company_id]; ?></b>                               
						</td>
					</tr>
					<tr class="form_caption" style="border:none;">
						<td colspan="15" align="center" style="border:none;font-size:12px; font-weight:bold">
							<?php if($from_date!="" || $to_date!="") echo "From : ".change_date_format($from_date)." To : ".change_date_format($to_date)."" ;?>
						</td>
					</tr>
				</thead>
			</table>
			<table width="1600" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
				<thead>
					<tr>
						<th rowspan="2" width="40">SL</th>
						<th rowspan="2" width="60">Job</th>
						<th rowspan="2" width="60">Year</th>
						<th rowspan="2" width="80">Buyer</th>
						<th rowspan="2" width="80">Style</th>
                        <th rowspan="2" width="80">Order No</th>
						<th rowspan="2" width="60">Prod.ID</th>
						<th colspan="3">Description</th>
						<th rowspan="2" width="120">Booking/Req.</th>
						<th rowspan="2" width="110">Opening Stock</th>
						<th rowspan="2" width="100">Reject Qty.</th>
						<th rowspan="2" width="100">Closing Stock</th>
						<th rowspan="2">Remarks</th>
					</tr>
					<tr>                         
						<th width="180">Product Name</th>
						<th width="100">Lot</th>
						<th width="100">Supplier</th>
					</tr> 
				</thead>
			</table>
			<div style="width:1600px; max-height:350px; overflow-y:scroll" id="scroll_body" > 
				<table width="1582" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_body">
				<?php
					/*$sql = "select a.booking_id, a.booking_no, c.po_breakdown_id, c.prod_id,
					sum(case when b.transaction_date<'".$from_date."' then c.reject_qty else 0 end) as rej_total_opening,
					sum(case when b.transaction_date between '".$from_date."' and '".$to_date."' then c.reject_qty else 0 end) as reject_qty
					from inv_receive_master a, inv_transaction b, order_wise_pro_details c where a.company_id='$cbo_company_id' and a.id=b.mst_id and b.id=c.trans_id and c.trans_type=4 and b.transaction_type=4 and a.entry_form=9 and c.entry_form=9 and a.item_category=1 and b.item_category=1 and c.reject_qty!=0 $prod_id_cond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.booking_id, a.booking_no, c.po_breakdown_id, c.prod_id order by c.prod_id";*/
					if($db_type==0) $year_cond="YEAR(d.insert_date)"; 
					else if($db_type==2) $year_cond="to_char(d.insert_date,'YYYY')";

					$sql = "select a.booking_id, a.booking_no,
					d.job_no_prefix_num, d.job_no, $year_cond as year, d.buyer_name, d.style_ref_no, e.id, e.po_number, c.prod_id,
					sum(case when b.transaction_date<'".$from_date."' then c.reject_qty else 0 end) as rej_total_opening,
					sum(case when b.transaction_date between '".$from_date."' and '".$to_date."' then c.reject_qty else 0 end) as reject_qty
					from inv_receive_master a, inv_transaction b, order_wise_pro_details c, wo_po_details_master d, wo_po_break_down e
					
					where a.company_id='$cbo_company_id' and a.id=b.mst_id and b.id=c.trans_id and c.po_breakdown_id=e.id and d.job_no=e.job_no_mst
					and c.trans_type=4 and b.transaction_type=4 and a.entry_form=9 and c.entry_form=9 and a.item_category=1 and b.item_category=1 and c.reject_qty!=0 $prod_id_cond 
					and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 and e.status_active=1 and e.is_deleted=0 
					
					group by a.booking_id, a.booking_no, d.job_no_prefix_num, d.job_no, d.insert_date, d.buyer_name, d.style_ref_no, e.id, e.po_number, c.prod_id order by c.prod_id";	
					//echo $sql;
					$i=1;
					$result = sql_select($sql);
					foreach($result as $row)
					{
						if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
						$closingStock=0;
		
						$opening=$row[csf("rej_total_opening")];
						$reject_qty=$row[csf("reject_qty")];
						
						$closingStock=$opening+$reject_qty;//-$scrap_out_qty;
						$tot_opening+=$opening;
						$tot_reject_qty+=$reject_qty;
						//$tot_scrap_out_qty+=$scrap_out_qty;
						$tot_closingStock+=$closingStock;
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="40"><?php echo $i; ?></td>
								<td width="60" align="center"><p><?php echo $row[csf('job_no_prefix_num')]; ?></p></td>
								<td width="60" align="center"><p><?php echo $row[csf('year')]; ?></p></td>
								<td width="80"><div style="word-wrap:break-word; width:80px"><?php echo $buyerArr[$row[csf('buyer_name')]]; ?></div></td>
								<td width="80"><div style="word-wrap:break-word; width:80px"><?php echo $row[csf('style_ref_no')]; ?></div></td>
                                <td width="80"><div style="word-wrap:break-word; width:80px"><?php echo $row[csf('po_number')]; ?></div></td>
								<td width="60" align="center"><p><?php echo $row[csf("prod_id")]; ?></p></td>
								<td width="180"><p><?php echo $product_array[$row[csf('prod_id')]]['dtls']; ?></p></td>
								<td width="100"><p><?php echo $product_array[$row[csf('prod_id')]]['lot']; ?></p></td> 
								<td width="100"><div style="word-wrap:break-word; width:100px"><?php echo $supplierArr[$product_array[$row[csf('prod_id')]]['supplier_id']]; ?></div></td> 
								<td width="120"><p><?php echo $row[csf("booking_no")]; ?></p></td>
								<td width="110" align="right"><p><?php echo number_format($opening,2); ?></p></td>
								<td width="100" align="right"><p><?php echo number_format($reject_qty,2); ?></p></td>
								<td width="100" align="right"><p><?php echo number_format($closingStock,2); ?></p></td>
								<td>&nbsp;</td>
							</tr>
						 <?php 												
						 $i++; 				
					}
					?>
				</table>
			</div> 
			<table width="1600" border="1" cellpadding="0" cellspacing="0" class="tbl_bottom" rules="all" > 
			   <tr>
					<td width="40" align="right">&nbsp;</th>
					<td width="60" align="right">&nbsp;</th>
					<td width="60" align="right">&nbsp;</th>
					<td width="80" align="right">&nbsp;</th>
					<td width="80" align="right">&nbsp;</th>
                    <td width="80" align="right">&nbsp;</th>
					<td width="60" align="right">&nbsp;</th>
					<td width="180" align="right">&nbsp;</th>
					<td width="100" align="right">&nbsp;</th>
					<td width="100" align="right">&nbsp;</th>
					<td width="120" align="right">Total</th>
					<td width="110" align="right" id="value_tot_opening"><?php echo number_format($tot_opening,2);  ?></td>
					<td width="100" align="right" id="value_tot_reject_qty"><?php echo number_format($tot_reject_qty,2);  ?></td>
					<td width="100" align="right" id="value_tot_closingStock"><?php echo number_format($tot_closingStock,2);  ?></td>
					<td align="right">&nbsp;</td>
				</tr>
			</table>
		</div>
		<?php
	}
	else if ($type==2)
	{
		?>
		<div>
			<table style="width:1400px" border="1" cellpadding="2" cellspacing="0"  id="caption" rules="all"> 
				<thead>
					<tr class="form_caption" style="border:none;">
						<td colspan="15" align="center" style="border:none;font-size:16px; font-weight:bold" ><?php echo $report_title; ?></td> 
					</tr>
					<tr class="form_caption" style="border:none;">
						<td colspan="15" align="center" style="border:none; font-size:14px;">
						   <b>Company Name : <?php echo $companyArr[$cbo_company_id]; ?></b>                               
						</td>
					</tr>
					<tr class="form_caption" style="border:none;">
						<td colspan="15" align="center" style="border:none;font-size:12px; font-weight:bold">
							<?php if($from_date!="" || $to_date!="") echo "From : ".change_date_format($from_date)." To : ".change_date_format($to_date)."" ;?>
						</td>
					</tr>
				</thead>
			</table>
			<table width="1400" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
				<thead>
					<tr>
						<th rowspan="2" width="40">SL</th>
                        <th rowspan="2" width="70">Sample ID</th>
                        <th rowspan="2" width="70">Year</th>
                        <th rowspan="2" width="100">Buyer</th>
                        <th rowspan="2" width="100">Style</th>
                        <th rowspan="2" width="120">Fab. Booking No.</th>
						<th rowspan="2" width="60">Prod.ID</th>
						<th colspan="3">Description</th>
						<th rowspan="2" width="110">Opening Stock</th>
						<th rowspan="2" width="100">Reject Qty.</th>
						<th rowspan="2" width="100">Closing Stock</th>
						<th rowspan="2">Remarks</th>
					</tr>
					<tr>                         
						<th width="180">Product Name</th>
						<th width="100">Lot</th>
						<th width="100">Supplier</th>
					</tr> 
				</thead>
			</table>
			<div style="width:1400px; max-height:350px; overflow-y:scroll" id="scroll_body" > 
				<table width="1382" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_body">
				<?php
					/*$sql = "select a.booking_id, a.booking_no, b.prod_id,
					sum(case when b.transaction_date<'".$from_date."' then b.cons_reject_qnty else 0 end) as rej_total_opening,
					sum(case when b.transaction_date between '".$from_date."' and '".$to_date."' then b.cons_reject_qnty else 0 end) as reject_qty
					from inv_receive_master a, inv_transaction b where a.company_id='$cbo_company_id' and a.id=b.mst_id and b.transaction_type=4 and a.entry_form=9 and a.item_category=1 and b.item_category=1 and b.cons_reject_qnty!=0 $prod_id_cond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.booking_id, a.booking_no, b.prod_id order by b.prod_id";*/
					if($db_type==0) $sample_year_cond="Year(a.insert_date)";
					else if($db_type==2) $sample_year_cond="TO_CHAR(a.insert_date,'YYYY')";
					else $sample_year_cond="";
					
					$sample_array=array();
					$sample_sql=sql_select("select a.booking_no, a.buyer_id, $sample_year_cond as year, b.style_id, b.style_des from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1");
					foreach($sample_sql as $row)
					{
						$sample_array[$row[csf("booking_no")]]["buyer_id"]=$row[csf("buyer_id")];
						$sample_array[$row[csf("booking_no")]]["year"]=$row[csf("year")];
						$sample_array[$row[csf("booking_no")]]["style_id"]=$row[csf("style_id")];
						$sample_array[$row[csf("booking_no")]]["style_des"]=$row[csf("style_des")];
					}
					
					if($txt_product_id=="") $prod_cond=""; else $prod_cond=" and b.prod_id in ($txt_product_id)";
					
					$sql = "select a.booking_id, a.booking_no, b.prod_id,
					sum(case when b.transaction_date<'".$from_date."' then b.cons_reject_qnty else 0 end) as rej_total_opening,
					sum(case when b.transaction_date between '".$from_date."' and '".$to_date."' then b.cons_reject_qnty else 0 end) as reject_qty
					from inv_receive_master a, inv_transaction b
					
					where a.company_id='$cbo_company_id' and a.id=b.mst_id and a.booking_without_order=1
					and b.transaction_type=4 and a.entry_form=9 and a.item_category=1 and b.item_category=1 and b.cons_reject_qnty!=0 $prod_cond 
					and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 
					
					group by a.booking_id, a.booking_no, b.prod_id order by b.prod_id";	
					//echo $sql;
					$i=1;
					$result = sql_select($sql);
					foreach($result as $row)
					{
						if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
						$closingStock=0;
		
						$opening=$row[csf("rej_total_opening")];
						$reject_qty=$row[csf("reject_qty")];
						$closingStock=$opening+$reject_qty;//-$scrap_out_qty;
						$tot_opening+=$opening;
						$tot_reject_qty+=$reject_qty;
						//$tot_scrap_out_qty+=$scrap_out_qty;
						$tot_closingStock+=$closingStock;
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="40"><?php echo $i; ?></td>
                                <td width="70" align="center"><p><?php echo $sample_array[$row[csf("booking_no")]]["style_id"]; ?></p></td>
                                <td width="70" align="center"><p><?php echo $sample_array[$row[csf("booking_no")]]["year"]; ?></p></td>
                                <td width="100" align="center"><p><?php echo $buyerArr[$sample_array[$row[csf("booking_no")]]["buyer_id"]]; ?></p></td>
                                <td width="100" align="center"><p><?php echo $sample_array[$row[csf("booking_no")]]["style_des"]; ?></p></td>
								<td width="120"><p><?php echo $row[csf("booking_no")]; ?></p></td>
								<td width="60" align="center"><p><?php echo $row[csf("prod_id")]; ?></p></td>
								<td width="180"><p><?php echo $product_array[$row[csf('prod_id')]]['dtls']; ?></p></td>
								<td width="100"><p><?php echo $product_array[$row[csf('prod_id')]]['lot']; ?></p></td> 
								<td width="100"><div style="word-wrap:break-word; width:100px"><?php echo $supplierArr[$product_array[$row[csf('prod_id')]]['supplier_id']]; ?></div></td> 
								<td width="110" align="right"><p><?php echo number_format($opening,2); ?></p></td>
								<td width="100" align="right"><p><?php echo number_format($reject_qty,2); ?></p></td>
								<td width="100" align="right"><p><?php echo number_format($closingStock,2); ?></p></td>
								<td>&nbsp;</td>
							</tr>
						 <?php 												
						 $i++; 				
					}
					?>
				</table>
			</div> 
			<table width="1400" border="1" cellpadding="0" cellspacing="0" class="tbl_bottom" rules="all" > 
			   <tr>
					<td width="40" align="right">&nbsp;</th>
                    <td width="70" align="right">&nbsp;</th>
                    <td width="70" align="right">&nbsp;</th>
                    <td width="100" align="right">&nbsp;</th>
                    <td width="100" align="right">&nbsp;</th>
					<td width="120" align="right">Total</th>
					<td width="60" align="right">&nbsp;</th>
					<td width="180" align="right">&nbsp;</th>
					<td width="100" align="right">&nbsp;</th>
					<td width="100" align="right">&nbsp;</th>
					<td width="110" align="right" id="value_tot_opening"><?php echo number_format($tot_opening,2);  ?></td>
					<td width="100" align="right" id="value_tot_reject_qty"><?php echo number_format($tot_reject_qty,2);  ?></td>
					<td width="100" align="right" id="value_tot_closingStock"><?php echo number_format($tot_closingStock,2);  ?></td>
					<td align="right">&nbsp;</td>
				</tr>
			</table>
		</div>
		<?php
	}
	else if ($type==3)
	{
		?>
		<div>
			<table style="width:1180px" border="1" cellpadding="2" cellspacing="0"  id="caption" rules="all"> 
				<thead>
					<tr class="form_caption" style="border:none;">
						<td colspan="10" align="center" style="border:none;font-size:16px; font-weight:bold" ><?php echo $report_title; ?></td> 
					</tr>
					<tr class="form_caption" style="border:none;">
						<td colspan="10" align="center" style="border:none; font-size:14px;">
						   <b>Company Name : <?php echo $companyArr[$cbo_company_id]; ?></b>                               
						</td>
					</tr>
					<tr class="form_caption" style="border:none;">
						<td colspan="10" align="center" style="border:none;font-size:12px; font-weight:bold">
							<?php if($from_date!="" || $to_date!="") echo "From : ".change_date_format($from_date)." To : ".change_date_format($to_date)."" ;?>
						</td>
					</tr>
				</thead>
			</table>
			<table width="1040" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
				<thead>
					<tr>
						<th rowspan="2" width="40">SL</th>
						<th rowspan="2" width="60">Prod.ID</th>
						<th colspan="3">Description</th>
						<th rowspan="2" width="110">Opening Stock</th>
						<th rowspan="2" width="100">Reject Qty.</th>
                        <th rowspan="2" width="100">Scrap Sales Qty</th>
						<th rowspan="2" width="100">Closing Stock</th>
						<th rowspan="2">Remarks</th>
					</tr>
					<tr>                         
						<th width="180">Product Name</th>
						<th width="100">Lot</th>
						<th width="100">Supplier</th>
					</tr> 
				</thead>
			</table>
			<div style="width:1040px; max-height:350px; overflow-y:scroll" id="scroll_body" > 
				<table width="1022" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_body">
				<?php
					/*$sql = "select a.booking_id, a.booking_no, b.prod_id,
					sum(case when b.transaction_date<'".$from_date."' then b.cons_reject_qnty else 0 end) as rej_total_opening,
					sum(case when b.transaction_date between '".$from_date."' and '".$to_date."' then b.cons_reject_qnty else 0 end) as reject_qty
					from inv_receive_master a, inv_transaction b where a.company_id='$cbo_company_id' and a.id=b.mst_id and b.transaction_type=4 and a.entry_form=9 and a.item_category=1 and b.item_category=1 and b.cons_reject_qnty!=0 $prod_id_cond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.booking_id, a.booking_no, b.prod_id order by b.prod_id";*/	
					
					$sql = "select a.booking_id, a.booking_no, c.prod_id,
					sum(case when b.transaction_date<'".$from_date."' then c.reject_qty else 0 end) as rej_total_opening,
					sum(case when b.transaction_date between '".$from_date."' and '".$to_date."' then c.reject_qty else 0 end) as reject_qty
					from inv_receive_master a, inv_transaction b, order_wise_pro_details c
					
					where a.company_id='$cbo_company_id' and a.id=b.mst_id and b.id=c.trans_id 
					and c.trans_type=4 and b.transaction_type=4 and a.entry_form=9 and c.entry_form=9 and a.item_category=1 and b.item_category=1 and c.reject_qty!=0 $prod_id_cond 
					and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 
					
					group by a.booking_id, a.booking_no, c.prod_id order by c.prod_id";	
					//echo $sql;
					$i=1;
					$result = sql_select($sql);
					foreach($result as $row)
					{
						if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
						$closingStock=0;
		
						$opening=$row[csf("rej_total_opening")];
						$reject_qty=$row[csf("reject_qty")];
						$scrap_out_qty=$data_array[$row[csf('prod_id')]]['scrip'];
						$closingStock=$opening+$reject_qty-$scrap_out_qty;
						$tot_opening+=$opening;
						$tot_reject_qty+=$reject_qty;
						$tot_scrap_out_qty+=$scrap_out_qty;
						$tot_closingStock+=$closingStock;
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="40"><?php echo $i; ?></td>
								<td width="60" align="center"><p><?php echo $row[csf("prod_id")]; ?></p></td>
								<td width="180"><p><?php echo $product_array[$row[csf('prod_id')]]['dtls']; ?></p></td>
								<td width="100"><p><?php echo $product_array[$row[csf('prod_id')]]['lot']; ?></p></td> 
								<td width="100"><div style="word-wrap:break-word; width:100px"><?php echo $supplierArr[$product_array[$row[csf('prod_id')]]['supplier_id']]; ?></div></td> 
								<td width="110" align="right"><p><?php echo number_format($opening,2); ?></p></td>
								<td width="100" align="right"><p><?php echo number_format($reject_qty,2); ?></p></td>
                                <td width="100" align="right"><p><?php echo number_format($scrap_out_qty,2); ?></p></td>
								<td width="100" align="right"><p><?php echo number_format($closingStock,2); ?></p></td>
								<td>&nbsp;</td>
							</tr>
						 <?php 												
						 $i++; 				
					}
					?>
				</table>
			</div> 
			<table width="1040" border="1" cellpadding="0" cellspacing="0" class="tbl_bottom" rules="all" > 
			   <tr>
					<td width="40" align="right">&nbsp;</th>
					<td width="60" align="right">&nbsp;</th>
					<td width="180" align="right">&nbsp;</th>
					<td width="100" align="right">&nbsp;</th>
					<td width="100" align="right">Total</th>
					<td width="110" align="right" id="value_tot_opening"><?php echo number_format($tot_opening,2);  ?></td>
					<td width="100" align="right" id="value_tot_reject_qty"><?php echo number_format($tot_reject_qty,2);  ?></td>
                    <td width="100" align="right" id="value_tot_scrap_out_qty"><?php echo number_format($tot_scrap_out_qty,2);  ?></td>
					<td width="100" align="right" id="value_tot_closingStock"><?php echo number_format($tot_closingStock,2);  ?></td>
					<td align="right">&nbsp;</td>
				</tr>
			</table>
		</div>
		<?php
	}
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename**$type"; 
    exit();
}
?>