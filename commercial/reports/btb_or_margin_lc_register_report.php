<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create BTB or Margin LC Register Report
				
Functionality	:	
JS Functions	:
Created by		:	Kausar
Creation date 	: 	21-12-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("BTB or Margin LC Register Report","../../", 1, 1, $unicode,1,1); 
?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

	var tableFilters = 
	{
		col_35: "none",
		col_operation: {
		id: ["value_tot_lc_value"],
		col: [9],
		operation: ["sum"],
		write_method: ["innerHTML"]
		}
	} 

	function openmypage(company_name,lc_number,ship_date,supplier_id,lc_date,exp_date,payterm,pi_id,action,title)
	{
		var popup_width="";
		if(action=="pi_details") popup_width="900px"; else popup_width="850px";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/btb_or_margin_lc_register_report_controller.php?company_name='+company_name+'&lc_number='+lc_number+'&ship_date='+ship_date+'&supplier_id='+supplier_id+'&lc_date='+lc_date+'&exp_date='+exp_date+'&payterm='+payterm+'&pi_id='+pi_id+'&action='+action, title, 'width='+popup_width+',height=390px,center=1,resize=0,scrolling=0','../');
	}	
	
	function generate_report(operation)
	{
		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
			var report_title=$( "div.form_caption" ).html();
			var data="action=report_generate"+get_submitted_data_string('cbo_company_id*cbo_issue_banking*cbo_item_category_id*cbo_lc_type_id*cbo_supply_source*cbo_bonded_warehouse*txt_date_from*txt_date_to',"../../")+'&report_title='+report_title;
			freeze_window(3);
			http.open("POST","requires/btb_or_margin_lc_register_report_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
	
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			var tot_rows=reponse[2];
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
			setFilterGrid("tbl_marginlc_list",-1,tableFilters);
	 		show_msg('3');
			release_freezing();
		}
	}
	
	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	
	function lc_details_popup(lc_no,action,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/btb_or_margin_lc_register_report_controller.php?lc_no='+lc_no+'&action='+action, title, 'width=620px,height=390px,center=1,resize=0,scrolling=0','../');
	}

</script>
</head>
<body onLoad="set_hotkey();">
    <div style="width:100%;" align="center">
    <?php echo load_freeze_divs ("../../",$permission);  ?>   		 
        <form name="marginlcregister_1" id="marginlcregister_1" autocomplete="off" > 
         <h3 style="width:1070px; margin-top:20px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3> 
         <div id="content_search_panel" style="width:1070px" >      
            <fieldset>  
                <table class="rpt_table" width="1060" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <thead>
                        <th width="120" class="must_entry_caption">Company</th>
                        <th width="150">Issue Banking</th>
                        <th width="150">Item Category</th>
                        <th width="120">LC Type</th>
                        <th width="150">Supply Source</th>
                        <th width="120">Bonded Warehouse</th>
                        <th>Date Range</th>
                        <th width="70"><input type="reset" name="res" id="res" value="Reset" style="width:70px" class="formbutton" onClick="reset_form('marginlcregister_1','report_container*report_container2','','','')" /></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td align="center">
                                <?php 
                                    echo create_drop_down( "cbo_company_id", 120, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "--Select Company--", $selected, "" );
                                ?>                            
                           </td>
                            <td align="center">
                                <?php 
                                    echo create_drop_down( "cbo_issue_banking", 145, "select id,bank_name from  lib_bank where issusing_bank=1 and status_active=1 and is_deleted=0  order by bank_name","id,bank_name", 1, "--Select Bank--", $selected, "" );
                                ?>                            
                           </td>
                           <td align="center">
								<?php
									echo create_drop_down( "cbo_item_category_id", 145,$item_category,"", 1, "-- Select Category--", $selected, "","","","","","");
                                ?> 
                          </td>
                          <td align="center">
                            	<?php
                            		echo create_drop_down( "cbo_lc_type_id",120,$lc_type,'',1,'--Select LC Type--',0,"",0); 
                            	?>  
<!--                            	<input style="width:120px;" name="txt_lc_type" id="txt_lc_type" class="text_boxes" onDblClick="openmypage_lc_type()" placeholder="Browse" readonly />
<input type="text" name="txt_lc_type_id" id="txt_lc_type_id" style="width:90px;"/>
-->                        </td>
							<td align="center">
                                <?php 
                                    echo create_drop_down( "cbo_supply_source", 150, $supply_source,"", 1, "--Select source--", "", "" );
                                ?>
                           </td>

                           <td align="center">
                                <?php 
                                    echo create_drop_down( "cbo_bonded_warehouse", 120, $yes_no,"", 1, "--Select Warehouse--", "", "" );
                                ?>
                           </td>
                            <td align="center">
                                <input type="date" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px;"/>                    							
                                To
                                <input type="date" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px;"/>                        
                            </td>
                            <td>
                                <input type="button" name="search" id="search" value="Show" onClick="generate_report(3)" style="width:70px" class="formbutton" />
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="8" align="center"><?php echo load_month_buttons(1);  ?></td>
                        </tr>
                    </tfoot>
                </table> 
            </fieldset>
        </div>
    <div style="margin-top:10px" id="report_container" align="center"></div>
    <div id="report_container2"></div>
 </form>  
 </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>

</html>
