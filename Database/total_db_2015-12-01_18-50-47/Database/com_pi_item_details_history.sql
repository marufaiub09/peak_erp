-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_pi_item_details_history`
--

DROP TABLE IF EXISTS `com_pi_item_details_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_pi_item_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) NOT NULL,
  `pi_id` int(11) NOT NULL DEFAULT '0',
  `work_order_no` varchar(25) NOT NULL,
  `work_order_id` int(11) NOT NULL DEFAULT '0',
  `work_order_dtls_id` int(11) NOT NULL DEFAULT '0',
  `determination_id` int(11) NOT NULL DEFAULT '0',
  `item_prod_id` int(11) NOT NULL DEFAULT '0',
  `item_group` varchar(50) NOT NULL,
  `item_description` text NOT NULL,
  `color_id` int(11) NOT NULL DEFAULT '0',
  `item_color` int(11) NOT NULL DEFAULT '0',
  `size_id` int(11) NOT NULL DEFAULT '0',
  `item_size` varchar(30) NOT NULL,
  `count_name` int(11) NOT NULL DEFAULT '0',
  `yarn_composition_item1` int(11) NOT NULL DEFAULT '0',
  `yarn_composition_percentage1` double NOT NULL DEFAULT '0',
  `yarn_composition_item2` int(11) NOT NULL DEFAULT '0',
  `yarn_composition_percentage2` double NOT NULL DEFAULT '0',
  `fabric_composition` varchar(255) NOT NULL,
  `fabric_construction` varchar(255) NOT NULL,
  `yarn_type` int(11) NOT NULL DEFAULT '0',
  `gsm` double NOT NULL DEFAULT '0',
  `dia_width` varchar(50) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `uom` int(5) NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `net_pi_rate` double NOT NULL DEFAULT '0',
  `net_pi_amount` double NOT NULL DEFAULT '0',
  `service_type` int(4) NOT NULL DEFAULT '0',
  `brand_supplier` varchar(150) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `pi_id` (`pi_id`),
  KEY `work_order_no` (`work_order_id`),
  KEY `item_group` (`item_group`),
  KEY `color_name` (`color_id`),
  KEY `size_id` (`size_id`),
  KEY `item_size` (`item_size`),
  KEY `count_name` (`count_name`),
  KEY `yarn_type` (`yarn_type`),
  KEY `service_type` (`service_type`),
  KEY `is_deleted` (`is_deleted`),
  KEY `status_active` (`status_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_pi_item_details_history`
--

LOCK TABLES `com_pi_item_details_history` WRITE;
/*!40000 ALTER TABLE `com_pi_item_details_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_pi_item_details_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
