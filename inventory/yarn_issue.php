<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Yarn Issue Entry
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	07-05-2013
Updated by 		: 	Kausar	
Update date		: 	29-10-2013	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Issue Info","../", 1, 1, $unicode,1,1); 

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

 	
// popup for booking no ----------------------	
function popuppage_fabbook()
{
	if( form_validation('cbo_company_name*cbo_issue_purpose','Company Name*Issue Purpose')==false )
	{
		return;
	}
	
	var company			 = $("#cbo_company_name").val();
	var issue_purpose	 = $("#cbo_issue_purpose").val();
	var page_link='requires/yarn_issue_controller.php?action=fabbook_popup&company='+company+'&issue_purpose='+issue_purpose;
	var title="K&D Information";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=400px, center=1, resize=0, scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var bookingNumber = this.contentDoc.getElementById("hidden_booking_number").value; //bookingID_bookingNo_buyerID_jobNo
		if (bookingNumber!="")
		{
			bookingNumber = bookingNumber.split("_"); 
			freeze_window(5);
			//alert (bookingNumber[3]);
			$("#txt_booking_id").val(bookingNumber[0]);
			$("#txt_booking_no").val(bookingNumber[1]);
			$("#cbo_buyer_name").val(bookingNumber[2]);
			
			if(issue_purpose==2)
			{
				$("#txt_buyer_job_no").val('');
				$("#txt_style_ref").val('');
				show_list_view(bookingNumber[0],'show_yarn_dyeing_list_view','requisition_item','requires/yarn_issue_controller','');
				load_drop_down( 'requires/yarn_issue_controller',bookingNumber[0],'load_drop_down_dyeing_color', 'dyeingColor_td' );
			}
			else
			{
				$("#txt_buyer_job_no").val(bookingNumber[3]);
				$("#txt_style_ref").val(bookingNumber[4]);
				$("#dyeingColor_td").html('<?php echo create_drop_down( "cbo_dyeing_color", 142, $blank_array,"", 1, "-- Select --", 0, "", 0 ); ?>');
			}
			
			release_freezing();	 
		}
	}		
}

function openmypage_lot()
{
	if( form_validation('cbo_company_name*cbo_basis*cbo_supplier','Company Name*Basis*Supplier')==false )//*cbo_issue_purpose*Issue Purpose
	{
		return;
	}
	
	var company = $("#cbo_company_name").val();
	var supplier = $("#cbo_supplier").val();
	var issue_purpose = $("#cbo_issue_purpose").val();
	var page_link='requires/yarn_issue_controller.php?action=yarnLot_popup&company='+company+'&supplier='+supplier+'&issue_purpose='+issue_purpose;
	var title="Yarn Lot Search";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=350px, center=1, resize=0, scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var hidden_prod_id = this.contentDoc.getElementById("hidden_prod_id").value; //bookingID_bookingNo_buyerID_jobNo
		if (hidden_prod_id!="")
		{ 
			freeze_window(5); 
			$("#txt_prod_id").val(hidden_prod_id);
			get_php_form_data(hidden_prod_id+"**"+issue_purpose, "populate_data_child_from", "requires/yarn_issue_controller" );
			release_freezing();	 
		}
	}	
}

function openmypage_requis()
{
	if( form_validation('cbo_company_name*cbo_basis','Company Name*Basis')==false )
	{
		return;
	}
	
	var company = $("#cbo_company_name").val();
	var page_link='requires/yarn_issue_controller.php?action=requis_popup&company='+company;
	var title="Yarn Requisition Search";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px, height=350px, center=1, resize=0, scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var hidden_req_no = this.contentDoc.getElementById("hidden_req_no").value; 

		if (hidden_req_no!="")
		{ 
			hidden_req_no = hidden_req_no.split(","); 
			freeze_window(5); 
			
			$('#txt_req_no').val(hidden_req_no[0]);
			show_list_view(hidden_req_no[0]+','+hidden_req_no[1]+','+hidden_req_no[2],'show_req_list_view','requisition_item','requires/yarn_issue_controller','');
			release_freezing();	 
		}
	}	
}

function openmypage_po() // issue quantity
{
	var purpose = $("#cbo_issue_purpose").val();
	var receive_basis=$('#cbo_basis').val();
	var booking_no=$('#txt_booking_no').val();
	var cbo_company_id = $('#cbo_company_name').val();
 	var save_data = $('#save_data').val();
	var all_po_id = $('#all_po_id').val();
	var issueQnty = $('#txt_issue_qnty').val(); 
	var retnQnty = $('#txt_returnable_qty').val(); 
	var distribution_method = $('#distribution_method_id').val();
	var job_no=$('#job_no').val();
	
	if(form_validation('cbo_company_name*cbo_basis*cbo_issue_purpose','Company*Basis*Issue Purpose')==false)
	{
		return;
	}  
	else if(receive_basis==1 && (purpose==1 || purpose==2 || purpose==4) )
	{ 
		if( form_validation('txt_booking_no','Booking')==false )
		{
			return;
		}
	}
	
	if(receive_basis==1 && purpose==2 && job_no=="")
	{ 
		alert("Please Select Job From Right Side List View");
		return;
	}
	 
	var title = 'PO Info';	
	var page_link = 'requires/yarn_issue_controller.php?receive_basis='+receive_basis+'&cbo_company_id='+cbo_company_id+'&booking_no='+booking_no+'&all_po_id='+all_po_id+'&save_data='+save_data+'&issueQnty='+issueQnty+'&retnQnty='+retnQnty+'&distribution_method='+distribution_method+'&job_no='+job_no+'&issue_purpose='+purpose+'&action=po_popup';
	  
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=370px,center=1,resize=1,scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var save_string=this.contentDoc.getElementById("save_string").value;	 
		var tot_issue_qnty=this.contentDoc.getElementById("tot_grey_qnty").value;  //this is issue qnty 
		var tot_retn_qnty=this.contentDoc.getElementById("tot_retn_qnty").value;  //this is returnable qnty 
 		var all_po_id=this.contentDoc.getElementById("all_po_id").value;  
		var distribution_method=this.contentDoc.getElementById("distribution_method").value;
		
		$('#save_data').val(save_string);
		$('#txt_issue_qnty').val(tot_issue_qnty);
		$('#txt_returnable_qty').val(tot_retn_qnty);
 		$('#all_po_id').val(all_po_id);
		$('#distribution_method_id').val(distribution_method);
	}
}

function fn_room_rack_self_box()
{
	if( $("#cbo_room").val()!=0 )  
		disable_enable_fields( 'cbo_rack', 0, '', '' ); 
	else
	{
		reset_form('','','cbo_rack*cbo_self*cbo_binbox','','','');
		disable_enable_fields( 'cbo_rack*cbo_self*cbo_binbox', 1, '', '' ); 
	}
	if( $("#cbo_rack").val()!=0 )  
		disable_enable_fields( 'cbo_self', 0, '', '' ); 
	else
	{
		reset_form('','','cbo_self*cbo_binbox','','','');
		disable_enable_fields( 'cbo_self*cbo_binbox', 1, '', '' ); 	
	}
	if( $("#cbo_self").val()!=0 )  
		disable_enable_fields( 'cbo_binbox', 0, '', '' ); 
	else
	{
		reset_form('','','cbo_binbox','','','');
		disable_enable_fields( 'cbo_binbox', 1, '', '' ); 	
	}
}

function generate_report_file(data,action,page)
{
	window.open("requires/yarn_issue_controller.php?data=" + data+'&action='+action, true );
}

function fnc_yarn_issue_entry(operation)
{
	if(operation==4)
	{
		var report_title=$( "div.form_caption" ).html();
		generate_report_file( $('#cbo_company_name').val()+'*'+$('#txt_system_no').val()+'*'+report_title+'*'+$('#txt_booking_id').val()+'*'+$('#is_approved').val(),'yarn_issue_print','requires/yarn_issue_controller');
	/*	print_report( $('#cbo_company_name').val()+'*'+$('#txt_system_no').val()+'*'+report_title+'*'+$('#txt_booking_id').val()+'*'+$('#is_approved').val(), "yarn_issue_print", "requires/yarn_issue_controller" ) */
		return;
	}
	else if(operation==2)
	{
		show_msg('13');
		return;
	}
	else
	{
		var is_approved=$('#is_approved').val();
			
		if(is_approved==1)
		{
			alert("Yarn issue is Approved. So Change Not Allowed");
			return;	
		}
		if( form_validation('cbo_company_name*cbo_basis*cbo_issue_purpose*txt_issue_date*cbo_supplier*cbo_store_name*txt_lot_no*txt_issue_qnty','Company Name*Basis*Issue Purpose*Issue Date*Supplier*Store Name*Lot No*Issue Quantity')==false )
		{
			return;
		}
		
		var purpose = parseInt($("#cbo_issue_purpose").val());
		if (purpose==1)
		{
			if(form_validation('cbo_knitting_source*cbo_knitting_company','Knitting Source*Knitting Company')==false)
			{
				return;
			}  
		}
		
		/*var check_issue_qnty = $("#txt_current_stock").val()*1+$("#hidden_p_issue_qnty").val()*1;
		if($("#txt_issue_qnty").val()*1>check_issue_qnty*1) 
		{
			alert("Issue Quantity Exced By Current Stock Quantity.");
			return;
		}*/
		
		var dataString = 'txt_system_no*cbo_company_name*cbo_basis*cbo_issue_purpose*txt_issue_date*txt_booking_no*txt_booking_id*cbo_location*cbo_knitting_source*cbo_knitting_company*cbo_supplier*cbo_store_name*txt_challan_no*cbo_other_party*cbo_buyer_name*txt_style_ref*txt_buyer_job_no*cbo_sample_type*txt_remarks*txt_req_no*txt_lot_no*cbo_yarn_count*cbo_color*cbo_room*txt_issue_qnty*txt_returnable_qty*txt_composition*cbo_brand*cbo_rack*txt_no_bag*txt_no_cone*txt_weight_per_bag*txt_weight_per_cone*cbo_yarn_type*cbo_dyeing_color*cbo_self*txt_current_stock*cbo_uom*cbo_binbox*update_id*save_data*all_po_id*txt_prod_id*job_no*cbo_ready_to_approved';
		
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
		//alert (data);return;
		freeze_window(operation);
		http.open("POST","requires/yarn_issue_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_yarn_issue_entry_reponse;
	}
}

function fnc_yarn_issue_entry_reponse()
{	
	if(http.readyState == 4) 
	{	  		
		var reponse=trim(http.responseText).split('**');
		//show_msg(reponse[0]); 
 		release_freezing();
		//alert(reponse);
		if(reponse[0]*1==20*1)
		{
			alert(reponse[1]);
			return;
		}
		else if(reponse[0]==10)
		{
			show_msg(reponse[0]);
			return;
		}
		else if(reponse[0]==11)
		{
			alert(reponse[1]);
			return;
		}
		else if(reponse[0]==0)
		{
 			show_msg(reponse[0]);
			$("#txt_system_no").val(reponse[1]);
 			//$("#tbl_master :input").attr("disabled", true);	
			disable_enable_fields( 'txt_system_no', 0, "", "" );
		}	
		else if(reponse[0]==1)
		{
			show_msg(reponse[0]);	
			//$("#tbl_master :input").attr("disabled", true);	
			disable_enable_fields( 'txt_system_no', 0, "", "" );		 
 			set_button_status(0, permission, 'fnc_yarn_issue_entry',1,1);
		}
				 	
 		$("#tbl_child").find('select,input:not([name="txt_req_no"])').val('');	
		$("#save_data").val('');
		$("#all_po_id").val('');
		$("#distribution_method_id").val('');
		show_list_view(reponse[1],'show_dtls_list_view','list_container_yarn','requires/yarn_issue_controller','');
		set_button_status(0, permission, 'fnc_yarn_issue_entry',1,1);
	}
}

function active_inactive()
{
	var basis = parseInt($("#cbo_basis").val());
	var purpose = parseInt($("#cbo_issue_purpose").val());
	if( form_validation('cbo_basis','Basis')==false )
	{
		$("#cbo_issue_purpose").val(0);
		return;
	}
	
	$("#cbo_other_party").val(0);
	$("#cbo_sample_type").val(0);
	$("#cbo_buyer_name").val(0);
	$("#cbo_knitting_source").val(0);
	$("#cbo_knitting_company").val(0);
	$("#txt_returnable_qty").val('');
	
	if(basis==1) //booking
	{
		$("#txt_issue_qnty").val('');
 		$("#txt_booking_no").val('');
		$("#save_data").val('');
		$("#all_po_id").val('');
 		$("#txt_issue_qnty").attr('readonly',true);	
		disable_enable_fields( 'txt_req_no', 1, "", "" ); // disable true
		disable_enable_fields( 'txt_booking_no*txt_lot_no*cbo_other_party*cbo_sample_type*cbo_buyer_name', 0, "", "" ); // disable false
	}
	else if(basis==3) //requisition
	{
		$("#txt_issue_qnty").val('');
 		$("#txt_booking_no").val('');
		$("#save_data").val('');
		$("#all_po_id").val('');
		
 		$("#txt_issue_qnty").attr('readonly',true);			
		disable_enable_fields( 'txt_req_no', 0, "", "" ); // disable false
		disable_enable_fields( 'txt_booking_no*txt_lot_no*cbo_other_party*cbo_sample_type*cbo_buyer_name', 1, "", "" ); // disable true
	}
	else //idependent
	{
		$("#txt_booking_no").val('');
		$("#save_data").val('');
		$("#all_po_id").val('');
		$("#txt_issue_qnty").removeAttr('readonly');
		disable_enable_fields( 'txt_lot_no*cbo_other_party*cbo_sample_type*cbo_buyer_name', 0, "", "" ); // disable false
 		disable_enable_fields( 'txt_booking_no*txt_req_no', 1, "", "" ); // disable false
	}	 
		 
	if(purpose==2)
	{
		document.getElementById('knit_source').innerHTML='Dyeing Source';
		document.getElementById('knit_com').innerHTML='Dyeing Company';
	}
	else
	{
		document.getElementById('knit_source').innerHTML='Knitting Source';
		document.getElementById('knit_com').innerHTML='Knitting Company';
	}	 
	
	$("#txt_issue_qnty").attr('placeholder','Double Click');
	$("#txt_issue_qnty").attr('ondblclick','openmypage_po()');
 	$("#txt_issue_qnty").attr('readOnly',true);
	$("#txt_returnable_qty").attr('readOnly',true);
	$("#txt_returnable_qty").attr('placeholder','Display');
 	//$issue_basis=array(1=>"Booking",2=>"Independent");
	//$yarn_issue_purpose=array(1=>"Knitting",2=>"Yarn Dyeing",3=>"Sales",4=>"Sample",5=>"Loan",6=>"Sample-material", 7=>"Yarn Test", 8=>"Sample-No Order");
 	if(basis==1 && (purpose==1 || purpose==12 || purpose==26 || purpose==29))
	{ 
		disable_enable_fields( 'txt_booking_no*cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_sample_type*cbo_other_party*cbo_buyer_name', 1, "", "" ); // disable true
	}
	else if(basis==1 && purpose==2)
	{
		disable_enable_fields( 'cbo_sample_type*cbo_other_party*cbo_buyer_name', 1, "", "" ); // disable true
		$("#cbo_other_party").val(0);
		$("#cbo_sample_type").val(0);
		$("#cbo_buyer_name").val(0);
	}
	else if(basis==1 && (purpose==3 || purpose==15 || purpose==30))
	{
		disable_enable_fields( 'cbo_sample_type*cbo_other_party', 1, "", "" ); // disable true
		disable_enable_fields( 'cbo_buyer_name', 0, "", "" ); // disable false
		$("#cbo_other_party").val(0);
		$("#cbo_sample_type").val(0);
	}
	else if(basis==1 && purpose==4)
	{
 		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_other_party*cbo_buyer_name', 1, "", "" ); // disable true
		$("#txt_issue_qnty").attr('placeholder','Entry/Double Click');
		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
	}
	else if(basis==1 && purpose==8)
	{ 
  		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_knitting_source*cbo_knitting_company*cbo_buyer_name', 0, "", "" ); // disable false	
		disable_enable_fields( 'cbo_other_party*cbo_buyer_name', 1, "", "" ); // disable true	
		$("#txt_issue_qnty").attr('placeholder','Entry');
		$("#txt_issue_qnty").removeAttr('ondblclick');
 		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
 	}
	else if(basis==2 && (purpose==1 || purpose==12 || purpose==26 || purpose==29))
	{
 		disable_enable_fields( 'cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_other_party*cbo_buyer_name', 1, "", "" ); // disable true
	}
	else if(basis==2 && purpose==2)
	{
 		disable_enable_fields( 'cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_other_party*cbo_buyer_name', 1, "", "" ); // disable true
		
		$("#txt_issue_qnty").attr('placeholder','Double Click');
		$("#txt_issue_qnty").attr('ondblclick','openmypage_po()');
		$("#txt_issue_qnty").attr('readOnly',true);
	}
	else if(basis==2 && (purpose==3 || purpose==15 || purpose==30))
	{
 		disable_enable_fields( 'cbo_buyer_name*cbo_other_party', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_knitting_source*cbo_knitting_company*cbo_other_party', 1, "", "" ); // disable true
		
		if(purpose==15)
		{
			disable_enable_fields( 'cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		}

		$("#txt_issue_qnty").attr('placeholder','Entry');
		$("#txt_issue_qnty").removeAttr('ondblclick');
 		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
	}
	else if(basis==2 && purpose==4)
	{
 		disable_enable_fields( 'cbo_sample_type*cbo_buyer_name*cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		disable_enable_fields( 'txt_booking_no*cbo_other_party', 1, "", "" ); // disable true
	}
	else if(basis==2 && purpose==5)
	{
 		disable_enable_fields( 'cbo_other_party', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_buyer_name*cbo_knitting_source*cbo_knitting_company', 1, "", "" ); // disable true
		$("#txt_issue_qnty").attr('placeholder','Entry');
		$("#txt_issue_qnty").removeAttr('ondblclick');
 		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
	}
	else if(basis==2 && purpose==6)
	{ 
		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_buyer_name*cbo_knitting_source*cbo_knitting_company*cbo_other_party', 1, "", "" ); // disable true
		$("#txt_issue_qnty").attr('placeholder','Entry');
		$("#txt_issue_qnty").removeAttr('ondblclick');
 		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
	}
	else if(basis==2 && purpose==7)
	{ 
		disable_enable_fields( 'cbo_sample_type*txt_booking_no*cbo_buyer_name*cbo_knitting_source*cbo_knitting_company*cbo_other_party*cbo_buyer_name', 1, "", "" ); // disable true
		$("#txt_issue_qnty").attr('placeholder','Entry');
		$("#txt_issue_qnty").removeAttr('ondblclick');
 		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
	}
	else if(basis==2 && purpose==8)
	{
 		disable_enable_fields( 'cbo_sample_type*cbo_buyer_name*cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_other_party', 1, "", "" ); // disable true
		$("#txt_issue_qnty").attr('placeholder','Entry');
		$("#txt_issue_qnty").removeAttr('ondblclick');
 		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
	}
	else if(basis==2 && purpose==10)
	{
 		disable_enable_fields( 'cbo_other_party*cbo_buyer_name*cbo_knitting_source*cbo_knitting_company', 0, "", "" ); // disable false
		disable_enable_fields( 'cbo_sample_type', 1, "", "" ); // disable true
		$("#txt_issue_qnty").attr('placeholder','Entry');
		$("#txt_issue_qnty").removeAttr('ondblclick');
 		$("#txt_issue_qnty").removeAttr('readOnly');
		$("#txt_returnable_qty").removeAttr('readOnly');
		$("#txt_returnable_qty").attr('placeholder','Entry');
	}
	
	if(purpose==3)
	{
		load_drop_down( 'requires/yarn_issue_controller',document.getElementById('cbo_company_name').value+'_'+0, 'load_drop_down_buyer', 'buyer_td_id' );
	}
	else
	{
		load_drop_down( 'requires/yarn_issue_controller',document.getElementById('cbo_company_name').value+'_'+1, 'load_drop_down_buyer', 'buyer_td_id' );
	}
}

function open_mrrpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/yarn_issue_controller.php?action=mrr_popup&company='+company; 
	var title="Search MRR Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=370px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var sysNumber=this.contentDoc.getElementById("hidden_sys_number").value.split(","); // system number
		//alert(sysNumber[1]);
 		$("#txt_system_no").val(sysNumber[0]);
		$("#is_approved").val(sysNumber[1]);
		
		// master part call here
		get_php_form_data(sysNumber[0], "populate_data_from_data", "requires/yarn_issue_controller");
		//$("#tbl_master").find('input,select').attr("disabled", true);	
		//disable_enable_fields( 'txt_system_no', 0, "", "" );	
 		//list view call here
		show_list_view(sysNumber[0],'show_dtls_list_view','list_container_yarn','requires/yarn_issue_controller','');
		set_button_status(0, permission, 'fnc_yarn_issue_entry',1,1);
 	}
}

//form reset/refresh function here
function fnResetForm()
{
	$("#tbl_master").find('input').attr("disabled", false);	
	//disable_enable_fields( 'cbo_company_name*cbo_basis*cbo_receive_purpose*cbo_store_name', 0, "", "" );
	$("#dyeingColor_td").html('<?php echo create_drop_down( "cbo_dyeing_color", 142, $blank_array,"", 1, "-- Select --", 0, "", 0 ); ?>');
	$("#tbl_master").find('input,select').attr("disabled", false);
	set_button_status(0, permission, 'fnc_yarn_issue_entry',1);
	reset_form('yarn_issue_1','list_container_yarn','','','','cbo_uom');
}

function generate_report_req(req_id)
{
	if ($("#txt_req_no").val()=="")
	{
		alert ("Please Select Requisition Number.");
		return;
	}
	else
	{
		if ($("#cbo_basis").val()==3)
		{
			//alert (req_id);
				generate_report_file( $("#cbo_company_name").val()+'_'+req_id+'_'+$('#txt_system_no').val(),'requisition_print','requires/yarn_issue_controller');
			//print_report($("#cbo_company_name").val()+'_'+req_id+'_'+$('#txt_system_no').val(), "requisition_print", "requires/yarn_issue_controller" ) ;
		}
		else
		{
			alert ("Basis is not Requisition.");
			return;
		}
	}
}

function generate_report_widthout_prog(i)
{
	if ($("#cbo_basis").val()==3)
	{
		var report_title=$( "div.form_caption" ).html();
		print_report( $('#cbo_company_name').val()+'*'+$('#txt_system_no').val()+'*'+report_title+'*'+$('#txt_booking_id').val()+'*'+$('#is_approved').val()+'*'+i, "yarn_issue_print", "requires/yarn_issue_controller" ) 
	}
	else
	{
		alert ("Basis is not Requisition.");
		return;
	}
}

function load_list_view (str)
{
	show_list_view(str+','+$("#cbo_company_name").val()+','+$("#cbo_buyer_name").val(),'show_req_list_view','requisition_item','requires/yarn_issue_controller','');
}

</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../",$permission);  ?><br />    		 
    <form name="yarn_issue_1" id="yarn_issue_1" autocomplete="off" > 
    
    <div style="width:980px; float:left; position:relative" align="center">       
    <table width="80%" cellpadding="0" cellspacing="2">
     	<tr>
        	<td width="100%" align="center" valign="top">   
            	<fieldset style="width:980px;">
                <legend>Yarn Issue</legend>
                <br />
                 	<fieldset style="width:950px;">                                       
                        <table  width="950" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                            <tr>
                           		<td colspan="6" align="center"><b>System ID</b>
                                	<input type="text" name="txt_system_no" id="txt_system_no" class="text_boxes" style="width:160px" placeholder="Double Click To Search" onDblClick="open_mrrpopup()" readonly />&nbsp;&nbsp;
                                </td>
                           </tr>
                           <tr>
                                <td  width="120" align="right" class="must_entry_caption">Company Name </td>
                                <td width="170">
                                    <?php 
                                     echo create_drop_down( "cbo_company_name", 170, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/yarn_issue_controller', this.value, 'load_drop_down_location', 'location_td' );load_drop_down( 'requires/yarn_issue_controller', this.value, 'load_drop_down_store', 'store_td' );load_drop_down( 'requires/yarn_issue_controller', this.value, 'load_drop_down_supplier', 'supplier' );load_drop_down( 'requires/yarn_issue_controller',this.value+'_'+1, 'load_drop_down_buyer', 'buyer_td_id' );" );
                                    ?>
                                </td>
                                <td width="120" align="right" class="must_entry_caption">Basis</td>
                                <td width="160">
                                    <?php 
                                        echo create_drop_down( "cbo_basis", 170, $issue_basis,"", 1, "-- Select Basis --", $selected, "active_inactive();", "", "");
                                        //load_drop_down( 'requires/yarn_issue_controller', this.value, 'load_drop_down_purpose', 'issue_purpose_td' );
                                    ?>
                                </td>
                                <td width="120" align="right" class="must_entry_caption">Issue Purpose</td>
                                <td width="" id="issue_purpose_td">
                                    <?php 
                                     echo create_drop_down( "cbo_issue_purpose", 170, $yarn_issue_purpose,"", 1, "-- Select Purpose --", $selected, "active_inactive()","","","","","9,10,11,13,14,16,27,28");
                                    ?>
                                </td>
                            </tr>
                            <tr>                           
                                <td  width="" align="right" class="must_entry_caption">Issue Date </td>
                                <td width="">
                                   <input type="text" name="txt_issue_date" id="txt_issue_date" class="datepicker" style="width:160px;" placeholder="Select Date" readonly />
                                </td>
                                <td width="" align="right" >Fab Booking No</td>
                                <td width="">
                                    <input name="txt_booking_no" id="txt_booking_no" class="text_boxes" style="width:160px"  placeholder="Double Click to Search" onDblClick="popuppage_fabbook();" readonly />
                                    <input type="hidden" name="txt_booking_id" id="txt_booking_id" />
                                </td>
                                <td width="" align="right">Location</td>
                                <td width="" id="location_td">
                                    <?php 
                                    echo create_drop_down( "cbo_location", 170, $blank_array,"", 1, "-- Select Location --", "", "" );
                                    ?>
                                </td>
                            </tr>
                            <tr>                          
                                <td width="" align="right" id="knit_source">Knitting Source</td>
                                <td width="170">
                                <?php
                                    echo create_drop_down( "cbo_knitting_source", 170, $knitting_source,"", 1, "-- Select --", $selected, "load_drop_down( 'requires/yarn_issue_controller', this.value+'**'+$('#cbo_company_name').val()+'**'+$('#cbo_issue_purpose').val(), 'load_drop_down_knit_com', 'knitting_company_td' );","","1,3" );
                                ?>
                                <td width="" align="right" id="knit_com"> Issue To </td>
                                <td width="" id="knitting_company_td">
                                <?php
                                    echo create_drop_down( "cbo_knitting_company", 170, $blank_array,"", 1, "-- Select --", $selected, "","","" );
                                ?>	
                                </td>
                                <td width="" align="right" class="must_entry_caption">Supplier</td>
                                <td width="" id="supplier">
                                <?php
                                   echo create_drop_down( "cbo_supplier", 170, $blank_array,"", 1, "-- Select --", 0, "",0 );
                                ?>
                                </td>
                            </tr>
                            <tr>
                           
                               <td  width="" align="right" >Challan/Program No</td>
                               <td width="">
                                    <input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:160px" placeholder="Entry" >
                       	       </td>
                               <td width="" align="right" >Other Party</td>
                               <td width="">
                                <?php 
                                    echo create_drop_down( "cbo_other_party", 170, "select id,other_party_name from lib_other_party where status_active=1 and is_deleted=0 order by other_party_name","id,other_party_name", 1, "-- Select --", $selected, "" );
                                ?>                                   
                             	</td>
                               <td width="" align="right">Sample Type</td>
                               <td width=""><?php 
                                     echo create_drop_down( "cbo_sample_type", 170, "select id,sample_name from lib_sample where status_active=1 and is_deleted=0 order by sample_name","id,sample_name", 1, "-- Select --", $selected, "","","" );
                                    ?>
                             	</td>                                   
                             </tr>
                             <tr>
                                <td align="right">Buyer Name</td>
                                <td id="buyer_td_id">
                                    <?php
                                       echo create_drop_down( "cbo_buyer_name", 170, $blank_array,"", 1, "-- Select Buyer --", 0, "",1 );
                                    ?>
                                </td>
                                <td align="right" >Style Reference</td>
                                <td>
                                    <input  type="text" name="txt_style_ref" id="txt_style_ref" class="text_boxes" style="width:160px"  readonly placeholder="Display" />
                                </td>
                                <td align="right">Buyer Job No</td>
                                <td>
                                    <input type="text" name="txt_buyer_job_no" id="txt_buyer_job_no" class="text_boxes" style="width:160px" readonly placeholder="Display" />
                                </td>
                             </tr>
                             <tr>
                                <td align="right">Remarks</td>
                                <td colspan="3" ><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:460px"  placeholder="Entry" /></td>
                                <td align="right">Ready to Approve</td>
                                <td>
                                <?php
							   		echo create_drop_down( "cbo_ready_to_approved", 172, $yes_no,"", 1, "-- Select--", 2, "","","" );
							   ?>
                                </td>
                             </tr>
                             <tr>
                                <td align="right">&nbsp;</td>
                                <td colspan="3">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td>&nbsp;</td>
                             </tr> 
                        </table>
                    </fieldset>
                    <br />
                    <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                    <tr>
                    <td width="49%" valign="top">
                    	<fieldset style="width:950px;">  
                        <legend>New Issue Item</legend>                                     
                            <table width="100%" cellspacing="2" cellpadding="0" border="0">
                                <tr>                               
                                    <td width="110" align="right">Requisition. No</td>
                                    <td>
                                    	<input type="text" name="txt_req_no" id="txt_req_no"  class="text_boxes" onDblClick="openmypage_requis()" placeholder="Browse or Write" style="width:150px;" onBlur="load_list_view(this.value);" />
                                  	</td>
                                    <td align="right">No. Of Cone</td>
                                    <td><input  type="text" name="txt_no_cone" id="txt_no_cone"class="text_boxes_numeric"  style="width:130px;" placeholder="Entry" /></td>
                                    <td align="right">Composition</td>   
                                    <td><input type="text" name="txt_composition" id="txt_composition" class="text_boxes" style="width:150px;" placeholder="Display" readonly ></td>
                                    <td align="right">UOM</td>
                                    <td><?php echo create_drop_down( "cbo_uom", 110, $unit_of_measurement,"", 1, "--Select--", $selected, "",1 ); ?></td>
                                </tr>
                                <tr>
                                    <td width="110" align="right" class="must_entry_caption">Lot No</td>
                                    <td><input type="text" name="txt_lot_no" id="txt_lot_no"  class="text_boxes" onDblClick="openmypage_lot()"  placeholder="Double Click" style="width:150px;" readonly />
                                    <input type="hidden" name="txt_prod_id" id="txt_prod_id"  readonly /></td>
                                    <td align="right">Weight per Bag</td>   
                                    <td>
                                        <input name="txt_weight_per_bag" id="txt_weight_per_bag" class="text_boxes_numeric" type="text" style="width:130px;" placeholder="Entry"/>
                                    </td>
                                    <td align="right">Yarn Type</td>   
                                    <td><?php echo create_drop_down( "cbo_yarn_type", 162, $yarn_type,"", 1, "--Select--", 0, "",1 ); ?></td>
                                    <td align="right">Room</td>
                                    <td>
                                     	<?php
											echo create_drop_down( "cbo_room", 110, "select room,room from inv_transaction where status_active=1 and room!=0 group by room order by room","room,room", 1, "--Select--", "", "fn_room_rack_self_box()",0 );
										?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="must_entry_caption">Issue Qty.</td>
                                    <td>
                                        <input type="text" name="txt_issue_qnty" id="txt_issue_qnty" class="text_boxes_numeric"  style="width:150px;" placeholder="Double Click" readonly onDblClick="openmypage_po()" />
                                        <input type="hidden" name="hidden_p_issue_qnty" id="hidden_p_issue_qnty" readonly />
                                    </td>
                                    <td align="right">Wght @ Cone</td>
                                    <td><input class="text_boxes_numeric"  name="txt_weight_per_cone" id="txt_weight_per_cone" type="text" style="width:130px;" placeholder="Entry"/></td>
                                    <td align="right">Color</td>
                                    <td>
										<?php
											echo create_drop_down( "cbo_color", 162, "select id,color_name from lib_color","id,color_name", 1, "--Select--", "", "",1 );
										?>
                                    </td>
                                    <td align="right">Rack</td>
                                    <td>
                                    	<?php
											echo create_drop_down( "cbo_rack", 110, "select rack,rack from inv_transaction where status_active=1 and rack!=0 group by rack order by rack","rack,rack", 1, "--Select--", "", "fn_room_rack_self_box()",1 );
										?>
                                    </td>
                                </tr>
                                <tr>
                                	<td align="right">Current Stock</td>
                                	<td><input type="text" name="txt_current_stock" id="txt_current_stock" class="text_boxes_numeric" style="width:150px;" placeholder="Display" readonly /></td>
                                    <td align="right" class="must_entry_caption">Store Name</td>
                                	<td id="store_td">
										<?php 
                                           echo create_drop_down( "cbo_store_name", 142, $blank_array,"", 1, "-- Select Store --", 0, "", 0 );
                                        ?>
                                 	</td> 
                                    <td align="right">Brand</td>
                                    <td>
                                    	<?php
											echo create_drop_down( "cbo_brand", 162, "select id,brand_name from lib_brand","id,brand_name", 1, "--Select--", "", "",1 );
										?>
                                    </td>
                                    <td align="right">Self</td>
                                    <td>
                                    	<?php
											echo create_drop_down( "cbo_self", 110, "select self,self from inv_transaction where status_active=1 and self!=0 group by self order by self","self,self", 1, "--Select--", "", "fn_room_rack_self_box()",1 );
										?>
                              		</td>
                                </tr>
                                <tr>
                                  	<td align="right">No. Of Bag</td>
                                  	<td><input  type="text" name="txt_no_bag" id="txt_no_bag"class="text_boxes_numeric"  style="width:150px;" placeholder="Entry" /></td>
                                    <td align="right">Dyeing Color</td>
                                  	<td id="dyeingColor_td"><?php echo create_drop_down( "cbo_dyeing_color", 142, $blank_array,"", 1, "-- Select --", 0, "", 0 ); ?></td>
                                  	<td align="right">Yarn Count</td>
                                  	<td>
										<?php
											echo create_drop_down( "cbo_yarn_count", 162, "select id,yarn_count from lib_yarn_count where is_deleted = 0 AND status_active = 1 ORDER BY yarn_count ASC","id,yarn_count", 1, "--Select--", 0, "",1 );
										?>
                                  	</td>
                                  	<td align="right">Bin/Box</td>
                                  	<td>
										<?php
                                            echo create_drop_down( "cbo_binbox", 110, "select bin_box,bin_box from inv_transaction where status_active=1 and bin_box!=0 group by bin_box order by bin_box","bin_box,bin_box", 1, "--Select--", "", "",1 );
                                        ?>
                                	</td>
                        		</tr>
                                <tr>
                                    <td align="right">Returnable Qty.</td>
                                  	<td><input type="text" name="txt_returnable_qty" id="txt_returnable_qty" class="text_boxes_numeric" placeholder="Display" style="width:150px;" readonly /></td>
                        		</tr>
                    		</table>
                    	</fieldset>
                    </td>
                    </tr>
                </table>                
               	<table cellpadding="0" cellspacing="1" width="100%">
                	<tr> 
                       <td colspan="6" align="center"></td>				
                	</tr>
                	<tr>
                        <td align="center" colspan="6" valign="middle" class="button_container"><div id="approved" style="float:left; font-size:24px; color:#FF0000;"></div>
                             <!-- details table id for update --> 
                             <input type="hidden" id="is_approved" name="is_approved" value="" readonly />                            
                             <input type="hidden" id="update_id" name="update_id" readonly />
                             <input type="hidden" name="save_data" id="save_data" readonly  />	
                             <input type="hidden" name="all_po_id" id="all_po_id" readonly />
                             <input type="hidden" name="distribution_method_id" id="distribution_method_id" readonly />
                             <input type="hidden" name="job_no" id="job_no" readonly /><!--For Basis Bokking and Yarn Dyeing Purpose-->
							 <?php echo load_submit_buttons( $permission, "fnc_yarn_issue_entry", 0,1,"fnResetForm()",1);?>&nbsp;
                            <input type="button" name="search" id="search" value="Requisition Details" onClick="generate_report_req(document.getElementById('txt_req_no').value)" style="width:130px" class="formbutton" />
                            <input type="button" name="without_prog" id="without_prog" value="Without Program" onClick="generate_report_widthout_prog(1)" style="width:130px" class="formbutton" />
                        </td>
                   </tr> 
                </table>                 
              	</fieldset>
              	<fieldset>
    			<div style="width:970px;" id="list_container_yarn"></div>
    		  	</fieldset>
           </td>
         </tr>
    </table>
    </div>
    
    <div style="float:left; position:relative; margin-left:15px" align="left" id="requisition_item"></div>
    
	</form>
</div>    
</body>  
<script src="../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
