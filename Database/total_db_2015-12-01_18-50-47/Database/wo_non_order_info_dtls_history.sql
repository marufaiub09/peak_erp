-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_non_order_info_dtls_history`
--

DROP TABLE IF EXISTS `wo_non_order_info_dtls_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_non_order_info_dtls_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) NOT NULL,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `requisition_dtls_id` int(11) NOT NULL DEFAULT '0',
  `po_breakdown_id` int(11) NOT NULL DEFAULT '0',
  `requisition_no` varchar(30) NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `yarn_count` int(11) NOT NULL DEFAULT '0',
  `yarn_comp_type1st` int(11) NOT NULL DEFAULT '0',
  `yarn_comp_percent1st` int(11) NOT NULL DEFAULT '0',
  `yarn_comp_type2nd` int(11) NOT NULL DEFAULT '0',
  `yarn_comp_percent2nd` int(11) NOT NULL DEFAULT '0',
  `yarn_type` int(11) NOT NULL DEFAULT '0',
  `color_name` int(11) NOT NULL DEFAULT '0',
  `req_quantity` int(11) NOT NULL DEFAULT '0',
  `supplier_order_quantity` double NOT NULL DEFAULT '0',
  `uom` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `inserted_by` tinyint(3) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` tinyint(3) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_non_order_info_dtls_history`
--

LOCK TABLES `wo_non_order_info_dtls_history` WRITE;
/*!40000 ALTER TABLE `wo_non_order_info_dtls_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_non_order_info_dtls_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
