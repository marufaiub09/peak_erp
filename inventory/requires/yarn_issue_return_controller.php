<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------
if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location", 170, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select Location --", $selected, "",0 );     	 
	exit();
}

if ($action=="load_drop_down_store")
{	  
	echo create_drop_down( "cbo_store_name", 100, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=1 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "-- Select --",0,"",0);  	 
	exit();
}

if ($action=="load_drop_down_knit_com")
{
	$exDataArr = explode("**",$data);	
	$knit_source=$exDataArr[0];
	$company=$exDataArr[1];
	if($company=="" || $company==0) $company_cod = ""; else $company_cod = " and id=$company";
	if($knit_source==1)
		echo create_drop_down( "cbo_knitting_company", 170, "select id,company_name from lib_company where status_active=1 and is_deleted=0 $company_cod order by company_name","id,company_name", 0, "-- Select --", $company, "" );
	else if($knit_source==3)
		echo create_drop_down( "cbo_knitting_company", 170, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$company' and b.party_type =20 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else
		echo create_drop_down( "cbo_knitting_company", 170, $blank_array,"", 1, "-- Select --", $selected, "","","" );
	exit();	
}

if ($action=="fabbook_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	
	function fn_check()
	{
			show_list_view ( document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $receive_basis; ?>, 'create_fabbook_search_list_view', 'search_div', 'yarn_issue_return_controller', 'setFilterGrid(\'list_view\',-1)');
	}
		
	function js_set_value(booking_dtls)
	{ 
 		//alert (booking_dtls);
		//$("#hidden_booking_id").val(booking_id);  
		$("#hidden_booking_number").val(booking_dtls);  
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="850" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th width="160">Buyer Name</th>
                    <th width="150">Search By</th>
                    <th width="200" align="center" id="search_by_td_up">Enter WO/Reqsn Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>
                        <?php  
						   echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "","" ); 
                        ?>
                    </td>
                    <td>
                        <?php  
							if($receive_basis==1) $search_by = array(1=>'Booking No', 2=>'Buyer Order', 3=>'Job No');
							else $search_by = array(1=>'Booking No', 2=>'Requisition No');
							
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../')";
							echo create_drop_down( "cbo_search_by", 130, $search_by, "", 0, "--Select--", "", $dd, 0,'');
                        ?>
                    </td>
                    <td align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="fn_check()" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_booking_id" value="" />
                    <input type="hidden" id="hidden_booking_number" value="" />
                    <input type="hidden" name="booking_without_order" id="booking_without_order" class="text_boxes" value=""> 
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" style="margin-top:10px" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}



if($action=="create_fabbook_search_list_view")
{
 	$ex_data = explode("_",$data);
	$buyer = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = trim($ex_data[2]);
	$txt_date_from = $ex_data[3];
	$txt_date_to = $ex_data[4];
	$company = $ex_data[5];
 	$receive_basis= $ex_data[6];
	
	if($receive_basis==1)
	{
		$sql_cond="";
		if(trim($txt_search_common)!="")
		{
			if(trim($txt_search_by)==1) // for Booking No
			{
				$sql_cond .= " and a.booking_no LIKE '%$txt_search_common%'";
				$search_field_cond_sample="and s.booking_no LIKE '%$txt_search_common%'";
			}
			else if(trim($txt_search_by)==2) // for buyer order
			{
				$sql_cond .= " and b.po_number LIKE '%$txt_search_common%'";	// wo_po_break_down		
				$search_field_cond_sample="";	
			}
			else if(trim($txt_search_by)==3) // for job no
			{
				$sql_cond .= " and a.job_no LIKE '%$txt_search_common%'";
				$search_field_cond_sample="";			
			}
		} 
		
		if( $txt_date_from!="" && $txt_date_to!="" )
		{ 
			if($db_type==0)
			{
				$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
				$search_field_cond_sample.="and s.booking_date between '".change_date_format($txt_date_from, "yyyy-mm-dd", "-")."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			}
			else
			{
				$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
				$search_field_cond_sample.="and s.booking_date like '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
			}
		}
		
		if( trim($buyer)!=0 ) $sql_cond .= " and a.buyer_id='$buyer'";
		if( trim($company)!=0 ) $sql_cond .= " and a.company_id='$company'";
		
		if( trim($buyer)!=0 ) $search_field_cond_sample .= " and s.buyer_id='$buyer'";
		if( trim($company)!=0 ) $search_field_cond_sample .= " and s.company_id='$company'";
		
		if(trim($data[0])!="" && ($search_by==2 || $search_by==3))
		{
			$sql = "select a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no as job_no_mst, 0 as type from wo_booking_mst a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_id=$company_id and a.buyer_id=$buyer_id and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $search_field_cond group by a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no"; 
		}
		else
		{
			$sql = "select a.id,a.booking_no_prefix_num,a.booking_no,a.booking_date,a.buyer_id,a.item_category,a.delivery_date,a.po_break_down_id,a.job_no as job_no_mst,0 as type
					from wo_booking_mst a, wo_po_break_down b, inv_issue_master c 
					where 
						a.job_no=b.job_no_mst and 
						a.id=c.booking_id and
						a.item_category=2 and 
						c.item_category=1 and
						a.status_active=1 and 
						a.is_deleted=0 and 
						b.status_active=1 and 
						b.is_deleted=0 
						$sql_cond 
						group by a.id,a.booking_no_prefix_num,a.booking_no,a.booking_date,a.buyer_id,a.po_break_down_id,a.item_category,a.delivery_date,a.job_no
					union all
					select s.id, s.booking_no_prefix_num as booking_no_prefix_num, s.booking_no, s.booking_date, s.buyer_id, s.item_category, s.delivery_date, null as po_break_down_id, null as job_no_mst, 1 as type
					from wo_non_ord_samp_booking_mst s 
					where 
						s.item_category=2 and 
						s.status_active=1 and 
						s.is_deleted=0 
						$search_field_cond_sample 
						group by s.id, s.booking_no_prefix_num, s.booking_no, s.booking_date, s.buyer_id, s.item_category, s.delivery_date
						"; 
		}
		//item_category=2 knit fabrics
		//echo $sql;//die;
		$result = sql_select($sql);
		$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
		?>
		<div align="left">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" >
                <thead>
                    <th width="30">SL</th>
                    <th width="105">Booking No</th>
                    <th width="90">Book. Date</th>               
                    <th width="100">Buyer</th>
                    <th width="90">Item Cat.</th>
                    <th width="90">Job No</th>
                    <th width="90">Order Qnty</th>
                    <th width="80">Ship. Date</th>
                    <th >Order No</th>
                </thead>
            </table>
            <div style="width:990px; max-height:240px; overflow-y:scroll" id="list_container_batch" >	 
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" id="list_view">  
                <?php
                    $i=1;
                    foreach ($result as $row)
                    {  
                        if ($i%2==0)  
                            $bgcolor="#E9F3FF";
                        else
                            $bgcolor="#FFFFFF";	 
					 if($row[csf('type')]==0)
					 {
                        $po_qnty_in_pcs=0; $po_no=''; $min_shipment_date='';
						if($row[csf('po_break_down_id')]!='')
						{
                        	$po_sql = "select a.style_ref_no, b.po_number, b.pub_shipment_date, b.po_quantity, a.total_set_qnty from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in (".$row[csf('po_break_down_id')].")";	
						}
                        $nameArray=sql_select($po_sql);
                        $style_ref_no="";
                        foreach ($nameArray as $po_row)
                        {
                            if($po_no=="") $po_no=$po_row[csf('po_number')]; else $po_no.=",".$po_row[csf('po_number')];
                            
                            if($min_shipment_date=='')
                            {
                                $min_shipment_date=$po_row[csf('pub_shipment_date')];
                            }
                            else
                            {
                                if($po_row[csf('pub_shipment_date')]<$min_shipment_date) $min_shipment_date=$po_row[csf('pub_shipment_date')]; else $min_shipment_date=$min_shipment_date;
                            }
                            
                            $po_qnty_in_pcs+=$po_row[csf('po_quantity')]*$po_row[csf('total_set_qnty')];
                            $style_ref_no = $po_row[csf('style_ref_no')];
                        }
					 }
                    ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $receive_basis; ?>_<?php echo $row[csf('id')]; ?>_<?php echo $row[csf('booking_no')]; ?>_<?php echo $row[csf('type')]; ?>');"> 
                            <td width="30"><?php echo $i; ?></td>
                            <td width="105"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                            <td width="90" align="center"><?php echo change_date_format($row[csf('booking_date')]); ?></td>               
                            <td width="100"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                            <td width="90"><p><?php echo $item_category[$row[csf('item_category')]]; ?></p></td>
                            <td width="90"><p><?php if($row[csf('type')]==0) echo $row[csf('job_no_mst')]; ?></p></td>
                            <td width="90" align="right"><?php if($row[csf('type')]==0) echo $po_qnty_in_pcs; ?></td>
                            <td width="80" align="center"><?php if($row[csf('type')]==0) echo change_date_format($min_shipment_date); ?></td>
                            <td><p><?php if($row[csf('type')]==0) echo $po_no; ?></p></td>
                        </tr>
                    <?php
                    $i++;
                    }
                    ?>
                </table>
			</div>
		</div>
	<?php	
	}
	else
	{
		$po_array=return_library_array("select id, po_number from wo_po_break_down","id","po_number");
		if($db_type==0)
		{
			$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company group by dtls_id", "dtls_id", "po_id"  );
		}
		else
		{
			$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company group by dtls_id", "dtls_id", "po_id"  );
		}
		
		if($buyer==0) $buyer_cond= "%%"; else $buyer_cond=$buyer;
		
		if($txt_search_by==1)
			$search_field= " a.booking_no";	
		else
			$search_field= " c.requisition_no";	
		 
		$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
		
		$sql = "select a.buyer_id, a.booking_no, b.knitting_source, b.knitting_party, c.requisition_no, c.prod_id, c.requisition_date, c.knit_id as knit_id, sum(c.yarn_qnty) as yarn_qnty
				from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c, inv_transaction d
				where a.id=b.mst_id and b.id=c.knit_id and d.requisition_no=c.requisition_no and d.receive_basis=3 and d.item_category=1 and a.company_id=$company and a.buyer_id like '$buyer_cond' and $search_field like '%$txt_search_common%' group by a.buyer_id, a.booking_no, b.knitting_source, b.knitting_party, c.requisition_no, c.prod_id, c.requisition_date, c.knit_id";
		//echo $sql;
		$result = sql_select($sql);
		$i=1; 
		?> 
		<div align="center">	
			<div style="width:770px;">
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" width="100%" rules="all">
					<thead>
						<tr>
							<th width="50">SL</th>
							<th width="110">Buyer</th>
							<th width="90">Reqsn. No</th>
                            <th width="130">Booking No</th>
                            <th width="250">Order No</th>
							<th>Reqsn. Date</th>
						</tr>
					</thead>
				  </table>  
			  </div>
			  <div style="width:770px; overflow-y:scroll; max-height:250px;" >
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" width="100%" rules="all">	
					<tbody>
						<?php 
						foreach($result as $row)
						{
							if ($i%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";					

							$po_id=array_unique(explode(",",$plan_details_array[$row[csf('knit_id')]]));
							$po_no=''; 
							
							foreach($po_id as $val)
							{
								if($po_no=='') $po_no=$po_array[$val]; else $po_no.=",".$po_array[$val];
							}
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value('<?php echo $receive_basis; ?>_<?php echo $row[csf('requisition_no')]; ?>_<?php echo $row[csf('requisition_no')]; ?>')" style="cursor:pointer" >
								<td width="50"><?php echo $i; ?></td>
								<td width="110"><p><?php echo $buyer_arr[$row[csf("buyer_id")]]; ?></p></td>
								<td width="90" align="center"><p><?php echo $row[csf("requisition_no")]; ?></p></td>
                                <td width="130"><p><?php echo $row[csf("booking_no")]; ?></p></td>
                                <td width="250"><p><?php echo $po_no; ?></p></td>
								<td align="center"><p><?php echo change_date_format($row[csf("requisition_date")]); ?></p></td>
						   </tr>
						<?php 
							$i++; 
						} 
						?>
					</tbody>
				</table>
		   </div>
		</div>    
    <?php
	}
	exit();		
}

if($action=="populate_knitting_source")
{
	$ex_data=explode("**",$data);
	$req_id=str_replace("'","",$ex_data[0]);
	$company_id=str_replace("'","",$ex_data[1]);
	$knit_sql=sql_select("select b.knitting_source,b.knitting_party from ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b where a.knit_id=b.mst_id and a.requisition_no='$req_id' and a.status_active=1 and b.status_active=1");
	foreach($knit_sql as $row)
	{
		echo "$('#cbo_knitting_source').val('".$row[csf("knitting_source")]."');\n";
		echo "load_drop_down( 'requires/yarn_issue_return_controller','".$row[csf("knitting_source")]."'+'**'+'".$company_id."', 'load_drop_down_knit_com', 'knitting_company_td' );;\n";
		echo "$('#cbo_knitting_company').val('".$row[csf("knitting_party")]."');\n";

	}
	
}


if($action=="po_popup")
{
	echo load_html_head_contents("PO Info", "../../", 1, 1,'','','');
	extract($_REQUEST);  
 
 	$data=explode("_",$data); 
	$po_id=$data[0]; //order ID 
	if($data[1])$type=$data[1]; else $type=0; //is popup search or not	
	$prevQnty=$data[2]; //previous input qnty po wise
	if($data[3]!="")$prev_method=$data[3]; 
	else $prev_method=$distribution_method;  
	if($data[4]!="")$issueQnty=$data[4];  
	else $issueQnty=$issueQnty; 
	 
?> 

	<script>
		var receive_basis=<?php echo $receive_basis; ?>;
		
		function fn_show_check()
		{
			if( form_validation('cbo_buyer_name','Buyer Name')==false )
			{
				return;
			}			
			show_list_view ( $('#txt_search_common').val()+'_'+$('#cbo_search_by').val()+'_'+<?php echo $cbo_company_id; ?>+'_'+$('#cbo_buyer_name').val()+'_'+'<?php echo $all_po_id; ?>'+'_'+<?php echo $receive_basis;?>, 'create_po_search_list_view', 'search_div', 'yarn_issue_return_controller', 'setFilterGrid(\'tbl_list_search\',-1);hidden_field_reset();');
			set_all();
		}
		
		function distribute_qnty(str)
		{
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_grey_qnty=$('#txt_prop_grey_qnty').val()*1;
				var tblRow = $("#tbl_list_search tr").length;
				var len=totalGrey=0;
				$("#tbl_list_search").find('tr').each(function()
				{
					len=len+1;
					var txtOrginal=$(this).find('input[name="txtOrginal[]"]').val()*1;
					if(txtOrginal==0)
					{
						$(this).remove();
					}
					else
					{
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;						
						var grey_qnty=(perc*txt_prop_grey_qnty)/100;
						
						totalGrey = totalGrey*1+grey_qnty*1;
						totalGrey = totalGrey.toFixed(2);						
						if(tblRow==len)
						{
							var balance = txt_prop_grey_qnty-totalGrey;
							if(balance!=0) grey_qnty=grey_qnty+(balance);							
						}
						$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
					}
				});
			}
			else
			{
				$('#txt_prop_grey_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					$(this).find('input[name="txtGreyQnty[]"]').val('');
				});
			}
		}
		
				
		var selected_id = new Array();
		
		 function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_po_row_id').value;
			if(old!="")
			{   
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{  
					js_set_value( old[i] ) 
				}
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#po_id').val( id );
		}
		
		function show_grey_prod_recv() 
		{ 
 			var po_id=$('#po_id').val();
			var prev_save_string=$('#prev_save_string').val();
			var prev_method=$('#prev_method').val();
			var prev_total_qnty=$('#prev_total_qnty').val();			
			show_list_view ( po_id+'_'+'1'+'_'+prev_save_string+'_'+prev_method+'_'+prev_total_qnty, 'po_popup', 'search_div', 'yarn_issue_return_controller', '');
		}
		
		function hidden_field_reset()
		{
			$('#po_id').val('');
			$('#save_string').val( '' );
			$('#tot_grey_qnty').val( '' );
			selected_id = new Array();
		}
		
		function fnc_close()
		{
			var save_string='';	 var tot_grey_qnty=''; var no_of_roll=''; 
			var po_id_array = new Array();
			
			$("#tbl_list_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtGreyQnty=$(this).find('input[name="txtGreyQnty[]"]').val();				
				tot_grey_qnty=tot_grey_qnty*1+txtGreyQnty*1;
						
				if(txtGreyQnty*1>0)
				{
					if(save_string=="")
					{
						save_string=txtPoId+"**"+txtGreyQnty;
					}
					else
					{
						save_string+=","+txtPoId+"**"+txtGreyQnty;
					}
					
					if( jQuery.inArray( txtPoId, po_id_array) == -1 ) 
					{
						po_id_array.push(txtPoId);
					}
				}
			});

			$('#save_string').val( save_string );
			$('#tot_grey_qnty').val( tot_grey_qnty );
			$('#all_po_id').val( po_id_array );
			$('#distribution_method').val( $('#cbo_distribiution_method').val() );			
			parent.emailwindow.hide();
		}
    </script>

</head>
<body>
	
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:10px">
        <?php if($type!=1){?>
        	<!-- previous data here---->
            <input type="hidden" name="prev_save_string" id="prev_save_string" class="text_boxes" value="<?php echo $save_data; ?>">
            <input type="hidden" name="prev_total_qnty" id="prev_total_qnty" class="text_boxes" value="<?php echo $issueQnty; ?>">
            <input type="hidden" name="prev_method" id="prev_method" class="text_boxes" value="<?php echo $distribution_method; ?>">
            <!--- END-------->
        	<input type="hidden" name="save_string" id="save_string" class="text_boxes" value="<?php echo $save_data; ?>">
            <input type="hidden" name="tot_grey_qnty" id="tot_grey_qnty" class="text_boxes" value="">
            <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
            <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
		<?php } ?>
	<?php   
	if($receive_basis==2)
	{
	?>
		<table cellpadding="0" cellspacing="0" width="620" class="rpt_table">
			<thead>
				<th>Buyer</th>
				<th>Search By</th>
				<th>Search</th>
				<th>
					<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
					<input type="hidden" name="po_id" id="po_id" value="">
				</th> 
			</thead>
			<tr class="general">
				<td align="center">
					<?php
						echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "","" ); 
					?>       
				</td>
				<td align="center">	
					<?php
						$search_by_arr=array(1=>"PO No",2=>"Job No");
						echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
					?>
				</td>                 
				<td align="center">				
					<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
				</td> 						
				<td align="center">
					<input type="button" name="button2" class="formbutton" value="Show" onClick="fn_show_check()" style="width:100px;" />
				</td>
			</tr>
		</table>
		<div id="search_div" style="margin-top:10px">
       	 	
        <?php
			if($all_po_id!="" || $po_id!="")
			{ 
			?>    
				
				<div style="width:600px; margin-top:10px" align="center">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
						<thead>
							<th>Total Return Qty</th>
							<th>Distribution Method</th>
						</thead>
						<tr class="general">
							<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
							<td>
								<?php
									$distribiution_method=array(1=>"Proportionately",2=>"Manually");
									echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);",0 );
								?>
							</td>
						</tr>
					</table>
				</div>
				<div style="margin-left:10px; margin-top:10px">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
						<thead>
							<th width="150">PO No</th>
							<th width="110">PO Qnty</th>
							<th width="110">Return Qnty</th>
						</thead>
					</table>
					<div style="width:600px; max-height:280px; overflow-y:scroll" id="list_container" align="left">  
						<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">   
					<?php
					 
						if($po_id=="")$po_id=$all_po_id; else $po_id=$po_id;
						$i=1; $tot_po_qnty=0;
						if($po_id!="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs 
							from wo_po_details_master a, wo_po_break_down b 
							where a.job_no=b.job_no_mst and b.id in ($po_id)";	
						}				 
						//echo $po_sql; 						
						$explSaveData = explode(",",$save_data); 
						$nameArray=sql_select($po_sql);
						foreach($nameArray as $row)
						{  
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
							
							$woQnty = explode("**",$explSaveData[$i-1]);
							if($woQnty[0]==$row[csf('id')]) $qnty = $woQnty[1]; else $qnty = "";						
							
						 ?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="150">
									<p><?php echo $row[csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
									<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
								</td>
								<td width="110" align="right">
									<?php echo $row[csf('po_qnty_in_pcs')]; ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
								</td>
								<td width="110" align="center">
									<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $qnty; ?>">
								</td>					
								
							</tr>
						<?php 
						$i++; 
						} 
						?>
						<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
					</table>                
				</div>
				<table width="620" id="table_id">
						 <tr>
							<td align="center" >
								<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
							</td>
						</tr>
					</table>
				</div>
          <?php } ?>  
            
       </div>     
	<?php
	}
	else
	{  
	?>
		<div style="width:600px; margin-top:10px" align="center">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
				<thead>
					<th>Total Return Qnty</th>
					<th>Distribution Method</th>
				</thead>
				<tr class="general">
					<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
					<td>
						<?php
							$distribiution_method=array(1=>"Proportionately",2=>"Manually");
                            echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);",0 );
						?>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:10px; margin-top:10px">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
				<thead>
					<th width="150">PO No</th>
					<th width="110">PO Qnty</th>
					<th width="110">Return Qnty</th>
				</thead>
			</table>
			<div style="width:600px; max-height:230px; overflow-y:scroll" id="list_container" align="left"> 
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">  
					<?php 
					$i=1; $tot_po_qnty=0; 
 					if($type==1 && $po_id!="")
					{
						if($booking_no=="")
						{
							$po_sql="select b.id, b.po_number, a.total_set_qnty, b.po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id)";
						}
						else
						{
							$po_sql="select b.id, b.po_number, a.total_set_qnty, b.po_quantity from wo_po_details_master a, wo_booking_dtls c, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' and b.id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by b.id, b.po_number, a.total_set_qnty, b.po_quantity";	
						}
					}
					else
					{
						if($booking_no=="")
						{
							$po_sql="select b.id, b.po_number, a.total_set_qnty, b.po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0";
						}
						else
						{ 
							if($receive_basis==1)
							{
								$po_sql="select b.id, b.po_number, a.total_set_qnty, b.po_quantity from wo_po_details_master a, wo_booking_dtls c, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by b.id, b.po_number, a.total_set_qnty, b.po_quantity";	
							}
							else
							{
								if($db_type==0)
								{
									$poID=return_field_value("group_concat(distinct(a.po_id)) as poid","ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c","b.id=a.dtls_id and b.id=c.knit_id and requisition_no='".$booking_no."'","poid");
								}
								else
								{
									$poID=return_field_value("LISTAGG(a.po_id, ',') WITHIN GROUP (ORDER BY a.po_id) as poid","ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c","b.id=a.dtls_id and b.id=c.knit_id and requisition_no='".$booking_no."'","poid");	
								}
								
								$po_sql="select b.id, b.po_number, a.total_set_qnty, b.po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($poID)";
							}
						}
					} 
					//echo $po_sql;
					if($save_string=="" && $type==1) $save_data=$prevQnty;
					$explSaveData = explode(",",$save_data);
 					$nameArray=sql_select($po_sql);
					foreach($nameArray as $row)
					{  
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
						
						$po_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('total_set_qnty')];
						$tot_po_qnty+=$po_qnty_in_pcs;
						
						$woQnty = explode("**",$explSaveData[$i-1]);
						if($woQnty[0]==$row[csf('id')]) $qnty = $woQnty[1]; else $qnty = "";						
						
					 ?>
						<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
							<td width="150">
								<p><?php echo $row[csf('po_number')]; ?></p>
								<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
								<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
							</td>
							<td width="110" align="right">
								<?php echo $po_qnty_in_pcs; ?>
								<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_qnty_in_pcs; ?>">
							</td>
							<td width="110" align="center">
								<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $qnty; ?>">
							</td>					
							
						</tr>
					<?php 
					$i++; 
					} 
					?>
					<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
				</table>
			</div>
			<table width="620" id="table_id">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
		</div>
	<?php
	}	
	?>
		</fieldset>
	</form>
        
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="create_po_search_list_view")
{
	$data = explode("_",$data);
	
	$search_string=trim($data[0]);
	$search_by=$data[1];
	
	$search_con="";
	if($search_by==1 && $search_string!="")
		$search_con = " and b.po_number like '%$search_string%'";
	else if($search_by==2 && $search_string!="")
		$search_con =" and a.job_no like '%$search_string%'"; 
		
	$company_id =$data[2];
	$buyer_id =$data[3];	
	$all_po_id=$data[4];
	$receiveBasis=$data[5];
	
	if($all_po_id!="")
		$po_id_cond=" or b.id in($all_po_id)";
	else 
		$po_id_cond="";
	
	$hidden_po_id=explode(",",$all_po_id);

	if($buyer_id==0) { echo "<b>Please Select Buyer First</b>"; die; }
	
	if($receiveBasis==1)
	{
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date 
		from wo_po_details_master a, wo_po_break_down b, wo_booking_dtls c 
		where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and a.company_name=$company_id and a.buyer_name=$buyer_id $search_con and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.id, a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date"; 
	}
	else
	{
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date 
		from wo_po_details_master a, wo_po_break_down b
		where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name=$buyer_id $search_con and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0"; //$po_id_cond

	}
	//echo $sql;die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Job No</th>
                <th width="110">Style No</th>
                <th width="110">PO No</th>
                <th width="90">PO Quantity</th>
                <th width="50">UOM</th>
                <th>Shipment Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:220px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1; $po_row_id='';
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
						
					if(in_array($selectResult[csf('id')],$hidden_po_id)) 
					{
						if($po_row_id=="") $po_row_id=$i; else $po_row_id.=",".$i;
					}
					
					$po_qnty_in_pcs=$selectResult[csf('po_quantity')]*$selectResult[csf('total_set_qnty')];
							
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
                            <td width="40" align="center"><?php echo "$i"; ?>
                             <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>	
                            </td>	
                            <td width="100"><p><?php echo $selectResult[csf('job_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                            <td width="90" align="right"><?php echo $po_qnty_in_pcs; ?></td> 
                            <td width="50" align="center"><p><?php echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                            <td align="center"><?php echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                        </tr>
                    <?php
                    $i++;
				}
			?>
				<input type="hidden" name="txt_po_row_id" id="txt_po_row_id" value="<?php echo $po_row_id; ?>"/>
            </table>
        </div>
         <table width="620" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                            <input type="button" name="close" onClick="show_grey_prod_recv();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>           
<?php
	
exit();
}



if($action=="itemdesc_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="170">Search By</th>
                    <th width="180" align="center" id="search_by_td_up">Enter Lot No</th>
                    <th width="240">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr class="general">                    
                    <td>
                        <?php  
                            $search_by = array(1=>'Lot No',2=>'Issue Number',3=>'Challan No',4=>'Item Name',5=>'Store Name');
							$dd="change_search_event(this.value, '0*0*0*0*1', '0*0*0*0*select id,store_name from lib_store_location where company_id=$company', '../../') ";
							echo create_drop_down( "cbo_search_by", 140, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+'<?php echo $booking_no; ?>'+'_'+'<?php echo $basis; ?>', 'create_item_search_list_view', 'search_div', 'yarn_issue_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div style="margin-top:5px" align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}



if($action=="create_item_search_list_view")
{
	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$fromDate = $ex_data[2];
	$toDate = $ex_data[3];
	$company = $ex_data[4];
	$booking_no = $ex_data[5];
	$basis = $ex_data[6];
	 
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{ 
		if(trim($txt_search_by)==1) // for lot no
 			$sql_cond .= " and c.lot LIKE '%$txt_search_common%'";	
 		else if(trim($txt_search_by)==2) // for issue no
 			$sql_cond .= " and a.issue_number LIKE '%$txt_search_common%'";
		else if(trim($txt_search_by)==3) // for chllan no
 			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";	
		else if(trim($txt_search_by)==4) // for Item Description
 			$sql_cond .= " and c.product_name_details LIKE '%$txt_search_common%'";	
		else if(trim($txt_search_by)==5) // for Store Name
 			$sql_cond .= " and b.store_id='$txt_search_common'"; 		 
 	} 
	
	if($basis==1)
	{
		if($booking_no!="") $sql_cond .= " and a.issue_basis=$basis and a.booking_no='$booking_no'";
	}
	else
	{
		if($booking_no!="") $sql_cond .= " and b.receive_basis=$basis and b.requisition_no='$booking_no'";
	}
	
	if( $txt_date_from!="" && $txt_date_to!="" ) 
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.issue_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.issue_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
		
	$sql="select a.id,a.issue_purpose,a.issue_number_prefix_num,a.issue_number,$year_field c.product_name_details,c.lot,a.challan_no,c.current_stock,b.supplier_id,c.id as prod_id 
			from inv_issue_master a, inv_transaction b, product_details_master c
			where a.id=b.mst_id and b.prod_id=c.id and a.status_active=1 and c.status_active=1 and b.item_category=1 and b.transaction_type=2 $sql_cond group by a.id, a.issue_purpose, a.issue_number_prefix_num, a.issue_number, a.challan_no, a.insert_date, b.supplier_id, c.id, c.product_name_details, c.lot, c.current_stock order by a.id desc";
	//echo $sql;// and c.current_stock>0	
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(2=>$yarn_issue_purpose,6=>$supplier_arr);
 	echo create_list_view("list_view", "Issue No, Year, Issue Purpose, Item Name Details, Lot No, Challan No, Supplier","70,55,125,250,100,90","920","260",0, $sql , "js_set_value", "challan_no,prod_id,id", "", 1, "0,0,issue_purpose,0,0,0,supplier_id", $arr, "issue_number_prefix_num,year,issue_purpose,product_name_details,lot,challan_no,supplier_id", "","",'0,0,0,0,0,0,0') ;	
	exit();
}




if($action=="populate_data_from_data")
{
	$ex_data = explode("_",$data);
	$challan_no = $ex_data[0];
	$prodID = $ex_data[1];
	$issueID = $ex_data[2];
	
	$sql = "select a.id, a.issue_purpose, a.issue_number_prefix_num, c.id as prod_id,c.supplier_id,c.product_name_details, c.lot,c.unit_of_measure,c.avg_rate_per_unit,sum(b.cons_quantity) as cons_quantity, sum(b.return_qnty) as returnable_qnty
			from inv_issue_master a, inv_transaction b, product_details_master c
			where a.id=b.mst_id and b.prod_id=c.id and a.id='$issueID' and c.id=$prodID and b.item_category=1 and b.transaction_type=2 group by a.id, a.issue_purpose, a.issue_number_prefix_num, c.id, c.supplier_id, c.product_name_details,c.lot,c.unit_of_measure,c.avg_rate_per_unit";
	//echo $sql; //and a.challan_no='$challan_no'
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#txt_item_description').val('".$row[csf("product_name_details")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#txt_supplier_id').val('".$row[csf("supplier_id")]."');\n";
		echo "$('#txt_yarn_lot').val('".$row[csf("lot")]."');\n";
		echo "$('#cbo_uom').val(".$row[csf("unit_of_measure")].");\n";
		echo "$('#txt_rate').val(".$row[csf("avg_rate_per_unit")].");\n";
		echo "$('#txt_issue_challan_no').val('".$row[csf("issue_number_prefix_num")]."');\n";
		echo "$('#txt_issue_id').val('".$row[csf("id")]."');\n";
		echo "$('#txt_issue_qnty').val('".$row[csf("cons_quantity")]."');\n";
		
		$totalReturned = return_field_value("sum(cons_quantity)","inv_transaction","issue_id='".$row[csf("id")]."' and prod_id='".$row[csf("prod_id")]."' and item_category=1 and transaction_type=4");
		if($totalReturned=="") $totalReturned=0;
		echo "$('#txt_total_return').val('".$totalReturned."');\n";
		echo "$('#txt_total_return_display').val('".$totalReturned."');\n";
		$netUsed = $row[csf("cons_quantity")]-$totalReturned;
		echo "$('#txt_net_used').val('".$netUsed."');\n";
		
		$returnableBl = $row[csf("returnable_qnty")]-$totalReturned;
		echo "$('#txt_returnable_qnty').val('".$row[csf("returnable_qnty")]."');\n";
		echo "$('#txt_returnable_bl_qnty').val('".$returnableBl."');\n";
		
		echo "return_qnty_basis(".$row[csf("issue_purpose")].");\n";
   	}	
	exit();	
}



//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_receive_master a, inv_transaction b","a.id=b.mst_id and a.recv_number=$txt_return_no and b.prod_id=$txt_prod_id and b.transaction_type=4"); 
		if($duplicate==1) 
		{
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			check_table_status( $_SESSION['menu_id'], 0 );
			die;
		}
		//------------------------------Check Brand END---------------------------------------//
		
		//adjust product master table START-------------------------------------//
 		$txt_return_qnty = str_replace("'","",$txt_return_qnty);
		$txt_return_value = str_replace("'","",$txt_amount);
		
		if($txt_return_qnty=="") $txt_return_qnty=0;
		if($txt_amount=="") $txt_amount=0;
		
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value,allocated_qnty,available_qnty from product_details_master where id=$txt_prod_id");
		$presentStock=$presentStockValue=$presentAvgRate=$allocated_qnty=$available_qnty=$allocated_qnty_balance=$available_qnty_balance=0;
 		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];	
			$allocated_qnty			=$result[csf("allocated_qnty")];
			$available_qnty			=$result[csf("available_qnty")];		
		}
		
		$issue_purpose=return_field_value("issue_purpose","inv_issue_master","id=$txt_issue_id");
 		if($issue_purpose==1 || $issue_purpose==2)
		{
			$allocated_qnty_balance=$allocated_qnty+$txt_return_qnty;
			$available_qnty_balance = $available_qnty;
		}
		else
		{
			$allocated_qnty_balance=$allocated_qnty;
			$available_qnty_balance = $available_qnty+$txt_return_qnty;
		}
		
		$nowStock 		= $presentStock+$txt_return_qnty;
		$nowStockValue 	= $presentStockValue+$txt_return_value;
		$nowAvgRate		= number_format( $nowStockValue/$nowStock,$dec_place[3],".","" );	
		//$nowAvailable 	= $available_qnty+$txt_return_qnty;
		$field_array_prod="last_purchased_qnty*current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";
		$data_array_prod=$txt_return_qnty."*".$nowStock."*".$nowStockValue."*".$allocated_qnty_balance."*".$available_qnty_balance."*'".$user_id."'*'".$pc_date_time."'";
		//$prodUpdate = sql_update("product_details_master",$field_array_prod,$data_array_prod,"id",$txt_prod_id,1);	 
		//adjust product master table END  -------------------------------------//
		//echo $data_array; die;
		
		/*$field_array = "id,mst_id,company_id,supplier_id,prod_id,item_category,transaction_type,transaction_date,store_id,cons_uom,cons_quantity,cons_rate,cons_amount,inserted_by,insert_date";
 		$data_array = "(".$transactionID.",".$id.",".$cbo_company_name.",".$cbo_return_to.",".$txt_prod_id.",1,3,".$txt_return_date.",".$cbo_store_name.",".$cbo_uom.",".$txt_issue_qnty.",".$txt_rate.",".$issue_stock_value.",'".$user_id."','".$pc_date_time."')"; */
		
 		 
		//yarn master table entry here START---------------------------------------//	
		//$currency=array(1=>"Taka",2=>"USD",3=>"EURO");	
		if(str_replace("'","",$txt_return_no)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$id=return_next_id("id", "inv_receive_master", 1);		
			$new_recv_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'YIR', date("Y",time()), 5, "select recv_number_prefix,recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='9' and $year_cond=".date('Y',time())." order by id DESC ", "recv_number_prefix", "recv_number_prefix_num" )); 
			
			$field_array="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, company_id, receive_basis, receive_date, booking_id, booking_no, knitting_source, knitting_company, challan_no, store_id, location_id, exchange_rate, currency_id, supplier_id, remarks, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_recv_number[1]."','".$new_recv_number[2]."','".$new_recv_number[0]."',9,1,".$cbo_company_name.",".$cbo_basis.",".$txt_return_date.",".$txt_booking_id.",".$txt_booking_no.",".$cbo_knitting_source.",".$cbo_knitting_company.",".$txt_return_challan_no.",".$cbo_store_name.",".$cbo_location.",1,1,".$txt_supplier_id.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_insert("inv_receive_master",$field_array,$data_array,1); 		
		}
		else
		{
			$new_recv_number[0] = str_replace("'","",$txt_return_no);
			$id=return_field_value("id","inv_receive_master","recv_number=$txt_return_no");			
			$field_array="entry_form*item_category*company_id*receive_basis*receive_date*booking_id*booking_no*knitting_source*knitting_company*challan_no*store_id*location_id*exchange_rate*currency_id*issue_id*remarks*updated_by*update_date";
			$data_array="9*1*".$cbo_company_name."*".$cbo_basis."*".$txt_return_date."*".$txt_booking_id."*".$txt_booking_no."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$txt_return_challan_no."*".$cbo_store_name."*".$cbo_location."*1*1*".$txt_supplier_id."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
 			//$rID = sql_update("inv_receive_master",$field_array,$data_array,"id",$id,1);
		}
		//yarn master table entry here END---------------------------------------// 
		 
  		
		//transaction table insert here START--------------------------------//
		$dtlsid = return_next_id("id", "inv_transaction", 1);		 
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$field_array_trans = "id,mst_id,receive_basis,company_id,supplier_id,prod_id,item_category,transaction_type,transaction_date,store_id,order_uom,order_qnty,order_rate,order_amount,cons_uom,cons_quantity,cons_reject_qnty,cons_rate,cons_amount,balance_qnty,balance_amount,issue_challan_no,issue_id,remarks,inserted_by,insert_date";
 		$data_array_trans= "(".$dtlsid.",".$id.",".$cbo_basis.",".$cbo_company_name.",".$txt_supplier_id.",".$txt_prod_id.",1,4,".$txt_return_date.",".$cbo_store_name.",".$cbo_uom.",".$txt_return_qnty.",".$txt_rate.",".$txt_amount.",".$cbo_uom.",".$txt_return_qnty.",".$txt_reject_qnty.",".$txt_rate.",".$txt_amount.",".$txt_return_qnty.",".$txt_amount.",".$txt_issue_challan_no.",".$txt_issue_id.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
		//echo $field_array."<br>".$data_array;die;
		//$dtlsrID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,1);		
 		//transaction table insert here END ---------------------------------//
		
		$proportQ=true; 
 		if(str_replace("'","",$save_data)!="")
		{		
			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,po_breakdown_id,prod_id,quantity,issue_purpose,inserted_by,insert_date";
			$save_string=explode(",",str_replace("'","",$save_data));		
			$po_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if( array_key_exists($order_id,$po_array) )
					$po_array[$order_id]+=$order_qnty;
				else
					$po_array[$order_id]=$order_qnty;
			}
			$i=0;
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$data_array_prop.="(".$id_proport.",".$dtlsid.",4,9,".$order_id.",".$txt_prod_id.",".$order_qnty.",".$issue_purpose.",".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			/*if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}*/
		}//end if
		//order_wise_pro_details table data insert END -----//
		
		$prodUpdate = sql_update("product_details_master",$field_array_prod,$data_array_prod,"id",$txt_prod_id,1);	

		if(str_replace("'","",$txt_return_no)=="")
		{
			$rID=sql_insert("inv_receive_master",$field_array,$data_array,1); 	
		}
		else
		{
			$rID = sql_update("inv_receive_master",$field_array,$data_array,"id",$id,1);
		}
		$dtlsrID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,1);
		if($data_array_prop!="")
		{
			$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
		}
		
		//echo "20**".$prodUpdate." && ".$rID." && ".$dtlsrID;
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		//mysql_query("ROLLBACK");die;
		if($db_type==0)
		{
			if( $prodUpdate && $rID && $dtlsrID && $proportQ)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_recv_number[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_recv_number[0];
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if( $prodUpdate && $rID && $dtlsrID && $proportQ)
			{
				oci_commit($con);  
				echo "0**".$new_recv_number[0];
			}
			else
			{
				oci_rollback($con);
				echo "10**".$new_recv_number[0];
			}
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		if( str_replace("'","",$update_id) == "" || str_replace("'","",$txt_prod_id) == "" || str_replace("'","",$before_prod_id) == "" )
		{
			echo "15";exit(); 
		}
		
		$sql = sql_select("select a.cons_quantity,a.cons_amount,b.current_stock,b.stock_value,b.allocated_qnty,b.available_qnty from inv_transaction a, product_details_master b where a.id=$update_id and a.prod_id=b.id");
		$beforeReturnQnty=$beforeReturnValue=0;
		$currentStockQnty=$currentStockValue=$before_available_qnty=0;
		foreach($sql as $result)
		{
			//current stock
			$currentStockQnty		=$result[csf("current_stock")];
			$currentStockValue		=$result[csf("stock_value")];
			//before return qnty
			$beforeReturnQnty		=$result[csf("cons_quantity")];
			$beforeReturnValue		=$result[csf("cons_amount")];
			$before_allocated_qnty	=$result[csf("allocated_qnty")];
			$before_available_qnty	=$result[csf("available_qnty")];
 		}
		
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value,allocated_qnty,available_qnty from product_details_master where id=$txt_prod_id");
		$presentStock=$presentStockValue=$presentAvgRate=$available_qnty=0;
 		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];	
			$allocated_qnty			=$result[csf("allocated_qnty")];
			$available_qnty			=$result[csf("available_qnty")];		
		}
		
		$issue_purpose=return_field_value("issue_purpose","inv_issue_master","id=$txt_issue_id");
 		
		//adjust product master table START-------------------------------------//
 		$txt_return_qnty = str_replace("'","",$txt_return_qnty);
		$txt_return_value = str_replace("'","",$txt_amount);
		
		if($txt_return_qnty=="") $txt_return_qnty=0;
		if($txt_return_value=="") $txt_return_value=0;
		
		$txt_prod_id = str_replace("'","",$txt_prod_id);
		$update_array="current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";
		$update_data = $updateID_array = array();
		if(str_replace("'","",$txt_prod_id) == str_replace("'","",$before_prod_id))
		{
			if($issue_purpose==1 || $issue_purpose==2)
			{
				$presentallocatedQnty=$before_allocated_qnty-$beforeReturnQnty+$txt_return_qnty;
				$presentAvailableQnty = $before_available_qnty;
			}
			else
			{
				$presentallocatedQnty=$before_allocated_qnty;
				$presentAvailableQnty = $before_available_qnty-$beforeReturnQnty+$txt_return_qnty;
			}
			
			$presentStockQnty   = $currentStockQnty-$beforeReturnQnty+$txt_return_qnty; //current qnty - before qnty + present return qnty
			$presentStockValue  = $currentStockValue-$beforeReturnValue+$txt_return_value; 
			$avgRate			= number_format($presentStockValue/$presentStockQnty,$dec_place[3],".","");
			//$presentAvailableQnty  = $before_available_qnty-$beforeReturnQnty+$txt_return_qnty;
			$data_array			= $presentStockQnty."*".$presentStockValue."*".$presentallocatedQnty."*".$presentAvailableQnty."*'".$user_id."'*'".$pc_date_time."'";
 			//$prodUpdate = sql_update("product_details_master",$update_array,$data_array,"id",$txt_prod_id,1);
			
		}
		else
		{
			if($issue_purpose==1 || $issue_purpose==2)
			{
				$adj_allocated_qnty=$before_allocated_qnty-$beforeReturnQnty;
				$adj_available_qnty=$before_available_qnty;
				
				$presentallocatedQnty=$allocated_qnty+$txt_return_qnty;
				$presentAvailableQnty = $available_qnty;
			}
			else
			{
				$adj_allocated_qnty=$before_allocated_qnty;
				$adj_available_qnty=$before_available_qnty-$beforeReturnQnty;
				
				$presentallocatedQnty=$allocated_qnty;
				$presentAvailableQnty = $available_qnty+$txt_return_qnty;
			} 
			//before
			$presentStockQnty   = $currentStockQnty-$txt_return_qnty; //current qnty - before qnty
			$presentStockValue  = $currentStockValue-$txt_return_value; 
			//$presentAvailableQnty =$beforeReturnQnty-$txt_return_qnty;
			$avgRate			= number_format($presentStockValue/$presentStockQnty,$dec_place[3],".","");
			$update_data[$before_prod_id]=explode("*",("".$presentStockQnty."*".$presentStockValue."*".$adj_allocated_qnty."*".$adj_available_qnty."*'".$user_id."'*'".$pc_date_time."'"));
			$updateID_array[]=$before_prod_id;
			//current
			$presentStockQnty   = $presentStock+$txt_return_qnty; //current qnty - before qnty + present return qnty
			$presentStockValue  = $presentStockValue+$txt_return_value; 
			//$presentAvailableQnty =$beforeReturnQnty+$txt_return_qnty;
			$avgRate			= number_format($presentStockValue/$presentStockQnty,$dec_place[3],".","");
			$update_data[$txt_prod_id]=explode("*",("".$presentStockQnty."*".$presentStockValue."*".$presentallocatedQnty."*".$presentAvailableQnty."*'".$user_id."'*'".$pc_date_time."'"));
			$updateID_array[]=$txt_prod_id;
			//$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array));
			
			//echo $before_allocated_qnty."**".$beforeReturnQnty."**".bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array);die;
		}
		//adjust product master table END  -------------------------------------//
		
 				
  		//yarn receive master table UPDATE here START----------------------//		
 		$field_array_upd="company_id*receive_basis*receive_date*booking_id*booking_no*booking_without_order*knitting_source*knitting_company*challan_no*store_id*location_id*exchange_rate*currency_id*supplier_id*remarks*updated_by*update_date";
		$data_array_upd="".$cbo_company_name."*".$cbo_basis."*".$txt_return_date."*".$txt_booking_id."*".$txt_booking_no."*".$booking_without_order."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$txt_return_challan_no."*".$cbo_store_name."*".$cbo_location."*1*1*".$txt_supplier_id."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
 		//echo $field_array."<br>".$data_array;die;
		//$rID=sql_update("inv_receive_master",$field_array_upd,$data_array_upd,"recv_number",$txt_return_no,1);	
		//yarn receive master table entry here END---------------------------------------//	 
  		 
  		
		//transaction table update here START--------------------------------//
 		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$field_array_trans= "receive_basis*company_id*supplier_id*prod_id*item_category*transaction_type*transaction_date*store_id*order_uom*order_qnty*order_rate*order_amount*cons_uom*cons_quantity*cons_reject_qnty*cons_rate*cons_amount*balance_qnty*balance_amount*issue_challan_no*issue_id*remarks*updated_by*update_date";
 		$data_array_trans= "".$cbo_basis."*".$cbo_company_name."*".$txt_supplier_id."*".$txt_prod_id."*1*4*".$txt_return_date."*".$cbo_store_name."*".$cbo_uom."*".$txt_return_qnty."*".$txt_rate."*".$txt_amount."*".$cbo_uom."*".$txt_return_qnty."*".$txt_reject_qnty."*".$txt_rate."*".$txt_amount."*".$txt_return_qnty."*".$txt_amount."*".$txt_issue_challan_no."*".$txt_issue_id."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
		//$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1); 		
 		//transaction table update here END ---------------------------------//
		
		
		$proportQ=true;
 		if(str_replace("'","",$save_data)!="")
		{		
			//$propQr = execute_query("DELETE FROM order_wise_pro_details WHERE trans_id=$update_id and entry_form=9 and trans_type=4");
			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,po_breakdown_id,prod_id,quantity,issue_purpose,inserted_by,insert_date";
			$save_string=explode(",",str_replace("'","",$save_data));		
			$po_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if( array_key_exists($order_id,$po_array) )
					$po_array[$order_id]+=$order_qnty;
				else
					$po_array[$order_id]=$order_qnty;
			}
			$i=0;
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$data_array_prop.="(".$id_proport.",".$update_id.",4,9,".$order_id.",".$txt_prod_id.",".$order_qnty.",".$issue_purpose.",".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			/*if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}*/
		}
		//order_wise_pro_details table data insert END -----//
		
		if(str_replace("'","",$txt_prod_id) == str_replace("'","",$before_prod_id))
		{
			$prodUpdate = sql_update("product_details_master",$update_array,$data_array,"id",$txt_prod_id,1);
		}
		else
		{
			$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array));
		}
		$rID=sql_update("inv_receive_master",$field_array_upd,$data_array_upd,"recv_number",$txt_return_no,1);
		$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
		$propQr = execute_query("DELETE FROM order_wise_pro_details WHERE trans_id=$update_id and entry_form=9 and trans_type=4");
		if($data_array_prop!="" && str_replace("'","",$booking_without_order)!=1)
		{
			$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
		}
 		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		//echo "20**".$prodUpdate." && ".$rID." && ".$transID."&&".$proportQ;mysql_query("ROLLBACK");die;
		if($db_type==0)
		{
			if($prodUpdate && $rID && $transID && $proportQ && $propQr)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_return_no);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($prodUpdate && $rID && $transID && $proportQ && $propQr)
			{
				oci_commit($con);
				echo "1**".str_replace("'","",$txt_return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_return_no);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		
	}		
}



if($action=="return_number_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_return_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="170">Search By</th>
                    <th width="270" align="center" id="search_by_td_up">Enter Return Number</th>
                    <th width="220">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr class="general">                    
                    <td>
                        <?php  
                            $search_by = array(1=>'Return Number');
							//$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 140, $search_by,"",0, "--Select--", "",1,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_return_search_list_view', 'search_div', 'yarn_issue_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_return_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div style="margin-top:5px" align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_return_search_list_view")
{
	
	$ex_data = explode("_",$data);
	$search_by = $ex_data[0];
	$search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
	
	$sql_cond="";
	if($search_by==1)
	{
		if($search_common!="") $sql_cond .= " and recv_number like '%$search_common'";
	}	 
	if( $txt_date_from!="" && $txt_date_to!="" ) 
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
	
	if($company!="") $sql_cond .= " and a.company_id='$company'";
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql = "select a.recv_number_prefix_num, a.recv_number, a.company_id, a.supplier_id, a.receive_date, a.item_category, a.recv_number, $year_field b.id, b.cons_quantity, b.cons_reject_qnty, b.cons_uom,b.cons_rate,b.cons_amount,c.product_name_details, c.id as prod_id,c.lot   
			from inv_receive_master a, inv_transaction b left join product_details_master c on b.prod_id=c.id
			where a.id=b.mst_id and b.item_category=1 and b.transaction_type=4 $sql_cond"; 
 	//echo $sql;
	$arr=array(5=>$unit_of_measurement);
 	echo create_list_view("list_view", "Return No, Year, Item Description, Return Qnty, Rejected Qnty, UOM, Lot No","80,60,250,100,100,80,100","850","260",0, $sql , "js_set_value", "recv_number", "", 1, "0,0,0,0,0,cons_uom,0", $arr, "recv_number_prefix_num,year,product_name_details,cons_quantity,cons_reject_qnty,cons_uom,lot","","",'0,0,0,2,2,0,0') ;	
 	exit();
}
 

if($action=="populate_master_from_data")
{  
	
 	$sql = "select id,recv_number,entry_form,item_category,company_id,receive_basis,receive_purpose,receive_date,booking_id,booking_no,knitting_source,knitting_company,yarn_issue_challan_no,challan_no,store_id,location_id,buyer_id,exchange_rate,currency_id,supplier_id,lc_no,source
			from inv_receive_master 
			where recv_number='$data'";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
 		echo "set_button_status(0, permission, 'fnc_yarn_issue_return_entry',1,1);";
		echo "$('#txt_return_no').val('".$row[csf("recv_number")]."');\n";
 		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		//echo "load_drop_down( 'requires/yarn_issue_return_controller', ".$row[csf("company_id")].", 'load_drop_down_location', 'location_td' );\n";
		//echo "load_drop_down( 'requires/yarn_issue_return_controller', ".$row[csf("company_id")].", 'load_drop_down_store', 'store_td' );\n";
 		echo "$('#cbo_basis').val('".$row[csf("receive_basis")]."');\n";
		echo "active_inactive('".$row[csf("receive_basis")]."');\n";
		echo "$('#txt_booking_no').val('".$row[csf("booking_no")]."');\n"; 
		echo "$('#txt_booking_id').val('".$row[csf("booking_id")]."');\n";
		echo "$('#cbo_location').val('".$row[csf("location_id")]."');\n";
		echo "$('#cbo_knitting_source').val('".$row[csf("knitting_source")]."');\n";
		echo "load_drop_down( 'requires/yarn_issue_return_controller', ".$row[csf("knitting_source")]."+'**'+".$row[csf("company_id")].", 'load_drop_down_knit_com', 'knitting_company_td' );\n";
		echo "$('#cbo_knitting_company').val('".$row[csf("knitting_company")]."');\n";
		echo "$('#txt_return_date').val('".change_date_format($row[csf("receive_date")])."');\n";
		echo "$('#txt_return_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "disable_enable_fields( 'cbo_company_name*cbo_basis', 1, '', '' );\n"; // disable true
				
   	}	
	exit();	
}



if($action=="show_dtls_list_view")
{
	
	$ex_data = explode("**",$data);
	$return_number = $ex_data[0];
	$ret_mst_id = $ex_data[1];
	
	$cond="";
	if($return_number!="") $cond .= " and a.recv_number='$return_number'";
	if($ret_mst_id!="") $cond .= " and a.id='$ret_mst_id'";
	
	$sql = "select a.recv_number,a.company_id,a.supplier_id,a.receive_date,a.item_category,a.recv_number,b.id, b.cons_quantity, b.cons_reject_qnty, b.cons_uom, b.cons_rate, b.cons_amount, c.product_name_details, c.id as prod_id   
			from  inv_receive_master a, inv_transaction b left join product_details_master c on b.prod_id=c.id
			where a.id=b.mst_id and b.item_category=1 and b.transaction_type=4 $cond";
	//echo $sql;
	$result = sql_select($sql);
	$i=1;
	$rettotalQnty=0;
	$rcvtotalQnty=0;
	$rejtotalQnty=0;
	$totalAmount=0;
	?> 
     	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:980px" >
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Return No</th>
                    <th>Item Description</th>
                    <th>Product ID</th>
                    <th>Return Qty</th>
                    <th>Reject Qty</th>
                    <th>UOM</th>                    
                    <th>Rate</th>
                    <th>Return Value</th> 
                </tr>
            </thead>
            <tbody>
            	<?php 
				foreach($result as $row){					
					if($i%2==0)
						$bgcolor="#E9F3FF";
					else 
						$bgcolor="#FFFFFF";
 					
					$rettotalQnty +=$row[csf("cons_quantity")];
					$rejtotalQnty +=$row[csf("cons_reject_qnty")];
 					$totalAmount +=$row[csf("cons_amount")];
 					
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>","child_form_input_data","requires/yarn_issue_return_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $row[csf("recv_number")]; ?></p></td>
                        <td width="250"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td width="80"><p><?php echo $row[csf("prod_id")]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("cons_quantity")]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("cons_reject_qnty")]; ?></p></td>
                        <td width="60"><p><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("cons_rate")]; ?></p></td>
                        <td width="100" align="right"><p><?php echo $row[csf("cons_amount")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                        <th colspan="4">Total</th>                         
                        <th><?php echo $rettotalQnty; ?></th> 
                        <th><?php echo $rejtotalQnty; ?></th>
                        <th colspan="2"></th>
                        <th><?php echo $totalAmount; ?></th>
                   </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}


if($action=="child_form_input_data")
{
	//$data // transaction id
  	$sql = "select b.id as prod_id, b.product_name_details, b.lot, a.id as tr_id, a.store_id, a.issue_id, a.cons_uom, a.cons_rate, a.cons_quantity,a.cons_reject_qnty, a.cons_amount, a.issue_challan_no,a.remarks 	
			from inv_transaction a, product_details_master b
 			where a.id=$data and a.status_active=1 and a.item_category=1 and transaction_type=4 and a.prod_id=b.id and b.status_active=1";
 	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		$issue_purpose=return_field_value("issue_purpose","inv_issue_master","id='".$row[csf("issue_id")]."'");
		
		echo "return_qnty_basis(".$issue_purpose.");\n";
		
 		echo "$('#txt_item_description').val('".$row[csf("product_name_details")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#before_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#txt_yarn_lot').val('".$row[csf("lot")]."');\n";
		echo "$('#cbo_store_name').val('".$row[csf("store_id")]."');\n";
		echo "$('#txt_return_qnty').val('".$row[csf("cons_quantity")]."');\n"; 
		echo "$('#txt_reject_qnty').val('".$row[csf("cons_reject_qnty")]."');\n";
 		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		echo "$('#cbo_uom').val('".$row[csf("cons_uom")]."');\n";
		echo "$('#txt_issue_id').val('".$row[csf("issue_id")]."');\n";
		
		$totalIssued = return_field_value("sum(b.cons_quantity)","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and a.id='".$row[csf("issue_id")]."' and b.prod_id='".$row[csf("prod_id")]."' and b.item_category=1 and b.transaction_type=2");
		
		$totalreturnable = return_field_value("sum(b.return_qnty)","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and a.id='".$row[csf("issue_id")]."' and b.prod_id='".$row[csf("prod_id")]."' and b.item_category=1 and b.transaction_type=2");
		
		if($totalIssued=="") $totalIssued=0;
		echo "$('#txt_issue_qnty').val('".$totalIssued."');\n";
		$totalReturn = return_field_value("sum(cons_quantity)","inv_transaction","issue_id='".$row[csf("issue_id")]."' and prod_id='".$row[csf("prod_id")]."' and item_category=1 and transaction_type=4");
		echo "$('#txt_total_return_display').val('".$totalReturn."');\n";
		$netUsed = $totalIssued-$totalReturn;
		echo "$('#txt_net_used').val('".$netUsed."');\n";
		echo "$('#hide_net_used').val('".$row[csf("cons_quantity")]."');\n";
		
		$returnableBl = $totalreturnable-$totalReturn;
		echo "$('#txt_returnable_qnty').val('".$totalreturnable."');\n";
		echo "$('#txt_returnable_bl_qnty').val('".$returnableBl."');\n";
		
		if($totalReturn=="") $totalReturn=0;  else $totalReturn=$totalReturn-$row[csf("cons_quantity")];
		echo "$('#txt_total_return').val('".$totalReturn."');\n";
		
		echo "$('#txt_rate').val('".$row[csf("cons_rate")]."');\n";		
		echo "$('#txt_amount').val(".$row[csf("cons_amount")].");\n";
		echo "$('#txt_issue_challan_no').val('".$row[csf("issue_challan_no")]."');\n";
		echo "$('#update_id').val(".$row[csf("tr_id")].");\n";
	
		//issue qnty popup data arrange 
		$sqlIN = sql_select("select po_breakdown_id,quantity from order_wise_pro_details where trans_id=".$row[csf("tr_id")]." and entry_form=9 and trans_type=4");
		$poWithValue="";
		$poID="";
		foreach($sqlIN as $res)
		{
			if($poWithValue!="") $poWithValue .=",";
			if($poID!="") $poID .=",";
			$poWithValue .= $res[csf("po_breakdown_id")]."**".$res[csf("quantity")];
			$poID .=$res[csf("po_breakdown_id")];
		}
		echo "$('#save_data').val('".$poWithValue."');\n"; 
		echo "$('#all_po_id').val('".$poID."');\n";
		
	}
 	echo "set_button_status(1, permission, 'fnc_yarn_issue_return_entry',1,1);\n";		
  	exit();
}

if ($action=="yarn_issue_return_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);

	$sql=" select id, recv_number, receive_basis, booking_no, booking_id, knitting_source, knitting_company, challan_no, receive_date from  inv_receive_master where recv_number='$data[1]' and entry_form=9 and item_category=1";
	
	$dataArray=sql_select($sql);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
?>
<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><?php echo $data[2]; ?> Challan</strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Return ID:</strong></td><td width="175px"><?php echo $dataArray[0][csf('recv_number')]; ?></td>
            <td width="130"><strong>Basis:</strong></td> <td width="175px"><?php echo $issue_basis[$dataArray[0][csf('receive_basis')]]; ?></td>
            <td width="125"><strong>F.Book/Req. No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('booking_no')]; ?></td>
        </tr>
        <tr>
            <td><strong>Return Source:</strong></td> <td width="175px"><?php echo $knitting_source[$dataArray[0][csf('knitting_source')]]; ?></td>
            <td><strong>Knitting Com :</strong></td><td width="175px"><?php if ($dataArray[0][csf('knitting_source')]==1) echo $company_library[$dataArray[0][csf('knitting_company')]]; elseif ($dataArray[0][csf('knitting_source')]==3) echo $supplier_library[$dataArray[0][csf('knitting_company')]]; ?></td>
            <td><strong>Return Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('receive_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Return Challan:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>&nbsp;</strong></td><td width="175px"><?php //echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>&nbsp;</strong></td><td width="175px"><?php //echo $dataArray[0][csf('remarks')]; ?></td>
        </tr>
    </table>
         <br>
	<div style="width:100%;">
    <table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="220" align="center">Item Description</th>
            <th width="60" align="center">Yarn Lot</th> 
            <th width="50" align="center">UOM</th>
            <th width="80" align="center">Returned Qnty</th>
            <th width="80" align="center">Rejected Qnty</th> 
            <th width="100" align="center">Store</th>
            <th width="" align="center">Remarks</th>
        </thead>
        <tbody>
<?php
	$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
	
	$i=1;
	$mst_id=$dataArray[0][csf('id')];

	$sql_dtls="Select a.id as pd_id, a.product_name_details, a.lot, b.id, b.cons_uom, b.cons_quantity, b.store_id, b.cons_reject_qnty, b.remarks from product_details_master a, inv_transaction b where a.id=b.prod_id and b.transaction_type=4 and b.item_category=1 and b.mst_id='$mst_id' and b.status_active=1 and b.is_deleted=0";
	//echo $sql_dtls;
	$sql_result = sql_select($sql_dtls);	
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
			$rej_quantity+=$row[csf('cons_reject_qnty')];
			$cons_quantity_sum += $row[csf('cons_quantity')];
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <td><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                <td align="center"><?php echo $row[csf("lot")]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></td>
                <td align="right"><?php echo $row[csf("cons_quantity")]; ?></td>
                <td align="right"><?php echo $row[csf("cons_reject_qnty")]; ?></td>
                <td align="center"><?php echo $store_arr[$row[csf("store_id")]]; ?></td>
                <td align="center"><?php echo $row[csf("remarks")]; ?></td>
			</tr>
			<?php $i++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" align="right">Total :</td>
                <td align="right"><?php echo $cons_quantity_sum; ?></td>
                <td align="right"><?php echo $rej_quantity; ?></td>
                <td colspan="2">&nbsp;</td>
            </tr>                           
        </tfoot>
    </table>
        <br>
		 <?php
            echo signature_table(37, $data[0], "900px");
         ?>
	</div>
	</div>
<?php
exit();			
}
?>