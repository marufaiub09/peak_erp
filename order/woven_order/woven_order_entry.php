﻿<?php
/*--------------------------------------------Comments----------------
Version (MySql)          :  V2
Version (Oracle)         :  V1
Converted by             :  MONZU
Converted Date           :  24-05-2014
Purpose			         : 	This form will create Knit Garments Order Entry
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 	13-10-2012
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 		
Update date		         : 		   
QC Performed BY	         :		
QC Date			         :	
Comments		         :From this version oracle conversion is start
----------------------------------------------------------------------*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Info","../../", 1, 1, $unicode,1,'');
?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";
// Master Form-----------------------------------------------------------------------------
	function internal(ref)
	{
	// alert(ref);
	 var internal_ref = [];
	 var int_ref=ref.split(",");
	 for(var i=0; i<int_ref.length; i++)
	 {
		 //alert(int_ref[i].replace(/\"/g,''));
		internal_ref[i]= int_ref[i].replace(/\"/g,'');
	 }
 	//var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];
	
	
          $("#txt_grouping").autocomplete({
			 source: internal_ref
		  });
	}

function openmypage(page_link,title)
{
	var garments_nature=document.getElementById('garments_nature').value;
	page_link=page_link+'&garments_nature='+garments_nature;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var theemail=this.contentDoc.getElementById("selected_job");
		if (theemail.value!="")
		{
			freeze_window(5);
		    reset_form('','','txt_po_no*txt_po_received_date*txt_pub_shipment_date*txt_org_shipment_date*txt_po_quantity*txt_avg_price*txt_amount*txt_excess_cut*txt_plan_cut*cbo_status*txt_details_remark*cbo_delay_for*show_textcbo_delay_for*cbo_packing_po_level*update_id_details*color_size_break_down','','');
			get_php_form_data(theemail.value, "populate_data_from_search_popup", "requires/woven_order_entry_controller" );
			show_list_view(theemail.value,'show_po_active_listview','po_list_view','../woven_order/requires/woven_order_entry_controller','');
		 	show_list_view(theemail.value,'show_deleted_po_active_listview','deleted_po_list_view','../woven_order/requires/woven_order_entry_controller','');
			set_button_status(1, permission, 'fnc_order_entry',1);
			load_drop_down( 'requires/woven_order_entry_controller', theemail.value, 'load_drop_down_projected_po', 'projected_po_td' )
			release_freezing();
		}
	}
}

function open_qoutation_popup(page_link,title)
{
	
	var cbo_company_name=document.getElementById('cbo_company_name').value;
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	page_link=page_link+'&cbo_company_name='+cbo_company_name+'&cbo_buyer_name='+cbo_buyer_name;
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var theemail=this.contentDoc.getElementById("selected_id");
		if (theemail.value!="")
		{
			freeze_window(5);
			get_php_form_data( theemail.value, "populate_data_from_search_popup_quotation", "requires/woven_order_entry_controller" );
			load_drop_down('requires/woven_order_entry_controller',document.getElementById("cbo_company_name").value, 'load_drop_down_location', 'location' );
			location_select();
			release_freezing();
		}
	}
}

function location_select()
{
if($('#cbo_location_name option').length==2)
	{
		if($('#cbo_location_name option:first').val()==0)
		{
			$('#cbo_location_name').val($('#cbo_location_name option:last').val());
			eval($('#cbo_location_name').attr('onchange')); 
		}
	}
	else if($('#cbo_location_name option').length==1)
	{
		$('#cbo_location_name').val($('#cbo_location_name option:last').val());
		eval($('#cbo_location_name').attr('onchange'));
	}	
}

function open_set_popup(unit_id)
{ 
		var	pcs_or_set="";
		var txt_job_no=document.getElementById('txt_job_no').value;
		var set_breck_down=document.getElementById('set_breck_down').value;
		var tot_set_qnty=document.getElementById('tot_set_qnty').value;
		var tot_smv_qnty=document.getElementById('tot_smv_qnty').value;

		if(unit_id==58)
		{
		 	pcs_or_set="Item Details For Set";
		}
		if(unit_id==57)
		{
		 	pcs_or_set="Item Details For Pack";
		}
		else
		{
		 	pcs_or_set="Item Details For Pcs";
		}
		
		var page_link="requires/woven_order_entry_controller.php?txt_job_no="+trim(txt_job_no)+"&action=open_set_list_view&set_breck_down="+set_breck_down+"&tot_set_qnty="+tot_set_qnty+'&unit_id='+unit_id+'&tot_smv_qnty='+tot_smv_qnty;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, pcs_or_set, 'width=620px,height=300px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var set_breck_down=this.contentDoc.getElementById("set_breck_down");
			var item_id=this.contentDoc.getElementById("item_id");
			var tot_set_qnty=this.contentDoc.getElementById("tot_set_qnty");
			var tot_smv_qnty=this.contentDoc.getElementById("tot_smv_qnty");
			document.getElementById('set_breck_down').value=set_breck_down.value;
			document.getElementById('item_id').value=item_id.value;
			document.getElementById('tot_set_qnty').value=tot_set_qnty.value;
			document.getElementById('tot_smv_qnty').value=tot_smv_qnty.value;
		}		
}
 
function fnc_order_entry( operation )
{
	if(operation==2)
	{
		alert("Delete Restricted")
		return;
	}
	
	if(operation==1)
	{
		var po_id="";
		var txt_job_no=document.getElementById('txt_job_no').value;
		var booking_no_with_approvet_status = return_ajax_request_value(txt_job_no+"_"+po_id, 'booking_no_with_approved_status', 'requires/woven_order_entry_controller')
		var booking_no_with_approvet_status_arr=booking_no_with_approvet_status.split("_");
		if(trim(booking_no_with_approvet_status_arr[0]) !="")
		{
			var al_magg="Main Fabric Approved Booking No "+booking_no_with_approvet_status_arr[0];
			if(booking_no_with_approvet_status_arr[1] !="")
			{
				al_magg+=" and Un-Approved Booking No "+booking_no_with_approvet_status_arr[1];
			}
			al_magg+=" found,\nPlease Un-approved the booking first";
			alert(al_magg)
		    return;
		}
		
		if(trim(booking_no_with_approvet_status_arr[1]) !="")
		{
			var al_magg=" Main Fabric Un-Approved Booking No "+booking_no_with_approvet_status_arr[1]+" Found\n If you update this job\n You have to update color size break down, Pre-cost and booking against this Job ";
			var r=confirm(al_magg);
			if(r==false)
			{
				return;
			}
			else
			{
			}
		    
		}
	}
	
	if (form_validation('cbo_company_name*cbo_location_name*cbo_buyer_name*txt_style_ref*cbo_product_department*txt_item_catgory*cbo_dealing_merchant*cbo_packing*item_id*tot_smv_qnty','Company Name*Location Name*Buyer Name*Style Ref*Product Department*Item Catagory*Dealing Merchant*Packing*Item Details*SMV')==false)
	{
		return;
	}	
	else
	{
		var data="action=save_update_delete_mst&operation="+operation+get_submitted_data_string('txt_job_no*garments_nature*cbo_company_name*cbo_location_name*cbo_buyer_name*txt_style_ref*txt_style_description*cbo_product_department*txt_product_code*cbo_sub_dept*cbo_currercy*cbo_agent*cbo_client*txt_repeat_no*cbo_region*txt_item_catgory*cbo_team_leader*cbo_dealing_merchant*cbo_packing*txt_remarks*cbo_ship_mode*cbo_order_uom*item_id*set_breck_down*tot_set_qnty*tot_smv_qnty*txt_quotation_id*update_id*txt_season',"../../");
		//alert(data)
		freeze_window(operation);
		http.open("POST","requires/woven_order_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_on_submit_reponse_mst;
	}
}
	
function fnc_on_submit_reponse_mst()
{
	if(http.readyState == 4) 
	{
		var reponse=trim(http.responseText).split('**');
		if(parseInt(trim(reponse[0]))==0 || parseInt(trim(reponse[0]))==1)
		{
		document.getElementById('txt_job_no').value=reponse[1];
		document.getElementById('update_id').value=reponse[1];
		document.getElementById('set_pcs').value=document.getElementById('cbo_order_uom').value
		document.getElementById('set_unit').value=document.getElementById('cbo_currercy').value
		set_button_status(1, permission, 'fnc_order_entry',1);
		}
		show_msg(trim(reponse[0]));
		release_freezing();
	}
}
// Master Form End -----------------------------------------------------------------------------

//Dtls Form-------------------------------------------------------------------------------------


function openmypage_for_po_copy(page_link,title)
{
	var garments_nature=document.getElementById('garments_nature').value;
	var txt_job_no=document.getElementById('txt_job_no').value;
	var cbo_company_name=document.getElementById('cbo_company_name').value;
	//alert(cbo_company_name)
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	var txt_style_ref=document.getElementById('txt_style_ref').value;
	page_link=page_link+'&garments_nature='+garments_nature+'&txt_job_no='+txt_job_no+'&cbo_company_name='+cbo_company_name+'&cbo_buyer_name='+cbo_buyer_name+'&txt_style_ref='+txt_style_ref;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var theemail=this.contentDoc.getElementById("po_id");
		if (theemail.value!="")
		{
			freeze_window(5);
			 get_details_form_data(theemail.value,'populate_order_details_form_data','requires/woven_order_entry_controller')
			 set_button_status(0, permission, 'fnc_order_entry_details',2);
			 document.getElementById('txt_po_no').value='';
		   // reset_form('','','txt_po_no*txt_po_received_date*txt_pub_shipment_date*txt_org_shipment_date*txt_po_quantity*txt_avg_price*txt_amount*txt_excess_cut*txt_plan_cut*cbo_status*txt_details_remark*cbo_delay_for*show_textcbo_delay_for*cbo_packing_po_level*update_id_details*color_size_break_down','','');
			//get_php_form_data(theemail.value, "populate_data_from_search_popup", "requires/woven_order_entry_controller" );
			//show_list_view(theemail.value,'show_po_active_listview','po_list_view','../woven_order/requires/woven_order_entry_controller','');
		 	//show_list_view(theemail.value,'show_deleted_po_active_listview','deleted_po_list_view','../woven_order/requires/woven_order_entry_controller','');
			//set_button_status(1, permission, 'fnc_order_entry',1);
			//load_drop_down( 'requires/woven_order_entry_controller', theemail.value, 'load_drop_down_projected_po', 'projected_po_td' )
			release_freezing();
		}
	}
}
function open_color_size_popup(page_link,title)
{
	if(document.getElementById('txt_po_quantity').value=="" || document.getElementById('txt_po_quantity').value==0)
	{
		alert('Please enter valid order quantity');
		$('#txt_po_quantity').focus();
		return false;
	}
	if(document.getElementById('update_id_details').value=="" || document.getElementById('update_id_details').value==0 )
	{
	   alert('Please Save The Po first.');
		return false;	
	}
	else
	{
		var update_id_details=document.getElementById('update_id_details').value;
		var txt_po_no=document.getElementById('txt_po_no').value;
		var txt_po_quantity=document.getElementById('txt_po_quantity').value;
		var set_breck_down=document.getElementById('set_breck_down').value;
		var item_id=document.getElementById('item_id').value;
		var tot_set_qnty=document.getElementById('tot_set_qnty').value;
		var cbo_order_uom=document.getElementById('cbo_order_uom').value;
		var color_size_break_down=document.getElementById('color_size_break_down').value;
		var txt_avg_price =document.getElementById('txt_avg_price').value;
		var txt_excess_cut=document.getElementById('txt_excess_cut').value;
		var cbo_company_name=document.getElementById('cbo_company_name').value;
		var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
		var txt_org_shipment_date=document.getElementById('txt_org_shipment_date').value;
		var txt_pub_shipment_date=document.getElementById('txt_pub_shipment_date').value;
		var cbo_packing_po_level=document.getElementById('cbo_packing_po_level').value;
		

		var page_link=page_link+'&data='+update_id_details+'&txt_po_quantity='+txt_po_quantity+'&set_breck_down='+set_breck_down+'&item_id='+item_id+'&tot_set_qnty='+tot_set_qnty+'&cbo_order_uom='+cbo_order_uom+'&color_size_break_down='+color_size_break_down+'&txt_po_no='+txt_po_no+'&txt_avg_price='+txt_avg_price+'&txt_excess_cut='+txt_excess_cut+'&cbo_company_name='+cbo_company_name+'&cbo_buyer_name='+cbo_buyer_name+'&txt_org_shipment_date='+txt_org_shipment_date+'&txt_pub_shipment_date='+txt_pub_shipment_date+'&cbo_packing_po_level='+cbo_packing_po_level;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1200px,height=550px,center=1,resize=1,scrolling=0','../../')
		
		emailwindow.onclose=function()
		{
			var tot_set_qnty=(document.getElementById('tot_set_qnty').value)*1;
		    var cbo_order_uom=document.getElementById('cbo_order_uom').value;
			var txt_avg_rate=this.contentDoc.getElementById("txt_tot_avg_rate");
			var txt_total_amt=this.contentDoc.getElementById("txt_tot_amount");
			var txt_avg_excess_cut=this.contentDoc.getElementById("txt_tot_excess_cut");
			var txt_total_plan_cut=this.contentDoc.getElementById("txt_tot_plancut");
			var txt_avg_price="";
			var txt_plan_cut=""
			if(cbo_order_uom==58)
			{
				txt_avg_price=txt_avg_rate.value*tot_set_qnty;
				document.getElementById('txt_avg_price').value=txt_avg_rate.value*tot_set_qnty;
				txt_plan_cut=number_format_common((txt_total_plan_cut.value/tot_set_qnty),6,0,0);
				document.getElementById('txt_plan_cut').value=number_format_common((txt_total_plan_cut.value/tot_set_qnty),6,0,0);
			}
			else
			{
				txt_avg_price=txt_avg_rate.value;
				document.getElementById('txt_avg_price').value=txt_avg_rate.value;
				txt_plan_cut=number_format_common(txt_total_plan_cut.value,6,0,0);;
			    document.getElementById('txt_plan_cut').value=txt_total_plan_cut.value;
			}
		    var txt_amount=txt_total_amt.value;
		    document.getElementById('txt_amount').value=txt_total_amt.value;
			var txt_excess_cut=txt_avg_excess_cut.value;
		    document.getElementById('txt_excess_cut').value=txt_avg_excess_cut.value;
			fnc_order_entry_details( 1 )
		}
	}
}


function set_excess_cut( val, excs )
{
	if (excs=="")
	{
		if ( val!="" || val!=0 )
		{
			var excs_cut=return_ajax_request_value(val+"_"+document.getElementById('cbo_company_name').value, "get_excess_cut_percent", "requires/woven_order_entry_controller") ;
			document.getElementById('txt_excess_cut').value=excs_cut;
			var txt_plan_cut=(val*1)+((excs_cut*val)/100);
			document.getElementById('txt_plan_cut').value=number_format_common(txt_plan_cut, 6, 0);
		}
	}
	else
	{
		var txt_plan_cut=(val*1)+((excs*val)/100);
		document.getElementById('txt_plan_cut').value=number_format_common(txt_plan_cut, 6, 0);
	}
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( 'txt_amount', 'txt_avg_price*txt_po_quantity', '*','', ddd );
}

function publish_shipment_date(company_id)
{
	var publish_shipment_date=return_global_ajax_value(company_id, 'publish_shipment_date', '', 'requires/woven_order_entry_controller');
	if(publish_shipment_date==1)
	{
		$('#txt_pub_shipment_date').attr('disabled',false);
	}
	else
	{
		$('#txt_pub_shipment_date').attr('disabled',true);
	}
}

function set_pub_ship_date()
{
	var company_id=$('#cbo_company_name').val()
	var publish_shipment_date=return_global_ajax_value(company_id, 'publish_shipment_date', '', 'requires/woven_order_entry_controller');
	if(publish_shipment_date==1)
	{
		$('#txt_pub_shipment_date').attr('disabled',false);
	}
	else
	{
		var txt_org_shipment_date=$('#txt_org_shipment_date').val()
		$('#txt_pub_shipment_date').val(txt_org_shipment_date);
	}
	
}
function format_date(date)
{
	
	var data=date.split('-');
	var new_date=data[2]+'-'+data[1]+'-'+data[0];
	return new_date; 
}
function fnc_order_entry_details( operation )
{
	var po_id=document.getElementById('update_id_details').value;
	var txt_job_no=document.getElementById('txt_job_no').value;
	var grouping=document.getElementById('txt_grouping').value;
	var po_update_period=document.getElementById('po_update_period_maintain').value;
	
	var po_datediff=document.getElementById('txt_po_datedif_hour').value*1;
	var po_update_period=document.getElementById('po_update_period_maintain').value*1;
	var txt_user_id=document.getElementById('txt_user_id').value*1;
	//alert(txt_user_id);
	
	
	//alert(po_update_period);return;
	if(operation==1)
	{
		//alert(operation);return;
		if(txt_user_id!=0)
		{
			if(po_update_period>po_datediff)
			{
				alert("Update Period Does Not Exsits, Please Try Afeter Update Period");return;
			}
		}
		var booking_no_with_approvet_status = return_ajax_request_value(txt_job_no+"_"+po_id, 'booking_no_with_approved_status', 'requires/woven_order_entry_controller')
		var booking_no_with_approvet_status_arr=booking_no_with_approvet_status.split("_");
		if(trim(booking_no_with_approvet_status_arr[0]) !="")
		{
			var al_magg="Main Fabric Approved Booking No "+booking_no_with_approvet_status_arr[0];
			if(booking_no_with_approvet_status_arr[1] !="")
			{
				al_magg+=" and Un-Approved Booking No "+booking_no_with_approvet_status_arr[1];
			}
			al_magg+=" found,\nPlease Un-approved the booking first";
			alert(al_magg)
		    return;
		}
		
		if(trim(booking_no_with_approvet_status_arr[1]) !="")
		{
			var al_magg=" Main Fabric Un-Approved Booking No "+booking_no_with_approvet_status_arr[1]+" Found\n If you update this job\n You have to update color size break down, Pre-cost and booking against this Job ";
			var r=confirm(al_magg);
			if(r==false)
			{
				return;
			}
			else
			{
			}
		}
	}
	
	if (form_validation('update_id*txt_po_no*txt_po_received_date*txt_po_quantity*txt_avg_price*txt_org_shipment_date','Master Info*PO Number*PO Received Date*PO Quantity*Avg. Price*Org. Shipment Date')==false)
	{
		return;   
	}
	
	var response1=return_global_ajax_value( grouping+"**"+txt_job_no, 'check_internal_ref', '', 'requires/woven_order_entry_controller');
	var response1=response1.split("_");
	var inter_ref=$('#txt_grouping').val();
	if(inter_ref!="")
	{
		if(response1[0]==0)
			{
				alert("Not Found This Internal Ref");
				$('#txt_grouping').val('');
				return;
			}
	}
	var response=return_global_ajax_value( txt_job_no+"**"+po_id+"**"+grouping, 'check_ref_no', '', 'requires/woven_order_entry_controller');
	var response=response.split("_");
		if(operation==0)
		{
			if(response[0]==1)
			{
				alert("This Internal Ref already used with another PO No ");
				return;
			}
		}
		else if(operation==1)
		{
			if(grouping!=response[2])
			{
			if(response[0]==1)
				{
					alert("This Internal Ref already used with another PO No ");
					return;
				}
			}
		}
			
		
	var defult_color=0;
	if(operation==0 && po_id=="")
	{
		var r=confirm("Please, Press OK for Default Color Size Breakdown\n Otherwise  Press Cencel");
		if(r==true)
		{
			defult_color=1
		}
		else
		{
			defult_color=0
		}
	}
	if(operation==0 && po_id!="")
	{
	   var r=confirm("Your going to copy a PO.\n Please, Press OK to copy\n Otherwise  Press Cencel");
		if(r==true)
		{
			//defult_color=1
		}
		else
		{
			return;
		}	
	}
	
	/*var txt_po_received_date=new Date(format_date(document.getElementById('txt_po_received_date').value));
	var txt_pub_shipment_date=new Date(format_date(document.getElementById('txt_pub_shipment_date').value));
	var txt_org_shipment_date=new Date(format_date(document.getElementById('txt_org_shipment_date').value));
	var txt_factory_rec_date=new Date(format_date(document.getElementById('txt_factory_rec_date').value));
	
	if (txt_po_received_date.getTime() > txt_pub_shipment_date.getTime()) {
    alert("PO Received Date date is after Pub. Shipment Date !");
	return;
    }
	if (txt_po_received_date.getTime() > txt_org_shipment_date.getTime()) {
    alert("PO Received Date date is after Org. Shipment Date!");
	return;
    }
	
	if (txt_po_received_date.getTime() > txt_factory_rec_date.getTime()) {
    alert("PO Received Date date is after Fac. Receive Date!");
	return;
    }
	
	if (txt_factory_rec_date.getTime() > txt_pub_shipment_date.getTime()) {
    alert("Fac. Receive Date date is after Pub. Shipment Date !");
	return;
    }
	if (txt_factory_rec_date.getTime() > txt_org_shipment_date.getTime()) {
    alert("Fac. Receive Date date is after Org. Shipment Date!");
	return;
    }
	
	if (txt_pub_shipment_date.getTime() > txt_org_shipment_date.getTime()) {
    alert("Pub. Shipment Date is after Org. Shipment Date!");
	return;
    }*/
	
	var txt_avg_price=document.getElementById('txt_avg_price').value;
	if(txt_avg_price==0)
	{
		alert("Avg. Price 0 not accepted")
	}
	else
	{
		
		var data="action=save_update_delete_dtls&operation="+operation+'&defult_color='+defult_color+get_submitted_data_string('cbo_order_status*txt_po_no*txt_po_received_date*txt_pub_shipment_date*txt_org_shipment_date*txt_factory_rec_date*txt_po_quantity*txt_avg_price*txt_amount*txt_excess_cut*txt_plan_cut*cbo_status*txt_details_remark*update_id_details*update_id*cbo_packing*cbo_delay_for*cbo_packing_po_level*color_size_break_down*txt_grouping*cbo_projected_po*cbo_tna_task*set_breck_down*txt_file_no*cbo_company_name',"../../");
		freeze_window(operation);
		http.open("POST","requires/woven_order_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_on_submit_reponse;
	}
}
	 
function fnc_on_submit_reponse()
{
	if(http.readyState == 4) 
	{
		 var reponse=http.responseText.split('**');
		 show_msg(trim(reponse[0]));
		if(trim(reponse[0]) ==24)
		 {
			release_freezing();	
			return; 
		 }

		 show_list_view(document.getElementById('txt_job_no').value,'show_po_active_listview','po_list_view','../woven_order/requires/woven_order_entry_controller','');
		 show_list_view(document.getElementById('txt_job_no').value,'show_deleted_po_active_listview','deleted_po_list_view','../woven_order/requires/woven_order_entry_controller','');
		 
		
		 if(trim(reponse[0]) !=11)
		 {
		reset_form('','','txt_po_no*txt_po_received_date*txt_pub_shipment_date*txt_org_shipment_date*txt_factory_rec_date*txt_po_quantity*txt_avg_price*txt_amount*txt_excess_cut*txt_plan_cut*cbo_status*txt_details_remark*cbo_delay_for*show_textcbo_delay_for*cbo_packing_po_level*update_id_details*color_size_break_down*txt_grouping*cbo_projected_po*cbo_tna_task','','');
		 $('#txt_avg_price').removeAttr('disabled');
		 $('#txt_avg_price').removeAttr('title');
		 document.getElementById('txt_total_job_quantity').value=trim(reponse[2])
		 document.getElementById('txt_avg_unit_price').value=trim(reponse[3])
		 document.getElementById('txt_job_total_price').value=trim(reponse[4])
		 document.getElementById('set_pcs').value=trim(reponse[5])
		 document.getElementById('set_unit').value=trim(reponse[6])
		 }
		 set_button_status(0, permission, 'fnc_order_entry_details',2);
		 load_drop_down( 'requires/woven_order_entry_controller', document.getElementById('txt_job_no').value, 'load_drop_down_projected_po', 'projected_po_td' )
	release_freezing();
	}
	
}
function get_details_form_data(id,type,path)
{
	reset_form('','','cbo_order_status*txt_po_no*txt_po_received_date*txt_pub_shipment_date*txt_org_shipment_date*txt_po_quantity*txt_avg_price*txt_amount*txt_excess_cut*txt_plan_cut*cbo_status*txt_details_remark*cbo_delay_for*show_textcbo_delay_for*cbo_packing_po_level*update_id_details*color_size_break_down*txt_grouping*cbo_projected_po*cbo_tna_task','','');
	get_php_form_data( id, type, path );
}

function set_tna_task()
{
	var txt_po_received_date=document.getElementById('txt_po_received_date').value
	var txt_pub_shipment_date=document.getElementById('txt_pub_shipment_date').value
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	load_drop_down( 'requires/woven_order_entry_controller', txt_po_received_date+"_"+txt_pub_shipment_date+"_"+cbo_buyer_name, 'load_drop_down_tna_task', 'tna_task_td' )
}

function sub_dept_load(cbo_buyer_name,cbo_product_department)
{
	if(cbo_buyer_name ==0 || cbo_product_department==0 )
	{
		return
	}
	else
	{
		load_drop_down( 'requires/woven_order_entry_controller',cbo_buyer_name+'_'+cbo_product_department, 'load_drop_down_sub_dep', 'sub_td' )
	}
}

function pop_entry_actual_po()
{
	var po_id = $('#update_id_details').val();
	var txt_job_no = $('#txt_job_no').val();
	if(po_id=="")
	{
		alert("Save The PO First");
		return;
	}
	var page_link='requires/woven_order_entry_controller.php?action=actual_po_info_popup&po_id='+po_id+'&txt_job_no='+txt_job_no;
	var title='Actual Po Entry Info';
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=400px,height=300px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
	{
		//var theform=this.contentDoc.forms[0]
		//var actual_infos=this.contentDoc.getElementById("actual_po_infos").value;  
		//$('#actual_po_infos_'+row_id).val(actual_infos);            
	}
}
//Dtls Form End -------------------------------------------------------------------------------------

function open_terms_condition_popup(page_link,title)
{
	var txt_job_no=document.getElementById('txt_job_no').value;
	if (txt_job_no=="")
	{
		alert("Save The Job No First")
		return;
	}	
	else
	{
	    page_link=page_link+get_submitted_data_string('txt_job_no','../../');
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=470px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
		}
	}
}

function po_update_period(company_id)
	{ 
		//var com=$('#cbo_company_name').val();
		//alert(com);
		get_php_form_data($('#cbo_company_name').val(),'update_period_maintained_data','requires/woven_order_entry_controller' );
		get_php_form_data($('#cbo_company_name').val(),'po_received_date_maintained_data','requires/woven_order_entry_controller' );
		
		var order_status=document.getElementById('cbo_order_status').value;
		var po_current_date_maintain=document.getElementById('po_current_date_maintain').value;
		var current_date='<?php echo date('d-m-Y'); ?>'
		if(po_current_date_maintain==1 && order_status==1)
		{
			$('#txt_po_received_date').attr('disabled','disabled');
			$('#txt_po_received_date').val(current_date);
		}
		else
		{
		$('#txt_po_received_date').val('');	
		$('#txt_po_received_date').removeAttr('disabled','');
		}
		
		
	}
	function po_recevied_date()
	{
		
		var po_current_date_maintain=document.getElementById('po_current_date_maintain').value;
		var order_status=document.getElementById('cbo_order_status').value;
		var current_date='<?php echo date('d-m-Y'); ?>'
		if(po_current_date_maintain==1 && order_status==1)
		{
			$('#txt_po_received_date').attr('disabled','disabled');
			$('#txt_po_received_date').val(current_date);
		}
		else
		{
		$('#txt_po_received_date').val('');	
		$('#txt_po_received_date').removeAttr('disabled','');
		}
		
	}
	
	
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
<!-- Important Field outside Form -->  
     <?php echo load_freeze_divs ("../../",$permission);  ?>
    	 
     <table width="90%" cellpadding="0" cellspacing="2" align="center" >
     	<tr>
        	<td width="70%" align="center" valign="top">  <!--   Form Left Container -->
            	<fieldset style="width:950px;">
                <legend>Garments Order Entry </legend>
                <form name="orderentry_1" id="orderentry_1" autocomplete="off">
            		<table  width="900" cellspacing="2" cellpadding="0" border="0">
                       <tr>
                            <td  width="130" height="" align="right">Job No</td>              <!-- 11-00030  -->
                                <td  width="170" >
                                <input style="width:160px;" type="text" title="Double Click to Search" onDblClick="openmypage('requires/woven_order_entry_controller.php?action=order_popup','Job/Order Selection Form')" class="text_boxes" placeholder="New Job No" name="txt_job_no" id="txt_job_no" readonly />                               
                                </td>
                                <td  width="130" align="right" class="must_entry_caption">Company Name </td>
                                <td width="170">
                               		<?php
									//echo "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name";
							   			echo create_drop_down( "cbo_company_name", 172, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/woven_order_entry_controller', this.value, 'load_drop_down_location', 'location' ); location_select();load_drop_down( 'requires/woven_order_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td' ); load_drop_down( 'requires/woven_order_entry_controller', this.value, 'load_drop_down_agent', 'agent_td' );publish_shipment_date( this.value ); load_drop_down( 'requires/woven_order_entry_controller', this.value, 'load_drop_down_party_type', 'party_type_td' );publish_shipment_date( this.value );po_update_period( this.value )" );
							   		?> 
                                     <input type="hidden" name="po_update_period_maintain" id="po_update_period_maintain" style="width:50px;" class="text_boxes" />
                                      <input type="hidden" name="po_current_date_maintain" id="po_current_date_maintain" style="width:50px;" class="text_boxes" />
                                 </td>
                              <td width="130" align="right" class="must_entry_caption">Location Name</td>
                              <td id="location">
                             	 <?php 
							  		echo create_drop_down( "cbo_location_name", 172, $blank_array,"", 1, "-- Select --", $selected, "" );
								 ?>	  
                              </td>
                        </tr>
                        <tr>
                        	<td align="right" class="must_entry_caption">Buyer Name</td>
                              <td id="buyer_td">
                              <?php 
                                echo create_drop_down( "cbo_buyer_name", 172, $blank_array,"", 1, "-- Select Buyer --", $selected, "" );
                               ?>	  
                              </td>
                            <td align="right" class="must_entry_caption">Style Ref.</td>
                        	<td>
                            	<input class="text_boxes" type="text" style="width:160px" placeholder="Double Click for Quotation" name="txt_style_ref" id="txt_style_ref" onDblClick="open_qoutation_popup('requires/woven_order_entry_controller.php?action=quotation_id_popup','Quotation ID Selection Form')"/>	
                            </td>
                            <td align="right">
                               Style Description
                            </td>
                            <td>	
                                <input class="text_boxes" type="text" style="width:160px;" name="txt_style_description" id="txt_style_description"/>
                            </td>
                        </tr>
                        <tr>
                            <td height="" align="right" class="must_entry_caption">Pord. Dept.</td>   
                                <td >
                                <?php
							   		echo create_drop_down( "cbo_product_department", 115, $product_dept, "", 1, "-Select-", $selected, "sub_dept_load(document.getElementById('cbo_buyer_name').value,this.value)", "", "" );
							    ?>
                                <input class="text_boxes" type="text" style="width:40px;" name="txt_product_code" id="txt_product_code" maxlength="10" title="Maximum 10 Character" />
                                </td>
                               
                               <td align="right">Sub. Dept </td>
                                <td id="sub_td">
                                <?php	  //where status_active =1 and is_deleted=0
									 echo create_drop_down( "cbo_sub_dept", 172, $blank_array,"", 1, "-- Select Sub Dep --", $selected, "" );
									 ?>
                                </td>
                              <td align="right">Currency</td>
                              <td>
                                <?php 
							  	echo create_drop_down( "cbo_currercy", 172, $currency,'', 0, "",2, "" );
								?>	  
                              </td>
                              
                        </tr>
                        <tr>
                            
                                <td  align="right">Repeat No</td>
                                <td > <input style="width:160px;"  class="text_boxes"  name="txt_repeat_no" id="txt_repeat_no"  />
                                </td>
                              <td  align="right">Region</td>
                              <td>
                              <?php 
							  	echo create_drop_down( "cbo_region", 172, $region, 1, "-- Select Region --", $selected, "" );
								?>	  
                              </td>
                              <td  align="right" class="must_entry_caption">
                               		Product Category
                              </td>
                              <td>
                             
                              <?php
                              echo create_drop_down( "txt_item_catgory", 172, $product_category,"", 1, "-- Select Product Category --", 1, "","","" );
							  ?>
                              </td>
                              </tr>
                        <tr>
                        	<td align="right" class="must_entry_caption">Team Leader</td>   
    						<td>
                             <?php  
							  	echo create_drop_down( "cbo_team_leader", 172, "select id,team_leader_name from lib_marketing_team where status_active =1 and is_deleted=0 order by team_leader_name","id,team_leader_name", 1, "-- Select Team --", $selected, "load_drop_down( 'requires/woven_order_entry_controller', this.value, 'cbo_dealing_merchant', 'div_marchant' ) " );
								?>		
                            </td>
							<td align="right" class="must_entry_caption">Dealing Merchant</td>   
    						<td id="div_marchant" > 
                            <?php 
							  	echo create_drop_down( "cbo_dealing_merchant", 172, $blank_array,"", 1, "-- Select Team Member --", $selected, "" );
								?>	
                           </td>
                           
                            <td align="right">Ship Mode</td>
                        	<td>
                            	 <?php	
									echo create_drop_down( "cbo_ship_mode", 172,$shipment_mode, 1, "", $selected, "" ); ?>	
                            </td>
                        </tr>
                        <tr>
                        	<td align="right">
                               Remarks
                            </td>
                            <td colspan="3">	
                                <input class="text_boxes" type="text" style="width:465px;"  name="txt_remarks" id="txt_remarks"/>
                            </td>
                            <td align="right" class="must_entry_caption">Packing </td>
                                <td  >
                                <?php	
									echo create_drop_down( "cbo_packing", 172, $packing,"", 1, "--Select--", $selected, "","","" ); ?>
                                </td>
                            </tr>
                            <tr>
                            <td align="right" class="must_entry_caption">Order Uom</td>
                        	<td>
                            	        <?php 
                                        //echo create_drop_down( "cbo_order_uom",60, $unit_of_measurement, "",0, "", 1, "show_hide_button(this.value)","","1,58" );
									    echo create_drop_down( "cbo_order_uom",60, $unit_of_measurement, "",0, "", 1, "","","1,58" );
										?>
                                        <input type="button" id="set_button" class="image_uploader" style="width:95px;" value="Item Details" onClick="open_set_popup(document.getElementById('cbo_order_uom').value)" /> 
                                        
                            </td>
                            <td align="right" class="must_entry_caption">SMV</td>
                        	<td>
                            	<input class="text_boxes_numeric" type="text" style="width:160px;" name="tot_smv_qnty" id="tot_smv_qnty" readonly/>
                            </td>
                            <td align="right">
                               Season 
                            </td>
                            <td>	
                                <input class="text_boxes" type="text" style="width:160px;" name="txt_season" id="txt_season"/>
                            </td>
                        </tr>
                        <tr>
                           <td align="right">Agent </td>
                          <td id="agent_td">
                                <?php	  //where status_active =1 and is_deleted=0
									 echo create_drop_down( "cbo_agent", 172, $blank_array,"", 1, "-- Select Agent --", $selected, "" );
									 ?>
                          </td>
                              <td align="right" >Client</td>
                        	<td id="party_type_td">
                            	  <?php	  //where status_active =1 and is_deleted=0
									 echo create_drop_down( "cbo_client", 172, $blank_array,"", 1, "-- Select Client --", $selected, "" );
									 ?>
                            </td>
                               <td align="right"></td>
                        	<td>
                            	
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Images</td>
                        	<td>
                            	<input type="button" class="image_uploader" style="width:172px" value="CLICK TO ADD/VIEW IMAGE" onClick="file_uploader ( '../../', document.getElementById('update_id').value,'', 'knit_order_entry', 0 ,1)">
                               
                            </td>
                            <td align="right">
                            	File
                            </td>
                             <td>
                            	<input type="button" class="image_uploader" style="width:172px" value="CLICK TO ADD FILE" onClick="file_uploader ( '../../', document.getElementById('update_id').value,'', 'knit_order_entry', 2 ,1)">
                            </td>
                            <td align="right"> </td>
                        	<td>
                            	<input type="button" id="set_button" class="image_uploader" style="width:160px;" value="Internal Ref" onClick="open_terms_condition_popup('requires/woven_order_entry_controller.php?action=terms_condition_popup','Terms Condition')" />
                            </td>
                            
                        </tr>
                        <tr>
                        	<td align="center" colspan="6" height="15" valign="middle">
                             	<input type="hidden" id="update_id"> 
                                <input type="hidden" id="txt_quotation_id"><input type="hidden" id="set_breck_down" />     
                                <input type="hidden" id="item_id" />
                                <input type="hidden" id="tot_set_qnty" />  
                                
                            </td>
                        </tr>
                         <tr>
                        	<td align="center" colspan="6" valign="middle" class="button_container">
                           <?php echo load_submit_buttons( $permission, "fnc_order_entry", 0,0,"reset_form('orderentry_1*orderdetailsentry_2','deleted_po_list_view*po_list_view','')",1);?>
                            </td>
                        </tr>
                         
                    </table>
                 </form>
              </fieldset>
           </td>
         </tr>
         <tr>
         	<td align="center">
            <fieldset style="width:1050px;">
                <legend>PO Details Entry</legend>
            	<form id="orderdetailsentry_2" autocomplete="off">
                    <table style="border:none" cellpadding="0" cellspacing="2" border="0">
                    <thead class="form_table_header">
                        <tr align="center" >
                             
                             <th  width="100" height="27">Order Status </th>
                             <th  width="100" height="27">PO No</th>
                             
                             <th  width="90" >PO Received Date  </th>
                             <th  width="90">Pub. Shipment Date</th>
                             <th  width="90">Org. Shipment Date</th>
                             <th  width="90">Fac. Receive Date</th>
                             <th  width="70">PO Quantity  </th>
                             <th  width="60">Avg. Price</th>
                             <th  width="85">Amount</th>
                             <th  width="60">Excess Cut %</th>
                              <th  width="70">Plan Cut</th>
                              <!-- <th  width="85">Country</th>-->
                             <th  width="85">Status</th>
                           </tr> 
                           </thead>
                        <tr>
                           
                                <td height="22" >
                                    <?php  echo create_drop_down( "cbo_order_status", 100, $order_status, 0, "", $selected,"", "po_recevied_date( this.value )" ); ?>
                                     
                                </td>
                              <td height="22" ><input class="text_boxes" name="txt_po_no" id="txt_po_no" type="text" value="" onDblClick="openmypage_for_po_copy('requires/woven_order_entry_controller.php?action=order_popup_for_copy','Order Selection Form')"  style="width:100px"/></td>
                              <td ><input name="txt_po_received_date" id="txt_po_received_date" class="datepicker" type="text" onChange="set_tna_task()" value="" style="width:100px;"readonly/>
                              </td>
                              <td ><input name="txt_pub_shipment_date" id="txt_pub_shipment_date" class="datepicker" type="text" onChange="set_tna_task()" value="" style="width:100px;" readonly/>
                              </td>
                              <td  ><input  name="txt_org_shipment_date" id="txt_org_shipment_date" class="datepicker" type="text" value=""  style="width:80px;" onChange="set_pub_ship_date()" readonly/>
                              </td>
                               <td><input  name="txt_factory_rec_date" id="txt_factory_rec_date" class="datepicker" type="text" value=""  style="width:80px;"  readonly/>
                              </td>
                              
                              <td>
                              	<input name="txt_po_quantity" id="txt_po_quantity" onDblClick="open_color_size_popup('requires/size_color_breakdown_controller.php?action=populate_size_color_breakdown_pop_up','Color Size Entry Form')" class="text_boxes_numeric" type="text"  style="width:70px" onBlur="set_excess_cut(this.value,document.getElementById('txt_excess_cut').value)" placeholder="Dbl. Click"/></td>
                              <td >
                              	<input name="txt_avg_price" id="txt_avg_price" onBlur="math_operation( 'txt_amount', 'txt_avg_price*txt_po_quantity', '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} )"  class="text_boxes_numeric" type="text" value=""  style="width:55px "  /></td>
                              <td>
                              	<input name="txt_amount" id="txt_amount" class="text_boxes_numeric" type="text" value=""  style="width:85px " readonly/>
                              </td>
                              <td >
                              	<input name="txt_excess_cut" id="txt_excess_cut" onBlur="set_excess_cut( document.getElementById('txt_po_quantity').value, this.value )" class="text_boxes_numeric" type="text" style="width:55px "/>
                               </td>
                               <td><input name="txt_plan_cut" id="txt_plan_cut"  class="text_boxes_numeric" type="text" value=""  style="width:65px "/></td>
                               <!--<td> 
                                    
                                        <?php
                                             // echo create_drop_down( "cbo_po_country", 90,"select id,country_name from lib_country where status_active=1 and is_deleted=0 order by country_name", "id,country_name", 1, "--- Select ", "" ); ?>
                                          
                                         
                                   </td>-->
                                   
                             
                              
                              <td >
							  <?php
                              echo create_drop_down( "cbo_status", 85, $row_status, 0, "", 1, "" ); 
							  ?>
                              </td>
                                   
                            	<td ></td>
                            </tr>
                            <tr>
                            	<td align="right"><strong>Projected Po</strong></td>
                                <td height="20" id="projected_po_td">
                                <?php
                                   echo create_drop_down( "cbo_projected_po", 110,$blank_array, "", 1, "--Select--", "" ); 
								?>
                                </td>
                                <td align="right"><strong>TNA From /Upto</strong></td>
                                <td height="20" id="tna_task_td">
                                <?php
                                   echo create_drop_down( "cbo_tna_task", 110,$blank_array, "", 1, "--Select--", "" ); 
								?>
                                </td>
                                <td align="right"><strong>Internal Ref/Grouping</strong></td>
                                <td  height="20">
                                <input type="text" id="txt_grouping" class="text_boxes" style="width:70px">
                                </td>
                                <td align="right"><strong>Delay For</strong></td>
                                <td colspan="4" height="20">
                                <?php
                                echo create_drop_down( "cbo_delay_for", 315, $delay_for, 0, "", 1, "" ); 
							    ?>
                                </td>
                                	
                        	</tr>
                             <tr>
                             <td align="right"><strong>Packing</strong> </td>
                                <td  >
                                <?php	
									echo create_drop_down( "cbo_packing_po_level", 100, $packing,"", 1, "--Select--", "", "","","" ); ?>
                                </td>
                            	<td align="right"><strong>Remarks</strong></td>
                                <td colspan="3" height="20">
                                <input type="text" id="txt_details_remark" class="text_boxes" style="width:283px">
                                </td>
                                <td align="right"><input type="button" value="Actual Po No" class="image_uploader" style="width:80px" onClick="pop_entry_actual_po()"/></td>
                                <td align="right"><strong>File No</strong></td>
                                <td  height="20">
                                <input type="text" id="txt_file_no" class="text_boxes" style="width:180px">
                                </td>
                                	
                        	</tr>
                            <tr>
                                <td colspan="12" height="20">
                                <input type="hidden" id="update_id_details">
                                <input type="hidden" id="color_size_break_down" value="" /> 
                                 <input type="hidden" id="txt_po_datedif_hour"  />   
                                  <input type="hidden" id="txt_user_id"  />   
                                </td>	
                        	</tr>
                            <tr>
                                <td colspan="12" height="50" valign="middle" align="center" class="button_container">
									<?php
									$dd="disable_enable_fields( 'txt_avg_price', 0 )";
									echo load_submit_buttons( $permission, "fnc_order_entry_details", 0,0 ,"reset_form('orderdetailsentry_2','','','',$dd)",2); 
									?>
                                </td>
                            </tr>
                             
                            <tr align="center">
                                <td colspan="12" id="po_list_view">
                                </td>	
                        	</tr>
                            <tr align="center">
                                <td colspan="12" id="deleted_po_list_view">
                                </td>	
                        	</tr>
                            <tr align="center">
                                <td colspan="12" id="">
                                    <table>
                                     <tr bgcolor="">
                                    <td  width="128" colspan="2">&nbsp;Projected Job Quantity</td>
                                    <td width="180"><input  value="" name="txt_projected_job_quantity" id="txt_projected_job_quantity" style="width:103px " class="text_boxes_numeric" readonly>
                                   <!-- <input type="text" class="text_boxes" style="width:40px;" readonly name="set_pcs" id="set_pcs" />-->
                                    <?php 
                                        echo create_drop_down( "pojected_set_pcs",60, $unit_of_measurement, "",1, "--", "", "",1,"1,58" );
										?>
                                    </td> 
                                    
                                    <td>&nbsp;&nbsp;Projected Avg Unit Price</td>
                                    <td  colspan="2">
                                    <div><input name="txt_projected_price" type="text" class="text_boxes_numeric" id="txt_projected_price" style="width:85px; text-align:right " value="<?php //echo $txt_unit_price; ?>" readonly>&nbsp;&nbsp;
                                   <!-- <input type="text" class="text_boxes" style="width:40px;" readonly name="set_unit" id="set_unit" value="USD"/>-->
                                    <?php
                                     echo create_drop_down( "projected_set_unit", 60, $currency,"", 1, "--", "", "" ,1,"");
									 ?>
                                    </div>
                                    </td>
                                    <td>&nbsp;Projected Total Price </td>
                                    <td><input  type="text" style="width:173px "  class='text_boxes_numeric' name="txt_project_total_price" id="txt_project_total_price" value="<?php echo $txt_total_price; ?>" readonly> 	</td>
                                    
                                    </tr>
                                    <tr bgcolor="">
                                    <td  width="128" colspan="2">&nbsp;Job Quantity</td>
                                    <td width="180"><input  value="" name="txt_total_job_quantity" id="txt_total_job_quantity" style="width:103px " class="text_boxes_numeric" readonly>
                                   <!-- <input type="text" class="text_boxes" style="width:40px;" readonly name="set_pcs" id="set_pcs" />-->
                                    <?php 
                                        echo create_drop_down( "set_pcs",60, $unit_of_measurement, "",1, "--", "", "",1,"1,58" );
										?>
                                    </td> 
                                    
                                    <td>&nbsp;&nbsp;Avg Unit Price</td>
                                    <td  colspan="2">
                                    <div><input name="txt_avg_unit_price" type="text" class="text_boxes_numeric" id="txt_avg_unit_price" style="width:85px; text-align:right " value="<?php echo $txt_unit_price; ?>" readonly>&nbsp;&nbsp;
                                   <!-- <input type="text" class="text_boxes" style="width:40px;" readonly name="set_unit" id="set_unit" value="USD"/>-->
                                    <?php
                                     echo create_drop_down( "set_unit", 60, $currency,"", 1, "--", "", "" ,1,"");
									 ?>
                                    </div>
                                    </td>
                                    <td>&nbsp;Total Price </td>
                                    <td><input  type="text" style="width:173px "  class='text_boxes_numeric' name="txt_job_total_price" id="txt_job_total_price" value="<?php echo $txt_total_price; ?>" readonly> 	</td>
                                    
                                    </tr>
                                    </table>
                                </td>	
                        	</tr>
                            
                       </table>
            	</form>
                </fieldset>
            </td>
         
         </tr>
	</table>
	</div>
</body>
   <script>
	set_multiselect('cbo_delay_for','0','0','','');
	//set_multiselect( fld_id, max_selection, is_update, update_values, on_close_fnc_param )
</script>        
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>