<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$location_arr=return_library_array( "select id, location_name from lib_location",'id','location_name');
$item_group_arr=return_library_array( "select id, item_name from lib_item_group",'id','item_name');
$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
$batch_arr=return_library_array( "select id, batch_no from pro_batch_create_mst",'id','batch_no');
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

if ($action=="load_drop_down_store")
{
		echo create_drop_down( "cbo_store_name", 140, "select a.id,a.store_name from lib_store_location a,lib_store_location_category  b where a.id=b.store_location_id and a.status_active=1 and a.is_deleted=0 and a.company_id=$data and b.category_type in (5,6,7) group by a.id,a.store_name order by a.store_name","id,store_name", 1, "-- Select Store --", 0, "",0 );  	 
	   exit();
}

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location", 140, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select Location --", $selected, "",0 );     	 
	exit();
}

if($action=="itemdesc_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{  
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="850" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="160">Item Category</th>
                    <th width="200">Item Group</th>
                    <th width="250">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>                    
                    <td>
                        <?php  
							
							echo create_drop_down( "cbo_item_category", 160, $item_category,"", 1, "-- Select --", 0, "", 0,"5,6,7","","","" );
                        ?>
                    </td>
                    <td width="" align="center">
                    	<?php  
                       
							echo create_drop_down( "cbo_item_group", 160, "select id,item_name from lib_item_group where item_category in(5,6,7) and status_active=1 and is_deleted=0 order by item_name", "id,item_name", 1, "-- Select --", 0, "", 0,"" );
                        ?>	
                    </td>
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:100px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:100px" placeholder="To Date" />
                    </td>
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_item_category').value+'_'+document.getElementById('cbo_item_group').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_item_search_list_view', 'search_div', 'chemical_dyes_issue_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="" />
                      <input type="hidden" id="new_prod_id" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


//this is need
if($action=="create_item_search_list_view")
{
	$ex_data = explode("_",$data);
	$item_category = $ex_data[0];
	$item_group = $ex_data[1];
	$fromDate = $ex_data[2];
	$toDate = $ex_data[3];
	$company = $ex_data[4];
	
	$sql_cond="";
	
	if( $item_category!=0 )  $item_category=" and b.item_category='$item_category'"; else $item_category="and b.item_category  in(5,6,7)";
	if( $item_group!=0 )  $item_group=" and c.item_group_id='$item_group'"; else $item_group="";
	
	if( $fromDate!="" || $toDate!="" )
	if($db_type==0)
		{
			 $sql_cond .= " and a.issue_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
		if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
		}
	if($db_type==2)
		{
			 $sql_cond .= " and a.issue_date  between '".change_date_format($fromDate,'yyyy-mm-dd',"-",1)."' and '".change_date_format($toDate,'yyyy-mm-dd','-',1)."'";
		if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
		}
	 

	
	$sql ="select b.id as tran_id,a.id as issue_id,c.id as prod_id,c.product_name_details,c.item_description,c.item_group_id,c.sub_group_name,c.item_size 
			from inv_issue_master a, inv_transaction b left join product_details_master c on b.prod_id=c.id and c.status_active=1
			where a.id=b.mst_id and a.status_active=1 and c.status_active=1  and b.transaction_type in(2,4) and c.current_stock>0 $sql_cond $item_category $item_group ";
	
	$item_group_arr=return_library_array( "select id, item_name from lib_item_group",'id','item_name');
	$arr=array(2=>$item_group_arr);
 	echo create_list_view("list_view", "Product Id, Issue Id, Item Group, Item SubGroup, Item Description, Item Size, Issue Challan No","60,60,150,150,230,100,80","900","260",0, $sql , "js_set_value", "tran_id,issue_id,prod_id,item_group_id", "", 1, "0,0,item_group_id,0,0,0,0", $arr, "prod_id,issue_id,item_group_id,sub_group_name,item_description,item_size,challan_no", "","",'0,0,0,0,0') ;	
	exit();
	
}


if($action=="populate_data_from_data")
{  

  // print_r($data);die;
	$ex_data = explode("_",$data);
	$trans_id = $ex_data[0];
	$issue_id = $ex_data[1];
	$prod_id=$ex_data[2];
	$item_group=$ex_data[3];
	if($db_type==0) $prod_des="concat(c.sub_group_name,',',c.item_description,',',c.item_size)";
	if($db_type==2) $prod_des="(c.sub_group_name||','||c.item_description||','||c.item_size)";
	$sql = "select  c.id as prod_id, $prod_des as pro_description,c.unit_of_measure,c.item_group_id,b.item_category
			from inv_issue_master a, inv_transaction b, product_details_master c
			where a.id=b.mst_id and b.prod_id=c.id and c.id=$prod_id and c.item_group_id=$item_group and b.id=$trans_id and a.id=$issue_id and b.transaction_type in(2) ";	
   
  // echo $sql;
   
    
	$res = sql_select($sql);
	$item_group_arr=return_library_array( "select id, item_name from lib_item_group",'id','item_name');
	foreach($res as $row)
	{
		
		$totalIssued = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=2 group by prod_id","to_issue");
	   $totalIssuedReturn = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue_return","inv_receive_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=4 group by prod_id","to_issue_return");
	  $issue_remain=$totalIssued-$totalIssuedReturn;
		
		echo "$('#total_issue').val($issue_remain);\n";
		echo "$('#txt_item_description').val('".$row[csf("pro_description")]."');\n";
		echo "$('#cbo_uom').val(".$row[csf("unit_of_measure")].");\n";
		echo "$('#cbo_item_category').val(".$row[csf("item_category")].");\n";
		echo "$('#cbo_item_group').val(".$row[csf("item_group_id")].");\n";
		echo "$('#txt_prod_id').val(".$row[csf("prod_id")].");\n";
   	}	
	exit();	
}

 //for batch popup
 
if($action=="batch_popup")
{
  	echo load_html_head_contents("Batch Info","../../../", 1, 1, '','1','');
	extract($_REQUEST);
?>
     
	<script>
	
	function js_set_value( data)
	{  
	
		var data=data.split("_");
		document.getElementById('hidden_batch_no').value=data[0];
		document.getElementById('hidden_batch_id').value=data[1];
		document.getElementById('issue_master_id').value=data[2];
		parent.emailwindow.hide();
	}
	
    </script>

</head>

<body>
<div align="center" style="width:100%; overflow-y:hidden;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
            <table cellpadding="0" cellspacing="0" width="500" class="rpt_table">
                <thead>
                    <th>Search By</th>
                    <th>Search</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" value="">
                        <input type="hidden" name="hidden_batch_no" id="hidden_batch_no" value="">
                         <input type="hidden" name="issue_master_id" id="issue_master_id" value="">
               
                    </th> 
                </thead>
                <tr class="general">
                    <td align="center">	
                        <?php
                            $search_by_arr=array(1=>"Batch No",2=>"Booking No");
                            echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>                 
                    <td align="center">				
                        <input type="text" style="width:140px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 						
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<?php echo $cbo_company_id; ?>, 'create_batch_search_list_view', 'search_div', 'chemical_dyes_issue_return_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
            <div id="search_div" style="margin-top:10px"></div>   
        </form>
 
</div>    
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}
//bat
if($action=="create_batch_search_list_view")
{
	$data=explode('_',$data);
	
	$search_string="%".trim($data[0])."%";
	$search_by =$data[1];
	$company_id =$data[2];
	
	if($search_by==1)
		$search_field='b.batch_no';
	else
		$search_field='a.booking_no';
	$sql = sql_select("select a.id as mst_id,a.issue_number,b.id as batch_id,b.batch_no,a.issue_purpose,a.location_id,a.order_id,a.challan_no from inv_issue_master a, pro_batch_create_mst b where a.company_id=$company_id and a.batch_no=b.id and $search_field like '$search_string' and a.status_active=1 and a.is_deleted=0 and a.entry_form=5"); 

	?>
    	
         <table width="730" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                  <tr>                	 
                        <th width="30">Sl</th>
                        <th width="130">Issue number</th>
                        <th width="100">Batch No</th>
                        <th width="120">Issue Purpose</th>
                        <th width="100">Location</th>
                        <th width="100">Order ID</th>
                        <th width="100">Challan no</th>
                   </tr>
              </thead>
           </table>
      <div style="width:710px; overflow-y:scroll; max-height:300px;font-size:12px; overflow-x:hidden; cursor:pointer;" id="scroll_body">
          <table width="710" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
             <tbody>
          	<?php 
				foreach($sql as $row)
				  {					
					if($i%2==0)
						$bgcolor="#E9F3FF";
					else 
						$bgcolor="#FFFFFF";
 				?>
                 	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='js_set_value("<?php echo $row[csf("batch_no")]."_".$row[csf("batch_id")]."_".$row[csf("mst_id")];?>")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="120"  align="center"><p><?php echo $row[csf("issue_number")]; ?></p></td>

                        <td width="150"  align="center"><p><?php echo $row[csf("batch_no")]; ?></p></td>
                        <td width="100"  align="center"><p><?php echo $yarn_issue_purpose[$row[csf("issue_purpose")]]; ?></p></td>
                        <td width="150" align="center"><p><?php echo $location_arr[$row[csf("location_id")]]; ?></p></td>
                        <td width="100" align="center"><p><?php echo $row[csf("order_id")]; ?></p></td>
                        <td width="100"  align="center"><p><?php echo $row[csf("challan_no")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
              
            </tbody>
        </table> 
</div>
    
    <?php
}

if($action=="populate_batch_no_data")
{ 
     
	 $data=explode("**",$data);
	 $company_name=$data[0];
	 $batch_id=$data[1];
	 $mst_id=$data[2];
	?>
     <table width="260" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Item name</th>
                    <th width="100">Issue qty</th>
                </tr>
            </thead>
            <tbody>
            <?php
			$item_arr=return_library_array( "select id, product_name_details from product_details_master",'id','product_name_details');
			
		     $sql="select  a.id,a.prod_id,sum(a.cons_quantity) as issue_qty from  inv_transaction a, inv_issue_master b where b.id=$mst_id and b.batch_no=$batch_id and b.id=a.mst_id and a.transaction_type=2 and b.entry_form=5 and b.company_id=$company_name group by a.id,a.prod_id";
		
			//echo $sql;
			$item_result=sql_select($sql);
          	foreach($item_result as $row)
			{		
			
			    $totalIssued = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=2 group by prod_id","to_issue");
	            $totalIssuedReturn = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue_return","inv_receive_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=4 group by prod_id","to_issue_return");
	            $issue_remain=$totalIssued-$totalIssuedReturn;	
				if($issue_remain>0)
				{	
		        if($i%2==0) $bgcolor="#E9F3FF";
			    else   $bgcolor="#FFFFFF";
				
 				?>
                 	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("prod_id")]."_".$row[csf("id")];?>","child_form_item_data","requires/chemical_dyes_issue_return_controller")'  style="cursor:pointer" >
                        <td width="150"  align="center"><p><?php echo $item_arr[$row[csf("prod_id")]]; ?></p></td>
                        <td width="100"  align="center"><p><?php echo $issue_remain; ?></p></td>
                   </tr>
              <?php $i++; 
				}
			} ?>
              
            </tbody>
        </table> 

   <?php	
}

if($action=="child_form_item_data")
{
	
    $data=explode("_",$data);
	$prod_id=$data[0];
	$trans_id=$data[1];
	if($db_type==2) $prd_des="(c.sub_group_name||','||c.item_description||','||c.item_size)";
	if($db_type==0) $prd_des="concat(c.sub_group_name,',',c.item_description,',',c.item_size)";
	$sql = "select  c.id as prod_id,$prd_des as pro_description,c.unit_of_measure,c.item_group_id,b.item_category
			from inv_transaction b, product_details_master c
			where  b.prod_id=c.id and c.id=$prod_id and b.id=$trans_id ";	
			//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		
	   	$totalIssued = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=2 group by prod_id","to_issue");
	    $totalIssuedReturn = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue_return","inv_receive_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=4 group by prod_id","to_issue_return");
	    $issue_remain=$totalIssued-$totalIssuedReturn;
		echo "$('#total_issue').val($issue_remain);\n";
		echo "$('#txt_item_description').val('".$row[csf("pro_description")]."');\n";
		echo "$('#cbo_uom').val(".$row[csf("unit_of_measure")].");\n";
		echo "$('#cbo_item_category').val(".$row[csf("item_category")].");\n";
		echo "$('#cbo_item_group').val(".$row[csf("item_group_id")].");\n";
		echo "$('#txt_prod_id').val(".$row[csf("prod_id")].");\n";
		echo "$('#new_prod_id').val(".$row[csf("prod_id")].");\n";
   	}	
	exit();
	
}
//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_receive_master a, inv_transaction b","a.id=b.mst_id and a.recv_number=$txt_return_no and b.prod_id=$txt_prod_id and b.transaction_type=4"); 
		if($duplicate==1) 
		{
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			die;
		}
		//------------------------------Check Brand END---------------------------------------//
		
 		
		//adjust product master table START-------------------------------------//
 		$txt_return_qnty = str_replace("'","",$txt_return_qnty);
		$txt_return_value = str_replace("'","",$txt_return_value);
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value from product_details_master where id=$txt_prod_id");
		$presentStock=$presentStockValue=$presentAvgRate=$available_qnty=0;
 		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];	
		}
		
		$txt_return_value=$presentAvgRate*$txt_return_qnty;
		$nowStock 		= $presentStock+$txt_return_qnty;
		$nowStockValue 	= $presentStockValue+$txt_return_value;
		$field_array="last_purchased_qnty*current_stock*stock_value*updated_by*update_date";
		$data_array="".$txt_return_qnty."*".$nowStock."*".$nowStockValue."*'".$user_id."'*'".$pc_date_time."'";
		
		//$prodUpdate = sql_update("product_details_master",$field_array,$data_array,"id",$txt_prod_id,1);	
	  
		//adjust product master table END  -------------------------------------//
		$field_array1 = "id,mst_id,company_id,supplier_id,prod_id,item_category,transaction_type,transaction_date,store_id,cons_uom,cons_quantity,cons_rate,cons_amount,inserted_by,insert_date";
 		$data_array1= "(".$transactionID.",".$id.",".$cbo_company_name.",".$cbo_return_to.",".$txt_prod_id.",1,3,".$txt_return_date.",".$cbo_store_name.",".$cbo_uom.",".$txt_issue_qnty.",".$txt_rate.",".$issue_stock_value.",'".$user_id."','".$pc_date_time."')"; 
		
 		 
		//yarn master table entry here START---------------------------------------//	
	    
		if(str_replace("'","",$txt_return_no)=="")
		{
			if($db_type==2 || $db_type==1) { $year_id=" extract(year from insert_date)="; }
		    if($db_type==0)  { $year_id="YEAR(insert_date)="; }
			$id=return_next_id("id", "inv_receive_master", 1);		
			$new_recv_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GIR', date("Y",time()), 5, "select recv_number_prefix,recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='29' and $year_id".date('Y',time())." order by recv_number_prefix_num DESC ", "recv_number_prefix", "recv_number_prefix_num" )); 
			$field_array1="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category,receive_basis, company_id,receive_date,challan_no, store_id, location_id,batch_id, exchange_rate, currency_id,remarks, inserted_by,insert_date";
			$data_array1="(".$id.",'".$new_recv_number[1]."','".$new_recv_number[2]."','".$new_recv_number[0]."',29,".$cbo_item_category.",".$cbo_issue_basis.",".$cbo_company_name.",".$txt_return_date.",".$txt_return_challan_no.",".$cbo_store_name.",".$cbo_location.",".$txt_batch_id.",1,1,".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
			//echo $data_array;die;
			//$rID=sql_insert("inv_receive_master",$field_array,$data_array,1); 
		}
		else
		{
			$new_recv_number[0] = str_replace("'","",$txt_return_no);
			$id=return_field_value("id","inv_receive_master","recv_number=$txt_return_no");	
					
			$field_array1="entry_form*item_category*company_id*receive_date*challan_no*store_id*location_id*exchange_rate*currency_id*remarks*updated_by*update_date";
			$data_array1="29*".$cbo_item_category."*".$cbo_company_name."*".$txt_return_date."*".$txt_return_challan_no."*".$cbo_store_name."*".$cbo_location."*1*1*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
			
 			//$rID = sql_update("inv_receive_master",$field_array,$data_array,"id",$id,1);
		}
	
		//transaction table insert here START--------------------------------//
		if($txt_reject_qnty=="")$txt_reject_qnty=0;

		$transactionID = return_next_id("id", "inv_transaction", 1); 				
		$field_array2 = "id,mst_id,company_id,prod_id,item_category,transaction_type,transaction_date,store_id,cons_uom,cons_quantity,cons_reject_qnty,cons_rate,cons_amount,balance_qnty,balance_amount,issue_challan_no,remarks,inserted_by,insert_date";
 		$data_array2 = "(".$transactionID.",".$id.",".$cbo_company_name.",".$txt_prod_id.",".$cbo_item_category.",4,".$txt_return_date.",".$cbo_store_name.",".$cbo_uom.",".$txt_return_qnty.",".$txt_reject_qnty.",".$presentAvgRate.",".$txt_return_value.",".$txt_return_qnty.",".$txt_return_value.",".$txt_return_challan_no.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
		//$flag=1;
        $prodUpdate = sql_update("product_details_master",$field_array,$data_array,"id",$txt_prod_id,1);
	
		//if( $prodUpdate) $flag=1; else $flag=10;
		if(str_replace("'","",$txt_return_no)=="")
		{
			 $rID=sql_insert("inv_receive_master",$field_array1,$data_array1,1); 
		}
		else
		{
		    $rID = sql_update("inv_receive_master",$field_array1,$data_array1,"id",$id,1);	
		}
		//echo $rID;die;
		if( $rID) $flag=1; else $flag=20;
		$dtlsrID = sql_insert("inv_transaction",$field_array2,$data_array2,1);	
		if( $dtlsrID) $flag=1; else $flag=30;
 		//transaction table insert here END ---------------------------------//
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if( $prodUpdate && $rID && $dtlsrID)
				{
					mysql_query("COMMIT");  
					echo "0**".$new_recv_number[0]."**".$transactionID."**".$txt_prod_id;
				}
			else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".$new_recv_number[0];
				}
		}
		if($db_type==2 || $db_type==1 )
			{
				//echo $flag;
				if( $prodUpdate && $rID && $dtlsrID)
				{
					oci_commit($con);  
					echo "0**".$new_recv_number[0]."**".$transactionID."**".$txt_prod_id;
				}
			else
				{
					oci_rollback($con);
					echo "10**".$new_recv_number[0];
				}
			}
		disconnect($con);
		die;
	  
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
	
	
	        $flag=0;
			$sql = sql_select("select a.cons_quantity,a.cons_amount,b.current_stock,b.stock_value from inv_transaction a, product_details_master b where a.id=$update_id and a.prod_id=b.id");
			$beforeReturnQnty=$beforeReturnValue=0;
			$currentStockQnty=$currentStockValue=$before_available_qnty=0;
			foreach($sql as $result)
			{
				//current stock
				$currentStockQnty		=$result[csf("current_stock")];
				$currentStockValue		=$result[csf("stock_value")];
				//before return qnty
				$beforeReturnQnty		=$result[csf("cons_quantity")];
				$beforeReturnValue		=$result[csf("cons_amount")];
				
			}
			//adjust product master table START-------------------------------------//
			$txt_return_qnty = str_replace("'","",$txt_return_qnty);
			$txt_return_value=$txt_return_qnty*$txt_avrage_rate;
			$txt_prod_id = str_replace("'","",$txt_prod_id);
			$update_array="last_purchased_qnty*current_stock*stock_value*updated_by*update_date";
			$update_data = $updateID_array = array();
			
			if(str_replace("'","",$txt_prod_id) == str_replace("'","",$before_prod_id))
			{
				$presentStockQnty   = $currentStockQnty-$beforeReturnQnty+$txt_return_qnty; //current qnty - before qnty + present return qnty
				$presentStockValue  = $currentStockValue-$beforeReturnValue+$txt_return_value; 
				$data_array			= "".$txt_return_qnty."*".$presentStockQnty."*".$presentStockValue."*'".$user_id."'*'".$pc_date_time."'";
				//$prodUpdate = sql_update("product_details_master",$update_array,$data_array,"id",$txt_prod_id,0);
				//if($prodUpdate) $flag=1; else $flag=20;
			}
			else
			{
				//before
				$presentStockQnty   = $currentStockQnty-$beforeReturnQnty; //current qnty - before qnty
				$presentStockValue  = $currentStockValue-$txt_return_value; 
				$update_data[$before_prod_id]=explode("*",("".$txt_return_qnty."*".$presentStockQnty."*".$presentStockValue."*'".$user_id."'*'".$pc_date_time."'"));
				$updateID_array[]=$before_prod_id;
				//current
				$sql = sql_select("select current_stock,stock_value,avg_rate_per_unit from product_details_master  where id=$txt_prod_id");
				foreach($sql as $result)
				  {
					//current stock after product
					$currentStockQntyAfter		=$result[csf("current_stock")];
					$currentStockValueAfter		=$result[csf("stock_value")];
					$currentAvarageRateAfter	=$result[csf("avg_rate_per_unit")];
					
				  }
				$txt_return_value=$txt_return_qnty*$currentAvarageRateAfter;
				$presentStockQnty   = $currentStockQntyAfter+$txt_return_qnty; //current qnty + present return qnty
				$presentStockValue  = $currentStockValueAfter+$txt_return_value; 
	
				$update_data[$txt_prod_id]=explode("*",("".$txt_return_qnty."*".$presentStockQnty."*".$presentStockValue."*'".$user_id."'*'".$pc_date_time."'"));
				$updateID_array[]=$txt_prod_id;
				//echo bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array);die;
				//$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array),0);
				//if($prodUpdate) $flag=1; else $flag=10;
			}
		
			//adjust product master table END  -------------------------------------//
			$field_array="item_category*company_id*receive_basis*receive_date*challan_no*store_id*location_id*remarks*updated_by*update_date";
			$data_array="".$cbo_item_category."*".$cbo_company_name."*".$cbo_issue_basis."*".$txt_return_date."*".$txt_return_challan_no."*".$cbo_store_name."*".$cbo_location."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
			//$rID=sql_update("inv_receive_master",$field_array,$data_array,"recv_number",$txt_return_no,1);	
			//if($rID) $flag=1; else $flag=30;
			if($txt_reject_qnty=="")$txt_reject_qnty=0;
			$txt_amount=((str_replace("'","",$txt_return_qnty))*str_replace("'","",$txt_avrage_rate));
			$field_array1 = "company_id*prod_id*item_category*transaction_type*transaction_date*store_id*cons_uom*cons_quantity*cons_reject_qnty*cons_amount*balance_qnty*balance_amount*issue_challan_no*remarks*updated_by*update_date";
			$data_array1 = "".$cbo_company_name."*".$txt_prod_id."*".$cbo_item_category."*4*".$txt_return_date."*".$cbo_store_name."*".$cbo_uom."*".$txt_return_qnty."*".$txt_reject_qnty."*".$txt_amount."*".$txt_return_qnty."*".$txt_amount."*".$txt_return_challan_no."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
			
		if(str_replace("'","",$txt_prod_id) == str_replace("'","",$before_prod_id))
			{
			    $prodUpdate = sql_update("product_details_master",$update_array,$data_array,"id",$txt_prod_id,0);
				if($prodUpdate) $flag=1; else $flag=20;	
			}
		else
			{
				$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array),0);
				if($prodUpdate) $flag=1; else $flag=10;
			}
			$rID=sql_update("inv_receive_master",$field_array,$data_array,"recv_number",$txt_return_no,1);	
			if($rID) $flag=1; else $flag=30;
			$transID = sql_update("inv_transaction",$field_array1,$data_array1,"id",$update_id,1); 
		    if($transID) $flag=1; else $flag=40;
       if($db_type==0)
		  {
		
			if( $rID && $transID)
			   {
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_return_no);
				}
			else
			   {
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_return_no);
			   }
		  }
		if($db_type==2 || $db_type==1 )
		  {
			 
			 if($flag==1)
			  {
				  oci_commit($con);
			    echo "1**".str_replace("'","",$txt_return_no);
			  }
			  else
			    {
				 oci_rollback($con);
			     echo "10**".str_replace("'","",$txt_return_no);
			  }
		  }
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		echo 13;
	}		
}

if($action=="return_number_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{   
 		$("#hidden_return_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="780" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="200">Return No</th>
                    <th width="150">Date</th>
                    <th width="150">Challan No</th>
                    <th width="170">Location</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr> 
                
                    <td>
                       <input type="text" id="src_return_no" name="src_return_no" style="width:190px;" class="text_boxes" />
                    </td>
                    <td align="center">
                    	<input type="text" name="txt_return_date" id="txt_return_date" class="datepicker" style="width:145px;" placeholder="Select Date" />
                    </td>
                    <td align="center" >
                    	<input type="text" id="src_challan_no" name="src_challan_no" style="width:145px;" class="text_boxes" />				
                    </td>    
                    <td align="center">
						<?php 
                        	echo create_drop_down( "cbo_location", 140, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$company' order by location_name","id,location_name", 1, "-- Select Location --", $selected, "",0 ); 
                        ?>
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('src_return_no').value+'_'+document.getElementById('txt_return_date').value+'_'+document.getElementById('src_challan_no').value+'_'+document.getElementById('cbo_location').value+'_'+<?php echo $company; ?>, 'create_return_search_list_view', 'search_div', 'chemical_dyes_issue_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
            <input type="hidden" id="hidden_return_number" value="" />
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_return_search_list_view")
{
	$ex_data = explode("_",$data);
	$return_no = $ex_data[0];
	$return_date = $ex_data[1];
	$challan_no = $ex_data[2];
	$location = $ex_data[3];
	$company = $ex_data[4];
	
	
	if($return_no!="") $return_no =" and a.recv_number='$return_no'"; else $return_no="";
	if($return_date!="") $return_date =" and a.receive_date='$return_date'"; else $return_date="";
	if($challan_no!="") $challan_no =" and a.challan_no='$challan_no'"; else $challan_no="";
	if($location!=0) $location =" and a.location_id='$location'"; else $location="";
	if($company!="") $company =" and a.company_id='$company'"; else $company="";
	if($db_type==0){ $nulvalue="IFNULL";}
	if($db_type==1  || $db_type==2 ){ $nulvalue="nvl";}
	
	$sql = "select distinct a.id as mst_id,a.recv_number,a.receive_basis,a.company_id,a.receive_date,a.challan_no,a.location_id,$nulvalue(a.batch_id,0) as batch
			 from inv_receive_master a, inv_transaction b
			where a.entry_form=29 $company $location $challan_no $return_date $return_no "; 
 
	//$arr=array(3=>$location_arr);
   ?>
    
       <table class="rpt_table" border="0" cellpadding="2" cellspacing="0" style="width:800px" >
        	<thead>
            	<tr>
                	<th width="50">SL</th>
                    <th width="200">Return No</th>
                    <th width="150">Return Date</th>
                    <th width="200">Return Challan No</th>
                    <th width="200">Location</th>
                </tr>
            </thead>
        </table>
   <div style="width:800px; overflow-y:scroll; max-height:300px;font-size:12px; overflow-x:hidden; cursor:pointer;" id="scroll_body">
        <table class="rpt_table" border="0" cellpadding="0" cellspacing="0" style="width:800px">
            <tbody>
            
            	<?php 
				$result=sql_select($sql);
				$i=1;
				foreach($result as $row){					
					if($i%2==0)
						$bgcolor="#E9F3FF";
					else 
						$bgcolor="#FFFFFF";
					$rettotalQnty +=$row[csf("cons_quantity")];
 					$totalAmount +=$row[csf("cons_amount")];
 					
 				?>
                 	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='js_set_value("<?php echo $row[csf("recv_number")]."**".$row[csf("batch")]."**".$row[csf("tr_id")]."**".$row[csf("receive_basis")];?>","","")' style="cursor:pointer" >
                        <td width="50"><?php echo $i; ?></td>
                        <td width="200"><p><?php echo $row[csf("recv_number")]; ?></p></td>
                        <td width="150"><p><?php echo $row[csf("receive_date")]; ?></p></td>
                        <td width="200"><p><?php echo $row[csf("challan_no")]; ?></p></td>
                        <td width="200"><p><?php echo $location_arr[$row[csf("location_id")]]; ?></p></td>

                   </tr>
                <?php $i++; } ?>
              
            </tbody>
        </table>
   </div>
 <?php
}

//for update

if($action=="populate_batch_no_data_update")
{ 
     
	 $data=explode("**",$data);
	 $company_name=$data[0];
	 $receive_no=$data[1];
	 $batch_id=$data[2];
	 $tr_id=$data[3];
	 $receive_id=$data[4];
	?>
     <table width="260" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Item name</th>
                    <th width="100">Issue qty</th>
                </tr>
            </thead>
            <tbody>
            <?php
			$item_arr=return_library_array( "select id, product_name_details from product_details_master",'id','product_name_details');
			if($db_type==0) $group_cond="group by a.prod_id";
			if($db_type==2) $group_cond="group by a.id,a.prod_id";
			 $sql="select  a.id,a.prod_id,sum(a.cons_quantity) as issue_qty from  inv_transaction a, inv_issue_master b where b.batch_no=$batch_id and b.id=a.mst_id and a.transaction_type=2 and b.entry_form=5 and b.company_id=$company_name $group_cond";
			
			
				// $sql="select  a.id,a.prod_id from  inv_transaction a, inv_issue_master b where b.batch_no=$batch_id and b.id=a.mst_id and b.company_id =$company_name	 and a.transaction_type=2 and b.entry_form=5   group by a.id,a.prod_id"; 
		
			
			$item_result=sql_select($sql);
          	foreach($item_result as $row)
			{		
			
			    $totalIssued = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=2 group by prod_id","to_issue");
	            $totalIssuedReturn = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue_return","inv_receive_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=4 group by prod_id","to_issue_return");
	            $issue_remain=$totalIssued-$totalIssuedReturn;	
						
		        if($i%2==0) $bgcolor="#E9F3FF";
			    else   $bgcolor="#FFFFFF";
 				?>
                 	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("prod_id")]."_".$row[csf("id")];?>","child_form_item_data","requires/chemical_dyes_issue_return_controller")'  style="cursor:pointer" >
                        <td width="150"  align="center"><p><?php echo $item_arr[$row[csf("prod_id")]]; ?></p></td>
                        <td width="100"  align="center"><p><?php echo $issue_remain; ?></p></td>
                   </tr>
              <?php $i++; 
			} ?>
              
            </tbody>
        </table> 

   <?php	
}



if($action=="populate_master_from_data")
{  
 	$sql = "select b.id,b.recv_number,b.company_id,b.receive_date,b.challan_no,b.receive_basis,b.location_id,b.batch_id ,a.id as tr_id	
			from  inv_transaction a, inv_receive_master b
			where b.recv_number='$data' and b.id=a.mst_id and a.transaction_type=4 and b.entry_form=29";
	//echo $sql;
	
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#txt_return_no').val('".$row[csf("recv_number")]."');\n";
 		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		echo "$('#cbo_location').val(".$row[csf("location_id")].");\n";
		echo "$('#txt_return_date').val('".change_date_format($row[csf("receive_date")])."');\n";
		echo "$('#txt_return_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_issue_basis').val('".$row[csf("receive_basis")]."');\n";
		echo "$('#txt_batch_name').val('".$batch_arr[$row[csf("batch_id")]]."');\n";
		echo "$('#update_id').val(".$row[csf("tr_id")].");\n";
		echo "disable_enable_fields( 'cbo_company_name*cbo_issue_basis', 1, '', '' );\n"; // disable true
				
   	}	
	exit();	
}

if($action=="show_dtls_list_view")	
{
	$return_number=str_replace("'","",$data);

	$cond="";
	if($return_number!="") $cond .= " and a.recv_number='$return_number'";

	$sql = "select b.id as tr_id,a.location_id,a.recv_number,a.company_id,a.receive_date,a.store_id,a.receive_basis,b.id,b.cons_quantity,b.cons_reject_qnty,b.cons_uom,b.cons_rate,            b.cons_amount,c.product_name_details,c.id as prod_id   
	       from  inv_receive_master a, inv_transaction b left join product_details_master c on b.prod_id=c.id
	       where a.id=b.mst_id  and b.transaction_type=4  and a.entry_form=29 $cond";	
	   	
	$result = sql_select($sql);
	$i=1;
	$rettotalQnty=0;
	$rcvtotalQnty=0;
	$totalAmount=0;
	?> 
     	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:850px" >
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Store Name</th>
                    <th>Basis</th>
                    <th>Item Description</th>
                    <th>Location</th>
                    <th>Return Qnty</th>
                    <th>Reject Qnty</th>
                    <th>UOM</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
				foreach($result as $row){					
					if($i%2==0)
						$bgcolor="#E9F3FF";
					else 
						$bgcolor="#FFFFFF";
 					
					$rettotalQnty +=$row[csf("cons_quantity")];
 					$totalAmount +=$row[csf("cons_amount")];
 					
 				?>
                 	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("recv_number")]."**".$row[csf("prod_id")]."**".$row[csf("tr_id")];?>","child_form_input_data_update","requires/chemical_dyes_issue_return_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $store_arr[$row[csf("store_id")]]; ?></p></td>
                        <td width="100"><p><?php echo $receive_basis_arr[$row[csf("receive_basis")]]; ?></p></td>
                        <td width="200"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td width="80"><p><?php echo $location_arr[$row[csf("location_id")]]; ?></p></td>
                        <td width="80" align="center"><p><?php echo $row[csf("cons_quantity")]; ?></p></td>
                        <td width="80" align="center"><p><?php echo $row[csf("cons_reject_qnty")]; ?></p></td>
                        <td width="80" align="center"><p><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
              
            </tbody>
        </table>
    <?php
	exit();
}

//for update 
if($action=="child_form_input_data_update")
{ 
    $data=explode('**',$data);
	
	
  	$sql = "select b.id as prod_id, b.product_name_details, b.lot, a.id as tr_id, a.store_id, a.cons_uom, a.cons_rate, a.cons_quantity,a.cons_reject_qnty, a.cons_amount,    a.issue_challan_no,a.remarks,a.item_category,a.machine_id,b.item_group_id,c.receive_basis,c.location_id,c.receive_date,c.receive_basis,c.challan_no,c.company_id,c.batch_id
			from inv_transaction a, product_details_master b, inv_receive_master c
 			where c.recv_number='".$data[0]."' and b.id=".$data[1]." and a.status_active=1 and a.id=".$data[2]."  and c.id=a.mst_id and transaction_type=4 and c.entry_form=29 and a.prod_id=b.id and b.status_active=1";
   //echo $sql;
	$result = sql_select($sql);
	foreach($result as $row)
	   {   
		
			echo "$('#txt_item_description').val('".$row[csf("product_name_details")]."');\n";
			echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
			echo "$('#txt_return_qnty').val('".$row[csf("cons_quantity")]."');\n"; 
			echo "$('#txt_reject_qnty').val('".$row[csf("cons_reject_qnty")]."');\n";
			echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
			echo "$('#cbo_uom').val('".$row[csf("cons_uom")]."');\n";
			echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
			echo "$('#before_prod_id').val('".$row[csf("prod_id")]."');\n";
			echo "$('#cbo_item_category').val(".$row[csf("item_category")].");\n";
			echo "$('#cbo_item_group').val(".$row[csf("item_group_id")].");\n";
			echo "$('#update_id').val(".$row[csf("tr_id")].");\n";
			
    	}
 	
	echo "set_button_status(1, permission, 'fnc_chemical_issue_return_entry',1);\n";		
  	exit();
}






if($action=="child_form_input_data")
{ 
    $data=explode('**',$data);
	
	
  	$sql = "select b.id as prod_id, b.product_name_details, b.lot, a.id as tr_id, a.store_id, a.cons_uom, a.cons_rate, a.cons_quantity,a.cons_reject_qnty, a.cons_amount,    a.issue_challan_no,a.remarks,a.item_category,a.machine_id,b.item_group_id,c.receive_basis,c.location_id,c.receive_date,c.receive_basis,c.challan_no,c.company_id,c.batch_id
			from inv_transaction a, product_details_master b, inv_receive_master c
 			where c.recv_number='".$data[0]."' and b.id=".$data[1]." and a.status_active=1 and a.id=".$data[3]."  and c.id=a.mst_id and transaction_type=4 and c.entry_form=29 and a.prod_id=b.id and b.status_active=1";
   //echo $sql;
	$result = sql_select($sql);
	foreach($result as $row)
	   {   
		
			echo "$('#txt_item_description').val('".$row[csf("product_name_details")]."');\n";
			echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
			echo "$('#txt_return_qnty').val('".$row[csf("cons_quantity")]."');\n"; 
			echo "$('#txt_reject_qnty').val('".$row[csf("cons_reject_qnty")]."');\n";
			echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
			echo "$('#cbo_uom').val('".$row[csf("cons_uom")]."');\n";
			echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
			echo "$('#before_prod_id').val('".$row[csf("prod_id")]."');\n";
			echo "$('#cbo_item_category').val(".$row[csf("item_category")].");\n";
			echo "$('#cbo_item_group').val(".$row[csf("item_group_id")].");\n";
			
    	}
 	
	echo "set_button_status(1, permission, 'fnc_chemical_issue_return_entry',1);\n";		
  	exit();
}



?>