<?php
error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
//--------------------------------------------------------------------------------------------------------------------
$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$costing_library=return_library_array( "select job_no, costing_per from wo_pre_cost_mst", "job_no", "costing_per"  );

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );     	 
	exit();
}

if($action=="order_no_search_popup")
{
	echo load_html_head_contents("Order No Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_order_id').val( id );
			$('#hide_order_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:780px;">
            <table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Order No</th>
                    <th>Shipment Date</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_order_no" id="hide_order_no" value="" />
                    <input type="hidden" name="hide_order_id" id="hide_order_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",0,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Order No",2=>"Style Ref",3=>"Job No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 110, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
                            <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
                        </td>	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value, 'create_order_no_search_list_view', 'search_div', 'process_loss_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    	</td>
                    </tr>
                    <tr>
                        <td colspan="5" height="20" valign="middle"><?php echo load_month_buttons(1); ?></td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_order_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];

	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==1) 
		$search_field="b.po_number"; 
	else if($search_by==2) 
		$search_field="a.style_ref_no"; 	
	else 
		$search_field="a.job_no";
		
	$start_date =$data[4];
	$end_date =$data[5];	
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format(trim($start_date),"yyyy-mm-dd")."' and '".change_date_format(trim($end_date),"yyyy-mm-dd")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format(trim($start_date),'','',1)."' and '".change_date_format(trim($end_date),'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	
	$company_short_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$arr=array (0=>$company_short_arr,1=>$buyer_arr);
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
	else $year_field="";//defined Later
	
	$sql= "select b.id, a.job_no, $year_field, a.job_no_prefix_num, a.company_name, a.buyer_name, a.style_ref_no, b.po_number, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $date_cond order by b.id, b.pub_shipment_date";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No, Po No, Shipment Date", "70,70,50,70,150,180","760","220",0, $sql , "js_set_value", "id,po_number","",1,"company_name,buyer_name,0,0,0,0,0",$arr,"company_name,buyer_name,year,job_no_prefix_num,style_ref_no,po_number,pub_shipment_date","",'','0,0,0,0,0,0,3','',1) ;
   exit(); 
}

$tmplte=explode("**",$data);

if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$company_name=str_replace("'","",$cbo_company_name);
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	
	$txt_job_no=str_replace("'","",$txt_job_no);
	if(trim($txt_job_no)!="") $job_no=trim($txt_job_no); else $job_no="%%";
	
	$cbo_year=str_replace("'","",$cbo_year);
	if(trim($cbo_year)!=0) 
	{
		if($db_type==0) $year_cond=" and YEAR(a.insert_date)=$cbo_year";
		else if($db_type==2) $year_cond=" and to_char(a.insert_date,'YYYY')=$cbo_year";
		else $year_cond="";
	}
	else $year_cond="";
	
	$date_cond='';
	if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
	{
		$date_cond=" and b.pub_shipment_date between $txt_date_from and $txt_date_to";
	}
	
	if(str_replace("'","",trim($txt_order_no))=="")
	{
		$po_id_cond="";
	}
	else
	{
		if(str_replace("'","",$hide_order_id)!="")
		{
			$po_id=str_replace("'","",$hide_order_id);
		}
		else
		{
			$po_number=trim(str_replace("'","",$txt_order_no))."%";
			if($db_type==0)
			{
				$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");
			}
			else
			{
				$po_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");
			}	
			if($po_id=="") $po_id=0;
		}
		
		$po_id_cond="and b.id in(".$po_id.")";
	}
	//echo $po_id_cond;
	
	$shipping_status_cond='';
	if(str_replace("'","",$shipping_status)!=0)
	{
		$shipping_status_cond=" and b.shiping_status=$shipping_status";
	}

	if($template==1)
	{
		$booking_arr=array();
		/*if($db_type==0)
		{
			$booking_arr=return_library_array( "select po_break_down_id, group_concat(distinct(booking_no)) as booking_no from wo_booking_dtls where status_active=1 and is_deleted=0 group by po_break_down_id", "po_break_down_id", "booking_no");
		}
		else
		{
			$booking_arr=return_library_array( "select po_break_down_id, LISTAGG(cast(booking_no as varchar2(4000)), ',') WITHIN GROUP (ORDER BY id) as booking_no from wo_booking_dtls where status_active=1 and is_deleted=0 group by po_break_down_id", "po_break_down_id", "booking_no");
		}*/
		$sql_wo="select b.po_break_down_id, a.id, a.booking_no_prefix_num as booking_no, a.insert_date from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category in(2,13) and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.po_break_down_id, a.id, a.booking_no_prefix_num, a.insert_date order by a.insert_date";
		$resultWo=sql_select($sql_wo);
		foreach($resultWo as $woRow)
		{
			$year=date('Y', strtotime($woRow[csf('insert_date')]));
			$booking_arr[$woRow[csf('po_break_down_id')]].=$woRow[csf('booking_no')]." (".$year."),";
		}
		
		$yarn_used_arr=array(); $grey_produced_arr=array(); $grey_used_arr=array(); $fin_produced_arr=array(); $fin_used_arr=array();
		$dataArrayTrans=sql_select("select po_breakdown_id, 
								sum(CASE WHEN entry_form ='3' and issue_purpose!=2 THEN quantity ELSE 0 END) AS yarn_issue_qnty,
								sum(CASE WHEN entry_form ='9' and issue_purpose!=2 THEN quantity ELSE 0 END) AS yarn_issue_return_qnty,
								sum(CASE WHEN entry_form ='2' THEN quantity ELSE 0 END) AS grey_production,
								sum(CASE WHEN entry_form ='16' THEN quantity ELSE 0 END) AS grey_issue,
								sum(CASE WHEN entry_form ='7' THEN quantity ELSE 0 END) AS finish_prodcution,
								sum(CASE WHEN entry_form ='18' THEN quantity ELSE 0 END) AS finish_issue,
								sum(CASE WHEN entry_form ='11' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_yarn,
								sum(CASE WHEN entry_form ='11' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_yarn,
								sum(CASE WHEN entry_form ='13' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_knit,
								sum(CASE WHEN entry_form ='13' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_knit,
								sum(CASE WHEN entry_form ='15' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_fin,
								sum(CASE WHEN entry_form ='15' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_fin
								from order_wise_pro_details where status_active=1 and is_deleted=0 and entry_form in(2,3,7,9,11,13,15,16,18) group by po_breakdown_id");
		foreach($dataArrayTrans as $row)
		{
			$yarn_used_arr[$row[csf('po_breakdown_id')]]=$row[csf('yarn_issue_qnty')]-$row[csf('yarn_issue_return_qnty')]+$row[csf('transfer_in_qnty_yarn')]-$row[csf('transfer_out_qnty_yarn')];
			$grey_produced_arr[$row[csf('po_breakdown_id')]]=$row[csf('grey_production')];
			$grey_used_arr[$row[csf('po_breakdown_id')]]=$row[csf('grey_issue')]+$row[csf('transfer_in_qnty_knit')]-$row[csf('transfer_out_qnty_knit')];
			$fin_produced_arr[$row[csf('po_breakdown_id')]]=$row[csf('finish_prodcution')];
			$fin_used_arr[$row[csf('po_breakdown_id')]]=$row[csf('finish_issue')]+$row[csf('transfer_in_qnty_fin')]-$row[csf('transfer_out_qnty_fin')];
		}
		
		$fin_fab_cons_arr=return_library_array( "select job_no, fab_knit_fin_req_kg from wo_pre_cost_sum_dtls", "job_no", "fab_knit_fin_req_kg");
		
		$prod_sql= "SELECT po_break_down_id,
					sum(CASE WHEN production_type ='1' THEN production_quantity END) AS cutting,
					sum(CASE WHEN production_type ='8' THEN production_quantity END) AS finishcompleted
					from pro_garments_production_mst
					where status_active=1 and is_deleted=0 group by po_break_down_id";
				
		$prodSQLresult = sql_select($prod_sql);	
		$prodArr = array();
		foreach($prodSQLresult as $row)
		{
			$prodArr[$row[csf('po_break_down_id')]]['cutting']=$row[csf('cutting')];
			$prodArr[$row[csf('po_break_down_id')]]['finishcompleted']=$row[csf('finishcompleted')];
		}
		
		$ex_factory_arr=return_library_array( "select po_break_down_id, sum(ex_factory_qnty) as qnty from pro_ex_factory_mst where status_active=1 and is_deleted=0 group by po_break_down_id", "po_break_down_id", "qnty");	
		
		if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
		else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
		else $year_field="";//defined Later
		$sql="select a.job_no_prefix_num, a.job_no, $year_field, a.company_name, a.buyer_name, a.style_ref_no, a.order_uom, a.gmts_item_id, a.total_set_qnty as ratio, b.id, b.po_number, b.pub_shipment_date, b.po_quantity, b.unit_price, b.po_total_price, b.shiping_status from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_name' and a.job_no_prefix_num like '$job_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $date_cond $buyer_id_cond $po_id_cond $shipping_status_cond $year_cond order by b.id, b.pub_shipment_date";
		//echo $sql; die;
		$result=sql_select($sql);
		$i=1; $tot_yarn_used=0; $tot_grey_used=0; $tot_grey_prod=0; $tot_fin_used=0; $tot_fin_prod=0; $tot_gmts_left_over=0; $tot_grey_left_over=0; $tot_fin_left_over=0;
		$tot_effective_fab_uses=0; $tot_opportunity_lost=0;
		ob_start();
		foreach( $result as $row )
		{
			if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			$gmts_item='';
			$gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
			foreach($gmts_item_id as $item_id)
			{
				if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
			}
			
			$order_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
			$unit_price=$row[csf('unit_price')]/$row[csf('ratio')];
			$booking_no=explode(",",substr($booking_arr[$row[csf('id')]],0,-1));
			$knit_proc_loss=$yarn_used_arr[$row[csf('id')]]-$grey_produced_arr[$row[csf('id')]];
			$knit_proc_loss_perc=($knit_proc_loss/$yarn_used_arr[$row[csf('id')]])*100;
			$fab_proc_loss=$yarn_used_arr[$row[csf('id')]]-$fin_produced_arr[$row[csf('id')]];
			$fab_proc_loss_perc=($fab_proc_loss/$yarn_used_arr[$row[csf('id')]])*100;
			$dye_fin_proc_loss=$grey_used_arr[$row[csf('id')]]-$fin_produced_arr[$row[csf('id')]];
			$dye_fin_proc_loss_perc=($dye_fin_proc_loss/$grey_used_arr[$row[csf('id')]])*100;
			
			$dzn_qnty=0;
			$costing_per_id=$costing_library[$row[csf('job_no')]];
			if($costing_per_id==1)
			{
				$dzn_qnty=12;
			}
			else if($costing_per_id==3)
			{
				$dzn_qnty=12*2;
			}
			else if($costing_per_id==4)
			{
				$dzn_qnty=12*3;
			}
			else if($costing_per_id==5)
			{
				$dzn_qnty=12*4;
			}
			else
			{
				$dzn_qnty=1;
			}
			
			$dzn_qnty=$dzn_qnty*$row[csf('ratio')];
			$fin_fab_cons=$fin_fab_cons_arr[$row[csf('job_no')]];
			$possible_cut=($dzn_qnty/$fin_fab_cons)*$fin_used_arr[$row[csf('id')]];
			$actual_cut=$prodArr[$row[csf('id')]]['cutting'];
			$finishcompleted=$prodArr[$row[csf('id')]]['finishcompleted'];
			$ex_factory_qnty=$ex_factory_arr[$row[csf('id')]];
			$effective_fab_uses=($fin_fab_cons/$dzn_qnty)*$finishcompleted;
			$gmts_proc_loss=$fin_used_arr[$row[csf('id')]]-$effective_fab_uses;
			$gmts_proc_loss_perc=($gmts_proc_loss/$fin_used_arr[$row[csf('id')]])*100;
			$tot_fab_loss=$knit_proc_loss+$dye_fin_proc_loss+$gmts_proc_loss;
			$tot_fab_loss_perc=$fab_proc_loss_perc+$gmts_proc_loss_perc;
			$gmts_left_over=$finishcompleted-$ex_factory_qnty;
			$grey_left_over=$grey_produced_arr[$row[csf('id')]]-$grey_used_arr[$row[csf('id')]];
			$fin_left_over=$fin_produced_arr[$row[csf('id')]]-$fin_used_arr[$row[csf('id')]];
			$avg_process_loss=($dye_fin_proc_loss_perc*$grey_left_over)/100;
			$opportunity_lost=(floor((($grey_left_over-$avg_process_loss+$fin_left_over)/$fin_fab_cons)*12)+$gmts_left_over)*$unit_price;
			
			$tot_yarn_used+=$yarn_used_arr[$row[csf('id')]]; 
			$tot_grey_used+=$grey_used_arr[$row[csf('id')]]; 
			$tot_grey_prod+=$grey_produced_arr[$row[csf('id')]]; 
			$tot_fin_used+=$fin_used_arr[$row[csf('id')]]; 
			$tot_fin_prod+=$fin_produced_arr[$row[csf('id')]]; 
			$tot_effective_fab_uses+=$effective_fab_uses; 
			$tot_gmts_left_over+=$gmts_left_over; 
			$tot_grey_left_over+=$grey_left_over; 
			$tot_fin_left_over+=$fin_left_over; 
			$tot_opportunity_lost+=$opportunity_lost;
			$color="";	
			if($possible_cut > $actual_cut)
			{
				$color="red";	
			}
			
			$html.="<tr bgcolor='$bgcolor' onclick=change_color('tr_$i','$bgcolor') id=tr_$i>";
			$html.='<td width="40">'.$i.'</td>
					<td width="98" style="padding-left:2px"><p>'.implode(",<br>",array_unique($booking_no)).'&nbsp;</p></td>
					<td width="70"><p>'.$row[csf('job_no_prefix_num')].'</p></td>
					<td width="50" align="center"><p>'.$row[csf('year')].'</p></td>
					<td width="70"><p>'.$buyer_arr[$row[csf('buyer_name')]].'</p></td>
					<td width="110"><p>'.$row[csf('style_ref_no')].'</p></td>
					<td width="130"><p>'.$row[csf('po_number')].'</p></td>
					<td width="130"><p>'.$gmts_item.'</p></td>
					<td width="50" align="center">'.$unit_of_measurement[$row[csf('order_uom')]].'&nbsp;</td>
					<td width="88" style="padding-right:2px" align="right">'.$order_qnty_in_pcs.'</td>
					<td width="80" align="center">'.change_date_format($row[csf('pub_shipment_date')]).'&nbsp;</td>
					<td width="98" align="right" style="padding-right:2px">'.number_format($yarn_used_arr[$row[csf('id')]],2,'.','').'</td>
					<td width="98" align="right" style="padding-right:2px">'.number_format($grey_produced_arr[$row[csf('id')]],2,'.','').'</td>
					<td width="78" align="right" style="padding-right:2px">'.number_format($knit_proc_loss_perc,2,'.','').'</td>
					<td width="98" align="right" style="padding-right:2px">'.number_format($grey_used_arr[$row[csf('id')]],2,'.','').'</td>
					<td width="98" align="right" style="padding-right:2px">'.number_format($fin_produced_arr[$row[csf('id')]],2,'.','').'</td>
					<td width="80" align="right">'.number_format($dye_fin_proc_loss_perc,2,'.','').'</td>
					<td width="80" align="right">'.number_format($fab_proc_loss_perc,2,'.','').'</td>
					<td width="98" align="right" style="padding-right:2px">'.number_format($fin_used_arr[$row[csf('id')]],2,'.','').'</td>
					<td width="100" align="right">'.number_format($possible_cut,0,'.','').'</td>
					<td width="100" align="right" bgcolor="'.$color.'">'.number_format($actual_cut,0,'.','').'</p></td>
					<td width="100" align="right">'.number_format($finishcompleted,0,'.','').'</td>
					<td width="100" align="right">'.number_format($effective_fab_uses,2,'.','').'</td>
					<td width="80" align="right">'.number_format($gmts_proc_loss_perc,2,'.','').'</td>
					<td width="100" align="right">'.number_format($tot_fab_loss,2,'.','').'</td>
					<td width="80" align="right">'.number_format($tot_fab_loss_perc,2,'.','').'</td>
					<td width="100" align="right">'.number_format($ex_factory_qnty,0,'.','').'</td>
					<td width="100" align="right">'.number_format($gmts_left_over,0,'.','').'</td>
					<td width="100" align="right">'.number_format($grey_left_over,2,'.','').'</td>
					<td width="100" align="right">'.number_format($fin_left_over,2,'.','').'</td>
					<td width="100" align="right">'.number_format($opportunity_lost,2,'.','').'</td>
					<td><p>'.$shipment_status[$row[csf('shiping_status')]].'</p></td>
				</tr>';
        	$i++;
		}
		
		$tot_knit_proc_loss=$tot_yarn_used-$tot_grey_used;
		$tot_knit_proc_loss_perc=($tot_knit_proc_loss/$tot_yarn_used)*100;
		$tot_dye_fin_proc_loss=$tot_grey_used-$tot_fin_prod;
		$tot_dye_fin_proc_loss_perc=($tot_dye_fin_proc_loss/$tot_grey_used)*100;
		$tot_fab_proc_loss=$tot_yarn_used-$tot_fin_prod;
		$tot_fab_proc_loss_perc=($tot_fab_proc_loss/$tot_yarn_used)*100;
		$tot_gmts_proc_loss=$tot_fin_used-$tot_effective_fab_uses;
		$tot_gmts_proc_loss_perc=($tot_gmts_proc_loss/$tot_fin_used)*100;
		$grand_tot_fab_loss=$tot_knit_proc_loss+$tot_dye_fin_proc_loss+$tot_gmts_proc_loss;
		$grand_fab_loss_perc=$tot_fab_proc_loss_perc+$tot_gmts_proc_loss_perc;
		
		$gmts_prod_in_kg=$tot_fin_used-$tot_effective_fab_uses;
		
		?>
		<div style="width:3000px">
			<fieldset style="width:100%;">	
				<table width="2080">
					<tr class="form_caption">
						<td colspan="20" align="center">Process Loss Report</td>
					</tr>
					<tr class="form_caption">
						<td colspan="20" align="center"><?php echo $company_arr[$company_name]; ?></td>
					</tr>
				</table>
                <b><u>Summary</u></b>
                <table id="table_header" class="rpt_table" width="530" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <thead>
                        <th width="40">SL</th>
                        <th width="120">Particulars</th>
                        <th width="140">Fabric Loss Qty. (Kg)</th>
                        <th width="100">Loss %</th>
                    	<th>Left Over</th>
                    </thead>
                    <tbody>
                        <tr bgcolor="<?php echo "#FFFFFF"; ?>">
                        	<td>1</td>
                       		<td>Knitting</td>
                           	<td align="right"><?php echo number_format($tot_knit_proc_loss,2); ?></td>
                           	<td align="right"><?php echo number_format($tot_knit_proc_loss_perc,2); ?></td>
                           	<td align="right"><?php echo number_format($grey_left_over,2)." (Kg)"; ?></td>
                        </tr>
                        <tr bgcolor="<?php echo "#E9F3FF"; ?>">
                        	<td>2</td>
                           	<td>Dyeing + Finishing</td>
                           	<td align="right"><?php echo number_format($tot_dye_fin_proc_loss,2); ?></td>
                           	<td align="right"><?php echo number_format($tot_dye_fin_proc_loss_perc,2); ?></td>
                           	<td align="right"><?php echo number_format($tot_fin_left_over,2)." (Kg)"; ?></td>
                        </tr>
                        <tr bgcolor="<?php echo "#FFFFFF"; ?>">
                        	<td>3</td>
                           	<td>Gmts. Production</td>
                           	<td align="right"><?php echo number_format($gmts_prod_in_kg,2); ?></td>
                           	<td align="right"><?php echo number_format($tot_gmts_proc_loss_perc,2); ?></td>
                           	<td align="right"><?php echo number_format($tot_gmts_left_over,0)." (Pcs)"; ?></td>
                        </tr>
                	</tbody>  
                   	<tfoot>
                    	<th width="40"></th>
                        <th width="120" align="right"></th>
                        <th width="110" align="right"><?php echo number_format($tot_knit_proc_loss+$tot_dye_fin_proc_loss+$gmts_prod_in_kg,2); ?></th>
                        <th width="100" align="right"><?php echo number_format($tot_knit_proc_loss_perc+$tot_dye_fin_proc_loss_perc+$tot_gmts_proc_loss_perc,2); ?></th>
                    	<th>&nbsp;</th>
                    </tfoot>      
                </table>
                <br>
                <b>Total Opportunity Loss (USD): <?php echo number_format($tot_opportunity_lost,2); ?></b>
				<table style="margin-top:10px" id="table_header_1" class="rpt_table" width="2980" cellpadding="0" cellspacing="0" border="1" rules="all">
					<thead>
						<th width="40">SL</th>
						<th width="100">Fabric Booking No</th>
						<th width="70">Job No</th>
						<th width="50">Year</th>
						<th width="70">Buyer</th>
						<th width="110">Style Ref.</th>
						<th width="130">Order No</th>
						<th width="130">Garments Item</th>
						<th width="50">UOM</th>
						<th width="90">Qnty (Pcs)</th>
						<th width="80">Shipment Date</th>
						<th width="100">Yarn Used</th>
						<th width="100">Grey Produced</th>
						<th width="80">Knit Pros. Loss %</th>
						<th width="100">Grey Used</th>
						<th width="100">Finish Fabric Produced</th>
						<th width="80">Dye/Fin Pros. Loss %</th>
						<th width="80">Fab. Pros. Loss%</th>
						<th width="100">Fab. Issue to Cut</th>
						<th width="100">Possible Cut Pcs</th>
						<th width="100">Actual Cut Pcs</th>
						<th width="100">Fin GMT- Pcs</th>
						<th width="100">Effective Fab Uses</th>
						<th width="80">Gmts Pros. Loss %</th>
						<th width="100">Total Pros. Loss (Kg)</th>
						<th width="80">Total Pros. Loss %</th>
						<th width="100">Ex-Fact Qty.</th>
						<th width="100">GMT Left Over</th>
						<th width="100">Grey Left Over</th>
						<th width="100">Fin Fab Left Over</th>
						<th width="100">Opportunity Loss (USD)</th>
						<th>Shipment Status</th>
					</thead>
				</table>
				<div style="width:3002px; max-height:400px; overflow-y:scroll" id="scroll_body">
					<table class="rpt_table" width="2980" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
						<?php 
							echo $html;
						?>
					</table>
					<table class="rpt_table" width="2980" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
						<tfoot>
							<th width="40"></th>
							<th width="100"></th>
							<th width="70"></th>
							<th width="50"></th>
							<th width="70"></th>
							<th width="110"></th>
							<th width="130"></th>
							<th width="130"></th>
							<th width="50"></th>
							<th width="90" id="total_order_qnty_in_pcs" align="right"></th>
							<th width="80"></th>
							<th width="100" id="value_yarn_used" align="right"></th>
							<th width="100" id="value_grey_produced" align="right"></th>
							<th width="80" align="right"><?php echo number_format($tot_knit_proc_loss_perc,2); ?></th>
							<th width="100" id="value_grey_used" align="right"></th>
							<th width="100" id="value_fin_produced" align="right"></th>
							<th width="80" align="right"><?php echo number_format($tot_dye_fin_proc_loss_perc,2); ?></th>
							<th width="80" align="right"><?php echo number_format($tot_fab_proc_loss_perc,2); ?></th>
							<th width="100" id="value_fin_used" align="right"></th>
							<th width="100" id="possible_cut_pcs" align="right"></th>
							<th width="100" id="actual_cut_pcs" align="right"></th>
							<th width="100" id="fin_gmts_pcs" align="right"></th>
							<th width="100" id="value_effec_fab_uses" align="right"></th>
							<th width="80" align="right"><?php echo number_format($tot_gmts_proc_loss_perc,2); ?></th>
							<th width="100" id="value_tot_fab_loss" align="right"></th>
							<th width="80" align="right"><?php echo number_format($grand_fab_loss_perc,2); ?></th>
							<th width="100" id="ex_factory" align="right"></th>
							<th width="100" id="gmts_left_over" align="right"></th>
							<th width="100" id="value_grey_left_over" align="right"></th>
							<th width="100" id="value_fin_left_over" align="right"></th>
							<th width="100" id="value_opportunity_loss" align="right"></th>
							<th></th>
						</tfoot>
					</table>
				</div>
			</fieldset>
		</div>
<?php
	}
	
	echo "$total_data****requires/$filename";
	exit();	
}

if($action=="trims_cost")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
?>
    <div>
        <fieldset style="width:600px;">
        <div style="width:600px" align="center">	
            <table class="rpt_table" width="470" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="120">Job No</th>
                    <th width="200">Order No</th>
                    <th>Order Qnty</th>
                </thead>
                <tr bgcolor="#FFFFFF">
                	<td align="center"><?php echo $job_no; ?></td>
                    <td><?php echo $po_no; ?></td>
                    <td align="right"><?php echo number_format($po_qnty,0); ?></td>
                </tr>
            </table>
            <table style="margin-top:10px" class="rpt_table" width="600" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                	<th width="40">SL</th>
                    <th width="130">Item Name</th>
                    <th width="90">Cons/Dzn</th>
                    <th width="80">Rate</th>
                    <th width="110">Trims Cost/Dzn</th>
                    <th>Total Trims Cost</th>
                </thead>
            </table>
            </div>
            <div style="width:620px; max-height:250px; overflow-y:scroll" id="scroll_body">
                <table class="rpt_table" width="600" cellpadding="0" cellspacing="0" border="1" rules="all">
                	<?php
					$costing_per=return_field_value("costing_per","wo_pre_cost_mst","job_no='$job_no' and status_active=1 and is_deleted=0");
                        
					$dzn_qnty=0;
					if($costing_per==1)
					{
						$dzn_qnty=12;
					}
					else if($costing_per==3)
					{
						$dzn_qnty=12*2;
					}
					else if($costing_per==4)
					{
						$dzn_qnty=12*3;
					}
					else if($costing_per==5)
					{
						$dzn_qnty=12*4;
					}
					else
					{
						$dzn_qnty=1;
					}
					
					$sql="select a.trim_group, a.rate, b.cons from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b where a.id=b.wo_pre_cost_trim_cost_dtls_id and b.po_break_down_id='$po_id' and a.job_no='$job_no' and a.status_active=1 and a.is_deleted=0";
					$trimsArray=sql_select($sql);
					$i=1;
					foreach($trimsArray as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="40"><?php echo $i; ?></td>
							<td width="130"><div style="width:130px; word-wrap:break-word"><?php echo $item_library[$row[csf('trim_group')]]; ?></div></td>
							<td width="90" align="right"><?php echo number_format($row[csf('cons')],2); ?></td>
							<td width="80" align="right"><?php echo number_format($row[csf('rate')],2); ?></td>
							<td width="110" align="right">
								<?php
                                    $trims_cost_per_dzn=$row[csf('cons')]*$row[csf('rate')]; 
                                    echo number_format($trims_cost_per_dzn,2);
									$tot_trims_cost_per_dzn+=$trims_cost_per_dzn; 
                                ?>
                            </td>
							<td align="right">
								<?php
                                	$trims_cost=($po_qnty/$dzn_qnty)*$trims_cost_per_dzn;
									echo number_format($trims_cost,2);
									$tot_trims_cost+=$trims_cost;
                                ?>
                            </td>
						</tr>
					<?php
					$i++;
					}
					?>
                	<tfoot>
                        <th colspan="4">Total</th>
                        <th><?php echo number_format($tot_trims_cost_per_dzn,2); ?></th>
                        <th><?php echo number_format($tot_trims_cost,2); ?></th>
                    </tfoot>    
                </table>
            </div>
        </fieldset>
    </div>
<?php
}

if($action=="other_cost")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
?>
    <div align="center">
        <fieldset style="width:600px;">
            <table class="rpt_table" width="470" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="120">Job No</th>
                    <th width="200">Order No</th>
                    <th>Order Qnty</th>
                </thead>
                <tr bgcolor="#FFFFFF">
                	<td align="center"><?php echo $job_no; ?></td>
                    <td><?php echo $po_no; ?></td>
                    <td align="right"><?php echo number_format($po_qnty,0); ?></td>
                </tr>
            </table>
            <table style="margin-top:10px" class="rpt_table" width="470" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                    <th width="200">Particulars</th>
                    <th width="90">Cost/Dzn</th>
                    <th>Total Cost</th>
                </thead>
				<?php
                $costing_per=return_field_value("costing_per","wo_pre_cost_mst","job_no='$job_no' and status_active=1 and is_deleted=0");
                    
                $dzn_qnty=0;
                if($costing_per==1)
                {
                    $dzn_qnty=12;
                }
                else if($costing_per==3)
                {
                    $dzn_qnty=12*2;
                }
                else if($costing_per==4)
                {
                    $dzn_qnty=12*3;
                }
                else if($costing_per==5)
                {
                    $dzn_qnty=12*4;
                }
                else
                {
                    $dzn_qnty=1;
                }
                    
                $sql="select common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='$job_no' and status_active=1 and is_deleted=0";
                $fabriccostArray=sql_select($sql);
                ?>
                <tr bgcolor="#E9F3FF">
                    <td>Commercial Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('comm_cost')],2); ?></td>
                    <td align="right">
                        <?php
                            $comm_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('comm_cost')]; 
                            echo number_format($comm_cost,2);
                        ?>
                    </td>
                </tr>
                <tr bgcolor="#FFFFFF">
                    <td>Lab Test Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('lab_test')],2); ?></td>
                    <td align="right">
                        <?php
                            $lab_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('lab_test')]; 
                            echo number_format($lab_cost,2);
                        ?>
                    </td>
                </tr>
                 <tr bgcolor="#E9F3FF">
                    <td>Inspection Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('inspection')],2); ?></td>
                    <td align="right">
                        <?php
                            $inspection_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('inspection')]; 
                            echo number_format($inspection_cost,2);
                        ?>
                    </td>
                </tr>
                <tr bgcolor="#FFFFFF">
                    <td>Freight Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('freight')],2); ?></td>
                    <td align="right">
                        <?php
                            $freight_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('freight')]; 
                            echo number_format($freight_cost,2);
                        ?>
                    </td>
                </tr>
                <tr bgcolor="#E9F3FF">
                    <td>Common OH Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('common_oh')],2); ?></td>
                    <td align="right">
                        <?php
                            $common_oh_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('common_oh')]; 
                            echo number_format($common_oh_cost,2);
							
							$tot_cost_per_dzn=$fabriccostArray[0][csf('comm_cost')]+$fabriccostArray[0][csf('lab_test')]+$fabriccostArray[0][csf('inspection')]+$fabriccostArray[0][csf('freight')]+$fabriccostArray[0][csf('common_oh')];
							$tot_cost=$comm_cost+$lab_cost+$inspection_cost+$freight_cost+$common_oh_cost;
                        ?>
                    </td>
                </tr>
                <tfoot>
                    <th>Total</th>
                    <th><?php echo number_format($tot_cost_per_dzn,2); ?></th>
                    <th><?php echo number_format($tot_cost,2); ?></th>
                </tfoot>    
            </table>
        </fieldset>
    </div>
<?php
}
disconnect($con);
?>