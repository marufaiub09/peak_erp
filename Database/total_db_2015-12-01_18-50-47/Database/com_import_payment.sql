-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_import_payment`
--

DROP TABLE IF EXISTS `com_import_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_import_payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `lc_id` varchar(50) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_head` int(11) NOT NULL,
  `adj_source` int(11) NOT NULL,
  `adj_source_ref` varchar(100) NOT NULL,
  `conversion_rate` double NOT NULL DEFAULT '0',
  `accepted_ammount` double NOT NULL DEFAULT '0',
  `domistic_currency` double NOT NULL DEFAULT '0',
  `remarks` varchar(500) DEFAULT NULL,
  `insert_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `status_active` int(1) NOT NULL DEFAULT '1' COMMENT '0:inactive; 1:active',
  `is_deleted` int(1) NOT NULL DEFAULT '0' COMMENT '0:not deleted, 1=deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_import_payment`
--

LOCK TABLES `com_import_payment` WRITE;
/*!40000 ALTER TABLE `com_import_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_import_payment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
