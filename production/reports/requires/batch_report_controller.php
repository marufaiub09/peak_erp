<?php 
header('Content-type:text/html; charset=utf-8');
session_start();

if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');

$user_name=$_SESSION['logic_erp']['user_id'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');

$search_by_arr=array(1=>"Date Wise Report",2=>"Wait For Heat Setting",5=>"Wait For Singeing",3=>"Wait For Dyeing",4=>"Wait For Re-Dyeing");//--------------------------------------------------------------------------------------------------------------------
if($action=="batchnumbershow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
if($db_type==0) $year_field_grpby="GROUP BY batch_no"; 
else if($db_type==2) $year_field_grpby=" GROUP BY batch_no,id,batch_no,batch_for,booking_no,color_id,batch_weight order by batch_no desc";
 $sql="select id,batch_no,batch_for,booking_no,color_id,batch_weight from pro_batch_create_mst where company_id=$company_name and is_deleted = 0 $year_field_grpby ";	
$arr=array(1=>$color_library);
	echo  create_list_view("list_view", "Batch no,Color,Booking no, Batch for,Batch weight ", "100,100,100,100,70","520","350",0, $sql, "js_set_value", "id,batch_no", "", 1, "0,color_id,0,0,0", $arr , "batch_no,color_id,booking_no,batch_for,batch_weight", "employee_info_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}//batchnumbershow;
if($action=="load_drop_down_buyer")
{ 
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
    echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90))  order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", 0, "" );
	exit();
}//cbo_buyer_name_td
if($action=="jobnumbershow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  {
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
if($db_type==0) $year_field="SUBSTRING_INDEX(a.insert_date, '-', 1) as year"; 
else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
if($db_type==0) $year_field_by="and YEAR(a.insert_date)"; 
else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
if($db_type==0) $year_field_grpby="GROUP BY a.job_no order by b.id desc"; 
else if($db_type==2) $year_field_grpby=" GROUP BY a.job_no,a.id,a.buyer_name,a.style_ref_no,a.gmts_item_id,b.po_number,a.job_no_prefix_num,a.insert_date,b.id order by b.id desc";
$year_job = str_replace("'","",$year);
if(trim($year)!=0) $year_cond=" $year_field_by=$year_job"; else $year_cond="";
$cbo_buyer_name=($cbo_buyer_name==0)?"%%" : "%$cbo_buyer_name%";
$sql="select a.id,a.buyer_name,a.style_ref_no,a.gmts_item_id,b.po_number,a.job_no_prefix_num as job_prefix,$year_field from wo_po_details_master a,wo_po_break_down b where b.job_no_mst=a.job_no and a.company_name=$company_id and a.buyer_name like '%$cbo_buyer_name%' $year_cond and a.is_deleted=0 $year_field_grpby";	
$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );

?>
<table width="500" border="1" rules="all" class="rpt_table">
	<thead>
        <tr>
            <th width="30">SL</th>
            <th width="100">Po number</th>
            <th width="50">Job no</th>
            <th width="40">Year</th>
            <th width="100">Buyer</th>
            <th width="100">Style</th>
            <th>Item Name</th>
        </tr>
   </thead>
</table>
<div style="max-height:380px; overflow:auto;">
<table id="table_body2" width="500" border="1" rules="all" class="rpt_table">
 <?php $rows=sql_select($sql);
	 $i=1;
	 foreach($rows as $data)
	 {
		 	if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
  ?>
	<tr bgcolor="<?php echo  $bgcolor;?>" onclick="js_set_value('<?php echo $data[csf('job_prefix')]; ?>')" style="cursor:pointer;">
		<td width="30"><?php echo $i; ?></td>
		<td width="100"><p><?php echo $data[csf('po_number')]; ?></p></td>
		<td align="center"  width="50"><p><?php echo $data[csf('job_prefix')]; ?></p></td>
        <td align="center" width="40"><p><?php echo $data[csf('year')]; ?></p></td>
		<td width="100"><p><?php echo $buyer[$data[csf('buyer_name')]]; ?></p></td>
		<td width="100"><p><?php echo $data[csf('style_ref_no')]; ?></p></td>
		<td><p><?php 
		$itemid=explode(",",$data[csf('gmts_item_id')]);
		foreach($itemid as $index=>$id){
			echo ($itemid[$index]==end($itemid))? $garments_item[$id] : $garments_item[$id].', ';
		}
		?></p></td>
	</tr>
    <?php $i++; } ?>
</table>
</div>
<script> setFilterGrid("table_body2",-1); </script>
<?php
	disconnect($con);
	exit();
}//JobNumberShow
if($action=="order_number_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
$buyer = str_replace("'","",$buyer_name);
$year = str_replace("'","",$year);
$buyer = str_replace("'","",$buyer_name);
if($db_type==0) $year_field="SUBSTRING_INDEX(b.insert_date, '-', 1) as year"; 
else if($db_type==2) $year_field="to_char(b.insert_date,'YYYY') as year";
if($db_type==0) $year_field_by="and YEAR(b.insert_date)"; 
else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
if ($company_name==0) $company=""; else $company=" and b.company_name=$company_name";
if(trim($year)!=0) $year_cond=" $year_field_by=$year"; else $year_cond="";
//echo $buyer;die;
if ($buyer==0) $buyername=""; else $buyername=" and b.buyer_name=$buyer";//$cbo_buyer_name=($cbo_buyer_name==0)?"%%" : "%$cbo_buyer_name%";
$sql = "select distinct a.id,b.job_no,a.po_number,b.company_name,b.buyer_name,b.job_no_prefix_num as job_prefix,$year_field from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company $buyername $year_cond order by a.id desc"; 
$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
?>
<table width="370" border="1" rules="all" class="rpt_table">
	<thead>
        <tr>
            <th width="30">SL</th>
            <th width="100">Order Number</th>
            <th width="50">Job no</th>
            <th width="80">Buyer</th>
            <th width="40">Year</th>
        </tr>
   </thead>
</table>
<div style="max-height:380px; overflow:auto;">
<table id="table_body2" width="370" border="1" rules="all" class="rpt_table">
 <?php $rows=sql_select($sql);
	 $i=1;
 foreach($rows as $data)
 {
	 if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
  ?>
	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="js_set_value('<?php echo $data[csf('po_number')]; ?>')" style="cursor:pointer;">
		<td width="30"><?php echo $i; ?></td>
		<td width="100"><p><?php echo $data[csf('po_number')]; ?></p></td>
		<td width="50"><p><?php echo $data[csf('job_prefix')]; ?></p></td>
		<td width="80"><p><?php echo $buyer[$data[csf('buyer_name')]]; ?></p></td>
		<td width="40" align="center"><p><?php echo $data[csf('year')]; ?></p></td>
	</tr>
    <?php $i++; } ?>
</table>
</div>
<script> setFilterGrid("table_body2",-1); </script>
<?php
	exit();
}
if($action=="batchextensionpopup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
$buyer = str_replace("'","",$buyer_name);
$year = str_replace("'","",$year);
$batch_number= str_replace("'","",$batch_number_show);
if($db_type==0) $year_field_by="and YEAR(a.insert_date)"; 
else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
if ($company_name==0) $company=""; else $company=" and a.company_id=$company_name";
if ($batch_number==0) $batch_no=""; else $batch_no=" and a.batch_no=$batch_number";
if(trim($year)!=0) $year_cond=" $year_field_by=$year"; else $year_cond="";
//echo $buyer;die;
if ($buyer==0) $buyername=""; else $buyername=" and b.buyer_name=$buyer";
 $sql="select a.id,a.batch_no,a.extention_no,a.batch_for,a.booking_no,a.color_id,a.batch_weight from pro_batch_create_mst a,pro_batch_create_dtls b where a.id=b.mst_id and a.is_deleted=0 $company $batch_no ";	
$arr=array(2=>$color_library);
	echo  create_list_view("list_view", "Batch no,Extention No,Color,Booking no, Batch for,Batch weight ", "100,100,100,100,100,170","620","350",0, $sql, "js_set_value", "extention_no,extention_no", "", 1, "0,0,color_id,0,0,0", $arr , "batch_no,extention_no,color_id,booking_no,batch_for,batch_weight", "employee_info_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}//batchnumbershow;
if($action=="batch_report")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$company = str_replace("'","",$cbo_company_name);
	$buyer = str_replace("'","",$cbo_buyer_name);
	$job_number = str_replace("'","",$job_number);
	$job_number_id = str_replace("'","",$job_number_show);
	$batch_no = str_replace("'","",$batch_number_show);
	//echo $batch_no;die;
	$batch_number_hidden = str_replace("'","",$batch_number);
	$ext_num = str_replace("'","",$txt_ext_no);
	$hidden_ext = str_replace("'","",$hidden_ext_no);
	$txt_order = str_replace("'","",$order_no);
	$hidden_order = str_replace("'","",$hidden_order_no);
	$cbo_type = str_replace("'","",$cbo_type);
	$year = str_replace("'","",$cbo_year);
	//echo $order_no;die;
	$txt_date_from = str_replace("'","",$txt_date_from);
	$txt_date_to = str_replace("'","",$txt_date_to);
	$process=33;
	$jobdata=($job_number_id )? " and d.job_no_prefix_num='".$job_number_id ."'" : '';
	$buyerdata=($buyer)?' and d.buyer_name='.$buyer : '';
	$batch_num=($batch_no)?" and a.batch_no='".$batch_no."'" : '';
	if(trim($ext_no)!="") $ext_no_search="%".trim($ext_no)."%"; else $ext_no_search="%%";
	if ($txt_order=="") $order_no=""; else $order_no="  and c.po_number='$txt_order'";
	//echo $order_no;die;
	if($db_type==0) $year_field_by="and YEAR(a.insert_date)"; 
	else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
	if($db_type==0) $find_inset="and  FIND_IN_SET(33,a.process_id)"; 
	else if($db_type==2) $find_inset="and   ',' || a.process_id || ',' LIKE '%33%'";
	if ($ext_num=="") $ext_no=""; else $ext_no="  and a.extention_no=$ext_num ";
	if(trim($year)!=0) $year_cond=" $year_field_by=$year"; else $year_cond="";
	if($txt_date_from && $txt_date_to)
	{
	if($db_type==0)
	{
	$date_from=change_date_format($txt_date_from,'yyyy-mm-dd');
	$date_to=change_date_format($txt_date_to,'yyyy-mm-dd');
	$dates_com="and a.batch_date BETWEEN '$date_from' AND '$date_to'";
	}
	if($db_type==2)
	{
	$date_from=change_date_format($txt_date_from,'','',1);
	$date_to=change_date_format($txt_date_to,'','',1);
	$dates_com="and a.batch_date BETWEEN '$date_from' AND '$date_to'";
	}
	}
	$yarn_lot_arr=array();
	if($db_type==0)
	{
	$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id,group_concat(distinct(a.yarn_lot)) as yarn_lot from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot!='0' group by a.prod_id, b.po_breakdown_id");
	}
	else if($db_type==2)
	{
	$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id,listagg(a.yarn_lot,',') within group (order by a.yarn_lot) as yarn_lot from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot!='0' group by a.prod_id, b.po_breakdown_id");
	}
	foreach($yarn_lot_data as $rows)
	{
		$yarn_lot=explode(",",$rows[csf('yarn_lot')]);
		$yarn_lot_arr[$rows[csf('prod_id')]][$rows[csf('po_breakdown_id')]]=implode(",",array_unique($yarn_lot));
	}
			//var_dump($yarn_lot);
	$sql_batch_dyeing=sql_select("select batch_id from  pro_fab_subprocess where entry_form=35 and status_active=1 and is_deleted=0 and batch_id>0");
	$i=1;
	foreach($sql_batch_dyeing as $row_dyeing)
	{
		if($i!==1) $row_d.=",";
		$row_d.=$row_dyeing[csf('batch_id')];
		$i++;
	}
	$sql_batch_h=sql_select("select batch_id from  pro_fab_subprocess where entry_form=32 and status_active=1 and is_deleted=0 and batch_id>0");
	$i=1;
	foreach($sql_batch_h as $row_h)
	{
		if($i!==1) $row_heat.=",";
		$row_heat.=$row_h[csf('batch_id')];
		$i++;
	}
	$sql_batch_h=sql_select("select batch_id from  pro_fab_subprocess where entry_form=47 and status_active=1 and is_deleted=0 and batch_id>0");
	$i=1;
	foreach($sql_batch_h as $row_s)
	{
		if($i!==1) $row_sin.=",";
		$row_sin.=$row_s[csf('batch_id')];
		$i++;
	}
	$sql_batch_dry=sql_select("select batch_id from  pro_fab_subprocess where entry_form=31 and status_active=1 and is_deleted=0 and batch_id>0");
	$i=1;
	foreach($sql_batch_dry as $rowdry)
	{
		if($i!==1) $row_dry.=",";
		$row_dry.=$rowdry[csf('batch_id')];
		$i++;
	}
	$sql_batch_stenter=sql_select("select batch_id from  pro_fab_subprocess where entry_form=48 and status_active=1 and is_deleted=0 and batch_id>0");
	$i=1;
	foreach($sql_batch_stenter as $row_sten)//Stentering
	{
		if($i!==1) $row_stentering.=",";
		$row_stentering.=$row_sten[csf('batch_id')];
		$i++;
	}
	if($db_type==0)
	{ 
		if($cbo_type==1)//Date Wise Report
		{
		$sql="
			select a.batch_no, a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  GROUP BY a.batch_no, b.item_description,b.prod_id order by a.batch_no
			";
		}
		else if($cbo_type==2) //wait for Heat Setting
		{
			if($row_h!=0)
			{
		$sql="
			select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst  $find_inset and a.id not in($row_heat) and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY a.batch_no, b.item_description order by a.batch_no ";
			}
		}
		else if($cbo_type==3)// Wait For Dyeing
		{
		$sql="
			select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst and a.id not in($row_d) and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY a.batch_no, b.item_description order by a.batch_no ";
		}
		else if($cbo_type==4) //Wait For Re-Dyeing
		{
		$sql="
			select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name
	from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst and a.batch_against=2 and a.id not in($row_d) and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY a.batch_no, b.item_description order by a.batch_no";
		}
		else if($cbo_type==5) //Wait For Singeing
		{
		if($db_type==0) $find_cond="and  FIND_IN_SET(94,a.process_id)"; 
		else if($db_type==2) $find_cond="and  ',' || a.process_id || ',' LIKE '%94%'";
		$w_sing_arr=array_chunk(array_unique(explode(",",$row_sin)),999);
		if($w_sing_arr!=0)
		{
			 $sql="
				select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst    and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 $find_cond  ";
			$p=1;
			foreach($w_sing_arr as $sing_row)
			{
				if($p==1) $sql .="and (a.id not in(".implode(',',$sing_row).")"; else  $sql .=" and a.id not in(".implode(',',$sing_row).")";
				$p++;
			}
			$sql .=")";
			$sql .=" GROUP BY a.batch_no, b.item_description,a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no,b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name order by a.batch_no"; 
		}
		}
	}
	else //Oracle start here
	{
	if($cbo_type==1)//Date Wise Report
		{
		$sql="
			select a.batch_no, a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY a.batch_no, b.item_description,b.prod_id,a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no,b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name order by a.batch_no
			";
		}
		else if($cbo_type==2) //wait for Heat Setting
		{
			$w_heat_arr=array_chunk(array_unique(explode(",",$row_heat)),999);
		if($w_heat_arr!=0)
		{
			 $sql="
				select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst  $find_inset  and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  ";
			$p=1;
			foreach($w_heat_arr as $h_batch_id)
			{
				if($p==1) $sql .="and (a.id not in(".implode(',',$h_batch_id).")"; else  $sql .=" and a.id not in(".implode(',',$h_batch_id).")";
				$p++;
			}
			$sql .=")";
			$sql .=" GROUP BY a.batch_no, b.item_description,a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no,b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name order by a.batch_no"; 
		}
		}
		else if($cbo_type==3)// Wait For Dyeing
		{
		$w_dyeing_arr=array_chunk(array_unique(explode(",",$row_d)),999);
		//print_r($w_dyeing_arr);die;
	if($w_dyeing_arr!=0)
		{
		  $sql="
			select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_mst a,pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_fab_subprocess e,pro_fab_subprocess_dtls f where  a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst  and  e.batch_id=a.id and e.id=f.mst_id  and e.entry_form=32 and a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 ";
			$p=1;
			foreach($w_dyeing_arr as $d_batch_id)
			{
				if($p==1) $sql .="and (a.id not in(".implode(',',$d_batch_id).")"; else  $sql .=" and a.id not in(".implode(',',$d_batch_id).")";
				$p++;
			}
			$sql .=")";
			$sql .=" GROUP BY a.batch_no, b.item_description,a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no,b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name order by a.batch_no"; 
			//echo $sql;die;
		}
		}
		else if($cbo_type==4) //Wait For Re-Dyeing
		{
		$w_dyeing_arr=array_chunk(array_unique(explode(",",$row_d)),999);
		if($w_dyeing_arr!=0)
			{
		$sql="
			select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) as po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name
	from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst and a.batch_against=2  and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0";
			$p=1;
			foreach($w_dyeing_arr as $d_batch_id)
			{
				if($p==1) $sql .="and (a.id not in(".implode(',',$d_batch_id).")"; else  $sql .=" and a.id not in(".implode(',',$d_batch_id).")";
				$p++;
			}
			$sql .=")";
			$sql .="  GROUP BY a.batch_no, b.item_description,a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no,b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name order by a.batch_no";
			}
		 }	
		else if($cbo_type==5) //Wait For Singeing
		{
			if($db_type==0) $find_cond="and  FIND_IN_SET(94,a.process_id)"; 
			else if($db_type==2) $find_cond="and  ',' || a.process_id || ',' LIKE '%94%'";
		$w_sing_arr=array_chunk(array_unique(explode(",",$row_sin)),999);
		if($w_sing_arr!=0)
		{
			 $sql="
				select a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $ext_no $year_cond and a.entry_form=0 and a.id=b.mst_id and b.po_id=c.id and d.job_no=c.job_no_mst    and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 $find_cond  ";
			$p=1;
			foreach($w_sing_arr as $sing_row)
			{
				if($p==1) $sql .="and (a.id not in(".implode(',',$sing_row).")"; else  $sql .=" and a.id not in(".implode(',',$sing_row).")";
				$p++;
			}
			$sql .=")";
			$sql .=" GROUP BY a.batch_no, b.item_description,a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no,b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name order by a.batch_no"; 
		}
	   }	
	}
	//echo $sql;//listagg(cast(a.po_number as varchar2(4000)),',') within group (order by a.po_number) as po_no
$batchdata=sql_select($sql);
?>
<div>
<fieldset style="width:1160px;">
<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> <?php echo $search_by_arr[$cbo_type]; ?> </strong>
<br><b>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?> </b>
 </div>
 <table class="rpt_table" width="1160" cellpadding="0" cellspacing="0" border="1" rules="all">
        <thead>
            <tr>
                <th width="30">SL</th>
                <th width="75">Batch Date</th>
                <th width="60">Batch No</th>
                <!--th width="40">Ext. No</th-->
                <th width="80">Batch Color</th>
                <th width="50">Buyer</th>
                <th width="120">PO No</th>
                <!--th width="70">Job</th-->
                <th width="100">Construction</th>
                <th width="150">Composition</th>
                <th width="50">Dia/ Width</th>
                <th width="50">GSM</th>
                <th width="60">Lot No</th>
                <th width="70">Batch Qty.</th>
                <!--th width="50">Batch Weight</th-->
                <th>Grey Issued</th>
            </tr>
        </thead>
</table>
<div style=" max-height:350px; width:1180px; overflow-y:scroll;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1160" cellpadding="0" cellspacing="0" border="1" rules="all">
<tbody>
			<?php 
			$booking_qnty_arr=array();
			$query=sql_select("select b.po_break_down_id, b.fabric_color_id, sum(b.grey_fab_qnty ) as grey_fab_qnty from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.booking_type=1 and a.is_deleted=0 and a.status_active=1 and b.booking_type=1 and b.status_active=1 and b.is_deleted=0 group by b.po_break_down_id, b.fabric_color_id");
			foreach($query as $row)
			{
				$booking_qnty_arr[$row[csf('po_break_down_id')]][$row[csf('fabric_color_id')]]=$row[csf('grey_fab_qnty')];
			}
			$sl=1;
			$i=1;
			$f=0;
			$btq=0;
			$batch_chk_arr=array();
			foreach($batchdata as $batch)
			{ 
			if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			$order_id=$batch[csf('po_id')];
			$color_id=$batch[csf('color_id')];
			$booking_qty=$booking_qnty_arr[$order_id][$color_id];
			$desc=explode(",",$batch[csf('item_description')]); 
			$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
			?>
            <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onclick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
            <?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
				{ $f++;
							?>
                <td width="30"><?php echo $f; ?></td>
                <td align="center" width="75" title="<?php echo change_date_format($batch[csf('batch_date')]); ?>"><p><?php echo change_date_format($batch[csf('batch_date')]); ?></p></td>
                <td align="center" width="60" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
                <!--td align="center" width="40" title="<?php //echo $batch[csf('extention_no')]; ?>"><p><?php //echo $batch[csf('extention_no')]; ?></p></td-->
                <td width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
                <td width="50" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
				<?php	
                $batch_chk_arr[]=$batch[csf('batch_no')];
                $book_qty+=$booking_qty;
                  } 
				else
				  { ?>
                <td width="30"><?php //echo $sl; ?></td>
                <td  align="center" width="75"><p><?php //echo $batch[csf('batch_date')]; ?></p></td>
                <td  align="center" width="60"><p><?php //echo $booking_qty; ?></p></td>
                <td  align="center" width="40"><p><?php //echo $batch[csf('extention_no')]; ?></p></td>
                <td  width="80"><p><?php //echo $color_library[$batch[csf('color_id')]]; ?></p></td>
                <td  width="50"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
				<?php }
				?>
                <td width="120" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
                <!--td width="70" title="<?php// echo $batch[csf('job_no_prefix_num')]; ?>"><p><?php //echo $batch[csf('job_no_prefix_num')]; ?></p></td-->
                <td width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
                <td width="150" title="<?php echo $desc[1]; ?>"><p><?php echo $desc[1]; ?></p></td>
                <td align="center" width="50" title="<?php echo $desc[3];  ?>"><p><?php echo $desc[3]; ?></p></td>
                <td align="center" width="50" title="<?php echo $desc[2]; ?>"><p><?php echo $desc[2]; ?></p></td>
                 <td align="left" width="60" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$order_id]; ?></p></td>
                <td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo $batch[csf('batch_qnty')];  ?></td>
                <!--td  align="right" width="50" title="<?php// echo $batch[csf('batch_weight')]; ?>"><?php //echo $batch[csf('batch_weight')]; ?></td-->
                <td>&nbsp;</td>
            </tr>
			<?php 
                $i++;
                $btq+=$batch[csf('batch_qnty')];
                $balance=$btq-$book_qty;
                $bal_qty=$balance;
                if($bal_qty>0)
                {
                $color="";	
                $txt="Over Batch Qty";
                }
                else if($bal_qty<0)
                {
                $color="red";
                $txt="Below Batch Qty";
                }
                } 
        	 ?>
       </tbody>
</table>
<table class="rpt_table" width="1160" cellpadding="0" cellspacing="0" border="1" rules="all">
        <tfoot>
            <tr>
                <th width="30">&nbsp;</th>
                <th width="75">&nbsp;</th>
                <th width="60">&nbsp;</th>
                <th width="40">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="50">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="150">&nbsp;</th>
                <th width="50">&nbsp;</th>
                <th width="50">&nbsp;</th>
                <th width="60">&nbsp;</th>
                <th width="70"><?php echo $btq; ?></th>
                <th width="50">&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <tr>
                <td colspan="10" align="right" style="border:none;"><b>Grey Req. Qty </b></td>
                <td colspan="11" align="left">&nbsp; 
                 <?php echo number_format($book_qty,2); ?>
                </td>
            </tr>
             <tr>
                <td colspan="10" align="right" style="border:none;"> <b>Batch Qty.</b></td>
                <td colspan="11" align="left">&nbsp; <?php echo number_format($btq,2); ?> </td>
            </tr>
             <tr>
                <td colspan="10" align="right" style="border:none;"><b>Balance Qty: &nbsp;&nbsp;</b> </td>
                <td colspan="11" align="left">&nbsp;<b><font color="<?php echo $color;?>"> <?php  echo number_format($bal_qty,2); ?> &nbsp;&nbsp;</font></b> </td>
            </tr>
        </tfoot>
</table>
</div>
</fieldset>
</div>
<?php
	exit();
}//BatchReport
?>