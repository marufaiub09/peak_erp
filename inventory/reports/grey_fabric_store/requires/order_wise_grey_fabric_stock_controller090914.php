<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
$store_arr=return_library_array( "select id, store_name from lib_store_location", "id", "store_name"  );
$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$other_party_arr=return_library_array( "select id,other_party_name from lib_other_party", "id", "other_party_name"  );
$machine_arr=return_library_array( "select id,dia_width from lib_machine_name", "id", "dia_width"  );

if($action=="load_drop_down_buyer")
{
	$data=explode("_",$data);
	if($data[1]==1) $party="1,3,21,90"; else $party="80";
	echo create_drop_down( "cbo_buyer_id", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[0]' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in ($party)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $selected, "","" );
	exit();
}

if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
	
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#hide_job_id').val( id );
		$('#hide_job_no').val( ddd );
	} 
	
		/*function js_set_value(str)
		{
			var splitData = str.split("_");
			//alert (splitData[1]);
			$("#hide_job_id").val(splitData[0]); 
			$("#hide_job_no").val(splitData[1]); 
			parent.emailwindow.hide();
		}*/
    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Job No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:100px;" onClick="reset_form('styleRef_form','search_div','','','','');"></th> 					<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                    <input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+'<?php echo $cbo_year_id; ?>'+'**'+'<?php echo $cbo_month_id; ?>', 'create_job_no_search_list_view', 'search_div', 'order_wise_grey_fabric_stock_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_job_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	$year_id=$data[4];
	$month_id=$data[5];
	//echo $month_id;
	
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	//$year="year(insert_date)";
	//if($month_id!=0) $month_cond=" and month(insert_date)=$month_id"; else $month_cond="";
	
	$arr=array (0=>$company_arr,1=>$buyer_arr);
	
	if($db_type==0)
	{
		if($year_id!=0) $year_search_cond=" and year(insert_date)=$year_id"; else $year_search_cond="";
		$year_cond= "year(insert_date)as year";
	}
	else if($db_type==2)
	{
		if($year_id!=0) $year_search_cond=" and TO_CHAR(insert_date,'YYYY')=$year_id"; else $year_search_cond="";
		$year_cond= "TO_CHAR(insert_date,'YYYY') as year";
	}
		
	$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no, $year_cond from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $year_search_cond $month_cond order by job_no DESC";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","620","270",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0,0','',1) ;
   exit(); 
} 

if ($action=="order_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
    <script>
	 
	function js_set_value(str)
	{
		var splitData = str.split("_");
		//alert (splitData[1]);
		$("#order_no_id").val(splitData[0]); 
		$("#order_no_val").val(splitData[1]); 
		parent.emailwindow.hide();
	}
		  
	</script>
     <input type="hidden" id="order_no_id" />
     <input type="hidden" id="order_no_val" />
 <?php
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and b.buyer_name=$data[1]";
	if ($data[2]=="") $order_no=""; else $order_no=" and a.po_number=$data[2]";
	$job_no=str_replace("'","",$txt_job_id);
	if ($data[2]=="") $job_no_cond=""; else $job_no_cond="  and b.job_no_prefix_num='$data[2]'";
	
	$sql="select a.id, a.po_number, b.job_no_prefix_num, b.job_no, b.buyer_name, b.style_ref_no from wo_po_details_master b, wo_po_break_down a  where b.job_no=a.job_no_mst and b.company_name=$data[0] and b.is_deleted=0 $buyer_name $job_no_cond ORDER BY b.job_no DESC";
	//echo $sql;
	$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$arr=array(1=>$buyer);
	
	echo  create_list_view("list_view", "Job No,Buyer,Style Ref.,Order No", "110,110,150,180","610","350",0, $sql, "js_set_value", "id,po_number", "", 1, "0,buyer_name,0,0,0", $arr , "job_no_prefix_num,buyer_name,style_ref_no,po_number", "order_wise_grey_fabric_stock_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
	disconnect($con);
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if(str_replace("'","",$cbo_buyer_id)!="" && str_replace("'","",$cbo_buyer_id)!=0) $buyer_id_cond=" and b.buyer_name=$cbo_buyer_id";
	$job_no=str_replace("'","",$txt_job_no);
	if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and b.job_no_prefix_num in ($job_no) ";
	$year_id=str_replace("'","",$cbo_year);

	if($db_type==0)
	{
		if($year_id!=0) $year_cond=" and year(b.insert_date)=$year_id"; else $year_cond="";
	}
	else if($db_type==2)
	{
		if($year_id!=0) $year_cond=" and TO_CHAR(b.insert_date,'YYYY')=$year_id"; else $year_cond="";
	}

	
	$order_no=str_replace("'","",$txt_order_id);
	if(str_replace("'","",$txt_order_id)!="" && str_replace("'","",$txt_order_id)!=0) $order_id_cond=" and a.id in ($order_no)";
	
	$date_from=str_replace("'","",$txt_date_from);
	if( $date_from=="") $receive_date=""; else $receive_date= " and e.receive_date <=".$txt_date_from."";
	
	//==========================================================
	if(str_replace("'","",$cbo_buyer_id)!="" && str_replace("'","",$cbo_buyer_id)!=0) $buyer_id_cond_trans=" and d.buyer_name=$cbo_buyer_id";
	$job_no=str_replace("'","",$txt_job_no);
	if ($job_no=="") $job_no_cond_trans=""; else $job_no_cond_trans=" and d.job_no_prefix_num in ($job_no) ";
	$year_id=str_replace("'","",$cbo_year);
	
	$variable_sql_result = sql_select("select auto_update from variable_settings_production where company_name=$cbo_company_id and variable_list=15 and item_category_id=13 and status_active=1");
	if($variable_sql_result[0][csf('auto_update')]==2)
	{
		$variable_set_cond=" and e.entry_form in (22)";
	}
	else
	{
		$variable_set_cond=" and e.entry_form in (2,22)";
	}
	
	
	if($db_type==0)
	{
		if($year_id!=0) $year_cond_trans=" and year(d.insert_date)=$year_id"; else $year_cond_trans="";
	}
	else if($db_type==2)
	{
		if($year_id!=0) $year_cond_trans=" and TO_CHAR(d.insert_date,'YYYY')=$year_id"; else $year_cond_trans="";
	}
	
	
	$order_no=str_replace("'","",$txt_order_id);
	if(str_replace("'","",$txt_order_id)!="" && str_replace("'","",$txt_order_id)!=0) $order_id_cond_trans=" and c.id in ($order_no)";
	
	$date_from=str_replace("'","",$txt_date_from);
	if( $date_from=="") $receive_date=""; else $receive_date_trans= " and a.transfer_date <=".$txt_date_from."";
	

	$composition_arr=array();
	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}

	$transfer_in_arr=array(); $trans_arr=array();	
	$sql_transfer_in="select a.to_order_id, b.from_prod_id, b.to_rack, b.to_shelf, b.y_count, b.brand_id, b.yarn_lot, sum(b.transfer_qnty) as transfer_in_qnty, sum(b.roll) as transfer_in_roll from  inv_item_transfer_mst a, inv_item_transfer_dtls b, wo_po_break_down c, wo_po_details_master d where c.job_no_mst=d.job_no and a.to_order_id=c.id and a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=13 $buyer_id_cond_trans $year_cond_trans $job_no_cond_trans $order_id_cond_trans group by a.to_order_id, b.from_prod_id, b.y_count, b.yarn_lot, b.to_rack, b.to_shelf, b.brand_id";
	$data_transfer_in_array=sql_select($sql_transfer_in);
	if(count($data_transfer_in_array)>0)
	{
		foreach( $data_transfer_in_array as $row )
		{
 			$transfer_in_arr[$row[csf('to_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('to_rack')]][$row[csf('to_shelf')]]['qty']=$row[csf('transfer_in_qnty')];
			$transfer_in_arr[$row[csf('to_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('to_rack')]][$row[csf('to_shelf')]]['roll']=$row[csf('transfer_in_roll')];
			
			$trans_data=$row[csf('to_order_id')]."_".$row[csf('from_prod_id')]."_".$row[csf('y_count')]."_".$row[csf('yarn_lot')]."_".$row[csf('to_rack')]."_".$row[csf('to_shelf')];
			//echo $trans_data."<br>";
			$trans_arr[]=$trans_data;
		}
	}
	//var_dump ($trans_arr);
	$product_array=array();	
	$prod_query="Select id, detarmination_id, gsm, dia_width, brand, yarn_count_id, lot, color from product_details_master where item_category_id=13 and company_id=$cbo_company_id and status_active=1 and is_deleted=0 ";
	$prod_query_sql=sql_select($prod_query);
	if(count($prod_query_sql)>0)
	{
		foreach( $prod_query_sql as $row )
		{
			$product_array[$row[csf('id')]]['detarmination_id']=$row[csf('detarmination_id')];
			$product_array[$row[csf('id')]]['gsm']=$row[csf('gsm')];
			$product_array[$row[csf('id')]]['dia_width']=$row[csf('dia_width')];
			$product_array[$row[csf('id')]]['brand']=$row[csf('brand')];
			$product_array[$row[csf('id')]]['yarn_count_id']=$row[csf('yarn_count_id')];
			$product_array[$row[csf('id')]]['lot']=$row[csf('lot')];
			$product_array[$row[csf('id')]]['color']=$row[csf('color')];
		}
	}
	
	//print_r($trans_arr);die;
	$transfer_out_arr=array(); 
	$sql_transfer_out="select a.from_order_id, b.from_prod_id, b.rack, b.shelf, b.y_count, b.brand_id, b.yarn_lot, sum(b.transfer_qnty) as transfer_out_qnty, sum(b.roll) as transfer_out_roll from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.from_order_id, b.from_prod_id, b.y_count, b.yarn_lot, b.rack, b.shelf, b.brand_id";
	$data_transfer_out_array=sql_select($sql_transfer_out);
	if(count($data_transfer_out_array)>0)
	{
		foreach( $data_transfer_out_array as $row )
		{
			$transfer_out_arr[$row[csf('from_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('shelf')]]['qty']=$row[csf('transfer_out_qnty')];
			$transfer_out_arr[$row[csf('from_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('shelf')]]['roll']=$row[csf('transfer_out_roll')];
		}
	}
	//print_r ($transfer_out_arr);
	//var_dump($transfer_out_arr);
	$transaction_date_array=array();
	$sql_date="Select prod_id, min(transaction_date) as min_date, max(transaction_date) as max_date from inv_transaction where status_active=1 and is_deleted=0 group by prod_id";
	$sql_date_result=sql_select($sql_date);
	foreach( $sql_date_result as $row )
	{
		$transaction_date_array[$row[csf('prod_id')]]['min_date']=$row[csf('min_date')];
		$transaction_date_array[$row[csf('prod_id')]]['max_date']=$row[csf('max_date')];
	}
	ob_start();
	?>
    <fieldset>
        <table cellpadding="0" cellspacing="0" width="1800">
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="25" style="font-size:18px"><strong><?php echo $report_title; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="25" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="25" style="font-size:14px"><strong> <?php if($date_from!="") echo "Upto : ".change_date_format(str_replace("'","",$txt_date_from)) ;?></strong></td>
            </tr>
        </table>
        <table width="1800" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" align="left">            	
            <thead>
            	<tr>
                	<th width="30" rowspan="2">SL</th>
                    <th colspan="7">Fabric Details</th>
                    <th colspan="3">Used Yarn Details</th>
                    <th width="100" rowspan="2">Booking/ Prog. No</th>
                    <th colspan="4">Receive Details</th>
                    <th colspan="4">Issue Details</th>
                    <th colspan="5">Stock Details</th>
            	</tr>
                <tr>
                	<th width="">Const. & Comp</th>
                    <th width="60">GSM</th>
                    <th width="60">F/Dia</th>
                    <th width="60">M/Dia</th>
                    <th width="60">Stich Length</th> 
                    <th width="80">Dyeing Color</th>
                    <th width="80">Color Type</th>
                    
                    <th width="60">Y. Count</th>
                    <th width="80">Y. Brand</th>
                    <th width="80">Y. Lot</th>
                    
                    <th width="80">Recv. Qty.</th>
                    <th width="80">Transf. In Qty.</th>
                    <th width="80">Total Recv.</th>
                    <th width="60">Recv. Roll</th>
                    
                    <th width="80">Issue Qty.</th>
                    <th width="80">Transf. Out Qty.</th>
                    <th width="80">Total Issue</th>
                    <th width="60">Issue Roll</th>
                    
                    <th width="80">Stock Qty.</th>
                    <th width="60">Roll Qty.</th>
                    <th width="50">Rack</th>
                    <th width="50">Shelf</th>
                    <th width="50">DOH</th>
                </tr>
            </thead>
        </table>
        <div style="width:1817px; overflow-y: scroll; max-height:380px;" id="scroll_body">
			<table width="1800" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" align="left"> 
            <?php
			$issue_qty_roll_array=array();
			$sql_issue="Select a.po_breakdown_id, a.prod_id, b.yarn_count, b.yarn_lot, b.rack, b.self, sum(a.quantity ) as issue_qnty, sum(b.no_of_roll) as issue_roll from order_wise_pro_details a, inv_grey_fabric_issue_dtls b where a.dtls_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.entry_form=16 group by a.po_breakdown_id, a.prod_id, b.yarn_count, b.yarn_lot, b.rack, b.self ";
			$result_sql_issue=sql_select( $sql_issue );
			foreach ($result_sql_issue as $row)
			{
				$issue_qty_roll_array[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['qty']=$row[csf('issue_qnty')];
				$issue_qty_roll_array[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['roll']=$row[csf('issue_roll')];
			}
			
		if($db_type==0)
		{
		   $sql_dtls="select distinct(a.po_number) as po_number, sum(a.po_quantity) as po_quantity, b.job_no, b.buyer_name, b.style_ref_no,
			sum(c.quantity) as quantity, c.po_breakdown_id, c.prod_id,
			d.febric_description_id, d.gsm, d.width, d.stitch_length, d.color_id, d.color_range_id, d.yarn_lot, d.yarn_count, d.brand_id, d.rack, d.self, d.machine_no_id, 
			sum(case when c.trans_type=1  then d.no_of_roll else 0 end) as rec_roll,
			e.booking_no
			from wo_po_break_down a, wo_po_details_master b, order_wise_pro_details c,  pro_grey_prod_entry_dtls d, inv_receive_master e
			where a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 and b.is_deleted=0 
			and c.trans_type=1  and c.trans_id!=0 and c.status_active=1 and c.is_deleted=0 and c.entry_form in(2,22)
			and c.po_breakdown_id=a.id 
			and c.dtls_id=d.id
			and e.id=d.mst_id and d.status_active=1 and d.is_deleted=0 AND e.company_id=$cbo_company_id
			and e.item_category=13 and e.status_active=1 and e.is_deleted=0 and a.id in(3278,3291,3301) $buyer_id_cond $job_no_cond $year_cond $order_id_cond $receive_date $variable_set_cond
			group by a.po_number, c.prod_id,  d.yarn_count, d.yarn_lot, d.rack, d.self order by a.po_number, c.prod_id
			";	
		}
		elseif($db_type==2)
		{
			$sql_dtls="select a.po_number as po_number, sum(a.po_quantity) as po_quantity, b.job_no, b.buyer_name, b.style_ref_no,
			sum(c.quantity) as quantity, max(c.po_breakdown_id) as po_breakdown_id, c.prod_id,
			listagg(d.stitch_length,',') within group (order by d.stitch_length) as stitch_length, 
			max(d.color_range_id) as color_range_id, d.yarn_lot, d.yarn_count, d.rack, d.self, max(d.machine_no_id) as machine_no_id, 
			sum(case when c.trans_type=1 then d.no_of_roll else 0 end) as rec_roll,
			listagg(e.booking_no,',') within group (order by e.booking_no) as booking_no
			from wo_po_break_down a, wo_po_details_master b, order_wise_pro_details c,  pro_grey_prod_entry_dtls d, inv_receive_master e
			where a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 and b.is_deleted=0 
			and c.trans_type=1 and c.trans_id!=0 and c.status_active=1 and c.is_deleted=0 and c.entry_form in(2,22)
			and c.po_breakdown_id=a.id 
			and c.dtls_id=d.id
			and e.id=d.mst_id and d.status_active=1 and d.is_deleted=0 AND e.company_id=$cbo_company_id
			and e.item_category=13 and e.status_active=1 and e.is_deleted=0 $buyer_id_cond $job_no_cond $order_id_cond $receive_date $variable_set_cond
			group by a.po_number, c.prod_id, d.yarn_count, d.yarn_lot, d.rack, d.self, b.job_no, b.buyer_name, b.style_ref_no order by a.po_number, c.prod_id";
		}
			
		//echo $sql_dtls;	
			
			$i=1; $k=1; //die;
			$order_arr=array(); 
			$nameArray=sql_select( $sql_dtls ); $trnsfer_in_qty=0;
			//echo $sql_dtls;die;
			$trans_in_array=array();//die;
			foreach ($nameArray as $row)
			{
				$prod_id=$row[csf("prod_id")];
				$order_id=$row[csf("po_breakdown_id")];
				$yarn_count=$row[csf('yarn_count')];//$product_array[$row[csf('prod_id')]]['yarn_count_id'];
				$yarn_lot=$row[csf('yarn_lot')];//$product_array[$row[csf('prod_id')]]['lot'];
				
				$rack=$row[csf("rack")];
				$selfd=$row[csf("self")];
				
				$count_id=explode(',',$yarn_count);
				$count_val='';
				foreach ($count_id as $val)
				{
					if($count_val=='') $count_val=$count_arr[$val]; else $count_val.=",".$count_arr[$val];
				}
				
				$trnsfer_in_qty=$transfer_in_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['qty'];
				$trnsfer_in_roll=$transfer_in_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['roll'];
				$trnsfer_out_qty=$transfer_out_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['qty'];
				$trnsfer_out_roll=$transfer_out_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['roll'];
				$issue_qty=$issue_qty_roll_array[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['qty'];
				$issue_roll=$issue_qty_roll_array[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['roll'];
				//$trans_in_array=$trnsfer_in_qty;
				
				$trans_data_in=$order_id."_".$prod_id."_".$yarn_count."_".$yarn_lot."_".$rack."_".$selfd;
				$trans_in_array[]=$trans_data_in;
				
				if(!in_array($row[csf('po_number')],$order_arr))
				{
					if($k!=1)
					{
						foreach($trans_arr as $key=>$val2)
						{
							$value=explode("_",$val2);
							$po_id=$value[0];
							
							$count=explode(',',$value[2]);
							$count_value='';
							foreach ($count as $val)
							{
								if($count_value=='') $count_value=$count_arr[$val]; else $count_value.=",".$count_arr[$val];
							}
							
							if($po_id==$prev_order_id)
							{
								if(!in_array($val2,$trans_in_array))
								{
									$trnsfer_in_qty=$transfer_in_arr[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['qty'];
									$trnsfer_in_roll=$transfer_in_arr[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['roll'];
									$issue_qty=$issue_qty_roll_array[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['qty'];
									$issue_roll=$issue_qty_roll_array[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['roll'];
									
									if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
								?>
									<tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
										<td width="30"><?php echo $i; ?></td>
										<td width=""><p><?php echo $composition_arr[$product_array[$value[1]]['detarmination_id']]; ?></p></td>
										<td width="60"><p><?php echo $product_array[$value[1]]['gsm']; ?></p></td>
										<td width="60"><p><?php echo $product_array[$value[1]]['dia_width']; ?></p></td>
										<td width="60"><p>&nbsp;</p></td>
										<td width="60"><p>&nbsp;</p></td>
										<td width="80"><p>&nbsp;</p></td>
										<td width="80"><p>&nbsp;<?php echo $row[csf('po_number')]; ?></p></td>
										<td width="60"><p><?php echo $count_value; ?></p></td> 
										<td width="80"><p><?php echo $brand_arr[$product_array[$value[1]]['brand']]; ?>&nbsp;</p></td>
										<td width="80"><p><?php echo $value[3]; ?></p></td>
										<td width="100"><p>&nbsp;</p></td>
										<td width="80" align="right"><p>&nbsp;</p></td>
										<td width="80" align="right"><p><?php echo number_format($trnsfer_in_qty,2); $tot_transfer_in_qty+=$trnsfer_in_qty; ?>&nbsp;</p></td>
										<td width="80" align="right"><p><?php $rec_bal=$trnsfer_in_qty; echo number_format($rec_bal,2); $tot_rec_bal+=$rec_bal; ?>&nbsp;</p></td>
										<td width="60" align="right"><p><?php $rec_roll=$trnsfer_in_roll; echo $rec_roll; $tot_rec_roll+=$rec_roll; ?>&nbsp;</p></td> 
										<td width="80" align="right"><p><?php echo number_format($issue_qty,2); $tot_issue_qty+=$issue_qty; ?>&nbsp;</p></td>
										<td width="80" align="right"><p><?php echo number_format($trnsfer_out_qty,2); $tot_transfer_out_qty+=$trnsfer_out_qty; ?>&nbsp;</p></td>
										<td width="80" align="right"><p><?php $issue_bal=$issue_qty+$trnsfer_out_qty; echo number_format($issue_bal,2); $tot_issue_bal+=$issue_bal; ?></p></td>
										<td width="60" align="right"><p><?php $iss_roll=$issue_roll-$trnsfer_out_roll; echo $iss_roll; $tot_issue_roll+=$iss_roll; ?>&nbsp;</p></td>
										<td width="80" align="right"><p><?php $stock=$rec_bal-$issue_bal; echo number_format($stock,2); $tot_stock+=$stock; ?>&nbsp;</p></td>
										<td width="60" align="right"><p><?php $stock_roll_qty=$rec_roll-$iss_roll; echo $stock_roll_qty; $tot_stock_roll_qty+=$stock_roll_qty; ?>&nbsp;</p></td>
										<td width="50" align="center"><p><?php echo $value[4]; ?></p></td>
										<td width="50" align="center"><p><?php echo $value[5]; ?></p></td>
										<?php
											$daysOnHand = datediff("d",change_date_format($transaction_date_array[$value[1]]['max_date'],'','',1),change_date_format(date("Y-m-d"),'','',1));
										?>
										<td width="50" align="center"><p><?php echo $daysOnHand; ?></p></td>
									</tr>
								<?php	
								$i++;
								}
							}
						}						
					?>
                        <tr class="tbl_bottom">
                            <td colspan="12" align="right"><b>Order Total</b></td>
                          	<td align="right"><?php echo number_format($tot_rec_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_transfer_in_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_rec_bal,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo $tot_rec_roll; ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_issue_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_transfer_out_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_issue_bal,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo $tot_issue_roll; ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_stock,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo $tot_stock_roll_qty; ?>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                	<?php
                        unset($tot_req_qty);
						unset($tot_rec_qty);
                        unset($tot_transfer_in_qty);
                        unset($tot_rec_bal);
						unset($tot_rec_roll);
						unset($tot_issue_qty);
						unset($tot_transfer_out_qty);
						unset($tot_issue_bal);
						unset($tot_issue_roll);
						unset($tot_stock);
						unset($tot_stock_roll_qty);
						unset($trans_in_array);
                    }	
                ?>
                    <tr><td colspan="25" style="font-size:14px" bgcolor="#CCCCAA">&nbsp;<b><?php echo "Order No: ".$row[csf('po_number')]."; Job No: ".$row[csf('job_no')]."; Style Ref: ".$row[csf('style_ref_no')]."; Buyer: ".$buyer_arr[$row[csf('buyer_name')]]."; RMG Qty: ".number_format($row[csf('po_quantity')],2); ?>&nbsp;</b></td></tr>
                <?php	
                    $order_arr[]=$row[csf('po_number')];
					 $k++;
                }
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
				?>
                <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                    <td width="30"><?php echo $i; ?></td>
                    <td width=""><p><?php echo $composition_arr[$product_array[$row[csf('prod_id')]]['detarmination_id']]; ?></p></td>
                    <td width="60"><p><?php echo $product_array[$row[csf('prod_id')]]['gsm']; ?></p></td>
                    <td width="60"><p><?php echo $product_array[$row[csf('prod_id')]]['dia_width']; ?></p></td>
                    <td width="60"><p><?php echo $machine_arr[$row[csf('machine_no_id')]]; ?></p></td>
                    <td width="60"><p><?php echo $row[csf('stitch_length')]; ?></p></td>
                    <td width="80"><p><?php echo $color_arr[$product_array[$row[csf('prod_id')]]['color']]; ?></p></td>
                    <td width="80"><p><?php echo $color_range[$row[csf('color_range_id')]]; ?></p></td>
                    <td width="60"><p><?php echo $count_val; ?></p></td> 
                    <td width="80"><p><?php echo $brand_arr[$product_array[$row[csf('prod_id')]]['brand']]; ?></p></td>
                    <td width="80"><p><?php echo $row[csf('yarn_lot')]; ?></p></td>
                    <td width="100"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                    
                    <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); $tot_rec_qty+=$row[csf('quantity')]; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($trnsfer_in_qty,2); $tot_transfer_in_qty+=$trnsfer_in_qty; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php $rec_bal=$row[csf('quantity')]+$trnsfer_in_qty; echo number_format($rec_bal,2); $tot_rec_bal+=$rec_bal; ?>&nbsp;</p></td>
                    <td width="60" align="right"><p><?php $rec_roll=$row[csf('rec_roll')]+$trnsfer_in_roll; echo $rec_roll; $tot_rec_roll+=$rec_roll; ?>&nbsp;</p></td> 
                    
                    <td width="80" align="right"><p><?php echo number_format($issue_qty,2); $tot_issue_qty+=$issue_qty; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($trnsfer_out_qty,2); $tot_transfer_out_qty+=$trnsfer_out_qty; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php $issue_bal=$issue_qty+$trnsfer_out_qty; echo number_format($issue_bal,2); $tot_issue_bal+=$issue_bal; ?></p></td>
                    <td width="60" align="right"><p><?php $iss_roll=$issue_roll-$trnsfer_out_roll; echo $iss_roll; $tot_issue_roll+=$iss_roll; ?>&nbsp;</p></td>
                    
                    <td width="80" align="right"><p><?php $stock=$rec_bal-$issue_bal; echo number_format($stock,2); $tot_stock+=$stock; ?>&nbsp;</p></td>
                    <td width="60" align="right"><p><?php $stock_roll_qty=$rec_roll-$iss_roll; echo $stock_roll_qty; $tot_stock_roll_qty+=$stock_roll_qty; ?>&nbsp;</p></td>
                    <td width="50" align="center"><p><?php echo $row[csf('rack')]; ?></p></td>
                    <td width="50" align="center"><p><?php echo $row[csf('self')]; ?></p></td>
					<?php
						$daysOnHand = datediff("d",change_date_format($transaction_date_array[$row[csf('prod_id')]]['max_date'],'','',1),change_date_format(date("Y-m-d"),'','',1));
                    ?>
                    <td width="50" align="center"><p><?php echo $daysOnHand; ?></p></td>
                </tr>
			<?php
				$prev_order_id=$row[csf('po_breakdown_id')];
				$i++;
				$grand_tot_rec_qty+=$row[csf('quantity')];
				$grand_tot_transfer_in_qty+=$trnsfer_in_qty;
				$grand_tot_rec_bal+=$rec_bal;
				$grand_tot_rec_roll+=$rec_roll;
				$grand_tot_issue_qty+=$issue_qty;
				$grand_tot_transfer_out_qty+=$trnsfer_out_qty;
				$grand_tot_issue_bal+=$issue_bal;
				$grand_tot_issue_roll+=$iss_roll;
				$grand_tot_stock+=$stock;
				$grand_tot_roll_qty+=$stock_roll_qty;
			}
			//var_dump($trans_arr);
			foreach($trans_arr as $key=>$val)
			{
				$value=explode("_",$val);
				$po_id=$value[0];
				if($po_id==$prev_order_id )
				{
					
					if(!in_array($val,$trans_in_array))
					{
						$trnsfer_in_qty=$transfer_in_arr[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['qty'];
						$trnsfer_in_roll=$transfer_in_arr[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['roll'];
						$issue_qty=$issue_qty_roll_array[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['qty'];
						$issue_roll=$issue_qty_roll_array[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['roll'];

						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						
							$count=explode(',',$value[2]);
							$count_value='';
							foreach ($count as $val)
							{
								if($count_value=='') $count_value=$count_arr[$val]; else $count_value.=",".$count_arr[$val];
							}
						
					?>
						<tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
							<td width="30"><?php echo $i; ?></td>
							<td width=""><p><?php echo $composition_arr[$product_array[$value[1]]['detarmination_id']]; ?></p></td>
							<td width="60"><p><?php echo $product_array[$value[1]]['gsm']; ?></p></td>
							<td width="60"><p><?php echo $product_array[$value[1]]['dia_width']; ?></p></td>
							<td width="60"><p>&nbsp;</p></td>
							<td width="60"><p>&nbsp;</p></td>
							<td width="80"><p>&nbsp;</p></td>
							<td width="80"><p>&nbsp;</p></td>
							<td width="60"><p><?php echo $count_value; ?></p></td> 
							<td width="80"><p><?php echo $brand_arr[$product_array[$value[1]]['brand']]; ?>&nbsp;</p></td>
							<td width="80"><p><?php echo $value[2]; ?></p></td>
							<td width="100"><p>&nbsp;</p></td>
							
							<td width="80" align="right"><p>&nbsp;</p></td>
							<td width="80" align="right"><p><?php echo number_format($trnsfer_in_qty,2); $tot_transfer_in_qty+=$trnsfer_in_qty; ?>&nbsp;</p></td>
							<td width="80" align="right"><p><?php $rec_bal=$trnsfer_in_qty; echo number_format($rec_bal,2); $tot_rec_bal+=$rec_bal; ?>&nbsp;</p></td>
							<td width="60" align="right"><p><?php $rec_roll=$trnsfer_in_roll; echo $rec_roll; $tot_rec_roll+=$rec_roll; ?>&nbsp;</p></td> 
							
							<td width="80" align="right"><p><?php echo number_format($issue_qty,2); $tot_issue_qty+=$issue_qty; ?>&nbsp;</p></td>
							<td width="80" align="right"><p><?php echo number_format($trnsfer_out_qty,2); $tot_transfer_out_qty+=$trnsfer_out_qty; ?>&nbsp;</p></td>
							<td width="80" align="right"><p><?php $issue_bal=$issue_qty+$trnsfer_out_qty; echo number_format($issue_bal,2); $tot_issue_bal+=$issue_bal; ?></p></td>
							<td width="60" align="right"><p><?php $iss_roll=$issue_roll-$trnsfer_out_roll; echo $iss_roll; $tot_issue_roll+=$iss_roll; ?>&nbsp;</p></td>
							
							<td width="80" align="right"><p><?php $stock=$rec_bal-$issue_bal; echo number_format($stock,2); $tot_stock+=$stock; ?>&nbsp;</p></td>
							<td width="60" align="right"><p><?php $stock_roll_qty=$rec_roll-$iss_roll; echo $stock_roll_qty; $tot_stock_roll_qty+=$stock_roll_qty; ?>&nbsp;</p></td>
							<td width="50" align="center"><p><?php echo $value[4]; ?></p></td>
							<td width="50" align="center"><p><?php echo $value[5]; ?></p></td>
							<?php
								$daysOnHand = datediff("d",change_date_format($transaction_date_array[$value[1]]['max_date'],'','',1),change_date_format(date("Y-m-d"),'','',1));
							?>
							<td width="50" align="center"><p><?php echo $daysOnHand; ?></p></td>
						</tr>
					<?php	
					$i++;
					}
				}
				$grand_tot_transfer_in_qty+=$trnsfer_in_qty;
				$grand_tot_rec_bal+=$rec_bal;
				$grand_tot_rec_roll+=$rec_roll;
				$grand_tot_issue_qty+=$issue_qty;
				$grand_tot_transfer_out_qty+=$trnsfer_out_qty;
				$grand_tot_issue_bal+=$issue_bal;
				$grand_tot_issue_roll+=$iss_roll;
				$grand_tot_stock+=$stock;
				$grand_tot_roll_qty+=$stock_roll_qty;
			}
			
			if (count($nameArray)==0)
			{
				foreach($trans_arr as $key=>$val)
				{
					$value=explode("_",$val);
					$po_id=$value[0];
					
					$trnsfer_in_qty=$transfer_in_arr[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['qty'];
					$trnsfer_in_roll=$transfer_in_arr[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['roll'];
					$issue_qty=$issue_qty_roll_array[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['qty'];
					$issue_roll=$issue_qty_roll_array[$po_id][$value[1]][$value[2]][$value[3]][$value[4]][$value[5]]['roll'];
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
					<tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
						<td width="30"><?php echo $i; ?></td>
						<td width=""><p><?php echo $composition_arr[$product_array[$value[1]]['detarmination_id']]; ?></p></td>
						<td width="60"><p><?php echo $product_array[$value[1]]['gsm']; ?></p></td>
						<td width="60"><p><?php echo $product_array[$value[1]]['dia_width']; ?></p></td>
						<td width="60"><p>&nbsp;</p></td>
						<td width="60"><p>&nbsp;</p></td>
						<td width="80"><p>&nbsp;</p></td>
						<td width="80"><p>&nbsp;</p></td>
						<td width="60"><p><?php echo $count_arr[$value[2]]; ?></p></td> 
						<td width="80"><p><?php echo $brand_arr[$product_array[$value[1]]['brand']]; ?>&nbsp;</p></td>
						<td width="80"><p><?php echo $value[2]; ?></p></td>
						<td width="100"><p>&nbsp;</p></td>
						
						<td width="80" align="right"><p>&nbsp;</p></td>
						<td width="80" align="right"><p><?php echo number_format($trnsfer_in_qty,2); $tot_transfer_in_qty+=$trnsfer_in_qty; ?>&nbsp;</p></td>
						<td width="80" align="right"><p><?php $rec_bal=$trnsfer_in_qty; echo number_format($rec_bal,2); $tot_rec_bal+=$rec_bal; ?>&nbsp;</p></td>
						<td width="60" align="right"><p><?php $rec_roll=$trnsfer_in_roll; echo $rec_roll; $tot_rec_roll+=$rec_roll; ?>&nbsp;</p></td> 
						
						<td width="80" align="right"><p><?php echo number_format($issue_qty,2); $tot_issue_qty+=$issue_qty; ?>&nbsp;</p></td>
						<td width="80" align="right"><p><?php echo number_format($trnsfer_out_qty,2); $tot_transfer_out_qty+=$trnsfer_out_qty; ?>&nbsp;</p></td>
						<td width="80" align="right"><p><?php $issue_bal=$issue_qty+$trnsfer_out_qty; echo number_format($issue_bal,2); $tot_issue_bal+=$issue_bal; ?></p></td>
						<td width="60" align="right"><p><?php $iss_roll=$issue_roll-$trnsfer_out_roll; echo $iss_roll; $tot_issue_roll+=$iss_roll; ?>&nbsp;</p></td>
						
						<td width="80" align="right"><p><?php $stock=$rec_bal-$issue_bal; echo number_format($stock,2); $tot_stock+=$stock; ?>&nbsp;</p></td>
						<td width="60" align="right"><p><?php $stock_roll_qty=$rec_roll-$iss_roll; echo $stock_roll_qty; $tot_stock_roll_qty+=$stock_roll_qty; ?>&nbsp;</p></td>
						<td width="50" align="center"><p><?php echo $value[4]; ?></p></td>
						<td width="50" align="center"><p><?php echo $value[5]; ?></p></td>
						<?php
							$daysOnHand = datediff("d",change_date_format($transaction_date_array[$value[1]]['max_date'],'','',1),change_date_format(date("Y-m-d"),'','',1));
						?>
						<td width="50" align="center"><p><?php echo $daysOnHand; ?></p></td>
					</tr>
					<?php	
				$i++;
				}
				$grand_tot_transfer_in_qty+=$trnsfer_in_qty;
				$grand_tot_rec_bal+=$rec_bal;
				$grand_tot_rec_roll+=$rec_roll;
				$grand_tot_issue_qty+=$issue_qty;
				$grand_tot_transfer_out_qty+=$trnsfer_out_qty;
				$grand_tot_issue_bal+=$issue_bal;
				$grand_tot_issue_roll+=$iss_roll;
				$grand_tot_stock+=$stock;
				$grand_tot_roll_qty+=$stock_roll_qty;
			}
			?>
            <tr class="tbl_bottom">
                <td colspan="12" align="right"><b>Order Total</b></td>
                <td align="right"><?php echo number_format($tot_rec_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_transfer_in_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_rec_bal,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo $tot_rec_roll; ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_issue_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_transfer_out_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_issue_bal,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo $tot_issue_roll; ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_stock,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo $tot_stock_roll_qty; ?>&nbsp;</td>
                <td align="right">&nbsp;</td>
                <td align="right">&nbsp;</td>
                <td align="right">&nbsp;</td>
            </tr>
        <tfoot>
            <tr >
                <th colspan="12" align="right"><b>Grand Total</b></th>
                <th align="right"><?php echo number_format($grand_tot_rec_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_transfer_in_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_rec_bal,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo $grand_tot_rec_roll; ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_issue_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_transfer_out_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_issue_bal,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo $grand_tot_issue_roll; ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_stock,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo $grand_tot_roll_qty; ?>&nbsp;</th>
                <th align="right">&nbsp;</th>
                <th align="right">&nbsp;</th>
                <th align="right">&nbsp;</th>
            </tr>
        </tfoot>
    </table>
    </div>
    </fieldset>
	<?php
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html####$filename"; 
    exit();
}
