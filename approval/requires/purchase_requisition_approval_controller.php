<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=$_SESSION['page_permission'];

include('../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$menu_id=$_SESSION['menu_id'];

$company_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');

if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$sequence_no='';
	$company_name=str_replace("'","",$cbo_company_name);
	if(str_replace("'","",$cbo_item_category_id)==0) $item_category_id="%%"; else $item_category_id=str_replace("'","",$cbo_item_category_id);
	$approval_type=str_replace("'","",$cbo_approval_type);
	//$user_id=7;
	$user_sequence_no=return_field_value("sequence_no","electronic_approval_setup","page_id=$menu_id and user_id=$user_id");
	$min_sequence_no=return_field_value("min(sequence_no)","electronic_approval_setup","page_id=$menu_id");
	
	if($approval_type==0)
	{
		if($user_sequence_no==$min_sequence_no)
		{
			$sql="select id, company_id, requ_no, item_category_id, requisition_date, delivery_date, 0 as approval_id from inv_purchase_requisition_mst where company_id=$company_name and item_category_id like '$item_category_id' and is_approved=$approval_type and status_active=1 and is_deleted=0 order by item_category_id, id";
		}
		else
		{
			
			$sequence_no=return_field_value("max(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no<$user_sequence_no and bypass=2");
			$user_sequence_no=$user_sequence_no-1;
			
			if($sequence_no==$user_sequence_no) $sequence_no_by_pass='';
			else
			{
			    if($db_type==0) $group_concat="group_concat";
				 if($db_type==2 || $db_type==1) $group_concat="wm_concat";
				$sequence_no_by_pass=return_field_value(" $group_concat(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no between $sequence_no and $user_sequence_no and bypass=1");
			}
		
			
			if($sequence_no_by_pass=="") $sequence_no_cond=" and b.sequence_no='$sequence_no'";
			else $sequence_no_cond=" and (b.sequence_no='$sequence_no' or b.sequence_no in ($sequence_no_by_pass))";
			
		
			$sql="select a.id, a.company_id, a.requ_no, a.item_category_id, a.requisition_date, a.delivery_date, b.id as approval_id from inv_purchase_requisition_mst a, approval_history b where a.id=b.mst_id and b.entry_form=1 and a.company_id=$company_name and a.item_category_id like '$item_category_id' and a.status_active=1 and a.is_deleted=0 and b.current_approval_status=1 and a.is_approved=1 $sequence_no_cond order by a.item_category_id, a.id";
		}
	}
	else
	{
		$sequence_no_cond=" and b.sequence_no='$user_sequence_no'";
		$sql="select a.id, a.company_id, a.requ_no, a.item_category_id, a.requisition_date, a.delivery_date, b.id as approval_id from inv_purchase_requisition_mst a, approval_history b where a.id=b.mst_id and b.entry_form=1 and a.company_id=$company_name and a.item_category_id like '$item_category_id' and a.status_active=1 and a.is_deleted=0 and b.current_approval_status=1 and a.is_approved=1 $sequence_no_cond order by a.item_category_id, a.id";
	}
	
	?>
    <form name="requisitionApproval_2" id="requisitionApproval_2">
        <fieldset style="width:650px; margin-top:10px">
        <legend>Purchase Requisition Approval</legend>	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="620" class="rpt_table" >
                <thead>
                	<th width="50"></th>
                    <th width="60">SL</th>
                    <th width="150">Requisition No</th>
                    <th width="120">Item Category</th>
                    <th width="120">Requisition Date</th>
                    <th>Delivery Date</th>
                </thead>
            </table>
            <div style="width:620px; overflow-y:scroll; max-height:330px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="602" class="rpt_table" id="tbl_list_search">
                    <tbody>
                        <?php 
                            $i=1;
                            $nameArray=sql_select( $sql );
                            foreach ($nameArray as $row)
                            {
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$value='';
								if($approval_type==0)
								{
									$value=$row[csf('id')];
								}
								else
								{
								if($db_type==0) 
									{
									$app_id=return_field_value("id","approval_history","mst_id ='".$row[csf('id')]."' and entry_form='1' order by id desc limit 0,1");
									}
								if($db_type==2 || $db_type==1) 
									{
									$app_id=return_field_value("id","approval_history","mst_id ='".$row[csf('id')]."' and entry_form='1' and ROWNUM=1 order by id desc");
									}
									$value=$row[csf('id')]."**".$app_id;
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
                                	<td width="50" align="center" valign="middle">
                                        <input type="checkbox" id="tbl_<?php echo $i;?>" />
                                        <input id="req_id_<?php echo $i;?>" name="req_id[]" type="hidden" value="<?php echo $value; ?>" />
                                        <input id="approval_id_<?php echo $i;?>" name="approval_id[]" type="hidden" value="<?php echo $row[csf('approval_id')]; ?>" />
                                        <input id="requisition_id_<?php echo $i;?>" name="requisition_id[]" type="hidden" value="<?php echo $row[csf('id')]; ?>" /><!--this is uesd for delete row-->
                                    </td>   
									<td width="60" align="center"><?php echo $i; ?></td>
									<td width="150">
                                    	<p><a href='##' style='color:#000' onclick="print_report(<?php echo $row[csf('company_id')]; ?>+'*'+<?php echo $row[csf('id')]; ?>,'purchase_requisition_print', '../inventory/requires/purchase_requisition_controller')">
									<?php echo $row[csf('requ_no')]; ?></a></p>
                                    </td>
                                    <td width="120"><p><?php echo $item_category[$row[csf('item_category_id')]]; ?></p></td>
									<td width="120" align="center"><?php if($row[csf('requisition_date')]!="0000-00-00") echo change_date_format($row[csf('requisition_date')]); ?>&nbsp;</td>
									<td align="center"><?php if($row[csf('delivery_date')]!="0000-00-00") echo change_date_format($row[csf('delivery_date')]); ?>&nbsp;</td>
								</tr>
								<?php
								$i++;
							}
                        ?>
                    </tbody>
                </table>
            </div>
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="620" class="rpt_table">
				<tfoot>
                    <td width="50" align="center" ><input type="checkbox" id="all_check" onclick="check_all('all_check')" /></td>
                    <td colspan="2" align="left"><input type="button" value="<?php if($approval_type==1) echo "Un-Approve"; else echo "Approve"; ?>" class="formbutton" style="width:100px" onclick="submit_approved(<?php echo $i; ?>,<?php echo $approval_type; ?>)"/></td>
				</tfoot>
			</table>
        </fieldset>
    </form>         
<?php
	exit();	
}


if ($action=="approve")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	//$user_id=7;
	$con = connect();
	if($db_type==0)
	{
		mysql_query("BEGIN");
	}
	
	$msg=''; $flag=''; $response='';
	
	$user_sequence_no=return_field_value("sequence_no","electronic_approval_setup","page_id=$menu_id and user_id=$user_id");
	$min_sequence_no=return_field_value("min(sequence_no)","electronic_approval_setup","page_id=$menu_id");
	
	if($approval_type==0)
	{
		$response=$req_nos;
		$field_array="id, entry_form, mst_id, approved_no, sequence_no, current_approval_status, approved_by, approved_date"; 
		$id=return_next_id( "id","approval_history", 1 ) ;
		
	/*	if($user_sequence_no==$min_sequence_no)
		{
			$rID=sql_multirow_update("inv_purchase_requisition_mst","is_approved",1,"id",$req_nos,0);
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$rID=sql_multirow_update("approval_history","current_approval_status",0,"id",$approval_ids,0);
			if($rID) $flag=1; else $flag=0;
		}
		*/
		$i=0; $approved_no_array=array();
		$reqs_ids=explode(",",$req_nos);
		foreach($reqs_ids as $val)
		{
			$approved_no=return_field_value("max(approved_no)","approval_history","mst_id='$val' and entry_form=1");
			if($user_sequence_no==$min_sequence_no) $approved_no=$approved_no+1;
		
			if($i!=0) $data_array.=",";
			
			$data_array.="(".$id.",1,".$val.",".$approved_no.",'".$user_sequence_no."',1,".$user_id.",'".$pc_date_time."')"; 
			
			$approved_no_array[$val]=$approved_no;
				
			$id=$id+1;
			$i++;
		}
		
		//echo "insert into approval_history (".$field_array.") Values ".$data_array."";die;
		/*$rID2=sql_insert("approval_history",$field_array,$data_array,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		*/
		if($user_sequence_no==$min_sequence_no)
		{
			$approved_string="";
			
			foreach($approved_no_array as $key=>$value)
			{
				$approved_string.=" WHEN $key THEN $value";
			}
			
			$approved_string_mst="CASE id ".$approved_string." END";
			$approved_string_dtls="CASE mst_id ".$approved_string." END";
			
			$sql_insert="insert into inv_purchase_rqsn_mst_htry(id, approved_no, reqsn_mst_id, requ_no, requ_no_prefix, requ_prefix_num, company_id, item_category_id, location_id, division_id, department_id, section_id, requisition_date, store_name, pay_mode, source, cbo_currency, delivery_date, remarks, is_approved, inserted_by, insert_date, updated_by, update_date, status_active, is_deleted) 
				select	
				'', $approved_string_mst, id, requ_no, requ_no_prefix, requ_prefix_num, company_id, item_category_id, location_id, division_id, department_id, section_id, requisition_date, store_name, pay_mode, source, cbo_currency, delivery_date, remarks, is_approved, inserted_by, insert_date, updated_by, update_date, status_active, is_deleted from  inv_purchase_requisition_mst where id in ($req_nos)";
					
			/*$rID3=execute_query($sql_insert,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			}*/ 
			
			$sql_insert_dtls="insert into inv_purchase_rqsn_dtls_htry(id, approved_no, mst_id, reqsn_dtls_id, product_id, user_code_maintain, required_for, cons_uom, quantity, rate, amount, stock, inserted_by, insert_date, updated_by, update_date, status_active, is_deleted) 
				select	
				'', $approved_string_dtls, mst_id, id, product_id, user_code_maintain, required_for, cons_uom, quantity, rate, amount, stock, inserted_by, insert_date, updated_by, update_date, status_active, is_deleted from inv_purchase_requisition_dtls where mst_id in ($req_nos)";
			
			
			
		if($user_sequence_no==$min_sequence_no)
			{
				$rID=sql_multirow_update("inv_purchase_requisition_mst","is_approved",1,"id",$req_nos,0);
				if($rID) $flag=1; else $flag=0;
			}
		else
			{
				$rID=sql_multirow_update("approval_history","current_approval_status",0,"id",$approval_ids,0);
				if($rID) $flag=1; else $flag=0;
			}
			
		$rID2=sql_insert("approval_history",$field_array,$data_array,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		$rID3=execute_query($sql_insert,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			}	
			
			
					
			$rID4=execute_query($sql_insert_dtls,1);
			if($flag==1) 
			{
				if($rID4) $flag=1; else $flag=0; 
			} 
		}
		if($flag==1) $msg='19'; else $msg='21';
	}
	else
	{
		
		$req_nos = explode(',',$req_nos); 
		
		$reqs_ids=''; $app_ids='';
		
		foreach($req_nos as $value)
		{
			$data = explode('**',$value);
			$reqs_id=$data[0];
			$app_id=$data[1];
			
			if($reqs_ids=='') $reqs_ids=$reqs_id; else $reqs_ids.=",".$reqs_id;
			if($app_ids=='') $app_ids=$app_id; else $app_ids.=",".$app_id;
		}
		
		$rID=sql_multirow_update("inv_purchase_requisition_mst","is_approved",0,"id",$reqs_ids,0);
		if($rID) $flag=1; else $flag=10;
		
		$rID2=sql_multirow_update("approval_history","current_approval_status",1,"id",$approval_ids,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=20; 
		} 
			
		$data=$user_id."*'".$pc_date_time."'";
		
		$rID3=sql_multirow_update("approval_history","un_approved_by*un_approved_date",$data,"id",$app_ids,1);
		if($flag==1) 
		{
			if($rID3) $flag=1; else $flag=30; 
		} 
		
		$response=$reqs_ids;
		
		if($flag==1) $msg='20'; else $msg='22';
	}
	
	if($db_type==0)
	{ 
		if($flag==1)
		{
			mysql_query("COMMIT");  
			echo $msg."**".$response;
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo $msg."**".$response;
		}
	}
	
	if($db_type==2 || $db_type==1 )
	{
		
		if($flag==1)
		{
			oci_commit($con);
			echo $msg."**".$response;
		}
		else
		{
			oci_rollback($con);
			echo $msg."**".$response;
		}
	}
	disconnect($con);
	die;
	
}

?>