<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id=$_SESSION['logic_erp']['user_id'];


//Company Details
$sql = "SELECT id, company_name, company_short_name, group_id FROM lib_company";
$result = sql_select($sql);
$company_arr = array();
foreach($result as $row)
{
	$company_arr[$row[csf('id')]]['full'] = $row[csf('company_name')];
	$company_arr[$row[csf('id')]]['short'] = $row[csf('company_short_name')];
}

$bank_arr=return_library_array( "select id, bank_name from lib_bank",'id','bank_name');
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');

$lcDataArray=sql_select("SELECT id, export_lc_no, internal_file_no, lien_bank FROM com_export_lc");
$lc_arr = array();
foreach($lcDataArray as $row)
{
	$lc_arr[$row[csf('id')]]['lc'] = $row[csf('export_lc_no')];
	$lc_arr[$row[csf('id')]]['file'] = $row[csf('internal_file_no')];
	$lc_arr[$row[csf('id')]]['lien_bank'] = $row[csf('lien_bank')];
}

$scDataArray=sql_select("SELECT id, contract_no, internal_file_no, lien_bank FROM com_sales_contract");
$sc_arr = array();
foreach($scDataArray as $row)
{
	$sc_arr[$row[csf('id')]]['sc'] = $row[csf('contract_no')];
	$sc_arr[$row[csf('id')]]['file'] = $row[csf('internal_file_no')];
	$sc_arr[$row[csf('id')]]['lien_bank'] = $row[csf('lien_bank')];
}

$invoiceDataArray=sql_select("SELECT id, invoice_no, net_invo_value, invoice_date, ex_factory_date, is_lc, lc_sc_id FROM com_export_invoice_ship_mst");
$invoice_arr = array();
foreach($invoiceDataArray as $row)
{
	$invoice_arr[$row[csf('id')]]['no'] = $row[csf('invoice_no')];
	$invoice_arr[$row[csf('id')]]['inv_date'] = $row[csf('invoice_date')];
	$invoice_arr[$row[csf('id')]]['exf_date'] = $row[csf('ex_factory_date')];
	$invoice_arr[$row[csf('id')]]['value'] = $row[csf('net_invo_value')];
	$invoice_arr[$row[csf('id')]]['is_lc'] = $row[csf('is_lc')];
	$invoice_arr[$row[csf('id')]]['lc_sc_id'] = $row[csf('lc_sc_id')];
}

$realizeDataArray=sql_select("select d.invoice_bill_id, d.received_date, sum(CASE WHEN e.type=0 THEN e.document_currency else 0 END) AS shortrealized,
									sum(CASE WHEN e.type=1 THEN e.document_currency else 0 END) AS realized
									from com_export_proceed_realization d, com_export_proceed_rlzn_dtls e where d.id=e.mst_id and d.is_invoice_bill=1 and d.status_active=1 and d.is_deleted=0 and e.status_active=1 and e.is_deleted=0 group by d.invoice_bill_id,d.received_date");
$realize_arr = array();
foreach($realizeDataArray as $row)
{
	$realize_arr[$row[csf('invoice_bill_id')]]['date'] = $row[csf('received_date')];
	$realize_arr[$row[csf('invoice_bill_id')]]['realized'] = $row[csf('realized')];
	$realize_arr[$row[csf('invoice_bill_id')]]['shortrealized'] = $row[csf('shortrealized')];
}

$purchase_amnt_arr=return_library_array( "select doc_submission_mst_id, sum(lc_sc_curr) as purchase_amnt from com_export_doc_sub_trans where status_active=1 and is_deleted=0 group by doc_submission_mst_id",'doc_submission_mst_id','purchase_amnt');

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}

$tmplte=explode("**",$data);

if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;

if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if(str_replace("'","",$cbo_lien_bank)==0) $lien_bank_id="%%"; else $lien_bank_id=str_replace("'","",$cbo_lien_bank);
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_id=$cbo_buyer_name";
	}
	
	$based_on=array(0=>"Invoice Date",1=>"Submission Date",2=>"Purchase Date",3=>"Realization Date",4=>"Ex-Factory Date");
	
	if($template==1)
	{
		ob_start();
	?>
        <div style="width:2030px;">
            <fieldset style="width:100%;">	 
                <table width="2000" cellpadding="0" cellspacing="0" id="caption">
                    <tr>
                       <td align="center" width="100%" colspan="20" class="form_caption" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_name)]['full']; ?></strong></td>
                    </tr> 
                    <tr>  
                       <td align="center" width="100%" colspan="20" class="form_caption" style="font-size:16px"><strong><?php echo $report_title; ?></strong></td>
                    </tr>  
                    <tr> 
                       <td align="center" width="100%" colspan="20" class="form_caption" style="font-size:16px"><strong><?php echo "Based On: ".$based_on[str_replace("'","",$cbo_based)]; ?></strong></td>
                    </tr>
                </table>
                <br />
                <table width="2000" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <thead>
                        <tr>
                            <th width="50" rowspan="2">Sl</th>
                            <th width="80" rowspan="2">Company</th>
                            <th width="100" rowspan="2">File No.</th>
                            <th width="120" rowspan="2">LC/SC No.</th>
                            <th width="100" rowspan="2">Invoice No.</th>
                            <th width="90" rowspan="2"><?php if(str_replace("'","",$cbo_based)==4) echo $based_on[str_replace("'","",$cbo_based)]; else echo "Invoice Date"; ?></th>
                            <th width="110" rowspan="2">Invoice Value</th>
                            <th width="90" rowspan="2">Doc Sub Date</th>
                            <th width="100" rowspan="2">Bill/FDBC  No.</th>
                            <th width="100">Collection</th>
                            <th width="390" colspan="4"> Sub. Under Purchase</th>
                            <th width="300" colspan="3">Realized</th>
                            <th width="100" rowspan="2">Balance</th>
                            <th width="110" rowspan="2">Buyer</th>
                            <th rowspan="2">Lien Bank</th>
                        </tr>
                        <tr>
                            <th width="100">Amount</th>
                            <th width="120">Bill Amount</th>
                            <th width="110">Purchase Amount</th>
                            <th width="80">(%)</th>
                            <th width="80">Purchase Date</th>
                            <th width="120">Amount</th>
                            <th width="80">Date</th>
                            <th width="100">Short Realization</th>
                        </tr>
                    </thead>
                </table>
                <div style="width:2020px; max-height:300px; overflow-y:scroll" id="scroll_body">
   		 			<table width="2000" class="rpt_table" cellspacing="0" cellpadding="0" border="1" rules="all">
                    <?php
						if(str_replace("'","",$cbo_based)==0) $search_field="c.invoice_date";
						else if(str_replace("'","",$cbo_based)==1) $search_field="a.submit_date";
						else if(str_replace("'","",$cbo_based)==2) $search_field="a.negotiation_date";
						else if(str_replace("'","",$cbo_based)==3) $search_field="d.received_date";
						else if(str_replace("'","",$cbo_based)==4) $search_field="c.ex_factory_date";
						if($db_type==0)
						{
							if(str_replace("'","",$cbo_based)==3)
							{
								$sql="select a.id, a.company_id, a.buyer_id, a.lien_bank, a.submit_date, a.negotiation_date, a.bank_ref_no, group_concat(distinct(b.invoice_id)) as invoice_id, c.is_lc, c.lc_sc_id, 
										sum(case when a.submit_type=1 then b.net_invo_value else 0 end) as doc_collection,
										sum(case when a.submit_type=2 then b.net_invo_value else 0 end) as bill_amt
									from com_export_doc_submission_mst a, com_export_doc_submission_invo b, com_export_invoice_ship_mst c, com_export_proceed_realization d where a.id=b.doc_submission_mst_id and b.invoice_id=c.id and a.company_id=$cbo_company_name and a.lien_bank like '$lien_bank_id' and a.id=d.invoice_bill_id and d.is_invoice_bill=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and d.status_active=1 and d.is_deleted=0 and $search_field between $txt_date_from and $txt_date_to $buyer_id_cond group by a.id";
							}
							else
							{
								$sql="select a.id, a.submit_type, a.company_id, a.buyer_id, a.lien_bank, a.submit_date, a.negotiation_date, a.bank_ref_no, group_concat(distinct(b.invoice_id)) as invoice_id, c.is_lc, c.lc_sc_id, 
										sum(case when a.submit_type=1 then b.net_invo_value else 0 end) as doc_collection,
										sum(case when a.submit_type=2 then b.net_invo_value else 0 end) as bill_amt
									from com_export_doc_submission_mst a, com_export_doc_submission_invo b, com_export_invoice_ship_mst c where a.id=b.doc_submission_mst_id and b.invoice_id=c.id and a.company_id=$cbo_company_name and a.lien_bank like '$lien_bank_id' and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and $search_field between $txt_date_from and $txt_date_to $buyer_id_cond group by a.id";
							}
						}
						else if($db_type==2)
						{
							if(str_replace("'","",$cbo_based)==3)
							{
								$sql="select a.id, a.company_id, a.buyer_id, a.lien_bank, a.submit_date, a.negotiation_date, a.bank_ref_no, LISTAGG(CAST(b.invoice_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id, c.is_lc, c.lc_sc_id, 
										sum(case when a.submit_type=1 then b.net_invo_value else 0 end) as doc_collection,
										sum(case when a.submit_type=2 then b.net_invo_value else 0 end) as bill_amt
									from com_export_doc_submission_mst a, com_export_doc_submission_invo b, com_export_invoice_ship_mst c, com_export_proceed_realization d where a.id=b.doc_submission_mst_id and b.invoice_id=c.id and a.company_id=$cbo_company_name and a.lien_bank like '$lien_bank_id' and a.id=d.invoice_bill_id and d.is_invoice_bill=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and d.status_active=1 and d.is_deleted=0 and $search_field between $txt_date_from and $txt_date_to $buyer_id_cond group by a.id, a.company_id, a.buyer_id, a.lien_bank, a.submit_date, a.negotiation_date, a.bank_ref_no,c.is_lc, c.lc_sc_id";
							}
							else
							{
								$sql="select a.id, a.submit_type, a.company_id, a.buyer_id, a.lien_bank, a.submit_date, a.negotiation_date, a.bank_ref_no, LISTAGG(CAST(b.invoice_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id, c.is_lc, c.lc_sc_id, 
										sum(case when a.submit_type=1 then b.net_invo_value else 0 end) as doc_collection,
										sum(case when a.submit_type=2 then b.net_invo_value else 0 end) as bill_amt
									from com_export_doc_submission_mst a, com_export_doc_submission_invo b, com_export_invoice_ship_mst c where a.id=b.doc_submission_mst_id and b.invoice_id=c.id and a.company_id=$cbo_company_name and a.lien_bank like '$lien_bank_id' and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and $search_field between $txt_date_from and $txt_date_to $buyer_id_cond group by a.id, a.submit_type, a.company_id, a.buyer_id, a.lien_bank, a.submit_date, a.negotiation_date, a.bank_ref_no, c.is_lc, c.lc_sc_id";
							}
						}
						//echo $sql;
						$result=sql_select($sql);
						$i=1; $invoice_id_array=array();
						$total_inv_value=0; $total_doc_collection=0; $total_bill_amt=0; $total_purchase_amt=0; $total_realized_amt=0; $total_short_ship=0; $total_balance=0;
						foreach($result as $row)
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							
							$file_no='';
							if($row[csf('is_lc')]==1) $file_no=$lc_arr[$row[csf('lc_sc_id')]]['file']; else $file_no=$sc_arr[$row[csf('lc_sc_id')]]['file'];
						?>
                        	<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                            	<td width="50"><?php echo $i;?></td>
                                <td width="80"><?php echo $company_arr[$row[csf('company_id')]]['short']; ?></td>
                                <td width="100"><p>&nbsp;<?php echo $file_no; ?></p></td>
                                <td width="423">
                                	<table width="100%" cellspacing="0" cellpadding="0" border="0" rules="all">
                                    <?php
										$all_invoice_id=explode(",",$row[csf('invoice_id')]);
										$all_invoice_id=array_unique($all_invoice_id);
										$s=1; $sub_inv_value=0;
										foreach($all_invoice_id as $invoice_id)
										{
											if($s==1) $top_style="border-top:none"; else $top_style="";
											
											$is_lc_sc=$invoice_arr[$invoice_id]['is_lc'];
											
											$lc_sc_no='';
											
											if($is_lc_sc==1) 
												$lc_sc_no=$lc_arr[$invoice_arr[$invoice_id]['lc_sc_id']]['lc']; 
											else 
												$lc_sc_no=$sc_arr[$invoice_arr[$invoice_id]['lc_sc_id']]['sc']; 
											
											if(!in_array($invoice_id,$invoice_id_array))
											{
												$invoice_id_array[]=$invoice_id;
											}
										?>
                                        	<tr>
                                            	<td width="120" style="border-left:none; border-bottom:none;<?php echo $top_style; ?>"><p><?php echo $lc_sc_no; ?></p></td>
                                                <td width="100" style="border-bottom:none;<?php echo $top_style; ?>"><p><?php echo $invoice_arr[$invoice_id]['no']; ?></p></td>
                                                <td width="90" align="center" style="border-bottom:none;<?php echo $top_style; ?>">
													<?php 
														if(str_replace("'","",$cbo_based)==4)
															echo change_date_format($invoice_arr[$invoice_id]['exf_date']); 
														else 
															echo change_date_format($invoice_arr[$invoice_id]['inv_date']); 	
													?>
                                                </td>
                                                <td style="border-right:none; border-bottom:none;<?php echo $top_style; ?>" align="right">
                                                	<?php
														echo number_format($invoice_arr[$invoice_id]['value'],2,'.','');
													?>
                                                </td>
                                            </tr>
                                        <?php
											$s++;
											$sub_inv_value+=$invoice_arr[$invoice_id]['value'];
										}
									?>
                                    </table>
                                </td>
                                <td width="90" align="center"><?php echo change_date_format($row[csf('submit_date')]); ?>&nbsp;</td>
                                <td width="100"><p><?php echo $row[csf('bank_ref_no')]; ?>&nbsp;</p></td>
                                <td width="100" align="right"><?php echo number_format($row[csf('doc_collection')],2,'.',''); ?>&nbsp;</td>
                                <td width="120" align="right"><?php echo number_format($row[csf('bill_amt')],2,'.',''); ?>&nbsp;</td>
                                <?php
								if(!in_array($row[csf('bank_ref_no')],$temp_array))
								{
									$temp_array[]=$row[csf('bank_ref_no')];
									?>
                                    <td width="110" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$row[csf('id')]."'"  ?>,'purchase_popup_qnty')"><?php echo number_format($purchase_amnt_arr[$row[csf('id')]],2,'.',''); $total_purchase_amt += $purchase_amnt_arr[$row[csf('id')]];  ?></a></td>
                                    <td width="80" align="right">
                                        <?php
                                            $purcase_perc = ($purchase_amnt_arr[$row[csf('id')]]/$row[csf('bill_amt')])*100;
                                            echo number_format($purcase_perc,2,'.','')."&nbsp;";
                                        ?>
                                    </td>
                                    <td width="80" align="center"><?php if($row[csf('negotiation_date')]!="0000-00-00" && $row[csf('negotiation_date')]!="") echo change_date_format($row[csf('negotiation_date')]); ?>&nbsp;</td>
                                    <td width="120" align="right"><?php echo number_format($realize_arr[$row[csf('id')]]['realized'],2,'.',''); $total_realized_amt += $realize_arr[$row[csf('id')]]['realized']; ?>&nbsp;</td>
                                    <td width="80" align="center"><?php if($realize_arr[$row[csf('id')]]['date']!="0000-00-00" && $realize_arr[$row[csf('id')]]['date']!="") echo change_date_format($realize_arr[$row[csf('id')]]['date']); ?>&nbsp;</td>
                                    <td width="100" align="right"><?php echo number_format($realize_arr[$row[csf('id')]]['shortrealized'],2,'.',''); $total_short_ship += $realize_arr[$row[csf('id')]]['shortrealized']; ?>&nbsp;</td>
                                    
                                    <?php
								}
								else
								{
									?>
                                    <td width="110" align="right">&nbsp;</td>
                                    <td width="80" align="right">
                                        <?php
                                            $purcase_perc = ($purchase_amnt_arr[$row[csf('id')]]/$row[csf('bill_amt')])*100;
                                            echo number_format($purcase_perc,2,'.','')."&nbsp;";
                                        ?>
                                    </td>
                                    <td width="80" align="center"><?php if($row[csf('negotiation_date')]!="0000-00-00" && $row[csf('negotiation_date')]!="") echo change_date_format($row[csf('negotiation_date')]); ?>&nbsp;</td>
                                    <td width="120" align="right">&nbsp;</td>
                                    <td width="80" align="center"><?php if($realize_arr[$row[csf('id')]]['date']!="0000-00-00" && $realize_arr[$row[csf('id')]]['date']!="") echo change_date_format($realize_arr[$row[csf('id')]]['date']); ?>&nbsp;</td>
                                    <td width="100" align="right">&nbsp;</td>
                                    
                                    <?php
								}
								?>
                                <td width="100" align="right">
									<?php
                                        if($row[csf('doc_collection')]>0)
										{
                                            $balance = $row[csf('doc_collection')]-($realize_arr[$row[csf('id')]]['realized']+$realize_arr[$row[csf('id')]]['shortrealized']);
                                        }
										else
										{
                                        	$balance = $row[csf('bill_amt')]-($realize_arr[$row[csf('id')]]['realized']+$realize_arr[$row[csf('id')]]['shortrealized']);
                                        }
                                        echo number_format($balance,2)."&nbsp;";
                                     ?>
                                </td>
                                <td width="110"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                                <td><p><?php echo $bank_arr[$row[csf('lien_bank')]]; ?></p></td>
                            </tr>
                        <?php
							$i++;	
							
							$total_inv_value += $sub_inv_value;
							$total_doc_collection += $row[csf('doc_collection')];
							$total_bill_amt += $row[csf('bill_amt')];
							$total_balance += $balance;
						}
						?>
                        <tfoot>
                            <th colspan="3" align="right"><b>Total</b></th>
                            <th width="423">
                                <table width="100%" rules="all" class="rpt_table">
                                	<tr>
                                		<td style="border-top:none" width="100%" colspan="4" align="right"><?php echo number_format($total_inv_value,2);?></td>
                                    </tr>
                            	</table>
                            </th>
                            <th colspan="2">&nbsp;</th>
                            <th align="right"><?php echo number_format($total_doc_collection,2); ?></th>
                            <th align="right"><?php echo number_format($total_bill_amt,2); ?></th>
                            <th align="right"><?php echo number_format($total_purchase_amt,2); ?></th> 
                            <th colspan="2">&nbsp;</th> 				
                            <th align="right"><?php echo number_format($total_realized_amt,2); ?></th>
                            <th>&nbsp;</th>
                            <th align="right"><?php echo number_format($total_short_ship,2); ?></th>
                            <th align="right"><?php echo number_format($total_balance,2); ?></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>	
                        </tfoot>	
                	</table>
                </div>
                <br />
                <?php
				if(str_replace("'","",$cbo_based)==0 || str_replace("'","",$cbo_based)==4)
				{
					if(str_replace("'","",$cbo_based)==0) $search_field="a.invoice_date";
					else if(str_replace("'","",$cbo_based)==4) $search_field="a.ex_factory_date";

					if(count($invoice_id_array)>0) $invoice_id_cond=" and a.id not in (".implode(",",$invoice_id_array).")"; else $invoice_id_cond="";
				?>
                	<b>Un-Submitted Invoice</b>
					<table width="900" border="1" cellspacing="0" cellpadding="0" class="rpt_table" rules="all">
						<thead>
							<th width="100">Company</th>
							<th width="130">File No</th>
							<th width="130">LC/SC No</th>
							<th width="130">Invoice No</th>
							<th width="100"><?php echo $based_on[str_replace("'","",$cbo_based)]; ?></th>
							<th width="140">Invoice Value</th>
							<th>Lien Bank</th>
						</thead>
					</table>
                    <div style="width:920px; max-height:300px; overflow-y:scroll" id="scroll_body_bottom">
                    	<table width="900" border="1" cellspacing="0" cellpadding="0" class="rpt_table" rules="all">
					<?php
                        $query="SELECT a.id, a.benificiary_id, a.invoice_no, a.net_invo_value, a.invoice_date, a.ex_factory_date, a.is_lc, a.lc_sc_id FROM com_export_invoice_ship_mst a where a.benificiary_id=$cbo_company_name and a.is_deleted=0 and a.status_active=1 and $search_field between $txt_date_from and $txt_date_to $buyer_id_cond $invoice_id_cond";
						$cbo_lien_bank=str_replace("'","",$cbo_lien_bank); $tot_unsub_invoice_value=0;
					    $dataArray=sql_select($query);
					    foreach($dataArray as $row_uns)
					    {
							$file_no=''; $lc_sc_no=''; $lien_bank='';
							if($row_uns[csf('is_lc')]==1)
							{
								$file_no=$lc_arr[$row_uns[csf('lc_sc_id')]]['file'];
								$lc_sc_no=$lc_arr[$row_uns[csf('lc_sc_id')]]['lc'];
								$lien_bank=$lc_arr[$row_uns[csf('lc_sc_id')]]['lien_bank']; 
							}
							else 
							{
								$file_no=$sc_arr[$row_uns[csf('lc_sc_id')]]['file'];
								$lc_sc_no=$sc_arr[$row_uns[csf('lc_sc_id')]]['sc']; 
								$lien_bank=$sc_arr[$row_uns[csf('lc_sc_id')]]['lien_bank'];
							}
							
							if($cbo_lien_bank==0 || $cbo_lien_bank==$lien_bank)
							{
							?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
									<td width="100"><?php echo $company_arr[$row_uns[csf('benificiary_id')]]['short']; ?></td>
									<td width="130"><p>&nbsp;<?php echo $file_no; ?></p></td>
									<td width="130"><p>&nbsp;<?php echo $lc_sc_no; ?></p></td>
									<td width="130"><p><?php echo $row_uns[csf('invoice_no')]; ?>&nbsp;</p></td>
									<td width="100" align="center">
										<?php
											if(str_replace("'","",$cbo_based)==4)
												echo change_date_format($row_uns[csf('ex_factory_date')]); 
											else 
												echo change_date_format($row_uns[csf('invoice_date')]); 
										?>
									</td>
									<td width="140" align="right"><?php echo number_format($row_uns[csf('net_invo_value')],2,'.',''); ?>&nbsp;</td>
									<td><p>&nbsp;<?php echo $bank_arr[$lien_bank]; ?></p></td>
								</tr>
							<?php
								$i++;  
								$tot_unsub_invoice_value+=$row_uns[csf('net_invo_value')];
							}
						}
                    	?>
                        <tfoot>
                        	<tr>
                            	<th colspan="5" align="right">Total</th>
                                <th align="right"><?php echo number_format($tot_unsub_invoice_value,2); ?></th>
                                <th>&nbsp;</th>
                            </tr>
                            <tr>
                            	<th colspan="5" align="right">Grand Total</th>
                                <th align="right"><?php echo number_format($total_inv_value+$tot_unsub_invoice_value,2); ?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>   
				<?php
				}
				else
				{
					echo '<div style="display:none" id="scroll_body_bottom"></div>';
				}
				?>
            </fieldset>
        </div>
	<?php
	}
	
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data****$filename";
	exit();
disconnect($con);
}


if($action=="purchase_popup_qnty")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $id;die;//company_id
	
	$sql="select a.id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, sum(b.net_invo_value) as tot_inv_value, b.is_lc, b.lc_sc_id from  com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.submit_type=2 and a.id='$id' and a.status_active=1 and a.is_deleted=0 group by a.id , a.buyer_id, a.bank_ref_no, a.bank_ref_date, b.is_lc, b.lc_sc_id ";
	if($db_type==0)
	{
		$sql="select a.id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, sum(b.net_invo_value) as tot_inv_value, max(b.is_lc) as is_lc, group_concat(distinct b.lc_sc_id) as lc_sc_id from  com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.submit_type=2 and a.id='$id' and a.status_active=1 and a.is_deleted=0 group by a.id , a.buyer_id, a.bank_ref_no, a.bank_ref_date";
	}
	else if($db_type==2)
	{
		$sql="select a.id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, sum(b.net_invo_value) as tot_inv_value, max(b.is_lc) as is_lc,LISTAGG(CAST(b.lc_sc_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.lc_sc_id) lc_sc_id from  com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.submit_type=2 and a.id='$id' and a.status_active=1 and a.is_deleted=0 group by a.id , a.buyer_id, a.bank_ref_no, a.bank_ref_date";
	}
	//echo $sql;
	
	$dataArray=sql_select($sql);//$dataArray[0][csf('bank_ref_no')]
	$buyer_library=return_library_array( "select id,buyer_name from  lib_buyer", "id", "buyer_name"  );
	$lc_library=return_library_array( "select id,export_lc_no from  com_export_lc", "id", "export_lc_no"  );
	$sc_library=return_library_array( "select id,contract_no from  com_sales_contract", "id", "contract_no"  );
	?>
<script>

	function print_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	
		d.close();
		document.getElementById('scroll_body').style.overflowY="scroll";
		document.getElementById('scroll_body').style.maxHeight="none";
	}	
	
</script>	
	<div style="width:667px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
	<fieldset style="width:670px; margin-left:7px" >
	<div id="report_container" align="left">
    <table border="1" class="rpt_table" rules="all" width="650px" align="left">
        <thead>
            <tr>
                <th width="100">Buyer</th>
                <th width="100">Bill No</th>
                <th width="80">Bill Date</th>
                <th width="100">Bill Value</th>
                <th width="100">LC No.</th>
            </tr>
            <tr>
                <th width="100"><?php echo $buyer_library[$dataArray[0][csf('buyer_id')]]; ?></th>
                <th width="100"><?php echo $dataArray[0][csf('bank_ref_no')]; ?></th>
                <th width="80"><?php echo change_date_format($dataArray[0][csf('bank_ref_date')]); ?></th>
                <th width="120"><?php echo number_format($dataArray[0][csf('tot_inv_value')],2); ?></th>
                <th width="100"><p>
				<?php 
					if ($dataArray[0][csf('is_lc')]==1)
					{
						$lc_no_arr=array_unique(explode(",",$dataArray[0][csf('lc_sc_id')]));
						$lc_all="";
						foreach($lc_no_arr as $lc_id)
						{
							if($lc_all=="") $lc_all=$lc_library[$lc_id]; else $lc_all=$lc_all.", ".$lc_library[$lc_id];
						}
					}
					else
					{
						$sc_no_arr=array_unique(explode(",",$dataArray[0][csf('lc_sc_id')]));
						$lc_all="";
						foreach($sc_no_arr as $sc_id)
						{
							if($lc_all=="") $lc_all=$sc_library[$sc_id]; else $lc_all=$lc_all.", ".$sc_library[$sc_id];
						}
					}
					echo $lc_all;
					//echo $lc_library[$dataArray[0][csf('lc_sc_id')]]; else echo $sc_library[$dataArray[0][csf('lc_sc_id')]]; 
				?></p>
                </th>
           </tr>
        </thead>
    </table>
    <br /> <br /> <br />
    <?php
	$mst_id=$dataArray[0][csf('id')];
	//$dtls_sql="select a.id, a.acc_head, a.acc_loan, a.dom_curr, a.conver_rate, a.lc_sc_curr, b.net_invo_value from  com_export_doc_sub_trans a, com_export_doc_submission_invo b where a.doc_submission_mst_id='$mst_id' and b.doc_submission_mst_id='$mst_id' and a.status_active=1 and a.is_deleted=0 group by a.acc_head";
	$dtls_sql="select a.id, a.acc_head, a.acc_loan, sum(a.dom_curr) as dom_curr, a.conver_rate, sum(a.lc_sc_curr) as lc_sc_curr from  com_export_doc_sub_trans a where a.doc_submission_mst_id='$id' and a.status_active=1 and a.is_deleted=0 group by a.acc_head, a.id, a.acc_loan, a.conver_rate";
	//echo $dtls_sql;
	$dtls_sql_result=sql_select($dtls_sql);//$dataArray[0][csf('bank_ref_no')]
    ?>
    <div style="width:667px; overflow-y:scroll" id="scroll_body" align="left">
    <table cellspacing="0" width="650"  border="1" rules="all" class="rpt_table" >
        <thead>
            <th width="30">SL</th>
            <th width="" >Account Head</th>
            <th width="100" >Purchase Amount</th>
            <th width="80" >Pur.%(USD)</th>
            <th width="80" >Con. Rate</th>
            <th width="80" >BDT Amount</th>
        </thead>
		<?php
        $i=1;
		foreach($dtls_sql_result as $row)
		{
			if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
			$pur_per=($row[csf('lc_sc_curr')]/$row[csf('net_invo_value')])*100;
			$bdt_amount=$row[csf('lc_sc_curr')]*$row[csf('conver_rate')];
			?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
				<td><?php echo $i;  ?></td>
				<td><?php echo $commercial_head[$row[csf('acc_head')]]; ?></td>
				<td align="right"><?php echo number_format($row[csf('lc_sc_curr')],2); ?></td>
                <td align="right"><?php $pur_per=($row[csf('lc_sc_curr')]/$dataArray[0][csf('tot_inv_value')])*100; echo number_format($pur_per,4); //if($pur_per>0) echo "%"; ?></td>
                <td align="right"><?php echo number_format($row[csf('conver_rate')],3); ?></td>
                <td align="right"><?php echo number_format($bdt_amount,2); ?></td>
			</tr>
			<?php
			$tot_pur_qnty+=$row[csf('lc_sc_curr')];
			$tot_bdt_amount+=$bdt_amount;
			$i++;
		}
		?>
		<tfoot>
			<tr>
				<th colspan="2" align="right"><strong>Total :</strong></th>
				<th align="right"><?php echo number_format($tot_pur_qnty,2); ?></th>
				<th align="right"><?php $total_percent=($tot_pur_qnty/$dataArray[0][csf('tot_inv_value')])*100; echo number_format($total_percent,4); ?></th>
				<th align="right"><?php echo number_format($tot_bdt_amount,2); ?></th>
                <th align="right">&nbsp;</th>
			</tr>
		</tfoot>                           
	</table>
	</div>
    </div>
    </fieldset>
	<?php
    exit();	
}
?>
