﻿<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Yarn Stock Ledger
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	24-08-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Daily Yarn Stock","../../../", 1, 1, $unicode,1,1); 

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";



function openmypage_item()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/daily_yarn_stock_report_controller.php?action=item_description_search&company='+company; 
	var title="Search Item Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=620px,height=370px,center=1,resize=0,scrolling=0','../../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var prodID=this.contentDoc.getElementById("txt_selected_id").value; // product ID
		var prodDescription=this.contentDoc.getElementById("txt_selected").value; // product Description
 		$("#txt_product").val(prodDescription);
		$("#txt_product_id").val(prodID); 
 	}
}



function generate_report()
{
	if( form_validation('cbo_company_name*txt_date_from*txt_date_to','Company Name*From Date*To Date')==false )
	{
		return;
	}
	 
	var cbo_company_name = $("#cbo_company_name").val();
	var cbo_dyed_type = $("#cbo_dyed_type").val();
	var cbo_yarn_type = $("#cbo_yarn_type").val();
	var txt_count 	= $("#cbo_yarn_count").val();
	var txt_lot_no 	= $("#txt_lot_no").val();
	var from_date 	= $("#txt_date_from").val();
	var to_date 	= $("#txt_date_to").val();
	var store_wise 	= $("#cbo_store_wise").val();
	var store_name 	= $("#cbo_store_name").val();	
	
	var dataString = "&cbo_company_name="+cbo_company_name+"&cbo_dyed_type="+cbo_dyed_type+"&cbo_yarn_type="+cbo_yarn_type+"&txt_count="+txt_count+"&txt_lot_no="+txt_lot_no+"&cbo_value_with="+cbo_value_with+"&from_date="+from_date+"&to_date="+to_date+"&store_wise="+store_wise+"&store_name="+store_name;
 	var data="action=generate_report"+dataString;
	freeze_window(3);
	http.open("POST","requires/daily_yarn_stock_report_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = generate_report_reponse;  
}

function generate_report_reponse()
{	
	if(http.readyState == 4) 
	{	 
 		var reponse=trim(http.responseText).split("**");
		$("#report_container2").html(reponse[0]);  
		document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
		
		var tableFilters = { 
			col_0: "none", 
			col_operation: {
				id: ["value_total_opening_balance","value_total_purchase","value_total_inside_return","value_total_outside_return","value_total_rcv_loan","value_total_total_rcv","value_total_issue_inside","value_total_issue_outside","value_total_receive_return","value_total_issue_loan","value_total_total_delivery","value_total_stock_in_hand","value_total_alocatted","value_total_free_stock"],
				col: [8,9,10,11,12,13,14,15,16,17,18,19,20,21],
				operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
				write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
			}
		}
		setFilterGrid("table_body",-1,tableFilters);
		
		show_msg('3');
		release_freezing();
	}
} 
 

function new_window()
{
	 
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none"; 
		
		$("#table_body tr:first").hide();
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print" /><style></style></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');
		d.close(); 
		document.getElementById('scroll_body').style.overflow="auto"; 
		document.getElementById('scroll_body').style.maxHeight="250px";
		
		$("#table_body tr:first").show();
}


</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../../../",$permission);  ?><br />    		 
    <form name="stock_ledger_1" id="stock_ledger_1" autocomplete="off" > 
    <div style="width:100%;" align="center">
		<fieldset style="width:1150px;">
        <legend>Search Panel</legend> 
			<table class="rpt_table" width="1150" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <tr> 	 	
                        <th width="140" class="must_entry_caption">Company</th>                                
                        <th width="100">Dyed Type</th>
                        <th width="100">Yarn Type</th>
                        <th width="130">Count</th>
                        <th width="70">Lot</th>
                        <th width="110">Value</th>
                        <th class="must_entry_caption">Date</th>
                        <th width="75" class="must_entry_caption">Store Wise</th>
                        <th width="120">Store Name</th>
                        <th width="100"><input type="reset" name="res" id="res" value="Reset" style="width:80px" class="formbutton" /></th>
                    </tr>
                </thead>
                <tr class="general">
                    <td>
                            <?php 
                               echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/daily_yarn_stock_report_controller', this.value+'**'+document.getElementById('cbo_store_wise').value, 'load_drop_down_store', 'store_td' );" );
                            ?>                            
                    </td>
                    <td align="center">
                        <?php   
							$dyedType=array(0=>'All',1=>'Dyed Yarn',2=>'Non Dyed Yarn');
							echo create_drop_down( "cbo_dyed_type", 100, $dyedType,"", 0, "--Select--", $selected, "", "","");
						?>              
                    </td>
                    <td> 
						<?php
                            echo create_drop_down( "cbo_yarn_type", 100, $yarn_type,"", 1, "--Select--", 0, "",0 );
                        ?>
                    </td>
                    <td>
                    	<?php
							echo create_drop_down("cbo_yarn_count",130,"select id,yarn_count from lib_yarn_count where is_deleted=0 and status_active=1 order by yarn_count","id,yarn_count",0, "-- Select --", $selected, "");
						?>
                    	<!--<input type="text" id="txt_count" name="txt_count" class="text_boxes" style="width:60px" value="" />-->	
                    </td>
                    <td>
                    	<input type="text" id="txt_lot_no" name="txt_lot_no" class="text_boxes" style="width:60px" value="" />
                    </td>
                    <td>
						<?php   
							$valueWithArr=array(0=>'Value With 0',1=>'Value Without 0');
                            echo create_drop_down( "cbo_value_with", 110, $valueWithArr, "", 0, "--Select--", $selected, "", "", "");
                        ?>
                    </td>
                    <td align="center">
                         <input type="text" name="txt_date_from" id="txt_date_from" value="<?php echo date("d-m-Y", time() - 86400);?>" class="datepicker" style="width:70px"/>
                         To
                         <input type="text" name="txt_date_to" id="txt_date_to" value="<?php echo date("d-m-Y", time() - 86400);?>" class="datepicker" style="width:70px"/>                                                        
                    </td>
                    <td> 
						<?php
                            echo create_drop_down( "cbo_store_wise", 70, $yes_no,"", 0, "--Select--", 2, "load_drop_down( 'requires/daily_yarn_stock_report_controller', document.getElementById('cbo_company_name').value+'**'+this.value, 'load_drop_down_store', 'store_td' );",0 );
                        ?>
                    </td>
                    <td id="store_td">
						<?php 
                        	echo create_drop_down( "cbo_store_name", 110, $blank_array,"", 1, "-- All Store --", $storeName, "",1 );
                        ?>
                    </td>
                    <td>
                        <input type="button" name="search" id="search" value="Show" onClick="generate_report()" style="width:80px" class="formbutton" />
                    </td>
                </tr>
                <tr>
                    <td colspan="10" align="center"><?php echo load_month_buttons(1);  ?></td>
                </tr>
            </table> 
		</fieldset> 
    </div>
    <br /> 
    
        <!-- Result Contain Start-------------------------------------------------------------------->
         
        	<div id="report_container" align="center"></div>
            <div id="report_container2"></div> 
        
        <!-- Result Contain END-------------------------------------------------------------------->
    
    
    </form>    
</div>    
</body> 
<script>
	set_multiselect('cbo_yarn_count','0','0','','0');
</script> 
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
