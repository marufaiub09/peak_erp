<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will create for Export Proceed Realization
					
Functionality	:	
				

JS Functions	:

Created by		:	Fuad Shahriar	 
Creation date 	: 	04-06-2013
Updated by 		: 			
Update date		: 	Jahid	   	   

QC Performed BY	:		

QC Date			:	

Comments		: according to requirment of Sayed bhai such as validation change (entry search popup & Invoice no popup) & Store value for next (deducttion &distribution) convertion rate.

*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Export Proceeds Realization Form", "../../", 1, 1,'','1','');
?>	
 
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission='<?php echo $permission; ?>';

function openmypage_realizationUpdate()
{
	var buyerID = $("#cbo_buyer_name").val();
	var beneficiary_name = $("#cbo_beneficiary_name").val();
	
	if (form_validation('cbo_beneficiary_name','Company')==false )
	{
		return;
	}
	//alert(buyerID);
	var page_link='requires/export_proceed_realization_controller.php?action=proceed_realization_popup_search&beneficiary_name='+beneficiary_name+'&buyerID='+buyerID;
	var title='Export Proceeds Realization Entry Form';
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=400px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var realization_id=this.contentDoc.getElementById("hidden_realization_id").value;
		var invoice_bill_id=this.contentDoc.getElementById("hidden_invoice_bill_id").value;
		var is_invoiceBill=this.contentDoc.getElementById("hidden_is_invoiceBill").value;
		
		if(trim(realization_id)!="")
		{
			freeze_window(5);
			reset_form('exportProceedRealizationFrm_1','','','deduction_tot_row,1*distribution_tot_row,1','$(\'#tbl_deduction tbody tr:not(:first)\').remove();$(\'#tbl_distribution tbody tr:not(:first)\').remove();','cbo_beneficiary_name*cbo_buyer_name');
			get_php_form_data(invoice_bill_id+"**"+is_invoiceBill+"**"+realization_id, "populate_data_from_invoice_bill", "requires/export_proceed_realization_controller" );
			
			var submit_type=$('#submit_type').val();
			
			show_list_view( realization_id+"**0", 'details_list_view', 'deduction_list_view', 'requires/export_proceed_realization_controller', '' ) ;
			show_list_view( realization_id+"**1**"+submit_type, 'details_list_view', 'distribution_list_view', 'requires/export_proceed_realization_controller', '' ) ;	
			
			var deduction_tot_row = $('#tbl_deduction tbody tr').length; 
			var distribution_tot_row = $('#tbl_distribution tbody tr').length; 
			
			$('#deduction_tot_row').val(deduction_tot_row);
			$('#distribution_tot_row').val(distribution_tot_row);
			
			calculate_total('tbl_deduction','deduction');
			calculate_total('tbl_distribution','distribution');
			calculate_grand_total();			
			release_freezing();
		}
					 
	}
}

function openmypage_InvoiceBill()
{
	var buyerID = $("#cbo_buyer_name").val();
	var beneficiary_name = $("#cbo_beneficiary_name").val();
	
	if (form_validation('cbo_beneficiary_name','Beneficiary')==false )
	{
		return;
	}
	
	var page_link='requires/export_proceed_realization_controller.php?action=invoice_bill_popup_search&beneficiary_name='+beneficiary_name+'&buyerID='+buyerID;
	var title='Export Proceeds Realization Entry Form';
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=400px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var invoice_bill_id=this.contentDoc.getElementById("hidden_invoice_bill_id").value;
		var is_invoiceBill=this.contentDoc.getElementById("hidden_is_invoiceBill").value;

		if(trim(invoice_bill_id)!="")
		{
			freeze_window(5);
			reset_form('exportProceedRealizationFrm_1','','','deduction_tot_row,1*distribution_tot_row,1','$(\'#tbl_deduction tbody tr:not(:first)\').remove();$(\'#tbl_distribution tbody tr:not(:first)\').remove();','cbo_beneficiary_name*cbo_buyer_name');
			get_php_form_data(invoice_bill_id+"**"+is_invoiceBill, "populate_data_from_invoice_bill", "requires/export_proceed_realization_controller" );
			calculate_total('tbl_distribution','distribution');
			calculate_grand_total();
			release_freezing();
		}
	}
}

function fnc_export_proceed_realization(operation) 
{ 
	if(operation==2)
	{
		show_msg('13');
		return;
	}
		
	if ( form_validation('cbo_beneficiary_name*cbo_buyer_name*txt_invoice_bill_no*txt_received_date','Beneficiary*Buyer*Bill/Invoice No*Received Date')==false )
	{
		return;
	}
	else
	{
		//var deduction_tot_row=$('#deduction_tot_row').val(); var distribution_tot_row=$('#distribution_tot_row').val();
		
		var dataString_deduction=""; var dataString_distribution="";
		var j=0; var z=0; var tot_row=0;
		$("#tbl_deduction").find('tbody tr').each(function()
		{
			var cbodeductionHead=$(this).find('select[name="cbodeductionHead[]"]').val();
			
			var deductionDocumentCurrency=$(this).find('input[name="deductionDocumentCurrency[]"]').val();
			var deductionConversionRate=$(this).find('input[name="deductionConversionRate[]"]').val();
			var deductionDomesticCurrency=$(this).find('input[name="deductionDomesticCurrency[]"]').val();
			
			if(cbodeductionHead==0)
			{
				//alert("please Select Deductions Account Head");		
				$(this).find('select[name="cbodeductionHead[]"]').focus();		
				return;
			}
			else
			{
				j++;
				tot_row++;
				
				dataString_deduction+='&cbodeductionHead_' + j + '=' + cbodeductionHead + '&deductionDocumentCurrency_' + j + '=' + deductionDocumentCurrency + '&deductionConversionRate_' + j + '=' + deductionConversionRate+ '&deductionDomesticCurrency_' + j + '=' + deductionDomesticCurrency;
			}
		});
		
		$("#tbl_distribution").find('tbody tr').each(function()
		{
			var cbodistributionHead=$(this).find('select[name="cbodistributionHead[]"]').val();
			var acLoanNo=$(this).find('input[name="acLoanNo[]"]').val();
			var distributionDocumentCurrency=$(this).find('input[name="distributionDocumentCurrency[]"]').val();
			var distributionConversionRate=$(this).find('input[name="distributionConversionRate[]"]').val();
			var distributionDomesticCurrency=$(this).find('input[name="distributionDomesticCurrency[]"]').val();
			
			if(cbodistributionHead==0)
			{
				//alert("please Select Distributions Account Head");		
				$(this).find('select[name="cbodistributionHead[]"]').focus();		
				return;
			}
			else
			{
				z++;
				tot_row++;
				
				dataString_distribution+='&cbodistributionHead_' + z + '=' + cbodistributionHead + '&acLoanNo_' + z + '=' + acLoanNo + '&distributionDocumentCurrency_' + z + '=' + distributionDocumentCurrency + '&distributionConversionRate_' + z + '=' + distributionConversionRate+ '&distributionDomesticCurrency_' + z + '=' + distributionDomesticCurrency;
			}
		});
		
		if(tot_row<1)
		{
			alert("No Deductions or Distributions Data Insert");	
			return;
		}
		
		var data="action=save_update_delete&operation="+operation+'&deduction_tot_row='+j+'&distribution_tot_row='+z+get_submitted_data_string('cbo_beneficiary_name*cbo_buyer_name*txt_invoice_bill_no*invoice_bill_id*is_invoice_bill*txt_received_date*txt_remarks*update_id',"../../")+dataString_deduction+dataString_distribution;
		//alert(data);return;
		freeze_window(operation);
		
		http.open("POST","requires/export_proceed_realization_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange =fnc_export_proceed_realization_Reply_info;
	}	

}

function fnc_export_proceed_realization_Reply_info()
{
	if(http.readyState == 4) 
	{
		// alert(http.responseText);
		var reponse=trim(http.responseText).split('**');	
			
		show_msg(trim(reponse[0]));
		
		if((reponse[0]==0 || reponse[0]==1))
		{
			document.getElementById('update_id').value = reponse[1];
			
			var submit_type=$('#submit_type').val();
			show_list_view( reponse[1]+"**0", 'details_list_view', 'deduction_list_view', 'requires/export_proceed_realization_controller', '' ) ;
			show_list_view( reponse[1]+"**1**"+submit_type, 'details_list_view', 'distribution_list_view', 'requires/export_proceed_realization_controller', '' ) ;	
			
			var deduction_tot_row = $('#tbl_deduction tbody tr').length; 
			var distribution_tot_row = $('#tbl_distribution tbody tr').length; 
			
			$('#deduction_tot_row').val(deduction_tot_row);
			$('#distribution_tot_row').val(distribution_tot_row);
			
			calculate_total('tbl_deduction','deduction');
			calculate_total('tbl_distribution','distribution');
			calculate_grand_total();
			set_button_status(1, permission, 'fnc_export_proceed_realization',1);	
		}
		release_freezing();	
	}
}


 // function :: calculate rate, discount, amount         
function calculate(i,field_id,table_id,prefix)
{
	var DocumentCurrency= $('#'+prefix+'DocumentCurrency_'+i).val()*1; 
	var ConversionRate= $('#'+prefix+'ConversionRate_'+i).val()*1;
	var DomesticCurrency= $('#'+prefix+'DomesticCurrency_'+i).val()*1;
	
	if(field_id=="DocumentCurrency_")
	{
		if(ConversionRate!="" && DomesticCurrency!="")
		{
			var DomsCurr=DocumentCurrency*ConversionRate;
			$('#'+prefix+'DomesticCurrency_'+i).val(DomsCurr.toFixed(2));
		}
		else if(ConversionRate=="" && DomesticCurrency!="")
		{
			var ConvRate=DomesticCurrency/DocumentCurrency;
			$('#'+prefix+'ConversionRate_'+i).val(ConvRate.toFixed(4));
		}
		else if(ConversionRate!="" && DomesticCurrency=="")
		{
			var DomsCurr=DocumentCurrency*ConversionRate;
			$('#'+prefix+'DomesticCurrency_'+i).val(DomsCurr.toFixed(2));
		}
		
	}
	else if(field_id=="ConversionRate_")
	{
		if(DocumentCurrency!="" && DomesticCurrency!="")
		{
			var DomsCurr=DocumentCurrency*ConversionRate;
			$('#'+prefix+'DomesticCurrency_'+i).val(DomsCurr.toFixed(2));
		}
		else if(DocumentCurrency=="" && DomesticCurrency!="")
		{
			var DocCurr=DomesticCurrency/ConversionRate;
			$('#'+prefix+'DocumentCurrency_'+i).val(DocCurr.toFixed(4));
		}
		else if(DocumentCurrency!="" && DomesticCurrency=="")
		{
			var DomsCurr=DocumentCurrency*ConversionRate;
			$('#'+prefix+'DomesticCurrency_'+i).val(DomsCurr.toFixed(2));
		}
		
	}
	else if(field_id=="DomesticCurrency_")
	{
		if(DocumentCurrency!="" && ConversionRate!="")
		{
			var DocCurr=DomesticCurrency/ConversionRate;
			$('#'+prefix+'DocumentCurrency_'+i).val(DocCurr.toFixed(2));
		}
		else if(DocumentCurrency=="" && ConversionRate!="")
		{
			var DocCurr=DomesticCurrency*ConversionRate;
			$('#'+prefix+'DocumentCurrency_'+i).val(DocCurr.toFixed(2));
		}
		else if(DocumentCurrency!="" && ConversionRate=="")
		{
			var ConvRate=DocumentCurrency/DocumentCurrency;
			$('#'+prefix+'ConversionRate_'+i).val(ConvRate.toFixed(4));
		}
		
	}
	/*if(field_id=="deductionDocumentCurrency_")
	{
		if(deductionDocumentCurrency!="" && deductionConversionRate!="")
		{
			var deductionDomsCurr=deductionDocumentCurrency*deductionConversionRate;
			$('#deductionDomesticCurrency_'+i).val(deductionDomsCurr.toFixed(2));
		}
		else if(deductionDocumentCurrency!="" && deductionDomesticCurrency!="")
		{
			var deductionConvRate=deductionDocumentCurrency/deductionDomesticCurrency;
			$('#deductionConversionRate_'+i).val(deductionConvRate.toFixed(4));
		}
		
	}
	else if(field_id=="deductionConversionRate_")
	{
		if(deductionConversionRate!="" && deductionDocumentCurrency!="")
		{
			var deductionDomsCurr=deductionDocumentCurrency*deductionConversionRate;
			$('#deductionDomesticCurrency_'+i).val(deductionDomsCurr.toFixed(2));
		}
		else if(deductionConversionRate!="" && deductionDomesticCurrency!="")
		{
			var deductionDocCurr=deductionDomesticCurrency/deductionConversionRate;
			$('#deductionDocumentCurrency_'+i).val(deductionDocCurr.toFixed(4));
		}
		
	}
	else if(field_id=="deductionDomesticCurrency_")
	{
		if(deductionDomesticCurrency!="" && deductionConversionRate!="")
		{
			var deductionDocCurr=deductionDomesticCurrency/deductionConversionRate;
			$('#deductionDocumentCurrency_'+i).val(deductionDocCurr.toFixed(2));
		}
		else if(deductionDomesticCurrency!="" && deductionDocumentCurrency!="")
		{
			var deductionConvRate=deductionDocumentCurrency/deductionDomesticCurrency;
			$('#deductionConversionRate_'+i).val(deductionConvRate.toFixed(4));
		}
	}*/
	
	
	calculate_total(table_id,prefix);
	calculate_grand_total();
}

function calculate_total(table_id,prefix)
{
	var total_document_currency=0; var total_domestic_currency=0;
	$("#"+table_id).find('tbody tr').each(function()
	{
		var DocumentCurrency=$(this).find('input[name="'+prefix+'DocumentCurrency[]"]').val()*1;
		var DomesticCurrency=$(this).find('input[name="'+prefix+'DomesticCurrency[]"]').val()*1;

		total_document_currency=(total_document_currency*1)+DocumentCurrency;
		total_domestic_currency=(total_domestic_currency*1)+DomesticCurrency;
		
		$('#total_'+prefix+'_document_currency').val(total_document_currency);
		$('#total_'+prefix+'_domestic_currency').val(total_domestic_currency);
	});

}

function calculate_grand_total()
{
	var total_deduction_document_currency= $('#total_deduction_document_currency').val()*1;
	var total_deduction_domestic_currency= $('#total_deduction_domestic_currency').val()*1;
	var total_distribution_document_currency= $('#total_distribution_document_currency').val()*1;
	var total_distribution_domestic_currency= $('#total_distribution_domestic_currency').val()*1;
	
	var grand_total_document_currency=total_deduction_document_currency+total_distribution_document_currency;
	var grand_total_domestic_currency=total_deduction_domestic_currency+total_distribution_domestic_currency;
	
	$('#grand_total_document_currency').val(grand_total_document_currency);
	$('#grand_total_domestic_currency').val(grand_total_domestic_currency);
}

function fnc_addRow( i, table_id, tr_id )
{ 
	var prefix=tr_id.substr(0, tr_id.length-1);
	//var row_num=$('#'+table_id+' tbody tr').length;
	var row_num=$('#'+prefix+'_tot_row').val();
	
	if(prefix=="distribution")
	{
		var con_rate_distribute="";
		if(row_num==1)
		{
			con_rate_distribute=$('#distributionConversionRate_1').val();
		}
		else
		{
			con_rate_distribute=$('#distributionConversionRate_'+row_num).val();
		}
	}
	else
	{
		var coversation_rate="";
		if(row_num==1)
		{
			coversation_rate=$('#deductionConversionRate_1').val();
		}
		else
		{
			coversation_rate=$('#deductionConversionRate_'+row_num).val();
		}
	}
	
	row_num++;
	
	var clone= $("#"+tr_id+i).clone();
	clone.attr({
		id: tr_id + row_num,
	});
	
	clone.find("input,select").each(function(){
		  
	$(this).attr({ 
	  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ row_num },
	  'name': function(_, name) { return name },
	  'value': function(_, value) { return value }              
	});
	 
	}).end();
	
	$("#"+tr_id+i).after(clone);
	
	$('#'+prefix+'DocumentCurrency_'+row_num).removeAttr("value").attr("value","");
	$('#'+prefix+'ConversionRate_'+row_num).removeAttr("value").attr("value","");
	$('#'+prefix+'DomesticCurrency_'+row_num).removeAttr("value").attr("value","");
	$('#cbo'+prefix+'Head_'+row_num).val(0);
	
	$('#'+prefix+'DocumentCurrency_'+row_num).removeAttr("onKeyUp").attr("onKeyUp","calculate("+row_num+",'DocumentCurrency_','"+table_id+"','"+prefix+"');");
	$('#'+prefix+'ConversionRate_'+row_num).removeAttr("onKeyUp").attr("onKeyUp","calculate("+row_num+",'ConversionRate_','"+table_id+"','"+prefix+"');");
	$('#'+prefix+'DomesticCurrency_'+row_num).removeAttr("onKeyUp").attr("onKeyUp","calculate("+row_num+",'DomesticCurrency_','"+table_id+"','"+prefix+"');");
	
	if(prefix=="distribution")
	{
		$('#cbodistributionHead_'+row_num).removeAttr("onChange").attr("onChange","get_php_form_data(this.value+'**"+row_num+"**'+$('#cbo_beneficiary_name').val(), 'populate_acc_loan_no_data','requires/export_proceed_realization_controller');check_duplication("+row_num+")");
		$('#cbo'+prefix+'Head_'+row_num).removeAttr("disabled");
		$('#acLoanNo_'+row_num).val('');
		$('#acLoanNo_'+row_num).removeAttr("disabled");
		$('#'+prefix+'DocumentCurrency_'+row_num).removeAttr("disabled");
		$('#'+prefix+'ConversionRate_'+row_num).removeAttr("disabled");
		$('#'+prefix+'DomesticCurrency_'+row_num).removeAttr("disabled");
	}
	
	$('#'+prefix+'increase_'+row_num).removeAttr("value").attr("value","+");
	$('#'+prefix+'decrease_'+row_num).removeAttr("value").attr("value","-");
	$('#'+prefix+'increase_'+row_num).removeAttr("onclick").attr("onclick","fnc_addRow("+row_num+",'"+table_id+"','"+tr_id+"');");
	$('#'+prefix+'decrease_'+row_num).removeAttr("onclick").attr("onclick","fnc_deleteRow("+row_num+",'"+table_id+"','"+tr_id+"');");
	
	$('#'+prefix+'_tot_row').val(row_num);
	set_all_onclick();
	
	$('#deductionConversionRate_'+row_num).val(coversation_rate);
	$('#distributionConversionRate_'+row_num).val(con_rate_distribute);
	
}

function fnc_deleteRow(rowNo,table_id,tr_id) 
{ 
	var numRow = $('#'+table_id+' tbody tr').length; 
	var prefix=tr_id.substr(0, tr_id.length-1);
	
	//var isDisbled=$('#'+tr_id+rowNo).find('select[name="'+'cbo'+prefix+'Head[]'+'"]').is(":disabled");// Both Works Perfectly
	var isDisbled=$('#cbo'+prefix+'Head_'+rowNo).is(":disabled");// Both Works Perfectly

	if(numRow!=1 && isDisbled==false)
	{
		$("#"+tr_id+rowNo).remove();
		
		calculate_total(table_id,prefix);
		calculate_grand_total();
	}
}

function check_duplication(rowNo)
{
	var cbodistributionHead=$("#cbodistributionHead_"+rowNo).val();
	var i=0;
	if(cbodistributionHead==1)
	{
		$("#tbl_distribution").find('tbody tr').each(function()
		{
			var distributionHead=$(this).find('select[name="cbodistributionHead[]"]').val();
			if(distributionHead==1)
			{
				i=i+1;
			}
			if(i*1>1)
			{
				alert("Duplicate Account Head For Negotiation");
				$("#cbodistributionHead_"+rowNo).val(0);
				$("#acLoanNo_"+rowNo).val('');
				$("#distributionDocumentCurrency_"+rowNo).val('');
				$("#distributionConversionRate_"+rowNo).val('');
				$("#distributionDomesticCurrency_"+rowNo).val('');
				
				/*$(this).find('select[name="cbodistributionHead[]"]').val(0);
				$(this).find('input[name="acLoanNo[]"]').val('');
				$(this).find('input[name="distributionDocumentCurrency[]"]').val('');
				$(this).find('input[name="distributionConversionRate[]"]').val('');
				$(this).find('input[name="distributionDomesticCurrency[]"]').val('');*/
				return false;
			}
		});	
	}
	
}
	
</script>

</head>
 
<body onLoad="set_hotkey();">
	<div style="width:100%;" align="center">																	
     	<?php echo load_freeze_divs ("../../",$permission); ?>
        <form name="exportProceedRealizationFrm_1" id="exportProceedRealizationFrm_1" autocomplete="off" method="POST"  >
        	<fieldset style="width:850px; margin-bottom:10px;">
            <legend>Export Proceeds Realization Entry</legend>
                <table cellpadding="2" cellspacing="1" width="100%">
                	<tr>
                    	<td colspan="6" align="center"><b>Previous Entry Search: </b> 
                    	    <input type="text" name="search_realization" id="search_realization" class="text_boxes" placeholder="Double click for update" onDblClick="openmypage_realizationUpdate();" readonly />
                        </td>
                    </tr>
                    <tr height="10"><td></td></tr>
                  	<tr>
                    	<td width="110" class="must_entry_caption">Beneficiary</td>
                       	<td> 
                        	<?php
							  echo create_drop_down( "cbo_beneficiary_name", 152, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Beneficiary --", 0, "load_drop_down( 'requires/export_proceed_realization_controller', this.value, 'load_drop_down_buyer', 'buyer_td_id' );",0 );
 							?>
                        </td>
                    	<td width="120" class="must_entry_caption">Buyer</td>
                        <td id="buyer_td_id">
                        	<?php
								echo create_drop_down( "cbo_buyer_name", 152, $blank_array,"", 1, "-- Select Buyer --", 0, "" );
							?>                          
                        </td>
                        <td width="120" class="must_entry_caption">Bill/Invoice No</td>
                    	<td>
                        	<input type="text" name="txt_invoice_bill_no" id="txt_invoice_bill_no" style="width:140px" class="text_boxes" placeholder="Double Click to Search" onDblClick="openmypage_InvoiceBill()" readonly />
                            <input type="hidden" name="invoice_bill_id" id="invoice_bill_id" readonly/>
                            <input type="hidden" name="is_invoice_bill" id="is_invoice_bill" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td class="must_entry_caption">Received Date</td>
                        <td><input name="txt_received_date" style="width:140px" id="txt_received_date" class="datepicker" readonly></td>
                        <td>LC/SC No</td>
                        <td>
                        	<input type="text" name="txt_lc_sc_no" id="txt_lc_sc_no" style="width:140px" class="text_boxes" disabled="disabled" placeholder="Display" />
                        </td>
                        <td>Currency</td>
                        <td>
							<?php
							   	echo create_drop_down( "cbo_currency_name", 152, $currency,"", 1, "Display", 0, "",1 );
							?>
                        </td>
                    </tr>
                    <tr>
                    	<td>Bill/Invoice Date</td>
                        <td>
                            <input type="text" name="txt_bill_invoice_date" id="txt_bill_invoice_date" placeholder="Display" class="datepicker" style="width:140px" disabled="disabled" />
                        </td>
						<td>
                            Bill/Invoice Amount
                        </td>
                        <td>
                            <input type="text" name="txt_bill_invoice_amnt" id="txt_bill_invoice_amnt" placeholder="Display" class="text_boxes_numeric" style="width:140px;" disabled="disabled" />
                        </td>
                        <td>Negotiated Amount</td>
						<td>
                        	<input type="text" name="txt_negotiated_amount" id="txt_negotiated_amount" placeholder="Display" class="text_boxes_numeric" style="width:140px;" disabled="disabled" />
                            <input type="hidden" name="submit_type" id="submit_type" value="" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td>Remarks</td>
                        <td colspan="5"><input name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:712px"></td>
                    </tr>
                </table>
			</fieldset>   
            <br>
			<fieldset style="width:850px;">
				<legend>Deductions at Source</legend>
				<table id="tbl_deduction" width="100%" border="0" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
					<thead>						
                        <th class="must_entry_caption">Account Head</th>
                        <th>Document Currency</th>
                        <th>Conversion Rate</th>
                        <th>Domestic Currency</th>
                        <th></th>						
					</thead>
					<tbody id="deduction_list_view">
						<tr class="general" id='deduction_1' align="center">
							<td>
                            	<select class="combo_boxes" id="cbodeductionHead_1" name="cbodeductionHead[]" style="width:172px">
                                	<option value="0">-- Select Account Head --</option>
										<?php
											asort($commercial_head);
											foreach($commercial_head as $key=>$value)
											{
											?>
                                            	<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php	
											}
											//echo create_drop_down( "cbodeductionHead_1", 172, $commercial_head,"", 1, "-- Select Account Head --", 0, "" );
                                        ?>
                                </select>
							</td>
							<td>
                            	<input type="text" name="deductionDocumentCurrency[]" id="deductionDocumentCurrency_1" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(1,'DocumentCurrency_','tbl_deduction','deduction')"/>
                            </td>
							<td>
                            	<input type="text" name="deductionConversionRate[]" id="deductionConversionRate_1" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(1,'ConversionRate_','tbl_deduction','deduction')"/>
                            </td>
							<td>
                            	<input type="text" name="deductionDomesticCurrency[]" id="deductionDomesticCurrency_1" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(1,'DomesticCurrency_','tbl_deduction','deduction')" />
                            </td>
                            <td width="65">
                                <input type="button" id="deductionincrease_1" name="deductionincrease[]" style="width:30px" class="formbuttonplasminus" value="+" onClick="fnc_addRow(1,'tbl_deduction','deduction_')" />
                                <input type="button" id="deductiondecrease_1" name="deductiondecrease[]" style="width:30px" class="formbuttonplasminus" value="-" onClick="fnc_deleteRow(1,'tbl_deduction','deduction_');" />
                            </td>
						</tr>
					</tbody>
                    <tfoot class="tbl_bottom">
                    	<tr>
                        	<td>Total</td>
                            <td><input style="width:177px;" type="text" class="text_boxes_numeric" id="total_deduction_document_currency" disabled /></td>
                            <td></td>
                            <td><input style="width:177px;" type="text" class="text_boxes_numeric" id="total_deduction_domestic_currency" disabled /></td>
                        </tr>
                    </tfoot>
				</table>
			</fieldset> 
            <br>  
            <fieldset style="width:850px;">
				<legend>Distributions</legend>
				<table id="tbl_distribution" width="100%" border="0" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
					<thead>						
                        <th class="must_entry_caption">Account Head</th>
                        <th>AC / Loan No</th>
                        <th>Document Currency</th>
                        <th>Conversion Rate</th>
                        <th>Domestic Currency</th>
                        <th></th>						
					</thead>
					<tbody id="distribution_list_view">
						<tr class="general" id='distribution_1' align="center">
							<td> 
                            	<select class="combo_boxes" name="cbodistributionHead[]" id="cbodistributionHead_1" onChange="get_php_form_data( this.value+'**1**'+$('#cbo_beneficiary_name').val(), 'populate_acc_loan_no_data', 'requires/export_proceed_realization_controller' );check_duplication(1);" style="width:172px">
                                	<option value="0">-- Select Account Head --</option>
										<?php
											foreach($commercial_head as $key=>$value)
											{
											?>
                                            	<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php	
											}
											//echo create_drop_down( "cbodistributionHead_1", 172, $commercial_head,"", 1, "-- Select Account Head --", 0, "get_php_form_data( this.value+'**1**'+$('#cbo_beneficiary_name').val(), 'populate_acc_loan_no_data', 'requires/export_proceed_realization_controller' );" );
                                        ?>
                                </select>
							</td>
                            <td>
                            	<input type="text" name="acLoanNo[]" id="acLoanNo_1" class="text_boxes" style="width:130px;" />
                            </td>
							<td>
                            	<input type="text" name="distributionDocumentCurrency[]" id="distributionDocumentCurrency_1" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(1,'DocumentCurrency_','tbl_distribution','distribution')"/>
                            </td>
							<td>
                            	<input type="text" name="distributionConversionRate[]" id="distributionConversionRate_1" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(1,'ConversionRate_','tbl_distribution','distribution')"/>
                            </td>
							<td>
                            	<input type="text" name="distributionDomesticCurrency[]" id="distributionDomesticCurrency_1" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(1,'DomesticCurrency_','tbl_distribution','distribution')" />
                            </td>
                            <td width="65">
                                <input type="button" id="distributionincrease_1" name="increase[]" style="width:30px" class="formbuttonplasminus" value="+" onClick="fnc_addRow(1,'tbl_distribution','distribution_')" />
                                <input type="button" id="distributiondecrease_1" name="decrease[]" style="width:30px" class="formbuttonplasminus" value="-" onClick="fnc_deleteRow(1,'tbl_distribution','distribution_');" />
                            </td>
						</tr>
					</tbody>
                    <tfoot class="tbl_bottom">
                    	<tr>
                        	<td colspan="2">Total</td>
                            <td><input style="width:130px;" type="text" class="text_boxes_numeric" id="total_distribution_document_currency" disabled /></td>
                            <td></td>
                            <td><input style="width:130px;" type="text" class="text_boxes_numeric" id="total_distribution_domestic_currency" disabled /></td>
                        </tr>
                    </tfoot>
				</table>
                <br>
                <table width="850" border="0" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
                    <thead>						
                            <th></th>
                            <th>Document Currency</th>
                            <th>Domestic Currency</th>						
                    </thead>
                    <tfoot class="tbl_bottom">					
                        <tr>
                            <td align="right"><b>Grand Total</b></td>
                            <td><input style="width:180px;" type="text" class="text_boxes_numeric" id="grand_total_document_currency" disabled /></td>
                            <td><input style="width:180px;" type="text" class="text_boxes_numeric" id="grand_total_domestic_currency" disabled /></td>						
                        </tr>
                    </tfoot>
                </table> 
			</fieldset>
            <br>         
            <table width="850">
                <tr>
                    <td colspan="6" height="50" valign="middle" align="center" class="button_container">
                        <?php 
							echo load_submit_buttons( $permission, "fnc_export_proceed_realization", 0,0 ,"reset_form('exportProceedRealizationFrm_1','','','deduction_tot_row,1*distribution_tot_row,1','disable_enable_fields(\'cbodistributionHead_1*acLoanNo_1*distributionDocumentCurrency_1*distributionConversionRate_1*distributionDomesticCurrency_1\',0)');$('#tbl_deduction tbody tr:not(:first)').remove();$('#tbl_distribution tbody tr:not(:first)').remove();",1) ;
                        ?>
                        <input type="hidden" name="update_id" id="update_id" readonly />
                        <input type="hidden" name="deduction_tot_row" id="deduction_tot_row" value="1" readonly />
                        <input type="hidden" name="distribution_tot_row" id="distribution_tot_row" value="1" readonly />
                    </td>
                </tr>
            </table>
		</form>
	</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>