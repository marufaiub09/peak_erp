-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppl_yarn_requisition_entry`
--

DROP TABLE IF EXISTS `ppl_yarn_requisition_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppl_yarn_requisition_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `knit_id` int(11) NOT NULL DEFAULT '0',
  `requisition_no` int(11) NOT NULL DEFAULT '0',
  `prod_id` int(11) NOT NULL DEFAULT '0',
  `no_of_cone` int(11) NOT NULL DEFAULT '0',
  `requisition_date` date NOT NULL,
  `yarn_qnty` double NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Yes 0;No',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Yes 0:No ',
  PRIMARY KEY (`id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`),
  KEY `knit_id` (`knit_id`),
  KEY `requisition_no` (`requisition_no`),
  KEY `prod_id` (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppl_yarn_requisition_entry`
--

LOCK TABLES `ppl_yarn_requisition_entry` WRITE;
/*!40000 ALTER TABLE `ppl_yarn_requisition_entry` DISABLE KEYS */;
INSERT INTO `ppl_yarn_requisition_entry` VALUES (1,3,1,9,0,'0000-00-00',2,2,'2015-05-04 15:20:55',0,'0000-00-00 00:00:00',1,0),(2,5,2,10,0,'2015-05-04',3,2,'2015-05-04 16:42:28',0,'0000-00-00 00:00:00',1,0),(3,6,3,139,0,'2015-05-09',3,2,'2015-05-09 12:04:24',0,'0000-00-00 00:00:00',1,0),(4,7,4,308,0,'2015-05-11',652,17,'2015-05-12 17:58:11',0,'0000-00-00 00:00:00',1,0),(5,8,5,962,1,'2015-05-26',10,2,'2015-05-26 09:50:00',0,'0000-00-00 00:00:00',1,0),(6,9,6,962,0,'2015-05-27',147.48,17,'2015-05-27 16:49:14',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `ppl_yarn_requisition_entry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
