-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sample_ex_factory_dtls`
--

DROP TABLE IF EXISTS `sample_ex_factory_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sample_ex_factory_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_ex_factory_mst_id` int(11) DEFAULT NULL,
  `order_type` int(11) NOT NULL,
  `sample_development_id` int(11) DEFAULT NULL,
  `sample_name` int(11) DEFAULT NULL,
  `ex_factory_qty` int(11) DEFAULT NULL,
  `carton_qty` int(11) DEFAULT NULL,
  `invoice_no` int(11) DEFAULT NULL,
  `lc_sc_id` int(11) DEFAULT NULL,
  `lc_sc_no` varchar(100) DEFAULT NULL,
  `carton_per_qty` int(11) DEFAULT NULL,
  `remarks` text,
  `shiping_status` int(2) DEFAULT NULL,
  `inserted_by` int(11) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sample_ex_factory_dtls`
--

LOCK TABLES `sample_ex_factory_dtls` WRITE;
/*!40000 ALTER TABLE `sample_ex_factory_dtls` DISABLE KEYS */;
/*!40000 ALTER TABLE `sample_ex_factory_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
