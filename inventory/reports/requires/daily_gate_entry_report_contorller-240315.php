﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if ($action=="load_drop_down_supplier")
{	//echo "select a.id,a.supplier_name from lib_supplier a,lib_supplier_tag_company b where a.id=b.supplier_id 	 and a.tag_company=$data order by supplier_name";
  echo create_drop_down( "cbo_supplier", 170, "select a.id,a.supplier_name from lib_supplier a,lib_supplier_tag_company b where a.id=b.supplier_id 	 and b.tag_company=$data order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  

  
	/*echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET($data,tag_company) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 ); */ 	 
	exit();
}

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
$sample_arr=return_library_array(" select id,sample_name from lib_sample where status_active=1 order by sample_name",'id','sample_name');
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
$buyer_name_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');

//style search------------------------------//
if($action=="chalan_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	//echo $style_id;die;

	?>
        
<script>
	function js_set_value(str)
	{
	
		var splitData = str.split("_");	  	 
		$("#hidden_chalan_id").val(splitData[0]); 
		$("#hidden_chalan_no").val(splitData[1]); 
		$("#hidden_search_number").val($("#cbo_search_by").val()); 
	
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th id="search_by_td_up" width="200">Enter Booking No</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php 
						$search_arr=array(1=>"Chalan No",2=>"System ID"); 
						$dd="change_search_event(this.value, '0*0*0*0*0', '0*0*0*0*0', '../../') ";
                            echo create_drop_down( "cbo_search_by", 170, $search_arr,"",1, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                     <td align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>   
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $category; ?>, 'create_chalan_search_list_view', 'search_div', 'daily_gate_entry_report_contorller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_chalan_id" value="" />
                    <input type="hidden" id="hidden_chalan_no" value="" />
                    <input type="hidden" id="hidden_search_number" value="" />
                  
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_chalan_search_list_view")
{
	
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
	$item_category= $ex_data[5];
 	
	$sql_cond="";
	
	if(trim($txt_search_by)==0) { echo "please select Search By";die;}
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for challan
		{
			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";	
		}
		else if(trim($txt_search_by)==2) // for System ID
		{
			$sql_cond .= " and a.sys_number LIKE '%$txt_search_common%'";				
		}
 	}
	
	 if(trim($item_category)!=0) { $sql_cond .= " and b.item_category_id=$item_category"; }
 		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and a.in_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	
	
	//if($txt_search_by==1 )
	//{
 		$sql = "select a.id ,a.challan_no as chalan,a.sys_number_prefix_num,a.in_date,b.sample_id,party_type 	from  inv_gate_in_mst a, inv_gate_in_dtl b 
				where a.id=b.mst_id and a.status_active=1 $sql_cond";
				
				//a.supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
	//}
   
   	/*if($txt_search_by==2)
		{
			$sql = "select id ,sys_number as chalan,supplier_name,receive_date,sample 	
					from  inv_gate_in_mst
					where  status_active=1 $sql_cond";//a.supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
		}*/
	//echo $sql;die;//"select id,sample_name from lib_sample where status_active=1 order by sample_name","id,sample_name"
	$result = sql_select($sql);
  	$party_type_arr=array(1=>"Buyer",2=>"Supplier",3=>"Other Party");
	$sample_arr=return_library_array( "select id,sample_name from lib_sample where status_active=1 order by sample_name",'id','sample_name');
	$arr=array(2=>$party_type_arr,3=>$sample_arr);
	echo  create_list_view("list_view", "Challan No/System ID, Receive Date, Supplier, Sample","150,100,120,150","600","400",0, $sql , "js_set_value", "id,sys_number_prefix_num", "", 1, "0,0,party_type,sample_id", $arr, "sys_number_prefix_num,in_date,party_type,sample_id", "",'','0,3,0,0,0,0') ;		
	exit();	
	
}





//order search------------------------------//
if($action=="pi_search")
{
	
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	
	if($company==0) $company_name=""; else $company_name=" and a.company_id=$company";
	if($category==0) $item_cate=""; else $item_cate="and b.item_category_id=$category";
	
	$sql ="select a.id,a.pi_reference,a.sys_number_prefix_num,a.party_type,a.in_date	
					from  inv_gate_in_mst a,inv_gate_in_dtl b 
					where  a.id=b.mst_id and a.status_active=1 $company_name $item_cate "; 
	$arr=array(1=>$supplier_arr);
	echo create_list_view("list_view", "PI/WO/REQ,Sys No,Receive Date","150,150,100,","450","310",0, $sql , "js_set_value", "id,pi_reference", "", 1, "0,0,0", $arr, "pi_reference,sys_number_prefix_num,in_date", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}

//report generated here--------------------//
if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_item_cat=str_replace("'","",$cbo_item_cat);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_supplier_name=str_replace("'","",$cbo_supplier);
	$txt_pi_no=str_replace("'","",$txt_pi_no);
	$cbo_gate_type=str_replace("'","",$cbo_gate_type);
	$txt_challan=str_replace("'","",$txt_challan);
	//$txt_search_item=str_replace("'","",$txt_search_item);
	$hidden_pi_id=str_replace("'","",$hidden_pi_id);
	//echo $txt_challan;
	
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	
	if($cbo_item_cat!=0) $item_category_cond=" and b.item_category_id=$cbo_item_cat"; else $item_category_cond.="";
    if($cbo_company_name!=0) $company_cond.=" and a.company_id=$cbo_company_name"; else $company_cond.="";
	if(str_replace("'","",$cbo_gate_type)==1)
	  {
		if(str_replace("'","",$txt_challan)!="")  $sql_condition.=" and a.challan_no='".str_replace("'","",$txt_challan)."'"; else $sql_condition.="";
	  }
	if(str_replace("'","",$cbo_gate_type)==2)
	  {
		if(str_replace("'","",$txt_challan)!="")  $sql_condition.=" and a.sys_number='".str_replace("'","",$txt_challan)."'"; else $sql_condition.="";
	  }
    //if($hidden_pi_id!="") $sql_condition.=" and a.id in ($hidden_pi_id)"; else $sql_condition.="";
   // if($hidden_pi_id==""  && $txt_pi_no!="") $sql_condition.=" and a.pi_wo_req_number='$txt_pi_no'"; else $sql_condition.="";
	if($cbo_supplier_name!=0) $supplier_name_cond=" and a.sending_company=$cbo_supplier_name"; else $supplier_name_cond="";
	if($txt_challan!=0) $challan_sys_cond=" and a.sys_number_prefix_num=$txt_challan"; else $challan_sys_cond="";
	//if($txt_challan!=0) $challan_sys_cond=" and a.challan_no=$txt_challan"; else $challan_sys_cond="";
	if($txt_pi_no!=0) $pi_refernce_cond=" and a.pi_reference='$txt_pi_no'"; else $pi_refernce_cond="";
	if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond.=" and a.in_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $date_cond.=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.in_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $date_cond="";
		} 
	//echo $sql_condition;die;
	if($cbo_gate_type==0 || $cbo_gate_type==1)
			{
		 $sql="select a.id,a.sys_number,a.sending_company,a.in_date,a.challan_no,a.gate_pass_no,a.carried_by,a.pi_reference,b.sample_id,	b.item_category_id,	a.party_type,b.item_description,b.quantity,	b.uom,	b.uom_qty,	b.rate,b.amount,b.remarks,a.time_hour,a.time_minute
	from inv_gate_in_mst a,inv_gate_in_dtl b where a.id=b.mst_id   and a.status_active=1 and a.is_deleted=0 $company_cond $challan_sys_cond  $date_cond $item_category_cond $supplier_name_cond $pi_refernce_cond";
	$data_result=sql_select($sql);
			}
		  
	       
		  // print_r($data_detail);
			ob_start();	
			
				   if($cbo_gate_type==0 || $cbo_gate_type==1)
					{
				   
				   if(count($data_result)>0)
					{
					//echo "fffffffffffff";
		   ?>
        <fieldset style="width:1825px;">
				<table width="1825" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="19" align="center" style="border:none;font-size:16px; font-weight:bold" >Daily Gate Entry Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="19" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				 
				<table width="1805" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
					<thead>
						<tr>
							<th width="30">SL</th>
							<th width="100">Receive date</th>
							<th width="120">System ID</th>
							<th width="125">PI/WO/REQ</th>
							<th width="125">Challan No</th>
							<th width="100">Gate Pass No</th>
							<th width="100">Sample Type</th>
							<th width="100">Item Category</th>
                            <th width="140">Description</th>
							<th width="80">Receive Qty</th>
							<th width="60">UOM</th>
                            <th width="80">UOM Qty</th> 
                            <th width="50">Rate</th>  
                            <th width="80">Amount</th> 
                          
                            <th width="120">Supplier</th>
                            <th width="120">Buyer</th>
                           
							<th width="80">In time</th>
                            <th width="80">Carried By</th>
                            <th>Remarks</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1825px; overflow-y: scroll; max-height:400px;" id="scroll_body">
				<table width="1805" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
					<tbody>
					<?php
					$i=1;$total_receive=0;
					
					foreach($data_result as $value)
					{
						
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							$party_type=$value[csf("party_type")];	
							$sending_company=$value[csf("sending_company")];
							if($party_type==1)
							{
								//echo $sending_company;
							$buyer_name=$buyer_name_arr[$sending_company];
							}
							else if($party_type==2)
							{
							$supplier_name=$supplier_arr[$sending_company];
							}
							

							?>
							<tr  bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo change_date_format($value[csf("in_date")]); ?>&nbsp;</p></td>
								<td width="120"  align="center"><p><?php  echo $value[csf("sys_number")]; ?>&nbsp;</p></td>
								<td width="125"  align="center"><p><?php echo $value[csf("pi_reference")]; ?>&nbsp;</p></td>
								<td width="125" align="center"><p><?php echo $value[csf("challan_no")]; ?>&nbsp;</p></td>
                                <td width="100" align="center"><p><?php echo $value[csf("gate_pass_no")]; ?>&nbsp;</p></td>
                                <td width="100" align="center"><p><?php echo $sample_arr[$value[csf("sample_id")]]; ?>&nbsp;</p></td>
								<td width="100" align="center"><p><?php echo $item_category[$value[csf("item_category_id")]]; ?>&nbsp;</p></td>
								<td width="140" align="center"><p><?php echo $value[csf("item_description")]; ?>&nbsp;</p></td>
								<td width="80"  align="right"><p><?php $total_receive+=$value[csf("quantity")]; echo number_format($value[csf("quantity")]); ?>&nbsp;</p></td>
								<td width="60" align="center"><p><?php echo $unit_of_measurement[$value[csf("uom")]]; ?>&nbsp;</p></td>
                                <td width="80" align="center"><p><?php echo number_format($value[csf("uom_qty")]); ?>&nbsp;</p></td>
                                <td width="50" align="center"><p><?php echo number_format($value[csf("rate")]); ?>&nbsp;</p></td>
                                <td width="80" align="center"><p><?php echo number_format($value[csf("amount")]); ?>&nbsp;</p></td>
                              
                                <td width="120"  align="center"><p><?php echo $supplier_name; ?>&nbsp;</p></td>
                                <td width="120"  align="center"><p><?php  echo $buyer_name; ?>&nbsp;</p></td>
                               
								<td width="80" align="center"><p>
								<?php
								    if($value[csf("time_hour")]==24)
									   { 
										  $hour=$value[csf("time_hour")]-12;
										  $am="AM";
									   }
								  else  if($value[csf("time_hour")]==12)
									   { 
										  $hour=$value[csf("time_hour")];
										  $am="PM";
									   }
								  else  if($value[csf("time_hour")]>12 && $value[csf("time_hour")]<24)
									   { 
										  $hour=$value[csf("time_hour")]-12;
										  $am="PM";
									   } 
								   else
								       {
									    $hour=$value[csf("time_hour")];
										$am="AM";
									   }
								// echo $value[csf("time_hour")];
								 echo $hour.":".$value[csf("time_minute")]." ".$am ; ?>&nbsp;</p></td>
                                 
                                 <td width="80"><p><?php echo $value[csf("carried_by")]; ?></p></td>
								
								<td ><p><?php echo $value[csf("remarks")]; ?></p></td>
							</tr>
							<?php
							$total_uom_qty+=$value[csf("uom_qty")];
							$total_amount+=$value[csf("amount")];
							$i++;
						}
					
					?>   
					</tbody>
                    <tfoot>
                    	<th colspan="9" >Total:</th>
                        <th id="value_total_receive"><?php echo number_format($total_receive,0); ?></th>
                        <th id=""></th>
                        <th id=""><?php echo number_format($total_uom_qty,0); ?></th>
                         <th id=""></th>
                        <th id=""><?php echo number_format($total_amount,0); ?></th>
                        <th id=""></th>
                        <th id=""></th>
                        <th id=""></th>
                        <th id=""></th>
                        
                        <th></th>
                    </tfoot>
                    <?php
					
					}
				}// Gate IN End
					
		if($cbo_item_cat!=0) $out_cond=" and b.item_category_id=$cbo_item_cat"; else $out_cond.="";			
	    if($cbo_company_name!=0) $company_cond=" and a.company_id=$cbo_company_name"; else $company_cond="";
		if($txt_challan!=0) $challan_sys_cond=" and a.sys_number_prefix_num=$txt_challan"; else $challan_sys_cond="";
	    if(str_replace("'","",$txt_search_item)==1)
	 		 {
				if(str_replace("'","",$txt_challan)!="")  $out_cond.=" and a.challan_no='".str_replace("'","",$txt_challan)."'"; else $out_cond.="";
	 		 }
		if(str_replace("'","",$txt_search_item)==2)
		 	 {
				if(str_replace("'","",$txt_challan)!="")  $out_cond.=" and a.sys_number='".str_replace("'","",$txt_challan)."'"; else $out_cond.="";
		 	 }
		
		if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $out_cond.=" and a.out_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $out_cond.=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $out_cond="and a.out_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $out_cond="";
		} 
			$sql_data=sql_select(" select out_date, gate_pass_id,out_time  from  inv_gate_out_scan where status_active=1 and is_deleted=0");
			$gate_scan_array=array();
			foreach($sql_data as $row)
			{
				$gate_scan_array[$row[csf('gate_pass_id')]]['out_date']=$row[csf('out_date')];
				$gate_scan_array[$row[csf('gate_pass_id')]]['out_time']=$row[csf('out_time')];
			} 
			$sql_gate=sql_select("select gate_pass_id from  inv_gate_out_scan where  status_active=1 and is_deleted=0");
			$i=1;
			foreach($sql_gate as $row_g)
			{
				if($i!==1) $row_cond.=",";
				$row_cond.=$row_g[csf('gate_pass_id')];
				$i++;
			}
					
			if($cbo_gate_type==0 || $cbo_gate_type==2)
			{
			$sql_out="select a.id,a.sys_number,a.sys_number_prefix_num,a.sent_by,	a.issue_purpose,a.sent_to,a.out_date,a.returnable,a.challan_no,a.carried_by,a.get_pass_no,b.sample_id,b.item_category_id,b.item_description,	b.quantity,b.uom,b.uom_qty,b.remarks,a.time_hour,a.time_minute
						from inv_gate_pass_mst a, inv_gate_pass_dtls b	where a.id=b.mst_id  and a.status_active=1  and a.is_deleted=0 and a.sys_number in(select a.gate_pass_id from inv_gate_out_scan a where  a.status_active=1  and a.is_deleted=0 ) $company_cond $out_cond  $challan_sys_cond";
							//echo $sql_out;
			$get_out_data=sql_select($sql_out);		
			}
					
					?>
				</table>
			 </div>
			<br /><br />
			 <?php
				    if($cbo_gate_type==0 || $cbo_gate_type==2)
					{
				   if(count($get_out_data)>0)
					{
				   
				   
				   ?>
              <fieldset style="width:1620px;">
            <table width="1600" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="17" align="center" style="border:none;font-size:16px; font-weight:bold" >Daily Gate Out Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="17" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				  
				<table width="1600" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_2" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="80" >Out date</th>
                             <th width="120">Gate Pass No</th>
                            <th width="120">Sample Type</th>
                            <th width="120">Item Category</th>
                             <th width="150">Description</th>
                             <th width="80">Quantity</th>
                             <th width="60">UOM</th>
                            <th width="80">UOM Qty</th>
                            <th width="100">Sent By</th>
                            <th width="100">Sent to</th>
							<th width="60" >Returnable</th>
                            <th width="120">Buyer</th>
                            <th width="100" >Purpose</th>
                            <th width="80">Out time</th>
                            <th width="100" >Carried By</th>
                           
							
                            <th>Remarks</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1620px; overflow-y: scroll; max-height:500px;" id="scroll_body" align="left">
				<table width="1600" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body_1" align="left">
					<tbody>
					<?php
		/*
		if($txt_date_from!="" && $txt_date_to!="") $out_cond.=" and a.out_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $out_cond.=""; */
		
					$i=1;$total_out=0;
					//count($get_out_data);
					
					foreach($get_out_data as $val)
					{
						
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							$gate_out_date=$gate_scan_array[$val[csf('sys_number')]]['out_date'];
							$gate_out_time=$gate_scan_array[$val[csf('sys_number')]]['out_time'];
							
							?>
							<tr  bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('trout_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="trout_<?php echo $i; ?>">
								<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
								<td width="80"><p><?php echo change_date_format($gate_out_date); ?>&nbsp;</p></td>
                                
                                <td width="120"><p><?php echo $val[csf("sys_number")]; ?>&nbsp;</p></td>
								<td width="120"><p><?php echo $sample_arr[$val[csf("sample_id")]]; ?>&nbsp;</p></td>
								<td width="120"><p><?php echo $item_category[$val[csf("item_category_id")]]; ?>&nbsp;</p></td>
                                <td width="150"><p><?php echo  $val[csf("item_description")]; ?>&nbsp;</p></td>
                                <td width="80" align="right"><p><?php echo $val[csf("quantity")]; ?>&nbsp;</p></td>
								<td width="60" align="center"><p><?php echo $unit_of_measurement[$val[csf("uom")]]; ?>&nbsp;</p></td>
								<td width="80"  align="right"><p><?php echo $val[csf("uom_qty")]; ?>&nbsp;</p></td>
								<td width="100" align="center"><p><?php echo $val[csf("sent_by")]; ?>&nbsp;</p></td>
                                <td width="100" align="center"><p><?php echo $val[csf("sent_to")]; ?>&nbsp;</p></td>
                                <td width="60" align="center"><p><?php echo  $yes_no[$val[csf("returnable")]]; ?>&nbsp;</p></td>
                                
                                <td width="120"  align="center"><p><?php echo $val[csf("sent_to")]; ?>&nbsp;</p></td>
								<td width="100" align="center"><p><?php echo $val[csf("issue_purpose")]; ?>&nbsp;</p></td>
                                
                                <?php
								   /* if($val[csf("time_hour")]==24)
									   { 
										  $hour=$val[csf("time_hour")]-12;
										  $am="AM";
									   }
								  else  if($val[csf("time_hour")]==12)
									   { 
										  $hour=$val[csf("time_hour")];
										  $am="PM";
									   }
								  else  if($val[csf("time_hour")]>12 && $val[csf("time_hour")]<24)
									   { 
										  $hour=$val[csf("time_hour")]-12;
										  $am="PM";
									   } 
								   else
								       {
									    $hour=$val[csf("time_hour")];
										$am="AM";
									   }*/
								// echo $value[csf("time_hour")];
								  ?>
                                <td width="80" align="center"><p><?php echo  $gate_out_time;//$hour.":".$value[csf("time_minute")]." ".$am ; ?>&nbsp;</p></td>
                                <td width="100" align="center"><p><?php echo $val[csf("carried_by")]; ?>&nbsp;</p></td>
                                <td width=""><p><?php echo $val[csf("remarks")]; ?>&nbsp;</p></td>
								
							</tr>
							<?php
							$tot_quantity+=$val[csf("quantity")];
							$tot_uom_qty+=$val[csf("uom_qty")];
							$i++;
						}
					
					?>   
					</tbody>
                    <tfoot>
                    	<th colspan="6">Total</th><th><?php echo $tot_quantity;?></th>
                        <th></th> <th><?php echo $tot_uom_qty;?></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th> <th></th>
                    </tfoot>
                    
                    <?php
					}
				} //Out End
					?>
				</table>
                </div>
                </fieldset>
			
       </fieldset>
		  
			<?php	 
	
	
	  foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();

          
				 
	
	
	
	/*
			
	foreach (glob("*.xls") as $filename) {
	if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc,ob_get_contents());
	echo "$html**$filename**$cbo_item_cat"; 
	exit();*/
	
}
disconnect($con);
?>

