<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
//--------------------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 130, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );     	 
	exit();
}

if($action=="search_popup")
{
	extract($_REQUEST);
	if($type==1) $tittle="Booking"; else $tittle="Job";
	echo load_html_head_contents($tittle." No Info", "../../../", 1, 1,'','','');
	
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_id').val( id );
			$('#hide_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter <?php echo $tittle; ?> No </th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_no" id="hide_no" value="" />
                    <input type="hidden" name="hide_id" id="hide_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyerID,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
							if($type==1) $search_by_arr=array(1=>"Booking No",2=>"Job No"); else $search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+<?php echo $type; ?>, 'create_job_booking_no_search_list_view', 'search_div', 'fabric_booking_approval_status_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_job_booking_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";
	$type=$data[4];

	$arr=array (0=>$company_arr,1=>$buyer_arr);
	
	if($type==1)	
	{
		if($search_by==1) $search_field="booking_no"; else $search_field="job_no";
		
		if($data[1]==0)
		{
			if ($_SESSION['logic_erp']["data_level_secured"]==1)
			{
				if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
			}
			else $buyer_id_cond="";
		}
		else $buyer_id_cond=" and buyer_id=$data[1]";
		
		$sql= "select id, booking_no, booking_no_prefix_num, job_no, company_id, buyer_id from wo_booking_mst where status_active=1 and is_deleted=0 and ready_to_approved=1 and company_id=$company_id and $search_field like '$search_string' and item_category in(2,13) $buyer_id_cond order by booking_no";
			
		echo create_list_view("tbl_list_search", "Company,Buyer Name,Booking No,Job No", "140,140,140","600","240",0, $sql , "js_set_value", "id,booking_no", "", 1, "company_id,buyer_id,0,0", $arr , "company_id,buyer_id,booking_no,job_no", "",'','0,0,0,0','',1) ;
	}
	else
	{
		if($search_by==1) $search_field="job_no"; else $search_field="style_ref_no";
		
		if($data[1]==0)
		{
			if ($_SESSION['logic_erp']["data_level_secured"]==1)
			{
				if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
			}
			else $buyer_id_cond="";
		}
		else $buyer_id_cond=" and buyer_name=$data[1]";
		
		$sql= "select id, job_no, company_name, buyer_name, style_ref_no from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond order by job_no";
			
		echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Style Ref. No,", "120,120,120","600","240",0, $sql , "js_set_value", "id,job_no", "", 1, "company_name,buyer_name,0,0", $arr , "company_name,buyer_name,job_no,style_ref_no", "",'','0,0,0,0','',1) ;
	}
   exit(); 
} 

$tmplte=explode("**",$data);

if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
 
 	if(str_replace("'","",trim($cbo_date_by))==1)
	{
		if(str_replace("'","",trim($txt_date_from))!="" && str_replace("'","",trim($txt_date_to))!="")
		{
			$date_cond=" and a.booking_date between $txt_date_from and $txt_date_to";
		}
		else
		{
			$date_cond="";
		}
	}
	else if(str_replace("'","",trim($cbo_date_by))==2)
	{
		if(str_replace("'","",trim($txt_date_from))!="" && str_replace("'","",trim($txt_date_to))!="")
		{
			$date_cond=" and a.insert_date between $txt_date_from and $txt_date_to";
		}
		else
		{
			$date_cond="";
		}
	}
	
	
 	if($template==1)
	{
		$type = str_replace("'","",$cbo_type);
		if(str_replace("'","",$cbo_buyer_name)==0)
		{
			if ($_SESSION['logic_erp']["data_level_secured"]==1)
			{
				if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
			}
			else
			{
				$buyer_id_cond="";
			}
		}
		else
		{
			$buyer_id_cond=" and a.buyer_id=$cbo_buyer_name";
		}
		
		if(str_replace("'","",$hide_booking_id)=="") $booking_cond=""; else $booking_cond=" and a.id in(".str_replace("'","",$hide_booking_id).")";
		
		$txt_job_no=str_replace("'","",$txt_job_no);
		if($txt_job_no=="") $job_cond=""; else $job_cond=" and a.job_no in('".implode("','",explode("*",$txt_job_no))."')";
		
		$dealing_merchant_array = return_library_array("select id, team_member_name from lib_mkt_team_member_info","id","team_member_name");
		$job_dealing_merchant_array = return_library_array("select job_no, dealing_marchant from wo_po_details_master","job_no","dealing_marchant");
		
		$po_array=array();
		$poData=sql_select( "select id, po_number, pub_shipment_date from wo_po_break_down");
		foreach($poData as $po_row)
		{
			$po_array[$po_row[csf('id')]]['no']=$po_row[csf('po_number')];
			$po_array[$po_row[csf('id')]]['ship_date']=change_date_format($po_row[csf('pub_shipment_date')]);
		}
		
		$designation_array=return_library_array( "select id, custom_designation from lib_designation", "id", "custom_designation" );
		
		$user_name_array=array();
		$userData=sql_select( "select id, user_name, user_full_name, designation from user_passwd");
		foreach($userData as $user_row)
		{
			$user_name_array[$user_row[csf('id')]]['name']=$user_row[csf('user_name')];
			$user_name_array[$user_row[csf('id')]]['full_name']=$user_row[csf('user_full_name')];
			$user_name_array[$user_row[csf('id')]]['designation']=$designation_array[$user_row[csf('designation')]];	
		}
		
		$approved_no_array=return_library_array( "select mst_id, max(approved_no) as approved_no from approval_history where entry_form=7 group by mst_id", "mst_id", "approved_no");
		$signatory=return_field_value("group_concat(user_id) as user_id", "electronic_approval_setup", "entry_form=7 and is_deleted=0","user_id" );
		
		//$last_user_id=return_field_value("max(user_id) as user_id", "electronic_approval_setup", "entry_form=7 and is_deleted=0","user_id" );
		$last_user_id=return_field_value("user_id", "electronic_approval_setup", "entry_form=7 and is_deleted=0 order by sequence_no desc limit 0,1","user_id" );
		
		$user_approval_array=array();
		$query="select mst_id, approved_no, approved_by, approved_date from approval_history where entry_form=7";
		$result=sql_select( $query );
        foreach ($result as $row)
		{
			$user_approval_array[$row[csf('mst_id')]][$row[csf('approved_no')]][$row[csf('approved_by')]]=$row[csf('approved_date')];
		}
		
		ob_start();
		?>
        <fieldset style="width:1480px;">
        	<table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                   <td align="center" width="100%" colspan="12" style="font-size:16px"><strong><?php echo $report_title; ?></strong></td>
                </tr>
                <tr>
                   <td align="center" width="100%" colspan="12" style="font-size:16px"><strong><?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?></strong></td>
                </tr>
            </table>	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1460" class="rpt_table" >
                <thead>
                    <th width="40">SL</th>
                    <th width="100">Job No</th>
                    <th width="80">Buyer Name</th>
                    <th width="110">Dealing Merchant</th>
                    <th width="50">Image</th>
                    <th width="50">File</th>
                    <th width="200">Order No</th>
                    <th width="80">Shipment Date (Min)</th>
                    <th width="120">Booking No</th>
                    <th width="80">Type</th>
                    <th width="120">Signatory</th>
                    <th width="110">Designation</th>
                    <th width="100">Approval Date</th>
                    <th width="100">Approval Time</th>
                    <th>Approve No</th>
                </thead>
            </table>
			<div style="width:1460px; overflow-y:scroll; max-height:330px;" id="scroll_body">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1442" class="rpt_table" id="tbl_list_search">
                    <tbody>
                        <?php 
							$i=1; $signatory=explode(",",$signatory); $rowspan=count($signatory);
							
							if($type==2) $approved_cond=" and a.is_approved=1"; else $approved_cond="";
							
							$sql="select a.id, a.company_id, a.booking_no, a.buyer_id, a.booking_no_prefix_num, a.job_no, a.po_break_down_id, a.item_category, a.fabric_source, a.booking_type, a.is_short, a.is_approved, a.insert_date, a.booking_date from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$cbo_company_name and a.item_category in(2,13) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.ready_to_approved=1 $buyer_id_cond $booking_cond $job_cond $approved_cond $date_cond group by a.id order by a.insert_date desc";
							//echo $sql;
                            $nameArray=sql_select( $sql );
                            foreach ($nameArray as $row)
                            {
								$full_approval=$user_approval_array[$row[csf('id')]][$approved_no_array[$row[csf('id')]]][$last_user_id];
								
								if((($type==1 && $full_approval=="") || $row[csf('is_approved')]==0) || ($type==2 && $full_approval!=""))
								{
									if ($i%2==0)  
										$bgcolor="#E9F3FF";
									else
										$bgcolor="#FFFFFF";
									
									$dealing_merchant=$dealing_merchant_array[$job_dealing_merchant_array[$row[csf('job_no')]]];
									$po_id=explode(",",$row[csf('po_break_down_id')]);
									$po_no=''; $min_ship_date='';
									
									foreach($po_id as $val)
									{
										if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
										
										$ship_date=$po_array[$val]['ship_date'];
										
										if($min_ship_date=="")
										{
											$min_ship_date=$ship_date;
										}
										else
										{
											if($min_ship_date>$ship_date) $min_ship_date=$ship_date;
										}
									}
									
									if($row[csf('booking_type')]==4) 
									{
										$booking_type=3; 
										$booking_type_text="Sample";
									}
									else 
									{
										$booking_type=$row[csf('is_short')];
										
										if($row[csf('is_short')]==1) $booking_type_text="Short"; else $booking_type_text="Main"; 
									}
									
									$z=0;
									foreach($signatory as $val)
									{
									?>
										<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
										<?php
										if($z==0)
										{
											if(str_replace("'","",trim($cbo_date_by))==1)
											{
												$date_all="B Date : ".change_date_format($row[csf('booking_date')]);
											}
											else if(str_replace("'","",trim($cbo_date_by))=='2')
											{
												$insert_date=$row[csf('insert_date')];
												$date_all="In Date: ".date("d-m-Y",strtotime($insert_date)); 
											}

										?>
											<td width="40" rowspan="<?php echo $rowspan; ?>"><?php echo $i; ?></td>
											<td width="100" rowspan="<?php echo $rowspan; ?>" align="center"><p><?php echo $row[csf('job_no')]; ?></p></td>
											<td width="80" rowspan="<?php echo $rowspan; ?>"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?>&nbsp;</p></td>
                                            <td width="110" rowspan="<?php echo $rowspan; ?>"><p><?php echo $dealing_merchant; ?>&nbsp;</p></td>
                                            <td width="50" rowspan="<?php echo $rowspan; ?>" align="center"><a href="##" onClick="openImgFile('<?php echo $row[csf('job_no')];?>','img');">View</a></td>
                                            <td width="50" rowspan="<?php echo $rowspan; ?>" align="center"><a href="##" onClick="openImgFile('<?php echo $row[csf('job_no')];?>','file');">View</a></td>
											<td width="200" rowspan="<?php echo $rowspan; ?>">
                                            
                                            <p><a href='##' style='color:#000' onClick="generate_fabric_report2(<?php echo $booking_type; ?>,'<?php echo $row[csf('booking_no')]; ?>',<?php echo $row[csf('company_id')]; ?>,'<?php echo $row[csf('po_break_down_id')]; ?>',<?php echo $row[csf('item_category')].','.$row[csf('fabric_source')]; ?>,'<?php echo $row[csf('job_no')]; ?>','<?php echo $row[csf('is_approved')]; ?>')"><?php echo $po_no; ?></a></p>
                                            </td>
                                            <td width="80" rowspan="<?php echo $rowspan; ?>" align="center"><p><?php echo $min_ship_date; ?></p></td>
											<td width="120" rowspan="<?php echo $rowspan; ?>">
												<p><a href='##' style='color:#000' onClick="generate_worder_report(<?php echo $booking_type; ?>,'<?php echo $row[csf('booking_no')]; ?>',<?php echo $row[csf('company_id')]; ?>,'<?php echo $row[csf('po_break_down_id')]; ?>',<?php echo $row[csf('item_category')].','.$row[csf('fabric_source')]; ?>,'<?php echo $row[csf('job_no')]; ?>','<?php echo $row[csf('is_approved')]; ?>');"><?php echo $row[csf('booking_no')]."<br>".$date_all; ?></a></p>
											</td>
                                            <td width="80" onClick="generate_worder_report2(<?php echo $booking_type; ?>,'<?php echo $row[csf('booking_no')]; ?>',<?php echo $row[csf('company_id')]; ?>,'<?php echo $row[csf('po_break_down_id')]; ?>',<?php echo $row[csf('item_category')].','.$row[csf('fabric_source')]; ?>,'<?php echo $row[csf('job_no')]; ?>','<?php echo $row[csf('is_approved')]; ?>');" rowspan="<?php echo $rowspan; ?>" align="center"><p><a href='##' style='color:#000'><?php echo $booking_type_text; ?></a></p></td>
										<?php
										}
										
										$approval_date=$user_approval_array[$row[csf('id')]][$approved_no_array[$row[csf('id')]]][$val];
										$date=''; $time=''; $approved_no='';
										if($approval_date!="") 
										{
											$date=date("d-M-Y",strtotime($approval_date)); 
											$time=date("h:i:s A",strtotime($approval_date)); 
											$approved_no=$approved_no_array[$row[csf('id')]];
										}
										?>
											<td width="120"><p><?php echo $user_name_array[$val]['full_name']; ?>&nbsp;</p></td>
                                            <td width="110"><p><?php echo $user_name_array[$val]['designation']; ?>&nbsp;</p></td>
											<td width="100" align="center"><p><?php if($row[csf('is_approved')]!=0) echo $date; ?>&nbsp;</p></td>
											<td width="100" align="center"><p><?php if($row[csf('is_approved')]!=0) echo $time; ?>&nbsp;</p></td>
											<td><p>&nbsp;<?php if($row[csf('is_approved')]!=0) echo $approved_no; ?>&nbsp;</p></td>
										</tr>
									<?php
										$z++;
									}
								$i++;
								}
							}
							?>
                    </tbody>
                </table>
			</div>
      	</fieldset>      
	<?php
	}
	

	foreach (glob("$user_name*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_name."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename="requires/".$user_name."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
 	
}

if($action=="img")
{
	echo load_html_head_contents("Image View", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	
?>
	<fieldset style="width:600px; margin-left:5px">
		<div style="width:100%; word-wrap:break-word" id="scroll_body">
             <table border="0" rules="all" width="100%" cellpadding="2" cellspacing="2">
             	<tr>
					<?php
					$i=0;
                    $sql="select image_location from common_photo_library where master_tble_id='$job_no' and form_name='knit_order_entry' and file_type=1";
                    $result=sql_select($sql);
                    foreach($result as $row)
                    {
						$i++;
                    ?>
                    	<td align="center"><img width="300px" height="180px" src="../../../<?php echo $row[csf('image_location')];?>" /></td>
                    <?php
						if($i%2==0) echo "</tr><tr>";
                    }
                    ?>
                </tr>
            </table>
        </div>	
	</fieldset>     
<?php
exit();
}

if($action=="file")
{
	echo load_html_head_contents("File View", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	
?>
	<fieldset style="width:600px; margin-left:5px">
		<div style="width:100%; word-wrap:break-word" id="scroll_body">
             <table border="0" rules="all" width="100%" cellpadding="2" cellspacing="2">
             	<tr>
					<?php
					$i=0;
                    $sql="select image_location from common_photo_library where master_tble_id='$job_no' and form_name='knit_order_entry' and file_type=2";
                    $result=sql_select($sql);
                    foreach($result as $row)
                    {
						$i++;
                    ?>
                    	<td width="100" align="center"><a href="../../../<?php echo $row[csf('image_location')]; ?>"><img width="89" height="97" src="../../../file_upload/blank_file.png"><br>File-<?php echo $i; ?></a></td>
                    <?php
						if($i%6==0) echo "</tr><tr>";
                    }
                    ?>
                </tr>
            </table>
        </div>	
	</fieldset>     
<?php
exit();
}

?>