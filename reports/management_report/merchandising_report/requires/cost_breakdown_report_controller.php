<?php
error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
//--------------------------------------------------------------------------------------------------------------------
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$buyer_short_name_library=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$item_library=return_library_array( "select id, item_name from lib_item_group", "id", "item_name"  );
$yarn_count_library=return_library_array( "select id, yarn_count from lib_yarn_count", "id", "yarn_count"  );
$dealing_merchant_array = return_library_array("select id, team_member_name from lib_mkt_team_member_info","id","team_member_name");
$team_library=return_library_array( "select id, team_name from lib_marketing_team", "id", "team_name"  );
$costing_library=return_library_array( "select job_no, costing_date from wo_pre_cost_mst", "job_no", "costing_date"  );

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );     	 
	exit();
}

if ($action=="load_drop_down_team_member")
{
	echo create_drop_down( "cbo_team_member", 150, "select id,team_member_name from lib_mkt_team_member_info where team_id='$data' and status_active=1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "- Team Member-", $selected, "" ); 
}

$tmplte=explode("**",$data);

if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;

if( $action=="report_generate" )
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 

	$company_name=str_replace("'","",$cbo_company_name);
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	
	$date_cond='';
	if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
	{
		$start_date=str_replace("'","",$txt_date_from);
		$end_date=str_replace("'","",$txt_date_to); 
		$date_cond=" and b.pub_shipment_date between '$start_date' and '$end_date'";
	}
	
	$cbo_team_name=str_replace("'","",$cbo_team_name);
	$cbo_team_member=str_replace("'","",$cbo_team_member);
	
	if($cbo_team_name==0) $team_name_cond=""; else $team_name_cond=" and a.team_leader='$cbo_team_name'";
	if($cbo_team_member==0) $team_member_cond=""; else $team_member_cond=" and a.dealing_marchant='$cbo_team_member'";
	
	if($template==1)
	{
		ob_start();
	?>
	
        <div style="width:2900px">
        <fieldset style="width:100%;">	
            <table width="2880">
                <tr class="form_caption">
                    <td colspan="32" align="center">Cost Breakdown Report</td>
                </tr>
                <tr class="form_caption">
                    <td colspan="32" align="center"><?php echo $company_library[$company_name]; ?></td>
                </tr>
            </table>
            <table id="table_header_1" class="rpt_table" width="2880" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="40">SL</th>
                    <th width="70">Job No</th>
                    <th width="50">Year</th>
                    <th width="60">Buyer</th>
                    <th width="100">Team</th>
                    <th width="110">Dealing Merchant</th>
                    <th width="110">Order No</th>
                    <th width="110">Style Ref.</th>
                    <th width="110">Garments Item</th>
                    <th width="90">Order Qnty</th>
                    <th width="50">UOM</th>
                    <th width="90">Qnty (Pcs)</th>
                    <th width="80">Shipment Date</th>
                    <th width="230">Fabric Description</th>
                    <th width="70">Knit Fab. Cons</th>
                    <th width="60">Knit Fab. Rate</th>
                    <th width="70">Woven Fab. Cons</th>
                    <th width="65">Woven Fab. Rate</th>
                    <th width="80">Fab. Cost/Dzn</th>
                    <th width="80">Trims cost/Dzn</th>
                    <th width="80">Print/Emb/ Dzn</th>
                    <th width="80">CM cost/Dzn</th>
                    <th width="85">Commission</th>
                    <th width="80">Other Cost</th>
                    <th width="80">Total cost/Dzn</th>
                    <th width="100">Total CM cost</th>
                    <th width="100">Total Cost</th>
                    <th width="65">Cost Per unit</th>
                    <th width="65">Order Price</th>
                    <th width="100">Order Value</th>
                    <th width="100">Margin</th>
                    <th width="100">Total Trims Cost</th>
                    <th>Total Emb/Print Cost</th>
                </thead>
            </table>
            <div style="width:2900px; max-height:400px; overflow-y:scroll" id="scroll_body">
                <table class="rpt_table" width="2880" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
                <?php
                $i=1; $total_order_qnty=0; $total_order_qnty_in_pcs=0; $grand_tot_cm_cost=0; $grand_tot_cost=0; $tot_order_value=0; $tot_margin=0; $grand_tot_trims_cost=0; $grand_tot_embell_cost=0; $tot_knit_charge=0; $tot_yarn_dye_charge=0; $tot_dye_finish_charge=0; $yarn_desc_array=array(); $fabriccostArray=array(); $trims_cons_cost_array=array();
				$prodcostArray=array(); $fabricArray=array(); $yarncostArray=array();
                
				$fabricDataArray=sql_select("select a.job_no, a.fab_nature_id, a.fabric_description, a.fabric_source, a.rate, a.avg_finish_cons, b.yarn_amount, b.conv_amount from wo_pre_cost_fabric_cost_dtls a, wo_pre_cost_sum_dtls b where a.job_no=b.job_no and a.fabric_source!=3 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
				foreach($fabricDataArray as $fabricRow)
				{
					$fabricArray[$fabricRow[csf('job_no')]].=$fabricRow[csf('fab_nature_id')]."**".$fabricRow[csf('fabric_description')]."**".$fabricRow[csf('fabric_source')]."**".$fabricRow[csf('rate')]."**".$fabricRow[csf('avg_finish_cons')]."**".$fabricRow[csf('yarn_amount')]."**".$fabricRow[csf('conv_amount')].",";
				}
				
				$yarncostDataArray=sql_select("select job_no, count_id, type_id, sum(cons_qnty) as cons_qnty, sum(amount) as amount from wo_pre_cost_fab_yarn_cost_dtls where status_active=1 and is_deleted=0 group by job_no, count_id, type_id");
				foreach($yarncostDataArray as $yarnRow)
				{
				   $yarncostArray[$yarnRow[csf('job_no')]].=$yarnRow[csf('count_id')]."**".$yarnRow[csf('type_id')]."**".$yarnRow[csf('cons_qnty')]."**".$yarnRow[csf('amount')].",";
				}
				
				$fabriccostDataArray=sql_select("select job_no, costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where status_active=1 and is_deleted=0");
				foreach($fabriccostDataArray as $fabRow)
				{
					 $fabriccostArray[$fabRow[csf('job_no')]]['costing_per_id']=$fabRow[csf('costing_per_id')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['trims_cost']=$fabRow[csf('trims_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['embel_cost']=$fabRow[csf('embel_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['cm_cost']=$fabRow[csf('cm_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['commission']=$fabRow[csf('commission')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['common_oh']=$fabRow[csf('common_oh')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['lab_test']=$fabRow[csf('lab_test')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['inspection']=$fabRow[csf('inspection')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['freight']=$fabRow[csf('freight')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['comm_cost']=$fabRow[csf('comm_cost')];
				}
				
				$trimscostDataArray=sql_select("select a.job_no, b.po_break_down_id, sum(b.cons*a.rate) as total from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b where a.id=b.wo_pre_cost_trim_cost_dtls_id and a.status_active=1 and a.is_deleted=0 group by a.job_no, b.po_break_down_id");
				foreach($trimscostDataArray as $trimsRow)
				{
					 $trims_cons_cost_array[$trimsRow[csf('job_no')]][$trimsRow[csf('po_break_down_id')]]=$trimsRow[csf('total')];
				}
				 
				$prodcostDataArray=sql_select("select job_no, 
									  sum(CASE WHEN cons_process=1 THEN amount END) AS knit_charge,
									  sum(CASE WHEN cons_process=30 THEN amount END) AS yarn_dye_charge,
									  sum(CASE WHEN cons_process not in(1,2,30) THEN amount END) AS dye_finish_charge
									  from wo_pre_cost_fab_conv_cost_dtls where status_active=1 and is_deleted=0 group by job_no");
				foreach($prodcostDataArray as $prodRow)
				{
					$prodcostArray[$prodRow[csf('job_no')]]['knit_charge']=$prodRow[csf('knit_charge')];
					$prodcostArray[$prodRow[csf('job_no')]]['yarn_dye_charge']=$prodRow[csf('yarn_dye_charge')];
					$prodcostArray[$prodRow[csf('job_no')]]['dye_finish_charge']=$prodRow[csf('dye_finish_charge')];
				}					  
				 
				if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
				else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
				else $year_field="";//defined Later
				
                $sql="select a.job_no_prefix_num, a.job_no, $year_field, a.company_name, a.buyer_name, a.team_leader, a.dealing_marchant, a.style_ref_no, a.order_uom, a.gmts_item_id, a.total_set_qnty as ratio, b.id, b.po_number, b.pub_shipment_date, b.po_quantity, b.unit_price, b.po_total_price, b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_name' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $date_cond $buyer_id_cond $team_name_cond $team_member_cond order by b.id, b.pub_shipment_date";
			
                $nameArray=sql_select($sql);
                $tot_rows=count($nameArray);
                foreach($nameArray as $row )
                {
                    if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					$costing_date=$costing_library[$row[csf('job_no')]];
                ?>
                    <tr bgcolor="<?php echo $bgcolor;?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i; ?>">
                        <td width="40"><?php echo $i; ?></td>
                        <td width="70" align="center"><?php echo $row[csf('job_no_prefix_num')]; ?></td>
                        <td width="50" align="center"><?php echo $row[csf('year')]; ?></td>
                        <td width="60"><?php echo $buyer_short_name_library[$row[csf('buyer_name')]]; ?></td>
                        <td width="100"><p><?php echo $team_library[$row[csf('team_leader')]]; ?></p></td>
                        <td width="110"><p><?php echo $dealing_merchant_array[$row[csf('dealing_marchant')]]; ?></p></td>
                        <td width="110"><p><a href="##" onclick="generate_pre_cost_report('preCostRptOrder','<?php echo $row[csf('id')]; ?>','<?php echo $row[csf('company_name')]; ?>','<?php echo $row[csf('buyer_name')]; ?>','<?php echo $row[csf('style_ref_no')]; ?>','<?php echo $costing_date; ?>')"><?php echo $row[csf('po_number')]; ?></a></p></td>
                        <td width="110"><p><?php echo $row[csf('style_ref_no')]; ?></p></td>
                        <td width="110">
                            <p>
                                <?php
                                    $gmts_item='';
                                    $gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
                                    foreach($gmts_item_id as $item_id)
                                    {
                                        if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
                                    }
                                    echo $gmts_item;
                                ?>
                            </p>
                        </td>
                        <td width="90" align="right" >
                            <?php 
                                echo number_format($row[csf('po_quantity')],0,'.',''); 
                                $total_order_qnty+=$row[csf('po_quantity')];
                            ?>
                        </td>
                        <td width="50" align="center"><?php echo $unit_of_measurement[$row[csf('order_uom')]]; ?></td>
                        <td width="90" align="right">
                        <?php 
                            $order_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
							$plan_cut_qnty=$row[csf('plan_cut')]*$row[csf('ratio')];
                            echo number_format($order_qnty_in_pcs,0,'.',''); 
                            $total_order_qnty_in_pcs+=$order_qnty_in_pcs;
                        ?>
                        </td>
                        <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                        <?php
                        $fabric_desc=''; $fabric_cost_per_dzn=0; $knit_fabric_cons=0; $knit_fabric_rate=0; $knit_fabric_amnt=0; $yarn_cost=0;$conversion_cost=0;
						$knit_fabric_purc_amnt=0; $woven_fabric_cons=0; $woven_fabric_rate=0; $woven_fabric_amnt=0; $other_cost=0;
                        $tot_cost_per_dzn=0; $tot_cm_cost=0; $tot_cost=0; $tot_trims_cost=0; $tot_embell_cost=0; $cost_per_unit=0; $margin=0;

						$fabricData=explode(",",substr($fabricArray[$row[csf('job_no')]],0,-1));
                        foreach($fabricData as $fabricRow)
                        {
							$fabricRow=explode("**",$fabricRow);
							$fab_nature_id=$fabricRow[0];
							$fabric_description=$fabricRow[1];
							$fabric_source=$fabricRow[2];
							$rate=$fabricRow[3];
							$avg_finish_cons=$fabricRow[4];
							$yarn_amount=$fabricRow[5];
							$conv_amount=$fabricRow[6];
							
                            if($fabric_desc=="") $fabric_desc=$fabric_description; else $fabric_desc.=",".$fabric_description;
                            if($fab_nature_id==2)
                            {
                                $knit_fabric_cons+=$avg_finish_cons;
                                if($fabric_source==2)
                                {
                                    $knit_fabric_purc_amnt+=$avg_finish_cons*$rate;	
                                }
                            }
                            else if($fab_nature_id==3)
                            {
                                if($fabric_source==2)
                                { 
                                    $woven_fabric_cons+=$avg_finish_cons;
                                    $woven_fabric_amnt+=$avg_finish_cons*$rate;
                                }
                            }
                            
                            $yarn_cost=$yarn_amount;
                            $conversion_cost=$conv_amount;
                        }
						
                        $knit_fabric_amnt=$knit_fabric_purc_amnt+$yarn_cost+$conversion_cost;
                        $knit_fabric_rate=$knit_fabric_amnt/$knit_fabric_cons;
                        $woven_fabric_rate=$woven_fabric_amnt/$woven_fabric_cons;
                        $fabric_cost_per_dzn=$knit_fabric_amnt+$woven_fabric_amnt;

					 	$dzn_qnty=0;
						$costing_per_id=$fabriccostArray[$row[csf('job_no')]]['costing_per_id'];
                        if($costing_per_id==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($costing_per_id==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($costing_per_id==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($costing_per_id==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						
						$dzn_qnty=$dzn_qnty*$row[csf('ratio')];
						
						$other_cost=$fabriccostArray[$row[csf('job_no')]]['common_oh']+$fabriccostArray[$row[csf('job_no')]]['lab_test']+$fabriccostArray[$row[csf('job_no')]]['inspection']+$fabriccostArray[$row[csf('job_no')]]['freight']+$fabriccostArray[$row[csf('job_no')]]['comm_cost'];
                        
						$trims_cons_cost=$trims_cons_cost_array[$row[csf('job_no')]][$row[csf('id')]];

                        $tot_cost_per_dzn=$fabric_cost_per_dzn+$trims_cons_cost+$fabriccostArray[$row[csf('job_no')]]['cm_cost']+$fabriccostArray[$row[csf('job_no')]]['commission']+$fabriccostArray[$row[csf('job_no')]]['embel_cost']+$other_cost;
                        $cost_per_unit=$tot_cost_per_dzn/$dzn_qnty;
                        
                        $tot_cm_cost=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['cm_cost'];
                        $tot_cost=($order_qnty_in_pcs/$dzn_qnty)*$tot_cost_per_dzn;
                        $tot_trims_cost=($order_qnty_in_pcs/$dzn_qnty)*$trims_cons_cost;
                        $tot_embell_cost=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['embel_cost'];
                        $margin=$row[csf('po_total_price')]-$tot_cost;
						
						$yarnData=explode(",",substr($yarncostArray[$row[csf('job_no')]],0,-1));
						foreach($yarnData as $yarnRow)
						{
							$yarnRow=explode("**",$yarnRow);
							$count_id=$yarnRow[0];
							$type_id=$yarnRow[1];
							$cons_qnty=$yarnRow[2];
							$amount=$yarnRow[3];
													
							$yarn_desc=$yarn_count_library[$count_id]."**".$yarn_type[$type_id];
							$req_qnty=($plan_cut_qnty/$dzn_qnty)*$cons_qnty;
							$req_amnt=($plan_cut_qnty/$dzn_qnty)*$amount;
							 
							$yarn_desc_array[$yarn_desc]['qnty']+=$req_qnty;
							$yarn_desc_array[$yarn_desc]['amnt']+=$req_amnt;
						}
						 
						$tot_knit_charge+=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['knit_charge'];
						$tot_yarn_dye_charge+=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['yarn_dye_charge']; 
						$tot_dye_finish_charge+=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['dye_finish_charge'];			  
                        ?>
                        <td width="230">
                            <p>
                                <?php $fabric_desc=explode(",",$fabric_desc); echo join(",<br>",array_unique($fabric_desc)); ?>
                            </p>
                        </td>
                        <td width="70" align="right"><?php echo number_format($knit_fabric_cons,2,'.',''); ?></td>
                        <td width="60" align="right"><?php echo number_format($knit_fabric_rate,2,'.',''); ?></td>
                        <td width="70" align="right"><?php echo number_format($woven_fabric_cons,2,'.',''); ?></td>
                        <td width="65" align="right"><?php echo number_format($woven_fabric_rate,2,'.',''); ?></td>
                        <?php
							if($fabric_cost_per_dzn>0) $td_color=""; else $td_color="#FF0000";
						?>
                        <td width="80" align="right" bgcolor="<?php echo $td_color; ?>">
                        <?php 
                            echo number_format($fabric_cost_per_dzn,2,'.',''); 
                            $fabric_cost_summary+=($order_qnty_in_pcs/$dzn_qnty)*$fabric_cost_per_dzn;
                        ?>
                        </td>
                        <?php
						 	if($trims_cons_cost>0) $trims_td_color=""; else $trims_td_color="#FF0000";
							
							$po_id=$row[csf('id')]; $po_qnty=$order_qnty_in_pcs; $po_no=$row[csf('po_number')];  $job_no=$row[csf('job_no')];
						?>
                        <td width="80" align="right" bgcolor="<?php echo $trims_td_color; ?>"><?php echo "<a href='#report_details' onclick= \"openmypage($po_id,'$po_qnty','$po_no','$job_no','trims_cost','Trims Cost Info');\">".number_format($trims_cons_cost,2,'.','')."</a>"; ?></td>
                        <td width="80" align="right"><?php  echo number_format($fabriccostArray[$row[csf('job_no')]]['embel_cost'],2,'.',''); ?></td>
                        <?php
							if($fabriccostArray[$row[csf('job_no')]]['cm_cost']>0) $cm_td_color=""; else $cm_td_color="#FF0000";
						?>
                        <td width="80" align="right" bgcolor="<?php echo $cm_td_color; ?>"><?php echo number_format($fabriccostArray[$row[csf('job_no')]]['cm_cost'],2,'.',''); ?></td>
                        <td width="85" align="right">
							<?php 
                                echo number_format($fabriccostArray[$row[csf('job_no')]]['commission'],2,'.',''); 
                                $comm_cost_summary+=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['commission'];
                            ?>
                        </td>
                        <td width="80" align="right">
							<?php 
                                echo "<a href='#report_details' onclick= \"openmypage($po_id,'$po_qnty','$po_no','$job_no','other_cost','Other Cost Info');\">".number_format($other_cost,2,'.','')."</a>";
                                $other_cost_summary+=($order_qnty_in_pcs/$dzn_qnty)*$other_cost;
                            ?>
                        </td>
                        <td width="80" align="right"><?php echo number_format($tot_cost_per_dzn,2,'.',''); ?></td>
                        <td width="100" align="right">
                            <?php 
                                echo number_format($tot_cm_cost,2,'.',''); 
                                $grand_tot_cm_cost+=$tot_cm_cost;
                            ?>
                        </td>
                        <td width="100" align="right">
                            <?php 
                                echo number_format($tot_cost,2,'.','');
                                $grand_tot_cost+=$tot_cost; 
                            ?>
                        </td>
                        <td width="65" align="right"><?php echo number_format($cost_per_unit,2,'.',''); ?></td>
                        <td width="65" align="right"><?php echo number_format($row[csf('unit_price')],2); ?></td>
                        <td width="100" align="right">
                            <?php 
                                echo number_format($row[csf('po_total_price')],2,'.',''); 
                                $tot_order_value+=$row[csf('po_total_price')];
                            ?>
                        </td>
                        <td width="100" align="right">
                            <?php 
                                echo number_format($margin,2,'.','');
                                $tot_margin+=$margin; 
                            ?>
                        </td>
                        <td width="100" align="right">
                            <?php 
                                echo number_format($tot_trims_cost,2,'.',''); 
                                $grand_tot_trims_cost+=$tot_trims_cost;
                            ?>
                        </td>
                        <td align="right">
                            <?php 
                                echo number_format($tot_embell_cost,2,'.','');
                                $grand_tot_embell_cost+=$tot_embell_cost; 
                            ?>
                        </td>
                    </tr>
                <?php
                $i++;
                }
                ?>
                </table>
                <table class="rpt_table" width="2880" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tfoot>
                        <th width="40"></th>
                        <th width="70"></th>
                        <th width="50"></th>
                        <th width="60"></th>
                        <th width="100"></th>
                        <th width="110"></th>
                        <th width="110"></th>
                        <th width="110"></th>
                        <th width="110" align="right">Total</th>
                        <th width="90" align="right" id="total_order_qnty"><?php echo number_format($total_order_qnty,0); ?></th>
                        <th width="50"></th>
                        <th width="90" align="right" id="total_order_qnty_in_pcs"><?php echo number_format($total_order_qnty_in_pcs,0); ?></th>
                        <th width="80"></th>
                        <th width="230"></th>
                        <th width="70"></th>
                        <th width="60"></th>
                        <th width="70"></th>
                        <th width="65"></th>
                        <th width="80"></th>
                        <th width="80"></th>
                        <th width="80"></th>
                        <th width="80"></th>
                        <th width="85"></th>
                        <th width="80"></th>
                        <th width="80"></th>
                        <th width="100" align="right" id="value_tot_cm_cost"><?php echo number_format($grand_tot_cm_cost,2); ?></th>
                        <th width="100" align="right" id="value_tot_cost"><?php echo number_format($grand_tot_cost,2); ?></th>
                        <th width="65"></th>
                        <th width="65"></th>
                        <th width="100" align="right" id="value_order"><?php echo number_format($tot_order_value,2); ?></th>
                        <th width="100" align="right" id="value_margin"><?php echo number_format($tot_margin,2); ?></th>
                        <th width="100" align="right" id="value_tot_trims_cost"><?php echo number_format($grand_tot_trims_cost,2); ?></th>
                        <th align="right" id="value_tot_embell_cost"><?php echo number_format($grand_tot_embell_cost,2); ?></th>
                    </tfoot>
                </table>
            </div>
            <table>
                <tr><td height="15"></td></tr>
            </table>
            <table style="margin-left:20px" width="1500">
                <tr>
                    <td width="400" valign="top"><b><u>Cost Summary</u></b>
                        <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                            <thead>
                                <th width="140">Particulars</th>
                                <th width="160">Amount</th>
                                <th>Percentage</th>
                            </thead>
                            <tr bgcolor="#E9F3FF">
                                <td>Fabric Cost</td>
                                <td align="right"><?php echo number_format($fabric_cost_summary,2); ?>
                                </td>
                                <td align="right"><?php echo number_format((($fabric_cost_summary*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td>Trims Cost</td>
                                <td align="right"><?php echo number_format($grand_tot_trims_cost,2); ?></td>
                               <td align="right"><?php echo number_format((($grand_tot_trims_cost*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#E9F3FF">
                                <td>Embellish Cost</td>
                                <td align="right"><?php echo number_format($grand_tot_embell_cost,2); ?></td>
                                <td align="right"><?php echo number_format((($grand_tot_embell_cost*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td>Commision Cost</td>
                                <td align="right"><?php echo number_format($comm_cost_summary,2); ?></td>
                                <td align="right"><?php echo number_format((($comm_cost_summary*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#E9F3FF">
                                <td>Other Cost</td>
                                <td align="right"><?php echo number_format($other_cost_summary,2); ?></td>
                                <td align="right"><?php echo number_format((($other_cost_summary*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td>Total Cost</td>
                                <td align="right"><?php $total_cost_summ=$grand_tot_cost-$grand_tot_cm_cost; echo number_format($total_cost_summ,2); ?></td>
                                <td align="right"><?php echo number_format((($total_cost_summ*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#E9F3FF">
                                <td>Total Order Value</td>
                                <td align="right"><?php echo number_format($tot_order_value,2); ?></td>
                                <td align="right"><?php echo number_format((($tot_order_value*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td>CM Value</td>
                                <td align="right">
                                    <?php 
                                        $cm_value=$tot_order_value-$total_cost_summ;
                                        echo number_format($cm_value,2); 
                                    ?>
                                </td>
                                <td align="right"><?php echo number_format((($cm_value*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#E9F3FF">
                                <td>CM Cost</td>
                                <td align="right"><?php echo number_format($grand_tot_cm_cost,2); ?></td>
                                <td align="right"><?php echo number_format((($grand_tot_cm_cost*100)/$tot_order_value),2); ?></td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                                <td>Margin</td>
                                <td align="right">
                                    <?php 
                                        $margin_value=$cm_value-$grand_tot_cm_cost;
                                        echo number_format($margin_value,2); 
                                    ?>
                                </td>
                                <td align="right"><?php echo number_format((($margin_value*100)/$tot_order_value),2); ?></td>
                            </tr>
                        </table>
                    </td>
                    <td width="50"></td>
                    <td width="570" valign="top"><b><u>Yarn Summary</u></b>
                    	<table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                            <thead>
                            	<th width="30">SL</th>
                                <th width="80">Yarn Count</th>
                                <th width="120">Type</th>
                                <th width="120">Req. Qnty</th>
                                <th width="80">Avg. rate</th>
                                <th>Amount</th>
                            </thead>
                            <?php
							$s=1; $tot_yarn_req_qnty=0; $tot_yarn_req_amnt=0;
							
							foreach($yarn_desc_array as $key=>$value)
							{
								
								if($s%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
								$yarn_desc=explode("**",$key);
								
								if($yarn_desc[0]!="")
								{
									$tot_yarn_req_qnty+=$yarn_desc_array[$key]['qnty']; 
									$tot_yarn_req_amnt+=$yarn_desc_array[$key]['amnt'];
								?>
									<tr bgcolor="<?php echo $bgcolor;?>" onclick="change_color('tr3_<?php echo $s; ?>','<?php echo $bgcolor; ?>')" id="tr3_<?php echo $s;?>">
										<td><?php echo $s; ?></td>
										<td align="center"><?php echo $yarn_desc[0]; ?></td>
										<td><?php echo $yarn_desc[1]; ?></td>
										<td align="right"><?php echo number_format($yarn_desc_array[$key]['qnty'],2); ?></td>
										<td align="right"><?php echo number_format($yarn_desc_array[$key]['amnt']/$yarn_desc_array[$key]['qnty'],2); ?></td>
										<td align="right"><?php echo number_format($yarn_desc_array[$key]['amnt'],2); ?></td>
									</tr>
								<?php	
								$s++;
								}
							}
							?>
                            <tfoot>
                            	<th colspan="3" align="right">Total</th>
                                <th align="right"><?php echo number_format($tot_yarn_req_qnty,2); ?></th>
                                <th align="right"><?php echo number_format($tot_yarn_req_amnt/$tot_yarn_req_qnty,2); ?></th>
                                <th align="right"><?php echo number_format($tot_yarn_req_amnt,2); ?></th>
                            </tfoot>
                    	</table>  
                    </td>
                    <td width="50"></td>
                    <td width="450" valign="top"><b><u>Fabric Production Charge</u></b>
                    	<?php
							$tot_prod_charge=$tot_knit_charge+$tot_yarn_dye_charge+$tot_dye_finish_charge;	  
						?>
                    	<table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                            <thead>
                           		<th width="30">SL</th>
                                <th width="160">Particulars</th>
                                <th width="140">Amount</th>
                                <th>Percentage</th>
                            </thead>
                            <tr bgcolor="#E9F3FF">
                            	<td>1</td>
                                <td>Knitting Charge</td>
                                <td align="right"><?php echo number_format($tot_knit_charge,2); ?></td>
                                <td align="right"><?php echo number_format((($tot_knit_charge*100)/$tot_prod_charge),2); ?></td>
                            </tr>
                            <tr bgcolor="#FFFFFF">
                            	<td>2</td>
                                <td>Yarn Dyeing Charge</td>
                                <td align="right"><?php echo number_format($tot_yarn_dye_charge,2); ?></td>
                               <td align="right"><?php echo number_format((($tot_yarn_dye_charge*100)/$tot_prod_charge),2); ?></td>
                            </tr>
                            <tr bgcolor="#E9F3FF">
                            	<td>3</td>
                                <td>Dyeing & Finishing Charge</td>
                                <td align="right"><?php echo number_format($tot_dye_finish_charge,2); ?></td>
                                <td align="right"><?php echo number_format((($tot_dye_finish_charge*100)/$tot_prod_charge),2); ?></td>
                            </tr>
                            <tfoot>
                            	<th colspan="2" align="right">Total</th>
                                <th align="right"><?php echo number_format($tot_prod_charge,2); ?></th>
                                <th align="right"><?php echo number_format((($tot_prod_charge*100)/$tot_prod_charge),2); ?></th>
                            </tfoot>
                    	</table>        
                    </td>
                </tr>
            </table>
            <br />
            <table>
                <tr>
                	<?php
					$tot_order_value=number_format($tot_order_value,2,'.','');
					$fabric_cost_summary=number_format($fabric_cost_summary,2,'.','');
					$grand_tot_trims_cost=number_format($grand_tot_trims_cost,2,'.','');
					$grand_tot_embell_cost=number_format($grand_tot_embell_cost,2,'.','');
					$comm_cost_summary=number_format($comm_cost_summary,2,'.','');
					$other_cost_summary=number_format($other_cost_summary,2,'.','');
					$grand_tot_cm_cost=number_format($grand_tot_cm_cost,2,'.','');
					$margin_value=number_format($margin_value,2,'.','');

					$chart_data_qnty="Order Value;".$tot_order_value."\nFabric Cost;".$fabric_cost_summary."\nTrims Cost;".$grand_tot_trims_cost."\nEmbellishment Cost;".$grand_tot_embell_cost."\nCommission Cost;".$comm_cost_summary."\nOthers Cost;".$other_cost_summary."\nCM Cost;".$grand_tot_cm_cost."\nMargin;".$margin_value."\n";
					 
					?>
                    <input type="hidden" id="graph_data" value="<?php echo substr($chart_data_qnty,0,-1); ?>"/>
                    <td colspan="5" id="chartdiv"></td>
                </tr>
            </table>
            </fieldset>
        </div>
<?php
	}
	 
	echo "$total_data****requires/$filename****$tot_rows";
	exit();	
}

if($action=="trims_cost")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
?>
    <div>
        <fieldset style="width:600px;">
        <div style="width:600px" align="center">	
            <table class="rpt_table" width="470" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="120">Job No</th>
                    <th width="200">Order No</th>
                    <th>Order Qnty</th>
                </thead>
                <tr bgcolor="#FFFFFF">
                	<td align="center"><?php echo $job_no; ?></td>
                    <td><?php echo $po_no; ?></td>
                    <td align="right"><?php echo number_format($po_qnty,0); ?></td>
                </tr>
            </table>
            <table style="margin-top:10px" class="rpt_table" width="600" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                	<th width="40">SL</th>
                    <th width="130">Item Name</th>
                    <th width="90">Cons/Dzn</th>
                    <th width="80">Rate</th>
                    <th width="110">Trims Cost/Dzn</th>
                    <th>Total Trims Cost</th>
                </thead>
            </table>
            </div>
            <div style="width:620px; max-height:250px; overflow-y:scroll" id="scroll_body">
                <table class="rpt_table" width="600" cellpadding="0" cellspacing="0" border="1" rules="all">
                	<?php
					$costing_per=return_field_value("costing_per","wo_pre_cost_mst","job_no='$job_no' and status_active=1 and is_deleted=0");
                        
					$dzn_qnty=0;
					if($costing_per==1)
					{
						$dzn_qnty=12;
					}
					else if($costing_per==3)
					{
						$dzn_qnty=12*2;
					}
					else if($costing_per==4)
					{
						$dzn_qnty=12*3;
					}
					else if($costing_per==5)
					{
						$dzn_qnty=12*4;
					}
					else
					{
						$dzn_qnty=1;
					}
					
					$sql="select a.trim_group, a.rate, b.cons from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b where a.id=b.wo_pre_cost_trim_cost_dtls_id and b.po_break_down_id='$po_id' and a.job_no='$job_no' and a.status_active=1 and a.is_deleted=0";
					$trimsArray=sql_select($sql);
					$i=1;
					foreach($trimsArray as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="40"><?php echo $i; ?></td>
							<td width="130"><div style="width:130px; word-wrap:break-word"><?php echo $item_library[$row[csf('trim_group')]]; ?></div></td>
							<td width="90" align="right"><?php echo number_format($row[csf('cons')],2); ?></td>
							<td width="80" align="right"><?php echo number_format($row[csf('rate')],2); ?></td>
							<td width="110" align="right">
								<?php
                                    $trims_cost_per_dzn=$row[csf('cons')]*$row[csf('rate')]; 
                                    echo number_format($trims_cost_per_dzn,2);
									$tot_trims_cost_per_dzn+=$trims_cost_per_dzn; 
                                ?>
                            </td>
							<td align="right">
								<?php
                                	$trims_cost=($po_qnty/$dzn_qnty)*$trims_cost_per_dzn;
									echo number_format($trims_cost,2);
									$tot_trims_cost+=$trims_cost;
                                ?>
                            </td>
						</tr>
					<?php
					$i++;
					}
					?>
                	<tfoot>
                        <th colspan="4">Total</th>
                        <th><?php echo number_format($tot_trims_cost_per_dzn,2); ?></th>
                        <th><?php echo number_format($tot_trims_cost,2); ?></th>
                    </tfoot>    
                </table>
            </div>
        </fieldset>
    </div>
<?php
}

if($action=="other_cost")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
?>
    <div align="center">
        <fieldset style="width:600px;">
            <table class="rpt_table" width="470" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="120">Job No</th>
                    <th width="200">Order No</th>
                    <th>Order Qnty</th>
                </thead>
                <tr bgcolor="#FFFFFF">
                	<td align="center"><?php echo $job_no; ?></td>
                    <td><?php echo $po_no; ?></td>
                    <td align="right"><?php echo number_format($po_qnty,0); ?></td>
                </tr>
            </table>
            <table style="margin-top:10px" class="rpt_table" width="470" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                    <th width="200">Particulars</th>
                    <th width="90">Cost/Dzn</th>
                    <th>Total Cost</th>
                </thead>
				<?php
                $costing_per=return_field_value("costing_per","wo_pre_cost_mst","job_no='$job_no' and status_active=1 and is_deleted=0");
                    
                $dzn_qnty=0;
                if($costing_per==1)
                {
                    $dzn_qnty=12;
                }
                else if($costing_per==3)
                {
                    $dzn_qnty=12*2;
                }
                else if($costing_per==4)
                {
                    $dzn_qnty=12*3;
                }
                else if($costing_per==5)
                {
                    $dzn_qnty=12*4;
                }
                else
                {
                    $dzn_qnty=1;
                }
                    
                $sql="select common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='$job_no' and status_active=1 and is_deleted=0";
                $fabriccostArray=sql_select($sql);
                ?>
                <tr bgcolor="#E9F3FF">
                    <td>Commercial Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('comm_cost')],2); ?></td>
                    <td align="right">
                        <?php
                            $comm_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('comm_cost')]; 
                            echo number_format($comm_cost,2);
                        ?>
                    </td>
                </tr>
                <tr bgcolor="#FFFFFF">
                    <td>Lab Test Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('lab_test')],2); ?></td>
                    <td align="right">
                        <?php
                            $lab_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('lab_test')]; 
                            echo number_format($lab_cost,2);
                        ?>
                    </td>
                </tr>
                 <tr bgcolor="#E9F3FF">
                    <td>Inspection Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('inspection')],2); ?></td>
                    <td align="right">
                        <?php
                            $inspection_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('inspection')]; 
                            echo number_format($inspection_cost,2);
                        ?>
                    </td>
                </tr>
                <tr bgcolor="#FFFFFF">
                    <td>Freight Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('freight')],2); ?></td>
                    <td align="right">
                        <?php
                            $freight_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('freight')]; 
                            echo number_format($freight_cost,2);
                        ?>
                    </td>
                </tr>
                <tr bgcolor="#E9F3FF">
                    <td>Common OH Cost</td>
                    <td align="right"><?php echo number_format($fabriccostArray[0][csf('common_oh')],2); ?></td>
                    <td align="right">
                        <?php
                            $common_oh_cost=($po_qnty/$dzn_qnty)*$fabriccostArray[0][csf('common_oh')]; 
                            echo number_format($common_oh_cost,2);
							
							$tot_cost_per_dzn=$fabriccostArray[0][csf('comm_cost')]+$fabriccostArray[0][csf('lab_test')]+$fabriccostArray[0][csf('inspection')]+$fabriccostArray[0][csf('freight')]+$fabriccostArray[0][csf('common_oh')];
							$tot_cost=$comm_cost+$lab_cost+$inspection_cost+$freight_cost+$common_oh_cost;
                        ?>
                    </td>
                </tr>
                <tfoot>
                    <th>Total</th>
                    <th><?php echo number_format($tot_cost_per_dzn,2); ?></th>
                    <th><?php echo number_format($tot_cost,2); ?></th>
                </tfoot>    
            </table>
        </fieldset>
    </div>
<?php
}
disconnect($con);
?>