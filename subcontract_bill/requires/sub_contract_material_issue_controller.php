<?php
include('../../includes/common.php');
session_start();

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }

$trans_Type="2";

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 140, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", $selected, "" );
	exit();		 
}

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_party_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "","","","","","",5 );
	
	exit();	 
}
if ($action=="load_drop_down_buyer_pop")
{
	echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data' $buyer_cond  and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "" );
	exit();
} 

if($action=="load_drop_down_company_supplier")
{
	$data = explode("**",$data);
	if($data[0]==3)
	{
		//echo create_drop_down( "cbo_company_supplier", 140, "select id, supplier_name from lib_supplier where find_in_set(2,party_type) and find_in_set($data[1],tag_company) and status_active=1 and is_deleted=0","id,supplier_name", 1, "--Select Supplier--", 1, "" );
		echo create_drop_down( "cbo_company_supplier", 140, "select a.id, a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type=2 and  a.status_active=1 and a.is_deleted=0 order by a.supplier_name","id,supplier_name", 1, "--Select Supplier--", $selected, "" );
	}
	else if($data[0]==1)
	{
		if($data[1]!="")
		{
			 echo create_drop_down( "cbo_company_supplier", 140,"select id,company_name from lib_company where is_deleted=0 and status_active=1 order by company_name","id,company_name", 1, "--Select Supplier--", $data[1], "",1 );	
		}
		else
		{
			 echo create_drop_down( "cbo_company_supplier", 140, $blank_array,"", 1, "--Select Company--", $selected, "",0 );
		}
	}
	else
	{
		echo create_drop_down( "cbo_company_supplier", 140, $blank_array,"", 1, "--Select Supplier--", $selected, "",0 );
	}
	exit();	
}

if($action=="load_drop_down_issueto")
{
	echo create_drop_down( "cbo_company_supplier", 140, "select id, company_name from lib_company where is_deleted=0 and status_active=1 order by company_name","id,company_name", 1, "-- Select Comp/Supp --", $data, "",1);
	exit();
}

if ($action=="issue_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	?>
	<script>
		function js_set_value(id)
		{ 
			$("#hidden_mst_id").val(id);
			document.getElementById('selected_issue').value=id;
			parent.emailwindow.hide();
		}		
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="issueidsearch_1"  id="issueidsearch_1" autocomplete="off">
                <table width="750" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="140">Company Name</th>
                        <th width="70">Source</th>
                        <th width="140">Supplier Name</th>
                        <th width="80">Issue ID</th>
                        <th width="60">Year</th>
                        <th width="170">Date Range</th>
                        <th><input type="reset" name="res" id="res" value="Reset" style="width:70px" class="formbutton" onClick="reset_form('issueidsearch_1','search_div','','','','');" /></th>           
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="selected_issue"><?php $data=explode("_",$data); ?>  <!--  echo $data;-->
								<?php 
									echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $data[0], "");//load_drop_down( 'sub_contract_material_issue_controller', this.value, 'load_drop_down_buyer_pop', 'buyer_td' ); 
								?>
                            </td>
                            <td>
                            	<?php
								   echo create_drop_down( "cbo_source", 70, $knitting_source,"", 1, "-- Select Source --", 0, "load_drop_down( 'sub_contract_material_issue_controller', this.value+'**'+$('#cbo_company_name').val(), 'load_drop_down_company_supplier', 'issue_to_td' );",0,'1,3' );
							   ?>
                            </td>
                            <td id="issue_to_td">
								<?php 
									echo create_drop_down( "cbo_company_supplier", 140, $blank_array,"", 1, "--Select Supplier--", $data[2], "" ); 
                                ?>
                            </td>
                            <td>
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes_numeric" style="width:75px" />
                            </td>
                            <td> 
                                <?php
                                    $selected_year=date("Y");
                                    echo create_drop_down( "cbo_year", 60, $year,"", 1, "-Year-", $selected_year, "",0 );
                                ?>
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:65px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:65px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_year').value+'_'+document.getElementById('cbo_company_supplier').value+'_'+document.getElementById('cbo_source').value, 'create_issue_search_list_view', 'search_div', 'sub_contract_material_issue_controller', 'setFilterGrid(\'tbl_po_list\',-1)')" style="width:70px;" /></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="center" height="40" valign="middle">
								<?php echo load_month_buttons(1);  ?>
                                <input type="hidden" name="hidden_mst_id" id="hidden_mst_id" class="datepicker" style="width:70px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7" align="center" valign="top" id=""><div id="search_div"></div> </td>
                        </tr>
                    </tbody>
                </table>  
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if($action=="create_issue_search_list_view")
{
	$data=explode('_',$data);
	//echo $data[2];
	if ($data[0]!=0) $company=" and a.company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!="" &&  $data[2]!="") $issue_date_cond = "and a.subcon_date between '".change_date_format($data[1],"", "",1)."' and '".change_date_format($data[2],"", "",1)."'"; else $issue_date_cond ="";
	if ($data[3]!='') $issue_id_cond=" and a.prefix_no_num= '$data[3]'"; else $issue_id_cond="";
	if ($data[5]!=0) $party_cond=" and a.party_id= '$data[5]'"; else $party_cond="";
	if ($data[6]!=0) $source_cond=" and a.prod_source= '$data[6]'"; else $source_cond="";
	//$trans_Type="issue";
	if($db_type==0)
	{
		$sql= "select a.id, a.sys_no, a.prefix_no_num, YEAR(a.insert_date) as year, a.location_id, a.prod_source, a.party_id, a.subcon_date, a.chalan_no, a.remarks, a.status_active, group_concat(b.order_id) as order_id from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.company_id=$data[0] and a.trans_type=2 and a.status_active=1 $issue_id_cond $issue_date_cond $party_cond  $source_cond group by  a.id, a.sys_no, a.prefix_no_num, a.insert_date, a.location_id, a.prod_source, a.party_id, a.subcon_date, a.chalan_no, a.remarks, a.status_active order by a.id DESC";
	}
	else if($db_type==2)
	{
		$sql= "select a.id, a.sys_no, a.prefix_no_num, TO_CHAR(a.insert_date,'YYYY') as year, a.location_id, a.prod_source, a.party_id, a.subcon_date, a.chalan_no, a.remarks, a.status_active, listagg(b.order_id,',') within group (order by b.order_id) as order_id from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.company_id=$data[0] and a.trans_type=2 and a.status_active=1 $issue_id_cond $issue_date_cond $party_cond  $source_cond group by  a.id, a.sys_no, a.prefix_no_num, a.insert_date, a.location_id, a.prod_source, a.party_id, a.subcon_date, a.chalan_no, a.remarks, a.status_active order by a.id DESC";
	}
	//echo $sql;
	$result = sql_select($sql);
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$company_party_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$po_array=array();
	$po_sql="select id, order_no, cust_style_ref from  subcon_ord_dtls where status_active=1 and is_deleted=0";
	$result_po = sql_select($po_sql);
	foreach($result_po as $row)
	{
		$po_array[$row[csf("id")]]['po']=$row[csf("order_no")];
		$po_array[$row[csf("id")]]['style']=$row[csf("cust_style_ref")];
	}
	?> 
    <script>
		$(document).ready(function(e) {
            setFilterGrid('tbl_po_list',-1);
        });

	</script>   
    <div>
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table">
            <thead>
                <th width="30" >SL</th>
                <th width="60" >Issue ID</th>
                <th width="60" >Year</th>
                <th width="100" >Prod Source</th>
                <th width="120" >Issue To</th>
                <th width="65" >Issue Date</th>
                <th width="70" >Issue Challan</th>
                <th width="110">Style</th>
                <th>Order</th>
            </thead>
     	</table>
     </div>
     <div style="width:750px; max-height:270px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="730" class="rpt_table" id="tbl_po_list">
			<?php
			$i=1;
            foreach( $result as $row )
            {
                if ($i%2==0)  $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$order_no=''; $style_name="";
				$order_id=array_unique(explode(",",$row[csf("order_id")]));
				foreach($order_id as $val)
				{
					if($order_no=="") $order_no=$po_array[$val]['po']; else $order_no.=", ".$po_array[$val]['po'];
					if($style_name=="") $style_name=$po_array[$val]['style']; else $style_name.=", ".$po_array[$val]['style'];
				}
				
				if($row[csf("prod_source")]==1) $prod_company=$company_arr[$row[csf("party_id")]]; else $prod_company=$company_party_arr[$row[csf("party_id")]];
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value(<?php echo $row[csf("id")];?>);" > 
						<td width="30" align="center"><?php echo $i; ?></td>
						<td width="60" align="center"><?php echo $row[csf("prefix_no_num")]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf("year")]; ?></td>		
						<td width="100"><?php echo $knitting_source[$row[csf("prod_source")]];  ?></td>	
						<td width="120"><?php echo $prod_company; ?></td>
						<td width="65"><?php echo change_date_format($row[csf("subcon_date")]);?> </td>	
						<td width="70" align="center"><?php echo $row[csf("chalan_no")]; ?></td>
                        <td width="110"><p><?php echo $style_name; ?></p></td>
                        <td><p><?php echo $order_no; ?><p></td>
					</tr>
				<?php 
				$i++;
            }
   		?>
			</table>
		</div> 
	<?php	
	exit();		
}

if ($action=="load_php_data_to_form")
{
	//echo "select id,sys_no,company_id,location_id,prod_source,party_id,subcon_date,chalan_no,remarks,status_active from sub_material_mst where id='$data'";die;
	$nameArray=sql_select( "select id,sys_no,company_id,location_id,prod_source,party_id,subcon_date,chalan_no,remarks,status_active from sub_material_mst where id='$data'" ); 
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_issue_no').value 		= '".$row[csf("sys_no")]."';\n";
		echo "document.getElementById('cbo_company_name').value 	= '".$row[csf("company_id")]."';\n";
		echo "$('#cbo_company_name').attr('disabled','true')".";\n"; 
		//load_drop_down( 'requires/sub_contract_material_issue_controller', this.value, 'load_drop_down_company_supplier', 'issue_to_td' );		 
		echo "load_drop_down( 'requires/sub_contract_material_issue_controller', document.getElementById('cbo_company_name').value, 'load_drop_down_location', 'location_td' );\n";
		echo "load_drop_down( 'requires/sub_contract_material_issue_controller', $('#cbo_company_name').val(), 'load_drop_down_issueto', 'issue_to_td' );";
		echo "document.getElementById('cbo_location_name').value	= '".$row[csf("location_id")]."';\n";
		echo "document.getElementById('cbo_source').value			= '".$row[csf("prod_source")]."';\n"; 
		//echo "load_drop_down( 'requires/sub_contract_material_issue_controller', document.getElementById('cbo_source').value, 'load_drop_down_company_supplier','issue_to_td');\n";
		echo "document.getElementById('cbo_company_supplier').value	= '".$row[csf("party_id")]."';\n"; 
		echo "document.getElementById('txt_issue_date').value 		= '".change_date_format($row[csf("subcon_date")])."';\n";   
		echo "document.getElementById('txt_issue_challan').value	= '".$row[csf("chalan_no")]."';\n"; 
		echo "document.getElementById('txt_remarks').value			= '".$row[csf("remarks")]."';\n"; 
	    echo "document.getElementById('update_id').value            = '".$row[csf("id")]."';\n";
		echo "set_button_status(0, '".$_SESSION['page_permission']."','fnc_material_issue',1,1);\n";
	}
	exit();	
}

if ($action=="order_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	?>
	<script>
		function js_set_value(id)
		{ 
			$("#hidden_mst_id").val(id);
			document.getElementById('selected_order').value=id;
			parent.emailwindow.hide();
 		}
    </script>
    </head>
    <body>
    <div align="center" style="width:100%;" >
    <form name="searchjobfrm_1"  id="searchjobfrm_1" autocomplete="off">
        <table width="750" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
        	<thead>
                <tr>
                    <th colspan="6"><?php echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" ); ?></th>
                </tr>
                <tr>               	 
                    <th width="140">Company Name</th>
                    <th width="140">Party Name</th>
                    <th width="170">Date Range</th>
                    <th width="100">Search Job</th>
                    <th width="100">Search Order</th>
                    <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>
                </tr> 
            </thead>
                 <tbody>
                    <tr>
                        <td> <input type="hidden" id="selected_order">  
                            <?php   
                                $data=explode("_",$data);
                                echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $data[1],"",1 );
                            ?>
                        </td>
                        <td id="buyer_td">
                            <?php echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[1]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --",$data[3], "",'' );   	 
                            ?>
                        </td>
                        <td align="center">
                            <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                            <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                        </td> 
                        <td >
                             <input type="text" name="txt_search_job" id="txt_search_job" class="text_boxes" style="width:100px" placeholder="Job" />
                        </td>
                        <td align="center" id="search_by_td">
                            <input type="text" name="txt_search_order" id="txt_search_order" class="text_boxes" style="width:100px" placeholder="Order" />
                        </td>
                        <td align="center">
                            <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_job').value+'_'+document.getElementById('txt_search_order').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_order_search_list_view', 'search_div', 'sub_contract_material_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center" height="40" valign="middle">
                            <?php echo load_month_buttons(1);  ?>
                            <input type="hidden" name="hidden_mst_id" id="hidden_mst_id" class="text" style="width:70px">
                        </td>
                    </tr>
                </tbody>            
            </table>
            <div id="search_div"></div>    
        </form>
       </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?php
	exit();
}

/* ===========   Order PopUP End Here ====================== */
if($action=="create_order_search_list_view")
{	
$data=explode('_',$data);
	$search_job=str_replace("'","",$data[4]);
	$search_order=trim(str_replace("'","",$data[5]));
	$search_type =$data[6];
	
	if ($data[0]!=0) $company=" and company_id='$data[0]'"; else  $company="";
	if ($data[1]!=0) $buyer=" and party_id='$data[1]'"; else $buyer="";
	
	if($search_type==1)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num='$search_job'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no='$search_order'";
	}
	else if($search_type==4 || $search_type==0)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '%$search_job%'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '%$search_order%'";
	}
	else if($search_type==2)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '$search_job%'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '$search_order%'";
	}
	else if($search_type==3)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '%$search_job'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '%$search_order'";
	}	
	
	if($db_type==0)
	{ 
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2],'yyyy-mm-dd')."' and '".change_date_format($data[3],'yyyy-mm-dd')."'"; else $order_rcv_date ="";
	}
	else
	{
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2], "", "",1)."' and '".change_date_format($data[3], "", "",1)."'"; else $order_rcv_date ="";
	}
	
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$arr=array (3=>$comp);
	if($db_type==0)
	{
		$sql= "select a.id, b.id as ord_id, a.subcon_job, a.job_no_prefix_num, year(a.insert_date)as year, a.company_id, a.location_id, a.party_id, a.status_active, b.id, b.order_no, b.order_rcv_date, b.delivery_date, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $order_rcv_date $company $buyer $search_job_cond $search_order_cond order by a.id DESC";
	}
	else if($db_type==2)
	{
		$sql= "select a.id, b.id as ord_id, a.subcon_job, a.job_no_prefix_num, TO_CHAR(a.insert_date,'YYYY') as year, a.company_id, a.location_id, a.party_id, a.status_active, b.id, b.order_no, b.order_rcv_date, b.delivery_date, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $order_rcv_date $company $buyer $search_job_cond $search_order_cond order by a.id DESC";
	}
	//echo $sql;die;
	echo  create_list_view("list_view", "Job No,Year,Order No,Company,Ord Receive Date,Delivery Date","70,80,150,150,100","750","250",0,$sql, "js_set_value","ord_id","",1,"0,0,0,company_id,0,0",$arr,"job_no_prefix_num,year,order_no,company_id,order_rcv_date,delivery_date", "",'','0,0,0,0,3,3') ;
	exit();	
}

if($action=="load_php_data_to_form_order")
{
	$nameArray=sql_select( "select id,order_no from subcon_ord_dtls where id='$data'" );
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txtorderno_1').value	= '".$row[csf("order_no")]."';\n"; 
		echo "document.getElementById('order_no_id').value	= '".$row[csf("id")]."';\n"; 
	}
	exit();
}

if ($action=="material_description_popup")
{
	echo load_html_head_contents("Material Description Form", "../../", 1, 1,'',1,'');
	extract($_REQUEST);
	$ex_data=explode('_',$data);
	$order_array=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
	?>
    <script>
		  function js_set_value(id,val,dia)
		  {
			  //alert (val)
		  	  $("#description_id").val(id);
			  $("#material_description").val(val);
			  $("#dia").val(dia);
			  parent.emailwindow.hide();
		  }
	</script>
    </head>
    <body>
            <input type="hidden" name="description_id" id="description_id">
        	<input type="hidden" name="material_description" id="material_description">
            <input type="hidden" name="dia" id="dia">

    <div style="width:650px;">
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
            	<tr>
                	<th colspan="9" align="center">RECEIVED ITEM, Order No :<?php echo $order_array[$ex_data[2]]; ?></th>
                </tr>
                <tr>
                    <th width="30" >SL</th>
                    <th width="220" >Material Description</th>
                    <th width="60" >UOM</th>
                    <th width="60" >Dia</th>
                    <th width="60" >Rec. Qty</th>
                    <th width="60" >Roll/Bag</th>
                    <th width="60" >Cone</th>
                    <th width="60" >Iss. Qty</th>
                    <th width="80" >Balance</th>
                </tr>
            </thead>
     	</table>
     </div>
     <div style="width:667px; max-height:80px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table" id="tbl_po_list">
			<?php
			if($data!=0){ $category_id=" and a.item_category_id=$ex_data[0]";}else{echo "Please Select item category First."; die; }
			if($db_type==0)
			{
				$group_cond=" group by a.material_description, a.subcon_uom, a.grey_dia";
				$id="a.id as id";
			}
			else if($db_type==2)
			{
				$group_cond=" group by a.material_description, a.subcon_uom, a.grey_dia";
				$id="log_concat(a.id) as id";
			}
			$issue_balance_array=array();
			$sql_issue="select $id, a.material_description, a.subcon_uom, sum(a.subcon_roll) as subcon_roll, sum(a.quantity) as quantity, sum(a.rec_cone) as rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.material_description<>' ' and b.trans_type in (2,3) and b.status_active=1 and b.is_deleted=0 and a.order_id='$ex_data[2]' $category_id $group_cond";
			$sql_issue_result=sql_select($sql_issue);
			foreach( $sql_issue_result as $row )
			{
				$issue_balance_array[$row[csf("material_description")]][$row[csf("subcon_uom")]][$row[csf("grey_dia")]]=$row[csf("quantity")];
			}
			//var_dump($issue_balance_array);
			
			$sql="select $id, a.material_description, a.subcon_uom, sum(a.subcon_roll) as subcon_roll, sum(a.quantity) as quantity, sum(a.rec_cone) as rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.material_description<>' ' and b.trans_type=1 and b.status_active=1 and b.is_deleted=0 and a.order_id='$ex_data[2]' $category_id $group_cond";
			
			$i=1;
			$nameArray=sql_select($sql);
            foreach( $nameArray as $row )
            {
                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value('<?php echo $row[csf("id")]; ?>','<?php echo $row[csf("material_description")]; ?>','<?php echo $row[csf("grey_dia")]; ?>');" > 
						<td width="30" align="center"><?php echo $i; ?></td>
						<td width="220" align="center"><?php echo $row[csf("material_description")]; ?></td>		
						<td width="60" align="center"><?php echo $unit_of_measurement[$row[csf("subcon_uom")]]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf("grey_dia")]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf("quantity")]; ?></td>	
                        <td width="60" align="center"><?php echo $row[csf("subcon_roll")]; ?></td>	
                        <td width="60" align="center"><?php echo $row[csf("rec_cone")]; ?></td>
                        <td width="60" align="center"><?php echo $issue_balance_array[$row[csf("material_description")]][$row[csf("subcon_uom")]][$row[csf("grey_dia")]]; ?></td>	
                        <td width="80" align="center"><?php echo $row[csf("quantity")]-$issue_balance_array[$row[csf("material_description")]][$row[csf("subcon_uom")]][$row[csf("grey_dia")]]; ?></td>	
					</tr>
				<?php 
				$i++;
            }
   		?>
			</table>
		</div>
        <br>
    <div style="width:650px;">
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
            	<tr>
                	<th colspan="7" align="center">ALL ITEM</th>
                </tr>
                <tr>
                    <th width="30" >SL</th>
                    <th width="220" >Material Description</th>
                    <th width="60" >UOM</th>
                    <th width="60" >Dia</th>
                    <th width="60" >Rec. Qty</th>
                    <th width="60" >Roll/Bag</th>
                    <th width="60" >Cone</th>
                </tr>
            </thead>
     	</table>
     </div>
        <div style="width:667px; max-height:200px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table" >
			<?php
			if($ex_data[0]!=0){ $category_id=" and a.item_category_id=$ex_data[0]";}else{echo "Please Select item category First."; die; }
			if($db_type==0)
			{
				$sql="select a.id, a.material_description, a.subcon_uom, sum(a.subcon_roll) as subcon_roll, sum(a.quantity) as quantity, sum(a.rec_cone) as rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and b.trans_type=1 and b.status_active=1 and b.is_deleted=0 and a.id not in (select a.id from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and b.trans_type=1 and a.material_description<>' ' and b.status_active=1 and b.is_deleted=0 and a.order_id='$ex_data[2]' $category_id ) $category_id $group_cond ";			
			}
			else if($db_type==2)
			{
				$sql="select log_concat(a.id) as id, a.material_description, a.subcon_uom, sum(a.subcon_roll) as subcon_roll, sum(a.quantity) as quantity, sum(a.rec_cone) as rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and b.trans_type=1 and b.status_active=1 and b.is_deleted=0 and a.id not in (select a.id from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and b.trans_type=1 and a.material_description<>' ' and b.status_active=1 and b.is_deleted=0 and a.order_id='$ex_data[2]' $category_id ) $category_id $group_cond ";//
			}
			$i=1;
			$nameArray=sql_select($sql);
            foreach( $nameArray as $row )
            {
                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value('<?php echo $row[csf("id")]; ?>','<?php echo $row[csf("material_description")]; ?>');" > 
						<td width="30" align="center"><?php echo $i; ?></td>
						<td width="220" align="center"><?php echo $row[csf("material_description")]; ?></td>		
						<td width="60" align="center"><?php echo $unit_of_measurement[$row[csf("subcon_uom")]]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf("grey_dia")]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf("quantity")]; ?></td>	
                        <td width="60" align="center"><?php echo $row[csf("subcon_roll")]; ?></td>	
                        <td width="60" align="center"><?php echo $row[csf("rec_cone")]; ?></td>	
					</tr>
				<?php 
				$i++;
            }
   		?>
			</table>
		</div>         
    </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?php
  exit();
}

if($action=="category_description_list_view")
{	
	$sql = "select a.id, a.mst_id, a.item_category_id, a.material_description, a.quantity, a.subcon_uom, a.subcon_roll, a.grey_dia, a.status_active, b.order_no from sub_material_dtls a,subcon_ord_dtls b where a.order_id=b.id and a.status_active=1 and a.mst_id='$data'"; 
		
	$arr=array(1=>$item_category,4=>$unit_of_measurement,7=>$row_status);
	echo  create_list_view("list_view", "Order No,Item Catg,Material Des.,Receive Qty,UOM,Roll,Dia,Status", "80,100,200,80,60,60,60,80","800","250",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form_dtls'", 1, "0,item_category_id,0,0,subcon_uom,0,0,status_active",$arr,"order_no,item_category_id,material_description,quantity,subcon_uom,subcon_roll,grey_dia,status_active", "requires/sub_contract_material_issue_controller","","0,0,0,0,0,0,0");
	exit();
}

if ($action=="load_php_data_to_form_dtls")
{
	$nameArray=sql_select( "select a.id, a.mst_id, a.item_category_id, a.material_description, a.quantity, a.subcon_uom, a.subcon_roll, a.rec_cone, a.grey_dia, a.status_active, b.id as order_tbl_id, b.order_no from sub_material_dtls a,subcon_ord_dtls b where a.order_id=b.id and a.id='$data'" );		  
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txtorderno_1').value 				= '".$row[csf("order_no")]."';\n";  
		echo "document.getElementById('cboitemcategory_1').value 			= '".$row[csf("item_category_id")]."';\n";  		  
		echo "document.getElementById('materialdescription_1').value 		= '".$row[csf("material_description")]."';\n";
		echo "document.getElementById('txtissuequantity_1').value			= '".$row[csf("quantity")]."';\n";
		echo "document.getElementById('cbouom_1').value					= '".$row[csf("subcon_uom")]."';\n";
		echo "document.getElementById('txt_roll').value					= '".$row[csf("subcon_roll")]."';\n";
		echo "document.getElementById('txt_cone').value					= '".$row[csf("rec_cone")]."';\n";  
		echo "document.getElementById('txt_dia').value					= '".$row[csf("grey_dia")]."';\n";  
		echo "document.getElementById('order_no_id').value				= '".$row[csf("order_tbl_id")]."';\n"; 
		echo "document.getElementById('update_id_dtl').value				= '".$row[csf("id")]."';\n"; 
		echo "change_uom('".$row[csf("item_category_id")]."');\n";				
		echo "set_button_status( 1,'".$_SESSION['page_permission']."', 'fnc_material_issue',1);\n";	
	}	
	exit();
}

if ($action=="load_php_data_for_dtls")
{
	$ex_data=explode('_',$data);
	$sql_rec="Select a.id, a.quantity, a.subcon_roll, a.rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.order_id=$ex_data[0] and a.id=$ex_data[1] and a.status_active=1 and b.trans_type=1 and a.is_deleted=0";
	$sql_result_rec = sql_select($sql_rec);
	$sql_iss="Select sum(a.quantity) as quantity, sum (a.subcon_roll) as subcon_roll, sum (a.rec_cone) as rec_cone from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.order_id=$ex_data[0] and b.trans_type=2 and a.status_active=1 and a.is_deleted=0";
	$sql_result_iss = sql_select($sql_iss);
	$tot_roll=0;
	$tot_dia=$sql_result_rec[0][csf('grey_dia')]*1;//-($sql_result_iss[0][csf('grey_dia')]*1)
	$tot_qty=$sql_result_rec[0][csf('quantity')]*1-($sql_result_iss[0][csf('quantity')]*1);
	$tot_roll=$sql_result_rec[0][csf('subcon_roll')]*1-($sql_result_iss[0][csf('subcon_roll')]*1);
	$tot_cone=$sql_result_rec[0][csf('rec_cone')]*1-($sql_result_iss[0][csf('rec_cone')]*1);
	echo "$('#txt_dia').val('".$tot_dia."');\n";
	echo "$('#txtissuequantity_1').attr('placeholder','".$tot_qty."');\n";
	echo "$('#txt_roll').attr('placeholder','".$tot_roll."');\n";
	echo "$('#txt_cone').attr('placeholder','".$tot_cone."');\n";
	exit();
}

/* =============== Save Update Delete Start Here  ===================*/
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$trans_Type="2";  
	// --------------------------------- Insert Start Here -------------------------
	if ($operation==0)   
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)";	
		}
		else if($db_type==2)
		{
			$year_cond=" and TO_CHAR(insert_date,'YYYY')";	
		}
		//echo $update_id;die;
		$new_issue_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'ISU', date("Y",time()), 5, "select id,prefix_no,prefix_no_num from sub_material_mst where company_id=$cbo_company_name and trans_type=2 $year_cond=".date('Y',time())." order by id desc", "prefix_no", "prefix_no_num" ));
		if(str_replace("'",'',$update_id)=="")
		{	
			$id=return_next_id( "id", "sub_material_mst",1); 		
			$field_array="id,sys_no,prefix_no,prefix_no_num,trans_type,company_id,location_id,prod_source,party_id,subcon_date,chalan_no,remarks,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_issue_no[0]."','".$new_issue_no[1]."','".$new_issue_no[2]."','".$trans_Type."',".$cbo_company_name.",".$cbo_location_name.",".$cbo_source.",".$cbo_company_supplier.",".$txt_issue_date.",".$txt_issue_challan.",".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$txt_issue_no=$new_issue_no[0];
		}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="company_id*location_id*prod_source*party_id*subcon_date*chalan_no*remarks*updated_by*update_date";				
			$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_source."*".$cbo_company_supplier."*".$txt_issue_date."*".$txt_issue_challan."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 	
			$txt_issue_no=$txt_issue_no; 
		}		
		$id1=return_next_id( "id", "sub_material_dtls",1); 
		$field_array2="id, mst_id, order_id, item_category_id, material_description, quantity, subcon_uom, subcon_roll, rec_cone, grey_dia, inserted_by, insert_date";		
		$data_array2="(".$id1.",".$id.",".$order_no_id.",".$cboitemcategory_1.",".$materialdescription_1.",".$txtissuequantity_1.",".$cbouom_1.",".$txt_roll.",".$txt_cone.",".$txt_dia.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
		
		if(str_replace("'",'',$update_id)=="")
		{
			//echo "INSERT INTO sub_material_mst (".$field_array.") VALUES ".$data_array; //die;  			
			$rID=sql_insert("sub_material_mst",$field_array,$data_array,0);//die;
		}
		else
		{
			$rID=sql_update("sub_material_mst",$field_array,$data_array,"id",$update_id,0); 
		}
		//echo "INSERT INTO sub_material_dtls (".$field_array2.") VALUES ".$data_array2; die;		
		$rID2=sql_insert("sub_material_dtls",$field_array2,$data_array2,1);//	die;	
		
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$txt_issue_no)."**".str_replace("'", '',$id)."**".str_replace("'",'',$id1);	
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$txt_issue_no)."**".str_replace("'", '',$id)."**".str_replace("'",'',$id1);	
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$txt_issue_no)."**".str_replace("'", '',$id)."**".str_replace("'",'',$id1);
			}
		}	
		disconnect($con);
		die;
	}	
	// ================================ Update Here ============================
	else if ($operation==1)   
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}		
		$field_array="company_id*location_id*prod_source*party_id*subcon_date*chalan_no*remarks*updated_by*update_date";				
		$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_source."*".$cbo_company_supplier."*".$txt_issue_date."*".$txt_issue_challan."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 	
		$rID=sql_update("sub_material_mst",$field_array,$data_array,"id",$update_id,0);  //die;	
				
		$field_array2="order_id*item_category_id*material_description*quantity*subcon_uom*subcon_roll*rec_cone*grey_dia*updated_by*update_date";		
		$data_array2="".$order_no_id."*".$cboitemcategory_1."*".$materialdescription_1."*".$txtissuequantity_1."*".$cbouom_1."*".$txt_roll."*".$txt_cone."*".$txt_dia."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 					
		$rID2=sql_update("sub_material_dtls",$field_array2,$data_array2,"id",$update_id_dtl,1);  		
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$txt_issue_no)."**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id_dtl);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$txt_issue_no)."**".str_replace("'", '',$update_id)."**".str_replace("'",'',$update_id_dtl);	
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$txt_issue_no)."**".str_replace("'", '',$update_id)."**".str_replace("'",'',$update_id_dtl);	
			}
		}	
		disconnect($con);
		die;
	}	
	// =================================== Delete Here=========================
	else if ($operation==2)   
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="updated_by*update_date*status_active*is_deleted";
		$data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
		$rID=sql_delete("sub_material_dtls",$field_array,$data_array,"id","".$update_id_dtl."",1);			
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$txt_issue_no)."**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id_dtl);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id_dtl);
			}
		}
		disconnect($con);
		die;
	}
}	
/* =============== Save Update Delete End Here  ===================*/

if($action=="material_issue_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$party_library=return_library_array( "select id, supplier_name from lib_supplier", "id","supplier_name"  );
	$order_array=return_library_array( "select id, order_no from subcon_ord_dtls", "id","order_no"  );
	
	$sql_mst="Select sys_no, prod_source, party_id, subcon_date, chalan_no, remarks from sub_material_mst where company_id=$data[0] and id='$data[1]' and trans_type=2 and status_active=1 and is_deleted=0";
	$dataArray=sql_select($sql_mst);
	?>
    <div style="width:930px;">
         <table width="930" cellspacing="0" align="right" border="0">
            <tr>
                <td colspan="6" align="center" style="font-size:x-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <?php
                        $nameArray=sql_select( "select plot_no, level_no, road_no, block_no, country_id, province, city, zip_code, email, website, vat_number from lib_company where id=$data[0] and status_active=1 and is_deleted=0"); 
                        foreach ($nameArray as $result)
                        { 
                        ?>
                            Plot No: <?php echo $result[csf('plot_no')]; ?> 
                            Level No: <?php echo $result[csf('level_no')]?>
                            Road No: <?php echo $result[csf('road_no')]; ?> 
                            Block No: <?php echo $result[csf('block_no')];?> 
                            City No: <?php echo $result[csf('city')];?> 
                            Zip Code: <?php echo $result[csf('zip_code')]; ?> 
                            Province No: <?php echo $result[csf('province')];?> 
                            Country: <?php echo $country_arr[$result[csf('country_id')]]; ?><br> 
                            Email Address: <?php echo $result[csf('email')];?> 
                            Website No: <?php echo $result[csf('website')];?> <br>
                            <b> Vat No : <?php echo $result[csf('vat_number')]; ?></b> <?php
							
                        }
                    ?> 
                </td>
            </tr>           
        	<tr>
                <td colspan="6" align="center" style="font-size:20px"><u><strong><?php echo $data[3]; ?></strong></u></td>
            </tr>
            <tr>
                <td width="130"><strong>Issue No :</strong></td> <td width="175"><?php echo $dataArray[0][csf('sys_no')]; ?></td>
                <td width="130"><strong>Issue Date: </strong></td><td width="175px"> <?php echo change_date_format($dataArray[0][csf('subcon_date')]); ?></td>
                <td width="130"><strong>Source :</strong></td> <td width="175"><?php echo $knitting_source[$dataArray[0][csf('prod_source')]]; ?></td>
            </tr>
             <tr>
             <?php
			 	if($dataArray[0][csf('prod_source')]==3)
				{
					$party_add=$dataArray[0][csf('party_id')];
					$nameArray=sql_select( "select address_1, web_site, email, country_id from lib_supplier where id=$party_add"); 
					foreach ($nameArray as $result)
					{ 
                    	$address="";
						if($result!="") $address=$result['address_1'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
					}
					$party_name=$party_library[$dataArray[0][csf('party_id')]].' : Address :- '.$address;;
				}
				else if($dataArray[0][csf('prod_source')]==1)
				{
					$party_name=$company_library[$dataArray[0][csf('party_id')]];
				}
				
			 ?>
             
                <td><strong>Issue To: </strong></td><td colspan="5"> <?php echo $party_name; ?></td>
            </tr>
            <tr>
            	<td>Issue Challan:</td><td><?php echo $dataArray[0][csf('chalan_no')]; ?></td>
                <td>Remarks:</td><td colspan="3"><?php echo $dataArray[0][csf('remarks')]; ?></td>
            </tr>
        </table>
         <br>
        <div style="width:100%;">
		<table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="30">SL</th>
                <th width="80" align="center">Order No</th>
                <th width="130" align="center">Item Category</th>
                <th width="180" align="center">Material Description</th> 
                <th width="60" align="center">Dia</th>
                <th width="80" align="center">Issue Qty</th>
                <th width="60" align="center">UOM</th>                   
                <th width="70" align="center">Roll/Bag</th>
                <th width="" align="center">Cone</th>
            </thead>         
         <?php	
			$i=1;
			$mst_id=$data[1];
			$sql_result =sql_select("select order_id, item_category_id, material_description, quantity, subcon_uom, subcon_roll, rec_cone, grey_dia from sub_material_dtls where mst_id='$mst_id' and status_active=1 and is_deleted=0"); 
			foreach($sql_result as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			?>
				<tr bgcolor="<?php echo $bgcolor; ?>"> 
                    <td><?php echo $i; ?></td>
                    <td><p><?php echo $order_array[$row[csf('order_id')]]; ?></p></td>
                    <td><p><?php echo $item_category[$row[csf('item_category_id')]]; ?></p></td>
                    <td><p><?php echo $row[csf('material_description')]; ?></p></td>
                    <td><p><?php echo $row[csf('grey_dia')]; ?></p></td>
                    <td align="right"><?php echo number_format($row[csf('quantity')],2,'.',''); $tot_issue_qty+=$row[csf('quantity')]; ?>&nbsp;</td>
                    <td align="center"><p><?php echo $unit_of_measurement[$row[csf('subcon_uom')]]; ?></p></td>
                    <td align="right"><?php echo $row[csf('subcon_roll')]; $tot_roll_qty+=$row[csf('subcon_roll')]; ?>&nbsp;</td>
                    <td align="right"><?php echo $row[csf('rec_cone')]; $tot_cone_qty+=$row[csf('rec_cone')]; ?>&nbsp;</td>
                </tr>
                <?php
                $i++;
			}
			?>
        	<tr> 
                <td align="right" colspan="5"><strong>Total</strong></td>
                <td align="right"><?php echo number_format($tot_issue_qty,2,'.',''); ?>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo $tot_roll_qty; ?>&nbsp;</td>
                <td align="right"><?php echo $tot_cone_qty; ?>&nbsp;</td>
			</tr>
        </table>
        <br>
		 <?php
            echo signature_table(61, $data[0], "930px");
         ?>
   </div>
   </div>
	<?php
}
?>
