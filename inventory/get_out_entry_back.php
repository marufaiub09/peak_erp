<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Out Order Entry
				
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	01-10-2013
Updated by 		: 	Kausar 	(Creating print report)	
Update date		: 	11-01-2014		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Receive Info","../", 1, 1, $unicode,1,1); 

?>
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
 
  	
// popup for SYSTEM ID----------------------
function openmypage_system()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();
	page_link='requires/get_out_entry_controller.php?action=system_id_popup&company='+company;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe',page_link,'Gate Out Id Popup', 'width=900px, height=350px, center=1, resize=0, scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var sysNumber=this.contentDoc.getElementById("hidden_sys_number").value; // system number
		if(sysNumber!="")
		{
			freeze_window(5);
			$("#txt_system_id").val(sysNumber);
			reset_form('','','txt_item_description*txt_quantity*cbo_uom*txt_rate*txt_amount','','','');
			get_php_form_data(sysNumber, "populate_master_from_data", "requires/get_out_entry_controller" );
			show_list_view(sysNumber,'show_dtls_list_view','list_container','requires/get_out_entry_controller','');
			set_button_status(0, permission, 'fnc_getout_entry',1,1);
			release_freezing();
		}
	}
}


function fnc_getout_entry(operation)
{
	if(operation==4)
	{
		 var report_title=$( "div.form_caption" ).html();
		 print_report( $('#cbo_company_name').val()+'*'+$('#txt_system_id').val()+'*'+report_title, "get_out_entry_print", "requires/get_out_entry_controller" ) 
		 return;
	}
	else if(operation==0 || operation==1 || operation==2)
	{
		if( form_validation('cbo_company_name*txt_sent_by*txt_sent_to*txt_receive_date*txt_start_hours*txt_start_minuties*txt_item_description*txt_quantity*cbo_uom*txt_rate','Company Name*Sent By*Sent To*Out Date*Out Time*Out Time*Item Description*Quantity*UOM*Rate')==false )
		{
			return;
		}	
		var dataString = "txt_system_id*cbo_company_name*cbo_sample*cbo_item_category*txt_sent_by*txt_sent_to*txt_receive_date*txt_challan_no*cbo_currency*txt_gate_pass_no*txt_start_hours*txt_start_minuties*txt_item_description*txt_quantity*cbo_uom*txt_rate*txt_amount*txt_remarks*update_id";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
		freeze_window(operation);
		http.open("POST","requires/get_out_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_getout_entry_reponse;
	}
}

function fnc_getout_entry_reponse()
{	
	if(http.readyState == 4) 
	{
		//alert(http.responseText);
		var response=trim(http.responseText).split('**');
		if(response[0]==0)
		{
			show_msg(trim(response[0]));
		}
		else if(response[0]==1)
		{
			show_msg(trim(response[0]));
		}
		else if(response[0]==2)
		{
		reset_form('getout_1','list_container','','','','');
		set_button_status(0, permission, 'fnc_getout_entry',1,1);
		show_msg(trim(response[0]));
		release_freezing();
			return;
		}
		else if(response[0]==10 || response[0]==15)
		{
			show_msg(trim(response[0]));
			release_freezing();
			return;
		}
		else if(response[0]==20)
		{
			alert(response[1]);
			release_freezing();
			return;
		}
		
		$("#txt_system_id").val(response[2]);
		$("#update_id").val(response[3]);
		show_list_view(response[2],'show_dtls_list_view','list_container','requires/get_out_entry_controller','');
		reset_form('','','txt_item_description*cbo_uom*txt_quantity*txt_rate*txt_amount*txt_remarks','','','');
		set_button_status(0, permission, 'fnc_getout_entry',1,1);
		release_freezing();
 	}
}

//amount calculate
function fn_calculate_amount()
{
	var quantity = $("#txt_quantity").val();
	var rate = $("#txt_rate").val();
	var  amount=quantity*rate*1;
	$("#txt_amount").val(number_format_common(amount,"","",7));
}

function fnc_move_cursor(val,id, field_id,lnth,max_val)
{
	var str_length=val.length;
	if(str_length==lnth)
	{
		$('#'+field_id).select();
		$('#'+field_id).focus();
	}
	
	if(val>max_val)
	{
		document.getElementById(id).value=max_val;
	}
}

function fnResetForm()
{
	reset_form('getout_1','list_container','','','','');
}

</script>
<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
      <?php echo load_freeze_divs ("../",$permission);  ?>
      <form name="getout_1" id="getout_1"  autocomplete="off">
    <div style="width:90%;">
    <fieldset style="width:950px; flot:left">
    <legend>Get Out</legend>
    <fieldset style="width:900px;">
        <table width="800" cellpadding="0" cellspacing="2" id="tbl_master">
            <tr>
                <td colspan="6" align="center"><b>Gate Out ID</b>
                    <input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:155px" placeholder="Double Click To Search" 				onDblClick="openmypage_system();" readonly />
                </td>
            </tr>
            <tr>
                <td width="120" align="right" class="must_entry_caption">Company Name</td>
                <td width="150">
					<?php 
                   	 echo create_drop_down( "cbo_company_name", 150, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected,"","0" );
					?>
                </td>
                <td width="120" align="right">Sample</td>
                <td width="150">
                	<?php 
						echo create_drop_down( "cbo_sample", 150, "select id,sample_name from lib_sample where status_active=1 order by sample_name","id,sample_name",1, "-- Select --", 0, "" ); 
					?> 
                </td>
                <td width="120" align="right" >Item Catagory</td>
                <td width="140">
					<?php 
						echo create_drop_down( "cbo_item_category", 140, $item_category,"", 1, "--- Select ---", $selected,0 ,"");
					?>
                </td>
            </tr>
            <tr>
                <td align="right" class="must_entry_caption">Sent By</td>
                <td >
                    <input type="text" name="txt_sent_by" id="txt_sent_by" class="text_boxes" style="width:140px;">
                </td>
                <td  align="right" class="must_entry_caption">Sent To</td>
                <td >
                	<input type="text" name="txt_sent_to" id="txt_sent_to" class="text_boxes" style="width:140px;">
                </td>
                <td align="right" class="must_entry_caption">Out Date</td>
                <td >
                	<input type="text" name="txt_receive_date" id="txt_receive_date" class="datepicker" style="width:130px;" placeholder="Select Date" />
                </td>
            </tr>
            <tr>
                <td align="right">Challan No</td>
                <td>
                	<input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes_numeric" style="width:140px;">
                </td>
                <td align="right">Currency</td>
                <td>
               		<?php 
						echo create_drop_down( "cbo_currency", 150, $currency,"", 1, "-- Select Currency --", $currencyID, "",0 );
					?>
                </td>
                <td align="right">Gate Pass No</td>
                <td >
                	<input type="text" name="txt_gate_pass_no" id="txt_gate_pass_no" class="text_boxes_numeric" style="width:130px;">
                </td>
            </tr>
            <tr>
                <td align="right" class="must_entry_caption">Out-Time</td>
                <td>
                	<input type="text" name="txt_start_hours" id="txt_start_hours" class="text_boxes_numeric" placeholder="Hours" style="width:60px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_hours','txt_start_minuties',2,23);" />
                    <input type="text" name="txt_start_minuties" id="txt_start_minuties" class="text_boxes_numeric" placeholder="Minutes" style="width:60px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_minuties','txt_start_date',2,59)" />
                </td>
                <td align="right"></td>
                <td colspan="3">
                	
                </td>
            </tr>
        </table>
    </fieldset>
    <br>
       
    <fieldset style="900px;">
    <legend>Item Part</legend>
        <table width="900" cellpadding="0" cellspacing="2" border="0" class="rpt_table" align="center">
            <thead>
                <th width="300" align="center" class="must_entry_caption">Item Description</th>
                <th width="60" align="center" class="must_entry_caption">Quantity</th>
                <th width="110" align="center" class="must_entry_caption">UOM</th>
                <th width="70" align="center" class="must_entry_caption">Rate</th>
                <th width="90" align="center">Amount</th>
                <th width="270" align="center">Rmarks</th>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" name="txt_item_description" id="txt_item_description" class="text_boxes" style="width:300px;"></td>
                    <td><input type="text" name="txt_quantity" id="txt_quantity" class="text_boxes_numeric" onKeyUp="fn_calculate_amount()" style="width:60px;"></td>
                    <td><?php echo create_drop_down( "cbo_uom", 110, $unit_of_measurement,"", 1, "-- Select UOM --", $selected, "",0 ); ?></td>
                    <td><input type="text" name="txt_rate" id="txt_rate" class="text_boxes_numeric" onKeyUp="fn_calculate_amount()" style="width:70px"></td>
                    <td><input type="text" name="txt_amount" id="txt_amount" class="text_boxes_numeric" style="width:90px" randomly disabled></td>
                    <td><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:270px"></td>
                </tr>
            </tbody>
        </table>
    </fieldset>
             
        <br>
        <table cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td align="center" colspan="6" valign="middle" class="button_container">
                    <input type="hidden" id="update_id" name="update_id" value="" />
						<?php 
                            echo load_submit_buttons( $permission, "fnc_getout_entry", 0,1,"fnResetForm()",1);
                        ?>
                </td>
            </tr> 
        </table> 
    </fieldset>
    <br>
    <fieldset style="width:870px;">
    <div style="width:850px;" id="list_container"></div>
    </fieldset>
    </div>
    </form>
    </div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>