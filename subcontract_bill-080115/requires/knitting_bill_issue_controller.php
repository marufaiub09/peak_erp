﻿<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 150, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "--Select Location--", $selected, "","","","","","",3 );
	exit();	 
}

if ($action=="load_drop_down_party_name")
{
	$data=explode('_',$data);
	if($data[1]==2)
	{
		echo create_drop_down( "cbo_party_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "show_list_view(document.getElementById('cbo_party_source').value+'***'+this.value,'knitting_delivery_list_view','knitting_info_list','requires/knitting_bill_issue_controller','setFilterGrid(\'tbl_list_search\',-1)');","","","","","",5 ); 
	}
	else if($data[1]==1)
	{	
		echo create_drop_down( "cbo_party_name", 150, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "-- Select Party --", $selected, "show_list_view(document.getElementById('cbo_party_source').value+'***'+this.value,'knitting_delivery_list_view','knitting_info_list','requires/knitting_bill_issue_controller','setFilterGrid(\'tbl_list_search\',-1)');","","","","","",5 ); 
	}
	else
	{
		echo create_drop_down( "cbo_party_name", 150, $blank_array,"", 1, "-- Select Party --", $selected, "",0,"","","","",5);
	}
	exit();
}

if ($action=="load_drop_down_party_name_popup")
{
	echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "","","","","","",5 );
	exit();
}

if ($action=="bill_no_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	$ex_data=explode('_',$data);
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('issue_id').value=id;
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="knittingbill_1"  id="knittingbill_1" autocomplete="off">
                <table width="630" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="150">Company Name</th>
                        <th width="150">Party Name</th>
                        <th width="80">Bill ID</th>
                        <th width="170">Date Range</th>
                        <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>           
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="issue_id">  
								<?   
									echo create_drop_down( "cbo_company_id", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $ex_data[0],"load_drop_down( 'knitting_bill_issue_controller', this.value, 'load_drop_down_party_name_popup', 'party_td' );",0 );
                                ?>
                            </td>
                            <td width="140" id="party_td">
								<?
									echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$ex_data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $ex_data[1], "","","","","","",5 );
                                ?> 
                            </td>
                            <td>
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes_numeric" style="width:75px" />
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value, 'kniting_bill_list_view', 'search_div', 'knitting_bill_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" height="40" valign="middle">
								<? echo load_month_buttons(1);  ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                    </tbody>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?
	exit();
}

if ($action=="kniting_bill_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company_name=" and company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $party_name_cond=" and party_id='$data[1]'"; else $party_name_cond="";
	if ($data[2]!="" &&  $data[3]!="") $return_date = "and bill_date between '".change_date_format($data[2], "mm-dd-yyyy", "/",1)."' and '".change_date_format($data[3], "mm-dd-yyyy", "/",1)."'"; else $return_date="";
	if ($data[4]!='') $bill_id_cond=" and prefix_no_num='$data[4]'"; else $bill_id_cond="";
	
	$company_id=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$location=return_library_array( "select id,location_name from lib_location",'id','location_name');
	$party_arr=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
	
	$arr=array (2=>$location,4=>$party_arr,5=>$knitting_source,6=>$bill_for);
	if($db_type==0)
	{
		$year_cond= "year(insert_date)as year";
	}
	else if($db_type==2)
	{
		$year_cond= "TO_CHAR(insert_date,'YYYY') as year";
	}
	
	$sql= "select id, bill_no, prefix_no_num, $year_cond, location_id, bill_date, party_id, party_source, bill_for from subcon_inbound_bill_mst where status_active=1 and process_id=2 $company_name $party_name_cond $return_date $bill_id_cond order by id DESC";
	
	echo  create_list_view("list_view", "Bill No,Year,Location,Bill Date,Party,Source,Bill For", "50,40,80,70,100,120,140","630","250",0, $sql , "js_set_value", "id", "", 1, "0,0,location_id,0,party_id,party_source,bill_for", $arr , "prefix_no_num,year,location_id,bill_date,party_id,party_source,bill_for", "knitting_bill_issue_controller","",'0,0,0,3,0,0,0') ;
	exit();
}

if ($action=="load_php_data_to_form_issue")
{
	$nameArray= sql_select("select id, bill_no, company_id, location_id, bill_date, party_id, party_source, attention, bill_for from subcon_inbound_bill_mst where id='$data'");
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_bill_no').value 					= '".$row[csf("bill_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "load_drop_down( 'requires/knitting_bill_issue_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_location', 'location_td' );\n";
		echo "document.getElementById('cbo_location_name').value			= '".$row[csf("location_id")]."';\n"; 
		echo "document.getElementById('txt_bill_date').value 				= '".change_date_format($row[csf("bill_date")])."';\n";   
		echo "document.getElementById('cbo_party_source').value				= '".$row[csf("party_source")]."';\n"; 
		echo "load_drop_down( 'requires/knitting_bill_issue_controller', document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_party_source').value, 'load_drop_down_party_name', 'party_td' );\n";
		echo "document.getElementById('cbo_party_name').value				= '".$row[csf("party_id")]."';\n"; 
		echo "document.getElementById('txt_attention').value				= '".$row[csf("attention")]."';\n"; 
		echo "document.getElementById('cbo_bill_for').value					= '".$row[csf("bill_for")]."';\n"; 
	    echo "document.getElementById('update_id').value            		= '".$row[csf("id")]."';\n";
	}	
	exit();
}

if ($action=="knitting_delivery_list_view")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	//echo $data;
	$data=explode('***',$data);
	$update_id=$data[2];
	$delv_id=implode(',',explode('_',$data[3]));
	if($data[0]==2)
	{
		?>
		<script>
		
		</script>
		</head>
		<body>
			<div style="width:100%;">
				<table cellspacing="0" cellpadding="0" border="1" rules="all" width="817px" class="rpt_table">
					<thead>
						<th width="30">SL</th>
						<th width="80">Challan No</th>
						<th width="80">Delivery Date</th>
						<th width="150">Order No</th>                    
						<th width="200">Fabric Description</th>
						<th width="100">Delivery Qty</th>
						<th width="100">Process</th>
						<th width="" >Currency</th>
					</thead>
			 </table>
        </div>
        <div style="width:820px;max-height:180px; overflow-y:scroll" id="sewing_production_list_view">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800px" class="rpt_table" id="tbl_list_search">
            <?
				$order_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
				$currency_arr=return_library_array( "select b.id, a.currency_id from  subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst",'id','currency_id');
                $i=1;
				if(!$update_id)
				{
					$sql="select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where b.bill_status=0 and a.id=b.mst_id and a.party_id='$data[1]' and b.process_id='2' and a.status_active=1 and a.is_deleted=0"; 
				}
				else
				{
					$sql="(select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id='2' and a.status_active=1 and b.bill_status='0')
					 union 
					 	(select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id='2' and b.id in ( $delv_id ) and a.status_active=1)";
				}
				//echo $sql;
				$sql_result =sql_select($sql);
                foreach($sql_result as $row)
				{
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
					<tr id="tr_<?  echo $row[csf('id')]; ?>" bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<? echo $row[csf('id')]."***".$currency_arr[$row[csf('order_id')]]; ?>');" > 
                        <td width="30" align="center"><? echo $i; ?></td>
                        <td width="80" align="center"><p><? echo $row[csf('challan_no')]; ?></p></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf('delivery_date')]); ?></td>
                        <td width="150" align="center"><? echo $order_arr[$row[csf('order_id')]]; ?></td>
                        <?
                        $process_id_val=$row[csf('process_id')];
						if($process_id_val==1 || $process_id_val==5)
						{
							$item_id_arr=$garments_item;
						}
						else
						{
							$item_id_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');	
						}
						?>
                        <td width="200" align="center"><? echo $item_id_arr[$row[csf('item_id')]]; ?></td>
                        <td width="100" align="right"><? echo $row[csf('delivery_qty')]; ?>&nbsp;</td>
                        <td width="100" align="center"><? echo $production_process[$row[csf('process_id')]]; ?></td>
                        <td width="" align="center"><? echo $currency[$currency_arr[$row[csf('order_id')]]]; ?>
                        <input type="hidden" id="currid<? echo $row[csf('id')]; ?>" value="<? echo $currency_arr[$row[csf('order_id')]]; ?>"></td>
                    </tr>
                    <?php
                    $i++;
                }
				?>
                </table>
         </div>
        <div>
            <table width="1000">
                <tr style="border:none">
                    <td align="center" colspan="11" >
                         <input type="button" id="show_button" align="middle" class="formbutton" style="width:100px" value="Close" onClick="window_close()" />
                    </td>
                </tr>
           </table>
      </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?
	}
	else if($data[0]==1)
	{
		?>
		</head>
		<body>
			<div style="width:100%;">
				<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1000px" class="rpt_table">
					<thead>
						<th width="30">SL</th>
						<th width="50" style="display:none">Receive No</th>
						<th width="70" >Challan No</th>
						<th width="70">Receive Date</th>
                        <th width="70">Job</th>
                        <th width="100">Style</th>                    
						<th width="100">Order No</th>
                        <th width="90">Body Part</th>
						<th width="180">Fabric Description</th>
                        <th width="60">Color Type</th>
						<th width="90">Receive Qty</th>
                        <th width="">Roll Qty</th>
					</thead>
			 </table>
        </div>
        <div style="width:1000px;max-height:180px; overflow-y:scroll" id="sewing_production_list_view">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="980px" class="rpt_table" id="tbl_list_search">
            <?
				//return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
				$bill_qty_array=array();
				$sql_bill="select challan_no, order_id, febric_description_id, body_part_id, item_id, sum(packing_qnty) as roll_qty, sum(delivery_qty) as bill_qty from subcon_inbound_bill_dtls where status_active=1 and is_deleted=0 group by challan_no, order_id, febric_description_id, body_part_id, item_id";
				$sql_bill_result =sql_select($sql_bill);
				foreach($sql_bill_result as $row)
				{
					$bill_qty_array[$row[csf('challan_no')]][$row[csf('order_id')]][$row[csf('item_id')]][$row[csf('body_part_id')]][$row[csf('febric_description_id')]]['qty']=$row[csf('bill_qty')];
					$bill_qty_array[$row[csf('challan_no')]][$row[csf('order_id')]][$row[csf('item_id')]][$row[csf('body_part_id')]][$row[csf('febric_description_id')]]['roll']=$row[csf('roll_qty')];
				}
				
				$product_dtls_arr=return_library_array( "select id, product_name_details from  product_details_master",'id','product_name_details');
				$currency_arr=return_library_array( "select b.id, a.currency_id from  subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst",'id','currency_id');
				
				$color_type_array=array();
				$color_type_sql="select a.color_type_id, a.lib_yarn_count_deter_id, b.po_break_down_id from  wo_pre_cost_fabric_cost_dtls a, wo_pre_cos_fab_co_avg_con_dtls b where a.id=b.pre_cost_fabric_cost_dtls_id and a.status_active=1 and is_deleted=0 and a.company_id='$data[1]'";
				$color_type_sql_result =sql_select($color_type_sql);
				foreach($color_type_sql_result as $row)
				{
					$color_type_array[$row[csf('po_break_down_id')]][$row[csf('lib_yarn_count_deter_id')]]['color_type']=$row[csf('color_type_id')];
				}
				
				$job_order_arr=array();
				$sql_job="Select a.job_no_prefix_num, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0";
				$sql_job_result =sql_select($sql_job);
				foreach($sql_job_result as $row)
				{
					$job_order_arr[$row[csf('id')]]['job']=$row[csf('job_no_prefix_num')];
					$job_order_arr[$row[csf('id')]]['style']=$row[csf('style_ref_no')];
					$job_order_arr[$row[csf('id')]]['po']=$row[csf('po_number')];
				}
                $i=1;
				if($db_type==0)
				{
					if(!$update_id)
					{
						$sql="select a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as roll_qty, c.po_breakdown_id, sum(c.quantity) as quantity from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and a.knitting_company='$data[1]' and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.receive_basis in (2,9) and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id order by a.recv_number_prefix_num"; 
						
					}
					else
					{
						$sql="(select  a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as roll_qty, c.po_breakdown_id, sum(c.quantity) as quantity from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and a.knitting_company='$data[1]' and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.receive_basis in (2,9) and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id )
						 union 
							(select a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as roll_qty, c.po_breakdown_id, sum(c.quantity) as quantity from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and a.knitting_company='$data[1]'  and c.id in ( $delv_id ) and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.receive_basis in (2,9) and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id ) order by  recv_number_prefix_num";
					}
				}
				else if($db_type==2)
				{
					if(!$update_id)
					{
						$sql="select a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as roll_qty, c.po_breakdown_id, sum(c.quantity) as quantity from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and a.knitting_company='$data[1]' and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.receive_basis in (2,9) and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id order by a.recv_number_prefix_num"; 
					}
					else
					{
						$sql="(select a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as roll_qty, c.po_breakdown_id, sum(c.quantity) as quantity from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and a.knitting_company='$data[1]' and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.receive_basis in (2,9) and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id )
						 union 
							(select a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as roll_qty, c.po_breakdown_id, sum(c.quantity) as quantity from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and a.knitting_company='$data[1]'  and c.id in ( $delv_id ) and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.receive_basis in (2,9) and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.challan_no, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id) order by recv_number_prefix_num";
					}
				}
				//echo $sql;
				$sql_result =sql_select($sql);
                foreach($sql_result as $row)
				{
					$bill_qty=$bill_qty_array[$row[csf('recv_number_prefix_num')]][$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('body_part_id')]][$row[csf('febric_description_id')]]['qty'];
					$roll_qty=$bill_qty_array[$row[csf('recv_number_prefix_num')]][$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('body_part_id')]][$row[csf('febric_description_id')]]['roll'];
					$avilable_qty=$row[csf('quantity')]-$bill_qty;
					$avilable_roll=$row[csf('roll_qty')]-$roll_qty;
					
					if(!$update_id)
					{
						if($avilable_qty>0)
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							$all_value=$row[csf('recv_number_prefix_num')].'_'.$row[csf('po_breakdown_id')].'_'.$row[csf('prod_id')].'_'.$row[csf('body_part_id')].'_'.$row[csf('febric_description_id')];
						?><!--<td width="70" style="display:none"><p><? //echo $row[csf('challan_no')]; ?></p></td>-->
							<tr id="tr_<?  echo $all_value; ?>" bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<? echo $all_value."***".'1'; ?>');" > 
								<td width="30" align="center"><? echo $i; ?></td>
								<td width="70" ><? echo $row[csf('recv_number_prefix_num')]; ?></td>
								
								<td width="70" align="center"><? echo change_date_format($row[csf('receive_date')]); ?></td>
								<td width="70"><? echo $job_order_arr[$row[csf('po_breakdown_id')]]['job']; ?></td>
								<td width="100"><p><? echo $job_order_arr[$row[csf('po_breakdown_id')]]['style']; ?></p></td>
								<td width="100"><p><? echo $job_order_arr[$row[csf('po_breakdown_id')]]['po']; ?></p></td>
								<td width="90"><p><? echo $body_part[$row[csf('body_part_id')]]; ?></p></td>
								<td width="180"><p><? echo $product_dtls_arr[$row[csf('prod_id')]]; ?></p></td>
								<td width="60"><p><? echo $color_type[$color_type_array[$row[csf('po_breakdown_id')]][$row[csf('febric_description_id')]]['color_type']]; ?></p></td>
								<td width="90" align="right"><? echo number_format($avilable_qty,2,'.',''); ?></td>
								<td width="" align="right"><? echo number_format($avilable_roll,2,'.',''); ?>
								<input type="hidden" style="width:40px" id="currid<? echo $all_value; ?>" value="<? echo 1; ?>"></td>
							</tr>
							<?php
							$i++;
						}
					}					
					else
					{
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$all_value=$row[csf('recv_number_prefix_num')].'_'.$row[csf('po_breakdown_id')].'_'.$row[csf('prod_id')].'_'.$row[csf('body_part_id')].'_'.$row[csf('febric_description_id')];
					?><!--<td width="70" style="display:none"><p><? //echo $row[csf('challan_no')]; ?></p></td>-->
						<tr id="tr_<?  echo $all_value; ?>" bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<? echo $all_value."***".'1'; ?>');" > 
							<td width="30" align="center"><? echo $i; ?></td>
							<td width="70" ><? echo $row[csf('recv_number_prefix_num')]; ?></td>
							
							<td width="70" align="center"><? echo change_date_format($row[csf('receive_date')]); ?></td>
							<td width="70"><? echo $job_order_arr[$row[csf('po_breakdown_id')]]['job']; ?></td>
							<td width="100"><p><? echo $job_order_arr[$row[csf('po_breakdown_id')]]['style']; ?></p></td>
							<td width="100"><p><? echo $job_order_arr[$row[csf('po_breakdown_id')]]['po']; ?></p></td>
							<td width="90"><p><? echo $body_part[$row[csf('body_part_id')]]; ?></p></td>
							<td width="180"><p><? echo $product_dtls_arr[$row[csf('prod_id')]]; ?></p></td>
							<td width="60"><p><? echo $color_type[$color_type_array[$row[csf('po_breakdown_id')]][$row[csf('febric_description_id')]]['color_type']]; ?></p></td>
							<td width="90" align="right"><? echo number_format($avilable_qty,2,'.',''); ?></td>
							<td width="" align="right"><? echo number_format($row[csf('roll_qty')],2,'.',''); ?>
							<input type="hidden" style="width:40px" id="currid<? echo $all_value; ?>" value="<? echo 1; ?>"></td>
						</tr>
						<?php
						$i++;
					}
				}
				?>
            </table>
        </div>
        <div>
            <table width="1000">
                <tr style="border:none">
                    <td align="center" colspan="11" >
                         <input type="button" id="show_button" align="middle" class="formbutton" style="width:100px" value="Close" onClick="window_close()" />
                    </td>
                </tr>
           </table>
      </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?	
	}
	exit();
}

if ($action=="load_php_dtls_form")  //new issue
{
	//echo $data;
	$data = explode("***",$data);
	//echo $data[0];
	
	$old_selected_id=explode(",",$data[0]); 
	$challan=""; $po_id=""; $item_id=""; $body_part_id=""; $febric_description_id=""; 
	foreach($old_selected_id as $val)
	{
		$selected_id_arr[]=$val;
		$ex_data=explode("_",$val);
		if($challan=="") $challan=$ex_data[0]; else $challan.=','.$ex_data[0];
		if($po_id=="") $po_id=$ex_data[1]; else $po_id.=','.$ex_data[1];
		if($item_id=="") $item_id=$ex_data[2]; else $item_id.=','.$ex_data[2];
		if($body_part_id=="") $body_part_id=$ex_data[3]; else $body_part_id.=','.$ex_data[3];
		if($febric_description_id=="") $febric_description_id=$ex_data[4]; else $febric_description_id.=','.$ex_data[4];
	}
	
	$old_issue_id=explode(",",$data[1]); 
	$old_challan=""; $old_po_id=""; $old_item_id=""; $old_body_part_id=""; $old_febric_description_id=""; 
	foreach($old_issue_id as $value)
	{
		$old_selected_id_arr[]=$value;
		$old_data=explode("_",$value);
		if($old_challan=="") $old_challan=$old_data[0]; else $old_challan.=','.$old_data[0];
		if($old_po_id=="") $old_po_id=$old_data[1]; else $old_po_id.=','.$old_data[1];
		if($old_item_id=="") $old_item_id=$old_data[2]; else $old_item_id.=','.$old_data[2];
		if($old_body_part_id=="") $old_body_part_id=$old_data[3]; else $old_body_part_id.=','.$old_data[3];
		if($old_febric_description_id=="") $old_febric_description_id=$old_data[4]; else $old_febric_description_id.=','.$old_data[4];
	}
	
	$bill_challan=implode(",",array_intersect(explode(",",$challan), explode(",",$old_challan)));
	$bill_po_id=implode(",",array_intersect(explode(",",$po_id), explode(",",$old_po_id)));
	$bill_item_id=implode(",",array_intersect(explode(",",$item_id), explode(",",$old_item_id)));
	$bill_body_part_id=implode(",",array_intersect(explode(",",$body_part_id), explode(",",$old_body_part_id)));
	
	$bill_febric_description_id=implode(",",array_intersect(explode(",",$febric_description_id), explode(",",$old_febric_description_id)));
	$dele_item_id="'".implode("','",explode(",",$bill_item_id))."'";
	//echo $body_part_id.'=='.$old_body_part_id;
	$del_challan=implode(",",array_diff(explode(",",$challan), explode(",",$old_challan)));
	$del_po_id=implode(",",array_diff(explode(",",$po_id), explode(",",$old_po_id)));
	$del_item_id=implode(",",array_diff(explode(",",$item_id), explode(",",$old_item_id)));
	$del_body_part_id=implode(",",array_diff(explode(",",$body_part_id), explode(",",$old_body_part_id)));
	$del_febric_description_id=implode(",",array_diff(explode(",",$febric_description_id), explode(",",$old_febric_description_id)));	
	$add_del_item_id="'".implode("','",explode(",",$del_item_id))."'";
	
	
	$delete_challan=implode(",",array_diff(explode(",",$old_challan), explode(",",$challan)));
	$delete_po_id=implode(",",array_diff(explode(",",$old_po_id), explode(",",$po_id)));
	$delete_item_id=implode(",",array_diff(explode(",",$old_item_id), explode(",",$item_id)));
	$delete_body_part_id=implode(",",array_diff(explode(",",$old_body_part_id), explode(",",$body_part_id)));
	$delete_febric_description_id=implode(",",array_diff(explode(",",$old_febric_description_id), explode(",",$febric_description_id)));	
	
	$old_selected_id="'".implode("','",explode(",",$data[0]))."'";
	$old_issue_id="'".implode("','",explode(",",$data[1]))."'";
	
	$old_bill_id=array_intersect(explode(",",$old_selected_id), explode(",",$old_issue_id));
	$old_bill_id=implode(",",$old_bill_id);
	
	$data_selected=implode(',',explode('_',$data[0]));
	$data_issue=implode(',',explode('_',$data[1]));
	
	$del_id=array_diff(explode(",",$data_selected), explode(",",$data_issue));
	//$bill_id=array_intersect(explode(",",$data_selected), explode(",",$data_issue));
	//$delete_id=array_diff(explode(",",$data_issue), explode(",",$data_selected));
	$bill_id=array_intersect(explode(",",$old_selected_id), explode(",",$old_issue_id));
	$delete_id=array_diff(explode(",",$old_issue_id), explode(",",$old_selected_id));
	
	$del_id=implode(",",$del_id); $bill_id=implode(",",$bill_id);   $delete_id=implode(",",$delete_id);

	
	if ($data[3]==2)
	{
		/*$del_id=array_diff(explode(",",$data[0]), explode(",",$data[1]));
		$bill_id=array_intersect(explode(",",$data[0]), explode(",",$data[1]));
		$delete_id=array_diff(explode(",",$data[1]), explode(",",$data[0]));
		$del_id=implode(",",$del_id); $bill_id=implode(",",$bill_id);   $delete_id=implode(",",$delete_id);*/
		//echo $delete_id;
		$yarndesc_arr=return_library_array( "select id, material_description from sub_material_dtls",'id','material_description');
		$febricdesc_arr=return_library_array( "select id, const_comp from lib_subcon_charge",'id','const_comp');
		
		$order_array=array();
		$order_sql="Select b.id, b.order_no, b.order_uom, b.cust_buyer, b.cust_style_ref, b.rate, b.amount, a.currency_id from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0";
		$order_sql_result =sql_select($order_sql);
		foreach ($order_sql_result as $row)
		{
			$order_array[$row[csf("id")]]['order_no']=$row[csf("order_no")];
			$order_array[$row[csf("id")]]['order_uom']=$row[csf("order_uom")];
			$order_array[$row[csf("id")]]['cust_buyer']=$row[csf("cust_buyer")];
			$order_array[$row[csf("id")]]['cust_style_ref']=$row[csf("cust_style_ref")];
			$order_array[$row[csf("id")]]['rate']=$row[csf("rate")];
			$order_array[$row[csf("id")]]['amount']=$row[csf("amount")];
			$order_array[$row[csf("id")]]['currency_id']=$row[csf("currency_id")];
		}
		//var_dump($order_array);die;
		if($db_type==0)
		{
			if( $data[2]!="" )
			{
				$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where mst_id in ($data[2]) and status_active=1 and process_id=2"; 
			}
			else
			{
				if($bill_id!="" && $del_id!="")
					$sql="(select id as upd_id, delivery_id, delivery_date, challan_no, group_concat(item_id  SEPARATOR '_') as item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id=2 group by id, delivery_id, delivery_date, challan_no, item_id, packing_qnty, delivery_qty, rate, amount, remarks, order_id)
					 union
					 (select 0, group_concat(b.id  SEPARATOR '_') as delivery_id, a.delivery_date, a.challan_no, group_concat(b.item_id  SEPARATOR '_') as item_id, b.carton_roll, b.delivery_qty, 0, 0, null, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and b.process_id=2 group by b.id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, b.order_id)";
				else if($bill_id!="" && $del_id=="")
					$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id=2";
				else  if($bill_id=="" && $del_id!="")
					$sql="select 0, b.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, 0, 0, 0, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and a.is_deleted=0 and b.process_id=2"; 
			}		
		}
		else if ($db_type==2)
		{
			if( $data[2]!="" )
			{
				$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where mst_id in ($data[2]) and status_active=1 and process_id=2"; 
			}
			else
			{
				if($bill_id!="" && $del_id!="")
					$sql="(select id as upd_id, delivery_id, delivery_date, challan_no, listagg(item_id,'_') within group (order by item_id) as item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id=2 group by id, delivery_id, delivery_date, challan_no, item_id, packing_qnty, delivery_qty, rate, amount, remarks, order_id)
					 union
					 (select 0, listagg(b.id,'_') within group (order by b.id) as delivery_id, a.delivery_date, a.challan_no, listagg(b.item_id,'_') within group (order by b.item_id) as item_id, b.carton_roll, b.delivery_qty, 0, 0, null, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and b.process_id=2 group by b.id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, b.order_id)";
				else if($bill_id!="" && $del_id=="")
					$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id=2";
				else  if($bill_id=="" && $del_id!="")
					$sql="select 0, b.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, 0, 0, 0, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and a.is_deleted=0 and b.process_id=2"; 
			}
		}
		//echo $sql;
		$sql_result =sql_select($sql);
		$k=0;
		$num_rowss=count($sql_result);
		foreach ($sql_result as $row)
		{
			$k++;
			if( $data[2]!="" )
			{
				if($data[1]=="") $data[1]=$row[csf("delivery_id")]; else $data[1].=",".$row[csf("delivery_id")];
			}
			?>
			 <tr align="center">				
				<td>
				 <? if ($k==$num_rowss) { ?>
					<input type="hidden" name="issue_id_all" id="issue_id_all"  style="width:80px" value="<? echo $data[1]; ?>" />
					<input type="hidden" name="delete_id" id="delete_id"  style="width:80px" value="<? echo $delete_id; ?>" />
					<? } ?>
					<input type="hidden" name="curanci_<? echo $k; ?>" id="curanci_<? echo $k; ?>"  style="width:80px" value="<? echo $order_array[$row[csf("order_id")]]['currency_id']; ?>" />
					<input type="hidden" name="updateiddtls_<? echo $k; ?>" id="updateiddtls_<? echo $k; ?>" value="<? echo ($row[csf("upd_id")] != 0 ? $row[csf("upd_id")] : "") ?>">
					<input type="hidden" name="deliveryid_<? echo $k; ?>" id="deliveryid_<? echo $k; ?>" value="<? echo $row[csf("delivery_id")]; ?>">
					<input type="text" name="deleverydate_<? echo $k; ?>" id="deleverydate_<? echo $k; ?>"  class="datepicker" style="width:60px" value="<? echo change_date_format($row[csf("delivery_date")]); ?>" disabled />									
				</td>
				<td>
					<input type="text" name="challenno_<? echo $k; ?>" id="challenno_<? echo $k; ?>"  class="text_boxes" style="width:40px" value="<? echo $row[csf("challan_no")]; ?>" readonly />							 
				</td>
				<td>
					<input type="hidden" name="ordernoid_<? echo $k; ?>" id="ordernoid_<? echo $k; ?>" value="<? echo $row[csf("order_id")]; ?>">
					<input type="text" name="orderno_<? echo $k; ?>" id="orderno_<? echo $k; ?>"  class="text_boxes" style="width:70px" value="<? echo $order_array[$row[csf("order_id")]]['order_no']; ?>" readonly />										
				</td>
				<td>
					<input type="text" name="stylename_<? echo $k; ?>" id="stylename_<? echo $k; ?>"  class="text_boxes" style="width:80px;" value="<? echo $order_array[$row[csf("order_id")]]['cust_style_ref']; ?>" />
				</td>
				<td>
					<input type="text" name="buyername_<? echo $k; ?>" id="buyername_<? echo $k; ?>"  class="text_boxes" style="width:70px" value="<? echo $order_array[$row[csf("order_id")]]['cust_buyer']; ?>" />								
				</td>
				<td>			
					<input name="numberroll_<? echo $k; ?>" id="numberroll_<? echo $k; ?>" type="text" class="text_boxes" style="width:40px" value="<? echo $row[csf("carton_roll")]; ?>" readonly />							
				</td> 
				<td style="display:none">
                    <input type="hidden" name="compoid_<? echo $k; ?>" id="compoid_<? echo $k; ?>" value="<? echo $row[csf("febric_description_id")]; ?>">
					<input type="text" name="yarndesc_<? echo $k; ?>" id="yarndesc_<? echo $k; ?>"  class="text_boxes" style="width:115px" value="<? echo $yarndesc_arr[$row[csf("order_id")]]; ?>" readonly/>
				</td>
                <td>
					<?
						$item=$row[csf("item_id")];
						$body=return_field_value("body_part","lib_subcon_charge", "id=$item","body_part");
					?>
                	<input type="hidden" name="bodypartid_<? echo $k; ?>" id="bodypartid_<? echo $k; ?>" value="<? echo $body; ?>">
					<input type="text" name="bodypartdesc_<? echo $k; ?>" id="bodypartdesc_<? echo $k; ?>"  class="text_boxes" style="width:80px" value="<? echo $body_part[$body]; ?>" readonly/>
				</td>
				<td>
                	<input type="hidden" name="itemid_<? echo $k; ?>" id="itemid_<? echo $k; ?>" value="<? echo $row[csf("item_id")]; ?>">
					<input type="text" name="febricdesc_<? echo $k; ?>" id="febricdesc_<? echo $k; ?>"  class="text_boxes" style="width:135px" value="<? echo $febricdesc_arr[$row[csf("item_id")]]; ?>" readonly/>
				</td>
				<td>
					<? 
					if ($body==1 || $body==20)
					{
						$selected_uom=12;
					}
					else
					{
						$selected_uom=$row[csf("uom")];
					}
					echo create_drop_down( "cbouom_$k", 50, $unit_of_measurement,"", 1, "-UOM-",$selected_uom,"",0,'1,2,12',"" );?>
				</td>
				<td>
					<input type="text" name="deliveryqnty_<? echo $k; ?>" id="deliveryqnty_<? echo $k; ?>"  class="text_boxes_numeric" style="width:40px" value="<? echo $row[csf("delivery_qty")]; ?>" readonly />
				</td>
				<td>
					<input type="text" name="txtrate_<? echo $k; ?>" id="txtrate_<? echo $k; ?>"  class="text_boxes" style="width:40px" value="<? echo $order_array[$row[csf("order_id")]]['rate']; ?>" readonly />
				</td>
					<?
						$total_amount=$row[csf("delivery_qty")]*$order_array[$row[csf("order_id")]]['rate'];
					?>
				<td>
					<input type="text" name="amount_<? echo $k; ?>" id="amount_<? echo $k; ?>" style="width:40px"  class="text_boxes"  value="<? echo $total_amount; ?>" readonly />
				</td>
				<td>
					 <input type="button" name="remarks_<? echo $k; ?>" id="remarks_<? echo $k; ?>"  class="formbuttonplasminus" style="width:20px" value="R" onClick="openmypage_remarks(<? echo $k; ?>);" />
                     <input type="hidden" name="remarksvalue_<? echo $k; ?>" id="remarksvalue_<? echo $k; ?>" class="text_boxes" value="<? echo $row[csf("remarks")]; ?>" />
				</td>
			</tr>
		<?	
		}
	}
	else if ($data[3]==1)
	{
		/*$data_selected=implode(',',explode('_',$data[0]));
		$del_id=array_diff(explode(",",$data_selected), explode(",",$data_issue));
		//$del_id=array_diff(explode(",",$data[0]), explode(",",$data[1]));
		$bill_id=array_intersect(explode(",",$data[0]), explode(",",$data[1]));
		$delete_id=array_diff(explode(",",$data[1]), explode(",",$data[0]));
		$del_id=implode(",",$del_id); $bill_id=implode(",",$bill_id); $delete_id=implode(",",$delete_id);
		//echo $del_id;*/

		$job_order_arr=array();
		$sql_job="Select a.job_no_prefix_num, a.buyer_name, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0";
		$sql_job_result =sql_select($sql_job);
		foreach($sql_job_result as $row)
		{
			$job_order_arr[$row[csf('id')]]['job']=$row[csf('job_no_prefix_num')];
			$job_order_arr[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')];
			$job_order_arr[$row[csf('id')]]['style']=$row[csf('style_ref_no')];
			$job_order_arr[$row[csf('id')]]['po']=$row[csf('po_number')];
		}
		
		$composition_arr=array();
		$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
		$data_array=sql_select($sql_deter);
		if(count($data_array)>0)
		{
			foreach( $data_array as $row )
			{
				if(array_key_exists($row[csf('id')],$composition_arr))
				{
					$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
				}
				else
				{
					$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
				}
			}
		}
		
		//var_dump($composition_arr);	
		$determ_arr = return_library_array( "select mst_id, copmposition_id from lib_yarn_count_determina_dtls",'mst_id','copmposition_id');	
		$byuer_arr=return_library_array( "select id, short_name from  lib_buyer",'id','short_name');
		$product_dtls_arr=return_library_array( "select id, product_name_details from  product_details_master",'id','product_name_details');
		//var_dump($order_array);die;
		if($del_body_part_id!="") $body_part_cond=" and b.body_part_id in ($body_part_id)"; else $body_part_cond="";
		if($del_challan!="") $del_challan_cond=" and a.recv_number_prefix_num in ($challan)"; else $del_challan_cond="";
		if($del_po_id!="") $del_po_id_cond="  and c.po_breakdown_id in ($po_id)"; else $del_po_id_cond="";
		if($add_del_item_id!="") $del_item_id_cond="  and c.prod_id in ($add_del_item_id)"; else $del_item_id_cond="";
		if($del_febric_description_id!="") $del_febric_id_cond="  and b.febric_description_id in ($febric_description_id)"; else $del_febric_id_cond="";
		
		if($db_type==0)
		{
			if( $data[2]!="" )
			{
				$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, body_part_id, febric_description_id, uom, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where mst_id in ($data[2]) and status_active=1 and process_id=2"; 
			}
			else
			{
				if($bill_id!="" && $del_id!="")
					$sql="(select id as upd_id, delivery_date, challan_no, item_id, body_part_id, febric_description_id, uom, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where challan_no in ($bill_challan) and order_id in ($bill_po_id) and item_id in ($bill_item_id) and body_part_id in ($bill_body_part_id) and febric_description_id in ($bill_febric_description_id) and status_active=1 and is_deleted=0 and process_id=2)
					 union
					 (select 0 as upd_id, a.receive_date as delivery_date, a.recv_number_prefix_num as challan_no, group_concat(b.prod_id SEPARATOR '_') as item_id, b.body_part_id, b.febric_description_id, null as uom, sum(b.no_of_roll) as carton_roll, sum(c.quantity) as delivery_qty, sum(b.rate) as rate, sum(b.amount) as amount, null as remarks, c.po_breakdown_id as order_id from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.item_category=13 and a.receive_basis in (2,9) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $del_challan_cond $del_po_id_cond $del_item_id_cond $body_part_cond  $del_febric_id_cond group by a.id, a.recv_number_prefix_num, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id) order by challan_no";
				else if($bill_id!="" && $del_id=="")
					$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, body_part_id, febric_description_id, uom, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where challan_no in ($bill_challan) and order_id in ($bill_po_id) and item_id in ($bill_item_id) and body_part_id in ($bill_body_part_id) and febric_description_id in ($bill_febric_description_id) and status_active=1 and is_deleted=0 and process_id=2";
				else  if($bill_id=="" && $del_id!="")
					$sql="select 0 as upd_id, a.receive_date as delivery_date, a.recv_number_prefix_num as challan_no, b.prod_id as item_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as carton_roll, sum(c.quantity) as delivery_qty, sum(b.rate) as rate, sum(b.amount) as amount, null as remarks, c.po_breakdown_id as order_id from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.item_category=13 and a.receive_basis in (2,9) and a.recv_number_prefix_num in ($challan) and c.po_breakdown_id in ($po_id) and c.prod_id in ($item_id) and b.body_part_id in ($body_part_id) and b.febric_description_id in ($febric_description_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.recv_number_prefix_num, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id";
					//$sql="select 0, b.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, 0, 0, 0, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and a.is_deleted=0 and b.process_id=2"; 
			}
		}
		else if ($db_type==2)
		{
			if( $data[2]!="" )
			{
				$sql="select id as upd_id, delivery_date, challan_no, item_id, body_part_id, febric_description_id, uom, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where mst_id in ($data[2]) and status_active=1 and process_id=2"; 
				
				$sql_result_arr =sql_select($sql);
				foreach ($sql_result_arr as $row)
				{
					$update_chk_str[]=$row[csf("challan_no")].'_'.$row[csf('order_id')].'_'.$row[csf('item_id')].'_'.$row[csf('body_part_id')].'_'.$row[csf('febric_description_id')];
					$issue_chk_str[]=$row[csf("challan_no")].'_'.$row[csf('order_id')].'_'.$row[csf('item_id')].'_'.$row[csf('body_part_id')].'_'.$row[csf('febric_description_id')];
				}
				 
			}
			else
			{
				if($bill_id!="" && $del_id!="")
					$sql="(select id as upd_id, delivery_date, challan_no, item_id, body_part_id, febric_description_id, uom, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where challan_no in ($bill_challan) and order_id in ($bill_po_id) and item_id in ($dele_item_id) and body_part_id in ($bill_body_part_id) and febric_description_id in ($bill_febric_description_id) and status_active=1 and is_deleted=0 and process_id=2)
					 union
					 (select 0 as upd_id, a.receive_date as delivery_date, a.recv_number_prefix_num as challan_no, listagg(b.prod_id,',') within group (order by b.prod_id) as item_id, b.body_part_id, b.febric_description_id, null as uom, sum(b.no_of_roll) as carton_roll, sum(c.quantity) as delivery_qty, sum(b.rate) as rate, sum(b.amount) as amount, null as remarks, c.po_breakdown_id as order_id from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.item_category=13  and a.receive_basis in (2,9) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $del_challan_cond $del_po_id_cond $del_item_id_cond $body_part_cond  $del_febric_id_cond group by a.id, a.recv_number_prefix_num, a.receive_date, b.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id) order by challan_no";
				else if($bill_id!="" && $del_id=="")
					$sql="select id as upd_id, delivery_date, challan_no, item_id, body_part_id, febric_description_id, uom, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where challan_no in ($bill_challan) and order_id in ($bill_po_id) and item_id in ($dele_item_id) and body_part_id in ($bill_body_part_id) and febric_description_id in ($bill_febric_description_id) and status_active=1 and is_deleted=0 and process_id=2";
				else  if($bill_id=="" && $del_id!="")
					$sql="select 0 as upd_id, a.receive_date as delivery_date, a.recv_number_prefix_num as challan_no, c.prod_id as item_id, b.body_part_id, b.febric_description_id, sum(b.no_of_roll) as carton_roll, sum(c.quantity) as delivery_qty, sum(b.rate) as rate, sum(b.amount) as amount, null as remarks, c.po_breakdown_id as order_id from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.knitting_source=1 and c.trans_type=1 and a.entry_form=22 and c.entry_form=22 and a.item_category=13 and a.receive_basis in (2,9) and a.recv_number_prefix_num in ($challan) and c.po_breakdown_id in ($po_id) and c.prod_id in ($item_id) and b.body_part_id in ($body_part_id) and b.febric_description_id in ($febric_description_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.recv_number_prefix_num, a.receive_date, c.prod_id, b.body_part_id, b.febric_description_id, c.po_breakdown_id";
					//$sql="select 0, b.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, 0, 0, 0, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and a.is_deleted=0 and b.process_id=2"; 
			}
		}
		
		echo $sql;
		$sql_result =sql_select($sql); 
		$k=0; $num_rowss=count($sql_result);  $previous_chk_str="";
		foreach ($sql_result as $row)
		{
			if( $data[2]!="" )
			{
				//if($data[1]=="") $data[1]=$row[csf("delivery_id")]; else $data[1].=",".$row[csf("delivery_id")];
				foreach ($issue_chk_str as $val)
				{
					if($data[1]=="") $data[1]=$val; else $data[1].=",".$val;
				}
				//if($data[1]=="") $data[1]=$row[csf("challan_no")].'_'.$row[csf('order_id')].'_'.$row[csf('item_id')].'_'.$row[csf('body_part_id')].'_'.$row[csf('febric_description_id')]; else $data[1].=",".$row[csf("challan_no")].'_'.$row[csf('order_id')].'_'.$row[csf('item_id')].'_'.$row[csf('body_part_id')].'_'.$row[csf('febric_description_id')];
			}
			$item_id=implode(",",array_unique(explode(",",$row[csf('item_id')])));
			$chk_str=$row[csf("challan_no")].'_'.$row[csf('order_id')].'_'.$item_id.'_'.$row[csf('body_part_id')].'_'.$row[csf('febric_description_id')];
			
			if($data[2]=="") $previous_chk_str=$selected_id_arr;  else $previous_chk_str=$update_chk_str;
			$count_selected_id_arr=count($selected_id_arr);
			//print_r ($update_chk_str);
			if(in_array($chk_str,$previous_chk_str))
			{
				$k++;
				?>
				 <tr align="center">				
					<td>
					 <? if ($k==$count_selected_id_arr) { ?>
						<input type="text" name="issue_id_all" id="issue_id_all"  style="width:80px" value="<? echo $data[1]; ?>" />
						<input type="hidden" name="delete_id" id="delete_id"  style="width:80px" value="<? echo $delete_id; ?>" />
						
						<? } ?>
						<input type="hidden" name="curanci_<? echo $k; ?>" id="curanci_<? echo $k; ?>"  style="width:80px" value="<? echo '1'; ?>" />
						<input type="hidden" name="updateiddtls_<? echo $k; ?>" id="updateiddtls_<? echo $k; ?>" value="<? echo ($row[csf("upd_id")] != 0 ? $row[csf("upd_id")] : "") ?>">
						<input type="hidden" name="deliveryid_<? echo $k; ?>" id="deliveryid_<? echo $k; ?>" style="width:45px" value="<? echo $row[csf("delivery_id")]; ?>">
						<input type="text" name="deleverydate_<? echo $k; ?>" id="deleverydate_<? echo $k; ?>"  class="datepicker" style="width:60px" value="<? echo change_date_format($row[csf("delivery_date")]); ?>" disabled/>									
					</td>
					<td>
						<input type="text" name="challenno_<? echo $k; ?>" id="challenno_<? echo $k; ?>"  class="text_boxes" style="width:40px" value="<? echo $row[csf("challan_no")]; ?>" readonly />							 
					</td>
					<td>
						<input type="hidden" name="ordernoid_<? echo $k; ?>" id="ordernoid_<? echo $k; ?>" value="<? echo $row[csf("order_id")]; ?>">
						<input type="text" name="orderno_<? echo $k; ?>" id="orderno_<? echo $k; ?>"  class="text_boxes" style="width:70px" value="<? echo $job_order_arr[$row[csf('order_id')]]['po']; ?>" readonly />										
					</td>
					<td>
						<input type="text" name="stylename_<? echo $k; ?>" id="stylename_<? echo $k; ?>"  class="text_boxes" style="width:80px;" value="<? echo $job_order_arr[$row[csf('order_id')]]['style']; ?>" readonly />
					</td>
					<td>
						<input type="text" name="buyername_<? echo $k; ?>" id="buyername_<? echo $k; ?>"  class="text_boxes" style="width:70px" value="<? echo $byuer_arr[$job_order_arr[$row[csf('order_id')]]['buyer_name']]; ?>" readonly />								
					</td>
					<td>			
						<input name="numberroll_<? echo $k; ?>" id="numberroll_<? echo $k; ?>" type="text" class="text_boxes_numeric" style="width:40px" value="<? echo $row[csf("carton_roll")]; ?>" />							
					</td> 
					<td style="display:none">
						 <input type="hidden" name="compoid_<? echo $k; ?>" id="compoid_<? echo $k; ?>" value="<? echo $row[csf("febric_description_id")]; ?>">
						<input type="text" name="yarndesc_<? echo $k; ?>" id="yarndesc_<? echo $k; ?>"  class="text_boxes" style="width:115px" value="<? echo $composition_arr[$determ_arr[$row[csf('febric_description_id')]]]; ?>" readonly/>
					</td>
					<td>
						<input type="hidden" name="bodypartid_<? echo $k; ?>" id="bodypartid_<? echo $k; ?>" value="<? echo $row[csf("body_part_id")]; ?>">
						<input type="text" name="bodypartdesc_<? echo $k; ?>" id="bodypartdesc_<? echo $k; ?>"  class="text_boxes" style="width:80px" value="<? echo $body_part[$row[csf("body_part_id")]]; ?>" readonly/>
					</td>
					<td>
						<input type="hidden" name="itemid_<? echo $k; ?>" id="itemid_<? echo $k; ?>" value="<? echo $item_id; ?>">
						<input type="text" name="febricdesc_<? echo $k; ?>" id="febricdesc_<? echo $k; ?>"  class="text_boxes" style="width:135px" value="<? echo $product_dtls_arr[$item_id]; ?>" readonly/>
					</td>
					<td>
						<? 
						if ($row[csf("body_part_id")]==1 || $row[csf("body_part_id")]==20)
						{
							$selected_uom=12;
						}
						else
						{
							$selected_uom=$row[csf("uom")];
						}
						echo create_drop_down( "cbouom_$k", 50, $unit_of_measurement,"", 1, "-UOM-",$selected_uom,"",0,'1,2,12',"" );?>
					</td>
					<td>
						<input type="text" name="deliveryqnty_<? echo $k; ?>" id="deliveryqnty_<? echo $k; ?>"  class="text_boxes_numeric" style="width:40px" value="<? echo $row[csf("delivery_qty")]; ?>" onBlur="qnty_caluculation(<? echo $k; ?>);" />
					</td>
					<td>
						<input type="text" name="txtrate_<? echo $k; ?>" id="txtrate_<? echo $k; ?>"  class="text_boxes" style="width:40px" value="<? echo $row[csf("rate")]; ?>" onBlur="qnty_caluculation(<? echo $k; ?>);" />
					</td>
						<?
							$total_amount=$row[csf("delivery_qty")]*$row[csf("rate")];;
						?>
					<td>
						<input type="text" name="amount_<? echo $k; ?>" id="amount_<? echo $k; ?>" style="width:40px"  class="text_boxes"  value="<? echo $total_amount; ?>" readonly />
					</td>
					<td>
						 <input type="button" name="remarks_<? echo $k; ?>" id="remarks_<? echo $k; ?>"  class="formbuttonplasminus" style="width:20px" value="R" onClick="openmypage_remarks(<? echo $k; ?>);" />
						 <input type="hidden" name="remarksvalue_<? echo $k; ?>" id="remarksvalue_<? echo $k; ?>" class="text_boxes" value="<? echo $row[csf("remarks")]; ?>" />
					</td>
				 </tr>
			<?
			}
		}
	}
	else
	{
	?>
		<tr align="center">				
			<td>
				<input type="hidden" name="updateiddtls_1" id="updateiddtls_1">
				<input type="text" name="deleverydate_1" id="deleverydate_1"  class="datepicker" style="width:60px" readonly />									
			</td>
			<td>
				<input type="text" name="challenno_1" id="challenno_1"  class="text_boxes" style="width:40px" readonly />
			</td>
			<td>
				<input type="hidden" name="ordernoid_1" id="ordernoid_1" value="">
				<input type="text" name="orderno_1" id="orderno_1"  class="text_boxes" style="width:70px" readonly />
			</td>
			<td>
				<input type="text" name="stylename_1" id="stylename_1"  class="text_boxes" style="width:80px;" />
			</td>
			<td>
				<input type="text" name="buyername_1" id="buyername_1"  class="text_boxes" style="width:70px" />
			</td>
			<td>			
				<input name="numberroll_1" id="numberroll_1" type="text" class="text_boxes" style="width:40px" readonly />
			</td>  
			<td style="display:none">
				<input type="text" name="yarndesc_1" id="yarndesc_1"  class="text_boxes" style="width:115px" readonly/>
			</td>
			<td>
				<input type="text" name="bodypart_1" id="bodypart_1"  class="text_boxes" style="width:80px" readonly/>
			</td>
			<td>
				<input type="text" name="febricdesc_1" id="febricdesc_1"  class="text_boxes_numeric" style="width:135px" readonly/>
			</td>
			<td>
				<? echo create_drop_down( "cbouom_1", 50, $unit_of_measurement,"", 1, "-UOM-",0,"",0,"1,2,12" );?>
			</td>
			<td>
				<input type="text" name="deliveryqnty_1" id="deliveryqnty_1"  class="text_boxes_numeric" style="width:40px" />
			</td>
			<td>
				<input type="text" name="txtrate_1" id="txtrate_1"  class="text_boxes_numeric" style="width:40px" />
			</td>
			<td>
				<input type="text" name="amount_1" id="amount_1" style="width:40px"  class="text_boxes"  readonly />
			</td>
			<td>
				<input type="button" name="remarks_1" id="remarks_1"  class="formbuttonplasminus" value="R" onClick="openmypage_remarks(1);" />
				<input type="hidden" name="remarksvalue_1" id="remarksvalue_1" class="text_boxes" />
			</td>
		</tr>
	<?	
	}
	exit();
}

if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Trems & Condition Search","../../", 1, 1, $unicode);
	extract($_REQUEST);
	
	$_SESSION['page_permission']=$permission;
?>
	<script>
	var permission='<? echo $permission; ?>';
	function add_break_down_tr(i) 
	{
		var row_num=$('#tbl_termcondi_details tr').length-1;
		if (row_num!=i)
		{
			return false;
		}
		else
		{
			i++;
			
			$("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			'name': function(_, name) { return name + i },
			'value': function(_, value) { return value }              
			});  
			}).end().appendTo("#tbl_termcondi_details");
			$('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
			$('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
			$('#termscondition_'+i).val("");
			$('#sltd_'+i).val(i);
			//$('#sl_td').i
			//alert(i)
			//document.getElementById('sltd_'+i).innerHTML=i;
		}
	}

	function fn_deletebreak_down_tr(rowNo) 
	{   
		var numRow = $('table#tbl_termcondi_details tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_termcondi_details tbody tr:last').remove();
		}
	}

	function fnc_kniting_terms_condition( operation )
	{
		var row_num=$('#tbl_termcondi_details tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			if (form_validation('termscondition_'+i,'Term Condition')==false)
			{
				return;
			}
			data_all=data_all+get_submitted_data_string('txt_bill_no*termscondition_'+i,"../../");
		}
		var data="action=save_update_delete_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
		//freeze_window(operation);
		http.open("POST","knitting_bill_issue_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_kniting_terms_condition_reponse;
	}

	function fnc_kniting_terms_condition_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split('**');
			//if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				parent.emailwindow.hide();
			}
		}
	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
<? echo load_freeze_divs ("../../",$permission);  ?>
    <fieldset>
    <input type="hidden" id="txt_bill_no" name="txt_bill_no" value="<? echo str_replace("'","",$txt_bill_no) ?>"/>
        <form id="termscondi_1" autocomplete="off">
        <table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
            <thead>
                <tr>
                    <th width="50">Sl</th><th width="530">Terms</th><th ></th>
                </tr>
            </thead>
            <tbody>
				<?
                $data_array=sql_select("select id, terms from  subcon_terms_condition where bill_no=$txt_bill_no");// quotation_id='$data'
                if(count($data_array)>0)
                {
					$i=0;
					foreach( $data_array as $row )
					{
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$i++;
						?>
						<tr id="settr_1" align="center" bgcolor="<? echo $bgcolor;  ?>">
                            <td >
                                <input type="text" id="sltd_<? echo $i;?>" name="sltd_<? echo $i;?>" style="width:100%;" value="<? echo $i; ?>" disabled/> 
                            </td>
                            <td>
                                <input type="text" id="termscondition_<? echo $i;?>" name="termscondition_<? echo $i;?>" style="width:95%"  class="text_boxes"  value="<? echo $row[csf('terms')]; ?>"  /> 
                            </td>
                            <td> 
                                <input type="button" id="increase_<? echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr(<? echo $i; ?> )" />
                                <input type="button" id="decrease_<? echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_deletebreak_down_tr(<? echo $i; ?>);" />
                            </td>
                        </tr>
                        <?
					}
                }
                else
                {
					$data_array=sql_select("select id, terms from  lib_terms_condition where is_default=1");// quotation_id='$data'
					foreach( $data_array as $row )
					{
						if ($i%2==0) $bgcolor="#E9F3FF";  else  $bgcolor="#FFFFFF";
						$i++;
						?>
						<tr id="settr_1" align="center" bgcolor="<? echo $bgcolor;  ?>">
                            <td >
                                <input type="text" id="sltd_<? echo $i;?>" name="sltd_<? echo $i;?>" style="width:100%;" value="<? echo $i; ?>" disabled /> 
                            </td>
                            <td>
                                <input type="text" id="termscondition_<? echo $i;?>" name="termscondition_<? echo $i;?>" style="width:95%" class="text_boxes" value="<? echo $row[csf('terms')]; ?>" /> 
                            </td>
                            <td>
                                <input type="button" id="increase_<? echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr(<? echo $i; ?> )" />
                                <input type="button" id="decrease_<? echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_deletebreak_down_tr(<? echo $i; ?> );" />
                            </td>
						</tr>
						<? 
					}
                } 
                ?>
            </tbody>
        </table>
        <table width="650" cellspacing="0" class="" border="0">
            <tr>
                <td align="center" height="15" width="100%"> </td>
            </tr>
            <tr>
                <td align="center" width="100%" class="button_container">
					<?
						echo load_submit_buttons( $permission, "fnc_kniting_terms_condition", 0,0 ,"reset_form('termscondi_1','','','','')",1) ; 
                    ?>
                </td> 
            </tr>
        </table>
        </form>
    </fieldset>
	</div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?
	exit();
}

if($action=="save_update_delete_terms_condition")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		//if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id=return_next_id( "id", "subcon_terms_condition", 1 ) ;
		 $field_array="id,bill_no,terms,entry_form";
		 for ($i=1;$i<=$total_row;$i++)
		 {
			$termscondition="termscondition_".$i;
			if ($i!=1) $data_array .=",";
			$data_array .="(".$id.",".$txt_bill_no.",".$$termscondition.",1)";
			$id=$id+1;
		 }
		// echo  $data_array;
		$rID_de3=execute_query( "delete from subcon_terms_condition where  bill_no =".$txt_bill_no."",0);

		$rID=sql_insert("subcon_terms_condition",$field_array,$data_array,1);
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$txt_bill_no;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$txt_bill_no;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "0**".$txt_bill_no;
			}
			else{
				oci_rollback($con);  
				echo "10**".$txt_bill_no;
			}
		}
		disconnect($con);
		die;
	}		
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$bill_process_id="2";
	if ($operation==0)   // Insert Here========================================================================================delivery_id
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if (is_duplicate_field( "delivery_id", "subcon_inbound_bill_dtls", "mst_id=$update_id" )==1)
		{
			echo "11**0"; 
			die;			
		}
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)";	
		}
		else if($db_type==2)
		{
			$year_cond=" and TO_CHAR(insert_date,'YYYY')";	
		}
		
		$new_bill_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'KNT', date("Y",time()), 5, "select prefix_no, prefix_no_num from  subcon_inbound_bill_mst where company_id=$cbo_company_id and process_id=$bill_process_id $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
			
		if(str_replace("'",'',$update_id)=="")
		{
			$id=return_next_id( "id", "subcon_inbound_bill_mst", 1 ) ; 	
			$field_array="id, prefix_no, prefix_no_num, bill_no, company_id, location_id, bill_date, party_id, party_source, attention, bill_for, process_id, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_bill_no[1]."','".$new_bill_no[2]."','".$new_bill_no[0]."',".$cbo_company_id.",".$cbo_location_name.",".$txt_bill_date.",".$cbo_party_name.",".$cbo_party_source.",".$txt_attention.",".$cbo_bill_for.",'".$bill_process_id."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
			//echo "INSERT INTO subcon_inbound_bill_mst (".$field_array.") VALUES ".$data_array; die; 
			$rID=sql_insert("subcon_inbound_bill_mst",$field_array,$data_array,1);
			if($rID) $flag=1; else $flag=0;
			$return_no=$new_bill_no[0]; 
		}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="location_id*bill_date*party_id*party_source*attention*bill_for*updated_by*update_date";
			$data_array="".$cbo_location_name."*".$txt_bill_date."*".$cbo_party_name."*".$cbo_party_source."*".$txt_attention."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			$rID=sql_update("subcon_inbound_bill_mst",$field_array,$data_array,"id",$update_id,0);
			if($rID) $flag=1; else $flag=0;
			$return_no=str_replace("'",'',$txt_bill_no);
		}
		$id1=return_next_id( "id", "subcon_inbound_bill_dtls",1);
		$field_array1 ="id, mst_id, delivery_id, delivery_date, challan_no, order_id, item_id, febric_description_id, body_part_id, uom, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id, inserted_by, insert_date";
		$field_array_up ="delivery_id*delivery_date*challan_no*order_id*item_id* febric_description_id*body_part_id*uom*packing_qnty*delivery_qty*rate*amount*remarks*currency_id*updated_by*update_date";
		$field_array_delivery="bill_status";
		$field_array_order="cust_buyer*cust_style_ref";
		$process_id=2;
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
		{
			$delivery_id="deliveryid_".$i;
			$delevery_date="deleverydate_".$i;
			$challen_no="challenno_".$i;
			$orderid="ordernoid_".$i;
			$style_name="stylename_".$i;
			$buyer_name="buyername_".$i;
			$item_id="itemid_".$i;
			$compoid="compoid_".$i;
			$bodypartid="bodypartid_".$i;
			$cbouom="cbouom_".$i;
			$number_roll="numberroll_".$i;
			$quantity="deliveryqnty_".$i;
			$rate="txtrate_".$i;
			$amount="amount_".$i;
			$remarks="remarksvalue_".$i;
			$curanci="curanci_".$i;
			$updateid_dtls="updateiddtls_".$i;
			
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{
				if($$amount!="")
				{
				if ($add_comma!=0) $data_array1 .=",";
				$data_array1 .="(".$id1.",".$id.",".$$delivery_id.",".$$delevery_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$compoid.",".$$bodypartid.",".$$cbouom.",".$$number_roll.",".$$quantity.",".$$rate.",".$$amount.",".$$remarks.",".$$curanci.",'".$process_id."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id1=$id1+1;
				$add_comma++;
				}
				//$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				//$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1")); 
				
				$id_arr_deli=implode(',',explode('_',str_replace("'",'',$$delivery_id)));
				$delv_id=explode(',',$id_arr_deli);
				//$id_arr_delivery=explode(',',$id_arr_deli);
				//$data_array_delivery[explode(',',str_replace("'",'',$$delivery_id))] =explode("*",("1"));
				foreach($delv_id as $val)
				{
					$id_arr_delivery[]=$val;
					$data_array_delivery[$val] =explode("*",("1"));
				}
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$delivery_id."*".$$delevery_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$compoid."*".$$bodypartid."*".$$cbouom."*".$$number_roll."*".$$quantity."*".$$rate."*".$$amount."*".$$remarks."*".$$curanci."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1"));
			}
			//order table insert====================================================================================================
			if(str_replace("'",'',$$style_name)=="" || str_replace("'",'',$$buyer_name)=="")  
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));
			}
			else
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));	
			}
		}
		$rID1=execute_query(bulk_update_sql_statement("subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
		if($rID1) $flag=1; else $flag=0;
		if (str_replace("'",'',$cbo_party_source)==2)
		{
			//echo bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr );die;
			$rID2=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery ));
			if($rID2) $flag=1; else $flag=0;
			$rID4=execute_query(bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr ));
			if($rID4) $flag=1; else $flag=0;
		}
		
		if($data_array1!="")
		{
			//echo "insert into subcon_inbound_bill_dtls (".$field_array1.") values ".$data_array1;die;
			$rID1=sql_insert("subcon_inbound_bill_dtls",$field_array1,$data_array1,1);
			if($rID1) $flag=1; else $flag=0;
		}
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here=============================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$id=str_replace("'",'',$update_id);
		$field_array="location_id*bill_date*party_id*party_source*attention*bill_for*updated_by*update_date";
		$data_array="".$cbo_location_name."*".$txt_bill_date."*".$cbo_party_name."*".$cbo_party_source."*".$txt_attention."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		 
		//$dtls_update_id_array=array();
		$sql_dtls="Select id from subcon_inbound_bill_dtls where mst_id=$update_id and status_active=1 and is_deleted=0";
		$nameArray=sql_select( $sql_dtls );
		foreach($nameArray as $row)
		{
			$dtls_update_id_array[]=$row[csf('id')];
		}
		 
		$return_no=str_replace("'",'',$txt_bill_no);
		
		$id1=return_next_id( "id", "subcon_inbound_bill_dtls",1);
		$field_array1 ="id, mst_id, delivery_id, delivery_date, challan_no, order_id, item_id, febric_description_id, body_part_id, uom, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id, inserted_by, insert_date";
		$field_array_up ="delivery_id*delivery_date*challan_no*order_id*item_id*febric_description_id*body_part_id*uom*packing_qnty*delivery_qty*rate*amount*remarks*currency_id*updated_by*update_date";
		$field_array_delivery="bill_status";
		$field_array_order="cust_buyer*cust_style_ref";
		$process_id=2;
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
		{
			$delivery_id="deliveryid_".$i;
			$delevery_date="deleverydate_".$i;
			$challen_no="challenno_".$i;
			$orderid="ordernoid_".$i;
			$style_name="stylename_".$i;
			$buyer_name="buyername_".$i;
			$item_id="itemid_".$i;
			$compoid="compoid_".$i;
			$bodypartid="bodypartid_".$i;
			$cbouom="cbouom_".$i;
			$number_roll="numberroll_".$i;
			$quantity="deliveryqnty_".$i;
			$rate="txtrate_".$i;
			$amount="amount_".$i;
			$remarks="remarksvalue_".$i;
			$curanci="curanci_".$i;
			$updateid_dtls="updateiddtls_".$i;
			//echo $up_id=str_replace("'",'',$$updateid_dtls);
				
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{ 
				if ($add_comma!=0) $data_array1 .=",";
				$data_array1 .="(".$id1.",".$id.",".$$delivery_id.",".$$delevery_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$compoid.",".$$bodypartid.",".$$cbouom.",".$$number_roll.",".$$quantity.",".$$rate.",".$$amount.",".$$remarks.",".$$curanci.",'".$process_id."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id1=$id1+1;
				$add_comma++;
				//$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				//$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1")); 
				
				$id_arr_deli=implode(',',explode('_',str_replace("'",'',$$delivery_id)));
				$delv_id=explode(',',$id_arr_deli);
				foreach($delv_id as $val)
				{
					$id_arr_delivery[]=$val;
					$data_array_delivery[$val] =explode("*",("1"));
				}
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$delivery_id."*".$$delevery_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$compoid."*".$$bodypartid."*".$$cbouom."*".$$number_roll."*".$$quantity."*".$$rate."*".$$amount."*".$$remarks."*".$$curanci."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				//$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				//$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1"));
				$id_arr_deli=implode(',',explode('_',str_replace("'",'',$$delivery_id)));
				$delv_id=explode(',',$id_arr_deli);
				foreach($delv_id as $val)
				{
					$id_arr_delivery[]=$val;
					$data_array_delivery[$val] =explode("*",("1"));
				}
				
			}
			//order table insert====================================================================================================
			if (str_replace("'",'',$cbo_party_source)==2)
			{
				if(str_replace("'",'',$$style_name)=="" || str_replace("'",'',$$buyer_name)=="")  
				{
					$order_id_arr[]=str_replace("'",'',$$orderid);
					$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));
				}
				else
				{
					$order_id_arr[]=str_replace("'",'',$$orderid);
					$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));	
				}
			}
			//order table insert====================================================================================================
		}
		
		//echo $dis_delete_id; die;

		
		//echo bulk_update_sql_statement( "subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ); die;
		$rID=sql_update("subcon_inbound_bill_mst",$field_array,$data_array,"id",$update_id,0);
		if($rID) $flag=1; else $flag=0;
		
		if (str_replace("'",'',$cbo_party_source)==2)
		{
			//echo bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery );
			$rID2=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery ));
			if($rID2) $flag=1; else $flag=0;
			//echo bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr );die;
			$rID4=execute_query(bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr ));
			if($rID4) $flag=1; else $flag=0;
		}
		
		$rID1=execute_query(bulk_update_sql_statement("subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
		if($rID1) $flag=1; else $flag=0;
		
		if($data_array1!="")
		{
			//echo "insert into subcon_inbound_bill_dtls (".$field_array1.") values ".$data_array1;
			$rID1=sql_insert("subcon_inbound_bill_dtls",$field_array1,$data_array1,0);
			if($rID1) $flag=1; else $flag=0;
		}
		if (str_replace("'",'',$cbo_party_source)==1)
		{
			if(implode(',',$id_arr)!="")
			{
			$distance_delete_id=implode(',',array_diff($dtls_update_id_array,$id_arr));
			}
			else
			{
				$distance_delete_id=implode(',',$dtls_update_id_array);
			}
			if(str_replace("'",'',$distance_delete_id)!="")
			{
				$rID3=execute_query( "delete from subcon_inbound_bill_dtls where id in ($distance_delete_id)",0);
				if($rID3) $flag=1; else $flag=0;
			}
		}
		else
		{
			if(str_replace("'",'',$delete_id)!="")
			{
				$dele_id=str_replace("'",'',$delete_id);
				$delete_id_all="'".implode("','",explode(",",$dele_id))."'";
				//echo "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id_all)";die;
				$rID3=execute_query( "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id_all)",0);
				
				if($rID3) $flag=1; else $flag=0;
				
				//echo "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id_all)";die;
				$new_delete_id=explode("_",str_replace("'",'',$delete_id));
				for ($i=0;$i<count($new_delete_id);$i++)
				{
					$id_delivery[]=$new_delete_id[$i];
					$data_delivery[str_replace("'",'',$new_delete_id[$i])] =explode(",",("0"));
				}
				
				//echo bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery ); die;
				$rID6=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_delivery,$id_delivery ));
				if($rID6) $flag=1; else $flag=0;
			}
		}
				
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		disconnect($con);
		die;
	}
	else if ($operation==2)  //Delete here======================================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$id=str_replace("'",'',$update_id);
		$return_no=str_replace("'",'',$txt_bill_no);
		$field_array_delivery="bill_status";
		if(str_replace("'",'',$delete_id)!="")
		{
			$delete_id=str_replace("'",'',$delete_id);
			$rID3=execute_query( "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id)",0);
			$delete_id=explode(",",str_replace("'",'',$delete_id));
			for ($i=0;$i<count($delete_id);$i++)
			{
				$id_delivery[]=$delete_id[$i];
				$data_delivery[str_replace("'",'',$delete_id[$i])] =explode(",",("0"));
			}
		}
		//echo bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery );
		$rID4=execute_query(bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery ));
		
		if($db_type==0)
		{
			if($rID3 && $rID4)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		disconnect($con);
		die;
	}
}
//======================================================================Bill Print============================================================================================
if($action=="knitting_bill_print") 
{
    extract($_REQUEST);
	$data=explode('*',$data);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$party_library=return_library_array( "select id,buyer_name from lib_buyer", "id","buyer_name"  );
	$yarn_desc_arr=return_library_array( "select id,yarn_description from lib_subcon_charge",'id','yarn_description');
	
	$sql_mst="Select id, bill_no, bill_date, party_id, party_source, attention, bill_for from subcon_inbound_bill_mst where company_id=$data[0] and id='$data[1]' and status_active=1 and is_deleted=0";
	$dataArray=sql_select($sql_mst);
	?>
    <div style="width:930px;">
         <table width="930" cellspacing="0" align="right" border="0">
            <tr>
                <td colspan="6" align="center" style="font-size:x-large"><strong><? echo $company_library[$data[0]]; ?></strong></td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <?
                        $nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
                        foreach ($nameArray as $result)
                        { 
                        ?>
                            Plot No: <? echo $result['plot_no']; ?> 
                            Level No: <? echo $result['level_no']?>
                            Road No: <? echo $result['road_no']; ?> 
                            Block No: <? echo $result['block_no'];?> 
                            City No: <? echo $result['city'];?> 
                            Zip Code: <? echo $result['zip_code']; ?> 
                            Province No: <?php echo $result['province'];?> 
                            Country: <? echo $country_arr[$result['country_id']]; ?><br> 
                            Email Address: <? echo $result['email'];?> 
                            Website No: <? echo $result['website'];
                        }
                    ?> 
                </td>
            </tr>           
        	<tr>
                <td colspan="6" align="center" style="font-size:20px"><u><strong><? echo $data[3]; ?></strong></u></td>
            </tr>
            <tr>
                <td width="130"><strong>Bill No :</strong></td> <td width="175"><? echo $dataArray[0][csf('bill_no')]; ?></td>
                <td width="130"><strong>Bill Date: </strong></td><td width="175px"> <? echo change_date_format($dataArray[0][csf('bill_date')]); ?></td>
                <td width="130"><strong>Source :</strong></td> <td width="175"><? echo $knitting_source[$dataArray[0][csf('party_source')]]; ?></td>
            </tr>
             <tr>
             <?
			 
			 	if($dataArray[0][csf('party_source')]==2)
				{
					$party_add=$dataArray[0][csf('party_id')];
					$nameArray=sql_select( "select address_1, web_site, buyer_email, country_id from  lib_buyer where id=$party_add"); 
					foreach ($nameArray as $result)
					{ 
                    	$address="";
						if($result!="") $address=$result['address_1'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
					}
					$party_name=$party_library[$dataArray[0][csf('party_id')]].' : Address :- '.$address;
				}
				else
				{
					if($dataArray[0][csf('attention')]!='')
					{
						$attention='('.$dataArray[0][csf('attention')].')';
					}
					else
					{
						$attention='';
					}
					$party_name=$company_library[$dataArray[0][csf('party_id')]].' '.$attention;
				}
			 ?>
             
                <td><strong>Party Name : </strong></td><td colspan="5"> <? echo $party_name; ?></td>
            </tr>
        </table>
         <br>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="30">SL</th>
                <th width="60" align="center">Challan No</th>
                <th width="65" align="center">D. Date</th>
                <th width="70" align="center">Order</th> 
                <th width="70" align="center">Buyer</th>
                <th width="70" align="center">Style</th>
                <th width="180" align="center">F. Des.</th>
                <th width="30" align="center">Roll</th>
                <th width="60" align="center">D. Qty</th>
                <th width="30" align="center">UOM</th>
                <th width="30" align="center">Rate</th>
                <th width="70" align="center">Amount</th>
                <th width="" align="center">Remarks</th>
            </thead>
		 <?
		 	if($dataArray[0][csf('party_source')]==2)
			{
				$order_array=array();
				$order_sql="select id, order_no, order_uom, cust_buyer, cust_style_ref from subcon_ord_dtls where status_active=1 and is_deleted=0";
				$order_sql_result =sql_select($order_sql);
				foreach($order_sql_result as $row)
				{
					$order_array[$row[csf('id')]]['po_number']=$row[csf('order_no')];
					$order_array[$row[csf('id')]]['buyer_name']=$row[csf('cust_buyer')];
					$order_array[$row[csf('id')]]['style_ref_no']=$row[csf('cust_style_ref')];
				}
				$const_comp_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');
			}
			else if($dataArray[0][csf('party_source')]==1)
			{
				$order_array=array();
				$job_sql="select a.job_no, a.buyer_name, a.style_ref_no, b.id, b.po_number from wo_po_details_master a,  wo_po_break_down b where a.job_no=b.job_no_mst and  a.status_active=1 and a.is_deleted=0 and  b.status_active=1 and b.is_deleted=0";
				$job_sql_result =sql_select($job_sql);
				foreach($job_sql_result as $row)
				{
					$order_array[$row[csf('id')]]['buyer_name']=$party_library[$row[csf('buyer_name')]];
					$order_array[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
					$order_array[$row[csf('id')]]['po_number']=$row[csf('po_number')];
				}
				$const_comp_arr=return_library_array( "select id,product_name_details from product_details_master",'id','product_name_details');
			}
			//var_dump($order_array);
     		$i=1;
			$mst_id=$dataArray[0][csf('id')];
			$sql_result =sql_select("select id, delivery_id, delivery_date, challan_no, order_id, item_id, uom, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id from subcon_inbound_bill_dtls  where mst_id='$mst_id' and process_id='2' and status_active=1 and is_deleted=0"); 
			foreach($sql_result as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			?>
				<tr bgcolor="<? echo $bgcolor; ?>"> 
                    <td><? echo $i; ?></td>
                    <td><p><? echo $row[csf('challan_no')]; ?></p></td>
                    <td><p><? echo change_date_format($row[csf('delivery_date')]); ?></p></td>
                    <td><p><? echo $order_array[$row[csf('order_id')]]['po_number']; ?></p></td>
                    <td><p><? echo $order_array[$row[csf('order_id')]]['buyer_name']; ?></p></td>
                    <td><p><? echo $order_array[$row[csf('order_id')]]['style_ref_no']; ?></p></td>
                    <td><p><? echo $const_comp_arr[$row[csf('item_id')]]; ?></p></td>
                    <td align="right"><p><? echo $row[csf('packing_qnty')]; $tot_packing_qty+=$row[csf('packing_qnty')]; ?>&nbsp;</p></td>
                    <td align="right"><p><? echo number_format($row[csf('delivery_qty')],2,'.',''); $tot_delivery_qty+=$row[csf('delivery_qty')]; ?>&nbsp;</p></td>
                    <td><p><? echo $unit_of_measurement[$row[csf('uom')]]; ?></p></td>
                    <td align="right"><p><? echo number_format($row[csf('rate')],2,'.',''); ?>&nbsp;</p></td>
                    <td align="right"><p><? echo number_format($row[csf('amount')],2,'.','');  $total_amount += $row[csf('amount')]; ?>&nbsp;</p></td>
                    <td><p><? echo $row[csf('remarks')]; ?></p></td>
                    <? 
					$carrency_id=$row['currency_id'];
					if($carrency_id==1){$paysa_sent="Paisa";} else if($carrency_id==2){$paysa_sent="CENTS";}
				   ?>
                </tr>
                <?php
                $i++;
			}
			?>
        	<tr> 
                <td align="right" colspan="7"><strong>Total</strong></td>
                <td align="right"><? echo $tot_packing_qty; ?>&nbsp;</td>
                <td align="right"><? echo number_format($tot_delivery_qty,2,'.',''); ?>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><? echo $format_total_amount=number_format($total_amount,2,'.',''); ?>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
           <tr>
               <td colspan="13" align="left"><b>In Word: <? echo number_to_words($format_total_amount,$currency[$carrency_id],$paysa_sent); ?></b></td>
           </tr>
        </table>
         <table width="930" align="left" > 
        	<tr><td colspan="2">&nbsp;</td> </tr>
            <tr><td colspan="2" align="center"><b>TERMS & CONDITION</b></td> </tr>
        <?
/*			$terms_cond_id=$dataArray[0][csf('terms_and_condition')];
			
			if ($terms_cond_id!='')
			{
*/				$bill_no=$dataArray[0][csf('bill_no')];
				$sql_terms="Select id,terms from subcon_terms_condition where entry_form=1 and bill_no='$bill_no' ";
				$result_sql_terms =sql_select($sql_terms);
			//}

			$i=1;
			if(count($result_sql_terms)>0)
			{
				foreach($result_sql_terms as $rows)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
					<tr bgcolor="<? echo $bgcolor; ?>"> 
						<td width="30"><? echo $i; ?></td>
						<td><p><? echo $rows[csf('terms')]; ?></p></td>
					</tr>
				<?
				$i++;
				}
			}
			?>
        </table>
        <br>
		 <?
            echo signature_table(47, $data[0], "930px");
         ?>
   </div>
   </div>
<?
}

if($action=="remarks_popup")
{
	echo load_html_head_contents("Remarks","../../", 1, 1, $unicode);
	extract($_REQUEST);
	?>
    <script>
	function js_set_value(val)
	{
		document.getElementById('text_new_remarks').value=val;
		parent.emailwindow.hide();
	}
	</script>
    </head>
<body>
<div align="center">
	<fieldset style="width:400px;margin-left:4px;">
        <form name="remarksfrm_1"  id="remarksfrm_1" autocomplete="off">
            <table cellpadding="0" cellspacing="0" width="370" >
                <tr>
                    <td align="center"><input type="hidden" name="auto_id" id="auto_id" value="<? echo $data; ?>" />
                      <textarea id="text_new_remarks" name="text_new_remarks" class="text_area" title="Maximum 1000 Character" maxlength="1000" style="width:330px; height:270px" placeholder="Remarks Here. Maximum 1000 Character." ><? echo $data; ?></textarea>
                    </td>
                </tr>
                <tr>
                	<td align="center">
                 <input type="button" id="formbuttonplasminus" align="middle" class="formbutton" style="width:100px" value="Close" onClick="js_set_value(document.getElementById('text_new_remarks').value)" />
                 	</td>
                </tr>
            </table>
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
exit();
}