-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_capacity_year_dtls`
--

DROP TABLE IF EXISTS `lib_capacity_year_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_capacity_year_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `month_id` tinyint(2) NOT NULL DEFAULT '0',
  `working_day` int(11) NOT NULL DEFAULT '0',
  `capacity_month_min` int(11) NOT NULL DEFAULT '0',
  `capacity_month_pcs` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_capacity_year_dtls`
--

LOCK TABLES `lib_capacity_year_dtls` WRITE;
/*!40000 ALTER TABLE `lib_capacity_year_dtls` DISABLE KEYS */;
INSERT INTO `lib_capacity_year_dtls` VALUES (1,1,1,26,960960,320320),(2,1,2,0,0,0),(3,1,3,0,0,0),(4,1,4,25,2772000,924000),(5,1,5,0,0,0),(6,1,6,0,0,0),(7,1,7,0,0,0),(8,1,8,0,0,0),(9,1,9,0,0,0),(10,1,10,0,0,0),(11,1,11,0,0,0),(12,1,12,0,0,0),(13,2,1,0,0,0),(14,2,2,0,0,0),(15,2,3,0,0,0),(16,2,4,0,0,0),(17,2,5,0,0,0),(18,2,6,0,0,0),(19,2,7,0,0,0),(20,2,8,0,0,0),(21,2,9,30,1260000,193846),(22,2,10,0,0,0),(23,2,11,0,0,0),(24,2,12,0,0,0);
/*!40000 ALTER TABLE `lib_capacity_year_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
