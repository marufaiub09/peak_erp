<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_buyer_name")
{
	echo create_drop_down( "cbo_buyer_id", 181, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "","","","","","",4 ); 
	exit();
}

//=================BROWSE LIST VIEW==============
if ($action=="list_view_subcon_dying_charge")
{
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$color_library_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name");  
	$arr=array (0=>$company_arr,2=>$process_type,3=>$conversion_cost_head_array,4=>$color_library_arr,5=>$fabric_typee,7=>$unit_of_measurement,8=>$production_process,10=>$buyer_arr,11=>$row_status);
	echo  create_list_view ( "list_view", "Company Name,Const. Compo.,Process Type,Process Name,Color,Width/Dia type,In House Rate,UOM,Rate type,Cust. Rate,Buyer,Status", "90,130,70,70,70,80,60,40,60,70,80,50","970","250",1, "select id, comapny_id, const_comp, process_type_id, process_id, color_id, width_dia_id, in_house_rate, uom_id, rate_type_id ,customer_rate, buyer_id, status_active from lib_subcon_charge where status_active!=0 and is_deleted=0 and rate_type_id in (3,4,7,8)", "get_php_form_data", "id","'load_php_data_to_form'", 1, "comapny_id,0,process_type_id,process_id,color_id,width_dia_id,0,uom_id,rate_type_id,0,buyer_id,status_active", $arr, "comapny_id,const_comp,process_type_id,process_id,color_id,width_dia_id,in_house_rate,uom_id,rate_type_id,customer_rate,buyer_id,status_active","requires/lib_subcontract_dyeing_controller", 'setFilterGrid("list_view",-1);','0,0,0,0,0,0,2,0,0,0,0,0' );
								
}

if ($action=="load_php_data_to_form")
{
	$color_library_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name");
	$nameArray=sql_select( "select id, comapny_id, const_comp, process_type_id, process_id, color_id, width_dia_id, in_house_rate, uom_id, rate_type_id, customer_rate, buyer_id,status_active from lib_subcon_charge where id='$data'" );
	foreach ($nameArray as $inf)
	{
		echo "document.getElementById('cbo_company_id').value 		= '".($inf[csf("comapny_id")])."';\n";    
		echo "document.getElementById('text_const_compo').value 	= '".($inf[csf("const_comp")])."';\n"; 
		echo "document.getElementById('cbo_process_type').value  	= '".($inf[csf("process_type_id")])."';\n";
		echo "document.getElementById('cbo_process_id').value  		= '".($inf[csf("process_id")])."';\n";
		echo "document.getElementById('txt_color').value  			= '".$color_library_arr[$inf[csf("color_id")]]."';\n";
		echo "document.getElementById('cbo_dia_width').value  		= '".($inf[csf("width_dia_id")])."';\n"; 
		echo "document.getElementById('text_in_house_rate').value  	= '".($inf[csf("in_house_rate")])."';\n"; 
		echo "document.getElementById('cbo_uom').value  			= '".($inf[csf("uom_id")])."';\n";
		echo "document.getElementById('cbo_rate_type').value  		= '".($inf[csf("rate_type_id")])."';\n"; 
		echo "document.getElementById('txt_customer_rate').value  		= '".($inf[csf("customer_rate")])."';\n"; 
		echo "document.getElementById('cbo_buyer_id').value  		= '".($inf[csf("buyer_id")])."';\n"; 
		echo "document.getElementById('cbo_status').value  			= '".($inf[csf("status_active")])."';\n"; 
		echo "document.getElementById('update_id').value  			= '".($inf[csf("id")])."';\n"; 
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_dyeing_charge',1);\n";  
	}
}
//=================SAVE UPDATE DELETE==============
$color_library_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)
	{
		$color_tbl_id = return_id( str_replace("'","",$txt_color), $color_library_arr, "lib_color", "id,color_name");
		
		if (is_duplicate_field( "id", "lib_subcon_charge", "comapny_id=$cbo_company_id and const_comp=$text_const_compo and process_type_id=$cbo_process_type and process_id=$cbo_process_id and color_id=$color_tbl_id and width_dia_id=$cbo_dia_width and in_house_rate=$text_in_house_rate and uom_id=$cbo_uom and rate_type_id=$cbo_rate_type and id!=$update_id and is_deleted=0 and status_active=1" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$id=return_next_id( "id", "lib_subcon_charge", 1 ) ; 
			$color_tbl_id = return_id( str_replace("'","",$txt_color), $color_library_arr, "lib_color", "id,color_name");
			$field_array="id,comapny_id,const_comp,process_type_id,process_id,color_id,width_dia_id,in_house_rate,uom_id,rate_type_id,customer_rate,buyer_id,inserted_by,insert_date,status_active,is_deleted";
			$data_array="(".$id.",".$cbo_company_id.",".$text_const_compo.",".$cbo_process_type.",".$cbo_process_id.",'".$color_tbl_id."',".$cbo_dia_width.",".$text_in_house_rate.",".$cbo_uom.",".$cbo_rate_type.",".$txt_customer_rate.",".$cbo_buyer_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',".$cbo_status.",'0')";
			$rID=sql_insert("lib_subcon_charge",$field_array,$data_array,1);
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo "0**".str_replace("'",'',$id);
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$id;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
			else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		}
	}
//=================UPDATE==============
	else if ($operation==1)   // Update Here
	{
		/*if (is_duplicate_field( "id", "lib_subcon_charge", "comapny_id=$cbo_company_id and rate_type_id=$cbo_rate_type and  uom_id=$cbo_uom  and in_house_rate=$text_in_house_rate and id!=$update_id and is_deleted=0 and status_active=1" ) == 1)
		{
			echo "11**0"; die;
		}*/
		$color_tbl_id = return_id( str_replace("'","",$txt_color), $color_library_arr, "lib_color", "id,color_name");
		if (is_duplicate_field( "id", "lib_subcon_charge", "comapny_id=$cbo_company_id and const_comp=$text_const_compo and process_type_id=$cbo_process_type and process_id=$cbo_process_id and color_id=$color_tbl_id and width_dia_id=$cbo_dia_width and in_house_rate=$text_in_house_rate and uom_id=$cbo_uom and rate_type_id=$cbo_rate_type and id!=$update_id and is_deleted=0 and status_active=1" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$id=str_replace("'",'',$update_id);
			$color_tbl_id = return_id( str_replace("'","",$txt_color), $color_library_arr, "lib_color", "id,color_name");
			$field_array="comapny_id*const_comp*process_type_id*process_id*color_id*width_dia_id*in_house_rate*uom_id*rate_type_id*customer_rate*buyer_id*updated_by*update_date*status_active*is_deleted";
			$data_array="".$cbo_company_id."*".$text_const_compo."*".$cbo_process_type."*".$cbo_process_id."*".$color_tbl_id."*".$cbo_dia_width."*".$text_in_house_rate."*".$cbo_uom."*".$cbo_rate_type."*".$txt_customer_rate."*".$cbo_buyer_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*".$cbo_status."*'0'";
			
			$rID=sql_update("lib_subcon_charge",$field_array,$data_array,"id","".$update_id."",1);
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo "1**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
			else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		}
	}
//=================DELETE==============
	else if ($operation==2)
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array="updated_by*update_date*status_active*is_deleted";
		$data_array="".$_SESSION['logic_erp']['user_id']."*'".$date."'*'0'*'1'";
		
		$rID=sql_delete("lib_subcon_charge",$field_array,$data_array,"id","".$update_id."",1);
		
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "2**".$rID;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$rID;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
	      if($rID )
			    {
					oci_commit($con);   
					echo "2**".$rID;
				}
			else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		}
		disconnect($con);
		die;
	}
	
}
?>
        
