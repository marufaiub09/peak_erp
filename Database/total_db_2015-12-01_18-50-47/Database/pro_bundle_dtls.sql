-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_bundle_dtls`
--

DROP TABLE IF EXISTS `pro_bundle_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_bundle_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bundle_mst_id` int(11) NOT NULL DEFAULT '0',
  `pcs_per_bundle` int(11) NOT NULL DEFAULT '0',
  `pcs_range_start` int(11) NOT NULL DEFAULT '0',
  `pcs_range_end` int(11) NOT NULL DEFAULT '0',
  `color_size_id` int(11) NOT NULL DEFAULT '0',
  `bundle_bar_code` varchar(35) NOT NULL,
  `bundle_bar_code_prefix` varchar(15) NOT NULL DEFAULT '0',
  `cut_no` int(11) NOT NULL DEFAULT '0',
  `custom_bundle_no` int(11) NOT NULL DEFAULT '0',
  `custom_gmts_range_start` int(11) NOT NULL DEFAULT '0',
  `custom_gmts_range_end` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_bundle_dtls`
--

LOCK TABLES `pro_bundle_dtls` WRITE;
/*!40000 ALTER TABLE `pro_bundle_dtls` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_bundle_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
