-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pri_quo_trim_cost_dtls`
--

DROP TABLE IF EXISTS `wo_pri_quo_trim_cost_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pri_quo_trim_cost_dtls` (
  `id` int(11) NOT NULL DEFAULT '0',
  `quotation_id` int(11) NOT NULL DEFAULT '0',
  `trim_group` int(11) NOT NULL DEFAULT '0',
  `cons_uom` int(11) NOT NULL DEFAULT '0',
  `cons_dzn_gmts` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `apvl_req` int(11) NOT NULL DEFAULT '0',
  `nominated_supp` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  KEY `quotation_id` (`quotation_id`),
  KEY `trim_group` (`trim_group`),
  KEY `apvl_req` (`apvl_req`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pri_quo_trim_cost_dtls`
--

LOCK TABLES `wo_pri_quo_trim_cost_dtls` WRITE;
/*!40000 ALTER TABLE `wo_pri_quo_trim_cost_dtls` DISABLE KEYS */;
INSERT INTO `wo_pri_quo_trim_cost_dtls` VALUES (1,1,17,52,1.1,0.4,0.44,1,3,2,'2015-11-01 11:13:28',2,'2015-11-01 11:15:34',1,0),(2,1,2,2,1.1,0.15,0.165,1,3,2,'2015-11-01 11:13:28',2,'2015-11-01 11:15:34',1,0),(3,1,6,2,1.1,0.25,0.275,1,3,2,'2015-11-01 11:13:28',2,'2015-11-01 11:15:34',1,0),(4,1,209,2,1.1,1.55,1.705,1,3,2,'2015-11-01 11:15:34',0,NULL,1,0),(5,1,388,2,1.1,0.1,0.11,1,3,2,'2015-11-01 11:15:34',0,NULL,1,0),(6,1,25,2,1.1,0.45,0.495,1,3,2,'2015-11-01 11:15:34',0,NULL,1,0),(7,1,49,2,1.1,0.2,0.22,1,3,2,'2015-11-01 11:15:34',0,NULL,1,0),(8,1,18,1,1.1,0.75,0.825,1,3,2,'2015-11-01 11:15:34',0,NULL,1,0),(9,1,19,1,1.1,0.25,0.275,1,3,2,'2015-11-01 11:15:34',0,NULL,1,0),(10,1,132,1,1.1,0.05,0.055,1,3,2,'2015-11-01 11:15:34',0,NULL,1,0);
/*!40000 ALTER TABLE `wo_pri_quo_trim_cost_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
