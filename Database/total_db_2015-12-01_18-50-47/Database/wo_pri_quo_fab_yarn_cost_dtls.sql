-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pri_quo_fab_yarn_cost_dtls`
--

DROP TABLE IF EXISTS `wo_pri_quo_fab_yarn_cost_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pri_quo_fab_yarn_cost_dtls` (
  `id` int(11) NOT NULL DEFAULT '0',
  `quotation_id` int(11) NOT NULL DEFAULT '0',
  `count_id` int(11) NOT NULL DEFAULT '0',
  `copm_one_id` int(11) NOT NULL DEFAULT '0',
  `percent_one` int(11) NOT NULL DEFAULT '0',
  `copm_two_id` int(11) NOT NULL DEFAULT '0',
  `percent_two` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `cons_ratio` double NOT NULL,
  `cons_qnty` double NOT NULL DEFAULT '0',
  `supplier_id` int(11) NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  KEY `quotation_id` (`quotation_id`),
  KEY `count_id` (`count_id`),
  KEY `copm_one_id` (`copm_one_id`),
  KEY `copm_two_id` (`copm_two_id`),
  KEY `type_id` (`type_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pri_quo_fab_yarn_cost_dtls`
--

LOCK TABLES `wo_pri_quo_fab_yarn_cost_dtls` WRITE;
/*!40000 ALTER TABLE `wo_pri_quo_fab_yarn_cost_dtls` DISABLE KEYS */;
INSERT INTO `wo_pri_quo_fab_yarn_cost_dtls` VALUES (1,1,1,1,100,0,0,1,100,2.98,0,2.71,8.0758,2,'2015-11-01 11:03:41',0,NULL,1,0);
/*!40000 ALTER TABLE `wo_pri_quo_fab_yarn_cost_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
