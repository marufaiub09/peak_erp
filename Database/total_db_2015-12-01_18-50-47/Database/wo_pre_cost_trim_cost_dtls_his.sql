-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pre_cost_trim_cost_dtls_his`
--

DROP TABLE IF EXISTS `wo_pre_cost_trim_cost_dtls_his`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pre_cost_trim_cost_dtls_his` (
  `id` int(11) NOT NULL DEFAULT '0',
  `approved_no` int(11) DEFAULT NULL,
  `pre_cost_trim_cost_dtls_id` int(11) DEFAULT NULL,
  `job_no` varchar(25) NOT NULL,
  `trim_group` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL,
  `brand_sup_ref` varchar(100) NOT NULL,
  `cons_uom` int(11) NOT NULL DEFAULT '0',
  `cons_dzn_gmts` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `apvl_req` int(11) NOT NULL DEFAULT '0',
  `nominated_supp` int(11) NOT NULL DEFAULT '0',
  `cons_breack_down` text NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  KEY `job_no` (`job_no`),
  KEY `trim_group` (`trim_group`),
  KEY `description` (`description`),
  KEY `brand_sup_ref` (`brand_sup_ref`),
  KEY `apvl_req` (`apvl_req`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pre_cost_trim_cost_dtls_his`
--

LOCK TABLES `wo_pre_cost_trim_cost_dtls_his` WRITE;
/*!40000 ALTER TABLE `wo_pre_cost_trim_cost_dtls_his` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_pre_cost_trim_cost_dtls_his` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
