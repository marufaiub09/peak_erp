<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Date Wise Ship Quantity Report.
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	08-06-2014
Updated by 		: 	Maruf	
Update date		: 	08-12-2015	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
error_reporting('0');
session_start();



if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Cost Breakdown Report","../../../", 1, 1, $unicode,1,1);
?>	

<script>

 	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../../logout.php";  
	var permission = '<? echo $permission; ?>';
	
	function fn_report_generated()
	{
		if(form_validation('cbo_company_name','Company Name')==false)
		{
			return;
		}
		else
		{	
			var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*cbo_year_from*cbo_month_from*cbo_year_to*cbo_month_to',"../../../");
			freeze_window(3);
			http.open("POST","requires/date_wise_ship_qty_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
		
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			$('#report_container2').html("");
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Html Preview" name="Print" class="formbutton" style="width:100px"/>&nbsp;&nbsp;&nbsp;<input type="button" onclick="print_report()" value="Print" name="Print" class="formbutton" style="width:100px"/>';
			show_msg('3');
			release_freezing();
		}
	}
	
	function new_window()
	{
		
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css"/><style>.verticalText2{writing-mode: tb-rl; filter: flipv fliph; -webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);-o-transform: rotate(270deg);-ms-transform: rotate(270deg);transform: rotate(270deg);width: 1em;line-height: 1em;};@media print {thead {display: table-header-group;}}</style></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');
		d.close(); 
	
	
	}


	function print_report()
	{
		$('#form_body').hide();
		$('#report_container').hide();
		window.print(document.getElementById('report_container').innerHTML);
		$('#form_body').show();
		$('#report_container').show();
	}
	
	function fn_report_generated_order()
	{
		if(form_validation('cbo_company_name','Company Name')==false)
		{
			return;
		}
		else
		{	
			var data="action=report_generate_order"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*cbo_year_from*cbo_month_from*cbo_year_to*cbo_month_to',"../../../");
			freeze_window(3);
			http.open("POST","requires/date_wise_ship_qty_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_order_reponse;
		}
	}
		
	function fn_report_generated_order_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			$('#report_container2').html("");
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Html Preview" name="Print" class="formbutton" style="width:100px"/>&nbsp;&nbsp;&nbsp;<input type="button" onclick="print_report()" value="Print" name="Print" class="formbutton" style="width:100px"/>';
			show_msg('3');
			release_freezing();
		}
	}	
</script>


<style>
	.verticalText2 
	{
	 writing-mode: tb-rl;
     filter: flipv fliph;
    -webkit-transform: rotate(270deg);
    -moz-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
    transform: rotate(270deg);
    width: 1em;
    line-height: 1em;
	
	}
</style>

</head>
 
<body onLoad="set_hotkey();">
		 
<form id="ship_status_rpt" action="" autocomplete="off" method="post">
    <div style="width:100%;" align="center" id="form_body">
        <?php echo load_freeze_divs ("../../../",""); ?>
         <h3 align="left" id="accordion_h1" style="width:800px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
            <div id="content_search_panel"> 
            <fieldset style="width:800px;">
                <table class="rpt_table" width="790" cellpadding="1" cellspacing="2" align="center">
                	<thead>
                    	<tr>                   
                            <th width="130">Company Name</th>
                            <th width="120">Buyer Name</th>
                            <th width="160">Start Range</th>
                            <th width="160">End Range</th>
                            <th><input type="reset" id="reset_btn" class="formbutton" style="width:100px" value="Reset" onClick="reset_form('ship_status_rpt','report_container*report_container2','','','');" /></th>
                        </tr>
                     </thead>
                    <tbody>
                    <tr class="general">
                        <td> 
                            <?php
                                echo create_drop_down( "cbo_company_name", 130, "select comp.id, comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/date_wise_ship_qty_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                            ?>
                        </td>
                        <td id="buyer_td"> 
                            <?php echo create_drop_down( "cbo_buyer_name", 120, $blank_array,"", 1, "-- All Buyer --", $selected, "",0,"" ); ?>
                        </td>
                        <td colspan="2">
                        <!--<input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:100px" placeholder="From Date" >&nbsp; To
                        <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:100px"  placeholder="To Date" >-->
							<?php
								$year_current=date("Y");
								$month_current=date("m");
                            	echo create_drop_down( "cbo_year_from", 70, $year,"", 1, "-Select-",$year_current);
								echo " ";
								echo create_drop_down( "cbo_month_from", 70, $months,"", 1, "-Select-",$month_current);
								echo " ";
								echo '&nbsp;<span style="font-size:16px; font-weight:bold">To:</span>';
								echo " ";
                        		echo create_drop_down( "cbo_year_to", 70, $year,"", 1, "-Select-",$year_current);
								echo " ";
								echo create_drop_down( "cbo_month_to", 70, $months,"", 1, "-Select-",$month_current);
                        
                        	?>
                        </td>
                        <td>
                            <input type="button" id="show_button" class="formbutton" style="width:90px" value="Buyer Wise" onClick="fn_report_generated()" />
                            <input type="button" id="show_button" class="formbutton" style="width:90px" value="Order Wise" onClick="fn_report_generated_order()" />
                        </td>
                    </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>
    <div id="report_container" align="center"></div>
    <div id="report_container2"></div>
 </form>    
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
