-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_standard_cm_entry`
--

DROP TABLE IF EXISTS `lib_standard_cm_entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_standard_cm_entry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `applying_period_date` date DEFAULT NULL,
  `applying_period_to_date` date DEFAULT NULL,
  `bep_cm` double NOT NULL,
  `asking_profit` double NOT NULL,
  `max_profit` double NOT NULL,
  `asking_cm` double NOT NULL,
  `monthly_cm_expense` double NOT NULL,
  `no_factory_machine` int(11) NOT NULL DEFAULT '0',
  `working_hour` double NOT NULL,
  `cost_per_minute` double NOT NULL,
  `asking_avg_rate` double NOT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `actual_cm` double NOT NULL,
  `depreciation_amorti` double NOT NULL,
  `interest_expense` double NOT NULL,
  `income_tax` double NOT NULL,
  `operating_expn` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `applying_period_date` (`applying_period_date`),
  KEY `applying_period_to_date` (`applying_period_to_date`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_standard_cm_entry`
--

LOCK TABLES `lib_standard_cm_entry` WRITE;
/*!40000 ALTER TABLE `lib_standard_cm_entry` DISABLE KEYS */;
INSERT INTO `lib_standard_cm_entry` VALUES (1,1,'2015-01-01','2015-01-31',3,4,5,7,10000000,240,10,2.6709,1.5,1,0,2,'2015-01-04 04:05:33',1,'2015-05-06 11:04:22',0,4,0,4,0.4,3),(2,1,'2015-03-01','2015-03-31',3,4,5,7,10000000,240,10,2.6709,1.5,1,0,1,'2015-03-10 12:36:13',1,'2015-05-06 11:04:31',0,4,0,3,0.4,3),(3,1,'2015-05-01','2015-05-31',3,4,5,7,10000000,240,10,2.6709,1.5,1,0,2,'2015-05-04 17:21:07',1,'2015-05-04 17:45:31',0,4,0,3,0.4,3),(4,1,'2015-02-01','2015-02-28',3,4,5,7,10000000,240,10,2.6709,1.5,1,0,2,'2015-05-04 17:22:53',1,'2015-05-04 17:46:27',0,4,0,3,0.4,3),(5,1,'2015-04-01','2015-04-30',3,4,5,7,10000000,240,10,2.6709,1.5,1,0,2,'2015-05-04 17:24:34',1,'2015-05-04 17:46:44',0,4,0,3,0.4,3),(6,1,'2015-06-01','2015-06-30',3,4,5,7,10000000,240,10,2.6709,1.5,1,0,2,'2015-05-04 17:24:54',1,'2015-05-04 17:46:39',0,4,0,4,0.4,3),(7,1,'2015-07-01','2015-07-31',3,4,5,7,10000000,240,10,2.6709,1.5,1,0,2,'2015-05-04 17:25:03',1,'2015-05-06 11:04:54',0,4,0,3,0.4,3),(8,1,'2015-08-01','2015-08-31',3,4,5,7,10000000,177,10,3.6216,1.5,1,0,20,'2015-08-24 13:10:04',20,'2015-08-26 09:20:11',0,4,0,4,0.4,3),(9,1,'2015-09-01','2015-09-30',3,4,5,7,10000000,200,10,3.2051,1.5,1,0,2,'2015-09-02 17:04:19',2,'2015-10-11 14:43:47',0,4,0,4,0.4,3),(10,1,'2015-10-01','2015-10-31',3,4,5,7,10000000,177,8,4.527,1.5,1,0,2,'2015-10-11 14:42:16',0,NULL,0,4,0,4,0.4,3),(11,1,'2015-11-01','2015-11-30',3,4,5,7,10000000,200,8,4.0064,1.5,1,0,2,'2015-10-11 14:43:41',0,NULL,0,4,0,4,0.4,3);
/*!40000 ALTER TABLE `lib_standard_cm_entry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
