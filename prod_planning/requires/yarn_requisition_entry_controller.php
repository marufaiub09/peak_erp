<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=$_SESSION['page_permission'];

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$company_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
$supllier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 130, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );     	 
	exit();
}

if($action=="order_no_search_popup")
{
	echo load_html_head_contents("Order No Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_order_id').val( id );
			$('#hide_order_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:780px;">
            <table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Order No</th>
                    <th>Shipment Date</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_order_no" id="hide_order_no" value="" />
                    <input type="hidden" name="hide_order_id" id="hide_order_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",0,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Order No",2=>"Style Ref",3=>"Job No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 110, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
                            <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
                        </td>	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value, 'create_order_no_search_list_view', 'search_div', 'yarn_requisition_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    	</td>
                    </tr>
                    <tr>
                        <td colspan="5" height="20" valign="middle"><?php echo load_month_buttons(1); ?></td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_order_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==1) 
		$search_field="b.po_number"; 
	else if($search_by==2) 
		$search_field="a.style_ref_no"; 	
	else 
		$search_field="a.job_no";
		
	$start_date =trim($data[4]);
	$end_date =trim($data[5]);	
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,"yyyy-mm-dd", "-")."' and '".change_date_format($end_date,"yyyy-mm-dd", "-")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,'','',1)."' and '".change_date_format($end_date,'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	
	$arr=array (0=>$company_arr,1=>$buyer_arr);
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql= "select b.id, $year_field a.job_no, a.job_no_prefix_num, a.company_name, a.buyer_name, a.style_ref_no, b.po_number, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $date_cond order by b.id, b.pub_shipment_date";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No, Po No, Shipment Date", "100,120,50,60,130,120","760","220",0, $sql , "js_set_value", "id,po_number", "", 1, "company_name,buyer_name,0,0,0,0,0", $arr , "company_name,buyer_name,year,job_no_prefix_num,style_ref_no,po_number,pub_shipment_date", "",'','0,0,0,0,0,0,3','',1) ;
	
   exit(); 
}


if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$type = str_replace("'","",$cbo_type);
	$planning_status=str_replace("'","",$cbo_planning_status);
	$company_name=$cbo_company_name;
	
	//if(str_replace("'","",$cbo_buyer_name)==0) $buyer_name="%%"; else $buyer_name=str_replace("'","",$cbo_buyer_name);
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_id=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	
	if(str_replace("'","",trim($txt_order_no))=="")
	{
		$po_id_cond="";
	}
	else
	{
		if(str_replace("'","",$hide_order_id)!="")
		{
			$po_id=str_replace("'","",$hide_order_id);
		}
		else
		{
			$po_number=trim(str_replace("'","",$txt_order_no))."%";
			if($db_type==0)
			{
				$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");
			}
			else
			{
				$po_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");
			}
			if($po_id=="") $po_id=0;
		}
		
		$po_id_cond="and c.po_id in(".$po_id.")";
	}
	//echo $po_id_cond;
	if(str_replace("'","",$txt_machine_dia)=="") $machine_dia="%%"; else $machine_dia="%".str_replace("'","",$txt_machine_dia)."%";
	
	$po_array=array();
	$costing_sql=sql_select("select a.job_no, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company_name");
	foreach($costing_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')]; 
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')]; 
	}
	
	if($db_type==0)
	{
		$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_name group by dtls_id", "dtls_id", "po_id"  );
	}
	else
	{
		$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_name group by dtls_id", "dtls_id", "po_id"  );
	}
	
	$reqs_array=array();
	$reqs_sql=sql_select("select knit_id, requisition_no as reqs_no, sum(yarn_qnty) as yarn_req_qnty from ppl_yarn_requisition_entry where status_active=1 and is_deleted=0 group by knit_id, requisition_no");
	foreach($reqs_sql as $row)
	{
		$reqs_array[$row[csf('knit_id')]]['reqs_no']=$row[csf('reqs_no')];
		$reqs_array[$row[csf('knit_id')]]['qnty']=$row[csf('yarn_req_qnty')]; 
	}
	
	?>
	<fieldset style="width:1800px; margin-top:10px">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1780" class="rpt_table" >
			<thead>
				<th width="40">SL</th>
                <th width="80">Program No</th>
				<th width="80">Program Date</th>
				<th width="70">Buyer</th>
                <th width="100">Job No</th>
				<th width="140">Order No</th>
				<th width="80"><?php echo $company_arr[str_replace("'","",$company_name)]; ?></th>
				<th width="110">Style</th>
                <th width="80">Dia / GG</th>
                <th width="145">Fabric Desc.</th>
                <th width="70">Fabric Gsm</th>
				<th width="70">Fabric Dia</th>
				<th width="80">Width/Dia Type</th>
                <th width="90">Color Range</th>
                <th width="100">Program Qnty</th>
				<th width="100">Yarn Req. Qnty</th>
                <th width="70">Req. No</th>
                <th width="80">Start Date</th>
				<th width="80">T.O.D</th>
				<th>Status</th>
			</thead>
		</table>
		<div style="width:1780px; overflow-y:scroll; max-height:330px;" id="buyer_list_view" align="center">
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1760" class="rpt_table" id="tbl_list_search">
				<tbody>
					<?php 
						$i=1; $k=1; $tot_program_qnty=0; $tot_yarn_req_qnty=0; $machine_dia_gg_array=array();
						$sql="select a.company_id, a.buyer_id, a.body_part_id, a.fabric_desc, a.gsm_weight, a.dia, a.width_dia_type, b.id, b.color_range, b.machine_dia, b.machine_gg, b.program_qnty, b.program_date, b.status, b.start_date, b.end_date from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id=$company_name and b.knitting_source=$type and machine_dia like '$machine_dia' and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $buyer_id_cond $po_id_cond group by a.company_id, a.buyer_id, a.body_part_id, a.fabric_desc, a.gsm_weight, a.dia, a.width_dia_type, b.id, b.color_range, b.machine_dia, b.machine_gg, b.program_qnty, b.program_date, b.status, b.start_date, b.end_date order by b.machine_dia,b.machine_gg";	
						
						$nameArray=sql_select( $sql );
						foreach ($nameArray as $row)
						{
							$machine_dia_gg=$row[csf('machine_dia')].'X'.$row[csf('machine_gg')];
							
							/*$sql_reqs="select knit_id, requisition_no as reqs_no, sum(yarn_qnty) as yarn_req_qnty from ppl_yarn_requisition_entry where knit_id=".$row[csf('id')]." and status_active=1 and is_deleted=0 group by knit_id, requisition_no";
							$result_reqs=sql_select($sql_reqs);
							$yarn_req_qnty=$result_reqs[0][csf('yarn_req_qnty')];
							$reqs_no=$result_reqs[0][csf('reqs_no')];
							$reqs_knit_id=$result_reqs[0][csf('knit_id')];*/
							
							$yarn_req_qnty=$reqs_array[$row[csf('id')]]['qnty'];
							$reqs_no=$reqs_array[$row[csf('id')]]['reqs_no'];
							
							$balance_qnty=$row[csf('program_qnty')]-$yarn_req_qnty;
							
							if(($planning_status==3 && $balance_qnty<=0) || ($planning_status==1 && $balance_qnty>0))
							{
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
						   
								if(!in_array($machine_dia_gg, $machine_dia_gg_array))
								{
									if ($k!=1)
									{
									?>
										<tr bgcolor="#CCCCCC">
											<td colspan="14" align="right"><b>Sub Total</b></td>
											<td align="right"><b><?php echo number_format($sub_tot_program_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($sub_tot_yarn_req_qnty,2,'.',''); ?></b></td>
											<td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
										</tr>
									<?php
										$sub_tot_program_qnty = 0;
										$sub_tot_yarn_req_qnty = 0;
									}
									
								?>
									<tr bgcolor="#EFEFEF">
										<td colspan="20"><b>Machine Dia:- <?php echo $machine_dia_gg; ?></b></td>
									</tr>
								<?php
									$machine_dia_gg_array[]=$machine_dia_gg;
									$k++;
								}
								
								$po_id=array_unique(explode(",",$plan_details_array[$row[csf('id')]]));
								$po_no=''; $style_ref=''; $job_no='';
								
								foreach($po_id as $val)
								{
									if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
									if($style_ref=='') $style_ref=$po_array[$val]['style_ref'];
									if($job_no=='') $job_no=$po_array[$val]['job_no'];
								}
								
								$cons_comps=explode(",",$row[csf('fabric_desc')]);
								$comps=$cons_comps[1];
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
									<td width="40"><?php echo $i; ?></td>
                                    <td width="80">&nbsp;&nbsp;<?php echo $row[csf('id')]; ?>&nbsp;</td>
									<td width="80" align="center"><?php echo change_date_format($row[csf('program_date')]); ?></td>
									<td width="70"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                                    <td width="100"><p><?php echo $job_no; ?></p></td>
									<td width="140"><p><?php echo $po_no; ?></p></td>
									<td width="80"><p><?php echo $plan_details_array[$row[csf('id')]]; ?></p></td>
									<td width="110"><p><?php echo $style_ref; ?></p></td>
									<td width="80"><p><?php echo $machine_dia_gg; ?></p></td>
									<td width="145"><p><?php echo $row[csf('fabric_desc')]; ?></p></td>
									<td width="70"><p><?php echo $row[csf('gsm_weight')]; ?></p></td>
									<td width="70"><p><?php echo $row[csf('dia')]; ?></p></td>
									<td width="80"><?php echo $fabric_typee[$row[csf('width_dia_type')]]; ?></td>
                                    <td width="90"><p><?php echo $color_range[$row[csf('color_range')]]; ?></p></td>
									<td align="right" width="100"><?php echo number_format($row[csf('program_qnty')],2); ?></td>
									<td align="right" width="100">
										<input type="text" name="txt_yarn_req_qnty[]" id="txt_yarn_req_qnty_<?php echo $i; ?>" style="width:90px" class="text_boxes_numeric" readonly value="<?php if($yarn_req_qnty>0) echo number_format($yarn_req_qnty,2); ?>" placeholder="Single Click" onClick="openmypage_yarnReq(<?php echo $i; ?>,'<?php echo $row[csf('id')]; ?>',<?php echo $company_name; ?>,'<?php echo $comps; ?>','<?php echo $job_no; ?>')"/>
									</td>
									<td align="center" width="70"><?php echo "<a href='##' onclick=\"generate_report2(".$row[csf('company_id')].",".$row[csf('id')].")\">$reqs_no </a>" //if($type==3) echo $po_no; else ?></td>
									<td width="80" align="center">&nbsp;<?php if($row[csf('start_date')]!="" && $row[csf('start_date')]!="0000-00-00") echo change_date_format($row[csf('start_date')]); ?></td>
									<td width="80" align="center">&nbsp;<?php if($row[csf('end_date')]!="" && $row[csf('end_date')]!="0000-00-00") echo change_date_format($row[csf('end_date')]); ?></td>
									<td><p><?php echo $knitting_program_status[$row[csf('status')]]; ?>&nbsp;</p></td>
								</tr>
								<?php
								
								$sub_tot_program_qnty+=$row[csf('program_qnty')];
								$sub_tot_yarn_req_qnty+=$yarn_req_qnty;
								
								$tot_program_qnty+=$row[csf('program_qnty')];
								$tot_yarn_req_qnty+=$yarn_req_qnty;
								
								$i++;
							}
						}
						if($i>1)
						{
						?>
							<tr bgcolor="#CCCCCC">
								<td colspan="14" align="right"><b>Sub Total</b></td>
                                <td align="right"><b><?php echo number_format($sub_tot_program_qnty,2,'.',''); ?></b></td>
                                <td align="right"><b><?php echo number_format($sub_tot_yarn_req_qnty,2,'.',''); ?></b></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
							</tr>
						<?php
						}
						?>
				</tbody>
				<tfoot>
					<th colspan="14" align="right">Grand Total</th>
					<th align="right"><?php echo number_format($tot_program_qnty,2,'.',''); ?></th>
					<th align="right"><?php echo number_format($tot_yarn_req_qnty,2,'.',''); ?></th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
                    <th>&nbsp;</th>
					<th>&nbsp;</th>
				</tfoot>
			</table>
		</div>
	</fieldset>      
	<?php
	exit();
}

if($action=="print")
{
	echo load_html_head_contents("Program Qnty Info", "../", 1, 1,'','','');
	extract($_REQUEST);	$data = explode('*',$data);
	$company_id = $data[0];
	$program_id = $data[1];
	//echo $company_id;die;
	
	$company_details=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$country_arr=return_library_array( "select id,country_name from lib_country",'id','country_name');
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
	$buyer_details=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	
	if($db_type==0)
	{
		$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_id group by dtls_id", "dtls_id", "po_id"  );
	}
	else
	{
		$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_id group by dtls_id", "dtls_id", "po_id"  );
	}
	
	$po_dataArray=sql_select("select id, po_number, job_no_mst from wo_po_break_down");
	foreach($po_dataArray as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no_mst')];
	}
	
	$product_details_array=array();
	$sql="select id, supplier_id, lot, current_stock, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color, brand from product_details_master where item_category_id=1 and company_id=$company_id and status_active=1 and is_deleted=0";
	$result = sql_select($sql);
	
	foreach($result as $row)
	{
		$compos='';
		if($row[csf('yarn_comp_percent2nd')]!=0)
		{
			$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
		}
		else
		{
			$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
		}
		
		$product_details_array[$row[csf('id')]]['count']=$count_arr[$row[csf('yarn_count_id')]];
		$product_details_array[$row[csf('id')]]['comp']=$compos;
		$product_details_array[$row[csf('id')]]['type']=$yarn_type[$row[csf('yarn_type')]];
		$product_details_array[$row[csf('id')]]['lot']=$row[csf('lot')];
		$product_details_array[$row[csf('id')]]['brand']=$brand_arr[$row[csf('brand')]];
	}
	
	?>
	<div style="margin-left:20px; width:850px">
    	<div style="width:100px;float:left;position:relative;margin-top:10px">
        	<?php $image_location=return_field_value("image_location","common_photo_library","master_tble_id=$company_id and form_name='company_details' and is_deleted=0"); ?>
            <img src='../../<?php echo $image_location; ?>' height='100%' width='100%' />
        </div>
        <div style="width:50px;float:left;position:relative;margin-top:10px"></div>
    	<div style="float:left;position:relative;">   
        <table width="100%" style="margin-top:10px">
            <tr>
                <td align="center" style="font-size:16px;">
                  <?php      
                        echo $company_details[$company_id];
                  ?>
                </td>
            </tr>
            <tr>
                <td align="center" style="font-size:14px">  
                <?php
                $nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$company_id"); 
                foreach ($nameArray as $result)
                { 
                ?>
                    Plot No: <?php echo $result['plot_no']; ?> 
                    Level No: <?php echo $result['level_no']?>
                    Road No: <?php echo $result['road_no']; ?> 
                    Block No: <?php echo $result['block_no'];?> 
                    City No: <?php echo $result['city'];?> 
                    Zip Code: <?php echo $result['zip_code']; ?> 
                    Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
                    Email Address: <?php echo $result['email'];?> 
                    Website No: <?php echo $result['website'];
                }
                ?>   
               </td> 
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td width="100%" align="center" style="font-size:14px;"><b><u>Knitting Program</u></b></td>
            </tr>
        </table>
        </div>
	</div>
    <div style="margin-left:10px;float:left; width:850px">
        <?php 
			$dataArray=sql_select("select id, mst_id, knitting_source, knitting_party, program_date, color_range, stitch_length, machine_dia, machine_gg, program_qnty, remarks from ppl_planning_info_entry_dtls where id=$program_id"); 
			
			$mst_dataArray=sql_select("select booking_no, buyer_id, fabric_desc, gsm_weight, dia from ppl_planning_info_entry_mst where id=".$dataArray[0][csf('mst_id')]); 
			$booking_no=$mst_dataArray[0][csf('booking_no')];
			$buyer_id=$mst_dataArray[0][csf('buyer_id')];
			$fabric_desc=$mst_dataArray[0][csf('fabric_desc')];
			$gsm_weight=$mst_dataArray[0][csf('gsm_weight')];
			$dia=$mst_dataArray[0][csf('dia')];
		?>
        <table width="100%" style="margin-top:20px" cellspacing="7">
            <tr>
                <td width="140"><b>Program No:</b></td><td width="170"><?php echo $dataArray[0][csf('id')]; ?></td>
                <td width="170"><b>Program Date:</b></td><td><?php echo change_date_format($dataArray[0][csf('program_date')]); ?></td>
            </tr>
            <tr>
                <td><b>Factory:</b></td>
                <td>
                    <?php 
                        if($dataArray[0][csf('knitting_source')]==1) echo $company_details[$dataArray[0][csf('knitting_party')]]; 
                        else if($dataArray[0][csf('knitting_source')]==3) echo $supplier_library[$dataArray[0][csf('knitting_party')]];
                    ?>
                </td>
                <td><b>Fabrication & FGSM:</b></td><td><?php echo $fabric_desc." & ".$gsm_weight; ?></td>
            </tr>
            <tr>
            	<td><b>Address:</b></td>
                <td colspan="3">
                    <?php 
						$address='';
                        if($dataArray[0][csf('knitting_source')]==1)
						{
							$addressArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,city from lib_company where id=$company_id");
							foreach ($nameArray as $result)
							{ 
							?>
								Plot No: <?php echo $result['plot_no']; ?> 
								Level No: <?php echo $result['level_no']?>
								Road No: <?php echo $result['road_no']; ?> 
								Block No: <?php echo $result['block_no'];?> 
								City No: <?php echo $result['city'];?> 
								Country: <?php echo $country_arr[$result['country_id']]; 
							}
						}
                        else if($dataArray[0][csf('knitting_source')]==3)
						{
							$address=return_field_value("address_1","lib_supplier","id=".$dataArray[0][csf('knitting_party')]);
							echo $address;
						}
                    ?>
                </td>
            </tr>
            <tr>
                <td><b>Buyer Name:</b></td>
                <td>
                    <?php 
                        echo $buyer_details[$buyer_id];
						
						$po_id=array_unique(explode(",",$plan_details_array[$dataArray[0][csf('id')]]));
						$po_no=''; $job_no=''; 

						foreach($po_id as $val)
						{
							if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
							if($job_no=='') $job_no=$po_array[$val]['job_no'];
						}
                    ?>
                </td>
                <td><b>Order No:</b></td><td><?php echo $po_no; ?></td>
            </tr>
            <tr>
                <td><b>Booking No:</b></td><td><b><?php echo $booking_no; ?></b></td>
                <td><b>Job No:</b></td><td><b><?php echo $job_no; ?></b></td>
            </tr>
        </table>
        
        <table style="margin-top:10px;" width="850" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="100">Requisition No</th>
                <th width="110">Lot No</th>
                <th width="220">Yarn Description</th>
                <th width="110">Brand</th>
                <th width="110">Requisition Qnty</th>
                <th>Remarks</th>
            </thead>
            <?php
				$i=1; $tot_reqsn_qnty=0;
				$sql="select requisition_no, prod_id, yarn_qnty from ppl_yarn_requisition_entry where knit_id='".$dataArray[0][csf('id')]."' and status_active=1 and is_deleted=0";
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					?>
                    <tr>
                        <td width="40" align="center"><?php echo $i; ?></td>
                        <td width="100">&nbsp;&nbsp;<?php echo $selectResult[csf('requisition_no')]; ?></td>
                        <td width="110">&nbsp;&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['lot']; ?></td>
                        <td width="220">&nbsp;&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['count']." ".$product_details_array[$selectResult[csf('prod_id')]]['comp']." ".$product_details_array[$selectResult[csf('prod_id')]]['type']; ?></td>
                        <td width="110">&nbsp;&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['brand']; ?></td>
                        <td width="110" align="right"><?php echo number_format($selectResult[csf('yarn_qnty')],2); ?>&nbsp;&nbsp;</td>
                        <td>&nbsp;</td>	
                    </tr>
					<?php
					$tot_reqsn_qnty+=$selectResult[csf('yarn_qnty')];
					$i++;
				}
			?>
            <tfoot>
                <th colspan="5" align="right"><b>Total</b></th>
                <th align="right"><?php echo number_format($tot_reqsn_qnty,2); ?>&nbsp;&nbsp;</th>
                <th>&nbsp;</th>
            </tfoot>
        </table>
        <table width="850" cellpadding="0" cellspacing="0" border="1" rules="all" style="margin-top:20px;" class="rpt_table">
			<tr>
                <td width="120">&nbsp;&nbsp;<b>Colour:</b></td>
                <td width="150">&nbsp;&nbsp;<?php echo $color_range[$dataArray[0][csf('color_range')]]; ?></td>
                <td width="120">&nbsp;&nbsp;<b>GGSM OR S/L:</b></td>
                <td width="150">&nbsp;&nbsp;<?php echo $dataArray[0][csf('stitch_length')]; ?></td>
                <td width="120">&nbsp;&nbsp;<b>FGSM:</b></td>
                <td>&nbsp;&nbsp;<?php echo $gsm_weight; ?></td>
            </tr>
        </table>
        <table style="margin-top:20px;" width="850" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th width="110">Finish Dia</th>
                <th width="220">Machine Dia & Gauge</th>
                 <th width="110">Program Qnty</th>
                <th>Remarks</th>
            </thead>
            <tr>
                <td width="150">&nbsp;&nbsp;<?php echo $dia; ?></td>
                <td width="280">&nbsp;&nbsp;<?php echo $dataArray[0][csf('machine_dia')]."X".$dataArray[0][csf('machine_gg')]; ?></td>
                <td width="150" align="right">&nbsp;&nbsp;<?php echo number_format( $dataArray[0][csf('program_qnty')],2); ?>&nbsp;&nbsp;</td>
                <td><?php echo $dataArray[0][csf('remarks')]; ?></td>	
            </tr>
            <tr height="70" valign="middle">
            	<td colspan="4"><b>Advice:</b></td>
            </tr>
        </table>
        <table width="850"> 
        	<tr>
				<td width="100%" height="90" colspan="4"></td>
			</tr> 
            <tr>
				<td width="25%" align="center"><strong style="text-decoration:overline">Checked By</strong></td>
                <td width="25%" align="center"><strong style="text-decoration:overline">Store Incharge</strong></td>
                <td width="25%" align="center"><strong style="text-decoration:overline">Knitting Manager</strong></td>
                <td width="25%" align="center"><strong style="text-decoration:overline">Authorised By</strong></td>
			</tr> 
        </table>
    </div>
<?php 
exit();   
}

if($action=="yarn_req_qnty_popup")
{
	echo load_html_head_contents("Program Qnty Info", "../../", 1, 1,'','','');
	extract($_REQUEST); 
	?>
     
	<script>
		var permission='<?php echo $permission; ?>';
		
		function calculate(field_id)
		{
			var txt_no_of_cone=$('#txt_no_of_cone').val()*1;
			var txt_weight_per_cone=$('#txt_weight_per_cone').val()*1;
			var txt_yarn_qnty=$('#txt_yarn_qnty').val()*1;
			
			if(field_id=="txt_yarn_qnty")
			{
				if(txt_no_of_cone>0)
				{
					var weightPerCone=txt_yarn_qnty/txt_no_of_cone;
					$('#txt_weight_per_cone').val(weightPerCone.toFixed(2));
				}
				else 
				{
					$('#txt_weight_per_cone').val('');
				}
			}
			else
			{
				if(txt_weight_per_cone=="" && txt_yarn_qnty!="")
				{
					if(txt_no_of_cone>0)
					{
						var weightPerCone=txt_yarn_qnty/txt_no_of_cone;
						$('#txt_weight_per_cone').val(weightPerCone.toFixed(2));
					}
					else 
					{
						$('#txt_weight_per_cone').val('');
					}
				}
				else
				{
					var yarnQnty=txt_no_of_cone*txt_weight_per_cone;
					$('#txt_yarn_qnty').val(yarnQnty);
				}
			}
		}
		
		function openpage_lot()
		{
			var page_link='yarn_requisition_entry_controller.php?action=lot_info_popup&companyID='+<?php echo $companyID; ?>+'&knit_dtlsId='+<?php echo $knit_dtlsId; ?>+'&comps='+'<?php echo $comps; ?>'+'&job_no='+'<?php echo $job_no; ?>';
			var title='Lot Info';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=730px,height=350px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var prod_id=this.contentDoc.getElementById("hidden_prod_id").value;
				var data=this.contentDoc.getElementById("hidden_data").value.split("**");
				
				$('#prod_id').val(prod_id);
				$('#txt_lot').val(data[0]);	
				$('#cbo_yarn_count').val(data[1]);	
				$('#cbo_yarn_type').val(data[2]);
				$('#txt_color').val(data[3]);
				$('#txt_composition').val(data[4]);
				
				/*$('#cbocomposition1').val(data[4]);
				$('#txt_percentage1').val(data[5]);	
				$('#cbocomposition2').val(data[6]);
				
				var perc_2nd='';
				if(data[7]>0) perc_2nd=data[7]; 
				
				$('#txt_percentage2').val(perc_2nd);*/

			}
		}
		
		function fnc_yarn_req_entry(operation)
		{
				
			if(form_validation('txt_lot*txt_yarn_qnty','Lot*Yarn Qnty')==false )
			{
				return;
			}
			
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('prod_id*txt_no_of_cone*txt_reqs_date*txt_yarn_qnty*updateId*update_dtls_id*txt_requisition_no',"../../");
			
			freeze_window(operation);
			
			http.open("POST","yarn_requisition_entry_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange=fnc_yarn_req_entry_Reply_info;
		}
	
		function fnc_yarn_req_entry_Reply_info()
		{
			if(http.readyState == 4) 
			{
				 //alert(http.responseText);return;
				var reponse=trim(http.responseText).split('**');	
				
				if(reponse[0]==11)	
				{
					alert("Duplicate Item Not Allowed");
				}
				else if(reponse[0]==17)	
				{
					alert("Yarn Requisition Qnty Exceeds Program Qnty");
				}
				else
				{
					show_msg(reponse[0]);
					
					if((reponse[0]==0 || reponse[0]==1 || reponse[0]==2))
					{
						reset_form('yarnReqQnty_1','','','','','updateId');
						$('#txt_requisition_no').val(reponse[3]);
						show_list_view(reponse[1], 'requisition_info_details', 'list_view', 'yarn_requisition_entry_controller', '' ) ;
					}
				}
				
				set_button_status(reponse[2], permission, 'fnc_yarn_req_entry',1);
				release_freezing();	
			}
		}
		
		function fnc_close()
		{
			 //$yarn_req_qnty=return_field_value( "sum(yarn_qnty) as yarn_qnty","ppl_yarn_requisition_entry", "knit_id=$knit_dtlsId","yarn_qnty");
			//$('#hidden_yarn_req_qnty').val( //echo $yarn_req_qnty; );
			parent.emailwindow.hide();
		}
		
    </script>

</head>

<body>
<div align="center">
	<div><?php echo load_freeze_divs ("../../",$permission,1); ?></div>
	<form name="yarnReqQnty_1" id="yarnReqQnty_1">
        <fieldset style="width:800px; margin-top:10px">
            <legend>New Entry</legend>
            <table width="800" align="center" border="0">
            	<tr>
                    <td colspan="3" align="right"><strong>Requisition No</strong></td>
                    <td colspan="3" align="left">
                        <input type="text" name="txt_requisition_no" id="txt_requisition_no" class="text_boxes" style="width:130px;" placeholder="Display" disabled/>
                    </td>
                </tr>
                <tr>
                    <td class="must_entry_caption">Lot</td>
                    <td>
                        <input type="text" name="txt_lot" id="txt_lot" class="text_boxes" placeholder="Double Click" style="width:130px;" onDblClick="openpage_lot();" readonly/>
                        <input type="hidden" name="prod_id" id="prod_id" class="text_boxes" readonly/>
                        <input type="hidden" name="hidden_yarn_req_qnty" id="hidden_yarn_req_qnty" class="text_boxes" readonly/>
                    </td>
                    <td>Yarn Count</td>
                    <td>
                        <?php
							echo create_drop_down( "cbo_yarn_count", 142, "select id,yarn_count from lib_yarn_count where is_deleted = 0 AND status_active = 1 ORDER BY yarn_count ASC","id,yarn_count", 1, "Display", 0, "",1 );
						?>
                    </td>
                    <td>Yarn Type</td>
                    <td>
                        <?php
							echo create_drop_down( "cbo_yarn_type", 142, $yarn_type,"", 1, "Display", 0, "",1 );
						?>
                    </td>
                </tr>
                <tr>
                    <td>Composition</td>
                    <td colspan="3">
                    	<input type="text" name="txt_composition" id="txt_composition" class="text_boxes" placeholder="Display" style="width:390px;" disabled/>
                    </td>
                    <td>Color</td>
                    <td>
                        <input type="text" name="txt_color" id="txt_color" class="text_boxes" placeholder="Display" style="width:130px;" disabled/>
                    </td>
                </tr>
                <tr>
                	<td class="must_entry_caption">Yarn Reqs. Qnty</td>
                    <td>
                        <input type="text" name="txt_yarn_qnty" id="txt_yarn_qnty" class="text_boxes_numeric" style="width:130px;"/><!-- onKeyUp="calculate(this.id);"-->
                    </td>
                    <td>No of Cone</td>
                    <td>
                        <input type="text" name="txt_no_of_cone" id="txt_no_of_cone" class="text_boxes_numeric" style="width:130px;"/><!-- onKeyUp="calculate(this.id);"-->
                    </td>
                    <td>Requisition Date</td>
                    <td>
                        <input type="text" name="txt_reqs_date" id="txt_reqs_date" class="datepicker" style="width:130px;"/><!-- onKeyUp="calculate(this.id);"-->
                    </td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" align="center" class="button_container">
						<?php 
							echo load_submit_buttons($permission, "fnc_yarn_req_entry", 0,0,"reset_form('yarnReqQnty_1','','','','','updateId');",1);
                        ?>
                        <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                        <input type="hidden" name="updateId" id="updateId" class="text_boxes" value="<?php echo str_replace("'",'',$knit_dtlsId); ?>">
                        <input type="hidden" name="update_dtls_id" id="update_dtls_id" class="text_boxes">
                    </td>	  
                </tr>
             </table>
		</fieldset>
        <div id="list_view" style="margin-top:10px">
        	<?php
			if(str_replace("'",'',$knit_dtlsId)!="")
			{
			?>
				<script>
					show_list_view('<?php echo str_replace("'",'',$knit_dtlsId); ?>', 'requisition_info_details', 'list_view', 'yarn_requisition_entry_controller', '' ) ;
                </script>
            <?php
			}
			?>
        </div>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="lot_info_popup")
{
	echo load_html_head_contents("Lot Info", "../../", 1, 1,'','','');
	extract($_REQUEST);  
	?>
    
    <script>
	
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
		
		function js_set_value(id,data)
		{
			$('#hidden_prod_id').val(id);
			$('#hidden_data').val(data);
			parent.emailwindow.hide();
		}
		
    </script>

</head>

<body>
<div align="center" style="width:720px;">
	<form name="searchfrm" id="searchfrm">
		<fieldset style="width:710px;">
         	<input type="hidden" name="hidden_prod_id" id="hidden_prod_id" class="text_boxes" value="">  
            <input type="hidden" name="hidden_data" id="hidden_data" class="text_boxes" value="">
            <div><b><?php echo $comps; ?></b></div><div style="float:left"><b><u>Allocated Grey Yarn</u></b></div>
            <table width="100%" border="1" rules="all" class="rpt_table">
                <thead>
                    <th width="40">Sl No</th>
                    <th width="120">Supplier</th>
                    <th width="60">Count</th>
                    <th width="130">Composition</th>
                    <th width="80">Type</th>
                    <th width="80">Color</th>
                    <th width="80">Lot No</th>
                    <th>Allocated Bl Qnty</th>
                </thead>
            </table>
            <div style="width:100%; overflow-y:scroll; max-height:140px;" id="scroll_body" align="left">
                <table class="rpt_table" rules="all" border="1" width="692" id="tbl_list_search">
                <?php
					$allocated_qnty_array=array(); $all_prod_id=''; $booking_no='';
					$sql_allo="select b.booking_no, c.item_id as prod_id, sum(c.qnty) as allocated_qnty from ppl_planning_info_entry_dtls a, ppl_planning_info_entry_mst b, inv_material_allocation_dtls c, inv_material_allocation_mst d where a.id=$knit_dtlsId and b.id=a.mst_id and b.booking_no=c.booking_no and c.mst_id=d.id and c.item_category=1 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.item_id, b.booking_no";
					$data_array=sql_select($sql_allo);
					foreach($data_array as $row_allo)
					{
						if($all_prod_id=='') $all_prod_id=$row_allo[csf('prod_id')]; else $all_prod_id.=",".$row_allo[csf('prod_id')];
						$allocated_qnty_array[$row_allo[csf('prod_id')]]=$row_allo[csf('allocated_qnty')];
						$booking_no=$row_allo[csf('booking_no')];
					}
					
					$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
					if($db_type==0)
					{
						$all_knit_id=return_field_value("group_concat(distinct(b.id)) as knit_id","ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b","a.id=b.mst_id and a.booking_no='$booking_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0","knit_id");
					}
					else
					{
						$all_knit_id=return_field_value("LISTAGG(b.id, ',') WITHIN GROUP (ORDER BY b.id) as knit_id","ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b","a.id=b.mst_id and a.booking_no='$booking_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0","knit_id");	
						$all_knit_id=implode(",",array_unique(explode(",",$all_knit_id)));
					}
					
					if($all_prod_id!="")
					{
						$sql="select id, supplier_id, lot, current_stock, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color from product_details_master where company_id=$companyID and current_stock>0 and id in($all_prod_id) and item_category_id=1 and status_active=1 and is_deleted=0";
						$result = sql_select($sql);
						
						$i=1;
						foreach($result as $row)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
							
							$compos='';
							if($row[csf('yarn_comp_percent2nd')]!=0)
							{
								$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
							}
							else
							{
								$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
							}

							$requi_qnty=return_field_value("sum(yarn_qnty) as requi_qnty","ppl_yarn_requisition_entry","prod_id=".$row[csf('id')]." and knit_id in($all_knit_id) and status_active=1 and is_deleted=0","requi_qnty");
							
							$bal_alloc_qnty=$allocated_qnty_array[$row[csf('id')]]-$requi_qnty;
							$data=$row[csf('lot')]."**".$row[csf('yarn_count_id')]."**".$row[csf('yarn_type')]."**".$color_library[$row[csf('color')]]."**".$compos;
							
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>" style="cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>,'<?php echo $data; ?>');"> 
								<td width="40" align="center"><?php echo $i; ?></td>	
								<td width="120"><p><?php echo $supllier_arr[$row[csf('supplier_id')]]; ?></p></td>               
								<td width="60"><p><?php echo $count_arr[$row[csf('yarn_count_id')]]; ?></p></td>
								<td width="130"><p><?php echo $compos; ?></p></td>  
								<td width="80"><p><?php echo $yarn_type[$row[csf('yarn_type')]]; ?></p></td>
								<td width="80"><p><?php echo $color_library[$row[csf('color')]]; ?></p></td>
								<td width="80"><p><?php echo $row[csf('lot')]; ?></p></td>                  
								<td align="right"><?php echo number_format($bal_alloc_qnty,2); ?></td>
							</tr>
						<?php
						$i++;
						}
					}
					else echo "<tr><td colspan='8' align='center'>No Item Found</td></tr>";
				?>
                </table>
            </div>
            <div style="float:left"><b><u>Dyed Yarn</u></b></div>
            <table width="100%" border="1" rules="all" class="rpt_table">
                <thead>
                    <th width="40">Sl No</th>
                    <th width="120">Supplier</th>
                    <th width="60">Count</th>
                    <th width="130">Composition</th>
                    <th width="80">Type</th>
                    <th width="80">Color</th>
                    <th width="80">Lot No</th>
                    <th>Stock Qnty</th>
                </thead>
                <?php
					$i=1; $bal_alloc_qnty=0;
					$query="select c.id, c.supplier_id, c.lot, c.current_stock, c.yarn_comp_type1st, c.yarn_comp_percent1st, c.yarn_comp_type2nd, c.yarn_comp_percent2nd, c.yarn_count_id, c.yarn_type, c.color from inv_transaction a, inv_receive_master b, product_details_master c where a.mst_id=b.id and a.prod_id=c.id and a.company_id=$companyID and a.job_no='$job_no' and c.current_stock>0 and b.entry_form=1 and b.receive_basis=2 and b.receive_purpose=2 and a.transaction_type=1 and a.item_category=1 and b.status_active=1 and b.is_deleted=0 group by c.id, c.supplier_id, c.lot, c.current_stock, c.yarn_comp_type1st, c.yarn_comp_percent1st, c.yarn_comp_type2nd, c.yarn_comp_percent2nd, c.yarn_count_id, c.yarn_type, c.color";
					$dyedYarnData= sql_select($query);
					foreach($dyedYarnData as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
						
						$compos='';
						if($row[csf('yarn_comp_percent2nd')]!=0)
						{
							$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
						}
						else
						{
							$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
						}

						$data=$row[csf('lot')]."**".$row[csf('yarn_count_id')]."**".$row[csf('yarn_type')]."**".$color_library[$row[csf('color')]]."**".$compos;
						
						?>
						<tr bgcolor="<?php echo $bgcolor;?>" id="searchf<?php echo $i;?>" style="cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>,'<?php echo $data; ?>');"> 							<td width="40" align="center"><?php echo $i; ?></td>	
							<td width="120"><p><?php echo $supllier_arr[$row[csf('supplier_id')]]; ?></p></td>               
							<td width="60"><p><?php echo $count_arr[$row[csf('yarn_count_id')]]; ?></p></td>
							<td width="130"><p><?php echo $compos; ?></p></td>  
							<td width="80"><p><?php echo $yarn_type[$row[csf('yarn_type')]]; ?></p></td>
							<td width="80"><p><?php echo $color_library[$row[csf('color')]]; ?></p></td>
							<td width="80"><p><?php echo $row[csf('lot')]; ?></p></td>                  
							<td align="right"><?php echo number_format($row[csf('current_stock')],2); ?></td>
						</tr>
					<?php
					$i++;
					}
				?>
            </table>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit();
}


if($action=="requisition_info_details")
{
?>
	<table width="790" border="1" rules="all" class="rpt_table">
        <thead>
        	<th width="80">Lot No</th>
            <th width="70">Count</th>
            <th width="80">Type</th>
            <th width="150">Composition</th>
            <th width="90">Color</th>
            <th width="90">No of Cone</th> 
            <th width="90">Requisition Date</th>
            <th>Yarn Reqs. Qnty</th>
        </thead>
    </table>
    <div style="width:790px; overflow-y:scroll; max-height:300px;" id="scroll_body" align="left">
        <table class="rpt_table" rules="all" border="1" width="773" id="tbl_list_search">
        <?php
			$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
            
            $query=sql_select("select id, knit_id, requisition_no, prod_id, no_of_cone, requisition_date, yarn_qnty from ppl_yarn_requisition_entry where knit_id=$data and status_active = '1' and is_deleted = '0'");
            $i=1; $tot_yarn_qnty=0;
            foreach($query as $selectResult)
            {
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";
                
				$dataArray=sql_select("select lot, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color from product_details_master where id=".$selectResult[csf('prod_id')]."");
				
                $compos='';
                if($dataArray[0][csf('yarn_comp_percent2nd')]!=0)
                {
                    $compos=$composition[$dataArray[0][csf('yarn_comp_type1st')]]." ".$dataArray[0][csf('yarn_comp_percent1st')]."%"." ".$composition[$dataArray[0][csf('yarn_comp_type2nd')]]." ".$dataArray[0][csf('yarn_comp_percent2nd')]."%";
                }
                else
                {
                    $compos=$composition[$dataArray[0][csf('yarn_comp_type1st')]]." ".$dataArray[0][csf('yarn_comp_percent1st')]."%"." ".$composition[$dataArray[0][csf('yarn_comp_type2nd')]];
                }
                
				$tot_yarn_qnty+=$selectResult[csf('yarn_qnty')];
				
                ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>" style="cursor:pointer" onClick="get_php_form_data(<?php echo $selectResult[csf('id')]; ?>, 'populate_requisition_data', 'yarn_requisition_entry_controller' );"> 
                    <td width="80"><p><?php echo $dataArray[0][csf('lot')]; ?></p></td>              
                    <td width="70"><p><?php echo $count_arr[$dataArray[0][csf('yarn_count_id')]]; ?></p></td>
                    <td width="80"><p><?php echo $yarn_type[$dataArray[0][csf('yarn_type')]]; ?></p></td>
                    <td width="150"><p><?php echo $compos; ?></p></td>  
                    <td width="90"><p><?php echo $color_library[$dataArray[0][csf('color')]]; ?></p></td>
                    <td align="right" width="90"><?php echo number_format($selectResult[csf('no_of_cone')],0); ?></td>
                    <td align="center" width="90"><?php echo change_date_format($selectResult[csf('requisition_date')]); ?></td>
                    <td align="right"><?php echo number_format($selectResult[csf('yarn_qnty')],2); ?></td>
                </tr>
            <?php
            $i++;
            }
        ?>
        	<tfoot>
            	<th colspan="7">Total</th>
                <th><?php echo number_format($tot_yarn_qnty,2); ?></th>
            </tfoot>
        </table>
    </div>
<?php	
	
	exit();
}

if($action=="populate_requisition_data")
{
	$sql="select id, knit_id, requisition_no, prod_id, no_of_cone, requisition_date, yarn_qnty from ppl_yarn_requisition_entry where id=$data";
	$data_array=sql_select($sql);
	foreach ($data_array as $row)
	{ 
		$dataArray=sql_select("select lot, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color from product_details_master where id=".$row[csf('prod_id')]);
		
		$compos='';
		if($dataArray[0][csf('yarn_comp_percent2nd')]!=0)
		{
			$compos=$composition[$dataArray[0][csf('yarn_comp_type1st')]]." ".$dataArray[0][csf('yarn_comp_percent1st')]."%"." ".$composition[$dataArray[0][csf('yarn_comp_type2nd')]]." ".$dataArray[0][csf('yarn_comp_percent2nd')]."%";
		}
		else
		{
			$compos=$composition[$dataArray[0][csf('yarn_comp_type1st')]]." ".$dataArray[0][csf('yarn_comp_percent1st')]."%"." ".$composition[$dataArray[0][csf('yarn_comp_type2nd')]];
		}
		
		echo "document.getElementById('txt_requisition_no').value 			= '".$row[csf("requisition_no")]."';\n";
		echo "document.getElementById('txt_lot').value 						= '".$dataArray[0][csf("lot")]."';\n";
		echo "document.getElementById('cbo_yarn_count').value 				= '".$dataArray[0][csf("yarn_count_id")]."';\n";
		echo "document.getElementById('cbo_yarn_type').value 				= '".$dataArray[0][csf("yarn_type")]."';\n";
		
		echo "document.getElementById('txt_composition').value 				= '".$compos."';\n";
		
		echo "document.getElementById('txt_color').value 					= '".$color_library[$dataArray[0][csf("color")]]."';\n";
		echo "document.getElementById('txt_no_of_cone').value 				= '".$row[csf("no_of_cone")]."';\n";
		echo "document.getElementById('txt_reqs_date').value 				= '".change_date_format($row[csf("requisition_date")])."';\n";
		echo "document.getElementById('txt_yarn_qnty').value 				= '".$row[csf("yarn_qnty")]."';\n";
		echo "document.getElementById('prod_id').value 						= '".$row[csf("prod_id")]."';\n";
		echo "document.getElementById('update_dtls_id').value 				= '".$row[csf("id")]."';\n";
		
		/*echo "document.getElementById('cbocomposition1').value 			= '".$dataArray[0][csf("yarn_comp_type1st")]."';\n";
		echo "document.getElementById('txt_percentage1').value 				= '".$dataArray[0][csf("yarn_comp_percent1st")]."';\n";
		echo "document.getElementById('cbocomposition2').value 				= '".$dataArray[0][csf("yarn_comp_type2nd")]."';\n";
		echo "document.getElementById('txt_percentage2').value 				= '".$dataArray[0][csf("yarn_comp_percent2nd")]."';\n";*/
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_yarn_req_entry',1);\n";  
		exit();
	}
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if($operation==0)  // Insert Here
	{
		
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
	 	
		if(is_duplicate_field( "prod_id", "ppl_yarn_requisition_entry", "knit_id=$updateId and prod_id=$prod_id and status_active=1 and is_deleted=0" )==1)
		{
			echo "11**".str_replace("'","",$updateId)."**0";
			exit();			
		}
		
		$program_qnty=return_field_value("program_qnty","ppl_planning_info_entry_dtls", "id=$updateId");
		$req_qnty=return_field_value("sum(yarn_qnty)","ppl_yarn_requisition_entry", "knit_id=$updateId and status_active=1 and is_deleted=0")+str_replace("'","",$txt_yarn_qnty);
		
		if($req_qnty>$program_qnty)
		{
			echo "17**".str_replace("'","",$updateId)."**0";
			exit();
		}
		
		$requisition_no=return_field_value( "requisition_no","ppl_yarn_requisition_entry", "knit_id=$updateId");
		if($requisition_no=="")
		{
			$requisition_no=return_next_id( "requisition_no","ppl_yarn_requisition_entry", 1 ) ;
		}
		else $requisition_no=$requisition_no;
		
		$id=return_next_id( "id","ppl_yarn_requisition_entry", 1 ) ;
		
		$field_array="id, knit_id, requisition_no, prod_id, no_of_cone, requisition_date, yarn_qnty, inserted_by, insert_date"; 
		
		$data_array="(".$id.",".$updateId.",".$requisition_no.",".$prod_id.",".$txt_no_of_cone.",".$txt_reqs_date.",".$txt_yarn_qnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
		
		//echo "insert into ppl_yarn_requisition_entry (".$field_array.") Values ".$data_array."";die;
		$rID=sql_insert("ppl_yarn_requisition_entry",$field_array,$data_array,1);

		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'","",$updateId)."**0**".$requisition_no;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**".str_replace("'","",$updateId)."**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID)
			{
				oci_commit($con);  
				echo "0**".str_replace("'","",$updateId)."**0**".$requisition_no;
			}
			else
			{
				oci_rollback($con); 
				echo "5**".str_replace("'","",$updateId)."**0";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if(is_duplicate_field( "prod_id", "ppl_yarn_requisition_entry", "knit_id=$updateId and prod_id=$prod_id and id<>$update_dtls_id and status_active=1 and is_deleted=0" )==1)
		{
			echo "11**".str_replace("'","",$updateId)."**1";
			exit();			
		}
		
		$program_qnty=return_field_value("program_qnty","ppl_planning_info_entry_dtls", "id=$updateId");
		$req_qnty=return_field_value("sum(yarn_qnty)","ppl_yarn_requisition_entry", "knit_id=$updateId and id<>$update_dtls_id and status_active=1 and is_deleted=0")+str_replace("'","",$txt_yarn_qnty);
		
		if($req_qnty>$program_qnty)
		{
			echo "17**".str_replace("'","",$updateId)."**1";
			exit();
		}
		
		$field_array_update="prod_id*no_of_cone*requisition_date*yarn_qnty*updated_by*update_date";
		
		$data_array_update=$prod_id."*".$txt_no_of_cone."*".$txt_reqs_date."*".$txt_yarn_qnty."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$rID=sql_update("ppl_yarn_requisition_entry",$field_array_update,$data_array_update,"id",$update_dtls_id,1);
		
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$updateId)."**0**".str_replace("'","",$txt_requisition_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID)
			{
				oci_commit($con);    
				echo "1**".str_replace("'","",$updateId)."**0**".str_replace("'","",$txt_requisition_no);
			}
			else
			{
				oci_rollback($con);
				echo "6**0**1";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array_update="status_active*is_deleted*updated_by*update_date";
		
		$data_array_update="0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$rID=sql_update("ppl_yarn_requisition_entry",$field_array_update,$data_array_update,"id",$update_dtls_id,1);

		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$updateId)."**0**".str_replace("'","",$txt_requisition_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "7**0**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID)
			{
				oci_commit($con);  
				echo "2**".str_replace("'","",$updateId)."**0**".str_replace("'","",$txt_requisition_no);
			}
			else
			{
				oci_rollback($con); 
				echo "7**0**1";
			}
		}
		disconnect($con);
		die;
	}
}
?>