﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------- Start-------------------------------------//

if($db_type==0) $select_field="group"; 
else if($db_type==2) $select_field="wm";
else $select_field="";//defined Later		
		
if ($action=="get_btb_limit")
{
	$nameArray=sql_select( "SELECT max_btb_limit FROM variable_settings_commercial where company_name like '$data' and variable_list=6 and is_deleted = 0 AND status_active = 1" );
 	if($nameArray)
	{
		foreach ($nameArray as $row)
		{
			echo "document.getElementById('txt_max_btb_limit').value = ".$row[csf("max_btb_limit")].";\n";  	 
		}
	}
	else
	{
		echo "document.getElementById('txt_max_btb_limit').value ='';\n";  
	}
}

if ($action=="load_drop_down_buyer_search")
{
	echo create_drop_down( "cbo_buyer_name", 162, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  

	exit();
}

if ($action=="load_drop_down_applicant_name")
{
	$sql = "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id and b.tag_company='$data' and a.id in (select  buyer_id from  lib_buyer_party_type where party_type in (22,23)) order by buyer_name";  
 	echo create_drop_down( "txt_applicant_name", 162, $sql,"id,buyer_name", 1, "---- Select ----", 0, "" );
	exit();
}

if ($action=="load_drop_down_notifying_party")
{
	$sql = "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id and b.tag_company='$data' and a.id in (select  buyer_id from  lib_buyer_party_type where party_type in (4,6)) order by buyer_name";  
 	echo create_drop_down( "cbo_notifying_party", 162, $sql, "id,buyer_name", 0, "", '', '');
	exit();
}

if ($action=="load_drop_down_consignee")
{
	$sql = "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id and b.tag_company='$data' and a.id in (select  buyer_id from  lib_buyer_party_type where party_type in (5,6)) order by buyer_name";  
 	echo create_drop_down( "cbo_consignee", 162, $sql,"id,buyer_name", 0, "", '', '' );
	exit();
} 

if ($action=="eval_multi_select")
{
	echo "set_multiselect('cbo_notifying_party*cbo_consignee','0*0','0','','0*0');\n";
	exit();
}
 
if($action=='populate_data_from_sales_contract')
{
	$btblc_library=return_library_array( "select id, lc_number from com_btb_lc_master_details", "id", "lc_number"  );
	
	$data_array=sql_select("select id,contact_system_id,contract_no,contract_date,beneficiary_name,buyer_name,applicant_name,notifying_party,consignee, convertible_to_lc,lien_bank,lien_date,contract_value,currency_name,tolerance,last_shipment_date, expiry_date, shipping_mode,pay_term,inco_term,inco_term_place,contract_source,port_of_entry,port_of_loading,port_of_discharge,internal_file_no,shipping_line,doc_presentation_days,max_btb_limit,foreign_comn,local_comn,remarks,tenor,discount_clauses,converted_from,converted_btb_lc_list,claim_adjustment,bank_file_no,sc_year,bl_clause,export_item_category from com_sales_contract where id='$data'");
	foreach ($data_array as $row)
	{
		$btblc_no=""; $btb_attach_id='';
		if($row[csf("converted_btb_lc_list")]!="")
		{
			$btblc_id=explode(",",$row[csf("converted_btb_lc_list")]);
			foreach($btblc_id as $val)
			{
				if($btblc_no=="") $btblc_no=$btblc_library[$val]; else $btblc_no.="*".$btblc_library[$val];
				
				$attach_id=return_field_value("id","com_btb_export_lc_attachment","import_mst_id=$val");
				if($btb_attach_id=="") $btb_attach_id=$attach_id; else $btb_attach_id.=",".$attach_id;
			}
		}
		
		$sales_cotract_no=return_field_value("contract_no","com_sales_contract","id='".$row[csf('converted_from')]."'");
		
		if($db_type==0)
		{
			$attached_po_id=return_field_value("group_concat(wo_po_break_down_id)","com_sales_contract_order_info","com_sales_contract_id=$data and status_active=1 and is_deleted=0");
		}
		else
		{
			$attached_po_id=return_field_value("LISTAGG(wo_po_break_down_id, ',') WITHIN GROUP (ORDER BY wo_po_break_down_id) as po_id","com_sales_contract_order_info","com_sales_contract_id=$data and status_active=1 and is_deleted=0","po_id");	
		}
		
		$sc_amnd=return_field_value("count(id)","com_sales_contract_amendment","contract_id=$data and is_original=0 and status_active=1 and is_deleted=0");
		
		echo "document.getElementById('txt_system_id').value 			= '".$row[csf("id")]."';\n";
		echo "document.getElementById('contact_system_id').value 		= '".$row[csf("contact_system_id")]."';\n";
		echo "document.getElementById('cbo_beneficiary_name').value 	= '".$row[csf("beneficiary_name")]."';\n";
		echo "$('#cbo_beneficiary_name').attr('disabled','true')".";\n";
		echo "document.getElementById('txt_internal_file_no').value		= '".$row[csf("internal_file_no")]."';\n";
		echo "document.getElementById('txt_bank_file_no').value 		= '".$row[csf("bank_file_no")]."';\n";
		echo "document.getElementById('txt_year').value 				= '".$row[csf("sc_year")]."';\n";
		echo "document.getElementById('txt_contract_no').value 			= '".$row[csf("contract_no")]."';\n";
		echo "document.getElementById('txt_contract_value').value 		= '".$row[csf("contract_value")]."';\n";
		echo "document.getElementById('cbo_currency_name').value 		= '".$row[csf("currency_name")]."';\n";
		echo "document.getElementById('txt_contract_date').value 		= '".change_date_format($row[csf("contract_date")])."';\n";
		echo "document.getElementById('cbo_convertible_to_lc').value 	= '".$row[csf("convertible_to_lc")]."';\n";
		
		echo "load_drop_down( 'requires/sales_contract_controller', document.getElementById('cbo_beneficiary_name').value, 'load_drop_down_buyer_search', 'buyer_td_id' );\n";
		echo "load_drop_down( 'requires/sales_contract_controller', document.getElementById('cbo_beneficiary_name').value, 'load_drop_down_applicant_name', 'applicant_name_td');\n";
		echo "load_drop_down( 'requires/sales_contract_controller', document.getElementById('cbo_beneficiary_name').value, 'load_drop_down_notifying_party', 'notifying_party_td' );\n";
		echo "load_drop_down( 'requires/sales_contract_controller', document.getElementById('cbo_beneficiary_name').value, 'load_drop_down_consignee', 'consignee_td' );\n";
 		echo "get_php_form_data( document.getElementById('cbo_beneficiary_name').value, 'eval_multi_select', 'requires/sales_contract_controller' );\n";
		
		echo "document.getElementById('cbo_buyer_name').value			= '".$row[csf("buyer_name")]."';\n";
		echo "document.getElementById('txt_applicant_name').value		= '".$row[csf("applicant_name")]."';\n";
		echo "document.getElementById('cbo_notifying_party').value		= '".$row[csf("notifying_party")]."';\n";
		echo "document.getElementById('cbo_consignee').value			= '".$row[csf("consignee")]."';\n";
		echo "document.getElementById('cbo_lien_bank').value 			= '".$row[csf("lien_bank")]."';\n";
		echo "document.getElementById('txt_lien_date').value 			= '".change_date_format($row[csf("lien_date")])."';\n";
		echo "document.getElementById('txt_last_shipment_date').value 	= '".change_date_format($row[csf("last_shipment_date")])."';\n";
		echo "document.getElementById('txt_expiry_date').value 			= '".change_date_format($row[csf("expiry_date")])."';\n";
		echo "document.getElementById('txt_tolerance').value 			= '".$row[csf("tolerance")]."';\n";
		echo "document.getElementById('cbo_shipping_mode').value 		= '".$row[csf("shipping_mode")]."';\n";
		echo "document.getElementById('cbo_pay_term').value 			= '".$row[csf("pay_term")]."';\n";
		echo "document.getElementById('txt_tenor').value 				= '".$row[csf("tenor")]."';\n";
		echo "document.getElementById('cbo_inco_term').value 			= '".$row[csf("inco_term")]."';\n";
		echo "document.getElementById('txt_inco_term_place').value 		= '".$row[csf("inco_term_place")]."';\n";
		echo "document.getElementById('cbo_contract_source').value 		= '".$row[csf("contract_source")]."';\n";
		echo "document.getElementById('txt_port_of_entry').value 		= '".$row[csf("port_of_entry")]."';\n";
		echo "document.getElementById('txt_port_of_loading').value 		= '".$row[csf("port_of_loading")]."';\n";
		echo "document.getElementById('txt_port_of_discharge').value 	= '".$row[csf("port_of_discharg")]."';\n";
		echo "document.getElementById('txt_shipping_line').value 		= '".$row[csf("shipping_line")]."';\n";
		echo "document.getElementById('txt_doc_presentation_days').value = '".$row[csf("doc_presentation_days")]."';\n";
		echo "document.getElementById('txt_max_btb_limit').value 		= '".$row[csf("max_btb_limit")]."';\n";
		echo "document.getElementById('txt_foreign_comn').value 		= '".$row[csf("foreign_comn")]."';\n";
		echo "document.getElementById('txt_local_comn').value 			= '".$row[csf("local_comn")]."';\n";
		echo "document.getElementById('txt_discount_clauses').value 	= '".$row[csf("discount_clauses")]."';\n";
		echo "document.getElementById('txt_claim_adjustment').value 	= '".$row[csf("claim_adjustment")]."';\n";
		echo "document.getElementById('txt_converted_from').value 		= '".$sales_cotract_no."';\n";
		echo "document.getElementById('txt_converted_from_id').value 	= '".$row[csf("converted_from")]."';\n";
		echo "document.getElementById('txt_converted_btb_lc').value 	= '".$btblc_no."';\n";
		echo "document.getElementById('txt_converted_btb_id').value 	= '".$row[csf("converted_btb_lc_list")]."';\n";
		echo "document.getElementById('txt_attach_row_id').value 		= '".$btb_attach_id."';\n";
		echo "document.getElementById('txt_remarks').value 				= '".$row[csf("remarks")]."';\n";
		echo "document.getElementById('txt_bl_clause').value 			= '".$row[csf("bl_clause")]."';\n";
		echo "document.getElementById('cbo_export_item_category').value = '".$row[csf("export_item_category")]."';\n";
		echo "document.getElementById('hidden_selectedID').value 		= '".$attached_po_id."';\n";
		echo "convertible_to_lc_display();\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_sales_contract',1);\n";  
		echo "set_multiselect('cbo_notifying_party*cbo_consignee','0*0','1','".$row[csf('notifying_party')]."*".$row[csf('consignee')]."','0*0');\n";
		
		if($sc_amnd>0)
		{
			echo "disable_enable_fields('txt_contract_value*txt_last_shipment_date*txt_expiry_date*cbo_shipping_mode*cbo_inco_term*txt_inco_term_place*txt_port_of_entry*txt_port_of_loading*txt_port_of_discharge*cbo_pay_term*txt_tenor*txt_claim_adjustment*txt_discount_clauses*txt_bl_clause*txt_remarks',1);\n";
		}
		else
		{
			echo "disable_enable_fields('txt_contract_value*txt_last_shipment_date*txt_expiry_date*cbo_shipping_mode*cbo_inco_term*txt_inco_term_place*txt_port_of_entry*txt_port_of_loading*txt_port_of_discharge*cbo_pay_term*txt_tenor*txt_claim_adjustment*txt_discount_clauses*txt_bl_clause*txt_remarks',0);\n";
		}
		
		exit();
	}
}

 
if($action=="fake_sc")
{
	echo load_html_head_contents("Sales Contract Form", "../../../", 1, 1,'','1','');
	extract($_REQUEST);
	?>
	<script>
	function js_set_value( contract_no_id )
	{
		contract_no=contract_no_id.split("_");
		
		document.getElementById('hidden_contract_no').value=contract_no[0];
		document.getElementById('hidden_contract_id').value=contract_no[1];
		parent.emailwindow.hide();
	}
	</script>
	</head>
	
	<body>
	<div align="center" style="width:100%" >
	<form name="search_sc_frm"  id="search_sc_frm">
		<fieldset style="width:600px"> 
			<?php
				$sql = "SELECT id, contract_no, contract_value, contract_date FROM com_sales_contract where beneficiary_name='$beneficiary' and convertible_to_lc=3 and is_deleted = 0 AND status_active = 1";
				echo create_list_view("list_view", "Contract No,Contract Value,Contract Date", "150,150,100","500","320",0, $sql , "js_set_value", "contract_no,id", "", 1, "0,0,0", $arr , "contract_no,contract_value,contract_date", "",'','0,2,3') ;
				 
			?>
		</fieldset>
        	<input type="hidden" id="hidden_contract_no" />
            <input type="hidden" id="hidden_contract_id" />
		</form>
	   </div>
	</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}
 
    
if($action=="fake_btb")
 {
	echo load_html_head_contents("Sales Contract Form", "../../../", 1, 1,'','1','');
	extract($_REQUEST);
	
	$btb_id_array=explode(",",$txt_converted_btb_id);
	?>
     
	<script>
	
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}

		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}

		function js_set_value( str ) 
		{

			if (str!="") str=str.split("_");
			
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				selected_attach_id.push( str[3] );
			}
			else
			{
				for( var i = 0; i < selected_id.length; i++ )
				{
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
				selected_attach_id.splice( i, 1 );
			}
			var id =''; var name =''; var attach_id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
				attach_id += selected_attach_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			attach_id = attach_id.substr( 0, attach_id.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
			$('#txt_attach_id').val( attach_id );
		}
		
    </script>
	</head>

	<body>
	<div align="center" style="width:730px;" >
	<form name="search_btb_frm"  id="search_btb_frm">
        <fieldset style="width:730px"> 
            <input type="hidden" name="txt_selected" id="txt_selected" class="text_boxes" readonly />
            <input type="hidden" name="txt_selected_id" id="txt_selected_id" class="text_boxes" readonly />
            <input type="hidden" name="txt_attach_id" id="txt_attach_id" class="text_boxes" readonly />
            <table width="100%" style="margin-top:5px">  
                <tr>
                    <td>
					<?php
						$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
						
						$arr=array (3=>$supplier_arr,4=>$item_category);
						if($txt_converted_btb_id=="")
						{
							$sql = "SELECT a.id, a.lc_number, a.lc_value, a.lc_date, a.supplier_id, a.item_category_id, b.id as attach_id FROM com_btb_lc_master_details a, com_btb_export_lc_attachment b where a.id=b.import_mst_id and b.lc_sc_id='$sales_contract' and b.is_lc_sc=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.lc_number, a.lc_value, a.lc_date, a.supplier_id, a.item_category_id, b.id"; 
						}
						else
						{
							$sql = "SELECT a.id, a.lc_number, a.lc_value, a.lc_date, a.supplier_id, a.item_category_id, b.id as attach_id FROM com_btb_lc_master_details a, com_btb_export_lc_attachment b where a.id=b.import_mst_id and (b.lc_sc_id='$sales_contract' or a.id not in($txt_converted_btb_id)) and b.is_lc_sc=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.lc_number, a.lc_value, a.lc_date, a.supplier_id, a.item_category_id, b.id"; 
						}	
						
						echo create_list_view("tbl_list_search", "BTB LC No,LC Value,LC Date,Supplier,Item Group", "130,130,100,130,130","700","300",0, $sql , "js_set_value", "id,lc_number,attach_id", "", 1, "0,0,0,supplier_id,item_category_id", $arr , "lc_number,lc_value,lc_date,supplier_id,item_category_id", "","",'0,2,3,0,0','',1) ;
					
					?>
                    </td>
                </tr>
                <tr>
                    <td align="center"><input type="hidden" name="close" onClick="parent.emailwindow.hide();"  class="formbutton" value="Close" /></td>
                </tr>
            </table>
        </fieldset>
	</form>
    </div>
    
</body>           
    <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>	
<?php	 
	 exit();
 }


if($action=="sales_contact_search")
{
	 echo load_html_head_contents("Sales Contract Form", "../../../", 1, 1,'','1','');
	?>
	<script>
		function js_set_value(id)
		{
			$('#hidden_sales_contract_id').val(id);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:1030px;">
	<form name="searchscfrm"  id="searchscfrm">
		<fieldset style="width:1028px; margin-left:3px">
            <legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="80%" class="rpt_table">
                <thead>
                    <th>Company</th>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th>Enter</th>
                    <th><input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                    <input type="hidden" name="id_field" id="id_field" value="" /></th>
                </thead>
                <tr class="general">
                    <td>
                        <?php
                            echo create_drop_down( "cbo_company_name", 162, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--- Select ---", 0, "load_drop_down( 'sales_contract_controller', this.value, 'load_drop_down_buyer_search', 'buyer_td_id' );" );
                        ?>                        
                      </td>
                      <td id="buyer_td_id">
                        <?php
                            echo create_drop_down( "cbo_buyer_name", 162, $blank_array,"", 1, "--- Select Buyer ---", $selected, "" );
                        ?>
                     </td>                  
                    <td> 
                        <?php
                            $arr=array(1=>'SC No',2=>'File No');
                            echo create_drop_down( "cbo_search_by", 162, $arr,"", 1, "--- Select ---", 0, "" );
                        ?>
                    </td>						
                    <td id="search_by_td">
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                        <input type="hidden" id="hidden_sales_contract_id" />
                    </td>                       
                     <td>
                        <input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value, 'create_sc_search_list_view', 'search_div', 'sales_contract_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                     </td>
                </tr>
            </table>
            <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
        </fieldset>
    </form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
 
}

if ($action=="order_popup")
{
	echo load_html_head_contents("Sales Contract Form", "../../../", 1, 1,'','1','');
	extract($_REQUEST);
	?>
     
	<script>
	 var selected_id = new Array, selected_name = new Array();	
	 function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#txt_selected_id').val( id );
		}
	
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
	<form name="searchpofrm"  id="searchpofrm">
		<fieldset style="width:880px">
			<table width="650" cellspacing="0" cellpadding="0" class="rpt_table">
                <thead>
                    <th>Company</th>
                    <th>Search By</th>
                    <th>Search</th>
                    <th>File No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:100px;"></th>                    
                </thead>
                <tr class="general">
                    <td align="center">
                        <?php 
							echo create_drop_down( "cbo_company_name", 150, "select comp.id, comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "-- Select Company --", $company_id,"" );
						?>
                    </td>
                    <td align="center">
                        <?php
                        	$arr=array(1=>'PO Number',2=>'Job No',3=>'Style Ref No');
							echo create_drop_down( "cbo_search_by", 150, $arr,"",0, "--- Select ---", '',"" );
						?>
                     </td>
                     <td align="center">
                        <input type="text" name="txt_search_text" id="txt_search_text" class="text_boxes" style="width:150px" />
                        <input type="hidden" id="hidden_type" value="<?php echo $types; ?>" />	
                        <input type="hidden" id="hidden_buyer_id" value="<?php echo $buyer_id; ?>" />	
                        <input type="hidden" id="hidden_po_selectedID" value="<?php echo $selectID; ?>" />
                        <input type="hidden" id="sales_contractID" value="<?php echo $sales_contractID; ?>" />
                        <input type="hidden" name="txt_selected_id" id="txt_selected_id" value="" />				
                    </td>
                    <td align="center">
                        <input type="text" id="txt_file_no" name="txt_file_no" class="text_boxes" style="width:80px" >
                     </td>
                    <td align="center">
                        <input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_text').value+'**'+document.getElementById('hidden_type').value+'**'+document.getElementById('hidden_buyer_id').value+'**'+document.getElementById('hidden_po_selectedID').value+'**'+document.getElementById('sales_contractID').value+'**'+document.getElementById('txt_file_no').value, 'create_po_search_list_view', 'search_div', 'sales_contract_controller', 'setFilterGrid(\'tbl_list_search\',-1)')" style="width:100px;" />
                    </td>
            </tr>
        </table>
        <div style="width:880px; margin-top:5px" id="search_div" align="left"></div>
	</fieldset>
	</form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
  exit();
}
if($action=="create_po_search_list_view")
{
	$data=explode('**',$data);
	if ($data[0]!=0) $company=" and wm.company_name='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[2]!='') 
	{
		if($data[1]==1)
			$search_text=" and wb.po_number like '".trim($data[2])."%'";
		else if($data[1]==2)	 
			$search_text=" and wm.job_no like '".trim($data[2])."%'"; 
		else if($data[1]==3)
			$search_text=" and wm.style_ref_no like '".trim($data[2])."%'";	
	}
	$action_types = $data[3];
	$buyer_id = $data[4];	
	if($data[5]=="") $selected_order_id = ""; else $selected_order_id = "and wb.id not in (".$data[5].")";
	$sales_contractID = $data[6];
	$txt_file_no = $data[7];
	
	//echo $txt_file_no;die;
	
	if($txt_file_no!="") $file_no_cond=" and wb.file_no='$data[7]'"; else $file_no_cond="";
	
	if($action_types=='attached_po_status')
	{
		$lc_details=return_library_array( "select id, export_lc_no from com_export_lc",'id','export_lc_no');
		$sc_details=return_library_array( "select id, contract_no from com_sales_contract",'id','contract_no');
		
		$lc_array=array(); $sc_array=array(); $attach_qnty_array=array();
		$sql_lc_sc="select com_export_lc_id as id, wo_po_break_down_id, sum(attached_qnty) as qnty, 1 as type from com_export_lc_order_info where status_active=1 and is_deleted=0 group by wo_po_break_down_id, com_export_lc_id
		union all
		select com_sales_contract_id as id, wo_po_break_down_id, sum(attached_qnty) as qnty, 2 as type from com_sales_contract_order_info where status_active=1 and is_deleted=0 group by wo_po_break_down_id, com_sales_contract_id
		";
		$lc_sc_Array=sql_select($sql_lc_sc);
		foreach($lc_sc_Array as $row_lc_sc)
		{
			if(array_key_exists($row_lc_sc[csf('wo_po_break_down_id')],$attach_qnty_array))
			{
				 $attach_qnty_array[$row_lc_sc[csf('wo_po_break_down_id')]]+=$row_lc_sc[csf('qnty')];
			}
			else
			{
				$attach_qnty_array[$row_lc_sc[csf('wo_po_break_down_id')]]=$row_lc_sc[csf('qnty')];
			}
			
			if($row_lc_sc[csf('type')]==1)
			{
				if($row_lc_sc[csf('qnty')]>0)
				{
					if(array_key_exists($row_lc_sc[csf('wo_po_break_down_id')],$lc_array))
					{
						 $lc_array[$row_lc_sc[csf('wo_po_break_down_id')]].=",".$row_lc_sc[csf('id')];
					}
					else
					{
						$lc_array[$row_lc_sc[csf('wo_po_break_down_id')]]=$row_lc_sc[csf('id')];
					}
				}
			}
			else
			{
				if($row_lc_sc[csf('qnty')]>0)
				{
					if(array_key_exists($row_lc_sc[csf('wo_po_break_down_id')],$sc_array))
					{
						 $sc_array[$row_lc_sc[csf('wo_po_break_down_id')]].=",".$row_lc_sc[csf('id')];
					}
					else
					{
						$sc_array[$row_lc_sc[csf('wo_po_break_down_id')]]=$row_lc_sc[csf('id')];
					}
				}
			}
		}
		
		$sql = "SELECT wb.id, wb.po_number, wb.po_total_price, wb.po_quantity, wb.pub_shipment_date as shipment_date, wb.job_no_mst, wb.file_no, wm.style_ref_no, wm.gmts_item_id, wb.unit_price FROM wo_po_break_down wb, wo_po_details_master wm WHERE wb.job_no_mst = wm.job_no and wm.buyer_name like '$buyer_id' $company $search_text $file_no_cond and wb.is_deleted = 0 AND wb.status_active = 1 group by wb.id, wb.po_number, wb.po_total_price, wb.po_quantity, wb.pub_shipment_date, wb.job_no_mst, wb.file_no, wm.style_ref_no, wm.gmts_item_id, wb.unit_price";
		//echo $sql."<br>";
		 
		?>
        <div>
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="880" class="rpt_table" >
                <thead>
                    <th width="30">SL</th>
                    <th width="100">PO No</th>
                    <th width="110">Item</th>
                    <th width="100">Style No</th>
                    <th width="80">PO Quantity</th>
                    <th width="90">Price</th>
                    <th width="70">Shipment Date</th>
                    <th width="100">Attached With</th>
					<th width="100">LC/SC</th>
                    <th >File No</th>
                </thead>
            </table>
            <div style="width:880px; overflow-y:scroll; max-height:250px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="862" class="rpt_table" id="tbl_list_search" >
                <?php 
					$i=1;
                    $nameArray=sql_select( $sql );
					//var_dump($nameArray);die;
                    foreach ($nameArray as $selectResult)
                    {
						if(array_key_exists($selectResult[csf('id')],$attach_qnty_array))
						{
							$order_attached_qnty=$attach_qnty_array[$selectResult[csf('id')]];
							
							if($order_attached_qnty>=$selectResult[csf('po_quantity')])
							{
								$all_lc_id=explode(",",$lc_array[$selectResult[csf('id')]]);
								foreach($all_lc_id as $lc_id)
								{
									if($lc_id!=0)
									{
										if ($i%2==0)  
											$bgcolor="#E9F3FF";
										else
											$bgcolor="#FFFFFF";	
									?>	
										<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
											<td width="30"><?php echo $i; ?></td>
											<td width="100"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
											<td width="110">
												<p>
													<?php
														$gmts_item='';
														$gmts_item_id=explode(",",$selectResult[csf('gmts_item_id')]);
														foreach($gmts_item_id as $item_id)
														{
															if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
														}
														echo $gmts_item;
													?>
												</p>
											</td>
											<td width="100"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
											<td width="80" align="right"><?php echo $selectResult[csf('po_quantity')]; ?></td>
											<td width="90" align="right"><?php echo number_format($selectResult[csf('po_total_price')],2); ?></td>
											<td align="center" width="70"><?php echo change_date_format($selectResult[csf('shipment_date')]); ?></td>
											<td width="100"><p><?php echo $lc_details[$lc_id]; ?></p></td>
											<td align="center" width="100"><?php echo 'LC'; ?></td>
                                            <td ><?php echo $selectResult[csf('file_no')]; ?></td>
										</tr>
									<?php       	
									$i++;
									}
								}
								
								$all_sc_id=explode(",",$sc_array[$selectResult[csf('id')]]);

								foreach($all_sc_id as $sc_id)
								{
									if($sc_id!=0)
									{	
										if ($i%2==0)  
											$bgcolor="#E9F3FF";
										else
											$bgcolor="#FFFFFF";
									?>	
										<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
											<td width="30"><?php echo $i; ?></td>
											<td width="100"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
											<td width="110">
												<p>
													<?php
														$gmts_item='';
														$gmts_item_id=explode(",",$selectResult[csf('gmts_item_id')]);
														foreach($gmts_item_id as $item_id)
														{
															if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
														}
														echo $gmts_item;
													?>
												</p>
											</td>
											<td width="100"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
											<td width="80" align="right"><?php echo $selectResult[csf('po_quantity')]; ?></td>
											<td width="90" align="right"><?php echo number_format($selectResult[csf('po_total_price')],2); ?></td>
											<td align="center" width="70"><?php echo change_date_format($selectResult[csf('shipment_date')]); ?></td>
											<td width="100"><p><?php echo $sc_details[$sc_id]; ?></p></td>
											<td align="center" width="100"><?php echo 'SC'; ?></td>
                                            <td ><?php echo $selectResult[csf('file_no')]; ?></td>
										</tr>
									<?php       	
									$i++;
									}
								}
							}
						}
					}
				?>
				</table>
        	</div>
		</div>                   
	<?php
	exit();
	}
	
	if($action_types=='order_select_popup')
	{
		$sql = "SELECT wb.id, wb.po_number, wb.po_total_price, wb.po_quantity, wb.pub_shipment_date as shipment_date, wb.job_no_mst, wb.file_no, wm.style_ref_no, wm.gmts_item_id, wb.unit_price FROM wo_po_break_down wb, wo_po_details_master wm WHERE wb.job_no_mst = wm.job_no and wm.buyer_name like '$buyer_id' $selected_order_id $company $search_text $file_no_cond and wb.is_deleted = 0 AND wb.status_active = 1 group by wb.id, wb.po_number, wb.po_total_price, wb.po_quantity, wb.pub_shipment_date, wb.job_no_mst, wb.file_no, wm.style_ref_no, wm.gmts_item_id, wb.unit_price";
		
		//echo $sql."<br>"; 
	 ?>
        <div>
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="880" class="rpt_table" >
                <thead>
                    <th width="40">SL</th>
                    <th width="130">PO No</th>
                    <th width="130">Item</th>
                    <th width="120">Style No</th>
                    <th width="110">PO Quantity</th>
                    <th width="120">Price</th>
                    <th width="80">Shipment Date</th>
                    <th>File No</th>
                </thead>
            </table>
            <div style="width:880px; overflow-y:scroll; max-height:220px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="862" class="rpt_table" id="tbl_list_search" >
                <?php
					$i=1;
                    $nameArray=sql_select( $sql );
					
                    foreach ($nameArray as $selectResult)
                    {
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
							
						$lc_attached_qnty=return_field_value("sum(attached_qnty)","com_export_lc_order_info","wo_po_break_down_id='".$selectResult[csf('id')]."' and status_active = 1 and is_deleted=0");
						$sc_attached_qnty=return_field_value("sum(attached_qnty)","com_sales_contract_order_info","wo_po_break_down_id='".$selectResult[csf('id')]."' and status_active = 1 and is_deleted=0");
						$order_attached_qnty=$sc_attached_qnty+$lc_attached_qnty;
						//echo $order_attached_qnty.jahid;	
							
            			if($order_attached_qnty < $selectResult[csf('po_quantity')] )
						{
                    	?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
                                <td width="40" align="center"><?php echo "$i"; ?>
                                 <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>	
                                </td>	
                                <td width="130"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                                <td width="130">
                                    <p>
                                        <?php
                                            $gmts_item='';
                                            $gmts_item_id=explode(",",$selectResult[csf('gmts_item_id')]);
                                            foreach($gmts_item_id as $item_id)
                                            {
                                                if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
                                            }
                                            echo $gmts_item;
                                        ?>
                                    </p>
                                </td> 
                                <td width="120"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                                <td width="110" align="right"><?php echo $selectResult[csf('po_quantity')]; ?></td>
                                <td width="120" align="right"><?php echo number_format($selectResult[csf('po_total_price')],2); ?></td>
                                <td align="center" width="80"><?php echo change_date_format($selectResult[csf('shipment_date')]); ?></td>
                                <td ><?php echo $selectResult[csf('file_no')]; ?></td>	
                            </tr>
                   		<?php
                    	$i++;
						}
                    }
                    ?>
                </table>
            </div>
            <table width="880" cellspacing="0" cellpadding="0" style="border:none" align="center">
                <tr>
                    <td align="center" height="30" valign="bottom">
                        <div style="width:100%"> 
                            <div style="width:50%; float:left" align="left">
                                <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                            </div>
                            <div style="width:50%; float:left" align="left">
                                <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
	<?php
    }
	exit();
}  

if($action=="show_po_active_listview")
{
	$sql="select wb.id, ci.id as idd, wm.gmts_item_id, wb.po_number, wb.po_total_price, wb.po_quantity, wb.pub_shipment_date as shipment_date, wb.job_no_mst, wm.job_no_prefix_num, wm.style_ref_no,ci.attached_qnty, ci.attached_rate, ci.attached_value, ci.status_active from wo_po_break_down wb, wo_po_details_master wm, com_sales_contract_order_info ci where wb.job_no_mst = wm.job_no and wb.id=ci.wo_po_break_down_id and ci.com_sales_contract_id='$data' and ci.status_active = '1' and ci.is_deleted = '0' order by ci.id";
	
	/*$arr=array(9=>$attach_detach_array);
	echo create_list_view("list_view", "Order Number,Order Qty,Order Value,Attached Qty,Rate,Attached Value,Style Ref,Item,Job No,Status", "100,100,100,100,60,100,150,150,100,80","1050","200",0, $sql, "get_php_form_data", "idd", "'populate_order_details_form_data'", 0, "0,0,0,0,0,0,0,0,0,status_active", $arr, "po_number,po_quantity,po_total_price,attached_qnty,attached_rate,attached_value,style_ref_no,style_description,job_no_prefix_num,status_active", "requires/sales_contract_controller",'','0,1,1,1,2,2,0,0,0,0','1,po_quantity,po_total_price,attached_qnty,0,attached_value,0,0,0,0');*/
	
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1050" class="rpt_table" >
            <thead>
                <th width="120">Order Number</th>
                <th width="90">Order Qty</th>
                <th width="100">Order Value</th>
                <th width="90">Attached Qty</th>
                <th width="60">Rate</th>
                <th width="100">Attached Value</th>
                <th width="150">Style Ref</th>
                <th width="150">Gmts. Item</th>
                <th width="100">Job No</th>
                <th>Status</th>
            </thead>
        </table>
        <div style="width:1050px; overflow-y:scroll; max-height:200px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1032" class="rpt_table" id="tbl_list_search" >
            <?php
                $i=1;
                $nameArray=sql_select( $sql );
                foreach ($nameArray as $selectResult)
                {
                    if($i%2==0) $bgcolor="#E9F3FF";
                    else $bgcolor="#FFFFFF";

                    ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="get_php_form_data('<?php echo $selectResult[csf('idd')]; ?>','populate_order_details_form_data','requires/sales_contract_controller')"> 
                        <td width="120"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                        <td width="90" align="right"><?php echo $selectResult[csf('po_quantity')]; ?></td>
                        <td width="100" align="right"><?php echo number_format($selectResult[csf('po_total_price')],2); ?></td>
                        <td width="90" align="right"><?php echo $selectResult[csf('attached_qnty')]; ?></td>
                        <td width="60" align="right"><?php echo number_format($selectResult[csf('attached_rate')],2); ?></td>
                        <td width="100" align="right"><?php echo number_format($selectResult[csf('attached_value')],2); ?></td>
                        <td width="150"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                        <td width="150">
                            <p>
                                <?php
                                    $gmts_item='';
                                    $gmts_item_id=explode(",",$selectResult[csf('gmts_item_id')]);
                                    foreach($gmts_item_id as $item_id)
                                    {
                                        if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
                                    }
                                    echo $gmts_item;
                                ?>
                            </p>
                        </td> 
                        <td width="100"><?php echo $selectResult[csf('job_no_mst')]; ?></td>
                        <td><?php echo $attach_detach_array[$selectResult[csf('status_active')]]; ?></td>	
                    </tr>
                <?php
                	$i++;
                }
                ?>
            </table>
        </div>
	</div>   
    <?php
	exit();
}

if($action=="populate_order_details_form_data")
{
	$data_array=sql_select("select wb.id, ci.id as idd, wm.style_description, wb.po_number, wb.po_total_price, wb.po_quantity, wb.pub_shipment_date as shipment_date,wb.job_no_mst, wm.style_ref_no, wm.gmts_item_id, wb.unit_price,ci.attached_qnty,ci.attached_rate, ci.attached_value, ci.status_active, ci.com_sales_contract_id, ci.fabric_description, ci.category_no, ci.hs_code from wo_po_break_down wb, wo_po_details_master wm, com_sales_contract_order_info ci where wb.job_no_mst = wm.job_no and wb.id=ci.wo_po_break_down_id and ci.id='$data' and ci.status_active = '1' and ci.is_deleted = '0' order by ci.id");
 
	foreach ($data_array as $row)
	{ 	
		echo "$('#tbl_order_list tbody tr:not(:first)').remove();\n";
		 
		$gmts_item='';
		$gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
		foreach($gmts_item_id as $item_id)
		{
			if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
		}
		
		echo "document.getElementById('txtordernumber_1').value 			= '".$row[csf("po_number")]."';\n";
		echo "document.getElementById('txtorderqnty_1').value 				= '".$row[csf("po_quantity")]."';\n";
		echo "document.getElementById('txtordervalue_1').value 				= '".$row[csf("po_total_price")]."';\n";
		echo "document.getElementById('txtattachedqnty_1').value 			= '".$row[csf("attached_qnty")]."';\n";
		echo "document.getElementById('hideattachedqnty_1').value 			= '".$row[csf("attached_qnty")]."';\n";
		echo "document.getElementById('hiddenunitprice_1').value 			= '".$row[csf("attached_rate")]."';\n";
		echo "document.getElementById('txtattachedvalue_1').value 			= '".$row[csf("attached_value")]."';\n";
		echo "document.getElementById('txtstyleref_1').value 				= '".$row[csf("style_ref_no")]."';\n";
		echo "document.getElementById('txtitemname_1').value 				= '".$gmts_item."';\n";
		echo "document.getElementById('txtjobno_1').value 				= '".$row[csf("job_no_mst")]."';\n";
		echo "document.getElementById('cbopostatus_1').value 				= '".$row[csf("status_active")]."';\n";
		echo "document.getElementById('txtfabdescrip_1').value 				= '".$row[csf("fabric_description")]."';\n";
		echo "document.getElementById('txtcategory_1').value 				= '".$row[csf("category_no")]."';\n";
		echo "document.getElementById('txthscode_1').value 				= '".$row[csf("hs_code")]."';\n";
		
		echo "document.getElementById('hiddenwopobreakdownid_1').value 		= '".$row[csf("id")]."';\n";
		echo "document.getElementById('hiddensalescontractorderid').value 	= '".$row[csf("idd")]."';\n";
		echo "document.getElementById('txt_tot_row').value 	= '1';\n";
		
		echo "math_operation( 'totalOrderqnty', 'txtorderqnty_', '+', 1 );\n";
		echo "math_operation( 'totalOrdervalue', 'txtordervalue_', '+', 1 );\n";
		echo "math_operation( 'totalAttachedqnty', 'txtattachedqnty_', '+', 1 );\n";
		echo "math_operation( 'totalAttachedvalue', 'txtattachedvalue_', '+', 1 );\n";
		
		$order_attahed_qnty_sc=0; $order_attahed_qnty_lc=0; $order_attahed_val_sc=0; $order_attahed_val_lc=0; $sc_no=''; $lc_no=''; 	
		$sql_sc ="SELECT a.contract_no, sum(b.attached_qnty) as at_qt,sum(b.attached_value) as at_val FROM com_sales_contract a, com_sales_contract_order_info b WHERE a.id=b.com_sales_contract_id and b.wo_po_break_down_id='".$row[csf("id")]."' and b.id!='".$data."' and b.status_active = 1 and b.is_deleted=0 group by a.id, a.contract_no";
		$result_array_sc=sql_select($sql_sc);
		foreach($result_array_sc as $scArray)
		{
			if ($sc_no=="") $sc_no = $scArray[csf('contract_no')]; else $sc_no.=",".$scArray[csf('contract_no')];
			$order_attahed_qnty_sc+=$scArray[csf('at_qt')];
			//$order_attahed_val_sc+=$scArray[csf('at_val')];
		}
		
		$sql_lc="SELECT a.export_lc_no, sum(b.attached_qnty) as at_qt,sum(b.attached_value) as at_val FROM com_export_lc a, com_export_lc_order_info b WHERE a.id=b.com_export_lc_id and b.wo_po_break_down_id='".$row[csf("id")]."' and b.status_active = 1 and b.is_deleted=0 group by a.id, a.export_lc_no";
		$result_array_sc=sql_select($sql_lc);
		foreach($result_array_sc as $lcArray)
		{
			if ($lc_no=="") $lc_no = $lcArray[csf('export_lc_no')]; else $lc_no.=",".$lcArray[csf('export_lc_no')];
			$order_attahed_qnty_lc+=$lcArray[csf('at_qt')];
			//$order_attahed_val_lc+=$lcArray[csf('at_val')];
		}
		
		$order_attached_qnty=$order_attahed_qnty_sc+$order_attahed_qnty_lc;
		//$order_attached_val=$order_attahed_val_sc+$order_attahed_val_lc;
		
		echo "document.getElementById('order_attached_qnty_1').value 		= '".$order_attached_qnty."';\n";
		echo "document.getElementById('order_attached_lc_no_1').value 		= '".$lc_no."';\n";
		echo "document.getElementById('order_attached_lc_qty_1').value 	= '".$order_attahed_qnty_lc."';\n";
		echo "document.getElementById('order_attached_sc_no_1').value 		= '".$sc_no."';\n";
		echo "document.getElementById('order_attached_sc_qty_1').value 	= '".$order_attahed_qnty_sc."';\n";
		
		if($db_type==0)
		{
			$attached_po_id=return_field_value("group_concat(wo_po_break_down_id)","com_sales_contract_order_info","com_sales_contract_id='".$row[csf("com_sales_contract_id")]."' and status_active=1 and is_deleted=0");
		}
		else
		{
			$attached_po_id=return_field_value("LISTAGG(wo_po_break_down_id, ',') WITHIN GROUP (ORDER BY wo_po_break_down_id) as po_id","com_sales_contract_order_info","com_sales_contract_id='".$row[csf("com_sales_contract_id")]."' and status_active=1 and is_deleted=0","po_id"); 
		}
		echo "document.getElementById('hidden_selectedID').value 		= '".$attached_po_id."';\n";
			 
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_po_selection_save',2);\n";  		 
		exit();
	}
}

if($action=="populate_attached_po_id")
{
	if($db_type==0)
	{
	   $attached_po_id=return_field_value("group_concat(wo_po_break_down_id)","com_sales_contract_order_info","com_sales_contract_id='$data' and status_active=1 and is_deleted=0");
	}
	else
	{
		$attached_po_id=return_field_value("LISTAGG(wo_po_break_down_id, ',') WITHIN GROUP (ORDER BY wo_po_break_down_id) as po_id","com_sales_contract_order_info","com_sales_contract_id='$data' and status_active=1 and is_deleted=0","po_id"); 
	}
	
	echo "document.getElementById('hidden_selectedID').value 		= '".$attached_po_id."';\n";
	exit();	
}


if($action=="order_list_for_attach")
{
	$explode_data = explode("**",$data);//0->wo_po_break_down id's, 1->table row
	$data=$explode_data[0];
	$table_row=$explode_data[1];
	
	if($data!="")
	{
		$data_array="SELECT wb.id, wb.po_number, wb.po_total_price, wb.po_quantity, wb.pub_shipment_date as shipment_date, wb.job_no_mst, wm.style_ref_no,wm.style_description,wb.unit_price FROM wo_po_break_down wb, wo_po_details_master wm WHERE wb.job_no_mst = wm.job_no AND wb.id in ($data) AND wb.is_deleted = 0 AND wb.status_active = 1";
		
		$data_array=sql_select($data_array);
 		foreach ($data_array as $row)
		{
			$table_row++;
			$order_attahed_qnty_sc=0; $order_attahed_qnty_lc=0; $order_attahed_val_sc=0; $order_attahed_val_lc=0; $sc_no=''; $lc_no=''; 	
			$sql_sc ="SELECT a.contract_no, sum(b.attached_qnty) as at_qt,sum(b.attached_value) as at_val FROM com_sales_contract a, com_sales_contract_order_info b WHERE a.id=b.com_sales_contract_id and b.wo_po_break_down_id='".$row[csf("id")]."' and b.status_active = 1 and b.is_deleted=0 group by a.id, a.contract_no";
			$result_array_sc=sql_select($sql_sc);
			foreach($result_array_sc as $scArray)
			{
				if ($sc_no=="") $sc_no = $scArray[csf('contract_no')]; else $sc_no.=",".$scArray[csf('contract_no')];
				$order_attahed_qnty_sc+=$scArray[csf('at_qt')];
				$order_attahed_val_sc+=$scArray[csf('at_val')];
			}
			
			$sql_lc="SELECT a.export_lc_no, sum(b.attached_qnty) as at_qt,sum(b.attached_value) as at_val FROM com_export_lc a, com_export_lc_order_info b WHERE a.id=b.com_export_lc_id and b.wo_po_break_down_id='".$row[csf("id")]."' and b.status_active = 1 and b.is_deleted=0 group by a.id, a.export_lc_no";
			$result_array_sc=sql_select($sql_lc);
			foreach($result_array_sc as $lcArray)
			{
				if ($lc_no=="") $lc_no = $lcArray[csf('export_lc_no')]; else $lc_no.=",".$lcArray[csf('export_lc_no')];
				$order_attahed_qnty_lc+=$lcArray[csf('at_qt')];
				$order_attahed_val_lc+=$lcArray[csf('at_val')];
			}
			
			$order_attached_qnty=$order_attahed_qnty_sc+$order_attahed_qnty_lc;
			$order_attached_val=$order_attahed_val_sc+$order_attahed_val_lc;
			
			$remaining_qnty = $row[csf("po_quantity")]-$order_attached_qnty; 
			$remaining_value = $row[csf("po_total_price")]-$order_attached_val;
			
			?>	
			<tr class="general" id="tr_<?php echo $table_row; ?>">
				<td>
					<input type="text" name="txtordernumber_<?php echo $table_row; ?>" id="txtordernumber_<?php echo $table_row; ?>" class="text_boxes" style="width:100px"  value="<?php echo $row[csf("po_number")]; ?>" onDblClick= "openmypage('requires/sales_contract_controller.php?action=order_popup&types=order_select_popup&buyer_id='+document.getElementById('cbo_buyer_name').value+'&selectID='+document.getElementById('hidden_selectedID').value+'&sales_contractID='+document.getElementById('txt_system_id').value+'&company_id='+document.getElementById('cbo_beneficiary_name').value,'PO Selection Form','<?php echo $table_row; ?>')" readonly= "readonly" placeholder="Double Click" />
					<input type="hidden" name="hiddenwopobreakdownid_<?php echo $table_row; ?>" id="hiddenwopobreakdownid_<?php echo $table_row; ?>" readonly= "readonly" value="<?php echo $row[csf("id")]; ?>" />
				</td>
				<td>
					<input type="text" name="txtorderqnty_<?php echo $table_row; ?>" id="txtorderqnty_<?php echo $table_row; ?>" class="text_boxes" style="width:65px; text-align:right" readonly= "readonly" value="<?php echo $row[csf("po_quantity")]; ?>" />
				</td>
				<td>
					<input type="text" name="txtordervalue_<?php echo $table_row; ?>" id="txtordervalue_<?php echo $table_row; ?>" class="text_boxes" style="width:80px; text-align:right" readonly= "readonly" value="<?php echo number_format($row[csf("po_total_price")],2,'.',''); ?>" />
				</td>
				<td>
					<input type="text" name="txtattachedqnty_<?php echo $table_row; ?>" id="txtattachedqnty_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:65px" onKeyUp="validate_attach_qnty(<?php echo $table_row; ?>)" value="<?php echo $remaining_qnty; ?>" />
					<input type="hidden" name="hideattachedqnty_<?php echo $table_row; ?>" id="hideattachedqnty_<?php echo $table_row;?>" class="text_boxes_numeric" value="<?php echo $remaining_qnty; ?>"/>
				</td>
				<td>
                    <input type="text" name="hiddenunitprice_<?php echo $table_row; ?>" id="hiddenunitprice_<?php echo $table_row; ?>" value="<?php echo $row[csf("unit_price")]; ?>" style="width:50px" class="text_boxes_numeric" onKeyUp="calculate_attach_val(<?php echo $table_row; ?>)" />
				</td>
				<td>
					<input type="text" name="txtattachedvalue_<?php echo $table_row; ?>" id="txtattachedvalue_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:80px" readonly= "readonly" value="<?php echo number_format($remaining_value,2,'.',''); ?>" />
				</td>
				<td>
					<input type="text" name="txtstyleref_<?php echo $table_row; ?>" id="txtstyleref_<?php echo $table_row; ?>" class="text_boxes" style="width:90px" readonly= "readonly" value="<?php echo $row[csf("style_ref_no")]; ?>" />
				</td>
				<td>
					<input type="text" name="txtitemname_<?php echo $table_row; ?>" id="txtitemname_<?php echo $table_row; ?>" class="text_boxes" style="width:110px" readonly= "readonly" value="<?php echo $row[csf("style_description")]; ?>" />
				</td>
				<td>
					<input type="text" name="txtjobno_<?php echo $table_row; ?>" id="txtjobno_<?php echo $table_row; ?>" class="text_boxes" style="width:80px" readonly= "readonly" value="<?php echo $row[csf("job_no_mst")]; ?>"  />
				</td>
                <td><input type="text" name="txtfabdescrip_<?php echo $table_row; ?>" id="txtfabdescrip_<?php echo $table_row; ?>" class="text_boxes" style="width:90px" /></td>
                <td>
					<input type="text" name="txtcategory_<?php echo $table_row; ?>" id="txtcategory_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:50px"  />
				</td>
                <td>
					<input type="text" name="txthscode_<?php echo $table_row; ?>" id="txthscode_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:40px" />
				</td>
				<td>                             
					<?php 
						 echo create_drop_down( "cbopostatus_".$table_row, 60, $attach_detach_array,"", $row[csf("status_active")], "", 1, "" );
					?>
				</td>
               		<input type="hidden" name="order_attached_qnty_<?php echo $table_row; ?>" id="order_attached_qnty_<?php echo $table_row; ?>" value="<?php echo $order_attached_qnty; ?>" readonly= "readonly" />
                    <input type="hidden" name="order_attached_lc_no_<?php echo $table_row; ?>" id="order_attached_lc_no_<?php echo $table_row; ?>" value="<?php echo $lc_no; ?>" readonly= "readonly" />
                    <input type="hidden" name="order_attached_lc_qty_<?php echo $table_row; ?>" id="order_attached_lc_qty_<?php echo $table_row; ?>" value="<?php echo $order_attahed_qnty_lc; ?>" readonly= "readonly" />
                    <input type="hidden" name="order_attached_sc_no_<?php echo $table_row; ?>" id="order_attached_sc_no_<?php echo $table_row; ?>" value="<?php echo $sc_no; ?>" readonly= "readonly" />
                    <input type="hidden" name="order_attached_sc_qty_<?php echo $table_row; ?>" id="order_attached_sc_qty_<?php echo $table_row; ?>" value="<?php echo $order_attahed_qnty_sc; ?>" readonly= "readonly" />
			</tr>
		<?php

		}//end foreach
		
	}//end if data condition
	
}

if($action=="create_sc_search_list_view")
{
	$data=explode('**',$data); 
	if($data[0]!=0){ $company_id=" and beneficiary_name = $data[0]";}else{ $company_id=""; }
	if($data[1]!=0){ $buyer_id=" and buyer_name = $data[1]";}else{ $buyer_id=""; }
	$search_by=$data[2];
	$search_text=$data[3];
	
	if($search_by==0)
	{
		$search_condition="";
	}
	else if($search_by==1)
	{
		$search_condition="and contract_no like '$search_text%'";
	}
	else if($search_by==2)
	{
		$search_condition="and internal_file_no like '$search_text%'";
	}
	
	if($db_type==0) $year_field="YEAR(insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql = "select id,contract_no, internal_file_no, $year_field contact_prefix_number, contact_system_id, beneficiary_name,buyer_name, applicant_name,contract_value, lien_bank,pay_term, last_shipment_date,contract_date from com_sales_contract where status_active=1 and is_deleted=0 $company_id $buyer_id $search_condition order by id";
	
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$bank_arr=return_library_array( "select id, bank_name from lib_bank",'id','bank_name');
	$arr=array (4=>$comp,5=>$buyer_arr,6=>$buyer_arr,8=>$bank_arr,9=>$pay_term);
	echo create_list_view("list_view", "Contract No,File No,Year,System ID,Company,Buyer Name,Applicant Name,Contract Value,Lien Bank,Pay Term,Last Ship Date,Contract Date", "80,80,60,70,70,70,70,100,110,70,80,70","1025","320",0, $sql, "js_set_value", "id", "", 1, "0,0,0,0,beneficiary_name,buyer_name,applicant_name,0,lien_bank,pay_term,0,0", $arr , "contract_no,internal_file_no,year,contact_prefix_number,beneficiary_name,buyer_name,applicant_name,contract_value,lien_bank,pay_term,last_shipment_date,contract_date", "",'','0,0,0,0,0,0,0,2,0,0,3,3') ;
	
}

if ($action=="save_update_delete_mst")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		// && is_duplicate_field( "internal_file_no", "com_sales_contract", "internal_file_no='$txt_internal_file_no'" )==1
		if (is_duplicate_field( "contract_no", "com_sales_contract", "contract_no=$txt_contract_no and beneficiary_name=$cbo_beneficiary_name and buyer_name=$cbo_buyer_name and lien_bank=$cbo_lien_bank" )==1)
		{
			echo "11**0"; 
			die;			
		}
		
 		$maximum_tolarence = str_replace("'", '', $txt_contract_value)+(str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_tolerance))/100;
		$minimum_tolarence = str_replace("'", '', $txt_contract_value)-(str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_tolerance))/100;
 		
		$foreign_comn_value = (str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_foreign_comn))/100;
		$local_comn_value = (str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_local_comn))/100;
		
		$max_btb_limit_value = (str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_max_btb_limit))/100;
		
		if($db_type==0) $year_cond="YEAR(insert_date)"; 
		else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
		else $year_cond="";//defined Later
		
		$new_contact_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_beneficiary_name), '', 'SC', date("Y",time()), 5, "select contact_prefix,contact_prefix_number from com_sales_contract where beneficiary_name=$cbo_beneficiary_name and $year_cond=".date('Y',time())." order by id desc ", "contact_prefix", "contact_prefix_number" ));
		
		$id=return_next_id( "id", "com_sales_contract", 1 ) ;
				 
		$field_array="id,contact_prefix,contact_prefix_number,contact_system_id,contract_no,contract_date,beneficiary_name,buyer_name,applicant_name,notifying_party,consignee,convertible_to_lc,lien_bank, lien_date,tolerance,maximum_tolarence, minimum_tolarence, last_shipment_date,expiry_date, shipping_mode,pay_term,inco_term,inco_term_place, contract_source, port_of_entry, port_of_loading, port_of_discharge, internal_file_no,shipping_line, doc_presentation_days, max_btb_limit,max_btb_limit_value, foreign_comn, foreign_comn_value, local_comn, local_comn_value, remarks, discount_clauses, tenor,currency_name, contract_value,converted_from, converted_btb_lc_list,claim_adjustment, bank_file_no,sc_year,bl_clause, export_item_category, inserted_by,insert_date,status_active";
		
		$data_array="(".$id.",'".$new_contact_system_id[1]."',".$new_contact_system_id[2].",'".$new_contact_system_id[0]."',".$txt_contract_no.",".$txt_contract_date.",".$cbo_beneficiary_name.",".$cbo_buyer_name.",".$txt_applicant_name.",".$cbo_notifying_party.",".$cbo_consignee.",".$cbo_convertible_to_lc.",".$cbo_lien_bank.",".$txt_lien_date.",".$txt_tolerance.",".$maximum_tolarence.",".$minimum_tolarence.",".$txt_last_shipment_date.",".$txt_expiry_date.",".$cbo_shipping_mode.",".$cbo_pay_term.",".$cbo_inco_term.",".$txt_inco_term_place.",".$cbo_contract_source.",".$txt_port_of_entry.",".$txt_port_of_loading.",".$txt_port_of_discharge.",".$txt_internal_file_no.",".$txt_shipping_line.",".$txt_doc_presentation_days.",".$txt_max_btb_limit.",".$max_btb_limit_value.",".$txt_foreign_comn.",".$foreign_comn_value.",".$txt_local_comn.",".$local_comn_value.",".$txt_remarks.",".$txt_discount_clauses.",".$txt_tenor.",".$cbo_currency_name.",".$txt_contract_value.",".$txt_converted_from_id.",".$txt_converted_btb_id.",".$txt_claim_adjustment.",".$txt_bank_file_no.",".$txt_year.",".$txt_bl_clause.",".$cbo_export_item_category.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
		
		$flag=1; 
		$txt_attach_row_id=str_replace("'", '', $txt_attach_row_id);
		if($txt_attach_row_id!="")
		{
			$field_array_status="lc_sc_id*is_lc_sc";
			$data_array_status=$id."*1";
	
			$rID2=sql_multirow_update("com_btb_export_lc_attachment",$field_array_status,$data_array_status,"id",$txt_attach_row_id,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}	
		
		$rID=sql_insert("com_sales_contract",$field_array,$data_array,1);
		if($flag==1)
		{
			if($rID) $flag=1; else $flag=0;
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$id."**".$new_contact_system_id[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**"."&nbsp;";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);   
				echo "0**".$id."**".$new_contact_system_id[0];
			}
			else
			{
				oci_rollback($con); 
				echo "5**0**"."&nbsp;";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
 
		if (is_duplicate_field( "contract_no", "com_sales_contract", "contract_no=$txt_contract_no and beneficiary_name=$cbo_beneficiary_name and buyer_name=$cbo_buyer_name and lien_bank=$cbo_lien_bank and id<>$txt_system_id" )==1)
		{
			echo "11**0"; 
			die;			
		}
		
 		$maximum_tolarence = str_replace("'", '', $txt_contract_value)+(str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_tolerance))/100;
		$minimum_tolarence = str_replace("'", '', $txt_contract_value)-(str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_tolerance))/100;
 		
		$foreign_comn_value = (str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_foreign_comn))/100;
		$local_comn_value = (str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_local_comn))/100;
		
		$max_btb_limit_value = (str_replace("'", '', $txt_contract_value)*str_replace("'", '', $txt_max_btb_limit))/100;
		//update code here		
		$field_array="contract_no*contract_date*beneficiary_name*buyer_name*applicant_name*convertible_to_lc*lien_bank*lien_date*tolerance*last_shipment_date*expiry_date*shipping_mode*pay_term*inco_term*inco_term_place*contract_source*port_of_entry*port_of_loading*port_of_discharge*internal_file_no*shipping_line*doc_presentation_days*max_btb_limit*max_btb_limit_value*foreign_comn*foreign_comn_value*local_comn*local_comn_value*remarks*discount_clauses*tenor*currency_name*contract_value*converted_from*converted_btb_lc_list*claim_adjustment*bank_file_no*sc_year*bl_clause*export_item_category*updated_by*update_date*status_active";
		
		$data_array="".$txt_contract_no."*".$txt_contract_date."*".$cbo_beneficiary_name."*".$cbo_buyer_name."*".$txt_applicant_name."*".$cbo_convertible_to_lc."*".$cbo_lien_bank."*".$txt_lien_date."*".$txt_tolerance."*".$txt_last_shipment_date."*".$txt_expiry_date."*".$cbo_shipping_mode."*".$cbo_pay_term."*".$cbo_inco_term."*".$txt_inco_term_place."*".$cbo_contract_source."*".$txt_port_of_entry."*".$txt_port_of_loading."*".$txt_port_of_discharge."*".$txt_internal_file_no."*".$txt_shipping_line."*".$txt_doc_presentation_days."*".$txt_max_btb_limit."*".$max_btb_limit_value."*".$txt_foreign_comn."*".$foreign_comn_value."*".$txt_local_comn."*".$local_comn_value."*".$txt_remarks."*".$txt_discount_clauses."*".$txt_tenor."*".$cbo_currency_name."*".$txt_contract_value."*".$txt_converted_from_id."*".$txt_converted_btb_id."*".$txt_claim_adjustment."*".$txt_bank_file_no."*".$txt_year."*".$txt_bl_clause."*".$cbo_export_item_category."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
 		//print_r($data_array);die;
		
		$flag=1; 
		$txt_attach_row_id=str_replace("'", '', $txt_attach_row_id);
		if($txt_attach_row_id!="")
		{
			$field_array_status="lc_sc_id*is_lc_sc";
			$data_array_status=$txt_system_id."*1";
	
			$rID2=sql_multirow_update("com_btb_export_lc_attachment",$field_array_status,$data_array_status,"id",$txt_attach_row_id,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}	
		
		$rID=sql_update("com_sales_contract",$field_array,$data_array,"id","".$txt_system_id."",1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0;
		} 
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'", '', $txt_system_id)."**".str_replace("'", '', $contact_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**".str_replace("'", '', $txt_system_id)."**".str_replace("'", '', $contact_system_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "1**".str_replace("'", '', $txt_system_id)."**".str_replace("'", '', $contact_system_id);
			}
			else
			{
				oci_rollback($con);
				echo "6**".str_replace("'", '', $txt_system_id)."**".str_replace("'", '', $contact_system_id);
			}
		}
		
		disconnect($con);
		die;
	}
	
}

if ($action=="save_update_delete_contract_order_info")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
				
 		$field_array="id,com_sales_contract_id,wo_po_break_down_id,attached_qnty,attached_rate,attached_value,status_active,fabric_description,category_no,hs_code,inserted_by,insert_date";
		$id = return_next_id( "id", "com_sales_contract_order_info", 1 );
		for($j=1;$j<=$noRow;$j++)
		{ 	
			$hiddenwopobreakdownid="hiddenwopobreakdownid_".$j;
			$txtattachedqnty="txtattachedqnty_".$j;
			$hiddenunitprice="hiddenunitprice_".$j;
			$txtattachedvalue="txtattachedvalue_".$j;
			$cbopostatus="cbopostatus_".$j;
			$txtfabdescrip="txtfabdescrip_".$j;
			$txtcategory="txtcategory_".$j;
			$txthscode="txthscode_".$j;
			
			if($$hiddenwopobreakdownid!="")
			{
				if($data_array!="") $data_array.=",";
				
				$data_array.="(".$id.",".$txt_system_id.",".$$hiddenwopobreakdownid.",".$$txtattachedqnty.",".$$hiddenunitprice.",".$$txtattachedvalue.",".$$cbopostatus.",".$$txtfabdescrip.",".$$txtcategory.",".$$txthscode.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id = $id+1;
			}
		}

		$rID=sql_insert("com_sales_contract_order_info",$field_array,$data_array,1);
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'", '', $txt_system_id)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID)
			{
				oci_commit($con);  
				echo "0**".str_replace("'", '', $txt_system_id)."**0";
			}
			else
			{
				oci_rollback($con);
				echo "5**0**0";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
				 
		 //update code here
		$field_array="id,com_sales_contract_id,wo_po_break_down_id,attached_qnty,attached_rate,attached_value,status_active,fabric_description,category_no,hs_code,inserted_by,insert_date";
		$field_array_update="wo_po_break_down_id*attached_qnty*attached_rate*attached_value*status_active*fabric_description*category_no*hs_code*updated_by*update_date";
		
		$hiddensalescontractorderid=str_replace("'", '', $hiddensalescontractorderid);
		$id = return_next_id( "id", "com_sales_contract_order_info", 1 );
		for($j=1;$j<=$noRow;$j++)
		{ 	
			$hiddenwopobreakdownid="hiddenwopobreakdownid_".$j;
			$txtattachedqnty="txtattachedqnty_".$j;
			$hiddenunitprice="hiddenunitprice_".$j;
			$txtattachedvalue="txtattachedvalue_".$j;
			$cbopostatus="cbopostatus_".$j;
			$txtfabdescrip="txtfabdescrip_".$j;
			$txtcategory="txtcategory_".$j;
			$txthscode="txthscode_".$j;
			
			if($j==1)
			{
				if($hiddensalescontractorderid!="")
				{
					if(str_replace("'", '', $$cbopostatus)==0)
					{
						$invoice_no="";
						$po_id=$$hiddenwopobreakdownid;
						$sql_invoice="select a.invoice_no from com_export_invoice_ship_mst a, com_export_invoice_ship_dtls b where a.id=b.mst_id and a.lc_sc_id=$txt_system_id and a.is_lc=2 and b.po_breakdown_id=$po_id and b.current_invoice_qnty>0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.invoice_no";
						$data=sql_select($sql_invoice);
						if(count($data)>0)
						{
							foreach($data as $row)
							{
								if($invoice_no=="") $invoice_no=$row[csf('invoice_no')]; else $invoice_no.=",\n".$row[csf('invoice_no')];	
							}
							echo "13**".$invoice_no."**1"; 
							die;	
						}
					}
					
					$data_array_update="".$$hiddenwopobreakdownid."*".$$txtattachedqnty."*".$$hiddenunitprice."*".$$txtattachedvalue."*".$$cbopostatus."*".$$txtfabdescrip."*".$$txtcategory."*".$$txthscode."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				}
				else
				{
					if($data_array!="") $data_array.=",";
					
					$data_array ="(".$id.",".$txt_system_id.",".$$hiddenwopobreakdownid.",".$$txtattachedqnty.",".$$hiddenunitprice.",".$$txtattachedvalue.",".$$cbopostatus.",".$$txtfabdescrip.",".$$txtcategory.",".$$txthscode.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
					$id = $id+1;
				}
			}
			else
			{
				if($$hiddenwopobreakdownid!="")
				{
					if($data_array!="") $data_array.=",";
					
					$data_array.="(".$id.",".$txt_system_id.",".$$hiddenwopobreakdownid.",".$$txtattachedqnty.",".$$hiddenunitprice.",".$$txtattachedvalue.",".$$cbopostatus.",".$$txtfabdescrip.",".$$txtcategory.",".$$txthscode.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
					$id = $id+1;
				}
			}
		}
		
		//echo "insert into com_sales_contract_order_info (".$field_array.") values".$data_array;die;
	
		$flag=1;
		if($data_array!="")
		{
			$rID2=sql_insert("com_sales_contract_order_info",$field_array,$data_array,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		if($data_array_update!="")
		{
			$rID=sql_update("com_sales_contract_order_info",$field_array_update,$data_array_update,"id","".$hiddensalescontractorderid."",1);
			if($flag==1) 
			{
				if($rID) $flag=1; else $flag=0;
			} 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'", '', $txt_system_id)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);   
				echo "1**".str_replace("'", '', $txt_system_id)."**0";
			}
			else
			{
				oci_rollback($con);
				echo "6**0**1";
			}
		}
		disconnect($con);
		die;
	}
	
}

?>


 