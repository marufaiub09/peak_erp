<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will create Machine List Library
					 
Functionality	:	 

JS Functions	:

Created by		:	CTO 
Creation date 	: 	08-10-2012
Updated by 		: Maruf		
Update date		: 09-12-2015		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);

$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Sewing Operation Entry", "../../", 1, 1,$unicode,'','');
 ?>	
 
<script language="javascript">
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission='<?php echo $permission; ?>';


function fnc_machine_name_entry( operation )
{
  	if (form_validation('cbo_company_name*cbo_location_name*cbo_floor_name*txt_machine_no*cbo_catagory','Company Name*Location Name*Floor Name*Machine No*Machine Category')==false)
	{
		return;  //'txt_operation*txt_rate*cbo_uom*cbo_resource*txt_operator_smv*txt_helper_smv*txt_total_smv
	}
	
	else
	{
		eval(get_submitted_variables('cbo_company_name*cbo_location_name*cbo_floor_name*txt_machine_no*cbo_catagory*txt_group*txt_dia_width*txt_gauge*txt_extra_cylinder*txt_no_of_feeder*txt_attachment*txt_prod_capacity*cbo_capacity_uom*txt_brand*txt_origin*txt_purchase_date*txt_purchase_cost*txt_accumulated_dep*txt_depreciation_rate*cbo_depreciation_method*cbo_status*txt_remarks*update_id'));
		
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name*cbo_location_name*cbo_floor_name*txt_machine_no*cbo_catagory*txt_group*txt_dia_width*txt_gauge*txt_extra_cylinder*txt_no_of_feeder*txt_attachment*txt_prod_capacity*cbo_capacity_uom*txt_brand*txt_origin*txt_purchase_date*txt_purchase_cost*txt_accumulated_dep*txt_depreciation_rate*cbo_depreciation_method*cbo_status*txt_remarks*txt_seq_no*update_id',"../../");
		freeze_window(operation);
		http.open("POST","requires/machine_name_entry_controller.php", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_machine_name_entry_reponse;
	}
}

function fnc_machine_name_entry_reponse()
{
	if(http.readyState == 4) 
	{
		//alert(http.responseText);
		var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		//document.getElementById('update_id').value  = reponse[2];
		show_list_view('','machine_entry_list_view','machine_entry_list','../production/requires/machine_name_entry_controller','setFilterGrid("list_view",-1)');
		set_button_status(0, permission, 'fnc_machine_name_entry',1);
		reset_form('machinename_1','','');
		release_freezing();
	}
}	

</script>
</head>
<body onLoad="set_hotkey()">
<div align="center" style="width:100%">
         <?php echo load_freeze_divs ("../../",$permission);  ?>
	<fieldset style="width:700px;">
	<legend>Machine Name Info</legend>
		<form name="machinename_1" id="machinename_1" autocomplete="off">	
			<table cellpadding="0" cellspacing="2" width="600">
				<tr>
					<td width="125" class="must_entry_caption">Company</td>
					<td width="175">		<?php 
												 
                                    echo create_drop_down( "cbo_company_name", 162, "select id,company_name from lib_company comp where is_deleted=0 and status_active=1 $company_cond order by company_name",'id,company_name', 1,"--- Select Company ---",'' ,"load_drop_down( 'requires/machine_name_entry_controller', this.value, 'load_drop_down_location', 'location' ); show_list_view(this.value,'machine_entry_list_view','machine_entry_list','../production/requires/machine_name_entry_controller','setFilterGrid(\'list_view\',-1)')" );
                                ?>
                                 
					</td>
                    <td width="125">Attachment</td>
					<td>
						<input type="text" name="txt_attachment" id="txt_attachment" class="text_boxes" style="width:150px">
					</td>
				</tr>
                <tr>
					<td class="must_entry_caption">Location</td>
					<td  id="location">			<?php 
                                                    echo create_drop_down( "cbo_location_name", 162, $blank_array,'', 1, '--- Select Location ---', 0, "load_drop_down( 'requires/machine_name_entry_controller', this.value, 'load_drop_down_floor', 'floor' )"  );
                                                ?>
                    			 
						 
					</td>
                    <td>Prod. Capacity</td>
					<td>
					<input type="text" name="txt_prod_capacity" id="txt_prod_capacity" class="text_boxes_numeric" style="width:150px"/>
					</td>
				</tr>			
				<tr>
					<td class="must_entry_caption">Floor No</td>
					<td  id="floor">
                    			<?php 
                                    echo create_drop_down( "cbo_floor_name", 162, "select id,floor_name from lib_prod_floor where is_deleted=0 and status_active=1 order by floor_name",'id', 'floor_name', "" );
                                ?>
						 
					</td>
                    <td>Capacity UOM</td>
					<td>
                    			<?php 
                                    echo create_drop_down( "cbo_capacity_uom", 162, $unit_of_measurement,'', '', "" );
                                ?>
						 
					</td>
				</tr>
                <tr>
					<td class="must_entry_caption">Machine No</td>
					<td>
					<input type="text" name="txt_machine_no" id="txt_machine_no" class="text_boxes" style="width:150px">
					</td>
                    <td>Brand</td>
					<td>
					<input type="text" name="txt_brand" id="txt_brand" class="text_boxes" style="width:150px">
					</td>
				</tr>
                <tr>
					<td class="must_entry_caption">Category</td>
					<td>
                    			<?php 
                                    echo create_drop_down( "cbo_catagory", 162, $machine_category,'', '', "" );
                                ?>
					</td>
                    <td>Origin</td>
					<td>
					<input type="text" name="txt_origin" id="txt_origin" class="text_boxes" style="width:150px">
					</td>
				</tr>
                <tr>
					<td>Group</td>
					<td>
					<input type="text" name="txt_group" id="txt_group" class="text_boxes" style="width:150px">
					</td>
                    <td>Purchase Date</td>
					<td>
					<input type="text" name="txt_purchase_date" id="txt_purchase_date" class="datepicker" style="width:150px;">
					 
					</td>
				</tr>
                <tr>
					<td>Dia/Width</td>
					<td>
					<input type="text" name="txt_dia_width" id="txt_dia_width" class="text_boxes_numeric" style="width:150px">
					</td>
                    <td>Purchase Cost</td>
					<td>
					<input type="text" name="txt_purchase_cost" id="txt_purchase_cost" class="text_boxes_numeric"  style="width:150px">
					</td>
				</tr>
                <tr>
					<td>Gauge</td>
					<td>
					<input type="text" name="txt_gauge" id="txt_gauge" class="text_boxes" style="width:150px">
					</td>
                    <td>Accumulated Dep.</td>
					<td>
					<input type="text" name="txt_accumulated_dep" id="txt_accumulated_dep" class="text_boxes_numeric" style="width:150px">
					</td>
				</tr>
                <tr>
					<td>Extra Cylinder</td>
					<td><input type="text" name="txt_extra_cylinder" id="txt_extra_cylinder" class="text_boxes" style="width:150px"></td>
                    <td>Depreciation Rate</td>
					<td><input type="text" name="txt_depreciation_rate" id="txt_depreciation_rate" class="text_boxes_numeric" style="width:150px">
					</td>
				</tr>
                <tr>
					<td>No of feeder</td>
					<td><input type="text" name="txt_no_of_feeder" id="txt_no_of_feeder" class="text_boxes" style="width:150px"></td>
                    <td>Depreciation Method</td>
					<td>
                    			<?php 
                                    echo create_drop_down( "cbo_depreciation_method", 162, $depreciation_method,'', '', "" );
                                ?>
                    	 
					</td>
				</tr>
                <tr>
					<td>Remarks</td>
					<td ><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:150px"></td>
					<td>Sequence No</td>
					<td ><input type="text" name="txt_seq_no" id="txt_seq_no" class="text_boxes_numeric" style="width:150px"></td>
				</tr>
                <tr>
                	<td>Status</td>
					<td colspan="3"><?php 
                                    echo create_drop_down( "cbo_status", 162, $row_status,'', '', '' );
                                ?> 	
                         
						<input type="hidden" name="update_id" id="update_id" class="text_boxes" style="width:150px">
					</td>
				</tr>
                <tr>
					<td colspan="4" height="15"></td>
				</tr>
				<tr>
				  <td colspan="4" align="center" class="button_container">
								<?php 
                                    echo load_submit_buttons( $permission, "fnc_machine_name_entry", 0,0 ,"reset_form('machinename_1','','',1)");
                                ?>	
					</td>				
				</tr>
                <tr>
					<td colspan="4" height="15"></td>
				</tr>
                
                <tr>
					<td colspan="4" align="center" id="machine_entry_list">
                     
                    
                    </td>
				</tr>			
			</table>
		</form>
    </fieldset>	
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript">//set_bangla();</script>
<script>
$("#cbo_location_name").val(0);

</script>
</html>


