<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_id", 150, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select --", $selected, "",0 );     	 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	//echo $datediff;
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_location=str_replace("'","",$cbo_location_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$company_short_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name"  );
	
	
		$sql_floor=sql_select("Select id, floor_name from  lib_prod_floor where company_id=$cbo_company and location_id=$cbo_location and status_active=1 and is_deleted=0 order by floor_name ");
		$count_data=count($sql_floor);
		$count_hd=count($sql_floor)+1;
		$width_hd=$count_hd*80;
		
		$table_width=90+($count_data*100)*3;
	ob_start();	
	//$table_width=90+($datediff*160);
?>
	<div id="scroll_body" align="center" style="height:auto; width:auto; margin:0 auto; padding:0;">
        <table width="<?php echo $table_width; ?>" cellpadding="0" cellspacing="0" id="caption" align="center">
            <tr>
               <td align="center" width="100%" colspan="<?php echo $count_hd; ?>" class="form_caption" ><strong style="font-size:18px">Company Name:<?php echo $company_library[$cbo_company]; ?></strong></td>
            </tr> 
            <tr>  
               <td align="center" width="100%" colspan="<?php echo $count_hd; ?>" class="form_caption" ><strong style="font-size:14px"><?php echo $report_title; ?></strong></td>
            </tr>
            <tr>  
               <td align="center" width="100%" colspan="<?php echo $count_hd; ?>" class="form_caption" ><strong style="font-size:14px"> <?php echo "From : ".change_date_format(str_replace("'","",$txt_date_from))." To : ".change_date_format(str_replace("'","",$txt_date_to))."" ;?></strong></td>
            </tr>  
        </table>
        <?php
		
		if ($cbo_location==0) $location_id =""; else $location_id =" and a.location=$cbo_location ";
		if($db_type==0)
		{
			if( $date_from==0 && $date_to==0 ) $production_date=""; else $production_date= " and a.production_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			if( $date_from==0 && $date_to==0 ) $production_date=""; else $production_date= " and a.production_date between '".date("j-M-Y",strtotime(str_replace("'","",$date_from)))."' and '".date("j-M-Y",strtotime(str_replace("'","",$date_to)))."'";
		}
		

		$sql_result="Select a.id, a.production_type, a.floor_id, a.production_date, b.production_qnty as production_qnty from pro_garments_production_mst a, pro_garments_production_dtls b where a.id=b.mst_id and a.production_type in (1,5,8) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_id=$cbo_company $location_id $production_date";
		//echo $sql_result;	
		$sql_dtls=sql_select($sql_result);
		$floor_qnty=array();
		
		foreach ( $sql_dtls as $row )
		{
			$floor_qnty[change_date_format($row[csf("production_date")],'','',1)][$row[csf("floor_id")]][$row[csf("production_type")]] +=$row[csf("production_qnty")];
		}
		$head_arr=array(1=>"Cutting Production",5=>"Sewing Production",8=>"Finishing Production");
		$unit_wise_total_array=array();
		?>
        <div align="center" style="height:auto;">
        <table width="<?php echo $table_width; ?> " border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all"> 
            <thead>
                <tr>
                    <th width="90" rowspan="2" >Date</th>
                    <?php
                     foreach ( $head_arr as $head=>$headval )
					 {
						?>
                            <th width="<?php echo $width_hd; ?>" colspan="<?php echo $count_hd; ?>"><?php echo $headval; ?></th>
						<?php
					 }
					?>
                </tr>
               <tr>
					<?php
					foreach ( $head_arr as $head=>$headval )
					{
						foreach ( $sql_floor as $rows )
						{
							?>
                                <th width="80" colspan=""><?php echo $rows[csf("floor_name")]; ?></th>
							<?php
						}
						?>
						<th width="80" colspan="">Total</th>
					<?php
					}
                   ?>
               </tr>
            </thead>
        </table>
        </div>
        <div align="center" style="max-height:300px" id="scroll_body2">
        <table align="center" cellspacing="0" width="<?php echo $table_width; ?> "  border="1" rules="all" class="rpt_table" >
			<?php
			$value_define=array();
            for($j=0;$j<$datediff;$j++)
            {
                if ($j%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";
                    
                $date_data_array=array();
				$newdate =add_date(str_replace("'","",$txt_date_from),$j);
				$newdate=date("d-M-Y",strtotime(str_replace("'","",$newdate)));
                $date_data_array[$j]=$newdate;
            ?>
            <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $j; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $j; ?>">
                <td width="90"><?php echo change_date_format($newdate); ?></td>
                <?php
                foreach ( $head_arr as $head=>$headval )
                {
                    $site_tot_qnty='';
                    foreach ( $sql_floor as $rows )
                    { 
                        ?>
                            <td width="80" align="right"><?php  echo number_format($floor_qnty[$date_data_array[$j]][$rows[csf("id")]][$head],0); ?></td>
                        <?php
                         $site_tot_qnty+=$floor_qnty[$date_data_array[$j]][$rows[csf("id")]][$head];
                         $unit_wise_total_array[$head][$rows[csf("id")]] +=$floor_qnty[$date_data_array[$j]][$rows[csf("id")]][$head];
						 if($floor_qnty[$date_data_array[$j]][$rows[csf("id")]][$head]!="")  echo $value_define[$head][$rows[csf("id")]] +=1;
						 
                    }
                    ?>
                        <td width="80" align="right"><strong><?php echo number_format($site_tot_qnty,0); ?></strong></td>
                    <?php
                }
                ?>
            </tr>
            <?php
            }
            ?>
            <tr bgcolor="#dddddd">
                <td align="center" width="90"><strong>Total : </strong></td>
                <?php
                foreach ( $head_arr as $head=>$headval )
                {
                    $grand_total_tot='';
                    foreach ( $sql_floor as $rows )
                    {
                        ?>
                            <td width="80" align="right"><strong><?php echo number_format($unit_wise_total_array[$head][$rows[csf("id")]],0); ?></strong></td>
                        <?php
                        $grand_total_tot+=$unit_wise_total_array[$head][$rows[csf("id")]];
                    }
                    ?>
                    <td width="80" align="right"><strong><?php echo number_format($grand_total_tot,0); ?></strong></td>
                    <?php
                }
                ?>
            </tr>
            <tr bgcolor="#dddddd">
                <td align="center" width="90"><strong>Avg : </strong></td>
                <?php
				$m=1;
                foreach ( $head_arr as $head=>$headval )
                {
                    $avg='';
                    $avg_tot='';
                    foreach ( $sql_floor as $rows )
                    {
                        $avg=$unit_wise_total_array[$head][$rows[csf("id")]]/ $value_define[$head][$rows[csf("id")]];
						//$avg=$unit_wise_total_array[$head][$rows[csf("id")]]/$value_define[$head][$rows[csf("id")]];
                        ?>
                            <td width="80" align="right"><strong><?php echo number_format( $avg,2); ?></strong></td>
                        <?php
                        $avg_tot+=$avg;
                    }
                    ?>
                    <td width="80" align="right"><strong><?php  echo number_format($avg_tot,2); ?></strong></td>
                <?php
                }
                ?>
            </tr>
        </table>
    </div>
    </div>
<?php    
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();      
}
?>
