﻿<?
/*--- ----------------------------------------- Comments
Purpose			: 	This form will create Sub-contract Knitting bill issue
Functionality	:	
JS Functions	:
Created by		:	Kausar 
Creation date 	: 	27-05-2014	
Updated by 		: 		
Update date		: 
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Knitting bill issue", "../", 1,1, $unicode,1,'');

?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
	var permission='<? echo $permission; ?>';
	
	var str_atention = [<? echo substr(return_library_autocomplete( "select attention from subcon_inbound_bill_mst group by attention", "attention" ), 0, -1); ?>];

	$(document).ready(function(e)
	 {
            $("#txt_attention").autocomplete({
			 source: str_atention
		  });
     });

	function openmypage_bill()
	{ 
		var data=document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_party_name').value;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/knitting_bill_issue_controller.php?data='+data+'&action=bill_no_popup','Bill Popup', 'width=680px,height=400px,center=1,resize=1,scrolling=0','')
		
		emailwindow.onclose=function()
		{
			var theemail=this.contentDoc.getElementById("issue_id") //Access form field with id="emailfield"
			if (theemail.value!="")
			{
				freeze_window(5);
				get_php_form_data( theemail.value, "load_php_data_to_form_issue", "requires/knitting_bill_issue_controller" );
				window_close( theemail.value);
				show_list_view(document.getElementById('cbo_party_source').value+'***'+document.getElementById('cbo_party_name').value+'***'+document.getElementById('update_id').value+'***'+document.getElementById('issue_id_all').value,'knitting_delivery_list_view','knitting_info_list','requires/knitting_bill_issue_controller','set_all()','','');

				//show_list_view( data, action, div, path, extra_func, is_append ) 
	 			setFilterGrid('tbl_list_search',-1);
				set_button_status(1, permission, 'fnc_knitting_bill_issue',1);
				set_all_onclick();
				release_freezing();
			}
		}
	}

	function set_all()
	{
		var old=document.getElementById('issue_id_all').value;
		//alert (old);
		if(old!="")
		{   
			old=old.split(",");
			for(var i=0; i<old.length; i++)
			{   
				var cur=document.getElementById('currid'+old[i]).value;
				js_set_value( old[i]+'***'+document.getElementById('currid'+old[i]).value );
			}
		}
	}

	function window_close( frm )
	{
		
		if ( !frm ) var frm='';
		// alert (frm)
		if ($('#update_id').val()!=frm)
			var issue_id=document.getElementById('issue_id_all').value;
		else
			var issue_id='';
		var data=document.getElementById('selected_order_id').value+"***"+issue_id+"***"+frm+"***"+document.getElementById('cbo_party_source').value;
		//alert (data)
		var list_view_orders = return_global_ajax_value( data, 'load_php_dtls_form', '', 'requires/knitting_bill_issue_controller');
		if(list_view_orders!='')
		{
			$("#bill_issue_table tr").remove();
			$("#bill_issue_table").append(list_view_orders);
		}
		
		var tot_row=$('#bill_issue_table tr').length;
		math_operation( "total_qnty", "deliveryqnty_", "+", tot_row );
		math_operation( "total_amount", "amount_", "+", tot_row );
		set_all_onclick();
	}
	
	function check_all_data()
	{
		var tbl_row_count = document.getElementById( 'list_view_issue' ).rows.length;
		//alert (tbl_row_count)
		tbl_row_count = tbl_row_count - 1;
		
		for( var i = 1; i <= tbl_row_count; i++ ) {
			eval($('#tr_'+i).attr("onclick"));  
		}
	}
	
	function toggle( x, origColor ) {
		var newColor = 'yellow';
		if ( x.style ) {
		x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
		}
	}
	var selected_id = new Array(); var selected_currency_id = new Array();
	function js_set_value(id)
	{
		//alert (id);
		//var source=$('#cbo_party_source').val();
		//alert (source)
		var str=id.split("***");
		
		if( jQuery.inArray( str[1], selected_currency_id ) != -1  || selected_currency_id.length<1 )
		{
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
			if( jQuery.inArray(  str[0] , selected_id ) == -1) {
				
				selected_id.push( str[0] );
				selected_currency_id.push( str[1] );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str[0]  ) break;
			}
				selected_id.splice( i, 1 );
				selected_currency_id.splice( i, 1 );
			}
			var id = ''; var currency = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				currency += selected_currency_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			currency = currency.substr( 0, currency.length - 1 );
			$('#selected_order_id').val( id );
			$('#selected_currency_no').val( currency );
		}
		else
		{
			$('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			{ 
				$(this).html('Currency Mix Not Allowed').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			});
		}
	}
	
	function fnc_knitting_bill_issue( operation )
	{
		if(operation==4)
		{
			var report_title=$( "div.form_caption" ).html();
			print_report( $('#cbo_company_id').val()+'*'+$('#update_id').val()+'*'+$('#txt_bill_no').val()+'*'+report_title, "knitting_bill_print", "requires/knitting_bill_issue_controller") 
			//return;
			show_msg("3");
		}
		else if(operation==0 || operation==1 || operation==2)
		{
			if( form_validation('cbo_company_id*txt_bill_date*cbo_party_name*cbo_party_source','Company Name*Bill Date*Party Name*Party Source')==false)
			{
				return;
			}
			else
			{
				var source=$('#cbo_party_source').val();
				var tot_row=$('#bill_issue_table tr').length;
				var data1="action=save_update_delete&operation="+operation+"&tot_row="+tot_row+get_submitted_data_string('txt_bill_no*cbo_company_id*cbo_location_name*txt_bill_date*cbo_party_name*cbo_party_source*txt_attention*cbo_bill_for*update_id',"../");
				var data2='';
				for(var i=1; i<=tot_row; i++)
				{
					if (form_validation('cbouom_'+i+'*txtrate_'+i,'Uom*Rate')==false)
					{
						return;
					}
					else if($('#txtrate_'+i).val()==0)
					{
						alert ("Rate Not Blank or Zero.");
						$('#txtrate_'+i).focus();
						return;
					}
					else
					{
						if(source==2)
						{
						data2+=get_submitted_data_string('deleverydate_'+i+'*challenno_'+i+'*ordernoid_'+i+'*stylename_'+i+'*buyername_'+i+'*itemid_'+i+'*compoid_'+i+'*bodypartid_'+i+'*cbouom_'+i+'*numberroll_'+i+'*deliveryqnty_'+i+'*txtrate_'+i+'*amount_'+i+'*remarksvalue_'+i+'*deliveryid_'+i+'*curanci_'+i+'*updateiddtls_'+i+'*delete_id',"../");//
						}
						else
						{
						data2+=get_submitted_data_string('deleverydate_'+i+'*challenno_'+i+'*ordernoid_'+i+'*stylename_'+i+'*buyername_'+i+'*itemid_'+i+'*compoid_'+i+'*bodypartid_'+i+'*cbouom_'+i+'*numberroll_'+i+'*deliveryqnty_'+i+'*txtrate_'+i+'*amount_'+i+'*remarksvalue_'+i+'*deliveryid_'+i+'*curanci_'+i+'*updateiddtls_'+i,"../");//
						}
					}
				}
				//alert (data2);return;
				var data=data1+data2;
				freeze_window(operation);
				http.open("POST","requires/knitting_bill_issue_controller.php",true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_knitting_save_update_delete_response;
			}
		}
	}

	function fnc_knitting_save_update_delete_response()
	{
		if(http.readyState == 4) 
		{
			//alert (http.responseText);return;
			var response=trim(http.responseText).split('**');
			//if (response[0].length>2) reponse[0]=10;
			show_msg(response[0]);
			if(response[0]==0 || response[0]==1)
			{
				document.getElementById('update_id').value = response[1];
				document.getElementById('txt_bill_no').value = response[2];
				window_close(response[1]);
				set_button_status(1, permission, 'fnc_knitting_bill_issue',1);
			}
			release_freezing();
		}
	}
	
	function open_terms_condition_popup(page_link,title)
	{
		var txt_bill_no=document.getElementById('txt_bill_no').value;
		if (txt_bill_no=="")
		{
			alert("Save The Knitting Bill First");
			return;
		}	
		else
		{
			page_link=page_link+get_submitted_data_string('txt_bill_no','../');
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=720px,height=370px,center=1,resize=1,scrolling=0','')
			emailwindow.onclose=function(){};
		}
	}
	
	function openmypage_remarks(id)
	{
		var data=document.getElementById('remarksvalue_'+id).value;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/knitting_bill_issue_controller.php?data='+data+'&action=remarks_popup','Remarks Popup', 'width=420px,height=320px,center=1,resize=1,scrolling=0','')
		
		emailwindow.onclose=function()
		{
			var theemail=this.contentDoc.getElementById("text_new_remarks");//Access form field with id="emailfield"
			if (theemail.value!="")
			{
				$('#remarksvalue_'+id).val(theemail.value);
			}
		}
	}
	
	function qnty_caluculation(id)
	{
		$("#amount_"+id).val(($("#deliveryqnty_"+id).val()*1)*($("#txtrate_"+id).val()*1));
		var tot_row=$('#bill_issue_table tr').length;
		math_operation( "total_qnty", "deliveryqnty_", "+", tot_row );
		math_operation( "total_amount", "amount_", "+", tot_row );
	}
</script>
</head>
<body onLoad="set_hotkey()">
    <div align="center" style="width:100%;">
    <? echo load_freeze_divs ("../",$permission);  ?>
    <form id="knitigbillissue_1" name="knitigbillissue_1" autocomplete="off">
    <fieldset style="width:850px;">
    <legend>Knitting Bill Info </legend>
        <table cellpadding="0" cellspacing="2" width="850">
            <tr>
                <td align="right" colspan="3"><strong>Bill No </strong></td>
                <td colspan="3">
                    <input type="hidden" name="hidden_tot" id="hidden_tot"  />
                    <input type="text" name="selected_order_id" id="selected_order_id" />
                    <input type="hidden" name="selected_currency_no" id="selected_currency_no" />
                    <input type="hidden" name="sel_order_pro_id" id="sel_order_pro_id" />
                    <input type="hidden" name="update_id" id="update_id" /><br>
                    <input type="text" name="txt_bill_no" id="txt_bill_no" class="text_boxes" style="width:140px" placeholder="Double Click to Search" onDblClick="openmypage_bill();" readonly tabindex="1" >
                </td>
            </tr>
            <tr>
                <td width="110" class="must_entry_caption">Company Name</td>
                <td width="150">
                    <?php 
                        echo create_drop_down( "cbo_company_id",150,"select id,company_name from lib_company comp where is_deleted=0 and status_active=1 $company_cond order by company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down( 'requires/knitting_bill_issue_controller', this.value, 'load_drop_down_location', 'location_td');","","","","","",2);	
                    ?>
                </td>
                <td width="110">Location Name</td>                                              
                <td width="150" id="location_td">
                    <? 
                        echo create_drop_down( "cbo_location_name", 150, $blank_array,"", 1, "--Select Location--", $selected,"","","","","","",3);
                    ?>
                </td>
                <td width="110" class="must_entry_caption">Bill Date</td>                                              
                <td width="150">
                    <input class="datepicker" type="text" style="width:140px" name="txt_bill_date" id="txt_bill_date" tabindex="4" value="<? echo date('d-m-Y'); ?>" />
                </td>
            </tr> 
            <tr>
                <td class="must_entry_caption">Party Source</td>
                <td>
                    <?
                        echo create_drop_down( "cbo_party_source", 150, $knitting_source,"", 1, "-- Select Party --", $selected, "load_drop_down( 'requires/knitting_bill_issue_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_down_party_name', 'party_td' );",0,"1,2","","","",5);     //setFilterGrid('list_view',-1)
                    ?> 
                </td>
                <td class="must_entry_caption">Party Name</td>
                <td id="party_td">
                    <?
                        echo create_drop_down( "cbo_party_name", 150, $blank_array,"", 1, "-- Select Party --", $selected, "",0,"","","","",6);
                    ?> 
                </td>
                <td>Attention</td>
                <td><input class="text_boxes" type="text" style="width:140px" name="txt_attention" id="txt_attention" tabindex="7" /></td>
            </tr>
            <tr>
                <td style="display:none">Bill For</td>
                <td style="display:none">
                    <?
                        echo create_drop_down( "cbo_bill_for", 150, $bill_for,"", 1, "-- Select Party --", $selected, "",0,"","","","",8);
                    ?> 
                </td>
            </tr>
        </table>
        </fieldset>
        <br>
        <fieldset style="width:1050px;">
        <legend>Knitting Bill Info</legend>
        <table  style="border:none; width:930px;" cellpadding="0" cellspacing="1" border="0" id="">
            <thead class="form_table_header">
                <th width="60">Delivery Date </th>
                <th width="40">Cln. No.</th>
                <th width="70">Order No.</th>
                <th width="80">Cust.Style</th>
                <th width="70">Cust.Buyer</th>
                <th width="40">Roll</th>
                <th width="120" style="display:none">Yarn Des.</th>
                <th width="80">Body Part</th>                                      
                <th width="140">Fabric Des.</th>
                <th width="50" class="must_entry_caption">UOM</th>
                <th width="40">Delv. Qty</th>
                <th width="40" class="must_entry_caption">Rate</th>
                <th width="40" >Amount</th>
                <th>RMK</th>
            </thead>
            <tbody id="bill_issue_table">
                <tr align="center">				
                    <td>
                        <input type="hidden" name="updateiddtls_1" id="updateiddtls_1">
                        <input type="text" name="deleverydate_1" id="deleverydate_1"  class="datepicker" style="width:60px" readonly />									
                    </td>
                    <td>
                        <input type="text" name="challenno_1" id="challenno_1"  class="text_boxes" style="width:40px" readonly />
                    </td>
                    <td>
                        <input type="hidden" name="ordernoid_1" id="ordernoid_1" value="">
                        <input type="text" name="orderno_1" id="orderno_1"  class="text_boxes" style="width:70px" readonly />
                    </td>
                    <td>
                        <input type="text" name="stylename_1" id="stylename_1"  class="text_boxes" style="width:80px;" />
                    </td>
                    <td>
                        <input type="text" name="buyername_1" id="buyername_1"  class="text_boxes" style="width:70px" />
                    </td>
                    <td>			
                        <input name="numberroll_1" id="numberroll_1" type="text" class="text_boxes" style="width:40px" readonly />
                    </td>  
                    <td style="display:none">
                        <input type="text" name="yarndesc_1" id="yarndesc_1"  class="text_boxes" style="width:115px" readonly/>
                    </td>
                    <td>
                        <input type="text" name="bodypart_1" id="bodypart_1"  class="text_boxes" style="width:80px" readonly/>
                    </td>
                    <td>
                        <input type="text" name="febricdesc_1" id="febricdesc_1"  class="text_boxes_numeric" style="width:135px" readonly/>
                    </td>
                    <td>
						<? echo create_drop_down( "cbouom_1", 50, $unit_of_measurement,"", 1, "-UOM-",0,"",0,"1,2,12" );?>
                    </td>
                    <td>
                        <input type="text" name="deliveryqnty_1" id="deliveryqnty_1"  class="text_boxes_numeric" style="width:40px" />
                    </td>
                    <td>
                        <input type="text" name="txtrate_1" id="txtrate_1"  class="text_boxes_numeric" style="width:40px" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" style="width:40px"  class="text_boxes"  readonly />
                    </td>
                    <td>
                        <input type="button" name="remarks_1" id="remarks_1"  class="formbuttonplasminus" value="R" onClick="openmypage_remarks(1);" />
                        <input type="hidden" name="remarksvalue_1" id="remarksvalue_1" class="text_boxes" />
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td width="65px">&nbsp;</td>								
                    <td width="60px" style="display:none">&nbsp;</td>
                    <td width="70px">&nbsp;</td>
                    <td width="80px">&nbsp;</td>
                    <td width="80px">&nbsp;</td>
                    <td width="50px">&nbsp;</td>
                    <td width="120px">&nbsp;</td>
                    <td width="140px" colspan="2" align="center"><input type="button" id="set_button" class="image_uploader" style="width:140px; margin-left:5px; margin-top:2px;" value="Terms Condition" onClick="open_terms_condition_popup('requires/knitting_bill_issue_controller.php?action=terms_condition_popup','Terms Condition')" /></td>
                    <td width="50px">Total Qty</td>
                    <td width="40px">
                        <input type="text" name="total_qnty" id="total_qnty"  class="text_boxes_numeric" style="width:40px" value="" readonly disabled />
                    </td>
                    <td width="40px">Total</td>
                    <td width="40px">
                        <input type="text" name="total_amount" id="total_amount"  class="text_boxes_numeric" style="width:40px" value="" readonly disabled />
                    </td>
                    <td width="">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="13" height="15" align="center"> </td>
                </tr>
                <tr>
                    <td colspan="13" align="center" class="button_container">
                    <? 
					$date=date('d-m-Y');
                    echo load_submit_buttons($permission,"fnc_knitting_bill_issue",0,1,"reset_form('knitigbillissue_1','knitting_info_list','','txt_bill_date,".$date."','$(\'#bill_issue_table tr:not(:first)\').remove();')",1);
                    ?> 
                    </td>
                </tr>  
                <tr>
                    <td colspan="13" id="list_view" align="center"></td>
                </tr>
            </tfoot>                                                            
        </table>
        </fieldset> 
        </form>
        <br>
        <div id="knitting_info_list"></div>                           
    </div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>
