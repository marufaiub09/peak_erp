<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$batch_arr = return_library_array("select id, batch_no from pro_batch_create_mst","id","batch_no");

//====================Location ACTION========
if ($action=="load_drop_down_location")
{
	$data=explode("_",$data);
	echo create_drop_down( "cbo_location", 150, "select id,location_name from lib_location where company_id='$data[0]' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

if ($action=="load_drop_down_store")
{
	echo create_drop_down( "cbo_store_name", 160, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=2 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "--Select store--",0,"");
	exit();
}

if($action=="load_drop_down_dyeing_com")
{
	$data = explode("_",$data);
	$company_id=$data[1];

	if($data[0]==1)
	{
		echo create_drop_down( "cbo_dyeing_company", 160, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "--Select Dyeing Company--", "$company_id", "","" );
	}
	else if($data[0]==3)
	{
		echo create_drop_down( "cbo_dyeing_company", 160, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type=21 and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "--Select Dyeing Company--", 1, "" );
	}
	else
	{
		echo create_drop_down( "cbo_dyeing_company", 160, $blank_array,"",1, "--Select Dyeing Company--", 1, "" );
	}
	exit();
}

//====================SYSTEM ID POPUP========
if ($action=="systemId_popup")
{
	echo load_html_head_contents("System ID Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id)
		{
			$('#hidden_sys_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
<div align="center" style="width:840px;">
    <form name="searchsystemidfrm"  id="searchsystemidfrm">
        <fieldset style="width:830px;">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" width="800" border="1" rules="all" class="rpt_table">
                <thead>
                    <th>Production Date Range</th>
                    <th>Search By</th>
                    <th id="search_by_td_up">Please Enter System Id</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" name="hidden_sys_id" id="hidden_sys_id" class="text_boxes" value="">
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px;">To<input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px;">
                    </td>
                    <td>
						<?php
							$search_by_arr=array(1=>"System ID",2=>"Challan No.");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td id="search_by_td">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td>
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value, 'create_finish_search_list_view', 'search_div', 'finish_fabric_receive_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </table>
            <table width="100%" style="margin-top:5px;">
                <tr>
                    <td colspan="5">
                        <div style="width:100%; margin-top:10px; margin-left:3px;" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table>
    	</fieldset>
    </form>
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_finish_search_list_view")
{
	$data = explode("_",$data);
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$start_date =$data[2];
	$end_date =$data[3];
	$company_id =$data[4];

	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and receive_date between '".change_date_format(trim($start_date),"yyyy-mm-dd","-")."' and '".change_date_format(trim($end_date),"yyyy-mm-dd","-")."'";
		}
		else
		{
			$date_cond="and receive_date between '".change_date_format(trim($start_date),'','',1)."' and '".change_date_format(trim($end_date),'','',1)."'";
		}
	}
	else
	{
		$date_cond="";
	}
	
	if(trim($data[0])!="")
	{
		if($search_by==1)
			$search_field_cond="and recv_number like '$search_string'";
		else
			$search_field_cond="and challan_no like '$search_string'";
	}
	else
	{
		$search_field_cond="";
	}
	
	if($db_type==0) $year_field="YEAR(insert_date)"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY')";
	else $year_field="";//defined Later
	
	$sql = "select id, recv_number, recv_number_prefix_num, knitting_source, knitting_company, receive_date, challan_no, $year_field as year from inv_receive_master where entry_form=7 and status_active=1 and is_deleted=0 and company_id=$company_id $search_field_cond $date_cond"; 
	//echo $sql;die;
	$result = sql_select($sql);

	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$supllier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table">
        <thead>
            <th width="50">SL</th>
            <th width="60">Year</th>
            <th width="80">Received ID</th>
            <th width="140">Dyeing Source</th>
            <th width="120">Dyeing Company</th>
            <th width="100">Production date</th>
            <th width="100">Production Qnty</th>
            <th>Challan No</th>
        </thead>
	</table>
	<div style="width:820px; max-height:230px; overflow-y:scroll" id="list_container_batch" align="left">	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table" id="tbl_list_search">  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	 
					
                if($row[csf('knitting_source')]==1)
					$dye_comp=$company_arr[$row[csf('knitting_company')]]; 
				else
					$dye_comp=$supllier_arr[$row[csf('knitting_company')]];
				
				$recv_qnty=return_field_value("sum(receive_qnty)","pro_finish_fabric_rcv_dtls","mst_id='".$row[csf('id')]."' and status_active=1 and is_deleted=0");
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>);"> 
                    <td width="50"><?php echo $i; ?></td>
                    <td width="60" align="center"><p><?php echo $row[csf('year')]; ?></p></td>
                    <td width="80"><p>&nbsp;<?php echo $row[csf('recv_number_prefix_num')]; ?></p></td>
                    <td width="140"><p><?php echo $knitting_source[$row[csf('knitting_source')]]; ?></p></td>
                    <td width="120"><p><?php echo $dye_comp; ?></p></td>
                    <td width="100" align="center"><?php echo change_date_format($row[csf('receive_date')]); ?></td>
                    <td width="100" align="right"><?php echo number_format($recv_qnty,2); ?>&nbsp;</td>
                    <td><p>&nbsp;<?php echo $row[csf('challan_no')]; ?></p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
    </div>
<?php	
exit();
}

if($action=='populate_data_from_finish_fabric')
{
	$data_array=sql_select("select id, recv_number, company_id, receive_basis, store_id, location_id, knitting_source, knitting_company, receive_date, challan_no, store_id from inv_receive_master where id='$data'");
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('txt_system_id').value 				= '".$row[csf("recv_number")]."';\n";
		echo "document.getElementById('cbo_production_basis').value 		= '".$row[csf("receive_basis")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "$('#cbo_company_id').attr('disabled','true')".";\n";
		echo "set_production_basis();\n";
		//echo "load_drop_down('requires/finish_fabric_receive_controller', $row[company_id], 'load_drop_down_location', 'location_td' );\n";
		
		echo "document.getElementById('txt_production_date').value 			= '".change_date_format($row[csf("receive_date")])."';\n";
		echo "document.getElementById('cbo_dyeing_source').value 			= '".$row[csf("knitting_source")]."';\n";
		
		echo "load_drop_down('requires/finish_fabric_receive_controller', ".$row[csf("knitting_source")]."+'_'+".$row[csf("company_id")].", 'load_drop_down_dyeing_com', 'dyeingcom_td' );\n";
		
		echo "document.getElementById('cbo_dyeing_company').value 			= '".$row[csf("knitting_company")]."';\n";
		echo "document.getElementById('txt_challan_no').value 				= '".$row[csf("challan_no")]."';\n";
		echo "document.getElementById('cbo_location').value 				= '".$row[csf("location_id")]."';\n";
		echo "document.getElementById('cbo_store_name').value 				= '".$row[csf("store_id")]."';\n";
		echo "document.getElementById('update_id').value 					= '".$row[csf("id")]."';\n";
		
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_finish_production',1);\n";  
		exit();
	}
}

if ($action=="batch_number_popup")
{
	echo load_html_head_contents("Batch Number Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id,booking_no,job_no)
		{
			$('#hidden_batch_id').val(id);
			$('#booking_no').val(booking_no);
			$('#job_no').val(job_no);
			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
<div align="center" style="width:800px;">
    <form name="searchbatchnofrm"  id="searchbatchnofrm">
        <fieldset style="width:790px; margin-left:10px">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" border="1" rules="all" width="770" class="rpt_table">
                <thead>
                    <th width="240">Batch Date Range</th>
                    <th width="170">Search By</th>
                    <th id="search_by_td_up" width="200">Please Enter Batch No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" value="">
                        <input type="hidden" name="job_no" id="job_no" class="text_boxes" value="">
                        <input type="hidden" name="booking_no" id="booking_no" class="text_boxes" value="">
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px;">To
                        <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px;">
                    </td>
                    <td>
						<?php
							$search_by_arr=array(0=>"Batch No",1=>"Fabric Booking no.",2=>"Color",3=>"Order No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td align="center" id="search_by_td" width="140px">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value, 'create_batch_search_list_view', 'search_div', 'finish_fabric_receive_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </table>
            <div style="width:100%; margin-top:5px" id="search_div" align="left"></div>
        </fieldset>
    </form>
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_batch_search_list_view")
{
	$data = explode("_",$data);
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$start_date =$data[2];
	$end_date =$data[3];
	$company_id =$data[4];

	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and batch_date between '".change_date_format(trim($start_date), "yyyy-mm-dd", "-")."' and '".change_date_format(trim($end_date), "yyyy-mm-dd", "-")."'";
		}
		else
		{
			$date_cond="and batch_date between '".change_date_format(trim($start_date),'','',1)."' and '".change_date_format(trim($end_date),'','',1)."'";
		}
	}
	else
	{
		$date_cond="";
	}
	
	if(trim($data[0])!="")
	{
		if($search_by==0)
			$search_field_cond="and batch_no like '$search_string'";
		else if($search_by==1)
			$search_field_cond="and booking_no like '$search_string'";
		else
			$search_field_cond="and color_id in(select id from lib_color where color_name like '$search_string')";
	}
	else
	{
		$search_field_cond="";
	}
	
	$po_arr=array();
	$po_data=sql_select("select id, po_number, job_no_mst from wo_po_break_down");	
	foreach($po_data as $row)
	{
		$po_arr[$row[csf('id')]]['po_no']=$row[csf('po_number')];
		$po_arr[$row[csf('id')]]['job_no']=$row[csf('job_no_mst')];
	}
	
	if($db_type==0)
	{
		$order_id_arr=return_library_array( "select mst_id, group_concat(po_id) as po_id from pro_batch_create_dtls where status_active=1 and is_deleted=0 group by mst_id",'mst_id','order_id');
	}
	else
	{
		$order_id_arr=return_library_array( "select mst_id, LISTAGG(cast(po_id as VARCHAR2(4000)), ',') WITHIN GROUP (ORDER BY po_id) as po_id from pro_batch_create_dtls where status_active=1 and is_deleted=0 group by mst_id",'mst_id','po_id');
	}
	
	$sql = "select id, batch_no, extention_no, batch_date, batch_weight, booking_no, color_id, batch_against, booking_without_order, re_dyeing_from from pro_batch_create_mst where entry_form=0 and batch_for=1 and batch_against<>4 and company_id=$company_id and status_active=1 and is_deleted=0 $search_field_cond $date_cond"; 
	//echo $sql;die; 
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="780" class="rpt_table" >
        <thead>
            <th width="40">SL</th>
            <th width="90">Batch No</th>
            <th width="80">Extention No</th>
            <th width="80">Batch Date</th>
            <th width="80">Batch Qnty</th>
            <th width="115">Booking No</th>
            <th width="110">Color</th>
            <th>Po No</th>
        </thead>
    </table>
    <div style="width:780px; overflow-y:scroll; max-height:250px;" id="buyer_list_view" align="center">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="760" class="rpt_table" id="tbl_list_search" >
        <?php
            $i=1;
            $nameArray=sql_select( $sql );
            foreach ($nameArray as $selectResult)
            {
                $po_no=''; $job_array=array();
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$order_id=array_unique(explode(",",$order_id_arr[$selectResult[csf('id')]]));
				foreach($order_id as $value)
				{
					if($po_no=='') $po_no=$po_arr[$value]['po_no']; else $po_no.=",".$po_arr[$value]['po_no'];
					$job_no=$po_arr[$value]['job_no'];
					if(!in_array($job_no,$job_array))
					{
						$job_array[]=$job_no;
					}
				}
				$job_no=implode(",",$job_array);
				?>
				<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>,'<?php echo $selectResult[csf('booking_no')]; ?>','<?php echo $job_no; ?>')"> 
					<td width="40" align="center"><?php echo $i; ?></td>	
					<td width="90"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
					<td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?>&nbsp;</p></td>
					<td width="80" align="center"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
					<td width="80" align="right"><?php echo $selectResult[csf('batch_weight')]; ?>&nbsp;</td> 
					<td width="115"><p><?php echo $selectResult[csf('booking_no')]; ?>&nbsp;</p></td>
					<td width="110"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
					<td><p><?php echo $po_no; ?>&nbsp;</p></td>	
				</tr>
				<?php
				$i++;
                /*$sql_re= "select id, batch_no, extention_no, batch_date, batch_weight, booking_no, color_id, batch_against, booking_without_order, re_dyeing_from from pro_batch_create_mst where batch_for=1 and entry_form=0 and batch_against<>4 and status_active=1 and is_deleted=0 and id='".$selectResult[csf('re_dyeing_from')]."'";*/
            }
        ?>
        </table>
    </div>
<?php
exit();
}

if($action=='populate_data_from_batch')
{
	$data_array=sql_select("select batch_no, batch_weight, color_id, booking_without_order from pro_batch_create_mst where id='$data'");
	foreach ($data_array as $row)
	{ 
		$recv_qnty=return_field_value("sum(b.receive_qnty)","inv_receive_master a, pro_finish_fabric_rcv_dtls b"," a.id=b.mst_id and a.entry_form=7 and b.batch_id=$data and b.status_active=1 and b.is_deleted=0");
		$batch_qnty=return_field_value("sum(batch_qnty)","pro_batch_create_dtls","mst_id=$data and status_active=1 and is_deleted=0");
		
		$yet_to_recv_qnty=$batch_qnty-$recv_qnty;
		
		echo "document.getElementById('txt_batch_no').value 				= '".$row[csf("batch_no")]."';\n";
		echo "document.getElementById('txt_batch_id').value 				= '".$data."';\n";
		echo "document.getElementById('txt_color').value 					= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('txt_batch_qnty').value 				= '".$batch_qnty."';\n";
		echo "document.getElementById('txt_total_received').value 			= '".$recv_qnty."';\n";
		echo "document.getElementById('txt_yet_receive').value 				= '".$yet_to_recv_qnty."';\n";
		echo "document.getElementById('batch_booking_without_order').value 	= '".$row[csf("booking_without_order")]."';\n";
		
		if($row[csf("booking_without_order")]==1)
		{
			echo "$('#txt_production_qty').removeAttr('readonly','readonly');\n";
			echo "$('#txt_production_qty').removeAttr('onClick','onClick');\n";	
			echo "$('#txt_production_qty').removeAttr('placeholder','placeholder');\n";		
		}
		else
		{
			echo "$('#txt_production_qty').attr('readonly','readonly');\n";
			echo "$('#txt_production_qty').attr('onClick','openmypage_po();');\n";	
			echo "$('#txt_production_qty').attr('placeholder','Single Click to Search');\n";	
		}
		
		exit();
	}
}

if($action=='show_fabric_desc_listview')
{
	$composition_arr=array();
 	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from  lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}
	
	$prod_array=array();
	$prodData=sql_select("select id, item_description, product_name_details, detarmination_id, gsm, dia_width from product_details_master where item_category_id=13");
	foreach($prodData as $row)
	{
		$prod_array[$row[csf('id')]]['desc']=$row[csf('product_name_details')];
		$prod_array[$row[csf('id')]]['comp']=$row[csf('item_description')];
		$prod_array[$row[csf('id')]]['dt_id']=$row[csf('detarmination_id')];
		$prod_array[$row[csf('id')]]['gsm']=$row[csf('gsm')];
		$prod_array[$row[csf('id')]]['dia']=$row[csf('dia_width')];
	}
	
	$data_array=sql_select("select prod_id, sum(batch_qnty) as qnty from pro_batch_create_dtls where mst_id='$data' and status_active=1 and is_deleted=0 group by prod_id");
	?>
    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="350">
        <thead>
            <th width="30">SL</th>
            <th width="230">Fabric Description</th>
            <th>Qnty</th>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach($data_array as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$determination_id=$prod_array[$row[csf('prod_id')]]['dt_id'];
				$gsm=$prod_array[$row[csf('prod_id')]]['gsm'];
				$dia=$prod_array[$row[csf('prod_id')]]['dia'];
				
				if($determination_id==0 || $determination_id=="")
				{
					$comps=$prod_array[$row[csf('prod_id')]]['comp'];
				}
				else
				{
					$comps=$composition_arr[$determination_id];
				}
				
				$prod_id=$row[csf('prod_id')];
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" onClick='set_form_data("<?php echo $comps."**".$determination_id."**".$gsm."**".$dia."**".$prod_id; ?>")' style="cursor:pointer">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $prod_array[$row[csf('prod_id')]]['desc']; ?></td>
                    <td align="right"><?php echo number_format($row[csf('qnty')],2,'.',''); ?></td>
                </tr>
            <?php 
            $i++; 
            } 
            ?>
        </tbody>
    </table>
<?php
exit();
}

if ($action=="fabricDescription_popup")
{
	echo load_html_head_contents("Fabric Description Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
		
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
		
		function js_set_value(comp,gsm,detarmination_id)
		{
			$('#hidden_desc_no').val(comp);
			$('#hidden_gsm').val(gsm);
			//$('#hidden_dia_width').val(dia_width);
			$('#fabric_desc_id').val(detarmination_id);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:520px;margin-left:10px">
            <input type="hidden" name="hidden_desc_no" id="hidden_desc_no" class="text_boxes" value="">  
            <input type="hidden" name="hidden_gsm" id="hidden_gsm" class="text_boxes" value=""> 
            <input type="hidden" name="hidden_dia_width" id="hidden_dia_width" class="text_boxes" value="">  
            <input type="hidden" name="fabric_desc_id" id="fabric_desc_id" class="text_boxes" value="">  
            
            <div style="margin-left:10px; margin-top:10px">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="480">
                    <thead>
                        <th width="40">SL</th>
                        <th width="120">Construction</th>
                        <th>Composition</th>
                        <th width="100">GSM/Weight</th>
                    </thead>
                </table>
                <div style="width:500px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="480" id="tbl_list_search">  
                        <?php 
						$composition_arr=array();
						$compositionData=sql_select("select mst_id, copmposition_id, percent from lib_yarn_count_determina_dtls where status_active=1 and is_deleted=0");
						foreach( $compositionData as $row )
						{
							$composition_arr[$row[csf('mst_id')]].=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
						}
						
                        $i=1;
						$data_array=sql_select("select id,construction,fab_nature_id,gsm_weight from lib_yarn_count_determina_mst where fab_nature_id=2 and status_active=1 and is_deleted=0"); 
                        foreach($data_array as $row)
                        {  
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
							
							/*$comp='';$construction=$row[csf('construction')]; 
                            $determ_sql=sql_select("select copmposition_id, percent from lib_yarn_count_determina_dtls where mst_id=".$row[csf('id')]." and status_active=1 and is_deleted=0");
                            foreach( $determ_sql as $d_row )
                            {
                                $comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
                            }*/
							$comp=$composition_arr[$row[csf('id')]];
							$cons_comp=$row[csf('construction')].", ".$comp;
                         ?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value('<?php echo $cons_comp; ?>','<?php echo $row[csf('gsm_weight')]; ?>','<?php echo $row[csf('id')]; ?>')" style="cursor:pointer" >
                                <td width="40"><?php echo $i; ?></td>
                                <td width="120"><p><?php echo $row[csf('construction')]; ?></p></td>
                                <td><p><?php echo $comp; ?></p></td>
                                <td width="100"><?php echo $row[csf('gsm_weight')]; ?></td>
                            </tr>
                        <?php 
                        $i++; 
                        } 
                        ?>
                    </table>
                </div> 
            </div>
		</fieldset>
	</form>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if ($action=="po_popup")
{
	echo load_html_head_contents("PO Info", "../../", 1, 1,'','','');
	
	extract($_REQUEST);
	$data=explode("_",$data);
	$po_id=$data[0]; $type=$data[1];
	if($type==1) 
	{
		$dtls_id=$data[2]; 
		$roll_maintained=$data[3]; 
		$save_data=$data[4]; 
		$prev_distribution_method=$data[5]; 
		$production_basis=$data[6]; 
	}
?>
	<script>
		var production_basis=<?php echo $production_basis; ?>;
		var roll_maintained=<?php echo $roll_maintained; ?>;
		
		function distribute_qnty(str)
		{
			if(str==1 && roll_maintained==0)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_finish_qnty=$('#txt_prop_finish_qnty').val()*1;
				var tblRow = $("#tbl_list_search tr").length;
				var len=totalFinish=0;
				
				$("#tbl_list_search").find('tr').each(function()
				{
					len=len+1;
					var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
					var perc=(po_qnty/tot_po_qnty)*100;
					
					var finish_qnty=(perc*txt_prop_finish_qnty)/100;
					totalFinish = totalFinish*1+finish_qnty*1;
					totalFinish = totalFinish.toFixed(2);
											
					if(tblRow==len)
					{
						var balance = txt_prop_finish_qnty-totalFinish;
						if(balance!=0) totalFinish=totalFinish*1+(balance*1);
					}
					
					$(this).find('input[name="txtfinishQnty[]"]').val(finish_qnty.toFixed(2));
					
				});
			}
			else
			{
				$('#txt_prop_finish_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					$(this).find('input[name="txtfinishQnty[]"]').val('');
				});
			}
		}

		var selected_id = new Array();

		 function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i,1 );
			}
		}

		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_po_row_id').value;
			if(old!="")
			{
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{
					js_set_value( old[i],0 )
				}
			}
		}

		function js_set_value( str )
		{

			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );

			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );

			$('#po_id').val( id );
		}

		function show_finish_fabric_recv()
		{
			var po_id=$('#po_id').val();
			show_list_view ( po_id+'_'+'1'+'_'+'<?php echo $dtls_id; ?>'+'_'+'<?php echo $roll_maintained; ?>'+'_'+'<?php echo $save_data; ?>'+'_'+'<?php echo $prev_distribution_method; ?>'+'_'+'<?php echo $production_basis; ?>', 'po_popup', 'search_div', 'finish_fabric_receive_controller', '');
		}

		function hidden_field_reset()
		{
			$('#po_id').val('');
			$('#save_string').val( '' );
			$('#tot_finish_qnty').val( '' );
			selected_id = new Array();
		}

		function fnc_close()
		{
			var save_string='';	 var tot_finish_qnty='';
			var po_id_array = new Array(); var buyer_id_array = new Array(); var buyer_name_array = new Array();

			$("#tbl_list_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtfinishQnty=$(this).find('input[name="txtfinishQnty[]"]').val();
				var txtRoll=$(this).find('input[name="txtRoll[]"]').val();
				var buyerId=$(this).find('input[name="buyerId[]"]').val();
				var buyerName=$(this).find('input[name="buyerName[]"]').val();

				tot_finish_qnty=tot_finish_qnty*1+txtfinishQnty*1;
				
				if(roll_maintained==0)
				{
					txtRoll=0;
				}
				
				if(txtfinishQnty*1>0)
				{
					if(save_string=="")
					{
						save_string=txtPoId+"**"+txtfinishQnty+"**"+txtRoll;
					}
					else
					{
						save_string+=","+txtPoId+"**"+txtfinishQnty+"**"+txtRoll;
					}
					
					if( jQuery.inArray( txtPoId, po_id_array) == -1 )
					{
						po_id_array.push(txtPoId);
					}
					
					if( jQuery.inArray( buyerId, buyer_id_array) == -1 )
					{
						buyer_id_array.push(buyerId);
						buyer_name_array.push(buyerName);
					}
				}
			});
			
			$('#save_string').val( save_string );
			$('#tot_finish_qnty').val(tot_finish_qnty);
			$('#all_po_id').val( po_id_array );
			$('#buyer_id').val( buyer_id_array );
			$('#buyer_name').val( buyer_name_array );
			$('#distribution_method').val( $('#cbo_distribiution_method').val() );

			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
	<?php
	if($type!=1)
	{
	?>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:10px">
        	<input type="hidden" name="save_string" id="save_string" class="text_boxes" value="">
            <input type="hidden" name="tot_finish_qnty" id="tot_finish_qnty" class="text_boxes" value="">
            <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
            <input type="hidden" name="buyer_id" id="buyer_id" class="text_boxes" value="">
            <input type="hidden" name="buyer_name" id="buyer_name" class="text_boxes" value="">
            <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
	<?php
	}
	
	if($production_basis==4 && $type!=1)
	{
	?>
		<table cellpadding="0" cellspacing="0" width="620" class="rpt_table">
			<thead>
				<th>Buyer</th>
				<th>Search By</th>
				<th>Search</th>
				<th>
					<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
					<input type="hidden" name="po_id" id="po_id" value="">
				</th>
			</thead>
			<tr class="general">
				<td align="center">
					<?php
						echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "",$data[0] );
					?>
				</td>
				<td align="center">
					<?php
						$search_by_arr=array(1=>"PO No",2=>"Job No");
						echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
					?>
				</td>
				<td align="center">
					<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
				</td>
				<td align="center">
					<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<?php echo $cbo_company_id; ?>+'_'+document.getElementById('cbo_buyer_name').value+'_'+'<?php echo $all_po_id; ?>', 'create_po_search_list_view', 'search_div', 'finish_fabric_receive_controller', 'setFilterGrid(\'tbl_list_search\',-1);hidden_field_reset();');set_all();" style="width:100px;" />
				</td>
			</tr>
		</table>
		<div id="search_div" style="margin-top:10px">
        	<?php
			if($save_data!="")
			{
			?>
            	<div style="width:600px; margin-top:10px" align="center">
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
                        <thead>
                            <th>Total Finish Qnty</th>
                            <th>Distribution Method</th>
                        </thead>
                        <tr class="general">
                            <td><input type="text" name="txt_prop_finish_qnty" id="txt_prop_finish_qnty" class="text_boxes_numeric" value="<?php echo $txt_production_qty; ?>" style="width:120px" onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
                            <td>
                                <?php
                                    $distribiution_method=array(1=>"Proportionately",2=>"Manually");
                                    echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0, "",$prev_distribution_method, "distribute_qnty(this.value);",0 );
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
				<div style="margin-left:10px; margin-top:10px">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
						<thead>
							<th width="130">PO No</th>
                            <th width="90">Shipment Date</th>
                            <th width="100">PO Qnty</th>
                            <th width="120">Finish Qnty</th>
							<?php
							if($roll_maintained==1)
							{
							?>
								<th>Roll</th>
							<?php
							}
							?>
						</thead>
					</table>
					<div style="width:600px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
						<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">  
							<?php 
							$i=1; $tot_po_qnty=0; $po_array=array();  

							$explSaveData = explode(",",$save_data); 	
							for($z=0;$z<count($explSaveData);$z++)
							{
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
									
								$po_wise_data = explode("**",$explSaveData[$z]);
								$order_id=$po_wise_data[0];
								$finish_qnty=$po_wise_data[1];
								$roll_no=$po_wise_data[2];
								
								$po_data=sql_select("select a.buyer_name, b.po_number, b.pub_shipment_date, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
								
								if(!(in_array($order_id,$po_array)))
								{
									$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$po_array[]=$order_id;
								}

								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
                                    <td width="130">
                                        <p><?php echo $po_data[0][csf('po_number')]; ?></p>
                                        <input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
                                        <input type="hidden" name="buyerId[]" id="buyerId_<?php echo $i; ?>" class="text_boxes" value="<?php echo $po_data[0][csf('buyer_name')]; ?>">
                                        <input type="hidden" name="buyerName[]" id="buyerName_<?php echo $i; ?>" class="text_boxes" value="<?php echo $buyer_arr[$po_data[0][csf('buyer_name')]]; ?>">
                                    </td>
                                    <td width="90" align="center"><?php echo change_date_format($po_data[0][csf('pub_shipment_date')]); ?></td>
                                    <td width="100" align="right">
                                        <?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>
                                        <input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
                                    </td>
                                    <td width="120" align="center">
                                    	<input type="text" name="txtfinishQnty[]" id="txtfinishQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $finish_qnty; ?>">
                                    </td>
                                    <?php
                                    if($roll_maintained==1)
                                    {
                                    ?>
                                        <td align="center">
                                            <input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($roll_no!=0) echo $roll_no; ?>" disabled="disabled"/>
                                        </td>
                                    <?php
                                    }
                                    ?>
                                </tr>
							<?php 
							$i++;
							}
							?>
							<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
						</table>
					</div>
					<table width="620">
						 <tr>
							<td align="center" >
								<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
							</td>
						</tr>
					</table>
				</div>
			<?php
			}
			?> 
        </div>
	<?php
	}
	else
	{
	?>
		<div style="width:600px; margin-top:10px" align="center">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
				<thead>
					<th>Total Finish Qnty</th>
					<th>Distribution Method</th>
				</thead>
				<tr class="general">
					<td><input type="text" name="txt_prop_finish_qnty" id="txt_prop_finish_qnty" class="text_boxes_numeric" value="<?php echo $txt_production_qty; ?>" style="width:120px" onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
					<td>
						<?php
							$distribiution_method=array(1=>"Proportionately",2=>"Manually");
							echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0, "",$prev_distribution_method, "distribute_qnty(this.value);",0 );
						?>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:10px; margin-top:10px">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
				<thead>
                	<th width="130">PO No</th>
                    <th width="90">Shipment Date</th>
                    <th width="100">PO Qnty</th>
                    <th width="120">Finish Qnty</th>
                    <?php
					if($roll_maintained==1)
					{
					?>
						<th>Roll</th>
                    <?php
					}
					?>
				</thead>
			</table>
			<div style="width:600px; max-height:280px; overflow-y:scroll" id="list_container" align="left">
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">
					<?php
					$i=1; $tot_po_qnty=0; $po_array=array();
					
					if($save_data!="" && $production_basis==4)
					{ 
						$po_id = explode(",",$po_id);
						$explSaveData = explode(",",$save_data); 	
						for($z=0;$z<count($explSaveData);$z++)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$po_wise_data = explode("**",$explSaveData[$z]);
							$order_id=$po_wise_data[0];
							$finish_qnty=$po_wise_data[1];
							$roll_no=$po_wise_data[2];
							
							if(in_array($order_id,$po_id))
							{
								$po_data=sql_select("select a.buyer_name, b.po_number, b.pub_shipment_date, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
									
								if(!(in_array($order_id,$po_array)))
								{
									$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$po_array[]=$order_id;
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="130">
										<p><?php echo $po_data[0][csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
										<input type="hidden" name="buyerId[]" id="buyerId_<?php echo $i; ?>" class="text_boxes" value="<?php echo $po_data[0][csf('buyer_name')]; ?>">
										<input type="hidden" name="buyerName[]" id="buyerName_<?php echo $i; ?>" class="text_boxes" value="<?php echo $buyer_arr[$po_data[0][csf('buyer_name')]]; ?>">
									</td>
                                    <td width="90" align="center"><?php echo change_date_format($po_data[0][csf('pub_shipment_date')]); ?></td>
									<td width="100" align="right">
										<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
									</td>
									<td width="120" align="center">
										<input type="text" name="txtfinishQnty[]" id="txtfinishQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $finish_qnty; ?>">
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($roll_no!=0) echo $roll_no; ?>" disabled="disabled"/>
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++;
							}
						}
						
						if(count($po_array)<1)
						{
							$result=implode(",",$po_id);
						}
						else
						{
							$result=implode(",",array_diff($po_id, $po_array));
						}
						if($result!="")
						{
							if($roll_maintained==1)
							{
								$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, d.roll_no, d.qnty from wo_po_details_master a, wo_po_break_down b left join pro_roll_details d on b.id=d.po_breakdown_id and d.entry_form=3 and d.status_active=1 and d.is_deleted=0 where a.job_no=b.job_no_mst and b.id in ($result)";
							}
							else
							{
								$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($result)";
							}
							
							$nameArray=sql_select($po_sql);
							foreach($nameArray as $row)
							{  
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								if(!(in_array($row[csf('id')],$po_array)))
								{
									$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
									$po_array[]=$row[csf('id')];
								}
							
							 ?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="130">
										<p><?php echo $row[csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
										<input type="hidden" name="buyerId[]" id="buyerId_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('buyer_name')]; ?>">
										<input type="hidden" name="buyerName[]" id="buyerName_<?php echo $i; ?>" class="text_boxes" value="<?php echo $buyer_arr[$row[csf('buyer_name')]]; ?>">
									</td>
                                    <td width="90" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
									<td width="100" align="right">
										<?php echo $row[csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
									</td>
									<td width="120" align="center">
										<input type="text" name="txtfinishQnty[]" id="txtfinishQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php //echo $finish_qnty; ?>">
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($row[csf('roll_no')]!=0) echo $row[csf('roll_no')]; ?>" disabled="disabled"/>
									</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++; 
							} 
						}
					}
					else if($save_data!="" && $production_basis==5)
					{
						$finish_qnty_array=array();
						$explSaveData = explode(",",$save_data); 
						
						if($roll_maintained==1)
						{
							for($i=0;$i<count($explSaveData);$i++)
							{
								$po_wise_data = explode("**",$explSaveData[$i]);
								$order_id=$po_wise_data[0];
								$finish_qnty=$po_wise_data[1];
								$roll_no=$po_wise_data[2];
								
								$finish_qnty_array[$order_id][$roll_no]=$finish_qnty;
							} 		
						}
						else
						{
							for($i=0;$i<count($explSaveData);$i++)
							{
								$po_wise_data = explode("**",$explSaveData[$i]);
								$order_id=$po_wise_data[0];
								$finish_qnty=$po_wise_data[1];

								$finish_qnty_array[$order_id]=$finish_qnty;
							} 		
						}
						
						if($roll_maintained==1)
						{
							$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity, c.roll_no, c.batch_qnty as qnty from wo_po_details_master a, wo_po_break_down b, pro_batch_create_dtls c where a.job_no=b.job_no_mst and b.id=c.po_id and c.mst_id='$txt_batch_id' and c.status_active=1 and c.is_deleted=0";
						}
						else
						{
							$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity, sum(c.batch_qnty) as qnty from wo_po_details_master a, wo_po_break_down b, pro_batch_create_dtls c where a.job_no=b.job_no_mst and b.id=c.po_id and c.mst_id='$txt_batch_id' and c.status_active=1 and c.is_deleted=0 group by b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity";
						}

						$po_array=array(); $tot_po_qnty=0;
						$nameArray=sql_select($po_sql);
						foreach($nameArray as $row)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
							
							$po_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('total_set_qnty')];
							if(!(in_array($row[csf('id')],$po_array)))
							{
								$tot_po_qnty+=$po_qnty_in_pcs;
								$po_array[]=$row[csf('id')];
							}
							
							if($roll_maintained==1)
							{
								$finish_qnty=$finish_qnty_array[$row[csf('id')]][$row[csf('roll_no')]];
							}
							else 
							{
								$finish_qnty=$finish_qnty_array[$row[csf('id')]];
							}
							
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="130">
									<p><?php echo $row[csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
									<input type="hidden" name="buyerId[]" id="buyerId_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('buyer_name')]; ?>">
									<input type="hidden" name="buyerName[]" id="buyerName_<?php echo $i; ?>" class="text_boxes" value="<?php echo $buyer_arr[$row[csf('buyer_name')]];?>">
								</td>
                                <td width="90" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
								<td width="100" align="right">
									<?php echo $po_qnty_in_pcs; ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_qnty_in_pcs; ?>">
								</td>
								<td width="120" align="center">
                                    <input type="text" name="txtfinishQnty[]" id="txtfinishQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $finish_qnty; ?>">
								</td>
								<?php
								if($roll_maintained==1)
								{
								?>
									<td align="center">
										<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($row[csf('roll_no')]!=0) echo $row[csf('roll_no')]; ?>" disabled="disabled"/>
									</td>
								<?php
								}
								?>
							</tr>
						<?php 
						$i++;
						}
					}
					else
					{
						if($type==1)
						{
							if($po_id!="")
							{
								if($roll_maintained==1)
								{
									$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity, d.roll_no, d.qnty from wo_po_details_master a, wo_po_break_down b left join pro_roll_details d on b.id=d.po_breakdown_id and d.entry_form=3 and d.status_active=1 and d.is_deleted=0 where a.job_no=b.job_no_mst and b.id in ($po_id)";
								}
								else
								{
									$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id)";
								}
							}
						}
						else
						{
							if($roll_maintained==1)
							{
								$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity, c.roll_no, c.batch_qnty as qnty from wo_po_details_master a, wo_po_break_down b, pro_batch_create_dtls c where a.job_no=b.job_no_mst and b.id=c.po_id and c.mst_id='$txt_batch_id' and c.status_active=1 and c.is_deleted=0";
							}
							else
							{
								$po_sql="select b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity, sum(c.batch_qnty) as qnty from wo_po_details_master a, wo_po_break_down b, pro_batch_create_dtls c where a.job_no=b.job_no_mst and b.id=c.po_id and c.mst_id='$txt_batch_id' and c.status_active=1 and c.is_deleted=0 group by b.id, a.buyer_name, b.po_number, b.pub_shipment_date, a.total_set_qnty, b.po_quantity";
							}
						}
					
						$po_array=array(); $tot_po_qnty=0;
						$nameArray=sql_select($po_sql);
						foreach($nameArray as $row)
						{
							if ($i%2==0)
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
							
							$po_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('total_set_qnty')];
							if(!(in_array($row[csf('id')],$po_array)))
							{
								$tot_po_qnty+=$po_qnty_in_pcs;
								$po_array[]=$row[csf('id')];
							}

						 ?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="130">
									<p><?php echo $row[csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
									<input type="hidden" name="buyerId[]" id="buyerId_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('buyer_name')]; ?>">
									<input type="hidden" name="buyerName[]" id="buyerName_<?php echo $i; ?>" class="text_boxes" value="<?php echo $buyer_arr[$row[csf('buyer_name')]];?>">
								</td>
                                <td width="90" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
								<td width="100" align="right">
									<?php echo $po_qnty_in_pcs; ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_qnty_in_pcs; ?>">
								</td>
								<td width="120" align="center">
									<input type="text" name="txtfinishQnty[]" id="txtfinishQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="">
								</td>
								<?php
								if($roll_maintained==1)
								{
								?>
									<td align="center">
										<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($row[csf('roll_no')]!=0) echo $row[csf('roll_no')]; ?>" disabled="disabled"/>
									</td>
								<?php
								}
								?>
							</tr>
						<?php
						$i++;
						}
					}
					?>
					<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
				</table>
			</div>
			<table width="620">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
		</div>
	<?php
	}
	if($type!=1)
	{
	?>
		</fieldset>
	</form>
    <?php
	}
	?>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}
if($action=="create_po_search_list_view")
{
	$data = explode("_",$data);

	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];

	if($search_by==1)
		$search_field='b.po_number';
	else
		$search_field='a.job_no';
		
	$company_id =$data[2];
	$buyer_id =$data[3];
	
	$all_po_id=$data[4];
	
	if($all_po_id!="")
		$po_id_cond=" or b.id in($all_po_id)";
	else 
		$po_id_cond="";
	
	$hidden_po_id=explode(",",$all_po_id);

	if($buyer_id==0) $buyer="%%"; else $buyer=$buyer_id; 
	
	$sql = "select a.job_no, a.buyer_name, a.style_ref_no, a.order_uom, b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name like '$buyer' and $search_field like '$search_string' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0"; 
	//echo $sql;die;// $po_id_cond
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="30">SL</th>
                <th width="90">Job No</th>
                <th width="100">Style No</th>
                <th width="100">PO No</th>
                <th width="80">PO Quantity</th>
                <th width="100">Buyer</th>
                <th>Shipment Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:240px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1; $po_row_id='';
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
						
					$roll_used=0;
					
					if(in_array($selectResult[csf('id')],$hidden_po_id)) 
					{
						if($po_row_id=="") $po_row_id=$i; else $po_row_id.=",".$i;
						
						$roll_data_array=sql_select("select roll_no from pro_roll_details where po_breakdown_id=".$selectResult[csf('id')]." and roll_used=1 and entry_form=1 and status_active=1 and is_deleted=0");
						if(count($roll_data_array)>0)
						{
							$roll_used=1;
						}
						else
							$roll_used=0;
					}
							
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i; ?>)"> 
                            <td width="30" align="center">
								<?php echo $i; ?>
                            	<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>
                            </td>	
                            <td width="90"><p><?php echo $selectResult[csf('job_no')]; ?></p></td>
                            <td width="100"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                            <td width="100"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                            <td width="80" align="right"><?php echo $selectResult[csf('po_qnty_in_pcs')]; ?></td> 
                            <td width="100"><p><?php echo $buyer_arr[$selectResult[csf('buyer_name')]]; ?></p></td>
                            <td align="center"><?php echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                        </tr>
                    <?php
                    $i++;
				}
			?>
				<input type="hidden" name="txt_po_row_id" id="txt_po_row_id" value="<?php echo $po_row_id; ?>"/>
            </table>
        </div>
         <table width="620" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                            <input type="button" name="close" onClick="show_finish_fabric_recv();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>           
<?php
exit();
}
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		$finish_recv_num=''; $finish_update_id=''; $flag=1;
		
		if(str_replace("'","",$update_id)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$new_finish_recv_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'FFPE', date("Y",time()), 5, "select recv_number_prefix, recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_id and entry_form='7' and $year_cond=".date('Y',time())." order by id desc", "recv_number_prefix", "recv_number_prefix_num" ));
		 	
			$id=return_next_id( "id", "inv_receive_master", 1 ) ;
					 
			$field_array="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, receive_basis, company_id, receive_date, challan_no, store_id, location_id, knitting_source, knitting_company, inserted_by, insert_date";
			
			$data_array="(".$id.",'".$new_finish_recv_system_id[1]."',".$new_finish_recv_system_id[2].",'".$new_finish_recv_system_id[0]."',7,2,".$cbo_production_basis.",".$cbo_company_id.",".$txt_production_date.",".$txt_challan_no.",".$cbo_store_name.",".$cbo_location.",".$cbo_dyeing_source.",".$cbo_dyeing_company.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into inv_receive_master (".$field_array.") values ".$data_array;die;
			/*$rID=sql_insert("inv_receive_master",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;*/
			
			$finish_recv_num=$new_finish_recv_system_id[0];
			$finish_update_id=$id;
		}
		else
		{
			$field_array_update="receive_basis*receive_date*challan_no*store_id*location_id*knitting_source*knitting_company*updated_by*update_date";
			
			$data_array_update=$cbo_production_basis."*".$txt_production_date."*".$txt_challan_no."*".$cbo_store_name."*".$cbo_location."*".$cbo_dyeing_source."*".$cbo_dyeing_company."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			/*$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($rID) $flag=1; else $flag=0; */
			
			$finish_recv_num=str_replace("'","",$txt_system_id);
			$finish_update_id=str_replace("'","",$update_id);
		}
		
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		$ItemDesc=str_replace("'","",$txt_fabric_desc).", ".str_replace("'","",$txt_gsm).", ".str_replace("'","",$txt_dia_width);
		
		if(str_replace("'","",$cbo_production_basis)==4)
		{
			$order_id=explode(",",str_replace("'","",$all_po_id));
			
			if(count($order_id)>1)
			{
				check_table_status( $_SESSION['menu_id'],0);
				echo "20**0"; 
				die;
			}
			
			$batchData=sql_select("select a.id, a.batch_weight from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.batch_no=$txt_batch_no and a.color_id='$color_id' and b.po_id=$all_po_id and a.status_active=1 and a.is_deleted=0 group by a.id, a.batch_weight");
			
			if(count($batchData)>0)
			{
				$batch_id=$batchData[0][csf('id')];
				$curr_batch_weight=$batchData[0][csf('batch_weight')]+str_replace("'", '',$txt_production_qty);
				$field_array_batch_update="batch_weight*updated_by*update_date";
				$data_array_batch_update=$curr_batch_weight."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				
				/*$rID6=sql_update("pro_batch_create_mst",$field_array_batch_update,$data_array_batch_update,"id",$batch_id,0);
				if($flag==1) 
				{
					if($rID6) $flag=1; else $flag=0;
				} */
			}
			else
			{
				if(is_duplicate_field( "batch_no", "pro_batch_create_mst", "batch_no=$txt_batch_no" )==1)
				{
					check_table_status( $_SESSION['menu_id'],0);
					echo "11**0"; 
					die;			
				}
				
				$batch_id=return_next_id( "id", "pro_batch_create_mst", 1 ) ;
						 
				$field_array_batch="id, batch_no, entry_form, batch_date, company_id, color_id, batch_weight, inserted_by, insert_date";
				
				$data_array_batch="(".$batch_id.",".$txt_batch_no.",7,".$txt_production_date.",".$cbo_company_id.",'".$color_id."',".$txt_production_qty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				//echo "insert into pro_batch_create_mst (".$field_array_batch.") values ".$data_array_batch;die;
				/*$rID6=sql_insert("pro_batch_create_mst",$field_array_batch,$data_array_batch,0);
				if($flag==1) 
				{
					if($rID6) $flag=1; else $flag=0;
				}*/ 
			}
		}
		else
		{
			$batch_id=str_replace("'","",$txt_batch_id);
		}
		
		if(str_replace("'","",$fabric_desc_id)=="") $fabric_desc_id=0;
			
		if(str_replace("'","",$fabric_desc_id)==0)
		{
			$fabric_description=trim(str_replace("'","",$txt_fabric_desc));
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=2 and detarmination_id=$fabric_desc_id and item_description='$fabric_description' and gsm=$txt_gsm and dia_width=$txt_dia_width and color='$color_id' and status_active=1 and is_deleted=0");
		}
		else
		{
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=2 and detarmination_id=$fabric_desc_id and gsm=$txt_gsm and dia_width=$txt_dia_width and color='$color_id' and status_active=1 and is_deleted=0");
		}
		
		if(count($row_prod)>0)
		{
			$prod_id=$row_prod[0][csf('id')];
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock_qnty=$row_prod[0][csf('current_stock')];
	
				$curr_stock_qnty=$stock_qnty+str_replace("'", '',$txt_production_qty);
				$avg_rate_per_unit=0;$stock_value=0;
	
				$field_array_prod_update="store_id*avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*color*updated_by*update_date";
				
				$data_array_prod_update=$cbo_store_name."*".$avg_rate_per_unit."*".$txt_production_qty."*".$curr_stock_qnty."*".$stock_value."*".$color_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				
				/*$rID2=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
				if($flag==1) 
				{
					if($rID2) $flag=1; else $flag=0; 
				} */
			}
		}
		else
		{
			$prod_id=return_next_id( "id", "product_details_master", 1 ) ;
			
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock_qnty=$txt_production_qty;
				$last_purchased_qnty=$txt_production_qty;
			}
			else
			{
				$stock_qnty=0;
				$last_purchased_qnty=0;
			}
			
			$avg_rate_per_unit=0; $stock_value=0;
			$prod_name_dtls=trim(str_replace("'","",$txt_fabric_desc)).", ".trim(str_replace("'","",$txt_gsm)).", ".trim(str_replace("'","",$txt_dia_width));
			$field_array_prod="id, company_id, store_id, item_category_id, detarmination_id, item_description, product_name_details, unit_of_measure, avg_rate_per_unit, last_purchased_qnty, current_stock, stock_value, color, gsm, dia_width, inserted_by, insert_date";
			
			$data_array_prod="(".$prod_id.",".$cbo_company_id.",".$cbo_store_name.",2,".$fabric_desc_id.",".$txt_fabric_desc.",'".$prod_name_dtls."',0,".$avg_rate_per_unit.",".$last_purchased_qnty.",".$stock_qnty.",".$stock_value.",".$color_id.",".$txt_gsm.",".$txt_dia_width.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			/*$rID2=sql_insert("product_details_master",$field_array_prod,$data_array_prod,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} */
		}
		
		if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$id_trans=return_next_id( "id", "inv_transaction", 1 ) ;
			
			$order_rate=0; $order_amount=0; $cons_rate=0; $cons_amount=0;
			
			$field_array_trans="id, mst_id, receive_basis, pi_wo_batch_no, company_id, prod_id, item_category, transaction_type, transaction_date, store_id, order_uom, order_qnty, order_rate, order_amount, cons_uom, cons_quantity, cons_reject_qnty, cons_rate, cons_amount, balance_qnty, balance_amount, machine_id, rack, self, inserted_by, insert_date";
			
			$data_array_trans="(".$id_trans.",".$finish_update_id.",".$cbo_production_basis.",".$batch_id.",".$cbo_company_id.",".$prod_id.",2,1,".$txt_production_date.",".$cbo_store_name.",12,".$txt_production_qty.",".$order_rate.",".$order_amount.",12,".$txt_production_qty.",".$txt_reject_qty.",".$cons_rate.",".$cons_amount.",".$txt_production_qty.",".$cons_amount.",".$cbo_machine_name.",".$txt_rack.",".$txt_shelf.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			/*$rID3=sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} */
		}
		else
		{
			$id_trans=0;
		}
		
		$id_dtls=return_next_id( "id", "pro_finish_fabric_rcv_dtls", 1 ) ;
		
		$rate=0; $amount=0;
		
		$field_array_dtls="id, mst_id, trans_id, prod_id, batch_id, body_part_id, fabric_description_id, gsm, width, color_id, receive_qnty, reject_qty, no_of_roll, order_id, buyer_id, machine_no_id, rack_no, shelf_no, inserted_by, insert_date";
		 
		$data_array_dtls="(".$id_dtls.",".$finish_update_id.",".$id_trans.",".$prod_id.",".$batch_id.",".$cbo_body_part.",".$fabric_desc_id.",".$txt_gsm.",".$txt_dia_width.",".$color_id.",".$txt_production_qty.",".$txt_reject_qty.",".$txt_no_of_roll.",".$all_po_id.",".$buyer_id.",".$cbo_machine_name.",".$txt_rack.",".$txt_shelf.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
		/*$rID4=sql_insert("pro_finish_fabric_rcv_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1) 
		{
			if($rID4) $flag=1; else $flag=0; 
		} */
		
		$field_array_batch_dtls="id, mst_id, po_id, item_description, roll_no, batch_qnty, dtls_id, inserted_by, insert_date";
		$id_dtls_batch = return_next_id( "id", "pro_batch_create_dtls", 1 ); 
				
		$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, inserted_by, insert_date";
		
		$save_string=explode(",",str_replace("'","",$save_data));
		
		$po_array=array();
		$id_roll = return_next_id( "id", "pro_roll_details", 1 );
		for($i=0;$i<count($save_string);$i++)
		{
			$order_dtls=explode("**",$save_string[$i]);
			
			$order_id=$order_dtls[0];
			$order_qnty_roll_wise=$order_dtls[1];
			$roll_no=$order_dtls[2];
			
			if($data_array_roll!="") $data_array_roll.=",";
			
			$data_array_roll.="(".$id_roll.",".$finish_update_id.",".$id_dtls.",'".$order_id."',4,'".$order_qnty_roll_wise."','".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_roll = $id_roll+1;
			
			if(array_key_exists($order_id,$po_array))
			{
				$po_array[$order_id]+=$order_qnty_roll_wise;
			}
			else
			{
				$po_array[$order_id]=$order_qnty_roll_wise;
			}
			
			if(str_replace("'","",$cbo_production_basis)==4)
			{
				if($data_array_batch_dtls!="") $data_array_batch_dtls.=",";
				$data_array_batch_dtls .="(".$id_dtls_batch.",'".$batch_id."',".$order_id.",'".$ItemDesc."','".$roll_no."',".$order_qnty_roll_wise.",".$id_dtls.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id_dtls_batch = $id_dtls_batch+1;
				
				$id_roll = $id_roll+1;
				
				if($data_array_roll_for_batch!="") $data_array_roll_for_batch.=",";
				$data_array_roll_for_batch.="(".$id_roll.",".$batch_id.",".$id_dtls_batch.",".$order_id.",2,".$order_qnty_roll_wise.",'".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			}
		}
		
		$field_array_proportionate="id, trans_id, trans_type, entry_form, dtls_id, po_breakdown_id, prod_id, color_id, quantity, inserted_by, insert_date";
 		$id_prop = return_next_id( "id", "order_wise_pro_details", 1 );
		foreach($po_array as $key=>$val)
		{
			$order_id=$key;
			$order_qnty=$val;
			
			if($data_array_prop!="") $data_array_prop.= ",";
			$data_array_prop.="(".$id_prop.",".$id_trans.",1,7,".$id_dtls.",'".$order_id."','".$prod_id."','".$color_id."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_prop = $id_prop+1;
		}
		
		
		if(str_replace("'","",$update_id)=="")
		{
			$rID=sql_insert("inv_receive_master",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($rID) $flag=1; else $flag=0; 
		}
		
		if(str_replace("'","",$cbo_production_basis)==4)
		{
			if(count($batchData)>0)
			{
				$rID6=sql_update("pro_batch_create_mst",$field_array_batch_update,$data_array_batch_update,"id",$batch_id,0);
				if($flag==1) 
				{
					if($rID6) $flag=1; else $flag=0;
				} 
			}
			else
			{
				$rID6=sql_insert("pro_batch_create_mst",$field_array_batch,$data_array_batch,0);
				if($flag==1) 
				{
					if($rID6) $flag=1; else $flag=0;
				} 
			}
		}
		if(count($row_prod)>0)
		{
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$rID2=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
				if($flag==1) 
				{
					if($rID2) $flag=1; else $flag=0; 
				} 
			}
		}
		else
		{
			$rID2=sql_insert("product_details_master",$field_array_prod,$data_array_prod,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$rID3=sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
		}
		
		$rID4=sql_insert("pro_finish_fabric_rcv_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1) 
		{
			if($rID4) $flag=1; else $flag=0; 
		} 
		
		if($data_array_roll!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$batch_booking_without_order)!=1)
		{
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
			$rID5=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			}
		}
		
		//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;	
		if($data_array_prop!="" && str_replace("'","",$batch_booking_without_order)!=1)
		{
			$rID7=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
			if($flag==1) 
			{
				if($rID7) $flag=1; else $flag=0; 
			} 
		}
		
		if(str_replace("'","",$batch_booking_without_order)==1 && str_replace("'","",$cbo_production_basis)==4)
		{
			$data_array_batch_dtls="(".$id_dtls_batch.",'".$batch_id."',0,'".$ItemDesc."','".$txt_no_of_roll."',".$txt_production_qty.",".$id_dtls.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		}
		
		if($data_array_batch_dtls!="")
		{
			//echo "insert into pro_batch_create_dtls (".$field_array_batch_dtls.") values ".$data_array_batch_dtls;die;	
			$rID8=sql_insert("pro_batch_create_dtls",$field_array_batch_dtls,$data_array_batch_dtls,1);
			if($flag==1) 
			{
				if($rID8) $flag=1; else $flag=0; 
			} 
		}
		
		if($data_array_roll_for_batch!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$batch_booking_without_order)!=1)
		{
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll_for_batch;die;	
			$rID9=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll_for_batch,1);
			if($flag==1) 
			{
				if($rID9) $flag=1; else $flag=0; 
			} 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$finish_update_id."**".$finish_recv_num."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "0**".$finish_update_id."**".$finish_recv_num."**0";
			}
			else
			{
				oci_rollback($con);
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		check_table_status( $_SESSION['menu_id'],0);
				
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$flag=1;
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		$ItemDesc=str_replace("'","",$txt_fabric_desc).", ".str_replace("'","",$txt_gsm).", ".str_replace("'","",$txt_dia_width);
		if(str_replace("'","",$cbo_production_basis)==4)
		{
			$order_id=explode(",",str_replace("'","",$all_po_id));
			if(count($order_id)>1)
			{
				check_table_status( $_SESSION['menu_id'],0);
				echo "20**0"; 
				die;
			}
			
			/*$batch_weight= return_field_value("batch_weight","pro_batch_create_mst","id=$txt_batch_id");
			$adjust_batch_weight=$batch_weight-str_replace("'", '',$hidden_receive_qnty);
			
			$rID_batch_adjust=sql_update("pro_batch_create_mst","batch_weight",$adjust_batch_weight,"id",$txt_batch_id,0);
			if($flag==1) 
			{
				if($rID_batch_adjust) $flag=1; else $flag=0; 
			} */
			
			$batchData=sql_select("select a.id, a.batch_weight from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.batch_no=$txt_batch_no and a.color_id='$color_id' and b.po_id=$all_po_id and a.status_active=1 and a.is_deleted=0 group by a.id, a.batch_weight");
			
			if(count($batchData)>0)
			{
				$batch_id=$batchData[0][csf('id')];
				if($batch_id==str_replace("'","",$txt_batch_id))
				{
					$curr_batch_weight=$batchData[0][csf('batch_weight')]+str_replace("'", '',$txt_production_qty)-str_replace("'", '',$hidden_receive_qnty);
					$field_array_batch_update="batch_weight*updated_by*update_date";
					$data_array_batch_update=$curr_batch_weight."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
					
					/*$rID6=sql_update("pro_batch_create_mst",$field_array_batch_update,$data_array_batch_update,"id",$batch_id,0);
					if($flag==1)
					{
						if($rID6) $flag=1; else $flag=0;
					}*/
				}
				else
				{
					$batch_weight= return_field_value("batch_weight","pro_batch_create_mst","id=$txt_batch_id");
					$adjust_batch_weight=$batch_weight-str_replace("'", '',$hidden_receive_qnty);
					
					/*$rID_batch_adjust=sql_update("pro_batch_create_mst","batch_weight",$adjust_batch_weight,"id",$txt_batch_id,0);
					if($flag==1) 
					{
						if($rID_batch_adjust) $flag=1; else $flag=0; 
					} */
					
					$curr_batch_weight=$batchData[0][csf('batch_weight')]+str_replace("'", '',$txt_production_qty);
					$field_array_batch_update="batch_weight*updated_by*update_date";
					$data_array_batch_update=$curr_batch_weight."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
					
					/*$rID6=sql_update("pro_batch_create_mst",$field_array_batch_update,$data_array_batch_update,"id",$batch_id,0);
					if($flag==1)
					{
						if($rID6) $flag=1; else $flag=0;
					}*/
				}
			}
			else
			{
				$batch_weight= return_field_value("batch_weight","pro_batch_create_mst","id=$txt_batch_id");
				$adjust_batch_weight=$batch_weight-str_replace("'", '',$hidden_receive_qnty);
				
				/*$rID_batch_adjust=sql_update("pro_batch_create_mst","batch_weight",$adjust_batch_weight,"id",$txt_batch_id,0);
				if($flag==1) 
				{
					if($rID_batch_adjust) $flag=1; else $flag=0; 
				} */
				
				if(is_duplicate_field( "batch_no", "pro_batch_create_mst", "batch_no=$txt_batch_no" )==1)
				{
					check_table_status( $_SESSION['menu_id'],0);
					echo "11**0"; 
					die;			
				}
				
				$batch_id=return_next_id( "id", "pro_batch_create_mst", 1 ) ;
				$field_array_batch="id, batch_no, entry_form, batch_date, company_id, color_id, batch_weight, inserted_by, insert_date";
				
				$data_array_batch="(".$batch_id.",".$txt_batch_no.",7,".$txt_production_date.",".$cbo_company_id.",'".$color_id."',".$txt_production_qty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				//echo "insert into pro_batch_create_mst (".$field_array_batch.") values ".$data_array_batch;die;
				/*$rID6=sql_insert("pro_batch_create_mst",$field_array_batch,$data_array_batch,0);
				if($flag==1)
				{
					if($rID6) $flag=1; else $flag=0;
				}*/
			}
			
			$batch_dtls_id= return_field_value("id","pro_batch_create_dtls","mst_id=$txt_batch_id and dtls_id=$update_dtls_id");
			
			/*$delete_batch_dtls=execute_query( "delete from pro_batch_create_dtls where mst_id=$txt_batch_id and id='$batch_dtls_id'",0);
			if($flag==1) 
			{
				if($delete_batch_dtls) $flag=1; else $flag=0; 
			} 
			
			if(str_replace("'","",$roll_maintained)==1)
			{
				$delete_batch_roll=execute_query( "delete from pro_roll_details where mst_id=$txt_batch_id and dtls_id='$batch_dtls_id' and entry_form=2",0);
				if($flag==1) 
				{
					if($delete_batch_roll) $flag=1; else $flag=0; 
				}
			}*/
		}
		else
		{
			$batch_id=str_replace("'","",$txt_batch_id);
		}
		
		$field_array_update="receive_basis*receive_date*challan_no*store_id*location_id*knitting_source*knitting_company*updated_by*update_date";
			
		$data_array_update=$cbo_production_basis."*".$txt_production_date."*".$txt_challan_no."*".$cbo_store_name."*".$cbo_location."*".$cbo_dyeing_source."*".$cbo_dyeing_company."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} */
		
		/*if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$stock= return_field_value("current_stock","product_details_master","id=$previous_prod_id");
			$adjust_curr_stock=$stock-str_replace("'", '',$hidden_receive_qnty);
			$cur_st_value=0; $cur_st_rate=0;
			
			$field_array_adjust="current_stock*avg_rate_per_unit*stock_value";
			$data_array_adjust=$adjust_curr_stock."*".$cur_st_value."*".$cur_st_rate;
			
			$rID_adjust=sql_update("product_details_master",$field_array_adjust,$data_array_adjust,"id",$previous_prod_id,0);
			if($flag==1) 
			{
				if($rID_adjust) $flag=1; else $flag=0; 
			} 
		}*/
		
		if(str_replace("'","",$fabric_desc_id)=="") $fabric_desc_id=0;
			
		if(str_replace("'","",$fabric_desc_id)==0)
		{
			$fabric_description=trim(str_replace("'","",$txt_fabric_desc));
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=2 and detarmination_id=$fabric_desc_id and item_description='$fabric_description' and gsm=$txt_gsm and dia_width=$txt_dia_width and color='$color_id' and status_active=1 and is_deleted=0");
		}
		else
		{
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=2 and detarmination_id=$fabric_desc_id and gsm=$txt_gsm and dia_width=$txt_dia_width and color='$color_id' and status_active=1 and is_deleted=0");
		}
		
		if(count($row_prod)>0)
		{
			$prod_id=$row_prod[0][csf('id')];
			if($prod_id==str_replace("'","",$previous_prod_id))
			{
				if(str_replace("'","",$fabric_store_auto_update)==1)
				{
					$stock_qnty=$row_prod[0][csf('current_stock')];
		
					$curr_stock_qnty=$stock_qnty+str_replace("'", '',$txt_production_qty)-str_replace("'", '',$hidden_receive_qnty);
					$avg_rate_per_unit=0; $stock_value=0;
		
					$field_array_prod_update="store_id*avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*color*updated_by*update_date";
					
					$data_array_prod_update=$cbo_store_name."*".$avg_rate_per_unit."*".$txt_production_qty."*".$curr_stock_qnty."*".$stock_value."*'".$color_id."'*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
					
					/*$rIDP=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
					if($flag==1) 
					{
						if($rIDP) $flag=1; else $flag=0; 
					} */
				}
			}
			else
			{
				if(str_replace("'","",$fabric_store_auto_update)==1)
				{
					$stock= return_field_value("current_stock","product_details_master","id=$previous_prod_id");
					$adjust_curr_stock=$stock-str_replace("'", '',$hidden_receive_qnty);
					$cur_st_value=0; $cur_st_rate=0;
					
					$field_array_adjust="current_stock*avg_rate_per_unit*stock_value";
					$data_array_adjust=$adjust_curr_stock."*".$cur_st_value."*".$cur_st_rate;
					
					/*$rID_adjust=sql_update("product_details_master",$field_array_adjust,$data_array_adjust,"id",$previous_prod_id,0);
					if($flag==1) 
					{
						if($rID_adjust) $flag=1; else $flag=0; 
					}*/ 
					
					$stock_qnty=$row_prod[0][csf('current_stock')];
		
					$curr_stock_qnty=$stock_qnty+str_replace("'", '',$txt_production_qty);
					$avg_rate_per_unit=0; $stock_value=0;
		
					$field_array_prod_update="store_id*avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*color*updated_by*update_date";
					
					$data_array_prod_update=$cbo_store_name."*".$avg_rate_per_unit."*".$txt_production_qty."*".$curr_stock_qnty."*".$stock_value."*'".$color_id."'*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
					
					/*$rIDP=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
					if($flag==1) 
					{
						if($rIDP) $flag=1; else $flag=0; 
					} */
				}
			}
		}
		else
		{
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock= return_field_value("current_stock","product_details_master","id=$previous_prod_id");
				$adjust_curr_stock=$stock-str_replace("'", '',$hidden_receive_qnty);
				$cur_st_value=0; $cur_st_rate=0;
				
				$field_array_adjust="current_stock*avg_rate_per_unit*stock_value";
				$data_array_adjust=$adjust_curr_stock."*".$cur_st_value."*".$cur_st_rate;
				
				/*$rID_adjust=sql_update("product_details_master",$field_array_adjust,$data_array_adjust,"id",$previous_prod_id,0);
				if($flag==1) 
				{
					if($rID_adjust) $flag=1; else $flag=0; 
				}*/ 
			}
			
			$prod_id=return_next_id( "id", "product_details_master", 1 ) ;
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock_qnty=$txt_production_qty;
				$last_purchased_qnty=$txt_production_qty;
			}
			else
			{
				$stock_qnty=0;
				$last_purchased_qnty=0;
			}
			
			$avg_rate_per_unit=0; $stock_value=0;
			$prod_name_dtls=trim(str_replace("'","",$txt_fabric_desc)).", ".trim(str_replace("'","",$txt_gsm)).", ".trim(str_replace("'","",$txt_dia_width));
			$field_array_prod="id, company_id, store_id, item_category_id, detarmination_id, item_description, product_name_details, unit_of_measure, avg_rate_per_unit, last_purchased_qnty, current_stock, stock_value, color, gsm, dia_width, inserted_by, insert_date";
			
			$data_array_prod="(".$prod_id.",".$cbo_company_id.",".$cbo_store_name.",2,".$fabric_desc_id.",".$txt_fabric_desc.",'".$prod_name_dtls."',0,".$avg_rate_per_unit.",".$last_purchased_qnty.",".$stock_qnty.",".$stock_value.",".$color_id.",".$txt_gsm.",".$txt_dia_width.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			/*$rIDP=sql_insert("product_details_master",$field_array_prod,$data_array_prod,0);
			if($flag==1) 
			{
				if($rIDP) $flag=1; else $flag=0; 
			}*/ 
		}
		
		if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$order_rate=0; $order_amount=0; $cons_rate=0; $cons_amount=0;
			
			$sqlBl = sql_select("select cons_quantity,cons_amount,balance_qnty,balance_amount from inv_transaction where id=$update_trans_id");
			$before_receive_qnty	= $sqlBl[0][csf("cons_quantity")]; 
			$beforeAmount			= $sqlBl[0][csf("cons_amount")];
			$beforeBalanceQnty		= $sqlBl[0][csf("balance_qnty")]; 
			$beforeBalanceAmount	= $sqlBl[0][csf("balance_amount")];
			
			$adjBalanceQnty		=$beforeBalanceQnty-$before_receive_qnty+str_replace("'", '',$txt_production_qty);
			$adjBalanceAmount	=$beforeBalanceAmount-$beforeAmount+$con_amount; 
			
			$field_array_trans_update="receive_basis*pi_wo_batch_no*prod_id*transaction_date*store_id*order_qnty*order_rate*order_amount*cons_quantity*cons_reject_qnty*cons_rate*cons_amount*balance_qnty*balance_amount*machine_id*rack*self*updated_by*update_date";
			
			$data_array_trans_update=$cbo_production_basis."*'".$batch_id."'*".$prod_id."*".$txt_production_date."*".$cbo_store_name."*".$txt_production_qty."*".$order_rate."*".$order_amount."*".$txt_production_qty."*".$txt_reject_qty."*".$cons_rate."*".$cons_amount."*".$adjBalanceQnty."*".$adjBalanceAmount."*".$cbo_machine_name."*".$txt_rack."*".$txt_shelf."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			/*$rID3=sql_update("inv_transaction",$field_array_trans_update,$data_array_trans_update,"id",$update_trans_id,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			}*/ 
		}
		
		$rate=0; $amount=0;
		$field_array_dtls_update="prod_id*batch_id*body_part_id*fabric_description_id*gsm*width*color_id*receive_qnty*reject_qty*no_of_roll*order_id*buyer_id*machine_no_id*rack_no*shelf_no*updated_by*update_date";
		
		$data_array_dtls_update=$prod_id."*'".$batch_id."'*".$cbo_body_part."*".$fabric_desc_id."*".$txt_gsm."*".$txt_dia_width."*".$color_id."*".$txt_production_qty."*".$txt_reject_qty."*".$txt_no_of_roll."*".$all_po_id."*".$buyer_id."*".$cbo_machine_name."*".$txt_rack."*".$txt_shelf."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";

		/*$rID4=sql_update("pro_finish_fabric_rcv_dtls",$field_array_dtls_update,$data_array_dtls_update,"id",$update_dtls_id,0);
		if($flag==1) 
		{
			if($rID4) $flag=1; else $flag=0; 
		} 
		
		if(str_replace("'","",$roll_maintained)==1)
		{
			$delete_roll=execute_query( "delete from pro_roll_details where dtls_id=$update_dtls_id and entry_form=4",0);
			if($flag==1) 
			{
				if($delete_roll) $flag=1; else $flag=0; 
			} 
		}*/
		
		$field_array_batch_dtls="id, mst_id, po_id, item_description, roll_no, batch_qnty, dtls_id, inserted_by, insert_date";
		$id_dtls_batch = return_next_id( "id", "pro_batch_create_dtls", 1 ); 
		
		$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, inserted_by, insert_date";
		
		$save_string=explode(",",str_replace("'","",$save_data));
		$po_array=array();
		
		for($i=0;$i<count($save_string);$i++)
		{
			$order_dtls=explode("**",$save_string[$i]);
			
			$order_id=$order_dtls[0];
			$order_qnty_roll_wise=$order_dtls[1];
			$roll_no=$order_dtls[2];
			
			if($data_array_roll!="" ) $data_array_roll.=",";
			
			if( $id_roll=="" ) $id_roll = return_next_id( "id", "pro_roll_details", 1 ); else $id_roll = $id_roll+1;
			
			$data_array_roll.="(".$id_roll.",".$update_id.",".$update_dtls_id.",'".$order_id."',4,'".$order_qnty_roll_wise."','".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			if(array_key_exists($order_id,$po_array))
			{
				$po_array[$order_id]+=$order_qnty_roll_wise;
			}
			else
			{
				$po_array[$order_id]=$order_qnty_roll_wise;
			}
			
			if(str_replace("'","",$cbo_production_basis)==4)
			{
				if($data_array_batch_dtls!="" ) $data_array_batch_dtls.=",";
				
				$data_array_batch_dtls .="(".$id_dtls_batch.",'".$batch_id."',".$order_id.",'".$ItemDesc."','".$roll_no."',".$order_qnty_roll_wise.",".$update_dtls_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id_dtls_batch = $id_dtls_batch+1;
				
				$id_roll = $id_roll+1;
				if($data_array_roll_for_batch!="" ) $data_array_roll_for_batch.=",";
				$data_array_roll_for_batch.="(".$id_roll.",'".$batch_id."',".$id_dtls_batch.",".$order_id.",2,".$order_qnty_roll_wise.",'".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			}
		}
		
		/*if($data_array_roll!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$batch_booking_without_order)!=1)
		{
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;
			$rID5=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			}
		}*/
		
		/*$delete_prop=execute_query( "delete from order_wise_pro_details where dtls_id=$update_dtls_id and trans_id=$update_trans_id and entry_form=7",0);
		if($flag==1) 
		{
			if($delete_prop) $flag=1; else $flag=0; 
		}*/

		$field_array_proportionate="id, trans_id, trans_type, entry_form, dtls_id, po_breakdown_id, prod_id, color_id, quantity, inserted_by, insert_date";
		$id_prop = return_next_id( "id", "order_wise_pro_details", 1 );
		foreach($po_array as $key=>$val)
		{
			$order_id=$key;
			$order_qnty=$val;
			
			if($data_array_prop!="" ) $data_array_prop.=",";
			$data_array_prop.="(".$id_prop.",".$update_trans_id.",1,7,".$update_dtls_id.",'".$order_id."','".$prod_id."','".$color_id."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_prop = $id_prop+1;
		}
		
		if(str_replace("'","",$cbo_production_basis)==4)
		{
			if(count($batchData)>0)
			{
				if($batch_id==str_replace("'","",$txt_batch_id))
				{
					$rID6=sql_update("pro_batch_create_mst",$field_array_batch_update,$data_array_batch_update,"id",$batch_id,0);
					if($flag==1)
					{
						if($rID6) $flag=1; else $flag=0;
					}
				}
				else
				{
					$rID_batch_adjust=sql_update("pro_batch_create_mst","batch_weight",$adjust_batch_weight,"id",$txt_batch_id,0);
					if($flag==1) 
					{
						if($rID_batch_adjust) $flag=1; else $flag=0; 
					} 
					
					$rID6=sql_update("pro_batch_create_mst",$field_array_batch_update,$data_array_batch_update,"id",$batch_id,0);
					if($flag==1)
					{
						if($rID6) $flag=1; else $flag=0;
					}
				}
			}
			else
			{
				$rID_batch_adjust=sql_update("pro_batch_create_mst","batch_weight",$adjust_batch_weight,"id",$txt_batch_id,0);
				if($flag==1) 
				{
					if($rID_batch_adjust) $flag=1; else $flag=0; 
				} 
				
				$rID6=sql_insert("pro_batch_create_mst",$field_array_batch,$data_array_batch,0);
				if($flag==1)
				{
					if($rID6) $flag=1; else $flag=0;
				}
			}
			
			$delete_batch_dtls=execute_query( "delete from pro_batch_create_dtls where mst_id=$txt_batch_id and id='$batch_dtls_id'",0);
			if($flag==1) 
			{
				if($delete_batch_dtls) $flag=1; else $flag=0; 
			} 
			
			if(str_replace("'","",$roll_maintained)==1)
			{
				$delete_batch_roll=execute_query( "delete from pro_roll_details where mst_id=$txt_batch_id and dtls_id='$batch_dtls_id' and entry_form=2",0);
				if($flag==1) 
				{
					if($delete_batch_roll) $flag=1; else $flag=0; 
				}
			}
		}
		
		$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} 
		
		if(count($row_prod)>0)
		{
			$prod_id=$row_prod[0][csf('id')];
			if($prod_id==str_replace("'","",$previous_prod_id))
			{
				if(str_replace("'","",$fabric_store_auto_update)==1)
				{
					$rIDP=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
					if($flag==1) 
					{
						if($rIDP) $flag=1; else $flag=0; 
					} 
				}
			}
			else
			{
				if(str_replace("'","",$fabric_store_auto_update)==1)
				{
					$rID_adjust=sql_update("product_details_master",$field_array_adjust,$data_array_adjust,"id",$previous_prod_id,0);
					if($flag==1) 
					{
						if($rID_adjust) $flag=1; else $flag=0; 
					} 
					
					$rIDP=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
					if($flag==1) 
					{
						if($rIDP) $flag=1; else $flag=0; 
					} 
				}
			}
		}
		else
		{
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$rID_adjust=sql_update("product_details_master",$field_array_adjust,$data_array_adjust,"id",$previous_prod_id,0);
				if($flag==1) 
				{
					if($rID_adjust) $flag=1; else $flag=0; 
				} 
			}
			
			$rIDP=sql_insert("product_details_master",$field_array_prod,$data_array_prod,0);
			if($flag==1) 
			{
				if($rIDP) $flag=1; else $flag=0; 
			} 
		}
		
		
		if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$rID3=sql_update("inv_transaction",$field_array_trans_update,$data_array_trans_update,"id",$update_trans_id,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
		}
		$rID4=sql_update("pro_finish_fabric_rcv_dtls",$field_array_dtls_update,$data_array_dtls_update,"id",$update_dtls_id,0);
		if($flag==1) 
		{
			if($rID4) $flag=1; else $flag=0; 
		} 
		
		if(str_replace("'","",$roll_maintained)==1)
		{
			$delete_roll=execute_query( "delete from pro_roll_details where dtls_id=$update_dtls_id and entry_form=4",0);
			if($flag==1) 
			{
				if($delete_roll) $flag=1; else $flag=0; 
			} 
		}
		if($data_array_roll!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$batch_booking_without_order)!=1)
		{
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;
			$rID5=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			}
		}
		$delete_prop=execute_query( "delete from order_wise_pro_details where dtls_id=$update_dtls_id and trans_id=$update_trans_id and entry_form=7",0);
		if($flag==1) 
		{
			if($delete_prop) $flag=1; else $flag=0; 
		}
		//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;	
		if($data_array_prop!="" && str_replace("'","",$batch_booking_without_order)!=1)
		{
			$rID6=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
			if($flag==1) 
			{
				if($rID6) $flag=1; else $flag=0; 
			} 
		}
		
		if(str_replace("'","",$batch_booking_without_order)==1 && str_replace("'","",$cbo_production_basis)==4)
		{
			$data_array_batch_dtls="(".$id_dtls_batch.",'".$batch_id."',0,'".$ItemDesc."','".$txt_no_of_roll."',".$txt_production_qty.",".$update_dtls_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		}
		
		if($data_array_batch_dtls!="")
		{
			//echo "insert into pro_batch_create_dtls (".$field_array_batch_dtls.") values ".$data_array_batch_dtls;die;	
			$rID7=sql_insert("pro_batch_create_dtls",$field_array_batch_dtls,$data_array_batch_dtls,1);
			if($flag==1) 
			{
				if($rID7) $flag=1; else $flag=0; 
			} 
		}
		
		if($data_array_roll_for_batch!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$batch_booking_without_order)!=1)
		{
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll_for_batch;die;	
			$rID8=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll_for_batch,1);
			if($flag==1) 
			{
				if($rID8) $flag=1; else $flag=0; 
			} 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'", '', $update_id)."**".str_replace("'", '', $txt_system_id)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**0**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "1**".str_replace("'", '', $update_id)."**".str_replace("'", '', $txt_system_id)."**0";
			}
			else
			{
				oci_rollback($con);
				echo "6**0**0**1";
			}
		}
		disconnect($con);
		die;
	}
}

if($action=="show_finish_fabric_listview")
{
	$machine_arr = return_library_array("select id, machine_no as machine_name from lib_machine_name","id","machine_name");
	$fabric_desc_arr=return_library_array("select id, item_description from product_details_master where item_category_id=2","id","item_description");
	$composition_arr=array();
 	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}
	
	$sql="select id, prod_id, batch_id, body_part_id, fabric_description_id, gsm, width, color_id, receive_qnty, reject_qty, machine_no_id from pro_finish_fabric_rcv_dtls where mst_id='$data' and status_active = '1' and is_deleted = '0'";
	$result=sql_select($sql);
	
	//$arr=array(0=>$batch_arr,1=>$body_part,2=>$composition_arr,5=>$color_arr,8=>$machine_arr);
	//echo  create_list_view("list_view", "Batch,Body Part,Fabric Description,GSM,Dia / Width,Color, Qc Pass Qty, Reject Qty, Machine No", "80,100,150,60,70,80,80,80,100","820","200",0, $sql, "put_data_dtls_part", "id", "'populate_finish_details_form_data'", 0, "batch_id,body_part_id,fabric_description_id,0,0,color_id,0,0,machine_no_id", $arr, "batch_id,body_part_id,fabric_description_id,gsm,width,color_id,receive_qnty,reject_qty,machine_no_id", "requires/finish_fabric_receive_controller",'','0,0,0,0,0,0,2,2,0');
?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="820" class="rpt_table">
        <thead>
            <th width="80">Batch</th>
            <th width="100">Body Part</th>
            <th width="150">Fabric Description</th>
            <th width="60">GSM</th>
            <th width="70">Dia / Width</th>
            <th width="80">Color</th>
            <th width="80">QC Pass Qty</th>
            <th width="80">Reject Qty</th>
            <th>Machine No</th>
        </thead>
	</table>
	<div style="width:820px; max-height:200px; overflow-y:scroll" id="list_container_batch" align="left">	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table" id="list_view">  
        <?php
            $i=1;
            foreach($result as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	 
					
                if($row[csf('fabric_description_id')]==0 || $row[csf('fabric_description_id')]=="")
					$fabric_desc=$fabric_desc_arr[$row[csf('prod_id')]]; 
				else
					$fabric_desc=$composition_arr[$row[csf('fabric_description_id')]];
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="put_data_dtls_part(<?php echo $row[csf('id')]; ?>,'populate_finish_details_form_data', 'requires/finish_fabric_receive_controller');"> 
                    <td width="80"><p><?php echo $batch_arr[$row[csf('batch_id')]]; ?></p></td>
                    <td width="100"><p><?php echo $body_part[$row[csf('body_part_id')]]; ?>&nbsp;</p></td>
                    <td width="150"><p><?php echo $fabric_desc; ?></p></td>
                    <td width="60"><p><?php echo $row[csf('gsm')]; ?>&nbsp;</p></td>
                    <td width="70"><p><?php echo $row[csf('width')]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $color_arr[$row[csf('color_id')]]; ?>&nbsp;</p></td>
                    <td width="80" align="right"><?php echo number_format($row[csf('receive_qnty')],2); ?></td>
                    <td width="80" align="right"><?php echo number_format($row[csf('reject_qty')],2); ?>&nbsp;</td>
                    <td><p><?php echo $machine_arr[$row[csf('machine_no_id')]]; ?>&nbsp;</p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
    </div>
<?php	
	exit();
}

if($action=='populate_finish_details_form_data')
{
	$data=explode("**",$data);
	$id=$data[0];
	$roll_maintained=$data[1];
	
	$data_array=sql_select("select a.receive_basis, b.id, b.trans_id, b.prod_id, b.batch_id, b.body_part_id, b.fabric_description_id, b.gsm, b.width, b.color_id, b.receive_qnty, b.reject_qty, b.no_of_roll, b.machine_no_id, b.rack_no, b.shelf_no, b.order_id, b.buyer_id from inv_receive_master a, pro_finish_fabric_rcv_dtls b where a.id=b.mst_id and b.id='$id' and a.item_category=2 and a.entry_form=7");
	foreach ($data_array as $row)
	{ 
		$buyer_name='';
		$buyer=explode(",",$row[csf('buyer_id')]);
		foreach($buyer as $val )
		{
			if($buyer_name=='') $buyer_name=$buyer_arr[$val]; else $buyer_name.=",".$buyer_arr[$val];
		}
		
		$comp='';
		if($row[csf('fabric_description_id')]==0 || $row[csf('fabric_description_id')]=="")
		{
			$comp=return_field_value("item_description","product_details_master","id=".$row[csf('prod_id')]);
		}
		else
		{
			$determination_sql=sql_select("select a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id and a.id=".$row[csf('fabric_description_id')]);
					
			if($determination_sql[0][csf('construction')]!="")
			{
				$comp=$determination_sql[0][csf('construction')].", ";
			}
			
			foreach( $determination_sql as $d_row )
			{
				$comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
			}
		}
		
		echo "document.getElementById('txt_batch_id').value 				= '".$row[csf("batch_id")]."';\n";
		echo "document.getElementById('txt_batch_no').value 				= '".$batch_arr[$row[csf("batch_id")]]."';\n";
		
		if($row[csf('receive_basis')]==5)
		{
			echo "get_php_form_data('".$row[csf("batch_id")]."', 'populate_data_from_batch', 'requires/finish_fabric_receive_controller' );\n";
			echo "show_list_view('".$row[csf("batch_id")]."','show_fabric_desc_listview','list_fabric_desc_container','requires/finish_fabric_receive_controller','');\n";
		}
		else
		{
			echo "get_php_form_data('".$row[csf("order_id")]."', 'load_color', 'requires/finish_fabric_receive_controller' );\n";
		}
	
		echo "document.getElementById('cbo_body_part').value 				= '".$row[csf("body_part_id")]."';\n";
		echo "document.getElementById('txt_fabric_desc').value 				= '".$comp."';\n";
		echo "document.getElementById('fabric_desc_id').value 				= '".$row[csf("fabric_description_id")]."';\n";
		echo "document.getElementById('txt_color').value 					= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('txt_gsm').value 						= '".$row[csf("gsm")]."';\n";
		echo "document.getElementById('txt_dia_width').value 				= '".$row[csf("width")]."';\n";
		echo "document.getElementById('txt_production_qty').value 			= '".$row[csf("receive_qnty")]."';\n";
		echo "document.getElementById('txt_reject_qty').value 				= '".$row[csf("reject_qty")]."';\n";
		echo "document.getElementById('txt_no_of_roll').value 				= '".$row[csf("no_of_roll")]."';\n";
		echo "document.getElementById('buyer_name').value 					= '".$buyer_name."';\n";
		echo "document.getElementById('buyer_id').value 					= '".$row[csf("buyer_id")]."';\n";
		echo "document.getElementById('cbo_machine_name').value 			= '".$row[csf("machine_no_id")]."';\n";
		echo "document.getElementById('txt_rack').value 					= '".$row[csf("rack_no")]."';\n";
		echo "document.getElementById('txt_shelf').value 					= '".$row[csf("shelf_no")]."';\n";
		echo "document.getElementById('hidden_receive_qnty').value 			= '".$row[csf("receive_qnty")]."';\n";
		echo "document.getElementById('all_po_id').value 					= '".$row[csf("order_id")]."';\n";
		echo "document.getElementById('previous_prod_id').value 			= '".$row[csf("prod_id")]."';\n";
		echo "document.getElementById('update_dtls_id').value 				= '".$row[csf("id")]."';\n";
		echo "document.getElementById('update_trans_id').value 				= '".$row[csf("trans_id")]."';\n";
		
		$save_string='';
		if($roll_maintained==1)
		{
			$data_roll_array=sql_select("select id, po_breakdown_id, qnty,roll_no from pro_roll_details where dtls_id='$id' and entry_form=4 and status_active=1 and is_deleted=0");
			foreach($data_roll_array as $row_roll)
			{ 
				if($save_string=="")
				{
					$save_string=$row_roll[csf("po_breakdown_id")]."**".$row_roll[csf("qnty")]."**".$row_roll[csf("roll_no")];
				}
				else
				{
					$save_string.=",".$row_roll[csf("po_breakdown_id")]."**".$row_roll[csf("qnty")]."**".$row_roll[csf("roll_no")];
				}
			}
		}
		else
		{
			$data_po_array=sql_select("select po_breakdown_id, quantity from order_wise_pro_details where dtls_id='$id' and entry_form=7 and status_active=1 and is_deleted=0");
			foreach($data_po_array as $row_po)
			{ 
				if($save_string=="")
				{
					$save_string=$row_po[csf("po_breakdown_id")]."**".$row_po[csf("quantity")];
				}
				else
				{
					$save_string.=",".$row_po[csf("po_breakdown_id")]."**".$row_po[csf("quantity")];
				}
			}
		}
		echo "document.getElementById('save_data').value 				= '".$save_string."';\n";
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_finish_production',1);\n";  
		exit();
	}
}

if($action=="roll_maintained")
{
	$roll_maintained=return_field_value("fabric_roll_level","variable_settings_production","company_name='$data' and variable_list=3 and is_deleted=0 and status_active=1"); 
	
	$fabric_store_auto_update=return_field_value("auto_update","variable_settings_production","company_name='$data' and variable_list=15 and item_category_id=2 and is_deleted=0 and status_active=1");
	
	if($roll_maintained==1) $roll_maintained=$roll_maintained; else $roll_maintained=0;
	
	if($fabric_store_auto_update==2) $fabric_store_auto_update=0; else $fabric_store_auto_update=1;
	
	echo "document.getElementById('roll_maintained').value 				= '".$roll_maintained."';\n";
	echo "document.getElementById('fabric_store_auto_update').value 		= '".$fabric_store_auto_update."';\n";
	
	echo "reset_form('finishFabricEntry_1','list_fabric_desc_container','','','set_production_basis();','cbo_production_basis*cbo_company_id*txt_production_date*txt_challan_no*roll_maintained*fabric_store_auto_update');\n";
	
	exit();	
}

if($action=="load_color")
{
	$sql="select c.color_name from wo_booking_mst a, wo_booking_dtls b, lib_color c where a.booking_no=b.booking_no and b.fabric_color_id=c.id and b.po_break_down_id in($data) and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.fabric_color_id, c.color_name";
	//echo $sql;die;
	echo "var str_color = [". substr(return_library_autocomplete( $sql, "color_name" ), 0, -1). "];\n";
	echo "$('#txt_color').autocomplete({
						 source: str_color
					  });\n";
	exit();	
}

if($action=="check_batch_no")
{
	$data=explode("**",$data);
	$sql="select id, batch_no from pro_batch_create_mst where batch_no='".trim($data[0])."' and company_id='".$data[1]."' and is_deleted=0 and status_active=1 and entry_form=0 order by id desc";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo $data_array[0][csf('id')];
	}
	else
	{
		echo "0";
	}
	exit();	
}
if($action=="load_drop_down_body_part")
{
	$data = explode("**",$data);
	$prod_id=$data[0];
	$batch_id=$data[1];
	
	$booking_without_order=''; $booking_id=''; $po_id='';
	$sql="select a.booking_without_order, a.booking_no_id, b.po_id from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.id=$batch_id and b.prod_id=$prod_id and b.status_active=1 and b.is_deleted=0";
	$result=sql_select($sql);
	foreach($result as $row)
	{
		$booking_without_order=$row[csf('booking_without_order')];
		$booking_id=$row[csf('booking_no_id')];
		if($row[csf('po_id')]>0) $po_id.=$row[csf('po_id')].",";
	}
	
	if($booking_without_order==1)
	{
		if($db_type==0)
		{
			$show_body_part_id=return_field_value("group_concat(a.body_part_id) as body_part_id","pro_grey_prod_entry_dtls a, inv_receive_master b","a.mst_id=b.id and a.prod_id=$prod_id and b.booking_id=$booking_id and b.booking_without_order=1 and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0",'body_part_id');
		}
		else
		{ 
			$show_body_part_id=return_field_value("LISTAGG(a.body_part_id, ',') WITHIN GROUP (ORDER BY a.body_part_id) as body_part_id","pro_grey_prod_entry_dtls a, inv_receive_master b","a.mst_id=b.id and a.prod_id=$prod_id and b.booking_id=$booking_id and b.booking_without_order=1 and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0",'body_part_id');
		}
	}
	else
	{
		$po_id=implode(",",array_unique(explode(",",substr($po_id,0,-1))));
		if($db_type==0)
		{
			$show_body_part_id=return_field_value("group_concat(a.body_part_id) as body_part_id","pro_grey_prod_entry_dtls a, order_wise_pro_details b","a.id=b.dtls_id and a.prod_id=$prod_id and b.po_breakdown_id in ($po_id) and b.entry_form in(2,13,22) and b.trans_type in(1,5) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0",'body_part_id');
		}
		else
		{ 
			$show_body_part_id=return_field_value("LISTAGG(a.body_part_id, ',') WITHIN GROUP (ORDER BY a.body_part_id) as body_part_id","pro_grey_prod_entry_dtls a, order_wise_pro_details b","a.id=b.dtls_id and a.prod_id=$prod_id and b.po_breakdown_id in ($po_id) and b.entry_form in(2,13,22) and b.trans_type in(1,5) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0",'body_part_id');
		}
	}
	$show_body_part_id=implode(",",array_unique(explode(",",$show_body_part_id)));
	echo create_drop_down( "cbo_body_part", 182, $body_part,"", 1, "-- Select Body Part --", 0, "",0, $show_body_part_id );
	exit();
}

?>