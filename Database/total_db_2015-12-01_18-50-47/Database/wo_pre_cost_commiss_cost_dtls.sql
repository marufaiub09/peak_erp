-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pre_cost_commiss_cost_dtls`
--

DROP TABLE IF EXISTS `wo_pre_cost_commiss_cost_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pre_cost_commiss_cost_dtls` (
  `id` int(11) NOT NULL DEFAULT '0',
  `job_no` varchar(25) NOT NULL,
  `particulars_id` int(11) NOT NULL DEFAULT '0',
  `commission_base_id` int(11) NOT NULL DEFAULT '0',
  `commision_rate` double NOT NULL,
  `commission_amount` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  KEY `job_no` (`job_no`),
  KEY `particulars_id` (`particulars_id`),
  KEY `commission_base_id` (`commission_base_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pre_cost_commiss_cost_dtls`
--

LOCK TABLES `wo_pre_cost_commiss_cost_dtls` WRITE;
/*!40000 ALTER TABLE `wo_pre_cost_commiss_cost_dtls` DISABLE KEYS */;
INSERT INTO `wo_pre_cost_commiss_cost_dtls` VALUES (1,'PAL-15-00195',2,2,0.5,6,25,'2015-05-07 16:58:56',0,NULL,1,0),(2,'PAL-15-00195',1,0,0,0,25,'2015-05-07 16:58:56',0,NULL,1,0),(3,'PAL-15-00412',1,1,2,0.504,1,'2015-10-06 13:11:34',0,NULL,1,0),(4,'PAL-15-00412',1,0,0,0,1,'2015-10-06 13:11:34',0,NULL,1,0);
/*!40000 ALTER TABLE `wo_pre_cost_commiss_cost_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
