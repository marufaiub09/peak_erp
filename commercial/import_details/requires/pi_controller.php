﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$user_id = $_SESSION['logic_erp']["user_id"];

$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$size_library=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );


//------------------------------------------Load Drop Down on Change---------------------------------------------//
if ($action=="load_supplier_dropdown")
{
	$data = explode('_',$data);
	
	if($data[1]==0) 
	{
		echo create_drop_down( "cbo_supplier_id", 151, $blank_array,'', 1, '-- Select Supplier --',0,'',0);
	}
	else if($data[1]==1)
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =2 and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);		
	}
	else if($data[1]==2 || $data[1]==3 || $data[1]==13 || $data[1]==14)
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =9 and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);
	}
	else if($data[1]==4)
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(4,5) and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);
		
	}
	else if($data[1]==5 || $data[1]==6 || $data[1]==7)
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type=3 and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);
	}
	else if($data[1]==9 || $data[1]==10)
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 6 and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);
	}
	else if($data[1]==11)
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 8 and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);
	}
	else if($data[1]==12 || $data[1]==24 || $data[1]==25)
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(20,21,22,23,24,30,31,32,35,36,37,38,39) and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);
	}
	else
	{
		echo create_drop_down( "cbo_supplier_id", 151,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 7 and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);

	} 
	exit();
}

if ($action=="save_update_delete")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	 
	if ($operation==0)  // Insert Here
	{ 
		if (is_duplicate_field( "pi_number", "com_pi_master_details", "pi_number=$pi_number and importer_id=$cbo_importer_id and supplier_id=$cbo_supplier_id" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			 
			$id=return_next_id( "id", "com_pi_master_details", 1 ); 
			
			$field_array="id,item_category_id,importer_id,supplier_id,pi_number,pi_date,last_shipment_date,pi_validity_date,currency_id,source,hs_code,internal_file_no,intendor_name,pi_basis_id,remarks,inserted_by,insert_date";
			
			$data_array="(".$id.",".$cbo_item_category_id.",".$cbo_importer_id.",".$cbo_supplier_id.",".$pi_number.",".$pi_date.",".$last_shipment_date.",".$pi_validity_date.",".$cbo_currency_id.",".$cbo_source_id.",".$hs_code.",".$txt_internal_file_no.",".$intendor_name.",".$cbo_pi_basis_id.",".$txt_remarks.",".$user_id.",'".$pc_date_time."')";
			
			$rID=sql_insert("com_pi_master_details",$field_array,$data_array,1);
			if($db_type==0)
			{
				if($rID )
				{
					mysql_query("COMMIT");  
					echo "0**".$id;
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "5**0";
				}
			}
			else if($db_type==2 || $db_type==1 )
			{
				if($rID )
				{
					oci_commit($con);  
					echo "0**".$id;
				}
				else
				{
					oci_rollback($con); 
					echo "5**0";
				}
			}
			disconnect($con);
			die;
		}
	}
	else if ($operation==1)   // Update Here
	{
		$sql_attach=sql_select("select a.lc_number from com_btb_lc_master_details a, com_btb_lc_pi b where a.id=b.com_btb_lc_master_details_id and b.pi_id=$update_id and b.status_active=1 and b.is_deleted=0 group by a.id, a.lc_number");
		if(count($sql_attach)>0)
		{
			$lc_number=$sql_attach[0][csf('lc_number')];
			echo "14**".$lc_number."**1"; 
			die;	
		}
		
		$sql_app=sql_select("select approved from com_pi_master_details where id=$update_id and approved=1");
		if(count($sql_app)>0)
		{
			echo "16**1**1"; 
			die;	
		}
		
		if(is_duplicate_field("pi_number", "com_pi_master_details","pi_number=$pi_number and importer_id=$cbo_importer_id and supplier_id=$cbo_supplier_id and id!=$update_id")==1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$field_array="item_category_id*importer_id*supplier_id*pi_number*pi_date*last_shipment_date*pi_validity_date*currency_id*source*hs_code*internal_file_no*intendor_name*pi_basis_id*remarks*updated_by*update_date";
			
			$data_array="".$cbo_item_category_id."*".$cbo_importer_id."*".$cbo_supplier_id."*".$pi_number."*".$pi_date."*".$last_shipment_date."*".$pi_validity_date."*".$cbo_currency_id."*".$cbo_source_id."*".$hs_code."*".$txt_internal_file_no."*".$intendor_name."*".$cbo_pi_basis_id."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$rID=sql_update("com_pi_master_details",$field_array,$data_array,"id",$update_id,1);
			
			if($db_type==0)
			{
				if($rID)
				{
					mysql_query("COMMIT");  
					echo "1**".str_replace("'", '', $update_id);
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "6**".str_replace("'", '', $update_id);
				}
			}
			else if($db_type==2 || $db_type==1 )
			{
				if($rID)
				{
					oci_commit($con);  
					echo "1**".str_replace("'", '', $update_id);
				}
				else
				{
					oci_rollback($con); 
					echo "6**".str_replace("'", '', $update_id);
				}
			}
			
			disconnect($con);
			die;
		}
	}
	else if ($operation==2)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$sql_attach=sql_select("select a.lc_number from com_btb_lc_master_details a, com_btb_lc_pi b where a.id=b.com_btb_lc_master_details_id and b.pi_id=$update_id and b.status_active=1 and b.is_deleted=0 group by a.id, a.lc_number");
		if(count($sql_attach)>0)
		{
			$lc_number=$sql_attach[0][csf('lc_number')];
			echo "14**".$lc_number."**1"; 
			die;	
		}
		
		$sql_app=sql_select("select approved from com_pi_master_details where id=$update_id and approved=1");
		if(count($sql_app)>0)
		{
			echo "16**1**1"; 
			die;	
		}
		
		$field_array="status_active*is_deleted*updated_by*update_date";
		$data_array="0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$rID=sql_update("com_pi_master_details",$field_array,$data_array,"id",$update_id,0);
		
		$field_array_dtls="status_active*is_deleted*updated_by*update_date";
		$data_array_dtls="0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$rID2=sql_update("com_pi_item_details",$field_array_dtls,$data_array_dtls,"pi_id",$update_id,1);
		
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "2**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "7**".str_replace("'", '', $update_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID2)
			{
				oci_commit($con);   
				echo "2**0";
			}
			else
			{
				oci_rollback($con);  
				echo "7**".str_replace("'", '', $update_id);
			}
		}
		
		disconnect($con);
		die;
	}
}

///Save Item Details Table

if ($action=="save_update_delete_dtls")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
	 
		$id=return_next_id( "id","com_pi_item_details", 1 ) ;
	
		$field_array="id,pi_id,work_order_no,work_order_id,work_order_dtls_id,booking_without_order,determination_id,item_prod_id,item_group,item_description,color_id,size_id,item_size,count_name,yarn_composition_item1,yarn_composition_percentage1,yarn_composition_item2,yarn_composition_percentage2,fabric_composition,fabric_construction,yarn_type,gsm,dia_width,weight,item_color,uom,quantity,rate,amount,net_pi_rate,net_pi_amount,service_type,brand_supplier,gmts_item_id,embell_name,embell_type,lot_no,yarn_color,color_range,inserted_by,insert_date"; 
		
		$field_array_update="total_amount*upcharge*discount*net_total_amount";
		if($cbo_currency_id==1)
		{
			$txt_total_amount=number_format($txt_total_amount,$dec_place[4],'.','');
			$txt_total_amount_net=number_format($txt_total_amount_net,$dec_place[4],'.','');
		}
		else
		{
			$txt_total_amount=number_format($txt_total_amount,$dec_place[5],'.','');
			$txt_total_amount_net=number_format($txt_total_amount_net,$dec_place[5],'.','');
		}
		$data_array_update=$txt_total_amount."*'".$txt_upcharge."'*'".$txt_discount."'*".$txt_total_amount_net;
		
		for($i=1;$i<=$total_row;$i++)
		{
			$colorName="colorName_".$i;  
			$sizeName="sizeName_".$i;
			$countName="countName_".$i;
			$yarnCompositionItem1="yarnCompositionItem1_".$i;
			$yarnCompositionPercentage1="yarnCompositionPercentage1_".$i;
			$yarnCompositionItem2="yarnCompositionItem2_".$i;
			$yarnCompositionPercentage2="yarnCompositionPercentage2_".$i;			 
			$yarnType="yarnType_".$i;
			$uom="uom_".$i;
			$quantity="quantity_".$i;
			$rate="rate_".$i;
			$amount="amount_".$i;
			$workOrderNo="workOrderNo_".$i;
			$workOrderId="hideWoId_".$i;
			$workOrderDtlsId="hideWoDtlsId_".$i;
			$itemProdId="itemProdId_".$i;
			$determinationId="hideDeterminationId_".$i; 
			$construction="construction_".$i; 
			$composition="composition_".$i;
			$gsm="gsm_".$i;
			$diawidth="diawidth_".$i;
			$weight="weight_".$i;
			$itemgroupid="itemgroupid_".$i;
			$itemdescription="itemdescription_".$i;
			$servicetype="servicetype_".$i;
			$item_color="itemColor_".$i;
			$itemSize="itemSize_".$i;
			$brandSupRef="brandSupRef_".$i;
			$lot="lot_".$i;
			$yarnColor="yarnColor_".$i;
			$colorRange="colorRange_".$i;
			$gmtsitem="gmtsitem_".$i;
			$embellname="embellname_".$i;
			$embelltype="embelltype_".$i;
			$bookingWithoutOrder="bookingWithoutOrder_".$i;
			
			$perc=(str_replace("'","",$$amount)/$txt_total_amount)*100;
			$net_pi_amount=($perc*$txt_total_amount_net)/100;
			$net_pi_rate=$net_pi_amount/str_replace("'","",$$quantity);
			
			if($cbo_currency_id==1)
				$net_pi_amount=number_format($net_pi_amount,$dec_place[4],'.','');
			else
				$net_pi_amount=number_format($net_pi_amount,$dec_place[5],'.','');
					
			$net_pi_rate=number_format($net_pi_rate,$dec_place[3],'.','');
			
			if(str_replace("'","",$$colorName)!="")
			{ 
				if (!in_array(str_replace("'","",$$colorName),$new_array_color))
				{
					$color_id = return_id( str_replace("'","",$$colorName), $color_library, "lib_color", "id,color_name");  
					$new_array_color[$color_id]=str_replace("'","",$$colorName);
				}
				else $color_id =  array_search(str_replace("'","",$$colorName), $new_array_color); 
			}
			else
			{
				$color_id=0;
			}
			
			if($cbo_item_category_id==4 || $cbo_item_category_id==12)
			{
				if(str_replace("'","",$$sizeName)!="")
				{
					if (!in_array(str_replace("'","",$$sizeName),$new_array_size))
					{
					  $size_id = return_id( str_replace("'","",$$sizeName), $size_library, "lib_size", "id,size_name");  
					  $new_array_size[$size_id]=str_replace("'","",$$sizeName);
					}
					else $size_id =  array_search(str_replace("'","",$$sizeName), $new_array_size); 
				}
				else
				{
					$size_id=0;
				}
				
				if(str_replace("'","",$$item_color)!="")
				{
					if (!in_array(str_replace("'","",$$item_color),$new_array_color))
					{
						$item_color_id = return_id( str_replace("'","",$$item_color), $color_library, "lib_color", "id,color_name");  
						$new_array_color[$item_color_id]=str_replace("'","",$$item_color);
					}
					else $item_color_id =  array_search(str_replace("'","",$$item_color), $new_array_color); 
				}
				else
				{
					$item_color_id=0;
				}
			}
			else
			{
				$size_id=0;
				$item_color_id=0;
			}
			
			if ($data_array!="") $data_array.=",";
			
			$data_array .="(".$id.",".$update_id.",'".str_replace("'","",$$workOrderNo)."','".str_replace("'","",$$workOrderId)."','".str_replace("'","",$$workOrderDtlsId)."','".str_replace("'","",$$bookingWithoutOrder)."','".str_replace("'","",$$determinationId)."','".str_replace("'","",$$itemProdId)."','".str_replace("'","",$$itemgroupid)."','".str_replace("'","",$$itemdescription)."','".str_replace("'","",$color_id)."','".str_replace("'","",$size_id)."','".str_replace("'","",$$itemSize)."','".str_replace("'","",$$countName)."','".str_replace("'","",$$yarnCompositionItem1)."','".str_replace("'","",$$yarnCompositionPercentage1)."','".str_replace("'","",$$yarnCompositionItem2)."','".str_replace("'","",$$yarnCompositionPercentage2)."','".str_replace("'","",$$composition)."','".str_replace("'","",$$construction)."','".str_replace("'","",$$yarnType)."','".str_replace("'","",$$gsm)."','".str_replace("'","",$$diawidth)."','".str_replace("'","",$$weight)."','".str_replace("'","",$item_color_id)."','".str_replace("'","",$$uom)."',".$$quantity.",".$$rate.",".$$amount.",".$net_pi_rate.",".$net_pi_amount.",'".str_replace("'","",$$servicetype)."','".str_replace("'","",$$brandSupRef)."','".str_replace("'","",$$gmtsitem)."','".str_replace("'","",$$embellname)."','".str_replace("'","",$$embelltype)."','".str_replace("'","",$$lot)."','".str_replace("'","",$$yarnColor)."','".str_replace("'","",$$colorRange)."',".$user_id.",'".$pc_date_time."')"; 
			
			$id=$id+1;
			
		}
		//echo "5**insert into com_pi_item_details (".$field_array_update.") Values ".$data_array_update."";die;
		
		$rID=sql_insert("com_pi_item_details",$field_array,$data_array,0);
		if($rID) $flag=1; else $flag=0;
		
		$rID2=sql_update("com_pi_master_details",$field_array_update,$data_array_update,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		}   
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'", '', $update_id)."**1";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "0**".str_replace("'", '', $update_id)."**1";
			}
			else
			{
				oci_rollback($con); 
				echo "5**0**0";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$sql_attach=sql_select("select a.lc_number from com_btb_lc_master_details a, com_btb_lc_pi b where a.id=b.com_btb_lc_master_details_id and b.pi_id='$update_id' and b.status_active=1 and b.is_deleted=0 group by a.id, a.lc_number");
		if(count($sql_attach)>0)
		{
			$lc_number=$sql_attach[0][csf('lc_number')];
			echo "14**".$lc_number."**1"; 
			die;	
		}
		
		$sql_app=sql_select("select approved from com_pi_master_details where id='$update_id' and approved=1");
		if(count($sql_app)>0)
		{
			echo "16**1**1"; 
			die;	
		}
		
		$field_array_update="pi_id*work_order_no*work_order_id*work_order_dtls_id*booking_without_order*determination_id*item_prod_id*item_group*item_description*color_id*size_id*item_size*count_name*yarn_composition_item1*yarn_composition_percentage1*yarn_composition_item2*yarn_composition_percentage2*fabric_composition*fabric_construction*yarn_type*gsm*dia_width*weight*item_color*uom*quantity*rate*amount*net_pi_rate*net_pi_amount*service_type*brand_supplier*gmts_item_id*embell_name*embell_type*lot_no*yarn_color*color_range*updated_by*update_date";
			
		$field_array="id,pi_id,work_order_no,work_order_id,work_order_dtls_id,booking_without_order,determination_id,item_prod_id,item_group,item_description,color_id,size_id,item_size,count_name,yarn_composition_item1,yarn_composition_percentage1,yarn_composition_item2,yarn_composition_percentage2,fabric_composition,fabric_construction,yarn_type,gsm,dia_width,weight,item_color,uom,quantity,rate,amount,net_pi_rate,net_pi_amount,service_type,brand_supplier,gmts_item_id,embell_name,embell_type,lot_no,yarn_color,color_range,inserted_by,insert_date"; 
		
		$field_array_update2="total_amount*upcharge*discount*net_total_amount";
		if($cbo_currency_id==1)
		{
			$txt_total_amount=number_format($txt_total_amount,$dec_place[4],'.','');
			$txt_total_amount_net=number_format($txt_total_amount_net,$dec_place[4],'.','');
		}
		else
		{
			$txt_total_amount=number_format($txt_total_amount,$dec_place[5],'.','');
			$txt_total_amount_net=number_format($txt_total_amount_net,$dec_place[5],'.','');
		}
		$data_array_update2=$txt_total_amount."*'".$txt_upcharge."'*'".$txt_discount."'*".$txt_total_amount_net;
		
		$id = return_next_id( "id","com_pi_item_details", 1 );
		$data_array==""; $data_array_update=array();
		
		for($i=1;$i<=$total_row;$i++)
		{
			$updateIdDtls="updateIdDtls_".$i;
			$colorName="colorName_".$i; 
			$sizeName="sizeName_".$i;
			$countName="countName_".$i;
			$yarnCompositionItem1="yarnCompositionItem1_".$i;
			$yarnCompositionPercentage1="yarnCompositionPercentage1_".$i;
			$yarnCompositionItem2="yarnCompositionItem2_".$i;
			$yarnCompositionPercentage2="yarnCompositionPercentage2_".$i;			 
			$yarnType="yarnType_".$i;
			$uom="uom_".$i;
			$quantity="quantity_".$i;
			$rate="rate_".$i;
			$amount="amount_".$i;
			$workOrderNo="workOrderNo_".$i;
			$workOrderId="hideWoId_".$i;
			$workOrderDtlsId="hideWoDtlsId_".$i;
			$itemProdId="itemProdId_".$i;
			$determinationId="hideDeterminationId_".$i; 
			$construction="construction_".$i; 
			$composition="composition_".$i;
			$gsm="gsm_".$i;
			$diawidth="diawidth_".$i;
			$weight="weight_".$i;
			$itemgroupid="itemgroupid_".$i;
			$itemdescription="itemdescription_".$i;
			$servicetype="servicetype_".$i;
			$item_color="itemColor_".$i;
			$itemSize="itemSize_".$i;
			$brandSupRef="brandSupRef_".$i;
			$lot="lot_".$i;
			$yarnColor="yarnColor_".$i;
			$colorRange="colorRange_".$i;
			$gmtsitem="gmtsitem_".$i;
			$embellname="embellname_".$i;
			$embelltype="embelltype_".$i;
			$bookingWithoutOrder="bookingWithoutOrder_".$i;
						
			$perc=(str_replace("'","",$$amount)/$txt_total_amount)*100;
			$net_pi_amount=($perc*$txt_total_amount_net)/100;
			$net_pi_rate=$net_pi_amount/str_replace("'","",$$quantity);
			
			if($cbo_currency_id==1)
				$net_pi_amount=number_format($net_pi_amount,$dec_place[4],'.','');
			else
				$net_pi_amount=number_format($net_pi_amount,$dec_place[5],'.','');
					
			$net_pi_rate=number_format($net_pi_rate,$dec_place[3],'.','');

 			  
			if(str_replace("'","",$$colorName)!="")
			{ 
				if (!in_array(str_replace("'","",$$colorName),$new_array_color))
				{
					$color_id = return_id( str_replace("'","",$$colorName), $color_library, "lib_color", "id,color_name");  
					$new_array_color[$color_id]=str_replace("'","",$$colorName);
				}
				else $color_id =  array_search(str_replace("'","",$$colorName), $new_array_color); 
			}
			else
			{
				$color_id=0;
			}
			
			if($cbo_item_category_id==4 || $cbo_item_category_id==12)
			{
				if(str_replace("'","",$$sizeName)!="")
				{
					if (!in_array(str_replace("'","",$$sizeName),$new_array_size))
					{
					  $size_id = return_id( str_replace("'","",$$sizeName), $size_library, "lib_size", "id,size_name");  
					  $new_array_size[$size_id]=str_replace("'","",$$sizeName);
					}
					else $size_id =  array_search(str_replace("'","",$$sizeName), $new_array_size); 
				}
				else
				{
					$size_id=0;
				}
				
				if(str_replace("'","",$$item_color)!="")
				{
					if (!in_array(str_replace("'","",$$item_color),$new_array_color))
					{
						$item_color_id = return_id( str_replace("'","",$$item_color), $color_library, "lib_color", "id,color_name");  
						$new_array_color[$item_color_id]=str_replace("'","",$$item_color);
					}
					else $item_color_id =  array_search(str_replace("'","",$$item_color), $new_array_color); 
				}
				else
				{
					$item_color_id=0;
				}
			}
			else
			{
				$size_id=0;
				$item_color_id=0;
			}
			
			if(str_replace("'","",$$updateIdDtls)!="")
			{
				$id_arr[]=str_replace("'",'',$$updateIdDtls);
				$data_array_update[str_replace("'",'',$$updateIdDtls)] = explode("*",($update_id."*'".str_replace("'","",$$workOrderNo)."'*'".str_replace("'","",$$workOrderId)."'*'".str_replace("'","",$$workOrderDtlsId)."'*'".str_replace("'","",$$bookingWithoutOrder)."'*'".str_replace("'","",$$determinationId)."'*'".str_replace("'","",$$itemProdId)."'*'".str_replace("'","",$$itemgroupid)."'*'".str_replace("'","",$$itemdescription)."'*'".str_replace("'","",$color_id)."'*'".str_replace("'","",$size_id)."'*'".str_replace("'","",$$itemSize)."'*'".str_replace("'","",$$countName)."'*'".str_replace("'","",$$yarnCompositionItem1)."'*'".str_replace("'","",$$yarnCompositionPercentage1)."'*'".str_replace("'","",$$yarnCompositionItem2)."'*'".str_replace("'","",$$yarnCompositionPercentage2)."'*'".str_replace("'","",$$composition)."'*'".str_replace("'","",$$construction)."'*'".str_replace("'","",$$yarnType)."'*'".str_replace("'","",$$gsm)."'*'".str_replace("'","",$$diawidth)."'*'".str_replace("'","",$$weight)."'*'".str_replace("'","",$item_color_id)."'*'".str_replace("'","",$$uom)."'*".$$quantity."*".$$rate."*".$$amount."*".$net_pi_rate."*".$net_pi_amount."*'".str_replace("'","",$$servicetype)."'*'".str_replace("'","",$$brandSupRef)."'*'".str_replace("'","",$$gmtsitem)."'*'".str_replace("'","",$$embellname)."'*'".str_replace("'","",$$embelltype)."'*'".str_replace("'","",$$lot)."'*'".str_replace("'","",$$yarnColor)."'*'".str_replace("'","",$$colorRange)."'*".$user_id."*'".$pc_date_time."'"));
			}
			else
			{
				if($data_array!="") $data_array.=","; 		
						
				$data_array .="(".$id.",".$update_id.",'".str_replace("'","",$$workOrderNo)."','".str_replace("'","",$$workOrderId)."','".str_replace("'","",$$workOrderDtlsId)."','".str_replace("'","",$$bookingWithoutOrder)."','".str_replace("'","",$$determinationId)."','".str_replace("'","",$$itemProdId)."','".str_replace("'","",$$itemgroupid)."','".str_replace("'","",$$itemdescription)."','".str_replace("'","",$color_id)."','".str_replace("'","",$size_id)."','".str_replace("'","",$$itemSize)."','".str_replace("'","",$$countName)."','".str_replace("'","",$$yarnCompositionItem1)."','".str_replace("'","",$$yarnCompositionPercentage1)."','".str_replace("'","",$$yarnCompositionItem2)."','".str_replace("'","",$$yarnCompositionPercentage2)."','".str_replace("'","",$$composition)."','".str_replace("'","",$$construction)."','".str_replace("'","",$$yarnType)."','".str_replace("'","",$$gsm)."','".str_replace("'","",$$diawidth)."','".str_replace("'","",$$weight)."','".str_replace("'","",$item_color_id)."','".str_replace("'","",$$uom)."',".$$quantity.",".$$rate.",".$$amount.",".$net_pi_rate.",".$net_pi_amount.",'".str_replace("'","",$$servicetype)."','".str_replace("'","",$$brandSupRef)."','".str_replace("'","",$$gmtsitem)."','".str_replace("'","",$$embellname)."','".str_replace("'","",$$embelltype)."','".str_replace("'","",$$lot)."','".str_replace("'","",$$yarnColor)."','".str_replace("'","",$$colorRange)."',". $user_id.",'".$pc_date_time."')"; 
				$id=$id+1;
			}
		}

	 	$flag=1;
		if(count($data_array_update)>0)
		{
			$rID=execute_query(bulk_update_sql_statement( "com_pi_item_details", "id", $field_array_update, $data_array_update, $id_arr ));
			//$rID=bulk_update_sql_statement( "com_pi_item_details", "id", $field_array_update, $data_array_update, $id_arr );
			//echo "6**0**1".$rID;
			if($rID) $flag=1; else $flag=0;
		}
		
		if($data_array!="")
		{
			$rID2=sql_insert("com_pi_item_details",$field_array,$data_array,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		if($txt_deleted_id!="")
		{
			$field_array_status="updated_by*update_date*status_active*is_deleted";
			$data_array_status=$user_id."*'".$pc_date_time."'*0*1";
	
			$rID3=sql_multirow_update("com_pi_item_details",$field_array_status,$data_array_status,"id",$txt_deleted_id,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
		}

		$rID4=sql_update("com_pi_master_details",$field_array_update2,$data_array_update2,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID4) $flag=1; else $flag=0; 
		} 
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'", '', $update_id)."**1";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "1**".str_replace("'", '', $update_id)."**1";
			}
			else
			{
				oci_rollback($con);
				echo "6**0**1";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$sql_attach=sql_select("select a.lc_number from com_btb_lc_master_details a, com_btb_lc_pi b where a.id=b.com_btb_lc_master_details_id and b.pi_id='$update_id' and b.status_active=1 and b.is_deleted=0 group by a.id, a.lc_number");
		if(count($sql_attach)>0)
		{
			$lc_number=$sql_attach[0][csf('lc_number')];
			echo "14**".$lc_number."**1"; 
			die;	
		}
		
		$sql_app=sql_select("select approved from com_pi_master_details where id='$update_id' and approved=1");
		if(count($sql_app)>0)
		{
			echo "16**1**1"; 
			die;	
		}
		
		$flag=1;

		$field_array_update_mst="total_amount*upcharge*discount*net_total_amount";
		if($cbo_currency_id==1)
		{
			$txt_total_amount=number_format($txt_total_amount,$dec_place[4],'.','');
			$txt_total_amount_net=number_format($txt_total_amount_net,$dec_place[4],'.','');
		}
		else
		{
			$txt_total_amount=number_format($txt_total_amount,$dec_place[5],'.','');
			$txt_total_amount_net=number_format($txt_total_amount_net,$dec_place[5],'.','');
		}
		
		$data_array_update_mst=$txt_total_amount."*'".$txt_upcharge."'*'".$txt_discount."'*".$txt_total_amount_net;
		
		$field_array_update="quantity*rate*amount*net_pi_rate*net_pi_amount*updated_by*update_date";
		for($i=1;$i<=$total_row;$i++)
		{
			$updateIdDtls="updateIdDtls_".$i;
			$quantity="quantity_".$i;
			$rate="rate_".$i;
			$amount="amount_".$i;
			
			$perc=(str_replace("'","",$$amount)/$txt_total_amount)*100;
			$net_pi_amount=($perc*$txt_total_amount_net)/100;
			$net_pi_rate=$net_pi_amount/str_replace("'","",$$quantity);
			
			if($cbo_currency_id==1)
				$net_pi_amount=number_format($net_pi_amount,$dec_place[4],'.','');
			else
				$net_pi_amount=number_format($net_pi_amount,$dec_place[5],'.','');
					
			$net_pi_rate=number_format($net_pi_rate,$dec_place[3],'.','');

			if(str_replace("'","",$$updateIdDtls)!="")
			{
				$id_arr[]=str_replace("'",'',$$updateIdDtls);
				$data_array_update[str_replace("'",'',$$updateIdDtls)] = explode(",",($$quantity.",".$$rate.",".$$amount.",".$net_pi_rate.",".$net_pi_amount.",". $user_id.",'".$pc_date_time."'"));
			}
			
		}
		
		if($data_array_update!="")
		{
			$rID2=execute_query(bulk_update_sql_statement( "com_pi_item_details", "id", $field_array_update, $data_array_update, $id_arr ));
			if($rID2) $flag=1; else $flag=0;
			//$rID=bulk_update_sql_statement( "com_pi_item_details", "id", $field_array_update, $data_array_update, $id_arr );
		}
		
		if($txt_deleted_id!="")
		{
			$field_array_status="updated_by*update_date*status_active*is_deleted";
			$data_array_status=$user_id."*'".$pc_date_time."'*0*1";
	
			$delete=sql_multirow_update("com_pi_item_details",$field_array_status,$data_array_status,"id",$txt_deleted_id,0);
			if($flag==1) 
			{
				if($delete) $flag=1; else $flag=0; 
			} 
		}
		
		$rID=sql_update("com_pi_master_details",$field_array_update_mst,$data_array_update_mst,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} 

		if($txt_total_amount>0) $button_status=1; else $button_status=0;
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'", '', $update_id)."**$button_status";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "7**".str_replace("'", '', $update_id)."**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);    
				echo "2**".str_replace("'", '', $update_id)."**$button_status";
			}
			else
			{
				oci_rollback($con); 
				echo "7**".str_replace("'", '', $update_id)."**1";
			}
		}
		
		disconnect($con);
		die;
	}
}

//---------------------------------------------- Start Pi Details -----------------------------------------------------------------------//

 if( $action == 'pi_details' ) 
 {	
	$data = explode( '_', $data );
	$pi_basis_id=$data[0];
	$item_category_id=$data[1];
	$type=$data[2];
	$pi_id=$data[3];
	
	if($item_category_id!=0)
	{
	?>
    <table class="rpt_table" width="100%" cellspacing="0" cellpadding="0" border="1" rules="all" id="tbl_pi_item">
		<?php
        if($item_category_id==1)
        {
		?>
        	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th class="must_entry_caption">WO</th>
                <?php 
                } 
                ?>
                <th class="must_entry_caption">Color</th>
                <th class="must_entry_caption">Count</th>
                <th class="must_entry_caption">Composition</th>
                <th class="must_entry_caption">Yarn Type</th>
                <th>UOM</th>
                <th class="must_entry_caption">Quantity</th>
                <th class="must_entry_caption">Rate</th>
                <th>Amount</th>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       	<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
                $disable_status=1;
            }
            else
            {
                $disable="";
                $disable_status=0;
            }
			
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, color_id, count_name, yarn_composition_item1, yarn_composition_percentage1, yarn_composition_item2, yarn_composition_percentage2, yarn_type, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id' and status_active=1 and is_deleted=0" );

            if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td>
                        <input type="text" name="colorName_1" id="colorName_1" class="text_boxes" onFocus="add_auto_complete( 1 )" style="width:<?php if( $pi_basis_id== 1 ) echo "80px;"; else echo "100px;"; ?>"  maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                    <?php
                        echo create_drop_down( "countName_1", 85, "SELECT id,yarn_count FROM lib_yarn_count WHERE status_active = 1 AND is_deleted = 0 ORDER BY yarn_count ASC",'id,yarn_count', 1, '-Select-',0,"",$disable_status); 
                    ?>                         
                    </td>
                    <td>
                        <?php
                            if( $pi_basis_id == 1 ) $composition_item1_width = 75; else $composition_item1_width = 85;
                            echo create_drop_down( "yarnCompositionItem1_1",$composition_item1_width, $composition,'', 1, '-Select-',0,"control_composition(1,'comp_one')",$disable_status); 
                        ?>    
                        
                        <input type="text" name="yarnCompositionPercentage1_1" id="yarnCompositionPercentage1_1" class="text_boxes_numeric" value="" onChange="control_composition(1,'percent_one')" style="width:25px;" <?php echo $disable; ?>/>%
                        <?php
                            if( $pi_basis_id == 1 ) $composition_item2_width = 75; else $composition_item2_width = 85;
                            echo create_drop_down( "yarnCompositionItem2_1",$composition_item2_width, $composition,'', 1, '-Select-',0,"control_composition(1,'comp_two');",$disable_status); 
                        ?>   
                         
                        <input type="text" name="yarnCompositionPercentage2_1" id="yarnCompositionPercentage2_1" class="text_boxes_numeric" value="" onChange="control_composition(1,'percent_two')" style="width:25px;" <?php echo $disable; ?>/>%
                    </td>
                    <td>
                        <?php
                            if( $pi_basis_id == 1 ) $yarn_type_width = 90; else $yarn_type_width = 100;
                            echo create_drop_down( "yarnType_1",$composition_item1_width,$yarn_type,'', 1,'-Select-',0,"",$disable_status); 
                        ?>    
                    </td>
                    <td>
                        <?php
                            if( $pi_basis_id == 1 ) $yarn_uom = 60; else $yarn_uom = 85;
                            echo create_drop_down( "uom_1", $yarn_uom, $unit_of_measurement,'', 0, '',12,'',1,12); 
                        ?>
                         
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:61px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:45px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:75px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count WHERE status_active = 1 AND is_deleted = 0 ORDER BY yarn_count",'id','yarn_count');
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td>
                            <input type="text" name="colorName_<?php echo $i; ?>" id="colorName_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" style="width:<?php if( $pi_basis_id == 1 ) echo "80px;"; else echo "100px;"; ?>"  maxlength="50" <?php echo $disable; ?> value="<?php echo $color_library[$row[csf('color_id')]]; ?>"/>
                        </td>
                        <td>
							<?php
                                echo create_drop_down( "countName_".$i, 85, $count_arr,'', 1, '-Select-',$row[csf('count_name')],"",$disable_status); 
                            ?>                         
                        </td>
                        <td>
                            <?php
                                if( $pi_basis_id == 1 ) $composition_item1_width = 75; else $composition_item1_width = 85;
                                echo create_drop_down( "yarnCompositionItem1_".$i,$composition_item1_width, $composition,'', 1, '-Select-',$row[csf('yarn_composition_item1')],"control_composition($i,'comp_one')",$disable_status); 
                            ?>    
                            <input type="text" name="yarnCompositionPercentage1_<?php echo $i; ?>" id="yarnCompositionPercentage1_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('yarn_composition_percentage1')]; ?>" onChange="control_composition(<?php echo $i; ?>,'percent_one')" style="width:25px;" <?php echo $disable; ?>/>%
                            <?php
                                if( $pi_basis_id == 1 ) $composition_item2_width = 75; else $composition_item2_width = 85;
                                echo create_drop_down( "yarnCompositionItem2_".$i,$composition_item2_width, $composition,'', 1, '-Select-',$row[csf('yarn_composition_item2')],"control_composition($i,'comp_two')",$disable_status); 
                            ?>   
                            <input type="text" name="yarnCompositionPercentage2_<?php echo $i; ?>" id="yarnCompositionPercentage2_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('yarn_composition_percentage2')]; ?>" onChange="control_composition(<?php echo $i; ?>,'percent_two')" style="width:25px;" <?php echo $disable; ?>/>%
                        </td>
                        <td>
                            <?php
                                if( $pi_basis_id == 1 ) $yarn_type_width = 90; else $yarn_type_width = 100;
                                echo create_drop_down( "yarnType_".$i,$composition_item1_width,$yarn_type,'', 1,'-Select-',$row[csf('yarn_type')],"",$disable_status); 
                            ?>    
                        </td>
                        <td>
                            <?php
                                if( $pi_basis_id == 1 ) $yarn_uom = 60; else $yarn_uom = 85;
                                echo create_drop_down( "uom_".$i, $yarn_uom, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,12); 
                            ?>
                             
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
            </tbody>
            <tfoot class="tbl_bottom">
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Total</td>
                    <td>
                        <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:75px;" readonly/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Upcharge</td>
                    <td>
                        <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Discount</td>
                    <td>
                        <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Net Total</td>
                    <td>
                        <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:75px;" readonly/>
                    </td>
                    <?php 
                    if($pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
            </tfoot>
        <?php
        }
		else if($item_category_id==2 || $item_category_id==13)
		{
		?>
        	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th>WO</th>
                <?php 
                } 
                ?>
                <th class="must_entry_caption">Construction</th>
                <th>Composition</th>
                <th class="must_entry_caption">Color</th>					
                <th>GSM</th>
                <th class="must_entry_caption">Dia/Width</th>
                <th>UOM</th>
                <th class="must_entry_caption">Quantity</th>
                <th class="must_entry_caption">Rate</th>
                <th>Amount</th>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       		<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
                $disable_status=1;
            }
            else
            {
                $disable="";
                $disable_status=0;
            }
			
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, determination_id, color_id, fabric_construction, fabric_composition, gsm, dia_width, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id' and quantity>0 and status_active=1 and is_deleted=0" );

			if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td> 
                        <input type="text" name="construction_1" id="construction_1" class="text_boxes" style="width:110px" onDblClick="openmypage_fabricDescription(1)" placeholder="Double Click To Search" readonly <?php echo $disable; ?>/> <!--onFocus="add_auto_complete( 1 );"-->
                        <input type="hidden" name="hideDeterminationId_1" id="hideDeterminationId_1" readonly />
                    </td>
                    <td>
                        <input type="text" name="composition_1" id="composition_1" class="text_boxes" value="" style="width:120px" disabled="disabled"/> <!--onFocus="add_auto_complete(1);"-->
                    </td> 
                    <td>
                        <input type="text" name="colorName_1" id="colorName_1" class="text_boxes" onFocus="add_auto_complete( 1 )" value="" style="width:80px" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="gsm_1" id="gsm_1" class="text_boxes_numeric" value="" style="width:60px" disabled="disabled"/>
                    </td>
                    <td>
                        <input type="text" name="diawidth_1" id="diawidth_1" class="text_boxes" onFocus="add_auto_complete( 1 )" value="" style="width:70px" <?php echo $disable; ?>/>
                    </td>
                     <td>
                        <?php 
                            if( $search[0] == 1 ) $yarn_uom = 60; else $yarn_uom = 85;
                            echo create_drop_down( "uom_1", $yarn_uom, $unit_of_measurement,'', 0, '',12,'',1,12);            
                        ?>						 
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:61px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:45px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:75px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                    </td>	
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td> 
                            <input type="text" name="construction_<?php echo $i; ?>" id="construction_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('fabric_construction')]; ?>" style="width:110px" onDblClick="openmypage_fabricDescription(<?php echo $i; ?>)" placeholder="Double Click To Search" readonly <?php echo $disable; ?>/>
                            <input type="hidden" name="hideDeterminationId_<?php echo $i; ?>" id="hideDeterminationId_<?php echo $i; ?>" value="<?php echo $row[csf('determination_id')]; ?>" readonly />
                        </td>
                        <td>
                            <input type="text" name="composition_<?php echo $i; ?>" id="composition_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('fabric_composition')]; ?>" style="width:120px" disabled="disabled"/>
                        </td> 
                        <td>
 							<input type="text" name="colorName_<?php echo $i; ?>" id="colorName_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" style="width:80px" maxlength="50" <?php echo $disable; ?> value="<?php echo $color_library[$row[csf('color_id')]]; ?>"/>                        
                        </td>
                        <td>
                            <input type="text" name="gsm_<?php echo $i; ?>" id="gsm_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('gsm')]; ?>" style="width:60px"  disabled="disabled"/>
                        </td>
                        <td>
                            <input type="text" name="diawidth_<?php echo $i; ?>" id="diawidth_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" value="<?php echo $row[csf('dia_width')]; ?>" style="width:70px" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <?php
                                if( $pi_basis_id == 1 ) $yarn_uom = 60; else $yarn_uom = 85;
                                echo create_drop_down( "uom_".$i, $yarn_uom, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,12); 
                            ?>
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
        </tbody>	
        <tfoot class="tbl_bottom">
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Total</td>
                <td>
                    <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:75px;" readonly/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Upcharge</td>
                <td>
                    <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Discount</td>
                <td>
                    <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Net Total</td>
                <td>
                    <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:75px;" readonly/>
                </td>
                <?php 
                if($pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
        </tfoot>
        <?php     
		}
		else if($item_category_id==3 || $item_category_id==14)
		{
		 ?>
        	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th>WO</th>
                <?php 
                } 
                ?>
                <th>Construction</th>
                <th>Composition</th>
                <th>Color</th>
                <th>Weight</th>
                <th>Width</th>
                <th>UOM</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Amount</th>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       	<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
                $disable_status=1;
            }
            else
            {
                $disable="";
                $disable_status=0;
            }
			
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, determination_id, color_id, fabric_construction, fabric_composition, weight, dia_width, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id' and quantity>0 and status_active=1 and is_deleted=0" );

			if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td> 
                        <input type="text" name="construction_1" id="construction_1" class="text_boxes" style="width:110px" onDblClick="openmypage_fabricDescription(1)" placeholder="Double Click To Search" readonly <?php echo $disable; ?>/>
                        <input type="hidden" name="hideDeterminationId_1" id="hideDeterminationId_1" readonly />
                    </td>
                    <td>
                        <input type="text" name="composition_1" id="composition_1" class="text_boxes" value="" style="width:120px" disabled="disabled"/>
                    </td> 
                    <td>
                        <input type="text" name="colorName_1" id="colorName_1" class="text_boxes" onFocus="add_auto_complete( 1 )" value="" style="width:80px" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="weight_1" id="weight_1" class="text_boxes_numeric" value="" style="width:60px" disabled="disabled"/>
                    </td>
                    <td>
                        <input type="text" name="diawidth_1" id="diawidth_1" class="text_boxes" onFocus="add_auto_complete( 1 )" value="" style="width:70px" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <?php 
                            if( $search[0] == 1 ) $yarn_uom = 60; else $yarn_uom = 85;
                            echo create_drop_down( "uom_1", $yarn_uom, $unit_of_measurement,'', 0, '',27,'',1,27);            
                        ?>						 
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:61px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:45px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:75px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                    </td>	
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td> 
                            <input type="text" name="construction_<?php echo $i; ?>" id="construction_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('fabric_construction')]; ?>" style="width:110px" onDblClick="openmypage_fabricDescription(<?php echo $i; ?>)" placeholder="Double Click To Search" readonly <?php echo $disable; ?>/>
                            <input type="hidden" name="hideDeterminationId_<?php echo $i; ?>" id="hideDeterminationId_<?php echo $i; ?>" value="<?php echo $row[csf('determination_id')]; ?>" readonly />
                        </td>
                        <td>
                            <input type="text" name="composition_<?php echo $i; ?>" id="composition_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('fabric_composition')]; ?>" style="width:120px" disabled="disabled"/>
                        </td> 
                        <td>
 							<input type="text" name="colorName_<?php echo $i; ?>" id="colorName_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" style="width:80px" maxlength="50" <?php echo $disable; ?> value="<?php echo $color_library[$row[csf('color_id')]]; ?>"/>                        
                        </td>
                        <td>
                            <input type="text" name="weight_<?php echo $i; ?>" id="weight_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('weight')]; ?>" style="width:60px" disabled="disabled"/>
                        </td>
                        <td>
                            <input type="text" name="diawidth_<?php echo $i; ?>" id="diawidth_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" value="<?php echo $row[csf('dia_width')]; ?>" style="width:70px" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <?php
                                if( $pi_basis_id == 1 ) $yarn_uom = 60; else $yarn_uom = 85;
                                echo create_drop_down( "uom_".$i, $yarn_uom, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,27); 
                            ?>
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
            </tbody>	
			<tfoot class="tbl_bottom">
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Total</td>
                <td>
                    <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:75px;" readonly/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Upcharge</td>
                <td>
                    <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Discount</td>
                <td>
                    <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Net Total</td>
                <td>
                    <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:75px;" readonly/>
                </td>
                <?php 
                if($pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
		</tfoot>
        <?php     
		}
		else if($item_category_id==4)
		{
		 ?>
        	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th>WO</th>
                <?php 
                } 
                ?>
                <th class="must_entry_caption">Item Group</th>
                <th class="must_entry_caption">Item Description</th>
                <th>Brand/ Supp. Ref</th>
                <th>Gmts Color</th>
                <th>Gmts Size</th>
                <th class="must_entry_caption">Item Color</th>
                <th>Item Size</th>
                <th>UOM</th>
                <th class="must_entry_caption">Quantity</th>
                <th class="must_entry_caption">Rate</th>
                <th>Amount</th>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       	<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
                $disable_status=1;
            }
            else
            {
                $disable="";
                $disable_status=0;
            }
			
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, color_id, item_group, item_description, size_id, item_color, item_size, uom, quantity, rate, amount, brand_supplier, booking_without_order from com_pi_item_details where pi_id='$pi_id' and status_active=1 and is_deleted=0" );

			if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td> 
						 <?php
                            echo create_drop_down( "itemgroupid_1", 110, "SELECT id,item_name FROM lib_item_group WHERE item_category =$item_category_id AND status_active = 1 AND is_deleted = 0 ORDER BY item_name ASC",'id,item_name', 1, '-Select-',0,"get_php_form_data( this.value+'**'+'uom_1', 'get_uom', 'requires/pi_controller' );",$disable_status); 
                         ?>  
                    </td>
                    <td>
                        <input type="text" name="itemdescription_1" id="itemdescription_1" class="text_boxes" value="" style="width:130px" maxlength="200" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="brandSupRef_1" id="brandSupRef_1" class="text_boxes" value="" maxlength="150" style="width:80px" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="colorName_1" id="colorName_1" class="text_boxes" onFocus="add_auto_complete( 1 )" value="" style="width:70px" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="sizeName_1" id="sizeName_1" class="text_boxes" value="" onFocus="add_auto_complete( 1 )" style="width:60px;" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="itemColor_1" id="itemColor_1" class="text_boxes" value="" onFocus="add_auto_complete( 1 )" style="width:70px" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="itemSize_1" id="itemSize_1" class="text_boxes" value="" style="width:60px;" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <?php 
                            echo create_drop_down( "uom_1", 60, $unit_of_measurement,'', 0, '',0,'',1);            
                        ?>		
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:61px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:45px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:75px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                        <input type="hidden" name="bookingWithoutOrder_1" id="bookingWithoutOrder_1" readonly/>
                    </td>	
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td> 
						 <?php
                            echo create_drop_down( "itemgroupid_".$i, 110, "SELECT id,item_name FROM lib_item_group WHERE item_category =$item_category_id AND status_active = 1 AND is_deleted = 0 ORDER BY item_name ASC",'id,item_name', 1, '-Select-',$row[csf('item_group')],"get_php_form_data( this.value+'**'+'uom_$i', 'get_uom', 'requires/pi_controller' );",$disable_status); 
                         ?>  
                    	</td>
                        <td>
                            <input type="text" name="itemdescription_<?php echo $i; ?>" id="itemdescription_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('item_description')]; ?>" style="width:130px" maxlength="200" <?php echo $disable; ?>/>
                        </td>
                        <td>
                        	<input type="text" name="brandSupRef_<?php echo $i; ?>" id="brandSupRef_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('brand_supplier')]; ?>" style="width:80px" maxlength="150" <?php echo $disable; ?>/>
                        </td>
                        <td>
 							<input type="text" name="colorName_<?php echo $i; ?>" id="colorName_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )"  style="width:70px" maxlength="50" <?php echo $disable; ?> value="<?php echo $color_library[$row[csf('color_id')]]; ?>"/>                        
                        </td>
                        <td>
                            <input type="text" name="sizeName_<?php echo $i; ?>" id="sizeName_<?php echo $i; ?>" class="text_boxes" value="<?php echo $size_library[$row[csf('size_id')]]; ?>" onFocus="add_auto_complete( <?php echo $i; ?> )" style="width:60px;" maxlength="50" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <input type="text" name="itemColor_<?php echo $i; ?>" id="itemColor_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" value="<?php echo $color_library[$row[csf('item_color')]]; ?>" style="width:70px" maxlength="50" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <input type="text" name="itemSize_<?php echo $i; ?>" id="itemSize_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('item_size')]; ?>" style="width:60px;" maxlength="50" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <?php
                                echo create_drop_down( "uom_".$i, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1); 
                            ?>
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                            <input type="hidden" name="bookingWithoutOrder_<?php echo $i; ?>" id="bookingWithoutOrder_<?php echo $i; ?>" value="<?php echo $row[csf('booking_without_order')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
            </tbody>	
            <tfoot class="tbl_bottom">
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Total</td>
                <td>
                    <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:75px;" readonly/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Upcharge</td>
                <td>
                    <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Discount</td>
                <td>
                    <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Net Total</td>
                <td>
                    <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:75px;" readonly/>
                </td>
                <?php 
                if($pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
        </tfoot>
        <?php     
		}
		else if($item_category_id==12)
		{
		 ?>
         	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th>WO</th>
                <?php 
                } 
                ?>
                <th class="must_entry_caption">Service Type</th>
                <th class="must_entry_caption">Description</th>
                <th>Gmts Color</th>
                <th>Gmts Size</th>
                <th>Item Color</th>
                <th>Item Size</th>
                <th>UOM</th>
                <th class="must_entry_caption">Quantity</th>
                <th class="must_entry_caption">Rate</th>
                <th>Amount</th>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       	<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
                $disable_status=1;
            }
            else
            {
                $disable="";
                $disable_status=0;
            }
			
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, color_id, service_type, item_description, size_id, item_color, item_size, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id' and status_active=1 and is_deleted=0" );

			if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td> 
                    	<?php echo create_drop_down( "servicetype_1", 110, $conversion_cost_head_array,'', 1,'-Select-',0,"",$disable_status); ?> 
                    </td>
                    <td>
                        <input type="text" name="itemdescription_1" id="itemdescription_1" class="text_boxes" value="" style="width:150px" maxlength="200" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="colorName_1" id="colorName_1" class="text_boxes" onFocus="add_auto_complete( 1 )" value="" style="width:80px" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="sizeName_1" id="sizeName_1" class="text_boxes" value="" onFocus="add_auto_complete( 1 )" style="width:70px;" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="itemColor_1" id="itemColor_1" class="text_boxes" value="" onFocus="add_auto_complete( 1 )" style="width:80px" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <input type="text" name="itemSize_1" id="itemSize_1" class="text_boxes" value="" style="width:70px;" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <?php 
                            echo create_drop_down( "uom_1", 60, $unit_of_measurement,'', 0, '',0,'',0);            
                        ?>		
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:61px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:45px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:75px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                    </td>	
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td>
							<?php echo create_drop_down( "servicetype_".$i, 110, $conversion_cost_head_array,'', 1,'-Select-',$row[csf('service_type')],"",$disable_status); ?>
                        </td>
                        <td>
                            <input type="text" name="itemdescription_<?php echo $i; ?>" id="itemdescription_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('item_description')]; ?>" style="width:150px" maxlength="200" <?php echo $disable; ?>/>
                        </td>
                        <td>
 							<input type="text" name="colorName_<?php echo $i; ?>" id="colorName_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" style="width:80px" maxlength="50" <?php echo $disable; ?> value="<?php echo $color_library[$row[csf('color_id')]]; ?>"/>                        
                        </td>
                        <td>
                            <input type="text" name="sizeName_<?php echo $i; ?>" id="sizeName_<?php echo $i; ?>" class="text_boxes" value="<?php echo $size_library[$row[csf('size_id')]]; ?>" onFocus="add_auto_complete( <?php echo $i; ?> )" style="width:70px;" maxlength="50" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <input type="text" name="itemColor_<?php echo $i; ?>" id="itemColor_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" value="<?php echo $color_library[$row[csf('item_color')]]; ?>" style="width:80px" maxlength="50" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <input type="text" name="itemSize_<?php echo $i; ?>" id="itemSize_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('item_size')]; ?>" style="width:70px;" maxlength="50" <?php echo $disable; ?>/>
                        </td>
                        <td>
                            <?php
                                echo create_drop_down( "uom_".$i, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',$disable_status); 
                            ?>
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
            </tbody>	
            <tfoot class="tbl_bottom">
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Total</td>
                    <td>
                        <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:75px;" readonly/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Upcharge</td>
                    <td>
                        <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Discount</td>
                    <td>
                        <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Net Total</td>
                    <td>
                        <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:75px;" readonly/>
                    </td>
                    <?php 
                    if($pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
        	</tfoot>
        <?php     
		}
		else if($item_category_id==24)
		{
		 ?>
         	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th>WO</th>
                <?php 
                } 
                ?>
                <th class="must_entry_caption">Lot</th>
                <th>Count</th>
                <th>Yarn Description</th>
                <th>Yarn Color</th>
                <th>Color Range</th>
                <th>UOM</th>
                <th class="must_entry_caption">Quantity</th>
                <th class="must_entry_caption">Rate</th>
                <th>Amount</th>
                <?php 
                if($pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       	<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
                $disable_status=1;
				$placeholder="";
            }
            else
            {
                $disable="";
                $disable_status=0;
				$placeholder="Doublic Click";
            }
			
			$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count WHERE status_active = 1 AND is_deleted = 0 ORDER BY yarn_count",'id','yarn_count');
			$color_array=return_library_array( "select id, color_name from lib_color WHERE status_active = 1 AND is_deleted = 0 ORDER BY color_name",'id','color_name');
			
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, lot_no, yarn_color, count_name, color_range, item_description, item_prod_id, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id' and status_active=1 and is_deleted=0" );

			if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:115px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td>
                        <input type="text" name="lot_1" id="lot_1" class="text_boxes" value="" style="width:80px" maxlength="200" <?php echo $disable; ?> onDblClick="openmypage_item_desc(1);" placeholder="<?php echo $placeholder; ?>" readonly/>
                        <input type="hidden" name="itemProdId_1" id="itemProdId_1" readonly value=""/> 
                    </td>
                    <td>
                    	<?php echo create_drop_down( "countName_1", 90, $count_arr,'', 1, '-Select-', 0,"",1); ?>
                    </td>
                    <td>
                        <input type="text" name="itemdescription_1" id="itemdescription_1" class="text_boxes" value="" style="width:200px" maxlength="200" disabled/>
                    </td>
                    <td>
                    	<?php echo create_drop_down( "yarnColor_1", 110, $color_array,'', 1, '-Select-', 0,"",$disable_status); ?>
                    </td>
                    <td>
                        <?php echo create_drop_down( "colorRange_1", 110, $color_range,'', 1, '-Select-', 0,"",$disable_status); ?>
                    </td>
                    <td>
                        <?php 
							echo create_drop_down( "uom_1", 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,12); 
                        ?>		
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:61px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:45px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:75px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                    </td>	
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:115px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td>
                            <input type="text" name="lot_<?php echo $i; ?>" id="lot_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('lot_no')];?>" style="width:80px" maxlength="200" <?php echo $disable; ?> onDblClick="openmypage_item_desc(<?php echo $i; ?>);" placeholder="<?php echo $placeholder; ?>" readonly/>
                            <input type="hidden" name="itemProdId_<?php echo $i; ?>" id="itemProdId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('item_prod_id')];?>"/> 
                        </td>
                        <td>
                            <?php echo create_drop_down( "countName_".$i, 90, $count_arr,'', 1, '-Select-', $row[csf('count_name')],"",1); ?>
                        </td>
                        <td>
                            <input type="text" name="itemdescription_<?php echo $i; ?>" id="itemdescription_<?php echo $i; ?>" class="text_boxes" style="width:200px" value="<?php echo $row[csf('item_description')];?>" disabled/>
                        </td>
                        <td>
                            <?php echo create_drop_down( "yarnColor_".$i, 110, $color_array,'', 1, '-Select-', $row[csf('yarn_color')],"",$disable_status); ?>
                        </td>
                        <td>
                            <?php echo create_drop_down( "colorRange_".$i, 110, $color_range,'', 1, '-Select-', $row[csf('color_range')],"",$disable_status); ?>
                        </td>
                        <td>
                            <?php
								echo create_drop_down( "uom_".$i, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,12);
                            ?>
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
            </tbody>	
            <tfoot class="tbl_bottom">
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Total</td>
                    <td>
                        <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:75px;" readonly/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Upcharge</td>
                    <td>
                        <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Discount</td>
                    <td>
                        <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:75px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Net Total</td>
                    <td>
                        <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:75px;" readonly/>
                    </td>
                    <?php 
                    if($pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
        	</tfoot>
        <?php     
		}
		else if($item_category_id==25)
		{
		 ?>
         	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th>WO</th>
                <?php 
                } 
                ?>
                <th class="must_entry_caption">Gmts Item</th>
                <th class="must_entry_caption">Embellishment Name</th>
                <th class="must_entry_caption">Embellishment Type</th>
                <th>Gmts Color</th>
                <th>UOM</th>
                <th class="must_entry_caption">Quantity</th>
                <th class="must_entry_caption">Rate</th>
                <th>Amount</th>
                <?php 
                if($pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       	<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
                $disable_status=1;
            }
            else
            {
                $disable="";
                $disable_status=0;
            }
			
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, color_id, gmts_item_id, embell_name, embell_type, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id' and status_active=1 and is_deleted=0" );

			if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td> 
                    	<?php echo create_drop_down( "gmtsitem_1", 150, $garments_item,'', 1, '-Select-', 0,"",$disable_status); ?> 
                    </td>
                    <td>
                    	<?php echo create_drop_down( "embellname_1", 130, $emblishment_name_array,'', 1, '-Select-', 0,"load_drop_down( 'requires/pi_controller', this.value+'**'+".$disable_status."+'**'+'embelltype_1', 'load_drop_down_embelltype', 'embelltypeTd_1');",$disable_status); ?>
                    </td>
                    <td id="embelltypeTd_1">
                    	<?php echo create_drop_down( "embelltype_1", 130, $blank_array,'', 1, '-Select-', 0,"",$disable_status); ?>
                    </td>
                    <td>
                        <input type="text" name="colorName_1" id="colorName_1" class="text_boxes" value="" onFocus="add_auto_complete( 1 )" style="width:90px;" maxlength="50" <?php echo $disable; ?>/>
                    </td>
                    <td>
                        <?php 
							echo create_drop_down( "uom_1", 70, $unit_of_measurement,'', 0, '',0,'',1,2);           
                        ?>		
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:81px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:50px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:85px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                    </td>	
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td>
                        	<?php echo create_drop_down( "gmtsitem_".$i, 150, $garments_item,'', 1, '-Select-', $row[csf('gmts_item_id')],"",$disable_status); ?>
                        </td>
                        <td>
                            <?php echo create_drop_down( "embellname_".$i, 130, $emblishment_name_array,'', 1, '-Select-', $row[csf('embell_name')],"load_drop_down( 'requires/pi_controller', this.value+'**'+".$disable_status."+'**'+'embelltype_$i', 'load_drop_down_embelltype', 'embelltypeTd_$i');",$disable_status); ?>
                        </td>
                        <td id="embelltypeTd_<?php echo $i; ?>">
                        	<?php
								$emb_arr=array();
								if($row[csf('embell_name')]==1) $emb_arr=$emblishment_print_type;
								else if($row[csf('embell_name')]==2) $emb_arr=$emblishment_embroy_type;
								else if($row[csf('embell_name')]==3) $emb_arr=$emblishment_wash_type;
								else if($row[csf('embell_name')]==4) $emb_arr=$emblishment_spwork_type;
								else $emb_arr=$blank_array;
								 
								echo create_drop_down( "embelltype_".$i, 130, $emb_arr,'', 1, '-Select-', $row[csf('embell_type')],"",$disable_status); 
							?>
                        </td>
                        <td>
                            <input type="text" name="colorName_<?php echo $i; ?>" id="colorName_<?php echo $i; ?>" class="text_boxes" onFocus="add_auto_complete( <?php echo $i; ?> )" style="width:90px" maxlength="50" <?php echo $disable; ?> value="<?php echo $color_library[$row[csf('color_id')]]; ?>"/>
                        </td>
                        <td>
                            <?php
								echo create_drop_down( "uom_".$i, 70, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,2); 
                            ?>
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:81px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:50px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:85px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
            </tbody>	
            <tfoot class="tbl_bottom">
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Total</td>
                    <td style="text-align:center">
                        <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:85px;" readonly/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Upcharge</td>
                    <td style="text-align:center">
                        <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:85px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Discount</td>
                    <td style="text-align:center">
                        <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:85px;" onKeyUp="calculate_total_amount(2)"/>
                    </td>
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    <?php 
                    } 
                    ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Net Total</td>
                    <td style="text-align:center">
                        <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:85px;" readonly/>
                    </td>
                    <?php 
                    if($pi_basis_id == 2 ) 
                    { 
                    ?>
                        <td width="65">&nbsp;</td> 
                    <?php
                    }
                    ?>
                </tr>
        	</tfoot>
        <?php     
		}
		else
		{
			$item_group_arr=return_library_array( "SELECT id,item_name FROM lib_item_group",'id','item_name');
		 ?>
        	<thead>
				<?php 
                if($pi_basis_id == 1) 
                { 
                ?>
                    <th>&nbsp;</th>
                    <th>WO</th>
                <?php 
                } 
                ?>
                <th class="must_entry_caption">Item Group</th>
                <th class="must_entry_caption">Item Description</th>
                <th>Item Size</th>
                <th>UOM</th>
                <th class="must_entry_caption">Quantity</th>
                <th class="must_entry_caption">Rate</th>
                <th>Amount</th>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <th></th>
                <?php 
                } 
                ?>
            </thead>
            <tbody>
       	<?php
            if($pi_basis_id==1)
            {
                $disable="disabled='disabled'";
				$placeholder="";
            }
            else
            {
                $disable="";
				$placeholder="Doublic Click";
            }
			//echo "select id, work_order_no, work_order_id, work_order_dtls_id, item_prod_id, item_group, item_description, item_size, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id'";
			$nameArray=sql_select( "select id, work_order_no, work_order_id, work_order_dtls_id, item_prod_id, item_group, item_description, item_size, uom, quantity, rate, amount from com_pi_item_details where pi_id='$pi_id' and status_active=1 and is_deleted=0" );

			if($type==1 || count($nameArray)<1)
            {
				$tot_amnt=''; $upcharge=''; $discount=''; $tot_amnt_net=''; $txt_tot_row=0;
            ?>
                <tr class="general" id="row_1">
                    <?php 
                    if( $pi_basis_id == 1 ) 
                    { 
                    ?>
                        <td>
                            <input type="checkbox" name="workOrderChkbox_1" id="workOrderChkbox_1" value="" />
                        </td>
                        <td>
                            <input type="text" name="workOrderNo_1" id="workOrderNo_1" class="text_boxes" value="" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(1);" readonly />			
                            <input type="hidden" name="hideWoId_1" id="hideWoId_1" readonly />
                            <input type="hidden" name="hideWoDtlsId_1" id="hideWoDtlsId_1" readonly />
                        </td>
                    <?php 
                    } 
                    ?>
                    <td> 
						 <?php
                            echo create_drop_down( "itemgroupid_1", 130, $item_group_arr,'', 1, '-Select-',0,"get_php_form_data( this.value+'**'+'uom_1', 'get_uom', 'requires/pi_controller' );",1); 
                         ?>  
                    </td>
                    <td>
                        <input type="text" name="itemdescription_1" id="itemdescription_1" class="text_boxes" value="" style="width:200px" maxlength="200" <?php echo $disable; ?> onDblClick="openmypage_item_desc(1);" placeholder="<?php echo $placeholder; ?>" readonly/>
                        <input type="hidden" name="itemProdId_1" id="itemProdId_1" readonly value=""/> 
                    </td>
                    <td>
                        <input type="text" name="itemSize_1" id="itemSize_1" class="text_boxes" value="" style="width:90px;" maxlength="50" disabled="disabled"/>
                    </td>
                    <td>
                        <?php 
                            echo create_drop_down( "uom_1", 80, $unit_of_measurement,'', 0, '',0,'',1);            
                        ?>		
                    </td>
                    <td>
                        <input type="text" name="quantity_1" id="quantity_1" class="text_boxes_numeric" value="" style="width:90px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="rate_1" id="rate_1" class="text_boxes_numeric" value="" style="width:75px;" onKeyUp="calculate_amount(1)" />
                    </td>
                    <td>
                        <input type="text" name="amount_1" id="amount_1" class="text_boxes_numeric" value="" style="width:85px;" readonly/>
                        <input type="hidden" name="updateIdDtls_1" id="updateIdDtls_1" readonly/>
                    </td>	
                    <?php 
                    if( $pi_basis_id == 2 ) 
                    { 
                    ?>
                    <td width="65">
                        <input type="button" id="increase_1" name="increase_1" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( 1 )" />
                        <input type="button" id="decrease_1" name="decrease_1" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                    </td> 
                    <?php
                    }
                    ?> 
                </tr>
            <?php
            }
			else
			{
				$data_array=sql_select("select total_amount, upcharge, discount, net_total_amount from com_pi_master_details where id='$pi_id'");
				$tot_amnt=$data_array[0][csf('total_amount')]; 
				$upcharge=$data_array[0][csf('upcharge')];  
				$discount=$data_array[0][csf('discount')];  
				$tot_amnt_net=$data_array[0][csf('net_total_amount')]; 
				
				$i=1;
				foreach ($nameArray as $row)
				{
				?>
                    <tr class="general" id="row_<?php echo $i; ?>">
                        <?php 
                        if( $pi_basis_id == 1 ) 
                        { 
                        ?>
                            <td>
                                <input type="checkbox" name="workOrderChkbox_<?php echo $i; ?>" id="workOrderChkbox_<?php echo $i; ?>" value="" />
                            </td>
                            <td>
                                <input type="text" name="workOrderNo_<?php echo $i; ?>" id="workOrderNo_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('work_order_no')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $i; ?>);" readonly />			
                                <input type="hidden" name="hideWoId_<?php echo $i; ?>" id="hideWoId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('work_order_id')]; ?>" />
                                <input type="hidden" name="hideWoDtlsId_<?php echo $i;?>" id="hideWoDtlsId_<?php echo $i;?>" readonly value="<?php echo $row[csf('work_order_dtls_id')];?>"/>
                            </td>
                        <?php 
                        } 
                        ?>
                        <td> 
						 <?php
                            echo create_drop_down( "itemgroupid_".$i, 130, $item_group_arr,'', 1, '-Select-',$row[csf('item_group')],"get_php_form_data( this.value+'**'+'uom_$i', 'get_uom', 'requires/pi_controller' );",1); 
                         ?>  
                    	</td>
                        <td>
                            <input type="text" name="itemdescription_<?php echo $i; ?>" id="itemdescription_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('item_description')]; ?>" style="width:200px" maxlength="200" <?php echo $disable; ?> onDblClick="openmypage_item_desc(<?php echo $i; ?>);" readonly />
                            <input type="hidden" name="itemProdId_<?php echo $i; ?>" id="itemProdId_<?php echo $i; ?>" readonly value="<?php echo $row[csf('item_prod_id')]; ?>"/> 
                        </td>
                        <td>
                            <input type="text" name="itemSize_<?php echo $i; ?>" id="itemSize_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('item_size')]; ?>" style="width:90px;" maxlength="50"  disabled="disabled"/>
                        </td>
                        <td>
                            <?php
                                echo create_drop_down( "uom_".$i, 80, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1); 
                            ?>
                        </td>
                        <td>
                            <input type="text" name="quantity_<?php echo $i; ?>" id="quantity_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('quantity')]; ?>" style="width:90px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:75px;" onKeyUp="calculate_amount(<?php echo $i; ?>)" />
                        </td>
                        <td>
                            <input type="text" name="amount_<?php echo $i; ?>" id="amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:85px;" readonly/>
                            <input type="hidden" name="updateIdDtls_<?php echo $i; ?>" id="updateIdDtls_<?php echo $i; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/>
                        </td>
                        <?php 
                        if( $pi_basis_id == 2 ) 
                        { 
                        ?>
                        <td width="65">
                            <input type="button" id="increase_<?php echo $i; ?>" name="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
                            <input type="button" id="decrease_<?php echo $i; ?>" name="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
                        </td> 
                        <?php
                        }
                        ?> 
                    </tr>
            	<?php		
				$i++;
				}
				$txt_tot_row=$i-1;
			}
			?>
            </tbody>	
            <tfoot class="tbl_bottom">
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Total</td>
                <td style="text-align:center">
                    <input type="text" name="txt_total_amount" id="txt_total_amount" class="text_boxes_numeric" value="<?php echo $tot_amnt; ?>" style="width:85px;" readonly/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Upcharge</td>
                <td style="text-align:center">
                    <input type="text" name="txt_upcharge" id="txt_upcharge" class="text_boxes_numeric" value="<?php echo $upcharge; ?>" style="width:85px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Discount</td>
                <td style="text-align:center">
                    <input type="text" name="txt_discount" id="txt_discount" class="text_boxes_numeric" value="<?php echo $discount; ?>" style="width:85px;" onKeyUp="calculate_total_amount(2)"/>
                </td>
                <?php 
                if( $pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
            <tr>
                <?php 
                if( $pi_basis_id == 1 ) 
                { 
                ?>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                <?php 
                } 
                ?>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Net Total</td>
                <td style="text-align:center">
                    <input type="text" name="txt_total_amount_net" id="txt_total_amount_net" class="text_boxes_numeric" value="<?php echo $tot_amnt_net; ?>" style="width:85px;" readonly/>
                </td>
                <?php 
                if($pi_basis_id == 2 ) 
                { 
                ?>
                    <td width="65">&nbsp;</td> 
                <?php
                }
                ?>
            </tr>
        </tfoot>
        <?php     
		}
        ?>
    </table>
    <table width="100%">
        <tr>
            <td class="button_container" colspan="2"></td>
        </tr>
        <tr>
            <td width="15%">
                <?php
                if($pi_basis_id == 1) 
                {
                ?>
                    <input form="form_all" type="checkbox" name="check_all" id="check_all" value=""  onclick="fnCheckUnCheckAll(this.checked)"/> Check / Uncheck All
                <?php
                }
                ?>
                &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td width="80%" align="center"> 
                <input type="hidden" name="txt_deleted_id" id="txt_deleted_id" readonly/> 
                <input type="hidden" name="txt_tot_row" id="txt_tot_row" value="<?php echo $txt_tot_row; ?>" readonly/>                      
               <?php echo load_submit_buttons( $_SESSION['page_permission'], "fnc_pi_item_details", 0,0 ,"reset_form('pimasterform_2','','','txt_tot_row,0','$(\'#tbl_pi_item tbody tr:not(:first)\').remove();')",2) ; ?>
            </td>    
        </tr>				
    </table>
    <?php
	}
	exit();
}

//---------------------------------------------End Pi Details------------------------------------------------------------------------//



if ($action=="get_uom")
{
	$data=explode("**",$data);
	$database_id=$data[0];
	$field_id=$data[1];
	$nameArray=sql_select( "select order_uom from lib_item_group where id='$database_id'" );
	foreach ($nameArray as $row)
	{
	 	echo "document.getElementById('$field_id').value = ".trim($row[csf("order_uom")]).";\n";  
		die;
	}
}

if($action=="load_drop_down_embelltype")
{
	$data=explode("**",$data);
	$embell_name=$data[0];
	$disable_status=$data[1];
	$field_id=$data[2];
    
	if($embell_name==1)
		echo create_drop_down( "$field_id", 130, $emblishment_print_type,'', 1, '-Select-', 0,"",$disable_status); 
	else if($embell_name==2)
		echo create_drop_down( "$field_id", 130, $emblishment_embroy_type,'', 1, '-Select-', 0,"",$disable_status); 
	else if($embell_name==3)
		echo create_drop_down( "$field_id", 130, $emblishment_wash_type,'', 1, '-Select-', 0,"",$disable_status); 	
	else if($embell_name==4)
		echo create_drop_down( "$field_id", 130, $emblishment_spwork_type,'', 1, '-Select-', 0,"",$disable_status); 
	else
		echo create_drop_down( "$field_id", 130, $blank_array,'', 1, '-Select-', 0,"",$disable_status); 
		
	exit();		
}

//--------------------
if ($action=="fabricDescription_popup")
{
	echo load_html_head_contents("Fabric Description Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
		
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
		
		var selected_id = new Array, selected_name = new Array();
		
		function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_data' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name ='';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}
		
    </script>

</head>

<body>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:720px;margin-left:10px">
			<?php 
				if($prev_attached_id=="")
					$id_cond="";
				else
					$id_cond="and id not in($prev_attached_id)";
					
				$data_array=sql_select("select id, construction, fab_nature_id, gsm_weight from lib_yarn_count_determina_mst where fab_nature_id=$fabricNature and status_active=1 and is_deleted=0 $id_cond"); 
			?>
            <input type="hidden" name="txt_selected_id" id="txt_selected_id" class="text_boxes" value="">
            <input type="hidden" name="txt_selected" id="txt_selected" class="text_boxes" value="">
            <div style="margin-left:10px; margin-top:10px">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="680">
                    <thead>
                        <th width="50">SL</th>
                        <th width="100">Fabric Nature</th>
                        <th width="150">Construction</th>
                        <th width="100">GSM/Weight</th>
                        <th>Composition</th>
                    </thead>
                </table>
                <div style="width:700px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="680" id="tbl_list_search">  
                        <?php 
                        $i=1; 
                        foreach($data_array as $row)
                        {  
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
								
                            $comp='';
                            $construction=$row[csf('construction')];
							
                            $determ_sql=sql_select("select copmposition_id, percent from lib_yarn_count_determina_dtls where mst_id=".$row[csf('id')]." and status_active=1 and is_deleted=0");
                            foreach( $determ_sql as $d_row )
                            {
                                $comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
                            }
                            
                         ?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="text-decoration:none; cursor:pointer" id="search<?php echo $i; ?>">
                                <td width="50"><?php echo $i; ?>
                                    <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $row[csf('id')]; ?>"/>
                                    <input type="hidden" name="txt_data" id="txt_data<?php echo $i ?>" value="<?php echo $construction."**".$comp."**". $row[csf('gsm_weight')]; ?>"/>	
                                </td>
                                <td width="100"><?php echo $item_category[$row[csf('fab_nature_id')]]; ?></td>
                                <td width="150"><p><?php echo $row[csf('construction')]; ?></p></td>
                                <td width="100"><?php echo $row[csf('gsm_weight')]; ?></td>
                                <td><p><?php echo $comp; ?></p></td>
                            </tr>
                        <?php 
                        $i++; 
                        } 
                        ?>
                    </table>
                </div> 
            </div>
            <table width="700" cellspacing="0" cellpadding="0" style="border:none" align="center">
                <tr>
                    <td align="center" height="30" valign="bottom">
                        <div style="width:100%"> 
                            <div style="width:50%; float:left" align="left">
                                <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                            </div>
                            <div style="width:50%; float:left" align="left">
                            	<input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbuttonplasminus" value="Close" style="width:100px" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
		</fieldset>
	</form>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if ($action=="pi_popup")
{
	echo load_html_head_contents("PI Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 
	<script>
		/*$(document).ready(function(e) {
            load_drop_down( 'pi_controller',<?php echo $importer_id; ?>+'_'+<?php echo $item_category; ?>, 'load_supplier_dropdown', 'supplier_td' );
			$('#cbo_supplier_id').val( <?php echo $supplier_id; ?> );
        });*/
		
		function js_set_value( pi_id )
		{
			document.getElementById('txt_selected_pi_id').value=pi_id;
			parent.emailwindow.hide();
		}
		
    </script>

</head>

<body>
<div align="center" style="width:900px;">
	<form name="searchpifrm"  id="searchpifrm">
		<fieldset style="width:100%;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="800px" class="rpt_table">
                <thead>
                    <th>Company</th>
                    <th>Supplier</th>
                    <th>PI Number</th>
                    <th>Date Range</th>
                    <th>
                    	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="txt_item_category" id="txt_item_category" class="text_boxes" style="width:70px" value="<?php echo $item_category; ?>">
                    	<input type="hidden" name="txt_selected_pi_id" id="txt_selected_pi_id" class="text_boxes" style="width:70px" value="">   
                    </th> 
                </thead>
                <tr class="general">
                    <td>
						 <?php echo create_drop_down( "cbo_importer_id", 151,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, 'Select',$importer_id,"load_drop_down( 'pi_controller',this.value+'_'+$item_category, 'load_supplier_dropdown', 'supplier_td' );",0); ?>       
                    </td>
                    <td id="supplier_td">	
                        <?php
							echo create_drop_down( "cbo_supplier_id", 151, $blank_array,"", 1, "-- Select Supplier --", 0, "" );
						?> 
                    </td>                 
                    <td> 
                        <input type="text" name="txt_pi_no" id="txt_pi_no" class="text_boxes" style="width:120px">
                    </td>						
                    <td align="center">
                      <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">To
					  <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_pi_no').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_item_category').value+'_'+document.getElementById('cbo_importer_id').value+'_'+document.getElementById('cbo_supplier_id').value, 'create_pi_search_list_view', 'search_div', 'pi_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                     </td>
                </tr>
                <tr>
                	<td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
           </table>
           <table width="100%" style="margin-top:5px">
                <tr>
                    <td colspan="5">
                        <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table> 
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	$('#cbo_supplier_id').val( <?php echo $supplier_id; ?> );
</script>
</html>
<?php
}

if($action=="create_pi_search_list_view")
{
	$data=explode('_',$data);
	 
	if ($data[0]!="") $pi_number=" and pi_number like '%".trim($data[0])."%'"; else { $pi_number = ''; }
	if ($data[1]!="" &&  $data[2]!="")
	{
		if($db_type==0)
		{
			$pi_date = "and pi_date between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'";
		}
		else
		{
			$pi_date = "and pi_date between '".change_date_format($data[1],'','',1)."' and '".change_date_format($data[2],'','',1)."'";
		}
	}
	else
	{
		$pi_date ="";
	}
	$item_category_id =$data[3];
	$importer_id =$data[4];
	if($data[5]==0) $supplier_id="%%"; else $supplier_id =$data[5];
	  
	if($importer_id==0) { echo "Please Select Company First."; die; }
	
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$supplier=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	
	$arr=array (2=>$item_category,3=>$comp,4=>$supplier,7=>$pi_basis);
	 
	
	$sql= "select id,pi_number,pi_date,item_category_id,importer_id,supplier_id,last_shipment_date,hs_code,pi_basis_id from com_pi_master_details where supplier_id like '$supplier_id' and importer_id = $importer_id and item_category_id = $item_category_id $pi_number $pi_date and status_active=1 and is_deleted=0 order by pi_number";  
	
	echo create_list_view("list_view", "PI No,PI Date,Item Category,Importer,Supplier,Last Shipment Date,HS Code,PI Basis", "100,80,80,130,100,90,100","880","270",0, $sql , "js_set_value", "id", "", 1, "0,0,item_category_id,importer_id,supplier_id,0,0,pi_basis_id", $arr , "pi_number,pi_date,item_category_id,importer_id,supplier_id,last_shipment_date,hs_code,pi_basis_id", "",'','0,3,0,0,0,3,0,0');
	 
exit();	
} 

if ($action=="populate_data_from_search_popup")
{
	$data_array=sql_select("select id,item_category_id,importer_id,supplier_id,pi_number,pi_date,last_shipment_date,pi_validity_date,currency_id,source,hs_code,internal_file_no,intendor_name,pi_basis_id,remarks,approved,approved from com_pi_master_details where id='$data'");
	foreach ($data_array as $row)
	{
		echo "document.getElementById('cbo_item_category_id').value = '".$row[csf("item_category_id")]."';\n";  
		echo "document.getElementById('cbo_importer_id').value = '".$row[csf("importer_id")]."';\n";  
		
		echo "load_drop_down('requires/pi_controller',".$row[csf("importer_id")]."+'_'+".$row[csf("item_category_id")].", 'load_supplier_dropdown', 'supplier_td' );\n";
		
		if($row[csf("last_shipment_date")]=="0000-00-00" || $row[csf("last_shipment_date")]=="") $last_shipment_date=""; else $last_shipment_date=change_date_format($row[csf("last_shipment_date")]);
		if($row[csf("pi_validity_date")]=="0000-00-00" || $row[csf("pi_validity_date")]=="") $pi_validity_date=""; else $pi_validity_date=change_date_format($row[csf("pi_validity_date")]);
		echo "document.getElementById('cbo_supplier_id').value = '".$row[csf("supplier_id")]."';\n";  
		echo "document.getElementById('pi_number').value = '".$row[csf("pi_number")]."';\n";  
		echo "document.getElementById('pi_date').value = '".change_date_format($row[csf("pi_date")])."';\n";  
		echo "document.getElementById('last_shipment_date').value = '".$last_shipment_date."';\n";  
		echo "document.getElementById('pi_validity_date').value = '".$pi_validity_date."';\n";  
		echo "document.getElementById('cbo_currency_id').value = '".$row[csf("currency_id")]."';\n";  
		echo "document.getElementById('cbo_source_id').value = '".$row[csf("source")]."';\n";  
		echo "document.getElementById('hs_code').value = '".$row[csf("hs_code")]."';\n";  
		echo "document.getElementById('txt_internal_file_no').value = '".$row[csf("internal_file_no")]."';\n";  
		echo "document.getElementById('intendor_name').value = '".$row[csf("intendor_name")]."';\n";  
		echo "document.getElementById('cbo_pi_basis_id').value = '".$row[csf("pi_basis_id")]."';\n";  
		echo "document.getElementById('txt_remarks').value = '".($row[csf("remarks")])."';\n";
		echo "document.getElementById('hide_approved_status').value = '".$row[csf("approved")]."';\n";
		echo "document.getElementById('update_id').value = '".$row[csf("id")]."';\n";
		
		echo "$('#cbo_item_category_id').attr('disabled','true')".";\n";
		echo "$('#cbo_pi_basis_id').attr('disabled','true')".";\n";
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_pi_mst',1);\n";
		
		echo "document.getElementById('is_approved').value = '".$row[csf("approved")]."';\n";
		
		if($row[csf("approved")]==1)
	  	{
			echo "$('#approved').text('Approved');\n"; 
	  	}
	  	else
	  	{
		 	echo "$('#approved').text('');\n";
	  	}	 
	}
	
	exit();
}
 
if ($action=="wo_popup")
{
	echo load_html_head_contents("WO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
	
		var item_category=<?php echo $item_category; ?>;
		
		/*$(document).ready(function(e) {
            load_drop_down( 'pi_controller',<?phpecho $importer_id; ?>+'_'+item_category, 'load_supplier_dropdown', 'supplier_td' );
			$('#cbo_supplier_id').val( <?phpecho $supplier_id; ?> );
        });*/
		
		var selected_id = new Array, selected_name = new Array();
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) 
		{
			if (str!="") str=str.split("_");

			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			
			if(item_category==4)
				var id_sensitivity=str[1]+"_"+str[2]+"_"+str[3];
			else 
				var id_sensitivity=str[1];
				
			if( jQuery.inArray( id_sensitivity, selected_id ) == -1 ) {
				selected_id.push( id_sensitivity );
			}
			else 
			{
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == id_sensitivity ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#txt_selected_wo_id').val( id );
		}
	
		function reset_hide_field()
		{
			$('#txt_selected_wo_id').val( '' );
			selected_id = new Array(); selected_name = new Array();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:900px;">
	<form name="searchwofrm"  id="searchwofrm">
		<fieldset style="width:100%;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="800" class="rpt_table">
                <thead>
                    <th>Company</th>
                    <th>Supplier</th>
                    <th>WO Number</th>
                    <th>WO Date Range</th>
                    <th>
                    	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" onClick="reset_hide_field()" />
                        <input type="hidden" name="txt_item_category" id="txt_item_category" class="text_boxes" style="width:70px" value="<?php echo $item_category; ?>">
                    	<input type="hidden" name="txt_selected_wo_id" id="txt_selected_wo_id" class="text_boxes" value="">   
                    </th> 
                </thead>
                <tr class="general">
                    <td>
						 <?php echo create_drop_down( "cbo_importer_id", 151,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, 'Select',$importer_id,"load_drop_down( 'pi_controller',this.value+'_'+$item_category, 'load_supplier_dropdown', 'supplier_td' );",0); ?>       
                    </td>
                    <td id="supplier_td">	
                        <?php
							echo create_drop_down( "cbo_supplier_id", 151, $blank_array,"", 1, "-- Select Supplier --", 0, "" );
						?> 
                    </td>                 
                    <td> 
                        <input type="text" name="txt_wo_no" id="txt_wo_no" class="text_boxes" style="width:120px">
                    </td>						
                    <td align="center">
                      <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">To
					  <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_wo_no').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_item_category').value+'_'+document.getElementById('cbo_importer_id').value+'_'+document.getElementById('cbo_supplier_id').value, 'create_wo_search_list_view', 'search_div', 'pi_controller', 'setFilterGrid(\'tbl_list_search\',-1);reset_hide_field();')" style="width:100px;" />
                     </td>
                </tr>
                <tr>
                	<td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
           </table>
           <table width="100%" style="margin-top:5px">
                <tr>
                    <td colspan="5">
                        <div style="width:100%; margin-top:10px; margin-left:10px" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table> 
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	$('#cbo_supplier_id').val( <?php echo $supplier_id; ?> );
</script>
</html>
<?php
}

if($action=="create_wo_search_list_view")
{
	$data = explode("_",$data);
	
	$item_category_id =$data[3];
	$company_id =$data[4];
	
	if($company_id==0) { echo "Please Select Company First."; die; }
	if($data[5]==0) $supplier_id="%%"; else $supplier_id =$data[5];
	
	if($item_category_id==2 || $item_category_id==3 || $item_category_id==4 || $item_category_id==12 || $item_category_id==24 || $item_category_id==25)
	{
		if ($data[1]!="" &&  $data[2]!="") 
		{
			if($db_type==0)
			{
				$wo_date_cond = "and a.booking_date between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'";
				$sample_wo_date_cond = "and s.booking_date between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'";
			}
			else
			{
				$wo_date_cond = "and a.booking_date between '".change_date_format($data[1],'','',1)."' and '".change_date_format($data[2],'','',1)."'";
				$sample_wo_date_cond = "and s.booking_date between '".change_date_format($data[1],'','',1)."' and '".change_date_format($data[2],'','',1)."'";
			}
		}
		else 
		{
			$wo_date_cond ="";
			$sample_wo_date_cond ="";
		}
	}
	else
	{
		if ($data[1]!="" &&  $data[2]!="")
		{
			if($db_type==0)
			{
				$wo_date_cond = "and a.wo_date between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'"; 
			}
			else
			{
				$wo_date_cond = "and a.wo_date between '".change_date_format($data[1],'','',1)."' and '".change_date_format($data[2],'','',1)."'"; 
			}
		}
		else
		{
			$wo_date_cond ="";
		}
	}

	if($item_category_id==1)
	{
		if ($data[0]!="") $wo_number=" and a.wo_number like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql = "select a.wo_number_prefix_num, a.wo_number, a.wo_date, a.supplier_id, b.id, b.color_name, b.yarn_count, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.uom from wo_non_order_info_mst a, wo_non_order_info_dtls b where a.id=b.mst_id and a.company_name=$company_id and a.item_category=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id"; 
		//echo $sql;die;
		//$result = sql_select($sql);
	
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$yarn_count = return_library_array('SELECT id,yarn_count FROM lib_yarn_count','id','yarn_count');
		$arr=array (2=>$supplier_arr,3=>$color_library,4=>$yarn_count,5=>$composition,7=>$composition,9=>$yarn_type,10=>$unit_of_measurement);
		
		echo create_list_view("tbl_list_search", "WO Number, WO Date, Supplier, Color, Count, Item 1st, Perc. 1st, Item 2nd, Perc. 2nd, Yarn Type, UOM", "90,80,100,80,60,70,60,70,60,80","880","250",0, $sql, "js_set_value", "id", "", 1, "0,0,supplier_id,color_name,yarn_count,yarn_comp_type1st,0,yarn_comp_type2nd,0,yarn_type,uom", $arr , "wo_number_prefix_num,wo_date,supplier_id,color_name,yarn_count,yarn_comp_type1st,yarn_comp_percent1st,yarn_comp_type2nd,yarn_comp_percent2nd,yarn_type,uom", "",'','0,3,0,0,0,0,2,0,2,0,0','',1);
	}
	else if($item_category_id==2)
	{
		if ($data[0]!="") $wo_number=" and a.booking_no like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql = "select a.booking_no_prefix_num, a.booking_no, a.booking_date, a.supplier_id, b.id, b.fabric_color_id, b.construction, b.copmposition, b.gsm_weight, b.dia_width, b.uom from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$company_id and a.item_category=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id"; 
		//echo $sql;die;
		//$result = sql_select($sql);
	
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$arr=array (2=>$supplier_arr,3=>$color_library,8=>$unit_of_measurement);
		
		echo  create_list_view("tbl_list_search", "WO Number, WO Date, Supplier, Color, Construction, Copmposition, GSM, Dia/Width, UOM", "105,80,100,80,120,120,60,70,60","880","250",0, $sql, "js_set_value", "id", "", 1, "0,0,supplier_id,fabric_color_id,0,0,0,0,uom", $arr , "booking_no_prefix_num,booking_date,supplier_id,fabric_color_id,construction,copmposition,gsm_weight,dia_width,uom", "",'','0,3,0,0,0,0,0,0,0','',1);
	}
	else if($item_category_id==3)
	{
		if ($data[0]!="") $wo_number=" and a.booking_no like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql = "select a.booking_no_prefix_num,a.booking_no, a.booking_date, a.supplier_id, b.id, b.fabric_color_id, b.construction, b.copmposition, b.gsm_weight, b.dia_width, b.uom from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$company_id and a.item_category=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id"; 
		//echo $sql;die;
		//$result = sql_select($sql);
	
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$arr=array (2=>$supplier_arr,3=>$color_library,8=>$unit_of_measurement);
		
		echo  create_list_view("tbl_list_search", "WO Number, WO Date, Supplier, Color, Construction, Copmposition, Weight, Width, UOM", "105,80,100,80,120,120,60,70,60","880","250",0, $sql, "js_set_value", "id", "", 1, "0,0,supplier_id,fabric_color_id,0,0,0,0,uom", $arr , "booking_no_prefix_num,booking_date,supplier_id,fabric_color_id,construction,copmposition,gsm_weight,dia_width,uom", "",'','0,3,0,0,0,0,0,0,0','',1);
	}
	else if($item_category_id==4)
	{
		if ($data[0]!="") 
		{
			$wo_number=" and a.booking_no like '%".trim($data[0])."'";
			$sample_wo_number="and s.booking_no like '%".trim($data[0])."'";
		}
		else 
		{ 
			$wo_number = ''; 
			$sample_wo_number = ''; 
		}
		
		if($db_type==0) 
		{
			$year_field="YEAR(a.insert_date) as year,";
			$year_field_sample="YEAR(s.insert_date) as year,";  
		}
		else if($db_type==2) 
		{
			$year_field="to_char(a.insert_date,'YYYY') as year,";
			$year_field_sample="to_char(s.insert_date,'YYYY') as year,";
		}
		else 
		{
			$year_field="";//defined Later
			$year_field_sample="";//defined Later
		}
		
		$sql = "select a.booking_no_prefix_num, a.booking_no, a.booking_date, a.supplier_id, b.id as dtls_id, b.trim_group, c.description, b.Sensitivity as sensitivity, c.id, b.uom, 0 as type, $year_field a.booking_type, a.is_short from wo_booking_mst a, wo_booking_dtls b, wo_trim_book_con_dtls c where a.booking_no=b.booking_no and b.id=c.wo_trim_booking_dtls_id and a.company_id=$company_id and a.item_category=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond 
			union all
				select s.booking_no_prefix_num, s.booking_no, s.booking_date, s.supplier_id, d.id as dtls_id, d.trim_group, d.fabric_description as description, 0 as sensitivity, d.id as id, d.uom, 1 as type, $year_field_sample s.booking_type, s.is_short FROM wo_non_ord_samp_booking_mst s, wo_non_ord_samp_booking_dtls d WHERE s.booking_no=d.booking_no and s.company_id=$company_id and s.status_active =1 and s.is_deleted=0 and d.status_active =1 and d.is_deleted=0 and s.item_category=4 $sample_wo_number $sample_wo_date_cond order by type, id"; 
		//echo $sql;//die;
		//$result = sql_select($sql);
	
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$item_group_arr=return_library_array( "select id,item_name FROM lib_item_group",'id','item_name');
		//$arr=array (2=>$supplier_arr,3=>$item_group_arr,5=>$size_color_sensitive,6=>$unit_of_measurement);
		//echo create_list_view("tbl_list_search", "WO Number, WO Date, Supplier, Item Group, Item Description, sensitivity, UOM", "80,80,160,100,170,140,60","880","250",0, $sql, "js_set_value", "id,sensitivity,type", "", 1, "0,0,supplier_id,trim_group,0,sensitivity,uom", $arr , "booking_no_prefix_num,booking_date,supplier_id,trim_group,description,sensitivity,uom", "",'','0,3,0,0,0,0,0','',1);
		
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="880" class="rpt_table">
            <thead>
                <th width="40">SL No</th>
                <th width="60">WO No</th>
                <th width="50">Year</th>
                <th width="105">Type</th>
                <th width="80">WO Date</th>
                <th width="130">Supplier</th>
                <th width="100">Item Group</th>
                <th width="140">Item Description</th>
                <th width="105">sensitivity</th>
                <th>uom</th>
            </thead>
         </table>
         <div style="width:880px; max-height:250px; overflow-y:scroll">
             <table cellspacing="0" cellpadding="0" border="1" rules="all" width="860" class="rpt_table" id="tbl_list_search">
             <?php 
             $i=1;
             $nameArray=sql_select( $sql );
             foreach ($nameArray as $row)
             {
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				if($row[csf('type')]==1) 
				{
					$booking_type_text="Sample Without Order";
				}
				else
				{
					if($row[csf('booking_type')]==5) 
					{
						$booking_type_text="Sample With Order";
					}
					else 
					{
						if($row[csf('is_short')]==1) $booking_type_text="Short"; else $booking_type_text="Main"; 
					}	
				}
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="tr_<?php echo $i;?>" onClick="js_set_value('<?php echo $i."_".$row[csf('id')]."_".$row[csf('sensitivity')]."_".$row[csf('type')]; ?>')"> 				
                	<td width="40"><?php echo "$i"; ?></td>	
                    <td width="60"><p><?php echo $row[csf('booking_no_prefix_num')];?></p></td>
                    <td width="50"><p><?php echo $row[csf('year')];?></p></td>
                    <td width="105"><p><?php echo $booking_type_text; ?></p></td> 
                    <td width="80" align="center"><p><?php echo change_date_format($row[csf('booking_date')]); ?>&nbsp;</p></td>
                    <td width="130"><p><?php echo $supplier_arr[$row[csf('supplier_id')]]; ?>&nbsp;</p></td>
                    <td width="100"><p><?php echo $item_group_arr[$row[csf('trim_group')]]; ?></p></td>
                    <td width="140"><p><?php echo $row[csf('description')]; ?></p></td>
                    <td width="105"><p><?php echo $size_color_sensitive[$row[csf('sensitivity')]]; ?>&nbsp;</p></td>
                    <td align="center"><p><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></p></td>
                </tr>
             <?php
             $i++;
             }
             ?>
            </table>
        </div>  
		<table width="880" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                        <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	<?php
	}
	else if($item_category_id==12)
	{
		if ($data[0]!="") $wo_number=" and a.booking_no like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql = "select a.booking_no_prefix_num, a.booking_no, a.booking_date, a.supplier_id, b.id, b.process, b.uom, b.color_size_table_id, b.fabric_color_id, b.item_size, c.fabric_description from wo_booking_mst a, wo_booking_dtls b, wo_pre_cost_fab_conv_cost_dtls c where a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and a.company_id=$company_id and a.item_category=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id"; 
		//echo $sql;die;
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$sizeColor_arr=array(); $desc_arr=array();
		$col_size=sql_select( "select id, color_number_id, size_number_id from wo_po_color_size_breakdown where status_active=1 and is_deleted=0" );
		foreach($col_size as $csRow)
		{
			$sizeColor_arr[$csRow[csf('id')]]['color']=$csRow[csf('color_number_id')];
			$sizeColor_arr[$csRow[csf('id')]]['size']=$csRow[csf('size_number_id')];
		}
		
		$descArrray=sql_select( "select id, body_part_id, color_type_id, construction, composition from wo_pre_cost_fabric_cost_dtls where status_active=1 and is_deleted=0");
		foreach($descArrray as $dRow)
		{
			$descArrray[$dRow[csf('id')]]['bp']=$dRow[csf('body_part_id')];
			$descArrray[$dRow[csf('id')]]['ct']=$dRow[csf('color_type_id')];
			$descArrray[$dRow[csf('id')]]['cons']=$dRow[csf('construction')];
			$descArrray[$dRow[csf('id')]]['comp']=$dRow[csf('composition')];
		}
		
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table">
            <thead>
                <th width="40">SL No</th>
                <th width="70">WO No</th>
                <th width="80">WO Date</th>
                <th width="120">Supplier</th>
                <th width="80">process</th>
                <th width="130">Description</th>
                <th width="80">Gmts Color</th>
                <th width="80">Item Color</th>
                <th width="60">Gmts Size</th>
                <th width="60">Item Size</th>
                <th>uom</th>
            </thead>
         </table>
         <div style="width:888px; max-height:250px; overflow-y:scroll">
             <table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table" id="tbl_list_search">
             <?php 
             $i=1;
             $nameArray=sql_select( $sql );
             foreach ($nameArray as $selectResult)
             {
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
                    
                //$col_size=sql_select( "select color_number_id, size_number_id from wo_po_color_size_breakdown where id='".$selectResult[csf('color_size_table_id')]."'" );
				
				if($selectResult[csf('fabric_description')]==0)
				{
					$desc="All Fabrics";
				}
				else
				{
					//$descArrray=sql_select( "select body_part_id, color_type_id, construction, composition from wo_pre_cost_fabric_cost_dtls where id='".$selectResult[csf('fabric_description')]."'" );
					//$desc=$body_part[$descArrray[0][csf('body_part_id')]].", ".$color_type[$descArrray[0][csf('color_type_id')]].", ".$descArrray[0][csf('construction')].", ".$descArrray[0][csf('composition')];
					
					$desc=$body_part[$descArrray[$selectResult[csf('fabric_description')]]['bp']].", ".$color_type[$descArrray[$selectResult[csf('fabric_description')]]['ct']].", ".$descArrray[$selectResult[csf('fabric_description')]]['cons'].", ".$descArrray[$selectResult[csf('fabric_description')]]['comp'];
				}
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="tr_<?php echo $i;?>" onClick="js_set_value('<?php echo $i."_".$selectResult[csf('id')]; ?>')"> 				
                	<td width="40"><?php echo "$i"; ?></td>	
                    <td width="70"><p><?php echo $selectResult[csf('booking_no_prefix_num')];?></p></td>
                    <td width="80"><p><?php echo change_date_format($selectResult[csf('booking_date')]); ?>&nbsp;</p></td> 
                    <td width="120"><p><?php echo $supplier_arr[$selectResult[csf('supplier_id')]]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $conversion_cost_head_array[$selectResult[csf('process')]]; ?>&nbsp;</p></td>
                    <td width="130"><p><?php echo $desc; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $color_library[$sizeColor_arr[$selectResult[csf('color_size_table_id')]]['color']]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $color_library[$selectResult[csf('fabric_color_id')]]; ?>&nbsp;</p></td>
                    <td width="60"><p><?php echo $size_library[$sizeColor_arr[$selectResult[csf('color_size_table_id')]]['size']]; ?>&nbsp;</p></td>
                    <td width="60"><p><?php echo $selectResult[csf('item_size')]; ?>&nbsp;</p></td>
                    <td><p><?php echo $unit_of_measurement[$selectResult[csf('uom')]]; ?>&nbsp;</p></td>
                </tr>
             <?php
             $i++;
             }
             ?>
            </table>
        </div>  
		<table width="870" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                        <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	<?php
	}
	else if($item_category_id==24)
	{
		if ($data[0]!="") $wo_number=" and a.ydw_no like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql = "select a.yarn_dyeing_prefix_num, a.ydw_no, a.booking_date, a.supplier_id, b.id, b.product_id, b.job_no, b.uom, b.yarn_description, b.yarn_color, b.color_range from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b where a.id=b.mst_id and a.company_id=$company_id and a.item_category_id=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id";  
		//echo $sql;die;
		//$result = sql_select($sql);
	
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$lot_arr=return_library_array( "select id, lot from product_details_master where item_category_id=1",'id','lot');
		$color_array=return_library_array( "select id, color_name from lib_color",'id','color_name');
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table">
            <thead>
                <th width="40">SL No</th>
                <th width="70">WO No</th>
                <th width="80">WO Date</th>
                <th width="130">Supplier</th>
                <th width="100">Job No</th>
                <th width="80">Lot</th>
                <th width="150">Yarn Description</th>
                <th width="80">Yarn Color</th>
                <th width="80">Color_range</th>
                <th>UOM</th>
            </thead>
         </table>
         <div style="width:888px; max-height:250px; overflow-y:scroll">
             <table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table" id="tbl_list_search">
             <?php 
             $i=1;
             $nameArray=sql_select( $sql );
             foreach ($nameArray as $selectResult)
             {
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="tr_<?php echo $i;?>" onClick="js_set_value('<?php echo $i."_".$selectResult[csf('id')]; ?>')"> 				
                	<td width="40"><?php echo "$i"; ?></td>	
                    <td width="70"><p><?php echo $selectResult[csf('yarn_dyeing_prefix_num')];?></p></td>
                    <td width="80"><p><?php echo change_date_format($selectResult[csf('booking_date')]); ?>&nbsp;</p></td> 
                    <td width="130"><p><?php echo $supplier_arr[$selectResult[csf('supplier_id')]]; ?>&nbsp;</p></td>
                    <td width="100"><p><?php echo $selectResult[csf('job_no')]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $lot_arr[$selectResult[csf('product_id')]]; ?>&nbsp;</p></td>
                    <td width="150"><p><?php echo $selectResult[csf('yarn_description')]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $color_array[$selectResult[csf('yarn_color')]]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $color_range[$selectResult[csf('color_range')]]; ?>&nbsp;</p></td>
                    <td><p><?php echo $unit_of_measurement[$selectResult[csf('uom')]]; ?>&nbsp;</p></td>
                </tr>
             <?php
             $i++;
             }
             ?>
            </table>
        </div>  
		<table width="870" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                        <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	<?php
	}
	else if($item_category_id==25)
	{
		if ($data[0]!="") $wo_number=" and a.booking_no like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql = "select a.booking_no_prefix_num, a.booking_no, a.booking_date, a.supplier_id, a.buyer_id, b.id, b.gmts_color_id, b.gmt_item, c.emb_name, c.emb_type from wo_booking_mst a, wo_booking_dtls b, wo_pre_cost_embe_cost_dtls c where a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and a.company_id=$company_id and a.item_category=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id"; 
		//echo $sql;die;
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table">
            <thead>
                <th width="40">SL No</th>
                <th width="70">WO No</th>
                <th width="80">WO Date</th>
                <th width="120">Supplier</th>
                <th width="80">Buyer</th>
                <th width="150">Gmts Item</th>
                <th width="110">Embell. Name</th>
                <th width="110">Embell. Type</th>
                <th>Gmts Color</th>
            </thead>
         </table>
         <div style="width:888px; max-height:250px; overflow-y:scroll">
             <table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table" id="tbl_list_search">
             <?php 
             $i=1;
             $nameArray=sql_select( $sql );
             foreach ($nameArray as $selectResult)
             {
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="tr_<?php echo $i;?>" onClick="js_set_value('<?php echo $i."_".$selectResult[csf('id')]; ?>')"> 				
                	<td width="40"><?php echo "$i"; ?></td>	
                    <td width="70"><p><?php echo $selectResult[csf('booking_no_prefix_num')];?></p></td>
                    <td width="80"><p><?php echo change_date_format($selectResult[csf('booking_date')]); ?>&nbsp;</p></td> 
                    <td width="120"><p><?php echo $supplier_arr[$selectResult[csf('supplier_id')]]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $buyer_arr[$selectResult[csf('buyer_id')]]; ?>&nbsp;</p></td>
                    <td width="150"><p><?php echo $garments_item[$selectResult[csf('gmt_item')]]; ?>&nbsp;</p></td>
                    <td width="110"><p><?php echo $emblishment_name_array[$selectResult[csf('emb_name')]]; ?>&nbsp;</p></td>
                    <td width="110">
                    	<p>
							<?php 
								if($selectResult[csf('emb_name')]==1) echo $emblishment_print_type[$selectResult[csf('emb_type')]];
								else if($selectResult[csf('emb_name')]==2) echo $emblishment_embroy_type[$selectResult[csf('emb_type')]];
								else if($selectResult[csf('emb_name')]==3) echo $emblishment_wash_type[$selectResult[csf('emb_type')]];
								else if($selectResult[csf('emb_name')]==4) echo $emblishment_spwork_type[$selectResult[csf('emb_type')]]; 
							?>
                            &nbsp;
                    	</p>
                    </td>
                    <td><p><?php echo $color_library[$selectResult[csf('gmts_color_id')]]; ?>&nbsp;</p></td>
                </tr>
             <?php
             $i++;
             }
             ?>
            </table>
        </div>  
		<table width="870" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                        <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	<?php
	}
	else if($item_category_id==11)
	{
		if ($data[0]!="") $wo_number=" and a.wo_number like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql="select a.wo_number_prefix_num, a.wo_number, a.wo_date, a.item_category, a.supplier_id, b.id, b.item_id, b.uom, c.item_group_id, c.item_description, c.item_size from wo_non_order_info_mst a, wo_non_order_info_dtls b, product_details_master c where a.id=b.mst_id and b.item_id=c.id and a.company_name=$company_id and a.item_category in(4,11) and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id";  
		//echo $sql;die;
		$result = sql_select($sql);
	
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$item_group_arr=return_library_array( "select id,item_name FROM lib_item_group",'id','item_name');
		$arr=array (2=>$supplier_arr,3=>$item_category,4=>$item_group_arr,7=>$unit_of_measurement);
		
		echo create_list_view("tbl_list_search", "WO Number, WO Date, Supplier, Item Category, Item Group, Item Description, Item Size, UOM", "70,80,140,90,120,160,80","880","250",0, $sql, "js_set_value", "id", "", 1, "0,0,supplier_id,item_category,item_group_id,0,0,uom", $arr , "wo_number_prefix_num,wo_date,supplier_id,item_category,item_group_id,item_description,item_size,uom", "",'','0,3,0,0,0,0,0,0','',1);
	}
	else
	{
		if ($data[0]!="") $wo_number=" and a.wo_number like '%".trim($data[0])."'"; else { $wo_number = ''; }
		$sql="select a.wo_number_prefix_num, a.wo_number, a.wo_date, a.supplier_id, b.id, b.item_id, b.uom, c.item_group_id, c.item_description, c.item_size from wo_non_order_info_mst a, wo_non_order_info_dtls b, product_details_master c where a.id=b.mst_id and b.item_id=c.id and a.company_name=$company_id and a.item_category=$item_category_id and a.supplier_id like '$supplier_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $wo_number $wo_date_cond order by a.id";  
		//echo $sql;die;
		$result = sql_select($sql);
	
		$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
		$item_group_arr=return_library_array( "select id,item_name FROM lib_item_group",'id','item_name');
		$arr=array (2=>$supplier_arr,3=>$item_group_arr,6=>$unit_of_measurement);
		
		echo create_list_view("tbl_list_search", "WO Number, WO Date, Supplier, Item Group, Item Description, Item Size, UOM", "125,80,130,120,150,110,60","880","250",0, $sql, "js_set_value", "id", "", 1, "0,0,supplier_id,item_group_id,0,0,uom", $arr , "wo_number_prefix_num,wo_date,supplier_id,item_group_id,item_description,item_size,uom", "",'','0,3,0,0,0,0,0','',1);
	}
	
exit();	
} 
if( $action == 'populate_data_wo_form' ) 
{
	$data=explode('**',$data);
	$wo_dtls_id=$data[0];
	$item_category_id=$data[1];
	$tblRow=$data[2];
	if($item_category_id==1)
	{
		$data_array=sql_select("select a.id, a.wo_number, b.id as dtls_id, b.color_name, b.yarn_count, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.supplier_order_quantity, b.uom, b.rate, b.amount from wo_non_order_info_mst a, wo_non_order_info_dtls b where a.id=b.mst_id and b.id in($wo_dtls_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
					<input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
				</td>
				<td>
					<input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('wo_number')]; ?>" style="width:80px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
					<input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
					<input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
				</td>
				<td>
					<input type="text" name="colorName_<?php echo $tblRow; ?>" id="colorName_<?php echo $tblRow; ?>" class="text_boxes" style="width:80px"  maxlength="50" value="<?php echo $color_library[$row[csf('color_name')]]; ?>" disabled="disabled" />
				</td>
				<td>
				<?php
					echo create_drop_down( "countName_".$tblRow, 85, "SELECT id,yarn_count FROM lib_yarn_count WHERE status_active = 1 AND is_deleted = 0 ORDER BY yarn_count ASC",'id,yarn_count', 1, '-Select-',$row[csf('yarn_count')],"",1); 
				?>                         
				</td>
				<td>
					<?php
						echo create_drop_down( "yarnCompositionItem1_".$tblRow,75, $composition,'', 1, '-Select-',$row[csf('yarn_comp_type1st')],"",1); 
					?>    
					
					<input type="text" name="yarnCompositionPercentage1_<?php echo $tblRow; ?>" id="yarnCompositionPercentage1_<?php echo $tblRow; ?>" class="text_boxes_numeric" style="width:25px;" value="<?php echo $row[csf('yarn_comp_percent1st')]; ?>" disabled="disabled"/>%
					<?php
						echo create_drop_down( "yarnCompositionItem2_".$tblRow,75, $composition,'', 1, '-Select-',$row[csf('yarn_comp_type2nd')],"",1); 
					?>   
					 
					<input type="text" name="yarnCompositionPercentage2_<?php echo $tblRow; ?>" id="yarnCompositionPercentage2_<?php echo $tblRow; ?>" class="text_boxes_numeric" style="width:25px;" value="<?php echo $row[csf('yarn_comp_percent2nd')]; ?>" disabled="disabled"/>%
				</td>
				<td>
					<?php
						echo create_drop_down( "yarnType_".$tblRow,80,$yarn_type,'', 1,'-Select-',$row[csf('yarn_type')],"",1); 
					?>    
				</td>
				<td id="tduom_"<?php echo $tblRow; ?>>
					<?php
						echo create_drop_down( "uom_".$tblRow, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1); 
					?>
				</td>
				<td>
					<input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('supplier_order_quantity')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px; text-align:right;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px; text-align:right;" readonly/>
                    <input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly value=""/>
				</td>
			</tr>
		<?php
		}
	}
	else if($item_category_id==2)
	{
		$data_array=sql_select("select a.id, a.booking_no, b.id as dtls_id, b.fabric_color_id, b.construction, b.copmposition, b.gsm_weight, b.dia_width, b.uom, b.fin_fab_qnty, b.rate, (b.fin_fab_qnty*b.rate) as amount, c.lib_yarn_count_deter_id from wo_booking_mst a, wo_booking_dtls b, wo_pre_cost_fabric_cost_dtls c where a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and b.id in($wo_dtls_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
					<input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
				</td>
				<td>
					<input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('booking_no')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
					<input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
					<input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
				</td>
                <td> 
                    <input type="text" name="construction_<?php echo $tblRow; ?>" id="construction_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('construction')]; ?>" style="width:110px" disabled="disabled"/>
                    <input type="hidden" name="hideDeterminationId_<?php echo $tblRow; ?>" id="hideDeterminationId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('lib_yarn_count_deter_id')]; ?>" readonly />
                </td>
                <td>
                    <input type="text" name="composition_<?php echo $tblRow; ?>" id="composition_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('copmposition')]; ?>" style="width:110px" disabled="disabled"/>
                </td> 
                <td>
                    <input type="text" name="colorName_<?php echo $tblRow; ?>" id="colorName_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $color_library[$row[csf('fabric_color_id')]]; ?>" style="width:80px" maxlength="50" disabled="disabled"/>
                </td>
                <td>
                    <input type="text" name="gsm_<?php echo $tblRow; ?>" id="gsm_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('gsm_weight')]; ?>" style="width:60px" disabled="disabled"/>
                </td>
                <td>
                    <input type="text" name="diawidth_<?php echo $tblRow; ?>" id="diawidth_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('dia_width')]; ?>" style="width:70px" disabled="disabled"/>
                </td>
                 <td>
                    <?php 
                        echo create_drop_down( "uom_".$tblRow, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,12);            
                    ?>						 
                </td>
				<td>
					<input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('fin_fab_qnty')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px; text-align:right;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px; text-align:right;" readonly/>
                    <input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly value=""/>
				</td>
			</tr>
		<?php
		}
	}
	else if($item_category_id==3)
	{
		$data_array=sql_select("select a.id, a.booking_no, b.id as dtls_id, b.fabric_color_id, b.construction, b.copmposition, b.gsm_weight, b.dia_width, b.uom, b.fin_fab_qnty, b.rate, (b.fin_fab_qnty*b.rate) as amount, c.lib_yarn_count_deter_id from wo_booking_mst a, wo_booking_dtls b, wo_pre_cost_fabric_cost_dtls c where a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and b.id in($wo_dtls_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
					<input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
				</td>
				<td>
					<input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('booking_no')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
					<input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
					<input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
				</td>
                <td> 
                    <input type="text" name="construction_<?php echo $tblRow; ?>" id="construction_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('construction')]; ?>" style="width:120px" disabled="disabled"/>
                    <input type="hidden" name="hideDeterminationId_<?php echo $tblRow; ?>" id="hideDeterminationId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('lib_yarn_count_deter_id')]; ?>" readonly />
                </td>
                <td>
                    <input type="text" name="composition_<?php echo $tblRow; ?>" id="composition_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('copmposition')]; ?>" style="width:120px" disabled="disabled"/>
                </td> 
                <td>
                    <input type="text" name="colorName_<?php echo $tblRow; ?>" id="colorName_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $color_library[$row[csf('fabric_color_id')]]; ?>" style="width:80px" maxlength="50" disabled="disabled"/>
                </td>
                <td>
                    <input type="text" name="weight_<?php echo $tblRow; ?>" id="weight_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('gsm_weight')]; ?>" style="width:60px" disabled="disabled"/>
                </td>
                <td>
                    <input type="text" name="diawidth_<?php echo $tblRow; ?>" id="diawidth_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('dia_width')]; ?>" style="width:70px" disabled="disabled"/>
                </td>
                 <td>
                    <?php 
                        echo create_drop_down( "uom_".$tblRow, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,27);            
                    ?>						 
                </td>
				<td>
					<input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('fin_fab_qnty')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                    <input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly value=""/>
				</td>
			</tr>
		<?php
		}
	}
	else if($item_category_id==4)
	{
		$wo_dtls_id=explode(",",$wo_dtls_id);
		$dtls_id=''; $sam_dtls_id='';
		foreach($wo_dtls_id as $dtlsId_sensitivity)
		{
			$dtlsId_sensitivity=explode("_",$dtlsId_sensitivity);
			//$dlstId=$dtlsId_sensitivity[0];
			$con_dtls=$dtlsId_sensitivity[0];
			$sensitivity=$dtlsId_sensitivity[1];
			$booking_without_order=$dtlsId_sensitivity[2];
			if($booking_without_order==1)
			{
				$sam_dtls_id.=$con_dtls.",";
			}
			else
			{
				$dtls_id.=$con_dtls.",";
			}
		}
		
		$item_group_arr=return_library_array("SELECT id,item_name FROM lib_item_group WHERE item_category=4 AND status_active=1 and is_deleted=0 order by item_name",'id','item_name');
		
		$dtls_id=substr($dtls_id,0,-1);
		$sam_dtls_id=substr($sam_dtls_id,0,-1);
		
		if($dtls_id!="" && $sam_dtls_id!="")
		{
			$data_array=sql_select("select a.id, a.booking_no, b.id as dtls_id, b.trim_group, c.description, b.uom, c.rate, c.cons as qnty, c.color_number_id, c.item_color, c.gmts_sizes as size_id, c.item_size, c.brand_supplier from wo_booking_mst a, wo_booking_dtls b, wo_trim_book_con_dtls c where a.booking_no=b.booking_no and b.id=c.wo_trim_booking_dtls_id and c.id in($dtls_id)
						union all
						select s.id, s.booking_no, d.id as dtls_id, d.trim_group, d.fabric_description as description, d.uom, d.rate, d.trim_qty as qnty, d.gmts_color as color_number_id, d.fabric_color as item_color, d.gmts_size as size_id, d.item_size, d.barnd_sup_ref as brand_supplier from wo_non_ord_samp_booking_mst s, wo_non_ord_samp_booking_dtls d where s.booking_no=d.booking_no and d.id in($sam_dtls_id)");
		}
		else if($sam_dtls_id!="")
		{
			$data_array=sql_select("select a.id, a.booking_no, b.id as dtls_id, b.trim_group, b.fabric_description as description, b.uom, b.rate, b.trim_qty as qnty, b.gmts_color as color_number_id, b.fabric_color as item_color, b.gmts_size as size_id, b.item_size, b.barnd_sup_ref as brand_supplier from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and b.id in($sam_dtls_id)");
		}
		else
		{
			$data_array=sql_select("select a.id, a.booking_no, b.id as dtls_id, b.trim_group, c.description, b.uom, c.rate, c.cons as qnty, c.color_number_id, c.item_color, c.gmts_sizes as size_id, c.item_size, c.brand_supplier from wo_booking_mst a, wo_booking_dtls b, wo_trim_book_con_dtls c where a.booking_no=b.booking_no and b.id=c.wo_trim_booking_dtls_id and c.id in($dtls_id)");
		}
		
		foreach($data_array as $row)
		{
			$tblRow++;
			$amount=$row[csf('rate')]*$row[csf('qnty')];
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
					<input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
				</td>
				<td>
					<input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('booking_no')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
					<input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
					<input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
				</td>
				<td> 
					<?php
                        echo create_drop_down( "itemgroupid_".$tblRow, 110, $item_group_arr,'', 1, '-Select-',$row[csf('trim_group')],"get_php_form_data( this.value+'**'+'uom_$tblRow', 'get_uom', 'requires/pi_controller' );",1); 
                    ?>  
				</td>
				<td>
					<input type="text" name="itemdescription_<?php echo $tblRow; ?>" id="itemdescription_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('description')]; ?>" style="width:130px" maxlength="200" disabled="disabled"/>
				</td>
				 <td>
						<input type="text" name="brandSupRef_<?php echo $tblRow; ?>" id="brandSupRef_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('brand_supplier')]; ?>" style="width:80px" maxlength="150" disabled="disabled"/>
					</td>
				<td>
					<input type="text" name="colorName_<?php echo $tblRow; ?>" id="colorName_<?php echo $tblRow; ?>" class="text_boxes" style="width:70px" maxlength="50" value="<?php echo $color_library[$row[csf('color_number_id')]]; ?>" disabled="disabled"/>                        
				</td>
				<td>
					<input type="text" name="sizeName_<?php echo $tblRow; ?>" id="sizeName_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $size_library[$row[csf('size_id')]]; ?>" style="width:60px;" maxlength="50" disabled="disabled"/>
				</td>
				<td>
					<input type="text" name="itemColor_<?php echo $tblRow; ?>" id="itemColor_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $color_library[$row[csf('item_color')]]; ?>" style="width:70px" maxlength="50" disabled="disabled"/>
				</td>
				<td>
					<input type="text" name="itemSize_<?php echo $tblRow; ?>" id="itemSize_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('item_size')]; ?>" style="width:60px;" maxlength="50" disabled="disabled"/>
				</td>
				 <td>
					<?php 
						echo create_drop_down( "uom_".$tblRow, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1);            
					?>						 
				</td>
				<td>
					<input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('qnty')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $amount; ?>" style="width:75px;" readonly/>
					<input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly value=""/>
                    <input type="hidden" name="bookingWithoutOrder_<?php echo $tblRow; ?>" id="bookingWithoutOrder_<?php echo $tblRow; ?>" value="<?php echo $booking_without_order; ?>"/>
				</td>
			</tr>
		<?php
		}
	}
	else if($item_category_id==12)
	{
		$descArray=array();
		$descData=sql_select( "select id, body_part_id, color_type_id, construction, composition from wo_pre_cost_fabric_cost_dtls" );
		foreach($descData as $descRow)
		{
			$descArray[$descRow[csf('id')]]['bp']=$descRow[csf('body_part_id')];
			$descArray[$descRow[csf('id')]]['ct']=$descRow[csf('color_type_id')];
			$descArray[$descRow[csf('id')]]['con']=$descRow[csf('construction')];
			$descArray[$descRow[csf('id')]]['com']=$descRow[csf('composition')];
		}
		
		$col_sizeArr=array();
		$col_sizeData=sql_select( "select id, color_number_id, size_number_id from wo_po_color_size_breakdown" );
		foreach($col_sizeData as $colSizeRow)
		{
			$col_sizeArr[$colSizeRow[csf('id')]]['color']=$colSizeRow[csf('color_number_id')];
			$col_sizeArr[$colSizeRow[csf('id')]]['size']=$colSizeRow[csf('size_number_id')];
		}
		
		$data_array=sql_select("select a.id, a.booking_no, b.id as dtls_id, b.process, b.uom, b.color_size_table_id, b.fabric_color_id, b.item_size, b.wo_qnty as qnty, b.rate, b.amount, c.fabric_description from wo_booking_mst a, wo_booking_dtls b, wo_pre_cost_fab_conv_cost_dtls c where a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and b.id in($wo_dtls_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			
			$col_size=sql_select( "select color_number_id, size_number_id from wo_po_color_size_breakdown where id='$row[color_size_table_id]'" );
				
			if($row[csf('fabric_description')]==0)
			{
				$desc="All Fabrics";
			}
			else
			{
				//$descArrray=sql_select( "select body_part_id, color_type_id, construction, composition from wo_pre_cost_fabric_cost_dtls where id='$row[fabric_description]'" );
				$desc=$body_part[$descArray[$row[csf('fabric_description')]]['bp']].", ".$color_type[$descArray[$row[csf('fabric_description')]]['ct']].", ".$descArray[$row[csf('fabric_description')]]['con'].", ".$descArray[$row[csf('fabric_description')]]['com'];
			}
			
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
                    <input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
                </td>
                <td>
                    <input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('booking_no')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
                    <input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
                    <input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
                </td>
                <td>
                	<?php echo create_drop_down( "servicetype_".$tblRow, 110, $conversion_cost_head_array,'', 1,'-Select-',$row[csf('process')],"",1); ?> 
                </td>
                <td>
                    <input type="text" name="itemdescription_<?php echo $tblRow; ?>" id="itemdescription_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $desc; ?>" style="width:150px" maxlength="200" disabled="disabled"/>
                </td>
                <td>
                    <input type="text" name="colorName_<?php echo $tblRow; ?>" id="colorName_<?php echo $tblRow; ?>" class="text_boxes" style="width:80px" maxlength="50" value="<?php echo $color_library[$col_sizeArr[$row[csf('fabric_description')]]['color']]; ?>" disabled="disabled"/>                        
                </td>
                <td>
                    <input type="text" name="sizeName_<?php echo $tblRow; ?>" id="sizeName_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $size_library[$col_sizeArr[$row[csf('fabric_description')]]['size']]; ?>" style="width:70px;" maxlength="50" disabled="disabled"/>
                </td>
                <td>
                    <input type="text" name="itemColor_<?php echo $tblRow; ?>" id="itemColor_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $color_library[$row[csf('fabric_color_id')]]; ?>" style="width:80px" maxlength="50" disabled="disabled"/>
                </td>
                <td>
                    <input type="text" name="itemSize_<?php echo $tblRow; ?>" id="itemSize_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('item_size')]; ?>" style="width:70px;" maxlength="50" disabled="disabled"/>
                </td>
                 <td>
                    <?php 
                        echo create_drop_down( "uom_".$tblRow, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1);            
                    ?>						 
                </td>
                <td>
                    <input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('qnty')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                    <input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly value=""/>
                </td>
			</tr>
		<?php
		}
	}
	else if($item_category_id==24)
	{
		$lot_arr=array();
		$lot_data=sql_select( "select id, lot, yarn_count_id from product_details_master where item_category_id=1");
		foreach($lot_data as $lotRow)
		{
			$lot_arr[$lotRow[csf('id')]]['lot']=$lotRow[csf('lot')];
			$lot_arr[$lotRow[csf('id')]]['count']=$lotRow[csf('yarn_count_id')];
		}
		$color_array=return_library_array( "select id, color_name from lib_color",'id','color_name');
		$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count WHERE status_active = 1 AND is_deleted = 0 ORDER BY yarn_count",'id','yarn_count');
		
		$data_array=sql_select("select a.id, a.ydw_no, b.id as dtls_id, b.product_id, b.uom, b.yarn_description, b.yarn_color, b.color_range, b.yarn_wo_qty as qnty, b.dyeing_charge as rate, b.amount from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b where a.id=b.mst_id and b.id in($wo_dtls_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
                    <input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
                </td>
                <td>
                    <input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('ydw_no')]; ?>" style="width:115px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
                    <input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
                    <input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
                </td>
                <td>
                    <input type="text" name="lot_<?php echo $tblRow; ?>" id="lot_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $lot_arr[$row[csf('product_id')]]['lot']; ?>" style="width:80px" maxlength="200" disabled readonly/>
                    <input type="hidden" name="itemProdId_<?php echo $tblRow; ?>" id="itemProdId_<?php echo $tblRow; ?>" readonly value="<?php echo $row[csf('product_id')];?>"/> 
                </td>
                <td>
                    <?php echo create_drop_down( "countName_".$tblRow, 90, $count_arr,'', 1, '-Select-', $lot_arr[$row[csf('product_id')]]['count'],"",1); ?>
                </td>
                <td>
                    <input type="text" name="itemdescription_<?php echo $tblRow; ?>" id="itemdescription_<?php echo $tblRow; ?>" class="text_boxes" style="width:200px" value="<?php echo $row[csf('yarn_description')]; ?>" disabled/>
                </td>
                <td>
                    <?php echo create_drop_down( "yarnColor_".$tblRow, 110, $color_array,'', 1, '-Select-', $row[csf('yarn_color')],"",1); ?>
                </td>
                <td>
                    <?php echo create_drop_down( "colorRange_".$tblRow, 110, $color_range,'', 1, '-Select-', $row[csf('color_range')],"",1); ?>
                </td>
                <td>
                    <?php
                        echo create_drop_down( "uom_".$tblRow, 60, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,12);
                    ?>
                </td>
                <td>
                    <input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('qnty')]; ?>" style="width:61px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:45px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:75px;" readonly/>
                    <input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly value=""/>
                </td>
			</tr>
		<?php
		}
	}
	else if($item_category_id==25)
	{
		$data_array=sql_select("select a.id, a.booking_no, b.id as dtls_id, b.gmts_color_id, b.gmt_item, b.wo_qnty as qnty, b.rate, b.amount, c.emb_name, c.emb_type from wo_booking_mst a, wo_booking_dtls b, wo_pre_cost_embe_cost_dtls c where a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and b.id in($wo_dtls_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
                    <input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
                </td>
                <td>
                    <input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('booking_no')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
                    <input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
                    <input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
                </td>
                <td>
					<?php echo create_drop_down( "gmtsitem_".$tblRow, 150, $garments_item,'', 1, '-Select-', $row[csf('gmt_item')],"",1); ?>
                </td>
                <td>
                    <?php echo create_drop_down( "embellname_".$tblRow, 130, $emblishment_name_array,'', 1, '-Select-', $row[csf('emb_name')],"load_drop_down( 'requires/pi_controller', this.value+'**'+1+'**'+'embelltype_$tblRow', 'load_drop_down_embelltype', 'embelltypeTd_$tblRow');",1); ?>
                </td>
                <td id="embelltypeTd_<?php echo $tblRow; ?>">
                    <?php 
						$emb_arr=array();
						if($row[csf('emb_name')]==1) $emb_arr=$emblishment_print_type;
						else if($row[csf('emb_name')]==2) $emb_arr=$emblishment_embroy_type;
						else if($row[csf('emb_name')]==3) $emb_arr=$emblishment_wash_type;
						else if($row[csf('emb_name')]==4) $emb_arr=$emblishment_spwork_type;
						else $emb_arr=$blank_array;
						
						echo create_drop_down( "embelltype_".$tblRow, 130, $emb_arr,'', 1, '-Select-', $row[csf('emb_type')],"",1); 
					?>
                </td>
                <td>
                    <input type="text" name="colorName_<?php echo $tblRow; ?>" id="colorName_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $color_library[$row[csf('gmts_color_id')]]; ?>" style="width:90px" maxlength="50" disabled="disabled"/>
                </td>
                 <td>
                    <?php 
						echo create_drop_down( "uom_".$tblRow, 70, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1,2);         
                    ?>						 
                </td>
                <td>
                    <input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('qnty')]; ?>" style="width:81px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:50px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:85px;" readonly/>
                    <input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly value=""/>
                </td>
			</tr>
		<?php
		}
	}
	else
	{
		$item_group_arr=return_library_array( "SELECT id,item_name FROM lib_item_group",'id','item_name');
		$data_array=sql_select("select a.id, a.wo_number, a.wo_date, a.supplier_id, b.id as dtls_id, b.item_id, b.uom, b.supplier_order_quantity, b.rate, b.amount, c.item_group_id, c.item_description, c.item_size from wo_non_order_info_mst a, wo_non_order_info_dtls b, product_details_master c where a.id=b.mst_id and b.item_id=c.id and b.is_deleted=0 and b.id in($wo_dtls_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td>
					<input type="checkbox" name="workOrderChkbox_<?php echo $tblRow; ?>" id="workOrderChkbox_<?php echo $tblRow; ?>" value="" />
				</td>
				<td>
					<input type="text" name="workOrderNo_<?php echo $tblRow; ?>" id="workOrderNo_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('wo_number')]; ?>" style="width:100px;" placeholder="Dbl click" onDblClick="openmypage_wo(<?php echo $tblRow; ?>);" readonly />			
					<input type="hidden" name="hideWoId_<?php echo $tblRow; ?>" id="hideWoId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('id')]; ?>" readonly />
					<input type="hidden" name="hideWoDtlsId_<?php echo $tblRow; ?>" id="hideWoDtlsId_<?php echo $tblRow; ?>" value="<?php echo $row[csf('dtls_id')]; ?>" readonly />
				</td>
                <td> 
				<?php
                	echo create_drop_down( "itemgroupid_".$tblRow, 130, $item_group_arr,'', 1, '- Select -',$row[csf('item_group_id')],"get_php_form_data( this.value+'**'+'uom_$tblRow', 'get_uom', 'requires/pi_controller' );",1); 
            	?>  
                </td>
                <td>
                    <input type="text" name="itemdescription_<?php echo $tblRow; ?>" id="itemdescription_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('item_description')]; ?>" style="width:200px" maxlength="200" <?php echo $disable; ?> onDblClick="openmypage_item_desc(<?php echo $tblRow; ?>);" placeholder="Double Click" readonly/>
                    <input type="hidden" name="itemProdId_<?php echo $tblRow; ?>" id="itemProdId_<?php echo $tblRow; ?>" readonly value="<?php echo $row[csf('item_id')]; ?>"/> 
                </td>
                <td>
                    <input type="text" name="itemSize_<?php echo $tblRow; ?>" id="itemSize_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('item_size')]; ?>" style="width:90px;" maxlength="50" disabled="disabled"/>
                </td>
                <td>
                    <?php 
                        echo create_drop_down( "uom_$tblRow", 80, $unit_of_measurement,'', 0, '',$row[csf('uom')],'',1); 
                    ?>		
                </td>
                <td>
                    <input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('supplier_order_quantity')]; ?>" style="width:90px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('rate')]; ?>" style="width:75px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
                </td>
                <td>
                    <input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('amount')]; ?>" style="width:85px;" readonly/>
                    <input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly/>
                </td>
			</tr>
		<?php
		}
	}
	exit();
}

if ($action=="itemDesc_popup")
{
	echo load_html_head_contents("WO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 
	<script>
	
	var item_category=<?php echo $item_category; ?>;
	var selected_id = new Array, selected_name = new Array();
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");

			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
							
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#txt_selected_item_id').val( id );
		}
	
		function reset_hide_field()
		{
			$('#txt_selected_item_id').val( '' );
		}
	
    </script>

</head>

<body>
<div align="center" style="width:800px;">
	<form name="searchwofrm"  id="searchwofrm">
		<fieldset style="width:100%;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="700px" class="rpt_table">
                <thead>
                    <th>Company</th>
                    <th>Supplier</th>
                    <th>Item Description</th>
                    <th>
                    	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" onClick="reset_hide_field()" />
                        <input type="hidden" name="txt_item_category" id="txt_item_category" class="text_boxes" style="width:70px" value="<?php echo $item_category; ?>">
                    	<input type="hidden" name="txt_selected_item_id" id="txt_selected_item_id" class="text_boxes" value="">   
                    </th> 
                </thead>
                <tr class="general">
                    <td>
						 <?php echo create_drop_down( "cbo_importer_id", 151,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, 'Select',0,"load_drop_down( 'pi_controller',this.value+'_'+$item_category, 'load_supplier_dropdown', 'supplier_td' );",0); ?>       
                    </td>
                    <td id="supplier_td">	
                        <?php
							echo create_drop_down( "cbo_supplier_id", 151, $blank_array,"", 1, "-- Select Supplier --", 0, "" );
						?> 
                    </td>                 
                    <td> 
                        <input type="text" name="txt_item_desc" id="txt_item_desc" class="text_boxes" style="width:120px">
                    </td>						
            		 <td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_item_desc').value+'_'+document.getElementById('txt_item_category').value+'_'+document.getElementById('cbo_importer_id').value+'_'+document.getElementById('cbo_supplier_id').value, 'create_item_search_list_view', 'search_div', 'pi_controller', 'setFilterGrid(\'tbl_list_search\',-1);reset_hide_field();')" style="width:100px;" />
                     </td>
                </tr>
           </table>
           <table width="100%" style="margin-top:5px">
                <tr>
                    <td colspan="5">
                        <div style="width:100%; margin-top:10px; margin-left:10px" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table> 
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="create_item_search_list_view")
{
	$data = explode("_",$data);
	
	$item_category_id =$data[1];
	$company_id =$data[2];
	
	if($item_category_id==1) $field_name="product_name_details"; else $field_name="item_description";
	
	if($company_id==0) { echo "Please Select Company First."; die; }
	if($data[3]==0) $supplier_id="%%"; else $supplier_id =$data[3];
	
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	if($item_category_id==1)
	{
		$sql = "select id, item_category_id, supplier_id, lot, product_name_details, unit_of_measure from product_details_master where company_id=$company_id and item_category_id=$item_category_id and supplier_id like '$supplier_id' and product_name_details like '%".trim($data[0])."%' and status_active=1 and is_deleted=0";
		
		$arr=array (0=>$item_category,1=>$supplier_arr,4=>$unit_of_measurement);
		
		echo create_list_view("tbl_list_search", "Item Category, Supplier, Lot No, Yarn Description, UOM", "100,170,100,260","770","250",0, $sql, "js_set_value", "id", "", 1, "item_category_id,supplier_id,0,0,unit_of_measure", $arr , "item_category_id,supplier_id,lot,product_name_details,unit_of_measure", "",'','0,0,0,0,0','',1);
	}
	else
	{
		$sql = "select id, item_category_id, supplier_id, item_group_id, item_description, item_size, unit_of_measure from product_details_master where company_id=$company_id and item_category_id=$item_category_id and supplier_id like '$supplier_id' and item_description like '%".trim($data[0])."%' and status_active=1 and is_deleted=0";
		
		$item_group_arr=return_library_array( "select id, item_name from lib_item_group",'id','item_name');
		$arr=array (0=>$item_category,1=>$supplier_arr,2=>$item_group_arr,5=>$unit_of_measurement);
		
		echo create_list_view("tbl_list_search", "Item Category, Supplier, Item Group, Item Description, Item Size, UOM", "120,120,120,160,90","770","250",0, $sql, "js_set_value", "id", "", 1, "item_category_id,supplier_id,item_group_id,0,0,unit_of_measure", $arr , "item_category_id,supplier_id,item_group_id,item_description,item_size,unit_of_measure", "",'','0,0,0,0,0,0','',1);

	}
	//echo $sql;
exit();	
} 

if( $action == 'populate_data_item_form' ) 
{
	$data=explode('**',$data);
	$item_id=$data[0];
	$item_category_id=$data[1];
	$tblRow=$data[2];

	if($item_category_id==1)
	{
		$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count WHERE status_active = 1 AND is_deleted = 0 ORDER BY yarn_count",'id','yarn_count');
		$color_array=return_library_array( "select id, color_name from lib_color WHERE status_active = 1 AND is_deleted = 0 ORDER BY color_name",'id','color_name');

		$data_array=sql_select("select id, lot, product_name_details, yarn_count_id, unit_of_measure from product_details_master where id in($item_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
            	<td>
                    <input type="text" name="lot_<?php echo $tblRow; ?>" id="lot_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('lot')];?>" style="width:80px" maxlength="200" onDblClick="openmypage_item_desc(<?php echo $tblRow; ?>);" placeholder="Double Click" readonly/>
                    <input type="hidden" name="itemProdId_<?php echo $tblRow; ?>" id="itemProdId_<?php echo $tblRow; ?>" readonly value="<?php echo $row[csf('id')];?>"/> 
                </td>
                <td>
                    <?php echo create_drop_down( "countName_".$tblRow, 90, $count_arr,'', 1, '-Select-', $row[csf('yarn_count_id')],"",1); ?>
                </td>
                <td>
                    <input type="text" name="itemdescription_<?php echo $tblRow; ?>" id="itemdescription_<?php echo $tblRow; ?>" class="text_boxes" style="width:200px" value="<?php echo $row[csf('product_name_details')];?>" disabled/>
                </td>
                <td>
                    <?php echo create_drop_down( "yarnColor_".$tblRow, 110, $color_array,'', 1, '-Select-',0,"",0); ?>
                </td>
                <td>
                    <?php echo create_drop_down( "colorRange_".$tblRow, 110, $color_range,'', 1, '-Select-',0,"",0); ?>
                </td>
                <td>
                    <?php
                        echo create_drop_down( "uom_".$tblRow, 60, $unit_of_measurement,'', 0, '',12,'12',1); 
                    ?>
                </td>
				<td>
					<input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="" style="width:61px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="" style="width:45px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="" style="width:75px;" readonly/>
					<input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly/>
				</td>
				<td width="65">
					<input type="button" id="increase_<?php echo $tblRow; ?>" name="increase_<?php echo $tblRow; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $tblRow; ?> )" />
					<input type="button" id="decrease_<?php echo $tblRow; ?>" name="decrease_<?php echo $tblRow; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $tblRow; ?>);" />
				</td>
			</tr>
		<?php
		}
	}
	else
	{
		$data_array=sql_select("select id, item_group_id, item_description, item_size, unit_of_measure from product_details_master where id in($item_id)");
		foreach($data_array as $row)
		{
			$tblRow++;
			?>
			<tr class="general" id="row_<?php echo $tblRow; ?>">
				<td> 
					<?php
						echo create_drop_down( "itemgroupid_".$tblRow, 130, "SELECT id,item_name FROM lib_item_group WHERE item_category =$item_category_id AND status_active = 1 AND is_deleted = 0 ORDER BY item_name ASC",'id,item_name', 1, '-Select-',$row[csf('item_group_id')],"get_php_form_data( this.value+'**'+'uom_$tblRow', 'get_uom', 'requires/pi_controller' );",1); 
					?>  
				</td>
				<td>
					<input type="text" name="itemdescription_<?php echo $tblRow; ?>" id="itemdescription_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('item_description')]; ?>" style="width:200px" maxlength="200" <?php echo $disable; ?> onDblClick="openmypage_item_desc(<?php echo $tblRow; ?>);" placeholder="Double Click" readonly/>
					<input type="hidden" name="itemProdId_<?php echo $tblRow; ?>" id="itemProdId_<?php echo $tblRow; ?>" readonly value="<?php echo $row[csf('id')]; ?>"/> 
				</td>
				<td>
					<input type="text" name="itemSize_<?php echo $tblRow; ?>" id="itemSize_<?php echo $tblRow; ?>" class="text_boxes" value="<?php echo $row[csf('item_size')]; ?>" style="width:90px;" maxlength="50" disabled="disabled"/>
				</td>
				<td>
					<?php 
						echo create_drop_down( "uom_$tblRow", 80, $unit_of_measurement,'', 0, '',$row[csf('unit_of_measure')],'',1);            
					?>		
				</td>
				<td>
					<input type="text" name="quantity_<?php echo $tblRow; ?>" id="quantity_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="" style="width:90px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="rate_<?php echo $tblRow; ?>" id="rate_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="" style="width:75px;" onKeyUp="calculate_amount(<?php echo $tblRow; ?>)" />
				</td>
				<td>
					<input type="text" name="amount_<?php echo $tblRow; ?>" id="amount_<?php echo $tblRow; ?>" class="text_boxes_numeric" value="" style="width:85px;" readonly/>
					<input type="hidden" name="updateIdDtls_<?php echo $tblRow; ?>" id="updateIdDtls_<?php echo $tblRow; ?>" readonly/>
				</td>
				<td width="65">
					<input type="button" id="increase_<?php echo $tblRow; ?>" name="increase_<?php echo $tblRow; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $tblRow; ?> )" />
					<input type="button" id="decrease_<?php echo $tblRow; ?>" name="decrease_<?php echo $tblRow; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $tblRow; ?>);" />
				</td>
			</tr>
		<?php
		}
	}
	exit();
}
?>


 