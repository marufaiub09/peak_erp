-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ac_coa_mst`
--

DROP TABLE IF EXISTS `ac_coa_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_coa_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(50) DEFAULT NULL,
  `restricted_user_id` varchar(50) DEFAULT NULL,
  `ac_subgroup_id` int(11) DEFAULT NULL,
  `ac_code` int(20) NOT NULL,
  `ac_description` varchar(100) NOT NULL,
  `control_ac_id` int(2) DEFAULT NULL,
  `ac_nature_id` int(2) DEFAULT NULL,
  `inter_company_ac_with` int(10) DEFAULT NULL,
  `currency_id` int(11) NOT NULL,
  `income_summery_ac` int(11) DEFAULT NULL,
  `original_depriciation_ac` int(11) DEFAULT '0',
  `costcenter_based` tinyint(1) NOT NULL COMMENT '0=no,1=yes',
  `inserted_by` varchar(32) DEFAULT NULL COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes;2=delete',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ac_coa_mst`
--

LOCK TABLES `ac_coa_mst` WRITE;
/*!40000 ALTER TABLE `ac_coa_mst` DISABLE KEYS */;
/*!40000 ALTER TABLE `ac_coa_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:38
