-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pre_cost_fabric_cost_dtls_h`
--

DROP TABLE IF EXISTS `wo_pre_cost_fabric_cost_dtls_h`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pre_cost_fabric_cost_dtls_h` (
  `id` int(11) NOT NULL DEFAULT '0',
  `approved_no` int(11) DEFAULT NULL,
  `pre_cost_fabric_cost_dtls_id` int(11) DEFAULT NULL,
  `job_no` varchar(25) NOT NULL,
  `item_number_id` int(11) NOT NULL DEFAULT '0',
  `body_part_id` int(11) NOT NULL DEFAULT '0',
  `fab_nature_id` int(11) NOT NULL DEFAULT '0',
  `color_type_id` int(11) NOT NULL DEFAULT '0',
  `lib_yarn_count_deter_id` int(11) NOT NULL DEFAULT '0',
  `construction` varchar(150) NOT NULL,
  `composition` varchar(200) NOT NULL,
  `fabric_description` varchar(150) NOT NULL,
  `gsm_weight` int(11) NOT NULL DEFAULT '0',
  `color_size_sensitive` int(11) NOT NULL DEFAULT '0',
  `color` int(11) NOT NULL DEFAULT '0',
  `avg_cons` double NOT NULL DEFAULT '0',
  `fabric_source` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `avg_finish_cons` double NOT NULL,
  `avg_process_loss` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` int(1) NOT NULL DEFAULT '1',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `costing_per` int(11) NOT NULL DEFAULT '0',
  `consumption_basis` int(11) NOT NULL DEFAULT '0',
  `process_loss_method` int(11) NOT NULL DEFAULT '0',
  `cons_breack_down` text NOT NULL,
  `msmnt_break_down` text NOT NULL,
  `color_break_down` text NOT NULL,
  `yarn_breack_down` varchar(150) NOT NULL,
  `marker_break_down` text NOT NULL,
  `width_dia_type` tinyint(1) NOT NULL DEFAULT '0',
  KEY `job_no` (`job_no`),
  KEY `item_number_id` (`item_number_id`),
  KEY `body_part_id` (`body_part_id`),
  KEY `fab_nature_id` (`fab_nature_id`),
  KEY `color_type_id` (`color_type_id`),
  KEY `lib_yarn_count_determination_id` (`lib_yarn_count_deter_id`),
  KEY `fabric_description` (`fabric_description`),
  KEY `color_size_sensitive` (`color_size_sensitive`),
  KEY `color` (`color`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`),
  KEY `construction` (`construction`),
  KEY `composition` (`composition`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pre_cost_fabric_cost_dtls_h`
--

LOCK TABLES `wo_pre_cost_fabric_cost_dtls_h` WRITE;
/*!40000 ALTER TABLE `wo_pre_cost_fabric_cost_dtls_h` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_pre_cost_fabric_cost_dtls_h` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
