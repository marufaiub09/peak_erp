-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `wo_trim_booking_data_park`
--

DROP TABLE IF EXISTS `wo_trim_booking_data_park`;
/*!50001 DROP VIEW IF EXISTS `wo_trim_booking_data_park`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `wo_trim_booking_data_park` (
 `id` tinyint NOT NULL,
  `job_no` tinyint NOT NULL,
  `company_name` tinyint NOT NULL,
  `pub_shipment_date` tinyint NOT NULL,
  `grouping` tinyint NOT NULL,
  `wo_pre_cost_trim_cost_dtls` tinyint NOT NULL,
  `cons` tinyint NOT NULL,
  `costing_per` tinyint NOT NULL,
  `buyer_name` tinyint NOT NULL,
  `style_ref_no` tinyint NOT NULL,
  `po_id` tinyint NOT NULL,
  `po_number` tinyint NOT NULL,
  `trim_group` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `brand_sup_ref` tinyint NOT NULL,
  `req_qnty` tinyint NOT NULL,
  `cons_uom` tinyint NOT NULL,
  `cu_woq` tinyint NOT NULL,
  `bal_woq` tinyint NOT NULL,
  `rate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `wo_trim_booking_data_park`
--

/*!50001 DROP TABLE IF EXISTS `wo_trim_booking_data_park`*/;
/*!50001 DROP VIEW IF EXISTS `wo_trim_booking_data_park`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `wo_trim_booking_data_park` AS select `d`.`id` AS `id`,`a`.`job_no` AS `job_no`,`a`.`company_name` AS `company_name`,`b`.`pub_shipment_date` AS `pub_shipment_date`,`b`.`grouping` AS `grouping`,`c`.`id` AS `wo_pre_cost_trim_cost_dtls`,`d`.`cons` AS `cons`,`e`.`costing_per` AS `costing_per`,`a`.`buyer_name` AS `buyer_name`,`a`.`style_ref_no` AS `style_ref_no`,`b`.`id` AS `po_id`,`b`.`po_number` AS `po_number`,`c`.`trim_group` AS `trim_group`,`c`.`description` AS `description`,`c`.`brand_sup_ref` AS `brand_sup_ref`,(case `e`.`costing_per` when 1 then round((((`d`.`cons` / 12) * `b`.`plan_cut`) / `cc`.`conversion_factor`),4) when 2 then round((((`d`.`cons` / 1) * `b`.`plan_cut`) / `cc`.`conversion_factor`),4) when 3 then round((((`d`.`cons` / 24) * `b`.`plan_cut`) / `cc`.`conversion_factor`),4) when 4 then round((((`d`.`cons` / 36) * `b`.`plan_cut`) / `cc`.`conversion_factor`),4) when 5 then round((((`d`.`cons` / 48) * `b`.`plan_cut`) / `cc`.`conversion_factor`),4) else 0 end) AS `req_qnty`,`cc`.`order_uom` AS `cons_uom`,if((`f`.`wo_qnty` <> ''),round(sum(`f`.`wo_qnty`),4),0) AS `cu_woq`,(case `e`.`costing_per` when 1 then round(((((`d`.`cons` / 12) * `b`.`plan_cut`) / `cc`.`conversion_factor`) - if((`f`.`wo_qnty` <> ''),round(sum(`f`.`wo_qnty`),4),0)),4) when 2 then round(((((`d`.`cons` / 1) * `b`.`plan_cut`) / `cc`.`conversion_factor`) - if((`f`.`wo_qnty` <> ''),round(sum(`f`.`wo_qnty`),4),0)),4) when 3 then round(((((`d`.`cons` / 24) * `b`.`plan_cut`) / `cc`.`conversion_factor`) - if((`f`.`wo_qnty` <> ''),round(sum(`f`.`wo_qnty`),4),0)),4) when 4 then round(((((`d`.`cons` / 36) * `b`.`plan_cut`) / `cc`.`conversion_factor`) - if((`f`.`wo_qnty` <> ''),round(sum(`f`.`wo_qnty`),4),0)),4) when 5 then round(((((`d`.`cons` / 48) * `b`.`plan_cut`) / `cc`.`conversion_factor`) - if((`f`.`wo_qnty` <> ''),round(sum(`f`.`wo_qnty`),4),0)),4) else 0 end) AS `bal_woq`,round((`c`.`rate` * `cc`.`conversion_factor`),8) AS `rate` from (((((`wo_po_details_master` `a` join `wo_po_break_down` `b`) join `wo_pre_cost_mst` `e`) join `wo_pre_cost_trim_cost_dtls` `c`) join `lib_item_group` `cc`) join (`wo_pre_cost_trim_co_cons_dtls` `d` left join `wo_booking_dtls` `f` on(((`f`.`job_no` = `d`.`job_no`) and (`f`.`pre_cost_fabric_cost_dtls_id` = `d`.`wo_pre_cost_trim_cost_dtls_id`) and (`f`.`po_break_down_id` = `d`.`po_break_down_id`) and (`f`.`booking_type` = 2))))) where ((`a`.`job_no` = `b`.`job_no_mst`) and (`a`.`job_no` = `c`.`job_no`) and (`a`.`job_no` = `e`.`job_no`) and (`a`.`job_no` = `d`.`job_no`) and (`c`.`id` = `d`.`wo_pre_cost_trim_cost_dtls_id`) and (`b`.`id` = `d`.`po_break_down_id`) and (`cc`.`id` = `c`.`trim_group`)) group by `d`.`id` order by `b`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:47
