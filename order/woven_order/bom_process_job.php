﻿<?php
/*--------------------------------------------Comments----------------
Version (MySql)          :  V1
Version (Oracle)         :  V1
Converted by             :  MONZU
Converted Date           :  
Purpose			         : 	This form will create Bom Process
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 	17-11-2014
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 		
Update date		         : 		   
QC Performed BY	         :		
QC Date			         :	
Comments		         :
----------------------------------------------------------------------*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
echo load_html_head_contents("Order Info","../../", 1, 1, $unicode,1,'');
?>	
<script>

</script>
</head>
<body>
<?php
//echo load_freeze_divs ("../../",$permission); 
$gmtsitem_ratio_array=array();
$gmtsitem_ratio_sql=sql_select("select job_no,gmts_item_id,set_item_ratio from wo_po_details_mas_set_details  where job_no ='FAL-14-01157' ");// where job_no ='FAL-14-01157'
foreach($gmtsitem_ratio_sql as $gmtsitem_ratio_sql_row)
{
$gmtsitem_ratio_array[$gmtsitem_ratio_sql_row[csf('job_no')]][$gmtsitem_ratio_sql_row[csf('gmts_item_id')]]=$gmtsitem_ratio_sql_row[csf('set_item_ratio')];	
}

$costing_per_arr=return_library_array( "select job_no,costing_per from wo_pre_cost_mst where job_no ='FAL-14-01157' ", "job_no", "costing_per");// where job_no ='FAL-14-01157'




// Fabric Part=======================
$cons_arr= array();
$sql_pre_cost_cons=sql_select("select
    a.job_no,
    b.po_break_down_id,
    a.id as pre_cost_fabric_cost_dtls_id,
    a.item_number_id,
    a.fab_nature_id,
    b.color_size_table_id,
    b.color_number_id,
    b.gmts_sizes as size_number_id,
    b.cons,
    b.requirment,
    a.rate 
	FROM
    wo_pre_cost_fabric_cost_dtls a,
    wo_pre_cos_fab_co_avg_con_dtls b
WHERE
    a.job_no=b.job_no and
    a.id=b.pre_cost_fabric_cost_dtls_id and
    a.job_no='FAL-14-01157' and
    a.status_active=1 and a.is_deleted=0
    order by a.id");// a.job_no='FAL-14-01157' and 
foreach($sql_pre_cost_cons as $cons_row)
{
$cons_arr[$cons_row[csf('job_no')]][$cons_row[csf('po_break_down_id')]][$cons_row[csf('pre_cost_fabric_cost_dtls_id')]][$cons_row[csf('item_number_id')]][$cons_row[csf('color_number_id')]][$cons_row[csf('size_number_id')]]['cons']=$cons_row[csf('cons')];
$cons_arr[$cons_row[csf('job_no')]][$cons_row[csf('po_break_down_id')]][$cons_row[csf('pre_cost_fabric_cost_dtls_id')]][$cons_row[csf('item_number_id')]][$cons_row[csf('color_number_id')]][$cons_row[csf('size_number_id')]]['requirment']=$cons_row[csf('requirment')];
$cons_arr[$cons_row[csf('job_no')]][$cons_row[csf('po_break_down_id')]][$cons_row[csf('pre_cost_fabric_cost_dtls_id')]][$cons_row[csf('item_number_id')]][$cons_row[csf('color_number_id')]][$cons_row[csf('size_number_id')]]['rate']=$cons_row[csf('rate')];
}
//print_r($cons_arr); 

$application_data=array();
//$po_qty_array=array();
$sql="select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.fab_nature_id,d.fabric_source   from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_fabric_cost_dtls d where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no  and b.id=c.po_break_down_id and a.job_no='FAL-14-01157' and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 order by b.id,pre_cost_dtls_id";//and a.job_no='FAL-14-01157'
$data_arr=sql_select($sql);
foreach($data_arr as $row)
{
	$costing_per_qty=0;
	$costing_per=$costing_per_arr[$row[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	$set_item_ratio=$gmtsitem_ratio_array[$row[csf('job_no')]][$row[csf('item_number_id')]];
	$cons=$cons_arr[$row[csf('job_no')]][$row[csf('id')]][$row[csf('pre_cost_dtls_id')]][$row[csf('item_number_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['cons'];
	$requirment=$cons_arr[$row[csf('job_no')]][$row[csf('id')]][$row[csf('pre_cost_dtls_id')]][$row[csf('item_number_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['requirment'];
	$rate=0;
	$rate=$cons_arr[$row[csf('job_no')]][$row[csf('id')]][$row[csf('pre_cost_dtls_id')]][$row[csf('item_number_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['rate'];
	if($rate=="")
	{
	  $rate=0;	
	}
	$req_fin_fab_qnty =def_number_format(($row[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$cons,5,"");
	$req_grey_fab_qnty =def_number_format(($row[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$requirment,5,"");
	$amount=def_number_format(($req_grey_fab_qnty*$rate),5,"");
	if($row[csf('fab_nature_id')]==2)
	{
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_finish_req']+=$req_fin_fab_qnty;
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_grey_req']+=$req_grey_fab_qnty;
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_amount']+=$amount;
	
	
		if($row[csf('fabric_source')]==1)
		{
		$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_finish_req_prod']+=$req_fin_fab_qnty;	
		$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_grey_req_prod']+=$req_grey_fab_qnty;
		}
		if($row[csf('fabric_source')]==2)
		{
		 $application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_finish_req_purc']+=$req_fin_fab_qnty;	
		 $application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_grey_req_purc']+=$req_grey_fab_qnty;
		}
		if($row[csf('fabric_source')]==3)
		{
		$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_finish_req_buysu']+=$req_fin_fab_qnty;
		$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['kint_grey_req_buysu']+=$req_grey_fab_qnty;
		}
		
	}
	if($row[csf('fab_nature_id')]==3)
	{
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_finish_req']+=$req_fin_fab_qnty;
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_grey_req']+=$req_grey_fab_qnty;
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['wovent_amount']+=$amount;
	
	    if($row[csf('fabric_source')]==1)
		{
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_finish_req_prod']+=$req_fin_fab_qnty;
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_grey_req_prod']+=$req_grey_fab_qnty;
		}
		if($row[csf('fabric_source')]==2)
		{
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_finish_req_purc']+=$req_fin_fab_qnty;
	$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_grey_req_purc']+=$req_grey_fab_qnty;
		}
		if($row[csf('fabric_source')]==3)
		{
		$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_finish_req_buysu']+=$req_fin_fab_qnty;
		$application_data[$row[csf('job_no')]][$row[csf('id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]['woven_grey_req_buysu']+=$req_grey_fab_qnty;
		}
	}
}
// Fabric Part End=======================


// Yarn 
$yarn_data=array();
$sql_yarn="select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.fab_nature_id,e.cons,F.CONS_QNTY,F.RATE,F.AMOUNT   from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_fabric_cost_dtls d,wo_pre_cos_fab_co_avg_con_dtls e,wo_pre_cost_fab_yarn_cost_dtls f where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no and a.job_no=e.job_no and a.job_no=f.job_no and b.id=c.po_break_down_id and d.id=e.pre_cost_fabric_cost_dtls_id and c.po_break_down_id=e.po_break_down_id and C.COLOR_NUMBER_ID=E.COLOR_NUMBER_ID and C.SIZE_NUMBER_ID=E.GMTS_SIZES and D.ID=F.FABRIC_COST_DTLS_ID and a.job_no='FAL-14-01157' and e.cons !=0 and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 order by b.id,pre_cost_dtls_id";
$data_arr_yarn=sql_select($sql_yarn);
foreach($data_arr_yarn as $yarn_row)
{
    $costing_per_qty=0;
	$costing_per=$costing_per_arr[$yarn_row[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	
	$set_item_ratio=$gmtsitem_ratio_array[$yarn_row[csf('job_no')]][$yarn_row[csf('item_number_id')]];
	$reqyarnqnty =def_number_format(($yarn_row[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$yarn_row[csf("CONS_QNTY")],5,"");
	$yarnamount=def_number_format(($reqyarnqnty*$yarn_row[csf("RATE")]),5,"");
	$yarn_data[$yarn_row[csf('job_no')]][$yarn_row[csf('id')]][$yarn_row[csf('item_number_id')]][$yarn_row[csf('country_id')]][$yarn_row[csf('color_number_id')]][$yarn_row[csf('size_number_id')]]['req_yarn_qnty']+=$reqyarnqnty;
	$yarn_data[$yarn_row[csf('job_no')]][$yarn_row[csf('id')]][$yarn_row[csf('item_number_id')]][$yarn_row[csf('country_id')]][$yarn_row[csf('color_number_id')]][$yarn_row[csf('size_number_id')]]['yarn_amount']+=$yarnamount;

}


//Yarn End


// Conversion 
$conv_data=array();
$sql_conv="select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.fab_nature_id,e.cons,F.req_qnty,F.charge_unit,F.amount,f.color_break_down   from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_fabric_cost_dtls d,wo_pre_cos_fab_co_avg_con_dtls e,wo_pre_cost_fab_conv_cost_dtls f where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no and a.job_no=e.job_no and a.job_no=f.job_no and b.id=c.po_break_down_id and d.id=e.pre_cost_fabric_cost_dtls_id and c.po_break_down_id=e.po_break_down_id and C.COLOR_NUMBER_ID=E.COLOR_NUMBER_ID and C.SIZE_NUMBER_ID=E.GMTS_SIZES and D.ID=F.fabric_description and a.job_no='FAL-14-01157' and e.cons !=0 and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1
UNION ALL
select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.fab_nature_id,e.cons,F.req_qnty,F.charge_unit,F.amount,f.color_break_down   from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_fabric_cost_dtls d,wo_pre_cos_fab_co_avg_con_dtls e,wo_pre_cost_fab_conv_cost_dtls f where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no and a.job_no=e.job_no and a.job_no=f.job_no and b.id=c.po_break_down_id and d.id=e.pre_cost_fabric_cost_dtls_id and c.po_break_down_id=e.po_break_down_id and C.COLOR_NUMBER_ID=E.COLOR_NUMBER_ID and C.SIZE_NUMBER_ID=E.GMTS_SIZES and F.fabric_description=0 and a.job_no='FAL-14-01157' and e.cons !=0 and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 ";
$data_arr_conv=sql_select($sql_conv);
foreach($data_arr_conv as $conv_row)
{
    $costing_per_qty=0;
	$costing_per=$costing_per_arr[$conv_row[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	
	$set_item_ratio=$gmtsitem_ratio_array[$conv_row[csf('job_no')]][$conv_row[csf('item_number_id')]];
	$convcolorrate=array();
	if($conv_row[csf('color_break_down')] !="")
	{
		$arr_1=explode("__",$conv_row[csf('color_break_down')]);
		for($ci=0;$ci<count($arr_1);$ci++)
		{
		$arr_2=explode("_",$arr_1[$ci]);
		$convcolorrate[$arr_2[0]]=$arr_2[1];
			
		}
	}
	//print_r($convcolorrate);
	//echo "<br/>";
	$convrate=0;
	$convqnty =def_number_format(($conv_row[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$conv_row[csf("req_qnty")],5,"");
	if($conv_row[csf('color_break_down')] !="")
	{
	$convrate=$convcolorrate[$conv_row[csf('color_number_id')]];
	}
	else
	{
	$convrate=$conv_row[csf('charge_unit')];
	}
	
	$convamount=def_number_format($convqnty*$convrate,5,"");
	$conv_data[$conv_row[csf('job_no')]][$conv_row[csf('id')]][$conv_row[csf('item_number_id')]][$conv_row[csf('country_id')]][$conv_row[csf('color_number_id')]][$conv_row[csf('size_number_id')]]['conv_qnty']+=$convqnty;
	$conv_data[$conv_row[csf('job_no')]][$conv_row[csf('id')]][$conv_row[csf('item_number_id')]][$conv_row[csf('country_id')]][$conv_row[csf('color_number_id')]][$conv_row[csf('size_number_id')]]['conv_amount']+=$convamount;

}

//die;
//Conversion End

// Embellish And Wash Part =======================

$emb_data=array();
$wash_data=array();
$sqlemb="select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.emb_name,d.emb_type,d. cons_dzn_gmts,d.rate,d. amount   from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_embe_cost_dtls d where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no  and b.id=c.po_break_down_id and a.job_no='FAL-14-01157' and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 order by b.id,pre_cost_dtls_id";//and a.job_no='FAL-14-01157'
$data_arremb=sql_select($sqlemb);
foreach($data_arremb as $rowemb)
{
	$costing_per_qty=0;
	$costing_per=$costing_per_arr[$rowemb[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	$set_item_ratio=$gmtsitem_ratio_array[$rowemb[csf('job_no')]][$rowemb[csf('item_number_id')]];
	
	
	$emb_req =def_number_format(($rowemb[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowemb[csf("cons_dzn_gmts")],5,"");
	$embamount=def_number_format(($emb_req*$rowemb[csf("rate")]),5,"");
	
	
	if($rowemb[csf('emb_name')]==3)
	{
	$wash_data[$rowemb[csf('job_no')]][$rowemb[csf('id')]][$rowemb[csf('item_number_id')]][$rowemb[csf('country_id')]][$rowemb[csf('color_number_id')]][$rowemb[csf('size_number_id')]]['wash_cons']+=$emb_req;
	//$application_data[$rowemb[csf('job_no')]][$rowemb[csf('id')]][$rowemb[csf('item_number_id')]][$rowemb[csf('country_id')]][$rowemb[csf('color_number_id')]][$rowemb[csf('size_number_id')]]['wash_rate']+=$rowemb[csf("rate")];
	$wash_data[$rowemb[csf('job_no')]][$rowemb[csf('id')]][$rowemb[csf('item_number_id')]][$rowemb[csf('country_id')]][$rowemb[csf('color_number_id')]][$rowemb[csf('size_number_id')]]['wash_amount']+=$embamount;
	}
	else
	{
	$emb_data[$rowemb[csf('job_no')]][$rowemb[csf('id')]][$rowemb[csf('item_number_id')]][$rowemb[csf('country_id')]][$rowemb[csf('color_number_id')]][$rowemb[csf('size_number_id')]]['emb_cons']+=$emb_req;
	//$application_data[$rowemb[csf('job_no')]][$rowemb[csf('id')]][$rowemb[csf('item_number_id')]][$rowemb[csf('country_id')]][$rowemb[csf('color_number_id')]][$rowemb[csf('size_number_id')]]['emb_rate']+=$rowemb[csf("rate")];
	$emb_data[$rowemb[csf('job_no')]][$rowemb[csf('id')]][$rowemb[csf('item_number_id')]][$rowemb[csf('country_id')]][$rowemb[csf('color_number_id')]][$rowemb[csf('size_number_id')]]['emb_amount']+=$embamount;
	}
}
// Embellish And Wash Part End=======================

// Commercial  Part =======================

$commercial_data=array();
$sqlcommercial="select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.rate,d. amount   from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_comarci_cost_dtls d where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no  and b.id=c.po_break_down_id and a.job_no='FAL-14-01157' and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 order by b.id,pre_cost_dtls_id";//and a.job_no='FAL-14-01157'
$data_arrcommercial=sql_select($sqlcommercial);
foreach($data_arrcommercial as $rowecommrecail)
{
	$costing_per_qty=0;
	$costing_per=$costing_per_arr[$rowecommrecail[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	$set_item_ratio=$gmtsitem_ratio_array[$rowecommrecail[csf('job_no')]][$rowecommrecail[csf('item_number_id')]];
	
	$commercialamount =def_number_format(($rowecommrecail[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowecommrecail[csf("rate")],5,"");
	
	$commercial_data[$rowecommrecail[csf('job_no')]][$rowecommrecail[csf('id')]][$rowecommrecail[csf('item_number_id')]][$rowecommrecail[csf('country_id')]][$rowecommrecail[csf('color_number_id')]][$rowecommrecail[csf('size_number_id')]]['commercial_amount']+=$commercialamount;
}
// Commercial Part End=======================


// Commision  Part =======================

$commision_data=array();
$sqlcommision="select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.commision_rate,d.commission_amount   from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_commiss_cost_dtls d where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no  and b.id=c.po_break_down_id and a.job_no='FAL-14-01157' and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 order by b.id,pre_cost_dtls_id";//and a.job_no='FAL-14-01157'
$data_arrcommision=sql_select($sqlcommision);
foreach($data_arrcommision as $rowcommision)
{
	$costing_per_qty=0;
	$costing_per=$costing_per_arr[$rowcommision[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	$set_item_ratio=$gmtsitem_ratio_array[$rowcommision[csf('job_no')]][$rowcommision[csf('item_number_id')]];
	
	$commisionamount =def_number_format(($rowcommision[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowcommision[csf("commision_rate")],5,"");
	
	$commision_data[$rowcommision[csf('job_no')]][$rowcommision[csf('id')]][$rowcommision[csf('item_number_id')]][$rowcommision[csf('country_id')]][$rowcommision[csf('color_number_id')]][$rowcommision[csf('size_number_id')]]['commision_amount']+=$commisionamount;
}
// Commision Part End=======================

// others  Part =======================

$lab_test_data=array();
$inspection_data=array();
$cm_cost_data=array();
$freight_data=array();
$currier_data=array();
$certificate_data=array();
$common_oh_data=array();

$sqlothers="select a.job_no,b.id,c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty ,d.id as pre_cost_dtls_id,d.lab_test,d.inspection,d.cm_cost,d.freight,d.currier_pre_cost,d.certificate_pre_cost,d.common_oh  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c,wo_pre_cost_dtls d where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and a.job_no=d.job_no  and b.id=c.po_break_down_id and a.job_no='FAL-14-01157' and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 order by b.id,pre_cost_dtls_id";//and a.job_no='FAL-14-01157'
$data_others=sql_select($sqlothers);
foreach($data_others as $rowothers)
{
	$costing_per_qty=0;
	$costing_per=$costing_per_arr[$rowothers[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	$set_item_ratio=$gmtsitem_ratio_array[$rowothers[csf('job_no')]][$rowothers[csf('item_number_id')]];
	
	$labtestdata =def_number_format(($rowothers[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowothers[csf("lab_test")],5,"");
	$inspection =def_number_format(($rowothers[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowothers[csf("inspection")],5,"");
	$cmcost =def_number_format(($rowothers[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowothers[csf("cm_cost")],5,"");
	$freight =def_number_format(($rowothers[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowothers[csf("freight")],5,"");
	
	$currierprecost =def_number_format(($rowothers[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowothers[csf("currier_pre_cost")],5,"");
	
	$certificateprecost =def_number_format(($rowothers[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowothers[csf("certificate_pre_cost")],5,"");
	$commonoh =def_number_format(($rowothers[csf("plan_cut_qnty")]/($costing_per_qty*$set_item_ratio))*$rowothers[csf("common_oh")],5,"");
	
	$lab_test_data[$rowothers[csf('job_no')]][$rowothers[csf('id')]][$rowothers[csf('item_number_id')]][$rowothers[csf('country_id')]][$rowothers[csf('color_number_id')]][$rowothers[csf('size_number_id')]]['lab_test']+=$labtestdata;
	
	$inspection_data[$rowothers[csf('job_no')]][$rowothers[csf('id')]][$rowothers[csf('item_number_id')]][$rowothers[csf('country_id')]][$rowothers[csf('color_number_id')]][$rowothers[csf('size_number_id')]]['inspection']+=$inspection;
	
	$cm_cost_data[$rowothers[csf('job_no')]][$rowothers[csf('id')]][$rowothers[csf('item_number_id')]][$rowothers[csf('country_id')]][$rowothers[csf('color_number_id')]][$rowothers[csf('size_number_id')]]['cm_cost']+=$cmcost;
	
	$freight_data[$rowothers[csf('job_no')]][$rowothers[csf('id')]][$rowothers[csf('item_number_id')]][$rowothers[csf('country_id')]][$rowothers[csf('color_number_id')]][$rowothers[csf('size_number_id')]]['freight']+=$freight;
	
	$currier_data[$rowothers[csf('job_no')]][$rowothers[csf('id')]][$rowothers[csf('item_number_id')]][$rowothers[csf('country_id')]][$rowothers[csf('color_number_id')]][$rowothers[csf('size_number_id')]]['currier_pre_cost']+=$currierprecost;
	
	$certificate_data[$rowothers[csf('job_no')]][$rowothers[csf('id')]][$rowothers[csf('item_number_id')]][$rowothers[csf('country_id')]][$rowothers[csf('color_number_id')]][$rowothers[csf('size_number_id')]]['certificate_pre_cost']+=$certificateprecost;
	
	$common_oh_data[$rowothers[csf('job_no')]][$rowothers[csf('id')]][$rowothers[csf('item_number_id')]][$rowothers[csf('country_id')]][$rowothers[csf('color_number_id')]][$rowothers[csf('size_number_id')]]['common_oh']+=$commonoh;



}
// others Part End=======================





$sql_in="select a.job_no,b.id,c.id as color_size_table_id, c.item_number_id,c.country_id,c.color_number_id,c.size_number_id,c.order_quantity ,c.plan_cut_qnty    from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c  where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst  and b.id=c.po_break_down_id and a.job_no='FAL-14-01157' and a.company_name=1   and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 order by b.id";//and a.job_no='FAL-14-01157'
$data_arr_in=sql_select($sql_in);
$i=1;
$id=return_next_id( "id", " wo_bom_process", 1 ) ;
foreach($data_arr_in as $row_in)
{
	
	
	$costing_per_qty=0;
	$costing_per=$costing_per_arr[$row_in[csf('job_no')]];
	if($costing_per==1)
	{
	$costing_per_qty=12	;
	}
	if($costing_per==2)
	{
	$costing_per_qty=1;	
	}
	if($costing_per==3)
	{
	$costing_per_qty=24	;
	}
	if($costing_per==4)
	{
	$costing_per_qty=36	;
	}
	if($costing_per==5)
	{
	$costing_per_qty=48	;
	}
	$set_item_ratio=$gmtsitem_ratio_array[$row_in[csf('job_no')]][$row_in[csf('item_number_id')]];
	
	
	
	$kint_finish_req =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_finish_req'];
	
	$kint_finish_req_prod =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_finish_req_prod'];
	$kint_finish_req_purc =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_finish_req_purc'];
	$kint_finish_req_buysu =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_finish_req_buysu'];
	
	
	$kint_grey_req =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_grey_req'];
	
	$kint_grey_req_prod =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_grey_req_prod'];
	$kint_grey_req_purc =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_grey_req_purc'];
	$kint_grey_req_buysu =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_grey_req_buysu'];
	
	
	$kint_amount=$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['kint_amount'];
	$kint_rate=def_number_format($kint_amount/$kint_grey_req,5,"");
	
	$woven_finish_req =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_finish_req'];
	
	$woven_finish_req_prod =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_finish_req_prod'];
	$woven_finish_req_purc =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_finish_req_purc'];
	$woven_finish_req_buysu =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_finish_req_buysu'];
	
	
	$woven_grey_req =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_grey_req'];
	
	$woven_grey_req_prod =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_grey_req_prod'];
	$woven_grey_req_purc =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_grey_req_purc'];
	$woven_grey_req_buysu =$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['woven_grey_req_buysu'];
	
	
	$wovent_amount=$application_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['wovent_amount'];
	$woven_rate=def_number_format($wovent_amount/$woven_grey_req,5,"");
	
	//Yarn
	$req_yarn_qnty=$yarn_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['req_yarn_qnty'];
	$yarn_amount=$yarn_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['yarn_amount'];
	$yarn_rate=def_number_format($yarn_amount/$req_yarn_qnty,5,"");
	
	
	//Yarn End
	
	//Conversion
	$conv_qnty=$conv_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['conv_qnty'];
	$conv_amount=$conv_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['conv_amount'];
	$conv_rate=def_number_format($conv_amount/$conv_qnty,5,"");
	
	
	//Conversion End
	
	
	//Embellish and Wash
	$emb_cons =$emb_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['emb_cons'];
	$emb_amount=$emb_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['emb_amount'];
	$emb_rate=def_number_format($emb_amount/$emb_cons,5,"");
	
	$wash_cons =$wash_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['wash_cons'];
	$wash_amount=$wash_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['wash_amount'];
	$wash_rate=def_number_format($wash_amount/$wash_cons,5,"");
	
	//Embellish and Wash End
	
	//commercial 
	$commercial_amount=$commercial_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['commercial_amount'];
	$commercial_rate=def_number_format($commercial_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	//commercial End
	
	//Commision 
	$commision_amount=$commision_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['commision_amount'];
	$commision_rate=def_number_format($commision_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	//Commision End
	
	//Others 
	$lab_test_amount=$lab_test_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['lab_test'];
	$lab_test_rate=def_number_format($lab_test_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	
	$inspection_amount=$inspection_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['inspection'];
	$inspection_rate=def_number_format($inspection_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	
	$cm_cost_amount=$cm_cost_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['cm_cost'];
	$cm_cost_rate=def_number_format($cm_cost_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	
	$freight_amount=$freight_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['freight'];
	$freight_rate=def_number_format($freight_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	
	$currier_amount=$currier_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['currier_pre_cost'];

	$currier_rate=def_number_format($currier_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	$certificate_amount=$certificate_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['certificate_pre_cost'];

	$certificate_rate=def_number_format($certificate_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	
	$common_oh_amount=$common_oh_data[$row_in[csf('job_no')]][$row_in[csf('id')]][$row_in[csf('item_number_id')]][$row_in[csf('country_id')]][$row_in[csf('color_number_id')]][$row_in[csf('size_number_id')]]['common_oh'];

	$common_oh_rate=def_number_format($common_oh_amount/($row_in[csf('plan_cut_qnty')]/($costing_per_qty*$set_item_ratio)),5,"");
	
	
	
	
	//Others End
	
	$field_array="id,job_no,po_break_down_id,color_size_table_id,item_number_id,country_id,color_number_id,size_number_id,order_quantity,plan_cut_qnty,kint_fin_fab_qnty,kint_fin_fab_qnty_prod,kint_fin_fab_qnty_purc,kint_fin_fab_qnty_buysu,kint_grey_fab_qnty,kint_grey_fab_qnty_prod,kint_grey_fab_qnty_purc,kint_grey_fab_qnty_buysu,kint_rate,kint_amount,woven_fin_fab_qnty,woven_fin_fab_qnty_prod,woven_fin_fab_qnty_purc,woven_fin_fab_qnty_buysu,woven_grey_fab_qnty,woven_grey_fab_qnty_prod,woven_grey_fab_qnty_purc,woven_grey_fab_qnty_buysu,woven_rate,woven_amount,yarn_qnty,yarn_rate,yarn_amount,conv_qnty,conv_rate,conv_amount,emb_cons,emb_rate,emb_amount,wash_cons,wash_rate,wash_amount,commercial_rate,commercial_amount,commision_rate,commision_amount,lab_test_rate,lab_test_amount,inspection_rate,inspection_amount,cm_cost_rate,cm_cost_amount,freight_rate,freight_amount,currier_rate,currier_amount,certificate_rate,certificate_amount,common_oh_rate,common_oh_amount";//,woven_fin_fab_qnty,woven_grey_fab_qnty,woven_rate,woven_amount
	$con = connect();
	$data_array="(".$id.",'".$row_in[csf('job_no')]."',".$row_in[csf('id')].",".$row_in[csf('color_size_table_id')].",".$row_in[csf('item_number_id')].",".$row_in[csf('country_id')].",".$row_in[csf('color_number_id')].",".$row_in[csf('size_number_id')].",".$row_in[csf('order_quantity')].",".$row_in[csf('plan_cut_qnty')].",'".$kint_finish_req."','".$kint_finish_req_prod."','".$kint_finish_req_purc."','".$kint_finish_req_buysu."','".$kint_grey_req."','".$kint_grey_req_prod."','".$kint_grey_req_purc."','".$kint_grey_req_buysu."','".$kint_rate."','".$kint_amount."','".$woven_finish_req."','".$woven_finish_req_prod."','".$woven_finish_req_purc."','".$woven_finish_req_buysu."','".$woven_grey_req."','".$woven_grey_req_prod."','".$woven_grey_req_purc."','".$woven_grey_req_buysu."','".$woven_rate."','".$wovent_amount."','".$req_yarn_qnty."','".$yarn_rate."','".$yarn_amount."','".$conv_qnty."','".$conv_rate."','".$conv_amount."','".$emb_cons."','".$emb_rate."','".$emb_amount."','".$wash_cons."','".$wash_rate."','".$wash_amount."','".$commercial_rate."','".$commercial_amount."','".$commision_rate."','".$commision_amount."','".$lab_test_rate."','".$lab_test_amount."','".$inspection_rate."','".$inspection_amount."','".$cm_cost_rate."','".$cm_cost_amount."','".$freight_rate."','".$freight_amount."','".$currier_rate."','".$currier_amount."','".$certificate_rate."','".$certificate_amount."','".$common_oh_rate."','".$common_oh_amount."')";
    $rID=sql_insert("wo_bom_process",$field_array,$data_array,0);
	//echo "insert into wo_bom_process (".$field_array.")".$data_array;
	//die;
    oci_commit($con);
    $id++;
    $i++;
}


//print_r($application_data);
echo $i."row Inserted OK".$id;


?>
</body>
</html>