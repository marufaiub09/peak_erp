<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Color And Size Breakdown Report.
Functionality	:	
JS Functions	:
Created by		:	Kausar 
Creation date 	: 	29-12-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Color And Size Breakdown Report", "../../", 1, 1,$unicode,1,1);
?>	
<script>

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission = '<?php echo $permission; ?>';	

	function openmypage_job()
	{
/*		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
*/			var data = $("#cbo_company_id").val()+"_"+$("#cbo_buyer_id").val();
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/size_and_color_break_report_controller.php?data='+data+'&action=job_no_popup', 'Job No Search', 'width=700px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var theemailid=this.contentDoc.getElementById("txt_job_id");
				var response=theemailid.value.split('_');
				if ( theemailid.value!="" )
				{
					//alert (response[0]);
					freeze_window(5);
					$("#hidd_job_id").val(response[0]);
					$("#txt_job_no").val(response[1]);
					release_freezing();
				}
			}
		//}
	}
	
	function openmypage_po()
	{
/*		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
*/			var data = $("#cbo_company_id").val()+"_"+$("#cbo_buyer_id").val()+"_"+$("#txt_job_no").val();
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/size_and_color_break_report_controller.php?data='+data+'&action=po_no_popup', 'PO No Search', 'width=500px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var theemailid=this.contentDoc.getElementById("txt_po_id");
				var theemailval=this.contentDoc.getElementById("txt_po_val");
				if (theemailid.value!="" || theemailval.value!="")
				{
					//alert (theemailid.value);
					freeze_window(5);
					$("#hidd_po_id").val(theemailid.value);
					$("#txt_po_no").val(theemailval.value);
					release_freezing();
				}
			}
		//}
	}
	
	
function fn_report_generated(operation)
	{
		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
			var report_title=$( "div.form_caption" ).html();
			var data="action=report_generate"+get_submitted_data_string('cbo_company_id*cbo_buyer_id*txt_job_no*hidd_job_id*hidd_po_id*txt_date_from*txt_date_to',"../../")+'&report_title='+report_title;
			freeze_window(3);
			http.open("POST","requires/size_and_color_break_report_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
	
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			var tot_rows=reponse[2];
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
	 		show_msg('3');
			release_freezing();
		}
	}
	
function fn_report_generated_cutoff(operation)
	{
		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
			var report_title=$( "div.form_caption" ).html();
			var data="action=report_generate_cutoff"+get_submitted_data_string('cbo_company_id*cbo_buyer_id*txt_job_no*hidd_job_id*hidd_po_id*txt_date_from*txt_date_to',"../../")+'&report_title='+report_title;
			freeze_window(3);
			http.open("POST","requires/size_and_color_break_report_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_cutoff_reponse;
		}
	}
	
	function fn_report_generated_cutoff_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			var tot_rows=reponse[2];
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
	 		show_msg('3');
			release_freezing();
		}
	}
		
	
	
	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
function openmypage_job_color_size(page_link,title)
{
	//alert("monzu");
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
	{
	}
}

</script>
</head>
<body onLoad="set_hotkey();">
<div style="width:100%;" align="center">
    <?php echo load_freeze_divs ("../../",$permission);  ?><br />    		 
    <form name="colorsizebreak_1" id="colorsizebreak_1" autocomplete="off" > 
        <div style="width:100%;" align="center"><br>
            <fieldset style="width:870px;">
            <legend>Search Panel</legend> 
            <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" align="center">
                <thead>                    
                    <th width="150" class="must_entry_caption">Company Name</th>
                    <th width="130">Buyer</th>
                    <th width="100" >Job No.</th>
                    <th width="100" >PO No.</th>
                    <th width="" >Date</th>
                    <th width="170"><input type="reset" id="reset_btn" class="formbutton" style="width:70px" value="Reset" onClick="reset_form('colorsizebreak_1','report_container*report_container2','','','')" /></th>
                </thead>
                 <tbody>
                    <tr>
                        <td> 
							<?php
								echo create_drop_down( "cbo_company_id", 150, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down('requires/size_and_color_break_report_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                            ?>
                        </td>
                        <td id="buyer_td">
							<?php 
								echo create_drop_down( "cbo_buyer_id", 130, $blank_array,"", 1, "--Select Buyer--", $selected, "",1,"" );
                            ?>
                        </td>
                        <td>
                            <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:90px" placeholder="Double Click" onDblClick="openmypage_job();" />
                            <input type="hidden" id="hidd_job_id" name="hidd_job_id" style="width:90px" />
                        </td>
                        <td>
                            <input type="text" name="txt_po_no" id="txt_po_no" class="text_boxes" style="width:90px" placeholder="Double Click" onDblClick="openmypage_po();" />
                            <input type="hidden" id="hidd_po_id" name="hidd_po_id" style="width:90px" />
                        </td>
                        <td align="center">
                            <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" > To
                            <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px"  placeholder="To Date"  >
                        </td>
                        <td align="center">
                            <input type="button" id="show_button" class="formbutton" style="width:70px" value="Show" onClick="fn_report_generated(0)" />
                            <input type="button" id="show_button" class="formbutton" style="width:80px" value="By Cut-Off" onClick="fn_report_generated_cutoff(0)" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <?php echo load_month_buttons(1); ?>
                        </td>
                    </tr>
                </tbody>
            </table> 
        	</fieldset>
        </div>
        <div id="report_container" align="center"></div>
        <div id="report_container2" align="left"></div>
    </form> 
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
