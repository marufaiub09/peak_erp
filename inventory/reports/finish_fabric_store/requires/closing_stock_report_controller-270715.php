<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_store")
{
	$data=explode('_',$data);
	echo create_drop_down( "cbo_store_name", 120, "select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id  and a.status_active=1 and a.is_deleted=0 and a.company_id=$data[0] and  b.category_type=$data[1] order by a.store_name","id,store_name", 1, "--Select Store--", 1, "",0 );
	exit();
	//select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id and a.status_active=1 and a.is_deleted=0 and a.company_id=$data[0] and b.category_type=$data[1] order by a.store_name
}

if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_item_category_id=str_replace("'","",$data[1]);
	$txt_item_acc=str_replace("'","",$data[2]);
	$txt_product_id_des=str_replace("'","",$data[3]);
	$txt_product_id_no=str_replace("'","",$data[4]);
	//print_r ($data); 
?>	
    <script>
	/* var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_account_id').val( id );
		$('#item_account_val').val( ddd );
	}*/ 
	
	var selected_id = new Array;
		var selected_name = new Array;
		var selected_no = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
			//alert(strCon);
			var splitSTR = strCon.split("_");
			var str = splitSTR[0];
			var selectID = splitSTR[1];
			var selectDESC = splitSTR[2];
			//$('#txt_individual_id' + str).val(splitSTR[1]);
			//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
			
			toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( selectID, selected_id ) == -1 ) {
				selected_id.push( selectID );
				selected_name.push( selectDESC );
				selected_no.push( str );				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == selectID ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
				selected_no.splice( i, 1 ); 
			}
			var id = ''; var name = ''; var job = ''; var num='';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
				num += selected_no[i] + ','; 
			}
			id 		= id.substr( 0, id.length - 1 );
			name 	= name.substr( 0, name.length - 1 ); 
			num 	= num.substr( 0, num.length - 1 );
			//alert(num);
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name ); 
			$('#txt_selected_no').val( num );
		}
		  
	</script>
<!--    <input type="hidden" id="item_account_id" />
    <input type="hidden" id="item_account_val" />
-->   
	<div align="center" style="width:100%;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="600" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="100">Product Id</th>
                    <th width="200">Item Description</th>
                    <th width="100">Gsm</th>
                    <th width="100">Dia</th>
                    <th><input type="reset" id="" value="Reset" style="width:80px;" class="formbutton" /></th>
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td align="center"><input type="text" style="width:70px" class="text_boxes"  name="txt_prod_id" id="txt_prod_id" /></td>
                    <td align="center"><input type="text" style="width:160px" class="text_boxes"  name="txt_item_description" id="txt_item_description" /></td>
                    <td align="center"><input type="text" style="width:70px" class="text_boxes"  name="txt_gsm" id="txt_gsm" /></td>
                    <td align="center"><input type="text" style="width:70px" class="text_boxes"  name="txt_dia" id="txt_dia" /></td> 
                    <td align="center">
                    	<input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view( document.getElementById('txt_prod_id').value+'_'+document.getElementById('txt_item_description').value+'_'+document.getElementById('txt_gsm').value+'_'+document.getElementById('txt_dia').value+'_'+<?php echo $cbo_company_name; ?>+'_'+<?php echo $cbo_item_category_id; ?>+'_'+'<?php echo $txt_item_acc; ?>'+'_'+'<?php echo $txt_product_id_des; ?>'+'_'+'<?php echo $txt_product_id_no; ?>', 'create_item_search_list_view', 'search_div', 'closing_stock_report_controller', '');" style="width:80px;" />				
                    </td>
                </tr>
            </tbody>
        </table>    
    <div align="center" valign="top" style="margin-top:5px" id="search_div"> </div> 
    </form>	

    </div> 
    
     
 <?php
 
}

if ($action=="create_item_search_list_view")
{
	$ex_data=explode("_",$data);
	$txt_prod_id=str_replace("'","",$ex_data[0]);
	$txt_item_description=str_replace("'","",$ex_data[1]);
	$txt_gsm=str_replace("'","",$ex_data[2]);
	$txt_dia=str_replace("'","",$ex_data[3]);
	$cbo_company_name=str_replace("'","",$ex_data[4]);
	$cbo_item_category_id=str_replace("'","",$ex_data[5]);
	$txt_item_acc=str_replace("'","",$ex_data[6]);
	$txt_product_id_des=str_replace("'","",$ex_data[7]);
	$txt_product_id_no=str_replace("'","",$ex_data[8]);
	
	$sql_cond_all="";

	if($txt_prod_id!="") $sql_cond_all=" and id=$txt_prod_id";
	if($txt_item_description!="") $sql_cond_all.=" and item_description like '%$txt_item_description'";
	if($txt_gsm!="") $sql_cond_all.=" and gsm='$txt_gsm'";
	if($txt_dia!="") $sql_cond_all.=" and dia_width='$txt_dia'";
	if($cbo_company_name!=0) $sql_cond_all.=" and company_id=$cbo_company_name";
	if($cbo_item_category_id!=0) $sql_cond_all.=" and item_category_id=$cbo_item_category_id";
	
	$color_arr = return_library_array("select id,color_name from lib_color where status_active=1 and is_deleted=0","id","color_name");
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	
	$sql="SELECT id,item_description,color,gsm,dia_width,supplier_id from  product_details_master where status_active=1 and is_deleted=0 $sql_cond_all";
	//echo $sql;
	$arr=array(1=>$color_arr,4=>$supplierArr);
	echo  create_list_view("list_view", "Item Description,Color,Gsm,Dia,Supplier,Product ID", "150,120,70,70,130","680","300",0, $sql , "js_set_value", "id,item_description", "", 1, "0,color,0,0,supplier_id,0", $arr , "item_description,color,gsm,dia_width,supplier_id,id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
	
	//echo create_list_view("list_view", "Style Ref No,Job No,Year","160,90,100","400","310",0, $sql , "js_set_value", "id,style_ref_no", "", 1, "0", $arr, "style_ref_no,job_no_prefix_num,year", "","setFilterGrid('list_view',-1)","0","",1) ;
	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	echo "<input type='hidden' id='txt_selected_no' />";
	?>
    <script language="javascript" type="text/javascript">
	/*var account_des='<?php// echo $txt_item_acc;?>';
	var account_id='<?php// echo $txt_product_id_des;?>';
	var account_no='<?php// echo $txt_product_id_no;?>';
	alert(account_id);
	if(account_no!="")
	{
		account_no_arr=account_no.split(",");
		account_id_arr=account_id.split(",");
		account_des_arr=account_des.split(",");
		var str_ref="";
		for(var k=0;k<account_no_arr.length; k++)
		{
			str_ref=account_no_arr[k]+'_'+account_id_arr[k]+'_'+account_des_arr[k];
			js_set_value(str_ref);
		}
	}*/
	</script>  
    <?php
	
	exit();
}


if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_store_name=str_replace("'","",$cbo_store_name);
	$cbo_item_category_id=str_replace("'","",$cbo_item_category_id);
	$txt_product_id_des=str_replace("'","",$txt_product_id_des);
	$txt_product_id=str_replace("'","",$txt_product_id);

	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name"); 
	$storeArr = return_library_array("select id,store_name from lib_store_location where status_active=1 and is_deleted=0","id","store_name"); 
	$itemnameArr = return_library_array("select id,item_name from lib_item_creation where status_active=1 and is_deleted=0","id","item_name");
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	$determinaArr = return_library_array("select id,construction from  lib_yarn_count_determina_mst where status_active=1 and is_deleted=0","id","construction");
	$color_arr=return_library_array("select id,color_name from lib_color where status_active=1 and is_deleted=0","id","color_name");
	
	if($db_type==0)
	{
		$select_from_date=change_date_format($from_date,'yyyy-mm-dd');
		$select_from_to=change_date_format($to_date,'yyyy-mm-dd');
	}
	else if($db_type==2) 
	{
		$select_from_date=change_date_format($from_date,'','',1);
		$select_from_to=change_date_format($to_date,'','',1);
	}
	else 
	{
		$select_from_date="";
		$select_from_to="";
	}
	
	
	
	$days_doh=array();
	if($db_type==2)
	{
		$returnRes="select prod_id, min(transaction_date) || ',' || max(transaction_date )  as trans_date from inv_transaction where transaction_type in (1,3) and  item_category in (2,3) and status_active=1 and is_deleted=0 group by prod_id ";
		//$returnRes_result= sql_select($returnRes);
	}
	else
	{
		$returnRes="select prod_id, concat(min(transaction_date),',',max(transaction_date))  as trans_date from inv_transaction where transaction_type in (1,3) and  item_category in (2,3) and status_active=1 and is_deleted=0 group by prod_id ";
		//$returnRes_result= sql_select($returnRes);
		//$returnRes=explode(",",return_field_value("concat(min(transaction_date),',',max(transaction_date))","inv_transaction","prod_id=".$row[csf("prod_id")]));
		//$daysOnHand = datediff("d",$returnRes[1],date("Y-m-d"));
	}
	$returnRes_result= sql_select($returnRes);
	foreach($returnRes_result as $row_d)
	{
		$date_total=explode(",",$row_d[csf('trans_date')]);
		if($db_type==2)
		{
			$today= change_date_format(date("Y-m-d"),'','',1);	
			$daysOnHand = datediff("d",change_date_format($date_total[1],'','',1),$today);
		}
		else
		{
			$today= change_date_format(date("Y-m-d"));	
			$daysOnHand = datediff("d",change_date_format($date_total[1]),$today);
		}
		$days_doh[$row_d[csf('prod_id')]]['daysonhand']=$daysOnHand ;
	} 

	/*$prod_id_arr=array();
	$issue_sql = "select a.prod_id,
                        sum(case when a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as issue_total_opening,
						sum(case when a.transaction_type=2 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as issue,
						sum(case when a.transaction_type=3 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as receive_return,
						sum(case when a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_rate else 0 end) as rate 
						from inv_transaction a, inv_issue_master c
                        where a.mst_id=c.id and a.transaction_type in (2,3) and  a.item_category in (2,3) and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.prod_id";
						//echo $issue_sql;
	$issue_result = sql_select($issue_sql);
	foreach($issue_result as $row_arr)
	{
		$prod_id_arr[$row_arr[csf('prod_id')]]['issue_total_opening']=$row_arr[csf('issue_total_opening')];
		$prod_id_arr[$row_arr[csf('prod_id')]]['issue']=$row_arr[csf('issue')];
		$prod_id_arr[$row_arr[csf('prod_id')]]['receive_return']=$row_arr[csf('receive_return')];
		$prod_id_arr[$row_arr[csf('prod_id')]]['rate']=$row_arr[csf('rate')];
	} //var_dump($prod_id_arr);
	
	$sql="Select a.prod_id,b.id,b.store_id,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description,b.item_size,b.color,b.detarmination_id,b.gsm,b.dia_width,
		sum(case when a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as purchase,
		sum(case when a.transaction_type=3 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as receive_return
		from inv_transaction a, product_details_master b, inv_receive_master c where a.prod_id=b.id and a.mst_id=c.id and a.transaction_type in (1,3) and a.item_category in (2,3) and a.company_id=c.company_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $company_id $item_category_id $group_id $store_id $item_account $select_groupby";
	 	//echo  $sql;//die;
	$result = sql_select($sql);*/
	
	$sql_all_cond="";
	
	if ($cbo_company_name!=0) $sql_all_cond =" and a.company_id='$cbo_company_name'";
	if ($cbo_item_category_id!=0) $sql_all_cond.=" and b.item_category_id='$cbo_item_category_id'";
	if ($txt_product_id_des!="") $sql_all_cond.=" and a.prod_id in ($txt_product_id_des)";
	if ($txt_product_id!="") $sql_all_cond.=" and a.prod_id in ($txt_product_id)";
	if ($cbo_store_name!=0) $sql_all_cond.=" and a.store_id='$cbo_store_name'";

	
	if($db_type==0) $select_groupby=" group by a.prod_id  order by a.prod_id,b.store_id, b.item_category_id, a.item_category ASC";
	if($db_type==2) $select_groupby=" group by a.prod_id,b.id,b.store_id,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description,b.item_size,b.color,b.detarmination_id,b.gsm,b.dia_width order by a.prod_id,b.store_id, b.item_category_id ASC";
	else $select_groupby="";
	$data_trns_array=array();
	$trnasactionData=sql_select("Select b.id,
		sum(case when a.transaction_type=1 and a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type=2 and a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as iss_total_opening,
		sum(case when a.transaction_type=3 and a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as rcv_return_opening,
		sum(case when a.transaction_type=4 and a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as iss_return_opening,
		sum(case when a.transaction_type=1 and a.transaction_date between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as receive,
		sum(case when a.transaction_type=2 and a.transaction_date between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as issue,
		sum(case when a.transaction_type=3 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as rec_return,
		sum(case when a.transaction_type=4 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as issue_return,
		sum(case when a.transaction_date between '".$select_from_date."' and '".$select_from_to."' then a.cons_rate else 0 end) as rate
		from inv_transaction a, product_details_master b
		where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0  and a.item_category in (1,2,3,4) $sql_all_cond  group by b.id order by b.id ASC");
		
		
			
		/*
		
		echo "Select b.id,
		sum(case when a.transaction_type in(1) and a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type in(2) and a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as iss_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as receive,
		sum(case when a.transaction_type=2 and a.transaction_date between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as issue,
		sum(case when a.transaction_type=3 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as rec_return,
		sum(case when a.transaction_type=4 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as issue_return,
		sum(case when a.transaction_date between '".$select_from_date."' and '".$select_from_to."' then a.cons_rate else 0 end) as rate
		
		from inv_transaction a, product_details_master b
		where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.order_id=0 $company_id and a.item_category in (2,3) $store_id $item_account group by b.id order by b.id ASC";*/
		
	foreach($trnasactionData as $row)
	{
		$data_trns_array[$row[csf("id")]]['rcv_total_opening']=$row[csf("rcv_total_opening")];
		$data_trns_array[$row[csf("id")]]['iss_total_opening']=$row[csf("iss_total_opening")];
		$data_trns_array[$row[csf("id")]]['rcv_return_opening']=$row[csf("rcv_return_opening")];
		$data_trns_array[$row[csf("id")]]['iss_return_opening']=$row[csf("iss_return_opening")];
		$data_trns_array[$row[csf("id")]]['receive']=$row[csf("receive")];
		$data_trns_array[$row[csf("id")]]['issue_return']=$row[csf("issue_return")];
		$data_trns_array[$row[csf("id")]]['issue']=$row[csf("issue")];
		$data_trns_array[$row[csf("id")]]['rec_return']=$row[csf("rec_return")];
		$data_trns_array[$row[csf("id")]]['avg_rate']=$row[csf("rate")];
	}
	
	$i=1;
	ob_start();	
	?>
    <div> 
        <table style="width:1500px" border="1" cellpadding="2" cellspacing="0" class="" id="table_header_1" > 
            <thead>
                <tr class="form_caption" style="border:none;">
                    <td colspan="16" align="center" style="border:none;font-size:16px; font-weight:bold" >Finish Fabric <?php echo $report_title; ?></td> 
                </tr>
                <tr style="border:none;">
                    <td colspan="16" align="center" style="border:none; font-size:14px;">
                       <b>Company Name : <?php echo $companyArr[$cbo_company_name]; ?></b>                               
                    </td>
                </tr>
                <tr style="border:none;">
                    <td colspan="16" align="center" style="border:none;font-size:12px; font-weight:bold">
                        <?php if($from_date!="" || $to_date!="")echo "From : ".change_date_format($from_date,'dd-mm-yyyy')." To : ".change_date_format($to_date,'dd-mm-yyyy')."" ;?>
                    </td>
                </tr>
            </thead>
        </table>
        <table style="width:1500px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="" > 
            <thead>
                <tr>
                    <th rowspan="2" width="40">SL</th>
                    <th colspan="6">Description</th>
                    <th rowspan="2" width="110">Opening Stock</th>
                    <th colspan="3">Receive</th>
                    <th colspan="3">Issue</th>
                    <th rowspan="2" width="100">Closing Stock</th>
                    <!--<th rowspan="2" width="100" style="display:none">Avg. Rate</th>
                    <th rowspan="2" width="100" style="display:none">Stock Value</th>-->
                    <th rowspan="2">DOH</th>
                </tr> 
                <tr> 
                	<th width="60">Prod. ID</th>                    
                    <th width="120">Construction</th>
                    <th width="180">Composition</th>
                    <th width="70">GSM</th>
                    <th width="80">Dia/ Width</th>
                    <th width="100">Color</th>
                    <th width="80">Purchase</th>
                    <th width="80">Issue Return</th> 
                    <th width="100">Total Received</th>
                    <th width="80">Issue</th>
                    <th width="80">Receive Return</th>
                    <th width="100">Total Issue</th> 
                </tr> 
            </thead>
        </table>
        <div style="width:1518px; max-height:280px; overflow-y:scroll" id="scroll_body" > 
        <table style="width:1500px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body">
        <?php
			$composition_arr=array();
			$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
			$data_array=sql_select($sql_deter);
			if(count($data_array)>0)
			{
				foreach( $data_array as $row )
				{
					if(array_key_exists($row[csf('id')],$composition_arr))
					{
						$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
					}
					else
					{
						$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
					}
				}
			}
			
			
			
			$sql_all_cond="";
			if ($cbo_company_name!=0) $sql_all_cond =" and company_id='$cbo_company_name'";
			if ($cbo_item_category_id!=0) $sql_all_cond.=" and item_category_id='$cbo_item_category_id'";
			if ($txt_product_id_des!="") $sql_all_cond.=" and id in ($txt_product_id_des)";
			if ($txt_product_id!="") $sql_all_cond.=" and id in ($txt_product_id)";
			if ($cbo_store_name!=0) $sql_all_cond.=" and store_id='$cbo_store_name'";
			
			
				
			if ($cbo_item_category_id==0) $category_id_cond="and item_category_id in (2,3)"; else $category_id_cond="";
           // $trans = return_library_array("select  prod_id, sum(quantity) as qnty from order_wise_pro_details where entry_form in(2,22) and trans_id!=0 and trans_type=1 and status_active=1 and is_deleted=0 group by prod_id order by prod_id","prod_id","qnty");
            $sql="select id, detarmination_id, gsm, dia_width, color, current_stock from product_details_master  where status_active=1 and is_deleted=0 $category_id_cond  $sql_all_cond order by id";
			//echo $sql."<br>";	
			 $result = sql_select($sql);
			$tot_opening_bal=0;$tot_purchuse=0;$tot_issue_return=0;$tot_receive=0; $tot_issue=0;$tot_receive_return=0;$tot_total_issue=0;$total_closing_stock=0;$tot_closing_stock=0;
            foreach($result as $row)
            {
                if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
					
 				$openingBalance =($data_trns_array[$row[csf("id")]]['rcv_total_opening']+$data_trns_array[$row[csf("id")]]['iss_return_opening'])-($data_trns_array[$row[csf("id")]]['iss_total_opening']+$data_trns_array[$row[csf("id")]]['rcv_return_opening']);
				
				$purchase = $data_trns_array[$row[csf("id")]]['receive'];
				$issue_return=$data_trns_array[$row[csf("id")]]['issue_return'];
				$totalReceive=$purchase+$issue_return;//
				$issue=$data_trns_array[$row[csf("id")]]['issue'];
				$rec_return=$data_trns_array[$row[csf("id")]]['rec_return'];
				$totalIssue=$issue+$rec_return;//
				
				$closingStock=$openingBalance+$totalReceive-$totalIssue;
				//$avgRate=$data_trns_array[$row[csf("id")]]['avg_rate'];
				//$stockValue=$closingStock*$avgRate;
				 
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
						<td width="40"><?php echo $i; ?></td>
                        <td width="60"><?php echo $row[csf("id")]; ?></td>								
						<td width="120"><?php echo $determinaArr[$row[csf("detarmination_id")]]; ?></td>                                 
						<td width="180"><p><?php echo $composition_arr[$row[csf('detarmination_id')]]; ?></p></td>
						<td width="70"><p><?php echo $row[csf("gsm")]; ?></p></td> 
						<td width="80"><p><?php echo $row[csf("dia_width")]; ?></p></td> 
						<td width="100"><p><?php echo $color_arr[$row[csf("color")]]; ?></p></td> 
                        
						<td width="110" align="right"><p><?php echo number_format($openingBalance,2);$tot_opening_bal+=$openingBalance; ?></p></td>
						
						<td width="80" align="right"><p><?php echo number_format($purchase,2); $tot_purchuse+=$purchase; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($issue_return,2); $tot_issue_return+=$issue_return; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalReceive,2); $tot_receive+=$totalReceive; ?></p></td>
                        
						<td width="80" align="right"><p><?php echo number_format($issue,2); $tot_issue+=$issue; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($rec_return,2); $tot_receive_return+=$rec_return; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalIssue,2); $tot_total_issue+=$totalIssue; ?></p></td>
                        
                        <td width="100" align="right"><p><?php echo number_format($closingStock,2); $total_closing_stock+=$closingStock; ?></p></td>
                        	
					
						<td align="right"><?php echo $days_doh[$row[csf('id')]]['daysonhand']; //$daysOnHand;  ?>&nbsp;<!--<td width="100" align="right" style="display:none"><p><?php //echo number_format($avgRate,2); ?></p></td>
                        <td width="100" align="right" style="display:none"><p><?php //echo number_format($stockValue,2); $tot_closing_stock+=$stockValue; ?></p></td>--></td>
					</tr>
                <?php 												
                $i++; 				
			}
		?>
        </table>
       </div>
        <table style="width:1500px" border="1" cellpadding="2" cellspacing="0" class="tbl_bottom" rules="all" id="">
            <tr>
            	<td align="right" width="40">&nbsp;</td>
                <td align="right" width="60">&nbsp;</td>
                <td align="right" width="120">&nbsp;</td>
                <td align="right" width="180">&nbsp;</td>
                <td align="right" width="70">&nbsp;</td>
                <td align="right" width="80">&nbsp;</td>
                <td align="right" width="100">Total</td>
                <td align="right" width="110" id="tot_opening_bal" ><?php echo number_format($tot_opening_bal,2); ?></td>
                <td align="right" width="80" id="tot_purchuse" ><?php echo number_format($tot_purchuse,2); ?></td>
                <td align="right" width="80" id="tot_issue_return" ><?php echo number_format($tot_issue_return,2); ?></td>
                <td align="right" width="100" id="tot_receive" ><?php echo number_format($tot_receive,2); ?></td>
                <td align="right" width="80" id="tot_issue" ><?php echo number_format($tot_issue,2); ?></td>
                <td align="right" width="80" id="tot_receive_return" ><?php echo number_format($tot_receive_return,2); ?></td>
                <td align="right" width="100" id="tot_total_issue" ><?php echo number_format($tot_total_issue,2); ?></td>
                <td align="right" width="100" id="total_closing_stock" ><?php echo number_format($total_closing_stock,2); ?></td>
<!--                <td align="right" width="100" style="display:none">&nbsp;</td>
                <td align="right" width="100" id="tot_closing_stock" style="display:none"><?php //echo number_format($tot_closing_stock,2); ?></td>
-->                <td align="right">&nbsp;</td>  
            </tr>
        </table>
    </div>
    <?php
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}
?>