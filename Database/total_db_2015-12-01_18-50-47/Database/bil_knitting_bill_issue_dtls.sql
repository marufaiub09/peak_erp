-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bil_knitting_bill_issue_dtls`
--

DROP TABLE IF EXISTS `bil_knitting_bill_issue_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bil_knitting_bill_issue_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) NOT NULL DEFAULT '0',
  `delivery_date` date DEFAULT NULL,
  `challan_no` varchar(100) DEFAULT NULL,
  `order_no` varchar(100) DEFAULT NULL,
  `order_no_type` tinyint(1) NOT NULL DEFAULT '0',
  `sample_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No;1=Yes',
  `order_style` varchar(100) DEFAULT NULL,
  `buyer_knitting` varchar(100) DEFAULT NULL,
  `yarn_count` varchar(100) DEFAULT NULL,
  `roll` int(11) NOT NULL DEFAULT '0',
  `fabric_type` varchar(100) DEFAULT NULL,
  `fabric_qty` double NOT NULL DEFAULT '0',
  `qty_type` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `remarks` varchar(100) DEFAULT NULL,
  `inserted_by` int(4) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(4) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `master_id` (`master_id`),
  KEY `delivery_date` (`delivery_date`),
  KEY `challan_no` (`challan_no`),
  KEY `order_no` (`order_no`),
  KEY `order_no_type` (`order_no_type`),
  KEY `fabric_type` (`fabric_type`),
  KEY `roll` (`roll`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bil_knitting_bill_issue_dtls`
--

LOCK TABLES `bil_knitting_bill_issue_dtls` WRITE;
/*!40000 ALTER TABLE `bil_knitting_bill_issue_dtls` DISABLE KEYS */;
/*!40000 ALTER TABLE `bil_knitting_bill_issue_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:39
