-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppl_gsd_entry_dtls`
--

DROP TABLE IF EXISTS `ppl_gsd_entry_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppl_gsd_entry_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL,
  `row_sequence_no` int(11) NOT NULL,
  `body_part_id` int(11) NOT NULL,
  `lib_sewing_id` int(11) NOT NULL,
  `resource_gsd` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `oparetion_type_id` int(11) NOT NULL,
  `total_smv` float NOT NULL DEFAULT '0',
  `no_of_worker_calculative` float(8,2) NOT NULL DEFAULT '0.00',
  `no_of_worker_rounding` int(5) NOT NULL DEFAULT '0',
  `target_per_hour_operation` int(11) NOT NULL DEFAULT '0',
  `target_per_day_operation` int(11) NOT NULL DEFAULT '0',
  `operation_id` int(11) NOT NULL DEFAULT '0',
  `operator_smv` float(8,2) NOT NULL DEFAULT '0.00',
  `helper_smv` float(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppl_gsd_entry_dtls`
--

LOCK TABLES `ppl_gsd_entry_dtls` WRITE;
/*!40000 ALTER TABLE `ppl_gsd_entry_dtls` DISABLE KEYS */;
/*!40000 ALTER TABLE `ppl_gsd_entry_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
