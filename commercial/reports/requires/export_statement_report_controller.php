<?php

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id=$_SESSION['logic_erp']['user_id'];


$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
$company_library=return_library_array( "select id,company_name from lib_company where status_active=1 and is_deleted=0", "id", "company_name"  );


if ($action=="load_drop_down_buyer")
{
	//echo "yes";
	echo create_drop_down( "cbo_buyer_name", 155, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	$cbo_lien_bank=str_replace("'","",$cbo_lien_bank);
	$cbo_search_by=str_replace("'","",$cbo_search_by);
	$txt_exchange_rate=str_replace("'","",$txt_exchange_rate);
	
	//echo $cbo_company_name."___".$cbo_buyer_name."___".$cbo_lien_bank."___".$cbo_search_by."___".$txt_exchange_rate;die;
	
    if($cbo_company_name ==0) $cbo_company_name =""; else $cbo_company="and b.beneficiary_name='$cbo_company_name'";
	if($cbo_buyer_name == 0) $cbo_buyer_name=""; else $cbo_buyer_name = "and b.buyer_name='$cbo_buyer_name'";
	if($cbo_lien_bank == 0) $cbo_lien_bank=""; else $cbo_lien_bank = "and b.lien_bank='$cbo_lien_bank'";
	
	

	$sc_id_group=0;$inv_id=0;$invoice_id=0;$sub_id=0;$lc_id_group=0;$total_inv_value=array();$total_realize_value=array(); $buyer_result=array();$unsubmit_inv=array();$no_inv_lc=array();
	


	/*echo "SELECT b.id as lc_id,  b.export_lc_no as export_lc_no, b.lc_value as lc_value,b.buyer_name, b.replacement_lc, null as convertible_to_lc, 1 as type
FROM  com_export_lc b
WHERE b.status_active=1 and b.is_deleted=0 $cbo_company $cbo_buyer_name $cbo_lien_bank
union all
SELECT b.id as lc_id,  b.contract_no  as export_lc_no, b.contract_value  as lc_value,b.buyer_name, null as replacement_lc , b.convertible_to_lc, 2 as type
FROM com_sales_contract b 
WHERE  b.status_active=1 and b.is_deleted=0 $cbo_company $cbo_buyer_name $cbo_lien_bank order by buyer_name asc";die;*/

$sql=sql_select("SELECT b.id as lc_id,  b.export_lc_no as export_lc_no, b.lc_value as lc_value,b.buyer_name, b.replacement_lc, 1 as type
FROM  com_export_lc b
WHERE b.status_active=1 and b.is_deleted=0 $cbo_company $cbo_buyer_name $cbo_lien_bank
union all
SELECT b.id as lc_id,  b.contract_no  as export_lc_no, b.contract_value  as lc_value,b.buyer_name, null as replacement_lc, 2 as type
FROM com_sales_contract b 
WHERE  b.status_active=1 and b.is_deleted=0 $cbo_company $cbo_buyer_name $cbo_lien_bank order by buyer_name asc");

	foreach($sql as $row)
	{
		
		if($row[csf("type")]==1){ if($lc_id_group==0) $lc_id_group=$row[csf("lc_id")]; else $lc_id_group=$lc_id_group.",".$row[csf("lc_id")]; } 
		if($row[csf("type")]==2){ if($sc_id_group==0) $sc_id_group=$row[csf("lc_id")]; else $sc_id_group=$sc_id_group.",".$row[csf("lc_id")]; } 
		$total_inv_value[$row[csf("lc_id")]]['lc_value']+=$row[csf("lc_value")];
		if($row[csf('type')] == 2 && $row_result[csf('convertible_to_lc')] ==1 || $row_result[csf('convertible_to_lc')] ==3 ) $sc_value_1_3 += $row_result[csf('lc_sc_value')];
		
		if($row[csf("type")]==1)
		{
			$lc_result[$row[csf("lc_id")]]["type"]=$row[csf("type")];
			$lc_result[$row[csf("lc_id")]]["lc_no"]=$row[csf("export_lc_no")];
			$lc_result[$row[csf("lc_id")]]["lc_val"]+=$row[csf("lc_value")];
			if($row[csf('replacement_lc')] == 2) 
			{
				$buyer_lc_val[$row[csf("buyer_name")]] +=$row[csf("lc_value")];
			}
			
		}
		if($row[csf("type")]==2)
		{
			$sc_result[$row[csf("lc_id")]]["type"]=$row[csf("type")];
			$sc_result[$row[csf("lc_id")]]["lc_no"]=$row[csf("export_lc_no")];
			$sc_result[$row[csf("lc_id")]]["lc_val"]+=$row[csf("lc_value")];
			$buyer_lc_val[$row[csf("buyer_name")]] +=$row[csf("lc_value")];
		}
 		
	}
	//var_dump($buyer_lc_val);die;
	
	
	
	
	if($db_type==0)
	{
		$sql_sub=sql_select("SELECT
		a.lc_sc_id as lc_id, group_concat(d.id) as invoice_id,b.id as sub_id,b.bank_ref_no, group_concat(d.invoice_no) as invoice_no, sum(d.invoice_quantity) as invoice_quantity, sum(d.net_invo_value) as invoice_value, b.submit_type, sum(d.freight_amnt_by_supllier) as freight_amnt_by_supllier, sum(d.paid_amount) as paid_amount,  b.buyer_id, sum(case when b.submit_type=1 then a.net_invo_value end) as sub_collection,sum(case when b.submit_type=2 then a.net_invo_value end) as sub_purchase, sum(d.advice_amount) as advice_amount,a.is_lc, 1 as type
		
		from 
		com_export_doc_submission_invo a, com_export_doc_submission_mst b, com_export_invoice_ship_mst d
		where 
		a.doc_submission_mst_id=b.id  and a.invoice_id=d.id and a.is_lc=1 and a.is_converted=0 and d.status_active=1 and d.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.lc_sc_id in($lc_id_group) group by b.id
		
		union all
		
		SELECT
		a.lc_sc_id as lc_id, group_concat(d.id) as invoice_id,b.id as sub_id,b.bank_ref_no, group_concat(d.invoice_no) as invoice_no, sum(d.invoice_quantity) as invoice_quantity, sum(d.net_invo_value) as invoice_value,  b.submit_type, sum(d.freight_amnt_by_supllier) as freight_amnt_by_supllier, sum(d.paid_amount) as paid_amount,  b.buyer_id, sum(case when b.submit_type=1 then a.net_invo_value end) as sub_collection,sum(case when b.submit_type=2 then a.net_invo_value end) as sub_purchase, sum(d.advice_amount) as advice_amount,a.is_lc, 2 as type
		
		from 
		com_export_doc_submission_invo a, com_export_doc_submission_mst b, com_export_invoice_ship_mst d
		where 
		a.doc_submission_mst_id=b.id  and a.invoice_id=d.id and a.is_lc=2 and a.is_converted=0 and d.status_active=1 and d.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.lc_sc_id in($sc_id_group)  group by b.id");
	}
	else if($db_type==2)
	{
		$sql_sub=sql_select("SELECT
		LISTAGG(CAST( a.lc_sc_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.lc_sc_id) as lc_id, LISTAGG(CAST( d.id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY d.id)  as invoice_id, b.id as sub_id,b.bank_ref_no, LISTAGG(CAST(  d.invoice_no  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY  d.invoice_no)  as invoice_no, sum(d.invoice_quantity) as invoice_quantity, sum(d.net_invo_value) as invoice_value,  b.submit_type, sum(d.freight_amnt_by_supllier) as freight_amnt_by_supllier, sum(d.paid_amount) as paid_amount,  b.buyer_id, sum(case when b.submit_type=1 then a.net_invo_value end) as sub_collection,sum(case when b.submit_type=2 then a.net_invo_value end) as sub_purchase, sum(d.advice_amount) as advice_amount,a.is_lc, 1 as type
		
		from 
		com_export_doc_submission_invo a, com_export_doc_submission_mst b, com_export_invoice_ship_mst d
		where 
		a.doc_submission_mst_id=b.id  and a.invoice_id=d.id and a.is_lc=1 and a.is_converted=0 and d.status_active=1 and d.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.lc_sc_id in( $lc_id_group ) 
		group by 
		b.id,b.bank_ref_no,b.buyer_id,b.submit_type,a.is_lc
		
		union all
		
		SELECT
		LISTAGG(CAST( a.lc_sc_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.lc_sc_id) as lc_id, LISTAGG(CAST( d.id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY d.id) as invoice_id,b.id as sub_id,b.bank_ref_no,  LISTAGG(CAST( d.invoice_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY  d.invoice_no)  as invoice_no, sum(d.invoice_quantity) as invoice_quantity, sum(d.net_invo_value) as invoice_value, b.submit_type, sum(d.freight_amnt_by_supllier) as freight_amnt_by_supllier, sum(d.paid_amount) as paid_amount,  b.buyer_id, sum(case when b.submit_type=1 then a.net_invo_value end) as sub_collection,sum(case when b.submit_type=2 then a.net_invo_value end) as sub_purchase, sum(d.advice_amount) as advice_amount,a.is_lc, 2 as type
		
		from 
		com_export_doc_submission_invo a, com_export_doc_submission_mst b, com_export_invoice_ship_mst d
		where 
		a.doc_submission_mst_id=b.id  and a.invoice_id=d.id and a.is_lc=2 and a.is_converted=0 and d.status_active=1 and d.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.lc_sc_id in($sc_id_group) 
		group by 
		b.id,b.bank_ref_no,b.buyer_id,b.submit_type,a.is_lc");//$lc_id_group
	}
	 $invoice_id_lc=0;$invoice_id_sc=0;
	foreach($sql_sub as $row_sub)
	{
		if($sub_id==0) $sub_id=$row_sub[csf("sub_id")]; else $sub_id=$sub_id.",".$row_sub[csf("sub_id")];
		
		if($row_sub[csf('type')]==1)
		{
			if($invoice_id_lc==0) $invoice_id_lc=$row_sub[csf("invoice_id")]; else $invoice_id_lc=$invoice_id_lc.",".$row_sub[csf("invoice_id")];
		}
		else if(($row_sub[csf('type')]==2))
		{
			if($invoice_id_sc==0) $invoice_id_sc=$row_sub[csf("invoice_id")]; else $invoice_id_sc=$invoice_id_sc.",".$row_sub[csf("invoice_id")];
		}
		
	//	if($row_sub[csf("buyer_id")]==1 && $row_sub[csf('type')]==2) { if( !in_array($row_sub[csf("invoice_id")],$buyid)) $buyid[]=$row_sub[csf("invoice_id")]; }
	
		//if($invoice_id==0) $invoice_id=$row_sub[csf("invoice_id")]; else $invoice_id=$invoice_id.",".$row_sub[csf("invoice_id")];
		//$invoice_id_test[$row_sub[csf("buyer_id")]].=$row_sub[csf("invoice_id")].",";

		if($row_sub[csf("is_lc")]==1) $lc_contac_id[]=$row_sub[csf("lc_id")]; else $sales_con_id[]=$row_sub[csf("lc_id")];
		
		$submission_result[$row_sub[csf("sub_id")]]["is_lc"]=$row_sub[csf("is_lc")];
		$submission_result[$row_sub[csf("sub_id")]]["lc_id"]=implode(",",array_unique(explode(",",$row_sub[csf("lc_id")])));			
		$submission_result[$row_sub[csf("sub_id")]]["sub_id"]=$row_sub[csf("sub_id")];
		$submission_result[$row_sub[csf("sub_id")]]["buyer_id"]=$row_sub[csf("buyer_id")];
		$submission_result[$row_sub[csf("sub_id")]]["bank_ref_no"]=$row_sub[csf("bank_ref_no")];
		$submission_result[$row_sub[csf("sub_id")]]["invoice_no"]=$row_sub[csf("invoice_no")];
		$submission_result[$row_sub[csf("sub_id")]]["invoice_quantity"]+=$row_sub[csf("invoice_quantity")];
		$submission_result[$row_sub[csf("sub_id")]]["invoice_value"]+=$row_sub[csf("invoice_value")];
		$submission_result[$row_sub[csf("sub_id")]]["advice_amount"]+=$row_sub[csf("advice_amount")];
		$submission_result[$row_sub[csf("sub_id")]]["freight_amnt_by_supllier"]+=$row_sub[csf("freight_amnt_by_supllier")];
		$submission_result[$row_sub[csf("sub_id")]]["paid_amount"]+=$row_sub[csf("paid_amount")];
		$submission_result[$row_sub[csf("sub_id")]]["sub_collection"]+=$row_sub[csf("sub_collection")];
		$submission_result[$row_sub[csf("sub_id")]]["sub_purchase"]+=$row_sub[csf("sub_purchase")];
		$submission_result[$row_sub[csf("sub_id")]]["submit_type"]+=$row_sub[csf("submit_type")];
		
		
	}
	
	//var_dump($submission_result);die;
	
	//realization data start
	
		$sql_relz=sql_select("SELECT a.invoice_bill_id, sum(case when b.type=0 then b.document_currency else 0 end) as deducation, sum(case when b.type=1 then b.document_currency else 0 end) as distribution, sum(case when b.type=1 and b.account_head=1 then b.document_currency else 0 end) as nagotiate_distribution
		 from  com_export_proceed_realization a, com_export_proceed_rlzn_dtls b,  com_export_doc_submission_mst c
	where a.id=b.mst_id and a.invoice_bill_id=c.id and a.is_invoice_bill=1 and a.invoice_bill_id in ( $sub_id ) group by a.invoice_bill_id");
	$rlz_sub_id=array();
	foreach($sql_relz as $row_relz)
	{
		$rlz_sub_id[]=+$row_relz[csf("invoice_bill_id")];
		$total_realize_value[$row_relz[csf('invoice_bill_id')]]['rlz_val']+=$row_relz[csf("deducation")]+$row_relz[csf("distribution")];
		$total_realize_value[$row_relz[csf('invoice_bill_id')]]['deducation']+=$row_relz[csf("deducation")];
		$total_realize_value[$row_relz[csf('invoice_bill_id')]]['distribution']+=$row_relz[csf("distribution")];
		$total_realize_value[$row_relz[csf('invoice_bill_id')]]['nagotiate_distribution']+=$row_relz[csf("nagotiate_distribution")];
	}
	
		$sql_relz_sc=sql_select("SELECT a.invoice_bill_id, sum(case when b.type=0 then b.document_currency else 0 end) as deducation, sum(case when b.type=1 then b.document_currency else 0 end) as distribution, sum(case when b.type=1 and b.account_head=1 then b.document_currency else 0 end) as nagotiate_distribution
	 from  com_export_proceed_realization a, com_export_proceed_rlzn_dtls b,  com_export_doc_submission_mst c
where a.id=b.mst_id and a.invoice_bill_id=c.id and a.is_invoice_bill=2 and a.invoice_bill_id in ( $sub_id ) group by a.invoice_bill_id");
	
	
	foreach($sql_relz_sc as $row_relz)
	{
		$rlz_sub_id[]=+$row_relz[csf("invoice_bill_id")];
		$total_realize_value[$row_relz[csf("invoice_bill_id")]]['rlz_val']+=$row_relz[csf("deducation")]+$row_relz[csf("distribution")];
		$total_realize_value[$row_relz[csf("invoice_bill_id")]]['deducation']+=$row_relz[csf("deducation")];
		$total_realize_value[$row_relz[csf("invoice_bill_id")]]['distribution']+=$row_relz[csf("distribution")];
		$total_realize_value[$row_relz[csf('invoice_bill_id')]]['nagotiate_distribution']+=$row_relz[csf("nagotiate_distribution")];
	}
	
	//realization data end
	
	
	$invoice_id_lc_arr=array_chunk(array_unique(explode(",",$invoice_id_lc)),999);
	$invoice_id_sc_arr=array_chunk(array_unique(explode(",",$invoice_id_sc)),999);
	
	
	
	
	$sql_unsubmit_lc= "SELECT b.id as lc_sc_id, a.id, a.buyer_id, a.invoice_no, a.invoice_quantity, a.net_invo_value as invoice_value, b.export_lc_no as export_lc_no, b.lc_value as lc_sc_val, a.remarks, 1 as type from com_export_invoice_ship_mst a, com_export_lc b where a.lc_sc_id=b.id and a.is_lc=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0  and a.lc_sc_id in($lc_id_group)";
	$p=1;
	foreach($invoice_id_lc_arr as $invoice_id_lc)
	{
		if($p==1) $sql_unsubmit_lc .="and (a.id not in(".implode(',',$invoice_id_lc).")"; else  $sql_unsubmit_lc .=" and a.id not in(".implode(',',$invoice_id_lc).")";
		
		$p++;
	}
	$sql_unsubmit_lc .=")";
	
	//echo $sql_unsubmit_lc;die;
	
	$sql_unsubmit_result_lc=sql_select($sql_unsubmit_lc);
	foreach($sql_unsubmit_result_lc as $row)
	{
		$unsubmit_inv[$row[csf('id')]]['id']=$row[csf('id')];
		$unsubmit_inv[$row[csf('id')]]['type']=$row[csf('type')];
		$unsubmit_inv[$row[csf('id')]]['buyer_id']=$row[csf('buyer_id')];
		$unsubmit_inv[$row[csf('id')]]['invoice_no']=$row[csf('invoice_no')];
		$unsubmit_inv[$row[csf('id')]]['invoice_quantity']=$row[csf('invoice_quantity')];
		$unsubmit_inv[$row[csf('id')]]['invoice_value']=$row[csf('invoice_value')];
		$unsubmit_inv[$row[csf('id')]]['export_lc_no']=$row[csf('export_lc_no')];
		$unsubmit_inv[$row[csf('id')]]['lc_sc_val']=$row[csf('lc_sc_val')];
		$unsubmit_inv[$row[csf('id')]]['lc_sc_id']=$row[csf('lc_sc_id')];
		$unsubmit_inv[$row[csf('id')]]['remarks']=$row[csf('remarks')];
	
		$buyer_inhand_total[$row[csf('buyer_id')]]+=$row[csf('invoice_value')];
		
		$lc_contac_id[]=$row[csf("lc_sc_id")];
	}
	
	$sql_unsubmit_sc =" SELECT b.id as lc_sc_id, a.id, a.buyer_id, a.invoice_no, a.invoice_quantity, a.net_invo_value as invoice_value, b.contract_no as export_lc_no, b.contract_value as lc_sc_val, a.remarks, 2 as type from com_export_invoice_ship_mst a,  com_sales_contract b where a.lc_sc_id=b.id and a.is_lc=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.lc_sc_id in( $sc_id_group )  ";
	$q=1;
	
	//Buyer Cout
	foreach($invoice_id_sc_arr as $invoice_id_sc)
	{
		//print_r($invoice_id_sc); die;
		if($q==1) $sql_unsubmit_sc .="and ( a.id not in(".implode(',',$invoice_id_sc).")"; else $sql_unsubmit_sc .=" and a.id not in(".implode(',',$invoice_id_sc).")";
		$q++;
	}
	$sql_unsubmit_sc .=" )";
	$sql_unsubmit_result_sc=sql_select($sql_unsubmit_sc);
	foreach($sql_unsubmit_result_sc as $row)
	{
		$unsubmit_inv[$row[csf('id')]]['id']=$row[csf('id')];
		$unsubmit_inv[$row[csf('id')]]['type']=$row[csf('type')];
		$unsubmit_inv[$row[csf('id')]]['buyer_id']=$row[csf('buyer_id')];
		$unsubmit_inv[$row[csf('id')]]['invoice_no']=$row[csf('invoice_no')];
		$unsubmit_inv[$row[csf('id')]]['invoice_quantity']=$row[csf('invoice_quantity')];
		$unsubmit_inv[$row[csf('id')]]['invoice_value']=$row[csf('invoice_value')];
		$unsubmit_inv[$row[csf('id')]]['export_lc_no']=$row[csf('export_lc_no')];
		$unsubmit_inv[$row[csf('id')]]['lc_sc_val']=$row[csf('lc_sc_val')];
		$unsubmit_inv[$row[csf('id')]]['lc_sc_id']=$row[csf('lc_sc_id')];
		$unsubmit_inv[$row[csf('id')]]['remarks']=$row[csf('remarks')];
	
		$buyer_inhand_total[$row[csf('buyer_id')]]+=$row[csf('invoice_value')];
		$sales_con_id[]=$row[csf("lc_sc_id")];
	}
	
	
	$lc_id_arr_unique=array_unique($lc_contac_id);
	$sc_id_arr_unique=array_unique($sales_con_id);
	if(empty($lc_id_arr_unique)) $lc_id_arr_unique[]=0;
	if(empty($sc_id_arr_unique)) $sc_id_arr_unique[]=0;
		
	
	$sql_no_inv_lc=sql_select("select b.id as lc_sc_id, b.buyer_name as buyer_id, b.export_lc_no as lc_sc_no, b.lc_value as lc_sc_val, 1 as type from  com_export_lc b where  b.status_active=1 and b.is_deleted=0 and b.id not in(".implode(",",$lc_id_arr_unique).") $cbo_company $cbo_buyer_name $cbo_lien_bank
	union all
	select b.id as lc_sc_id, b.buyer_name as buyer_id, b.contract_no as lc_sc_no, b.contract_value as lc_sc_val, 2 as type  from  com_sales_contract b where  b.status_active=1 and b.is_deleted=0 and  b.id not in(".implode(",",$sc_id_arr_unique).") $cbo_company $cbo_buyer_name $cbo_lien_bank
	");
	
	foreach($sql_no_inv_lc as $row)
	{
		$no_inv_lc[$row[csf('lc_sc_id')]]['id'] = $row[csf('lc_sc_id')];
		$no_inv_lc[$row[csf('lc_sc_id')]]['type']=$row[csf('type')];
		$no_inv_lc[$row[csf('lc_sc_id')]]['buyer_id']=$row[csf('buyer_id')];
		$no_inv_lc[$row[csf('lc_sc_id')]]['lc_sc_no']=$row[csf('lc_sc_no')];
		$no_inv_lc[$row[csf('lc_sc_id')]]['lc_sc_val']=$row[csf('lc_sc_val')];
	}
	//print_r($no_inv_lc);
	
	
	//var_dump($total_realize_value);die;
	//$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$purchase_val_arr=return_library_array("select doc_submission_mst_id, sum(lc_sc_curr) as dom_curr from com_export_doc_sub_trans where status_active =1 and	is_deleted=0 group by doc_submission_mst_id","doc_submission_mst_id","dom_curr");
	//var_dump($purchase_val_arr);die;
	$lc_gross_val_arr=return_library_array("select lc_sc_id, sum(invoice_value) as invoice_value from com_export_invoice_ship_mst where is_lc=1 and status_active =1 and	is_deleted=0 group by lc_sc_id","lc_sc_id","invoice_value");
	$sc_gross_val_arr=return_library_array("select lc_sc_id, sum(invoice_value) as invoice_value from com_export_invoice_ship_mst where is_lc=2 and status_active =1 and	is_deleted=0 group by lc_sc_id","lc_sc_id","invoice_value");
	

		//lc wise table create here
		//exclude full realize buyer/only full realize wise data create here 
			
		$k=1;$i=1;$purchase=0;$gt_total_val=0;$total_inv_qty=0;$total_invoice_val=0;$total_sub_collectin=0;$total_sub_pur=0;$all_purchase=0;$total_in_hand=0;$total_freight=0;$total_paid=0;$total_panding=0;$total_parcentage=0;$html_data="";$total_advice_amt=0;$lc_id_arr=array();$tr_color=array();
		
		foreach($submission_result as $lc_key=>$row_result)
		{
			if ($k%2==0)
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			if($cbo_search_by==1)
			{
				$purchase=$purchase_val_arr[$row_result['sub_id']];
				$sub_extra=$row_result['sub_purchase']- $purchase;
					if($row_result['submit_type']==1)
					{
						$sub_collection= $row_result[('sub_collection')];
						$sub_purchase=0;
						$purchase=0;
						$advice_amt=$row_result[('advice_amount')];
					}
					else
					{
						$sub_collection= $sub_extra;
						$sub_purchase=$row_result[('sub_purchase')];
						$purchase=$purchase;
						$advice_amt=0;
					}
					
				$onclick=" change_color('tr_".$k."','".$bgcolor."')";
				$html_data.='<tr bgcolor="'.$bgcolor.'" onclick="'.$onclick.'" id="tr_'.$k.'">';
				$html_data.='<td  width="50"><p>'.$k.'</p></td>
							<td  width="120"><p>'.$buyer_arr[$row_result["buyer_id"]].'</p></td>';
							if($row_result["is_lc"]==1)
							{
					$html_data.='<td  width="50" align="center"><p>Lc</p></td>
								<td  width="130"><p>'.$lc_result[$row_result[('lc_id')]]["lc_no"].'</p></td>';
							}
							else
							{
					$html_data.='<td  width="50" align="center"><p>Sc</p></td>
								<td  width="130"><p>'.$sc_result[$row_result[('lc_id')]]["lc_no"].'</p></td>';
							}
							if($row_result["is_lc"]==1)
							{
								if(!in_array($row_result['lc_id'],$lc_id_arr))
								{
					$html_data.='<td align="right" width="110"><p>'. number_format($lc_result[$row_result[('lc_id')]]["lc_val"],2,".","").'</p></td>';
								$gt_total_val+=$lc_result[$row_result[('lc_id')]]["lc_val"];
								//$buyer_result[$row_result["buyer_id"]]['lc_val']+=$lc_result[$row_result[('lc_id')]]["lc_val"];
								$lc_id_arr[$row_result['lc_id']]=$row_result['lc_id'];
								}
								else
								{
					$html_data.='<td align="right" width="110"><p></p></td>';
								}
							
							}
							else
							{
								if(!in_array($row_result['lc_id'],$sc_check_id_arr))
								{
					$html_data.='<td align="right" width="110"><p>'. number_format($sc_result[$row_result[('lc_id')]]["lc_val"],2,".","").'</p></td>';
								$gt_total_val+=$sc_result[$row_result[('lc_id')]]["lc_val"];
								//$buyer_result[$row_result["buyer_id"]]['lc_val']+=$sc_result[$row_result[('lc_id')]]["lc_val"];
								$sc_check_id_arr[$row_result['lc_id']]=$row_result['lc_id'];
								}
								else
								{
					$html_data.='<td align="right" width="110"><p></p></td>';
								}
							
							}
				$html_data.='<td align="center" width="110"><p>'.$row_result[('bank_ref_no')].'</p></td>
							<td align="center" width="130"><p>'.$row_result[('invoice_no')].'</p></td> 
							<td align="right" width="100"><p>'. $row_result[('invoice_quantity')].'</p></td>
							<td align="right" width="110"><p>'.number_format($row_result[('invoice_value')],2,".","").'</p></td>
							<td width="110" align="right"><p>'.number_format($sub_collection,2,".","").'</p></td>
							<td width="110" align="right"><p>'. number_format($sub_purchase,2,".","").'</p></td>
							<td width="110" align="right"><p>'. number_format($purchase,2,".","").'</p></td>
							<td width="110" align="right"><p>'.number_format($total_realize_value[ $lc_key][('distribution')],2,".","").'</p>
	</td>
							<td width="110" align="right"><p>'.number_format($total_realize_value[ $lc_key][('deducation')],2,".","").'</p>
	</td>
							<td align="right" width="110" ><p>'.number_format($advice_amt,2,".","").'</p></td>
							<td align="right" width="110"><p>'.number_format($row_result[('freight_amnt_by_supllier')],2,".","").'</p></td>
							<td align="right" width="110"><p>'. number_format($row_result[('paid_amount')],2,".","").'</p></td>
							<td width="110" align="right"><p>'. number_format($pandin_charse=$row_result[('freight_amnt_by_supllier')]- $row_result[('paid_amount')],2,".","").'</p></td>
							<td align="right" width="110"><p>'. number_format($freight_in_usd=$row_result[('freight_amnt_by_supllier')]/$txt_exchange_rate,2,".","").'</p></td>
							<td align="center"><p>'.number_format($parcentage=(($freight_in_usd/$row_result[('invoice_value')])*100),2,".","").'%												                                </p></td>
							</tr>';
								
				
				$total_inv_qty += $row_result[('invoice_quantity')];
				$total_invoice_val +$row_result[('invoice_value')];
				$total_sub_collectin +=$sub_collection;
				$total_sub_pur +=$sub_purchase;
				$all_purchase+=$purchase;
				$total_realize+=$total_realize_value[$lc_key][('distribution')];
				$total_deduction+=$total_realize_value[$lc_key][('deducation')];
				$total_freight +=$row_result[('freight_amnt_by_supllier')];
				$total_paid += $row_result[('paid_amount')];
				$total_panding+= $pandin_charse;
				$total_parcentage+=$parcentage;
				$total_advice_amt+=$row_result[('advice_amount')];
				
				
				
				if(in_array($lc_key,$rlz_sub_id))
				{
					$sub_collection=$sub_purchase=$purchase=0;
				}
				
				$buyer_result[$row_result["buyer_id"]]['buyer_name']=$row_result["buyer_id"];
				$buyer_result[$row_result["buyer_id"]]['invoice_quantity']+=$row_result[('invoice_quantity')];
				$buyer_result[$row_result["buyer_id"]]['invoice_value']+=$row_result[('invoice_value')];
				$buyer_result[$row_result["buyer_id"]]['sub_collection']+=$sub_collection;
				$buyer_result[$row_result["buyer_id"]]['sub_purchase']+=$sub_purchase;
				$buyer_result[$row_result["buyer_id"]]['purchase']+=$purchase;
				$buyer_result[$row_result["buyer_id"]]['distribution']+=$total_realize_value[ $lc_key][('distribution')];
				$buyer_result[$row_result["buyer_id"]]['deducation']+=$total_realize_value[ $lc_key][('deducation')];
				$buyer_result[$row_result["buyer_id"]]['advice_amount']+=$advice_amt;
				$buyer_result[$row_result["buyer_id"]]['freight_amnt_by_supllier']+=$row_result[('freight_amnt_by_supllier')];
				$buyer_result[$row_result["buyer_id"]]['paid_amount']+=$row_result[('paid_amount')];
				$buyer_result[$row_result["buyer_id"]]['pandin_charse']+=$pandin_charse;
				$buyer_result[$row_result["buyer_id"]]['freight_in_usd']+=$freight_in_usd;
			}
			
			if($cbo_search_by==2)
			{
				
			
				if($row_result[('is_lc')]==1)
				{
					$sub_inv_gros_val=$lc_gross_val_arr[$row_result[('lc_id')]];
				}
				else
				{
					$sub_inv_gros_val=$sc_gross_val_arr[$row_result[('lc_id')]];
				}
				
				if($sub_inv_gros_val!=$total_realize_value[$row_result[('sub_id')]]['rlz_val']) $total_realize_value[$row_result[('sub_id')]]['rlz_val']=$sub_inv_gros_val;
				
				if($lc_result[$row_result[('lc_id')]]["lc_val"]!=$total_realize_value[$row_result[('sub_id')]]['rlz_val'])
				{
					
					$purchase=$purchase_val_arr[$row_result['sub_id']];
					$sub_extra=$row_result['sub_purchase']- $purchase;
					if($row_result['submit_type']==1)
					{
						$sub_collection= $row_result[('sub_collection')];
						$sub_purchase=0;
						$purchase=0;
						$advice_amt=$row_result[('advice_amount')];
					}
					else
					{
						$sub_collection= $sub_extra;
						$sub_purchase=$row_result[('sub_purchase')];
						$purchase=$purchase;
						$advice_amt=0;
					}
					
					$onclick=" change_color('tr_".$k."','".$bgcolor."')";
					$html_data.='<tr bgcolor="'.$bgcolor.'" onclick="'.$onclick.'" id="tr_'.$k.'">';
					$html_data.='<td  width="50"><p>'.$k.'</p></td>
								<td  width="120"><p>'.$buyer_arr[$row_result["buyer_id"]].'</p></td>';
								if($row_result["is_lc"]==1)
								{
						$html_data.='<td  width="50" align="center"><p>Lc</p></td>
									<td  width="130"><p>'.$lc_result[$row_result[('lc_id')]]["lc_no"].'</p></td>';
								}
								else
								{
						$html_data.='<td  width="50" align="center"><p>Sc</p></td>
									<td  width="130"><p>'.$sc_result[$row_result[('lc_id')]]["lc_no"].'</p></td>';
								}
								if($row_result["is_lc"]==1)
								{
									if(!in_array($row_result['lc_id'],$lc_id_arr))
									{
						$html_data.='<td align="right" width="110"><p>'. number_format($lc_result[$row_result[('lc_id')]]["lc_val"],2,".","").'</p></td>';
									$gt_total_val+=$lc_result[$row_result[('lc_id')]]["lc_val"];
									//$buyer_result[$row_result["buyer_id"]]['lc_val']+=$lc_result[$row_result[('lc_id')]]["lc_val"];
									$lc_id_arr[$row_result['lc_id']]=$row_result['lc_id'];
									}
									else
									{
						$html_data.='<td align="right" width="110"><p></p></td>';
									}
								
								}
								else
								{
									if(!in_array($row_result['lc_id'],$sc_check_id_arr))
									{
						$html_data.='<td align="right" width="110"><p>'. number_format($sc_result[$row_result[('lc_id')]]["lc_val"],2,".","").'</p></td>';
									$gt_total_val+=$sc_result[$row_result[('lc_id')]]["lc_val"];
									//$buyer_result[$row_result["buyer_id"]]['lc_val']+=$sc_result[$row_result[('lc_id')]]["lc_val"];
									$sc_check_id_arr[$row_result['lc_id']]=$row_result['lc_id'];
									}
									else
									{
						$html_data.='<td align="right" width="110"><p></p></td>';
									}
								
								}
					$html_data.='<td align="center" width="110"><p>'.$row_result[('bank_ref_no')].'</p></td>
								<td align="center" width="130"><p>'.$row_result[('invoice_no')].'</p></td> 
								<td align="right" width="100"><p>'. $row_result[('invoice_quantity')].'</p></td>
								<td align="right" width="110"><p>'.number_format($row_result[('invoice_value')],2,".","").'</p></td>
								<td width="110" align="right"><p>'.number_format($sub_collection,2,".","").'</p></td>
								<td width="110" align="right"><p>'. number_format($sub_purchase,2,".","").'</p></td>
								<td width="110" align="right"><p>'. number_format($purchase,2,".","").'</p></td>
								<td width="110" align="right"><p>'.number_format($total_realize_value[ $lc_key][('distribution')],2,".","").'</p>
		</td>
								<td width="110" align="right"><p>'.number_format($total_realize_value[ $lc_key][('deducation')],2,".","").'</p>
		</td>
								<td align="right" width="110" ><p>'.number_format($advice_amt,2,".","").'</p></td>
								<td align="right" width="110"><p>'.number_format($row_result[('freight_amnt_by_supllier')],2,".","").'</p></td>
								<td align="right" width="110"><p>'. number_format($row_result[('paid_amount')],2,".","").'</p></td>
								<td width="110" align="right"><p>'. number_format($pandin_charse=$row_result[('freight_amnt_by_supllier')]- $row_result[('paid_amount')],2,".","").'</p></td>
								<td align="right" width="110"><p>'. number_format($freight_in_usd=$row_result[('freight_amnt_by_supllier')]/$txt_exchange_rate,2,".","").'</p></td>
								<td align="center"><p>'.number_format($parcentage=(($freight_in_usd/$row_result[('invoice_value')])*100),2,".","").'%												                                </p></td>
								</tr>';
									
					
					$total_inv_qty += $row_result[('invoice_quantity')];
					$total_invoice_val +$row_result[('invoice_value')];
					$total_sub_collectin +=$sub_collection;
					$total_sub_pur +=$sub_purchase;
					$all_purchase+=$purchase;
					$total_realize+=$total_realize_value[$lc_key][('distribution')];
					$total_deduction+=$total_realize_value[$lc_key][('deducation')];
					$total_freight +=$row_result[('freight_amnt_by_supllier')];
					$total_paid += $row_result[('paid_amount')];
					$total_panding+= $pandin_charse;
					$total_parcentage+=$parcentage;
					$total_advice_amt+=$row_result[('advice_amount')];
					
					
					if(in_array($lc_key,$rlz_sub_id))
					{
						$sub_collection=$sub_purchase=$purchase=0;
					}
					
					
					$buyer_result[$row_result["buyer_id"]]['buyer_name']=$row_result["buyer_id"];
					$buyer_result[$row_result["buyer_id"]]['invoice_quantity']+=$row_result[('invoice_quantity')];
					$buyer_result[$row_result["buyer_id"]]['invoice_value']+=$row_result[('invoice_value')];
					$buyer_result[$row_result["buyer_id"]]['sub_collection']+=$sub_collection;
					$buyer_result[$row_result["buyer_id"]]['sub_purchase']+=$sub_purchase;
					$buyer_result[$row_result["buyer_id"]]['purchase']+=$purchase;
					$buyer_result[$row_result["buyer_id"]]['distribution']+=$total_realize_value[ $lc_key][('distribution')];
					$buyer_result[$row_result["buyer_id"]]['deducation']+=$total_realize_value[ $lc_key][('deducation')];
					$buyer_result[$row_result["buyer_id"]]['advice_amount']+=$advice_amt;
					$buyer_result[$row_result["buyer_id"]]['freight_amnt_by_supllier']+=$row_result[('freight_amnt_by_supllier')];
					$buyer_result[$row_result["buyer_id"]]['paid_amount']+=$row_result[('paid_amount')];
					$buyer_result[$row_result["buyer_id"]]['pandin_charse']+=$pandin_charse;
					$buyer_result[$row_result["buyer_id"]]['freight_in_usd']+=$freight_in_usd;
				}
			
			}
		
			if($cbo_search_by==3)
			{
				
				if($row_result[('is_lc')]==1)
				{
					$sub_inv_gros_val=$lc_gross_val_arr[$row_result[('lc_id')]];
				}
				else
				{
					$sub_inv_gros_val=$sc_gross_val_arr[$row_result[('lc_id')]];
				}
				//$sub_inv_gros_val=return_field_value("sum(invoice_value) as invoice_value","com_export_invoice_ship_mst","id in($inv_id)","invoice_value");
				//var_dump($sub_inv_gros_val);var_dump($total_realize_value[$row_result[csf('sub_id')]]['rlz_val']);echo $lc_result[$row_result[csf('lc_id')]]["lc_val"];
				if($sub_inv_gros_val!=$total_realize_value[$row_result[('sub_id')]]['rlz_val']) $total_realize_value[$row_result[('sub_id')]]['rlz_val']=$sub_inv_gros_val;
				
				if($lc_result[$row_result[('lc_id')]]["lc_val"]==$total_realize_value[$row_result[('sub_id')]]['rlz_val'])
				{
					$purchase=$purchase_val_arr[$row_result['sub_id']];
					$sub_extra=$row_result['sub_purchase']- $purchase;
					if($row_result['submit_type']==1)
					{
						$sub_collection= $row_result[('sub_collection')];
						$sub_purchase=0;
						$purchase=0;
						$advice_amt=$row_result[('advice_amount')];
					}
					else
					{
						$sub_collection= $sub_extra;
						$sub_purchase=$row_result[('sub_purchase')];
						$purchase=$purchase;
						$advice_amt=0;
					}
						
					$onclick=" change_color('tr_".$k."','".$bgcolor."')";
					$html_data.='<tr bgcolor="'.$bgcolor.'" onclick="'.$onclick.'" id="tr_'.$k.'">';
					$html_data.='<td  width="50"><p>'.$k.'</p></td>
								<td  width="120"><p>'.$buyer_arr[$row_result["buyer_id"]].'</p></td>';
								if($row_result["is_lc"]==1)
								{
						$html_data.='<td  width="50" align="center"><p>Lc</p></td>
									<td  width="130"><p>'.$lc_result[$row_result[('lc_id')]]["lc_no"].'</p></td>';
								}
								else
								{
						$html_data.='<td  width="50" align="center"><p>Sc</p></td>
									<td  width="130"><p>'.$sc_result[$row_result[('lc_id')]]["lc_no"].'</p></td>';
								}
								if($row_result["is_lc"]==1)
								{
									if(!in_array($row_result['lc_id'],$lc_id_arr))
									{
						$html_data.='<td align="right" width="110"><p>'. number_format($lc_result[$row_result[('lc_id')]]["lc_val"],2,".","").'</p></td>';
									$gt_total_val+=$lc_result[$row_result[('lc_id')]]["lc_val"];
									//$buyer_result[$row_result["buyer_id"]]['lc_val']+=$lc_result[$row_result[('lc_id')]]["lc_val"];
									$lc_id_arr[$row_result['lc_id']]=$row_result['lc_id'];
									}
									else
									{
						$html_data.='<td align="right" width="110"><p></p></td>';
									}
								
								}
								else
								{
									if(!in_array($row_result['lc_id'],$sc_check_id_arr))
									{
						$html_data.='<td align="right" width="110"><p>'. number_format($sc_result[$row_result[('lc_id')]]["lc_val"],2,".","").'</p></td>';
									$gt_total_val+=$sc_result[$row_result[('lc_id')]]["lc_val"];
									//$buyer_result[$row_result["buyer_id"]]['lc_val']+=$sc_result[$row_result[('lc_id')]]["lc_val"];
									$sc_check_id_arr[$row_result['lc_id']]=$row_result['lc_id'];
									}
									else
									{
						$html_data.='<td align="right" width="110"><p></p></td>';
									}
								
								}
					$html_data.='<td align="center" width="110"><p>'.$row_result[('bank_ref_no')].'</p></td>
								<td align="center" width="130"><p>'.$row_result[('invoice_no')].'</p></td> 
								<td align="right" width="100"><p>'. $row_result[('invoice_quantity')].'</p></td>
								<td align="right" width="110"><p>'.number_format($row_result[('invoice_value')],2,".","").'</p></td>
								<td width="110" align="right"><p>'.number_format($sub_collection,2,".","").'</p></td>
								<td width="110" align="right"><p>'. number_format($sub_purchase,2,".","").'</p></td>
								<td width="110" align="right"><p>'. number_format($purchase,2,".","").'</p></td>
								<td width="110" align="right"><p>'.number_format($total_realize_value[ $lc_key][('distribution')],2,".","").'</p>
		</td>
								<td width="110" align="right"><p>'.number_format($total_realize_value[ $lc_key][('deducation')],2,".","").'</p>
		</td>
								<td align="right" width="110" ><p>'.number_format($advice_amt,2,".","").'</p></td>
								<td align="right" width="110"><p>'.number_format($row_result[('freight_amnt_by_supllier')],2,".","").'</p></td>
								<td align="right" width="110"><p>'. number_format($row_result[('paid_amount')],2,".","").'</p></td>
								<td width="110" align="right"><p>'. number_format($pandin_charse=$row_result[('freight_amnt_by_supllier')]- $row_result[('paid_amount')],2,".","").'</p></td>
								<td align="right" width="110"><p>'. number_format($freight_in_usd=$row_result[('freight_amnt_by_supllier')]/$txt_exchange_rate,2,".","").'</p></td>
								<td align="center"><p>'.number_format($parcentage=(($freight_in_usd/$row_result[('invoice_value')])*100),2,".","").'%												                                </p></td>
								</tr>';
									
					
					$total_inv_qty += $row_result[('invoice_quantity')];
					$total_invoice_val +$row_result[('invoice_value')];
					$total_sub_collectin +=$sub_collection;
					$total_sub_pur +=$sub_purchase;
					$all_purchase+=$purchase;
					$total_realize+=$total_realize_value[$lc_key][('distribution')];
					$total_deduction+=$total_realize_value[$lc_key][('deducation')];
					$total_freight +=$row_result[('freight_amnt_by_supllier')];
					$total_paid += $row_result[('paid_amount')];
					$total_panding+= $pandin_charse;
					$total_parcentage+=$parcentage;
					$total_advice_amt+=$row_result[('advice_amount')];
					
					
					if(in_array($lc_key,$rlz_sub_id))
					{
						$sub_collection=$sub_purchase=$purchase=0;
					}
					
					
					$buyer_result[$row_result["buyer_id"]]['buyer_name']=$row_result["buyer_id"];
					$buyer_result[$row_result["buyer_id"]]['invoice_quantity']+=$row_result[('invoice_quantity')];
					$buyer_result[$row_result["buyer_id"]]['invoice_value']+=$row_result[('invoice_value')];
					$buyer_result[$row_result["buyer_id"]]['sub_collection']+=$sub_collection;
					$buyer_result[$row_result["buyer_id"]]['sub_purchase']+=$sub_purchase;
					$buyer_result[$row_result["buyer_id"]]['purchase']+=$purchase;
					$buyer_result[$row_result["buyer_id"]]['distribution']+=$total_realize_value[ $lc_key][('distribution')];
					$buyer_result[$row_result["buyer_id"]]['deducation']+=$total_realize_value[ $lc_key][('deducation')];
					$buyer_result[$row_result["buyer_id"]]['advice_amount']+=$advice_amt;
					$buyer_result[$row_result["buyer_id"]]['freight_amnt_by_supllier']+=$row_result[('freight_amnt_by_supllier')];
					$buyer_result[$row_result["buyer_id"]]['paid_amount']+=$row_result[('paid_amount')];
					$buyer_result[$row_result["buyer_id"]]['pandin_charse']+=$pandin_charse;
					$buyer_result[$row_result["buyer_id"]]['freight_in_usd']+=$freight_in_usd;
				
				}
			
			}
		
		$purchase=0;$inv_id=0;$sub_inv_gros_val=0;$sub_id_con=0;
		$k++;
		}
		
		
		$m=1;$buyer_inhand=array();
		foreach($unsubmit_inv as $lc_key=>$row_result)
		{
			if ($k%2==0)
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			$onclick=" change_color('tr_".$k."','".$bgcolor."')";
			$html_unsubmit_data.='<tr bgcolor="'.$bgcolor.'" onclick="'.$onclick.'" id="tr_'.$k.'">';
			$html_unsubmit_data.='<td  width="50"><p>'.$m.'</p></td>
								<td  width="120"><p>'.$buyer_arr[$row_result[("buyer_id")]].'</p></td>
								<td align="center" width="130"><p>'.$row_result[('invoice_no')].'</p></td> 
								<td align="right" width="120"><p>'. $row_result[('invoice_quantity')].'</p></td>
								<td align="right" width="120"><p>'. number_format($row_result[('invoice_value')],2,".","").'</p></td>';
								if($row_result[('type')]==1)
								{
			$html_unsubmit_data.='<td align="center" width="100"><p>Lc</p></td>';
								}
								else
								{
			$html_unsubmit_data.='<td align="center" width="100"><p>Sc</p></td>';
								}
			$html_unsubmit_data.='<td   align="center" width="120"><p>'.$row_result[('export_lc_no')].'</p></td>
								<td   align="right" width="100"><p>'.number_format($row_result[('lc_sc_val')],2,".","").'</p></td>
								<td  ><p>'.$row_result[('remarks')].'</p></td>
			</tr>';
							
			
			$unsbmit_inv_qty += $row_result[('invoice_quantity')];
			$unsbmit_invoice_val +=$row_result[('invoice_value')];
			$unsubmit_lc_val +=$row_result[('lc_sc_val')];
			$buyer_result[$row_result["buyer_id"]]['buyer_name']=$row_result["buyer_id"];
			$buyer_result[$row_result["buyer_id"]]['invoice_quantity']+=$row_result[('invoice_quantity')];
			$buyer_result[$row_result["buyer_id"]]['invoice_value']+=$row_result[('invoice_value')];
			$buyer_inhand[$row_result["buyer_id"]]+=$row_result[('invoice_value')];
			/*if($row_result['type']==1)//for lc check $lc_id_arr
			{
				if(!in_array($row_result[('lc_sc_id')],$lc_id_arr))
				{
					$buyer_result[$row_result["buyer_id"]]['lc_val']+=$row_result["lc_sc_val"];
					$lc_id_arr[$row_result['lc_sc_id']]=$row_result['lc_sc_id'];
				}
			}
			else if($row_result['type']==2) // for sc check $sc_check_id_arr
			{
				if(!in_array($row_result[('lc_sc_id')],$sc_check_id_arr))
				{
					$buyer_result[$row_result["buyer_id"]]['lc_val']+=$row_result["lc_sc_val"];
					$sc_check_id_arr[$row_result['lc_sc_id']]=$row_result['lc_sc_id'];
				}
			}*/
			$k++;$m++;
		}
		//print_r($buyer_result);
		//var_dump($sc_check_id_arr);
		
		$n=1;$html_no_invoice_data="";
		foreach($no_inv_lc as $lc_key=>$row_result)
		{
			if ($k%2==0)
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			$onclick=" change_color('tr_".$k."','".$bgcolor."')";
			$html_no_invoice_data.='<tr bgcolor="'.$bgcolor.'" onclick="'.$onclick.'" id="tr_'.$k.'">';
			$html_no_invoice_data.='<td  width="50"><p>'.$n.'</p></td>
								<td  width="120"><p>'.$buyer_arr[$row_result[("buyer_id")]].'</p></td>';
								if($row_result[('type')]==1)
								{
			$html_no_invoice_data.='<td align="center" width="100"><p>Lc</p></td>';
								}
								else
								{
			$html_no_invoice_data.='<td align="center" width="100"><p>Sc</p></td>';
								}
			$html_no_invoice_data.='<td   align="center" width="130"><p>'.$row_result[('lc_sc_no')].'</p></td>
								<td   align="right"><p>'.number_format($row_result[('lc_sc_val')],2,".","").'</p></td>
			</tr>';
							
			
			$total_no_invoice_val +=$row_result[('lc_sc_val')];
			
			$buyer_result[$row_result["buyer_id"]]['buyer_name']=$row_result["buyer_id"];
			$buyer_result[$row_result["buyer_id"]]['invoice_quantity']+=$row_result[('invoice_quantity')];
			$buyer_result[$row_result["buyer_id"]]['invoice_value']+=$row_result[('invoice_value')];
			/*if($row_result["type"]==1) // for lc check
			{
				if(!in_array($row_result[('id')],$lc_id_arr))
				{
					$buyer_result[$row_result["buyer_id"]]['lc_val']+=$row_result["lc_sc_val"];
					$lc_id_arr[$row_result['lc_sc_id']]=$row_result['id'];
				}
			}
			else if($row_result["type"]==2) // for sc check
			{
				if(!in_array($row_result[('id')],$sc_check_id_arr))
				{
					$buyer_result[$row_result["buyer_id"]]['lc_val']+=$row_result["lc_sc_val"];
					$sc_check_id_arr[$row_result['lc_sc_id']]=$row_result['id'];
				}
			}*/
			$k++;$n++;
		}
		//var_dump($no_inv_lc);
			
	ob_start();
?>
<div style="width:2150px">
    <div align="left">
        <table width="1500" cellpadding="0" cellspacing="0" id="caption" align="center">
        <tr>
        <td align="center" width="100%" colspan="15" class="form_caption" ><strong style="font-size:18px">Company Name:<?php echo " ".$company_library[$cbo_company_name]; ?></strong></td>
        </tr>
        <tr>  
        	<td align="center" width="100%" colspan="15" class="form_caption" ><strong style="font-size:18px"><?php echo $report_title; ?></strong></td>
        </tr> 
          
  
        </table>
        <br />
        <strong style="font-size:18px;">&nbsp; Buyer Wise:</strong>
    	<br />
       <table width="1950" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="">
        <thead>
            <tr>
                <th width="50" rowspan="2">Sl</th>
                <th width="120" rowspan="2">Buyer</th>
                <th width="110" rowspan="2">LC Value USD</th>
                <th colspan="2">Invoice</th>
                <th width="110" rowspan="2">Online/ Sub. as Collection</th>
                <th width="110" rowspan="2">Online/ Sub. as Purchase</th>
                <th width="110" rowspan="2">Purchase</th>
                <th width="100" rowspan="2">%</th>
                <th width="110" rowspan="2">Realized</th>
                <th width="110" rowspan="2">Deducation</th>
                <th width="110" rowspan="2">In Hand</th>
                <th width="110" rowspan="2">Advice Received</th>
                <th width="110" rowspan="2">Freight Charges(Tk)</th>
                <th width="110" rowspan="2">Freight Paid (Tk)</th>
                <th width="110" rowspan="2">Freight Pending (Tk)</th>
                <th width="110" rowspan="2">Freight In USD</th>
                <th rowspan="2" >Freight % on Inv. Value</th>
            </tr>
            <tr>
                <th width="110">Invoice  Qty.</th>
                <th width="110" >Invoice Value</th>
            </tr>
        </thead>
        </table>
        <div style="width:1968px; overflow-y:scroll; max-height:300px;font-size:12px; overflow-x:hidden;" id="scroll_body2" align="left">
        <table width="1950" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body2" align="left">
        <tbody>
			<?php
			//buyer wise table print here 
			$i=1;
			//var_dump($buyer_result);
            foreach($buyer_result as $buy_key=>$row_result)
            {
				if ($k%2==0)
				$bgcolor="#E9F3FF";
				else
				$bgcolor="#FFFFFF";
				$sub_extra=$row_result['sub_purchase']- $row_result['purchase'];
            ?>
            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $k; ?>">
                <td  width="50"><p><?php echo $i; ?>&nbsp;</p></td>
                <td  width="120"><p><?php echo  $buyer_arr[$row_result['buyer_name']]; ?>&nbsp;</p></td>
                <td align="right" width="110"><p><?php echo number_format($buyer_lc_val[$buy_key],2,".","");$buyer_total_lc_val+=$buyer_lc_val[$buy_key];?>&nbsp;</p></td>
                <td align="right" width="110"><p><?php echo  $row_result['invoice_quantity']; $buyer_total_inv_qty += $row_result['invoice_quantity']; ?>&nbsp;</p></td>
                <td align="right" width="110"><p><?php echo number_format($row_result['invoice_value'],2,".",""); $buy_total_invoice_val +=$row_result['invoice_value']; ?>&nbsp;</p></td>
                <td width="110" align="right"><p><?php echo number_format(($row_result['sub_collection']),2,".",""); $buy_total_sub_collectin +=$row_result['sub_collection'];?>&nbsp;</p></td>
                <td width="110" align="right"><p><?php echo number_format($row_result['sub_purchase'],2,".",""); $buy_total_sub_pur +=$row_result['sub_purchase'];?>&nbsp;</p></td>
                <td width="110" align="right"><p><?php echo number_format($row_result['purchase'],2); $buy_all_purchase+=$row_result['purchase'];?></p></td>
                <td width="100" align="right"><p><?php $parcentage=($row_result['purchase']/$row_result['sub_purchase'])*100; echo number_format($parcentage,2)."%"; ?></p></td>
                <td width="110" align="right">
                <p><?php echo number_format($row_result['distribution'],2,".",""); $buy_total_realize+=$row_result['distribution'];?>&nbsp;</p>
                </td>
                <td width="110" align="right">
                <p><?php echo number_format($row_result['deducation'],2,".",""); $buy_total_deduction+=$row_result['deducation']; ?>&nbsp;</p>
                </td>
                <td align="right" width="110">
                <p><?php  
				//echo number_format($buyer_inhand[$buy_key],2,".",""); $buy_total_in_hand +=$buyer_inhand[$buy_key];
				echo number_format($buyer_inhand_total[$buy_key],2,".",""); $buy_total_in_hand +=$buyer_inhand_total[$buy_key];  
				?>&nbsp;</p></td>
                <td align="right" width="110" ><p><?php  echo number_format($row_result['advice_amount'],2,".",""); $buy_total_advice_amt +=$row_result['advice_amount'];?>&nbsp;</p></td>
                <td align="right" width="110"><p><?php echo  number_format($row_result['freight_amnt_by_supllier'],2,".",""); $buy_total_freight +=$row_result['freight_amnt_by_supllier']; ?>&nbsp;</p></td>
                <td align="right" width="110"><p><?php echo number_format($row_result['paid_amount'],2,".",""); $buy_total_paid += $row_result['paid_amount']; ?>&nbsp;</p></td>
                <td width="110" align="right"><p><?php echo number_format($row_result['pandin_charse'],2,".","");$buy_total_panding+= $row_result['pandin_charse'];?>&nbsp;</p></td>
                <td align="right" width="110"><p><?php echo number_format($row_result['freight_in_usd'],2,".","")?>&nbsp;</p></td>
                <td align="center"><p><?php  $buy_parcentage=($row_result['freight_in_usd']/$row_result['invoice_value'])*100; echo number_format($buy_parcentage,2,".","");$buy_total_parcentage+=$buy_parcentage;?>%&nbsp;</p></td>
            </tr>
			<?php
			$purchase=0;$inv_id=0;$sub_inv_gros_val=0;
			$k++;$i++;
			}
            ?>
        </tbody>
        </table>
        <table width="1950" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="" align="left">
        <tfoot>
            <tr>
            <th width="50" align="right"></th>
            <th width="120" align="right">Grand Total:</th>
            <th  id="value_buyer_total_lc_val" width="110"><?php  echo number_format($buyer_total_lc_val,2); ?></th>
            <th  id="buyer_total_inv_qty" width="110"><?php echo number_format($buyer_total_inv_qty,0); ?></th>
            <th id="value_buy_total_invoice_val" width="110"><?php echo number_format($buy_total_invoice_val,2); ?></th>
            <th id="value_buy_total_sub_collectin" width="110"><?php echo number_format($buy_total_sub_collectin,2); ?></th> 
            <th id="value_buy_total_sub_pur" width="110"><?php echo number_format($buy_total_sub_pur,2); ?></th>
            <th id="value_buy_all_purchase" width="110"><?php echo number_format($buy_all_purchase,2); ?></th>
            <th width="100">&nbsp;</th>
            <th id="value_buy_total_realize" width="110"><?php echo number_format($buy_total_realize,2); ?></th>
            <th id="value_buy_total_deduction" width="110"><?php echo number_format($buy_total_deduction,2); ?></th>
            <th id="value_buy_total_in_hand" width="110"><?php echo number_format($buy_total_in_hand,2); ?></th>
            <th  width="110"></th>
            <th id="value_buy_total_freight" width="110"><?php echo number_format($buy_total_freight,2); ?></th>
            <th id="value_buy_total_paid" width="110"><?php echo number_format($buy_total_paid,2); ?></th>
            <th id="value_buy_total_panding" width="110"><?php echo number_format($buy_total_panding,2); ?></th>
            <th width="110">&nbsp;</th>
            <th id="buy_total_parcentage" ><?php echo number_format($buy_total_parcentage,2); ?></th>
            </tr>
        </tfoot>
        </table>
        </div>
        </div>
        
    <div align="left">	
    	<br />
        <strong style="font-size:18px;">&nbsp; Lc/Sc Wise(Submitted & Realized) :</strong>
        <br />
        <table width="2130" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="">
        <thead>
            <tr>
                <th width="50">Sl</th>
                <th width="120">Buyer</th>
                <th width="50">LC/SC</th>
                <th width="130">LC/SC No</th>
                <th width="110">LC Value USD</th>
                <th width="110">Bill Number</th>
                <th width="130">Invoice No</th>
                <th width="100">Invoice Qty.</th>
                <th width="110">Invoice Value</th>
                <th width="110">Online/ Sub. as Collection</th>
                <th width="110">Online/ Sub. as Purchase</th>
                <th width="110">Purchase</th>
                <th width="110">Realized</th>
                <th width="110">Deducation</th>
                <th width="110">Advice Received</th>
                <th width="110">Freight Charges(Tk)</th>
                <th width="110">Freight Paid (Tk)</th>
                <th width="110">Freight Pending (Tk)</th>
                <th width="110">Freight In USD</th>
                <th >Freight % on Inv. Value</th>
            </tr>
        </thead>
        </table>
    
        <div style="width:2150px; overflow-y:scroll; overflow-x:hidden; max-height:300px;font-size:12px;" id="scroll_body">
        <table width="2130" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
        <tbody>
		<?php
		//lc wise table print here
		
		echo $html_data;
		?>	
        </tbody>
        </table>
        </div>
        <table width="2130" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="">
        <tfoot>
            <tr>
            <th  align="right" width="50">&nbsp;</th>
            <th  align="right" width="120">&nbsp;</th>
            <th  align="right" width="50">&nbsp;</th>
            <th  align="right" width="130">Grand Total:</th>
            <th id="value_gt_total_val" width="110"><?php  echo number_format($gt_total_val,2); ?></th>
            <th width="110" ></th>
            <th  width="130"></th>
            <th align="right" id="total_inv_qty" width="100"><?php echo number_format($total_inv_qty,0); ?></th>
            <th id="value_total_invoice_val" width="110"><?php echo number_format($total_invoice_val,2); ?></th>
            <th  id="value_total_sub_collectin" width="110"><?php echo number_format($total_sub_collectin,2); ?></th> 
            <th id="value_total_sub_pur" width="110"><?php echo number_format($total_sub_pur,2); ?></th>
            <th  id="value_all_purchase" width="110"><?php echo number_format($all_purchase,2); ?></th>
            <th id="value_total_realize" width="110"><?php echo number_format($total_realize,2); ?></th>
            <th id="value_total_deduction" width="110"><?php echo number_format($total_deduction,2); ?></th>
            <th id="value_total_advice_amt" width="110"><?php echo number_format($total_advice_amt,2); ?></th>
            <th  id="value_total_freight" width="110"><?php echo number_format($total_freight,2); ?></th>
            <th id="value_total_paid" width="110"><?php echo number_format($total_paid,2); ?></th>
            <th id="value_total_panding" width="110"><?php echo number_format($total_panding,2); ?></th>
            <th width="110">&nbsp;</th>
            <th align="right" id="total_parcentage"><?php echo number_format($total_parcentage,2); ?></th>
            </tr>
        </tfoot>
        </table>
        </div>
        <div align="left">
        
    	<br />
        <strong style="font-size:18px;">&nbsp; Un-Submitted Invoice :</strong>
        <br />

        <table width="1200" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="" >
        <thead>
            <tr>
                <th width="50">Sl</th>
                <th width="120">Buyer</th>
                <th width="130">Invoice No</th>
                <th width="120">Invoice Qty.</th>
                <th width="120">Invoice Value</th>
                <th width="100">LC/SC </th>
                <th width="120">Lc/SC No</th>
                <th width="100">Lc/SC Value</th>
                <th >Remarks</th>
            </tr>
        </thead>
        </table>
    
        <div style="width:1220px; overflow-y:scroll; overflow-x:hidden; max-height:300px; font-size:12px;" id="scroll_body3" >
        <table width="1200" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body3" >
        <tbody>
		<?php
		//Unsubmitted Invoice data  print here
		echo $html_unsubmit_data;
		?>	
        </tbody>
        </table>
        </div>
        <table width="1200" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="" >
        <tfoot>
            <tr>
                <th  align="right" width="50"></th>
                <th  align="right"  width="120"></th>
                <th  align="right"  width="130">Grand Total:</th>
                <th id="unsbmit_inv_qty"  width="120"><?php  echo number_format($unsbmit_inv_qty,0); ?></th>
                <th align="right" id="value_unsbmit_invoice_val"  width="120"><?php echo number_format($unsbmit_invoice_val,2); ?></th>
                <th align="right"  width="100">&nbsp; </th>
                <th align="right"  width="120">&nbsp; </th>
                <th align="right" > <?php  ?> &nbsp;</th>
                <th  >&nbsp; </th>
            </tr>
        </tfoot>
        </table>
        </div>
        
        <div align="left">
        
    	<br />
        <strong style="font-size:18px;">&nbsp; No Invoice Lc/Sc :</strong>
        <br />

        <table width="550" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="" >
        <thead>
            <tr>
                <th width="50">Sl</th>
                <th width="120">Buyer</th>
                <th width="100">Lc/Sc</th>
                <th width="130">Lc No</th>
                <th >Lc Value</th>
            </tr>
        </thead>
        </table>
    
        <div style="width:570px; overflow-y:scroll; overflow-x:hidden; max-height:300px; font-size:12px;" id="scroll_body4" >
        <table width="550" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body4" >
        <tbody>
		<?php
		//No Invoice data  print here
		echo $html_no_invoice_data;
		?>	
        </tbody>
        </table>
        </div>
        <table width="550" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="" >
        <tfoot>
            <tr>
            <th width="50"></th>
            <th width="120" ></th>
            <th  width="100"></th>
            <th width="130">Grand Total:</th>
            <th id="value_total_no_invoice_val"><span style="font-size:11px; font-weight:bold;"><?php echo number_format($total_no_invoice_val,2); ?></span></th>
            </tr>
        </tfoot>
        </table>
        </div>
        
</div>	
<?php	   
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
}
disconnect($con);


?>
