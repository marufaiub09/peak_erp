<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------------------------------
$company_library=return_library_array( "select id, company_short_name from lib_company", "id", "company_short_name"  );
$buyer_short_name_library=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$costing_per_id_library=return_library_array( "select job_no, costing_per from wo_pre_cost_mst", "job_no", "costing_per");
$item_library=return_library_array( "select id, item_name from lib_item_group", "id", "item_name"  );
$color_name_library=return_library_array( "select id, color_name from lib_color", "id", "color_name"  );
$country_name_library=return_library_array( "select id, country_name from lib_country", "id", "country_name"  );
$order_arr=return_library_array( "select id, po_number from wo_po_break_down", "id", "po_number"  );

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 160, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );   	 
	exit();
}
$tmplte=explode("**",$data);
if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;


if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$company_name=str_replace("'","",$cbo_company_name);
	$serch_by=str_replace("'","",$cbo_search_by);
	$buyer_id_cond="";
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="")
			{
				$buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")";
			}
			else
			{
				$buyer_id_cond="";
			}
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	
	
	
	$txt_job_no=str_replace("'","",$txt_job_no);
	$txt_job_no=trim($txt_job_no);
	if($txt_job_no !="" || $txt_job_no !=0)
	{
		$year = substr(str_replace("'","",$cbo_year_selection), -2); 
		$job_no=$company_library[$company_name]."-".$year."-".str_pad($txt_job_no, 5, 0, STR_PAD_LEFT);
		$jobcond="and a.job_no='".$job_no."'";
	}
	else
	{
		$jobcond="";	
	}
	
	
	if(str_replace("'","",$cbo_item_group)=="")
	{
		$item_group_cond="";
	}
	else
	{
		$item_group_cond="and e.trim_group in(".str_replace("'","",$cbo_item_group).")";
	}
	
	
	$date_cond='';
	if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
	{
		$start_date=(str_replace("'","",$txt_date_from));
		$end_date=(str_replace("'","",$txt_date_to));
		$date_cond="and c.country_ship_date between '$start_date' and '$end_date'";
		
	}
	if (str_replace("'","",$txt_job_no)=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in (".str_replace("'","",$txt_job_no).") ";
	
	if(str_replace("'","",$txt_style_ref)!="") $style_ref_cond=" and a.style_ref_no like '%".str_replace("'","",$txt_style_ref)."%'"; else $style_ref_cond="";
	
	

  if(str_replace("'","",$cbo_search_by)==1)
  {
	if($template==1)
	{
		ob_start();
	?>
		<div style="width:2050px">
		<fieldset style="width:100%;">	
			<table width="2050">
				<tr class="form_caption">
					<td colspan="24" align="center">Accessories Followup Report</td>
				</tr>
				<tr class="form_caption">
					<td colspan="24" align="center"><? echo $company_library[$company_name]; ?></td>
				</tr>
			</table>
			<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all">
				<thead>
					<th width="30">SL</th>
					<th width="50">Buyer</th>
					<th width="100">Job No</th>
					<th width="100">Style Ref</th>
					<th width="90">Order No</th>
					<th width="80">Order Qnty</th>
					<th width="50">UOM</th>
					<th width="80">Qnty (Pcs)</th>
					<th width="80">Shipment Date</th>
					<th width="100">Trims Name</th>
					<th width="100">Brand/Sup Ref</th>
					<th width="60">Appr Req.</th>
					<th width="80">Approve Status</th>
                    <th width="100">Item Entry Date</th>
					<th width="100">Req Qnty</th>
					<th width="100">Pre Costing Value</th>
					<th width="90">WO Qnty</th>
                    <th width="60">Trims UOM</th>
                    <th width="100">WO Value</th>
                    <th width="70">WO Delay Days</th>
					<th width="90">In-House Qnty</th>
					<th width="90">Receive Balance</th>
					<th width="90">Issue to Prod.</th>
					<th>Left Over/Balance</th>
				</thead>
			</table>
			<div style="width:2030px; max-height:400px; overflow-y:scroll" id="scroll_body">
				<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
	<?
	$wo_qty_array=array();
	$wo_qty_summary_array=array();
	if($db_type==2)
	{
	 $wo_sql="select min(a.booking_date) as booking_date ,b.job_no,LISTAGG(CAST(a.booking_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id, sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b 
	where a.item_category=4 and a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id";
	}
	else if($db_type==0)
	{
	$wo_sql="select min(a.booking_date) as booking_date ,b.job_no,group_concat(a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id,sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b 
	where a.item_category=4 and a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id";
	}
	$dataArray=sql_select($wo_sql);
	foreach($dataArray as $row )
	{
		
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['booking_no']=$row[csf('booking_no')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['wo_qnty']=$row[csf('wo_qnty')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['rate']=$row[csf('rate')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['amount']=$row[csf('amount')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['wo_date']=$row[csf('booking_date')];
		
		$wo_qty_summary_array[$row[csf('trim_group')]]['wo_qnty']=$row[csf('wo_qnty')];
	}
	
	
	$conversion_factor_array=array();
	$conversion_factor=sql_select("select id ,conversion_factor from  lib_item_group  ");
	foreach($conversion_factor as $row_f)
	{
	$conversion_factor_array[$row_f[csf('id')]]['con_factor']=$row_f[csf('conversion_factor')];
	}
	
	$sql_po_qty_country_wise_arr=array();	
	$sql_po_qty_country_wise=sql_select("select  b.id,c.country_id, sum(c.order_quantity/a.total_set_qnty) as order_quantity_set  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst  and b.id=c.po_break_down_id and a.company_name=1  and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1  and a.company_name=$company_name $buyer_id_cond $date_cond $style_ref_cond $jobcond group by   b.id,c.country_id order by b.id,c.country_id");
	foreach( $sql_po_qty_country_wise as $sql_po_qty_country_wise_row)
	{
	$sql_po_qty_country_wise_arr[$sql_po_qty_country_wise_row[csf('id')]][$sql_po_qty_country_wise_row[csf('country_id')]]=$sql_po_qty_country_wise_row[csf('order_quantity_set')]; 
	}
 
    $receive_qty_array=array();
	$receive_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity   from  inv_receive_master c,product_details_master d,inv_trims_entry_dtls a , order_wise_pro_details b where a.mst_id=c.id and a.trans_id=b.trans_id and a.prod_id=d.id and b.trans_type=1 and b.entry_form=24 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
	foreach($receive_qty_data as $row)
	{
		$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['receive_qty']=$row[csf('quantity')];
	}
		
	$issue_qty_array=array();
	$issue_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity  from  inv_issue_master d,product_details_master p,inv_trims_issue_dtls a , order_wise_pro_details b where a.mst_id=d.id and a.prod_id=p.id and a.trans_id=b.trans_id and b.trans_type=2 and b.entry_form=25 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
	foreach($issue_qty_data as $row)
	{
		$issue_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['issue_qty']=$row[csf('quantity')];
	}
	$po_data_arr=array();
	$po_id_string="";
	$today=date("Y-m-d");
	$sql_query=sql_select("select a.buyer_name,a.job_no,a.job_no_prefix_num,style_ref_no, b.id,b.po_number,a.order_uom,sum(distinct b.po_quantity) as po_quantity,a.total_set_qnty  ,sum(distinct b.po_quantity*a.total_set_qnty) as po_quantity_psc ,sum(c.order_quantity) as order_quantity ,sum(c.order_quantity/a.total_set_qnty) as order_quantity_set,  b.pub_shipment_date,
	d.costing_per,
	e.id as trim_dtla_id,
	e.trim_group,
	e.description,
	e.brand_sup_ref,
	e.cons_uom,
	e.cons_dzn_gmts,
	e.rate,
	e.amount,
	e.apvl_req,
	e.nominated_supp,
	e.insert_date,
	f.cons,
	f.country_id,
	f.cons as cons_cal
	from 
	wo_po_details_master a, 
	wo_po_break_down b,
	wo_po_color_size_breakdown c
	left join 
	wo_pre_cost_trim_co_cons_dtls f 
	on
	c.job_no_mst=f.job_no and
	c.po_break_down_id=f.po_break_down_id
	join
	wo_pre_cost_trim_cost_dtls e
	on 
	f.job_no=e.job_no  and
	e.id=f.wo_pre_cost_trim_cost_dtls_id 
	$item_group_cond
	join 
	wo_pre_cost_mst d
	on
	e.job_no =d.job_no
	where 
	a.job_no=b.job_no_mst   and 
	a.job_no=c.job_no_mst   and 
	b.id=c.po_break_down_id and 
	a.company_name=1        and 
	a.is_deleted=0          and 
	a.status_active=1       and 
	b.is_deleted=0          and 
	b.status_active=1       and 
	c.is_deleted=0          and 
	c.status_active=1       and 
	a.company_name=$company_name $buyer_id_cond $date_cond $style_ref_cond $jobcond 
	group by a.buyer_name,a.job_no,a.job_no_prefix_num,style_ref_no, b.id,b.po_number,a.order_uom,a.total_set_qnty,b.pub_shipment_date,d.costing_per,
	e.id,
	e.trim_group,
	e.description,
	e.brand_sup_ref,
	e.cons_uom,
	e.cons_dzn_gmts,
	e.rate,
	e.amount,
	e.apvl_req,
	e.nominated_supp,
	e.insert_date,
	f.cons,
	f.pcs,
	f.country_id
	order by b.id, e.trim_group
	");
				$tot_rows=count($sql_query);
				$i=1;
				foreach($sql_query as $row)
				{
					       if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						   
						   $dzn_qnty=0;
							if($row[csf('costing_per')]==1)
							{
								$dzn_qnty=12;
							}
							else if($row[csf('costing_per')]==3)
							{
								$dzn_qnty=12*2;
							}
							else if($row[csf('costing_per')]==4)
							{
								$dzn_qnty=12*3;
							}
							else if($row[csf('costing_per')]==5)
							{
								$dzn_qnty=12*4;
							}
							else
							{
								$dzn_qnty=1;
							}
							 $po_qty=0;
							 if($row[csf('country_id')]==0)
							 {
								$po_qty=$row[csf('order_quantity_set')];
							 }
							 else
							 {
								$country_id= explode(",",$row[csf('country_id')]);
								for($cou=0;$cou<=count($country_id); $cou++)
								{
								$po_qty+=$sql_po_qty_country_wise_arr[$row[csf('id')]][$country_id[$cou]];
								}
							 }
							 
							 $req_qnty=($row[csf('cons_cal')]/$dzn_qnty)*$po_qty;
							 $req_value= $row[csf('rate')]*$req_qnty;
							 $booking_no=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['booking_no'];
							 
							 
							 $po_data_arr[$row[csf('id')]][buyer_name]=$row[csf('buyer_name')];
							 $po_data_arr[$row[csf('id')]][job_no_prefix_num]=$row[csf('job_no_prefix_num')];
							 $po_data_arr[$row[csf('id')]][style_ref_no]=$row[csf('style_ref_no')];
							 $po_data_arr[$row[csf('id')]][po_number]=$row[csf('po_number')];
							 $po_data_arr[$row[csf('id')]][order_quantity_set]=$row[csf('order_quantity_set')];
							 $po_data_arr[$row[csf('id')]][order_uom]=$row[csf('order_uom')];
							 $po_data_arr[$row[csf('id')]][order_quantity]=$row[csf('order_quantity')];
							 $po_data_arr[$row[csf('id')]][pub_shipment_date]=$row[csf('pub_shipment_date')];
					         $po_id_string.=$row[csf('id')].",";
					?>
					<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
						<td width="30" title="<? echo $po_qty; ?>"><p><? echo $i; ?>&nbsp;</p></td>
						<td width="50"><p><? echo $buyer_short_name_library[$row[csf('buyer_name')]]; ?>&nbsp;</p></td>
						<td width="100" align="center"><p><? echo $row[csf('job_no_prefix_num')]; ?>&nbsp;</p></td>
						<td width="100"><p><? echo $row[csf('style_ref_no')]; ?>&nbsp;</p></td>
						<td width="90"><p><a href='#report_details' onclick="generate_report('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','preCostRpt');"><? echo $row[csf('po_number')]; ?></a>&nbsp;</p></td>
						<td width="80" align="right"><p><a href='#report_details' onclick="order_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>' ,'order_qty_data');"><? echo number_format($row[csf('order_quantity_set')],0,'.',''); ?></a>&nbsp;</p></td>
						<td width="50" align="center"><p><? echo $unit_of_measurement[$row[csf('order_uom')]]; ?>&nbsp;</p></td>
						<td width="80" align="right"><p><? echo number_format($row[csf('order_quantity')],0,'.',''); ?>&nbsp;</p></td>
						<td width="80" align="center"><p><? echo change_date_format($row[csf('pub_shipment_date')]); ?>&nbsp;</p></td>
					
								<td width="100">
									<p>
										<? echo $item_library[$row[csf('trim_group')]]; ?>
									&nbsp;</p>
								</td>
								<td width="100">
									<p>
										<? echo $row[csf('brand_sup_ref')]; ?>
									&nbsp;</p>
								</td>
								<td width="60" align="center"><p><? if($row[csf('apvl_req')]==1) echo "Yes"; else echo "&nbsp;"; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><? echo $approved_status; ?>&nbsp;</p></td>
							
                                <td width="100" align="right"><p>
								<? 
								$insert_date=explode(" ",$row[csf('insert_date')]);
								echo change_date_format($insert_date[0],'','','');//echo change_date_format($row[csf('pre_date')],'','',1); 
								?>&nbsp;</p></td>
								<td width="100" align="right"><p><a href='#report_details' onclick="order_req_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('rate')]; ?>','<? echo $row[csf('trim_group')];?>' ,'<? echo $booking_no;?>','<? echo $row[csf('description')];?>','<? echo $row[csf('country_id')] ?>','<? echo $row[csf('trim_dtla_id')] ?>','order_req_qty_data');"><? echo number_format($req_qnty,2,'.',''); ?></a>&nbsp;</p></td>
                                
								<td width="100" align="right"><p><? echo number_format($req_value,2); ?>&nbsp;</p></td>
                                <?
							    $conversion_factor_rate=$conversion_factor_array[$row[csf('trim_group')]]['con_factor'];
								if(($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate) > $req_qnty)
								{
									$color_wo="red";	
								}
								
								else if(($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate) < $req_qnty )
								{
									$color_wo="yellow";		
								}
								
								else 
								{
								$color_wo="";	
								}
								
								$booking_no_arr=array_unique(explode(',',$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['booking_no']));
								//$booking_no_arr_d=implode(',',$booking_no_arr);
								//print $order_id.'='.	$trim_id;// $wo_qty_array[$order_id][$trim_id]['booking_no'];
								$main_booking_no_large_data="";
								foreach($booking_no_arr as $booking_no1)
								{	
									//if($booking_no1>0)
									//{
									if($main_booking_no_large_data=="") $main_booking_no_large_data=$booking_no1; else $main_booking_no_large_data.=",".$booking_no1;
									//}
									//print($main_booking_no_large_data);
								}
								?>
								<td width="90" align="right" title="<? echo 'conversion_factor='.$conversion_factor_rate;?>" bgcolor="<? echo $color_wo;?>"><p><a href='#report_details' onclick="openmypage('<? echo $row[csf('id')]; ?>','<? echo $row[csf('trim_group')]; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $main_booking_no_large_data;?>','<? echo $row[csf('trim_dtla_id')];?>','booking_info');">
								<? 
								//$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']
								 echo number_format($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate,2,'.','')
								?>
                                </a>&nbsp;</p></td>
                                <td width="60" align="center"><p><? echo $unit_of_measurement[$row[csf('cons_uom')]]; ?>&nbsp;</p></td>
                                <td width="100" align="right" title="<? echo number_format($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['rate'],2,'.',''); ?>"><p><?  echo number_format($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['amount'],2,'.',''); ?>&nbsp;</p></td>
                                <td width="70" align="right" title="<? //echo change_date_format($wo_day);?>"><p>
                                 <?
								$tot=change_date_format($insert_date[0]);
								if($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate<=0 )
								{
								 $daysOnHand = datediff('d',$tot,$today);
								}
								else
								{
									$wo_date=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_date'];
									$wo_date=change_date_format($wo_date);
									$daysOnHand = datediff('d',$tot,$wo_date);;
								}
								 echo $daysOnHand; 
								?>&nbsp;</p></td>
                                <?
								$inhouse_qnty=$receive_qty_array[$row[csf('id')]][$row[csf('trim_group')]]['receive_qty'];
								$balance=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate-$inhouse_qnty;
								
								
								$issue_qnty=$issue_qty_array[$row[csf('id')]][$row[csf('trim_group')]]['issue_qty'];
								$left_overqty=$inhouse_qnty-$issue_qnty;
								?>
                                
								<td width="90" align="right"><p><a href='#report_details' onclick="openmypage_inhouse('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_inhouse_info');"><? echo number_format($inhouse_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td width="90" align="right"><p><? echo number_format($balance,2,'.',''); ?>&nbsp;</p></td>
								<td width="90" align="right"><p><a href='#report_details' onclick="openmypage_issue('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_issue_info');"><? echo number_format($issue_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td align="right"><p><? echo number_format($left_overqty,2,'.',''); ?>&nbsp;</p></td>
							</tr>
							
					<?
					
						$total_wo_qnty+=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate;
						$total_wo_value+=$wo_qty_array[$order_id][$trim_id]['amount'];
						$total_in_qnty+=$inhouse_qnty;
						//$rec_bal=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']-$inhouse_qnty;
						$total_rec_bal_qnty+=$balance;
						
						$total_issue_qnty+=$issue_qnty;
						$total_leftover_qnty+=$left_overqty;
							
							
					    $item_array[$row[csf('trim_group')]]['req']+=$req_qnty;
						$item_array[$row[csf('trim_group')]]['wo']+=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate;
						$item_array[$row[csf('trim_group')]]['in']+=$inhouse_qnty;
						$item_array[$row[csf('trim_group')]]['issue']+=$issue_qnty;
						$item_array[$row[csf('trim_group')]]['leftover']+=$left_overqty;
				$i++;
				}
				?>
                 <?
				          $po_id_string=rtrim($po_id_string,",");
				
							if($db_type==2)
							{
							  $wo_sql_without_precost=sql_select("select min(a.booking_date) as booking_date ,b.job_no,LISTAGG(CAST(a.booking_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id,b.uom, sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b where a.item_category=4 and a.booking_no=b.booking_no and item_from_precost=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name and b.po_break_down_id in($po_id_string) group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id,b.uom");
							}
							else if($db_type==0)
							{
							$wo_sql_without_precost=sql_select("select min(a.booking_date) as booking_date ,b.job_no,group_concat(a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id,b.uom,sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b where a.item_category=4 and a.booking_no=b.booking_no and item_from_precost=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name and b.po_break_down_id in($po_id_string)  group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id,b.uom");
							}
							foreach($wo_sql_without_precost as $wo_row_without_precost)
							{
								
								//$po_data_arr[$row[csf('id')]][buyer_name]=$row[csf('buyer_name')];
							 //$po_data_arr[$row[csf('id')]][job_no_prefix_num]=$row[csf('job_no_prefix_num')];
							 //$po_data_arr[$row[csf('id')]][style_ref_no]=$row[csf('style_ref_no')];
							 //$po_data_arr[$row[csf('id')]][po_number]=$row[csf('po_number')];
							 //$po_data_arr[$row[csf('id')]][order_quantity_set]=$row[csf('order_quantity_set')];
							 //$po_data_arr[$row[csf('id')]][order_uom]=$row[csf('order_uom')];
							 //$po_data_arr[$row[csf('id')]][order_quantity]=$row[csf('order_quantity')];
							 //$po_data_arr[$row[csf('id')]][pub_shipment_date]=$row[csf('pub_shipment_date')];
								
							?>
                        <tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
						<td width="30" title="<? echo $po_qty; ?>"><p><? echo $i; ?>&nbsp;</p></td>
                        
						<td width="50"><p><? echo $buyer_short_name_library[$po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][buyer_name]]; ?>&nbsp;</p></td>
                        
						<td width="100" align="center"><p><? echo $po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][job_no_prefix_num]; ?>&nbsp;</p></td>
                        
						<td width="100"><p><? echo $po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][style_ref_no]; ?>&nbsp;</p></td>
                        
						<td width="90"><p><a href='#report_details' onclick="generate_report('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','preCostRpt');"><? echo $row[csf('po_number')]; ?></a>&nbsp;</p></td>
                        
						<td width="80" align="right"><p><a href='#report_details' onclick="order_qty_popup('<? echo $company_name; ?>','<? echo $wo_row_without_precost[csf('job_no')]; ?>','<? echo $wo_row_without_precost[csf('po_break_down_id')]; ?>', '<? echo $po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][buyer_name]; ?>' ,'order_qty_data');"><? echo number_format( $po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][order_quantity_set],0,'.',''); ?></a>&nbsp;</p></td>
                        
						<td width="50" align="center"><p><? echo $unit_of_measurement[$po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][order_uom]]; ?>&nbsp;</p></td>
						<td width="80" align="right"><p><? echo number_format($po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][order_quantity],0,'.',''); ?>&nbsp;</p></td>
						<td width="80" align="center"><p><? echo change_date_format($po_data_arr[$wo_row_without_precost[csf('po_break_down_id')]][pub_shipment_date]); ?>&nbsp;</p></td>
					
								<td width="100">
									<p>
										<? echo $item_library[$wo_row_without_precost[csf('trim_group')]]; ?>
									&nbsp;</p>
								</td>
								<td width="100">
									<p>
										<? //echo $row[csf('brand_sup_ref')]; ?>
									&nbsp;</p>
								</td>
								<td width="60" align="center"><p><? if($row[csf('apvl_req')]==1) echo "Yes"; else echo "&nbsp;"; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><? //echo $approved_status; ?>&nbsp;</p></td>
							
                                <td width="100" align="right"><p>
								<? 
								$insert_date=explode(" ",$row[csf('insert_date')]);
								//echo change_date_format($insert_date[0],'','','');//echo change_date_format($row[csf('pre_date')],'','',1); 
								?>&nbsp;</p></td>
								<td width="100" align="right"><p><a href='#report_details' onclick="order_req_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('rate')]; ?>','<? echo $row[csf('trim_group')];?>' ,'<? echo $booking_no;?>','<? echo $row[csf('description')];?>','<? echo $row[csf('country_id')] ?>','<? echo $row[csf('trim_dtla_id')] ?>','order_req_qty_data');"><? //echo number_format($req_qnty,2,'.',''); ?></a>&nbsp;</p></td>
                                
								<td width="100" align="right"><p><? // echo number_format($req_value,2); ?>&nbsp;</p></td>
                                <?
							    $conversion_factor_rate=$conversion_factor_array[$wo_row_without_precost[csf('trim_group')]]['con_factor'];
								if(($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate) > $req_qnty)
								{
									$color_wo="red";	
								}
								
								else if(($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate) < $req_qnty )
								{
									$color_wo="yellow";		
								}
								
								else 
								{
								$color_wo="";	
								}
								
								$booking_no_arr=array_unique(explode(',',$wo_row_without_precost[csf('booking_no')]));
								//$booking_no_arr_d=implode(',',$booking_no_arr);
								//print $order_id.'='.	$trim_id;// $wo_qty_array[$order_id][$trim_id]['booking_no'];
								$main_booking_no_large_data="";
								foreach($booking_no_arr as $booking_no1)
								{	
									//if($booking_no1>0)
									//{
									if($main_booking_no_large_data=="") $main_booking_no_large_data=$booking_no1; else $main_booking_no_large_data.=",".$booking_no1;
									//}
									//print($main_booking_no_large_data);
								}
								?>
								<td width="90" align="right" title="<? echo 'conversion_factor='.$conversion_factor_rate;?>" bgcolor="<? echo $color_wo;?>"><p><a href='#report_details' onclick="openmypage('<? echo $wo_row_without_precost[csf('po_break_down_id')]; ?>','<? echo $wo_row_without_precost[csf('trim_group')]; ?>','<? echo $wo_row_without_precost[csf('job_no')]; ?>','<? echo $main_booking_no_large_data;?>','<? echo $wo_row_without_precost[csf('pre_cost_fabric_cost_dtls_id')];?>','booking_info');">
								<? 
								//$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']
								 //echo number_format($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate,2,'.','')
								 echo $wo_row_without_precost[csf('wo_qnty')]*$conversion_factor_rate;
								?>
                                </a>&nbsp;</p></td>
                                <td width="60" align="center"><p><? echo $unit_of_measurement[$wo_row_without_precost[csf('uom')]]; ?>&nbsp;</p></td>
                                
                                <td width="100" align="right" title="<? echo number_format($wo_row_without_precost[csf('rate')],2,'.',''); ?>"><p><?  echo number_format($wo_row_without_precost[csf('amount')],2,'.',''); ?>&nbsp;</p></td>
                                <td width="70" align="right" title="<? //echo change_date_format($wo_day);?>"><p>
                                 <?
								/*$tot=change_date_format($insert_date[0]);
								if($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate<=0 )
								{
								 $daysOnHand = datediff('d',$tot,$today);
								}
								else
								{
									$wo_date=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_date'];
									$wo_date=change_date_format($wo_date);
									$daysOnHand = datediff('d',$tot,$wo_date);;
								}
								 echo $daysOnHand; */
								?>&nbsp;</p></td>
                                <?
								$inhouse_qnty=$receive_qty_array[$wo_row_without_precost[csf('po_break_down_id')]][$wo_row_without_precost[csf('trim_group')]]['receive_qty'];
								$balance=$wo_row_without_precost[csf('wo_qnty')]*$conversion_factor_rate-$inhouse_qnty;
								
								
								$issue_qnty=$issue_qty_array[$wo_row_without_precost[csf('po_break_down_id')]][$row[csf('trim_group')]]['issue_qty'];
								$left_overqty=$inhouse_qnty-$issue_qnty;
								?>
                                
								<td width="90" align="right"><p><a href='#report_details' onclick="openmypage_inhouse('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_inhouse_info');"><? echo number_format($inhouse_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td width="90" align="right"><p><? echo number_format($balance,2,'.',''); ?>&nbsp;</p></td>
								<td width="90" align="right"><p><a href='#report_details' onclick="openmypage_issue('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_issue_info');"><? echo number_format($issue_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td align="right"><p><? echo number_format($left_overqty,2,'.',''); ?>&nbsp;</p></td>
							</tr>
							
                            
                            <?
							
						$total_wo_qnty+=$wo_row_without_precost[csf('wo_qnty')]*$conversion_factor_rate;
						$total_wo_value+=$wo_row_without_precost[csf('amount')];
						$total_in_qnty+=$inhouse_qnty;
						//$rec_bal=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']-$inhouse_qnty;
						$total_rec_bal_qnty+=$balance;
						
						$total_issue_qnty+=$issue_qnty;
						$total_leftover_qnty+=$left_overqty;
							
							
					   // $item_array[$row[csf('trim_group')]]['req']+=$req_qnty;
						$item_array[$wo_row_without_precost[csf('trim_group')]]['wo']+=$wo_row_without_precost[csf('wo_qnty')]*$conversion_factor_rate;
						$item_array[$wo_row_without_precost[csf('trim_group')]]['in']+=$inhouse_qnty;
						$item_array[$wo_row_without_precost[csf('trim_group')]]['issue']+=$issue_qnty;
						$item_array[$wo_row_without_precost[csf('trim_group')]]['leftover']+=$left_overqty;
							 $i++;
							}
							?>
				</table>
				<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all">
					<tfoot>
						<th width="30"></th>
						<th width="50"></th>
						<th width="100"></th>
						<th width="100"></th>
						<th width="90"></th>
						<th width="80" align="right" id="total_order_qnty"><? //echo number_format($total_order_qnty,0); ?></th>
						<th width="50"></th>
						<th width="80" align="right" id="total_order_qnty_in_pcs"><? //echo number_format($total_order_qnty_in_pcs,0); ?></th>
						<th width="80"></th>
						<th width="100"></th>
						<th width="100"></th>
						<th width="60"></th>
						<th width="80"></th>
						<th width="100"></th>
						<th width="100" align="right" id="value_req_qnty"><? //echo number_format($total_req_qnty,2); ?></th>
						<th width="100" align="right" id="value_pre_costing"><? echo number_format($total_pre_costing_value,2); ?></th>
						<th width="90" align="right" id="value_wo_qty"><? //echo number_format($total_wo_qnty,2); ?></th>
                        <th width="60" align="right" ></th>
                        <th width="100" align="right" id=""><? echo number_format($total_wo_value,2); ?></th>
                        <th width="70" align="right"><p><? //echo number_format($req_value,2,'.',''); ?>&nbsp;</p></th>
                        <th width="90" align="right" id="value_in_qty"><? //echo number_format($total_in_qnty,2); ?></th>
						<th width="90" align="right" id="value_rec_qty"><? //echo number_format($total_rec_bal_qnty,2); ?></th>
						<th width="90" align="right" id="value_issue_qty"><? //echo number_format($total_issue_qnty,2); ?></th>
						<th align="right" id="value_leftover_qty"><? //echo number_format($total_leftover_qnty,2); ?></th>
					</tfoot>
				</table>
				</div>
				<table>
					<tr><td height="15"></td></tr>
				</table>
				<u><b>Summary</b></u>
				<table class="rpt_table" width="1200" cellpadding="0" cellspacing="0" border="1" rules="all">
					<thead>
						<th width="30">SL</th>
						<th width="110">Item</th>
						<th width="60">UOM</th>
						<th width="80">Approved %</th>
						<th width="110">Req Qty</th>
						<th width="110">WO Qty</th>
						<th width="80">WO %</th>
						<th width="110">In-House Qty</th>
						<th width="80">In-House %</th>
						<th width="110">In-House Balance Qty</th>
						<th width="110">Issue Qty</th>
						<th width="80">Issue %</th>
						<th>Left Over</th>
					</thead>
					<?
					$z=1; $tot_req_qnty_summary=0;
					foreach($item_array as $key=>$value)
					{
						if($z%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$tot_req_qnty_summary+=$value['req'];
						$tot_wo_qnty_summary+=$value['wo'];
						$tot_in_qnty_summary+=$value['in'];
						$tot_issue_qnty_summary+=$value['issue'];
						$tot_leftover_qnty_summary+=$value['leftover'];
					?>
						<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr2_<? echo $z; ?>','<? echo $bgcolor;?>')" id="tr2_<? echo $z; ?>">
							<td width="30"><? echo $z; ?></td>
							<td width="110"><p><? echo $item_library[$key]; ?></p></td>
							<td width="60" align="center"><? echo $uom_array[$key]; ?></td>
							<td width="80" align="right"><? $app_perc=($item_app_array[$key]['app']*100)/$item_app_array[$key]['all']; echo number_format($app_perc,2); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['req'],2); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['wo'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $wo_per=$value['wo']/$value['req']*100; echo number_format($wo_per,2).'%'; ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['in'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $in_per=$value['in']/$value['wo']*100; echo number_format($in_per,2).'%'; ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['wo']-$value['in'],2); $in_house_bal+=($value['wo']-$value['in']); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['issue'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $wo_per=$value['issue']/$value['wo']*100; echo number_format($wo_per,2).'%'; ?>&nbsp;</td>
							<td align="right"><? echo number_format($value['leftover'],2); ?>&nbsp;</td>
						</tr>
					<?	
					$z++;
					}
					?>
					<tfoot>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_req_qnty_summary,2); ?>&nbsp;</th>
						<th align="right"><? echo number_format($tot_wo_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_in_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($in_house_bal,2); ?>&nbsp;</th>
						<th align="right"><? echo number_format($tot_issue_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_leftover_qnty_summary,2); ?>&nbsp;</th>
					</tfoot>   	
				</table>
			</fieldset>
		</div>
	<?
	}
	}
	
	
	
	
	
	
	
//===========================================================================================================================================================

  if(str_replace("'","",$cbo_search_by)==2)
  {
	if($template==1)
	{
		
		ob_start();
	?>
		<div style="width:1780px">
		<fieldset style="width:100%;">	
			<table width="2050">
				<tr class="form_caption">
					<td colspan="24" align="center">Accessories Followup Report</td>
				</tr>
				<tr class="form_caption">
					<td colspan="24" align="center"><? echo $company_library[$company_name]; ?></td>
				</tr>
			</table>
			<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all">
				<thead>
					<th width="30">SL</th>
					<th width="50">Buyer</th>
					<th width="100">Job No</th>
					<th width="100">Style Ref</th>
					<th width="90">Order No</th>
					<th width="80">Order Qnty</th>
					<th width="50">UOM</th>
					<th width="80">Qnty (Pcs)</th>
					<th width="80">Shipment Date</th>
					<th width="100">Trims Name</th>
					<th width="100">Brand/Sup Ref</th>
					<th width="60">Appr Req.</th>
					<th width="80">Approve Status</th>
                    <th width="100">Item Entry Date</th>
					<th width="100">Req Qnty</th>
					<th width="100">Pre Costing Value</th>
					<th width="90">WO Qnty</th>
                    <th width="60">Trims UOM</th>
                    <th width="100">WO Value</th>
                    <th width="70">WO Delay Days</th>
					<th width="90">In-House Qnty</th>
					<th width="90">Receive Balance</th>
					<th width="90">Issue to Prod.</th>
					<th>Left Over/Balance</th>
				</thead>
			</table>
			<div style="width:2030px; max-height:400px; overflow-y:scroll" id="scroll_body">
				<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
	<?
	
	
	$app_sql=sql_select("select job_no_mst,accessories_type_id,approval_status from wo_po_trims_approval_info");
	$app_status_arr=array();
	foreach($app_sql as $row)
	{
		$app_status_arr[$row[csf("job_no_mst")]][$row[csf("accessories_type_id")]]=$row[csf("approval_status")];
	}
	
	$wo_qty_array=array();
	$wo_qty_summary_array=array();
	if($db_type==2)
	{
	 $wo_sql="select min(a.booking_date) as booking_date ,b.job_no,LISTAGG(CAST(a.booking_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id, sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b 
	where a.item_category=4 and a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id";
	}
	else if($db_type==0)
	{
	$wo_sql="select min(a.booking_date) as booking_date ,b.job_no,group_concat(a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id,sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b 
	where a.item_category=4 and a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id";
	}
	$dataArray=sql_select($wo_sql);
	foreach($dataArray as $row )
	{
		
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['booking_no']=$row[csf('booking_no')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['wo_qnty']=$row[csf('wo_qnty')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['rate']=$row[csf('rate')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['amount']=$row[csf('amount')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['wo_date']=$row[csf('booking_date')];
		
		$wo_qty_summary_array[$row[csf('trim_group')]]['wo_qnty']=$row[csf('wo_qnty')];
	}
	
	
	$conversion_factor_array=array();
	$conversion_factor=sql_select("select id ,conversion_factor from  lib_item_group  ");
	foreach($conversion_factor as $row_f)
	{
	$conversion_factor_array[$row_f[csf('id')]]['con_factor']=$row_f[csf('conversion_factor')];
	}
	
	$sql_po_qty_country_wise_arr=array();	
	$sql_po_qty_country_wise=sql_select("select  b.id,c.country_id, sum(c.order_quantity/a.total_set_qnty) as order_quantity_set  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst  and b.id=c.po_break_down_id and a.company_name=1  and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1  and a.company_name=$company_name $buyer_id_cond $date_cond $style_ref_cond $jobcond group by   b.id,c.country_id order by b.id,c.country_id");
	foreach( $sql_po_qty_country_wise as $sql_po_qty_country_wise_row)
	{
	$sql_po_qty_country_wise_arr[$sql_po_qty_country_wise_row[csf('id')]][$sql_po_qty_country_wise_row[csf('country_id')]]=$sql_po_qty_country_wise_row[csf('order_quantity_set')]; 
	}
 
    $receive_qty_array=array();
	$receive_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity   from  inv_receive_master c,product_details_master d,inv_trims_entry_dtls a , order_wise_pro_details b where a.mst_id=c.id and a.trans_id=b.trans_id and a.prod_id=d.id and b.trans_type=1 and b.entry_form=24 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
	foreach($receive_qty_data as $row)
	{
		$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['receive_qty']=$row[csf('quantity')];
	}
		
	$issue_qty_array=array();
	$issue_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity  from  inv_issue_master d,product_details_master p,inv_trims_issue_dtls a , order_wise_pro_details b where a.mst_id=d.id and a.prod_id=p.id and a.trans_id=b.trans_id and b.trans_type=2 and b.entry_form=25 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
	foreach($issue_qty_data as $row)
	{
		$issue_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['issue_qty']=$row[csf('quantity')];
	}
	$style_data_arr=array();
	$po_id_string="";
	$today=date("Y-m-d");
	$sql_query=sql_select("select a.buyer_name,a.job_no,a.job_no_prefix_num,style_ref_no, b.id,b.po_number,a.order_uom,sum(distinct b.po_quantity) as po_quantity,a.total_set_qnty  ,sum(distinct b.po_quantity*a.total_set_qnty) as po_quantity_psc ,sum(c.order_quantity) as order_quantity ,sum(c.order_quantity/a.total_set_qnty) as order_quantity_set,  b.pub_shipment_date,
	d.costing_per,
	e.id as trim_dtla_id,
	e.trim_group,
	e.description,
	e.brand_sup_ref,
	e.cons_uom,
	e.cons_dzn_gmts,
	e.rate,
	e.amount,
	e.apvl_req,
	e.nominated_supp,
	e.insert_date,
	f.cons,
	f.country_id,
	f.cons as cons_cal
	from 
	wo_po_details_master a, 
	wo_po_break_down b,
	wo_po_color_size_breakdown c
	left join 
	wo_pre_cost_trim_co_cons_dtls f 
	on
	c.job_no_mst=f.job_no and
	c.po_break_down_id=f.po_break_down_id
	join
	wo_pre_cost_trim_cost_dtls e
	on 
	f.job_no=e.job_no  and
	e.id=f.wo_pre_cost_trim_cost_dtls_id 
	$item_group_cond
	join 
	wo_pre_cost_mst d
	on
	e.job_no =d.job_no
	where 
	a.job_no=b.job_no_mst   and 
	a.job_no=c.job_no_mst   and 
	b.id=c.po_break_down_id and 
	a.company_name=1        and 
	a.is_deleted=0          and 
	a.status_active=1       and 
	b.is_deleted=0          and 
	b.status_active=1       and 
	c.is_deleted=0          and 
	c.status_active=1       and 
	a.company_name=$company_name $buyer_id_cond $date_cond $style_ref_cond $jobcond 
	group by a.buyer_name,a.job_no,a.job_no_prefix_num,style_ref_no, b.id,b.po_number,a.order_uom,a.total_set_qnty,b.pub_shipment_date,d.costing_per,
	e.id,
	e.trim_group,
	e.description,
	e.brand_sup_ref,
	e.cons_uom,
	e.cons_dzn_gmts,
	e.rate,
	e.amount,
	e.apvl_req,
	e.nominated_supp,
	e.insert_date,
	f.cons,
	f.pcs,
	f.country_id
	order by b.id, e.trim_group
	");
				$tot_rows=count($sql_query);
				$i=1;
				foreach($sql_query as $row)
				{
					      
						   
						   $dzn_qnty=0;
							if($row[csf('costing_per')]==1)
							{
								$dzn_qnty=12;
							}
							else if($row[csf('costing_per')]==3)
							{
								$dzn_qnty=12*2;
							}
							else if($row[csf('costing_per')]==4)
							{
								$dzn_qnty=12*3;
							}
							else if($row[csf('costing_per')]==5)
							{
								$dzn_qnty=12*4;
							}
							else
							{
								$dzn_qnty=1;
							}
							 $po_qty=0;
							 if($row[csf('country_id')]==0)
							 {
								$po_qty=$row[csf('order_quantity_set')];
							 }
							 else
							 {
								$country_id= explode(",",$row[csf('country_id')]);
								for($cou=0;$cou<=count($country_id); $cou++)
								{
								$po_qty+=$sql_po_qty_country_wise_arr[$row[csf('id')]][$country_id[$cou]];
								}
							 }
							 
							 $req_qnty=($row[csf('cons_cal')]/$dzn_qnty)*$po_qty;
							 $req_value= $row[csf('rate')]*$req_qnty;
							 $booking_no=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['booking_no'];
							 
							 
							$conversion_factor_rate=$conversion_factor_array[$row[csf('trim_group')]]['con_factor'];
							$wo_qnty=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate;
							
							$amount=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['amount'];
							$wo_date=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_date'];
							
							$receive_qty=$receive_qty_array[$row[csf('id')]][$row[csf('trim_group')]]['receive_qty'];
							
							$issue_qty=$issue_qty_array[$row[csf('id')]][$row[csf('trim_group')]]['issue_qty'];
							 
							 $style_data_arr[$row[csf('job_no')]][job_no]=$row[csf('job_no')];
							 $style_data_arr[$row[csf('job_no')]][buyer_name]=$row[csf('buyer_name')];
							 $style_data_arr[$row[csf('job_no')]][job_no_prefix_num]=$row[csf('job_no_prefix_num')];
							 $style_data_arr[$row[csf('job_no')]][style_ref_no]=$row[csf('style_ref_no')];
							 $style_data_arr[$row[csf('job_no')]][order_uom]=$row[csf('order_uom')];
							 
							 $style_data_arr[$row[csf('job_no')]][po_id][$row[csf('id')]]=$row[csf('id')];
							 $style_data_arr[$row[csf('job_no')]][po_number][$row[csf('id')]]=$row[csf('po_number')];
							 $style_data_arr[$row[csf('job_no')]][order_quantity_set][$row[csf('id')]]=$row[csf('order_quantity_set')];
							 
							 $style_data_arr[$row[csf('job_no')]][order_quantity][$row[csf('id')]]=$row[csf('order_quantity')];
							 $style_data_arr[$row[csf('job_no')]][pub_shipment_date][$row[csf('id')]]=change_date_format($row[csf('pub_shipment_date')]);
							 
							 $style_data_arr[$row[csf('job_no')]][trim_group][$row[csf('trim_group')]]=$row[csf('trim_group')];
							 
							 
							 
							 $po_id_string.=$row[csf('id')].",";
							 
							/* $style_data_arr[$row[csf('job_no')]][brand_sup_ref][$row[csf('trim_group')]]=$row[csf('brand_sup_ref')];
							 $style_data_arr[$row[csf('job_no')]][apvl_req][$row[csf('trim_group')]]=$row[csf('apvl_req')];
							 $style_data_arr[$row[csf('job_no')]][insert_date][$row[csf('trim_group')]]=$row[csf('insert_date')];*/
							 
							 
							 
							 $style_data_arr[$row[csf('job_no')]][trim_dtla_id][$row[csf('trim_dtla_id')]]=$row[csf('trim_dtla_id')];
							 $style_data_arr[$row[csf('job_no')]][trim_group][$row[csf('trim_dtla_id')]]=$row[csf('trim_group')];
							 $style_data_arr[$row[csf('job_no')]][brand_sup_ref][$row[csf('trim_dtla_id')]]=$row[csf('brand_sup_ref')];
							 $style_data_arr[$row[csf('job_no')]][apvl_req][$row[csf('trim_dtla_id')]]=$row[csf('apvl_req')];
							 $style_data_arr[$row[csf('job_no')]][insert_date][$row[csf('trim_dtla_id')]]=$row[csf('insert_date')];
							 
							 $style_data_arr[$row[csf('job_no')]][req_qnty][$row[csf('trim_dtla_id')]]+=$req_qnty;
							 $style_data_arr[$row[csf('job_no')]][req_value][$row[csf('trim_dtla_id')]]+=$req_value;
							 $style_data_arr[$row[csf('job_no')]][wo_qnty][$row[csf('trim_dtla_id')]]+=$wo_qnty;
							 $style_data_arr[$row[csf('job_no')]][cons_uom][$row[csf('trim_dtla_id')]]=$row[csf('cons_uom')];
							 $style_data_arr[$row[csf('job_no')]][amount][$row[csf('trim_dtla_id')]]+=$amount;
							 
							 $style_data_arr[$row[csf('job_no')]][wo_date][$row[csf('trim_dtla_id')]]=$wo_date;
							 
							 $style_data_arr[$row[csf('job_no')]][wo_qnty_trim_group][$row[csf('trim_group')]]+=$wo_qnty;
							 $style_data_arr[$row[csf('job_no')]][inhouse_qnty][$row[csf('trim_group')]]+=$receive_qty;
							 $style_data_arr[$row[csf('job_no')]][issue_qty][$row[csf('trim_group')]]+=$issue_qty;
							 
							 
							 
							 $style_data_arr[$row[csf('job_no')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]=$row[csf('trim_dtla_id')];

				}
				
				
				$po_id_string=rtrim($po_id_string,",");
				
				if($db_type==2)
				{
				  $wo_sql_without_precost=sql_select("select min(a.booking_date) as booking_date ,b.job_no,LISTAGG(CAST(a.booking_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id,b.uom, sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b where a.item_category=4 and a.booking_no=b.booking_no and item_from_precost=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name and b.po_break_down_id in($po_id_string) group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id,b.uom");
				}
				else if($db_type==0)
				{
				$wo_sql_without_precost=sql_select("select min(a.booking_date) as booking_date ,b.job_no,group_concat(a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id,b.uom,sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b where a.item_category=4 and a.booking_no=b.booking_no and item_from_precost=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name and b.po_break_down_id in($po_id_string)  group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id,b.uom");
				}
				//$style_data_arr1=array();
				foreach($wo_sql_without_precost as $wo_row_without_precost)
				{
					$style_data_arr[$wo_row_without_precost[csf('job_no')]][trim_group][$wo_row_without_precost[csf('trim_group')]]=$wo_row_without_precost[csf('trim_group')];
				}
				
				
				
							
							
				$i=1;
				foreach($style_data_arr as $key=>$value)
				{
					
					 $rowspan=count($value[trim_dtla_id]);
					
							 
					  if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";     
					?>
					<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
						<td width="30" title="<? echo $po_qty; ?>" rowspan="<? echo $rowspan; ?>"><p><? echo $i; ?>&nbsp;</p></td>
						<td width="50" rowspan="<? echo $rowspan; ?>"><p><? echo $buyer_short_name_library[$value[buyer_name]]; ?>&nbsp;</p></td>
						<td width="100" align="center" rowspan="<? echo $rowspan; ?>"><p><? echo $value[job_no_prefix_num]; ?>&nbsp;</p></td>
						<td width="100" rowspan="<? echo $rowspan; ?>"><p><? echo $value[style_ref_no]; ?>&nbsp;</p></td>
						<td width="90" rowspan="<? echo $rowspan; ?>">
                        <p>
                        <a href='#report_details' onclick="generate_report('<? echo $company_name; ?>','<? echo $value[job_no]; ?>','preCostRpt');">
						<? 
						$po_number=implode(",", $value[po_number]);
						echo $po_number; 
						?>
                        </a>&nbsp;
                        </p>
                        </td>
						<td width="80" align="right" rowspan="<? echo $rowspan; ?>">
                        <p>
                        <a href='#report_details' onclick="order_qty_popup('<? echo $company_name; ?>','<? echo $value[job_no]; ?>','<? echo $value[po_id]; ?>', '<? echo $value[buyer_name]; ?>' ,'order_qty_data');"><? echo number_format(array_sum($value[order_quantity_set]),0,'.',''); ?>
                        </a>
                        &nbsp;
                        </p>
                        </td>
                        
						<td width="50" align="center" rowspan="<? echo $rowspan; ?>"><p><? echo $unit_of_measurement[$row[csf('order_uom')]]; ?>&nbsp;</p></td>
						<td width="80" align="right" rowspan="<? echo $rowspan; ?>"><p><? echo number_format(array_sum($value[order_quantity]),0,'.',''); ?>&nbsp;</p></td>
						<td width="80" align="center" rowspan="<? echo $rowspan; ?>">
                        <p>
						<? 
						$pub_shipment_date=implode(",", $value[pub_shipment_date]);
						echo $pub_shipment_date; 
						?>
                        &nbsp;
                        </p>
                        </td>
					<?
					//$rowspannn=0;
					foreach($value[trim_group] as $key_trim=>$value_trim)
				     {
					 $gg=1;	
					 foreach($value[$key_trim] as $key_trim1=>$value_trim1)
				     { 
					// for($gg=1;$gg<=count($value[$key_trim]); $gg++)
					// {
						 $rowspannn=count($value[$key_trim]);
						 if($gg==1)
						 {
					      
						
					?>
								<td width="100">
									<p>
										<? echo $item_library[$value[trim_group][$key_trim1]]; ?>
									&nbsp;</p>
								</td>
								<td width="100">
									<p>
										<?
										echo $value[brand_sup_ref][$key_trim1];
										//echo $row[csf('brand_sup_ref')]; 
										?>
									&nbsp;</p>
								</td>
								<td width="60" align="center">
                                <p>
								<? 
								 
								if($value[apvl_req][$key_trim1]==1) echo "Yes"; else echo "&nbsp;"; 
								?>
                                &nbsp;
                                </p>
                                </td>
								<td width="80" align="center">
                                <p>
								<? 
								if($value[apvl_req][$key_trim1]==1)
								{
								$app_status=$app_status_arr[$value[job_no]][$key_trim1];
							    $approved_status=$approval_status[$app_status];
								}
								else
								{
								$approved_status="";	
								}
								echo $approved_status; 
								?>
                                &nbsp;
                                </p>
                                </td>
							
                                <td width="100" align="right"><p>
								<? 
								$insert_date=explode(" ",$value[insert_date][$key_trim1]);
								echo change_date_format($insert_date[0],'','','');//echo change_date_format($row[csf('pre_date')],'','',1); 
								?>&nbsp;</p></td>
								<td width="100" align="right">
                                <p>
                                <a href='#report_details' onclick="order_req_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('rate')]; ?>','<? echo $row[csf('trim_group')];?>' ,'<? echo $booking_no;?>','<? echo $row[csf('description')];?>','<? echo $row[csf('country_id')] ?>','<? echo $row[csf('trim_dtla_id')] ?>','order_req_qty_data');">
								<? 
								echo number_format($value[req_qnty][$key_trim1],2,'.',''); 
								?>
                                </a>
                                &nbsp;
                                </p>
                                </td>
                                
								<td width="100" align="right"><p><? echo number_format($value[req_value][$key_trim1],2); ?>&nbsp;</p></td>
                                <?
							   // $conversion_factor_rate=$conversion_factor_array[$row[csf('trim_group')]]['con_factor'];
								if($value[wo_qnty][$key_trim1] > $req_qnty)
								{
									$color_wo="red";	
								}
								
								else if($value[wo_qnty][$key_trim1] < $req_qnty )
								{
									$color_wo="yellow";		
								}
								
								else 
								{
								$color_wo="";	
								}
								
								$booking_no_arr=array_unique(explode(',',$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['booking_no']));
								//$booking_no_arr_d=implode(',',$booking_no_arr);
								//print $order_id.'='.	$trim_id;// $wo_qty_array[$order_id][$trim_id]['booking_no'];
								$main_booking_no_large_data="";
								foreach($booking_no_arr as $booking_no1)
								{	
									//if($booking_no1>0)
									//{
									if($main_booking_no_large_data=="") $main_booking_no_large_data=$booking_no1; else $main_booking_no_large_data.=",".$booking_no1;
									//}
									//print($main_booking_no_large_data);
								}
								?>
								<td width="90" align="right" title="<? echo 'conversion_factor='.$conversion_factor_rate;?>" bgcolor="<? echo $color_wo;?>"><p><a href='#report_details' onclick="openmypage('<? echo $row[csf('id')]; ?>','<? echo $row[csf('trim_group')]; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $main_booking_no_large_data;?>','<? echo $row[csf('trim_dtla_id')];?>','booking_info');">
								<? 
								//$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']
								 echo number_format($value[wo_qnty][$key_trim1],2,'.','')
								?>
                                </a>&nbsp;</p></td>
                                <td width="60" align="center"><p><? echo $unit_of_measurement[$value[cons_uom][$key_trim1]]; ?>&nbsp;</p></td>
                                <td width="100" align="right" title="<? echo number_format($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['rate'],2,'.',''); ?>">
                                <p>
								<?  
								echo number_format($value[amount][$key_trim1],2,'.',''); 
								?>
                                &nbsp;
                                </p>
                                </td>
                                <td width="70" align="right" title="<? echo change_date_format($value[wo_date][$key_trim1]);?>"><p>
                                 <?
								 
								$tot=change_date_format($insert_date[0]);
								if($value[wo_qnty][$key_trim1]<=0 )
								{
								 $daysOnHand = datediff('d',$tot,$today);
								}
								else
								{
									$wo_date=$value[wo_date][$key_trim1];
									$wo_date=change_date_format($wo_date);
									$daysOnHand = datediff('d',$tot,$wo_date);;
								}
								 echo $daysOnHand; 
								?>&nbsp;</p>
                                </td>
                                <?
								
								$inhouse_qnty=$value[inhouse_qnty][$key_trim];
								$balance=$value[wo_qnty_trim_group][$key_trim]-$inhouse_qnty;
								
								
								$issue_qnty=$value[issue_qty][$key_trim];
								$left_overqty=$inhouse_qnty-$issue_qnty;
								$rowspannnn=1;
								?>
                                
                                <td width="90" align="right" rowspan="<? echo $rowspannn; ?>"><p><a href='#report_details' onclick="openmypage_inhouse('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_inhouse_info');"><? echo number_format($inhouse_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td width="90" align="right" rowspan="<? echo $rowspannn; ?>"><p><? echo number_format($balance,2,'.',''); ?>&nbsp;</p></td>
								<td width="90" align="right" rowspan="<? echo $rowspannn; ?>"><p><a href='#report_details' onclick="openmypage_issue('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_issue_info');"><? echo number_format($issue_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td align="right" rowspan="<? echo $rowspannn; ?>"><p><? echo number_format($left_overqty,2,'.',''); ?>&nbsp;</p></td>
                                
                                <?
						 }
						 else
						 {
						 ?>
                                
								<td width="100">
									<p>
										<? echo $item_library[$value[trim_group][$key_trim1]]; ?>
									&nbsp;</p>
								</td>
								<td width="100">
									<p>
										<?
										echo $value[brand_sup_ref][$key_trim1];
										//echo $row[csf('brand_sup_ref')]; 
										?>
									&nbsp;</p>
								</td>
								<td width="60" align="center">
                                <p>
								<? 
								 
								if($value[apvl_req][$key_trim1]==1) echo "Yes"; else echo "&nbsp;"; 
								?>
                                &nbsp;
                                </p>
                                </td>
								<td width="80" align="center">
                                <p>
								<? 
								if($value[apvl_req][$key_trim1]==1)
								{
								$app_status=$app_status_arr[$value[job_no]][$key_trim1];
							    $approved_status=$approval_status[$app_status];
								}
								else
								{
								$approved_status="";	
								}
								echo $approved_status; 
								?>
                                &nbsp;
                                </p>
                                </td>
							
                                <td width="100" align="right"><p>
								<? 
								$insert_date=explode(" ",$value[insert_date][$key_trim1]);
								echo change_date_format($insert_date[0],'','','');//echo change_date_format($row[csf('pre_date')],'','',1); 
								?>&nbsp;</p></td>
								<td width="100" align="right">
                                <p>
                                <a href='#report_details' onclick="order_req_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('rate')]; ?>','<? echo $row[csf('trim_group')];?>' ,'<? echo $booking_no;?>','<? echo $row[csf('description')];?>','<? echo $row[csf('country_id')] ?>','<? echo $row[csf('trim_dtla_id')] ?>','order_req_qty_data');">
								<? 
								echo number_format($value[req_qnty][$key_trim1],2,'.','');  
								?>
                                </a>
                                &nbsp;
                                </p>
                                </td>
                                
								<td width="100" align="right"><p><? echo number_format($value[req_value][$key_trim1],2); ?>&nbsp;</p></td>
                                <?
								if($value[wo_qnty][$key_trim1] > $req_qnty)
								{
									$color_wo="red";	
								}
								
								else if($value[wo_qnty][$key_trim1] < $req_qnty )
								{
									$color_wo="yellow";		
								}
								
								else 
								{
								$color_wo="";	
								}
								
								$booking_no_arr=array_unique(explode(',',$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['booking_no']));
								//$booking_no_arr_d=implode(',',$booking_no_arr);
								//print $order_id.'='.	$trim_id;// $wo_qty_array[$order_id][$trim_id]['booking_no'];
								$main_booking_no_large_data="";
								foreach($booking_no_arr as $booking_no1)
								{	
									//if($booking_no1>0)
									//{
									if($main_booking_no_large_data=="") $main_booking_no_large_data=$booking_no1; else $main_booking_no_large_data.=",".$booking_no1;
									//}
									//print($main_booking_no_large_data);
								}
								?>
								<td width="90" align="right" title="<? echo 'conversion_factor='.$conversion_factor_rate;?>" bgcolor="<? echo $color_wo;?>"><p><a href='#report_details' onclick="openmypage('<? echo $row[csf('id')]; ?>','<? echo $row[csf('trim_group')]; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $main_booking_no_large_data;?>','<? echo $row[csf('trim_dtla_id')];?>','booking_info');">
								<? 
								//$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']
								 echo number_format($value[wo_qnty][$key_trim1],2,'.','')
								?>
                                </a>&nbsp;</p></td>
                                
                                <td width="60" align="center"><p><? echo $unit_of_measurement[$value[cons_uom][$key_trim1]]; ?>&nbsp;</p></td>
                                
                                <td width="100" align="right" title="<? echo number_format($wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['rate'],2,'.',''); ?>">
                                <p>
								<?  
								echo number_format($value[amount][$key_trim1],2,'.',''); 
								?>
                                &nbsp;
                                </p>
                                </td>
                                <td width="70" align="right" title="<? echo change_date_format($value[wo_date][$key_trim1]);?>"><p>
                                 <?
								$tot=change_date_format($insert_date[0]);
								if($value[wo_qnty][$key_trim1]<=0 )
								{
								 $daysOnHand = datediff('d',$tot,$today);
								}
								else
								{
									$wo_date=$value[wo_date][$key_trim1];
									$wo_date=change_date_format($wo_date);
									$daysOnHand = datediff('d',$tot,$wo_date);;
								}
								 echo $daysOnHand; 
								?>&nbsp;</p>
                                </td>
                                <?
						 }
								?>
							 </tr>
							
					<?
					
						$total_wo_qnty+=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate;
						$total_wo_value+=$wo_qty_array[$order_id][$trim_id]['amount'];
						$total_in_qnty+=$inhouse_qnty;
						//$rec_bal=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']-$inhouse_qnty;
						$total_rec_bal_qnty+=$balance;
						
						$total_issue_qnty+=$issue_qnty;
						$total_leftover_qnty+=$left_overqty;
							
							
					    $item_array[$row[csf('trim_group')]]['req']+=$req_qnty;
						$item_array[$row[csf('trim_group')]]['wo']+=$wo_qty_array[$row[csf('id')]][$row[csf('trim_group')]][$row[csf('trim_dtla_id')]]['wo_qnty']*$conversion_factor_rate;
						$item_array[$row[csf('trim_group')]]['in']+=$inhouse_qnty;
						$item_array[$row[csf('trim_group')]]['issue']+=$issue_qnty;
						$item_array[$row[csf('trim_group')]]['leftover']+=$left_overqty;
						
						$gg++;
			    }// end  foreach($value[$key_trim] as $key_trim1=>$value_trim1)
				}
				
				?>
               
                <?
				$i++;
				}
				
				?>
                 
				</table>
				<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all">
					<tfoot>
						<th width="30"></th>
						<th width="50"></th>
						<th width="100"></th>
						<th width="100"></th>
						<th width="90"></th>
						<th width="80" align="right" id="total_order_qnty"><? //echo number_format($total_order_qnty,0); ?></th>
						<th width="50"></th>
						<th width="80" align="right" id="total_order_qnty_in_pcs"><? //echo number_format($total_order_qnty_in_pcs,0); ?></th>
						<th width="80"></th>
						<th width="100"></th>
						<th width="100"></th>
						<th width="60"></th>
						<th width="80"></th>
						<th width="100"></th>
						<th width="100" align="right" id="value_req_qnty"><? //echo number_format($total_req_qnty,2); ?></th>
						<th width="100" align="right" id="value_pre_costing"><? echo number_format($total_pre_costing_value,2); ?></th>
						<th width="90" align="right" id="value_wo_qty"><? //echo number_format($total_wo_qnty,2); ?></th>
                        <th width="60" align="right" ></th>
                        <th width="100" align="right" id=""><? echo number_format($total_wo_value,2); ?></th>
                        <th width="70" align="right"><p><? //echo number_format($req_value,2,'.',''); ?>&nbsp;</p></th>
                        <th width="90" align="right" id="value_in_qty"><? //echo number_format($total_in_qnty,2); ?></th>
						<th width="90" align="right" id="value_rec_qty"><? //echo number_format($total_rec_bal_qnty,2); ?></th>
						<th width="90" align="right" id="value_issue_qty"><? //echo number_format($total_issue_qnty,2); ?></th>
						<th align="right" id="value_leftover_qty"><? //echo number_format($total_leftover_qnty,2); ?></th>
					</tfoot>
				</table>
				</div>
				<table>
					<tr><td height="15"></td></tr>
				</table>
				<u><b>Summary</b></u>
				<table class="rpt_table" width="1200" cellpadding="0" cellspacing="0" border="1" rules="all">
					<thead>
						<th width="30">SL</th>
						<th width="110">Item</th>
						<th width="60">UOM</th>
						<th width="80">Approved %</th>
						<th width="110">Req Qty</th>
						<th width="110">WO Qty</th>
						<th width="80">WO %</th>
						<th width="110">In-House Qty</th>
						<th width="80">In-House %</th>
						<th width="110">In-House Balance Qty</th>
						<th width="110">Issue Qty</th>
						<th width="80">Issue %</th>
						<th>Left Over</th>
					</thead>
					<?
					$z=1; $tot_req_qnty_summary=0;
					foreach($item_array as $key=>$value)
					{
						if($z%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$tot_req_qnty_summary+=$value['req'];
						$tot_wo_qnty_summary+=$value['wo'];
						$tot_in_qnty_summary+=$value['in'];
						$tot_issue_qnty_summary+=$value['issue'];
						$tot_leftover_qnty_summary+=$value['leftover'];
					?>
						<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr2_<? echo $z; ?>','<? echo $bgcolor;?>')" id="tr2_<? echo $z; ?>">
							<td width="30"><? echo $z; ?></td>
							<td width="110"><p><? echo $item_library[$key]; ?></p></td>
							<td width="60" align="center"><? echo $uom_array[$key]; ?></td>
							<td width="80" align="right"><? $app_perc=($item_app_array[$key]['app']*100)/$item_app_array[$key]['all']; echo number_format($app_perc,2); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['req'],2); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['wo'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $wo_per=$value['wo']/$value['req']*100; echo number_format($wo_per,2).'%'; ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['in'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $in_per=$value['in']/$value['wo']*100; echo number_format($in_per,2).'%'; ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['wo']-$value['in'],2); $in_house_bal+=($value['wo']-$value['in']); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['issue'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $wo_per=$value['issue']/$value['wo']*100; echo number_format($wo_per,2).'%'; ?>&nbsp;</td>
							<td align="right"><? echo number_format($value['leftover'],2); ?>&nbsp;</td>
						</tr>
					<?	
					$z++;
					}
					?>
					<tfoot>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_req_qnty_summary,2); ?>&nbsp;</th>
						<th align="right"><? echo number_format($tot_wo_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_in_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($in_house_bal,2); ?>&nbsp;</th>
						<th align="right"><? echo number_format($tot_issue_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_leftover_qnty_summary,2); ?>&nbsp;</th>
					</tfoot>   	
				</table>
			</fieldset>
		</div>
	<?
	}
	
}


	foreach (glob("*.xls") as $filename) {
	//if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc,ob_get_contents());
	echo "$total_data****$filename****$tot_rows";
	exit();	
}

if($action=="booking_info")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<!--<div style="width:880px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>-->
	<fieldset style="width:870px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
        <tr>
        <td align="center" colspan="8"><strong> WO  Summary</strong> </td>
         </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="100">Wo No</th>
                    <th width="75">Wo Date</th>
                     <th width="100">Country</th>
                     <th width="200">Item Description</th>
                    <th width="80">Wo Qty</th>
                    <th width="60">UOM</th>
                    <th width="100">Supplier</th>
				</thead>
                <tbody>
                <?
				
					
					$conversion_factor_array=array();
					$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
					$conversion_factor=sql_select("select id ,conversion_factor from  lib_item_group ");
					foreach($conversion_factor as $row_f)
					{
					$conversion_factor_array[$row_f[csf('id')]]['con_factor']=$row_f[csf('conversion_factor')];
					}
					
					$i=1;
					$country_arr_data=array();
					$sql_data=sql_select("select c.country_id,c.po_break_down_id,c.job_no_mst from wo_po_color_size_breakdown c  where c.po_break_down_id=$po_id and c.status_active=1 and c.is_deleted=0 group by c.country_id,c.po_break_down_id,c.job_no_mst  ");
					foreach($sql_data as $row_c)
					{
					$country_arr_data[$row_c[csf('po_break_down_id')]][$row_c[csf('job_no_mst')]]['country']=$row_c[csf('country_id')];
					}
					
					
						
					$item_description_arr=array();
					$wo_sql_trim=sql_select("select b.id,b.item_color,b.job_no, b.po_break_down_id, b.description,b.brand_supplier,b.item_size from wo_booking_dtls a, wo_trim_book_con_dtls b where a.id=b.wo_trim_booking_dtls_id and a.pre_cost_fabric_cost_dtls_id=$trim_dtla_id and a.is_deleted=0 and a.status_active=1 and a.job_no=b.job_no  group by b.id,b.po_break_down_id,b.job_no,b.description,b.brand_supplier,b.item_size,b.item_color");
					foreach($wo_sql_trim as $row_trim)
					{
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]][$trim_dtla_id]['description']=$row_trim[csf('description')];
	
					} 
					
					$boking_cond="";
					$booking_no= explode(',',$book_num);
					foreach($booking_no as $book_row)
					{
						if($boking_cond=="") $boking_cond="and a.booking_no in('$book_row'"; else  $boking_cond .=",'$book_row'";
						
					} 
					if($boking_cond!="")$boking_cond.=")";
					 $wo_sql="select a.booking_no, a.booking_date, a.supplier_id,b.job_no,b.country_id_string, b.po_break_down_id,sum(b.wo_qnty) as wo_qnty,b.uom from wo_booking_mst a, wo_booking_dtls b 
					where  a.item_category=4 and a.booking_no=b.booking_no  and a.is_deleted=0 and a.status_active=1 
					and b.status_active=1 and b.is_deleted=0 and  b.job_no='$job_no' and b.trim_group=$item_name and b.po_break_down_id='$po_id' and b.pre_cost_fabric_cost_dtls_id=$trim_dtla_id $boking_cond group by  b.po_break_down_id,b.job_no,
					a.booking_no, a.booking_date, a.supplier_id,b.uom,b.country_id_string";
					$dtlsArray=sql_select($wo_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							$description=$item_description_arr[$row[csf('po_break_down_id')]][$row[csf('job_no')]][$trim_dtla_id]['description'];
							$conversion_factor_rate=$conversion_factor_array[$item_name]['con_factor'];
							$country_arr_data=explode(',',$row[csf('country_id_string')]);
							$country_name_data="";
							foreach($country_arr_data as $country_row)
								{
									if($country_name_data=="") $country_name_data=$country_name_library[$country_row]; else $country_name_data.=",".$country_name_library[$country_row];
								}
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="100"><p><? echo $row[csf('booking_no')]; ?></p></td>
                            <td width="75"><p><? echo change_date_format($row[csf('booking_date')]); ?></p></td>
                             <td width="100"><p><? echo $country_name_data; ?></p></td>
                             <td width="200"><p><?  echo $description; ?></p></td>
                            <td width="80" align="right" title="<? echo 'conversion_factor='.$conversion_factor_rate; ?>"><p><? echo number_format($row[csf('wo_qnty')]*$conversion_factor_rate,2); ?></p></td>
                            <td width="60" align="center" ><p><? echo $unit_of_measurement[$row[csf('uom')]]; ?></p></td>
                            <td width="100"><p><? echo $supplier_arr[$row[csf('supplier_id')]]; ?></p></td>
                        </tr>
						<?
						$tot_qty+=$row[csf('wo_qnty')]*$conversion_factor_rate;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                   		 <td colspan="5" align="right">Total</td>
                    	<td  align="right"><? echo number_format($tot_qty,2); ?></td>
                        <td align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>
<?
if($action=="booking_inhouse_info")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<!--<div style="width:880px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>-->
	<fieldset style="width:870px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Prod. ID</th>
                    <th width="100">Recv. ID</th>
                    <th width="100">Chalan No</th>
                    <th width="100">Recv. Date</th>
                    <th width="80">Item Description.</th>
                    <th width="100">Recv. Qty.</th>
				</thead>
                <tbody>
                <?
					$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
					$i=1;
					//$wo_sql="select a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description,sum(a.cons_qnty) as cons_qnty  from inv_receive_master b, inv_trims_entry_dtls a where b.id=a.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4 group by a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description";
					
					//echo $receive_qty_data=("select b.po_breakdown_id,c.id as prod_id,c.item_description,d.recv_number,d.receive_date, a.item_group_id,sum(b.quantity) as quantity from  inv_receive_master d,inv_trims_entry_dtls a ,order_wise_pro_details b,product_details_master c where d.id=a.mst_id and a.trans_id=b.trans_id and b.trans_type=1 and a.item_group_id='$item_name' and b.po_breakdown_id=$po_id and b.entry_form=24 and a.prod_id=c.id and c.id=b.prod_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id,a.item_group_id,c.item_description,d.recv_number,d.receive_date, a.item_group_id,c.id");
					$receive_qty_data=("select a.id, c.po_breakdown_id,b.item_group_id,b.prod_id as prod_id,a.challan_no,b.item_description, a.recv_number, a.receive_date, SUM(c.quantity) as quantity
					from inv_receive_master a, inv_trims_entry_dtls b, order_wise_pro_details c 
					where a.id=b.mst_id  and a.entry_form=24 and  a.item_category=4  and b.id=c.dtls_id and c.trans_type=1 and  c.po_breakdown_id in($po_id)  and b.item_group_id='$item_name' and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 group by  c.po_breakdown_id,b.item_group_id,b.prod_id,a.id,b.item_description, a.recv_number,a.challan_no, a.receive_date");

					$dtlsArray=sql_select($receive_qty_data);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80"><p><? echo $row[csf('prod_id')]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('recv_number')]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('challan_no')]; ?></p></td>
                            <td width="100" align="center"><p><? echo  change_date_format($row[csf('receive_date')]); ?></p></td>
                            <td width="80" align="center"><p><? echo $row[csf('item_description')]; ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right"></td>
                        <td align="right">Total</td>
                        <td><? echo number_format($tot_qty,2); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>

<?				
if($action=="booking_issue_info")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:880px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:870px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Prod. ID</th>
                    <th width="100">Issue. ID</th>
                     <th width="100">Chalan No</th>
                     <th width="100">Issue. Date</th>
                    <th width="80">Item Description.</th>
                    <th width="100">Issue. Qty.</th>
				</thead>
                <tbody>
                <?
					$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
					$i=1;
					//$wo_sql="select a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description,sum(a.cons_qnty) as cons_qnty  from inv_receive_master b, inv_trims_entry_dtls a where b.id=a.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4 group by a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description";
					
				 $mrr_sql=("select a.id, a.issue_number,a.challan_no,b.prod_id, a.issue_date,b.item_description,SUM(c.quantity) as quantity
					from  inv_issue_master a,inv_trims_issue_dtls b, order_wise_pro_details c,product_details_master p 
					where a.id=b.mst_id  and a.entry_form=25 and p.id=b.prod_id and b.id=c.dtls_id and c.trans_type=2 and a.is_deleted=0 and a.status_active=1 and
					b.status_active=1 and b.is_deleted=0 and  c.po_breakdown_id in($po_id) and p.item_group_id='$item_name' group by c.po_breakdown_id,p.item_group_id,b.item_description,a.issue_number,a.id,a.issue_date,b.prod_id,a.challan_no ");					
					
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80" align="center"><p><? echo $row[csf('prod_id')]; ?></p></td>
                            <td width="100"><p><? echo $row[csf('issue_number')]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('challan_no')]; ?></p></td>
                            <td width="100" align="center"><p><? echo  change_date_format($row[csf('issue_date')]); ?></p></td>
                            <td width="80" align="center"><p><? echo $row[csf('item_description')]; ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right"></td>
                        <td align="right">Total</td>
                        <td><? echo number_format($tot_qty,2); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>
<?
if($action=="order_qty_data")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:780px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:770px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="750" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Buyer Name</th>
                    <th width="100">Order No</th>
                     <th width="100">Country</th>
                    <th width="80">Order Qty.</th>
                   
				</thead>
                <tbody>
                <?
					$i=1;
					
				 $gmt_item_id=return_field_value("item_number_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
				$country_id=return_field_value("country_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
					 //echo $gmt_item_id;
					 $sql_po_qty=sql_select("select sum(c.order_quantity) as order_quantity,c.country_id  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id and  b.id='".$po_id."' and c.item_number_id=' $gmt_item_id' and a.status_active=1 and b.status_active=1 and c.status_active=1 group by b.id,c.country_id ");
					list($sql_po_qty_row)=$sql_po_qty;
					$po_qty=$sql_po_qty_row[csf('order_quantity')];
					
					$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                       
					
					$sql=" select sum( c.order_quantity) as po_quantity ,c.country_id from wo_po_color_size_breakdown c  where c.po_break_down_id=$po_id and c.status_active=1 and c.is_deleted=0 group by c.country_id";					
					
					$dtlsArray=sql_select($sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80" align="center"><p><? echo $buyer_short_name_library[$buyer]; ?></p></td>
                            <td width="100"><p><? echo $order_arr[$po_id]; ?></p></td>
                             <td width="100" align="center"><p><? echo $country_name_library[$row[csf('country_id')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('po_quantity')],2); ?></p></td>
                           
                        </tr>
						<?
						$tot_qty+=$row[csf('po_quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right"></td>
                        <td align="right">Total</td>
                        <td><? echo number_format($tot_qty,2); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>
<?
if($action=="order_req_qty_data")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	
	?>
<!--	<div style="width:680px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Buyer Name</th>
                    <th width="100">Order No</th>
                     <th width="100">Item Description</th>
                     <th width="100">Country</th>
                    <th width="80">Req. Qty.</th>
                    <th width="">Req. Rate</th>
                   
				</thead>
                <tbody>
                <? 
				
					 $gmt_item_id=return_field_value("item_number_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
					 $country_id=return_field_value("country_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
					 $sql_po_qty=sql_select("select sum(c.order_quantity) as order_quantity,c.country_id  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id and  b.id='".$po_id."' and c.item_number_id=' $gmt_item_id' and a.status_active=1 and b.status_active=1 and c.status_active=1 group by b.id,c.country_id ");
					list($sql_po_qty_row)=$sql_po_qty;
					$po_qty=$sql_po_qty_row[csf('order_quantity')];
					
					
					
					$req_arr=array();
					$red_data=sql_select("select a.id,a.job_no,a.cons, a.po_break_down_id  from wo_pre_cost_trim_co_cons_dtls a , wo_pre_cost_trim_cost_dtls b where b.id=a.wo_pre_cost_trim_cost_dtls_id and b.trim_group=$item_group and a.job_no='$job_no' and a.po_break_down_id=$po_id and b.id=$trim_dtla_id");
					foreach($red_data as $row_data)
					{
					$req_arr[$row_data[csf('po_break_down_id')]][$row_data[csf('job_no')]]['cons']=$row_data[csf('cons')];
					}
					
					$wo_sql_trim=sql_select("select b.id,b.job_no, b.po_break_down_id, b.description from wo_booking_dtls a, wo_trim_book_con_dtls b where a.id=b.wo_trim_booking_dtls_id and a.is_deleted=0 and a.status_active=1 and a.job_no=b.job_no  group by b.id,b.po_break_down_id,b.job_no,b.description ");
					foreach($wo_sql_trim as $row_trim)
					{
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['job_no']=$row_trim[csf('job_no')];
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['description']=$row_trim[csf('description')];
						
	
					}
						
				/*$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");*/
                        
                       	$costing_per_id=return_field_value( "costing_per", "wo_pre_cost_mst","job_no ='$job_no'");
						

					   $dzn_qnty=0;
                        if(	$costing_per_id==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($costing_per_id==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($costing_per_id==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($costing_per_id==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						
					
					$i=1;
					
					if($country_id_string==0)
					{
						$contry_cond="";
					}
					else
					{
						$contry_cond="and c.country_id in(".$country_id_string.")";
					}
					
				  $sql=" select  sum(c.order_quantity) as po_quantity ,c.country_id as country_id from wo_po_color_size_breakdown c  
			where   c.job_no_mst='$job_no' and c.po_break_down_id=$po_id $contry_cond  and c.status_active=1 and c.is_deleted=0 group by c.country_id ";
			 			
					$dtlsArray=sql_select($sql);						
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							$cons=$req_arr[$po_id][$job_no]['cons'];
							$req_qty=($row[csf('po_quantity')]/$dzn_qnty)*$cons;
							$descript=$item_description_arr[$po_id][$job_no]['description'];
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80" align="center"><p><? echo $buyer_short_name_library[$buyer]; ?></p></td>
                            <td width="100"><p><? echo $order_arr[$po_id]; ?></p></td>
                            <td width="100"><p><? echo $description; ?></p></td>
                            <td width="100" align="center"><p><? echo  $country_name_library[$row[csf('country_id')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($req_qty,2); ?></p></td>
                            <td width="" align="right"><p><? echo number_format($rate,4); ?></p></td>
                           
                        </tr>
						<?
						$tot_qty+=$req_qty;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td  align="right"></td>
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_qty,2); ?> </td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>