<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_store")
{
	$data=explode('_',$data);
	//print_r ($data);  
	echo create_drop_down( "cbo_store_name", 120, "select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id and a.status_active=1 and a.is_deleted=0 and a.company_id=$data[0] and b.category_type=$data[1] order by a.store_name","id,store_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier_name", 120, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b,lib_supplier_tag_company c where  a.id=b.supplier_id and a.id=c.supplier_id and  c.tag_company=$data and b.party_type in (1,6,7,8,90) and a.status_active=1 and a.is_deleted=0 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", "", "" );  	 
	exit();
	
}

if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
?>	
    <script>
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
/*	 function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			alert (tbl_row_count);
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ ) {
				 eval($('#tr_'+i).attr("onclick"));  
			}
		}
*/		
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_account_id').val( id );
		$('#item_account_val').val( ddd );
	} 
		  
	</script>
     <input type="text" id="item_account_id" />
     <input type="text" id="item_account_val" />
 <?php
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	
	$sql="SELECT id,item_account,item_category_id,item_group_id,item_description,supplier_id from  product_details_master where item_category_id=$data[1] and status_active=1 and is_deleted=0"; 
	$arr=array(1=>$item_category,2=>$itemgroupArr,4=>$supplierArr);
	echo  create_list_view("list_view", "Product ID,Item Account,Item Category,Item Group,Item Description,Supplier", "70,70,110,150,150,100","780","400",0, $sql , "js_set_value", "id,item_description", "", 0, "0,0,item_category_id,item_group_id,0,supplier_id", $arr , "id,item_account,item_category_id,item_group_id,item_description,supplier_id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
	exit();
}

if ($action=="item_group_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
?>	
    <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('item_name_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
        <input type="hidden" id="item_name_id" />
    <?php
	$sql="SELECT id,item_name from  lib_item_group where item_category=$data[1] and status_active=1 and is_deleted=0"; //id=$data[1] and
	
	echo  create_list_view("list_view", "Item Name", "350","500","330",0, $sql , "js_set_value", "id,item_name", "", 1, "0", $arr , "item_name", "item_wise_purchase_report_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	if ($cbo_company_name==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_name'";
	if ($cbo_item_category_id==0) $item_category_id=""; else $item_category_id=" and c.item_category_id='$cbo_item_category_id'";
	if ($item_account_id==0) $item_code=""; else $item_code=" and b.prod_id in ($item_account_id)";
	if ($item_group_id==0) $group_id=""; else $group_id=" and c.item_group_id='$item_group_id'";
	if ($cbo_supplier_name==0) $supplier_id=""; else $supplier_id=" and a.supplier_id='$cbo_supplier_name'";
	if ($cbo_store_name==0){ $store_id="";}else{$store_id=" and a.store_id='$cbo_store_name'";}
	if ($item_group_id==0){ $group_id="";}else{$group_id=" and c.item_group_id='$item_group_id'";}
	if($db_type==2)
		{	
	if( $from_date==0 && $to_date==0 ) $transaction_date=""; else $transaction_date= " and b.transaction_date  between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."'";	
		}
		if($db_type==0)
		{	
	if( $from_date==0 && $to_date==0 ) $transaction_date=""; else $transaction_date= " and b.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'";	
		}
 	
 	
 	//library array-------------------
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name"); 
	$storeArr = return_library_array("select id,store_name from lib_store_location where status_active=1 and is_deleted=0","id","store_name"); 
	$itemnameArr = return_library_array("select id,item_name from lib_item_creation where status_active=1 and is_deleted=0","id","item_name");
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	$pi_numArr = return_library_array("select id,pi_number from  com_pi_master_details where status_active=1 and is_deleted=0","id","pi_number");
	$wo_numArr = return_library_array("select id,wo_number from  wo_non_order_info_mst where status_active=1 and is_deleted=0","id","wo_number");
	$req_numArr = return_library_array("select id,requ_no from  inv_purchase_requisition_mst where status_active=1 and is_deleted=0","id","requ_no");

		$sql = "select a.id,a.item_category,a.receive_basis,a.receive_date,a.currency_id,a.recv_number,a.supplier_id,a.booking_id,b.pi_wo_batch_no,b.transaction_date,b.cons_quantity,b.cons_uom,b.cons_rate,b.cons_amount,c.id as pro_id,c.item_description,c.item_category_id,c.item_group_id,a.store_id,c.item_size from inv_receive_master a,inv_transaction b,product_details_master c 	where a.id=b.mst_id and a.company_id=b.company_id and a.store_id=b.store_id and a.item_category=b.item_category and a.receive_date=b.transaction_date and b.item_category in (8,9,10,11,15,16,17,18,19,20,21,22) and a.item_category in (8,9,10,11,15,16,17,18,19,20,21,22) and a.entry_form=20 and b.transaction_type=1 and c.id=b.prod_id	$company_id $item_category_id $item_code  $supplier_id $store_id $transaction_date $group_id order by a.store_id,c.item_category_id,a.receive_date,a.recv_number "; 
		
	//echo $sql; 
	$result = sql_select($sql);	
	$r=1;
	ob_start();	
	?>
	<div style="width:800px;" id="scroll_body" > 
     <fieldset style="width:800px;">
        <table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
            <tr class="form_caption" style="border:none;">
                <td colspan="10" align="center" style="border:none;font-size:20px; font-weight:bold" ><strong>Item Wise Purchase/Receive Details</strong></td> 
            </tr>
            <tr style="border:none;">
                <td colspan="10" align="center" style="border:none; font-size:17px;"><strong>
                    Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?>  </strong>                              
                </td>
            </tr>
            <tr style="border:none;">
                <td colspan="10" align="center" style="border:none;font-size:12px; font-weight:bold">
                    <?php if($from_date!="" || $to_date!="")echo "From : ".change_date_format($from_date)."   To : ".change_date_format($to_date)."" ;?>
                </td>
            </tr>
            <?php
			 $all_data=array();
			
			foreach($result as $row)
			{
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['item_category_id']=$row[csf('item_category_id')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['item_group_id']=$row[csf('item_group_id')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['store_id']=$row[csf('store_id')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]][csf('pro_id')]=$row[csf('pro_id')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['transaction_date']=$row[csf('transaction_date')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['supplier_id']=$row[csf('supplier_id')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]][csf('cons_quantity')]=$row[csf('cons_quantity')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['cons_uom']=$row[csf('cons_uom')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['currency_id']=$row[csf('currency_id')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]][csf('cons_rate')]=$row[csf('cons_rate')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]][csf('cons_amount')]=$row[csf('cons_amount')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['booking_id']=$row[csf('booking_id')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]][csf('recv_number')]=$row[csf('recv_number')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]][csf('receive_basis')]=$row[csf('receive_basis')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['item_size']=$row[csf('item_size')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['item_description']=$row[csf('item_description')];
				$all_data[$row[csf('pro_id')]][$row[csf('store_id')]][$row[csf('id')]]['pi_wo_batch_no']=$row[csf('pi_wo_batch_no')];
				
				if(!in_array($row[csf('pro_id')],$data_a))
				{
					$data_a[$row[csf('pro_id')]]=$row[csf('pro_id')];
				}
			} 
			
			$k=1;
		 	$comp_total_qnty=0;
			$comp_total_amount=0;
			foreach($data_a as $val)
			{
				$item_data=$all_data[$val];
				$item_total_qnty=0;
				$item_total_amount=0;
				
				foreach($item_data as $store_id=>$store_data)
				{
					$total_qnty=0;
					$total_amount=0;
					foreach($store_data as $row_id=>$row_data)
					{
						
						if($new_itm[$store_data[$row_id]['item_description']]=="")
						{
							$new_itm[$store_data[$row_id]['item_description']]=$store_data[$row_id]['item_description'];
						?>
							<tr>
								<td colspan="4" style="font-size:14px"><b>Item Name :<?php echo $store_data[$row_id]['item_description'].','.$store_data[$row_id]['item_size'];?></b></td>
								<td colspan="3" style="font-size:14px"><b>Item Category :<?php echo $item_category[$store_data[$row_id]['item_category_id']]; ?></b></td>
								<td colspan="3" style="font-size:14px"><b>Item Group :<?php echo $itemgroupArr[$store_data[$row_id]['item_group_id']]; ?></b></td>
							</tr>
						<?php
						}
						if($new_arr[$store_data[$row_id]['item_description']][$store_id]=="")
						{
							$st=1;
							$new_arr[$store_data[$row_id]['item_description']][$store_id]= $store_id;
						?>
								<tr><td colspan="10" style="font-size:12px"><b>Store :<?php echo $storeArr[$store_id]; ?></b></td></tr>
							</table>
							
							<table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_2" >
							 <thead>
									<th width="30" >SL</th>
									<th width="80" >Receive Date</th>
									<th width="100" >Supplier</th>
									<th width="80" >Quantity</th>
									<th width="70" >UoM</th>
									<th width="70" >Currency</th>
									<th width="70" >Rate</th>
									<th width="70" >Amount</th>
									<th width="70" >Basis Ref.</th>
									<th width="120" >MRIR</th>                    
								</thead></tr>
								<tbody>
							<?php
						}
						if ($k%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
						if($row_data["transaction_type"]==1) 
							$stylecolor='style="color:#A61000"';
						else
							$stylecolor='style="color:#000000"'; 								
						?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $k; ?>">
                                <td align="center"><?php echo $k; ?></td>
                                <td align="center"><?php echo change_date_format($row_data['transaction_date']); ?></td>
                                <td align="center"><?php echo $supplierArr[$row_data['supplier_id']]; ?></td>
                                <td align="right"><?php echo $row_data[csf('cons_quantity')]; ?></td>
                                <td align="center"><?php echo $unit_of_measurement[$row_data['cons_uom']]; ?></td>
                                <td align="center"><?php echo $currency[$row_data['currency_id']]; ?></td>
                                <td align="right"><?php echo number_format($row_data[csf('cons_rate')],2,'.',''); ?></td>
                                <td align="right"><?php echo number_format($row_data[csf('cons_amount')],2,'.',''); ?></td>
								<?php 
									$receive_basis=$row_data[csf('receive_basis')];
									if ($receive_basis==1)
									{
								?>
										<td><?php echo $pi_numArr[$row_data[csf('pi_wo_batch_no')]]; ?></td>
								<?php
									}
									else if($receive_basis==2)
									{
								?>
										<td><?php echo $wo_numArr[$row_data[csf('pi_wo_batch_no')]]; ?></td>
								<?php
									}
									else if($receive_basis==4)
									{
								?>
										<td><?php echo $receive_basis_arr[$row_data[csf('receive_basis')]]; ?></td>
								<?php
									}
									else if($receive_basis==6)
									{
								?>
										<td><?php echo $receive_basis_arr[$row_data[csf('receive_basis')]]; ?></td>
                                <?php
									}
									else if($receive_basis==7)
									{
								?>
										<td><?php echo $req_numArr[$row_data[csf('pi_wo_batch_no')]]; ?></td>
								<?php
									}
								?>
                                <td align="center"><?php  echo $row_data[csf('recv_number')]; ?></td>
                                
                            </tr>
                        <?php
					 	$k++;	
					 	$total_qnty+=$row_data[csf('cons_quantity')];
						$total_amount+=$row_data[csf('cons_amount')];
					} 
						?>
                    	<tr>
                        	<td colspan="3" align="right"><b>Store Total: </b></td>
							<td align="right" ><b><?php echo number_format($total_qnty,0,'',','); ?></b></td>
                            <td align="right" colspan="4"><b><?php echo number_format($total_amount,2,'.',''); ?></b></td>
                            <td colspan="2">&nbsp;</td>
						</tr>
                        <?php
					$item_total_qnty+=$total_qnty;
					$item_total_amount+=$total_amount;
				}
				?>
				<tr>
					<td colspan="3" align="right"><b>Item Total: </b></td>
					<td align="right" ><b><?php echo number_format($item_total_qnty,0,'',','); ?></b></td>
					<td align="right" colspan="4"><b><?php echo number_format($item_total_amount,2,'.',''); ?></b></td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<?php
		 	$comp_total_qnty+=$item_total_qnty;
			$comp_total_amount+=$item_total_amount;
			}
			?>
				<tr>
					<td colspan="3" align="right"><b>Grand Total: </b></td>
					<td align="right" ><b><?php echo number_format($comp_total_qnty,0,'',','); ?></b></td>
					<td align="right" colspan="4"><b><?php echo number_format($comp_total_amount,2,'.',''); ?></b></td>
					<td colspan="2">&nbsp;</td>
				</tr>
           </tbody>
        </table>
    </fieldset>
   </div>
     <?php
	$html = ob_get_contents();
	ob_clean();
	//$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
	foreach (glob("*.xls") as $filename) {
	//if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc, $html);
	echo "$html**$filename"; 
	exit();	
}
?>