-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_booking_mst_hstry`
--

DROP TABLE IF EXISTS `wo_booking_mst_hstry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_booking_mst_hstry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) NOT NULL DEFAULT '0',
  `booking_id` int(11) NOT NULL DEFAULT '0',
  `booking_type` int(11) NOT NULL COMMENT '1=FB,2=TB,3=ServiceB,4=Sample B',
  `is_short` int(11) NOT NULL COMMENT '1=yes,2=no',
  `booking_no_prefix` varchar(12) NOT NULL,
  `booking_no_prefix_num` int(11) NOT NULL,
  `booking_no` varchar(25) NOT NULL,
  `company_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `job_no` varchar(25) NOT NULL,
  `po_break_down_id` varchar(150) NOT NULL,
  `item_category` int(11) NOT NULL,
  `fabric_source` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `exchange_rate` double NOT NULL,
  `pay_mode` int(11) NOT NULL,
  `source` int(11) NOT NULL,
  `booking_date` date NOT NULL,
  `delivery_date` date NOT NULL,
  `booking_month` int(11) NOT NULL,
  `booking_year` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `attention` varchar(200) NOT NULL,
  `booking_percent` int(11) NOT NULL DEFAULT '0',
  `colar_excess_percent` double NOT NULL,
  `cuff_excess_percent` double NOT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '0',
  `ready_to_approved` int(11) NOT NULL DEFAULT '0',
  `is_apply_last_update` int(11) NOT NULL,
  `rmg_process_breakdown` varchar(200) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_booking_mst_hstry`
--

LOCK TABLES `wo_booking_mst_hstry` WRITE;
/*!40000 ALTER TABLE `wo_booking_mst_hstry` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_booking_mst_hstry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
