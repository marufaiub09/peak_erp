-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_capacity_calc_mst`
--

DROP TABLE IF EXISTS `lib_capacity_calc_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_capacity_calc_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comapny_id` int(11) NOT NULL DEFAULT '0',
  `capacity_source` int(11) NOT NULL DEFAULT '0',
  `year` int(4) NOT NULL DEFAULT '0',
  `location_id` tinyint(3) NOT NULL DEFAULT '0',
  `avg_machine_line` int(11) NOT NULL DEFAULT '0',
  `basic_smv` double NOT NULL DEFAULT '0',
  `effi_percent` double NOT NULL DEFAULT '0',
  `inserted_by` tinyint(3) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` tinyint(3) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_capacity_calc_mst`
--

LOCK TABLES `lib_capacity_calc_mst` WRITE;
/*!40000 ALTER TABLE `lib_capacity_calc_mst` DISABLE KEYS */;
INSERT INTO `lib_capacity_calc_mst` VALUES (1,1,1,2015,1,20,3,55,1,'2015-03-08 11:40:48',1,'2015-08-20 16:16:47',1,0),(2,1,1,2015,1,20,6.5,50,3,'2015-09-10 10:17:49',3,'2015-09-10 10:24:17',1,0),(3,1,1,2015,1,22,4,50,2,'2015-10-11 14:44:52',2,'2015-10-11 14:45:09',1,0);
/*!40000 ALTER TABLE `lib_capacity_calc_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
