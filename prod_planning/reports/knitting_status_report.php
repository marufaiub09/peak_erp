<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Knitting Status Report.
Functionality	:	
JS Functions	:
Created by		:	Fuad 
Creation date 	: 	11-08-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Knitting Status Report", "../../", 1, 1,'',1,1);

?>	

<script>

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission = '<?php echo $permission; ?>';
 
function fn_report_generated(type)
{
	if (form_validation('cbo_company_name','Comapny Name')==false)
	{
		return;
	}
	
	var data="action=report_generate"+get_submitted_data_string('cbo_type*cbo_company_name*cbo_buyer_name*txt_machine_dia*cbo_party_type*txt_order_no*hide_order_id*txt_program_no*txt_date_from*txt_date_to*cbo_knitting_status',"../../");
	freeze_window(3);
	http.open("POST","requires/knitting_status_report_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fn_report_generated_reponse;
}
	

function fn_report_generated_reponse()
{
 	if(http.readyState == 4) 
	{
  		var response=trim(http.responseText).split("####");
		$('#report_container2').html(response[0]);
		document.getElementById('report_container').innerHTML='<a href="'+response[1]+'" style="text-decoration:none" ><input type="button" value="Convert To Excel" name="excel" id="excel" class="formbutton" style="width:155px"/></a>&nbsp;&nbsp;<input type="button" onclick="generate_requisition_report()" value="Requisition Print" name="Print" class="formbutton" style="width:150px"/>'; 
		
		//append_report_checkbox('table_header_1',1);
		// $("input:checkbox").hide();
		show_msg('3');
		release_freezing();
 	}
	
}

function openmypage_order()
{
	if(form_validation('cbo_company_name','Company Name')==false)
	{
		return;
	}
	
	var companyID = $("#cbo_company_name").val();
	var page_link='requires/knitting_status_report_controller.php?action=order_no_search_popup&companyID='+companyID;
	var title='Order No Search';
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=790px,height=390px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var order_no=this.contentDoc.getElementById("hide_order_no").value;
		var order_id=this.contentDoc.getElementById("hide_order_id").value;
		
		$('#txt_order_no').val(order_no);
		$('#hide_order_id').val(order_id);	 
	}
}
	

function generate_report(company_id,program_id)
{ 
	 print_report( company_id+'*'+program_id, "print", "requires/knitting_status_report_controller" ) ;
}

function selected_row(rowNo)
{
	var isChecked=$('#tbl_'+rowNo).is(":checked");
	var job_no=$('#job_no_'+rowNo).val();

	if(isChecked==true)
	{
		var tot_row=$('#tbl_list_search tbody tr').length;
		for(var i=1; i<=tot_row; i++)
		{ 
			if(i!=rowNo)
			{
				try 
				{
					if ($('#tbl_'+i).is(":checked"))
					{
						var job_noCurrent=$('#job_no_'+i).val();
						if((job_no!=job_noCurrent))
						{
							alert("Please Select Same Job No.");
							$('#tbl_'+rowNo).attr('checked',false);
							return;
						}
					}
				}
				catch(e) 
				{
					//got error no operation
				}
			}
		}
	}
}

function generate_requisition_report()
{ 
	var program_ids = ""; var total_tr=$('#tbl_list_search tbody tr').length;
	for(i=1; i<total_tr; i++)
	{
		try 
		{
			if ($('#tbl_'+i).is(":checked"))
			{
				program_id = $('#promram_id_'+i).val();
				if(program_ids=="") program_ids= program_id; else program_ids +=','+program_id;
			}
		}
		catch(e) 
		{
			//got error no operation
		}
	}
	
	if(program_ids=="")
	{
		alert("Please Select At Least One Program");
		return;
	}

	print_report(program_ids, "requisition_print", "requires/knitting_status_report_controller" ) ;
}

	
</script>


</head>
 
<body onLoad="set_hotkey();">

<form id="knittingStatusReport_1">
    <div style="width:100%;" align="center">    
    
        <?php echo load_freeze_divs ("../../",'');  ?>
         
         <h3 style="width:1100px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu(this.id,'content_search_panel','')">-Search Panel</h3> 
         <div id="content_search_panel" >      
         <fieldset style="width:1100px;">
             <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
             	<thead>
                    <th class="must_entry_caption">Company Name</th>
                    <th>Buyer Name</th>
                    <th>Order No</th>
                    <th>Machine Dia</th>
                    <th>Type</th>
                    <th>Party Type</th>
                    <th>Program No</th>
                    <th>Status</th>
                    <th>Program Date</th>
                    <th><input type="reset" name="res" id="res" value="Reset" onClick="reset_form('knittingStatusReport_1','report_container*report_container2','','','')" class="formbutton" style="width:70px" /></th>
                </thead>
                <tbody>
                    <tr class="general">
                        <td> 
                            <?php
                                echo create_drop_down( "cbo_company_name", 120, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "- Select Company -", $selected, "load_drop_down( 'requires/knitting_status_report_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                            ?>
                        </td>
                        <td id="buyer_td">
                            <?php 
                                echo create_drop_down( "cbo_buyer_name", 120, $blank_array,"", 1, "- All Buyer -", $selected, "",0,"" );
                            ?>
                        </td>
                        <td>
                            <input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:90px" placeholder="Browse Or Write" onDblClick="openmypage_order();" onChange="$('#hide_order_id').val('');" autocomplete="off">
                            <input type="hidden" name="hide_order_id" id="hide_order_id" readonly>
                        </td>
                        <td>
                            <input name="txt_machine_dia" id="txt_machine_dia" class="text_boxes" style="width:60px">
                        </td>
                        <td>
                        	<?php
								$search_by_arr=array(1=>"Inside",3=>"Outside",0=>"Without Source");
								echo create_drop_down( "cbo_type", 105, $search_by_arr,"",0, "", "","load_drop_down( 'requires/knitting_status_report_controller',this.value+'**'+$('#cbo_company_name').val(), 'load_drop_down_party_type', 'party_type_td' );",0 );
							?>
                        </td> 
                        <td id="party_type_td">
                        	<?php
								echo create_drop_down( "cbo_party_type", 120, $blank_array,"",1, "--Select--", "",'',1 );
							?>
                        </td> 
                        <td>
                            <input name="txt_program_no" id="txt_program_no" class="text_boxes_numeric" style="width:60px">
                        </td>
                         <td align="center">
                            <?php 
                                echo create_drop_down( "cbo_knitting_status", 110, $knitting_program_status,"", 0, "- Select -", $selected, "",0,"" );
                            ?>
                        </td>
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" value="" class="datepicker" style="width:60px" placeholder="From Date"/>
                            To
                            <input type="text" name="txt_date_to" id="txt_date_to" value="" class="datepicker" style="width:60px" placeholder="To Date"/>
                        </td>
                        <td>
                            <input type="button" id="show_button" class="formbutton" style="width:70px" value="Show" onClick="fn_report_generated(1)" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10" align="center" width="95%"><?php echo load_month_buttons(1); ?></td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
    	</div>
    </div>
    <div id="report_container" align="center"></div>
    <div id="report_container2" align="left"></div>
 </form>   
</body>
<script>
	set_multiselect('cbo_knitting_status','0','0','','');
</script>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
