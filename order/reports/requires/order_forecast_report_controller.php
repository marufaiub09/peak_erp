<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------------------------------
$company_library=return_library_array( "select id, company_short_name from lib_company", "id", "company_short_name"  );
$buyer_arr_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
//$agent_arr_library=return_library_array( "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id  and a.id in (select  buyer_id from lib_buyer_party_type where party_type in (20,21)) order by a.buyer_name", "id", "buyer_name"  );
$agent_arr_library=return_library_array( "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id  and a.id in (select  buyer_id from lib_buyer_party_type where party_type in (20,21)) order by buyer_name", "id", "buyer_name"  );
$order_arr=return_library_array( "select id, po_number from wo_po_break_down", "id", "po_number"  );
if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 130, "select buy.buyer_name, buy.id from lib_buyer buy where status_active =1 and is_deleted=0","id,buyer_name", 1, "-- All Buyer --", $selected, "" );   	 
	exit();
}

$tmplte=explode("**",$data);
if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$company_name=str_replace("'","",$cbo_company_name);
	$agent_name=str_replace("'","",$cbo_agent);
	$buyer_id_cond="";
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="")
			{
				$buyer_id_cond=" and a.buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")";
			}
			else
			{
				$buyer_id_cond="";
			}
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_id=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	if($agent_name==0) $agent_cond=""; else $agent_cond=" and a.agent=$agent_name";
	//echo $buyer_id_cond;die;
	//if($year_from!=0 && $month_from!=0)
	//{
	$year_from=str_replace("'","",$cbo_year_from);
	$month_from=str_replace("'","",$cbo_month_from);
	$start_date=$year_from."-".$month_from."-01";
	
	$year_to=str_replace("'","",$cbo_year_to);
	$month_to=str_replace("'","",$cbo_month_to);
	$num_days = cal_days_in_month(CAL_GREGORIAN, $month_to, $year_to);
	$end_date=$year_to."-".$month_to."-$num_days";
	
	if($db_type==0) 
		{
			$date_cond=" and b.sales_target_date between '$start_date' and '$end_date'";
		}
		if($db_type==2) 
		{
			//echo "sdsdsd";die;
			 $date_cond=" and b.sales_target_date between '".date("j-M-Y",strtotime($start_date))."' and '".date("j-M-Y",strtotime($end_date))."'";
		}
		
	//}
	
ob_start();
		 $sql_sales=sql_select("select a.buyer_id, b.sales_target_date ,a.agent,b.sales_target_qty as sales_target_qty,b.sales_target_value from wo_sales_target_mst  a,wo_sales_target_dtls b where a.id=b.sales_target_mst_id and a.status_active=1 and a.is_deleted=0 and a.company_id=$company_name $buyer_id_cond $agent_cond  $date_cond order by b.sales_target_date,a.buyer_id ");
	$sale_data_arr=array();$buyer_tem_arr=array();$agent_tem_arr=array();
		foreach($sql_sales as $row)
		{
		$sale_data_arr[date("Y-m",strtotime($row[csf("sales_target_date")]))][$row[csf("buyer_id")]]['target_qty']=$row[csf("sales_target_qty")];
		$sale_data_arr[date("Y-m",strtotime($row[csf("sales_target_date")]))][$row[csf("buyer_id")]]['target_val']=$row[csf("sales_target_value")];
		$sale_data_arr[date("Y-m",strtotime($row[csf("sales_target_date")]))][$row[csf("buyer_id")]]['buyer_id']=$row[csf("buyer_id")];
		//$sale_data_arr[date("Y-m",strtotime($row[csf("sales_target_date")]))][$row[csf("buyer_id")]]['agent']=$row[csf("agent")];
		$buyer_tem_arr[$row[csf("buyer_id")]]=$row[csf("buyer_id")];
		$agent_tem_arr[$row[csf("buyer_id")]]=$row[csf("agent")];
		
		} //var_dump($sale_data_arr['2015-01'][30]['target_qty']);
			//$noOfPo=count($poDataArray);
		$total_month=count($sale_data_arr);
		$width=$total_month*2*90+450;
		//$width=($total_month*735)+100; 
		$colspan=$total_month;
		$colspan_mon=$total_month+2;
?>
<br>
 <fieldset style="width:<?php echo $width+20; ?>px;">
        	<table cellpadding="0" cellspacing="0" width="<?php echo $width; ?>">
                <tr>
                   <td align="center" width="100%" colspan="<?php echo $total_month*2+5 ?>" class="form_caption" style="font-size:16px;"><?php echo $company_library[$company_name]; ?></td>
                </tr>
                 <tr>
                   <td align="center" width="100%" colspan="<?php echo $total_month*2+5 ?>" class="form_caption"><strong>Sales Forecasting </strong></td>
                </tr>
            </table>	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $width; ?>" class="rpt_table" >
                <thead>
                	<tr>
                        <th width="40" rowspan="2">SL</th>
                        <th width="100" rowspan="2">Buyer Name</th>
                        <th width="100" rowspan="2">Agent Name</th>
                        <?php
							foreach($sale_data_arr as $yearMonth=>$val)
							{
								 $month_arr=explode("-",$yearMonth);
								$month_val=($month_arr[1]*1);
							?>
                            	<th width="180" colspan="2"><p><?php echo  $months[$month_val];; ?></p></th>
                            <?php	
							}
						?>
                    <th colspan="2">Total</th> 
                    </tr>
                    <tr>
                    	<?php
							for($z=1;$z<=$total_month;$z++)
							{
							?>
                            	<th width="90">Quantity</th>
                                <th width="90">Value</th>
                            <?php	
							}
						?>
                        <th width="90">Quantity</th>
                        <th>Value</th>
                    </tr>
                </thead>
            </table>
			<div style="width:<?php echo $width; ?>px; overflow-y:scroll; max-height:330px;" id="scroll_body">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $width-18; ?>" class="rpt_table" id="table_body" >
					<?php 
					   $i=1;
                        foreach($buyer_tem_arr as $buyer_id=>$val)
                        {
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
								 $sales_qnty=$sale_data_arr[$month_id][$buyer_id]['target_qty'];
								$agent_name=$agent_tem_arr[$buyer_id]
                       	?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
                                <td width="40"><?php echo $i; ?></td>
                                <td width="100"><p><?php echo $buyer_arr_library[$buyer_id]; ?>&nbsp;</p></td>
                                <td width="100"><p><?php echo  $agent_arr_library[$agent_name]; ?>&nbsp;</p></td>
                                <?php //$agent_arr_library
									$color_tot_qnty=0; $color_knit_tot_qnty=0; $z=1;
								 	foreach($sale_data_arr as $month_id=>$result)
									{
										 //echo $buyer_id;
										$sales_qnty=$sale_data_arr[$month_id][$buyer_id]['target_qty'];
										$sales_qnty_val=$sale_data_arr[$month_id][$buyer_id]['target_val'];
									?>
                                        <td width="90" align="right"><?php echo number_format($sales_qnty,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($sales_qnty_val,2); ?></td>
                                    <?php
										$z++;
										$tot_sales_qty+=$sales_qnty;	
										$tot_sales_qnty_val+=$sales_qnty_val;	
										$tot_sales_qty_month[$month_id]+=$sales_qnty;	
										$tot_sales_qnty_val_month[$month_id]+=$sales_qnty_val;	
									}
								?>
                                <td align="right" width="90"><?php echo number_format($tot_sales_qty,2,'.',''); ?></td>
                                <td align="right"><?php echo number_format($tot_sales_qnty_val,2,'.',''); ?></td>
                            </tr>
                            <?php
                            $i++;
							
							$total_sales_qty+=$tot_sales_qty;
							$total_tot_sales_qnty_val+=$tot_sales_qnty_val;
                        }
					?>
                	<tfoot>
                        <th width="100" colspan="3" align="right">Total</th>
                        <?php
                        	foreach($sale_data_arr as $month_id=>$result)
							{
								?>
						<th width="90" align="right"><?php echo number_format($tot_sales_qty_month[$month_id],2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($tot_sales_qnty_val_month[$month_id],2,'.',''); ?></th>
                        <?php	
							}
						?>
                            
                       
                        <th width="90" align="right"><?php echo number_format($total_sales_qty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($total_tot_sales_qnty_val,2,'.',''); ?></th>
                	</tfoot>    
				</table> 
			</div>
      	</fieldset> 
<?php
foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	
	exit();	
}


