-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_non_order_info_mst_history`
--

DROP TABLE IF EXISTS `wo_non_order_info_mst_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_non_order_info_mst_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL,
  `approved_no` int(11) NOT NULL,
  `garments_nature` tinyint(2) NOT NULL DEFAULT '0',
  `wo_number_prefix` varchar(10) NOT NULL,
  `wo_number_prefix_num` int(11) NOT NULL,
  `wo_number` varchar(20) NOT NULL,
  `company_name` int(11) NOT NULL DEFAULT '0',
  `buyer_po` varchar(200) NOT NULL,
  `requisition_no` varchar(100) NOT NULL,
  `delivery_place` varchar(200) NOT NULL,
  `wo_date` date NOT NULL,
  `supplier_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attention` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `wo_basis_id` int(11) NOT NULL DEFAULT '0',
  `item_category` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `delivery_date` date NOT NULL,
  `source` int(11) NOT NULL DEFAULT '0',
  `pay_mode` int(11) NOT NULL DEFAULT '0',
  `terms_and_condition` text NOT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: No, 1: Yes',
  `approved_by` tinyint(3) NOT NULL DEFAULT '0',
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_non_order_info_mst_history`
--

LOCK TABLES `wo_non_order_info_mst_history` WRITE;
/*!40000 ALTER TABLE `wo_non_order_info_mst_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_non_order_info_mst_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
