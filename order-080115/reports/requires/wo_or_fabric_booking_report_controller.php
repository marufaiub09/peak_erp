<?
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_id", 130, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $selected, "" );   	 
	exit();
}

if ($action=="job_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
?>	
    <script>
	function js_set_value( job_id )
	{
		//alert(po_id)
		document.getElementById('txt_job_id').value=job_id;
		parent.emailwindow.hide();
	}
		  
	</script>
     <input type="hidden" id="txt_job_id" />
 <?
	if ($data[0]==0) $company_id=""; else $company_id=" and company_name=$data[0]";
	if ($data[1]==0) $buyer_id=""; else $buyer_id=" and buyer_name=$data[1]";
	if($db_type==0)
	{
		if ($data[2]==0) $year_id_cond=""; else $year_id_cond=" and YEAR(insert_date)=$data[2]";
	}
	elseif($db_type==2)
	{
		if ($data[2]==0) $year_id_cond=""; else $year_id_cond=" and TO_CHAR(insert_date,'YYYY')=$data[2]";
	}
	
	if($db_type==0)
	{
		$year=" YEAR(insert_date) as year";
	}
	elseif($db_type==2)
	{
		$year=" TO_CHAR(insert_date,'YYYY') as year";
	}
	
	$buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
	
	$sql= "select id, job_no, $year, job_no_prefix_num, style_ref_no, buyer_name, style_description from wo_po_details_master where status_active=1 and is_deleted=0 $company_id $buyer_id $year_id_cond order by id DESC";
	//echo $sql;
	
	$arr=array(3=>$buyerArr);
	echo  create_list_view("list_view", "Job No,Year,Style Ref.,Buyer,Style Description", "70,70,130,140,170","630","320",0, $sql , "js_set_value", "id,job_no", "", 1, "0,0,0,buyer_name,0", $arr , "job_no_prefix_num,year,style_ref_no,buyer_name,style_description", "",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
	exit();
}

if($action=="wo_no_popup")
{
  	echo load_html_head_contents("Order Info","../../../", 1, 1, '','1','');
	extract($_REQUEST);
	$ex_data=explode('_',$data);
	$company_id=$ex_data[0];
	$buyer_id=$ex_data[1];
	$year_id=$ex_data[2];
	$category_id=$ex_data[3];
	$wo_type=$ex_data[4];
?>
	<script>
		function js_set_value(wo_id,wo_no)
		{
			document.getElementById('txt_wo_no').value=wo_no;
			document.getElementById('txt_wo_id').value=wo_id;
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
	<fieldset style="width:620px;margin-left:10px">
        <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
            <table cellpadding="0" cellspacing="0" border="1" rules="all" width="620" class="rpt_table">
                <thead>
                    <th>Buyer</th>
                    <th>Search</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="txt_wo_no" id="txt_wo_no" value="" style="width:50px">
                        <input type="hidden" name="txt_wo_id" id="txt_wo_id" value="" style="width:50px">
                    </th> 
                </thead>
                <tr class="general">
                    <td align="center">
                        <?
							echo create_drop_down( "cbo_buyer_id", 130, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $buyer_id, "" );   	 
                        ?>       
                    </td>
                    <td align="center">				
                        <input type="text" style="width:150px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 						
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_buyer_id').value+'_'+<? echo $company_id; ?>+'_'+document.getElementById('txt_search_common').value+'_'+<? echo $year_id; ?>+'_'+<? echo $category_id; ?>+'_'+<? echo $wo_type; ?>, 'create_wo_search_list_view', 'search_div', 'wo_or_fabric_booking_report_controller', 'setFilterGrid(\'list_view\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
            <div id="search_div" style="margin-top:10px"></div>   
        </form>
    </fieldset>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}

if ($action=="create_wo_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]==0) $buyer_id=""; else $buyer_id=" and buyer_id=$data[0]";
	if ($data[1]==0) $company_id=""; else $company_id=" and company_id=$data[1]";
	if ($data[2]==0) $search_wo=""; else $search_wo=" and booking_no_prefix_num=$data[2]";
	
	if($db_type==0)
	{
		if ($data[3]==0) $year_id_cond=""; else $year_id_cond=" and YEAR(insert_date)=$data[3]";
	}
	elseif($db_type==2)
	{
		if ($data[3]==0) $year_id_cond=""; else $year_id_cond=" and TO_CHAR(insert_date,'YYYY')=$data[3]";
	}
	
	if ($data[4]==0) $category_id_cond=""; else $category_id_cond=" and item_category=$data[4]";
	if ($data[5]==1 || $data[5]==2)  $wo_type_cond=" and booking_type in (1,2) and is_short='$data[5]'"; else $wo_type_cond="";
	if ($data[5]==3) $wo_type_cond_sam="  and booking_type=4"; else $wo_type_cond_sam="";
	
	if($db_type==0)
	{
		$year=" YEAR(insert_date) as year";
	}
	elseif($db_type==2)
	{
		$year=" TO_CHAR(insert_date,'YYYY') as year";
	}
	
	$buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
	if($data[5]==0)
	{
		$sql= "select id, booking_no, $year, booking_no_prefix_num, booking_date, buyer_id, booking_type, is_short, 0 as type from wo_booking_mst where status_active=1 and is_deleted=0 $company_id $buyer_id $year_id_cond $search_wo $category_id_cond $wo_type_cond $wo_type_cond_sam
		union all
		SELECT id, booking_no, $year, booking_no_prefix_num, booking_date, buyer_id, booking_type, is_short, 1 as type from wo_non_ord_samp_booking_mst where status_active=1 and is_deleted=0 $company_id $buyer_id $year_id_cond $search_wo $category_id_cond";
	}
	else if ($data[5]==1 || $data[5]==2 || $data[5]==3)
	{
		$sql= "select id, booking_no, $year, booking_no_prefix_num, booking_date, buyer_id, booking_type, is_short, 0 as type from wo_booking_mst where status_active=1 and is_deleted=0 $company_id $buyer_id $year_id_cond $search_wo $category_id_cond $wo_type_cond $wo_type_cond_sam";
	}
	else
	{
		$sql= "SELECT id, booking_no, $year, booking_no_prefix_num, booking_date, buyer_id, booking_type, is_short, 1 as type from wo_non_ord_samp_booking_mst where status_active=1 and is_deleted=0 $company_id $buyer_id $year_id_cond $search_wo $category_id_cond";
	}
	//echo $sql;
	
	//$arr=array(3=>$buyerArr);
	//echo  create_list_view("list_view", "WO No,Year,WO Type,Buyer,WO Date", "70,70,130,140,170","630","320",0, $sql , "js_set_value", "id,booking_no", "", 1, "0,0,0,buyer_id,0", $arr , "booking_no_prefix_num,year,style_ref_no,buyer_id,booking_date", "",'setFilterGrid("list_view",-1);','0,0,0,0,3','') ;
	
?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="30">SL</th>
                <th width="80">WO No </th>
                <th width="80">Year</th>
                <th width="130">WO Type</th>
                <th width="150">Buyer</th>
                <th width="100">WO Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:300px;" id="" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="list_view" >
            <?
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					if ($selectResult[csf("type")]==0)
					{	
						if ($selectResult[csf("booking_type")]==1 || $selectResult[csf("booking_type")]==2)
						{
							if ($selectResult[csf("is_short")]==1)
							{
								$wo_type="Short";
							}
							else
							{
								$wo_type="Main";
							}
						}
						elseif($selectResult[csf("booking_type")]==4)
						{
							$wo_type="Sample With Order";
						}
					}
					else
					{
						$wo_type="Sample Non Order";
					}					
				?>
                    <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<? echo $i;?>" onClick="js_set_value(<? echo $selectResult[csf('id')]; ?>,'<? echo $selectResult[csf('booking_no')]; ?>')"> 
                        <td width="30" align="center"><? echo $i; ?></td>	
                        <td width="80" align="center"><p><? echo $selectResult[csf('booking_no_prefix_num')]; ?></p></td>
                        <td width="80" align="center"><? echo $selectResult[csf('year')]; ?></td>
                        <td width="130"><p><? echo $wo_type; ?></p></td>
                        <td width="150"><p><? echo $buyerArr[$selectResult[csf('buyer_id')]]; ?></p></td>
                        <td  width="100" align="center"><? echo change_date_format($selectResult[csf('booking_date')]); ?></td>	
                    </tr>
                <?
                	$i++;
				}
			?>
            </table>
        </div>
	</div>           
	<?
	exit();
}

if ($action=="po_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);
	
?>	
    <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function check_all_data()
		{
			var row_num=$('#list_view tr').length-1;
			for(var i=1;  i<=row_num;  i++)
			{
				$("#tr_"+i).click();
			}
			
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
		  
	</script>
     <input type="hidden" id="txt_po_id" />
     <input type="hidden" id="txt_po_val" />
     <?
	 if ($data[0]==0) $company_name=""; else $job_num=" and a.company_name='$data[0]'";
	 if ($data[1]==0) $buyer_name=""; else $buyer_name=" and a.buyer_name='$data[1]'";
	 if ($data[2]=="") $job_num=""; else $job_num=" and a.job_no_prefix_num='$data[2]'";
	
	$sql= "select b.id, b.po_number, b.job_no_mst, b.pub_shipment_date,c.gmts_item_id from wo_po_details_master a, wo_po_break_down  b, wo_po_details_mas_set_details c where a.job_no=b.job_no_mst and a.job_no=c.job_no and a.status_active=1 and a.is_deleted=0 $job_num $company_name $buyer_name order by b.id Desc";
	
	$arr=array(3=>$garments_item);
	echo  create_list_view("list_view", "PO No.,Job No.,Pub Shipment Date,Item Name", "100,100,80,150","450","340",0, $sql , "js_set_value", "id,po_number", "", 1, "0,0,0,gmts_item_id", $arr , "po_number,job_no_mst,pub_shipment_date,gmts_item_id", "",'setFilterGrid("list_view",-1);','0,0,3,0','',1) ;
	exit();	 
}

if ($action=="report_generate")
{
	extract($_REQUEST);
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_buyer=str_replace("'","",$cbo_buyer_id);
	$cbo_category=str_replace("'","",$cbo_category_id);
	$wo_type=str_replace("'","",$cbo_wo_type);
	$year_id=str_replace("'","",$cbo_year_id);
	$job_year_id=str_replace("'","",$cbo_job_year_id);
	$wo_no=str_replace("'","",$txt_wo_no);
	$wo_id=str_replace("'","",$hidd_wo_id);
	
	$job_no=str_replace("'","",$txt_job_no);
	$hidd_job=str_replace("'","",$hidd_job_id);
	$po_no=str_replace("'","",$txt_po_no);
	$hidd_po=str_replace("'","",$hidd_po_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	if($db_type==0)
	{
		if ($year_id==0) $year_id_cond=""; else $year_id_cond=" and YEAR(a.insert_date)=$year_id";
		if ($year_id==0) $year_id_cond_sam=""; else $year_id_cond_sam=" and YEAR(s.insert_date)=$year_id";
	}
	elseif($db_type==2)
	{ 
		if ($year_id==0) $year_id_cond=""; else $year_id_cond=" and TO_CHAR(a.insert_date,'YYYY')=$year_id";
		if ($year_id==0) $year_id_cond_sam=""; else $year_id_cond_sam=" and TO_CHAR(s.insert_date,'YYYY')=$year_id";
	}
	
	if ($cbo_company==0) $company_id=""; else $company_id=" and a.company_id=$cbo_company";
	if ($cbo_company==0) $company_id_sam=""; else $company_id_sam=" and s.company_id=$cbo_company";
	if ($cbo_buyer==0) $buyer_id=""; else $buyer_id=" and a.buyer_id=$cbo_buyer";
	if ($cbo_buyer==0) $buyer_id_sam=""; else $buyer_id_sam=" and s.buyer_id=$cbo_buyer";
	if ($cbo_category==0) $category_id=""; else $category_id=" and a.item_category=$cbo_category";
	if ($cbo_category==0) $category_id_sam=""; else $category_id_sam=" and s.item_category=$cbo_category";
	if ($wo_no=="") $wo_no_cond=""; else $wo_no_cond=" and a.booking_no='$wo_no'";
	if ($wo_no=="") $wo_no_cond_sam=""; else $wo_no_cond_sam=" and s.booking_no='$wo_no'";
	
	if ($wo_type==1 || $wo_type==2)  $wo_type_cond=" and a.booking_type in (1,2) and a.is_short='$wo_type'"; else $wo_type_cond="";
	if ($wo_type==3) $wo_type_cond_sam="  and a.booking_type=4"; else $wo_type_cond_sam="";
	
	if ($job_no=="") $job_num=""; else $job_num=" and b.job_no='$job_no'";
	//if ($job_no=="") $job_num_mst=""; else $job_num_mst=" and b.job_no_mst=$job_no";
	if ($po_no=='') $po_no_no=""; else $po_no_no=" and d.po_number in ($po_no)";
	if ($hidd_po=="") $po_id=""; else $po_id=" and d.id in ( $hidd_po )";
	
	if($db_type==0)
	{
		if( $date_from=="" && $date_to=="" ) $booking_date_cond=""; else $booking_date_cond=" and a.booking_date between '".$date_from."' and '".$date_to."'";
		if( $date_from=="" && $date_to=="" ) $booking_date_cond_samp=""; else $booking_date_cond_samp=" and s.booking_date between '".$date_from."' and '".$date_to."'";
	}
	if($db_type==2)
	{
		if( $date_from=="" && $date_to=="" ) $booking_date_cond=""; else $booking_date_cond=" and a.booking_date between '".change_date_format($date_from,'dd-mm-yyyy','-',1)."' and '".change_date_format($date_to,'dd-mm-yyyy','-',1)."'";
	}
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$supplierArr=return_library_array( "select id,supplier_name from lib_supplier", "id", "supplier_name");	
	$buyerArr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name");
	$nonStyleArr=return_library_array( "select id, style_ref_no from sample_development_mst", "id", "style_ref_no");		
	if ($cbo_category==4) 
	{
		$colspan=18;
		$tbl_width=1400;
	}
	else
	{
		$colspan=19;
		$tbl_width=1500;
	}
	
	if($db_type==0)
	{
		 $job_sql="select a.job_no_prefix_num, a.job_no, Year(a.insert_date) as year, a.style_ref_no, a.garments_nature, 
		group_concat(b.id) as id,
		group_concat(b.po_number) as po_number
		from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.job_no_prefix_num, a.job_no, a.insert_date, a.style_ref_no, a.garments_nature";
	}
	elseif($db_type==2)
	{
		$job_sql="select a.job_no_prefix_num, a.job_no, TO_CHAR(a.insert_date,'YYYY') as year, a.style_ref_no, a.garments_nature, 
		LISTAGG(CAST(b.id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.id) as id,
		LISTAGG(CAST(b.po_number AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.po_number) as po_number
		from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.job_no_prefix_num, a.job_no, a.insert_date, a.style_ref_no, a.garments_nature";
	}
	$job_sql_result=sql_select($job_sql);
	foreach($job_sql_result as $row)
	{
		$order_no=implode(",",array_unique(explode(",",$row[csf("po_number")])));
		$order_id=implode(",",array_unique(explode(",",$row[csf("id")])));

		$job_array[$row[csf("job_no")]]['job']=$row[csf("job_no_prefix_num")];
		$job_array[$row[csf("job_no")]]['job_no']=$row[csf("job_no")];
		$job_array[$row[csf("job_no")]]['year']=$row[csf("year")];
		$job_array[$row[csf("job_no")]]['style_ref_no']=$row[csf("style_ref_no")];
		$job_array[$row[csf("job_no")]]['garments_nature']=$row[csf("garments_nature")];
		$job_array[$row[csf("job_no")]]['id']=$order_id;
		$job_array[$row[csf("job_no")]]['po_number']=$order_no;
	}
	
	$emb_item_array=array();
	
	$emb_sql="select a.emb_name, b.item_number_id, c.job_no_mst from wo_pre_cost_embe_cost_dtls a, wo_po_color_size_breakdown b, wo_po_break_down c where a.job_no=b.job_no_mst and c.id=b.po_break_down_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0";
	$emb_sql_result=sql_select($emb_sql);
	foreach($emb_sql_result as $row)
	{
		$emb_item_array[$row[csf("job_no_mst")]]['emb_name']=$row[csf("emb_name")];
		$emb_item_array[$row[csf("job_no_mst")]]['item_number_id']=$row[csf("item_number_id")];
	}
	//var_dump($job_array);
	ob_start();
	?>
    <fieldset>
        <table width="<? echo $tbl_width; ?>"  cellspacing="0" >
            <tr class="form_caption" style="border:none;">
                <td align="center" style="border:none; font-size:18px;" colspan="<? echo $colspan; ?>">
                    <? echo $company_library[str_replace("'","",$cbo_company_id)]; ?>                                
                </td>
            </tr>
            <tr class="form_caption" style="border:none;">
                <td align="center" style="border:none;font-size:16px; font-weight:bold"  colspan="<? echo $colspan; ?>"> <? echo $report_title ;?></td>
            </tr>
            <tr class="form_caption" style="border:none;">
                <td align="center" style="border:none;font-size:12px; font-weight:bold"  colspan="<? echo $colspan; ?>"> <? echo "From ".change_date_format($date_from)." To ".change_date_format($date_to);?></td>
            </tr>
        </table>
        <table width="<? echo $tbl_width; ?>" cellspacing="0" border="1" class="rpt_table" rules="all">
            <thead>
                <th width="30">SL</th>    
                <th width="60">Wo No</th>
                <th width="60">Wo Year</th>
                <th width="70">Wo Date</th>
                <th width="100">Wo Type</th>
                <th width="100">Ready To Approved</th>
                <th width="80">Approval Status</th>
                <th width="100">Supplier</th>
                <th width="100">Buyer</th>
                <th width="70">Job No.</th>
                <th width="60">Job Year</th>
                <th width="100">Style Ref.</th>
                <th width="100">Order No</th>
                <th width="80">Fabric Nature</th>
                <th width="50">UOM</th>
                <?
                    if ($cbo_category==4)
                    {
                        ?>
                            <th width="100">Wo Qty</th>
                        <?
                    }
                    else
                    {
                        ?>
                            <th width="100">Wo Qty (Fin.)</th>
                            <th width="100">Wo Qty (Grey)</th>
                        <?
                    }
                ?>
                <th>Remarks</th>
            </thead>
        </table>
        <div style="max-height:350px; overflow-y:scroll; width:<? echo $tbl_width+19; ?>px" id="scroll_body" >
            <table cellspacing="0" cellpadding="0" border="1" class="rpt_table"  width="<? echo $tbl_width; ?>" rules="all" id="table_body" >
            <?
			if($db_type==0)
			{	
				if($wo_type==0)
				{
					if($job_no!='' || $po_no!='')
					{
						$sql="Select a.booking_type, a.is_short, a.booking_no, Year(a.insert_date) as year_book, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category, sum(b.fin_fab_qnty) as fin_fab_qnty, sum(b.grey_fab_qnty) as grey_fab_qnty, sum(b.wo_qnty) as wo_qnty, 
						b.uom, b.job_no,
						 0 as type from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $company_id $buyer_id $category_id $job_num $po_id $booking_date_cond $wo_no_cond group by a.booking_type, a.is_short, a.booking_no, a.insert_date, a.booking_no_prefix_num, a.booking_date, a.is_approved, a.supplier_id, a.buyer_id, a.item_category order by a.booking_no";//die;
					}
					else
					{
						$sql="Select a.booking_type, a.is_short, a.booking_no, Year(a.insert_date) as year_book, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category, sum(b.fin_fab_qnty) as fin_fab_qnty, sum(b.grey_fab_qnty) as grey_fab_qnty, sum(b.wo_qnty) as wo_qnty, b.uom, b.job_no, 0 as type 
						from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $company_id $buyer_id $category_id $job_num $po_id $booking_date_cond $wo_no_cond group by a.booking_type, a.is_short, a.booking_no, a.insert_date, a.booking_no_prefix_num, a.booking_date, a.is_approved, a.supplier_id, a.buyer_id, a.item_category 
						 union all
						 SELECT s.booking_type, s.is_short, s.booking_no, Year(s.insert_date) as year_book, s.booking_no_prefix_num, s.fabric_source, s.booking_date, s.is_approved, s.ready_to_approved, s.supplier_id, s.buyer_id, s.item_category, sum(t.finish_fabric) as fin_fab_qnty, sum(t.grey_fabric) as grey_fab_qnty, sum(t.trim_qty) as wo_qnty, t.uom, '' as job_no, 1 as type 
						 FROM wo_non_ord_samp_booking_mst s, wo_non_ord_samp_booking_dtls t WHERE s.booking_no=t.booking_no and s.status_active =1 and s.is_deleted=0 $company_id_sam $buyer_id_sam $category_id_sam $booking_date_cond_samp $wo_no_cond_sam group by s.booking_type, s.is_short, s.booking_no, s.insert_date, s.booking_no_prefix_num, s.booking_date, s.is_approved, s.supplier_id, s.buyer_id, s.item_category";//die;
					}
				}
				else if ($wo_type==1 || $wo_type==2 || $wo_type==3)
				{
					 $sql="Select a.booking_type, a.is_short, a.booking_no, Year(a.insert_date) as year_book, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category, sum(b.fin_fab_qnty) as fin_fab_qnty, sum(b.grey_fab_qnty) as grey_fab_qnty, sum(b.wo_qnty) as wo_qnty, b.uom, b.job_no, 0 as type 
					 from wo_booking_mst a, wo_booking_dtls b, wo_po_details_master c, wo_po_break_down d where a.booking_no=b.booking_no and c.job_no=d.job_no_mst and a.job_no=c.job_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.is_deleted=0 and c.status_active=1 and d.is_deleted=0 and d.status_active=1  $company_id $buyer_id $category_id $job_num $po_id $booking_date_cond $wo_no_cond $wo_type_cond $wo_type_cond_sam group by a.booking_type, a.is_short, a.booking_no, a.insert_date, a.booking_no_prefix_num, a.booking_date, a.is_approved, a.supplier_id, a.buyer_id, a.item_category order by a.booking_no";//die;
				}
				else
				{
					$sql="SELECT s.booking_type, s.is_short, s.booking_no, Year(s.insert_date) as year_book, s.booking_no_prefix_num, s.fabric_source, s.booking_date, s.is_approved, s.ready_to_approved, s.supplier_id, s.buyer_id, s.item_category, sum(t.finish_fabric) as fin_fab_qnty, sum(t.grey_fabric) as grey_fab_qnty, sum(t.trim_qty) as wo_qnty, t.uom, '' as job_no, 1 as type  
					FROM wo_non_ord_samp_booking_mst s, wo_non_ord_samp_booking_dtls t WHERE s.booking_no=t.booking_no and s.status_active =1 and s.is_deleted=0 $company_id_sam $buyer_id_sam $category_id_sam $booking_date_cond_samp $wo_no_cond_sam group by s.booking_type, s.is_short, s.booking_no, s.insert_date, s.booking_no_prefix_num, s.booking_date, s.is_approved, s.supplier_id, s.buyer_id, s.item_category";//die;
				}
			}
			else if ($db_type==2)
			{
				if($wo_type==0)
				{
					if($job_no!='' || $po_no!='')
					{
						$sql="Select a.booking_type, a.is_short, a.booking_no, TO_CHAR(a.insert_date,'YYYY') as year_book, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category, sum(b.fin_fab_qnty) as fin_fab_qnty, sum(b.grey_fab_qnty) as grey_fab_qnty, sum(b.wo_qnty) as wo_qnty, 
						max(b.uom) as uom, 
						LISTAGG(CAST(b.job_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.job_no) as job_no,
						 0 as type from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $company_id $buyer_id $category_id $job_num  $booking_date_cond $wo_no_cond group by a.booking_type, a.is_short, a.booking_no, a.insert_date, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category order by a.booking_no";//die;
					}
					else
					{
						$sql="Select a.booking_type, a.is_short, a.booking_no, TO_CHAR(a.insert_date,'YYYY') as year_book, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category, sum(b.fin_fab_qnty) as fin_fab_qnty, sum(b.grey_fab_qnty) as grey_fab_qnty, sum(b.wo_qnty) as wo_qnty, max(b.uom) as uom,
						b.job_no, 0 as type 
						from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $company_id $buyer_id $category_id $job_num $po_id $booking_date_cond $wo_no_cond group by a.booking_type, a.is_short, a.booking_no, a.insert_date, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category,b.job_no 
						 union all
						 SELECT s.booking_type, s.is_short, s.booking_no, TO_CHAR(s.insert_date,'YYYY') as year_book, s.booking_no_prefix_num, s.fabric_source, s.booking_date, s.is_approved, s.ready_to_approved, s.supplier_id, s.buyer_id, s.item_category, sum(t.finish_fabric) as fin_fab_qnty, sum(t.grey_fabric) as grey_fab_qnty, sum(t.trim_qty) as wo_qnty, max(t.uom) as uom, null as job_no, 1 as type 
						 FROM wo_non_ord_samp_booking_mst s, wo_non_ord_samp_booking_dtls t WHERE s.booking_no=t.booking_no and s.status_active =1 and s.is_deleted=0 $company_id_sam $buyer_id_sam $category_id_sam $booking_date_cond_samp $wo_no_cond_sam group by s.booking_type, s.is_short, s.booking_no, s.insert_date, s.booking_no_prefix_num, s.fabric_source, s.booking_date, s.is_approved, s.ready_to_approved, s.supplier_id, s.buyer_id, s.item_category";//die;//LISTAGG(CAST(b.job_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.job_no) as job_no,
					}
				}
				else if ($wo_type==1 || $wo_type==2 || $wo_type==3)
				{
					 $sql="Select a.booking_type, a.is_short, a.booking_no, TO_CHAR(a.insert_date,'YYYY') as year_book, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category, sum(b.fin_fab_qnty) as fin_fab_qnty, sum(b.grey_fab_qnty) as grey_fab_qnty, sum(b.wo_qnty) as wo_qnty, max(b.uom) as uom,
					 LISTAGG(CAST(b.job_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.job_no) as job_no,
					  0 as type 
					 from wo_booking_mst a, wo_booking_dtls b, wo_po_details_master c, wo_po_break_down d where a.booking_no=b.booking_no and c.job_no=d.job_no_mst and a.job_no=c.job_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.is_deleted=0 and c.status_active=1 and d.is_deleted=0 and d.status_active=1  $company_id $buyer_id $category_id $job_num $po_id $booking_date_cond $wo_no_cond $wo_type_cond $wo_type_cond_sam group by a.booking_type, a.is_short, a.booking_no, a.insert_date, a.booking_no_prefix_num, a.fabric_source, a.booking_date, a.is_approved, a.ready_to_approved, a.supplier_id, a.buyer_id, a.item_category order by a.booking_no";//die;
				}
				else
				{
					$sql="SELECT s.booking_type, s.is_short, s.booking_no, TO_CHAR(s.insert_date,'YYYY') as year_book, s.booking_no_prefix_num, s.fabric_source, s.booking_date, s.is_approved, s.ready_to_approved, s.supplier_id, s.buyer_id, s.item_category, sum(t.finish_fabric) as fin_fab_qnty, sum(t.grey_fabric) as grey_fab_qnty, sum(t.trim_qty) as wo_qnty, max(t.uom) as uom, null as job_no, 1 as type  
					FROM wo_non_ord_samp_booking_mst s, wo_non_ord_samp_booking_dtls t WHERE s.booking_no=t.booking_no and s.status_active =1 and s.is_deleted=0 $company_id_sam $buyer_id_sam $category_id_sam $booking_date_cond_samp $wo_no_cond_sam group by s.booking_type, s.is_short, s.booking_no, s.insert_date, s.booking_no_prefix_num, s.fabric_source, s.booking_date, s.is_approved, s.ready_to_approved, s.supplier_id, s.buyer_id, s.item_category";//die;
				}	
			}
			//echo $sql;
			$sql_result=sql_select($sql); $i=1;
			foreach($sql_result as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				if ($row[csf("type")]==0)
				{	
					if ($row[csf("booking_type")]==1 || $row[csf("booking_type")]==2  || $row[csf("booking_type")]==3  || $row[csf("booking_type")]==6)
					{
						if ($row[csf("is_short")]==1)
						{
							$wo_type="Short";
							$wo_typw_id=1;
						}
						else
						{
							$wo_type="Main";
							$wo_typw_id=2;
						}
					}
					elseif($row[csf("booking_type")]==4)
					{
						$wo_type="Sample With Order";
						$wo_typw_id=3;
					}
				}
				else
				{
					$wo_type="Sample Non Order";
					$wo_typw_id=4;
				}
				$job_number=implode(",",array_unique(explode(",",$row[csf("job_no")])));
				?>
				<tr bgcolor="<? echo $bgcolor; ?>" onClick="change_color('tr_1nd<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_1nd<? echo $i; ?>">
					<td width="30"><? echo $i;?></td>
					<td width="60" align="center"><a href='#report_details' onClick="generate_fabric_report('<? echo $wo_typw_id; ?>','<? echo $row[csf("booking_no")]; ?>','<? echo $cbo_company; ?>','<? echo $job_array[$row[csf("job_no")]]['id']; ?>','<? echo $cbo_category; ?>','<? echo $row[csf("fabric_source")]; ?>','<? echo $job_array[$row[csf("job_no")]]['job_no']; ?>','<? echo $row[csf("is_approved")]; ?>','<? echo $cbo_category; ?>','<? echo $row[csf("is_short")]; ?>','<? echo $emb_item_array[$row[csf("job_no")]]['emb_name']; ?>','<? echo $emb_item_array[$row[csf("job_no")]]['item_number_id']; ?>');"><? echo $row[csf("booking_no_prefix_num")]; ?></a></td>	
                    <td width="60" align="center"><p><? echo $row[csf("year_book")]; ?></td>
					<td width="70" align="center"><p><? echo change_date_format($row[csf("booking_date")]); ?></p></td>
					<td width="100"><p><? echo $wo_type;?></p></td>
					<td width="100" align="center"><? echo $yes_no[$row[csf("ready_to_approved")]]; ?></td>
					<td width="80"><p><? echo $approval_type[$row[csf("is_approved")]]; ?></p></td>
					<td width="100"><p><? echo $supplierArr[$row[csf("supplier_id")]]; ?></p></td>
					<td width="100"><p><? echo $buyerArr[$row[csf("buyer_id")]]; ?></p></td>
                    <td width="70"><? echo $job_array[$job_number]['job'];//$row[csf("job_no_prefix_num")]; ?></td>
                    <td width="60"><? echo $job_array[$job_number]['year']; ?></td>
					<td width="100"><p><? if ($row[csf("type")]==0) echo $job_array[$job_number]['style_ref_no']; else echo $nonStyleArr[$job_array[$job_number]['style_ref_no']]; ?></p></td>
					<td width="100"><p><? echo $job_array[$job_number]['po_number']; ?></p></td>
					<td width="80"><p><? echo $item_category[$row[csf("item_category")]]; ?></p></td>
					<td width="50"><p><? if ($cbo_category==2) echo "KG"; else echo $unit_of_measurement[$row[csf("uom")]]; ?></p></td>
                    <?
						if ($cbo_category==4)
						{
							?>
                                <td width="100" align="right"><? echo number_format($row[csf("wo_qnty")]); ?>&nbsp;</td>
							<?
						}
						else
						{
							?>
                                <td width="100" align="right"><? echo number_format($row[csf("fin_fab_qnty")]); ?>&nbsp;</td>
                                <td width="100" align="right"><? echo number_format($row[csf("grey_fab_qnty")]); ?>&nbsp;</td>
							<?
						}
					?>
					<td width=""><? //echo $row[csf("po_number")]; ?>&nbsp;</td>
				</tr>
				<?
				$tot_wo_qnty+=$row[csf("wo_qnty")];
				$tot_fin_fab_qnty+=$row[csf("fin_fab_qnty")];
				$tot_grey_fab_qnty+=$row[csf("grey_fab_qnty")];
				$i++;
			}
			?>
            </table>
            <table class="rpt_table" width="<? echo $tbl_width; ?>" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tfoot>
                    <th width="30">&nbsp;</th>
                    <th width="60">&nbsp;</th>
                    <th width="60">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="60">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">Total</th>
                    <th width="50">&nbsp;</th>
					<?
						if ($cbo_category==4)
						{
							?>
                                <th width="100" align="right" id="tot_fin_fab_qnty"><? echo number_format($tot_wo_qnty,2); ?></th>
							<?
						}
						else
						{
							?>
                                <th width="100" align="right" id="tot_fin_fab_qnty"><? echo number_format($tot_fin_fab_qnty,2); ?></th>
                                <th width="100" align="right" id="tot_grey_fab_qnty"><? echo number_format($tot_grey_fab_qnty,2); ?></th>
							<?
						}
					?>    
                    <th width="">&nbsp;</th>
                </tfoot>
            </table>
        </div>
    </fieldset>
	<?
    foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	//echo "$total_data####$filename";
	exit();
}
?>
