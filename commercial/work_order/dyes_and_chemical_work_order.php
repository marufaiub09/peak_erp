<?php
/*-------------------------------------------- Comments

Purpose			: 	Yarn Work order entry
					
Functionality	:	
				

JS Functions	:

Created by		:	Bilas
Creation date 	: 	22-04-13
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:
Report	By		:	Aziz

*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Work Order","../../", 1, 1, $unicode,'',''); 

$color_sql = execute_query("select id,color_name from lib_color order by id");
$color_name = "";
while( $result = mysql_fetch_array($color_sql))
{ 
	$color_name.= "{value:'".$result[csf('color_name')]."',id:".$result[csf('id')]."},";
}
 
?> 

<script>

var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";

function fn_disable_enable(str)
{
	
	if( form_validation('cbo_company_name*cbo_item_category','Company Name*Item Category')==false )
	{
		$("#cbo_wo_basis").val(0);
		return;
	}
	if(str==1)
	{
 		$("#txt_req_numbers").attr("disabled",false);
	}
	else
	{
		$("#txt_req_numbers").val('');
 		$("#txt_req_numbers").attr("disabled",true);
	}
}

 
// for buyer po
function openmypage()
{
	
	var company = $("#cbo_company_name").val();
	var category = $("#cbo_item_category").val();
	var garments_nature = $("#garments_nature").val(); 
	var txt_req_dtls_id = $("#txt_req_dtls_id").val(); // if value has then it will be selected
	var req_numbers		= $("#txt_req_numbers").val();
	var req_numbers_id  = $("#txt_req_numbers_id").val();
	
 	var page_link = 'requires/dyes_and_chemical_work_order_controller.php?action=requitision_popup&company='+company+'&category='+category+'&garments_nature='+garments_nature+'&txt_req_dtls_id='+txt_req_dtls_id+'&req_numbers='+req_numbers+'&req_numbers_id='+req_numbers_id;
	var title = "Requisition No Search"; 
	
	if( form_validation('cbo_company_name*cbo_item_category','Company Name*Item Category')==false )
	{
		return;
	}
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
 		var requisition_id=this.contentDoc.getElementById("txt_selected_ids").value; // Requisition ID
		var requisition_number=this.contentDoc.getElementById("txt_selected_numbers").value; // Requisition Number
		var req_dtlsID=this.contentDoc.getElementById("txt_selected_dtls_id").value; // item id
 		
		var existing_req_dtlsID 	= ( $("#txt_req_dtls_id").val() ).split(",");
		var additional_req_dtlsID 	= req_dtlsID.split(",");
		if(existing_req_dtlsID!="")
			var unique_req_dtlsID		= additional_req_dtlsID.diff(existing_req_dtlsID);
		else
			var unique_req_dtlsID		= req_dtlsID;
			
 		$("#txt_req_numbers").val(requisition_number);
		$("#txt_req_numbers_id").val(requisition_id); 
		$("#txt_req_dtls_id").val(req_dtlsID);
		
		//if($("#txt_req_numbers").val()!="") $("#txt_req_numbers").val(","+requisition_number); else $("#txt_req_numbers").val(requisition_number); 
		//if($("#txt_req_numbers_id").val()!="") $("#txt_req_numbers_id").val(","+requisition_id); else $("#txt_req_numbers_id").val(requisition_id); 
		//if($("#txt_req_dtls_id").val()!="") $("#txt_req_dtls_id").val(","+req_dtlsID); else $("#txt_req_dtls_id").val(req_dtlsID); 
 		 
		if(requisition_number!="")
		{			
			freeze_window(5);			
			//show_list_view(requisition_id+'**'+req_dtlsID,'show_dtls_listview','details_container','requires/dyes_and_chemical_work_order_controller','');
			/*var row = $("#tbl_details tr:last").attr('id');	 
			var responseHtml = return_ajax_request_value(requisition_id+'**'+unique_req_dtlsID+'**'+row, 'show_dtls_listview', 'requires/dyes_and_chemical_work_order_controller');
			$("#tbl_details").append(responseHtml); */	
			
			var row = 0;	 
			var responseHtml = return_ajax_request_value(requisition_id+'**'+req_dtlsID+'**'+row, 'show_dtls_listview', 'requires/dyes_and_chemical_work_order_controller');
			$('#tbl_details tr:not(:first)').remove();
			$("#tbl_details").append(responseHtml); 	 			
 			release_freezing();
		}
		else
		{
			$("#details_container").html('');
		}
	}
}

   
function calculate_yarn_consumption_ratio(i)
{
	var cbocount=$('#txt_quantity_'+i).val();
	var cbocompone=$('#txt_rate_'+i).val();
	var amount =  cbocount*1*cbocompone*1;
	$('#txt_amount_'+i).val(amount.toFixed(2));
 }

function open_terms_condition_popup(page_link,title)
{
	var txt_wo_number=document.getElementById('update_id').value;
	if (txt_wo_number=="")
	{
		alert("Save The Yarn Work Order First");
		return;
	}	
	else
	{
	    page_link=page_link+get_submitted_data_string('update_id','../../');
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=720px,height=370px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function(){};
	}
}

function fn_inc_decr_row(rowid,type)
{
	
	if(type=="increase")
	{				
		var row = $("#tbl_details tr:last").attr('id');	
		var valuesLastRow = $("#tbl_details tr:last").find('input[name=txt_item_desc_'+row+']').val();
		if(valuesLastRow!="")
		{
			row = row*1+1;			
			if( form_validation('cbo_company_name*cbo_item_category','Company Name*Item Category')==false )	return; 			
			var company = $("#cbo_company_name").val();
			var itemCategory = $("#cbo_item_category").val();
			var responseHtml = return_ajax_request_value(row+'**'+company+'**'+itemCategory, 'append_load_details_container', 'requires/dyes_and_chemical_work_order_controller');
			$("#tbl_details tbody").append(responseHtml); 
		}
	}
	else if(type=="decrease")
	{ 			
		var row = $("#tbl_details tr").length-1;
		if(rowid*1!="" && row*1>1)
		{ 								 
			var vals = $("#txt_delete_row").val();
			var delID = $("#txt_row_id_"+rowid).val();
 			if(vals!="")
 				$("#txt_delete_row").val(vals+','+delID);
			else
				$("#txt_delete_row").val(delID);				
			
			//------------------remove--------------//	
			var dtlsID = $("#txt_req_dtls_id_"+rowid).val();
			var dtldArr = ($("#txt_req_dtls_id").val()).split(','); 
			$("#txt_req_dtls_id").val(dtldArr.remove(dtlsID));
			$("#tbl_details tr#"+rowid).remove(); 
  		}
		else
			return;		
	}
}


function fnc_chemical_order_entry(operation)
{
	
	if(operation==4)
	 {
		 /*print_report( $('#cbo_company_name').val()+'*'+$('#txt_wo_number').val()+'*'+$('#cbo_item_category').val()+'*'+$('#cbo_supplier').val()+'*'+$('#txt_wo_date').val()+'*'+$('#cbo_currency').val()+'*'+$('#cbo_wo_basis').val()+'*'+$('#cbo_pay_mode').val()+'*'+$('#cbo_source').val()+'*'+$('#txt_delivery_date').val()+'*'+$('#txt_attention').val()+'*'+$('#txt_req_numbers').val()+'*'+$('#txt_req_numbers_id').val()+'*'+$('#txt_delete_row').val()+'*'+$('#txt_delivery_place').val(), "dyes_chemical_work_print", "requires/dyes_and_chemical_work_order_controller" ) */
		  var form_caption=$( "div.form_caption" ).html();
		 print_report( $('#cbo_company_name').val()+'*'+$('#update_id').val()+'*'+form_caption, "dyes_chemical_work_print", "requires/dyes_and_chemical_work_order_controller" )
		 
		 return;
	 }
	 else if(operation==0 || operation==1 || operation==2)
	 {
		if( form_validation('cbo_company_name*cbo_item_category*cbo_supplier*txt_wo_date*cbo_currency*cbo_wo_basis*cbo_pay_mode*cbo_source*txt_delivery_date','Company Name*Item Category*Supplier Name*WO Date*Currency*WO Basis*Pay Mode*Source*Delivery Date')==false )
		{
			return;
		}
		
		if($("#cbo_wo_basis").val()==1 && form_validation('txt_req_numbers','Requisition NO')==false ) //buyer po basis
		{
			return;
		}
		else
		{
		try
		{
			var row = $("#tbl_details tr:last").attr('id'); 
			if(row<=0) throw "Save Not Possible!!Input Item Details For Save";
		}
		catch(err)
		{
			alert("Error : "+err);
			return;
		}
		
		// save data here
		var detailsData="";
		for(var i=1;i<=row;i++)
		{
			try{ 
					if( form_validation('txt_item_desc_'+i+'*cbogroup_'+i+'*cbouom_'+i+'*txt_quantity_'+i+'*txt_rate_'+i+'*txt_amount_'+i,'Item Account*Item Description*Item Size*Item Group*UOM*Work Order Quantity*Rate*Amount')==false)
					{
						return;
					}
					if( $("#txt_quantity_"+i).val()*1 <= 0 || $("#txt_rate_"+i).val()*1 <= 0)
					{
						alert("Quantity OR Rate Can not be 0 or less than 0");
						$("#txt_quantity_"+i).focus();
						return;
					}
					//detailsData+='*txt_req_dtls_id_'+i+'*txt_item_id_'+i+'*txt_req_no_id_'+i+'*txt_req_no_'+i+'*txt_item_acct_'+i+'*txt_item_desc_'+i+'*txt_item_size_'+i+'*cbogroup_'+i+'*txt_remarks_'+i+'*cbouom_'+i+'*txt_req_qnty_'+i+'*txt_quantity_'+i+'*txt_rate_'+i+'*txt_amount_'+i+'*txt_row_id_'+i;
					detailsData+='*txt_req_dtls_id_'+i+'*txt_item_id_'+i+'*txt_req_no_id_'+i+'*txt_req_no_'+i+'*txt_item_acct_'+i+'*txt_item_desc_'+i+'*txt_item_size_'+i+'*cbogroup_'+i+'*cbouom_'+i+'*txt_req_qnty_'+i+'*txt_quantity_'+i+'*txt_rate_'+i+'*txt_amount_'+i+'*txt_row_id_'+i+'*txt_remarks_'+i;
				}
			catch(err){}
		}
			//alert(detailsData);return;
			
			var is_approved=$('#is_approved').val();//Chech The Approval requisition item.. Change not allowed
					
			if(is_approved==1)
			{
				alert("This Order is Approved. So Change Not Allowed");
				return;	
			}
			
			//alert("ddd");return;
			var data="action=save_update_delete&operation="+operation+'&total_row='+row+get_submitted_data_string('garments_nature*txt_wo_number*cbo_company_name*cbo_item_category*cbo_supplier*txt_wo_date*cbo_currency*cbo_wo_basis*cbo_pay_mode*cbo_source*txt_delivery_date*txt_attention*txt_req_numbers*txt_req_numbers_id*txt_delivery_place*txt_delete_row*update_id'+detailsData,"../../");
			//alert(data);return
			freeze_window(operation);
			http.open("POST","requires/dyes_and_chemical_work_order_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			 
			http.onreadystatechange = fnc_chemical_order_entry_reponse;
		}
	 
	 }
	
}

function fnc_chemical_order_entry_reponse()
{
	
	if(http.readyState == 4) 
	{
		
		
	    var reponse=trim(http.responseText).split('**');
		//alert(http.responseText);
		show_msg(trim(reponse[0]));
		if(reponse[0]==0 || reponse[0]==1)
		{
			$("#txt_wo_number").val(reponse[1]);
			$("#update_id").val(reponse[2]);
			show_list_view(reponse[2],'show_dtls_listview_update','details_container','requires/dyes_and_chemical_work_order_controller','');
			set_button_status(1, permission, 'fnc_chemical_order_entry',1);	
		}
 		release_freezing();
		//reset_form('chemicalWorkOrder_1','details_container','','','','cbo_item_category*cbo_currency');
	}
}


function openmypage_wo()
{
	
	if( form_validation('cbo_company_name*cbo_item_category','Company Name*Item Category')==false )
	{
		return;
	}
	
	var company = $("#cbo_company_name").val();
	var itemCategory = $("#cbo_item_category").val();
	var garments_nature = $("#garments_nature").val();
	var page_link = 'requires/dyes_and_chemical_work_order_controller.php?action=wo_popup&company='+company+'&itemCategory='+itemCategory+'&garments_nature='+garments_nature;
	var title = "Order Search"; 
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=380px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		freeze_window(5);
		var theform=this.contentDoc.forms[0];
		var hidden_wo_number=this.contentDoc.getElementById("hidden_wo_number").value.split("_"); 
		
 		$("#txt_wo_number").val(hidden_wo_number[0]);
		$('#update_id').val(hidden_wo_number[1]);
		//reset_form('yarnWorkOrder_1','details_container','','','','cbo_item_category*cbo_currency');
		//alert("x");
		get_php_form_data(hidden_wo_number[1], "populate_data_from_search_popup", "requires/dyes_and_chemical_work_order_controller" );
		show_list_view(hidden_wo_number[1],'show_dtls_listview_update','details_container','requires/dyes_and_chemical_work_order_controller','');
		set_button_status(1, permission, 'fnc_chemical_order_entry',1);		
		release_freezing();
	}
	
}


function itemDetailsPopup()
{
	var company = $("#cbo_company_name").val();
	var itemCategory = $("#cbo_item_category").val(); 	
	var tot_row = $("#tbl_details tr:last").attr('id');	
	var itemIDS="";
	for(var i=1;i<=tot_row;i++)
	{
 		try
		{ 		
			if(itemIDS=="") itemIDS = $("#txt_item_id_"+i).val(); 
			else
			{ 
				if($("#txt_item_id_"+i).val() != "")
					itemIDS +=","+$("#txt_item_id_"+i).val();
			}
 		}
		catch(err){}
 	}
	
	var page_link = 'requires/dyes_and_chemical_work_order_controller.php?action=account_order_popup&company='+company+'&itemCategory='+itemCategory+'&itemIDS='+itemIDS;
	var title = 'Search Item Details';
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px,height=400px,center=1,resize=0,scrolling=0','../')
	
	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("item_1").value; 
 		if(theemail!="")
		{
	   
 			var data=theemail+"**"+tot_row+"**"+itemCategory;	
			var list_view_orders = return_global_ajax_value( data, 'load_php_popup_to_form', '', 'requires/dyes_and_chemical_work_order_controller');
  			
			$("#tbl_details tbody tr:last").remove();
				 
			$("#tbl_details tbody:last").append(list_view_orders);	
			set_all_onclick();
		}
		 
		release_freezing();
	}
		
}


// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === val) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
}
 
// two array diffrence
Array.prototype.diff = function(a) {
    return this.filter(function(i) {return !(a.indexOf(i) > -1);});
};

</script>	



<body onLoad="set_hotkey()">
<div align="center">
    <div style="width:1050px;">
        <?php echo load_freeze_divs ("../../",$permission);  ?><br />
    </div>
	    
		<fieldset style="width:1100px">
			<form name="chemicalWorkOrder_1" id="chemicalWorkOrder_1" method="" >
				<table cellpadding="0" cellspacing="2" width="900">
					<tr>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td><input type="hidden" name="update_id" id="update_id" value="">
					  <td>WO Number</td><input type="hidden" name="is_approved" id="is_approved" value="">
					  <td><input type="text" name="txt_wo_number"  id="txt_wo_number" class="text_boxes" style="width:159px" placeholder="Double Click to Search" onDblClick="openmypage_wo('x','WO Number Search');" readonly />
                      </td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
				  </tr>
					<tr>
						<td width="100" class="must_entry_caption">Company</td>
						<td width="170">
                        	<?php
							   	echo create_drop_down( "cbo_company_name", 170, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select --", 0, "load_drop_down( 'requires/dyes_and_chemical_work_order_controller', this.value, 'load_drop_down_supplier', 'supplier_td' );" );
 							?>
						</td>
                        <td width="130" class="must_entry_caption">Item Category</td>
						<td width="170"> 
                        	<?php
							   	//function create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index )
								echo create_drop_down( "cbo_item_category", 170, $item_category,"", 1, "-- Select --", 0, "load_drop_down( 'requires/dyes_and_chemical_work_order_controller', $('#cbo_wo_basis').val()+'**'+$('#cbo_company_name').val()+'**'+$('#cbo_item_category').val(), 'load_details_container', 'details_container' );",0,"5,6,7,23" );
 							?> 
                        </td>
						<td width="130" class="must_entry_caption">Supplier</td>
						<td width="170" id="supplier_td"> 
						  	<?php
							   	echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET(3,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
 							?>
						</td>
					</tr>
					<tr>
						
						<td width="" class="must_entry_caption">WO Date</td>
						<td width="">
							<input type="text" name="txt_wo_date" id="txt_wo_date" class="datepicker" style="width:159px"  />
 						</td>
						<td width="">Currency</td>
						<td width="">
                         	<?php
							   	echo create_drop_down( "cbo_currency", 170, $currency,"", 1, "-- Select --", 2, "",0 );
 							?>
                        </td>
                        <td width="" class="must_entry_caption">WO Basis</td>
						<td width="">
                        	<?php
							   	//create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index )
 								echo create_drop_down( "cbo_wo_basis", 170, $wo_basis,"", 1, "-- Select --", 0, "fn_disable_enable(this.value);load_drop_down( 'requires/dyes_and_chemical_work_order_controller', $('#cbo_wo_basis').val()+'**'+$('#cbo_company_name').val()+'**'+$('#cbo_item_category').val(), 'load_details_container', 'details_container' );",0,'','','','3' );
 							?>
                        </td>
					</tr>
					<tr>
                    	<td width="" class="must_entry_caption">Pay Mode</td>
						<td width="">
                        	<?php
							   	echo create_drop_down( "cbo_pay_mode", 170, $pay_mode,"", 1, "-- Select --", 0, "",0 );
 							?>
                        </td>
						<td width="" class="must_entry_caption">Source</td>
						<td width="">
                        	<?php
							   	echo create_drop_down( "cbo_source", 170, $source,"", 1, "-- Select --", 0, "",0 );
 							?>
                        </td> 
						<td width="" class="must_entry_caption">Delivery Date</td>
						<td width="">
							<input type="text" name="txt_delivery_date"  id="txt_delivery_date" class="datepicker"  style="width:159px" />
						</td>
					</tr>
					<tr>
						<td width="">Attention</td>
						<td width=""><input type="text" name="txt_attention"  id="txt_attention" style="width:159px " class="text_boxes" /></td>
						<td width="" >Requisition No</td>
						<td width="">
                        	<input type="text" name="txt_req_numbers"  id="txt_req_numbers" class="text_boxes" style="width:159px" placeholder="Double Click To Search" onDblClick="openmypage()" readonly disabled />
                            <input type="hidden" name="txt_req_numbers_id"  id="txt_req_numbers_id" readonly disabled />
                            <input type="hidden" name="txt_req_dtls_id"  id="txt_req_dtls_id" readonly disabled />
                            <!-- DELETED ROW ID HERE------> 
                            <input type="hidden" name="txt_delete_row"  id="txt_delete_row" readonly disabled />
                            <!-- DELETED ROW END------> 
                        </td>
						<td width="">Place Of Delivery</td>
						<td width=""><input type="text" name="txt_delivery_place"  id="txt_delivery_place"  style="width:159px" class="text_boxes" /></td>
					</tr>
					<tr>
						<td align="center" height="10" colspan="6">
                        	<input type="button" id="set_button" class="image_uploader" style="width:100px; margin-left:25px; margin-top:5px" value="Terms & Condition" onClick="open_terms_condition_popup('requires/dyes_and_chemical_work_order_controller.php?action=terms_condition_popup','Terms Condition')" />
                        </td>
					</tr>
                </table>
                <br />
                <div style="width:1100px" id="details_container" align="left">	</div>
                
				<table cellpadding="0" cellspacing="2" width="100%">
                	<tr>
				  		<td align="center" colspan="6" valign="middle" class="button_container"><div id="approved" style="float:left; font-size:24px; color:#FF0000;"></div>
							<?php
								//reset_form( forms, divs, fields, default_val, extra_func, non_refresh_ids ) 
								//load_submit_buttons( $permission, $sub_func, $is_update, $is_show_print, $refresh_function, $btn_id, $is_show_approve )
								echo load_submit_buttons( $permission, "fnc_chemical_order_entry", 0,1 ,"reset_form('chemicalWorkOrder_1','approved*details_container','','','','cbo_item_category*cbo_currency');$('#cbo_company_name').attr('disabled',false);$('#cbo_item_category').attr('disabled',false);$('#cbo_wo_basis').attr('disabled',false);",1); 
							?>
						</td>				
					</tr>
				</table> 
			</form>
		</fieldset>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>