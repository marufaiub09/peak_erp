﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------

if ($action=="load_drop_down_store")
{	
	$data=explode("**",$data);
	if($data[1]==2) $disable=1; else $disable=0; 
	echo create_drop_down( "cbo_store_name", 110, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET($data[0],company_id) and FIND_IN_SET(1,item_category_id) order by store_name","id,store_name", 1, "-- All Store--", 0, "",$disable );  	 
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,short_name from lib_supplier where status_active=1 and is_deleted=0","id","short_name"); 
	$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	
	$search_cond="";

	if($cbo_dyed_type==0) $search_cond .= ""; else if($cbo_dyed_type==1) $search_cond .= " and c.receive_purpose=2"; else if($cbo_dyed_type==2) $search_cond .= " and c.receive_purpose!=2";
	if($cbo_yarn_type==0) $search_cond .= ""; else $search_cond .= " and b.yarn_type=$cbo_yarn_type";
	if($txt_count=="") $search_cond .= ""; else $search_cond .= " and b.yarn_count_id in($txt_count)";
	if($txt_lot_no=="") $search_cond .= ""; else $search_cond .= " and b.lot like '$txt_lot_no%'";
	
	if($store_wise==1) 
	{
		if($store_name==0) $search_cond .=""; else $search_cond .= "  and a.store_id=$store_name";
		$group_by_cond="a.prod_id, a.store_id";
		$table_width='2000';
		$colspan='28';
		
		$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
	}
	else
	{
		if($cbo_value_with==0) $search_cond .=""; else $search_cond .= "  and b.current_stock>0";
		$group_by_cond="a.prod_id";
		$table_width='2100';
		$colspan='29';
	}
	
	$nameArray=sql_select( "select allocation from variable_settings_inventory where company_name=$cbo_company_name and item_category_id=1 and variable_list=18" );
	
	$allocated_qty_variable_settings=$nameArray[0][csf('allocation')];
	//echo $allocated_qty_variable_settings;die;
	//Master Query---------------------------------------------------- 
	$sql = "select a.prod_id, a.store_id, b.supplier_id, b.yarn_count_id, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color, b.lot, b.allocated_qnty, b.available_qnty,			
			sum(case when a.transaction_date<'".change_date_format($from_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as rcv_total_opening,
			sum(case when a.transaction_type in (1,5) and c.receive_purpose<>5 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as purchase,
			sum(case when a.transaction_type in (1,5) and c.receive_purpose=5 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as rcv_loan,
			sum(case when a.transaction_type=4 and c.knitting_source=1 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as rcv_inside_return,
			sum(case when a.transaction_type=4 and c.knitting_source=3 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as rcv_outside_return 			
			from inv_transaction a, product_details_master b, inv_receive_master c
			where a.prod_id=b.id and a.mst_id=c.id  and a.transaction_type in (1,4,5) and a.company_id=$cbo_company_name and a.item_category=1 and b.item_category_id=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $search_cond group by $group_by_cond order by b.yarn_count_id, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type ASC"; 
 	//echo $sql;
	$result = sql_select($sql);	
	$i=1;
	ob_start();	
	?>
        <div> 
            <table width="<?php echo $table_width; ?>" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
                <thead>
                    <tr class="form_caption" style="border:none;">
                        <td colspan="<?php echo $colspan; ?>" align="center" style="border:none;font-size:16px; font-weight:bold" >Daily Yarn Stock </td> 
                    </tr>
                    <tr style="border:none;">
                        <td colspan="<?php echo $colspan; ?>" align="center" style="border:none; font-size:14px;">
                            Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?>                                
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td colspan="<?php echo $colspan; ?>" align="center" style="border:none;font-size:12px; font-weight:bold">
                            <?php if($from_date!="" || $to_date!="")echo "From ".change_date_format($from_date,'dd-mm-yyyy')." To ".change_date_format($to_date,'dd-mm-yyyy')."" ;?>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="2" width="30">SL</th>
                        <th colspan="7">Description</th>
                        <th rowspan="2" width="110">Opening Stock</th>
                        <th colspan="5">Receive</th>
                        <th colspan="5">Delivery</th>
                        <th rowspan="2" width="100">Stock In Hand</th>
                        <?php 
							if($store_wise==1)
							{
								echo '<th rowspan="2" width="100">Store Name</th>';
							}
							else
							{
								echo '<th rowspan="2" width="100">Allocated to Order</th>';
								echo '<th rowspan="2" width="100">Un Allocated Qty.</th>';
							}
						?>
                        <th rowspan="2" width="50">Age (Days)</th>
                        <th rowspan="2" width="">DOH</th>
                    </tr> 
                    <tr>                         
                        <th width="60">Prod.ID</th>
                        <th width="60">Count</th>
                        <th width="100">Composition</th>
                        <th width="100">Yarn Type</th>
                        <th width="80">Color</th>
                        <th width="100">Lot</th>
                        <th width="80">Supplier</th>
                        <th width="90">Purchase</th>
                        <th width="90">Inside Return</th> 
                        <th width="90">Outside Return</th>
                        <th width="90">Loan</th>
                        <th width="100">Total Receved</th>
                        <th width="90">Inside</th>
                        <th width="90">Outside</th>
                        <th width="90">Receive Return</th>
                        <th width="90">Loan</th>
                        <th width="100">Total Delivery</th> 
                    </tr> 
                </thead>
          </table>  
          <div style="width:<?php echo $table_width+20; ?>px; overflow-y:scroll; max-height:250px" id="scroll_body" > 
          <table width="<?php echo $table_width; ?>" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body"  >   
			<?php
            foreach($result as $row)
            {
                if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
                if($row[csf("transaction_type")]==2 || $row[csf("transaction_type")]==3) 
                    $stylecolor='style="color:#A61000"';
                else
                    $stylecolor='style="color:#000000"'; 
                    
                $compositionDetails = $composition[$row[csf("yarn_comp_type1st")]]." ".$row[csf("yarn_comp_percent1st")]."%\n";
                if($row[csf("yarn_comp_type2nd")]!=0) $compositionDetails.=$composition[$row[csf("yarn_comp_type2nd")]]." ".$row[csf("yarn_comp_percent2nd")]."%";
                
				if($store_wise==1) $store_cond=" and a.store_id=".$row[csf("store_id")]; else $store_cond="";
				
                $issue_sql = "select
                        sum(case when a.transaction_date<'".change_date_format($from_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as issue_total_opening,
                        sum(case when c.knit_dye_source=1 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as issue_inside,
                        sum(case when c.knit_dye_source=3 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as issue_outside,
                        sum(case when c.entry_form=8 and a.transaction_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as rcv_return,
                        sum(case when c.issue_purpose=5 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as issue_loan		
                        from inv_transaction a, inv_issue_master c
                        where a.mst_id=c.id and a.transaction_type in (2,3,6) and a.prod_id=".$row[csf("prod_id")]." and a.item_category=1 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $store_cond"; 
               //echo $issue_sql;die;
                $issue_result = sql_select($issue_sql);
                                                    
                //opening balance of a product 
                //$openingBalance = return_field_value("sum(cons_quantity)","inv_transaction","prod_id=".$row[csf("prod_id")]." and transaction_date<'".change_date_format($from_date,'yyyy-mm-dd')."' and transaction_type in (1,4,5)"); and a.store_id=".$row[csf("store_id")]."
                $openingBalance = $row[csf("rcv_total_opening")]-$issue_result[0]['issue_total_opening'];
                
                $totalRcv = $row[csf("purchase")]+$row[csf("rcv_inside_return")]+$row[csf("rcv_outside_return")]+$row[csf("rcv_loan")];
                $totalIssue = $issue_result[0][csf("issue_inside")]+$issue_result[0][csf("issue_outside")]+$issue_result[0][csf("rcv_return")]+$issue_result[0][csf("issue_loan")];
                $stockInHand=$openingBalance+$totalRcv-$totalIssue;
                 
                //subtotal and group-----------------------
                $check_string=$row[csf("yarn_count_id")].$compositionDetails.$row[csf("yarn_type")];
                if( in_array($check_string,$checkArr) )
                {
                   $total_opening_balance+=$openingBalance;
                   $total_purchase+=$row[csf("purchase")];
                   $total_inside_return+=$row[csf("rcv_inside_return")];
                   $total_outside_return+=$row[csf("rcv_outside_return")];
                   $total_rcv_loan+=$row[csf("rcv_loan")];
                   $total_total_rcv+=$totalRcv;
                   $total_issue_inside+=$issue_result[0][csf("issue_inside")];
                   $total_issue_outside+=$issue_result[0][csf("issue_outside")];
                   $total_receive_return+=$issue_result[0][csf("rcv_return")];
                   $total_issue_loan+=$issue_result[0][csf("issue_loan")];
                   $total_total_delivery+=$totalIssue;
                   $total_stock_in_hand+=$stockInHand;
                   $total_alocatted +=$row[csf("allocated_qnty")];
                   $total_free_stock+=$row[csf("available_qnty")];
                }
                else
                {
                    if($i>1)
                    {
                    ?>
                        <tr bgcolor="#CCCCCC" style="font-weight:bold">
                            <td colspan="8" align="right">Sub Total</td>
                            <td width="110" align="right"><?php echo number_format($total_opening_balance,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_purchase,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_inside_return,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_outside_return,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_rcv_loan,2); ?></td>
                            <td width="100" align="right"><?php echo number_format($total_total_rcv,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_issue_inside,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_issue_outside,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_receive_return,2); ?></td>
                            <td width="90" align="right"><?php echo number_format($total_issue_loan,2); ?></td>
                            <td width="100" align="right"><?php echo number_format($total_total_delivery,2); ?></td> 
                            <td width="100" align="right"><?php echo number_format($total_stock_in_hand,2); ?></td>
                            <?php 
								if($store_wise==1) 
								{
									echo '<td width="100">&nbsp;</td>';
								}
								else
								{
									if($allocated_qty_variable_settings==1) echo '<td width="100" align="right">'.number_format($total_alocatted,2).'</td>'; else echo '<td width="100" align="right">&nbsp;</td>';
									if($allocated_qty_variable_settings==1) echo '<td width="100" align="right">'.number_format($total_free_stock,2).'</td>'; else echo '<td width="100" align="right">'.number_format($total_stock_in_hand,2).'</td>';
								} 
							?> 
                            <td width="50" align="right">&nbsp;</td> 
                            <td width="" align="right">&nbsp;</td> 
                        </tr>
                    <?php
                    }
                    $checkArr[]=$check_string;
                    $total_opening_balance=$openingBalance;
                    $total_purchase=$row[csf("purchase")];
                    $total_inside_return=$row[csf("rcv_inside_return")];
                    $total_outside_return=$row[csf("rcv_outside_return")];
                    $total_rcv_loan=$row[csf("rcv_loan")];
                    $total_total_rcv=$totalRcv;
                    $total_issue_inside=$issue_result[0][csf("issue_inside")];
                    $total_issue_outside=$issue_result[0][csf("issue_outside")];
                    $total_receive_return=$issue_result[0][csf("rcv_return")];
                    $total_issue_loan=$issue_result[0][csf("issue_loan")];
                    $total_total_delivery=$totalIssue;
                    $total_stock_in_hand=$stockInHand;
                    $total_alocatted=0;
                    $total_free_stock=0;
                }
                    //grand total===========================
                   $grand_total_opening_balance+=$openingBalance;
                   $grand_total_purchase+=$row[csf("purchase")];
                   $grand_total_inside_return+=$row[csf("rcv_inside_return")];
                   $grand_total_outside_return+=$row[csf("rcv_outside_return")];
                   $grand_total_rcv_loan+=$row[csf("rcv_loan")];
                   $grand_total_total_rcv+=$totalRcv;
                   $grand_total_issue_inside+=$issue_result[0][csf("issue_inside")];
                   $grand_total_issue_outside+=$issue_result[0][csf("issue_outside")];
                   $grand_total_receive_return+=$issue_result[0][csf("rcv_return")];
                   $grand_total_issue_loan+=$issue_result[0][csf("issue_loan")];
                   $grand_total_total_delivery+=$totalIssue;
                   $grand_total_stock_in_hand+=$stockInHand;
                   $grand_total_alocatted +=0;
                   $grand_total_free_stock+=0;
				?>                                 
					<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
						<td width="30"><?php echo $i; ?></td>								
						<td width="60"><?php echo $row[csf("prod_id")]; ?></td>                                 
						<td width="60"><p><?php echo $yarn_count_arr[$row[csf("yarn_count_id")]]; ?></p></td>
						<td width="100"><p><?php echo $compositionDetails; ?></p></td>
						<td width="100"><p><?php echo $yarn_type[$row[csf("yarn_type")]]; ?></p></td> 
						<td width="80"><p><?php echo $color_name_arr[$row[csf("color")]]; ?></p></td> 
						<td width="100"><p><?php echo $row[csf("lot")]; ?></p></td> 
						<td width="80"><p><?php echo $supplierArr[$row[csf("supplier_id")]]; ?></p></td>  
						<td width="110" align="right"><p><?php echo number_format($openingBalance,2); ?></p></td>
						
						<td width="90" align="right"><p><?php echo number_format($row[csf("purchase")],2); ?></p></td>
						<td width="90" align="right"><p><?php echo number_format($row[csf("rcv_inside_return")],2); ?></p></td>
						<td width="90" align="right"><p><?php echo number_format($row[csf("rcv_outside_return")],2); ?></p></td>
						<td width="90" align="right"><p><?php echo number_format($row[csf("rcv_loan")],2); ?></p></td>
						
						<td width="100" align="right"><p><?php echo number_format($totalRcv,2); ?></p></td>
						
						<td width="90" align="right"><p><?php echo number_format($issue_result[0][csf("issue_inside")],2); ?></p></td>
						<td width="90" align="right"><p><?php echo number_format($issue_result[0][csf("issue_outside")],2); ?></p></td>
						<td width="90" align="right"><p><?php echo number_format($issue_result[0][csf("rcv_return")],2); ?></p></td>
						<td width="90" align="right"><p><?php echo number_format($issue_result[0][csf("issue_loan")],2); ?></p></td>                                        
						<td width="100" align="right"><p><?php echo number_format($totalIssue,2); ?></p></td>
						<td width="100" align="right"><?php echo number_format($stockInHand,2); ?></td> 
						<?php
							if($store_wise==1) 
							{
								echo '<td width="100"><p>'.$store_arr[$row[csf("store_id")]].'</p></td>';
							}
							else
							{
								if($allocated_qty_variable_settings==1) echo '<td width="100" align="right">'.number_format($row[csf("allocated_qnty")],2).'</td>'; else echo '<td width="100" align="right">'.'</td>';
								if($allocated_qty_variable_settings==1) echo '<td width="100" align="right">'.number_format($row[csf("available_qnty")],2).'</td>'; else echo '<td width="100" align="right">'.number_format($stockInHand,2).'</td>';
							} 
							$returnRes=explode(",",return_field_value("concat(min(transaction_date),',',max(transaction_date))","inv_transaction","prod_id=".$row[csf("prod_id")]));
							//echo $returnRes;die;
							$ageOfDays = datediff("d",$returnRes[0],date("Y-m-d"));
							$daysOnHand = datediff("d",$returnRes[1],date("Y-m-d"));

						?>  
						<td width="50" align="right"><?php echo $ageOfDays; ?></td> 
						<td width="" align="right"><?php echo $daysOnHand; ?></td>
					</tr>
                <?php 												
                    $i++; 
                } 
				?> <!---- END FOREACH LOOP-----> 
                <tr bgcolor="#CCCCCC" style="font-weight:bold">
                    <td colspan="8" align="right">Sub Total</td>
                    <td width="110" align="right"><?php echo number_format($total_opening_balance,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_purchase,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_inside_return,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_outside_return,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_rcv_loan,2); ?></td>
                    <td width="100" align="right"><?php echo number_format($total_total_rcv,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_issue_inside,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_issue_outside,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_receive_return,2); ?></td>
                    <td width="90" align="right"><?php echo number_format($total_issue_loan,2); ?></td>
                    <td width="100" align="right"><?php echo number_format($total_total_delivery,2); ?></td>
                    <td width="100" align="right"><?php echo number_format($total_stock_in_hand,2); ?></td>
                    <?php 
						if($store_wise==1) 
						{
							echo '<td width="100">&nbsp;</td>';
						}
						else
						{
							if($allocated_qty_variable_settings==1) echo '<td width="100" align="right">'.number_format($total_alocatted,2).'</td>'; else echo '<td width="100" align="right">&nbsp;</td>';
							if($allocated_qty_variable_settings==1) echo '<td width="100" align="right">'.number_format($total_free_stock,2).'</td>'; else echo '<td width="100" align="right">'.number_format($total_stock_in_hand,2).'</td>';
						} 
					?> 
                    <td width="50" align="right">&nbsp;</td> 
                    <td width="" align="right">&nbsp;</td>  
                </tr>
            </table>  
        </div>  
        <table width="<?php echo $table_width; ?>" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_footer">  
            <tr class="tbl_bottom">
                <td width="30"></td>
                <td width="60"></td>
                <td width="60"></td>
                <td width="100"></td>
                <td width="100"></td>
                <td width="80"></td>
                <td width="100"></td>
                <td width="80" align="right">Grand Total</td>
                <td width="110" align="right" id="value_total_opening_balance"><?php echo number_format($grand_total_opening_balance,2); ?></td>
                <td width="90" align="right" id="value_total_purchase"><?php echo number_format($grand_total_purchase,2); ?></td>
                <td width="90" align="right" id="value_total_inside_return"><?php echo number_format($grand_total_inside_return,2); ?></td>
                <td width="90" align="right" id="value_total_outside_return"><?php echo number_format($grand_total_outside_return,2); ?></td>
                <td width="90" align="right" id="value_total_rcv_loan"><?php echo number_format($grand_total_rcv_loan,2); ?></td>
                <td width="100" align="right" id="value_total_total_rcv"><?php echo number_format($grand_total_total_rcv,2); ?></td>
                <td width="90" align="right" id="value_total_issue_inside"><?php echo number_format($grand_total_issue_inside,2); ?></td>
                <td width="90" align="right" id="value_total_issue_outside"><?php echo number_format($grand_total_issue_outside,2); ?></td>
                <td width="90" align="right" id="value_total_receive_return"><?php echo number_format($grand_total_receive_return,2); ?></td>
                <td width="90" align="right" id="value_total_issue_loan"><?php echo number_format($grand_total_issue_loan,2); ?></td>
                <td width="100" align="right" id="value_total_total_delivery"><?php echo number_format($grand_total_total_delivery,2); ?></td>
                <td width="100" align="right" id="value_total_stock_in_hand"><?php echo number_format($grand_total_stock_in_hand,2); ?></td>
                <?php 
					if($store_wise==1) 
					{
						echo '<td width="100">&nbsp;</td>';
					}
					else
					{
						if($allocated_qty_variable_settings==1) echo '<td width="100" align="right" id="value_total_alocatted">'.number_format($grand_total_alocatted,2).'</td>'; else echo '<td width="100" align="right" id="value_total_alocatted">&nbsp;</td>';
						if($allocated_qty_variable_settings==1) echo '<td width="100" align="right" id="value_total_free_stock">'.number_format($grand_total_free_stock,2).'</td>'; else echo '<td width="100" align="right" id="value_total_free_stock">'.number_format($grand_total_stock_in_hand,2).'</td>';
					} 
				?>  
                <td width="50" align="right">&nbsp;</td> 
                <td width="" align="right">&nbsp;</td>  
            </tr>
        </table>
    </div>    
	<?php	 
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}
?>
