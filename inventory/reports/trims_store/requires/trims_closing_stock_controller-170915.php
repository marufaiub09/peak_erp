<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
$trim_group= return_library_array( "select id, item_name from lib_item_group",'id','item_name');

if ($action=="item_description_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_desc_id').val( id );
		$('#item_desc_val').val( ddd );
	} 
		  
	</script>
     <input type="hidden" id="item_desc_id" />
     <input type="hidden" id="item_desc_val" />
 <?php
	if ($data[1]==0) $item_name =""; else $item_name =" and item_group_id in($data[1])";
	$sql="SELECT id, item_category_id, product_name_details from product_details_master where item_category_id=4 and status_active=1 and is_deleted=0 $item_name"; 
	$arr=array(0=>$item_category);
	echo  create_list_view("list_view", "Description,Product ID", "300,150","450","300",0, $sql , "js_set_value", "id,product_name_details", "", 1, "0,0,0", $arr , "product_name_details,id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
	exit();
}

if($action=="generate_report")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$determinaArr = return_library_array("select id,construction from lib_yarn_count_determina_mst where status_active=1 and is_deleted=0","id","construction");
	//echo $cbo_company_name;die;
	if ($cbo_company_name==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_name'";
	
	if($db_type==0) 
	{
		$from_date=change_date_format($from_date,'yyyy-mm-dd');
		$to_date=change_date_format($to_date,'yyyy-mm-dd');
	}
	else if($db_type==2) 
	{
		$from_date=change_date_format($from_date,'','',1);
		$to_date=change_date_format($to_date,'','',1);
	}
	else  
	{
		$from_date=""; $to_date="";
	}
	if ($cbo_item_group==0) 
	{
		$items_group=""; 
		$item="";
	}
	else 
	{
		 $items_group=" and b.prod_id in ($cbo_item_group)";
		$item=" and b.item_group_id in ($cbo_item_group)";
	}
	if ($item_description_id==0) 
	{
		$item_description=""; 
		$prod_cond="";
	}
	else 
	{
		$item_description=" and b.prod_id in ($item_description_id)";
		$prod_cond=" and b.id in ($item_description_id)";
	}
	$search_cond="";
	if($value_with==0) $search_cond =""; else $search_cond= "  and b.current_stock>0";
	
	$data_array=array();
	$trnasactionData=sql_select("Select b.id,
	sum(case when a.transaction_type in(1,4) and a.transaction_date<'".$from_date."' then a.cons_quantity else 0 end) as rcv_total_opening,
	sum(case when a.transaction_type in(2,3) and a.transaction_date<'".$from_date."' then a.cons_quantity else 0 end) as iss_total_opening,
	sum(case when a.transaction_type in(1) and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as receive,
	sum(case when a.transaction_type in(4) and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as issue_return,
	sum(case when a.transaction_type in(2) and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as issue,
	sum(case when a.transaction_type in(3) and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as receive_return
	from inv_transaction a, product_details_master b
	where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category_id=4 and a.item_category=4 and a.order_id=0 $company_id $item $prod_cond  group by b.id order by b.id ASC");	
	foreach($trnasactionData as $row)
	{
		$data_array[$row[csf("id")]]['rcv_total_opening']=$row[csf("rcv_total_opening")];
		$data_array[$row[csf("id")]]['iss_total_opening']=$row[csf("iss_total_opening")];
		$data_array[$row[csf("id")]]['receive']=$row[csf("receive")];
		$data_array[$row[csf("id")]]['issue']=$row[csf("issue")];
		$data_array[$row[csf("id")]]['receive_return']=$row[csf("receive_return")];
		$data_array[$row[csf("id")]]['issue_return']=$row[csf("issue_return")];
	}
	
	$date_array=array();
	$returnRes_date="select prod_id, min(transaction_date) as min_date, max(transaction_date) as max_date from inv_transaction where is_deleted=0 and status_active=1 and item_category=4 group by prod_id";
	$result_returnRes_date = sql_select($returnRes_date);
	foreach($result_returnRes_date as $row)	
	{
		$date_array[$row[csf("prod_id")]]['min_date']=$row[csf("min_date")];
		$date_array[$row[csf("prod_id")]]['max_date']=$row[csf("max_date")];
	}
	ob_start();	
	?>
    <div>
        <table style="width:1382px" border="1" cellpadding="2" cellspacing="0"  id="caption" rules="all"> 
            <thead>
                <tr class="form_caption" style="border:none;">
                    <td colspan="13" align="center" style="border:none;font-size:16px; font-weight:bold" ><?php echo $report_title; ?></td> 
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td colspan="13" align="center" style="border:none; font-size:14px;">
                       <b>Company Name : <?php echo $companyArr[$cbo_company_name]; ?></b>                               
                    </td>
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td colspan="13" align="center" style="border:none;font-size:12px; font-weight:bold">
                        <?php if($from_date!="" || $to_date!="") echo "From : ".change_date_format($from_date)." To : ".change_date_format($to_date)."" ;?>
                    </td>
                </tr>
            </thead>
        </table>
        <table width="1381" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
        	<thead>
               <tr>
                    <th rowspan="2" width="40">SL</th>
                    <th rowspan="2" width="60">Prod.ID</th>
                    <th colspan="2">Description</th>
                    <th rowspan="2" width="110">Opening Stock</th>
                    <th colspan="3">Receive</th>
                    <th colspan="3">Issue</th>
                    <th rowspan="2" width="100">Closing Stock</th>
                    <th rowspan="2" width="80">Age(Days)</th>
                    <th rowspan="2">DOH</th>
               </tr> 
               <tr>                         
                    <th width="120">Item Group</th>
                    <th width="180">Item Description</th>
                    <th width="80">Receive</th>
                    <th width="80">Issue Return</th>
                    <th width="100">Total Receive</th>
                    <th width="80">Issue</th>
                    <th width="80">Received Return</th>
                    <th width="100">Total Issue</th> 
               </tr> 
            </thead>
        </table>
        <div style="width:1400px; max-height:350px; overflow-y:scroll" id="scroll_body" > 
            <table width="1381" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_body">
            <?php
              $sql="select b.id, b.item_description,b.item_group_id, b.current_stock from product_details_master b where b.status_active=1 and b.is_deleted=0 and b.company_id='$cbo_company_name' and b.item_category_id='4' $item $prod_cond $search_cond order by b.id"; 	
                $result = sql_select($sql);
				$i=1;
                foreach($result as $row)
                {
                   if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
					$ageOfDays = datediff("d",$date_array[$row[csf("id")]]['min_date'],date("Y-m-d"));
					$daysOnHand = datediff("d",$date_array[$row[csf("id")]]['max_date'],date("Y-m-d")); 
    
                    $opening_bal=$data_array[$row[csf("id")]]['rcv_total_opening']-$data_array[$row[csf("id")]]['iss_total_opening'];

                    $receive = $data_array[$row[csf("id")]]['receive'];
					$issue = $data_array[$row[csf("id")]]['issue'];
					$issue_return=$data_array[$row[csf("id")]]['issue_return'];
					$receive_return=$data_array[$row[csf("id")]]['receive_return'];
                  
                    $tot_receive=$receive+$issue_return;
					$tot_issue=$issue+$receive_return;
									
					$closingStock=$opening_bal+$tot_receive-$tot_issue;
					if((($get_upto==1 && $ageOfDays>$txt_days) || ($get_upto==2 && $ageOfDays<$txt_days) || ($get_upto==3 && $ageOfDays>=$txt_days) || ($get_upto==4 && $ageOfDays<=$txt_days) || ($get_upto==5 && $ageOfDays==$txt_days) || $get_upto==0) && (($get_upto_qnty==1 && $closingStock>$txt_qnty) || ($get_upto_qnty==2 && $closingStock<$txt_qnty) || ($get_upto_qnty==3 && $closingStock>=$txt_qnty) || ($get_upto_qnty==4 && $closingStock<=$txt_qnty) || ($get_upto_qnty==5 && $closingStock==$txt_qnty) || $get_upto_qnty==0))
					{
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                            <td width="40"><?php echo $i; ?></td>	
                            <td width="60" align="center"><p><?php echo $row[csf("id")]; ?></p></td>
                            <td width="120"><p><?php echo $trim_group[$row[csf('item_group_id')]]; ?></p></td>
                            <td width="180"><?php echo $row[csf("item_description")]; ?></td>                                 
                            <td width="110" align="right"><p><?php echo number_format($opening_bal,2);  ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($receive,2); ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($issue_return,2); ?></p></td>
                            <td width="100" align="right"><p><?php echo number_format($tot_receive,2);  ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($issue,2);  ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($receive_return,2);  ?></p></td>
                            <td width="100" align="right"><p><?php echo number_format($tot_issue,2);  ?></p></td>
                            <td width="100" align="right"><p><?php echo number_format($closingStock,2); ?></p></td>
                            <td width="80" align="center"><?php echo $ageOfDays; ?></td>
                            <td align="center"><?php echo $daysOnHand; ?></td>
						</tr>
						<?php 
						$total_opening+=$opening_bal;
						$total_receive+=$receive;
						$total_issue_return+=$issue_return;
						$total_receive_balance+=$tot_receive;
						$total_issue+=$issue;
						$total_receive_return+=$receive_return;
						$total_issue_balance+=$tot_issue;
						$total_closing_stock+=$closingStock;								
						$i++; 				
					}
				}
				?>
            </table>
		</div> 
        <table width="1381" border="1" cellpadding="0" cellspacing="0" class="tbl_bottom" rules="all" > 
           <tr>
                <td width="40">&nbsp;</td>
                <td width="60">&nbsp;</td> 
                <td width="120">&nbsp;</td> 
                <td width="180" align="right">Total</td>
                <td width="110" align="right" id="total_opening_td"><?php echo number_format($total_opening,2); ?></td>
                <td width="80" align="right" id="total_receive_td"><?php echo number_format($total_receive,2); ?></td>
                <td width="80" align="right" id="total_issue_return_td"><?php echo number_format($total_issue_return,2); ?></td>
                <td width="100" align="right" id="total_receive_balance_td"><?php echo number_format($total_receive_balance,2); ?></td>
                <td width="80" align="right" id="total_issue_td"><?php echo number_format($total_issue,2); ?></td>
                <td width="80" align="right" id="total_receive_return_td"><?php echo number_format($total_receive_return,2); ?></td>
                <td width="100" align="right" id="total_issue_balance_td"><?php echo number_format($total_issue_balance,2); ?></td>
                <td width="100" align="right" id="total_closing_stock_td"><?php echo number_format($total_closing_stock,2); ?></td>
                <td width="80">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </div>
    <?php
	$html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}
?>