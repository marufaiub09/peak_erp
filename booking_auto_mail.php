<?php
date_default_timezone_set("Asia/Dhaka");

require_once('includes/common.php');
require_once('mailer/class.phpmailer.php');

$company_library=return_library_array( "select id, company_name from lib_company where id=1", "id", "company_name"  );
$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$supplier_library = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");

$next_date=add_date(date("Y-m-d"),1);
$prev_date=add_date(date("Y-m-d"),-1);

ob_start();

foreach($company_library as $compid=>$compname) /// Daily Order Entry
{
	if($db_type==0)
	{
		$current_date = date("Y-m-d H:i:s",strtotime(add_time(date("H:i:s",time()),0)));
		$previous_date = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($current_date))); 
	}
	else
	{
		$current_date = change_date_format(date("Y-m-d H:i:s",strtotime(add_time(date("H:i:s",time()),0))),'','',1);
		$previous_date = change_date_format(date('Y-m-d H:i:s', strtotime('-1 day', strtotime($current_date))),'','',1); 
	}
	//echo $previous_date;
	
	$a=mktime(0, 0, 0, date("m"), date("d")-1, date("Y"));
	//$b=$a+86399;
	
		$str_cond=" and insert_date between '".$previous_date."' and '".$current_date."'";
		$str_cond_a=" and a.insert_date between '".$previous_date."' and '".$current_date."'";
		$str_cond_b=" and b.insert_date between '".$previous_date."' and '".$current_date."'";
		$str_cond_c=" and c.insert_date between '".$previous_date."' and '".$current_date."'";
	
	//echo $str_cond_a;die;
	ob_start();
	?>
    
    <div style="width:920px">
        <table width="100%"  cellspacing="0" >
            <tr>
                <td colspan="9" align="center">
                    <font size="3">
                    <strong>Company Name:
                    <?php
                   
                    echo $company_library[$compid]; 
                    ?>
                    </strong>
                    </font>
                </td>
            </tr>
       </table>
    </div>
    <div align="center" style="width:920px;">
        <span style="font-size:14px; font-weight:bold">Daily Order Entry ( Date :<?php  echo date("d-m-Y", $a);  ?>)</span> 
    </div>
    
    <table width="920" border="1" rules="all" class="rpt_table" id="table_body3">
        <thead>
            <tr align="center">
                <th width="40">Sl</th>
                <th width="120">Company</th>
                <th width="100">Order No</th>
                <th width="100">Buyer</th>
                <th width="100">Style</th>
                <th width="100">Order Qty.</th>
                <th width="70">Unit Price</th>
                <th width="120">Value</th>
                <th width="100">Ship Date</th>
            </tr>
        </thead>
    </table>
    
    <table width="920" border="1" rules="all" class="rpt_table" id="table_body">
        <?php
        $i=0;
        $total_po_qty=0;
        $total_value=0;
        
		$sql_mst="select b.po_number,a.buyer_name,a.style_ref_no,b.po_quantity,b.unit_price,b.shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name like '$compid' and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 $str_cond_b";				
		$nameArray_mst=sql_select($sql_mst);
		$tot_rows=count($nameArray_mst);
		foreach($nameArray_mst as $row)
		{
            $i++;
            if ($i%2==0)  
                $bgcolor="#E9F3FF";
            else
                $bgcolor="#FFFFFF";
                
        ?>	
        <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                <td width="40" align="center"><?php echo $i;?></td>
                <td width="120">
                    <?php
                        echo $company_library[$compid];		
                    ?>
                </td>
                <td width="100"><?php echo $row[csf('po_number')]; ?></td>
                <td width="100"><?php echo $buyer_library[$row[csf('buyer_name')]]; ?></td>
                <td width="100"><?php echo $row[csf('style_ref_no')]; ?></td>
                <td width="100" align="right">
                    <?php 
                        echo number_format($row[csf('po_quantity')],2);
                        $total_po_qty+= $row[csf('po_quantity')];
                    ?>
                </td>
                <td width="70" align="right"><?php echo number_format($row[csf('unit_price')],2); ?></td>
                <td width="120" align="right">
                    <?php 
                            $value=$row[csf('po_quantity')]*$row[csf('unit_price')]; 
                            echo number_format($value,2);
                            $total_value+= $value;
                    ?>
                </td>
                <td width="100" align="center"><?php echo change_date_format($row[csf('shipment_date')]); ?></td>
            </tr>
        <?php
        }
		if($tot_rows==0)
		{
		?>
        	<tr><td colspan="9" align="center"><font size="+1"; color="#FF0000"><strong>NO ENTRY FOUND</strong></font></td></tr>
        
		<?php	
		}
        ?> 
              
    </table>
    <table width="920" border="1" class="rpt_table" rules="all">
           <tfoot>
                <th width="40">&nbsp;</th>
                <th width="120"><b>Total :</b></th>
                <th width="100">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100" align="right"><?php  echo number_format($total_po_qty,2); ?></th>
                <th width="70">&nbsp;</th>
                <th width="120" align="right"><?php  echo number_format($total_value,2); ?></th>
                <th width="100">&nbsp;</th>
          </tfoot>
        </table>
<?php

		$to="";
		
		$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=1 and b.mail_user_setup_id=c.id and a.company_id=$compid";
		
		$mail_sql=sql_select($sql);
		foreach($mail_sql as $row)
		{
			if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
		}
		
 		$subject="Daily Order Entry";
    	$message="";
    	$message=ob_get_contents();
    	//ob_clean();
		$header=mail_header();
		
		echo send_mail_mailer( $to, $subject, $message, $from_mail );
		
		/*if (mail($to,$subject,$message,$header))
			echo "****Mail Sent.---".date("Y-m-d");
		else
			echo "****Mail Not Sent.---".date("Y-m-d");*/
	 
}

?> 