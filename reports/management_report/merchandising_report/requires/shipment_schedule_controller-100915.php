﻿<?
include('../../../../includes/common.php');
session_start();
extract($_REQUEST);
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$date=date('Y-m-d');

$user_id=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 130, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
	exit(); 
} 

if($action=="load_drop_down_team_member")
{
	echo create_drop_down( "cbo_team_member", 120, "select id,team_member_name 	 from lib_mkt_team_member_info  where team_id='$data' and status_active=1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-Select Team Member-", $selected, "" );   	
	exit();  
}

if($action=="report_generate")
{
	$company_name=str_replace("'","",$cbo_company_name);
	$buyer_name=str_replace("'","",$cbo_buyer_name);
	$team_name=str_replace("'","",$cbo_team_name);
	$team_member=str_replace("'","",$cbo_team_member);
	$search_by=str_replace("'","",$cbo_search_by);
	$search_string=str_replace("'","",$txt_search_string);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	$category_by=str_replace("'","",$cbo_category_by);
	$year_id=str_replace("'","",$cbo_year);
	$rpt_type=str_replace("'","",$rpt_type);
	
	//if($data[0]==0) $company_name="%%"; else $company_name=$data[0];
	//if($data[1]==0) $buyer_name="%%"; else $buyer_name=$data[1];
	if($buyer_name==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	
	if(trim($date_from)!="") $start_date=$date_from;
	if(trim($date_to)!="") $end_date=$date_to;
	
	$cbo_order_status2=2;
	if($$cbo_order_status2==2) $cbo_order_status="%%"; else $cbo_order_status= "$cbo_order_status2";
	if(trim($team_name)=="0") $team_leader="%%"; else $team_leader="$team_name";
	if(trim($team_member)=="0") $dealing_marchant="%%"; else $dealing_marchant="$team_member";
	//if(trim($data[8])!="") $pocond="and b.id in(".str_replace("'",'',$data[8]).")"; else  $pocond="";
	
	if($db_type==0)
	{
		$start_date=change_date_format($date_from,'yyyy-mm-dd','-');
		$end_date=change_date_format($date_to,'yyyy-mm-dd','-');
	}
	if($db_type==2)
	{
		$start_date=change_date_format($date_from,'yyyy-mm-dd','-',1);
		$end_date=change_date_format($date_to,'yyyy-mm-dd','-',1);
	}
	
	//$cbo_category_by=$data[7]; $caption_date='';
	if($category_by==1)
	{
		if ($start_date!="" && $end_date!="")
		{
			$date_cond="and b.pub_shipment_date between '$start_date' and '$end_date'";
		}
		else	
		{
			$date_cond="";
		}
	}
	else
	{
		if ($start_date!="" && $end_date!="")
		{
			$date_cond=" and b.po_received_date between '$start_date' and '$end_date'";
		}
		else	
		{
			$date_cond="";
		}
	}
	//echo $date_cond;
	
	if($search_by==1)
	{
		if($search_string=="") $search_string_cond=""; else $search_string_cond= " and b.po_number like '$search_string'";
	}
	else if($search_by==2)
	{
		if($search_string=="") $search_string_cond=""; else $search_string_cond= " and a.job_no like '$search_string'";
	}
	if($db_type==0)
	{
		if($year_id!=0) $year_cond=" and YEAR(a.insert_date)=$year_id"; else $year_cond="";
	}
	else if ($db_type==2)
	{
		if($year_id!=0) $year_cond=" and to_char(a.insert_date,'YYYY')=$year_id"; else $year_cond="";
	}
	
	ob_start();
	if($rpt_type==1)
	{
		$buyer_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
		$company_short_name_arr=return_library_array( "select id,company_short_name from lib_company",'id','company_short_name');
		$company_team_name_arr=return_library_array( "select id,team_name from lib_marketing_team",'id','team_name');
		$company_team_member_name_arr=return_library_array( "select id,team_member_name from  lib_mkt_team_member_info",'id','team_member_name');
		$imge_arr=return_library_array( "select master_tble_id, image_location from  common_photo_library",'master_tble_id','image_location');
		$cm_for_shipment_schedule_arr=return_library_array( "select job_no,cm_for_sipment_sche from  wo_pre_cost_dtls",'job_no','cm_for_sipment_sche');
		
		$ex_factory_qty_arr=return_library_array( "select po_break_down_id, sum(ex_factory_qnty) as ex_factory_qnty from  pro_ex_factory_mst where  status_active=1 and is_deleted=0 group by po_break_down_id",'po_break_down_id','ex_factory_qnty');

		?>
		<div align="center">
			<div align="center">
			<table>
				<tr valign="top">
					<td valign="top">
					<h3 align="left" id="accordion_h2" class="accordion_h" onClick="accordion_menu( this.id,'content_summary1_panel', '')"> -Summary Panel</h3>
					<div id="content_summary1_panel"> 
					<fieldset>
					<table width="750" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all">
						<thead>
							<th width="50">SL</th>
							<th width="130">Company Name</th><th width="200">Buyer Name</th>
							<th width="130">Quantity</th><th width="100">Value</th><th width="50">Value %</th>
							<th width="130"><strong>Full Shipped</strong></th><th width="130"><strong>Partial Shipped</strong></th> 
							<th width="130"><strong>Running</strong></th><th><strong>Ex-factory Percentage</strong></th>  
						</thead>
						<tbody>
						<?
						$i=1; $total_po=0; $total_price=0;
						$po_qnty_array= array(); $po_value_array= array(); $po_full_shiped_array= array(); $po_full_shiped_value_array= array(); $po_partial_shiped_array= array(); $po_partial_shiped_value_array= array();  $po_running_array= array(); $po_running_value_array= array();
						//echo "select a.company_name,a.buyer_name,sum(b.po_quantity*a.total_set_qnty) as po_quantity, sum(b.po_total_price) as po_total_price   from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name like '$company_name' $buyer_id_cond and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond $search_string_cond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.company_name,a.buyer_name";
						$data_array=sql_select("select a.company_name,a.buyer_name,sum(b.po_quantity*a.total_set_qnty) as po_quantity, sum(b.po_total_price) as po_total_price   from wo_po_details_master a, wo_po_break_down b   where a.job_no=b.job_no_mst and a.company_name like '$company_name' $buyer_id_cond and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond $search_string_cond $year_cond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.company_name,a.buyer_name");
						foreach ($data_array as $row)
						{ 
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
							//$data_array_po=sql_select("select a.company_name, b.id, sum(b.po_quantity*a.total_set_qnty) as po_quantity , b.shiping_status  from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst   and a.company_name =$row[company_name] and a.buyer_name =$row[buyer_name] and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and b.status_active=1 ");
							$data_array_po=sql_select("select a.company_name, b.id, (b.po_quantity*a.total_set_qnty) as po_quantity , b.shiping_status  from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst   and a.company_name =".$row[csf('company_name')]." and a.buyer_name =".$row[csf('buyer_name')]." and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond $search_string_cond $year_cond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
							$full_shiped=0;$partial_shiped=0;
							foreach ($data_array_po as $row_po)
							{
								// $ex_factory_qnty=return_field_value( 'sum(ex_factory_qnty)','pro_ex_factory_mst', 'po_break_down_id="'.$row_po[id].'" and status_active=1 and is_deleted=0' );
								$ex_factory_qnty=$ex_factory_qty_arr[$row_po[csf("id")]];
								if($row_po[csf('shiping_status')]==3)
								{
									$full_shiped+=$ex_factory_qnty;
								}
								if($row_po[csf('shiping_status')]==2)
								{
									$partial_shiped+=$ex_factory_qnty;
								}
							}
							?>
							<tr bgcolor="<? echo $bgcolor;?>">
								<td width="50"><? echo $i;?></td>
								<td width="130"><? echo $company_short_name_arr[$row[csf('company_name')]]; ?></td>
								<td width="200"><? echo $buyer_short_name_arr[$row[csf('buyer_name')]];?></td>
								<td width="130" align="right">
								<? 
								echo number_format($row[csf('po_quantity')],0); $total_po +=$row[csf('po_quantity')]; 
								if (array_key_exists($row[csf('company_name')], $po_qnty_array)) 
								{
									$po_qnty_array[$row[csf('company_name')]]+=$row[csf('po_quantity')];
								}
								else
								{
									$po_qnty_array[$row[csf('company_name')]]=$row[csf('po_quantity')];	
								}
								?>
								</td>
								<td width="100" align="right">
								<? 
								echo number_format($row[csf('po_total_price')],2); $total_price+= $row[csf('po_total_price')];
								if (array_key_exists($row[csf('company_name')], $po_value_array)) 
								{
								$po_value_array[$row[csf('company_name')]]+=$row[csf('po_total_price')];
								}
								else
								{
								$po_value_array[$row[csf('company_name')]]=$row[csf('po_total_price')];	
								}
								?><input type="hidden" id="value_<? echo $i; ?>" value="<? echo $row[csf('po_total_price')];?>"/>
								</td>
								<td width="50" id="value_percent_<? echo $i; ?>" align="right"></td>
								<td width="130" align="right">
								<? 
								echo number_format($full_shiped,0); $full_shipped_total+=$full_shiped;
								if (array_key_exists($row[csf('company_name')], $po_full_shiped_array)) 
								{
									$po_full_shiped_array[$row[csf('company_name')]]+=$full_shiped;
								}
								else
								{
									$po_full_shiped_array[$row[csf('company_name')]]=$full_shiped;	
								}
								?>
								</td>
								<td width="130" align="right">
								<? 
								echo number_format($partial_shiped,0); $partial_shipped_total+=$partial_shiped;
								if (array_key_exists($row[csf('company_name')], $po_partial_shiped_array)) 
								{
									$po_partial_shiped_array[$row[csf('company_name')]]+=$partial_shiped;
								}
								else
								{
									$po_partial_shiped_array[$row[csf('company_name')]]=$partial_shiped;	
								}
								?>
								</td> 
								<td width="130" align="right">
								<? 
								$runing=$row[csf('po_quantity')]-($full_shiped+$partial_shiped); echo number_format($runing,0);$running_shipped_total+=$runing;
								if (array_key_exists($row[csf('company_name')], $po_running_array)) 
								{
									$po_running_array[$row[csf('company_name')]]+=$runing;
								}
								else
								{
									$po_running_array[$row[csf('company_name')]]=$runing;	
								}
								?>
								</td>
								<td align="right"><? $status=(($full_shiped+$partial_shiped)/$row[csf('po_quantity')])*100; $full_shipped_total_percent+=$status;  echo number_format($status,2); ?></td>
							</tr>
							<?
							$i++;
						}
						?>
						</tbody>
						<tfoot>
							<th width="50"></th>
							<th width="130"></th><th width="200"></th>
							<th width="130"><? echo number_format($total_po,0); ?></th><th width="100"><?  echo number_format($total_price,2); ?> <input type="hidden" id="total_value" value="<? echo $total_price;?>"/></th><th width="50"></th>
							<th width="130"><? echo number_format($full_shipped_total,0); ?></th><th width="130"><? echo number_format($partial_shipped_total,0); ?></th> 
							<th width="130"><? echo number_format($running_shipped_total,0); ?></th><th><input type="hidden" id="tot_row" value="<? echo $i;?>"/></th>  
						</tfoot>
					</table>
				</fieldset>
			</div>
					</td>
					<td valign="top">
					<h3 align="left" id="accordion_h3" class="accordion_h" onClick="accordion_menu( this.id,'content_summary2_panel', '')"> -Summary Panel</h3>
					<div id="content_summary2_panel"> 
					<fieldset>
						<table width="800" border="1" class="rpt_table" rules="all">
							<thead>
								<th>Company Name</th>
								<th>Particular Name</th>
								<th>Total Amount</th>
								<th>Full Shipped </th>
								<th>Partial Shipped </th>
								<th>Running </th>
								<th>Ex-factory Percentage</th>
							</thead>
						<?
						$comp_po_total=0; $comp_po_total_value=0; $total_full_shiped_qnty=0; $total_par_qnty=0; $total_run_qnty=0; $total_full_shiped_val=0; $total_par_val=0; $total_run_val=0;
						foreach($po_qnty_array as $key=> $value)
						{
							?>
							<tr>
								<td rowspan="2" align="center"><? echo $company_short_name_arr[$key];//echo $company_name; ?></td>
								<td align="center">PO Quantity</td>
								<td align="right"><? echo number_format($value+$po_qnty_array_projec[$key],0);$comp_po_total=$comp_po_total+$value+$po_qnty_array_projec[$key]; ?></td>
								<td align="right"><? echo number_format($po_full_shiped_array[$key],0); $total_full_shiped_qnty+=$po_full_shiped_array[$key];?></td>
								<td align="right"><? echo number_format($po_partial_shiped_array[$key],0); $total_par_qnty+=$po_partial_shiped_array[$key];?></td>
								<td align="right"><? echo number_format($po_running_array[$key],0); $total_run_qnty+=$po_running_array[$key]; ?> </td>
								<td align="right"><? $ex_factory_per=(($po_full_shiped_array[$key]+$po_partial_shiped_array[$key])/($value))*100; echo number_format($ex_factory_per,2).' %'; ?></td>
							</tr>
							<tr bgcolor="white">
								<td align="center">LC Value</td>
								<td align="right"><? echo number_format($po_value_array[$key],2);  $comp_po_total_value=$comp_po_total_value+$po_value_array[$key];?></td>
								<td align="right"><? $full_shiped_value=($po_value_array[$key]/$value)*$po_full_shiped_array[$key]; echo number_format($full_shiped_value,2); $total_full_shiped_val+=$full_shiped_value; ?></td>
								<td align="right"><? $full_partial_shipeddd_value=($po_value_array[$key]/$value)*$po_partial_shiped_array[$key]; echo number_format($full_partial_shipeddd_value,2); $total_par_val+=$full_partial_shipeddd_value; ?></td>
								<td align="right"><? $full_running_value=($po_value_array[$key]/$value)*$po_running_array[$key]; echo number_format($full_running_value,2); $total_run_val+=$full_running_value; ?></td>
								<td align="right"><? $ex_factory_per_value=(($full_shiped_value+$full_partial_shipeddd_value)/($po_value_array[$key]))*100; echo number_format($ex_factory_per_value,2).' %'; ?></td>
							</tr>
							<?
						}
						?>
						<tfoot>
							<tr>
								<th align="center" rowspan="2"> Total:</th>
								<th align="center">Qnty Total:</th>
								<th align="right"><? echo number_format($comp_po_total,0); ?></th>
								<th align="right"><? echo number_format($total_full_shiped_qnty,2); ?></th>
								<th align="right"><? echo number_format($total_par_qnty,2); ?></th>
								<th align="right"><? echo number_format($total_run_qnty,2); ?></th>
								<th align="right"><? //echo number_format($ex_factory_per_value,2).' %'; ?></th>
							</tr>
							<tr bgcolor="#999999">
								<th align="center">Value Total:</th>
								<th align="right"><? echo number_format($comp_po_total_value,2); ?></th>
								<th align="right"><? echo number_format($total_full_shiped_val,2); ?></th>
								<th align="right"><? echo number_format($total_par_val,2); ?></th>
								<th align="right"><? echo number_format($total_run_val,2); ?></th>
								<th align="right"><? //echo number_format($ex_factory_per_value,2).' %'; ?></th>
							</tr>
						</tfoot>
					</table>
					</fieldset>
				</div>
				</td>
				<td valign="top">
				<h3 align="left" id="accordion_h4" class="accordion_h" onClick="accordion_menu( this.id,'content_summary3_panel', '')"> -Shipment Performance Summary</h3>
				<div id="content_summary3_panel"> 
				</div>
				</td>
			</tr>
		</table>
		<h3 style="width:3100px;" align="left" id="accordion_h4" class="accordion_h" onClick="accordion_menu( this.id,'content_report_panel', '')"> -Report Panel</h3>
		<div id="content_report_panel"> 
		<table width="3100" id="table_header_1" border="1" class="rpt_table" rules="all">
			<thead>
				<tr>
					<th width="50">SL</th>
					<th width="70" >Company</th>
					<th width="70">Job No</th>
					<th width="60">Year</th>
					<th width="50">Buyer</th>
					<th width="110">PO No</th>
					<th width="100">Season</th>
					<th width="50">Agent</th>
					<th width="70">Order Status</th>
					<th width="70">Prod. Catg</th>
					<th width="40">Img</th>
					<th width="90">Style Ref</th>
					<th width="150">Item</th>
					<th width="200">Fab. Description</th>
					<th width="70">Ship Date</th>
					<th width="70">PO Rec. Date</th>
					<th width="50">Days in Hand</th>
					<th width="90">Order Qnty(Pcs)</th>
					<th width="90">Order Qnty</th>
					<th width="40">Uom</th>
					<th width="50">Per Unit Price</th>
					<th width="100">Order Value</th>
					<th width="100">LC/SC No</th>
					<th width="90">Ex-Fac Qnty </th>
					<th width="70">Last Ex-Fac Date</th>
					<th width="90">Short/Access Qnty</th>
					<th width="120">Short/Access Value</th>
					<th width="100">Yarn Req</th>
					<th width="100">CM </th>
					<th width="100" >Shipping Status</th>
					<th width="150"> Team Member</th>
					<th width="150">Team Name</th>
					<th width="100">File No</th>
					<th width="40">Id</th>
					<th>Remarks</th>
				</tr>
			</thead>
		</table>
		<div style=" max-height:400px; overflow-y:scroll; width:3120px"  align="left" id="scroll_body">
		<table width="3100" border="1" class="rpt_table" rules="all" id="table_body">
			<?
			if($db_type==0)
			{
				$fab_dec_cond="group_concat(fabric_description)";	
			}
			else if($db_type==2)
			{
				$fab_dec_cond="listagg(cast(fabric_description as varchar2(4000)),',') within group (order by fabric_description)";
			}
			$fabric_arr=array();
			$fab_sql=sql_select("select job_no, item_number_id, $fab_dec_cond as fabric_description from wo_pre_cost_fabric_cost_dtls where status_active=1 and is_deleted=0 group by job_no, item_number_id");
			foreach ($fab_sql as $row)
			{
				$fabric_arr[$row[csf('job_no')]][$row[csf('item_number_id')]]=$row[csf('fabric_description')];
			}
			//var_dump($fabric_arr);die;
			
			$i=1; $order_qnty_pcs_tot=0; $order_qntytot=0; $oreder_value_tot=0; $total_ex_factory_qnty=0; $total_short_access_qnty=0; $total_short_access_value=0; $yarn_req_for_po_total=0;
			if($db_type==0)
			{
				$lc_number_arr=return_library_array( "select a.wo_po_break_down_id, group_concat(b.export_lc_no) as export_lc_no  from com_export_lc_order_info a, com_export_lc b where a.com_export_lc_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.wo_po_break_down_id ",'wo_po_break_down_id','export_lc_no');
				
				$sc_number_arr=return_library_array( "select a.wo_po_break_down_id, group_concat(b.contract_no) as contract_no from com_sales_contract_order_info a, com_sales_contract b where a.com_sales_contract_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.wo_po_break_down_id ",'wo_po_break_down_id','contract_no');
			}
			if($db_type==2)
			{
				$lc_number_arr=return_library_array( "select a.wo_po_break_down_id, LISTAGG(b.export_lc_no,',') WITHIN GROUP (ORDER BY b.export_lc_no)  export_lc_no  from com_export_lc_order_info a, com_export_lc b where a.com_export_lc_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.wo_po_break_down_id ",'wo_po_break_down_id','export_lc_no');
				
				$sc_number_arr=return_library_array( "select a.wo_po_break_down_id, LISTAGG(b.contract_no) WITHIN GROUP (ORDER BY b.contract_no) contract_no from com_sales_contract_order_info a, com_sales_contract b where a.com_sales_contract_id=b.id and a.status_active=1 and 	a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.wo_po_break_down_id ",'wo_po_break_down_id','contract_no');
			}
			
			$data_array_group=sql_select("select b.grouping from wo_po_details_master a, wo_po_break_down b LEFT JOIN pro_ex_factory_mst c on b.id = c.po_break_down_id where  a.job_no=b.job_no_mst and a.company_name like '$company_name' $buyer_id_cond and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and b.status_active=1 $search_string_cond group by b.grouping");
			foreach ($data_array_group as $row_group)
			{ 
				$gorder_qnty_pcs_tot=0; $gorder_qntytot=0; $goreder_value_tot=0; $gtotal_ex_factory_qnty=0; $gtotal_short_access_qnty=0; $gtotal_short_access_value=0; $gyarn_req_for_po_total=0;
				?>
				<!--<tr bgcolor="<?// echo $bgcolor; ?>" style="vertical-align:middle" height="25" >
					<th width="50" align="center"><?// echo $row_group[csf('grouping')]; ?> </th>
					<th width="70" ></th>
					<th width="70"></th>
					<th width="50"></th>
					<th width="110"></th>
					<th width="100"></th>
					<th width="50"></th>
					<th width="70"></th>
					<th width="70"></th>
					<th width="30"></th>
					<th width="90"></th>
					<th width="150"></th>
					<th width="70"></th>
					<th width="70"></th>
					<th width="50"></th>
					<th width="90"></th>
					<th width="90"></th>
					<th width="40"></th>
					<th width="50"></th>
					<th width="100"></th>
					<th width="100"></th>
					<th width="90"></th>
					<th width="70"></th>
					<th width="90"></th>
					<th width="120"></th>
					<th width="100"></th>
					<th width="100"></th>
					<th width="100" ></th>
					<th width="150"></th>
					<th width="150"></th>
					<th width="100"></th>
					<th width="30"></th>
					<th></th>
				</tr>-->
			<? 
				if($db_type==0)
				{
					$data_array=sql_select("select a.job_no_prefix_num, a.job_no, YEAR(a.insert_date) as year, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant, b.id, b.is_confirmed, b.po_number, b.po_quantity, b.pub_shipment_date, b.po_received_date, DATEDIFF(b.pub_shipment_date,'$date') date_diff_1, DATEDIFF(b.shipment_date,'$date') date_diff_2, b.unit_price, b.po_total_price, b.details_remarks, b.shiping_status, b.file_no, sum(c.ex_factory_qnty) as ex_factory_qnty, MAX(c.ex_factory_date) as ex_factory_date, DATEDIFF(b.pub_shipment_date, MAX(c.ex_factory_date)) date_diff_3, DATEDIFF(b.shipment_date, MAX(c.ex_factory_date)) date_diff_4 from wo_po_details_master a, wo_po_break_down b LEFT JOIN pro_ex_factory_mst c on b.id = c.po_break_down_id and c.status_active=1 and c.is_deleted=0 where  a.job_no=b.job_no_mst and a.company_name like '$company_name'  $buyer_id_cond and a.team_leader like '$team_leader' and b.grouping='".$row_group[csf('grouping')]."' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond $year_cond and a.status_active=1 and b.status_active=1 $search_string group by b.id order by b.pub_shipment_date,a.job_no_prefix_num,b.id");
				}
				if($db_type==2)
				{
					$date=date('d-m-Y');
					if($row_group[csf('grouping')]!="")
					{
						$grouping="and b.grouping='".$row_group[csf('grouping')]."'";
					}
					if($row_group[csf('grouping')]=="")
					{
						$grouping="and b.grouping IS NULL";
					}
					$data_array=sql_select("select a.job_no_prefix_num, a.job_no, to_char(a.insert_date,'YYYY') as year, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant, a.season, b.id, b.is_confirmed ,b.po_number, b.po_quantity, b.shipment_date, b.pub_shipment_date, b.po_received_date, (b.pub_shipment_date - to_date('$date','dd-mm-yyyy')) date_diff_1,(b.shipment_date - to_date('$date','dd-mm-yyyy')) date_diff_2, b.unit_price, b.po_total_price, b.details_remarks, b.shiping_status,b.file_no, sum(c.ex_factory_qnty) as ex_factory_qnty, MAX(c.ex_factory_date) as ex_factory_date, (b.pub_shipment_date - MAX(c.ex_factory_date)) date_diff_3,(b.shipment_date - MAX(c.ex_factory_date)) date_diff_4 from wo_po_details_master a, wo_po_break_down b LEFT JOIN pro_ex_factory_mst c on b.id = c.po_break_down_id and c.status_active=1 and c.is_deleted=0 where  a.job_no=b.job_no_mst and a.company_name like '$company_name'  $buyer_id_cond and a.team_leader like '$team_leader'  $grouping and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond $year_cond and a.status_active=1 and b.status_active=1 $search_string_cond 
					group by a.job_no_prefix_num, a.job_no, a.insert_date, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant, a.season, b.id, b.is_confirmed ,b.po_number, b.po_quantity, b.shipment_date, b.pub_shipment_date, b.po_received_date, b.unit_price, b.po_total_price, b.details_remarks, b.shiping_status,b.file_no order by b.pub_shipment_date,a.job_no_prefix_num,b.id");
				}
					
				foreach ($data_array as $row)
				{ 
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
							
					$cons=0;
					$costing_per_pcs=0;
					$data_array_yarn_cons=sql_select("select yarn_cons_qnty from  wo_pre_cost_sum_dtls where  job_no='".$row[csf('job_no')]."'");
					$data_array_costing_per=sql_select("select costing_per from  wo_pre_cost_mst where  job_no='".$row[csf('job_no')]."'");
					list($costing_per)=$data_array_costing_per;
					if($costing_per[csf('costing_per')]==1) $costing_per_pcs=1*12;	
					else if($costing_per[csf('costing_per')]==2) $costing_per_pcs=1*1;	
					else if($costing_per[csf('costing_per')]==3) $costing_per_pcs=2*12;	
					else if($costing_per[csf('costing_per')]==4) $costing_per_pcs=3*12;	
					else if($costing_per[csf('costing_per')]==5) $costing_per_pcs=4*12;	
					
					$yarn_req_for_po=0;
					foreach($data_array_yarn_cons as $row_yarn_cons)
					{
						$cons=$row_yarn_cons[csf('yarn_cons_qnty')];
						$yarn_req_for_po=($row_yarn_cons[csf('yarn_cons_qnty')]/ $costing_per_pcs)*$row[csf('po_quantity')];
					}
					
					//--Calculation Yarn Required-------
					//--Color Determination-------------
					//==================================
					$shipment_performance=0;
					if($row[csf('shiping_status')]==1 && $row[csf('date_diff_1')]>10 )
					{
						$color="";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
					}
					
					if($row[csf('shiping_status')]==1 && ($row[csf('date_diff_1')]<=10 && $row[csf('date_diff_1')]>=0))
					{
						$color="orange";
						$number_of_order['yet']+=1;
						$shipment_performance=0;
					}
					if($row[csf('shiping_status')]==1 &&  $row[csf('date_diff_1')]<0)
					{
						$color="red";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
					}
							//=====================================
					if($row[csf('shiping_status')]==2 && $row[csf('date_diff_1')]>10 )
					{
						$color="";	
					}
					if($row[csf('shiping_status')]==2 && ($row[csf('date_diff_1')]<=10 && $row[csf('date_diff_1')]>=0))
					{
						$color="orange";	
					}
					if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_1')]<0)
					{
						$color="red";	
					}
					if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_2')]>=0)
					{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;	
					}
					if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_2')]<0)
					{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
					}
					//========================================
					if($row[csf('shiping_status')]==3 && $row[csf('date_diff_3')]>=0 )
					{
						$color="green";	
					}
					if($row[csf('shiping_status')]==3 &&  $row[csf('date_diff_3')]<0)
					{
						$color="#2A9FFF";	
					}
					if($row[csf('shiping_status')]==3 && $row[csf('date_diff_4')]>=0 )
					{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;
					}
					if($row[csf('shiping_status')]==3 &&  $row[csf('date_diff_4')]<0)
					{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
					}
					?>
					<tr bgcolor="<? echo $bgcolor;?>" style="vertical-align:middle" height="25" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
						<td width="50" bgcolor="<? echo $color; ?>"> <? echo $i; ?> </td>
						<td width="70"><div style="word-wrap:break-word; width:70px"><? echo $company_short_name_arr[$row[csf('company_name')]];?></div></td>
						<td width="70"><p><? echo $row[csf('job_no_prefix_num')]; ?></p></td>
						<td width="60"><p><? echo $row[csf('year')]; ?></p></td>
						<td width="50"><div style="word-wrap:break-word; width:50px"><? echo $buyer_short_name_arr[$row[csf('buyer_name')]];?></div></td>
						<td width="110"><div style="word-wrap:break-word; width:110px"><? echo $row[csf('po_number')];?></div></td>
						<td width="100"><div style="word-wrap:break-word; width:100px"><? echo $row[csf('season')];?></div></td>
						<td width="50"><div style="word-wrap:break-word; width:50px"><? echo $buyer_short_name_arr[$row[csf('agent_name')]];?></div></td>
						<td width="70"><div style="word-wrap:break-word; width:70px"><? echo $order_status[$row[csf('is_confirmed')]];?></div></td>
						<td width="70"><div style="word-wrap:break-word; width:70px"><? echo $product_category[$row[csf('product_category')]];?></div></td>
						<td width="40" onclick="openmypage_image('requires/shipment_schedule_controller.php?action=show_image&job_no=<? echo $row[csf("job_no")] ?>','Image View')"><img  src='../../../<? echo $imge_arr[$row[csf('job_no')]]; ?>' height='25' width='30' /></td>
						<td width="90"><div style="word-wrap:break-word; width:90px"><? echo $row[csf('style_ref_no')];?></div></td>
						<td width="150"><div style="word-wrap:break-word; width:150px">
						<? $gmts_item_id=explode(',',$row[csf('gmts_item_id')]);
							$fabric_description="";
							for($j=0; $j<=count($gmts_item_id); $j++)
							{
								if($fabric_description=="") $fabric_description=$fabric_arr[$row[csf('job_no')]][$gmts_item_id[$j]]; else $fabric_description.=','.$fabric_arr[$row[csf('job_no')]][$gmts_item_id[$j]];
								echo $garments_item[$gmts_item_id[$j]];
							}
							?></div></td>
						<td width="200"><div style="word-wrap:break-word; width:200px">
							<?
							$fabric_des="";
							$fabric_des=implode(",",array_unique(explode(",",$fabric_description)));
							echo $fabric_des;//$fabric_des;?></div></td>
						<td width="70"><div style="word-wrap:break-word; width:70px"><? echo change_date_format($row[csf('pub_shipment_date')],'dd-mm-yyyy','-');?></div></td>
						<td width="70"><div style="word-wrap:break-word; width:70px"><? echo change_date_format($row[csf('po_received_date')],'dd-mm-yyyy','-');?></div></td>
						<td width="50" bgcolor="<? echo $color; ?>"><div style="word-wrap:break-word; width:50px">
							<?
							if($row[csf('shiping_status')]==1 || $row[csf('shiping_status')]==2)
							{
								echo $row[csf('date_diff_1')];
							}
							if($row[csf('shiping_status')]==3)
							{
								echo $row[csf('date_diff_3')];
							}
							?></div></td>
						<td width="90" align="right"><p>
							<? 
							echo number_format(($row[csf('po_quantity')]*$row[csf('total_set_qnty')]),0);  
							$order_qnty_pcs_tot=$order_qnty_pcs_tot+($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
							$gorder_qnty_pcs_tot=$gorder_qnty_pcs_tot+($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
							?></p></td>
						<td width="90" align="right"><p>
							<? 
							echo number_format( $row[csf('po_quantity')],0);
							$order_qntytot=$order_qntytot+$row[csf('po_quantity')];
							$gorder_qntytot=$gorder_qntytot+$row[csf('po_quantity')];
							?></p></td>
						<td width="40"><p><? echo $unit_of_measurement[$row[csf('order_uom')]];?></p></td>
						<td width="50" align="right"><p><? echo number_format($row[csf('unit_price')],2);?></p></td>
						<td width="100" align="right"><p>
							<? 
								echo number_format($row[csf('po_total_price')],2);
								$oreder_value_tot=$oreder_value_tot+$row[csf('po_total_price')];
								$goreder_value_tot=$goreder_value_tot+$row[csf('po_total_price')];
							?></p></td>
						<td width="100" align="center"><div style="word-wrap:break-word; width:100px">
							<?
							if($lc_number_arr[$row[csf('id')]] !="")
							{
								echo "LC: ". $lc_number_arr[$row[csf('id')]];
							}
							if($sc_number_arr[$row[csf('id')]] !="")
							{
								echo " SC: ".$sc_number_arr[$row[csf('id')]];
							}
							?>
							</div></td>
						<td width="90" align="right"><p>
						<? 
							$ex_factory_qnty=$ex_factory_qty_arr[$row[csf("id")]];
							echo  number_format( $ex_factory_qnty,0); 
							$total_ex_factory_qnty=$total_ex_factory_qnty+$ex_factory_qnty ;
							$gtotal_ex_factory_qnty=$gtotal_ex_factory_qnty+$ex_factory_qnty ;;
							if ($shipment_performance==0)
							{
								$po_qnty['yet']+=($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
								$po_value['yet']+=100;
							}
							else if ($shipment_performance==1)
							{
								$po_qnty['ontime']+=$ex_factory_qnty;
								$po_value['ontime']+=((100*$ex_factory_qnty)/($row[csf('po_quantity')]*$row[csf('total_set_qnty')]));
								$po_qnty['yet']+=(($row[csf('po_quantity')]*$row[csf('total_set_qnty')])-$ex_factory_qnty);
							}
							else if ($shipment_performance==2)
							{
								$po_qnty['after']+=$ex_factory_qnty;
								$po_value['after']+=((100*$ex_factory_qnty)/($row[csf('po_quantity')]*$row[csf('total_set_qnty')]));
								$po_qnty['yet']+=(($row[csf('po_quantity')]*$row[csf('total_set_qnty')])-$ex_factory_qnty);
							}
							?></p></td>
						<td width="70"><a href="##" onClick="last_ex_factory_popup('last_ex_factory_Date','<? echo $row['job_no'];?>', '<? echo $row['id']; ?>','750px')"><div style="word-wrap:break-word; width:70px"><? echo change_date_format($row[csf('ex_factory_date')]); ?></div></a></td>
						<td  width="90" align="right"><p>
							<? 
								$short_access_qnty=($row[csf('po_quantity')]-$ex_factory_qnty); 
								echo number_format($short_access_qnty,0);
								$total_short_access_qnty=$total_short_access_qnty+$short_access_qnty;
								$gtotal_short_access_qnty=$gtotal_short_access_qnty+$short_access_qnty;;
							?></p>
						</td>
						<td width="120" align="right"><p>
							<? 
								$short_access_value=$short_access_qnty*$row[csf('unit_price')];
								echo number_format($short_access_value,2);
								$total_short_access_value=$total_short_access_value+$short_access_value;
								$gtotal_short_access_value=$gtotal_short_access_value+$short_access_value;
							?></p>
						</td>
						<td width="100" align="right" title="<? echo "Cons:".$cons."Costing per:".$costing_per[csf('costing_per')];?>"><p>
							<? 
								echo number_format($yarn_req_for_po,2);
								$yarn_req_for_po_total=$yarn_req_for_po_total+$yarn_req_for_po;
								$gyarn_req_for_po_total=$gyarn_req_for_po_total+$yarn_req_for_po;
							?></p>
						</td>
						<td width="100" align="right"><p><? echo number_format(($cm_for_shipment_schedule_arr[$row[csf('job_no')]]/ $costing_per_pcs)*$row[csf('po_quantity')],2); ?></p></td>
						<td width="100" align="center"><div style="word-wrap:break-word; width:100px"><? echo $shipment_status[$row[csf('shiping_status')]]; ?></div></td>
						<td width="150" align="center"><div style="word-wrap:break-word; width:150px"><? echo $company_team_member_name_arr[$row[csf('dealing_marchant')]];?></div></td>
						<td width="150" align="center"><div style="word-wrap:break-word; width:150px"><? echo $company_team_name_arr[$row[csf('team_leader')]];?></div></td>
						<td width="100"><div style="word-wrap:break-word; width:100px"><? echo $row[csf('file_no')]; ?></div></td>
						<td width="40"><p><? echo $row[csf('id')]; ?></p></td>
						<td><p><? echo $row[csf('details_remarks')]; ?></p></td>
					</tr>
				<?
				$i++;
				}
				?>
				<tr bgcolor="#CCCCCC" style="vertical-align:middle" height="25">
					<td width="50" align="center" >  Total: </td>
					<td width="70" ></td>
					<td width="70"></td>
					<td width="60"></td>
					<td width="50"></td>
					<td width="110"></td>
					<td width="100"></td>
					<td width="50"></td>
					<td width="70"></td>
					<td width="70"></td>
					<td width="40"></td>
					<td width="90"></td>
					<td width="150"></td>
					<td width="200"></td>
					<td width="70"></td>
					<td width="70"></td>
					<td width="50"></td>
					<td width="90" align="right"><? echo number_format($gorder_qnty_pcs_tot,0); ?></td>
					<td width="90" align="right"><? echo number_format($gorder_qntytot,0); ?></td>
					<td width="40"></td>
					<td width="50"></td>
					<td width="100" align="right"><? echo number_format($goreder_value_tot,2); ?></td>
					<td width="100"></td>
					<td width="90" align="right"><? echo number_format($gtotal_ex_factory_qnty,0); ?></td>
					<td width="70"></td>
					<td width="90" align="right"> <? echo number_format($gtotal_short_access_qnty,0); ?></td>
					<td width="120" align="right"> <? echo number_format($gtotal_short_access_value,0); ?></td>
					<td width="100" align="right"><? echo number_format($gyarn_req_for_po_total,2); ?></td>
					<td width="100"></td>
					<td width="100" ></td>
					<td width="150"></td>
					<td width="150"></td>
					<td width="100"></td>
					<td width="40"></td>
					<td></th>
				 </tr>
			<?
			}
			?>
			</table>
			</div>
			<table width="3100" id="report_table_footer" border="1" class="rpt_table" rules="all">
				<tfoot>
					<tr>
						<th width="50"></th>
						<th width="70" ></th>
						<th width="70"></th>
						<th width="60"></th>
						<th width="50"></th>
						<th width="110"></th>
						<th width="100"></th>
						<th width="50"></th>
						<th width="70"></th>
						<th width="70"></th>
						<th width="40"></th>
						<th width="90"></th>
						<th width="150"></th>
						<th width="200"></th>
						<th width="70"></th>
						<th width="70"></th>
						<th width="50"></th>
						<th width="90" id="total_order_qnty_pcs"><? echo number_format($order_qnty_pcs_tot,0); ?></th>
						<th width="90" id="total_order_qnty"><? echo number_format($order_qntytot,0); ?></th>
						<th width="40"></th>
						<th width="50"></th>
						<th width="100" id="value_total_order_value"><? echo number_format($oreder_value_tot,2); ?></th>
						<th width="100"></th>
						<th width="90" id="total_ex_factory_qnty"><? echo number_format($total_ex_factory_qnty,0); ?></th>
						<th width="70"></th>
						<th width="90" id="total_short_access_qnty"><? echo number_format($total_short_access_qnty,0); ?></th>
						<th width="120" id="value_total_short_access_value"><? echo number_format($total_short_access_value,0); ?></th>
						<th width="100" id="value_yarn_req_tot"><? echo number_format($yarn_req_for_po_total,2); ?></th>
						<th width="100"></th>
						<th width="100" ></th>
						<th width="150"> </th>
						<th width="150"></th>
						<th width="100"></th>
						<th width="40"></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
			<div id="shipment_performance" style="visibility:hidden">
				<fieldset>
					<table width="600" border="1" cellpadding="0" cellspacing="1" class="rpt_table" rules="all" >
						<thead>
							<tr>
								<th colspan="4"> <font size="4">Shipment Performance</font></th>
							</tr>
							<tr>
								<th>Particulars</th><th>No of PO</th><th>PO Qnty</th><th> %</th>
							</tr>
						</thead>
						<tr bgcolor="#E9F3FF">
							<td>On Time Shipment</td><td><? echo $number_of_order['ontime']; ?></td><td align="right"><? echo number_format($po_qnty['ontime'],0); ?></td><td align="right"><? echo number_format(((100*$po_qnty['ontime'])/$order_qnty_pcs_tot),2); ?></td>
							</tr>
							<tr bgcolor="#FFFFFF">
							<td> Delivery After Shipment Date</td><td><? echo $number_of_order['after']; ?></td><td align="right"><? echo number_format($po_qnty['after'],0); ?></td><td align="right"><? echo number_format(((100*$po_qnty['after'])/$order_qnty_pcs_tot),2); ?></td>
							</tr>
							<tr bgcolor="#E9F3FF">
							<td>Yet To Shipment </td><td><? echo $number_of_order['yet']; ?></td><td align="right"><? echo number_format($po_qnty['yet'],0); ?></td><td align="right"><? echo number_format(((100*$po_qnty['yet'])/$order_qnty_pcs_tot),2); ?></td>
							</tr>
							
							<tr bgcolor="#E9F3FF">
							<td> </td><td></td><td align="right"><? echo number_format($po_qnty['yet']+$po_qnty['ontime']+$po_qnty['after'],0); ?></td><td align="right"><? echo number_format(((100*$po_qnty['yet'])/$order_qnty_pcs_tot)+((100*$po_qnty['after'])/$order_qnty_pcs_tot)+((100*$po_qnty['ontime'])/$order_qnty_pcs_tot),2); ?></td>
						</tr>
					</table>
				</fieldset>
			</div>
			</div>
			</div>
		</div>
		<?
	}
	else if($rpt_type==2)
	{
		$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
		$Dealing_marcent_arr=return_library_array( "select id, team_member_name from lib_mkt_team_member_info",'id','team_member_name');
		$company_name_arr=return_library_array( "select id,company_name from lib_company",'id','company_name');
		?>
        <div style="width:1300px">
            <table width="1300" cellpadding="0" cellspacing="0" id="caption"  align="left">
            <tr>  
                <td align="center" width="100%" class="form_caption"  colspan="47"><strong style="font-size:18px"><? echo $company_name_arr[$company_name]; ?></strong></td>
            </tr>
            <tr>
                <td align="center" width="100%" class="form_caption"  colspan="47"><strong style="font-size:18px"><? echo $report_title; ?></strong></td>
            </tr>
            <tr>
                <td align="center" width="100%" class="form_caption"  colspan="47"><strong style="font-size:14px">From <? echo change_date_format($start_date); ?> To <? echo change_date_format($end_date); ?> </strong></td>
            </tr>
            </table>
            <br />       
            <table width="1400" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_header"  align="left">
                <thead>
                    <tr>
                        <th width="40">SL</th>
                        <th width="100">Buyer</th>
                        <th width="110">Style No</th>
                        <th width="110">PO No</th>
                        <th width="100">Dealing Merchant</th>
                        <th width="130">Item Description</th>
                        <th width="70">GSM</th>
                        <th width="250">Fabrication</th>
                        <th width="100">Order Qnty(Pcs)</th>
                        <th width="70"><? if($category_by==1) echo "Ship Date"; elseif($category_by==2) echo "PO Receive Date"; ?></th>
                        <th width="50">Unit Price</th>
                        <th width="100">FOB Price</th>
                        <th >Remarks</th>
                    </tr>
                </thead>
            </table>
            <div style="width:1400px; overflow-y:scroll; max-height:380px;font-size:12px; overflow-x:hidden;" id="scroll_body" align="left">
            <table width="1382" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
                <tbody>
                <?
                if($category_by==1)
                {
                    if ($start_date!="" && $end_date!="")
                    {
                        $date_cond=" and b.shipment_date between '$start_date' and '$end_date'";
                    }
                    else	
                    {
                        $date_cond="";
                    }
                }
                else
                {
                    if ($start_date!="" && $end_date!="")
                    {
                        $date_cond=" and b.po_received_date between '$start_date' and '$end_date'";
                    }
                    else	
                    {
                        $date_cond="";
                    }
                }
				
				//$fabrication_sql=sql_select("select listagg(cast(a.fabric_description as varchar2(4000)),',') within group (order by a.fabric_description) as fabric_description, b.po_break_down_id from wo_pre_cost_fabric_cost_dtls a,  wo_pre_cos_fab_co_avg_con_dtls b,  where a.job_no=b.job_no and  a.status_active=1 and a.is_deleted=0 and b.cons>0 and c.company_name=$company_name group by b.po_break_down_id");
				
				$fabrication_sql=sql_select("select a.fabric_description, b.po_break_down_id,a.gsm_weight from wo_pre_cost_fabric_cost_dtls a, 
				wo_pre_cos_fab_co_avg_con_dtls b where a.id=b.pre_cost_fabric_cost_dtls_id  and  a.status_active=1 and a.is_deleted=0 and b.cons>0");
				$fabrication_data_arr=array();
				foreach($fabrication_sql as $row)
				{
					$fabrication_data_arr[$row[csf("po_break_down_id")]].=$row[csf("fabric_description")].",";
					$gsm_data_arr[$row[csf("po_break_down_id")]].=$row[csf("gsm_weight")].",";
				}
				
                $main_sql="select a.id as job_id, a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, a.gmts_item_id, a.remarks,
				b.id as po_id, b.po_number, b.po_received_date, b.shipment_date, (a.total_set_qnty*b.po_quantity) as po_quantity_pcs,
				(b.unit_price/a.total_set_qnty) as unit_price_pcs,a.dealing_marchant
				from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and 
				b.status_active=1 and b.is_deleted=0 and a.company_name=$company_name $date_cond $search_string_cond $year_cond $buyer_id_cond
				and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' order by a.buyer_name, a.id, b.id";
				//echo $main_sql;die; 
                $main_result=sql_select($main_sql);
                $k=1;$m=1;
                $temp_arr_buyer=array();
                foreach($main_result as $row)
                {
					$po_total_price=0;
                    if(!in_array($row[csf("buyer_name")],$temp_arr_buyer))
                    {
                        $temp_arr_buyer[]=$row[csf("buyer_name")];
                        if($k!=1)
                        {
                            ?>
                            <tr bgcolor="#CCCCCC">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td align="right">Sub Total:&nbsp;</td>
                                <td align="right"><? echo number_format($buyer_tot_qnty,2); ?></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td align="right"><? echo number_format($buyer_tot_price,2); ?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?
                        }
                        $k++;
                    }
                    if($m%2==0)$bgcolor="#E9F3FF";
					else
					$bgcolor="#FFFFFF";
                    ?>
                    <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $m; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $m; ?>">
                        <td width="40" align="center"><p><? echo $m;?>&nbsp;</p></td>
                        <td width="100"><p><? echo $buyer_arr[$row[csf("buyer_name")]]; ?>&nbsp;</p></td>
                        <td width="110"><p><? echo $row[csf("style_ref_no")]; ?>&nbsp;</p></td>
                        <td width="110"><p><? echo $row[csf("po_number")]; ?>&nbsp;</p></td>
                        <td width="100"><p><? echo $Dealing_marcent_arr[$row[csf("dealing_marchant")]]; ?>&nbsp;</p></td>
                        <td width="130"><p>
						<?
						$garments_item_arr=explode(",",$row[csf("gmts_item_id")]);
						$all_garments_item="";
						foreach($garments_item_arr as $garments_item_id)
						{
							$all_garments_item.=$garments_item[$garments_item_id]." , ";
						}
						$all_garments_item=chop($all_garments_item," , ");
						echo $all_garments_item; 
						?>&nbsp;</p></td>
                        <td width="70"><p>
						<?
						$gsm_data=implode(", ",array_unique(explode(",",chop($gsm_data_arr[$row[csf("po_id")]]," , ")))); echo $gsm_data; 
						
						 //$gsm_data_arr[$row[csf("po_break_down_id")]];echo $row[csf("po_number")]; ?>&nbsp;</p></td>
                        <td width="250"><p><? $fabrication_data=implode(", ",array_unique(explode(",",chop($fabrication_data_arr[$row[csf("po_id")]]," , ")))); echo $fabrication_data; ?>&nbsp;</p></td>
                        <td width="100" align="right"><p><? echo number_format($row[csf("po_quantity_pcs")],2); $buyer_tot_qnty+=$row[csf("po_quantity_pcs")]; $total_po_qnty+=$row[csf("po_quantity_pcs")]; ?>&nbsp;</p></td>
                        <td width="70" align="center"><p>
                        <?
                        if($category_by==1)
                        {
                            if($row[csf("shipment_date")]!="" && $row[csf("shipment_date")]!="0000-00-00") echo change_date_format($row[csf("shipment_date")]);
                        }
                        else if($category_by==2)
                        {
                            if($row[csf("po_received_date")]!="" && $row[csf("po_received_date")]!="0000-00-00") echo change_date_format($row[csf("po_received_date")]);
                        }
                        ?>
                        &nbsp;</p></td>
                        <td width="50" align="right"><p><? echo number_format($row[csf("unit_price_pcs")],2) ?>&nbsp;</p></td>
                        <td width="100" align="right"><p><? $po_total_price=$row[csf("po_quantity_pcs")]*$row[csf("unit_price_pcs")]; echo number_format($po_total_price,2);  $buyer_tot_price+=$po_total_price; $total_po_price+=$po_total_price;?></p></td>
                        <td ><p><? echo $row[csf("remarks")]; ?></p></td>
                    </tr>
                    <?
                    $m++;
                }
                ?>
                    <tr bgcolor="#DDDDDD">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="right">Sub Total:&nbsp;</td>
                        <td align="right"><? echo number_format($buyer_tot_qnty,2); ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align="right"><? echo number_format($buyer_tot_price,2); ?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr  bgcolor="#CCCCCC">
                        <td width="40">&nbsp;</td>
                        <td width="100">&nbsp;</td>
                        <td width="110">&nbsp;</td>
                        <td width="110">&nbsp;</td>
                        <td width="100">&nbsp;</td>
                        <td width="130">&nbsp;</td>
                        <td width="70">&nbsp;</td>
                        <td width="250" align="right">Grand Total:&nbsp;</td>
                        <td width="100"  align="right"><? echo number_format($total_po_qnty,2); ?></td>
                        <td width="70">&nbsp;</td>
                        <td width="50">&nbsp;</td>
                        <td width="100"  align="right"><? echo number_format($total_po_price,2); ?></td>
                        <td >&nbsp;</td>
                    </tr>
                </tbody>
            </table>
            </div>
            <table width="1400" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="rpt_table_footer"  align="left">
                
                    
              
            </table>
        </div>
        <?
	}
	
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	
	disconnect($con);
	exit();
}

if($action=="show_image")
{
	echo load_html_head_contents("Set Entry","../../../../", 1, 1, $unicode);
    extract($_REQUEST);
	//echo "select image_location  from common_photo_library  where master_tble_id='$job_no' and form_name='knit_order_entry' and is_deleted=0 and file_type=1";
	$data_array=sql_select("select image_location  from common_photo_library  where master_tble_id='$job_no' and form_name='knit_order_entry' and is_deleted=0 and file_type=1");
	?>
    <table>
        <tr>
        <?
        foreach ($data_array as $row)
        {
			?>
			<td><img src='../../../../<? echo $row[csf('image_location')]; ?>' height='250' width='300' /></td>
			<?
        }
        ?>
        </tr>
    </table>
    <?
}

if($action=="last_ex_factory_Date")
{
 	echo load_html_head_contents("Last Ex-Factory Details", "../../../../", 1, 1,$unicode,'','');
	extract($_REQUEST);
	//echo $id;//$job_no;
	?>
	<div style="width:100%" align="center">
		<fieldset style="width:500px"> 
        <div class="form_caption" align="center"><strong>Last Ex-Factory Details</strong></div><br />
            <div style="width:100%"> 
            <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                <thead>
                    <tr>
                        <th width="35">SL</th>
                        <th width="90">Ex-fac. Date</th>
                        <th width="120">Challan No.</th>
                        <th width="100">Ex-Fact. Qnty.</th>
                        <th width="150">Trans. Com.</th>
                     </tr>   
                </thead> 	 	
            </table>  
        </div>
        <div style="width:100%; max-height:400px;">
            <table cellpadding="0" width="100%" cellspacing="0" border="1" rules="all" class="rpt_table">
                <?
                $i=1; $job_po_qnty=0; $job_plan_qnty=0; $job_total_price=0;  
                $ex_fac_sql="SELECT id, ex_factory_date, ex_factory_qnty, challan_no, transport_com from pro_ex_factory_mst where po_break_down_id=$id and status_active=1 and is_deleted=0";
                //echo $ex_fac_sql;
                $sql_dtls=sql_select($ex_fac_sql);
                
                foreach($sql_dtls as $row_real)
                { 
                    if ($i%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";                               
                    ?>
                    <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_l<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td> 
                        <td width="90"><? echo change_date_format($row_real[csf("ex_factory_date")]); ?></td>
                        <td width="120"><? echo $row_real[csf("challan_no")]; ?></td>
                        <td width="100" align="right"><? echo $row_real[csf("ex_factory_qnty")]; ?></td>
                        <td width="150"><? echo $row_real[csf("transport_com")]; ?></td>
                    </tr>
                    <? 
                    $rec_qnty+=$row_real[csf("ex_factory_qnty")];
                    $i++;
                }
                ?>
                <tfoot>
                    <th colspan="3">Total</th>
                    <th><? echo number_format($rec_qnty,2); ?></th>
                    <th>&nbsp;</th>
                </tfoot>
            </table>
        </div> 
		</fieldset>
	</div>    
	<?
    exit();	
}
?>	
