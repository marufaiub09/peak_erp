 
<script src="js/jquery.js" type="text/javascript"></script>

<script type="text/javascript">

$(function() {

    // hide all the sub-menus
	$("span.toggle").next().hide();
	// add a link nudging animation effect to each link
    $("#jQ-menu a, #jQ-menu span.toggle").hover(function() {
        $(this).stop().animate( {
			fontSize:"12px",
			paddingLeft:"5px"
			//color:"black"
        }, 100);
    }, function() {
        $(this).stop().animate( {
			fontSize:"12px",
			paddingLeft:"0px"
			//color:"black"
        }, 100);
    });

	// set the cursor of the toggling span elements
	$("span.toggle").css("cursor", "pointer");

	// prepend a plus sign to signify that the sub-menus aren't expanded
	$("span.toggle").prepend("+");
	// $("#jQ-menu ul > li").css('border-left','2px solid #FFFFFF'); //F33
	// $("#jQ-menu ul > li").css('border-top','0.2px solid #FFFFFF');
	// $("#jQ-menu ul > li").css('border-right','2px solid #FFFFFF');
	// $("span.toggle > ul li").css(' border-bottom-left-radius','50px');
	// $("span.toggle > ul li").css(' border-top-left-radius','50px');

	//$("span.toggle").children("li").css('border-left','1px solid red');

	// add a click function that toggles the sub-menu when the corresponding
	// span element is clicked
	$("span.toggle").click(function() {
		//$("span.toggle").next().hide();
		$(this).next().toggle(500);
		// switch the plus to a minus sign or vice-versa
		var v = $(this).html().substring( 0, 1 );
		if ( v == "+" )
			$(this).html( "-" + $(this).html().substring( 1 ) );
		else if ( v == "-" )
			$(this).html( "+" + $(this).html().substring( 1 ) );
	});
});
   
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#jQ-menu ul li a').bind("mouseover", function(){
           /* var color  = $(this).css("background-color");*/

            $(this).css({
                "background": "#00aeef",
                'display' : 'block'
            });

           $(this).bind("mouseout", function(){
                $(this).css("background", 'none');
            })
        })
    })
</script>

<style>
 
#jQ-menu{
	overflow: auto;
	font-size: 12px;
}
#jQ-menu ul {
	list-style-type: none;
	/*background-color:#6D6E71;*/
}

#jQ-menu a, #jQ-menu li {	
	color:rgba(255,255,255,1);
    display: block;
	text-decoration: none;
	padding-bottom: 5px;
	padding-top: 5px;
    white-space:nowrap;
}
#jQ-menu a, #jQ-menu > ul > li {
    color: #ffffff;
}
#jQ-menu li {
    padding-left: 5px;
}

 
</style>
<?php
session_start();
include('includes/common.php');
$uid=$_SESSION['logic_erp']["user_id"];

$_SESSION['module_id']="";
$_SESSION['module_id']=$_GET["module_id"];
$m_id = $_GET["module_id"];
 
$menu_details=array(); 
?>
</head>
<body> 

<!-- Menu Start -->
<div id="jQ-menu"> 



<ul>
		<?php
		$result=sql_select( "SELECT a.m_menu_id,a.menu_name,a.f_location,a.fabric_nature, b.save_priv,b.edit_priv,b.delete_priv,b.approve_priv FROM main_menu a,user_priv_mst b where a.m_module_id='$m_id' and a.position='1' and a.status='1' and a.m_menu_id=b.main_menu_id and b.valid=1 and b.user_id=".$uid." and b.show_priv=1 order by a.slno" );
		$i = 0;
		$leve1counter = count( $result );
		foreach ($result as $r_sql)
		//echo $leve1counter."sas";
 		{
			$i++;
			$result2=sql_select("SELECT a.m_menu_id,a.menu_name,a.f_location,a.fabric_nature, b.save_priv,b.edit_priv,b.delete_priv,b.approve_priv FROM main_menu a,user_priv_mst b where a.m_module_id=$m_id and a.root_menu='".$r_sql[csf('M_MENU_ID')]."'  and a.position=2 and a.status=1 and a.m_menu_id=b.main_menu_id and b.valid=1 and b.user_id=$uid and b.show_priv=1 order by a.slno");
		   
			//"select * from main_menu where m_module_id='$m_id' and root_menu='$r_sql[m_menu_id]' and position='2' and status='1' order by slno" );
        	 if( count( $result2 ) < 1)
			 {
				 
				 $men=$r_sql[csf('MENU_NAME')]."__".$r_sql[csf('fabric_nature')];
			 	 ?>
                 <li><a  id="lid<?php echo $r_sql[csf('M_MENU_ID')]; ?>" href="#one<?php echo $menu[$j]['menu_id']; ?>" onClick="<?php if( trim( $r_sql[csf('F_LOCATION')] ) == "" ) echo "javascript:return false;"; else { ?>javascript:callurl.load( 'main_body', '<?php echo $r_sql[csf('F_LOCATION')]. "?permission=" . $r_sql[csf('SAVE_PRIV')] . "_" .  $r_sql[csf('EDIT_PRIV')] . "_" . $r_sql[csf('DELETE_PRIV')] . "_" .  $r_sql[csf('APPROVE_PRIV')]."&mid=".$r_sql[csf('M_MENU_ID')]."&fnat=".$men; ?>', false, '', '' )<?php } ?>"><?php echo $r_sql[csf('MENU_NAME')]; ?></a></li>
                 <?php
			 }
			 else
			 {
				 echo '<li><span class="toggle">'.$r_sql[csf('MENU_NAME')].'</span> <ul>';
				 foreach ($result2 as $r_sql2)  
				{
					$i++;
					$result3=sql_select("SELECT a.m_menu_id,a.menu_name,a.f_location,a.fabric_nature, b.save_priv,b.edit_priv,b.delete_priv,b.approve_priv  FROM main_menu a,user_priv_mst b where a.m_module_id=$m_id and a.sub_root_menu ='".$r_sql2[csf('M_MENU_ID')]."'  and a.position=3 and a.status=1 and a.m_menu_id=b.main_menu_id and b.valid=1 and b.user_id=$uid and b.show_priv=1 order by a.slno");
					
					 
					 if( count( $result3 ) < 1)
					 {
						$men=$r_sql2[csf('MENU_NAME')]."__".$r_sql2[csf('fabric_nature')];
						 ?>
						 <li><a  id="lid<?php echo $r_sql2[csf('M_MENU_ID')]; ?>" href="#one<?php echo $menu[$j]['menu_id']; ?>" onClick="<?php if( trim( $r_sql2[csf('F_LOCATION')] ) == "" ) echo "javascript:return false;"; else { ?>javascript:callurl.load( 'main_body', '<?php echo $r_sql2[csf('F_LOCATION')] . "?permission=" . $r_sql2[csf('SAVE_PRIV')] . "_" .  $r_sql2[csf('EDIT_PRIV')] . "_" . $r_sql2[csf('DELETE_PRIV')] . "_" .  $r_sql2[csf('APPROVE_PRIV')]."&mid=".$r_sql2[csf('M_MENU_ID')]."&fnat=".$men; ?>', false, '', '' )<?php } ?>"><?php echo  $r_sql2[csf('MENU_NAME')]; ?></a></li>
						 <?php 
					 }
					 else
					 {
						 echo '<li><span class="toggle">'.$r_sql2[csf('MENU_NAME')].'</span> <ul>';
						 foreach ($result3 as $r_sql3)   
						  {
							  $men=$r_sql3[csf('MENU_NAME')]."__".$r_sql3[csf('fabric_nature')];
						  ?>
						 <li><a  id="lid<?php echo $r_sql3[csf('M_MENU_ID')]; ?>" href="#one<?php echo $menu[$j]['menu_id']; ?>" onClick="<?php if( trim( $r_sql3[csf('F_LOCATION')] ) == "" ) echo "javascript:return false;"; else { ?>javascript:callurl.load( 'main_body', '<?php echo $r_sql3[csf('F_LOCATION')]. "?permission=" . $r_sql3[csf('SAVE_PRIV')] . "_" .  $r_sql3[csf('EDIT_PRIV')] . "_" . $r_sql3[csf('DELETE_PRIV')] . "_" .  $r_sql3[csf('APPROVE_PRIV')]."&mid=".$r_sql3[csf('M_MENU_ID')]."&fnat=".$men; ?>', false, '', '' )<?php } ?>"><?php echo $r_sql3[csf('MENU_NAME')]; ?></a></li>
						 <?php
						  }
						  echo '</ul></li>';
					 }
					 
				}
				echo '</ul></li>';
			 }
		}
		
         ?>
		 

</ul>

 </div>
<!-- End Menu --> 

</body>
</html>
