<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');
$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------

$receipe_arr=array(); $liquior_arr=array();  
$receipeData=sql_select("select id,labdip_no, total_liquor from pro_recipe_entry_mst where status_active=1 and is_deleted=0");
foreach($receipeData as $row)
{
	$receipe_arr[$row[csf("id")]]=$row[csf("labdip_no")];
	$liquior_arr[$row[csf("id")]]=$row[csf("total_liquor")];
}

$color_arr=return_library_array("select id,color_name from lib_color" ,"id","color_name");

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 170, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

if($action=="machineNo_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$cbo_company_id=str_replace("'","",$cbo_company_id); 
	?>
    <script>
    function js_set_value(data)
    {
		var data=data.split("_");
		$("#hidden_machine_id").val(data[0]);
		$("#hidden_machine_name").val(data[1]); 
		parent.emailwindow.hide();
    }
	</script>
    
    <input type="hidden" id="hidden_machine_id" name="hidden_machine_id">
    <input type="hidden" id="hidden_machine_name" name="hidden_machine_name">
    
<?php 
	 $location_name=return_library_array( "select location_name,id from  lib_location where is_deleted=0", "id", "location_name"  );
	 $floor=return_library_array( "select floor_name,id from  lib_prod_floor where is_deleted=0", "id", "floor_name"  );
	 $arr=array(0=>$location_name,1=>$floor);  
	 
	 $sql="select location_id,floor_id,machine_no,machine_group,dia_width,gauge,id from lib_machine_name where is_deleted=0 and status_active=1 and company_id='$cbo_company_id' and category_id=2";
     echo create_list_view ( "list_view", "Location Name,Floor Name,Machine No,Machine Group,Dia Width,Gauge", "150,140,100,120,80","750","300",1, $sql, "js_set_value", "id,machine_no","", 1, "location_id,floor_id,0,0,0,0", $arr, "location_id,floor_id,machine_no,machine_group,dia_width,gauge", "", 'setFilterGrid("list_view",-1);','') ;

	exit();	 
}


if($action=="mrr_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(data)
	{
		$("#hidden_sys_id").val(data); 
		parent.emailwindow.hide();
	}
	
	$(document).ready(function(e) {
		setFilterGrid('tbl_list_search',-1);
	});
</script>

</head>
<input type="hidden" id="hidden_sys_id"/>

<body>
<?php
    $batch_arr = return_library_array("select id,batch_no from pro_batch_create_mst where company_id=$company and batch_against<>0 ","id","batch_no");
    $company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	
	if($db_type==0) $year_field="YEAR(insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year";
	else $year_field="";//defined Later
	
	$sql="select id,requ_no,requ_prefix_num,$year_field,company_id,requisition_date,batch_id,recipe_id,method from dyes_chem_issue_requ_mst where company_id=$company order by id";
	$result = sql_select($sql);
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="840" class="rpt_table">
        <thead>
            <th width="40">SL</th>
            <th width="120">Company</th>
            <th width="100">Requisition No</th>
            <th width="60">Year</th>
            <th width="100">Requisition Date </th>
            <th width="100">Method</th> 
            <th width="130">Recipe No</th>               
            <th>Batch No</th>
        </thead>
	</table>
	<div style="width:860px; max-height:300px; overflow-y:scroll" id="list_container_batch" align="left">	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="840" class="rpt_table" id="tbl_list_search">  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if ($i%2==0)  $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$batch_no='';
				$batch_id=explode(",",$row[csf('batch_id')]);	
				foreach($batch_id as $val)
				{
					if($batch_no=="") $batch_no=$batch_arr[$val]; else $batch_no.=", ".$batch_arr[$val]; 
				}
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>);"> 
                    <td width="40"><?php echo $i; ?></td>
                    <td width="120"><p><?php echo $company_arr[$row[csf('company_id')]]; ?></p></td>
                    <td width="100"><p><?php echo $row[csf('requ_prefix_num')]; ?></p></td> 
                    <td width="60" align="center"><p><?php echo $row[csf('year')]; ?></p></td>
                    <td width="100" align="center"><p><?php echo change_date_format($row[csf('requisition_date')]); ?></p></td>
                    <td width="100"><p><?php echo $dyeing_method[$row[csf('method')]]; ?>&nbsp;</p></td>
                    <td width="130"><p><?php echo $row[csf('recipe_id')]; ?></p></td>
                    <td><p><?php echo $batch_no; ?></p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
    </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}
if($action=="populate_data_from_data")
{
	$sql = sql_select("select id, requ_no,company_id,location_id,requisition_date,requisition_basis,recipe_id, method, machine_id from dyes_chem_issue_requ_mst where id=$data");
	foreach($sql as $row)
	{
		echo "document.getElementById('txt_mrr_no').value = '".$row[csf("requ_no")]."';\n"; 
		echo "document.getElementById('update_id').value = '".$row[csf("id")]."';\n"; 
		echo "document.getElementById('cbo_company_name').value = '".$row[csf("company_id")]."';\n";  
		echo "document.getElementById('cbo_location_name').value = '".$row[csf("location_id")]."';\n";  
		echo "document.getElementById('txt_requisition_date').value = '".change_date_format($row[csf("requisition_date")])."';\n"; 
		echo "document.getElementById('cbo_receive_basis').value = '".$row[csf("requisition_basis")]."';\n"; 
		echo "document.getElementById('cbo_method').value = '".$row[csf("method")]."';\n";
		echo "document.getElementById('machine_id').value = '".$row[csf("machine_id")]."';\n"; 
		
		$machine_name="";
		if($row[csf("machine_id")]>0)
		{
			$machine_name=return_field_value("machine_no","lib_machine_name","id=".$row[csf('machine_id')]);
		}
		echo "document.getElementById('txt_machine_no').value = '".$machine_name."';\n";
		
		echo "get_php_form_data('".$row[csf("recipe_id")]."', 'populate_data_from_recipe_popup', 'requires/chemical_dyes_issue_requisition_controller' );\n";
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_chemical_dyes_issue_requisition',1);\n";  
		exit();
	}
}

if ($action=="labdip_popup")
{
	echo load_html_head_contents("Labdip No Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		
		var selected_id = new Array(); var prevsubprocess_id='';  var prevseq_no='';
		
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_recipe_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		
		function js_set_value( str ) 
		{
			var currsubprocess_id=$('#subprocess_id' + str).val();
			var currseq_no=$('#seq_no' + str).val();
			if(prevsubprocess_id=='' || selected_id.length==0) 
			{
				prevsubprocess_id=$('#subprocess_id' + str).val();
				prevseq_no=$('#seq_no' + str).val();
			}
			else
			{
				if(currsubprocess_id!=prevsubprocess_id || currseq_no!=prevseq_no)
				{
					alert("Item and Sub Process of Selected Recipe Not Uniformed.");
					return;
				}
			}
			
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#recipe_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#recipe_id' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#recipe_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			
			id = id.substr( 0, id.length - 1 );
			
			$('#hidden_recipe_id').val(id);
			$('#hidden_subprocess_id').val(currsubprocess_id);
		}
    </script>
</head>

<body>
<div align="center" style="width:890px;">
    <form name="searchlabdipfrm" id="searchlabdipfrm">
        <fieldset style="width:885px;">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" width="880" border="1" rules="all" class="rpt_table">
                <thead>
                    <th>Recipe Date Range</th>
                    <th>Search By</th>
                    <th width="250" id="search_by_td_up">Enter Recipe System No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" name="hidden_recipe_id" id="hidden_recipe_id" class="text_boxes" value="">
                        <input type="hidden" name="hidden_subprocess_id" id="hidden_subprocess_id" class="text_boxes" value="">
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px;">To<input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px;">
                    </td>
                    <td>
						<?php
							$search_by_arr=array(1=>"Recipe System No",2=>"Labdip No");//,3=>"Recipe Description"
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td id="search_by_td">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td>
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value+'_'+'<?php echo $recipe_id; ?>', 'create_recipe_search_list_view', 'search_div', 'chemical_dyes_issue_requisition_controller', 'set_all();setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </table>
            <div style="width:100%; margin-top:5px; margin-left:3px;" id="search_div" align="left"></div>
    	</fieldset>
    </form>
</div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_recipe_search_list_view")
{
	$data = explode("_",$data);
	$search_string=trim($data[0]);
	$search_by=$data[1];
	$start_date =$data[2];
	$end_date =$data[3];
	$company_id =$data[4];
	$recipe_id =$data[5];
	
	$batch_arr=return_library_array( "select id,batch_no from pro_batch_create_mst",'id','batch_no');
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and a.recipe_date between '".change_date_format(trim($start_date), "yyyy-mm-dd", "-")."' and '".change_date_format(trim($end_date), "yyyy-mm-dd", "-")."'";
		}
		else
		{
			$date_cond="and a.recipe_date between '".change_date_format(trim($start_date), "yyyy-mm-dd", "-",1)."' and '".change_date_format(trim($end_date), "yyyy-mm-dd", "-",1)."'";
		}
	}
	else
	{
		$date_cond="";
	}
	if(trim($data[0])!="")
	{
		if($search_by==1)
			$search_field_cond="and a.id like '$search_string'";
		else if($search_by==2)
			$search_field_cond="and a.labdip_no like '".$search_string."%'";
	}
	else
	{
		$search_field_cond="";
	}
	
	if($db_type==0)
	{
		$sql = "select a.id, a.labdip_no, a.recipe_date, a.order_source, a.style_or_order, a.batch_id, a.color_id, a.color_range, a.total_liquor, group_concat(b.sub_process_id order by b.id) as sub_process_id, group_concat(concat_ws('**',b.sub_process_id,b.prod_id,b.seq_no) order by b.id) as seq_no from pro_recipe_entry_mst a, pro_recipe_entry_dtls b where a.id=b.mst_id and a.company_id='$company_id' and b.ratio>0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $search_field_cond $date_cond group by a.id"; 
	}
	else
	{
		$sql = "select a.id, a.labdip_no, a.recipe_date, a.order_source, a.style_or_order, a.batch_id, a.color_id, a.color_range, a.total_liquor, LISTAGG(b.sub_process_id, ',') WITHIN GROUP (ORDER BY b.id) as sub_process_id, LISTAGG(b.sub_process_id || '**' || b.prod_id || '**' || b.seq_no, ',') WITHIN GROUP (ORDER BY b.id) as seq_no from pro_recipe_entry_mst a, pro_recipe_entry_dtls b where a.id=b.mst_id and a.company_id='$company_id' and b.ratio>0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $search_field_cond $date_cond group by a.id, a.labdip_no, a.recipe_date, a.order_source, a.style_or_order, a.batch_id, a.color_id, a.color_range, a.total_liquor"; 
	}
	//echo $sql;
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="880" class="rpt_table" >
        <thead>
            <th width="35">SL</th>
            <th width="60">Recipe No</th>
            <th width="70">Labdip No</th>
            <th width="75">Recipe Date</th>
            <th width="170">Sub Process</th>
            <th width="90">Batch No</th>
            <th width="110">Booking No</th>
            <th width="80">Color</th>
            <th width="80">Color Range</th>
            <th>Total Liquor</th>
        </thead>
    </table>
    <div style="width:880px; overflow-y:scroll; max-height:220px;" id="buyer_list_view" align="center">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="862" class="rpt_table" id="tbl_list_search" >
        <?php
            $i=1; $recipe_row_id=''; $hidden_recipe_id=explode(",",$recipe_id);
            $nameArray=sql_select( $sql );
            foreach ($nameArray as $selectResult)
            {
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$subprocess='';
				$sub_process_id=array_unique(explode(",",$selectResult[csf('sub_process_id')]));
				foreach($sub_process_id as $process_id)
				{
					$subprocess.=$dyeing_sub_process[$process_id].",";
				}
				
				$seq_no=array_unique(explode(",",$selectResult[csf('seq_no')]));
				
				if(in_array($selectResult[csf('id')],$hidden_recipe_id)) 
				{ 
					if($recipe_row_id=="") $recipe_row_id=$i; else $recipe_row_id.=",".$i;
				}
            ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i; ?>)"> 
                    <td width="35" align="center"><?php echo $i; ?>
                    	<input type="hidden" name="recipe_id" id="recipe_id<?php echo $i; ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>
                        <input type="hidden" name="subprocess_id" id="subprocess_id<?php echo $i; ?>" value="<?php echo implode(",",$sub_process_id); ?>"/>
                        <input type="hidden" name="seq_no" id="seq_no<?php echo $i; ?>" value="<?php echo implode(",",$seq_no); ?>"/>
                    </td>	
                    <td width="60"><p><?php echo $selectResult[csf('id')]; ?></p></td>
                    <td width="70"><p><?php echo $selectResult[csf('labdip_no')]; ?></p></td>
                    <td width="75" align="center"><?php echo change_date_format($selectResult[csf('recipe_date')]); ?>&nbsp;</td>
                    <td width="170"><p><?php echo substr($subprocess,0,-1); ?></p></td> 
                    <td width="90"><p><?php echo $batch_arr[$selectResult[csf('batch_id')]]; ?>&nbsp;</p></td> 
                    <td width="110"><p><?php echo $selectResult[csf('style_or_order')]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $color_range[$selectResult[csf('color_range')]]; ?>&nbsp;</p></td>
                    <td align="right"><?php echo number_format($selectResult[csf('total_liquor')],2,'.',''); ?></td>	
                </tr>
            <?php
                $i++;
            }
        ?>
        </table>
    </div>
    <table width="750">
         <tr>
            <td align="center" >
                <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="parent.emailwindow.hide();" style="width:100px" />
                <input type="hidden" name="txt_recipe_row_id" id="txt_recipe_row_id" value="<?php echo $recipe_row_id; ?>"/>
            </td>
        </tr>
    </table>
<?php
	exit();
}

if($action=='populate_data_from_recipe_popup')
{
	if($db_type==0) 
	{
		$data_array=sql_select("select group_concat(id) as recipe_id, group_concat(batch_id) as batch_id, sum(total_liquor) as total_liquor from pro_recipe_entry_mst where id in($data)");
	}
	else
	{
		$data_array=sql_select("select listagg(id,',') within group (order by id) as recipe_id, listagg(batch_id,',') within group (order by batch_id) as batch_id, sum(total_liquor) as total_liquor from pro_recipe_entry_mst where id in($data)");	
	}
	
	$batch_id=implode(",",array_unique(explode(",",$data_array[0][csf("batch_id")])));
	if($db_type==0) 
	{
		$batchdata_array=sql_select("select group_concat(batch_no) as batch_no,  sum(batch_weight) as batch_weight from pro_batch_create_mst where id in($batch_id)");
	}
	else
	{
		$batchdata_array=sql_select("select listagg(CAST(batch_no  AS VARCHAR2(4000)),',') within group (order by id) as batch_no, sum(batch_weight) as batch_weight from pro_batch_create_mst where id in($batch_id)");	
	}
	
	echo "document.getElementById('txt_recipe_id').value 				= '".$data_array[0][csf("recipe_id")]."';\n";
	echo "document.getElementById('txt_recipe_no').value 				= '".$data_array[0][csf("recipe_id")]."';\n";
	echo "document.getElementById('txt_batch_no').value 				= '".$batchdata_array[0][csf("batch_no")]."';\n";
	echo "document.getElementById('txt_batch_id').value 				= '".$batch_id."';\n";
	echo "document.getElementById('txt_tot_liquor').value 				= '".$data_array[0][csf("total_liquor")]."';\n";
	echo "document.getElementById('txt_batch_weight').value 			= '".$batchdata_array[0][csf("batch_weight")]."';\n";
	exit();
}

if($action=="item_details")
{
	$data=explode("**",$data);
	$company_id=$data[0];
	$sub_process_id=$data[1];
	$recipe_id=$data[2];
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1105" class="rpt_table" >
        <thead>
            <th width="30">SL</th>
            <th width="83">Sub Process</th>
            <th width="50">Prod. ID</th>
            <th width="80">Item Category</th>
            <th width="90">Group</th>
            <th width="80">Sub Group</th>
            <th width="110">Item Description</th>
            <th width="40">UOM</th>
            <th width="50">Seq. No.</th>
            <th width="75">Dose Base</th>
            <th width="72">Ratio</th>
            <th width="80">Recipe Qnty.</th>
            <th width="53">Adj%.</th>
            <th width="87">Adj. Type</th>
            <th>Reqn. Qnty.</th>
        </thead>
    </table>
    <div style="width:1105px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1088" class="rpt_table" id="tbl_list_search">
            <tbody>
            <?php
                $item_group_arr=return_library_array( "select id, item_name from lib_item_group", "id", "item_name" );
				$batchWeight_arr=return_library_array( "select id, batch_weight from pro_batch_create_mst", "id", "batch_weight" );
				 
				$sql="select p.total_liquor, p.batch_id, a.id, a.item_category_id, a.item_group_id, a.sub_group_name, a.item_description, a.item_size, a.unit_of_measure, b.sub_process_id, b.dose_base, b.ratio, b.seq_no from pro_recipe_entry_mst p, product_details_master a, pro_recipe_entry_dtls b where p.id=b.mst_id and a.id=b.prod_id and b.mst_id in($recipe_id) and b.sub_process_id in($sub_process_id) and b.ratio>0 and b.status_active=1 and b.is_deleted=0 and a.company_id='$company_id' and a.item_category_id in(5,6,7) and a.status_active=1 and a.is_deleted=0 order by b.sub_process_id, b.seq_no";
				$i=1; $subprocessDataArr=array(); $subprocessProdQntyArr=array(); $prodDataArr=array();
				$nameArray=sql_select( $sql );
				foreach($nameArray as $selectResult)
				{
					$subprocessDataArr[$selectResult[csf('sub_process_id')]].=$selectResult[csf('id')].",";
					$prodDataArr[$selectResult[csf('id')]]=$selectResult[csf('item_category_id')]."**".$selectResult[csf('item_group_id')]."**".$selectResult[csf('sub_group_name')]."**".$selectResult[csf('item_description')]."**".$selectResult[csf('item_size')]."**".$selectResult[csf('unit_of_measure')];
					
					$ratio=$selectResult[csf('ratio')];
					if($selectResult[csf('dose_base')]==1)
					{
						$perc_calculate_qnty=$selectResult[csf('total_liquor')];
						$recipe_qnty=($perc_calculate_qnty*$ratio)/1000;
					}
					else if($selectResult[csf('dose_base')]==2)
					{
						$perc_calculate_qnty=$batchWeight_arr[$selectResult[csf('batch_id')]];
						$recipe_qnty=($perc_calculate_qnty*$ratio)/100;
					}

					$subprocessProdQntyArr[$selectResult[csf('sub_process_id')]][$selectResult[csf('id')]]['dosebase'].=$selectResult[csf('dose_base')].",";
					$subprocessProdQntyArr[$selectResult[csf('sub_process_id')]][$selectResult[csf('id')]]['recipe_qnty']+=$recipe_qnty;
					$subprocessProdQntyArr[$selectResult[csf('sub_process_id')]][$selectResult[csf('id')]]['lq_or_bw_qnty']+=$perc_calculate_qnty;
					$subprocessProdQntyArr[$selectResult[csf('sub_process_id')]][$selectResult[csf('id')]]['seq_no']=$selectResult[csf('seq_no')];
				}
				
				$sub_process_id=explode(",",$sub_process_id); $dosebase_mismatch_prod_id_arr=array();
				foreach($sub_process_id as $process_id)
				{
					$subprocessData=array_unique(explode(",",substr($subprocessDataArr[$process_id],0,-1)));
					foreach($subprocessData as $prod_id)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						
						$subprocessData=explode("**",$prodDataArr[$prod_id]);
						$item_category_id=$subprocessData[0];
						$item_group_id=$subprocessData[1];
						$sub_group_name=$subprocessData[2];
						$item_description=$subprocessData[3]." ".$subprocessData[4];
						$unit_of_measure=$subprocessData[5];
						$seq_no=$subprocessProdQntyArr[$process_id][$prod_id]['seq_no'];
						
						$dosebaseData=array_unique(explode(",",substr($subprocessProdQntyArr[$process_id][$prod_id]['dosebase'],0,-1)));
						if(count($dosebaseData)>1)
						{
							$dosebase=0;
							$ratio=0;
							$recipe_qnty=0;
							$dosebase_mismatch_prod_id[$process_id].=$prod_id.",";
						}
						else
						{
							$dosebase=implode(",",$dosebaseData);
							$recipe_qnty=number_format($subprocessProdQntyArr[$process_id][$prod_id]['recipe_qnty'],4,'.','');
							$lq_or_bw_qnty=$subprocessProdQntyArr[$process_id][$prod_id]['lq_or_bw_qnty'];
							if($dosebase==1)
							{
								$ratio=number_format(($recipe_qnty*1000)/$lq_or_bw_qnty,4,'.','');
							}
							else
							{
								$ratio=number_format(($recipe_qnty*100)/$lq_or_bw_qnty,4,'.','');
							}
						}
						
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>" style="vertical-align:middle"> 
							<td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td>
							<td width="83" id="subprocess_id_<?php echo $i; ?>"><?php echo $dyeing_sub_process[$process_id]; ?>
								<input type="hidden" name="txt_subprocess_id[]" id="txt_subprocess_id_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $process_id; ?>">
							</td>	
							<td width="50" id="product_id_<?php echo $i; ?>"><?php echo $prod_id; ?>
								<input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px" value="<?php echo $prod_id;?>">
							</td>
							<td width="80"><p><?php echo $item_category[$item_category_id]; ?></p>
								<input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px" value="<?php echo $item_category_id; ?>">
							</td>
							<td width="90" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$item_group_id]; ?>&nbsp;</p></td>
							<td width="80" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $sub_group_name; ?>&nbsp;</p></td>
							<td width="110" id="item_description_<?php echo $i; ?>"><p><?php echo $item_description; ?></p></td> 
							<td width="40" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$unit_of_measure]; ?></td>
                            <td width="50" align="center" id="seq_no_<?php echo $i; ?>"><?php echo $seq_no; ?></td>
							<td width="75" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 70, $dose_base, "", 1, "- Select Dose Base -",$dosebase,"",1); ?></td>
							<td width="72" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px" value="<?php echo $ratio; ?>" disabled></td>
							<td width="80" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:67px"  value="<?php echo $recipe_qnty; ?>" disabled ></td>
							<td width="53" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" value="" onKeyUp="calculate_requs_qty(<?php echo $i; ?>)"></td>
							<td width="87" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 80, $increase_decrease, "", 1, "- Select -","","calculate_requs_qty($i)"); ?></td>
							<td align="center" id="reqn_qnty_<?php echo $i; ?>">
							<input type="text" name="reqn_qnty_edit[]" id="reqn_qnty_edit_<?php echo $i;?>" class="text_boxes_numeric" value="<?php echo $recipe_qnty; ?>" style="width:75px" disabled>
							<input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="">
							<input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $recipe_qnty; ?>">
                            <input type="hidden" name="txt_seq_no[]" id="txt_seq_no_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $seq_no; ?>">	
							</td>
						</tr>
						<?php
						$i++;
					}
				}
            ?>
            </tbody>
        </table>
    </div>
    <div>
		<?php
        	//print($dosebase_mismatch_prod_id);
        ?> 
    </div>
<?php
	exit();	
}

if($action=="item_details_for_update")
{
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1105" class="rpt_table" >
        <thead>
            <th width="30">SL</th>
            <th width="83">Sub Process</th>
            <th width="50">Prod. ID</th>
            <th width="80">Item Category</th>
            <th width="90">Group</th>
            <th width="80">Sub Group</th>
            <th width="110">Item Description</th>
            <th width="40">UOM</th>
            <th width="50">Seq. No.</th>
            <th width="75">Dose Base</th>
            <th width="72">Ratio</th>
            <th width="80">Recipe Qnty.</th>
            <th width="53">Adj%.</th>
            <th width="87">Adj. Type</th>
            <th>Reqn. Qnty.</th>
        </thead>
    </table>
    <div style="width:1105px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1088" class="rpt_table" id="tbl_list_search">
            <tbody>
            <?php
                $item_group_arr=return_library_array( "select id, item_name from lib_item_group", "id", "item_name" );
				$sql="select a.id, a.item_category_id, a.item_group_id, a.sub_group_name, a.item_description, a.item_size, a.unit_of_measure, b.id as dtls_id, b.sub_process, b.dose_base, b.ratio, b.recipe_qnty, b.adjust_percent, b.adjust_type, b.required_qnty, b.req_qny_edit, b.seq_no from product_details_master a, dyes_chem_issue_requ_dtls b where a.id=b.product_id and b.mst_id=$data and b.status_active=1 and b.is_deleted=0 and a.item_category_id in(5,6,7) and a.status_active=1 and a.is_deleted=0 order by b.id";
				$i=1;
				$nameArray=sql_select( $sql );
				foreach($nameArray as $selectResult)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";

					$dosebase=$selectResult[csf('dose_base')];
					$recipe_qnty=$selectResult[csf('recipe_qnty')];
					$ratio=$selectResult[csf('ratio')];
					
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>" style="vertical-align:middle"> 
						<td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td>
						<td width="83" id="subprocess_id_<?php echo $i; ?>"><?php echo $dyeing_sub_process[$selectResult[csf('sub_process')]]; ?>
							<input type="hidden" name="txt_subprocess_id[]" id="txt_subprocess_id_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $selectResult[csf('sub_process')]; ?>">
						</td>	
						<td width="50" id="product_id_<?php echo $i; ?>"><?php echo $selectResult[csf('id')]; ?>
							<input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('id')]; ?>">
						</td>
						<td width="80"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p>
							<input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px" value="<?php echo $selectResult[csf('item_category_id')]; ?>">
						</td>
						<td width="90" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$selectResult[csf('item_group_id')]]; ?>&nbsp;</p></td>
						<td width="80" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $selectResult[csf('sub_group_name')]; ?>&nbsp;</p></td>
						<td width="110" id="item_description_<?php echo $i; ?>"><p><?php echo $selectResult[csf('item_description')]." ".$selectResult[csf('item_size')]; ?></p></td> 
						<td width="40" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$selectResult[csf('unit_of_measure')]]; ?></td>
                        <td width="50" align="center" id="seq_no_<?php echo $i; ?>"><?php echo $selectResult[csf('seq_no')]; ?></td>
						<td width="75" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 70, $dose_base, "", 1, "- Select Dose Base -",$dosebase,"",1); ?></td>
						<td width="72" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px" value="<?php echo number_format($ratio,4,'.',''); ?>" disabled></td>
						<td width="80" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:67px"  value="<?php echo number_format($recipe_qnty,4,'.',''); ?>" disabled ></td>
						<td width="53" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" value="<?php echo $selectResult[csf('adjust_percent')]; ?>" onKeyUp="calculate_requs_qty(<?php echo $i; ?>)"></td>
						<td width="87" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 80, $increase_decrease, "", 1, "- Select -",$selectResult[csf('adjust_type')],"calculate_requs_qty($i)"); ?></td>
						<td align="center" id="reqn_qnty_<?php echo $i; ?>">
						<input type="text" name="reqn_qnty_edit[]" id="reqn_qnty_edit_<?php echo $i;?>" class="text_boxes_numeric" value="<?php echo number_format($selectResult[csf('req_qny_edit')],4,'.',''); ?>" style="width:75px" disabled>
						<input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="<?php echo $selectResult[csf('dtls_id')]; ?>">
						<input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $selectResult[csf('required_qnty')]; ?>">	
                        <input type="hidden" name="txt_seq_no[]" id="txt_seq_no_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $selectResult[csf('seq_no')]; ?>">	
						</td>
					</tr>
					<?php
					$i++;
				}
            ?>
            </tbody>
        </table>
    </div>
<?php
	exit();	
}

if($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		$mst_id=""; $requ_no="";
		
		if(str_replace("'","",$update_id)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later		
			
			$new_requ_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '','DCIR', date("Y",time()), 5, "select requ_no_prefix,requ_prefix_num from dyes_chem_issue_requ_mst where company_id=$cbo_company_name and $year_cond=".date('Y',time())." order by id desc ", "requ_no_prefix", "requ_prefix_num" ));
			$id=return_next_id( "id", "dyes_chem_issue_requ_mst", 1 ) ;
			$field_array="id,requ_no,requ_no_prefix,requ_prefix_num,company_id,location_id,requisition_date,requisition_basis,batch_id,recipe_id,method,machine_id,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_requ_no[0]."','".$new_requ_no[1]."',".$new_requ_no[2].",".$cbo_company_name.",".$cbo_location_name.",".$txt_requisition_date.",".$cbo_receive_basis.",".$txt_batch_id.",".$txt_recipe_id.",".$cbo_method.",".$machine_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";

			$mst_id=$id; 
			$requ_no=$new_requ_no[0];
		}
		else
		{
			$field_array_up="location_id*requisition_date*recipe_id*batch_id*method*machine_id";
			$data_array_up=$cbo_location_name."*".$txt_requisition_date."*".$txt_recipe_id."*".$txt_batch_id."*".$cbo_method."*".$machine_id;
			$mst_id=str_replace("'","",$update_id);
			$requ_no=str_replace("'","",$txt_mrr_no);
		}
		
		$id_att=return_next_id( "id", "dyes_chem_requ_recipe_att", 1 ) ;
		$field_array_att="id,mst_id,recipe_id";
		$recipe_id_all=explode(",",str_replace("'","",$txt_recipe_id));
		foreach($recipe_id_all as $recipe_id)
		{
			if($data_array_att!="") $data_array_att.=",";
			$data_array_att.="(".$id_att.",".$mst_id.",".$recipe_id.")";
			$id_att=$id_att+1;
		}
		
		$id_dtls=return_next_id( "id", "dyes_chem_issue_requ_dtls", 1 ) ;
		$field_array_dtls="id,mst_id,requ_no,batch_id,recipe_id,requisition_basis,sub_process,product_id,item_category,dose_base,ratio, recipe_qnty, adjust_percent, adjust_type, required_qnty,req_qny_edit,seq_no,inserted_by,insert_date";
		for ($i=1;$i<=$total_row;$i++)
		{
			$txt_prod_id="txt_prod_id_".$i;
			$txt_item_cat="txt_item_cat_".$i;
			$cbo_dose_base="cbo_dose_base_".$i;
			$txt_ratio="txt_ratio_".$i;
			$txt_recipe_qnty="txt_recipe_qnty_".$i;
			$txt_adj_per="txt_adj_per_".$i;
			$cbo_adj_type="cbo_adj_type_".$i;
			$txt_reqn_qnty="txt_reqn_qnty_".$i;
			$txt_reqn_qnty_edit="reqn_qnty_edit_".$i;
			$updateIdDtls="updateIdDtls_".$i;
			$txt_subprocess_id="txt_subprocess_id_".$i;
			$txt_seq_no="txt_seq_no_".$i;
			
			if($data_array_dtls!="") $data_array_dtls .=",";
			$data_array_dtls .="(".$id_dtls.",".$mst_id.",'".$requ_no."',".$txt_batch_id.",".$txt_recipe_id.",".$cbo_receive_basis.",".$$txt_subprocess_id.",".$$txt_prod_id.",".$$txt_item_cat.",".$$cbo_dose_base.",".$$txt_ratio.",".$$txt_recipe_qnty.",".$$txt_adj_per.",".$$cbo_adj_type.",".$$txt_reqn_qnty.",".$$txt_reqn_qnty_edit.",".$$txt_seq_no.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_dtls=$id_dtls+1;
		}
		
		if(str_replace("'","",$update_id)=="")
		{
			//echo  "INSERT INTO dyes_chem_issue_requ_mst (".$field_array.") VALUES ".$data_array."";
			$rID=sql_insert("dyes_chem_issue_requ_mst",$field_array,$data_array,1);
			$delete_att=true;
		}
		else
		{
			$rID=sql_update("dyes_chem_issue_requ_mst",$field_array_up,$data_array_up,"id",$update_id,1);
			$delete_att=execute_query( "delete from dyes_chem_requ_recipe_att where mst_id=$update_id",0);
		}
		
		$rID_att=sql_insert("dyes_chem_requ_recipe_att",$field_array_att,$data_array_att,1);
		
		//echo  "INSERT INTO dyes_chem_issue_requ_dtls (".$field_array_dtls.") VALUES ".$data_array_dtls.""; 
		$rID_dtls=sql_insert("dyes_chem_issue_requ_dtls",$field_array_dtls,$data_array_dtls,1); 
		//echo "10**".($rID ."&&". $rID_att ."&&". $rID_dtls ."&&". $delete_att);die;
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $rID_att && $rID_dtls && $delete_att)
			{
				mysql_query("COMMIT");  
				echo "0**".$requ_no."**".$mst_id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$requ_no."**".$mst_id;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID_att && $rID_dtls && $delete_att)
			{
				oci_commit($con); 
				echo "0**".$requ_no."**".$mst_id;
			}
			else
			{
				oci_rollback($con);  
				echo "10**".$requ_no."**".$mst_id;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)  // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		$field_array_up="location_id*requisition_date*recipe_id*batch_id*method*machine_id";
		$data_array=$cbo_location_name."*".$txt_requisition_date."*".$txt_recipe_id."*".$txt_batch_id."*".$cbo_method."*".$machine_id;
		$mst_id=str_replace("'","",$update_id);
		$requ_no=str_replace("'","",$txt_mrr_no);
		
		$id_att=return_next_id( "id", "dyes_chem_requ_recipe_att", 1 ) ;
		$field_array_att="id,mst_id,recipe_id";
		$recipe_id_all=explode(",",str_replace("'","",$txt_recipe_id));
		foreach($recipe_id_all as $recipe_id)
		{
			if($data_array_att!="") $data_array_att.=",";
			$data_array_att .="(".$id_att.",".$mst_id.",".$recipe_id.")";
			$id_att=$id_att+1;
		}
		
		$id_dtls=return_next_id( "id", "dyes_chem_issue_requ_dtls", 1 ) ;
		$field_array_dtls="id,mst_id,requ_no,batch_id,recipe_id,requisition_basis,sub_process,product_id,item_category,dose_base,ratio, recipe_qnty, adjust_percent, adjust_type, required_qnty,req_qny_edit,seq_no,inserted_by,insert_date";

		for ($i=1;$i<=$total_row;$i++)
		{
			$txt_prod_id="txt_prod_id_".$i;
			$txt_item_cat="txt_item_cat_".$i;
			$cbo_dose_base="cbo_dose_base_".$i;
			$txt_ratio="txt_ratio_".$i;
			$txt_recipe_qnty="txt_recipe_qnty_".$i;
			$txt_adj_per="txt_adj_per_".$i;
			$cbo_adj_type="cbo_adj_type_".$i;
			$txt_reqn_qnty="txt_reqn_qnty_".$i;
			$txt_reqn_qnty_edit="reqn_qnty_edit_".$i;
			$updateIdDtls="updateIdDtls_".$i;
			$txt_subprocess_id="txt_subprocess_id_".$i;
			$txt_seq_no="txt_seq_no_".$i;
			
			if($data_array_dtls!="") $data_array_dtls .=",";
			$data_array_dtls .="(".$id_dtls.",".$mst_id.",'".$requ_no."',".$txt_batch_id.",".$txt_recipe_id.",".$cbo_receive_basis.",".$$txt_subprocess_id.",".$$txt_prod_id.",".$$txt_item_cat.",".$$cbo_dose_base.",".$$txt_ratio.",".$$txt_recipe_qnty.",".$$txt_adj_per.",".$$cbo_adj_type.",".$$txt_reqn_qnty.",".$$txt_reqn_qnty_edit.",".$$txt_seq_no.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_dtls=$id_dtls+1;
			
			/*if(str_replace("'",'',$$updateIdDtls)!="")
			{
				$id_arr[]=str_replace("'",'',$$updateIdDtls);
				$data_array_up[str_replace("'",'',$$updateIdDtls)] =explode("*",("".$$txt_adj_per."*".$$cbo_adj_type."*".$$txt_reqn_qnty."*".$$txt_reqn_qnty_edit."*".$$txt_seq_no."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));

			}
			else
			{
				if($data_array_dtls!="") $data_array_dtls .=",";
				$data_array_dtls .="(".$id_dtls.",".$mst_id.",'".$requ_no."',".$txt_batch_id.",".$txt_recipe_id.",".$cbo_receive_basis.",".$$txt_subprocess_id.",".$$txt_prod_id.",".$$txt_item_cat.",".$$cbo_dose_base.",".$$txt_ratio.",".$$txt_recipe_qnty.",".$$txt_adj_per.",".$$cbo_adj_type.",".$$txt_reqn_qnty.",".$$txt_reqn_qnty_edit.",".$$txt_seq_no.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id_dtls=$id_dtls+1;
			}*/
		}
		
		$rID=sql_update("dyes_chem_issue_requ_mst",$field_array_up,$data_array,"id",$update_id,1);
		$rID_att=sql_insert("dyes_chem_requ_recipe_att",$field_array_att,$data_array_att,1);
		$delete_att=execute_query( "delete from dyes_chem_requ_recipe_att where mst_id=$update_id",0);
		$delete_dtls=execute_query( "delete from dyes_chem_issue_requ_dtls where mst_id=$update_id",0);
		
		$rID_dtls=true;
		if($data_array_dtls!="")
		{
			//echo "INSERT INTO dyes_chem_issue_requ_dtls (".$field_array_dtls.") VALUES ".$data_array_dtls.""; 
			$rID_dtls=sql_insert("dyes_chem_issue_requ_dtls",$field_array_dtls,$data_array_dtls,1); 
		}
		
		/*$rID_dtls_update=true;
		if(count($data_array_up)>0)
		{
			//echo bulk_update_sql_statement( "dyes_chem_issue_requ_dtls", "id", $field_array_dtls_update, $data_array_up, $id_arr );
			$rID_dtls_update=execute_query(bulk_update_sql_statement("dyes_chem_issue_requ_dtls", "id", $field_array_dtls_update, $data_array_up, $id_arr ));
		}*/
		//echo "10**".$rID."**".$rID_att."**".$rID_att."**".$delete_att."**".$rID_dtls_update;die;
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $rID_att && $rID_dtls && $delete_att && $delete_dtls)
			{
				mysql_query("COMMIT");  
				echo "1**".$requ_no."**".$mst_id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$requ_no."**".$mst_id;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID_att && $rID_dtls && $delete_att && $delete_dtls)
			{
				oci_commit($con); 
				echo "1**".$requ_no."**".$mst_id;
			}
			else
			{
				oci_rollback($con);  
				echo "10**".$requ_no."**".$mst_id;
			}
		}
		disconnect($con);
		die;
	}
}

if($action=="chemical_dyes_issue_requisition_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql="select a.id, a.requ_no, a.location_id, a.requisition_date, a.requisition_basis, a.batch_id, a.recipe_id, a.machine_id from dyes_chem_issue_requ_mst a where a.id='$data[1]' and a.company_id='$data[0]'";
	$dataArray=sql_select($sql);
	
	$recipe_id=$dataArray[0][csf('recipe_id')];
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$country_arr=return_library_array( "select id, country_name from lib_country", "id", "country_name"  );
	$color_arr=return_library_array( "select id, color_name from lib_color", "id", "color_name"  );

	$order_id_arr=return_library_array( "select id, booking_id from  pro_recipe_entry_mst", "id", "booking_id"  );
	$order_no_arr=return_library_array( "select id, booking_no from  wo_booking_mst", "id", "booking_no"  );
	$job_no_arr=return_library_array( "select id, job_no from wo_booking_mst", "id", "job_no"  );
	
	if($db_type==0) 
	{
		$data_array=sql_select("select group_concat(id) as recipe_id, group_concat(batch_id) as batch_id, sum(total_liquor) as total_liquor, group_concat(concat_ws(':',batch_ratio,liquor_ratio) order by id) as ratio from pro_recipe_entry_mst where id in($recipe_id)");
	}
	else
	{
		$data_array=sql_select("select listagg(id,',') within group (order by id) as recipe_id, listagg(batch_id,',') within group (order by batch_id) as batch_id, sum(total_liquor) as total_liquor, listagg(batch_ratio || ':' || liquor_ratio,',') within group (order by id) as ratio from pro_recipe_entry_mst where id in($recipe_id)");
	}
	
	$batch_id=implode(",",array_unique(explode(",",$data_array[0][csf("batch_id")])));
	if($db_type==0) 
	{
		$batchdata_array=sql_select("select group_concat(batch_no) as batch_no, sum(batch_weight) as batch_weight, group_concat(distinct color_id) as color_id from pro_batch_create_mst where id in($batch_id)");
	}
	else
	{
		$batchdata_array=sql_select("select listagg(CAST(batch_no AS VARCHAR2(4000)),',') within group (order by id) as batch_no, listagg(color_id ,',') within group (order by id) as color_id, sum(batch_weight) as batch_weight from pro_batch_create_mst where id in($batch_id)");	
	}
	
	$po_no=''; $job_no=''; $buyer_name='';
	$po_data=sql_select("select distinct b.po_number, c.job_no, c.buyer_name from pro_batch_create_dtls a, wo_po_break_down b, wo_po_details_master c where a.po_id=b.id and b.job_no_mst=c.job_no and a.status_active=1 and a.is_deleted=0 and a.mst_id in(".$batch_id.") ");
	foreach($po_data as $row)
	{
		$po_no.=$row[csf('po_number')].","; 
		$job_no.=$row[csf('job_no')].",";
		$buyer_name.=$buyer_library[$row[csf('buyer_name')]].",";
	}
	
	$po_no=implode(", ",array_unique(explode(",",substr($po_no,0,-1))));
	$job_no=implode(", ",array_unique(explode(",",substr($job_no,0,-1))));
	$buyer_name=implode(",",array_unique(explode(",",substr($buyer_name,0,-1))));
	
	$color_id=array_unique(explode(",",$batchdata_array[0][csf('color_id')]));
	$color_name='';
	foreach($color_id as $color)
	{
		$color_name.=$color_arr[$color].",";
	}
	$color_name=substr($color_name,0,-1);
	//var_dump($recipe_color_arr);
?>
<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u><?php echo $data[2]; ?> Report</u></strong></td>
        </tr>
        <tr>
        	<td width="105"><strong>Req. ID </strong></td><td width="175px"><?php echo $dataArray[0][csf('requ_no')]; ?></td>
            <td width="115"><strong>Req. Date</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('requisition_date')]); ?></td>
            <td width="100"><strong>Buyer</strong></td><td><?php echo $buyer_name; ?></td>
       </tr>
        <tr>
            <td><strong>Order No</strong></td> <td><?php echo $po_no; ?></td>
            <td><strong>Job No</strong></td> <td><?php echo $job_no; ?></td>
            <td><strong>Issue Basis</strong></td> <td><?php echo $receive_basis_arr[$dataArray[0][csf('requisition_basis')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Batch No</strong></td><td><?php echo $batchdata_array[0][csf('batch_no')]; ?></td>
            <td><strong>Batch Weight</strong></td><td><?php echo $batchdata_array[0][csf('batch_weight')]; ?></td>
            <td><strong>Color</strong></td><td><?php echo $color_name; ?></td>
        </tr>
        <tr>
            <td><strong>Recipe No</strong></td><td><?php echo $data_array[0][csf("recipe_id")]; ?></td>
            <td><strong>Total Liq.(ltr)</strong></td><td><?php echo $data_array[0][csf("total_liquor")]; ?></td>
            <td>Liquor Ratio</td><td><?php echo $data_array[0][csf("ratio")]; ?></td>
        </tr>
        <tr>
            <td><strong>Machine No</strong></td>
            <td>
				<?php 
					$machine_data=sql_select("select machine_no, floor_id from lib_machine_name where id='".$dataArray[0][csf("machine_id")]."'");
                	echo $machine_data[0][csf('machine_no')]; 
                ?>
            </td>
            <td><strong>Floor Name</strong></td>
            <td colspan="3">
				<?php 
					$floor_name=return_field_value("floor_name","lib_prod_floor","id='".$machine_data[0][csf('floor_id')]."'");
                	echo $floor_name; 
                ?>
            </td>
        </tr>
    </table>
	<br>
    <table width="800" cellspacing="0" align="center" class="rpt_table" border="1" rules="all" >
        <thead bgcolor="#dddddd" align="center">
            <tr bgcolor="#CCCCFF">
                <th colspan="5" align="center"><strong>Fabrication</strong></th>
            </tr>
            <tr>   
                <th width="30">SL</th>
                <th width="100" >Dia/ W. Type</th>
                <th width="300" >Constrution & Composition</th>
                <th width="70" >Gsm</th>
                <th width="70" >Dia</th>
            </tr>
        </thead>
        <tbody>
		<?php
		$batch_id_qry=$dataArray[0][csf('batch_id')];
		//echo $batch_id_qry;die;
		$batch_query="Select id, item_description,width_dia_type from pro_batch_create_dtls where mst_id in($batch_id_qry) and status_active=1 and is_deleted=0";
		$result_batch_query=sql_select($batch_query);
		$j=1;
		foreach($result_batch_query as $rows)
		{
			if ($j%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
				$fabrication_full=$rows[csf("item_description")];
				$fabrication=explode(',',$fabrication_full);
				//print_r ($fabrication);
				?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $j; ?></td>
                <td align="center"><?php echo $fabric_typee[$rows[csf("width_dia_type")]]; ?></td>
                <td align="center"><?php echo $fabrication[0].", ".$fabrication[1]; ?></td>
                <td align="center"><?php echo $fabrication[2]; ?></td>
                <td align="center"><?php echo $fabrication[3]; ?></td>
            </tr>
			<?php
			$j++;
		}
		?>
        </tbody>
    </table>
	<div style="width:100%; margin-top:10px" >
    <table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
    	    <tr bgcolor="#CCCCFF">
                <th colspan="14" align="center"><strong>Dyes And Chemical Issue Requisition</strong></th>
            </tr>

	<?php
 	$group_arr=return_library_array( "select id,item_name from lib_item_group where item_category in (5,6,7) and status_active=1 and is_deleted=0",'id','item_name');
	
	$sql_dtls = "select a.requ_no, a.batch_id, a.recipe_id, b.id,
		b.sub_process, b.item_category, b.dose_base, b.ratio, b.recipe_qnty, b.adjust_percent, b.adjust_type, b.required_qnty, b.req_qny_edit,
	  	c.item_description, c.item_group_id, c.sub_group_name, c.item_size, c.unit_of_measure
	    from dyes_chem_issue_requ_mst a,  dyes_chem_issue_requ_dtls b, product_details_master c
	    where a.id=b.mst_id and b.product_id=c.id and b.item_category in (5,6,7) and b.req_qny_edit!=0 and c.item_category_id in (5,6,7) and a.id=$data[1] order by b.id, b.seq_no";
	 // echo $sql_dtls;//die;
	$sql_result= sql_select($sql_dtls);
	$group_arr=return_library_array( "select id, item_name from  lib_item_group", "id", "item_name"  );
	$sub_process_array=array();
	$sub_process_tot_rec_array=array();
	$sub_process_tot_req_array=array();
	
	foreach($sql_result as $row)
	{
		$sub_process_tot_rec_array[$row[csf("sub_process")]]+=$row[csf("recipe_qnty")];
		$sub_process_tot_req_array[$row[csf("sub_process")]]+=$row[csf("req_qny_edit")];
	}
	//var_dump($sub_process_tot_req_array);
	$i=1; $k=1; $recipe_qnty_sum=0; $req_qny_edit_sum=0; $recipe_qnty=0; $req_qny_edit=0;
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			if (!in_array($row[csf("sub_process")],$sub_process_array) )
			{
				$sub_process_array[]=$row[csf('sub_process')];
				if($k!=1)
				{
					?>
                    <tr>
                        <td colspan="7" align="right"><strong>Total :</strong></td>
                        <td align="right"><?php  echo number_format($recipe_qnty_sum,4,'.',''); ?></td>
                        <td>&nbsp;</td>
                		<td>&nbsp;</td>
                        <td align="right"><?php  echo number_format($req_qny_edit_sum,4,'.',''); ?></td>
                    </tr> 
			 <?php } 
			$recipe_qnty_sum=0;
			$req_qny_edit_sum=0;
			$k++; 
			?>
            <tr bgcolor="#CCCCCC">
                <th colspan="14"><strong><?php echo $dyeing_sub_process[$row[csf("sub_process")]]; ?></strong></th>
            </tr> 
            <tr>   
                <th width="30">SL</th>
                <th width="80" >Item Cat.</th>
                <th width="80" >Item Group</th>
                <!--<th width="100">Sub Group</th>-->
                <th width="150">Item Description</th>
                <th width="50" >UOM</th>
                <th width="110" >Dose Base</th> 
                <th width="40" >Ratio</th>
                <th width="60" >Recipe Qnty</th>
                <th width="50" >Adj%</th>
                <th width="60" >Adj Type</th> 
                <th width="80" >Iss. Qty.</th>
                <th width="60" >KG</th>
                <th width="60" >GM</th>
                <th width="60" >MG</th>
            </tr>
        </thead>
        <?php 
		}
		
		/*$iss_qnty=explode(".",$row[csf("req_qny_edit")]);
		$iss_qnty_gm=substr((string)$iss_qnty[1],0,3);
		$iss_qnty_mg=substr($iss_qnty[1],3);*/
		
		$iss_qnty=$row[csf("req_qny_edit")]*10000;
		$iss_qnty_kg=floor($iss_qnty/10000);
		$lkg=round($iss_qnty-$iss_qnty_kg*10000);
		$iss_qnty_gm=floor($lkg/10);
		$iss_qnty_mg=$lkg%10;
		
		?>
        <tbody>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <td><?php echo $item_category[$row[csf("item_category")]];  //echo $row[csf("sub_process")]; ?></td>
                <td><?php echo $group_arr[$row[csf("item_group_id")]]; ?></td>
                <!--<td><?phpecho $row[csf("sub_group_name")]; ?></td>-->
                <td><?php echo $row[csf("item_description")].' '.$row[csf("item_size")]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf("unit_of_measure")]]; ?></td>
                <td><?php echo $dose_base[$row[csf("dose_base")]]; ?></td>
                <td align="center"><?php echo number_format($row[csf("ratio")],4,'.',''); ?></td>
                <td align="right"><?php echo number_format($row[csf("recipe_qnty")],4,'.',''); ?></td>
                <td align="center"><?php echo $row[csf("adjust_percent")]; ?></td>
                <td><?php echo $increase_decrease[$row[csf("adjust_type")]]; ?></td>
                <td align="right"><?php echo number_format($row[csf("req_qny_edit")],4,'.',''); ?></td>
                <td align="right"><?php echo $iss_qnty_kg; ?></td>
                <td align="right"><?php echo $iss_qnty_gm; ?></td>
                <td align="right"><?php echo $iss_qnty_mg; ?></td>
			</tr>
        </tbody>
		<?php $i++;
        $recipe_qnty_sum +=$row[csf('recipe_qnty')];
        $req_qny_edit_sum +=$row[csf('req_qny_edit')];
         }
		foreach ($sub_process_tot_rec_array as $val_rec)
		{
			 $totval_rec=$val_rec;
		}
		foreach ($sub_process_tot_req_array as $val_req)
		{
			 $totval_req=$val_req;
		}
		?>
           <tr>
                <td colspan="7" align="right"><strong>Total :</strong></td>
                <td align="right"><?php echo number_format($totval_rec,4,'.',''); ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($totval_req,4,'.',''); ?></td>
            </tr>                           
      </table>
        <br>
		 <?php
            echo signature_table(15, $data[0], "900px");
         ?>
      </div>
    </div>         
	<?php
	exit();
}

?>