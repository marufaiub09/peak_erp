<?

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$break_down_id_lc=return_library_array( "select id,wo_po_break_down_id from com_export_lc_order_info", "id", "wo_po_break_down_id");
$break_down_id_sc=return_library_array( "select id,wo_po_break_down_id from com_sales_contract_order_info", "id", "wo_po_break_down_id");


if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	if($txt_date_from!="" && $txt_date_to!="")
	{
		$str_cond="and a.ex_factory_date between '$txt_date_from' and  '$txt_date_to' ";
	}
	else
	{
		$str_cond="";
	}
	

	
		
		//echo $cbo_company_name;die;
	?>
<div style="width:2070px;">
    <div style="width:1120px" >
        <table width="1090"  cellspacing="0"  align="center">
            <tr>
                <td align="center" colspan="10" class="form_caption">
                    <strong style="font-size:16px;">Company Name:<? echo  $company_library[$cbo_company_name] ;?></strong>
                </td>
            </tr>
            <tr class="form_caption">
            	<td colspan="10" align="center" class="form_caption"> <strong style="font-size:15px;">Ex-Factory Report</strong></td>
            </tr>
             <tr align="center">
                <td colspan="10" align="center" class="form_caption"> <strong style="font-size:15px;">Total Summary</strong></td>
            </tr>
            </table>
            
            
            <table width="1090" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" id="">
            <thead>
                <th width="40" height="34">SL</th>
                <th width="130">Buyer Name</th>
                <th width="100">PO Qty.</th>
                <th width="130">PO Value</th>
                <th width="100">PO Value(%)</th>
                <th width="100">Current Ex-Fact. Qty.</th>
                <th width="130">Current Ex-Fact. Value</th>
                <th width="100">Total Ex-Fact. Qty.</th>
                <th width="130">Total Ex-Fact. Value </th>
                <th >Total Ex-Fact. Value %</th>
            </thead>
         </table>
        <table width="1090" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" id="table_header_1">
        <?
		$sql="select distinct(c.buyer_name) as buyer_name 
from pro_ex_factory_mst a,wo_po_break_down b,wo_po_details_master c 
where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' and a.is_deleted=0 and a.status_active=1 $str_cond  order by c.buyer_name asc ";
//echo $sql;die;
		$sql_result=sql_select($sql);
		$i=1;
		foreach($sql_result as $row)
		{
			if ($i%2==0)  
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			$buyer_id=$row[csf("buyer_name")];
					
			
		?>     
            <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>" >
                <td width="40" align="center"><? echo $i; ?></td>
                <td width="130">
                <p><?
                echo $buyer_arr[$buyer_id];
                ?></p>
                </td>
                <?
					
					$master_sql="select sum(a.ex_factory_qnty) as ex_factory_qnty,c.buyer_name,b.po_number,b.po_quantity,b.unit_price
								from pro_ex_factory_mst a,wo_po_break_down b,wo_po_details_master c 
								where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' and c.buyer_name='$buyer_id' $str_cond  and a.is_deleted=0 and a.status_active=1 group by b.po_number order by a.ex_factory_date";
					
					//echo $master_sql;echo "<br>";
					$sql_result=sql_select($master_sql);
					foreach($sql_result as $row)
					{			
					$buyer_po_quantity+=$row[csf("po_quantity")];
					$buyer_po_value+=$row[csf("po_quantity")]*$row[csf("unit_price")];
					$current_ex_Fact_Qty+=$row[csf("ex_factory_qnty")];
					$current_ex_fact_value+=$current_ex_Fact_Qty*$row[csf("unit_price")];
					}
					$master_sql2="select sum(a.ex_factory_qnty) as to_ex_factory_qnty,b.unit_price
								from pro_ex_factory_mst a,wo_po_break_down b,wo_po_details_master c 
								where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' and c.buyer_name='$buyer_id'  and a.is_deleted=0 and a.status_active=1 group by b.po_number  order by a.ex_factory_date";
					$sql_result=sql_select($master_sql2);
					foreach($sql_result as $row)
					{
						$total_ex_fact_qty+=$row[csf("to_ex_factory_qnty")];
						$total_ex_fact_value=$total_ex_fact_qty*$row[csf("unit_price")];
						
					}
				
				?>
                <td width="100" align="right"><p><? echo number_format($buyer_po_quantity,0); $total_buyer_po_quantity+=$buyer_po_quantity; ?></p></td>
                <td width="130" align="right" ><p  id="value_<? echo $i ; ?>"><? echo number_format($buyer_po_value,2 ,'.', '');  $total_buyer_po_value+=$buyer_po_value; ?></p></td>
                <td width="100" align="right" id="pacent_<? echo $i; ?>">
                 
                </td>
                <td width="100" align="right">
                <p><?
				echo  $current_ex_Fact_Qty; $total_current_ex_Fact_Qty +=$current_ex_Fact_Qty;
				?></p>
                </td>
                <td width="130" align="right">
                <p><?
				echo number_format($current_ex_fact_value,2); $total_current_ex_fact_value+=$current_ex_fact_value;
				?></p>
                </td>
                <td align="right" width="100">
                <p><?
				echo $total_ex_fact_qty; $mt_total_ex_fact_qty+=$total_ex_fact_qty;
				?></p>
                </td>
                <td align="right" width="130">
                <p><?
				echo  number_format($total_ex_fact_value,2); $mt_total_ex_fact_value+=$total_ex_fact_value;
				?></p>
                </td>
                <td align="right">
                <p><?
				$total_ex_fact_value_parcentage=($total_ex_fact_value/$buyer_po_value)*100;
				echo number_format($total_ex_fact_value_parcentage,0)
				?> %</p>
                </td>
            </tr>
            <?
			$i++;
			$buyer_po_quantity=0;
			$buyer_po_value=0;
			$current_ex_Fact_Qty=0;
			$current_ex_fact_value=0;
			$total_ex_fact_qty=0;
			$total_ex_fact_value=0;
			
		}
			?>
            <input type="hidden" name="total_i" id="total_i" value="<? echo $i; ?>" />
            <tfoot>
                <th align="right" colspan="2"><b>Total:</b></th>
                <th  align="right"><p><strong style="font-size:12;"><? echo number_format($total_buyer_po_quantity,0);  ?></strong></p></th>
                <th  align="right"><p><strong style="font-size:12;" id="total"><? echo number_format($total_buyer_po_value,2 ,'.', ''); ?></strong></p> </th>
                <th align="right" id="total_i_value"></th>
                <th  align="right"><p><strong style="font-size:12;"><? echo number_format($total_current_ex_Fact_Qty,0); ?></strong></p></th>
                <th  align="right"><p><strong style="font-size:12;"><? echo  number_format($total_current_ex_fact_value,2); ?></strong></p></th>
                <th align="right"><p><strong style="font-size:12;"><? echo number_format($mt_total_ex_fact_qty,0); ?></strong></p></th>
                <th align="right"><p><strong style="font-size:12;"><? echo number_format($mt_total_ex_fact_value,2); ?></strong></p></th>
                <th align="right"></th>
            </tfoot>
        </table>
    </div>
    <br />
    <div style="width:2070px;">
        <table width="2050"  >
           <tr>
                <td colspan="19" class="form_caption"><strong style="font-size:16px;">Shipped Out Order Details Report</strong></td>
           </tr>
        </table>
                    <table width="2032" border="1" class="rpt_table" rules="all" id="">
                        <thead>
                            <th width="40">SL</th>
                            <th width="130">Order NO</th>
                            <th width="100" >LC/SC NO</th>
                            <th width="100">Buyer Name</th>
                            <th width="100">Style</th>
                            <th width="110">Item Name</th>
                            <th width="120">Shipment Date</th>
                            <th width="120">Last Ex-Fac. Date</th>
                            <th width="100">Days in Hand</th>
                            <th width="100">PO Qtny.</th>
                            <th width="100">Unit Price</th>
                            <th width="130">PO Value</th>
                            <th width="100">Current Ex-Fact. Qty</th>
                            <th width="130">Current Ex-Fact. Value</th>
                            <th width="100">Total Ex-Fact. Qty.</th>
                            <th width="130">Total Ex-Fact. Value</th>
                            <th width="100">Excess/ Shortage Qty</th>
                            <th width="130">Excess/ Shortage Value</th>
                            <th >Total Ex-Fact. Qty. %</th>
                      	</thead>
                    </table>
                    <div style="width:2050px; overflow-y:scroll; overflow-x:hidden; max-height:300px;"  id="scroll_body" >
                        <table width="2032" cellspacing="0" cellpadding="0"  border="1"  class="rpt_table" rules="all" id="table_body"> 
                              
                            <?
                                $i=1;
								$date=date("Y-m-d");
								$sql= "select c.id,a.po_break_down_id,a.item_number_id,max(a.ex_factory_date) as ex_factory_date,sum(a.ex_factory_qnty) as ex_factory_qnty,a.lc_sc_no,b.shipment_date,c.buyer_name,c.style_description,b.po_number,b.po_quantity,b.unit_price
								from pro_ex_factory_mst a,wo_po_break_down b,wo_po_details_master c 
								where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' $str_cond  and a.is_deleted=0 and a.status_active=1 group by b.po_number order by a.ex_factory_date ASC";
								echo $sql;die;
								$sql_result=sql_select($sql);
								
								//print_r($sql_result);die;
								
                                foreach($sql_result as $row)
                                {
                                if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                                else
                                $bgcolor="#FFFFFF";	
								
								
								?>
                            
                            <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                                <td width="40" align="center"><? echo "$i"; ?></td>
                                <td width="130" align="center">
                                    <p><?  echo $row[csf("po_number")]; ?></p>
                                </td>
                                <td width="100" align="center">
                                   <p> <?
								   $po_break_id=str_replace("'","",$row[csf("lc_sc_no")]);
									  if($break_down_id_lc[$po_break_id])
									  {
										 echo $lc_sc=return_field_value("b.export_lc_no as export_lc_no","com_export_lc_order_info a, com_export_lc b","a.com_export_lc_id=b.id and a.wo_po_break_down_id='$po_break_id'","export_lc_no"); 
									  }
									  else
									  {
										echo $lc_sc=return_field_value("b.contract_no as export_lc_no","com_sales_contract_order_info a, com_sales_contract b","a.com_sales_contract_id=b.id and a.wo_po_break_down_id='$po_break_id'","export_lc_no");  
									  }
									 
									  ?></p>
                                </td>
                                <td width="100" align="center" >
                                <p> <?
								  echo $buyer_arr[$row[csf("buyer_name")]];
 
								  ?></p>
                                </td>
                                <td width="100" title="<? echo $item_name;?>">
                                <p> <?  echo $row[csf("style_description")]; ?></p>
                                </td>
                                <td width="110" align="center">
                                <p> <?
								  echo $garments_item[$row[csf("item_number_id")]]; 
								  ?></p>
                                </td>
                                <td width="120" align="center">
                                 <p><?  echo $row[csf("shipment_date")]; ?></p>
                                </td>
                                <td width="120" align="center">
                                <p> <?  echo $row[csf("ex_factory_date")]; ?></p>
                                </td>
                                <td width="100" align="center">
                                <p> <?
								 $current_date=date("Y-m-d");
								 $shipment_date =$row[csf("shipment_date")];
								 $diff=datediff("d",$current_date, $shipment_date);  echo "$diff". " days";
								 ?></p>
                                </td>
                                <td width="100" align="right">
                                 <p><?   $po_quantity=$row[csf("po_quantity")]; echo number_format($po_quantity,0); $total_order_qnty +=$po_quantity; ?></p>
                                </td>
                                <td width="100" align="right">
                                 <p><?   $unit_price=$row[csf("unit_price")]; echo number_format($unit_price,2);  ?></p>
                                </td>
                                <td width="130" align="right">
                                 <p><?   $value=$po_quantity*$unit_price; echo number_format($value,2); $total_order_value +=$value; ?></p>
                                </td>
                                <td width="100" align="right">
                                <p> <?
                                  $current_ex_Fact_Qty=$row[csf("ex_factory_qnty")];
								  echo number_format($current_ex_Fact_Qty,0);
								   $total_dt_current_ex_Fact_Qty+=$current_ex_Fact_Qty;
								 
								 ?></p>
                                </td>
                                <td width="130" align="right">
                                <p> <? 
								 $current_ex_fact_value=$current_ex_Fact_Qty*$unit_price; echo number_format($current_ex_fact_value,2); $total_current_ex_Fact_value +=$current_ex_fact_value;
								 ?></p>
                                </td>
                               <td width="100" align="right">
                                <p><?
								 $po_number=$row[csf("po_number")];
								 $total_ex_fact_qty=return_field_value("sum(a.ex_factory_qnty) as po_quantity","pro_ex_factory_mst a, wo_po_break_down b,wo_po_details_master c","a.po_break_down_id=b.id and b.job_no_mst=c.job_no and b.po_number='$po_number' and c.company_name='$cbo_company_name' and a.is_deleted=0 and a.status_active=1","po_quantity");
								 echo  number_format($total_ex_fact_qty,0);
								  $gt_total_ex_fact_qty +=$total_ex_fact_qty;
								?></p>
                               </td>
                                <td width="130" align="right">
                                <p><?
								$total_ex_fact_value=$total_ex_fact_qty*$unit_price; echo number_format($total_ex_fact_value,2);
								 $gt_total_ex_fact_value +=$total_ex_fact_value; 
								?></p>
                               </td>
                                <td width="100" align="right">
                                <p><?
								$excess_shortage_qty=$po_quantity-$total_ex_fact_qty; echo number_format($excess_shortage_qty,0);
								$total_excess_shortage_qty +=$excess_shortage_qty;
								?></p>
                               </td>
                                <td width="130" align="right">
                               <p> <?
								$excess_shortage_value=$excess_shortage_qty*$unit_price; echo number_format($excess_shortage_value,2);
								$totao_excess_shortage_value +=$excess_shortage_value;
								?></p>
                               </td>
                                <td align="center">
                                <p> <?
								  $total_ex_fact_qty_parcent=($total_ex_fact_qty/$po_quantity)*100;
								  echo number_format($total_ex_fact_qty_parcent,0);
								 ?> %</p>   
                               </td>
                    
                            </tr>
                            <?
                            $i++;
                            } 
                            ?>
                           
               
                    <tfoot>
                            <tr>
                                <th colspan="9" align="right"><strong>Total</strong></th>
                                <th align="right" ><p><strong style="font-size:12px;"><? echo number_format($total_order_qnty,0); ?></strong></p></th>
                                <th align="right">&nbsp;</th>
                                <th align="right"><P><strong style="font-size:12px;"><? echo number_format($total_order_value,2); ?></strong></P></th>
                                <th align="right" ><p><strong style="font-size:12px;"><? echo number_format($total_current_ex_Fact_Qty,0); ?></strong></p></th>
                                <th align="right" ><p><strong style="font-size:12px;"><? echo number_format($total_current_ex_Fact_value,2); ?></strong></p></th>
                                <th align="right"><p><strong style="font-size:12px;"><? echo number_format($gt_total_ex_fact_qty,0); ?></strong></p></th>
                                <th align="right" ><p><strong style="font-size:12px;"><? echo number_format($gt_total_ex_fact_value,2); ?></strong></p></th>
                                <th align="right" ><p><strong style="font-size:12px;"><? echo number_format($total_excess_shortage_qty,0); ?></strong></p></th>
                                <th align="right"> <P><strong style="font-size:12px;"><? echo number_format($totao_excess_shortage_value,2); ?></strong></P></th>
                                <th >&nbsp;</th>
                            </tr>
                            </tfoot>
                        </table>
                        </div>
                    </div>
</div>
<script>
var len=(document.getElementById('total_i').value)-1;
var total_val=parseInt(document.getElementById('total').innerHTML);
var tot=0;
var row_tot=0;
for (j=1;j<=len;j++)
{
	row_tot=((parseInt(document.getElementById('value_'+j).innerHTML)*100)/total_val).toFixed(2);
	document.getElementById('pacent_'+j).innerHTML=row_tot;
	tot=(tot*1)+(row_tot*1);

}
document.getElementById('total_i_value').innerHTML=Math.ceil(tot);
</script>    
	<?
	// end if($type=="sewing_production_summary")


 /*$sql= "select a.id,a.po_break_down_id,a.item_number_id,a.country_id,a.location,a.ex_factory_date,a.ex_factory_qnty,a.total_carton_qnty,a.lc_sc_no,b.shipment_date,c.style_ref_no,c.order_uom,b.unit_price,b.set_pc_rate 
								from pro_ex_factory_info a,wo_po_break_down b,wo_po_details_master c 
								where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_id like '$company_name' and a.item_number_id in(select id from lib_garments_item where gmt_item_name like '$item_name' and is_deleted=0 and status_active=1) and a.po_number like '$order_number' and a.buyer_id in(select id from lib_buyer where buyer_name like '$buyer_name_search' and is_deleted=0 and status_active=1) and c.style_ref_no like '$style_name' and b.shipment_date like '$ship_date_search' and a.order_quantity like '$po_qty' $str_cond  and a.is_deleted=0 and a.status_active=1 order by a.ex_factory_date ASC";
                                $sql= "select a.id,a.po_break_down_id,a.item_number_id,a.country_id,a.location,a.ex_factory_date,a.ex_factory_qnty,a.total_carton_qnty,a.lc_sc_no,c.style_ref_no,c.order_uom,b.unit_price,b.set_pc_rate,b.shipment_date
								from pro_ex_factory_info a,wo_po_break_down b,wo_po_details_master c 
								where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_id like '$cbo_company_name' $str_cond  and a.is_deleted=0 and a.status_active=1 order by a.ex_factory_date ASC";*/


print_r($buyer_group);								

	exit();
}
disconnect($con);
?>
