<?

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id=$_SESSION['logic_erp']['user_id'];
//--------------------------------------------------------------------------------------------------------------------
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$job_smv_arr=return_library_array( "select job_no, set_smv from wo_po_details_master",'job_no','set_smv');
$basic_smv_arr=return_library_array( "select comapny_id, basic_smv from lib_capacity_calc_mst",'comapny_id','basic_smv');
$asking_avg_rate_arr=return_library_array( "select company_id, asking_avg_rate from lib_standard_cm_entry",'company_id','asking_avg_rate');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	//echo $cbo_year_from;die;

	$company_name=str_replace("'","",$cbo_company_name);
	
	$date_cond='';
	if(str_replace("'","",$cbo_year_from)!=0 && str_replace("'","",$cbo_month_from)!=0)
	{
		$start_year=str_replace("'","",$cbo_year_from);
		$start_month=str_replace("'","",$cbo_month_from);
		$start_date=$start_year."-".$start_month."-01";
		
		$end_year=str_replace("'","",$cbo_year_to);
		$end_month=str_replace("'","",$cbo_month_to);
		$end_date=$end_year."-".$end_month."-31";
		$date_cond=" and b.pub_shipment_date between '$start_date' and '$end_date'";
	}
	$data_array="select
					 a.job_no_prefix_num, 
					 a.job_no, 
					 a.company_name, 
					 a.buyer_name,
					 a.set_smv,
					 a.style_ref_no,
					 a.job_quantity, 
					 a.total_set_qnty, 
					 b.id, 
					 b.is_confirmed, 
					 b.po_number, 
					 b.po_quantity as po_quantity, 
					 (b.po_quantity*a.total_set_qnty) as po_quantity_pcs, 
					 b.pub_shipment_date, 
					 b.po_received_date, 
					 DATEDIFF(b.pub_shipment_date, '$date') date_diff_1, 
					 b.unit_price, 
					 b.po_total_price, 
					 b.t_year, 
					 b.t_month,
					 month(b.pub_shipment_date) as month_name  
			from 
					wo_po_details_master a, 
					wo_po_break_down b 
			where  
					a.job_no=b.job_no_mst   
					and a.company_name like '$company_name' 
					$date_cond  
					and a.status_active=1 
					and b.status_active=1 
			group by 
					b.id 
			order by 
					b.pub_shipment_date desc,a.job_no_prefix_num,b.id";
		
		echo $data_array;die;
		$check_month_fst=array();
		$result_po=sql_select($data_array);
		foreach($result_po as $row)
		{
			$po_total_price+= $row[csf("po_total_price")];
			$quantity_tot+=$row[csf("po_quantity_pcs")];
			$company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][month_name]=$row[csf("month_name")];
			$company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][order_qty_pcs]+=$row[csf("po_quantity_pcs")];
			$company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][order_value]+=$row[csf("po_total_price")];
			$company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][smv]+=($job_smv_arr[$row[csf("job_no")]])*$row[csf('po_quantity')];
			if($job_smv_arr[$row[csf("job_no")]] !=0)
			{
				$booked_basic_qnty=($row[csf("po_quantity")]*($job_smv_arr[$row[csf("job_no")]]))/$basic_smv_arr[$row[csf("company_name")]];
				$company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][booked_basic_qnty]+=$booked_basic_qnty;
				$booked_basic_qnty_tot+=$booked_basic_qnty;
			}
		}
		var_dump($company_buyer_array);die;
		ob_start();
?>
	<div style="width:1100px;">
    <fieldset style="width:1100px;">
        <table width="1050">
            <tr class="form_caption">
                <td colspan="10" align="center" style="font-size:16px;">SMV vs Cost Analysis Report</td>
            </tr>
            <tr class="form_caption">
                <td colspan="10" align="center" style="font-size:16px;"><? echo $company_library[$company_name]; ?></td>
            </tr>
        </table>
        <table id="" class="rpt_table" width="1050" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
            	<tr>
                	<th width="50">SL</th>
                    <th width="100">Company Name</th>
                    <th width="100">Buyer Name </th>
                    <th width="100">Avg.SMV</th>
                    <th width="100">Order Qty(pce)</th>
                    <th width="100">Order Value(USD)</th>
                    <th width="100">Avg.Rate(USD)</th>
                    <th width="100"> Buyer Share %</th>
                    <th width="100">Avg. Basic Rate(USD) </th>
                    <th width="100">Stnd. Rate(USD) </th>
                    <th >Stnd. Devn(USD)</th>
                </tr>
            </thead>
        </table>
        <div style=" max-height:400px; overflow-y:scroll; width:1100px"  align="left" id="scroll_body">
        <table id="table_body" class="rpt_table" width="1050" cellpadding="0" cellspacing="0" border="1" rules="all">
        	<tbody>
            <?
			$result=sql_select($data_array);
			$i=0;$check_month=array();$k=1;
			foreach($company_buyer_array as $key_com=>$value)
			{
				foreach($value as $buyer_key=>$row)
				{
					if(!in_array($row[month_name],$check_month))
					{
						if($k!=1)
						{
						?>
						<tr>
							<td ><? //echo $i; ?></td>
							<td ><? //echo $i; ?></td>
							<td ><? //echo $i; ?></td>
							<td align="right" style="font-weight:bold;"><? $smv_sub_to=($to_smv/$i); echo number_format($smv_sub_to,2); ?></td>
							<td align="right" style="font-weight:bold;"><? echo $total_po_qty; ?></td>
							<td align="right" style="font-weight:bold;"><? echo $total_po_val; ?></td>
							<td align="right" style="font-weight:bold;"><? echo number_format(($total_avg_rate/$i),2); ?></td>
							<td align="right" style="font-weight:bold;"><? echo number_format(($total_buyer_percent/$i),2); ?></td>
							<td align="right" style="font-weight:bold;"><? echo number_format(($total_avg_basic/$i),2);?></td>
							<td align="right" style="font-weight:bold;"><? //echo $i; ?></td>
							<td align="right" style="font-weight:bold;"><? echo number_format(($total_stnd_div/$i),2); ?></td>
						</tr>
						<?
						}
						?>
						<tr>
							<td colspan="11" align="center" style="font-weight:bold; font-size:16px;" ><? echo $months[$row[csf("month_name")]]; ?></td>
						</tr>
						<?
						$check_month[]=$row[month_name];
						$k++;
						$to_smv=="";$total_po_qty="";$total_po_val="";$total_avg_rate="";$total_buyer_percent="";$total_avg_basic="";$total_stnd_div="";
					}
					$i++;
					?>
					<tr>
						<td width="50" align="center"><? echo $i; ?></td>
						<td width="100"><? echo  $company_library[$key_com]; ?></td>
						<td width="100"><? echo $buyer_library[$buyer_key]; ?></td>
						<td width="100" align="right"><?  $smv=(($row[smv])/$row[order_value]); echo number_format($smv,2);  $to_smv+=$smv;?></td>
						<td width="100" align="right"><? echo number_format($row[order_qty_pcs],2); $total_po_qty+=$row[order_qty_pcs]; ?></td>
						<td width="100" align="right"><? echo number_format($row[order_value],2); $total_po_val+=$row[order_value]; ?></td>
						<td width="100" align="right"><? $avg_val=$row[order_value]/$row[order_qty_pcs]; echo number_format($avg_val,2); $total_avg_rate+=$avg_val; ?></td>
						<td width="100" align="right"><? $buyer_share=(($row[order_value]/$po_total_price)*100); echo number_format($buyer_share,2)."%"; $total_buyer_percent+=$buyer_share; ?></td>
						<td width="100"  align="right">
						 <?
						$avg_basic_rate=$row[order_value]/$row[booked_basic_qnty]; echo number_format($avg_basic_rate,2); $total_avg_basic+=$avg_basic_rate;
						?>
						</td>
						<td width="100">
					   
						</td>
						<td  align="right">
						<? 
						echo number_format((($row[order_value]/$row[booked_basic_qnty])-$asking_avg_rate_arr[$key_com]),2); 
						$total_stnd_div += (($row[order_value]/$row[booked_basic_qnty])-$asking_avg_rate_arr[$key_com]);
						?>
						</td>
					</tr>
					<?
				}
			}
			?>
                <tr>
                    <td ><? //echo $i; ?></td>
                    <td ><? //echo $i; ?></td>
                    <td ><? //echo $i; ?></td>
                    <td align="right" style="font-weight:bold;"><? $smv_sub_to=($to_smv/$i); echo number_format($smv_sub_to,2); ?></td>
                    <td align="right" style="font-weight:bold;"><? echo $total_po_qty; ?></td>
                    <td align="right" style="font-weight:bold;"><? echo $total_po_val; ?></td>
                    <td align="right" style="font-weight:bold;"><? echo number_format(($total_avg_rate/$i),2); ?></td>
                    <td align="right" style="font-weight:bold;"><? echo number_format(($total_buyer_percent/$i),2); ?></td>
                    <td align="right" style="font-weight:bold;"><? echo number_format(($total_avg_basic/$i),2);?></td>
                    <td align="right" style="font-weight:bold;"><? //echo $i; ?></td>
                    <td align="right" style="font-weight:bold;"><? echo number_format(($total_stnd_div/$i),2); ?></td>
                </tr>
            </tbody>
        </table>
        </div>
    </fieldset>
    </div>
<?
	 
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data****$filename";
	exit();
}

disconnect($con);
?>