<?php
/*-------------------------------------------- Comments
Version                  :   V1
Purpose			         : 	This form will create  MOnthly Capacity and order qty Report
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 	Maruf		
Update date		         : 	08-12-2015   
QC Performed BY	         :		
QC Date			         :	
Comments		         : From this version oracle conversion is start
*/
error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Info","../../../", 1, 1, $unicode);
?>	

<script>
var permission='<?php echo $permission; ?>';

 function generate_report_main()
 {
	 //var report=return_global_ajax_value($('#cbo_company_name').val()+'*'+$('#cbo_year_name').val()+'*'+$('#update_id').val()+'*'+$('#cbo_month').val()+'*'+$('#cbo_location_id').val(), 'capacity_allocation_print', '', 'requires/pre_cost_entry_controller');
	 print_report( $('#cbo_company_name').val()+'*'+$('#cbo_year_name').val()+'*'+$('#cbo_month').val()+'*'+$('#cbo_month_end').val(), "capacity_allocation_print", "requires/monthly_capacity_and_order_qnty_controller" ) 
	 return; 
 }
 
 
 function fn_report_generated()
	{
		if(form_validation('cbo_company_name*cbo_year_name*cbo_month*cbo_month_end','Company Name*Year*Start Month*End Month')==false)
		{
			return;
		}
		else
		{	
			var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_year_name*cbo_month*cbo_month_end',"../../../");
			freeze_window(3);
			http.open("POST","requires/monthly_capacity_and_order_qnty_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
		
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			var tot_rows=reponse[2];
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../../'); 
			append_report_checkbox('table_header_1',1);
			
			//setFilterGrid("table_body",-1,tableFilters);
			//show_graph( '', document.getElementById('graph_data').value, "pie", "chartdiv", "", "../../../", '',400,700 );
	 		show_msg('3');
			release_freezing();
		}
	}
	
</script>
</head>
<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
		<?php echo load_freeze_divs ("../../../");  ?>
        <h3 align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
        <div id="content_search_panel" > 
            <form>
                <div style="width:850px">
                    <fieldset>  
                    <legend>Monthly Capacity Allocation</legend>
                        <table cellpadding="0" cellspacing="2" width="500px" class="tbl_capacity_allocation">
                            <tr>
                                <td>Company</td>
                                <td>
                                <?php 
                                echo create_drop_down( "cbo_company_name", 150, "select id,company_name from lib_company where status_active=1 and is_deleted=0 order by company_name","id,company_name", 1, "-- Select Company --", $selected,"" );
                                ?>
                                </td>
                                
                                <td>Year</td>
                                <td>
                                <?php 
                                echo create_drop_down( "cbo_year_name", 150,$year,"id,year", 1, "-- Select Year --", $selected,"" );
                                ?>
                                </td>
                                <td>Start Month</td>
                                <td>
                                <?php
                                echo create_drop_down( "cbo_month", 160,$months,"", 1, "-- Select --", "","" );
                                ?>
                                </td>
                                <td>End Month</td>
                                <td>
                                <?php
                                echo create_drop_down( "cbo_month_end", 160,$months,"", 1, "-- Select --", "","" );
                                ?>
                                </td>
                                <td>
                                <input type="button" name="search" id="search" value="Show" onClick="fn_report_generated()" style="width:80px" class="formbutton" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </form>
        </div>
        <div id="report_container" align="center"></div>
        <div id="report_container2"> 
        </div>
    </div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>