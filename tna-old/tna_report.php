<?
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
 
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("TNA Process","../", 1, 1, $unicode,'','');

?>
 
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
		
var permission='<? echo $permission; ?>';



var tableFilters = 
 {
	col_33: "none",
	col_operation: {
	id: ["total_order_qnty","total_order_qnty_in_pcs","value_tot_cm_cost","value_tot_cost","value_order","value_margin","value_tot_trims_cost","value_tot_embell_cost"],
	col: [9,11,25,26,29,30,31,32],
	operation: ["sum","sum","sum","sum","sum","sum","sum","sum"],
	write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
	}
 }

function fnc_generate_report_main( operation )
{
	if ( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	freeze_window(operation);
	var data="action=generate_tna_report&"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*cbo_team_member*txt_date_from*txt_date_to*txt_job_no*txt_order_no*txt_style_ref_no',"../"); 
	http.open("POST","requires/tna_report_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_generate_report_main_reponse;
}

function fnc_generate_report_main_reponse()
{
	if(http.readyState == 4) 
	{
		var reponse=trim(http.responseText).split('****');
		//document.getElementById('report_container').innerHTML  = reponse[1];
		$("#report_container").html(reponse[0]);  
		document.getElementById('print_button').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
		
		setFilterGrid("table_body",-1,tableFilters);
		
		release_freezing();
	}
}

function new_window()
{
	document.getElementById('scroll_body').style.overflow="auto";
	document.getElementById('scroll_body').style.maxHeight="none";
	
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><title></title><link rel="stylesheet" href="../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	d.close(); 

	document.getElementById('scroll_body').style.overflowY="scroll";
	document.getElementById('scroll_body').style.maxHeight="400px";
}
function update_tna_process(type,id,po_id)
{ 

	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/tna_report_controller.php?type='+type+'&mid='+id+'&po_id='+po_id+'&action=edit_update_tna'+'&permission='+permission, "TNA Update", 'width=640px,height=240px,center=1,resize=1,scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var theemail=this.contentDoc.getElementById("selected_booking");
		if (theemail.value!="")
		{
			freeze_window(5);
			
			release_freezing();
		}
	}
}

function progress_comment_popup(job_no,po_id,template_id,tna_process_type)
{
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/tna_report_controller.php?job_no='+job_no+'&po_id='+po_id+'&template_id='+template_id+'&tna_process_type='+tna_process_type+'&action=update_tna_progress_comment'+'&permission='+permission, "TNA Progress Comment", 'width=1040px,height=460px,center=1,resize=1,scrolling=0','')
	
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		
		var theemail=this.contentDoc.getElementById("selected_booking");
		
		if (theemail.value!="")
		{
			freeze_window(5);
			
			release_freezing();
		}
	}
}


</script>
<body  onLoad="set_hotkey()">
<div align="center"> 
    <? echo load_freeze_divs ("../");  ?>
	<fieldset style="width:950px; text-align:left">
    	<table width="950" class="rpt_table">
        	<tr>
                <td width="120" class="must_entry_caption" align="center"> Company Name</td>
                <td width="120" align="center">Buyer Name</td>
                 <td width="120" align="center">Merchant</td>
                <td width="100" align="center">Ship From Date</td>
                <td width="100" align="center">Ship To Date</td>
                <td width="120" align="center">Job No</td>
                <td width="120" align="center">Order No</td>
                <td width="120" align="center">Style Ref. No</td>
                <!--<td><input type="text" name="txt_job_no" id="txt_job_no" autocomplete="off" class="text_boxes" onDblClick="openmypage('Order Info',1); return false"  style="width:100px" readonly value=""/></td>-->
                
                 
           </tr>
           <tr>
                <td>
                	<?
					   echo create_drop_down( "cbo_company_name", 130, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, " load_drop_down( 'requires/tna_report_controller', this.value, 'load_drop_down_buyer', 'buyer_td' ); " );
						?> 
                </td>
                <td id="buyer_td">
						 <? 
                            echo create_drop_down( "cbo_buyer_name", 130, $blank_array,"", 1, "-- Select Buyer --", $selected, "" );
                         ?>	
                 </td>
                <td id="team_td">
                                    
					<? 
						echo create_drop_down( "cbo_team_member", 130, "select id,team_member_name from  lib_mkt_team_member_info where status_active =1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-- Select Team Member --", $selected, "" );
                        //echo create_drop_down( "cbo_team_member", 172, $blank_array,"", 1, "- Select Team Member- ", $selected, "" );
                     ?>	
                                    
                </td>
                <td><input type="text" name="txt_date_from" id="txt_date_from"  class="datepicker" style="width:78px"  value=""/></td>
                <td><input type="text" name="txt_date_to" id="txt_date_to" class="datepicker"  style="width:78px"  value=""/></td>
                <td><input type="text" name="txt_job_no" id="txt_job_no" autocomplete="off" class="text_boxes" style="width:100px" /></td>
                <td><input type="text" name="txt_order_no" id="txt_order_no" autocomplete="off" class="text_boxes" style="width:100px" ></td>
                <td><input type="text" name="txt_style_ref_no" id="txt_style_ref_no" autocomplete="off" class="text_boxes" style="width:100px" /></td>
                <!--<td><input type="text" name="txt_job_no" id="txt_job_no" autocomplete="off" class="text_boxes" onDblClick="openmypage('Order Info',1); return false"  style="width:100px" readonly value=""/></td>-->
                
                 
           </tr>
           
           <tr>
            	<td colspan="12" align="center" height="40" valign="middle">
                    <? echo load_month_buttons(1); ?>
                </td>
            </tr>
           <tr>
           		<td colspan="12" height="40" valign="middle" align="center">
                	<input type="button" class="formbutton" style="width:200px" value="Generate Report" onClick="fnc_generate_report_main(3)" />
                </td>
           </tr>
           
        </table>
        <div id="print_button"></div>
        <div style="margin-top:5px" id="report_container"></div>
    </fieldset>
	 
</div>
    
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>
 
 
 

 
 

