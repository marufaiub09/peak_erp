<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Yarn Issue Return Entry
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	07-05-2013
Updated by 		: 	Kausar	(Creating Report)	
Update date		: 	13-01-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Issue Return Info","../", 1, 1, $unicode,1,1); 
/*$con = connect();
$allocated_qnty_arr = return_library_array( "select item_id, sum(qnty) as qnty from inv_material_allocation_dtls where status_active=1 and is_deleted=0 group by item_id",'item_id','qnty');

$receive_purpose_arr = return_library_array( "select id, receive_purpose from inv_receive_master",'id','receive_purpose');
$issue_purpose_arr = return_library_array( "select id, issue_purpose from inv_issue_master",'id','issue_purpose');

$recvDataArr=array();
$recvData=sql_select("select a.id, b.prod_id, sum(b.cons_quantity) as recvqnty from inv_receive_master a, inv_transaction b where a.id=b.mst_id and b.transaction_type=1 and a.entry_form=1 and a.item_category=1 and b.item_category=1 and b.status_active=1 and b.is_deleted=0 group by a.id, b.prod_id");
foreach($recvData as $row)
{
	$recvDataArr[$row[csf('id')]][$row[csf('prod_id')]]=$row[csf('recvqnty')];
	$prod_recvid_arr[$row[csf('prod_id')]].=$row[csf('id')].",";
}

$issRtnDataArr=array();
$issRtnData=sql_select("select b.prod_id, sum(b.cons_quantity) as issrtnqnty from inv_receive_master a, inv_transaction b where a.id=b.mst_id and b.transaction_type=4 and a.entry_form=9 and a.item_category=1 and b.item_category=1 and b.status_active=1 and b.is_deleted=0 group by b.prod_id");
foreach($issRtnData as $row)
{
	$issRtnDataArr[$row[csf('prod_id')]]=$row[csf('issrtnqnty')];
}

$issDataArr=array();
$issData=sql_select("select a.id, b.prod_id, sum(b.cons_quantity) as issqnty from inv_issue_master a, inv_transaction b where a.id=b.mst_id and b.transaction_type=2 and a.entry_form=3 and a.item_category=1 and b.item_category=1 and b.status_active=1 and b.is_deleted=0 group by a.id, b.prod_id");
foreach($issData as $row)
{
	$issDataArr[$row[csf('id')]][$row[csf('prod_id')]]=$row[csf('issqnty')];
	$prod_issid_arr[$row[csf('prod_id')]].=$row[csf('id')].",";
}

$recvRtnDataArr=array();
$recvRtnData=sql_select("select a.received_id, b.prod_id, sum(b.cons_quantity) as recvrtnqnty from inv_issue_master a, inv_transaction b where a.id=b.mst_id and b.transaction_type=3 and a.entry_form=8 and a.item_category=1 and b.item_category=1 and b.status_active=1 and b.is_deleted=0 group by a.received_id, b.prod_id");
foreach($recvRtnData as $row)
{ 
	$recvRtnDataArr[$row[csf('received_id')]][$row[csf('prod_id')]]=$row[csf('recvrtnqnty')];
	$prod_recvRtnid_arr[$row[csf('prod_id')]].=$row[csf('received_id')].",";
}

$updateID_array = $update_data = array();
$update_array	= "allocated_qnty*available_qnty";

$i=0;
$dataArray=sql_select("select id, current_stock, allocated_qnty, available_qnty from product_details_master where item_category_id=1 id =1591");
foreach($dataArray as $row)
{
	$i++;
	//echo $row['id']."**".$row['current_stock']."**".$row['allocated_qnty']."**".$allocated_qnty_arr[$row['id']]."**".$row['available_qnty']."<br>";	
	//echo $row['id']."**".$row['allocated_qnty']."**".$allocated_qnty_arr[$row['id']]."<br>";

	$allocated_qnty=$allocated_qnty_arr[$row[csf('id')]]; 
	$available_qnty=0;
	
	$available_qnty-=$allocated_qnty;
	
	$recvid=explode(",",substr($prod_recvid_arr[$row[csf('id')]],0,-1));
	$recvRtnid=explode(",",substr($prod_recvRtnid_arr[$row[csf('id')]],0,-1));
	$issid=explode(",",substr($prod_issid_arr[$row[csf('id')]],0,-1));
	$issRtnid=explode(",",substr($prod_issRtnid_arr[$row[csf('id')]],0,-1));
	
	foreach($recvid as $rid)
	{
		$recv_pr=$receive_purpose_arr[$rid];
		if($recv_pr==2)
		{
			$allocated_qnty+=$recvDataArr[$rid][$row[csf('id')]];
		}
		else
		{
			$available_qnty+=$recvDataArr[$rid][$row[csf('id')]];
		}
	}
	
	foreach($recvRtnid as $rtcid)
	{
		$ret_pr=$issue_purpose_arr[$rtcid];
		if($ret_pr==2)
		{
			$allocated_qnty-=$recvRtnDataArr[$rtcid][$row[csf('id')]];
		}
		else
		{
			$available_qnty-=$recvRtnDataArr[$rtcid][$row[csf('id')]];
		}
	}
	
	foreach($issid as $isid)
	{
		$iss_pr=$issue_purpose_arr[$isid];
		if($iss_pr==1 || $iss_pr==2)
		{
			$allocated_qnty-=$issDataArr[$isid][$row[csf('id')]];
		}
		else
		{
			$available_qnty-=$issDataArr[$isid][$row[csf('id')]];
		}
	}
	
	$issRtn=$issRtnDataArr[$row['id']];
	$available_qnty+=$issRtn;
	//echo $allocated_qnty."**".$available_qnty."<br>";
	//echo $row['id']."**".$row['current_stock']."**".$allocated_qnty."**".$available_qnty."<br>";
	
	if($allocated_qnty=="") $allocated_qnty=0;
	if($available_qnty=="") $available_qnty=0;
	
	if($i>999)
	{
		$updateID_array2[]=$row[csf('id')];
		$update_data2[$row[csf('id')]]=explode("*",($allocated_qnty."*".$available_qnty));
	}
	else
	{
		$updateID_array[]=$row[csf('id')];
		$update_data[$row[csf('id')]]=explode("*",($allocated_qnty."*".$available_qnty));
	}
	
}
//echo $i;
echo bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array);
disconnect($con);
//echo "<br>Fuad<br>";
//echo bulk_update_sql_statement("product_details_master","id",$update_array,$update_data2,$updateID_array2);
die;*/
?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";


function active_inactive(str)
{
	$("#txt_booking_no").val('');
	$("#txt_booking_id").val('');
	$("#txt_item_description").val('');
	$("#txt_prod_id").val('');
	$("#txt_supplier_id").val('');
	$("#txt_issue_id").val('');
	$("#txt_return_qnty").val('');
	$("#tbl_child").find('select,input').val('');	
	
 	if(str==1 || str==3)
	{
		disable_enable_fields( 'txt_booking_no', 0, "", "" ); // disable false
 	}
	else
	{		
		disable_enable_fields( 'txt_booking_no', 1, "", "" ); // disable true
	}
}

function return_qnty_basis(purpose)
{
	var basis = parseInt($("#cbo_basis").val());
	
	$("#save_data").val('');
	$("#all_po_id").val('');
	$("#txt_return_qnty").val('');
	$("#distribution_method").val('');
		
	if((basis==2 && (purpose==3 || purpose==5 || purpose==6 || purpose==7 || purpose==10 || purpose==15 || purpose==30)) || (basis==1 && purpose==8))
	{
		$("#txt_return_qnty").attr('placeholder','Entry');
		$("#txt_return_qnty").removeAttr('ondblclick');
 		$("#txt_return_qnty").removeAttr('readOnly');
	}
	else 
	{
 		$("#txt_return_qnty").attr('placeholder','Double Click');
		$("#txt_return_qnty").attr('ondblclick','openmypage_po()');
		$("#txt_return_qnty").attr('readOnly',true);
	}
}

// popup for booking no ----------------------	
function popuppage_fabbook()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	
	var receive_basis=$('#cbo_basis').val();
	var company = $("#cbo_company_name").val();
	var page_link='requires/yarn_issue_return_controller.php?action=fabbook_popup&company='+company+'&receive_basis='+receive_basis;
	var title="K&D Information";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=400px, center=1, resize=0, scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var bookingNumber = this.contentDoc.getElementById("hidden_booking_number").value; //bookingID_bookingNo_buyerID_jobNo
		if (bookingNumber!="")
		{
			bookingNumber = bookingNumber.split("_"); 
			freeze_window(5);
			$("#txt_booking_id").val(bookingNumber[1]);
			$("#txt_booking_no").val(bookingNumber[2]);
			$("#booking_without_order").val(bookingNumber[3]);
			if(bookingNumber[0]==3)
			{
				get_php_form_data(bookingNumber[0]+'**'+company, "populate_knitting_source", "requires/yarn_issue_return_controller");
			}
			release_freezing();	 
		}
		var without_order=$("#booking_without_order").val();
		//alert (without_order);
		if (without_order==1)
		{
			$('#txt_return_qnty').val('');
			$('#txt_return_qnty').removeAttr('readonly','readonly');	
			$('#txt_return_qnty').removeAttr('onDblClick','openmypage_po();');	
			$('#txt_return_qnty').attr('placeholder','Entry');
		}
		else
		{
			$('#txt_return_qnty').val('');
			$('#txt_return_qnty').attr('readonly','readonly');
			$('#txt_return_qnty').attr('onDblClick','openmypage_po();');	
			$('#txt_return_qnty').attr('placeholder','Double Click To Search');
		}
	}
}


function openmypage_po() // issue quantity
{
	var receive_basis=$('#cbo_basis').val();
	var booking_no=$('#txt_booking_no').val();
	var cbo_company_id = $('#cbo_company_name').val();
 	var save_data = $('#save_data').val();
	var all_po_id = $('#all_po_id').val();
	var issueQnty = $('#txt_return_qnty').val();
	var distribution_method =  $('#distribution_method').val();
	
	if(form_validation('cbo_company_name*cbo_basis*txt_item_description','Company Name*Basis*Item Description')==false)
	{
		return;
	} 	
	
	if($('#cbo_basis').val()==1 || $('#cbo_basis').val()==3)
	{
		if(form_validation('txt_booking_no','F. Booking/Reqsn. No')==false)
	{
		return;
	} 
	}
	 
	var title = 'PO Info';	
	var page_link = 'requires/yarn_issue_return_controller.php?receive_basis='+receive_basis+'&cbo_company_id='+cbo_company_id+'&booking_no='+booking_no+'&all_po_id='+all_po_id+'&save_data='+save_data+'&issueQnty='+issueQnty+'&distribution_method='+distribution_method+'&action=po_popup';
	  
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=370px,center=1,resize=1,scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var save_string=this.contentDoc.getElementById("save_string").value;	 
		var tot_issue_qnty=this.contentDoc.getElementById("tot_grey_qnty").value;  //this is issue qnty 
 		var all_po_id=this.contentDoc.getElementById("all_po_id").value;  
		var distribution_method = this.contentDoc.getElementById("distribution_method").value;  
		//alert (tot_issue_qnty);

		$('#save_data').val(save_string);
		$('#txt_return_qnty').val( tot_issue_qnty );
		//alert(iss_qty);
 		$('#all_po_id').val(all_po_id);
		$('#distribution_method').val(distribution_method);
						
		fn_calculateAmount(tot_issue_qnty);
	}
}


function open_itemdesc()
{
	if( form_validation('cbo_company_name*cbo_basis','Company Name*Baisi')==false )
	{
		return;
	}
	else if( $("#cbo_basis").val()==1 && form_validation('txt_booking_no','Booking No')==false  )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var basis = $("#cbo_basis").val();
	var booking_no = $("#txt_booking_no").val(); 
 	var page_link='requires/yarn_issue_return_controller.php?action=itemdesc_popup&company='+company+'&booking_no='+booking_no+'&basis='+basis; 
	var title="Search Item Description";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=400px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var mrrNumber=this.contentDoc.getElementById("hidden_recv_number").value; // mrr number
  		// master part call here
		get_php_form_data(mrrNumber, "populate_data_from_data", "requires/yarn_issue_return_controller");  		
		//list view call here
		//show_list_view(mrrNumber,'show_dtls_list_view','list_container_yarn','requires/yarn_issue_return_controller','');
 	}
}
 
function fn_calculateAmount(qnty)
{
	var without_order=$("#booking_without_order").val();
	//alert (without_order);
	if (without_order!=1)
	{
		var rate = $("#txt_rate").val();
		//var rcvQnty = $("#txt_net_used").val()*1+$("#hide_net_used").val()*1;
		var rcvQnty = $("#txt_total_return").val()*1+qnty*1;
		
		if(qnty=="") //
		{
			return;
		}
		else if(rcvQnty*1>$("#txt_issue_qnty").val()*1) //
		{
			alert("Returned Qnty Exceed Net Qnty.");
			$('#txt_return_qnty').val('');
			$('#txt_amount').val(0);
			$("#txt_total_return_display").val( $("#txt_total_return").val());
			return;
		}
		else
		{		
			var amount = rate*qnty;
			$('#txt_amount').val(number_format_common(amount,"","",4));
		}
		//--------------
		var totalReturn = $("#txt_total_return").val()*1+$("#txt_return_qnty").val()*1;
		var balanceQnty = $("#txt_issue_qnty").val()*1-totalReturn;
		$("#txt_total_return_display").val(  number_format_common(totalReturn,"","",1) );
		$("#txt_net_used").val( number_format_common(balanceQnty,"","",1) );
	}
}

function fnc_yarn_issue_return_entry(operation)
{
	if(operation==4)
	 {
		var report_title=$( "div.form_caption" ).html();
		print_report( $('#cbo_company_name').val()+'*'+$('#txt_return_no').val()+'*'+report_title,"yarn_issue_return_print", "requires/yarn_issue_return_controller")
		 return;
	 }
	else if(operation==0 || operation==1 || operation==2)
	{
		if( form_validation('cbo_company_name*cbo_basis*txt_return_date*txt_return_challan_no*txt_item_description','Company Name*Basis*Return Date*Challan No*Item Description')==false )
		{
			return;
		}	
		
		if($("#txt_return_qnty").val()*1<=0 && $("#txt_reject_qnty").val()*1<=0)
		{
			alert("Return Quantity Should be Greater Than Zero(0).");
			return;
		}
		
		if($('#cbo_basis').val()==1 && $('#txt_booking_no').val()=="")
		{
			alert("Please Select Booking No");
			$('#txt_booking_no').focus();
			return;
		}
		else if($('#cbo_basis').val()==3 && $('#txt_booking_no').val()=="")
		{
			alert("Please Select Reqsn. No");
			$('#txt_booking_no').focus();
			return;
		}
		
		var dataString = "txt_return_no*cbo_company_name*cbo_basis*txt_booking_no*txt_booking_id*cbo_location*cbo_knitting_source*cbo_knitting_company*txt_return_date*txt_return_challan_no*txt_item_description*txt_prod_id*txt_supplier_id*txt_issue_id*txt_yarn_lot*cbo_uom*txt_return_qnty*txt_reject_qnty*cbo_store_name*txt_remarks*txt_issue_qnty*txt_rate*txt_total_return*txt_amount*txt_net_used*txt_issue_challan_no*before_prod_id*update_id*save_data*all_po_id*booking_without_order";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
		freeze_window(operation);
		http.open("POST","requires/yarn_issue_return_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_yarn_issue_return_entry_reponse;
	}
}

function fnc_yarn_issue_return_entry_reponse()
{	
	if(http.readyState == 4) 
	{	  		
		//release_freezing();return;
		var reponse=trim(http.responseText).split('**');		
		if(reponse[0]==20)
		{
			alert(reponse[1]);
			release_freezing();
			return;
		}
		show_msg(reponse[0]); 		
		if(reponse[0]==0 || reponse[0]==1)
		{
			$("#txt_return_no").val(reponse[1]);
 			disable_enable_fields( 'cbo_company_name*cbo_basis', 1, "", "" ); // disable true
				
			show_list_view(reponse[1],'show_dtls_list_view','list_container_yarn','requires/yarn_issue_return_controller','');		
			//child form reset here after save data-------------//
			$("#tbl_child").find('input,select').val('');
			reset_form('','','save_data*all_po_id*distribution_method*before_prod_id*update_id','','','');
			set_button_status(0, permission, 'fnc_yarn_issue_return_entry',1,1);
		}
		release_freezing();
	}
}



function open_returnpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/yarn_issue_return_controller.php?action=return_number_popup&company='+company; 
	var title="Search Return Number Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=400px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var returnNumber=this.contentDoc.getElementById("hidden_return_number").value; // mrr number
  		// master part call here
		get_php_form_data(returnNumber, "populate_master_from_data", "requires/yarn_issue_return_controller");  		
		//list view call here
		show_list_view(returnNumber,'show_dtls_list_view','list_container_yarn','requires/yarn_issue_return_controller','');
		disable_enable_fields( 'cbo_company_name', 1, "", "" ); // disable true
		set_button_status(0, permission, 'fnc_yarn_issue_return_entry',1,1);
 	}
}

//form reset/refresh function here
function fnResetForm()
{
	$("#tbl_master").find('input,select').attr("disabled", false);	
	set_button_status(0, permission, 'fnc_yarn_issue_return_entry',1,0);
	reset_form('yarn_issue_return_1','list_container_yarn','','','','');
}

</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../",$permission);  ?><br />    		 
    <form name="yarn_issue_return_1" id="yarn_issue_return_1" autocomplete="off" > 
    <div style="width:100%;">       
    <table width="100%" cellpadding="0" cellspacing="2" align="center">
     	<tr>
        	<td width="80%" align="center" valign="top">   
            	<fieldset style="width:1000px;">
                <legend>Yarn Issue Return</legend>
                  <br />
                 	<fieldset style="width:900px;">                                       
                        <table  width="900" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                            <tr>
                           		<td colspan="3" align="right"><b>Return Number</b></td>
                           		<td colspan="3" align="left"><input type="text" name="txt_return_no" id="txt_return_no" class="text_boxes" style="width:160px" placeholder="Double Click To Search" onDblClick="open_returnpopup()" readonly /></td>
               		      </tr>
                           <!--<tr>
                           		<td colspan="6" align="center">&nbsp;</td>
                          </tr>-->
                          <tr>
                                <td  width="120" align="right" class="must_entry_caption">Company Name </td>
                                <td width="170">
									<?php 
                                     	echo create_drop_down( "cbo_company_name", 170, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/yarn_issue_return_controller', this.value, 'load_drop_down_location', 'location_td' );load_drop_down( 'requires/yarn_issue_return_controller', this.value, 'load_drop_down_store', 'store_td' );" );
                                    ?>
                                </td>
                                <td width="120" align="right" class="must_entry_caption">Basis</td>
                                <td width="160">
                                	<?php 
										echo create_drop_down( "cbo_basis", 170, $issue_basis,"", 1, "-- Select Basis --", $selected, "active_inactive(this.value);", "", "");
									?>
                                </td>
                                <td width="120" align="right" >F. Booking/Reqsn. No</td>
                                <td width="170">
                                	<input name="txt_booking_no" id="txt_booking_no" class="text_boxes" style="width:160px"  placeholder="Double Click to Search" onDblClick="popuppage_fabbook();" readonly disabled />
                                    <input type="hidden" name="txt_booking_id" id="txt_booking_id" />
                                    <input type="hidden" name="booking_without_order" id="booking_without_order"/>
                                </td>
                          </tr>
                          <tr>
                                <td width="130" align="right">Location</td>
                                <td width="170" id="location_td"><?php 
                                        echo create_drop_down( "cbo_location", 170, $blank_array,"", 1, "-- Select Location --", "", "" );
                                    ?></td>
                                <td width="94" align="right" >Return Source</td>
                                <td width="160">
                                	<?php
                                        echo create_drop_down( "cbo_knitting_source", 170, $knitting_source,"", 1, "-- Select --", $selected, "load_drop_down( 'requires/yarn_issue_return_controller', this.value+'**'+$('#cbo_company_name').val(), 'load_drop_down_knit_com', 'knitting_company_td' );","","1,3" );
                                    ?>
                                </td>
                                <td width="130" align="right">Knitting Company</td>
                                <td width="" id="knitting_company_td">
                                    <?php
                                        echo create_drop_down( "cbo_knitting_company", 170, $blank_array,"", 1, "-- Select --", $selected, "","","" );
                                    ?>	
                                </td>
                          </tr>
                          <tr>
                            <td align="right" class="must_entry_caption">Return Date</td>
                            <td><input type="text" name="txt_return_date" id="txt_return_date" class="datepicker" style="width:160px;" placeholder="Select Date" /></td>
                            <td align="right" class="must_entry_caption">Return Challan</td>
                            <td><input type="text" name="txt_return_challan_no" id="txt_return_challan_no" class="text_boxes" style="width:160px" /></td>
                            <td align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right" >&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                    </fieldset>
                    <br />
                    <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                     <tr>
                   	   <td width="50%" valign="top" align="center">
                         <fieldset style="width:460px; float:left">  
                                <legend>Return Item Info</legend>                                     
                                  <table  width="450" cellspacing="2" cellpadding="0" border="0">
                                          <tr>
                                               <td width="110" align="right" class="must_entry_caption">Item Description&nbsp;</td>
                                               <td colspan="3">
                                               		<input class="text_boxes" type="text" name="txt_item_description" id="txt_item_description" style="width:300px;" placeholder="Double Click To Search" onDblClick="open_itemdesc()" readonly  />
                                                    <input type="hidden" id="txt_prod_id" name="txt_prod_id" />
                                                    <input type="hidden" id="txt_supplier_id" name="txt_supplier_id" />
                                                    <input type="hidden" id="txt_issue_id" name="txt_issue_id" />
                                               </td> 
                                          </tr>
                                          <tr>
                                               <td width="110" align="right">Yarn Lot&nbsp;</td>
                                               <td width="158"><input class="text_boxes" type="text" name="txt_yarn_lot" id="txt_yarn_lot" style="width:150px;" placeholder="Display" readonly  /></td>
                                               <td width="41" align="right">UOM</td>
                                               <td width="131"><?php echo create_drop_down( "cbo_uom", 100, $unit_of_measurement,"", 1, "Display", 0, "",1 ); ?></td>
                                          </tr>
                                          <tr>
                                               <td width="110" align="right" class="must_entry_caption">Returned Qnty&nbsp;</td>
                                               <td><input class="text_boxes_numeric" type="text" name="txt_return_qnty" id="txt_return_qnty" style="width:150px;" placeholder="Double Click To Search" onKeyUp="fn_calculateAmount(this.value)" readonly onDblClick="openmypage_po()"   /></td>
                                               <td align="right" width="41">Store</td>
                                               <td id="store_td"><?php echo create_drop_down( "cbo_store_name", 100, $blank_array,"", 1, "-- Select --", $storeName, "" ); ?></td>
                                          </tr>
                                          <tr>
                                               <td width="110" align="right">Reject Qty&nbsp;</td>
                                               <td colspan="3"><input class="text_boxes_numeric" type="text" name="txt_reject_qnty" id="txt_reject_qnty" style="width:150px;" placeholder="Entry"  /></td>
                                          </tr>
                                          <tr>
                                               <td width="110" align="right">Remarks&nbsp;</td>
                                               <td colspan="3"><input class="text_boxes" type="text" name="txt_remarks" id="txt_remarks" style="width:300px;" placeholder="Entry"  /></td>
                                          </tr>
                                   </table>
                            </fieldset>
                       	 <fieldset style="width:460px; float:left; margin-left:5px">  
                           <legend>Display</legend>                                     
                                  <table  width="450" cellspacing="2" cellpadding="0" border="0" id="display_table" >
                                           <tr>
                                              <td width="110" align="right">Issue Qnty&nbsp;</td>
                                              <td width="100"><input class="text_boxes" type="text" name="txt_issue_qnty" id="txt_issue_qnty" style="width:100px;" placeholder="Display" readonly  /></td>
                                              <td width="120" align="right">Rate&nbsp;</td>
                                              <td width="100"><input class="text_boxes" type="text" name="txt_rate" id="txt_rate" style="width:100px;" placeholder="Display" readonly  /></td> 
                                          </tr>
                                          <tr>
                                              <td align="right">Total Return&nbsp;</td>
                                              <td>
                                              		<input class="text_boxes" type="hidden" name="txt_total_return" id="txt_total_return" style="width:100px;" placeholder="Display" readonly  />
                                                    <input class="text_boxes" type="text" name="txt_total_return_display" id="txt_total_return_display" style="width:100px;" placeholder="Display" readonly  />
                                              </td>
                                              <td align="right">Amount&nbsp;&nbsp;</td>
                                              <td><input class="text_boxes" type="text" name="txt_amount" id="txt_amount" style="width:100px;" placeholder="Display" readonly /></td>                               			  </tr>
                                          <tr>
                                              <td align="right">Net Used&nbsp;</td>
                                              <td>
                                              	<input class="text_boxes" type="text" name="txt_net_used" id="txt_net_used" style="width:100px;" placeholder="Display" readonly />
                                                <input class="text_boxes" type="hidden" name="hide_net_used" id="hide_net_used" readonly />
                                              </td>
                                              <td align="right">Issue Challan No&nbsp;</td>
                                              <td><input class="text_boxes" type="text" name="txt_issue_challan_no" id="txt_issue_challan_no" style="width:100px;" placeholder="Display" readonly  /></td>                                    
                                          </tr>
                                          <tr>
                                              <td align="right">Returnable Qty.</td>
                                              <td><input class="text_boxes" type="text" name="txt_returnable_qnty" id="txt_returnable_qnty" style="width:100px;" placeholder="Display" readonly /></td>
                                              <td align="right">Returnable Bl. Qty.</td>
                                              <td><input class="text_boxes" type="text" name="txt_returnable_bl_qnty" id="txt_returnable_bl_qnty" style="width:100px;" placeholder="Display" readonly /></td>                                   
                                          </tr>
                                          <tr>
                                            <td align="right">&nbsp;</td>
                                            <td colspan="2">&nbsp;</td>
                                            <td>&nbsp;</td>
                                          </tr>  
                                  </table>
                            </fieldset>
                   	   </td>
                    </tr>
                </table>                
               	<table cellpadding="0" cellspacing="1" width="100%">
                	<tr> 
                       <td colspan="6" align="center"></td>				
                	</tr>
                	<tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <!-- details table id for update -->
                             <input type="hidden" name="save_data" id="save_data" readonly  />	
                             <input type="hidden" name="all_po_id" id="all_po_id" readonly />
                             <input type="hidden" id="distribution_method" readonly />
                             
                             <input type="hidden" id="before_prod_id" name="before_prod_id" value="" />
                             <input type="hidden" id="update_id" name="update_id" value="" />
                             <!-- -->
							 <?php echo load_submit_buttons( $permission, "fnc_yarn_issue_return_entry", 0,1,"fnResetForm()",1);?>
                        </td>
                   </tr> 
                </table>                 
              	</fieldset>
              	<fieldset style="width:1000px;">
    			<div style="width:990px;" id="list_container_yarn"></div>
    		  	</fieldset>
           </td>
         </tr>
    </table>
    </div>
    </form>
</div>    
</body>  
<script src="../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
