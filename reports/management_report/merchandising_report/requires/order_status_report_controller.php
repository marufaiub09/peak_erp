<?php
error_reporting('0');
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}

if ($action=="job_popup")
{
  	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
<script>
	function js_set_value(str)
	{
		$("#hide_job_no").val(str);
		parent.emailwindow.hide(); 
	}
</script>
<?php
	if($buyer_id==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_cond="";
		}
		else
		{
			$buyer_cond="";
		}
	}
	else
	{
		$buyer_cond=" and a.buyer_name=$buyer_id";
	}
	
	if(trim($cbo_year)!=0) 
	{
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)=$cbo_year";
			$year_field="YEAR(insert_date)";
		}
		else
		{
			$year_cond=" and to_char(insert_date,'YYYY')=$cbo_year";	
			$year_field="to_char(insert_date,'YYYY')";
		}
	}
	else $year_cond="";

	$arr=array (2=>$company_library,3=>$buyer_arr);
	$sql= "select a.job_no_prefix_num, a.job_no, a.company_name,a.buyer_name,a.style_ref_no,$year_field as year from wo_po_details_master a where a.company_name=$company_id $buyer_cond $year_cond order by a.id";
	//echo $sql;
	echo  create_list_view("list_view", "Job No,Year,Company,Buyer Name,Style Ref. No", "70,70,120,100,100","570","320",0, $sql , "js_set_value", "year,job_no", "", 1, "0,0,company_name,buyer_name,0", $arr , "job_no_prefix_num,year,company_name,buyer_name,style_ref_no", "","setFilterGrid('list_view',-1)",'0,0,0,0,0');
	echo "<input type='hidden' id='hide_job_no' />";
	
	exit();
}

if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";
	}
	
	$cbo_year=str_replace("'","",$cbo_year);
	if(trim($cbo_year)!=0) 
	{
		if($db_type==0)
		{
			$year_cond=" and YEAR(a.insert_date)=$cbo_year";
		}
		else
		{
			$year_cond=" and to_char(a.insert_date,'YYYY')=$cbo_year";	
		}
	}
	
	$txt_job_no=str_replace("'","",$txt_job_no);
	if(trim($txt_job_no)!="") $job_no_cond="%".trim($txt_job_no); else $job_no_cond="%%";//."%"
	
	ob_start();

?>
	<fieldset style="width:1200px; margin-left:5px">	
        <table width="1200">
            <tr>
                <td align="center" width="100%" colspan="12" class="form_caption"><?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?></td>
            </tr>
        </table>
        <?php
		
		$exFactoryArr=return_library_array("select sum(ex_factory_qnty) as qnty,po_break_down_id from pro_ex_factory_mst where status_active=1 and is_deleted=0 group by po_break_down_id","po_break_down_id","qnty");
		
		$i=1; $all_po_id=0; $job_no=''; $tot_po_qnty=0; $tot_exfactory_qnty=0; $ratio=0;
		$sql="select a.buyer_name, a.job_no, a.total_set_qnty as ratio, b.id as po_id, b.po_number, b.po_quantity as po_qnty, b.plan_cut, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$cbo_company_name and a.job_no like '$job_no_cond' and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 $buyer_id_cond $year_cond order by b.pub_shipment_date, b.id";	
		$result=sql_select($sql);

		if(empty($result))
		{
			echo '<div align="left" style="width:1000px;"><h1 align="center" style="color:#f00;">Order not found</h></div>'; die;
		}
		?>
        <div style="width:100%" align="center"> 
            <table width="900" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="40">SL</th>
                    <th width="120">Order No</th>
                    <th width="110">Job No</th>
                    <th width="110">Buyer</th>					
                    <th width="100">Order Qty. (Pcs)</th>
                    <th width="100">Shipment Date</th>
                    <th width="100">Ex-Fact. Qty.</th>
                    <th width="90">Ship Out %</th>
                    <th>Short / Exces</th>
                </thead>
                <?php
                    foreach($result as $row)
                    {
                        if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                        
                        $po_qnty=$row[csf('po_qnty')]*$row[csf('ratio')];
						$plun_cut_qnty=$row[csf('plan_cut')]*$row[csf('ratio')];
                        $exfactory_qnty=$exFactoryArr[$row[csf('po_id')]];
                        $short_excess=$po_qnty-$exfactory_qnty;
                        $ship_perc=($exfactory_qnty*100)/$po_qnty;
                        
                        $tot_po_qnty+=$po_qnty; 
						$tot_plun_cut_qnty+=$plun_cut_qnty; 
                        $tot_exfactory_qnty+=$exfactory_qnty; 
                        
                        if($all_po_id=="") $all_po_id=$row[csf('po_id')]; else $all_po_id.=",".$row[csf('po_id')];
						
						$job_no=$row[csf('job_no')];
						$ratio=$row[csf('ratio')];
                    ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                            <td width="40"><?php echo $i; ?></td>
                            <td width="120"><p><?php echo $row[csf('po_number')]; ?></p></td>
                            <td width="110"><p><?php echo $row[csf('job_no')]; ?></p></td>
                            <td width="110"><p><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></p></td>
                            <td width="100" align="right"><?php echo $po_qnty; ?>&nbsp;</td>
                            <td width="100" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                            <td width="100" align="right"><?php echo $exfactory_qnty; ?>&nbsp;</td>
                            <td width="90" align="right"><?php echo number_format($ship_perc,2); ?>&nbsp;</td>
                            <td align="right"><?php echo $short_excess; ?>&nbsp;</td>
                        </tr>
                    <?php
                        $i++;
                    }
                ?>
                <tfoot>
                    <th align="right" colspan="4">Total</th>					
                    <th><?php echo $tot_po_qnty; ?>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th><?php echo $tot_exfactory_qnty; ?>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th><?php echo $tot_short_excess=$tot_po_qnty-$tot_exfactory_qnty; ?>&nbsp;</th>
                </tfoot>
            </table>
        </div>
        <br />
        <?php
			$embroDataArr=sql_select("select  
						sum(CASE WHEN emb_name=1 THEN cons_dzn_gmts ELSE 0 END) AS print_cons,
						sum(CASE WHEN emb_name=2 THEN cons_dzn_gmts ELSE 0 END) AS embro_cons,
						sum(CASE WHEN emb_name=3 THEN cons_dzn_gmts ELSE 0 END) AS wash_cons
						from wo_pre_cost_embe_cost_dtls where job_no='$job_no' and is_deleted=0 and status_active=1");
			$print_cons=$embroDataArr[0][csf('print_cons')];				
			$embro_cons=$embroDataArr[0][csf('embro_cons')];				
			$wash_cons=$embroDataArr[0][csf('wash_cons')];

			$gmtsProdDataArr=sql_select("select  
						sum(CASE WHEN production_type=1 THEN production_quantity ELSE 0 END) AS cutting_qnty,
						sum(CASE WHEN production_type=2 and embel_name=1 and production_source=1 THEN production_quantity ELSE 0 END) AS print_issue_qnty_in,
						sum(CASE WHEN production_type=2 and embel_name=1 and production_source=3 THEN production_quantity ELSE 0 END) AS print_issue_qnty_out,
						sum(CASE WHEN production_type=3 and embel_name=1 and production_source=1 THEN production_quantity ELSE 0 END) AS print_recv_qnty_in,
						sum(CASE WHEN production_type=3 and embel_name=1 and production_source=3 THEN production_quantity ELSE 0 END) AS print_recv_qnty_out,
						sum(CASE WHEN production_type=2 and embel_name=2 and production_source=1 THEN production_quantity ELSE 0 END) AS emb_issue_qnty_in,
						sum(CASE WHEN production_type=2 and embel_name=2 and production_source=3 THEN production_quantity ELSE 0 END) AS emb_issue_qnty_out,
						sum(CASE WHEN production_type=3 and embel_name=2 and production_source=1 THEN production_quantity ELSE 0 END) AS emb_recv_qnty_in,
						sum(CASE WHEN production_type=3 and embel_name=2 and production_source=3 THEN production_quantity ELSE 0 END) AS emb_recv_qnty_out,
						sum(CASE WHEN production_type=4 and production_source=1 THEN production_quantity ELSE 0 END) AS sew_input_qnty_in,
						sum(CASE WHEN production_type=4 and production_source=3 THEN production_quantity ELSE 0 END) AS sew_input_qnty_out,
						sum(CASE WHEN production_type=5 and production_source=1 THEN production_quantity ELSE 0 END) AS sew_recv_qnty_in,
						sum(CASE WHEN production_type=5 and production_source=3 THEN production_quantity ELSE 0 END) AS sew_recv_qnty_out,
						sum(CASE WHEN production_type=8 and production_source=1 THEN production_quantity ELSE 0 END) AS finish_qnty_in,
						sum(CASE WHEN production_type=8 and production_source=3 THEN production_quantity ELSE 0 END) AS finish_qnty_out,
						sum(CASE WHEN production_type=3 and embel_name=3 and production_source=1 THEN production_quantity ELSE 0 END) AS wash_recv_qnty_in,
						sum(CASE WHEN production_type=3 and embel_name=3 and production_source=3 THEN production_quantity ELSE 0 END) AS wash_recv_qnty_out,
						sum(CASE WHEN production_type=3 and embel_name=1 THEN reject_qnty ELSE 0 END) AS print_reject_qnty,
						sum(CASE WHEN production_type=3 and embel_name=2 THEN reject_qnty ELSE 0 END) AS emb_reject_qnty,
						sum(CASE WHEN production_type=5 THEN reject_qnty ELSE 0 END) AS sew_reject_qnty,
						sum(CASE WHEN production_type=8 THEN reject_qnty ELSE 0 END) AS finish_reject_qnty
						from pro_garments_production_mst where po_break_down_id in($all_po_id) and is_deleted=0 and status_active=1");
							
			$cutting_qnty=$gmtsProdDataArr[0][csf('cutting_qnty')];	

			$reqDataArray=sql_select("select sum((b.requirment/b.pcs)*a.plan_cut_qnty) as grey_req, sum((b.cons/b.pcs)*a.plan_cut_qnty) as finish_req from wo_po_color_size_breakdown a, wo_pre_cos_fab_co_avg_con_dtls b where a.po_break_down_id=b.po_break_down_id and a.color_number_id=b.color_number_id and a.size_number_id=b.gmts_sizes and a.is_deleted=0 and a.status_active=1 and a.po_break_down_id in ($all_po_id)");
			$grey_fabric_req_qnty=$reqDataArray[0][csf('grey_req')];
			$finish_fabric_req_qnty=$reqDataArray[0][csf('finish_req')];
			
			$yarnDataArr=sql_select("select  
						sum(CASE WHEN a.entry_form=3 and c.entry_form=3 and c.knit_dye_source!=3 THEN a.quantity ELSE 0 END) AS issue_qnty_in,
						sum(CASE WHEN a.entry_form=3 and c.entry_form=3 and c.knit_dye_source=3 THEN a.quantity ELSE 0 END) AS issue_qnty_out
						from order_wise_pro_details a, inv_transaction b, inv_issue_master c where a.trans_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and b.item_category=1 and c.issue_purpose!=2 and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1");
						
			$yarnReturnDataArr=sql_select("select  
						sum(CASE WHEN a.entry_form=9 and c.entry_form=9 and c.knitting_source!=3 THEN a.quantity ELSE 0 END) AS return_qnty_in,
						sum(CASE WHEN a.entry_form=9 and c.entry_form=9 and c.knitting_source=3 THEN a.quantity ELSE 0 END) AS return_qnty_out
						from order_wise_pro_details a, inv_transaction b, inv_receive_master c where a.trans_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and b.item_category=1 and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1");
			
			$yarn_qnty_in=$yarnDataArr[0][csf('issue_qnty_in')]-$yarnReturnDataArr[0][csf('return_qnty_in')];
			$yarn_qnty_out=$yarnDataArr[0][csf('issue_qnty_out')]-$yarnReturnDataArr[0][csf('return_qnty_out')];
			
			$dataArrayTrans=sql_select("select
										sum(CASE WHEN entry_form ='11' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_yarn,
										sum(CASE WHEN entry_form ='11' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_yarn,
										sum(CASE WHEN entry_form ='13' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_knit,
										sum(CASE WHEN entry_form ='13' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_knit,
										sum(CASE WHEN entry_form ='15' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_finish,
										sum(CASE WHEN entry_form ='15' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_finish
									from order_wise_pro_details where status_active=1 and is_deleted=0 and entry_form in(11,13,15) and po_breakdown_id in($all_po_id)");
						
			$transfer_in_qnty_yarn=$dataArrayTrans[0][csf('transfer_in_qnty_yarn')];
			$transfer_out_qnty_yarn=$dataArrayTrans[0][csf('transfer_out_qnty_yarn')];	
			$transfer_in_qnty_knit=$dataArrayTrans[0][csf('transfer_in_qnty_knit')];
			$transfer_out_qnty_knit=$dataArrayTrans[0][csf('transfer_out_qnty_knit')];
			$transfer_in_qnty_finish=$dataArrayTrans[0][csf('transfer_in_qnty_finish')];
			$transfer_out_qnty_finish=$dataArrayTrans[0][csf('transfer_out_qnty_finish')];
			
			$total_issued=$yarn_qnty_in+$yarn_qnty_out+$transfer_in_qnty_yarn-$transfer_out_qnty_yarn;
			$under_over_issued=$grey_fabric_req_qnty-$total_issued;
			
			$greyYarnIssueQnty=return_field_value("sum(a.cons_quantity) as issue_qnty","inv_transaction a, inv_issue_master b","a.mst_id=b.id and b.entry_form=3 and b.issue_basis=1 and b.issue_purpose=2 and a.job_no='$job_no' and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=2","issue_qnty");
			
			$dyedYarnRecvQnty=return_field_value("sum(a.cons_quantity) as recv_qnty","inv_transaction a, inv_receive_master b","a.mst_id=b.id and b.entry_form=1 and b.receive_basis=2 and b.receive_purpose=2 and a.job_no='$job_no' and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=1","recv_qnty");
			
			$dyed_yarn_balance=$greyYarnIssueQnty-$dyedYarnRecvQnty;
			
			$yarn_qnty_in_perc=($yarn_qnty_in/$grey_fabric_req_qnty)*100;
			$yarn_qnty_out_perc=($yarn_qnty_out/$grey_fabric_req_qnty)*100;
			$transfer_in_qnty_yarn_perc=($transfer_in_qnty_yarn/$grey_fabric_req_qnty)*100;
			$transfer_out_qnty_yarn_perc=($transfer_out_qnty_yarn/$grey_fabric_req_qnty)*100;
			$total_issued_perc=($total_issued/$grey_fabric_req_qnty)*100;
			$under_over_issued_perc=($under_over_issued/$grey_fabric_req_qnty)*100;
			$greyYarnIssueQnty_perc=($greyYarnIssueQnty/$grey_fabric_req_qnty)*100;
			$dyedYarnRecvQnty_perc=($dyedYarnRecvQnty/$greyYarnIssueQnty)*100;
			$dyed_yarn_balance_perc=($dyed_yarn_balance/$greyYarnIssueQnty)*100;
			
			$prodDataArr=sql_select("select  
						sum(CASE WHEN c.knitting_source!=3 THEN a.quantity ELSE 0 END) AS knit_qnty_in,
						sum(CASE WHEN c.knitting_source=3 THEN a.quantity ELSE 0 END) AS knit_qnty_out
						from order_wise_pro_details a, pro_grey_prod_entry_dtls b, inv_receive_master c where a.dtls_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and c.item_category=13 and a.entry_form in(2,22) and c.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 and c.receive_basis<>9");
			$knit_qnty_in=$prodDataArr[0][csf('knit_qnty_in')];
			$knit_qnty_out=$prodDataArr[0][csf('knit_qnty_out')];
			
			$prodFinDataArr=sql_select("select  
						sum(CASE WHEN c.knitting_source!=3 THEN a.quantity ELSE 0 END) AS finish_qnty_in,
						sum(CASE WHEN c.knitting_source=3 THEN a.quantity ELSE 0 END) AS finish_qnty_out
						from order_wise_pro_details a, pro_finish_fabric_rcv_dtls b, inv_receive_master c where a.dtls_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and c.item_category=2 and a.entry_form in(7,37) and c.entry_form in(7,37) and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 and c.receive_basis<>9");
			$finish_qnty_in=$prodFinDataArr[0][csf('finish_qnty_in')];
			$finish_qnty_out=$prodFinDataArr[0][csf('finish_qnty_out')];
			
			$issueData=sql_select("select 
									sum(CASE WHEN entry_form=16 THEN quantity ELSE 0 END) AS grey_issue_qnty,
									sum(CASE WHEN entry_form=18 THEN quantity ELSE 0 END) AS issue_to_cut_qnty
									from order_wise_pro_details where po_breakdown_id in($all_po_id) and status_active=1 and is_deleted=0");
			
			$issuedToDyeQnty=$issueData[0][csf('grey_issue_qnty')];	
			$issuedToCutQnty=$issueData[0][csf('issue_to_cut_qnty')];				
			
			$total_knitting=$knit_qnty_in+$knit_qnty_out+$transfer_in_qnty_knit-$transfer_out_qnty_knit;
			$process_loss=$total_issued-$knit_qnty_in-$knit_qnty_out;
			$under_over_prod=$grey_fabric_req_qnty-$total_knitting;
			$left_over=$total_knitting-$issuedToDyeQnty;
			
			$knit_qnty_in_perc=($knit_qnty_in/$grey_fabric_req_qnty)*100;
			$knit_qnty_out_perc=($knit_qnty_out/$grey_fabric_req_qnty)*100;
			$transfer_in_qnty_knit_perc=($transfer_in_qnty_knit/$grey_fabric_req_qnty)*100;
			$transfer_out_qnty_knit_perc=($transfer_out_qnty_knit/$grey_fabric_req_qnty)*100;
			$total_knitting_perc=($total_knitting/$grey_fabric_req_qnty)*100;
			$process_loss_perc=($process_loss/$grey_fabric_req_qnty)*100;
			$under_over_prod_perc=($under_over_prod/$grey_fabric_req_qnty)*100;
			$issuedToDyeQnty_perc=($issuedToDyeQnty/$grey_fabric_req_qnty)*100;
			$left_over_perc=($left_over/$total_knitting)*100;
			
			$total_finishing=$finish_qnty_in+$finish_qnty_out+$transfer_in_qnty_finish-$transfer_out_qnty_finish;
			$process_loss_finishing=$issuedToDyeQnty-$total_finishing;
			$under_over_finish_prod=$finish_fabric_req_qnty-$total_finishing;
			$finish_left_over=$total_finishing-$issuedToCutQnty;
			
			$finish_qnty_in_perc=($finish_qnty_in/$finish_fabric_req_qnty)*100;
			$finish_qnty_out_perc=($finish_qnty_out/$finish_fabric_req_qnty)*100;
			$transfer_in_qnty_finish_perc=($transfer_in_qnty_finish/$finish_fabric_req_qnty)*100;
			$transfer_out_qnty_finish_perc=($transfer_out_qnty_finish/$finish_fabric_req_qnty)*100;
			$total_finishing_perc=($total_finishing/$finish_fabric_req_qnty)*100;
			$process_loss_finishing_perc=($process_loss_finishing/$finish_fabric_req_qnty)*100;
			$under_over_finish_prod_perc=($under_over_finish_prod/$finish_fabric_req_qnty)*100;
			$issuedToCutQnty_perc=($issuedToCutQnty/$finish_fabric_req_qnty)*100;
			$finish_left_over_perc=($finish_left_over/$total_finishing)*100;
			
			$fab_recv_perc=($issuedToCutQnty/$finish_fabric_req_qnty)*100;
			$actual_perc=($cutting_qnty/$tot_plun_cut_qnty)*100;
		?>
        <table width="1000" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
			<thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="7" width="630">Yarn Status</th>
                    <th colspan="3" width="270">Dyed Yarn Status</th>
                </tr>
                <tr>
                    <th width="90">Required</th>
                    <th width="90">Issued Inside</th>
                    <th width="90">Issued SubCon</th>
                    <th width="90">Transfer In</th>
                    <th width="90">Transfer Out</th>
                    <th width="90">Total Issued</th>
                    <th width="90">Under or Over Issued</th>
                    <th width="90">Grey Yarn Issued</th>
                    <th width="90">Dyed Yarn Received</th>
                    <th>Balance</th>
                </tr>
			</thead>
            <tr bgcolor="#FFFFFF">
            	<td>Quantity</td>
                <td align="right"><?php echo number_format($grey_fabric_req_qnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($yarn_qnty_in,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($yarn_qnty_out,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_in_qnty_yarn,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_out_qnty_yarn,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_issued,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($under_over_issued,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($greyYarnIssueQnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($dyedYarnRecvQnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($dyed_yarn_balance,2,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
            	<td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($yarn_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($yarn_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_in_qnty_yarn_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_out_qnty_yarn_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_issued_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($under_over_issued_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($greyYarnIssueQnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($dyedYarnRecvQnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($dyed_yarn_balance_perc,2,'.',''); ?>&nbsp;</td>
            </tr>
    	</table> 
        <br />
        <table width="1000" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
			<thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="10" width="910">Knitting Production</th>
                </tr>
                <tr>
                    <th width="90">Gray Fab Req.</th>
                    <th width="90">Inside Prod.</th>
                    <th width="90">SubCon Prod.</th>
                    <th width="90">Transfer In</th>
                    <th width="90">Transfer Out</th>
                    <th width="90">Total Prod.</th>
                    <th width="90">Process Loss</th>
                    <th width="90">Under or Over Prod.</th>
                    <th width="90">Issued To Dyeing</th>
                    <th>Left Over</th>
                </tr>
			</thead>
            <tr bgcolor="#FFFFFF">
            	<td>Quantity</td>
                <td align="right"><?php echo number_format($grey_fabric_req_qnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($knit_qnty_in,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($knit_qnty_out,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_in_qnty_knit,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_out_qnty_knit,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_knitting,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($under_over_prod,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($issuedToDyeQnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($left_over,2,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
            	<td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($knit_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($knit_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_in_qnty_knit_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_out_qnty_knit_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_knitting_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($under_over_prod_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($issuedToDyeQnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($left_over_perc,2,'.',''); ?>&nbsp;</td>
            </tr>
    	</table>  
        <br />
        <table width="1000" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
			<thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="10" width="910">Dyeing and Finish Fabric Production</th>
                </tr>
                <tr>
                    <th width="90">Finish Fab Req.</th>
                    <th width="90">Inside Prod.</th>
                    <th width="90">SubCon Prod.</th>
                    <th width="90">Transfer In</th>
                    <th width="90">Transfer Out</th>
                    <th width="90">Total Prod.</th>
                    <th width="90">Process Loss</th>
                    <th width="90">Under or Over Prod.</th>
                    <th width="90">Issued To Cutting</th>
                    <th>Left Over</th>
                </tr>
			</thead>
            <tr bgcolor="#FFFFFF">
            	<td>Quantity</td>
                <td align="right"><?php echo number_format($finish_fabric_req_qnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_in,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_out,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_in_qnty_finish,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_out_qnty_finish,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_finishing,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss_finishing,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($under_over_finish_prod,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($issuedToCutQnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_left_over,2,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
            	<td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_in_qnty_finish_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($transfer_out_qnty_finish_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_finishing_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss_finishing_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($under_over_finish_prod_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($issuedToCutQnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_left_over_perc,2,'.',''); ?>&nbsp;</td>
            </tr>
    	</table>
        <br />
        <table width="640" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
			<thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="6" width="550">Cutting Production</th>
                </tr>
                <tr>
                    <th width="90">Fabric Req.</th>
                    <th width="90">Fabric Recv.</th>
                    <th width="90">Possible Cut (Pcs)</th>
                    <th width="90">Gmts. Req.</th>
                    <th width="90">Actual Cut (Pcs)</th>
                    <th>Process Loss</th>
                </tr>
			</thead>
            <tr bgcolor="#FFFFFF">
            	<td>Quantity</td>
                <td align="right"><?php echo number_format($finish_fabric_req_qnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($issuedToCutQnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php //echo number_format($possible_cut_qnty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_plun_cut_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($cutting_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php //echo number_format($cutting_process_loss,2,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
            	<td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($fab_recv_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php //echo number_format($possible_cut_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right">&nbsp;</td>
                <td align="right"><?php echo number_format($actual_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php //echo number_format($cutting_process_loss_perc,2,'.',''); ?>&nbsp;</td>
            </tr>
    	</table> 
        <?php
		if($print_cons>0)
		{
			$print_issue_qnty_in=$gmtsProdDataArr[0][csf('print_issue_qnty_in')];
			$print_issue_qnty_out=$gmtsProdDataArr[0][csf('print_issue_qnty_out')];
			$print_recv_qnty_in=$gmtsProdDataArr[0][csf('print_recv_qnty_in')];
			$print_recv_qnty_out=$gmtsProdDataArr[0][csf('print_recv_qnty_out')];
			$print_reject_qnty=$gmtsProdDataArr[0][csf('print_reject_qnty')];
			
			$total_print_issued=$print_issue_qnty_in+$print_issue_qnty_out;
			$total_print_recv=$print_recv_qnty_in+$print_recv_qnty_out;
			
			$print_issue_qnty_in_perc=($print_issue_qnty_in/$tot_po_qnty)*100;
			$print_issue_qnty_out_perc=($print_issue_qnty_out/$tot_po_qnty)*100;
			$print_recv_qnty_in_perc=($print_recv_qnty_in/$tot_po_qnty)*100;
			$print_recv_qnty_out_perc=($print_recv_qnty_out/$tot_po_qnty)*100;
			
			$total_print_issued_perc=($total_print_issued/$tot_po_qnty)*100;
			$total_print_recv_perc=($total_print_recv/$tot_po_qnty)*100;
			
			$print_reject_qnty_perc=($print_reject_qnty/$total_print_recv)*100;
		?>
        	<br />
            <table width="820" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <tr>
                        <th width="90" rowspan="2">Particulars</th>
                        <th colspan="8" width="730">Garments Printing</th>
                    </tr>
                    <tr>
                        <th width="90">Gmts. Req.</th>
                        <th width="90">Issued Inside</th>
                        <th width="90">Issued SubCon</th>
                        <th width="90">Total Issued</th>
                        <th width="90">Print Inside</th>
                        <th width="90">Print SubCon</th>
                        <th width="90">Total Print</th>
                        <th>Reject</th>
                    </tr>
                </thead>
                <tr bgcolor="#FFFFFF">
                    <td>Quantity</td>
                    <td align="right"><?php echo number_format($tot_po_qnty,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_issue_qnty_in,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_issue_qnty_out,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_print_issued,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_recv_qnty_in,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_recv_qnty_out,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_print_recv,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_reject_qnty,2,'.',''); ?>&nbsp;</td>
                </tr>
                <tr bgcolor="#E9F3FF">
                    <td>In %</td>
                    <td>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_issue_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_issue_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_print_issued_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_recv_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_recv_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_print_recv_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($print_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
                 </tr>
            </table>
        <?php
		}
		
		if($embro_cons>0)
		{
			$emb_issue_qnty_in=$gmtsProdDataArr[0][csf('emb_issue_qnty_in')];
			$emb_issue_qnty_out=$gmtsProdDataArr[0][csf('emb_issue_qnty_out')];
			$emb_recv_qnty_in=$gmtsProdDataArr[0][csf('emb_recv_qnty_in')];
			$emb_recv_qnty_out=$gmtsProdDataArr[0][csf('emb_recv_qnty_out')];
			$emb_reject_qnty=$gmtsProdDataArr[0][csf('emb_reject_qnty')];
			
			$total_emb_issued=$emb_issue_qnty_in+$emb_issue_qnty_out;
			$total_emb_recv=$emb_recv_qnty_in+$emb_recv_qnty_out;
			
			$emb_issue_qnty_in_perc=($emb_issue_qnty_in/$tot_po_qnty)*100;
			$emb_issue_qnty_out_perc=($emb_issue_qnty_out/$tot_po_qnty)*100;
			$emb_recv_qnty_in_perc=($emb_recv_qnty_in/$tot_po_qnty)*100;
			$emb_recv_qnty_out_perc=($emb_recv_qnty_out/$tot_po_qnty)*100;
			
			$total_emb_issued_perc=($total_emb_issued/$tot_po_qnty)*100;
			$total_emb_recv_perc=($total_emb_recv/$tot_po_qnty)*100;
			
			$emb_reject_qnty_perc=($emb_reject_qnty/$total_print_recv)*100;
		?>
        	<br />
            <table width="820" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <tr>
                        <th width="90" rowspan="2">Particulars</th>
                        <th colspan="8" width="730">Embroidery</th>
                    </tr>
                    <tr>
                        <th width="90">Gmts. Req.</th>
                        <th width="90">Issued Inside</th>
                        <th width="90">Issued SubCon</th>
                        <th width="90">Total Issued</th>
                        <th width="90">Emb. Inside</th>
                        <th width="90">Emb. SubCon</th>
                        <th width="90">Total Emb.</th>
                        <th>Reject</th>
                    </tr>
                </thead>
                <tr bgcolor="#FFFFFF">
                    <td>Quantity</td>
                    <td align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_issue_qnty_in,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_issue_qnty_out,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_emb_issued,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_recv_qnty_in,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_recv_qnty_out,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_emb_recv,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_reject_qnty,0,'.',''); ?>&nbsp;</td>
                </tr>
                <tr bgcolor="#E9F3FF">
                    <td>In %</td>
                    <td>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_issue_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_issue_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_emb_issued_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_recv_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_recv_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_emb_recv_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($emb_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
                 </tr>
            </table>
        <?php
		}
		
		$sew_input_qnty_in=$gmtsProdDataArr[0][csf('sew_input_qnty_in')];
		$sew_input_qnty_out=$gmtsProdDataArr[0][csf('sew_input_qnty_out')];
		$total_sew_issued=$sew_input_qnty_in+$sew_input_qnty_out;
		
		$sew_input_qnty_in_perc=($sew_input_qnty_in/$tot_po_qnty)*100;
		$sew_input_qnty_out_perc=($sew_input_qnty_out/$tot_po_qnty)*100;
		$total_sew_issued_perc=($total_sew_issued/$tot_po_qnty)*100;
		
		$sew_balance_qnty=$tot_po_qnty-$total_sew_issued;
		$sew_balance_qnty_perc=($sew_balance_qnty/$tot_po_qnty)*100;
		?>    
        <br />
        <table width="550" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="5" width="460">Input</th>
                </tr>
                <tr>
                    <th width="90">Gmts. Req.</th>
                    <th width="90">Input Inside</th>
                    <th width="90">Input SubCon</th>
                    <th width="90">Total Input</th>
                    <th>Balance</th>
                </tr>
            </thead>
            <tr bgcolor="#FFFFFF">
                <td>Quantity</td>
                <td align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_input_qnty_in,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_input_qnty_out,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_issued,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_balance_qnty,0,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
                <td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_input_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_input_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_issued_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_balance_qnty_perc,2,'.',''); ?>&nbsp;</td>
             </tr>
        </table>   
        <br />
        <table width="720" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="7" width="630">Accessories Status</th>
                </tr>
                <tr>
                    <th width="110">Item</th>
                    <th width="70">UOM</th>
                    <th width="90">Req. Qty.</th>
                    <th width="90">Received</th>
                    <th width="90">Recv. Balance</th>
                    <th width="90">Issued</th>
                    <th>Left Over</th>
                </tr>
            </thead>
            <?php
			$item_library=return_library_array( "select id, item_name from lib_item_group", "id", "item_name"  );
			$trims_array=array();
			$trimsDataArr=sql_select("select b.item_group_id,  
									sum(CASE WHEN a.entry_form=24 THEN a.quantity ELSE 0 END) AS recv_qnty,
									sum(CASE WHEN a.entry_form=25 THEN a.quantity ELSE 0 END) AS issue_qnty
									from order_wise_pro_details a, product_details_master b where a.prod_id=b.id and a.po_breakdown_id in($all_po_id) and b.item_category_id=4 and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 group by b.item_group_id");
			foreach($trimsDataArr as $row)	
			{
				$trims_array[$row[csf('item_group_id')]]['recv']=$row[csf('recv_qnty')];
				$trims_array[$row[csf('item_group_id')]]['iss']=$row[csf('issue_qnty')];
			}

			$trimsDataArr=sql_select("select a.job_no, a.costing_per, b.trim_group, b.cons_uom, sum(b.cons_dzn_gmts) cons_dzn_gmts from wo_pre_cost_mst a, wo_pre_cost_trim_cost_dtls b where a.job_no=b.job_no and b.status_active=1 and b.is_deleted=0 and b.job_no='$job_no' group by b.trim_group, a.job_no, a.costing_per, b.cons_uom");
			$i=1; $tot_accss_req_qnty=0; $tot_recv_qnty=0; $tot_iss_qnty=0; $tot_recv_bl_qnty=0; $tot_trims_left_over_qnty=0;
			foreach($trimsDataArr as $row)
			{
				if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$dzn_qnty='';
				if($row[csf('costing_per')]==1) $dzn_qnty=12;
				else if($row[csf('costing_per')]==3) $dzn_qnty=12*2;
				else if($row[csf('costing_per')]==4) $dzn_qnty=12*3;
				else if($row[csf('costing_per')]==5) $dzn_qnty=12*4;
				else $dzn_qnty=1;
				
				$dzn_qnty=$dzn_qnty*$ratio;
        		$accss_req_qnty=($row[csf('cons_dzn_gmts')]/$dzn_qnty)*$tot_po_qnty;
				
				$trims_recv=$trims_array[$row[csf('trim_group')]]['recv'];
				$trims_issue=$trims_array[$row[csf('trim_group')]]['iss'];
				$recv_bl=$accss_req_qnty-$trims_recv;
				$trims_left_over=$trims_recv-$trims_issue;
			?>
            	<tr bgcolor="<?php echo $bgcolor; ?>">
                    <td>&nbsp;</td>
                    <td><p><?php echo $item_library[$row[csf('trim_group')]]; ?>&nbsp;</p></td>
                    <td align="center"><?php echo $unit_of_measurement[$row[csf('cons_uom')]]; ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($accss_req_qnty,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($trims_recv,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($recv_bl,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($trims_issue,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($trims_left_over,2,'.',''); ?>&nbsp;</td>
                </tr>
            <?php
				$tot_accss_req_qnty+=$accss_req_qnty;
				$tot_recv_qnty+=$trims_recv;
				$tot_recv_bl_qnty+=$recv_bl;
				$tot_iss_qnty+=$trims_issue;
				$tot_trims_left_over_qnty+=$trims_left_over;
				$i++;
			}
			$tot_trims_left_over_qnty_perc=($tot_trims_left_over_qnty/$tot_recv_qnty)*100;
			?>
            <tfoot>
                <tr>
                    <th>Total</th>
                    <th align="right">&nbsp;</th>
                    <th align="right">&nbsp;</th>
                    <th align="right"><?php echo number_format($tot_accss_req_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><?php echo number_format($tot_recv_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><?php echo number_format($tot_recv_bl_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><?php echo number_format($tot_iss_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><?php echo number_format($tot_trims_left_over_qnty,0,'.',''); ?>&nbsp;</th>
                </tr>
             </tfoot>
        </table>    
        <br />
        <?php
			$sew_recv_qnty_in=$gmtsProdDataArr[0][csf('sew_recv_qnty_in')];
			$sew_recv_qnty_out=$gmtsProdDataArr[0][csf('sew_recv_qnty_out')];
			$total_sew_recv=$sew_recv_qnty_in+$sew_recv_qnty_out;
			
			$sew_recv_qnty_in_perc=($sew_recv_qnty_in/$tot_po_qnty)*100;
			$sew_recv_qnty_out_perc=($sew_recv_qnty_out/$tot_po_qnty)*100;
			$total_sew_recv_perc=($total_sew_recv/$tot_po_qnty)*100;
			
			$sew_balance_recv_qnty=$tot_po_qnty-$total_sew_recv;
			$sew_balance_recv_qnty_perc=($sew_balance_recv_qnty/$tot_po_qnty)*100;
			
			$sew_reject_qnty=$gmtsProdDataArr[0][csf('sew_reject_qnty')];
			$sew_reject_qnty_perc=($sew_reject_qnty/$total_print_recv)*100;
		?>
        <table width="720" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="7" width="630">Sewing Production</th>
                </tr>
                <tr>
                    <th width="90">Gmts. Req.</th>
                    <th width="90">Input Revd</th>
                    <th width="90">Sew Inside</th>
                    <th width="90">Sew SubCon</th>
                    <th width="90">Total Sew</th>
                    <th width="90">Sew Balance</th>
                    <th>Reject</th>
                </tr>
            </thead>
            <tr bgcolor="#FFFFFF">
                <td>Quantity</td>
                <td align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_issued,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_recv_qnty_in,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_recv_qnty_out,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_recv,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_balance_recv_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_reject_qnty,0,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
                <td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_issued_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_recv_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_recv_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_recv_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_balance_recv_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
             </tr>
        </table>  
        <?php
		if($wash_cons>0)
		{
			$wash_recv_qnty_in=$gmtsProdDataArr[0][csf('wash_recv_qnty_in')];
			$wash_recv_qnty_out=$gmtsProdDataArr[0][csf('wash_recv_qnty_out')];
			$total_wash_recv=$wash_recv_qnty_in+$wash_recv_qnty_out;
			
			$wash_recv_qnty_in_perc=($wash_recv_qnty_in/$tot_po_qnty)*100;
			$wash_recv_qnty_out_perc=($wash_recv_qnty_out/$tot_po_qnty)*100;
			$total_wash_recv_perc=($total_wash_recv/$tot_po_qnty)*100;
			
			$wash_balance_qnty=$tot_po_qnty-$total_wash_recv;
			$wash_balance_qnty_perc=($wash_balance_qnty/$tot_po_qnty)*100;
		?>
        	<br />
            <table width="550" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <tr>
                        <th width="90" rowspan="2">Particulars</th>
                        <th colspan="5" width="460">Garment Washing</th>
                    </tr>
                    <tr>
                        <th width="90">Gmts. Req.</th>
                        <th width="90">Wash Inside</th>
                        <th width="90">Wash SubCon</th>
                        <th width="90">Total Wash</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tr bgcolor="#FFFFFF">
                    <td>Quantity</td>
                    <td align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($wash_recv_qnty_in,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($wash_recv_qnty_out,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_wash_recv,0,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($wash_balance_qnty,0,'.',''); ?>&nbsp;</td>
                </tr>
                <tr bgcolor="#E9F3FF">
                    <td>In %</td>
                    <td>&nbsp;</td>
                    <td align="right"><?php echo number_format($wash_recv_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($wash_recv_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($total_wash_recv_perc,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($wash_balance_qnty_perc,2,'.',''); ?>&nbsp;</td>
                 </tr>
            </table>
        <?php
		}
		
		$finish_qnty_in=$gmtsProdDataArr[0][csf('finish_qnty_in')];
		$finish_qnty_out=$gmtsProdDataArr[0][csf('finish_qnty_out')];
		$total_finish_qnty=$finish_qnty_in+$finish_qnty_out;
		
		$finish_qnty_in_perc=($finish_qnty_in/$tot_po_qnty)*100;
		$finish_qnty_out_perc=($finish_qnty_out/$tot_po_qnty)*100;
		$total_finish_qnty_perc=($total_finish_qnty/$tot_po_qnty)*100;
		
		$finish_balance_qnty=$tot_po_qnty-$total_finish_qnty;
		$finish_balance_qnty_perc=($finish_balance_qnty/$tot_po_qnty)*100;
		
		$finish_reject_qnty=$gmtsProdDataArr[0][csf('finish_reject_qnty')];
		$finish_reject_qnty_perc=($finish_reject_qnty/$total_print_recv)*100;
		?>
        <br />
        <table width="720" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="7" width="630">Garment Finishing</th>
                </tr>
                <tr>
                    <th width="90">Gmts. Req.</th>
                    <th width="90">Received</th>
                    <th width="90">Finish Inside</th>
                    <th width="90">Finish SubCon</th>
                    <th width="90">Total Finish</th>
                    <th width="90">Finish Balance</th>
                    <th>Reject</th>
                </tr>
            </thead>
            <tr bgcolor="#FFFFFF">
                <td>Quantity</td>
                <td align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_recv,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_in,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_out,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_finish_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_balance_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_reject_qnty,0,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
                <td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($total_sew_recv_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_in_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_qnty_out_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_finish_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
             </tr>
        </table> 
        <br />
        <?php
			$left_over_finish_gmts=$total_finish_qnty-$tot_exfactory_qnty;
			$left_over_finish_gmts_perc=($left_over_finish_gmts/$tot_po_qnty)*100;
			$tot_exfactory_qnty_perc=($tot_exfactory_qnty/$tot_po_qnty)*100;
		?>
        <table width="450" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="4" width="360">Garment Delivery</th>
                </tr>
                <tr>
                    <th width="90">Gmts. Req.</th>
                    <th width="90">Total Finish</th>
                    <th width="90">Ex-Factory</th>
                    <th>Left Over</th>
                </tr>
            </thead>
            <tr bgcolor="#FFFFFF">
                <td>Quantity</td>
                <td align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($total_finish_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_exfactory_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($left_over_finish_gmts,0,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
                <td>In %</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($total_finish_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_exfactory_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($left_over_finish_gmts_perc,2,'.',''); ?>&nbsp;</td>
             </tr>
        </table>  
        <br />
        <table width="450" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <tr>
                    <th width="90" rowspan="2">Particulars</th>
                    <th colspan="4" width="360">Left Over</th>
                </tr>
                <tr>
                    <th width="90">Gray Fab.</th>
                    <th width="90">Finish Fab.</th>
                    <th width="90">Garment</th>
                    <th>Trims</th>
                </tr>
            </thead>
            <tr bgcolor="#FFFFFF">
                <td>Quantity</td>
                <td align="right"><?php echo number_format($left_over,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_left_over,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($left_over_finish_gmts,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_trims_left_over_qnty,2,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
                <td>In %</td>
                <td align="right"><?php echo number_format($left_over_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_left_over_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($left_over_finish_gmts_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_trims_left_over_qnty_perc,2,'.',''); ?>&nbsp;</td>
             </tr>
        </table> 
        <br />
        <table width="720" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
            	<tr>
                    <th width="450" colspan="5">Reject</th>
                    <th width="270" colspan="3">Process Loss</th>
                </tr>
                <tr>
                    <th width="90">Particulars</th>
                    <th width="90">Print Gmts.</th>
                    <th width="90">Emb Gmts.</th>
                    <th width="90">Sewing Gmts.</th>
                    <th width="90">Finishing Gmts.</th>
                    <th width="90">Yarn</th>
                    <th width="90">Dyeing</th>
                    <th>Cutting</th>
            	</tr>
            </thead>
            <tr bgcolor="#FFFFFF">
                <td>Quantity</td>
                <td align="right"><?php echo number_format($print_reject_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($emb_reject_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_reject_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_reject_qnty,0,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss_finishing,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($cutting_process_loss,2,'.',''); ?>&nbsp;</td>
            </tr>
            <tr bgcolor="#E9F3FF">
                <td>In %</td>
                <td align="right"><?php echo number_format($print_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($emb_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($sew_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($finish_reject_qnty_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($process_loss_finishing_perc,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($cutting_process_loss_perc,2,'.',''); ?>&nbsp;</td>
             </tr>
        </table>     
	</fieldset>
<?php
	$user_id=$_SESSION['logic_erp']['user_id'];
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
}

disconnect($con);
?>
