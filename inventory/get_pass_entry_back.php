<?
/*-------------------------------------------- Comments
Purpose			: 	This form will create Out Order Entry
				
Functionality	:	
JS Functions	:
Created by		:	Ashraful 
Creation date 	: 	01-10-2013
Updated by 		: 	
Update date		: 	8-11-2014		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Receive Info","../", 1, 1, $unicode,1,1); 

?>
<script>
var permission='<? echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
 
  	
// popup for SYSTEM ID----------------------
function openmypage_system()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();
	page_link='requires/get_pass_entry_controller.php?action=system_id_popup&company='+company;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe',page_link,'Gate Out Id Popup', 'width=900px, height=350px, center=1, resize=0, scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var sysNumber=(this.contentDoc.getElementById("hidden_sys_number").value).split('_');; // system number
		if(sysNumber!="")
		{
			freeze_window(5);
			$("#txt_system_id").val(sysNumber[1]);
			//reset_form('','','txt_item_description*txt_quantity*cbo_uom*txt_rate*txt_amount','','','');
			get_php_form_data(sysNumber[1], "populate_master_from_data", "requires/get_pass_entry_controller" );
			show_list_view(sysNumber[0],'show_update_list_view','list_container','requires/get_pass_entry_controller','');
			//set_button_status(0, permission, 'fnc_getpass_entry',1,1);
			release_freezing();
		}
	}
}
//openmypage_order


function openmypage_order()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();
	page_link='requires/get_pass_entry_controller.php?action=system_order&company='+company;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe',page_link,'Gate Out Id Popup', 'width=900px, height=350px, center=1, resize=0, scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var sysNumber=this.contentDoc.getElementById("hidden_order_number").value; // system number
		if(sysNumber!="")
		{
			//freeze_window(5);
			$("#txt_order").val(sysNumber);
		
			//release_freezing();
		}
	}
}
//Chalan or system id
function chalan_popup()
{
	if( form_validation('cbo_company_name*cbo_basis','Company Name*Basis')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();
	var basis= $("#cbo_basis").val();
	page_link='requires/get_pass_entry_controller.php?action=chalan_id_popup&company='+company+'&basis='+basis;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe',page_link,'Gate Out Id Popup', 'width=900px, height=450px, center=1, resize=0, scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var sysNumber=(this.contentDoc.getElementById("hidden_sys_number").value).split('_'); // system number
		
		if(sysNumber!="")
		{
			//freeze_window(5);
	
		
			$("#txt_chalan_no").val(sysNumber[1]);
			//reset_form('','','txt_item_description*txt_quantity*cbo_uom*txt_rate*txt_amount','','','');
			//get_php_form_data(sysNumber, "populate_master_from_data", "requires/get_pass_entry_controller" );
			show_list_view(sysNumber[0],'show_dtls_list_view','list_container','requires/get_pass_entry_controller','');
			
			release_freezing();
		}
	}
}

 function generate_report_file(data,action,page)
		{
			window.open("requires/get_pass_entry_controller.php?data=" + data+'&action='+action, true );
		}



function fnc_getpass_entry(operation)
{
	if(operation==4)
	{
		 var report_title=$( "div.form_caption" ).html();
		  generate_report_file( $('#cbo_company_name').val()+'*'+$('#txt_system_id').val()+'*'+report_title,'get_out_entry_print','requires/get_pass_entry_controller');
		 
		 
		// print_report( $('#cbo_company_name').val()+'*'+$('#txt_system_id').val()+'*'+report_title, "get_out_entry_print", "requires/get_pass_entry_controller" ) 
		 return;
	}
	else if(operation==0 || operation==1 || operation==2)
	{
		/*if( form_validation('cbo_company_name*cbo_basis*cbo_department_name*txt_sent_by*cbo_group*txt_sent_to*cbo_group*txt_receive_date*txt_start_hours*txt_start_minuties*txt_item_description*txt_quantity*cbo_uom*txt_rate','Company Name*Basis*Department*Sent By*Within Group*Sent To*Out Date*Out Time*Out Time*Item Description*Quantity*UOM*Rate')==false )
		{
			return;
		}	*/
		var dataString = "txt_system_id*cbo_company_name*cbo_basis*cbo_department_name*cbo_section*txt_sent_by*cbo_group*txt_sent_to*txt_rece_date*txt_start_hours*txt_start_minuties*cbo_returnable*cbo_delevery_as*txt_return_date*cbo_item_category*cbo_sample*txt_item_description*txt_quantity*cbo_uom*txt_rate*txt_amount*txt_order*txt_remarks*update_id*update_dtls_id*txt_attention*txt_chalan_no";//*cbo_basis*cbo_department_name*txt_sent_by*txt_sent_to*txt_attention*txt_challan_no*txt_start_hours*txt_start_minuties*txt_item_description*txt_quantity*cbo_uom*txt_rate*txt_amount*txt_remarks*update_id
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
		//alert(data);
		freeze_window(operation);
		http.open("POST","requires/get_pass_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_getpass_entry_reponse;
	}
}

function fnc_getpass_entry_reponse()
{	
	if(http.readyState == 4) 
	{
		//alert(http.responseText)
		var response=trim(http.responseText).split('**');
		if(response[0]==0)
		{
			show_msg(trim(response[0]));
		}
		else if(response[0]==1)
		{
			show_msg(trim(response[0]));
		}
		else if(response[0]==10 || response[0]==15)
		{
			show_msg(trim(response[0]));
			release_freezing();
			return;
		}
		else if(response[0]==20)
		{
			//alert(response[1]);
			release_freezing();
			return;
		}
		if(response[0]==0 || response[0]==1)
		{
		$("#txt_system_id").val(response[1]);
		$("#update_id").val(response[2]);
		show_list_view(response[2],'show_update_list_view','list_container','requires/get_pass_entry_controller','');
		disable_enable_fields('cbo_company_name*cbo_basis*txt_chalan_no*cbo_group*txt_sent_to*cbo_item_category*cbo_sample*txt_item_description*txt_quantity*txt_rate*txt_amount*txt_order',0);
		//reset_form('','','txt_item_description*cbo_uom*txt_quantity*txt_rate*txt_amount*txt_remarks','','','');
		}
		if(response[0]==2)
		{
			reset_form('getpass_1','list_container','cbo_item_category*cbo_sample*txt_item_description*cbo_uom*txt_quantity*txt_rate*txt_amount*txt_order*txt_remarks','','','');

		}
		//set_button_status(1, permission, 'fnc_getpass_entry',1,1);
		release_freezing();
 	}
}

//amount calculate
function fn_calculate_amount()
{
	var quantity = $("#txt_quantity").val();
	var rate = $("#txt_rate").val();
	var  amount=quantity*rate*1;
	$("#txt_amount").val(number_format_common(amount,"","",7));
}

function fnc_move_cursor(val,id, field_id,lnth,max_val)
{
	var str_length=val.length;
	if(str_length==lnth)
	{
		$('#'+field_id).select();
		$('#'+field_id).focus();
	}
	
	if(val>max_val)
	{
		document.getElementById(id).value=max_val;
	}
}

function fnResetForm()
{
	reset_form('getpass_1','list_container','','','','');
}

</script>
<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
      <? echo load_freeze_divs ("../",$permission);  ?>
      <form name="getpass_1" id="getpass_1"  autocomplete="off">
    <div style="width:90%;">
    <fieldset style="width:1000px; flot:left">
    <legend>Get Pass</legend>
    <fieldset style="width:900px;">
        <table width="880" cellpadding="0" cellspacing="2" id="tbl_master">
            <tr>
                <td colspan="6" align="center"><b>Gate Pass ID</b>
                    <input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:155px" placeholder="Double Click To Search" 				onDblClick="openmypage_system();" readonly />
                </td>
            </tr>
            <tr>
                <td width="120" align="right" class="must_entry_caption">Company Name</td>
                <td width="150">
					<? 
                   	 echo create_drop_down( "cbo_company_name", 150, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0  order by company_name","id,company_name", 1, "-- Select Company --", $selected,"","0" );
					?>
                </td>
                <td width="120" align="right" class="must_entry_caption">Basis</td>
                <td width="150" >
                	<? 
					$get_pass_basis=array(1=>"Independent",2=>"Challan(Yarn)",3=>"Challan(Gray Fabric)",4=>"Challan(Finish Fabric)",5=>"Challan(General Item)",6=>"Challan(Trims)",6=>"Challan(Dyes & Chemical)",7=>"Challan(Trims)");
						echo create_drop_down( "cbo_basis", 150, $get_pass_basis,"",1, "-- Select --", 0, "load_drop_down( 'requires/get_pass_entry_controller', this.value, 'load_drop_down_chalan', 'chalan_td');" ); 
					?> 
                </td>
                <td width="140" align="right" class="must_entry_caption">Ststem ID/Challan No</td>
                <td width="140" id="chalan_td">
					<input type="text" name="txt_chalan_no" id="txt_chalan_no" class="text_boxes" style="width:140px;" placeholder="Browse Or Scane" onClick="chalan_popup()">
                </td>
            </tr>
              <tr>
                <td width="120" align="right" class="must_entry_caption">Department</td>
                <td width="150">
					<? 
					
                   	 echo create_drop_down( "cbo_department_name", 150, "select id,department_name from  lib_department  where status_active=1 and is_deleted=0  order by department_name","id,department_name", 1, "-- Select Department --", $selected,"load_drop_down( 'requires/get_pass_entry_controller', this.value, 'load_drop_down_section', 'section_td');","0" );
					?>
                </td>
                <td width="120" align="right">Section</td>
                <td width="150" id="section_td">
                	<? 
						echo create_drop_down( "cbo_section", 150, "select id,section_name from  lib_section where status_active=1 order by section_name","id,section_name",1, "-- Select --", 0, "" ); 
					?> 
                </td>
               <td align="right" class="must_entry_caption">Sent By</td>
                <td >
                    <input type="text" name="txt_sent_by" id="txt_sent_by" class="text_boxes" style="width:140px;">
                </td>
            </tr>
            <tr>
                <td align="right" class="must_entry_caption">Within Group</td>
                <td >
                    <? 
						echo create_drop_down( "cbo_group", 150, $yes_no,"", 1, "-- Select Group --", "", "load_drop_down( 'requires/get_pass_entry_controller', this.value, 'load_drop_down_sent', 'sent_td');",0 );
					?>
                </td>
                <td  align="right" class="must_entry_caption">Sent To</td>
                <td id="sent_td">
                	<input type="text" name="txt_sent_to" id="txt_sent_to" class="text_boxes" style="width:140px;">
                </td>
                <td align="right" class="must_entry_caption">Attention</td>
                <td >
                   
                	<input type="text" name="txt_attention" id="txt_attention" class="text_boxes" style="width:140px;"  />
                </td>
            </tr>
            <tr>
                <td align="right" class="must_entry_caption">Out Date</td>
                <td >
                	
                     <input class="datepicker" type="text" style="width:130px;" name="txt_rece_date" id="txt_rece_date"  placeholder="Select Date" />
                </td>
                <td align="right" class="must_entry_caption">Out-Time</td>
                <td>
                	<input type="text" name="txt_start_hours" id="txt_start_hours" class="text_boxes_numeric" placeholder="Hours" style="width:60px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_hours','txt_start_minuties',2,23);" />
                    <input type="text" name="txt_start_minuties" id="txt_start_minuties" class="text_boxes_numeric" placeholder="Minutes" style="width:60px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_minuties','txt_start_date',2,59)" />
                </td>
                <td align="right">Returnable</td>
                <td>
               		<? 
						echo create_drop_down( "cbo_returnable", 100, $yes_no,"", 1, "-- Select  --", "2", "",0 );
					?>
                </td>
               
            </tr>
            <tr>
                <td align="right" >Delivery As</td>
                <td>
                  <?
				  $basis_arr=array(1=>"Short",2=>"Additional",3=>"Sample");
				  echo create_drop_down( "cbo_delevery_as", 150, $basis_arr,"", 1, "-- Select  --", 0, "",0 );
				  
				  ?>
                </td>
                 <td align="right" class="must_entry_caption">Est. Return Date</td>
                <td >
                	
                     <input class="datepicker" type="text" style="width:130px;" name="txt_return_date" id="txt_return_date"  placeholder="Select Date" />
                </td>
                <td align="right"> 
                  
                   </td>
                <td >
                	
                </td>
            </tr>
        </table>
    </fieldset>
    <br>
       
    <fieldset style="1000px;">
    <legend>Item Part</legend>
        <table width="980" cellpadding="0" cellspacing="2" border="0" class="rpt_table" align="center">
            <thead>
                <th width="140" align="center" class="must_entry_caption">Item Category</th>
                 <th width="120" align="center" >Sample</th>
                 <th width="200" align="center" class="must_entry_caption">Item Description</th>
                <th width="60" align="center" class="must_entry_caption">Quantity</th>
                <th width="60" align="center" class="must_entry_caption">UOM</th>
                <th width="70" align="center" class="must_entry_caption">Rate</th>
                <th width="80" align="center">Amount</th>
                <th width="80" align="center">Buyer Order</th>
                <th width="150" align="center">Rmarks</th>
            </thead>
            <tbody>
                <tr>
                    <td>
						<? 
                            echo create_drop_down( "cbo_item_category", 140,$item_category,1, "-- Select --", 0, "" ); 
                        ?>
                    </td>
                    <td>
                        <? 
						echo create_drop_down( "cbo_sample", 120, "select id,sample_name from lib_sample where status_active=1 order by sample_name","id,sample_name",1, "-- Select --", 0, "" ); 
					    ?> 
                    </td>
                    <td><input type="text" name="txt_item_description" id="txt_item_description" class="text_boxes" style="width:200px;"></td>
                    <td><input type="text" name="txt_quantity" id="txt_quantity" class="text_boxes_numeric" onKeyUp="fn_calculate_amount()" style="width:60px;"></td>
                    <td><? echo create_drop_down( "cbo_uom", 60, $unit_of_measurement,"", 1, "-- Select--", $selected, "",0 ); ?></td>
                    <td><input type="text" name="txt_rate" id="txt_rate" class="text_boxes_numeric" onKeyUp="fn_calculate_amount()" style="width:70px"></td>
                    <td><input type="text" name="txt_amount" id="txt_amount" class="text_boxes_numeric" style="width:80px" randomly disabled></td>
                    <td><input type="text" name="txt_order" id="txt_order" class="text_boxes" style="width:80px" placeholder="Browse"  onDblClick="openmypage_order();" readonly></td>
                    <td><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:150px">
                        <input type="text" id="update_dtls_id" name="update_dtls_id" value="" />
                     </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
             
        <br>
        <table cellpadding="0" cellspacing="1" width="100%">
            <tr>
                <td align="center" colspan="7" valign="middle" class="button_container">
                    <input type="hidden" id="update_id" name="update_id" value="" />
						<? 
                            echo load_submit_buttons( $permission, "fnc_getpass_entry", 0,1,"fnResetForm()",1);
                        ?>
                </td>
            </tr> 
        </table> 
    </fieldset>
    <br>
    <fieldset style="width:870px;">
    <div style="width:950px;" id="list_container"></div>
    </fieldset>
    </div>
    </form>
    </div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>