-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_serial_no_details`
--

DROP TABLE IF EXISTS `inv_serial_no_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_serial_no_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recv_trans_id` int(11) NOT NULL DEFAULT '0',
  `rev_rtn_trans_id` int(11) NOT NULL,
  `issue_trans_id` int(11) NOT NULL DEFAULT '0',
  `issue_rtn_trans_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL DEFAULT '0',
  `serial_no` varchar(50) NOT NULL,
  `is_issued` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: No, 1: Yes',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `serial_qty` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `recv_trans_id` (`recv_trans_id`),
  KEY `issue_trans_id` (`issue_trans_id`),
  KEY `prod_id` (`prod_id`),
  KEY `serial_no` (`serial_no`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_serial_no_details`
--

LOCK TABLES `inv_serial_no_details` WRITE;
/*!40000 ALTER TABLE `inv_serial_no_details` DISABLE KEYS */;
INSERT INTO `inv_serial_no_details` VALUES (1,30,0,31,0,14,'2',1,14,'2015-01-31 02:36:58',0,'0000-00-00 00:00:00',1,0,1),(2,37,0,0,0,0,'inter',0,2,'2015-01-31 22:07:49',0,'0000-00-00 00:00:00',1,0,0);
/*!40000 ALTER TABLE `inv_serial_no_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
