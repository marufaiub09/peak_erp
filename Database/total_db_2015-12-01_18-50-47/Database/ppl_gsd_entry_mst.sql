-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppl_gsd_entry_mst`
--

DROP TABLE IF EXISTS `ppl_gsd_entry_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppl_gsd_entry_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` tinyint(3) NOT NULL,
  `po_dtls_id` int(11) NOT NULL,
  `po_job_no` varchar(30) NOT NULL,
  `po_break_down_id` int(11) NOT NULL,
  `gmts_item_id` int(11) NOT NULL DEFAULT '0',
  `working_hour` int(11) NOT NULL,
  `total_smv` double NOT NULL,
  `allowance` double NOT NULL,
  `sam_style` double NOT NULL,
  `operation_count` int(11) NOT NULL DEFAULT '0',
  `pitch_time` double NOT NULL,
  `man_power_1` int(11) NOT NULL DEFAULT '0',
  `man_power_2` int(11) NOT NULL DEFAULT '0',
  `per_hour_gmt_target` double NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) DEFAULT '1',
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppl_gsd_entry_mst`
--

LOCK TABLES `ppl_gsd_entry_mst` WRITE;
/*!40000 ALTER TABLE `ppl_gsd_entry_mst` DISABLE KEYS */;
/*!40000 ALTER TABLE `ppl_gsd_entry_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
