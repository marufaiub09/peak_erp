-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_mail_address`
--

DROP TABLE IF EXISTS `user_mail_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_mail_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` int(1) NOT NULL DEFAULT '1',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_mail_address`
--

LOCK TABLES `user_mail_address` WRITE;
/*!40000 ALTER TABLE `user_mail_address` DISABLE KEYS */;
INSERT INTO `user_mail_address` VALUES (1,3,'20','applemhd@pkapparels.net',2,'2015-02-18 12:42:15',1,'2015-09-10 09:56:52',1,0),(2,3,'1','rasel.mia@logicsoftbd.com',1,'2015-09-10 10:03:04',0,'0000-00-00 00:00:00',1,0),(3,3,'32','saeed@logicsoftbd.com',1,'2015-09-12 11:34:01',1,'2015-09-12 11:34:23',1,0),(4,3,'2','liton@pkapparels.net',1,'2015-09-12 18:11:35',0,'0000-00-00 00:00:00',1,0),(5,1,'Sazzad','sazzad@peakapparels.com',1,'2015-09-12 18:13:11',0,'0000-00-00 00:00:00',1,0),(6,1,'Nawfal','nawfal@pkapparels.net',1,'2015-09-12 18:14:03',0,'0000-00-00 00:00:00',1,0),(7,1,'Tuhin','tuhin@pkapparels.net',1,'2015-09-12 18:14:34',0,'0000-00-00 00:00:00',1,0),(8,1,'Sobuz','sobuz@pkapparels.net',1,'2015-09-12 18:15:07',0,'0000-00-00 00:00:00',1,0),(9,3,'28','mutobani@gmail.com',1,'2015-09-17 18:14:01',0,'0000-00-00 00:00:00',1,0),(10,3,'13','humayun@peakapparels.net',1,'2015-10-07 15:20:29',0,'0000-00-00 00:00:00',1,0),(11,1,'K. Ahmed','k.ahmad@pkapparels.net',1,'2015-10-11 15:34:30',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `user_mail_address` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
