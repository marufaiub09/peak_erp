-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_department`
--

DROP TABLE IF EXISTS `lib_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(64) NOT NULL,
  `division_id` int(11) NOT NULL DEFAULT '0',
  `contact_person` varchar(64) NOT NULL,
  `contact_no` varchar(64) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `website` varchar(64) NOT NULL,
  `email` varchar(32) NOT NULL,
  `short_name` varchar(5) NOT NULL,
  `address` varchar(500) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `department_name` (`department_name`),
  KEY `division_id` (`division_id`),
  KEY `contact_person` (`contact_person`),
  KEY `contact_no` (`contact_no`),
  KEY `country_id` (`country_id`),
  KEY `website` (`website`),
  KEY `email` (`email`),
  KEY `short_name` (`short_name`),
  KEY `address` (`address`(255)),
  KEY `remark` (`remark`(255)),
  KEY `inserted_by` (`inserted_by`),
  KEY `insert_date` (`insert_date`),
  KEY `updated_by` (`updated_by`),
  KEY `updated_by_2` (`updated_by`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_department`
--

LOCK TABLES `lib_department` WRITE;
/*!40000 ALTER TABLE `lib_department` DISABLE KEYS */;
INSERT INTO `lib_department` VALUES (1,'Admin',1,'','',0,'','','','','',2,'2015-02-03 05:06:57',0,'0000-00-00 00:00:00',1,0,0),(2,'Merchandising',2,'','',0,'','','','','',2,'2015-02-03 05:08:18',0,'0000-00-00 00:00:00',1,0,0),(3,'Knitting',6,'','',0,'','','','','',2,'2015-02-03 05:08:35',0,'0000-00-00 00:00:00',1,0,0),(4,'Cutting',3,'','',0,'','','','','',2,'2015-02-03 05:08:44',0,'0000-00-00 00:00:00',1,0,0),(5,'Sewing',4,'','',0,'','','','','',2,'2015-02-03 05:08:54',0,'0000-00-00 00:00:00',1,0,0),(6,'Finishing',5,'','',0,'','','','','',2,'2015-02-03 05:09:07',0,'0000-00-00 00:00:00',1,0,0),(7,'Accounts',7,'','',0,'','','','','',2,'2015-02-03 05:12:09',0,'0000-00-00 00:00:00',1,0,0),(8,'IT',8,'','',0,'','','','','',2,'2015-02-03 05:12:56',0,'0000-00-00 00:00:00',1,0,0),(9,'Dying',9,'','',0,'','','','','',2,'2015-02-03 05:13:06',0,'0000-00-00 00:00:00',1,0,0),(10,'Sample',10,'','',0,'','','','','',2,'2015-02-04 21:24:39',0,'0000-00-00 00:00:00',1,0,0),(11,'Store',11,'','',0,'','','','','',2,'2015-02-04 21:26:29',0,'0000-00-00 00:00:00',1,0,0),(12,'Maintanence',12,'','',0,'','','','','',2,'2015-08-29 09:44:43',0,'0000-00-00 00:00:00',1,0,0);
/*!40000 ALTER TABLE `lib_department` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
