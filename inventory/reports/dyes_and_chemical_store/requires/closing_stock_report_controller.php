<?php
error_reporting('0');
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
if ($action=="load_drop_down_store")
{
	//echo $data;
 $data=explode('_',$data);
 if($db_type==0)
     {
	  
	echo create_drop_down( "cbo_store_name", 120, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET(".$data[0].",company_id) and FIND_IN_SET(".$data[1].",item_category_id) order by store_name","id,store_name", 1, "--Select Store--", 1, "",0 );
	exit();
	 
	 }
 if($db_type==2 || $db_type==1)
     { $find_inset_item=" ',' || item_category_id || ',' LIKE '%,$data[1],%' "; $find_inset_company=" ',' || company_id || ',' LIKE '%,$data[0],%' "; 

	//echo "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and $find_inset_company and $find_inset_item order by store_name";die;
		echo create_drop_down( "cbo_store_name", 120, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and $find_inset_company and $find_inset_item order by store_name","id,store_name", 1, "--Select Store--", 1, "",0 );
		exit();
		
	 }
}

if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_account_id').val( id );
		$('#item_account_val').val( ddd );
	} 
	</script>
     <input type="hidden" id="item_account_id" />
     <input type="hidden" id="item_account_val" />
 <?php
		$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
		$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
		if ($data[2]==0) $item_name =""; else $item_name =" and item_group_id in($data[2])";
		
	$sql="SELECT id,item_account,item_category_id,item_group_id,item_description,supplier_id from  product_details_master where item_category_id='$data[1]' $item_name and  status_active=1 and is_deleted=0"; 
		$arr=array(1=>$item_category,2=>$itemgroupArr,4=>$supplierArr);
		echo  create_list_view("list_view", "Item Account,Item Category,Item Group,Item Description,Supplier,Product ID", "70,110,150,150,100,70","780","400",0, $sql , "js_set_value", "id,item_description", "", 0, "0,item_category_id,item_group_id,0,supplier_id,0", $arr , "item_account,item_category_id,item_group_id,item_description,supplier_id,id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
		exit();
}

if ($action=="item_group_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('item_name_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
        <input type="hidden" id="item_name_id" />
    <?php
	if ($data[1]==0) $item_category =""; else $item_category ="  item_category in($data[1])";
	$sql="SELECT id,item_name from  lib_item_group where status_active=1 and is_deleted=0 $item_category"; //id=$data[1] and
	
	echo  create_list_view("list_view", "Item Name", "350","500","330",0, $sql , "js_set_value", "id,item_name", "", 1, "0", $arr , "item_name", "periodical_purchase_report_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	if ($cbo_company_name==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_name'";
	if ($cbo_item_category_id==0) $item_category_id=" and b.item_category_id in(5,6,7)"; else $item_category_id=" and b.item_category_id='$cbo_item_category_id'";
	if ($item_account_id==0) $item_account=""; else $item_account=" and b.id in ($item_account_id)";
	if ($item_group_id==0) $group_id=""; else $group_id=" and b.item_group_id='$item_group_id'";
	if ($cbo_store_name==0){ $store_id="";}else{$store_id=" and b.store_id='$cbo_store_name'";}

	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name"); 
	$storeArr = return_library_array("select id,store_name from lib_store_location where status_active=1 and is_deleted=0","id","store_name"); 
	$itemnameArr = return_library_array("select id,item_name from lib_item_creation where status_active=1 and is_deleted=0","id","item_name");
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	
	if($db_type==0) 
	{
		$from_date=change_date_format($from_date,'yyyy-mm-dd');
		$to_date=change_date_format($to_date,'yyyy-mm-dd');
	}
	else if($db_type==2) 
	{
		$from_date=change_date_format($from_date,'','',1);
		$to_date=change_date_format($to_date,'','',1);
	}
	else  
	{
		$from_date=""; $to_date="";
	}
	$search_cond="";
	if($value_with==0) $search_cond =""; else $search_cond= "  and b.current_stock>0";
	
	/*if($db_type==0)
	{
	$sql="Select a.prod_id,b.avg_rate_per_unit,b.id,b.store_id,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description,b.item_size,b.unit_of_measure,
		sum(case when a.transaction_date<'".change_date_format($from_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as purchase,
		sum(case when a.transaction_type=4 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as issue_return
		
		
		from inv_transaction a, product_details_master b, inv_receive_master c where a.prod_id=b.id and a.mst_id=c.id and a.transaction_type in (1,2,3,4) and b.item_category_id in (5,6,7) and  a.company_id=c.company_id and c.entry_form in (4,29) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $company_id $item_category_id $group_id $store_id $item_account group by a.prod_id  order by a.prod_id,b.store_id, b.item_category_id, b.item_group_id ASC";
	}
if($db_type==2)
	{
	$sql="Select a.prod_id,b.avg_rate_per_unit,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description,b.item_size,b.unit_of_measure,
		sum(case when a.transaction_date<'".change_date_format($from_date,'','',1)."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date  between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as purchase,
		sum(case when a.transaction_type=4 and a.transaction_date  between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as issue_return
		
		from inv_transaction a, product_details_master b, inv_receive_master c where a.prod_id=b.id and a.mst_id=c.id and a.transaction_type in (1,2,3,4) and b.item_category_id in (5,6,7) and  a.company_id=c.company_id and c.entry_form in (4,29) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $company_id $item_category_id $group_id $store_id $item_account group by a.prod_id,b.store_id,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description,b.item_size,b.unit_of_measure,b.avg_rate_per_unit order by a.prod_id, b.item_category_id,b.item_group_id ASC";
	}*/
	
		$sql=("Select b.id as prod_id,
		sum(case when a.transaction_type in(1,4) and a.transaction_date<'".$from_date."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type in(2,3) and a.transaction_date<'".$from_date."' then a.cons_quantity else 0 end) as iss_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as purchase,
		sum(case when a.transaction_type=2 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as issue,
		sum(case when a.transaction_type in(4) and a.item_category in (5,6,7) and b.item_category_id in (5,6,7)  and a.transaction_date between '".$from_date."'  and '".$to_date."' then a.cons_quantity else 0 end) as issue_return,
		sum(case when a.transaction_type in(3) and a.item_category in (5,6,7) and b.item_category_id in (5,6,7)  and a.transaction_date between '".$from_date."'  and '".$to_date."' then a.cons_quantity else 0 end) as receive_return
		from inv_transaction a, product_details_master b
		where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1  and  b.is_deleted=0 and a.transaction_type in (1,2,3,4) and b.item_category_id in (5,6,7) and  a.item_category in (5,6,7) and a.order_id=0 $company_id $group_id $prod_cond $store_id $item_account $search_cond  group by b.id order by b.id ASC");	
	//echo $sql;
	$trnasactionData = sql_select($sql);
	$data_array=array();
	foreach($trnasactionData as $row_p)
	{
		$data_array[$row_p[csf("prod_id")]]['rcv_total_opening']=$row_p[csf("rcv_total_opening")];
		$data_array[$row_p[csf("prod_id")]]['iss_total_opening']=$row_p[csf("iss_total_opening")];
		$data_array[$row_p[csf("prod_id")]]['purchase']=$row_p[csf("purchase")];
		$data_array[$row_p[csf("prod_id")]]['issue']=$row_p[csf("issue")];
		$data_array[$row_p[csf("prod_id")]]['issue_return']=$row_p[csf("issue_return")];
		$data_array[$row_p[csf("prod_id")]]['receive_return']=$row_p[csf("receive_return")];
	} //var_dump($data_array);die;
	//echo $data_array[333]['receive_return'].Fuad;
	$returnRes_date="select prod_id, min(transaction_date) as min_date, max(transaction_date) as max_date from inv_transaction where is_deleted=0 and status_active=1 and item_category in(5,6,7) group by prod_id";
	$result_returnRes_date = sql_select($returnRes_date);
	foreach($result_returnRes_date as $row)	
	{
		$date_array[$row[csf("prod_id")]]['min_date']=$row[csf("min_date")];
		$date_array[$row[csf("prod_id")]]['max_date']=$row[csf("max_date")];
	}
	$i=1;
	ob_start();	
	?>
	<div id="scroll_body" align="center" style="height:auto; width:auto; margin:0 auto; padding:0;">
        <table width="1780px" cellpadding="0" cellspacing="0" id="caption" align="center">
            <thead>
                <tr style="border:none;">
                    <td colspan="20" align="center" class="form_caption" style="border:none;font-size:16px; font-weight:bold" ><?php echo $report_title; ?></td> 
                </tr>
                <tr style="border:none;">
                    <td colspan="20" class="form_caption" align="center" style="border:none; font-size:14px;">
                       <b>Company Name : <?php echo $companyArr[$cbo_company_name]; ?></b>                               
                    </td>
                </tr>
                <tr style="border:none;">
                    <td colspan="20" align="center" class="form_caption" style="border:none;font-size:12px; font-weight:bold">
                        <?php if($from_date!="" || $to_date!="")echo "From : ".change_date_format($from_date,'dd-mm-yyyy')." To : ".change_date_format($to_date,'dd-mm-yyyy')."" ;?>
                    </td>
                </tr>
            </thead>
        </table>
        <table style="width:1780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" id="caption" >
        	<thead>
                <tr>
                    <th rowspan="2" width="40">SL</th>
                    <th colspan="7">Description</th>
                    <th rowspan="2" width="110">Opening Stock</th>
                    <th colspan="3">Receive</th>
                    <th colspan="3">Issue</th>
                    <th rowspan="2" width="100">Closing Stock</th>
                    <th rowspan="2" width="80">Avg. Rate</th>
                    <th rowspan="2" width="120">Stock Value</th>
                    <th rowspan="2" width="80">Age(Days)</th>
                    <th rowspan="2" width="">DOH</th>
                </tr> 
                <tr>                         
                    <th width="65">Prod. ID</th>
                    <th width="100">Item Category</th>
                    <th width="100">Item Group</th>
                    <th width="100">Item Sub-group</th>
                    <th width="180">Item Description</th>
                    <th width="70">Item Size</th>
                    <th width="60">UoM</th>
                    <th width="80">Purchase</th>
                    <th width="80">Issue Return</th> 
                    <th width="100">Total Received</th>
                    <th width="80">Issue</th>
                    <th width="80">Receive Return</th>
                    <th width="100">Total Issue</th> 
                </tr> 
            </thead>
        </table>
        <div style="width:1800px; max-height:400px; overflow-y:scroll">
        <table style="width:1780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body_id">
        <?php
            
		 $sql="select b.id, b.item_category_id,b.item_group_id,b.unit_of_measure,b.item_description,b.sub_group_name, b.current_stock,avg_rate_per_unit from product_details_master b where b.status_active=1 and b.is_deleted=0 and b.company_id='$cbo_company_name' $item_category_id $group_id $prod_cond $store_id $item_account $search_cond order by b.id";
			$result = sql_select($sql);
			foreach($result as $row)
            {
                if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
                if( $row[csf("transaction_type")]==1 || $row[csf("transaction_type")]==2 || $row[csf("transaction_type")]==3 || $row[csf("transaction_type")]==4 ) 
                    $stylecolor='style="color:#A61000"';
                else
                    $stylecolor='style="color:#000000"';
				$ageOfDays = datediff("d",$date_array[$row[csf("id")]]['min_date'],date("Y-m-d"));
				$daysOnHand = datediff("d",$date_array[$row[csf("id")]]['max_date'],date("Y-m-d")); 

               /* $issue_sql = "select
                        sum(case when a.transaction_date<'".change_date_format($from_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as issue_total_opening,
						sum(case when a.transaction_type=2 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as issue,
						sum(case when a.transaction_type=3 and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as receive_return
						from inv_transaction a, inv_issue_master c
                        where a.mst_id=c.id and a.transaction_type in (2,3) and a.prod_id=".$row[csf("prod_id")]." and a.item_category in (5,6,7) and c.entry_form in (5,28) and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0";
			if($db_type==2)
				{
				  $issue_sql = "select
                        sum(case when a.transaction_date<'".change_date_format($from_date,'','',1)."' then a.cons_quantity else 0 end) as issue_total_opening,
						sum(case when a.transaction_type=2 and a.transaction_date  between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as issue,
						sum(case when a.transaction_type=3 and a.transaction_date  between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as receive_return
						from inv_transaction a, inv_issue_master c
                        where a.mst_id=c.id and a.transaction_type in (2,3) and a.prod_id=".$row[csf("prod_id")]." and a.item_category in (5,6,7) and c.entry_form in (5,28) and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0";	
				}*/
				//echo $issue_sql;
				//$issue_result = sql_select($issue_sql);
				//$data_array[$row_p[csf("prod_id")]]['issue']
				
				//echo $row[csf("id")];
				//echo $issue_qty;
				//$totalIssue = $issue_result[0][csf('issue')]+$issue_result[0][csf('receive_return')];
				//$totalIssue = $issue_result[0][csf('issue')]+$data_array[$row[csf("id")]]['receive_return'];
				
				//$data_array[$row[csf("id")]]['receive_return'];
				  //$opening=$data_array[$row[csf("id")]]['rcv_total_opening']-$data_array[$row[csf("id")]]['iss_total_opening'];
				//echo $row[csf("id")];
				//echo $data_array[$row[csf("id")]]['purchase'];
				//echo $get_upto;die;
				$issue_qty=$data_array[$row[csf("id")]]['issue'];
				$openingBalance = $data_array[$row[csf("id")]]['rcv_total_opening']-$data_array[$row[csf("id")]]['iss_total_opening'];
				$totalReceive = $data_array[$row[csf("id")]]['purchase']+$data_array[$row[csf("id")]]['issue_return'];//+$openingBalance
				$totalIssue = $data_array[$row[csf("id")]]['issue']+$data_array[$row[csf("id")]]['receive_return'];
				$closingStock=$openingBalance+$totalReceive-$totalIssue;
				//$closingStock=$opening+$totalReceive-$totalIssue;
				if((($get_upto==1 && $ageOfDays>$txt_days) || ($get_upto==2 && $ageOfDays<$txt_days) || ($get_upto==3 && $ageOfDays>=$txt_days) || ($get_upto==4 && $ageOfDays<=$txt_days) || ($get_upto==5 && $ageOfDays==$txt_days) || $get_upto==0) && (($get_upto_qnty==1 && $closingStock>$txt_qnty) || ($get_upto_qnty==2 && $closingStock<$txt_qnty) || ($get_upto_qnty==3 && $closingStock>=$txt_qnty) || ($get_upto_qnty==4 && $closingStock<=$txt_qnty) || ($get_upto_qnty==5 && $closingStock==$txt_qnty) || $get_upto_qnty==0))
					{
				$stockValue=$closingStock*$row[csf("avg_rate_per_unit")];
				$totalStockValue+=$stockValue;
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
						<td width="40"><?php echo $i; ?></td>								
						<td width="65"><?php echo $row[csf("id")]; ?></td>                                 
						<td width="100"><p><?php echo $item_category[$row[csf("item_category_id")]]; ?></p></td>
						<td width="100"><p><?php echo $itemgroupArr[$row[csf("item_group_id")]]; ?></p></td> 
						<td width="100"><p><?php echo $row[csf("sub_group_name")]; ?></p></td> 
						<td width="180"><p><?php echo $row[csf("item_description")]; ?></p></td> 
						<td width="70"><p><?php echo $row[csf("item_size")]; ?></p></td>
                        <td width="60" align="center"><p><?php echo $unit_of_measurement[$row[csf("unit_of_measure")]]; ?></p></td>  
						<td width="110" align="right"><p><?php echo number_format($openingBalance,3); $tot_open_bl+=$openingBalance; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($data_array[$row[csf("id")]]['purchase'],3); $tot_purchase+=$data_array[$row[csf("id")]]['purchase']; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($data_array[$row[csf("id")]]['issue_return'],3); $tot_issue_return+=$data_array[$row[csf("id")]]['issue_return'];//$row[csf("issue_return")]; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalReceive,3); $tot_total_receive+=$totalReceive; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($issue_qty,3); $tot_issue+=$issue_qty; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($data_array[$row[csf("id")]]['receive_return'],3); $tot_rec_return+=$data_array[$row[csf("id")]]['receive_return'];//$issue_result[0][csf('receive_return')]; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalIssue,3); $tot_total_issue+=$totalIssue; ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($closingStock,3); $tot_closing_stock+=$closingStock; ?></p></td>
                        <td width="80" align="right"><p><?php echo number_format($row[csf("avg_rate_per_unit")],2); //number_format($row[csf("avg_rate")],2); $tot_avg_rate+=$row[csf("avg_rate")]; ?></p></td>
                        <td width="120" align="right"><p><?php echo number_format($stockValue,2); $tot_stock_value+=$stockValue; ?></p></td>
                        <td width="80" align="center"><?php echo $ageOfDays; ?></td>
						<td width="" align="center"><?php echo $daysOnHand;//$daysOnHand; ?></td>
					</tr>
                <?php 												
					} $i++; 				
			}
		?>
        </table>
        <table style="width:1780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body_footer">
        	<tfoot>
            	<tr>
                	<th width="40">&nbsp;</th>
                	<th width="65">&nbsp;</th>
                	<th width="100">&nbsp;</th>
                	<th width="100">&nbsp;</th>
                	<th width="100">&nbsp;</th>
                	<th width="180">&nbsp;</th>
                	<th width="70">&nbsp;</th>
                	<th width="60">&nbsp;</th>
                	<th width="110"><?php echo number_format($tot_open_bl,3); ?></th>
                	<th width="80"><?php echo number_format($tot_purchase,3); ?></th>
                    <th width="80"><?php echo number_format($tot_issue_return,3); ?></th>
                    <th width="100"><?php echo number_format($tot_total_receive,3); ?></th>
                    <th width="80"><?php echo number_format($tot_issue,3); ?></th>
                    <th width="80"><?php echo number_format($tot_rec_return,3); ?></th>
                    <th width="100"><?php echo number_format($tot_total_issue,3); ?></th>
                    <th width="100"><?php echo number_format($tot_closing_stock,3); ?></th>
                    <th width="80"><p><?php echo number_format($tot_stock_value/$tot_closing_stock,2); ?></p></th>
                    <th width="120"><p><?php echo number_format($tot_stock_value,2); ?></p></th>
                    <th width="80"><?php //echo number_format($tot_purchase,3); ?></th>
                    <th width="">&nbsp;</th>
                </tr>
            </tfoot>

        </table>
        </div>
        </div>
    <?php
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}
?>