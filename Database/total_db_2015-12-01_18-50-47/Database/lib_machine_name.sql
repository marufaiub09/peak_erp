-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_machine_name`
--

DROP TABLE IF EXISTS `lib_machine_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_machine_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `machine_no` varchar(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `machine_group` varchar(50) NOT NULL,
  `dia_width` int(11) NOT NULL DEFAULT '0',
  `gauge` varchar(15) NOT NULL,
  `extra_cylinder` varchar(100) NOT NULL,
  `no_of_feeder` varchar(50) NOT NULL,
  `attachment` varchar(100) NOT NULL,
  `prod_capacity` int(11) NOT NULL DEFAULT '0',
  `capacity_uom_id` int(11) NOT NULL DEFAULT '0',
  `brand` varchar(15) NOT NULL,
  `origin` varchar(50) NOT NULL,
  `purchase_date` date NOT NULL,
  `purchase_cost` double NOT NULL,
  `accumulated_dep` double NOT NULL,
  `depreciation_rate` double NOT NULL,
  `depreciation_method_id` int(11) NOT NULL,
  `remark` varchar(200) NOT NULL,
  `seq_no` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(4) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(4) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `location_id` (`location_id`),
  KEY `floor_id` (`floor_id`),
  KEY `machine_no` (`machine_no`),
  KEY `category_id` (`category_id`),
  KEY `machine_group` (`machine_group`),
  KEY `dia_width` (`dia_width`),
  KEY `gauge` (`gauge`),
  KEY `brand` (`brand`),
  KEY `purchase_date` (`purchase_date`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_machine_name`
--

LOCK TABLES `lib_machine_name` WRITE;
/*!40000 ALTER TABLE `lib_machine_name` DISABLE KEYS */;
INSERT INTO `lib_machine_name` VALUES (1,1,1,1,'1',1,'S/J',36,'24','20 Fleece','108','',350,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',1,2,'2015-01-07 03:14:24',1,'2015-05-04 10:24:04',1,0,0),(2,1,1,1,'2',1,'S/J',34,'24','','102','',350,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',2,2,'2015-01-07 03:16:50',1,'2015-05-04 10:20:56',1,0,0),(3,1,1,1,'3',1,'S/J',30,'24','','90','',320,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',3,2,'2015-01-07 03:17:17',1,'2015-05-04 10:21:12',1,0,0),(4,1,1,1,'4',1,'S/J',34,'24','','102','',350,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',4,2,'2015-01-07 03:17:50',1,'2015-05-04 10:21:28',1,0,0),(5,1,1,1,'5',1,'S/J',36,'24','20Fleece','108','',350,1,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',5,2,'2015-01-07 03:18:19',1,'2015-05-04 10:24:34',1,0,0),(6,1,1,1,'6',1,'Rib',38,'18','24 Inter Lock','76','',300,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',6,2,'2015-01-07 03:19:04',1,'2015-05-04 10:25:27',1,0,0),(7,1,1,1,'7',1,'S/J',40,'24','','120','',400,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',7,2,'2015-01-07 03:19:32',1,'2015-05-04 10:23:03',1,0,0),(8,1,1,1,'8',1,'S/J',38,'24','','114','',350,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',8,2,'2015-01-07 03:22:34',1,'2015-05-04 10:23:17',1,0,0),(9,1,1,1,'9',1,'Rib',42,'18','24 Inter Lock','84','',300,12,'Pailung','Taiyan','0000-00-00',0,0,0,1,'',9,2,'2015-01-07 03:22:58',1,'2015-05-04 10:25:57',1,0,0);
/*!40000 ALTER TABLE `lib_machine_name` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
