-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_fab_subprocess`
--

DROP TABLE IF EXISTS `pro_fab_subprocess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_fab_subprocess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `batch_id` int(11) NOT NULL DEFAULT '0',
  `batch_no` varchar(50) NOT NULL,
  `batch_ext_no` int(11) NOT NULL DEFAULT '0',
  `process_id` varchar(100) DEFAULT '0',
  `ltb_btb_id` int(11) NOT NULL DEFAULT '0',
  `water_flow_meter` varchar(200) NOT NULL,
  `process_end_date` date NOT NULL COMMENT 'Production Date',
  `process_start_date` date NOT NULL,
  `temparature` int(11) NOT NULL DEFAULT '0',
  `stretch` int(11) NOT NULL DEFAULT '0',
  `over_feed` int(11) NOT NULL DEFAULT '0',
  `feed_in` int(11) NOT NULL DEFAULT '0',
  `pinning` int(11) NOT NULL DEFAULT '0',
  `speed_min` int(11) NOT NULL DEFAULT '0',
  `start_minutes` double NOT NULL,
  `start_hours` double NOT NULL,
  `end_hours` double NOT NULL,
  `end_minutes` double NOT NULL,
  `machine_id` int(11) DEFAULT '0',
  `floor_id` int(11) NOT NULL DEFAULT '0',
  `load_unload_id` int(11) NOT NULL DEFAULT '0',
  `result` int(11) NOT NULL DEFAULT '0',
  `entry_form` tinyint(2) DEFAULT '0',
  `remarks` text NOT NULL,
  `multi_batch_load_id` int(3) NOT NULL,
  `shift_name` int(11) NOT NULL,
  `fabric_type` int(11) NOT NULL,
  `production_date` date NOT NULL COMMENT 'Process End Date',
  `chemical_name` varchar(100) NOT NULL,
  `length_shrinkage` varchar(100) NOT NULL,
  `width_shrinkage` varchar(100) NOT NULL,
  `spirality` varchar(100) NOT NULL,
  `width_dia_type` int(4) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `service_company` int(11) DEFAULT NULL,
  `received_chalan` varchar(100) DEFAULT NULL,
  `issue_chalan` varchar(100) DEFAULT NULL,
  `service_source` int(11) DEFAULT NULL,
  `issue_challan_mst_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_fab_subprocess`
--

LOCK TABLES `pro_fab_subprocess` WRITE;
/*!40000 ALTER TABLE `pro_fab_subprocess` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_fab_subprocess` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
