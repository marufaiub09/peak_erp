-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `subcon_outbound_bill_dtls`
--

DROP TABLE IF EXISTS `subcon_outbound_bill_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcon_outbound_bill_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `receive_id` int(11) NOT NULL DEFAULT '0',
  `receive_date` date NOT NULL,
  `challan_no` varchar(30) NOT NULL,
  `order_id` text NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `body_part_id` int(11) NOT NULL DEFAULT '0',
  `febric_description_id` int(11) NOT NULL DEFAULT '0',
  `embel_name` int(11) NOT NULL DEFAULT '0',
  `embel_type` int(11) NOT NULL DEFAULT '0',
  `roll_no` int(11) NOT NULL DEFAULT '0',
  `wo_num_id` int(11) NOT NULL DEFAULT '0',
  `receive_qty` double NOT NULL DEFAULT '0',
  `rec_qty_pcs` int(11) NOT NULL DEFAULT '0',
  `uom` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `remarks` varchar(150) NOT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `process_id` tinyint(3) NOT NULL DEFAULT '0',
  `prod_mst_id` int(11) NOT NULL DEFAULT '0',
  `inserted_by` tinyint(4) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` tinyint(4) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(3) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcon_outbound_bill_dtls`
--

LOCK TABLES `subcon_outbound_bill_dtls` WRITE;
/*!40000 ALTER TABLE `subcon_outbound_bill_dtls` DISABLE KEYS */;
/*!40000 ALTER TABLE `subcon_outbound_bill_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
