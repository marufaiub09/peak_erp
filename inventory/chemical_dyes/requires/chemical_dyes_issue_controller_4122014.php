<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');
$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$liquior_arr=return_library_array("select id,total_liquor from pro_recipe_entry_mst where status_active=1 and is_deleted=0" ,"id","total_liquor");
$rece_arr = return_library_array("select id,labdip_no from pro_recipe_entry_mst","id","labdip_no");

//--------------------------------------------------------------------------------------------
if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 170, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

if ($action=="load_drop_down_store")
{

		echo create_drop_down( "cbo_store_name", 170, "select a.id,a.store_name from lib_store_location a,lib_store_location_category  b where a.id=b.store_location_id and a.status_active=1 and a.is_deleted=0 and a.company_id=$data and b.category_type in (5,6,7) group by a.id,a.store_name order by a.store_name","id,store_name", 1, "-- Select Store --", 0, "",0 );  	 
	   exit();
}


if($action=="load_drop_down_purpose")
{ 
    $data=explode('_',$data);
	if($data[0]==13)
	{
      echo create_drop_down("cbo_sub_process", 170, $dyeing_sub_process,"", 1, "-- Select--", "", "fnc_item_details(this.value,'')");
	}
	else
	{
       echo create_drop_down("cbo_sub_process", 170, $dyeing_sub_process,"", 1, "-- Select--", "", "fnc_item_details(this.value,'')",'','','','',31 );
	}
	
}


if($action=="req_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
		var data=mrr.split("_")
 		$("#hidden_requ_no").val(data[0]); // mrr number
		$("#hidden_requ_id").val(data[1]); // mrr number
		$("#hidden_batch_id").val(data[2]); // mrr number
		$("#hidden_receipe_id").val(data[3]); // mrr number


		parent.emailwindow.hide();
	}
</script>

</head>
<input type="hidden" id="hidden_requ_no"/>
<input type="hidden" id="hidden_requ_id"/>
<input type="hidden" id="hidden_batch_id"/>
<input type="hidden" id="hidden_receipe_id"/>
<body>
	<div align="center" style="width:100%;" >
	<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="750" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                <thead>
                    <tr>                	 
                        <th width="180">Requisition No</th>
                        <th width="200">Batch No</th>
                        <th width="220">Date Range</th>
                        <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                    </tr>
                </thead>
                <tbody>
                    <tr>                    
                        <td align="center">
                            <input type="text" id="txt_req_no" name="" class="text_boxes" style="width:150px;" >
                        </td>
                        <td align="center">				
                            <input type="text" style="width:180px" class="text_boxes"  name="txt_batch_no" id="txt_batch_no" />	
                        </td>    
                        <td align="center">
                            <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date" />&nbsp;&nbsp;&nbsp;
                            <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date" />
                        </td> 
                        <td align="center">
                            <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_req_no').value+'_'+document.getElementById('txt_batch_no').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_requisition_list_view', 'search_div', 'chemical_dyes_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                        </td>
                </tr>
                <tr>                  
                    <td align="center" height="40" valign="middle" colspan="5">
                        <?php echo load_month_buttons(1);  ?>
                        <!-- Hidden field here-------->
                         <input type="hidden" id="hidden_return_number" value="" />
                        <!-- ---------END------------->
                    </td>
                </tr>    
                </tbody>
            </table>    
        <div align="center" style="margin-top:10px" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_requisition_list_view")
{
	$data=explode("_",$data);
	$req_no=str_replace("'","",$data[0]);
	$batch_no=str_replace("'","",$data[1]);
	$date_form=str_replace("'","",$data[2]);
	$date_to=str_replace("'","",$data[3]);
	$company_id=str_replace("'","",$data[4]);
	
	
	
	//if()
	
	if($req_no!="") $requisition_cond="and requ_no='$req_no'" ; else $requisition_cond="" ;
	if($batch_no!="") $batch_id=return_field_value("id as batch_id","pro_batch_create_mst","batch_no='".$batch_no."'","batch_id");
	if($batch_id>0) $batch_cond="and batch_id='$batch_id'"; else $batch_cond="";
	if($date_form!="" && $date_toz1="")
	{
		if($db_type==0) 
		{
			$date_form=change_date_format($date_form,"yyyy-mm-dd");
			$date_to=change_date_format($date_to,"yyyy-mm-dd");
		} 
		else 
		{
			$date_form=change_date_format($date_form,"yyyy-mm-dd","",-1);
			$date_to=change_date_format($date_to,"yyyy-mm-dd","",-1);
		}
		
		$date_cond="and requisition_date between $date_form and $date_to"; 
	
	}
	else
	{
		 $date_cond="";
	}

    $batch_arr = return_library_array("select id,batch_no from pro_batch_create_mst where company_id=$company_id and batch_against<>0 ","id","batch_no");
    $company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$arr=array(1=>$company_arr,3=>$receive_basis_arr,4=>$batch_arr,5=>$receipe_arr);
	$sql = "select requ_no,company_id,requisition_date,requisition_basis,batch_id,recipe_id,id from dyes_chem_issue_requ_mst where company_id=$company_id $requisition_cond $batch_cond $date_cond";
	//echo $sql;
	echo create_list_view("list_view", "Requisition No, Company, Requisition Date, Requisition Basis, Batch No, Recipe No","140,140,120,120,120","900","260",0, $sql , "js_set_value", "requ_no,id,batch_id,recipe_id", "", 1, "0,company_id,0,requisition_basis,batch_id,recipe_id", $arr, "requ_no,company_id,requisition_date,requisition_basis,batch_id,recipe_id", "",'','0,0,3,0,0,0') ;
}


if($action=="batch_popup")
{
  	echo load_html_head_contents("Batch Info","../../../", 1, 1, '','1','');
	extract($_REQUEST);
?>
     
	<script>
	
	function js_set_value( data)
	{
		var data=data.split("_");
		document.getElementById('hidden_batch_id').value=data[0];
		document.getElementById('hidden_batch_no').value=data[1];
		document.getElementById('hidden_batch_weight').value=data[2];
		document.getElementById('hidden_total_liquor').value=data[3];
		parent.emailwindow.hide();
	}
	
    </script>

</head>

<body>
<div align="center">

	<fieldset style="width:850px;margin-left:10px">
        <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
            <table cellpadding="0" cellspacing="0" width="500" class="rpt_table">
                <thead>
                    <th>Search By</th>
                    <th>Search</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" value="">
                        <input type="hidden" name="hidden_batch_no" id="hidden_batch_no" value="">
                         <input type="hidden" name="hidden_batch_weight" id="hidden_batch_weight" value="">
                        <input type="hidden" name="hidden_total_liquor" id="hidden_total_liquor" value="">
                    </th> 
                </thead>
                <tr class="general">
                    <td align="center">	
                        <?php
                            $search_by_arr=array(1=>"Batch No",2=>"Booking No");
                            echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>                 
                    <td align="center">				
                        <input type="text" style="width:140px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 						
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<?php echo $cbo_company_id; ?>, 'create_batch_search_list_view', 'search_div', 'chemical_dyes_issue_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
            <div id="search_div" style="margin-top:10px"></div>   
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_batch_search_list_view")
{
	$data=explode('_',$data);
	
	$search_string="%".trim($data[0])."%";
	$search_by =$data[1];
	$company_id =$data[2];
	
	if($search_by==1)
		$search_field='batch_no';
	else
		$search_field='booking_no';
		
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');

	$arr=array (5=>$batch_against,6=>$batch_for,8=>$color_arr);
	
	$sql = "select id, batch_no, extention_no, batch_weight, total_liquor, batch_date, batch_against, batch_for, booking_no, color_id from pro_batch_create_mst where company_id=$company_id and $search_field like '$search_string' and status_active=1 and is_deleted=0 and batch_against<>0"; 
		 
	echo  create_list_view("list_view", "Batch No,Ext. No,Batch Weight,Total Liquor, Batch Date,Batch Against,Batch For, Booking No, Color", "100,70,80,80,80,80,85,105,80","840","250",0, $sql, "js_set_value", "id,batch_no,batch_weight,total_liquor", "", 1, "0,0,0,0,0,batch_against,batch_for,0,color_id", $arr, "batch_no,extention_no,batch_weight,total_liquor,batch_date,batch_against,batch_for,booking_no,color_id", "",'','0,0,2,2,3,0,0,0,0');
	
exit();	
}

if($action=="item_details")
{
	
	$data=explode("**",$data);
	$company_id=$data[0];
	$sub_process_id=$data[1];
	$receipe_id=$data[2];
	$issue_basis=$data[5];
	$is_update=$data[6];
	$req_id=$data[7];
	$cbo_store=$data[8];
  
	//echo $req_id;die;
	$item_group_arr=return_library_array( "select id, item_name from lib_item_group",'id','item_name');
	?>
	<div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1210" class="rpt_table" >
            <thead>
                <th width="30">SL</th>
                <th width="120">Store Name</th>
                <th width="50">Product ID</th>
                <th width="100">Item Cat.</th>
                <th width="100">Group</th>
                <th width="100">Sub Group</th>
                <th width="140">Item Description</th>
                <th width="32">UOM</th>
                <th width="100">Dose Base</th>
                <th width="50">Ratio</th>
                <th width="70">Stock Qty</th>
                <th width="73">Recipe Qnty.</th>
                <th width="30">Adj%.</th>
                <th width="90">Adj. Type</th>
                <th width="">Issue Qnty.</th>
            </thead>
        </table>
        <div style="width:1210px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1192" class="rpt_table" id="tbl_list_search">
                <tbody>
                <?php
                if($is_update=="")
                {
					if($issue_basis==4)
					{
						$sql="select id,item_category_id, item_group_id, sub_group_name, item_description, item_size, unit_of_measure,current_stock from product_details_master where company_id='$company_id' and item_category_id in(5,6,7) and status_active=1 and is_deleted=0 order by item_category_id ";	
						//echo $sql;die;
						$i=1;
						
						$nameArray=sql_select( $sql );
						foreach ($nameArray as $selectResult)
						{
							
							$totalIssued = return_field_value("sum(b.cons_quantity) as to_issue","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=2 group by prod_id","to_issue");
	                        $totalIssuedReturn = return_field_value("sum(b.cons_quantity+b.cons_reject_qnty) as to_issue_return","inv_receive_master a, inv_transaction b"," a.id=b.mst_id and b.prod_id='".$row[csf("prod_id")]."' and b.item_category  in(5,6,7) and b.transaction_type=4 group by prod_id","to_issue_return");
	                        $issue_remain=$totalIssued-$totalIssuedReturn;
							
							if ($i%2==0)  
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>">
                            <td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td> 
                            <td width="120" id="store_td" >
							<?php 
                            if(str_replace("'","",$cbo_store)!=0)
                                  { 
                                
                                    echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --", $cbo_store, "",0 ); 
                                  }
                            else
                                  {
                                        echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --",0, "",0 ); 	
                                  }
                            ?>
                            </td>
							<td width="50" id="product_id_<?php echo $i; ?>"><?php echo $selectResult[csf('id')]; ?>
							<input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('id')]; ?>">
							</td>
							<td  width="100"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p>
							<input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('item_category_id')]; ?>">
							</td>
							<td width="100" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$selectResult[csf('item_group_id')]]; ?></p> &nbsp;</td>
							<td width="100" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $selectResult[csf('sub_group_name')]; ?></p></td>
							<td width="140" id="item_description_<?php echo $i; ?>"><p><?php echo $selectResult[csf('item_description')]." ".$selectResult[csf('item_size')]; ?></p></td> 
							<td width="32" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$selectResult[csf('unit_of_measure')]]; ?></td>
							<td width="100" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 100, $dose_base, "", 1, "- Select Dose Base -",$selectResult[csf('dose_base')],"calculate_receipe_qty($i)",1); ?></td>
							<td width="50" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo def_number_format($ratio,5,""); ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" disabled></td>             
                            <td width="70" align="center" id="stock_qty_<?php echo $i; ?>"><input type="text" name="stock_qty[]" id="stock_qty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo $selectResult[csf('current_stock')]; ?>"  disabled></td>
		
        					<td width="70" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo def_number_format($recipe_qnty,5,""); ?>" disabled></td>
							<td width="30" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:28px"  value="<?php //echo $ratio; ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" disabled></td>
							<td width="90" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 80, $increase_decrease, "", 1, "- Select -","","calculate_receipe_qty($i)",1); ?>
                            
                            <input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="<?php echo $selectResult[csf('dtls_id')]; ?>">	
							<input type="hidden" name="transId[]" id="transId_<?php echo $i; ?>" value="<?php echo $selectResult[csf('trans_id')]; ?>">	
                            <input type="hidden" name="stock_check[]" id="stock_check_<?php echo $i; ?>" value="<?php echo $selectResult[csf('current_stock')]; ?>">
                            </td>
   
							<td width="" align="center" id="reqn_qnty_<?php echo $i; ?>">
							<input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($recipe_qnty,5,""); ?>" readonly>
							<input type="text" name="reqn_qnty_edit[]" id="txt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%" onKeyUp="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)"  value="<?php //echo def_number_format($recipe_qnty,5,""); ?>" >
							<input type="hidden" name="hidreqn_qnty_edit[]" id="hidtxt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('req_qny_edit')],5,""); ?>" / >
							
							
							</td>
							
							</tr>
							<?php
							$i++;
						}
					}
					if($issue_basis==5)
					{
						$sql="select id,item_category_id, item_group_id, sub_group_name, item_description, item_size, unit_of_measure,current_stock from product_details_master where company_id='$company_id' and item_category_id in(5,6,7) and status_active=1 and is_deleted=0 order by item_category_id ";	
						$i=1;
						$nameArray=sql_select( $sql );
						foreach ($nameArray as $selectResult)
						{
							if ($i%2==0)  
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>"> 
							<td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td>
                            <td width="120" id="store_td" >
								<?php 
                                 if(str_replace("'","",$cbo_store)!=0)
								      { 
									
                                       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --", $cbo_store, "",0 ); 
								      }
							     else
									  {
									       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --",0, "",0 ); 	
									  }
                                ?>
                            </td>	
							<td width="50" id="product_id_<?php echo $i; ?>"><?php echo $selectResult[csf('id')]; ?>
							<input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('id')]; ?>">
							</td>
							<td  width="100"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p>
							<input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('item_category_id')]; ?>">
							</td>
							<td width="100" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$selectResult[csf('item_group_id')]]; ?></p> &nbsp;</td>
							<td width="100" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $selectResult[csf('sub_group_name')]; ?></p></td>
							<td width="140" id="item_description_<?php echo $i; ?>"><p><?php echo $selectResult[csf('item_description')]." ".$selectResult[csf('item_size')]; ?></p></td> 
							<td width="32" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$selectResult[csf('unit_of_measure')]]; ?></td>
							<td width="100" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 150, $dose_base, "", 1, "- Select Dose Base -",$selectResult[csf('dose_base')],"calculate_receipe_qty($i)"); ?></td>
							<td width="50" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php //echo def_number_format($ratio,5,""); ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)"/></td>
                            <td width="70" align="center" id="stock_qty_<?php echo $i; ?>"><input type="text" name="stock_qty[]" id="stock_qty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo $selectResult[csf('current_stock')]; ?>"  disabled></td>
                            
							<td width="70" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php //echo def_number_format($recipe_qnty,5,""); ?>" onClick="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)"  readonly /></td>
							<td width="30" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:30px"  value="<?php //echo $ratio; ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" ></td>
							<td width="90" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 90, $increase_decrease, "", 1, "- Select -","","calculate_receipe_qty($i)"); ?></td>
							<td width="" align="center" id="reqn_qnty_<?php echo $i; ?>">
							<input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php //echo def_number_format($recipe_qnty,5,""); ?>" readonly>
							<input type="text" name="reqn_qnty_edit[]" id="txt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php //echo def_number_format($recipe_qnty,5,"");  ?>"  >
							<input type="hidden" name="hidreqn_qnty_edit[]" id="hidtxt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php //echo def_number_format($selectResult[csf('req_qny_edit')],5,""); ?>" / >
							
							<input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="<?php echo $selectResult[csf('dtls_id')]; ?>">	
							<input type="hidden" name="transId[]" id="transId_<?php echo $i; ?>" value="<?php echo $selectResult[csf('trans_id')]; ?>">	
							</td>
							
							</tr>
							<?php
							$i++;
						}
					}
					if($issue_basis==7)
					{
						
						
						
						$total_issue_sql=sql_select("select a.issue_number, b.product_id, b.req_qny_edit as req_qny_edit from inv_issue_master a, dyes_chem_issue_dtls b where a.id=b.mst_id and a.entry_form=5 and a.issue_basis=7 and a.req_no=$req_id and b.sub_process=$sub_process_id");
						//echo $total_issue_sql;
						foreach($total_issue_sql as $row)
						{
							$total_issue_arr[$row[csf("product_id")]]+=$row[csf("req_qny_edit")];
							$prev_issue_arr[$row[csf("issue_number")]]+=$row[csf("req_qny_edit")];
						}
						//var_dump($prev_issue_arr); die;
						$issue_total_val=$issue_num_all="";
						foreach($prev_issue_arr as $issue_no=>$val)
						{
							if($issue_total_val!="") $issue_total_val .=",";
							$issue_total_val .=$val;
							if($issue_num_all!="") $issue_num_all .=",";
							$issue_num_all .=$issue_no;
						}
						
						
						$sql="select a.id,a.item_category_id, a.item_group_id, a.sub_group_name, a.item_description, a.item_size, a.unit_of_measure,a.current_stock, b.id as dtls_id, b.dose_base, b.ratio,recipe_qnty,adjust_percent,adjust_type,required_qnty,req_qny_edit from product_details_master a , dyes_chem_issue_requ_dtls b where a.id=b.product_id and b.mst_id=$req_id and b.sub_process=$sub_process_id and b.status_active=1 and b.is_deleted=0 and a.company_id='$company_id' and a.item_category_id in(5,6,7) and a.status_active=1 and a.is_deleted=0 order by a.item_category_id ";
						//echo $sql."df";die;
						$is_disabled=1;
						$i=1;
						$nameArray=sql_select( $sql );
						foreach ($nameArray as $selectResult)
						{
							if ($i%2==0)  
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							$req_qnty=$selectResult[csf('req_qny_edit')]-$total_issue_arr[$selectResult[csf("id")]];
							//echo $total_issue_arr[70]."jahd";
							//echo $selectResult[csf("dtls_id")];
							
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>"> 
							<td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td>
                            <td width="120" id="store_td" >
								<?php 
                                if(str_replace("'","",$cbo_store)!=0)
								      { 
									
                                       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --", $cbo_store, "",0 ); 
								      }
							    else
									  {
									       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --",0, "",0 ); 	
									  }
                                ?>
                            </td>	
							<td width="50" id="product_id_<?php echo $i; ?>"><?php echo $selectResult[csf('id')]; ?>
							<input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('id')]; ?>">
							</td>
							<td  width="100"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p>
							<input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('item_category_id')]; ?>">
							</td>
							<td width="100" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$selectResult[csf('item_group_id')]]; ?></p> &nbsp;</td>
							<td width="100" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $selectResult[csf('sub_group_name')]; ?></p></td>
							<td width="140" id="item_description_<?php echo $i; ?>"><p><?php echo $selectResult[csf('item_description')]." ".$selectResult[csf('item_size')]; ?></p></td> 
							<td width="32" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$selectResult[csf('unit_of_measure')]]; ?></td>
							<td width="100" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 150, $dose_base, "", 1, "- Select Dose Base -",$selectResult[csf('dose_base')],"calculate_receipe_qty($i)",1); ?></td>
							<td width="50" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo def_number_format($selectResult[csf('ratio')],5,""); ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" readonly></td>
                            <td width="70" align="center" id="stock_qty_<?php echo $i; ?>"><input type="text" name="stock_qty[]" id="stock_qty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo $selectResult[csf('current_stock')]; ?>"  disabled></td>
							<td width="70" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo def_number_format($selectResult[csf('recipe_qnty')],5,""); ?>"  onClick="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)" readonly></td>
							<td width="30" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:27px"  value="<?php echo $selectResult[csf('adjust_percent')]; ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" readonly></td>
							<td width="90" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 90, $increase_decrease, "", 1, "- Select -", $selectResult[csf('adjust_type')],"calculate_receipe_qty($i)",1); ?></td>
							<td width="" align="center" id="reqn_qnty_<?php echo $i; ?>">
							<input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($req_qnty,5,""); ?>" readonly>
							<input type="text" name="reqn_qnty_edit[]" id="txt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($req_qnty,5,""); ?>"  / >
							<input type="hidden" name="hidreqn_qnty_edit[]" id="hidtxt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($req_qnty,5,""); ?>" readonly / >
							
							<input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="<?php echo $selectResult[csf('dtls_id')]; ?>">	
							<input type="hidden" name="transId[]" id="transId_<?php echo $i; ?>" value="<?php echo $selectResult[csf('trans_id')]; ?>">	
							</td>
							
							</tr>
							<?php
							$i++;
						}
						echo '<input type="hidden"  id="txt_isu_qnty" value="'.$issue_total_val.'" >';
						echo '<input type="hidden"  id="txt_isu_num" value="'.$issue_num_all.'" >';
					}
                }
                else
                {
					$sql="select a.id,t.store_id,a.item_category_id, a.item_group_id, a.sub_group_name, a.item_description, a.item_size, a.unit_of_measure,a.current_stock, b.id as dtls_id,b.trans_id, b.dose_base, b.ratio,recipe_qnty,adjust_percent,adjust_type,required_qnty,req_qny_edit from product_details_master a , dyes_chem_issue_dtls b,inv_transaction t where a.id=b.product_id and b.trans_id=t.id and b.mst_id=$is_update and b.sub_process=$sub_process_id and b.status_active=1 and b.is_deleted=0 and a.company_id='$company_id' and a.item_category_id in(5,6,7) and a.status_active=1 and a.is_deleted=0 order by a.item_category_id";
					//echo $sql."aa";die;
					$i=1;
					$nameArray=sql_select( $sql );
					foreach ($nameArray as $selectResult)
					{
						
						//echo $selectResult[csf('store_id')];
						if ($i%2==0)  
						$bgcolor="#E9F3FF";
						else
						$bgcolor="#FFFFFF";
						if($issue_basis==4)
						{
						?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>"> 
                            <td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td>
                            <td width="120" id="store_td" >
								<?php 
                                if(str_replace("'","",$selectResult[csf('store_id')])!=0)
								      { 
								//	echo "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name";die;
									
                                       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --", $selectResult[csf('store_id')], "",0 ); 
								      }
							    else
									  {
									       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --",0, "",0 ); 	
									  }
                                ?>
                                
                            </td>	
                            <td width="50" id="product_id_<?php echo $i; ?>"><?php echo $selectResult[csf('id')]; ?>
                            <input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('id')]; ?>">
                            </td>
                            <td  width="100"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p>
                            <input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('item_category_id')]; ?>">
                            </td>
                            <td width="100" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$selectResult[csf('item_group_id')]]; ?></p> &nbsp;</td>
                            <td width="100" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $selectResult[csf('sub_group_name')]; ?></p></td>
                            <td width="140" id="item_description_<?php echo $i; ?>"><p><?php echo $selectResult[csf('item_description')]." ".$selectResult[csf('item_size')]; ?></p></td> 
                            <td width="32" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$selectResult[csf('unit_of_measure')]]; ?></td>
                            <td width="100" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 150, $dose_base, "", 1, "- Select Dose Base -",$selectResult[csf('dose_base')],"calculate_receipe_qty($i)",1); ?></td>
                            <td width="50" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo def_number_format($selectResult[csf('ratio')],5,""); ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" disabled></td>
                            <td width="70" align="center" id="stock_qty_<?php echo $i; ?>"><input type="text" name="stock_qty[]" id="stock_qty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo $selectResult[csf('current_stock')]; ?>"  disabled></td>
                            <td width="70" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo def_number_format($selectResult[csf('recipe_qnty')],5,""); ?>" disabled></td>
                            <td width="30" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:30px"  value="<?php echo $selectResult[csf('adjust_percent')]; ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" disabled></td>
                            <td width="90" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 90, $increase_decrease, "", 1, "- Select -", $selectResult[csf('adjust_type')],"calculate_receipe_qty($i)",1); ?></td>
                            <td width="" align="center" id="reqn_qnty_<?php echo $i; ?>">
                            <input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('required_qnty')],5,""); ?>" readonly>
                            <input type="text" name="reqn_qnty_edit[]" id="txt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('req_qny_edit')],5,""); ?>" onKeyUp="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)"  / >
                            <input type="hidden" name="hidreqn_qnty_edit[]" id="hidtxt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('req_qny_edit')],5,""); ?>" / >
                            <input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="<?php echo $selectResult[csf('dtls_id')]; ?>">	
                            <input type="hidden" name="transId[]" id="transId_<?php echo $i; ?>" value="<?php echo $selectResult[csf('trans_id')]; ?>">	
                            </td>
                            </tr>
						<?php
						}
						if($issue_basis==5)
						{
						?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>"> 
                            <td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td>
                            <td width="120" id="store_td" >
								<?php 
                                if(str_replace("'","",$selectResult[csf('store_id')])!=0)
								      { 
									
                                       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --", $selectResult[csf('store_id')], "",0 ); 
								      }
							    else
									  {
									       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --",0, "",0 ); 	
									  }
                                ?>
                              
                            </td>	
                            <td width="50" id="product_id_<?php echo $i; ?>"><?php echo $selectResult[csf('id')]; ?>
                            <input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('id')]; ?>">
                            </td>
                            <td  width="100"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p>
                            <input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('item_category_id')]; ?>">
                            </td>
                            <td width="100" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$selectResult[csf('item_group_id')]]; ?></p> &nbsp;</td>
                            <td width="100" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $selectResult[csf('sub_group_name')]; ?></p></td>
                            <td width="140" id="item_description_<?php echo $i; ?>"><p><?php echo $selectResult[csf('item_description')]." ".$selectResult[csf('item_size')]; ?></p></td> 
                            <td width="32" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$selectResult[csf('unit_of_measure')]]; ?></td>
                            <td width="100" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 150, $dose_base, "", 1, "- Select Dose Base -",$selectResult[csf('dose_base')],"calculate_receipe_qty($i)"); ?></td>
                            <td width="50" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo def_number_format($selectResult[csf('ratio')],5,""); ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" ></td>
                            <td width="70" align="center" id="stock_qty_<?php echo $i; ?>"><input type="text" name="stock_qty[]" id="stock_qty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo $selectResult[csf('current_stock')]; ?>"  disabled></td>
                            <td width="70" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo def_number_format($selectResult[csf('recipe_qnty')],5,""); ?>" onClick="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)" readonly ></td>
                            <td width="30" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:30px"  value="<?php echo $selectResult[csf('adjust_percent')]; ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" ></td>
                            <td width="90" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 90, $increase_decrease, "", 1, "- Select -", $selectResult[csf('adjust_type')],"calculate_receipe_qty($i)"); ?></td>
                            <td width="" align="center" id="reqn_qnty_<?php echo $i; ?>">
                            <input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('required_qnty')],5,""); ?>" readonly>
                            <input type="text" name="reqn_qnty_edit[]" id="txt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('req_qny_edit')],5,""); ?>"  onKeyUp="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)" / >
                            <input type="hidden" name="hidreqn_qnty_edit[]" id="hidtxt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('req_qny_edit')],5,""); ?>" / >
                            <input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="<?php echo $selectResult[csf('dtls_id')]; ?>">	
                            <input type="hidden" name="transId[]" id="transId_<?php echo $i; ?>" value="<?php echo $selectResult[csf('trans_id')]; ?>">	
                            </td>
                          </tr>
						<?php
						}
						if($issue_basis==7)
						{
						?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>"> 
                            <td width="30" align="center" id="sl_<?php echo $i; ?>"><?php echo $i; ?></td>
                            <td width="120" id="store_td" >
								<?php 
                                 if(str_replace("'","",$selectResult[csf('store_id')])!=0)
								      { 
									
                                       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --", $selectResult[csf('store_id')], "",0 ); 
								      }
							    else
									  {
									       	echo create_drop_down( "cbo_store_name_$i", 120,  "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$company_id and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --",0, "",0 ); 	
									  }
                                ?>
                               
                            </td>	
                            <td width="50" id="product_id_<?php echo $i; ?>"><?php echo $selectResult[csf('id')]; ?>
                            <input type="hidden" name="txt_prod_id[]" id="txt_prod_id_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('id')]; ?>">
                            </td>
                            <td  width="100"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p>
                            <input type="hidden" name="txt_item_cat[]" id="txt_item_cat_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo $selectResult[csf('item_category_id')]; ?>">
                            </td>
                            <td width="100" id="item_group_id_<?php echo $i; ?>"><p><?php echo $item_group_arr[$selectResult[csf('item_group_id')]]; ?></p> &nbsp;</td>
                            <td width="100" id="sub_group_name_<?php echo $i; ?>"><p><?php echo $selectResult[csf('sub_group_name')]; ?></p></td>
                            <td width="140" id="item_description_<?php echo $i; ?>"><p><?php echo $selectResult[csf('item_description')]." ".$selectResult[csf('item_size')]; ?></p></td> 
                            <td width="32" align="center" id="uom_<?php echo $i; ?>"><?php echo $unit_of_measurement[$selectResult[csf('unit_of_measure')]]; ?></td>
                            <td width="100" align="center" id="dose_base_<?php echo $i; ?>"><?php echo create_drop_down("cbo_dose_base_$i", 150, $dose_base, "", 1, "- Select Dose Base -",$selectResult[csf('dose_base')],"calculate_receipe_qty($i)",1); ?></td>
                            <td width="50" align="center" id="ratio_<?php echo $i; ?>"><input type="text" name="txt_ratio[]" id="txt_ratio_<?php echo $i; ?>" class="text_boxes_numeric" style="width:38px"  value="<?php echo def_number_format($selectResult[csf('ratio')],5,""); ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" readonly></td>
                            <td width="70" align="center" id="stock_qty_<?php echo $i; ?>"><input type="text" name="stock_qty[]" id="stock_qty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo $selectResult[csf('current_stock')]; ?>"  disabled></td>
                            <td width="70" align="center" id="recipe_qnty_<?php echo $i; ?>"><input type="text" name="txt_recipe_qnty[]" id="txt_recipe_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px"  value="<?php echo def_number_format($selectResult[csf('recipe_qnty')],5,""); ?>" onClick="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)" readonly></td>
                            <td width="30" align="center" id="adj_per_<?php echo $i; ?>"><input type="text" name="txt_adj_per[]" id="txt_adj_per_<?php echo $i; ?>" class="text_boxes_numeric" style="width:30px"  value="<?php echo $selectResult[csf('adjust_percent')]; ?>" onChange="calculate_receipe_qty(<?php echo $i; ?>)" readonly></td>
                            <td width="90" align="center" id="adj_type_<?php echo $i; ?>"><?php echo create_drop_down("cbo_adj_type_$i", 90, $increase_decrease, "", 1, "- Select -", $selectResult[csf('adjust_type')],"calculate_receipe_qty($i)",1); ?></td>
                            <td width="" align="center" id="reqn_qnty_<?php echo $i; ?>">
                            <input type="hidden" name="txt_reqn_qnty[]" id="txt_reqn_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('required_qnty')],5,""); ?>" readonly>
                            <input type="text" name="reqn_qnty_edit[]" id="txt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo def_number_format($selectResult[csf('req_qny_edit')],5,""); ?>"  onKeyUp="check_data('#txt_reqn_qnty_edit_<?php echo $i; ?>',<?php echo $selectResult[csf('current_stock')]; ?>)"  readonly / >
                            <input type="hidden" name="hidreqn_qnty_edit[]" id="hidtxt_reqn_qnty_edit_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80%"  value="<?php echo $selectResult[csf('req_qny_edit')]; ?>" readonly / >
                            <input type="hidden" name="updateIdDtls[]" id="updateIdDtls_<?php echo $i; ?>" value="<?php echo $selectResult[csf('dtls_id')]; ?>">	
                            <input type="hidden" name="transId[]" id="transId_<?php echo $i; ?>" value="<?php echo $selectResult[csf('trans_id')]; ?>">	
                            </td>
                            </tr>
						<?php
						}
						?>
						<?php
						$i++;
					}
                }
                
                ?>
                </tbody>
            </table>
        </div>
	</div>           
	<?php
	exit();	
}

if($action=="recipe_item_details")
{
	$sql="select distinct(sub_process) as sub_process_id from dyes_chem_issue_dtls where mst_id='$data' and status_active=1 and is_deleted=0 order by sub_process_id";
	//echo $sql;
	$nameArray=sql_select( $sql );
    foreach($nameArray as $row)
	{
	
	?>
        <h3 align="left" id="accordion_h<?php echo $row[csf("sub_process_id")]; ?>" style="width:910px" class="accordion_h" onClick="fnc_item_details(<?php echo $row[csf("sub_process_id")];?>,<?php echo $data ?>)"><span id="accordion_h<?php echo $row[csf("sub_process_id")]; ?>span">+</span><?php echo $dyeing_sub_process[$row[csf("sub_process_id")]]; ?></h3>
	<?php
	}
}
//data save update delete here------------------------------//

if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_issue_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="200">Issue No</th>
                    <th width="200">Requisition No</th>
                    <th width="200">Issue Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center" >
                        <input type="text" id="txt_issue_no" name="txt_issue_no" class="text_boxes" style="width:180px;" >
                    </td>
                    <td align="center">				
                        <input type="text" style="width:180px" class="text_boxes"  name="txt_req_no" id="txt_req_no" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view (document.getElementById('txt_issue_no').value+'_'+document.getElementById('txt_req_no').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'chemical_dyes_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_issue_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}



if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$txt_issue_no = str_replace("'","",$ex_data[0]);
	$txt_requisition_no =  str_replace("'","",$ex_data[1]);
	$fromDate =  str_replace("'","",$ex_data[2]);
	$toDate =  str_replace("'","",$ex_data[3]);
	$company =  str_replace("'","",$ex_data[4]);
	
	$sql_cond="";
	if($txt_issue_no!="") $sql_cond .= " and issue_number LIKE '%$txt_issue_no%'";	
	if($txt_requisition_no!="") $sql_cond .= " and req_no LIKE '%$txt_requisition_no%'";
	if($fromDate!="" && $toDate="")
	{
		if($db_type==0) 
		{
			$date_form=change_date_format($fromDate,"yyyy-mm-dd");
			$date_to=change_date_format($toDate,"yyyy-mm-dd");
		} 
		else 
		{
			$date_form=change_date_format($fromDate,"yyyy-mm-dd","",-1);
			$date_to=change_date_format($toDate,"yyyy-mm-dd","",-1);
		}
		
		$date_cond="and issue_date between $date_form and $date_to"; 
	
	}
	else
	{
		 $date_cond="";
	}	
	
	
	$batch_arr = return_library_array("select id,batch_no from pro_batch_create_mst where company_id=$ex_data[4] and batch_against<>0 ","id","batch_no");
	$company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$requisition_arr = return_library_array("select id,requ_no from dyes_chem_issue_requ_mst","id","requ_no");
	$arr=array(1=>$requisition_arr,2=>$company_arr,4=>$receive_basis_arr,5=>$batch_arr,6=>$receipe_arr);
	$sql = "select issue_number_prefix_num, issue_number,company_id,issue_date,issue_basis,batch_no,issue_date,id,req_no from inv_issue_master where company_id=$company and entry_form=5 $sql_cond $date_cond";
	
	echo create_list_view("list_view", "Issue No, Requisition No, Company, Requisition Date, Requisition Basis, Batch No,Issue Date","80,150,120,120,120,120","900","260",0, $sql , "js_set_value", "issue_number,id", "", 1, "0,req_no,company_id,0,issue_basis,batch_no,0", $arr, "issue_number_prefix_num,req_no,company_id,issue_date,issue_basis,batch_no,issue_date", "",'','0,0,0,3,0,0,3');
	?>
	</body>           
	<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
}


if($action=="populate_batch_receipe_data")
{
	//echo $data;
	$data=explode("**",$data);
	//print_r($data);
	$sql = sql_select("select id, batch_no, batch_weight, total_liquor,booking_no from pro_batch_create_mst where company_id=$data[0] and id=$data[1] and status_active=1 and is_deleted=0 and batch_against<>0"); 
	foreach($sql as $row)
	{
		$cbo_buyer_name=return_field_value("buyer_id","wo_booking_mst","booking_no='".$row[csf("booking_no")]."'");
		echo "document.getElementById('txt_batch_no').value = '".$row[csf("batch_no")]."';\n"; 
		echo "document.getElementById('txt_batch_weight').value = '".$row[csf("batch_weight")]."';\n"; 
		echo "document.getElementById('txt_tot_liquor').value = '".$liquior_arr[$data[2]]."';\n";
		echo "document.getElementById('cbo_buyer_name').value = '".$cbo_buyer_name."';\n";
	}
}
if($action=="populate_buyer_order_no_data")
{
	$data=explode("**",$data);
	$sql = sql_select("select id, batch_no, batch_weight, total_liquor,booking_no from pro_batch_create_mst where company_id=$data[0] and id in($data[1]) and status_active=1 and is_deleted=0 and batch_against<>0"); 
	
	foreach($sql as $row)
	{
		$po_no="";
		if ($row[csf("booking_no")]!='' || $row[csf("booking_no")]!=0)
		{
			
		 $cbo_buyer_name=return_field_value("buyer_id","wo_booking_mst","booking_no='".$row[csf("booking_no")]."'")."";
		$po_break_down_id=return_field_value("po_break_down_id","wo_booking_mst","booking_no='".$row[csf("booking_no")]."'");
		}
		$po_sql=sql_select("Select po_number from wo_po_break_down where id in(".$po_break_down_id.")");
		foreach($po_sql as $po_row)
		{
			$po_no.=$po_row[csf("po_number")].",";
		}
		//echo "document.getElementById('txt_batch_no').value = '".$row[csf("batch_no")]."';\n"; 
		//echo "document.getElementById('txt_batch_weight').value = '".$row[csf("batch_weight")]."';\n"; 
		//echo "document.getElementById('txt_tot_liquor').value = '".$row[csf("total_liquor")]."';\n";
		echo "document.getElementById('cbo_buyer_name').value = '".$cbo_buyer_name."';\n";
		echo "document.getElementById('txt_order_no').value = '".rtrim($po_no,',')."';\n";
		echo "document.getElementById('hidden_order_id').value = '".$po_break_down_id."';\n";
	}
}

if($action=="populate_data_from_data")
{
	$req_arr = return_library_array("select id,requ_no from dyes_chem_issue_requ_mst","id","requ_no");
	$sql = sql_select("select location_id,issue_date,issue_basis,req_no,batch_no,issue_purpose,loan_party,knit_dye_source,knit_dye_company,challan_no,lap_dip_no,order_id,buyer_id,style_ref,store_id from inv_issue_master where id=$data and entry_form=5");
	foreach($sql as $row)
	{
		echo "document.getElementById('cbo_location_name').value = '".$row[csf("location_id")]."';\n";  
		echo "document.getElementById('txt_issue_date').value = '".change_date_format($row[csf("issue_date")],"dd-mm-yyyy","-")."';\n"; 
		echo "document.getElementById('cbo_issue_basis').value = '".$row[csf("issue_basis")]."';\n";  
		echo "document.getElementById('txt_req_no').value = '".$req_arr[$row[csf("req_no")]]."';\n"; 
		echo "document.getElementById('txt_req_id').value = '".$row[csf("req_no")]."';\n"; 
		echo "document.getElementById('txt_batch_id').value = '".$row[csf("batch_no")]."';\n"; 
		echo "document.getElementById('cbo_issue_purpose').value = '".$row[csf("issue_purpose")]."';\n";  
		echo "document.getElementById('cbo_loan_party').value = '".$row[csf("loan_party")]."';\n"; 
		echo "document.getElementById('cbo_dying_source').value = '".$row[csf("knit_dye_source")]."';\n";  
		echo "document.getElementById('cbo_dying_company').value = '".$row[csf("knit_dye_company")]."';\n"; 
		echo "document.getElementById('txt_challan_no').value = '".$row[csf("challan_no")]."';\n";
		
		echo "document.getElementById('txt_recipe_no').value = '".$rece_arr[$row[csf("lap_dip_no")]]."';\n"; 
		echo "document.getElementById('txt_recipe_id').value = '".$row[csf("lap_dip_no")]."';\n";  
        echo "document.getElementById('txt_tot_liquor').value = '".$liquior_arr[$row[csf("lap_dip_no")]]."';\n"; 
		echo "document.getElementById('txt_order_no').value = '".$row[csf("order_id")]."';\n"; 
		echo "document.getElementById('hidden_order_id').value = '".$row[csf("order_id")]."';\n";  
		echo "document.getElementById('cbo_buyer_name').value = '".$row[csf("buyer_id")]."';\n"; 
		echo "document.getElementById('txt_style_no').value = '".$row[csf("style_ref")]."';\n"; 
		echo "document.getElementById('update_for_cost').value = '1';\n"; 
		$sql_batch = sql_select("select id, batch_no, batch_weight, total_liquor from pro_batch_create_mst where id='".$row[csf("batch_no")]."'  and status_active=1 and is_deleted=0"); 
		foreach($sql_batch as $row_batch)
	    {
		echo "document.getElementById('txt_batch_no').value = '".$row_batch[csf("batch_no")]."';\n"; 
		echo "document.getElementById('txt_batch_weight').value = '".$row_batch[csf("batch_weight")]."';\n"; 
		
	    }
		
	}

}

if($action=="save_update_delete")
{	  
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		check_table_status( $_SESSION['menu_id'],0);
		$dys_che_issue_num=''; $dys_che_update_id=''; $product_id=0;
	
		if(str_replace("'","",$update_id)=="")
		{
			 if($db_type==2 || $db_type==1) { $year_id=" extract(year from insert_date)="; }
			 if($db_type==0)  { $year_id="YEAR(insert_date)="; }
			$new_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'DCI', date("Y",time()), 5, "select issue_number_prefix, issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_name and entry_form=5 and $year_id".date('Y',time())." order by issue_number_prefix_num desc ", "issue_number_prefix", "issue_number_prefix_num" ));
			
	   
			$id=return_next_id( "id", "inv_issue_master", 1 );
			$field_array="id, issue_number_prefix, issue_number_prefix_num, issue_number,issue_basis, issue_purpose, entry_form, company_id,location_id,buyer_id,buyer_job_no,style_ref,req_no,batch_no, issue_date, knit_dye_source, knit_dye_company, challan_no,loan_party,lap_dip_no, order_id,inserted_by, insert_date";
			$data_array="(".$id.",'".$new_system_id[1]."',".$new_system_id[2].",'".$new_system_id[0]."',".$cbo_issue_basis.",".$cbo_issue_purpose.",5,".$cbo_company_name.",".$cbo_location_name.",".$cbo_buyer_name.",".$txt_order_no.",".$txt_style_no.",".$txt_req_id.",".$txt_batch_id.",".$txt_issue_date.",".$cbo_dying_source.",".$cbo_dying_company.",".$txt_challan_no.",".$cbo_loan_party.",".$txt_recipe_id.",".$hidden_order_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			//$rID=sql_insert("inv_issue_master",$field_array,$data_array,0);
			$dys_che_issue_num=$new_system_id[0];
			$dys_che_update_id=$id;
		}
		else
		{
			$field_array_update="issue_basis*issue_purpose*entry_form*company_id*location_id*buyer_id*buyer_job_no*style_ref*req_no*batch_no*issue_date*knit_dye_source*knit_dye_company*challan_no*loan_party*lap_dip_no*order_id*updated_by*update_date";
			$data_array_update="".$cbo_issue_basis."*".$cbo_issue_purpose."*5*".$cbo_company_name."*".$cbo_location_name."*".$cbo_buyer_name."*".$txt_order_no."*".$txt_style_no."*".$txt_req_id."*".$txt_batch_id."*".$txt_issue_date."*".$cbo_dying_source."*".$cbo_dying_company."*".$txt_challan_no."*".$cbo_loan_party."*".$txt_recipe_id."*".$hidden_order_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$dys_che_issue_num=str_replace("'","",$txt_mrr_no);
			$dys_che_update_id=str_replace("'","",$update_id);
			//$rID=sql_update("inv_issue_master", $field_array_update, $data_array_update, "id", $update_id,1); 
		}
		
		
		$updateID_array=array();
		$update_data=array();
		$id_trans=return_next_id( "id", "inv_transaction", 1 ) ;
		$field_array_trans="id, mst_id,requisition_no,receive_basis,pi_wo_batch_no, company_id, prod_id, item_category, transaction_type, transaction_date, cons_uom, cons_quantity, cons_rate, cons_amount, issue_challan_no, store_id, inserted_by, insert_date";
		$id_dtls=return_next_id( "id", "dyes_chem_issue_dtls", 1 ) ;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);
		$field_array_dtls="id,mst_id,trans_id,requ_no,batch_id,recipe_id,requisition_basis,sub_process,product_id,item_category,dose_base,ratio,recipe_qnty,adjust_percent,adjust_type, required_qnty,req_qny_edit,inserted_by,insert_date";
$adcomma=1;
		for ($i=1;$i<=$total_row;$i++)
		{
		$txt_prod_id="txt_prod_id_".$i;
		$txt_item_cat="txt_item_cat_".$i;
		$cbo_store_name="cbo_store_name_".$i;
		
		$cbo_dose_base="cbo_dose_base_".$i;
		$txt_ratio="txt_ratio_".$i;
		$txt_recipe_qnty="txt_recipe_qnty_".$i;
		$txt_adj_per="txt_adj_per_".$i;
		$cbo_adj_type="cbo_adj_type_".$i;
		$txt_reqn_qnty="txt_reqn_qnty_".$i;
		$txt_reqn_qnty_edit="txt_reqn_qnty_edit_".$i;
		$updateIdDtls="updateIdDtls_".$i;
		$transId="transId_".$i;
		$hidtxt_reqn_qnty_edit="hidtxt_reqn_qnty_edit_".$i;
		
		if(str_replace("'",'',$$txt_reqn_qnty_edit)>0)
		{
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value from product_details_master where id=".$$txt_prod_id."");
		$avg_rate=$stock_qnty=$stock_value=0;
		foreach($sql as $result)
		{
			$avg_rate = $result[csf("avg_rate_per_unit")];
			$stock_qnty = $result[csf("current_stock")];
			$stock_value = $result[csf("stock_value")];
		}
		$txt_reqn_qnty_e=str_replace("'","",$$txt_reqn_qnty_edit);
		$issue_stock_value = $avg_rate*str_replace("'","",$txt_reqn_qnty_e);
		if ($adcomma!=1) $data_array_trans .=",";
		$data_array_trans.="(".$id_trans.",".$dys_che_update_id.",".$txt_req_id.",".$cbo_issue_basis.",".$txt_batch_id.",".$cbo_company_name.",".$$txt_prod_id.",".$$txt_item_cat.",2,".$txt_issue_date.",12,".$txt_reqn_qnty_e.",".$avg_rate.",".$issue_stock_value.",".$txt_challan_no.",".$$cbo_store_name.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		if ($adcomma!=1) $data_array_dtls .=",";
		$data_array_dtls .="(".$id_dtls.",".$dys_che_update_id.",".$id_trans.",'".$dys_che_issue_num."',".$txt_batch_id.",".$txt_recipe_id.",".$cbo_issue_basis.",".$cbo_sub_process.",".$$txt_prod_id.",".$$txt_item_cat.",".$$cbo_dose_base.",".$$txt_ratio.",".$$txt_recipe_qnty.",".$$txt_adj_per.",".$$cbo_adj_type.",".$$txt_reqn_qnty.",".$$txt_reqn_qnty_edit.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
		//product master table data UPDATE START----------------------//
		$currentStock   = $stock_qnty-$txt_reqn_qnty_e;
		$StockValue	 	= $currentStock*$avg_rate;
		$avgRate	 	= number_format($avg_rate,$dec_place[3],'.',''); 
		$field_array_prod= "last_issued_qnty*current_stock*stock_value*updated_by*update_date"; 

		if(str_replace("'",'',$$txt_prod_id)!="")
			{
				$id_arr[]=str_replace("'",'',$$txt_prod_id);
			    $data_array_prod[str_replace("'",'',$$txt_prod_id)] =explode(",",("".$txt_reqn_qnty_e.",".$currentStock.",".$StockValue.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."'"));
			}
		//------------------ product_details_master END--------------//
		
		
		//LIFO/FIFO Start-----------------------------------------------//txt_issue_qnty
		$isLIFOfifo=return_field_value("store_method","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17 and item_category_id=".$$txt_item_cat." and status_active=1 and is_deleted=0");
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC";
		$field_array_mrr= "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$sql = sql_select("select id, cons_rate, balance_qnty, balance_amount from inv_transaction where prod_id=".$$txt_prod_id." and balance_qnty>0 and transaction_type in (1,4,5) and item_category=".$$txt_item_cat." order by transaction_date $cond_lifofifo");
		foreach($sql as $result)
		{				
			$recv_trans_id = $result[csf("id")]; // this row will be updated
			$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];
			$cons_rate = $result[csf("cons_rate")];
			$issueQntyBalance = $balance_qnty-$txt_reqn_qnty_e; // minus issue qnty
			$issueStockBalance = $balance_amount-($txt_reqn_qnty_e*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $txt_reqn_qnty_e*$cons_rate;
				//for insert
				if($data_array_mrr!="") $data_array_mrr .= ",";  
				$data_array_mrr .= "(".$mrrWiseIsID.",".$recv_trans_id.",".$id_trans.",5,".$$txt_prod_id.",".$txt_reqn_qnty_e.",".$cons_rate.",".$amount.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
				
				//for update
				$updateID_array[]=$recv_trans_id; 
				$update_data[$recv_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$mrrWiseIsID++;
				break;
			}
			else if($issueQntyBalance<0)
			{
				$issueQntyBalance = $txt_reqn_qnty_e-$balance_qnty;				
				$issue_qnty = $balance_qnty;				
				$amount = $issue_qnty*$cons_rate;
				
				//for insert
				if($data_array_mrr!="") $data_array_mrr .= ",";  
				$data_array_mrr .= "(".$mrrWiseIsID.",".$recv_trans_id.",".$id_trans.",5,".$$txt_prod_id.",".$balance_qnty.",".$cons_rate.",".$amount.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$recv_trans_id; 
				$update_data[$recv_trans_id]=explode("*",("0*0*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$issue_qnty = $issueQntyBalance;
				$mrrWiseIsID++;
			}
			
		}//end foreach
		$adcomma++;
		$id_trans=$id_trans+1;
		$id_dtls=$id_dtls+1;
		}
		}
		
		//echo bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array);
		//echo "insert into inv_mrr_wise_issue_details (".$field_array_mrr.") values ".$data_array_mrr;die;
		//echo bulk_update_sql_statement( "product_details_master", "id", $field_array_prod, $data_array_prod, $id_arr );die;
		//echo "insert into inv_transaction (".$field_array_trans.") values ".$data_array_trans;die;
		if(str_replace("'","",$update_id)=="")
		{
		 	$rID=sql_insert("inv_issue_master",$field_array,$data_array,0);
		}
		else
		{		
			$rID=sql_update("inv_issue_master", $field_array_update, $data_array_update, "id", $update_id,1); 
		}
		if($rID) $flag=1; else $flag=0;
		$rID2=sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
		if($flag==1) { if($rID2) $flag=1; else $flag=0; } 
		
		$rID3=sql_insert("dyes_chem_issue_dtls",$field_array_dtls,$data_array_dtls,1); 
		if($flag==1) { if($rID3) $flag=1; else $flag=0; } 
		
	    $rID4=execute_query(bulk_update_sql_statement( "product_details_master", "id", $field_array_prod, $data_array_prod, $id_arr ),1);
        if($flag==1) { if($rID4) $flag=1; else $flag=0; 	} 
		
		if($data_array_mrr!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array_mrr,$data_array_mrr,1);
			if($flag==1) { if($mrrWiseIssueID) $flag=1; else $flag=0; }
		}
		
		//transaction table stock update here------------------------//
		if(count($updateID_array)>0)
		{
			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array),1);
			if($flag==1) { if($upTrID) $flag=1; else $flag=0;} 
		}
		
		
 		
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$dys_che_update_id."**".$dys_che_issue_num."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
			//echo $flag;
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			
			if($flag==1)
			{
				oci_commit($con);
				echo "0**".$dys_che_update_id."**".$dys_che_issue_num."**0";
			}
			else
			{
				oci_rollback($con);
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		check_table_status( $_SESSION['menu_id'],0);
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		$updateID_array=array();
		$update_data=array();
		$field_array_update="issue_basis*issue_purpose*entry_form*company_id*location_id*buyer_id*buyer_job_no*style_ref*req_no*batch_no*issue_date*knit_dye_source*knit_dye_company*challan_no*loan_party*lap_dip_no*order_id*updated_by*update_date";
		$data_array_update="".$cbo_issue_basis."*".$cbo_issue_purpose."*5*".$cbo_company_name."*".$cbo_location_name."*".$cbo_buyer_name."*".$txt_order_no."*".$txt_style_no."*".$txt_req_id."*".$txt_batch_id."*".$txt_issue_date."*".$cbo_dying_source."*".$cbo_dying_company."*".$txt_challan_no."*".$cbo_loan_party."*".$txt_recipe_id."*".$hidden_order_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		//echo $data_array_update;
		$dys_che_issue_num=$txt_mrr_no;
		$dys_che_update_id=$update_id;
		$rID=sql_update("inv_issue_master",$field_array_update,$data_array_update,"id",$update_id,1); 
		if($rID) $flag=1; else $flag=0;
		
		
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);
		$up_field_array_trans="prod_id*item_category*transaction_type*transaction_date*cons_uom* cons_quantity*cons_rate*cons_amount*issue_challan_no*store_id*updated_by*update_date";
		$up_field_array_dtls="sub_process*product_id*item_category*dose_base*ratio*recipe_qnty*adjust_percent*adjust_type*required_qnty*req_qny_edit*updated_by*update_date";
		for ($i=1;$i<=$total_row;$i++)
		{
		$txt_prod_id="txt_prod_id_".$i;
		$txt_item_cat="txt_item_cat_".$i;
		$cbo_store_name="cbo_store_name_".$i;
		$cbo_dose_base="cbo_dose_base_".$i;
		$txt_ratio="txt_ratio_".$i;
		$txt_recipe_qnty="txt_recipe_qnty_".$i;
		$txt_adj_per="txt_adj_per_".$i;
		$cbo_adj_type="cbo_adj_type_".$i;
		$txt_reqn_qnty="txt_reqn_qnty_".$i;
		$txt_reqn_qnty_edit="txt_reqn_qnty_edit_".$i;
		$updateIdDtls="updateIdDtls_".$i;
		$transId="transId_".$i;
		$hidtxt_reqn_qnty_edit="hidtxt_reqn_qnty_edit_".$i;
		
		
		//===================================================
		$stock=sql_select("select current_stock, avg_rate_per_unit from product_details_master where id=".$$txt_prod_id."");
		$adjust_curr_stock=$stock[0][csf('current_stock')]+str_replace("'", '',$$hidtxt_reqn_qnty_edit);
		$adjust_rate=$stock[0][csf('avg_rate_per_unit')];
		$adjust_value=$adjust_curr_stock*$adjust_rate;
		
		$adjust_prod=sql_update("product_details_master","current_stock*stock_value",$adjust_curr_stock."*".$adjust_value,"id",$$txt_prod_id,0);
		if($adjust_prod) $flag=1; else $flag=80;
		//========================================================
		
		
		
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value from product_details_master where id=".$$txt_prod_id."");
		$avg_rate=$stock_qnty=$stock_value=0;
		foreach($sql as $result)
		{
			$avg_rate = $result[csf("avg_rate_per_unit")];
			$stock_qnty = $result[csf("current_stock")];
			$stock_value = $result[csf("stock_value")];
		}
		
		$txt_reqn_qnty_e=str_replace("'","",$$txt_reqn_qnty_edit);
		$issue_stock_value = $avg_rate*str_replace("'","",$txt_reqn_qnty_e);
		
		if(str_replace("'",'',$$transId)!="")
		{
				$id_arr_trans[]=str_replace("'",'',$$transId);
			    $data_array_trans[str_replace("'",'',$$transId)] =explode(",",("".$$txt_prod_id.",".$$txt_item_cat.",2,".$txt_issue_date.",12,".$txt_reqn_qnty_e.",".$avg_rate.",".$issue_stock_value.",".$txt_challan_no.",".$$cbo_store_name.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."'"));
		}
		
		if(str_replace("'",'',$$updateIdDtls)!="")
		{
				$id_arr_dtls[]=str_replace("'",'',$$updateIdDtls);
			    $data_array_dtls[str_replace("'",'',$$updateIdDtls)] =explode(",",("".$cbo_sub_process.",".$$txt_prod_id.",".$$txt_item_cat.",".$$cbo_dose_base.",".$$txt_ratio.",".$$txt_recipe_qnty.",".$$txt_adj_per.",".$$cbo_adj_type.",".$$txt_reqn_qnty.",".$$txt_reqn_qnty_edit.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."'"));
		}
		//product master table data UPDATE START----------------------//
		$currentStock   = $stock_qnty-$txt_reqn_qnty_e;
		$StockValue	 	= $currentStock*$avg_rate;
		$avgRate	 	= number_format($avg_rate,$dec_place[3],'.',''); 
		$field_array_prod= "avg_rate_per_unit*last_issued_qnty*current_stock*stock_value*updated_by*update_date"; 

		if(str_replace("'",'',$$txt_prod_id)!="")
		{
				$id_arr[]=str_replace("'",'',$$txt_prod_id);
			    $data_array_prod[str_replace("'",'',$$txt_prod_id)] =explode(",",("".$avgRate.",".$txt_reqn_qnty_e.",".$currentStock.",".$StockValue.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."'"));
		}
		//------------------ product_details_master END--------------//
		
		//transaction table Update START--------------------------//
		$update_array_tra = "balance_qnty*balance_amount*updated_by*update_date";
		$sql = sql_select("select a.id,a.balance_qnty,a.balance_amount,b.issue_qnty,b.rate,b.amount from inv_transaction a, inv_mrr_wise_issue_details b where a.id=b.recv_trans_id and b.issue_trans_id=".$$transId." and b.entry_form=5 and a.item_category =".$$txt_item_cat.""); 
		$updateID_array_tra = array();
		$update_data_tra = array();
		foreach($sql as $result)
		{
			$adjBalance = $result[csf("balance_qnty")]+$result[csf("issue_qnty")];
			$adjAmount = $result[csf("balance_amount")]+$result[csf("amount")];
			$updateID_array_tra[]=$result[csf("id")]; 
			$update_data_tra[$result[csf("id")]]=explode("*",("".$adjBalance."*".$adjAmount."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
		}
       // print_r($updateID_array);die;
		if(count($updateID_array_tra)>0)
		{
			//echo bulk_update_sql_statement("inv_transaction","id",$update_array_tra,$update_data_tra,$updateID_array_tra);die;
			$query=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_tra,$update_data_tra,$updateID_array_tra),1);
			if($flag==1) { if($query) $flag=1; else $flag=0; } 
			
			$query2 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=".$$transId." and entry_form=5",1);
			{  if($query2) $flag=1; else $flag=0; } 
		}
		//transaction table Update END----------------------------//
		//LIFO/FIFO Start-----------------------------------------------//txt_issue_qnty
		
		
		$isLIFOfifo=return_field_value("store_method","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17 and item_category_id=".$$txt_item_cat." and status_active=1 and is_deleted=0");
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC";
		$field_array_mrr= "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$sql = sql_select("select id, cons_rate, balance_qnty, balance_amount from inv_transaction where prod_id=".$$txt_prod_id." and balance_qnty>0 and transaction_type in (1,4,5) and item_category=".$$txt_item_cat." order by transaction_date $cond_lifofifo");
		foreach($sql as $result)
		{				
			$recv_trans_id = $result[csf("id")]; // this row will be updated
			$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];
			$cons_rate = $result[csf("cons_rate")];
			$issueQntyBalance = $balance_qnty-$txt_reqn_qnty_e; // minus issue qnty
			$issueStockBalance = $balance_amount-($txt_reqn_qnty_e*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $txt_reqn_qnty_e*$cons_rate;
				//for insert
				if($data_array_mrr!="") $data_array_mrr .= ",";  
				$data_array_mrr .= "(".$mrrWiseIsID.",".$recv_trans_id.",".str_replace("'",'',$$transId).",5,".$$txt_prod_id.",".$txt_reqn_qnty_e.",".$cons_rate.",".$amount.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
				
				//for update
				$updateID_array[]=$recv_trans_id; 
				$update_data[$recv_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				
				$mrrWiseIsID++;
				break;
			}
			else if($issueQntyBalance<0)
			{
				$issueQntyBalance = $txt_reqn_qnty_e-$balance_qnty;				
				$issue_qnty = $balance_qnty;				
				$amount = $issue_qnty*$cons_rate;
				
				//for insert
				if($data_array_mrr!="") $data_array_mrr .= ",";  
				$data_array_mrr .= "(".$mrrWiseIsID.",".$recv_trans_id.",".str_replace("'",'',$$transId).",5,".$$txt_prod_id.",".$balance_qnty.",".$cons_rate.",".$amount.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$recv_trans_id; 
				$update_data[$recv_trans_id]=explode("*",("0*0*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$issue_qnty = $issueQntyBalance;
				$mrrWiseIsID++;
			}
			
		}//end foreach
		
		}
	
		//print_r($data_array_mrr);die;	
		if(count($id_arr_trans)>0)
		{  
		//echo bulk_update_sql_statement("inv_transaction","id",$up_field_array_trans,$data_array_trans,$id_arr_trans);die;
		    $upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$up_field_array_trans,$data_array_trans,$id_arr_trans),1);
			if($flag==1) 
			{	 if($upTrID) $flag=1; else $flag=0; }
		}
	
		if(count($id_arr_dtls)>0)
		{ 
		    $upDtID=execute_query(bulk_update_sql_statement("dyes_chem_issue_dtls","id",$up_field_array_dtls,$data_array_dtls,$id_arr_dtls),1);
			if($flag==1) 	{ if($upDtID) $flag=1; else $flag=0; } 
		}
		
		if(count($id_arr)>0)
		{
			//echo bulk_update_sql_statement( "product_details_master", "id", $field_array_prod, $data_array_prod, $id_arr );die;
		    $rID4=execute_query(bulk_update_sql_statement( "product_details_master", "id", $field_array_prod, $data_array_prod, $id_arr ),1);
			if($flag==1) { if($rID4) $flag=1; else $flag=0; }
		}
	
	if($data_array_mrr!="")
		{	
		//echo "insert into 	inv_mrr_wise_issue_details($field_array_mrr)values".$data_array_mrr;die;
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array_mrr,$data_array_mrr,1);
			if($flag==1) 
			{
				if($mrrWiseIssueID) $flag=1; else $flag=0; 
			} 
		}
		
		if(count($updateID_array)>0)
		{  
		//echo bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array);die;
			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array),1);
			if($flag==1) { if($upTrID) $flag=1; else $flag=0; }
		}
 		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			//echo $flag;
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
			else
			{
				mysql_query("ROLLBACK");
				 
				echo "6**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			//echo $flag;
			if($flag==1)
			{
				oci_commit($con);
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
			else
			{
				oci_rollback($con);
				echo "6**0**"."&nbsp;"."**0**".$flag;
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		
	}		
}




if($action=="chemical_dyes_issue_cost_print")
{
	extract($_REQUEST);
	$data=explode('*',$data);
	
	$sql="select a.id, a.issue_number, a.issue_date, a.issue_basis,a.issue_purpose, a.req_no, a.batch_no, a.issue_purpose, a.loan_party,a.lap_dip_no, a.knit_dye_source, a.knit_dye_company, a.challan_no, a.buyer_job_no, a.buyer_id, a.style_ref from inv_issue_master a where a.id='$data[1]' and a.company_id='$data[0]'";
	//echo $sql;die;
	$dataArray=sql_select($sql);
	function return_field_value2( $field_name, $table_name, $query_cond, $return_fld_name, $new_conn )  // checked 3
{
	// This function will Return Single or Multiple field value 
	// concated with seperator having only one row result
	//Return value:  query result as filed value
	// Uses  single field:: return_field_value("buyer_name", "lib_buyer", "id=1");
	// Uses  multi field:: return_field_value("concate(buyer_name,'_',contact_person)", "lib_buyer", "id=1"); do not use concat
	if ($return_fld_name=="") $return_fld_name=$field_name;
	 
 	$queryText="select ".$field_name." from ".$table_name." where ".$query_cond." "  ;
	return $queryText; die;
	$nameArray=sql_select( $queryText, '',$new_conn ); 
	foreach ($nameArray as $result)
		if ($result[csf($return_fld_name)]!="") return $result[csf($return_fld_name)]; else return false;
	//die;
}
 
     $color_id=return_field_value("color_id"," pro_batch_create_mst","id=".$dataArray[0][csf('batch_no')]."");
	 //echo $color_id;die;
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$loan_party_arr=return_library_array( "select id, other_party_name from  lib_other_party", "id", "other_party_name"  );
	$batch_arr=return_library_array( "select id, batch_no from  pro_batch_create_mst", "id", "batch_no"  );
	$batch_weight_arr=return_library_array( "select id, batch_weight from  pro_batch_create_mst", "id", "batch_weight"  );
	$liquor_arr=return_library_array( "select id, total_liquor from  pro_batch_create_mst", "id", "total_liquor"  );
	$color_arr=return_library_array("select id,color_name from lib_color" ,"id","color_name");
?>
<div style="width:1100px; ">
    <table width="900" cellspacing="0" align="center">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u>Dyes & Chemical Issue Note</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Issue ID :</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="125"><strong>Issue Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
            <td width="130"><strong>Issue Basis :</strong></td> <td width="175px"><?php echo $receive_basis_arr[$dataArray[0][csf('issue_basis')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Req. No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('req_no')]; ?></td>
            <td><strong>Batch No :</strong></td><td width="175px"><?php echo $batch_arr[$dataArray[0][csf('batch_no')]]."/".$color_arr[$color_id]; ?></td>
			<td><strong>Issue Purpose :</strong></td> <td width="175px"><?php echo $yarn_issue_purpose[$dataArray[0][csf('issue_purpose')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Loan Party:</strong></td> <td width="175px"><?php echo $loan_party_arr[$dataArray[0][csf('loan_party')]]; ?></td>
            <td><strong>Dyeing Source :</strong></td><td width="175px"><?php echo $knitting_source[$dataArray[0][csf('knit_dye_source')]]; ?></td>
            <td><strong>Dyeing Company:</strong></td><td width="175px"><?php echo $company_library[$dataArray[0][csf('knit_dye_company')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Challan No :</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Recipe No :</strong></td><td width="175px"><?php echo $rece_arr[$dataArray[0][csf('lap_dip_no')]]; ?></td>
            <td><strong>Buyer Order:</strong></td><td width="175px"><?php echo $dataArray[0][csf('buyer_job_no')]; ?></td>
        </tr>
        <tr>
            <td><strong>Buyer Name:</strong></td> <td width="175px"><?php echo $buyer_library[$dataArray[0][csf('buyer_id')]]; ?></td>
            <td><strong>Style Ref. :</strong></td><td width="175px"><?php echo $dataArray[0][csf('style_ref')]; ?></td>
            <td><strong>Batch Weight:</strong></td><td width="175px"><?php echo $batch_weight_arr[$dataArray[0][csf('batch_no')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Total Liq.(ltr):</strong></td> <td width="175px"><?php echo $liquior_arr[$dataArray[0][csf('lap_dip_no')]]; ?></td>
            <td><strong>Bar Code:</strong></td><td  colspan="3" id="barcode_img_id"></td>

        </tr>
    </table>
        <br>
	<div style="width:1300px;">
    <table align="center" cellspacing="0" width="1100"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="200" >Store Name</th>
            <th width="80" >Item Cat.</th>
            <th width="80" >Item Group</th>
            <th width="100" >Sub Group</th>
            <th width="170" >Item Description</th>
            <th width="50" >UOM</th>
            <th width="110" >Dose Base</th> 
            <th width="40" >Ratio</th>
            <th width="60" >Recipe Qnty</th>
            <th width="50" >Adj%</th>
            <th width="60" >Adj Type</th> 
            <th width="60" >Issue Qnty</th>
            <th width="60" >Unit Price</th> 
            <th width="90" >Amount(BDT)</th>
        </thead>
        <tbody> 
   
<?php
 	$group_arr=return_library_array( "select id,item_name from lib_item_group where item_category in (5,6,7) and status_active=1 and is_deleted=0",'id','item_name');
// ****************************previous sql***************************************************
      $sql_dtls = "select b.id,a.issue_number,b.store_id,
	  b.cons_uom, b.cons_quantity,b.cons_amount, b.machine_category, b.machine_id, b.prod_id, b.location_id, b.department_id,       b.section_id,b.cons_rate,b.cons_amount,
	  c.item_description, c.item_group_id, c.sub_group_name, c.item_size,
	  d.sub_process, d.item_category, d.dose_base, d.ratio, d.recipe_qnty, d.adjust_percent, d.adjust_type, d.required_qnty, d.req_qny_edit
	  from inv_issue_master a, inv_transaction b, product_details_master c, dyes_chem_issue_dtls d
	  where a.id=b.mst_id and b.id =d.trans_id and d.product_id=c.id and b.prod_id=c.id and b.transaction_type=2 and a.entry_form=5 and b.item_category in (5,6,7) and a.id=$data[1]  order by d.sub_process ";  //group by d.id,d.sub_process
	//  ******************************************************************************************************************************************************	

	 
	  $sql_result= sql_select($sql_dtls);
	  $i=1;$m=1;
	$arr_sub_process=array();
	foreach($sql_result as $row)
	{

		
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
			
		
			$count=0;
			if(!in_array($row[csf("sub_process")],$arr_sub_process))
			{
				if($m!=1)
				{
					?> 
				   <tr>
						<td colspan="9" align="right"><strong>Total :</strong></td>
						<td align="right"><?php echo number_format($recipe_qnty_sum,1); ?></td>
						<td align="right" colspan="3"><?php echo number_format($req_qny_edit_sum,1); ?></td>
						<td align="right" colspan="2"><?php echo number_format($total_amount_sum,2); ?></td>
					</tr>
					<?php
							$recipe_qnty_sum=0;
							$req_qny_edit_sum=0;
							$total_amount_sum=0;
				}
		    ?> 
                    <tr>
                         <td colspan="15" style="font-size:15px; text-align:center"><strong>Dyeing Sub-Process: <?php echo  $dyeing_sub_process[$row[csf("sub_process")]] ?></strong></td>
                    </tr>
            <?php
			
            }
			$arr_sub_process[]=$row[csf("sub_process")];
			?>
                    <tr bgcolor="<?php echo $bgcolor; ?>">
                        <td align="center"><?php echo $i; ?></td>
                        <td><?php echo $store_library[$row[csf("store_id")]]; ?></td>
                        <td><?php echo $item_category[$row[csf("item_category")]]; ?></td>
                        <td><?php echo $group_arr[$row[csf("item_group_id")]]; ?></td>
                        <td><?php echo $row[csf("sub_group_name")]; ?></td>
                        <td><?php echo $row[csf("item_description")].' '.$row[csf("item_size")]; ?></td>
                        <td align="center"><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></td>
                        <td><?php echo $dose_base[$row[csf("dose_base")]]; ?></td>
                        <td align="right"><?php echo number_format($row[csf("ratio")],2); ?></td>
                        <td align="right"><?php echo $row[csf("recipe_qnty")]; ?></td>
                        <td align="right"><?php echo $row[csf("adjust_percent")]; ?></td>
                        <td><?php echo $increase_decrease[$row[csf("adjust_type")]]; ?></td>
                        <td align="right"><?php echo number_format($row[csf("req_qny_edit")],2); ?></td>
                        <td align="right"><?php echo number_format($row[csf("cons_rate")],2); ?></td>
                        <td align="right" colspan="3"><?php echo number_format($row[csf("cons_amount")],2); ?></td>
                    </tr>
          
			<?php
			$req_qny_edit=$row[csf('req_qny_edit')];
			$req_qny_edit_sum += $req_qny_edit;
			$recipe_qnty=$row[csf('recipe_qnty')];
			$recipe_qnty_sum += $recipe_qnty;
			$total_amount=$row[csf("cons_amount")];
			$total_amount_sum+=$total_amount;
			
			$grand_totao_amount+=$total_amount;
			$total_req_qny_edit+=$req_qny_edit;
			$grand_total_receive+=$req_qny_edit;
			 $i++; $m++;
			 } 
			 ?>
                    <tr>
                        <td colspan="9" align="right"><strong>Total :</strong></td>
                        <td align="right"><?php echo number_format($recipe_qnty_sum,1); ?></td>
                        <td align="right" colspan="3"><?php echo number_format($req_qny_edit_sum,1); ?></td>
                        <td align="right" colspan="2"><?php echo number_format($total_amount_sum,2); ?></td>
                        
                    </tr>
            </tbody>
           <tfoot>
                    <tr>
                        <td colspan="9" align="right"><strong>Grand Total :</strong></td>
                        <td align="right"><?php echo number_format($grand_total_receive,1); ?></td>
                        <td align="right" colspan="3"><?php echo number_format($total_req_qny_edit,1); ?></td>
                        <td align="right" colspan="2"><?php echo number_format($grand_totao_amount,2); ?></td>
                    </tr> 
                     
                    <tr>
                        <td colspan="13" align="right"><strong>Cost Per Kg</strong></td>
                       
                      
                        <td align="right" colspan="2"><?php echo number_format(($grand_totao_amount/$batch_weight_arr[$dataArray[0][csf('batch_no')]]),2); ?></td>
                    </tr>                       
            </tfoot>
      </table>
    
        <br>
		 <?php
            echo signature_table(9, $data[0], "900px");
         ?>
      </div>
   </div> 
    <script type="text/javascript" src="../../../js/jquery.js"></script>
     <script type="text/javascript" src="../../../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess ){
		   
			var value = valuess;//$("#barcodeValue").val();
		 // alert(value)
			var btype = 'code39';//$("input[name=btype]:checked").val();
			var renderer ='bmp';// $("input[name=renderer]:checked").val();
			 
			var settings = {
			  output:renderer,
			  bgColor: '#FFFFFF',
			  color: '#000000',
			  barWidth: 1,
			  barHeight: 30,
			  moduleSize:5,
			  posX: 10,
			  posY: 20,
			  addQuietZone: 1
			};
			//$("#barcode_img_id").html('11');
			 value = {code:value, rect: false};
			
			$("#barcode_img_id").show().barcode(value, btype, settings);
		  
		} 
  
	 generateBarcode('<?php echo $data[2]; ?>');
	 
	 
	 </script>
     <?php 
	
}




if($action=="chemical_dyes_issue_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql="select a.id, a.issue_number, a.issue_date, a.issue_basis,a.issue_purpose, a.req_no, a.batch_no, a.issue_purpose, a.loan_party, a.knit_dye_source, a.knit_dye_company, a.challan_no, a.buyer_job_no, a.buyer_id, a.style_ref from inv_issue_master a where a.id='$data[1]' and a.company_id='$data[0]'";
	//echo $sql;die;
	$dataArray=sql_select($sql);
        	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$loan_party_arr=return_library_array( "select id, other_party_name from  lib_other_party", "id", "other_party_name"  );
	$batch_arr=return_library_array( "select id, batch_no from  pro_batch_create_mst", "id", "batch_no"  );
	$batch_weight_arr=return_library_array( "select id, batch_weight from  pro_batch_create_mst", "id", "batch_weight"  );
	//$liquor_arr=return_library_array( "select id, total_liquor from  pro_batch_create_mst", "id", "total_liquor"  );
	
?>
<div style="width:1200px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u>Dyes & Chemical Issue Note</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Issue ID :</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="125"><strong>Issue Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
            <td width="130"><strong>Issue Basis :</strong></td> <td width="175px"><?php echo $receive_basis_arr[$dataArray[0][csf('issue_basis')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Req. No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('req_no')]; ?></td>
            <td><strong>Batch No :</strong></td><td width="175px"><?php echo $batch_arr[$dataArray[0][csf('batch_no')]]; ?></td>
			<td><strong>Issue Purpose :</strong></td> <td width="175px"><?php echo $yarn_issue_purpose[$dataArray[0][csf('issue_purpose')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Loan Party:</strong></td> <td width="175px"><?php echo $loan_party_arr[$dataArray[0][csf('loan_party')]]; ?></td>
            <td><strong>Dyeing Source :</strong></td><td width="175px"><?php echo $knitting_source[$dataArray[0][csf('knit_dye_source')]]; ?></td>
            <td><strong>Dyeing Company:</strong></td><td width="175px"><?php echo $company_library[$dataArray[0][csf('knit_dye_company')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Challan No :</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Recipe No :</strong></td><td width="175px"><?php //echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Buyer Order:</strong></td><td width="175px"><?php echo $dataArray[0][csf('buyer_job_no')]; ?></td>
        </tr>
        <tr>
            <td><strong>Buyer Name:</strong></td> <td width="175px"><?php echo $buyer_library[$dataArray[0][csf('buyer_id')]]; ?></td>
            <td><strong>Style Ref. :</strong></td><td width="175px"><?php echo $dataArray[0][csf('style_ref')]; ?></td>
            <td><strong>Batch Weight:</strong></td><td width="175px"><?php echo $batch_weight_arr[$dataArray[0][csf('batch_no')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Total Liq.(ltr):</strong></td> <td width="175px"><?php echo $liquor_arr[$dataArray[0][csf('batch_no')]]; ?></td>
            <td><strong>Bar Code:</strong></td><td  colspan="3" id="barcode_img_id"></td>
        </tr>
    </table>
        <br>
	<div style="width:100%;">
    <table align="right" cellspacing="0" width="1100"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="80" >Sub Process</th>
            <th width="200" >Store Name</th>
            <th width="80" >Item Cat.</th>
            <th width="80" >Item Group</th>
            <th width="100" >Sub Group</th>
            <th width="170" >Item Description</th>
            <th width="50" >UOM</th>
            <th width="110" >Dose Base</th> 
            <th width="40" >Ratio</th>
            <th width="60" >Recipe Qnty</th>
            <th width="50" >Adj%</th>
            <th width="60" >Adj Type</th> 
            <th width="60" >Issue Qnty</th>
        </thead>
        <tbody> 
   
<?php
 	$group_arr=return_library_array( "select id,item_name from lib_item_group where item_category in (5,6,7) and status_active=1 and is_deleted=0",'id','item_name');
	
 $sql_dtls = "select b.id,a.issue_number,b.store_id,
	  b.cons_uom, b.cons_quantity,b.cons_amount, b.machine_category, b.machine_id, b.prod_id, b.location_id, b.department_id,       b.section_id,b.cons_rate,b.cons_amount,
	  c.item_description, c.item_group_id, c.sub_group_name, c.item_size,
	  d.sub_process, d.item_category, d.dose_base, d.ratio, d.recipe_qnty, d.adjust_percent, d.adjust_type, d.required_qnty, d.req_qny_edit
	  from inv_issue_master a, inv_transaction b, product_details_master c, dyes_chem_issue_dtls d
	  where a.id=b.mst_id and b.id =d.trans_id and d.product_id=c.id and b.prod_id=c.id and b.transaction_type=2 and a.entry_form=5 and b.item_category in (5,6,7) and a.id=$data[1]  order by d.sub_process "; 
	 // echo $sql_dtls;die;
	  $sql_result= sql_select($sql_dtls);
	  $i=1;
	
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
			$recipe_qnty=$row[csf('recipe_qnty')];
			$recipe_qnty_sum += $recipe_qnty;
			
			$req_qny_edit=$row[csf('req_qny_edit')];
			$req_qny_edit_sum += $req_qny_edit;
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <td><?php echo $dyeing_sub_process[$row[csf("sub_process")]]; ?></td>
                <td align="center"><?php echo $store_library[$row[csf("store_id")]]; ?></td>
                <td><?php echo $item_category[$row[csf("item_category")]]; ?></td>
                <td><?php echo $group_arr[$row[csf("item_group_id")]]; ?></td>
                <td><?php echo $row[csf("sub_group_name")]; ?></td>
                <td><?php echo $row[csf("item_description")].' '.$row[csf("item_size")]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></td>
                <td><?php echo $dose_base[$row[csf("dose_base")]]; ?></td>
                <td align="right"><?php echo number_format($row[csf("ratio")],4); ?></td>
                <td align="right"><?php echo $row[csf("recipe_qnty")]; ?></td>
                <td align="right"><?php echo $row[csf("adjust_percent")]; ?></td>
                <td><?php echo $increase_decrease[$row[csf("adjust_type")]]; ?></td>
                <td align="right"><?php echo number_format($row[csf("req_qny_edit")],4); ?></td>
			</tr>
			<?php $i++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10" align="right"><strong>Total :</strong></td>
                <td align="right"><?php echo number_format($recipe_qnty_sum,4); ?></td>
                <td align="right" colspan="3"><?php echo number_format($req_qny_edit_sum,4); ?></td>
            </tr>                           
        </tfoot>
      </table>
        <br>
		 <?php
            echo signature_table(9, $data[0], "900px");
         ?>
      </div>
   </div> 
    <script type="text/javascript" src="../../../js/jquery.js"></script>
     <script type="text/javascript" src="../../../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess ){
		   
			var value = valuess;//$("#barcodeValue").val();
		// alert(value)
			var btype = 'code39';//$("input[name=btype]:checked").val();
			var renderer ='bmp';// $("input[name=renderer]:checked").val();
			 
			var settings = {
			  output:renderer,
			  bgColor: '#FFFFFF',
			  color: '#000000',
			  barWidth: 1,
			  barHeight: 30,
			  moduleSize:5,
			  posX: 10,
			  posY: 20,
			  addQuietZone: 1
			};
			//$("#barcode_img_id").html('11');
			 value = {code:value, rect: false};
			
			$("#barcode_img_id").show().barcode(value, btype, settings);
		  
		} 
  
	   generateBarcode('<?php echo $data[3]; ?>');
	 
	 
	 </script>
     <?php
	 exit(); 
 }
?>
