-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_export_doc_submission_invo`
--

DROP TABLE IF EXISTS `com_export_doc_submission_invo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_export_doc_submission_invo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_submission_mst_id` int(11) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `is_lc` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=LC; 2=SC',
  `lc_sc_id` int(11) NOT NULL DEFAULT '0',
  `is_converted` int(2) NOT NULL DEFAULT '0' COMMENT '0=not convert buyer to bank,1=convert buyer to bank',
  `submission_dtls_id` int(11) NOT NULL DEFAULT '0',
  `bl_no` varchar(50) NOT NULL,
  `invoice_date` date NOT NULL,
  `net_invo_value` double NOT NULL DEFAULT '0',
  `all_order_no` text NOT NULL COMMENT 'po breakdown id',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_export_doc_submission_invo`
--

LOCK TABLES `com_export_doc_submission_invo` WRITE;
/*!40000 ALTER TABLE `com_export_doc_submission_invo` DISABLE KEYS */;
INSERT INTO `com_export_doc_submission_invo` VALUES (1,1,11,1,29,0,0,'','0000-00-00',8,'625',1,'2015-10-25 13:45:30',0,'0000-00-00 00:00:00',1,0),(5,2,7,1,14,0,4,'5801048267','0000-00-00',92293.17,'397,398,399,400,401,403,404,406,409,410,412,413,414,415,416,417,418,426,427',13,'2015-11-11 09:42:32',0,'0000-00-00 00:00:00',1,0),(6,3,8,1,10,0,0,'','0000-00-00',1778,'316,318',13,'2015-11-15 16:11:07',0,'0000-00-00 00:00:00',1,0),(7,4,9,1,13,0,0,'','0000-00-00',23621.52,'315,320',13,'2015-11-15 16:19:14',0,'0000-00-00 00:00:00',1,0),(8,4,10,1,13,0,0,'','0000-00-00',22454.25,'368,370',13,'2015-11-15 16:19:14',0,'0000-00-00 00:00:00',1,0),(10,5,14,1,16,0,9,'','0000-00-00',29326.84,'388,389,384,385,386,387',13,'2015-11-15 16:31:12',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `com_export_doc_submission_invo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
