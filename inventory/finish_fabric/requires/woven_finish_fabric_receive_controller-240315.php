﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//echo "shajjad_".$data;

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

//load drop down supplier
if ($action=="load_drop_down_supplier")
{
	if($db_type==0)
	{
		echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET($data,tag_company) and FIND_IN_SET(2,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
		exit();
	}
	else
	{
		echo create_drop_down( "cbo_supplier", 170, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data' and b.party_type =2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
		exit();
	}
}


//load drop down store
if ($action=="load_drop_down_store")
{
	if($db_type==0)
	{	  
		echo create_drop_down( "cbo_store_name", 170, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET($data,company_id) and FIND_IN_SET(1,item_category_id) order by store_name","id,store_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
	}
	else
	{
		echo create_drop_down( "cbo_store_name", 170, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=1 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "-- Select --",0, "",0); 
		exit();
	}
}

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 170, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

//load drop down color
if($action=="load_drop_down_color")
{
	echo create_drop_down( "cbo_color", 110, "select id,color_name from lib_color where status_active=1 order by color_name and color_name!=''","id,color_name", 1, "--Select--", 0, "",0 );
	echo '<input type="button" name="btn_color" id="btn_color" class="formbutton"  style="width:20px" onClick="fn_color_new(this.id)" value="N" />';
	exit();
}
// wo/pi popup here----------------------// 
if ($action=="wopi_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
		$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
		$("#hidden_is_non_ord_sample").val(splitData[2]); // wo/pi number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_th_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            echo create_drop_down( "cbo_search_by", 170, $receive_basis_arr,"",1, "--Select--", $receive_basis,"",1 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_wopi_search_list_view', 'search_div', 'woven_finish_fabric_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_tbl_id" value="" />
                    <input type="hidden" id="hidden_wopi_number" value="" />
                    <input type="hidden" id="hidden_is_non_ord_sample" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}



if($action=="create_wopi_search_list_view")
{
	
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for pi
		{
			$sql_cond .= " and a.pi_number LIKE '%$txt_search_common%'";	
			if( $txt_date_from!="" || $txt_date_to!="" ) 
			{
				if($db_type==0)
				{
					$sql_cond .= " and a.pi_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
				}
				if($db_type==2 || $db_type==1)
				{ 
					$sql_cond .= " and a.pi_date  between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
				}
			}
			if(trim($company)!="") $sql_cond .= " and a.importer_id='$company'";
		}
		else if(trim($txt_search_by)==2) // for wo
		{
			$sql_cond .= " and booking_no LIKE '%$txt_search_common%'";	
			if( $txt_date_from!="" || $txt_date_to!="" ) 
			{
				if($db_type==0)
				{
					$sql_cond .= " and wo_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
				}
				if($db_type==2 || $db_type==1)
				{ 
					$sql_cond .= " and wo_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
				}
			}
			if(trim($company)!="") $sql_cond .= " and company_id='$company'";
		}
 	} 
 	
	if($txt_search_by==1 )
	{
		if($db_type==0)
		{
 		 	$sql = "select a.id as id,a.pi_number as wopi_number,b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source,0 as is_non_ord_sample 
				from com_pi_master_details a left join com_btb_lc_master_details b on FIND_IN_SET(a.id,b.pi_id)
				where  
				a.item_category_id = 3 and
				a.status_active=1 and a.is_deleted=0 and
				a.importer_id=$company 
				$sql_cond";//a.supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
		}
		else
		{	
			$sql = "select a.id as id, a.pi_number as wopi_number,  a.pi_date as wopi_date, a.supplier_id as supplier_id, a.currency_id as currency_id, a.source as source, c.lc_number as lc_number,0 as is_non_ord_sample
				from com_pi_master_details a 
				left join com_btb_lc_pi b on a.id=b.pi_id 
				left join com_btb_lc_master_details c on b.com_btb_lc_master_details_id=c.id
				where 
				a.item_category_id = 3 and
				a.status_active=1 and a.is_deleted=0
				$sql_cond";
		}
	}
	else if($txt_search_by==2)
	{
 		$sql = "select id,booking_no as wopi_number,' ' as lc_number, booking_date as wopi_date,supplier_id as supplier_id,currency_id as currency_id,source as source,0 as is_non_ord_sample 
				from wo_booking_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category=3 and pay_mode!=2 and 
				company_id=$company
				$sql_cond
				union all
			    SELECT id,booking_no as wopi_number,' ' as lc_number, booking_date as wopi_date,supplier_id as supplier_id,currency_id as currency_id,source as source ,1 as is_non_ord_sample
				from wo_non_ord_samp_booking_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category=3 and pay_mode!=2 and 
				company_id=$company
				$sql_cond";//supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
	}
	//echo $sql;die;
	$result = sql_select($sql);
 	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(3=>$supplier_arr,4=>$currency,5=>$source);
	echo  create_list_view("list_view", "WO/PI No, LC ,Date, Supplier, Currency, Source","120,120,120,200,120,120,120","900","260",0, $sql , "js_set_value", "id,wopi_number,is_non_ord_sample", "", 1, "0,0,0,supplier_id,currency_id,source", $arr, "wopi_number,lc_number,wopi_date,supplier_id,currency_id,source", "",'','0,0,0,0,0,0') ;	
	exit();	
	
}


//after select wo/pi number get form data here---------------------------//
if($action=="populate_data_from_wopi_popup")
{
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$hidden_is_non_ord_sample=$ex_data[2];
	$wopiNumber=$ex_data[3];
	
	//echo "shajjad_".$receive_basis.'_'.$hidden_is_non_ord_sample;
	
	if($receive_basis==1 )
	{
		if($db_type==0)
		{
 			$sql = "select b.id as id, b.lc_number as lc_number,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a left join com_btb_lc_master_details b on FIND_IN_SET(a.id,b.pi_id)
				where  
				a.item_category_id = 3 and
				a.status_active=1 and a.is_deleted=0 and
				a.id=$wo_pi_ID";
		}
		else
		{
			$sql = "select c.id as id, c.lc_number as lc_number,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a 
				left join com_btb_lc_pi b on a.id=b.pi_id 
				left join com_btb_lc_master_details c on b.com_btb_lc_master_details_id=c.id
				where  
				a.item_category_id = 3 and
				a.status_active=1 and a.is_deleted=0 and
				a.id=$wo_pi_ID";
		}
	}
	else if($receive_basis==2)
	{
		if($hidden_is_non_ord_sample==0)
		{
			
 		$sql = "select id,'' as lc_number,supplier_id as supplier_id,currency_id as currency_id,source as source 
				from wo_booking_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category = 3 and 
				id=$wo_pi_ID";
		}
		else
		{
			$sql = "select id,'' as lc_number,supplier_id as supplier_id,currency_id as currency_id,source as source 
				from wo_non_ord_samp_booking_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category = 3 and 
				id=$wo_pi_ID";
		}
	}
	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		echo "$('#txt_lc_no').val('".$row[csf("lc_number")]."');\n";
		if($row[csf("lc_number")]!="")
		{
			echo "$('#hidden_lc_id').val(".$row[csf("id")].");\n";
		}
		if($row[csf("currency_id")]==1)
		{
			echo "$('#txt_exchange_rate').val(1);\n";
			echo "$('#txt_exchange_rate').attr('disabled','disabled');\n";
		}
		if($row[csf("currency_id")]!=1)
		{
			$sql1 = sql_select("select exchange_rate,max(id) from inv_receive_master where item_category=3");
			foreach($sql1 as $row1)
			{
				echo "$('#txt_exchange_rate').val(".$row1[csf("exchange_rate")].");\n";
			}
			
			echo "$('#txt_exchange_rate').removeAttr('disabled','disabled');\n";
		}
	}
	if($hidden_is_non_ord_sample==1)
		{
			echo "$('#txt_receive_qty').removeAttr('readonly','readonly');\n";
			echo "$('#txt_receive_qty').removeAttr('onClick','onClick');\n";	
			echo "$('#txt_receive_qty').removeAttr('placeholder','placeholder');\n";		
		}
		else
		{
			echo "$('#txt_receive_qty').attr('readonly','readonly');\n";
			echo "$('#txt_receive_qty').attr('onClick','openmypage_po();');\n";	
			echo "$('#txt_receive_qty').attr('placeholder','Single Click');\n";	
		}
	exit();	
}
if($action=="set_exchange_rate")
{
	$sql1 = sql_select("select exchange_rate,max(id) from inv_receive_master where item_category=3");
	foreach($sql1 as $row1)
	{
		echo $row1[csf("exchange_rate")];
	}
}



//right side product list create here--------------------//
if($action=="show_product_listview")
{ 
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$hidden_is_non_ord_sample=$ex_data[2];
	$wopiNumber=$ex_data[3];
	$composition_arr=array();
	
 	$composition_arr=array();
 	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from  lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
			}
		}
	}

	
	if($receive_basis==1) // pi basis
	{
		if($db_type==0)
		{	
		 	$sql = "select a.pi_number as wopi_number, a.importer_id as company_id, a.supplier_id,b.id,b.determination_id as lib_yarn_count_deter_id, b.weight, b.dia_width, b.color_id,sum(b.quantity) as qnty 
		from com_pi_master_details a left join com_pi_item_details b on FIND_IN_SET(a.id,b.pi_id)
		where a.id=$wo_pi_ID group by b.fabric_construction ,b.fabric_composition, b.weight, b.dia_width, b.color_id "; 
		}
		else
		{  
			
			$sql = "select a.pi_number as wopi_number, a.importer_id as company_id, a.supplier_id,b.id,b.determination_id as lib_yarn_count_deter_id, b.weight, b.dia_width, b.color_id,sum(b.quantity) as qnty
			from com_pi_master_details a, com_pi_item_details b
			where a.id=b.pi_id and a.id=$wo_pi_ID group by b.fabric_construction ,b.fabric_composition, b.weight, b.dia_width, b.color_id,a.supplier_id,b.id,b.determination_id,a.importer_id,a.pi_number";  
		}
	}  
	else if($receive_basis==2) // wo basis
	{
		if($hidden_is_non_ord_sample==0)
		{
			if($db_type==0)
			{
				 $sql="select b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight,a.id,a.booking_no as wopi_number, a.dia_width, a.fabric_color_id,sum(a.grey_fab_qnty) as qnty from wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.booking_no='$wopiNumber' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by a.id,a.booking_no,b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, a.dia_width,a.fabric_color_id";
			}
			else
			{
				$sql="select b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight,a.id,a.booking_no as wopi_number, a.dia_width, a.fabric_color_id,sum(a.grey_fab_qnty) as qnty from wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.booking_no='$wopiNumber' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by a.id,a.booking_no,b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, a.dia_width,a.fabric_color_id";
			}

		}
		else
		{
				$sql="select id,booking_no as wopi_number,lib_yarn_count_deter_id, body_part as body_part_id, gsm_weight, dia_width,fabric_color as fabric_color_id, sum(grey_fabric) as qnty from wo_non_ord_samp_booking_dtls where booking_no='$wopiNumber' and status_active=1 and is_deleted=0 group by id,booking_no,lib_yarn_count_deter_id, body_part, gsm_weight, dia_width,fabric_color";

		}
	}	
	$result = sql_select($sql);
	$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$i=1; 
	?>
     
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0">
        	<thead><tr><th>SL</th><th>Product Name</th><th>Qnty</th></tr></thead>
            <tbody>
            	<?php foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					$fabric_desc=$composition_arr[$row[csf('lib_yarn_count_deter_id')]];	
				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $receive_basis."**".$row[csf("id")]."**".$row[csf("wopi_number")]."**".$hidden_is_non_ord_sample;?>","wo_pi_product_form_input","requires/woven_finish_fabric_receive_controller")' style="cursor:pointer" >
                		<td><?php echo $i; ?></td>
                    	<td><?php echo $fabric_desc; ?></td>
                        <td align="right"><?php echo number_format($row[csf('qnty')],2); ?></td>
                    </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
     </fieldset>   
	<?php	 
	exit();
}


// get form data from product click in right side
if($action=="wo_pi_product_form_input")
{
	$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$wopiNumber=$ex_data[2];
	$hidden_is_non_ord_sample=$ex_data[3];
	$composition_arr=array();
 	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from  lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
			}
		}
	}
	if($receive_basis==1) // pi basis
	{	
		if($db_type==0)
		{
			
		 	$sql = "select a.pi_number as wopi_number, a.importer_id as company_id, a.supplier_id,b.id,b.determination_id as lib_yarn_count_deter_id,b.fabric_construction ,b.fabric_composition, b.weight as gsm_weight, b.dia_width as dia_width, b.color_id as fabric_color_id,sum(b.quantity) as qnty,b.rate 
		from com_pi_master_details a left join com_pi_item_details b on FIND_IN_SET(a.id,b.pi_id)
		where b.id=$wo_pi_ID group by b.fabric_construction ,b.fabric_composition, b.weight, b.dia_width, b.color_id "; 
		}
		else
		{
			$sql = "select a.pi_number as wopi_number, a.importer_id as company_id, a.supplier_id,b.id,b.determination_id as lib_yarn_count_deter_id,b.fabric_construction ,b.fabric_composition, b.weight as gsm_weight, b.dia_width as dia_width, b.color_id as fabric_color_id,sum(b.quantity) as qnty,b.rate
			from com_pi_master_details a, com_pi_item_details b
			where a.id=b.pi_id and b.id=$wo_pi_ID group by a.pi_number, a.importer_id, a.supplier_id,b.id,b.determination_id,b.fabric_construction ,b.fabric_composition, b.weight, b.dia_width, b.color_id,b.rate";  
			
		} 	
	}  
	else if($receive_basis==2) // wo basis
	{
		if($hidden_is_non_ord_sample==0)
		{
			if($db_type==0)
			{
				$sql="select b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight,a.id,a.booking_no as wopi_number, a.dia_width, a.fabric_color_id,sum(a.grey_fab_qnty) as qnty,a.rate from wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.id='$wo_pi_ID' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, a.dia_width,a.fabric_color_id";
			}
			else
			{
				$sql="select b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight,a.id,a.booking_no as wopi_number, a.dia_width, a.fabric_color_id,sum(a.grey_fab_qnty) as qnty from wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.booking_no='$wopiNumber' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by a.id,a.booking_no,b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, a.dia_width,a.fabric_color_id";
			}

		}
		else
		{
			if($db_type==0)
			{
				$sql="select id,booking_no as wopi_number,lib_yarn_count_deter_id, body_part as body_part_id, gsm_weight, dia_width,fabric_color as fabric_color_id, sum(grey_fabric) as qnty,rate from wo_non_ord_samp_booking_dtls where id='$wo_pi_ID' and status_active=1 and is_deleted=0 group by lib_yarn_count_deter_id, body_part, gsm_weight, dia_width,fabric_color";
			}
			else
			{
				$sql="select id,booking_no as wopi_number,lib_yarn_count_deter_id, body_part as body_part_id, gsm_weight, dia_width,fabric_color as fabric_color_id, sum(grey_fabric) as qnty from wo_non_ord_samp_booking_dtls where booking_no='$wopiNumber' and status_active=1 and is_deleted=0 group by id,booking_no,lib_yarn_count_deter_id, body_part, gsm_weight, dia_width,fabric_color";
			}
		}
	}	
	$result = sql_select($sql); 
	foreach($result as $row)
	{ 
		$fabric_desc=$composition_arr[$row[csf('lib_yarn_count_deter_id')]];	
		echo "$('#txt_fabric_description').val('".$fabric_desc."');\n";
		echo "$('#fabric_desc_id').val('".$row[csf("lib_yarn_count_deter_id")]."');\n";
		echo "$('#txt_color').val('".$color_name_arr[$row[csf("fabric_color_id")]]."');\n";
		echo "$('#txt_width').val('".$row[csf("dia_width")]."');\n";
		echo "$('#txt_weight').val(".$row[csf("gsm_weight")].");\n";
		echo "$('#txt_rate').val(".$row[csf("rate")].");\n";
		
		/*$whereCondition = "yarn_count_id=".$row[csf("yarn_count")]." and yarn_comp_type1st=".$row[csf("yarn_comp_type1st")]." and yarn_comp_percent1st=".$row[csf("yarn_comp_percent1st")]."
						and yarn_comp_type2nd=".$row[csf("yarn_comp_type2nd")]." and yarn_comp_percent2nd=".$row[csf("yarn_comp_percent2nd")]." and yarn_type=".$row[csf("yarn_type")]." and color=".$row[csf("color_name")]."
						and company_id=$company_id and supplier_id=$supplier_id and item_category_id=1";
		if($whereCondition=="") $totalRcvQnty=0;
		$totalRcvQnty = return_field_value("sum(current_stock)","product_details_master","$whereCondition");
		$orderQnty = round($row[csf("quantity")])-round($totalRcvQnty);
		echo "$('#txt_order_qty').val(".$orderQnty.");\n";
		echo "control_composition('percent_one');\n";*/
	} 
	exit();	 
}




// LC popup here----------------------// 
if ($action=="lc_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
function js_set_value(str)
{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
		$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
		parent.emailwindow.hide();
}
	
	
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th>
                    	<input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  />
                   		<!-- Hidden field here-------->
                        <input type="hidden" id="hidden_tbl_id" value="" />
                        <input type="hidden" id="hidden_wopi_number" value="hidden_wopi_number" />
                        <!-- ---------END------------->
                    </th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            $search_by_arr=array(0=>'LC Number',1=>'Supplier Name');
							$dd="change_search_event(this.value, '0*1', '0*select id, supplier_name from lib_supplier', '../../') ";							
							echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+<?php echo $company; ?>, 'create_lc_search_list_view', 'search_div', 'woven_finish_fabric_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
           	 	</tr> 
            </tbody>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php

}


if($action=="create_lc_search_list_view")
{
	$ex_data = explode("_",$data);
	$cbo_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$company = $ex_data[2];
	
	if($cbo_search_by==1 && $txt_search_common!="") // lc number
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where lc_number LIKE '%$search_string%' and importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	} 
	else if($cbo_search_by==1 && $txt_search_common!="") //supplier
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where supplier_id='$search_string' and importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	} 
	else
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	}
	
	$company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$company_arr,2=>$supplier_arr,3=>$item_category);
	echo  create_list_view("list_view", "LC No,Importer,Supplier Name,Item Category,Value","120,150,150,120,120","750","260",0, $sql , "js_set_value", "id,lc_number", "", 1, "0,importer_id,supplier_id,item_category_id,0", $arr, "lc_number,importer_id,supplier_id,item_category_id,lc_value", "",'','0,0,0,0,0,1') ;	
	exit();
	
}


if($action=="show_ile")
{
	$ex_data = explode("**",$data);
	$company = $ex_data[0];
	$source = $ex_data[1];
	$rate = $ex_data[2];
	
	$sql="select standard from variable_inv_ile_standard where source='$source' and company_name='$company' and category=3 and status_active=1 and is_deleted=0 order by id";
	//echo "saju1_".$sql;
	$result=sql_select($sql);
	foreach($result as $row)
	{
		// NOTE :- ILE=standard, ILE% = standard/100*rate
		$ile = $row[csf("standard")];
		$ile_percentage = ( $row[csf("standard")]/100 )*$rate;
		echo $ile."**".number_format($ile_percentage,$dec_place[3],".","");
		exit();
	}
	exit();
}



//data save update delete here------------------------------//
if($action=="save_update_delete")
{	  
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		check_table_status( $_SESSION['menu_id'],0);
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		 
		//---------------Check Color---------------------------//
		
			$color_library=return_library_array( "select id,color_name from lib_color",'id','color_name');
			$txt_color = return_id( str_replace("'","",$txt_color), $color_library, "lib_color", "id,color_name");
		
		//----------------Check Color END---------------------//
		
		
		//---------------Check fabric Type---------------------------//
		if( str_replace("'","",$txt_fabric_type)!="" )
		{
			$woben_fabric_type_library = return_library_array( "select id,fabric_type from lib_woben_fabric_type",'id','fabric_type');
			$txt_fabric_type = return_id( str_replace("'","",$txt_fabric_type), $woben_fabric_type_library, "lib_woben_fabric_type", "id,fabric_type");
		}
		//----------------Check fabric Type END---------------------//
		
		
		//---------------Check Product ID --------------------------//
		//return_product_id($txt_fabric_type,$txt_fabric_description,$fabric_desc_id,$txt_color,$txt_width,$txt_weight,$company,$supplier,$store,$uom,$prodCode)
 		$rtnString =  return_product_id($txt_fabric_type,$txt_fabric_description,$fabric_desc_id,$txt_color,$txt_width,$txt_weight,$cbo_company_name,$cbo_supplier,$cbo_store_name,$cbo_uom,$txt_prod_code);
 		$expString = explode("***",$rtnString); 
		
		//echo $expString[1].'<br>'.$expString[2];die;;
		
		if($expString[0]==true && $expString[0]!="")
		{
			$prodMSTID = $expString[1];
		}
		else
		{
			$field_array = $expString[1];
			$data_array = $expString[2];		
			//$insertR = sql_insert("product_details_master",$field_array,$data_array,1); 
			//if($db_type==0)	{ mysql_query("COMMIT");} 	
			//if($db_type==0)	{ mysql_query("BEGIN"); }
			$prodMSTID = $expString[3];
		}
		

		 
		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_receive_master a, inv_transaction b","a.id=b.mst_id and a.recv_number=$txt_mrr_no and b.prod_id=$prodMSTID and b.transaction_type=1"); 
		/*if($duplicate==1 && str_replace("'","",$txt_mrr_no)=="") 
		{
			check_table_status( $_SESSION['menu_id'],0);
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			die;
		}*/
		//------------------------------Check Brand END---------------------------------------//
		 
		
		
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value from product_details_master where id=$prodMSTID");
		$presentStock=$presentStockValue=$presentAvgRate=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$product_name_details 	=$result[csf("product_name_details")];
		}		 
		//----------------Check Product ID END---------------------//
	 		
		
		/*if(str_replace("'","",$txt_mrr_no)!="")
		{
			$new_recv_number[0] = str_replace("'","",$txt_mrr_no);
			$id=return_field_value("id","inv_receive_master","recv_number=$txt_mrr_no");
			//yarn master table UPDATE here START----------------------//		
			$field_array="item_category*receive_basis*receive_purpose*receive_date*challan_no*store_id*exchange_rate*currency_id*supplier_id*lc_no*source*updated_by*update_date";
			$data_array="1*".$cbo_receive_basis."*".$cbo_receive_purpose."*".$txt_receive_date."*".$txt_challan_no."*".$cbo_store_name."*".$txt_exchange_rate."*".$cbo_currency."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_source."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
			$rID=sql_update("inv_receive_master",$field_array,$data_array,"id",$id,1);	
			//yarn master table UPDATE here END---------------------------------------// 
		}
		else  	
		{	
			// yarn master table entry here START---------------------------------------//		
			$id=return_next_id("id", "inv_receive_master", 1);		
			$new_recv_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'YRV', date("Y",time()), 5, "select recv_number_prefix,recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='1' order by recv_number_prefix_num DESC ", "recv_number_prefix", "recv_number_prefix_num" )); 
			
 			$field_array="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, company_id, receive_basis, receive_purpose, receive_date, challan_no, store_id, exchange_rate, currency_id, supplier_id, lc_no, source, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_recv_number[1]."','".$new_recv_number[2]."','".$new_recv_number[0]."',1,1,".$cbo_company_name.",".$cbo_receive_basis.",".$cbo_receive_purpose.",".$txt_receive_date.",".$txt_challan_no.",".$cbo_store_name.",".$txt_exchange_rate.",".$cbo_currency.",".$cbo_supplier.",".$hidden_lc_id.",".$cbo_source.",'".$user_id."','".$pc_date_time."')";
			//echo $field_array."<br>".$data_array;die;
			$rID=sql_insert("inv_receive_master",$field_array,$data_array,1); 		
			// yarn master table entry here END---------------------------------------// 
		}*/
		
		
		$woben_recv_num=''; $woben_update_id=''; $flag=1;
		
		if(str_replace("'","",$update_id)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$new_woven_finish_recv_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'WFR', date("Y",time()), 5, "select recv_number_prefix, recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='17' and $year_cond=".date('Y',time())." order by id desc ", "recv_number_prefix", "recv_number_prefix_num" ));
		 	
			$id=return_next_id( "id", "inv_receive_master", 1 ) ;
					 
			$field_array1="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, receive_basis, company_id, receive_date, challan_no, booking_id, booking_no, booking_without_order, store_id, location_id,supplier_id,lc_no, currency_id,exchange_rate, source, inserted_by, insert_date";
			
			$data_array1="(".$id.",'".$new_woven_finish_recv_system_id[1]."',".$new_woven_finish_recv_system_id[2].",'".$new_woven_finish_recv_system_id[0]."',17,3,".$cbo_receive_basis.",".$cbo_company_name.",".$txt_receive_date.",".$txt_challan_no.",".$txt_wo_pi_id.",".$txt_wo_pi.",".$booking_without_order.",".$cbo_store_name.",".$cbo_location_name.",".$cbo_supplier.",".$hidden_lc_id.",".$cbo_currency.",".$txt_exchange_rate.",".$cbo_source.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into pro_grey_prod_entry_mst (".$field_array.") values ".$data_array;die;
			//$rID=sql_insert("inv_receive_master",$field_array1,$data_array1,0);
			
			//if($rID) $flag=1; else $flag=0;
			
			$woben_recv_num=$new_woven_finish_recv_system_id[0];
			$woben_update_id=$id;
		}
		else
		{
			$field_array_update="receive_basis*receive_date*challan_no*booking_id*booking_no*booking_without_order*store_id*location_id*supplier_id*lc_no*currency_id*exchange_rate*source*updated_by*update_date";
			
			$data_array_update=$cbo_receive_basis."*".$txt_receive_date."*".$txt_challan_no."*".$txt_wo_pi_id."*".$txt_wo_pi."*".$booking_without_order."*".$cbo_store_name."*".$cbo_location_name."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_currency."*".$txt_exchange_rate."*".$cbo_source."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			/*$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($flag==1) 
			{
				if($rID) $flag=1; else $flag=0; 
			} */
			
			$woben_recv_num=str_replace("'","",$txt_mrr_no);
			$woben_update_id=str_replace("'","",$update_id);
		}
		
		
		
		// yarn details table entry here START-----------------------------------//		
		$rate = str_replace("'","",$txt_rate);
		$txt_ile = str_replace("'","",$txt_ile);
		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$ile = ($txt_ile/$rate)*100; // ile cost to ile
		$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
		$exchange_rate = str_replace("'","",$txt_exchange_rate);
		$conversion_factor = 1; // woven Fabric always Yds	
		$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
 		$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
		$con_amount = $cons_rate*$txt_receive_qty;
		$con_ile = $ile;//($ile/$domestic_rate)*100;
		$con_ile_cost = ($ile/100)*($rate*$exchange_rate);
		
		$dtlsid = return_next_id("id", "inv_transaction", 1);		 
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$field_array2 = "id,mst_id,receive_basis,pi_wo_batch_no,company_id,supplier_id,prod_id,product_code,item_category,transaction_type,transaction_date,store_id,order_id, order_uom, order_qnty, order_rate, order_ile,order_ile_cost, order_amount, cons_uom, cons_quantity, cons_rate, cons_ile, cons_ile_cost, cons_amount,balance_qnty, balance_amount,room,rack, self,bin_box, batch_lot,inserted_by,insert_date";
 		$data_array2 = "(".$dtlsid.",".$woben_update_id.",".$cbo_receive_basis.",".$txt_wo_pi_id.",".$cbo_company_name.",".$cbo_supplier.",".$prodMSTID.",".$txt_prod_code.",3,1,".$txt_receive_date.",".$cbo_store_name.",".$all_po_id.",".$cbo_uom.",".$txt_receive_qty.",".$txt_rate.",".$ile.",".$ile_cost.",".$txt_amount.",".$cbo_uom.",".$txt_receive_qty.",".$cons_rate.",".$con_ile.",".$con_ile_cost.",".$con_amount.",".$txt_receive_qty.",".$con_amount.",".$txt_room.",".$txt_rack.",".$txt_self.",".$txt_binbox.",".$txt_batch_lot.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
		//echo "INSERT INTO inv_transaction (".$field_array.") VALUES ".$data_array.""; die;
		//echo $field_array."<br>".$data_array;die;
		/*$dtlsrID = sql_insert("inv_transaction",$field_array2,$data_array2,1);
		if($flag==1) 
		{
			if($dtlsrID) $flag=1; else $flag=0; 
		} */
		//yarn details table entry here END-----------------------------------//
		
		
		//product master table data UPDATE START----------------------------------------------------------//	
		$stock_value 	= $domestic_rate*$txt_receive_qty;
  		$currentStock 	= $presentStock+$txt_receive_qty;
		$StockValue	 	= $presentStockValue+$stock_value;
		$avgRate		= $StockValue/$currentStock;
 		$field_array3="avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*updated_by*update_date";
 		$data_array3="".number_format($avgRate,$dec_place[3],".","")."*".$txt_receive_qty."*".$currentStock."*".number_format($StockValue,$dec_place[4],".","")."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'";
		/*$prodUpdate = sql_update("product_details_master",$field_array3,$data_array3,"id",$prodMSTID,1);
		if($flag==1) 
		{
			if($prodUpdate) $flag=1; else $flag=0; 
		}*/
		//------------------ product_details_master END---------------------------------------------------//
		
		//echo "20**".$rID." && ".$dtlsrID." && ".$prodUpdate;mysql_query("ROLLBACK"); die;
		
		$field_array_roll="id, mst_id,dtls_id, po_breakdown_id, entry_form, qnty, roll_no, inserted_by, insert_date";
		
		$save_string=explode(",",str_replace("'","",$save_data));
		
		$po_array=array();
		
		for($i=0;$i<count($save_string);$i++)
		{
			$order_dtls=explode("**",$save_string[$i]);
			
			$order_id=$order_dtls[0];
			$order_qnty_roll_wise=$order_dtls[1];
			$roll_no=$order_dtls[2];
			
			if($i==0) $add_comma=""; else $add_comma=",";
			
			if( $id_roll=="" ) $id_roll = return_next_id( "id", "pro_roll_details", 1 ); else $id_roll = $id_roll+1;
			
			$data_array_roll.="$add_comma(".$id_roll.",".$woben_update_id.",".$dtlsid.",'".$order_id."',5,'".$order_qnty_roll_wise."','".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			if(array_key_exists($order_id,$po_array))
			{
				$po_array[$order_id]+=$order_qnty_roll_wise;
			}
			else
			{
				$po_array[$order_id]=$order_qnty_roll_wise;
			}
		}
		
		//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
		/*if($data_array_roll!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$booking_without_order)==0)
		{
			$rID5=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			} 
		}*/
		
		$field_array_proportionate="id, trans_id, trans_type, entry_form, po_breakdown_id, prod_id,color_id, quantity, inserted_by, insert_date";

		$i=0;
		foreach($po_array as $key=>$val)
		{
			if($i==0) $add_comma=""; else $add_comma=",";
			
			if( $id_prop=="" ) $id_prop = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_prop = $id_prop+1;
			
			$order_id=$key;
			$order_qnty=$val;
			
			$data_array_prop.="$add_comma(".$id_prop.",".$dtlsid.",1,17,'".$order_id."','".$prodMSTID."','".$txt_color."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			$i++;
		}
		
		//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;
		
		//fast	
		
		if($expString[0]==true && $expString[0]!="")
		{
			//$prodMSTID = $expString[1];
		}
		else
		{
			//$field_array = $expString[1];
			//$data_array = $expString[2];
				
			$insertR = sql_insert("product_details_master",$field_array,$data_array,1); 
			//if($db_type==0)	{ mysql_query("COMMIT");} 	
			//if($db_type==0)	{ mysql_query("BEGIN"); }
			//$prodMSTID = $expString[3];
		}
		
		if($duplicate==1 && str_replace("'","",$txt_mrr_no)=="") 
		{
			check_table_status( $_SESSION['menu_id'],0);
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			die;
		}
		
		if(str_replace("'","",$update_id)=="")
		{
			
			
			$rID=sql_insert("inv_receive_master",$field_array1,$data_array1,0);
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($flag==1) 
			{
				if($rID) $flag=1; else $flag=0; 
			} 
		}
		
		
		
		
		
		$dtlsrID = sql_insert("inv_transaction",$field_array2,$data_array2,1);
		if($flag==1) 
		{
			if($dtlsrID) $flag=1; else $flag=0; 
		} 
		
		$prodUpdate = sql_update("product_details_master",$field_array3,$data_array3,"id",$prodMSTID,1); 
		if($flag==1) 
		{
			if($prodUpdate) $flag=1; else $flag=0; 
		}
		
		//echo $data_array_roll.'**'.str_replace("'","",$roll_maintained).'**'.str_replace("'","",$booking_without_order);die;
		
		if($data_array_roll!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$booking_without_order)==0)
		{
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;
			
			$rID5=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			} 
		}
		
		if($data_array_prop!="" && str_replace("'","",$booking_without_order)==0)
		{
			
			//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;
			
			$rID6=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
			if($flag==1) 
			{
				if($rID6) $flag=1; else $flag=0; 
			} 
		}
		
		//last	
 		
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$woben_update_id."**".$woben_recv_num."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "0**".$woben_update_id."**".$woben_recv_num."**0";
			}
			else
			{	
				oci_rollback($con);
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		check_table_status( $_SESSION['menu_id'],0);
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		if( str_replace("'","",$update_id) == "" )
		{
			echo "15";exit(); 
		}
		
				
		//previous product stock adjust here--------------------------//
		//product master table UPDATE here START ---------------------//
		$sql = sql_select("select a.prod_id,a.cons_quantity,a.cons_rate,a.cons_amount,b.avg_rate_per_unit,b.current_stock,b.stock_value from inv_transaction a, product_details_master b where a.id=$update_dtls_id and a.prod_id=b.id");
		$before_prod_id=$before_receive_qnty=$before_rate=$beforeAmount=$before_brand="";
		$beforeStock=$beforeStockValue=$beforeAvgRate=0;
		foreach( $sql as $row)
		{
			$before_prod_id 		= $row[csf("prod_id")]; 
			$before_receive_qnty 	= $row[csf("cons_quantity")]; //stock qnty
			$before_rate 			= $row[csf("cons_rate")]; 
			$beforeAmount			= $row[csf("cons_amount")]; //stock value
			
			$before_brand 			=$row[csf("brand")];
			$beforeStock			=$row[csf("current_stock")];
			$beforeStockValue		=$row[csf("stock_value")];
			$beforeAvgRate			=$row[csf("avg_rate_per_unit")];	
		}
		
		//stock value minus here---------------------------//
		$adj_beforeStock			=$beforeStock-$before_receive_qnty;
		$adj_beforeStockValue		=$beforeStockValue-$beforeAmount;
		$adj_beforeAvgRate			=number_format(($adj_beforeStockValue/$adj_beforeStock),$dec_place[3],'.','');	
		
		//$beforeStockAdjSQL = sql_update("product_details_master","avg_rate_per_unit*current_stock*stock_value","$adj_beforeAvgRate*$adj_beforeStock*$adj_beforeStockValue","id",$before_prod_id,1);			
		//product master table UPDATE here END   ---------------------//
		//----------------- END PREVIOUS STOCK ADJUST-----------------//
				
		 	
		
		//---------------Check Color---------------------------//
		
			$color_library=return_library_array( "select id,color_name from lib_color",'id','color_name');
			$txt_color = return_id( str_replace("'","",$txt_color), $color_library, "lib_color", "id,color_name");
		
		//----------------Check Color END---------------------//
		
		
		//---------------Check fabric Type---------------------------//
		if( str_replace("'","",$txt_fabric_type)!="" )
		{
			$woben_fabric_type_library = return_library_array( "select id,fabric_type from lib_woben_fabric_type",'id','fabric_type');
			$txt_fabric_type = return_id( str_replace("'","",$txt_fabric_type), $woben_fabric_type_library, "lib_woben_fabric_type", "id,fabric_type");
		}
		//----------------Check fabric Type END---------------------//
		
		 
		//---------------Check Product ID --------------------------//
		$rtnString =  return_product_id($txt_fabric_type,$txt_fabric_description,$fabric_desc_id,$txt_color,$txt_width,$txt_weight,$cbo_company_name,$cbo_supplier,$cbo_store_name,$cbo_uom,$txt_prod_code);
 		$expString = explode("***",$rtnString); 
		if($expString[0]==true && $expString[0]!="")
		{
			$prodMSTID = $expString[1];
		}
		else
		{
			$field_array = $expString[1];
			$data_array = $expString[2];		
			//$insertR = sql_insert("product_details_master",$field_array,$data_array,1); 
			//if($db_type==0)	{ mysql_query("COMMIT");} 	
			//if($db_type==0)	{ mysql_query("BEGIN"); }
			$prodMSTID = $expString[3];
		}
		
		//current product stock-------------------------//
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value from product_details_master where id=$prodMSTID");
		$presentStock=$presentStockValue=$presentAvgRate=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$product_name_details 	=$result[csf("product_name_details")];
		}	 
		//----------------Check Product ID END---------------------//
 		
		
		//yarn master table UPDATE here START----------------------//		
		/*$field_array="item_category*receive_basis*receive_date*challan_no*store_id*exchange_rate*currency_id*supplier_id*lc_no*source*updated_by*update_date";
		$data_array="1*".$cbo_receive_basis."*".$txt_receive_date."*".$txt_challan_no."*".$cbo_store_name."*".$txt_exchange_rate."*".$cbo_currency."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_source."*'".$user_id."'*'".$pc_date_time."'";
 		//echo $field_array."<br>".$data_array;die;
 		$rID=sql_update("inv_receive_master",$field_array,$data_array,"recv_number",$txt_mrr_no,1);	*/
		$flag=1;
		$field_array_update="receive_basis*receive_date*challan_no*booking_id*booking_no*booking_without_order*store_id*location_id*supplier_id*lc_no*currency_id*exchange_rate*source*updated_by*update_date";
		$data_array_update=$cbo_receive_basis."*".$txt_receive_date."*".$txt_challan_no."*".$txt_wo_pi_id."*".$txt_wo_pi."*".$booking_without_order."*".$cbo_store_name."*".$cbo_location_name."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_currency."*".$txt_exchange_rate."*".$cbo_source."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		/*$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		}*/
		//yarn master table UPDATE here END---------------------------------------// 
		
			
		
		// yarn details table UPDATE here START-----------------------------------//		
		$rate = str_replace("'","",$txt_rate);
		$txt_ile = str_replace("'","",$txt_ile);
		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$ile = ($txt_ile/$rate)*100; // ile cost to ile
		$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
		$exchange_rate = str_replace("'","",$txt_exchange_rate);
		$conversion_factor = 1; // yarn always KG	
		$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
 		$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
		
		$con_amount = $cons_rate*$txt_receive_qty;
		$con_ile = $ile;  
		$con_ile_cost = ($ile/100)*($rate*$exchange_rate);
		//echo "20**".$con_ile_cost; mysql_query("ROLLBACK"); die;
 		 
		$field_array1 = "receive_basis*pi_wo_batch_no*company_id*supplier_id*prod_id*product_code*item_category*transaction_type*transaction_date*store_id*order_id*order_uom*order_qnty*order_rate*order_ile*order_ile_cost*order_amount*cons_uom*cons_quantity*cons_rate*cons_ile*cons_ile_cost*cons_amount*balance_qnty*balance_amount*room*rack*self*bin_box*batch_lot*updated_by*update_date";
 		$data_array1 = "".$cbo_receive_basis."*".$txt_wo_pi_id."*".$cbo_company_name."*".$cbo_supplier."*".$prodMSTID."*".$txt_prod_code."*3*1*".$txt_receive_date."*".$cbo_store_name."*".$all_po_id."*".$cbo_uom."*".$txt_receive_qty."*".$txt_rate."*".$ile."*".$ile_cost."*".$txt_amount."*".$cbo_uom."*".$txt_receive_qty."*".$cons_rate."*".$con_ile."*".$con_ile_cost."*".$con_amount."*".$txt_receive_qty."*".$con_amount."*".$txt_room."*".$txt_rack."*".$txt_self."*".$txt_binbox."*".$txt_batch_lot."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
 		/*$dtlsrID = sql_update("inv_transaction",$field_array1,$data_array1,"id",$update_dtls_id,1); 
		if($flag==1) 
		{
			if($dtlsrID) $flag=1; else $flag=0; 
		} */
		//yarn details table UPDATE here END-----------------------------------//
		
    		
		//product master table data UPDATE START----------------------------------------------------------// 
		$field_array2="avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*updated_by*update_date";
		if($before_prod_id==$prodMSTID)
		{
			$currentStock	=$adj_beforeStock+$txt_receive_qty;
			$StockValue		=$adj_beforeStockValue+($domestic_rate*$txt_receive_qty);
			$avgRate		=number_format($StockValue/$currentStock,$dec_place[3],'.','');
 			$data_array2 = "".$avgRate."*".$txt_receive_qty."*".$currentStock."*".number_format($StockValue,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'";
			/*$prodUpdate = sql_update("product_details_master",$field_array2,$data_array2,"id",$prodMSTID,1);
			if($flag==1) 
			{
				if($prodUpdate) $flag=1; else $flag=0; 
			}*/ 
		}
		else
		{ 
			//before
			$updateID_array=$update_data=array();
			$updateID_array[]=$before_prod_id;
			$update_data[$before_prod_id]=explode("*",("".$adj_beforeAvgRate."*0*".$adj_beforeStock."*".number_format($adj_beforeStockValue,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
			//current			 
 			$presentStock 			= $presentStock+$txt_receive_qty;
			$presentStockValue	 	= $presentStockValue+($domestic_rate*$txt_receive_qty);
			$presentAvgRate			= number_format($presentStockValue/$presentStock,$dec_place[3],'.','');
			$updateID_array[]=$prodMSTID;
			$update_data[$prodMSTID]=explode("*",("".$presentAvgRate."*0*".$presentStock."*".number_format($presentStockValue,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
			/*$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$field_array2,$update_data,$updateID_array));
			if($flag==1) 
			{
				if($prodUpdate) $flag=1; else $flag=0; 
			} */
		}
		//------------------ product_details_master END---------------------------------------------------//
		//echo "20**".$beforeAmount."==".$StockValue."==".$avgRate;mysql_query("ROLLBACK");die; 
		
		
		$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, inserted_by, insert_date";
		
		$save_string=explode(",",str_replace("'","",$save_data));
		
		$po_array=array();
		$not_delete_roll_table_id=''; $j=0;
		
		for($i=0;$i<count($save_string);$i++)
		{
			$order_dtls=explode("**",$save_string[$i]);
			
			$order_id=$order_dtls[0];
			$order_qnty_roll_wise=$order_dtls[1];
			$roll_no=$order_dtls[2];
			$roll_not_delete_id=$order_dtls[3];
			$roll_id=$order_dtls[4];

			if($roll_not_delete_id==0)
			{
				if($j==0) $add_comma=""; else $add_comma=",";
				
				if( $id_roll=="" ) $id_roll = return_next_id( "id", "pro_roll_details", 1 ); else $id_roll = $id_roll+1;
				
				$data_array_roll.="$add_comma(".$id_roll.",".$update_id.",".$update_dtls_id.",'".$order_id."',5,'".$order_qnty_roll_wise."','".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$j++;
			}
			else
			{
				if($not_delete_roll_table_id=="") $not_delete_roll_table_id=$roll_id; else $not_delete_roll_table_id.=",".$roll_id;
			}
			
			if(array_key_exists($order_id,$po_array))
			{
				$po_array[$order_id]+=$order_qnty_roll_wise;
			}
			else
			{
				$po_array[$order_id]=$order_qnty_roll_wise;
			}
		}
		
		/*if(str_replace("'","",$roll_maintained)==1)
		{
			if($not_delete_roll_table_id=="") $delete_cond=""; else $delete_cond="and id not in($not_delete_roll_table_id)"; 
	
			$delete_roll=execute_query( "delete from pro_roll_details where dtls_id=$update_dtls_id and entry_form=5 $delete_cond",0);
			if($flag==1) 
			{
				if($delete_roll) $flag=1; else $flag=0; 
			} 
			
			//echo $flag;
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
			if($data_array_roll!="" && str_replace("'","",$booking_without_order)!=1)
			{
				$rID6=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
				if($flag==1) 
				{
					if($rID6) $flag=1; else $flag=0; 
				} 
			}
		}*/
		
		/*$delete_prop=execute_query( "delete from order_wise_pro_details where trans_id=$update_dtls_id  and entry_form=17",0);
		if($flag==1) 
		{
			if($delete_prop) $flag=1; else $flag=0; 
		}*/
		
		$field_array_proportionate="id, trans_id, trans_type, entry_form, dtls_id, po_breakdown_id, prod_id,color_id, quantity, inserted_by, insert_date";

		$i=0;
		foreach($po_array as $key=>$val)
		{
			if($i==0) $add_comma=""; else $add_comma=",";
			
			if( $id_prop=="" ) $id_prop = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_prop = $id_prop+1;
			
			$order_id=$key;
			$order_qnty=$val;
			
			$data_array_prop.="$add_comma(".$id_prop.",".$update_dtls_id.",1,17,".$update_dtls_id.",'".$order_id."','".$prod_id."','".$txt_color."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			$i++;
		}
		//fast
		
		if($expString[0]==true && $expString[0]!="")
		{
			//$prodMSTID = $expString[1];
		}
		else
		{
			//$field_array = $expString[1];
			//$data_array = $expString[2];		
			$insertR = sql_insert("product_details_master",$field_array,$data_array,1); 
			//if($db_type==0)	{ mysql_query("COMMIT");} 	
			//if($db_type==0)	{ mysql_query("BEGIN"); }
			//$prodMSTID = $expString[3];
		}
		
		//echo "insert into inv_receive_master (".$field_array_update.") values ".$data_array_update;die;	
		
		$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		}
		
		$dtlsrID = sql_update("inv_transaction",$field_array1,$data_array1,"id",$update_dtls_id,1); 
		if($flag==1) 
		{
			if($dtlsrID) $flag=1; else $flag=0; 
		} 
		
		if($before_prod_id==$prodMSTID)
		{
			$prodUpdate = sql_update("product_details_master",$field_array2,$data_array2,"id",$prodMSTID,1);
			if($flag==1) 
			{
				if($prodUpdate) $flag=1; else $flag=0; 
			} 
		}
		else
		{
			$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$field_array2,$update_data,$updateID_array));
			if($flag==1) 
			{
				if($prodUpdate) $flag=1; else $flag=0; 
			} 
		}
		
		if(str_replace("'","",$roll_maintained)==1)
		{
			if($not_delete_roll_table_id=="") $delete_cond=""; else $delete_cond="and id not in($not_delete_roll_table_id)"; 
	
			$delete_roll=execute_query( "delete from pro_roll_details where dtls_id=$update_dtls_id and entry_form=5 $delete_cond",0);
			if($flag==1) 
			{
				if($delete_roll) $flag=1; else $flag=0; 
			} 
			
			//echo $flag;
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
			if($data_array_roll!="" && str_replace("'","",$booking_without_order)!=1)
			{
				$rID6=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
				if($flag==1) 
				{
					if($rID6) $flag=1; else $flag=0; 
				} 
			}
		}
		
		
		
		$delete_prop=execute_query( "delete from order_wise_pro_details where trans_id=$update_dtls_id  and entry_form=17",0);
		if($flag==1) 
		{
			if($delete_prop) $flag=1; else $flag=0; 
		}
		
		
		if($data_array_prop!="" && str_replace("'","",$booking_without_order)!=1)
		{
			//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;	
			
			$rID7=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
			if($flag==1) 
			{
				if($rID7) $flag=1; else $flag=0; 
			} 
		}
		
		//last
  		
 		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
			else
			{
				oci_rollback($con);
				echo "6**0**"."&nbsp;"."**0";
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		// master table delete here---------------------------------------
		$mst_id = return_field_value("id","inv_receive_master","recv_number like $txt_recv_number");	
		if($mst_id=="" || $mst_id==0){ echo "15**0"; die;}
 		$rID = sql_update("inv_receive_master",'status_active*is_deleted','0*1',"id*item_category","$mst_id*1",1);
		$dtlsrID = sql_update("inv_transaction",'status_active*is_deleted','0*1',"mst_id*item_category","$mst_id*1",1);
		
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_mrr_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_mrr_no);
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID)
			{
				oci_commit($con); 
				echo "2**".str_replace("'","",$txt_mrr_no);
			}
			else
			{
				oci_rollback($con);
				echo "2**".str_replace("'","",$txt_mrr_no);
			}
		}
		disconnect($con);
		die;
	}		
}




if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
 							 echo create_drop_down( "cbo_supplier", 150, "select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td>
                        <?php  
                            $search_by = array(1=>'MRR No',2=>'Challan No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 120, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'woven_finish_fabric_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="hidden_recv_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}




if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
	
	 
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for mrr
		{
			$sql_cond .= " and a.recv_number LIKE '%$txt_search_common%'";	
			
		}
		else if(trim($txt_search_by)==2) // for chllan no
		{
			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";				
 		}		 
		 
 	} 
	
	if( $txt_date_from!="" || $txt_date_to!="" ) 
	{	
		if($db_type==0)
		{
			$sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		if($db_type==2 || $db_type==1)
		{ 
			$sql_cond .= " and a.receive_date  between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'";
		}
	}
	
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	if(trim($supplier)!=0) $sql_cond .= " and a.supplier_id='$supplier'";
	
	$sql = "select a.recv_number,a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis,sum(b.cons_quantity) as receive_qnty from inv_transaction b, inv_receive_master a left join com_btb_lc_master_details c on a.lc_no=c.id where a.id=b.mst_id and a.entry_form=17 and a.status_active=1 $sql_cond group by b.mst_id,a.recv_number,a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis";
	//echo $sql;
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$supplier_arr,5=>$receive_basis_arr);
	echo create_list_view("list_view", "MRR No, Supplier Name, Challan No, LC No, Receive Date, Receive Basis, Receive Qnty","120,120,120,120,120,100,100","900","260",0, $sql , "js_set_value", "recv_number", "", 1, "0,supplier_id,0,0,0,receive_basis,0", $arr, "recv_number,supplier_id,challan_no,lc_number,receive_date,receive_basis,receive_qnty", "",'','0,0,0,0,0,0,1') ;	
	exit();
	
}




if($action=="populate_data_from_data")
{
	
	$sql = "select id, recv_number, company_id, receive_basis, booking_id, booking_no,booking_without_order, location_id, receive_date, challan_no, store_id, lc_no, supplier_id, exchange_rate, currency_id, lc_no,source from inv_receive_master where recv_number='$data' and entry_form=17";
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#update_id').val(".$row[csf("id")].");\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		echo "$('#cbo_receive_basis').val(".$row[csf("receive_basis")].");\n";
		echo "$('#txt_wo_pi').val('".$row[csf("booking_no")]."');\n";
		echo "$('#txt_wo_pi_id').val(".$row[csf("booking_id")].");\n";
		echo "$('#booking_without_order').val(".$row[csf("booking_without_order")].");\n";
		echo "$('#cbo_location_name').val(".$row[csf("location_id")].");\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("receive_date")])."');\n";
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		echo "$('#txt_exchange_rate').val(".$row[csf("exchange_rate")].");\n";
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		echo "$('#hidden_lc_id').val(".$row[csf("lc_no")].");\n";
		$lcNumber = return_field_value("lc_number","com_btb_lc_master_details","id=".$row[csf("lc_no")]."");
		echo "$('#txt_lc_no').val('".$lcNumber."');\n";
		echo "show_list_view('".$row[csf("recv_number")]."**".$row[csf("id")]."','show_dtls_list_view','list_container_yarn','requires/woven_finish_fabric_receive_controller','');\n";
 	}
	exit();	
}




if($action=="show_dtls_list_view")
{
	$ex_data = explode("**",$data);
	$recv_number = $ex_data[0];
	$rcv_mst_id = $ex_data[1];
	$cond="";
	if($recv_number!="") $cond .= " and a.recv_number='$recv_number'";
	if($rcv_mst_id!="") $cond .= " and a.id='$rcv_mst_id'";
	 $sql = "select a.recv_number, b.id, b.receive_basis, b.pi_wo_batch_no, c.product_name_details, c.lot, b.order_uom, b.order_qnty, b.order_rate, b.order_ile_cost, b.order_amount,    b.cons_amount,b.batch_lot from inv_receive_master a, inv_transaction b,  product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category=3 and    a.entry_form=17 $cond";
	//echo $sql;		
	$result = sql_select($sql);
	$i=1;
	$totalQnty=0;
	$totalAmount=0;
	$totalbookCurr=0;
	?>
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:1000px" >
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>WO/PI No</th>
                    <th>MRR No</th>
                    <th>Product Details</th>
                    <th>Batch/Lot</th>
                    <th>UOM</th>
                    <th>Receive Qty</th>
                    <th>Rate</th>
                    <th>ILE Cost</th>
                    <th>Amount</th>
                    <th>Book Currency</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
				    foreach($result as $row)
					{  
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					$wopi="";
					if($row[csf("receive_basis")]==1)
						$wopi=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("pi_wo_batch_no")]."");	
					else if($row[csf("receive_basis")]==2)
						$wopi=return_field_value("booking_no","wo_booking_mst","id=".$row[csf("pi_wo_batch_no")]."");	
					$totalQnty +=$row[csf("order_qnty")];
					$totalAmount +=$row[csf("order_amount")];
					$totalbookCurr +=$row[csf("cons_amount")];	
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>","child_form_input_data","requires/woven_finish_fabric_receive_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $wopi; ?></p></td>
                        <td width="100"><p><?php echo $row[csf("recv_number")]; ?></p></td>
                        <td width="200"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td width="80"><p><?php echo $row[csf("batch_lot")]; ?></p></td>
                        <td width="70"><p><?php echo $unit_of_measurement[$row[csf("order_uom")]]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("order_qnty")]; ?></p></td>
                        <td width="60" align="right"><p><?php echo $row[csf("order_rate")]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("order_ile_cost")]; ?></p></td>                       
                        <td width="70" align="right"><p><?php echo $row[csf("order_amount")]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("cons_amount")]; ?></p></td>
                   </tr>
                   <?php $i++; } ?>
                	<tfoot>
                        <th colspan="6">Total</th>                         
                        <th><?php echo $totalQnty; ?></th>
                        <th colspan="2"></th>
                        <th><?php echo $totalAmount; ?></th>
                        <th><?php echo $totalbookCurr; ?></th>
                        <th></th>
                  </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}


if($action=="child_form_input_data")
{
	$rcv_dtls_id = $data;	
	$color_library=return_library_array( "select id,color_name from lib_color",'id','color_name');
	$woben_fabric_type_library = return_library_array( "select id,fabric_type from lib_woben_fabric_type",'id','fabric_type');
	
	$sql = "select a.company_id, a.currency_id, a.exchange_rate, a.booking_without_order, a.booking_no, b.id, b.receive_basis, b.pi_wo_batch_no, b.prod_id, b.brand_id,b.batch_lot, c.lot,c.yarn_type, c.detarmination_id,c.color,c.dia_width,c.weight, b.order_uom, b.order_qnty, b.order_rate,b.order_ile_cost,b.order_amount,b.cons_amount,b.no_of_bags,b.product_code,b.room,b.rack,b.self,b.bin_box from inv_receive_master a, inv_transaction b, product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.id='$rcv_dtls_id'";
	$result = sql_select($sql);
	foreach($result as $row)
	{
		
		$comp='';
		$determination_sql=sql_select("select a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id and a.id=$row[detarmination_id]");
				
		if($determination_sql[0][csf('construction')]!="")
		{
			$comp=$determination_sql[0][csf('construction')].", ";
		}
		
		foreach( $determination_sql as $d_row )
		{
			$comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
		}
		
		echo "$('#txt_fabric_type').val('".$woben_fabric_type_library[$row[csf("yarn_type")]]."');\n";
		echo "$('#txt_fabric_description').val('".$comp."');\n";
		echo "$('#fabric_desc_id').val(".$row[csf("detarmination_id")].");\n";
		echo "$('#txt_color').val('".$color_library[$row[csf("color")]]."');\n";
		echo "$('#txt_width').val('".$row[csf("dia_width")]."');\n";
        echo "$('#txt_weight').val(".$row[csf("weight")].");\n";	
        echo "$('#txt_batch_lot').val('".$row[csf("batch_lot")]."');\n";	
		echo "$('#txt_receive_qty').val(".$row[csf("order_qnty")].");\n";
		echo "$('#txt_rate').val(".$row[csf("order_rate")].");\n";
		echo "$('#txt_ile').val(".$row[csf("order_ile_cost")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("order_uom")].");\n";
		echo "$('#txt_amount').val(".$row[csf("order_amount")].");\n";
		echo "$('#txt_book_currency').val(".$row[csf("cons_amount")].");\n";
		echo "$('#txt_order_qty').val(0);\n";
		echo "$('#txt_prod_code').val('".$row[csf("product_code")]."');\n";
		echo "$('#txt_room').val(".$row[csf("room")].");\n";
		echo "$('#txt_rack').val(".$row[csf("rack")].");\n";
		echo "$('#txt_self').val(".$row[csf("self")].");\n";
		echo "$('#txt_binbox').val(".$row[csf("bin_box")].");\n";
		echo "$('#update_dtls_id').val(".$row[csf("id")].");\n";
		echo "$('#all_po_id').val('".$row[csf("order_id ")]."');\n";
		if($row[csf("receive_basis")]==1 || $row[csf("receive_basis")] ==2)
		{
			echo "show_list_view(".$row[csf("receive_basis")]."+'**'+".$row[csf("pi_wo_batch_no")]."+'**'+".$row[csf("booking_without_order")]."+'**'+'".$row[csf("booking_no")]."','show_product_listview','list_product_container','requires/woven_finish_fabric_receive_controller','');\n";
		}
		$roll_maintained=return_field_value("fabric_roll_level","variable_settings_production","company_name ='".$row[csf("company_id")]."' and variable_list=3 and is_deleted=0 and status_active=1");
		$save_string='';
		if($roll_maintained==1)
		{
			$data_roll_array=sql_select("select id, roll_used, po_breakdown_id, qnty, roll_no from pro_roll_details where dtls_id='$rcv_dtls_id' and entry_form=5 and status_active=1 and is_deleted=0");
			foreach($data_roll_array as $row_roll)
			{ 
				if($row_roll[csf('roll_used')]==1) $roll_id=$row_roll[csf('id')]; else $roll_id=0;
				$roll_id=$row_roll[csf('id')];
				
				if($save_string=="")
				{
					$save_string=$row_roll[csf("po_breakdown_id")]."**".$row_roll[csf("qnty")]."**".$row_roll[csf("roll_no")]."**".$roll_id."**".$row_roll[csf("id")];
				}
				else
				{
					$save_string.=",".$row_roll[csf("po_breakdown_id")]."**".$row_roll[csf("qnty")]."**".$row_roll[csf("roll_no")]."**".$roll_id."**".$row_roll[csf("id")];
				}
			}
		}
		else
		{
			$data_po_array=sql_select("select po_breakdown_id, quantity from order_wise_pro_details where trans_id='$rcv_dtls_id' and entry_form=17 and status_active=1 and is_deleted=0");
			foreach($data_po_array as $row_po)
			{ 
				if($save_string=="")
				{
					$save_string=$row_po[csf("po_breakdown_id")]."**".$row_po[csf("quantity")];
				}
				else
				{
					$save_string.=",".$row_po[csf("po_breakdown_id")]."**".$row_po[csf("quantity")];
				}
			}
		}
		echo "document.getElementById('save_data').value 				= '".$save_string."';\n";
		echo "set_button_status(1, permission, 'fnc_woben_finish_fab_receive_entry',1,1);\n";
		echo "disable_enable_fields( 'cbo_receive_basis*cbo_receive_purpose*txt_receive_date*txt_challan_no*cbo_store_name*txt_exchange_rate', 0, '', '');\n";
		echo "fn_calile();\n";
	}
	exit();
}




//################################################# function Here #########################################//




//function for domestic rate find--------------//
//parameters rate,ile cost,exchange rate,conversion factor
function return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor){
	$rate_ile=$rate+$ile_cost;
	$rate_ile_exchange=$rate_ile*$exchange_rate;
	$doemstic_rate=$rate_ile_exchange/$conversion_factor;
	return $doemstic_rate;	
}


//return product master table id ----------------------------------------//
function return_product_id($txt_fabric_type,$txt_fabric_description,$fabric_desc_id,$txt_color,$txt_width,$txt_weight,$company,$supplier,$store,$uom,$prodCode)
{	
	$whereCondition = "company_id=$company and supplier_id=$supplier and item_category_id=3 and detarmination_id=$fabric_desc_id and yarn_type=$txt_fabric_type and weight=$txt_weight and dia_width=$txt_width and color='$txt_color' and status_active=1 and is_deleted=0";
  	$prodMSTID = return_field_value("id","product_details_master","$whereCondition");
	$insertResult = true;
	
	if($prodMSTID==false || $prodMSTID=="")
	{
		// new product create here--------------------------//
		$fabric_type_arr=return_library_array( "select id, fabric_type from lib_woben_fabric_type",'id','fabric_type');
		$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
		
		$product_name_details=$fabric_type_arr[$txt_fabric_type].", ".trim(str_replace("'","",$txt_fabric_description)).", ".trim(str_replace("'","",$txt_weight)).", ".trim(str_replace("'","",$txt_width)).", ".$color_name_arr[$txt_color];
				//$product_name_details=$fabric_type_arr[$txt_fabric_type].", ".trim(str_replace("'","",$txt_fabric_description));


 		$prodMSTID=return_next_id("id", "product_details_master", 1);
		$field_array = "id,company_id,supplier_id,item_category_id,detarmination_id,product_name_details,item_code,unit_of_measure,yarn_type,color,dia_width,weight";
 		$data_array = "(".$prodMSTID.",".$company.",".$supplier.",3,".$fabric_desc_id.",'".$product_name_details."',".$prodCode.",".$uom.",".$txt_fabric_type.",".$txt_color.",".$txt_width.",".$txt_weight.")";
		//echo $field_array."<br>".$data_array."--".$product_name_details;die;
		$insertResult = false;
		//$insertResult = sql_insert("product_details_master",$field_array,$data_array,1);		
		
	}
	if($insertResult == true)
	{
		return $insertResult."***".$prodMSTID;
	}else{
		return $insertResult."***".$field_array."***".$data_array."***".$prodMSTID;
	}

}


if ($action=="po_popup")
{
	
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);

	$data=explode("_",$data);
	$po_id=$data[0]; $type=$data[1];
	
	if($type==1) 
	{
		$dtls_id=$data[2]; 
		$roll_maintained=$data[3]; 
		$save_data=$data[4]; 
		$prev_distribution_method=$data[5]; 
		$receive_basis=$data[6]; 
	}
?> 

	<script>
		var receive_basis=<?php echo $receive_basis; ?>;
		var roll_maintained=<?php echo $roll_maintained; ?>;
		
		function fn_show_check()
		{
			if( form_validation('cbo_buyer_name','Buyer Name')==false )
			{
				return;
			}			
			show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<?php echo $cbo_company_id; ?>+'_'+document.getElementById('cbo_buyer_name').value+'_'+'<?php echo $all_po_id; ?>', 'create_po_search_list_view', 'search_div', 'woven_finish_fabric_receive_controller', 'setFilterGrid(\'tbl_list_search\',-1);hidden_field_reset();');
			set_all();
		}
		
		function distribute_qnty(str)
		{
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_grey_qnty=$('#txt_prop_grey_qnty').val()*1;
				
				$("#tbl_list_search").find('tr').each(function()
				{
					var txtOrginal=$(this).find('input[name="txtOrginal[]"]').val()*1;
					var isDisbled=$(this).find('input[name="txtGreyQnty[]"]').is(":disabled");
					
					if(txtOrginal==0)
					{
						$(this).remove();
					}
					else if(isDisbled==false && txtOrginal==1)
					{
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;
						
						var grey_qnty=(perc*txt_prop_grey_qnty)/100;
						$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
					}
				});
			}
			else
			{
				$('#txt_prop_grey_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					if($(this).find('input[name="txtGreyQnty[]"]').is(":disabled")==false)
					{
						$(this).find('input[name="txtGreyQnty[]"]').val('');
					}
				});
			}
		}
		
		function roll_duplication_check(row_id)
		{
			var row_num=$('#tbl_list_search tr').length;
			var po_id=$('#txtPoId_'+row_id).val();
			var roll_no=$('#txtRoll_'+row_id).val();
			
			if(roll_no*1>0)
			{
				for(var j=1; j<=row_num; j++)
				{
					if(j==row_id)
					{
						continue;
					}
					else
					{
						var po_id_check=$('#txtPoId_'+j).val();
						var roll_no_check=$('#txtRoll_'+j).val();	
			
						if(po_id==po_id_check && roll_no==roll_no_check)
						{
							alert("Duplicate Roll No.");
							$('#txtRoll_'+row_id).val('');
							return;
						}
					}
				}
				
				var txtRollTableId=$('#txtRollTableId_'+row_id).val();
				var data=po_id+"**"+roll_no+"**"+txtRollTableId;
				var response=return_global_ajax_value( data, 'roll_duplication_check', '', 'woven_finish_fabric_receive_controller');
				var response=response.split("_");
				
				if(response[0]!=0)
				{
					var po_number=$('#tr_'+row_id).find('td:first').text();
					alert("This Roll Already Used. Duplicate Not Allowed");
					$('#txtRoll_'+row_id).val('');
					return;
				}
			}
			
		}
		
		function add_break_down_tr( i )
		{ 
			var cbo_distribiution_method=$('#cbo_distribiution_method').val();
			var isDisbled=$('#txtGreyQnty_'+i).is(":disabled");
			
			if(cbo_distribiution_method==2 && isDisbled==false)
			{
				var row_num=$('#tbl_list_search tr').length;
				row_num++;
				
				var clone= $("#tr_"+i).clone();
				clone.attr({
					id: "tr_" + row_num,
				});
				
				clone.find("input,select").each(function(){
					  
				$(this).attr({ 
				  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ row_num },
				  'name': function(_, name) { return name },
				  'value': function(_, value) { return value }              
				});
				 
				}).end();
				
				$("#tr_"+i).after(clone);
				
				$('#txtOrginal_'+row_num).removeAttr("value").attr("value","0");
				
				$('#txtRoll_'+row_num).removeAttr("value").attr("value","");

				$('#txtGreyQnty_'+row_num).removeAttr("value").attr("value","");
				$('#txtRoll_'+row_num).removeAttr("onBlur").attr("onBlur","roll_duplication_check("+row_num+");");
					
				$('#increase_'+row_num).removeAttr("value").attr("value","+");
				$('#decrease_'+row_num).removeAttr("value").attr("value","-");
				$('#increase_'+row_num).removeAttr("onclick").attr("onclick","add_break_down_tr("+row_num+");");
				$('#decrease_'+row_num).removeAttr("onclick").attr("onclick","fn_deleteRow("+row_num+");");
			}
		}
		
		function fn_deleteRow(rowNo) 
		{ 
			var cbo_distribiution_method=$('#cbo_distribiution_method').val();
			
			if(cbo_distribiution_method==2)
			{
				var txtOrginal=$('#txtOrginal_'+rowNo).val()*1;
				if(txtOrginal==0)
				{
					$("#tr_"+rowNo).remove();
				}
			}
		}
		
		var selected_id = new Array();
		
		 function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i,1 );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_po_row_id').value;
			if(old!="")
			{   
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{  
					js_set_value( old[i],0 ) 
				}
			}
		}
		
		function js_set_value( str, check_or_not ) 
		{
			if(check_or_not==1)
			{
				var roll_used=$('#roll_used'+str).val();
				if(roll_used==1)
				{
					var po_number=$('#search' + str).find("td:eq(3)").text();
					alert("Batch Roll Found Against PO- "+po_number);
					return;
				}
			}
			
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#po_id').val( id );
		}
		
		function show_grey_prod_recv() 
		{ 
			var po_id=$('#po_id').val();
			show_list_view ( po_id+'_'+'1'+'_'+'<?php echo $dtls_id; ?>'+'_'+'<?php echo $roll_maintained; ?>'+'_'+'<?php echo $save_data; ?>'+'_'+'<?php echo $prev_distribution_method; ?>'+'_'+'<?php echo $receive_basis; ?>', 'po_popup', 'search_div', 'woven_finish_fabric_receive_controller', '');
		}
		
		function hidden_field_reset()
		{
			$('#po_id').val('');
			$('#save_string').val( '' );
			$('#tot_grey_qnty').val( '' );
			$('#number_of_roll').val( '' );
			selected_id = new Array();
		}
		
		function fnc_close()
		{
			var save_string='';	 var tot_grey_qnty=''; var no_of_roll=''; 
			var po_id_array = new Array();
			
			$("#tbl_list_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtGreyQnty=$(this).find('input[name="txtGreyQnty[]"]').val();
				var txtRoll=$(this).find('input[name="txtRoll[]"]').val();
				var txtRollId=$(this).find('input[name="txtRollId[]"]').val();
				var txtRollTableId=$(this).find('input[name="txtRollTableId[]"]').val();
				
				tot_grey_qnty=tot_grey_qnty*1+txtGreyQnty*1;
				
				if(roll_maintained==0)
				{
					txtRoll=0;
				}
				
				if(txtRoll*1>0)
				{
					no_of_roll=no_of_roll*1+1;	
				}
				
				if(txtGreyQnty*1>0)
				{
					
					if(save_string=="")
					{
						save_string=txtPoId+"**"+txtGreyQnty+"**"+txtRoll+"**"+txtRollId+"**"+txtRollTableId;
					}
					else
					{
						save_string+=","+txtPoId+"**"+txtGreyQnty+"**"+txtRoll+"**"+txtRollId+"**"+txtRollTableId;
					}
					
					if( jQuery.inArray( txtPoId, po_id_array) == -1 ) 
					{
						po_id_array.push(txtPoId);
					}
				}
			});
			
			$('#save_string').val( save_string );
			$('#tot_grey_qnty').val( tot_grey_qnty );
			$('#number_of_roll').val( no_of_roll );
			$('#all_po_id').val( po_id_array );
			$('#distribution_method').val( $('#cbo_distribiution_method').val() );	
			
			parent.emailwindow.hide();
		}
    </script>

</head>

<body>
	<?php
	if($type!=1)
	{
	?>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:10px">
        	<input type="hidden" name="save_string" id="save_string" class="text_boxes" value="">
            <input type="hidden" name="tot_grey_qnty" id="tot_grey_qnty" class="text_boxes" value="">
            <input type="hidden" name="number_of_roll" id="number_of_roll" class="text_boxes" value="">
            <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
            <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
	<?php
	}
	
	if(($receive_basis==4 || $receive_basis==6 || $receive_basis==1)&& $type!=1)
	{
	?>
		<table cellpadding="0" cellspacing="0" width="620" class="rpt_table">
			<thead>
				<th>Buyer</th>
				<th>Search By</th>
				<th>Search</th>
				<th>
					<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
					<input type="hidden" name="po_id" id="po_id" value="">
				</th> 
			</thead>
			<tr class="general">
				<td align="center">
					<?php
						echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",$data[0] ); 
					?>       
				</td>
				<td align="center">	
					<?php
						$search_by_arr=array(1=>"PO No",2=>"Job No");
						echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
					?>
				</td>                 
				<td align="center">				
					<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
				</td> 						
				<td align="center">
					<input type="button" name="button2" class="formbutton" value="Show" onClick="fn_show_check();" style="width:100px;" />
				</td>
			</tr>
		</table>
		<div id="search_div" style="margin-top:10px">
       		<?php
			if($save_data!="")
			{
			?>
				<div style="width:600px; margin-top:10px" align="center">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
						<thead>
							<th>Total Grey Qnty</th>
							<th>Distribution Method</th>
						</thead>
						<tr class="general">
							<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php echo $txt_receive_qnty; ?>" style="width:120px" onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
							<td>
								<?php 
									$distribiution_method=array(1=>"Proportionately",2=>"Manually");
									echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0, "",$prev_distribution_method, "distribute_qnty(this.value);",0 );
								?>
							</td>
						</tr>
					</table>
				</div>
				<div style="margin-left:10px; margin-top:10px">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
						<thead>
							<th width="150">PO No</th>
							<th width="110">PO Qnty</th>
							<th width="110">Grey Qnty</th>
							<?php
							if($roll_maintained==1)
							{
							?>
								<th>Roll</th>
								<th width="65"></th>
							<?php
							}
							?>
						</thead>
					</table>
					<div style="width:600px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
						<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">  
							<?php 
							$i=1; $tot_po_qnty=0; $po_array=array();  

							$explSaveData = explode(",",$save_data); 	
							for($z=0;$z<count($explSaveData);$z++)
							{
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
									
								$po_wise_data = explode("**",$explSaveData[$z]);
								$order_id=$po_wise_data[0];
								$grey_qnty=$po_wise_data[1];
								$roll_no=$po_wise_data[2];
								$roll_not_delete_id=$po_wise_data[3];
								$roll_id=$po_wise_data[4];
								
								$po_data=sql_select("select b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
								
								if($roll_maintained==1)
								{
									$roll_used=return_field_value("roll_used","pro_roll_details","id='$roll_id'");
									
									if(!(in_array($order_id,$po_array)))
									{
										if($roll_not_delete_id==0) $tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										if($roll_used==1) $orginal_val=1; else $orginal_val=0;
									}
									
									if($roll_used==1)
									{
										$disable="disabled='disabled'";
										$roll_not_delete_id=$roll_not_delete_id;
									}
									else
									{
										$disable="";
										$roll_not_delete_id=0;
									}
								}
								else
								{
									if(!(in_array($order_id,$po_array)))
									{
										$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										$orginal_val=0;
									}
									
									$roll_not_delete_id=0;
									$roll_id=0;
									$disable="";	
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="150">
										<p><?php echo $po_data[0][csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
                                        <!--This is used for not delete row which is used in batch -->
                                        <input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
                                        <!--This is used for Duplication Check -->
									</td>
									<td width="110" align="right">
										<?php echo $po_data[0][csf('po_qnty_in_pcs')] ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
									</td>
									<td width="110" align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?>>
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++;
							}
							?>
							<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
						</table>
					</div>
					<table width="620">
						 <tr>
							<td align="center" >
								<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
							</td>
						</tr>
					</table>
				</div>
			<?php
			}
			?>
        </div>
	<?php
	}
	
	else
	{
	?>
		<div style="width:600px; margin-top:10px" align="center">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
				<thead>
					<th>Total Grey Qnty</th>
					<th>Distribution Method</th>
				</thead>
				<tr class="general">
					<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php echo $txt_receive_qnty; ?>" style="width:120px" onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
					<td>
						<?php 
							$distribiution_method=array(1=>"Proportionately",2=>"Manually");
							echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0, "",$prev_distribution_method, "distribute_qnty(this.value);",0 );
						?>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:10px; margin-top:10px">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
				<thead>
					<th width="150">PO No</th>
					<th width="110">PO Qnty</th>
					<th width="110">Grey Qnty</th>
                    <?php
					if($roll_maintained==1)
					{
					?>
                        <th>Roll</th>
                        <th width="65"></th>
					<?php
                    }
                    ?>
				</thead>
			</table>
			<div style="width:600px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">  
					<?php 
					$i=1; $tot_po_qnty=0;  $po_array=array();
					
					if($save_data!="" && ($receive_basis==4 || $receive_basis==6 || $receive_basis==1))
					{
						$po_id = explode(",",$po_id);
						$explSaveData = explode(",",$save_data); 	
						for($z=0;$z<count($explSaveData);$z++)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$po_wise_data = explode("**",$explSaveData[$z]);
							$order_id=$po_wise_data[0];
							$grey_qnty=$po_wise_data[1];
							$roll_no=$po_wise_data[2];
							$roll_not_delete_id=$po_wise_data[3];
							$roll_id=$po_wise_data[4];
							
							if(in_array($order_id,$po_id))
							{
								$po_data=sql_select("select b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
								
								if($roll_maintained==1)
								{
									$roll_used=return_field_value("roll_used","pro_roll_details","id='$roll_id'");
									
									if(!(in_array($order_id,$po_array)))
									{
										if($roll_not_delete_id==0) $tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										if($roll_used==1) $orginal_val=1; else $orginal_val=0;
									}
									
									if($roll_used==1)
									{
										$disable="disabled='disabled'";
										$roll_not_delete_id=$roll_not_delete_id;
									}
									else
									{
										$disable="";
										$roll_not_delete_id=0;
									}
										
								}
								else
								{
									if(!(in_array($order_id,$po_array)))
									{
										$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										$orginal_val=0;
									}
									
									$roll_id=0;
									$roll_not_delete_id=0;
									$disable="";
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="150">
										<p><?php echo $po_data[0][csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="110" align="right">
										<?php echo $po_data[0][csf('po_qnty_in_pcs')] ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
									</td>
									<td width="110" align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?>>
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++;
							}
						}
						
						$result=implode(",",array_diff($po_id, $po_array));
						if($result!="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($result)";
							$nameArray=sql_select($po_sql);
							foreach($nameArray as $row)
							{  
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$orginal_val=1;
								$roll_id=0;
								$roll_not_delete_id=0;

								$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
								
							 ?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="150">
										<p><?php echo $row[csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="110" align="right">
										<?php echo $row[csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
									</td>
									<td width="110" align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="" >
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="" onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++; 
							} 
						}
					}
					else if($save_data!="" && $receive_basis==2)
					{
						$po_id = explode(",",$po_id);
						$explSaveData = explode(",",$save_data); 	
						for($z=0;$z<count($explSaveData);$z++)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$po_wise_data = explode("**",$explSaveData[$z]);
							$order_id=$po_wise_data[0];
							$grey_qnty=$po_wise_data[1];
							$roll_no=$po_wise_data[2];
							$roll_not_delete_id=$po_wise_data[3];
							$roll_id=$po_wise_data[4];
							
							$po_data=sql_select("select b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
							
							if($roll_maintained==1)
							{
								$roll_used=return_field_value("roll_used","pro_roll_details","id='$roll_id'");
								
								if(!(in_array($order_id,$po_array)))
								{
									if($roll_not_delete_id==0) $tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$orginal_val=1;
									$po_array[]=$order_id;
								}
								else
								{
									if($roll_used==1) $orginal_val=1; else $orginal_val=0;
								}
								
								if($roll_used==1)
								{
									$disable="disabled='disabled'";
									$roll_not_delete_id=$roll_not_delete_id;
								}
								else
								{
									$disable="";
									$roll_not_delete_id=0;
								}
									
							}
							else
							{
								if(!(in_array($order_id,$po_array)))
								{
									$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$orginal_val=1;
									$po_array[]=$order_id;
								}
								else
								{
									$orginal_val=0;
								}
								
								$roll_id=0;
								$roll_not_delete_id=0;
								$disable="";
							}
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="150">
									<p><?php echo $po_data[0][csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
									<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
									<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
									<!--This is used for not delete row which is used in batch -->
									<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
									<!--This is used for Duplication Check -->
								</td>
								<td width="110" align="right">
									<?php echo $po_data[0][csf('po_qnty_in_pcs')] ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
								</td>
								<td width="110" align="center">
									<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?>>
								</td>
								<?php
								if($roll_maintained==1)
								{
								?>
									<td align="center">
										<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
									</td>
									<td width="65">
										<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
										<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
									</td>
								<?php
								}
								?>
							</tr>
						<?php 
						$i++;
						}
					}
					else
					{
						if($type==1)
						{
							if($po_id!="")
							{
								$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id)";
							}
						}
						else
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b, wo_booking_dtls c where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' group by b.id";
						}
						
						$nameArray=sql_select($po_sql);
						foreach($nameArray as $row)
						{  
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
							
							$orginal_val=1;
							$roll_id=0;
							$roll_not_delete_id=0;
							$disable="";
							$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
							
						 ?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="150">
									<p><?php echo $row[csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
									<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
									<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
                                    <!--This is used for not delete row which is used in batch -->
                                    <input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
                                    <!--This is used for Duplication Check -->
								</td>
								<td width="110" align="right">
									<?php echo $row[csf('po_qnty_in_pcs')]; ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
								</td>
								<td width="110" align="center">
									<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?>>
								</td>
								<?php
								if($roll_maintained==1)
								{
								?>
									<td align="center">
										<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
									</td>
									<td width="65">
										<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
										<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
									</td>
								<?php
								}
								?>
							</tr>
						<?php 
						$i++; 
						} 
					}
					?>
					<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
				</table>
			</div>
			<table width="620">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
		</div>
	<?php
	}
	if($type!=1)
	{
	?>
		</fieldset>
	</form>
    <?php
	}
	?>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="roll_duplication_check")
{
	$data=explode("**",$data);
	$po_id=$data[0];
	$roll_no=trim($data[1]);
	$roll_id=$data[2];
	
	if($roll_id=="" || $roll_id=="0")
	{
		$sql="select a.recv_number from inv_receive_master a, pro_roll_details b where a.id=b.mst_id and b.po_breakdown_id='$po_id' and b.roll_no='$roll_no' and a.is_deleted=0 and a.status_active=1 and b.entry_form=5 and b.is_deleted=0 and b.status_active=1";
	}
	else
	{
		$sql="select a.recv_number from inv_receive_master a, pro_roll_details b where a.id=b.mst_id and b.po_breakdown_id='$po_id' and b.roll_no='$roll_no' and a.is_deleted=0 and a.status_active=1 and b.entry_form=5 and b.id<>$roll_id and b.is_deleted=0 and b.status_active=1";
	}
	//echo $sql;
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('recv_number')];
	}
	else
	{
		echo "0_";
	}
	
	exit();	
}

if($action=="roll_maintained")
{
	$roll_maintained=return_field_value("fabric_roll_level","variable_settings_production","company_name ='$data' and variable_list=3 and is_deleted=0 and status_active=1");
	if($roll_maintained=="") $roll_maintained=0; else $roll_maintained=$roll_maintained;
	echo "document.getElementById('roll_maintained').value 	= '".$roll_maintained."';\n";
	exit();	
}
if($action=="create_po_search_list_view")
{
	$data = explode("_",$data);
	
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	
	if($search_by==1)
		$search_field='b.po_number';
	else
		$search_field='a.job_no';
		
	$company_id =$data[2];
	$buyer_id =$data[3];
	
	$all_po_id=$data[4];
	
	if($all_po_id!="")
		$po_id_cond=" or b.id in($all_po_id)";
	else 
		$po_id_cond="";
	
	$hidden_po_id=explode(",",$all_po_id);

	if($buyer_id==0) { echo "Please Select Buyer First."; die; }
	
	if($db_type==0)
	{
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name=$buyer_id and $search_field like '$search_string' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $po_id_cond group by b.id"; 
	}
	else
	{	
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name=$buyer_id and $search_field like '$search_string' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $po_id_cond order by b.id"; 
	}
	//echo $sql;die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Job No</th>
                <th width="110">Style No</th>
                <th width="110">PO No</th>
                <th width="90">PO Quantity</th>
                <th width="50">UOM</th>
                <th>Shipment Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:240px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1; $po_row_id='';
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
						
					$roll_used=0;
					
					if(in_array($selectResult[csf('id')],$hidden_po_id)) 
					{
						if($po_row_id=="") $po_row_id=$i; else $po_row_id.=",".$i;
						
						$roll_data_array=sql_select("select roll_no from pro_roll_details where po_breakdown_id=$selectResult[id] and roll_used=1 and entry_form=1 and status_active=1 and is_deleted=0");
						if(count($roll_data_array)>0)
						{
							$roll_used=1;
						}
						else
							$roll_used=0;
					}
							
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i; ?>,1)"> 
                            <td width="40" align="center">
								<?php echo $i; ?>
                            	<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>
                            	<input type="hidden" name="roll_used" id="roll_used<?php echo $i ?>" value="<?php echo $roll_used; ?>"/>	
                            </td>	
                            <td width="100"><p><?php echo $selectResult[csf('job_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                            <td width="90" align="right"><?php echo $selectResult[csf('po_qnty_in_pcs')]; ?></td> 
                            <td width="50" align="center"><p><?php echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                            <td align="center"><?php echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                        </tr>
                    <?php
                    $i++;
				}
			?>
				<input type="hidden" name="txt_po_row_id" id="txt_po_row_id" value="<?php echo $po_row_id; ?>"/>
            </table>
        </div>
         <table width="620" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                            <input type="button" name="close" onClick="show_grey_prod_recv();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>           
<?php
	
exit();
}

if ($action=="fabricDescription_popup")
{
	echo load_html_head_contents("Fabric Description Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
		
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
		
		function js_set_value(id,comp,gsm)
		{
			$('#hidden_desc_id').val(id);
			$('#hidden_desc_no').val(comp);
			$('#hidden_gsm').val(gsm);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:720px;margin-left:10px">
			<?php
                $data_array=sql_select("select id, construction, fab_nature_id, gsm_weight from lib_yarn_count_determina_mst where fab_nature_id=3 and status_active=1 and is_deleted=0");
            ?>
            <input type="hidden" name="hidden_desc_id" id="hidden_desc_id" class="text_boxes" value="">
            <input type="hidden" name="hidden_desc_no" id="hidden_desc_no" class="text_boxes" value="">  
            <input type="hidden" name="hidden_gsm" id="hidden_gsm" class="text_boxes" value="">  
            
            <div style="margin-left:10px; margin-top:10px">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="680">
                    <thead>
                        <th width="50">SL</th>
                        <th width="100">Fabric Nature</th>
                        <th width="150">Construction</th>
                        <th width="100">GSM/Weight</th>
                        <th>Composition</th>
                    </thead>
                </table>
                <div style="width:700px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="680" id="tbl_list_search">  
                        <?php 
                        $i=1;
                        foreach($data_array as $row)
                        {  
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
                            
							if($row[csf('construction')]!="")
							{    
                            	$comp=$row[csf('construction')].", ";
							}
                            $determ_sql=sql_select("select copmposition_id, percent from lib_yarn_count_determina_dtls where mst_id=$row[id] and status_active=1 and is_deleted=0");
                            foreach( $determ_sql as $d_row )
                            {
                                $comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
                            }
                            
                         ?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value(<?php echo $row[csf('id')]; ?>,'<?php echo $comp; ?>','<?php echo $row[csf('gsm_weight')]; ?>')" style="cursor:pointer" >
                                <td width="50"><?php echo $i; ?></td>
                                <td width="100"><?php echo $item_category[$row[csf('fab_nature_id')]]; ?></td>
                                <td width="150"><p><?php echo $row[csf('construction')]; ?></p></td>
                                <td width="100"><?php echo $row[csf('gsm_weight')]; ?></td>
                                <td><p><?php echo $comp; ?></p></td>
                            </tr>
                        <?php 
                        $i++; 
                        } 
                        ?>
                    </table>
                </div> 
            </div>
		</fieldset>
	</form>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}


if($action=="gwoven_finish_fabric_receive_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	$sql="select id, recv_number, receive_basis, booking_id, receive_date, challan_no, store_id, supplier_id, lc_no, currency_id, exchange_rate, source from inv_receive_master where id='$data[1]' and company_id='$data[0]'";
	//echo $sql;die;
	$dataArray=sql_select($sql);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$supplier_arr=return_library_array( "select id, supplier_name from  lib_supplier", "id", "supplier_name"  );
	$wo_arr=return_library_array( "select id, booking_no from  wo_booking_mst", "id", "booking_no"  );
	$pi_arr=return_library_array( "select id, pi_number from  com_pi_master_details", "id", "pi_number"  );
	$lc_arr=return_library_array( "select id, lc_number from  com_btb_lc_master_details", "id", "lc_number"  );
?>
<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u><?php echo $data[2]; ?> Report</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>MRR ID :</strong></td><td width="175px"><?php echo $dataArray[0][csf('recv_number')]; ?></td>
            <td width="130"><strong>Receive Basis:</strong></td> <td width="175px"><?php echo $receive_basis_arr[$dataArray[0][csf('receive_basis')]]; ?></td>
            <td width="125"><strong>WO/PI</strong></td><td width="175px"><?php if ($dataArray[0][csf('receive_basis')]==2) echo $wo_arr[$dataArray[0][csf('booking_id')]]; else if ($dataArray[0][csf('receive_basis')]==1) echo $pi_arr[$dataArray[0][csf('booking_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Receive Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('receive_date')]); ?></td>
            <td><strong>Challan No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Store Name:</strong></td> <td width="175px"><?php echo $store_library[$dataArray[0][csf('store_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Supplier:</strong></td><td width="175px"><?php echo $supplier_arr[$dataArray[0][csf('supplier_id')]]; ?></td>
            <td><strong>L/C No:</strong></td><td width="175px"><?php echo $lc_arr[$dataArray[0][csf('lc_no')]]; ?></td>
            <td><strong>Currency:</strong></td> <td width="175px"><?php echo $currency[$dataArray[0][csf('currency_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Exchange Rate:</strong></td><td width="175px"><?php echo $dataArray[0][csf('exchange_rate')]; ?></td>
            <td><strong>Source:</strong></td><td width="175px"><?php echo $source[$dataArray[0][csf('source')]]; ?></td>
            <td><strong>&nbsp;</strong></td> <td width="175px"><?php //echo $item_category[$dataArray[0][csf('item_category')]]; ?></td>
        </tr>
    </table>
        <br>
    <div style="width:100%;">
    <table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="260" >Fabric Details</th>
            <th width="40" >Width</th>
            <th width="50" >Weight</th>
            <th width="70" >Batch/Lot</th>
            <th width="50" >UOM</th>
            <th width="60" >Recv. Qnty.</th>
            <th width="60" >Rate</th>
            <th width="80" >Amount</th>
            <th width="100" >Book Currency</th> 
        </thead>
        <tbody> 
<?php
	 $sql_dtls = "select a.recv_number, a.receive_basis, b.id, b.pi_wo_batch_no, c.product_name_details, c.dia_width, c.weight, b.order_uom, b.order_qnty, b.order_rate, b.order_ile_cost, b.order_amount, b.cons_amount,b.batch_lot, c.weight, c.detarmination_id from inv_receive_master a, inv_transaction b,  product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category=3 and a.entry_form=17 and a.id='$data[1]'";
	
	$sql_result= sql_select($sql_dtls);
	$i=1;
	foreach($sql_result as $row)
	{
		if ($i%2==0)$bgcolor="#E9F3FF";						
		else $bgcolor="#FFFFFF";
		$wopi="";
		if($row[csf("receive_basis")]==1)
			$wopi=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("pi_wo_batch_no")]."");	
		else if($row[csf("receive_basis")]==2)
			$wopi=return_field_value("booking_no","wo_booking_mst","id=".$row[csf("pi_wo_batch_no")]."");	
		$totalQnty +=$row[csf("order_qnty")];
		$totalAmount +=$row[csf("order_amount")];
		$totalbookCurr +=$row[csf("cons_amount")];	
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <td><?php echo $row[csf("product_name_details")]; ?></td>
                <td><?php echo $row[csf("dia_width")]; ?></td>
                <td><?php echo $row[csf("weight")]; ?></td>
                <td align="center"><?php echo $row[csf("batch_lot")]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf("order_uom")]]; ?></td>
                <td align="right"><?php echo $row[csf("order_qnty")]; ?></td>
                <td align="right"><?php echo $row[csf("order_rate")]; ?></td>
                <td align="right"><?php echo $row[csf("order_amount")]; ?></td>
                <td align="right"><?php echo $row[csf("cons_amount")]; ?></td>
			</tr>
	<?php $i++; 
    } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6" align="right"><strong>Total :</strong></td>
                <td align="right"><?php echo $totalQnty; ?></td>
                <td align="right" colspan="2"><?php echo $totalAmount; ?></td>
                <td align="right"><?php echo $totalbookCurr; ?></td>
            </tr>                           
        </tfoot>
      </table>
        <br>
		 <?php
            echo signature_table(20, $data[0], "900px");
         ?>
      </div>
   </div>   
 <?php
 exit();	
}
?>
