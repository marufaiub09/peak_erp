<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_unit")
{
	echo create_drop_down( "cbo_unit_id", 150, "Select id, floor_name from  lib_prod_floor where company_id=$data and status_active=1 and is_deleted=0 order by floor_name","id,floor_name", 1, "-- Select --", $selected, "",0 );     	 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	//echo $datediff;
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_unit=str_replace("'","",$cbo_unit_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$company_short_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name"  );
	
function convertMinutes2Hours($Minutes)
{
    if ($Minutes < 0)
    {
        $Min = Abs($Minutes);
    }
    else
    {
        $Min = $Minutes;
    }
    $iHours = Floor($Min / 60);
    $Minutes = ($Min - ($iHours * 60)) / 100;
    $tHours = $iHours + $Minutes;
    if ($Minutes < 0)
    {
        $tHours = $tHours * (-1);
    }
    $aHours = explode(".", $tHours);
    $iHours = $aHours[0];
    if (empty($aHours[1]))
    {
        $aHours[1] = "00";
    }
    $Minutes = $aHours[1];
    if (strlen($Minutes) < 2)
    {
        $Minutes = $Minutes ."0";
    }
    $tHours = $iHours .":". $Minutes;
    return $tHours;
}
	
ob_start();	
	//$table_width=90+($datediff*160);
?>
	<div id="scroll_body" align="center" style="height:auto; width:auto; margin:0 auto; padding:0;">
        <table width="2250px" cellpadding="0" cellspacing="0" id="caption" align="center">
            <tr>
               <td align="center" width="100%" colspan="23" class="form_caption" ><strong style="font-size:18px"><?php echo $company_library[$cbo_company];?></strong></td>
            </tr> 
            <tr>  
               <td align="center" width="100%" colspan="23" class="form_caption" ><strong style="font-size:14px"><?php echo $report_title; ?></strong></td>
            </tr>
            <tr>  
               <td align="center" width="100%" colspan="23" class="form_caption" ><strong style="font-size:14px"> <?php echo "From : ".change_date_format(str_replace("'","",$txt_date_from))." To : ".change_date_format(str_replace("'","",$txt_date_to))."" ;?></strong></td>
            </tr>  
        </table>
        <table width="2250px" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" >
        	<thead>
                <th width="40">SL</th>
                <th width="120">Location</th>
                <th width="80">WO No</th>
                <th width="90">Job No</th>
                <th width="120">Buyer Name</th>
                <th width="130">Style No</th>
                <th width="140">Order No</th>
                <th width="100">Order Qty.</th>
                <th width="60">GSM</th>
                <th width="100">Fabric Color</th>
                <th width="100">Batch No/ Lot No.</th>
                <th width="100">Dyeing Qty</th>
                <th width="60">UL %</th>
                <th width="100">MC No.</th>
                <th width="90">MC Capacity</th>
                <th width="80">Loading Time</th>
                <th width="80">Unloading Time</th>
                <th width="70">Hour Used</th>
                <th width="90">Process/ Color Range</th>
                <th width="90">Dye & Chem Cost/Kg</th>
                <th width="110">Total Dye & Chem Cost</th>
                <th width="130">Dyeing Company</th>
                <th>Remarks</th>
            </thead>
        </table>
	<div style="width:2270px; overflow-y:scroll; max-height:350px; " >
        <table width="2250px" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" id="tbl_dyeing">
	 <?php	
	$location_library=return_library_array( "select id,location_name from lib_location", "id", "location_name");
	$buyer_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name");
	$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name");
	$batch_library=return_library_array( "select id,batch_no from pro_batch_create_mst", "id", "batch_no");
	$batch_color_library=return_library_array( "select id,color_id from pro_batch_create_mst", "id", "color_id");
	$gsm_library=return_library_array( "select id,gsm from  product_details_master", "id", "gsm");
	$machine_library=return_library_array( "select id,machine_no from lib_machine_name", "id", "machine_no");
	
	$order_arr=array();
	$sql_order="Select b.id, a.job_no, a.buyer_name, a.style_ref_no, b.po_number, b.po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1";
	$result_sql_order=sql_select($sql_order);
	foreach($result_sql_order as $row)
	{
		$order_arr[$row[csf('id')]]['job_no']=$row[csf('job_no')];
		$order_arr[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')];
		$order_arr[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
		$order_arr[$row[csf('id')]]['po_number']=$row[csf('po_number')];
		$order_arr[$row[csf('id')]]['po_quantity']=$row[csf('po_quantity')];
	}

	$load_data=array();
	$load_time_data=sql_select("select id,batch_id,batch_no,load_unload_id,end_hours,end_minutes from pro_fab_subprocess where load_unload_id=1 and entry_form=35 and company_id=$cbo_company and status_active=1  and is_deleted=0");
	
	foreach($load_time_data as $row_time)// for Loading time
	{
		$load_data[$row_time[csf('batch_id')]]['end_hours']=$row_time[csf('end_hours')];
		$load_data[$row_time[csf('batch_id')]]['end_minutes']=$row_time[csf('end_minutes')];
	}
		
		$sql_dtls="select b.id, a.batch_id, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.booking_no, b.color_range_id, c.prod_id, c.po_id, sum(c.batch_qnty) as qnty
		from pro_fab_subprocess a, pro_batch_create_mst b,  pro_batch_create_dtls c where a.batch_id=b.id and a.entry_form=35 and a.load_unload_id=2 and b.id=c.mst_id and a.company_id='$cbo_company' and a.process_end_date between '$date_from' and '$date_to' and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 
		group by b.id, a.batch_id, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.booking_no, b.color_range_id, c.prod_id, c.po_id order by b.id";		//echo $sql_dtls;
	$sql_result=sql_select($sql_dtls);
	$i=1;  $ul_percent=0; $tot_dye_qnty=0;$total_dye_chem_cost=0;
	foreach ( $sql_result as $row )
	{
		if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
		$order_id=$row[csf('po_id')];
		$batch_id=$row[csf('batch_id')];
		$machine_name=$row[csf('machine_id')];
		//print_r ($buyer_id);
		?>
        	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="40"><?php echo $i; ?></td>
                <td width="120"><p><?php echo $location_library[$row[csf('location_id')]]; ?></p></td>
                <td width="80"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                <td width="90" align="center"><p><?php echo $order_arr[$order_id]['job_no']; ?></p></td>
                <td width="120"><p><?php echo $buyer_library[$order_arr[$order_id]['buyer_name']]; ?></p></td>
                <td width="130"><p><?php echo $order_arr[$order_id]['style_ref_no'];  ?></p></td>
                <td width="140"><p><?php echo $order_arr[$order_id]['po_number']; ?></p></td>
                <td width="100" align="right"><p><?php echo number_format($order_arr[$order_id]['po_quantity'],2,'.','');  ?></p></td>
                <td width="60" align="center"><p><?php echo $gsm_library[$row[csf('prod_id')]]; ?></p></td>
                <td width="100"><p><?php echo $color_library[$row[csf('color_id')]]; ?></p></td>
                <td width="100"><p><?php echo $row[csf('batch_no')]; ?></p></td>
                <td width="100" align="right"><p><?php echo number_format($row[csf('qnty')],2,'.',''); $total_dyeing_qnty+=$dye_qnty; ?></p></td>
                <td width="60" align="right"><p><?php
				if ($machine_name!="" || $machine_name!=0)
				{
					$machine_capacity=return_field_value("sum(prod_capacity) as prod_capacity","lib_machine_name","id in ($machine_name) ",'prod_capacity');
					$ul_percent=$row[csf('qnty')]/$machine_capacity*100;
				 echo number_format($ul_percent,2,'.',''); } ?></p></td>
                <td width="100" align="center"><p><?php echo $machine_library[$row[csf('machine_id')]]; ?></p></td>
                <td width="90" align="right"><p><?php echo number_format($machine_capacity,2,'.',''); ?></p></td>
                <td width="80" align="center"><p><?php
					$start_time='';
					$start_time=$load_data[$row[csf('id')]]['end_hours'].':'.$load_data[$row[csf('id')]]['end_minutes'];
					 echo  $start_time; ?></p></td>
                <td width="80" align="center"><p><?php
					$end_time='';
					$end_time=$row[csf('end_hours')].':'.$row[csf('end_minutes')];
				 	echo $end_time; ?></p></td>
                <td width="70" align="right"><p><?php 
					 $start_time=strtotime($row[csf('start_date')]." ".$start_time);
					 $end_time=strtotime($row[csf('end_date')]." ".$end_time);
                    $timeDiffin=($end_time-$start_time)/60; 
				echo convertMinutes2Hours($timeDiffin);  ?></p></td>
                <td width="90"><p><?php echo $color_range[$row[csf('color_range_id')]]; ?></p></td>
                <td width="90" align="right"><p><?php
				if ($batch_id!="" || $batch_id!=0)
				{ 
					$chem_dye_cost = return_field_value("SUM(a.cons_amount) as cons_amount","inv_transaction a, dyes_chem_issue_dtls b","a.id=b.trans_id AND b.batch_id in ($batch_id) AND b.item_category in (5,6,7) AND b.is_deleted=0 AND b.status_active=1","cons_amount");
					//echo " select SUM(a.cons_amount) as cons_amount from inv_transaction a, dyes_chem_issue_dtls b where a.id=b.trans_id AND b.batch_id in ($batch_id) AND b.item_category in (5,6,7) AND b.is_deleted=0 AND b.status_active=1";
				echo number_format($chem_dye_cost,2,'.',''); $total_dye_chem_cost+=$chem_dye_cost; } ?></p></td>
                <td width="110" align="right"><p><?php echo $total_dye_chem_cost=($chem_dye_cost*$row[csf('qnty')])*1; //echo number_format($total_cost_dye_chem,4,'.',''); ?></p></td>
                <td width="130" align="center"><p><?php echo $company_library[$row[csf('dyeing_company')]]; ?></p></td>
                <td><p><?php echo $row[csf('remarks')]; ?></p></td>
            </tr>
         <?php
		 $i++;
		} ?>
        </table>
        <table width="2250px" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0">
        	<tfoot>
                <th width="40">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="90">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="130">&nbsp;</th>
                <th width="140">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="60">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100"><strong>Total</strong></th>
                <th width="100" id="total_dyeing_qnty" align="right"><?php echo number_format($total_dyeing_qnty,2,'.',''); ?></th>
                <th width="60">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="90">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="90">&nbsp;</th>
                <th width="90">&nbsp;</th>
                <th width="110" id="total_dye_chem_cost" align="right"><?php echo number_format($total_dye_chem_cost,4,'.',''); ?></th>
                <th width="130">&nbsp;</th>
                <th>&nbsp;</th>
            </tfoot>
        </table>
    </div>        
  </div> 
	<?php
	exit();
}
?>
