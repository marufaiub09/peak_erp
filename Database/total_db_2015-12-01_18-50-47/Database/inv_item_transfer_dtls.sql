-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_item_transfer_dtls`
--

DROP TABLE IF EXISTS `inv_item_transfer_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_item_transfer_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `from_prod_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Order To Order=Product Id',
  `to_prod_id` int(11) NOT NULL DEFAULT '0',
  `knit_program_id` int(11) NOT NULL DEFAULT '0',
  `rack` varchar(20) NOT NULL,
  `shelf` int(11) NOT NULL DEFAULT '0',
  `to_rack` varchar(30) NOT NULL,
  `to_shelf` int(11) NOT NULL DEFAULT '0',
  `from_program` int(11) NOT NULL DEFAULT '0',
  `to_program` varchar(100) NOT NULL,
  `y_count` varchar(50) NOT NULL,
  `stitch_length` varchar(40) NOT NULL,
  `bin_box` int(11) NOT NULL DEFAULT '0',
  `yarn_lot` varchar(50) NOT NULL,
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `item_group` int(11) NOT NULL DEFAULT '0',
  `from_store` int(11) NOT NULL DEFAULT '0',
  `to_store` int(11) NOT NULL DEFAULT '0',
  `item_category` int(11) NOT NULL DEFAULT '0',
  `transfer_qnty` double NOT NULL DEFAULT '0',
  `roll` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `transfer_value` double NOT NULL DEFAULT '0',
  `rate_in_usd` double NOT NULL DEFAULT '0',
  `transfer_value_in_usd` double NOT NULL DEFAULT '0',
  `uom` int(11) NOT NULL DEFAULT '0',
  `batch_id` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mst_id` (`mst_id`),
  KEY `from_prod_id` (`from_prod_id`),
  KEY `to_prod_id` (`to_prod_id`),
  KEY `yarn_lot` (`yarn_lot`),
  KEY `brand_id` (`brand_id`),
  KEY `item_group` (`item_group`),
  KEY `from_store` (`from_store`),
  KEY `to_store` (`to_store`),
  KEY `transfer_qnty` (`transfer_qnty`),
  KEY `rate` (`rate`),
  KEY `transfer_value` (`transfer_value`),
  KEY `uom` (`uom`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_item_transfer_dtls`
--

LOCK TABLES `inv_item_transfer_dtls` WRITE;
/*!40000 ALTER TABLE `inv_item_transfer_dtls` DISABLE KEYS */;
INSERT INTO `inv_item_transfer_dtls` VALUES (1,1,2660,2660,0,'',0,'',0,0,'','','',0,'',2,0,0,2,2,13,336,0,0,0,0,0,12,0,31,'2015-09-15 16:04:28',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `inv_item_transfer_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
