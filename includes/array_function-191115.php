<?php
//******************************************************************************************** please DO NOT CHANGE BELOW ARRAYS

$blank_array=array();    // for blank Drop Down or empty drop down
$mod_permission_type=array(0=>"Selective Permission",1=>"Full Permission",2=>"No Permission");
$form_permission_type=array(1=>"Permitted",2=>"Not Permitted");
$row_status=array(1=>"Active",2=>"InActive",3=>"Cancelled"); 
$knitting_program_status=array(1=>"Waiting",2=>"Running",3=>"Stop",4=>"Closed"); 
//$knitting_program_status=array(1=>"Running",2=>"Waiting",3=>"Stop"); 
$attach_detach_array=array(1=>"Attach",0=>"Detach");
$planning_status=array(1=>"Pending",2=>"Planning Done",3=>"Requisition Done",4=>"Demand Done");
$yes_no=array(1=>"Yes",2=>"No"); //2= Deleted,3= Locked
$approval_type=array(0=>"Un-Approved",1=>"Approved"); 
$months = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
$year = array(2010=>'2010',2011=>'2011',2012=>'2012',2013=>'2013',2014=>'2014',2015=>'2015',2016=>'2016',2017=>'2017',2018=>'2018',2019=>'2019',2020=>'2020',2021=>'2021');

$dec_place = array(1=>'2',2=>'2',3=>'8',4=>'2',5=>'4',6=>'0',7=>'2');
//1=qnty(kg),2=qnty(yds),3=Rate,4=Amount(local Currency/Taka), 5=Foreign Currency, 6=Gmts qnty, 7=%(percentage)

$all_cal_day=array(1=>"Saturday",2=>"Sunday",3=>"Monday",4=>"Tuesday",5=>"Wednesday",6=>"Thursday",7=>"Friday");

$integrated_project_list=array(1=>"Platform",2=>"HRM System",3=>"Acounts");
$string_search_type=array(1=>"Exact",2=>"Starts with",3=>"Ends with",4=>"Contents");
$calculation_basis=array(1=>"Order Qty",2=>"Plun CutQty");
$dyeing_re_process=array(1=>"Topping",2=>"Adding",3=>"Stripping");
// Plannig Board Parameter

//******************************************************************************************** please DO NOT CHANGE UPPER ARRAYS


// common for All Module //
$currency=array(1=>"Taka",2=>"USD",3=>"EURO",4=>"CHF",5=>"SGD");

// Library Module //
$user_type=array(1=>"General User",2=>"Admin User",3=>"Demo User");
$mail_user_type=array('1'=>'Management','2'=>'Marketing','3'=>'General');

$sample_type=array(2=>"PP",3=>"FIT",4=>"Size Set",5=>"Others",6=>"Development",7=>"Production",8=>"Tag",9=>"Photo",10=>"Packing",11=>"Final"); 
$trim_type=array(1=>"Sewing",2=>"Packing/Finishing");
$production_module=array(1=>"Production Update Areas",2=>"Sewing Piece Rate WQ Limit",3=>"Fabric in Roll Level",4=>"Fabric in Machine Level",13=>"Batch Maintained",15=>"Auto Fabric Store Update",23=>"Production Resource Allocation",24=>"Auto Batch No Creation",25=>"SMV Source For Efficiency",26=>"Sewing Production Start",27=>"Barcode Generation",28=>"Production Update for Reject Qty",29=>"Cutting Piece Rate WQ Limit",30=>"Piece Rate Safety %",31=>"Booking Approval needed for Knitting Plan",32=>"Cut Panel delv. Basis",33=>"Last Process Prod. Qty Control");

$subcon_variable=array(1=>"Dyeing & Finishing Bill Qty",2=>"Knitting Fabric From Yarn Count Det.",3=>"Bill Rate");  
$dyeing_finishing_bill=array(1=>"On Grey Qty",2=>"On Delivery Qty"); 
$production_update_areas=array(1=>"Gross Quantity Level",2=>"Color Level",3=>"Color & Size Level");

$item_category=array(1=>"Yarn",2=>"Knit Finish Fabrics",3=>"Woven Fabrics",4=>"Accessories",5=>"Chemicals",6=>"Dyes",7=>"Auxilary Chemicals",8=>"Spare Parts",9=>"Spare Parts & Machinaries",10=>"Other Capital Items",11=>"Stationaries",12=>"Services - Fabric",13=>'Grey Fabric(Knit)',14=>'Grey Fabric(woven)',15=>'Electical',16=>'Maintenance',17=>'Medical',18=>'ICT',19=>'Print & Publication',20=>'Utilities & Lubricants',21=>'Construction Materials',22=>'Printing Chemicals & Dyes',23=>'Dyes Chemicals & Auxilary Chemicals',24=>'Services - Yarn Dyeing ',25=>'Services - Embellishment',28=>'Cut Panel',30=>'Garments');

$report_signeture_list = array(0=>"-- Select Report --",1=>"Fabric Booking",2=>"Trims Booking",3=>"PI Wise Yarn Receive",4=>"Short Fabric Booking",5=>"Sample Fabric Booking -With order",6=>"Sample Fabric Booking -Without order",7=>"Yarn Receive Return",8=>"Dyes And Chemical Receive",9=>"Dyes And Chemical Issue",10=>"Dye/Chem Receive Return",11=>"General Item Receive",12=>"General Item Issue",13=>"General Item Receive Return",14=>"General Item Issue Return",15=>"Dyes And Chemical Issue Requisition",16=>"Knit Grey Fabric Receive",17=>"Knit Grey Fabric Issue",18=>"Grey Fabric Transfer Entry",19=>"Grey Fabric Order To Order Transfer Entry",20=>"Woven Finish Fabric Receive",21=>"Knit Finish Fabric Issue",22=>"Woven Finish Fabric Issue",23=>"Finish Fabric Transfer Entry",24=>"Finish Fabric Order To Order Transfer Entry",25=>"Purchase Requisition",26=>"Embellishment Issue Entry",27=>"Embellishment Receive Entry",28=>"Sewing Input",29=>"Sewing Output",30=>"Iron entry",31=>"Packing And Finishing",32=>"Ex-Factory",33=>"Gate In Entry",34=>"Gate Out Entry",35=>"Trims Receive Entry",36=>"Trims Issue",37=>"Yarn Issue Return",38=>"Yarn Transfer Entry",39=>"Yarn Order To Order Transfer Entry",40=>"Daily Yarn Demand",41=>"Knitting Plan Report",42=>"Yarn Work Order",43=>"Yarn Dyeing Work Order",44=>"Knitting Delivery Challan",45=>"SubCon Fabric Finishing Entry",46=>"SubCon Delivery Challan",47=>"SubCon Knitting Bill Issue",48=>"SubCon Dyeing And Finishing Bill Issue",49=>"Yarn Issue",50=>"SubCon Cutting Bill Issue",51=>"SubCon Material Return Challan",52=>"Batch Creation",53=>"Fabric Service Booking",54=>"Cutting Delivary To Input",55=>"Stationary Work Order",56=>"SubCon Batch Creation",57=>"Embellishment Work Order",58=>"Cut and Lay Entry",59=>"Dyes Chemical Work Order",60=>"Spare Parts Work Order",61=>"Subcon Material Issue",62=>"Recipe Entry",63=>"Garments Delivery",64=>"SubCon Knitting Delivery Challan",65=>"Yarn Receive",66=>"Knit Finish Fabric Receive",67=>"Finish Fabric Production Entry",68=>"Finish Fabric Delivery to Store",69=>"Quotation Evaluation",70=>"Grey Fabric Delivery to store roll wise",71=>"Grey Fabric Receive Roll By Batch",72=>"Grey Roll Issue to Sub Contact ",73=>"AOP Roll Receive",74=>"Finish Fabric Roll Receive By Cutting", 77=>"Sample Ex-factory",78=>"Scrap Out Challan",79=>"Service Booking For AOP",80=>"Lab Test Work Order",81=>"Service Booking For Knitting",82=>"Service Booking For Dyeing",83=>"Knit Finish Fabric Receive Return",84=>"Piece Rate Work Order",85=>"Knit Grey Fabric Receive Return",86=>"Sample Development",87=>"Knit Grey Fabric Issue Return",88=>"Finish Fabric Issue Return",89=>"Dye/Chem Issue Return",90=>"Trims Issue Return",91=>"Inspection",92=>"Service Booking For AOP Without Order");


$entry_form=array(1=>"Yarn Receive",2=>"Grey Receive",3=>"Yarn Issue",4=>"Chemical Receive",5=>"Chemical Issue",6=>"Dye Production Update",7=>"Finish Fabric Production Entry",8=>'Yarn Receive Return',9=>'Yarn Issue Return',10=>'Yarn Transfer Entry',11=>'Yarn Order To Order Transfer Entry',12=>'Grey Fabric Transfer Entry',13=>'Grey Fabric Order To Order Transfer Entry',14=>'Finish Fabric Transfer Entry',15=>'Finish Fabric Order To Order Transfer Entry',16=>'Knit Grey Fabric Issue',17=>'Woven Finish Fabric Receive',18=>'Knit Finish Fabric Issue',19=>'Woven Finish Fabric Issue',20=>'General Item Receive',21=>'General Item Issue',22=>'Knit Grey Fabric Receive',23=>'Woven Grey Fabric Receive',24=>'Trims Receive',25=>'Trims Issue',26=>'General Item Receive Return',27=>'General Item Issue Return',28=>'Dye/Chem Receive Return',29=>'Dye/Chem Issue Return',30=>'Slitting/Squeezing',31=>'Drying',32=>'Heat Setting',33=>'Compacting',34=>'Special Finish',35=>'Dyeing Production',36=>'SubCon Batch Creation',37=>"Finish Fabric Receive Entry",38=>"SubCon Dyeing Production",39=>"Doc. Submission to Buyer",40=>"Doc. Submission to Bank",41=>"Yarn Dying With Order",42=>"Yarn Dying Without Order",43=>"Main Trims Booking",44=>"Main Trims Booking V2",45=>"Knit Grey Fabric Receive Return",46=>"Knit Finish Fabric Receive Return",47=>"Singeing",48=>"Stentering",49=>"Trims Receive Return",50=>"Woven Grey Fabric Receive Return",51=>"Knit Grey Fabric Issue Return",52=>"Knit Finish Fabric Issue Return",53=>"Grey Fabric Delivery to store",54=>"Finish Fabric Delivery to store",55=>"Chemical Transfer Entry",56=>"Grey Fabric Delivery to store roll wise",57=>"General Item Transfer",58=>"Knit Grey Fabric Receive Roll",59=>"Recipe Entry",60=>"Dyeing Re Process",61=>"Grey Fabric Issue Roll Wise",62=>"Grey Fabric Receive Roll By Batch",63=>"Grey Roll Issue to Sub Contact ",64=>"Batch Creation For Roll",65=>"AOP Roll Receive",66=>"Finish Fabric Production and QC By Roll",67=>"Finish Fabric Roll Delevery To Store",68=>"Finish Fabric Roll Receive By Store",69=>"Purchase Requisition",70=>"Yarn Purchase Requisition",71=>"Finish Fabric Roll Issue",72=>"Finish Fabric Roll Receive By Cutting",73=>"Trims Issue Return",74=>"Batch Creation for Gmts Wash",75=>"Roll Splitting",76=>"cut and lay entry" ,77=>"cut and lay entry roll wise",78=>"Trims Order To Order Transfer Entry",79=>"Lab Test Work Order",80=>"Grey Fabric Order To Sample Transfer Entry",81=>"Grey Fabric Sample To Order Transfer Entry");

//Not Used
$entry_form_for_roll=array(1=>"grey_productin_entry", 2=>"batch_creation", 3=>"Dye Production Update",4=>"finish_fabric_receive",5=>"Woven Finish Fabric Receive",16=>"Knit Grey Fabric Issue",18=>'Knit Finish Fabric Issue',19=>'Woven Finish Fabric Issue',22=>'Knit Grey Fabric Receive',23=>'Woven Grey Fabric Receive',45=>"Knit Grey Fabric Receive Return");


$form_list_for_mail=array(1=>"Daily Order Entry",2=>"Yesterday Total Activities",3=>"TNA Task Mail",4=>"Order Position By Team",5=>"Booking Revised",6=>"Missing PO List in TNA Process",7=>"Order Revised",8=>"Cancelled Order");

$entry_form_for_approval=array(1=>"Purchase Requisition Approval", 2=>"Yarn WO Approval",3=>"Dyes/Chemical WO Approval",4=>"Spare parts WO Approval",5=>"Stationary WO Approval",6=>"Pro-forma Invoice WO Approval",7=>"Fabric Booking Approval",8=>"Trims Booking Approval",9=>"Sample Booking (Without Order) Approval",10=>"Price Quatation Approval",12=>"Short Fabric Booking Approval",13=>"Sample Fabric Booking-With Order",14=>"Yarn Delivery Approval",15=>"Pre-Costing",16=>"Dyeing Batch Approval",17=>"Other Purchase WO Approval",18=>"Yarn Purchase Requisition Approval",19=>"Gate Pass Activation Approval",20=>"yarn requisition approval");//,11=>"Yarn Delivery Approval"

$wages_rate_var_for=array(1=>"Garments Cutting",2=>"Garments Finishing");

$production_resource=array(1=>"Single Needle",2=>"Flat Lock",3=>"Over Lock",4=>"Button Hole",5=>"Button Stich",6=>"Snap Button",7=>"Eyelet Hole",8=>"Kansai",9=>"Feed Of the Arm",10=>"Rib Scissoring",11=>"Ngai Sing-76",12=>"Ngai Sing-82",13=>"Ngai Sing-84",14=>"Ngai Sing-85",15=>"Double Needle",16=>"Crease Machine",17=>"Fusing Machine",18=>"Bartack",19=>"S/N Lock-Edge Cutter",20=>"Flat Bed",40=>"Helper",41=>"QuaLity Inspector",42=>"Eyelet",43=>"Table",44=>"Exam Table",45=>"Double Needle Lock" ,46=>"Double Needle Flat Lock",47=>"4 OT Over Lock",48=>"Supervisor");

$machine_category=array(1=>"Knitting",2=>"Dyeing",3=>"Printing",4=>"Finishing",5=>"Embroidery",6=>"Washing",7=>"Cutting",8=>"Sewing",9=>"CAD Machine");
$depreciation_method=array(1=>"Straight-line",2=>"Reducing Balance");

$item_transfer_criteria=array(1=>"Company To Company",2=>"Store To Store",3=>"Style To Style",4=>"Order To Order",5=>"Item To Item",6=>"Order To Sample",7=>"Sample To Order");

$party_type=array(
1=>"Buyer",
2=>"Subcontract",
3=>"Buyer/Subcontract",
4=>"Notifying Party",
5=>"Consignee",
6=>"Notifying/Consignee",
7=>"Client",
20=>"Buying Agent",
21=>"Buyer/Buying Agent",
22=>"Export LC Applicant",
23=>"LC Applicant/Buying Agent",
30=>"Developing Buyer",
80=>"Other Buyer",
90=>"Buyer/Supplier",
100=>"Also Notify Party"
);

$party_type_supplier=array(
1 =>"Supplier",
2 =>"Yarn Supplier",
3 =>"Dyes & Chemical Supplier",
4 =>"Trims Supplier",
5 =>"Accessories Supplier",
6 =>"Machineries Supplier",
7 =>"General Item",
8 =>"Stationery Supplier",
9 =>"Fabric Supplier",
20 =>"Knit Subcontract",
21 =>"Dyeing/Finishing Subcontract",
22 =>"Garments Subcontract",
23 =>"Embellishment Subcontract",
24 =>"Fabric Washing Subcontract",
25 =>"AOP Subcontract",
26 =>"Lab Test Company",
30 =>"C & F Agent",
31 =>"Clearing Agent",
32 =>"Forwarding Agent",
35 =>"Transport Supplier",
36 =>"Labor Contractor",
37 =>"Civil Contractor",
38 =>"Interior",
39 =>"Other Contractor",
40 =>"Indentor",
41 =>"Inspection",
90 =>"Buyer/Supplier",
91 =>"Loan Party");

$tna_task_catagory=array(
1 =>"General",
5 =>"Sample Approval",
6 =>"Lab Dip Approval",
7 =>"Trims Approval",
8 =>"Embellishment Approval",
9 =>"Test Approval",
15 =>"Purchase",
20 =>"Material Receive",
25 =>"Fabric Production",
26 =>"Garments Production",
30 =>"Inspection",
35 =>"Export");

$supplier_nature=array(
1=>" Goods",
2 =>"Service",
3 =>"Both"
);
$fabric_typee=array(1=>"Open Width",2=>"Tubular",3=>"Niddle Open");	
$process_type=array(1=>"Main Process",2=>"Additional Process");	
$account_type=array(1=>"CD A/C", 2=>"STD A/C", 3=>"OD A/C", 4=>"CC A/C", 5=>"BTB Margin A/C", 6=>"ERQ A/C", 7=>"Imp. LC Margin A/C", 8=>"BG Margin A/C", 9=>"ECC A/C", 10=>"PC A/C", 11=>"Advance A/C");
$core_business=array(1=>"Manufacturing",2=>"Trading",3=>"Service",4=>"Educational",5=>"Social Welfare");
$company_nature=array(1=>"Private Ltd",2=>"Public Ltd",3=>"Sole Tradership",4=>"Partnership");
$loan_type=array(0=>"Percent",1=>"Fixed");
$commercial_module=array(5=>"Garments Export Capacity",6=>"Max BTB Limit",7=>"Max PC Limit",17=>"Possible Heads For BTB",18=>"Export Invoice Rate",19=>"Doc Monitoring Standard",20=>"Internal File Source");
$cost_heads=array(0=>"--Select--","Knitting Charge"=>"Knitting Charge","Fabric Dyeing Charge"=>"Fabric Dyeing Charge","Yarn Dyeing Charge"=>"Yarn Dyeing Charge","All Over Print Charge"=>"All Over Print Charge","Dyed Yarn Knit Charge"=>"Dyed Yarn Knit Charge","Stantering Charge"=>"Stantering Charge","Brush Peach Charge"=>"Brush Peach Charge","Washing Charge"=>"Washing Charge","Printing"=>"Printing","Embroidery"=>"Embroidery","Washing"=>"Washing");

$rate_for=array(1=>"Knitting",2=>"Warping",3=>"Sizing",4=>"Knotting/ Drawing",5=>"Weaving",10=>"Dying",20=>"Cutting",30=>"Sewing",35=>"Ironing",40=>"Finishing");


$cal_parameter=array(1=>"Sewing Thread",2=>"Carton",3=>"Carton Sticker",4=>"Blister Poly ",5=>"Elastic",6=>"Gum Tap",7=>"Tag Pin",8=>"Sequines");
$cm_cost_predefined_method=array(1=>"=((SMV*CPM)*Costing per + (SMV*CPM*Costing per)* Efficiency Wastage%)/Exchange Rate",2=>"(((SMV*CPM)*Costing per / Efficiency %)+((SMV*CPM)*Costing per / Efficiency %))/Exchange Rate",3=>"{(MCE/WD)/NFM)*MPL)}/[{(PHL)*WH}]*Costing Per/Exchange Rate");
$commercial_cost_predefined_method=array(1=>"Yarn+Trims+Fabric Purchase",2=>"On Selling Price",3=>"On Net Selling Price");

$test_for=array(1=>"Garments",2=>"Fabrics",3=>"Trims");


$testing_category=array(1=>"Color Fastness",
                     2=>"Dimension & Appearance ",
                     3=>"Strength ",
                     4=>"Fabric Construction ",
                     5=>"Composition ",
                     6=>"Flammability ",
                     8=>"Fabric Performance ",
                     10=>"Garments Accessories ",
                     11=>"Stability/Appearance ",
                     12=>"Chemical Analysis",
                     13=>"Physicals",
                     14=>"Safety"
                     );
// Library Module ends //



// Merchandising
$approval_status=array( 1=>"Submitted",2=>"Rejected",3=>"Approved",4=>"Cancelled",5=>"Re-Submitted");
$order_status=array(1=>"Confirmed",2=>"Projected");
$region=array(1=>"Asia",2=>"Africa",3=>"Australia",4=>"Antarctica",5=>"Europe",6=>"North America ",7=>"South America");
$packing=array(1=>"SCSS",2=>"ACSS",3=>"SCAS",4=>"ACAS");
//$ship_mode=array(1=>"Air",2=>"Sea");
$cut_up_array=array(1=>"1st Cut-up",2=>"2nd Cut-up",3=>"3rd Cut-up");
$product_dept=array(1=>"Mens",2=>"Ladies",3=>"Teen Age-Girls",4=>"Teen Age-Boys",5=>"Kids-Boys",6=>"Infant",7=>"Unisex",8=>"Kids-Girls",9=>"Baby");
//$pord_dept=array(1=>"Menz",2=>"Ladies",3=>"Teen Age-Girls",4=>"Teen Age-Boys",5=>"Kids",6=>"Infant",7=>"Intimates"); 
$shift_name=array(1=>"A",2=>"B",3=>"C");
$country_type=array(1=>"General",2=>"Special");

$product_category=array(1=>"Garments",2=>"Intimates",3=>"Sweater",4=>"Socks",5=>"Fabric");
$garments_item=array(
1=>"T-Shirt-Long Sleeve",
2=>"T-Shirt-Short Sleeve",
3=>"Polo Shirt-Long Sleeve",
4=>"Polo Shirt-Short Sleeve",
5=>"Tank Top",
6=>"T-Shirt 3/4 ARM",
7=>"Hoodies",
8=>"Henley",
9=>"T-Shirt-Sleeveless",
10=>"Raglans",
11=>"High Neck/Turtle Neck",
12=>"Scarf",
14=>"Blazer",
15=>"Jacket",
16=>"Night Wear",
17=>"Marry Dress",
18=>"Ladies Long Dress",
19=>"Girls Dress",
20=>"Full Pant",
21=>"Short Pant",
22=>"Trouser",
23=>"Payjama",
24=>"Romper Short Sleeve",
25=>"Romper Long Sleeve",
26=>"Romper Sleeveless",
27=>"Romper",
28=>"Legging",
29=>"Three Quater",
30=>"Skirts",
31=>"Jump Suit",
32=>"Cap",
33=>"Tanktop Pyjama",
34=>"Short Sleeve Pyjama",
35=>"Jogging Pant",
36=>"Bag",
37=>"Bra",
38=>"Underwear Bottom",
39=>"Sweat Shirt",
40=>"Singlet",
41=>"Teens Singlet",
42=>"Boxer",
43=>"Stripe Boxer",
44=>"Teens Boxer",
45=>"Jersy Boxer",
46=>"Panty",
47=>"Slip Brief",
48=>"Classic Brief",
49=>"Short Brief",
50=>"Mini Brief",
51=>"Bikini",
52=>"Lingerie",
53=>"Bikers",
54=>"Underwear",
60=>"Plain Socks",
61=>"Rib Socks",
62=>"Jacuard/Patern Socks",
63=>"Heavy Gauge Socks",
64=>"Sports Socks",
65=>" Terry Socks",
66=>"Tight Socks",
67=>"Sweat Pant",
68=>"Sports Ware",
69=>"Jogging Top",
70=>"Long Pant",
71=>"Pirates Pant",
72=>"Bolero",
73=>"Strap Top",
74=>"Ladies Gypsy",
75=>"Long Sleeve Body",
76=>"Tank Top Body",
77=>"Underwear Top",
78=>"Whiper",
79=>"Sleeping Bag",
80=>"Romper Long Sleeve Boys",
81=>"Romper Long Sleeve Girls",
82=>"Romper Long Sleeve Unisex",
83=>"Baby Bodies",
84=>"TQ Pintuck Tee",
85=>"LS Pintuck Tee",
86=>"Twist Neck Pintuck",
87=>"Maxi dress",
88=>"Lace Gupsy",
89=>"Gypsy Tie Neck Tee",
90=>"V-neck Tunic",
91=>"Roll Top Jogger",
92=>"Soft Touch Jogger",
93=>"Plaited Jogger",
94=>"Loopback Jogger",
95=>"Jegging",
96=>"Mens Night Wear"
);
$composition=array(
1=>"Cotton",
2=>"Spandex",
3=>"Viscose",
4=>"Polyester",
5=>"Organic",
6=>"BCI Cotton",
7=>"Modal",
8=>"Conventional",
9=>"ECRU Melange",
10=>"Elastane",
11=>"Carded Flo Fair",
12=>"Linen",
13=>"Slub",
14=>"Coolmax",
15=>"Creap Viscos",
16=>"Filament Viscose",
17=>"Merc Viscose",
18=>"Melange",
19=>"Ecru.Melange [Ctn 99%-Vis 1%]",
20=>"G.Melange [Ctn 95%-Vis 5%]",
21=>"G.Melange [Ctn 90%-Vis 10%]",
22=>"G.Melange [Ctn 85%-Vis 15%]",
23=>"G.Melange [Ctn 80%-Vis 20%]",
24=>"PC [Polyester 65%-Ctn 35%]",
25=>"Ecru.Melange[Ctn98%-Vis2%]",
26=>"PC [Polyester 90% - Ctn 10%]",
27=>"PC [Polyester 85% - Ctn 15%]",
28=>"PC [Polyester 80% - Ctn 20%]",
29=>"PC [Polyester 75% - Ctn 25%]",
30=>"PC [Polyester 70% - Ctn 30%]",
31=>"PC [Polyester 65% - Ctn 35%]",
32=>"PC [Polyester 60% - Ctn 40%]",
33=>"PC [Polyester 50% - Ctn 50%]",
34=>"CVC [Ctn 90% - Polyester 10%]",
35=>"CVC [Ctn 85% - Polyester 15%]",
36=>"CVC [Ctn 80% - Polyester 20%]",
37=>"CVC [Ctn 75% - Polyester 25%]",
38=>"CVC [Ctn 70% - Polyester 30%]",
39=>"CVC [Ctn 65% - Polyester 35%]",
40=>"CVC [Ctn 60% - Polyester 40%]",
41=>"Cotton-Modal [Ctn 50% - Modal 50%]",
42=>"Cotton-Viscose [Ctn 50% - Viscose 50%]",
43=>"Wool",
44=>"Silk",
45=>"CM [Ctn 62% - Modal 38%]",
46=>"Lyocell",
47=>"G.Melange [Ctn 93% - Viscose 7%]",
48=>"G.Melange [Ctn 60% - Viscose 40%]",
49=>"Tencel",
50=>"CVC [Ctn 2.80% - Polyester 20%]",
51=>"Inject",
52=>"Cotton-Lurex [Ctn 95% - Lurex 5%]",
53=>"M.Lurex [Ctn 90% - Vis 5% - Lurex 5%]",
54=>"Viscose-Polyester [Vis 50% - Polyester 50%]",
55=>"G.Melange [Ctn 92% - Viscose 8%]",
56=>"G.Melange [Ctn 70% - Viscose 30%]",
57=>"CD Melange",
58=>"CB Melange",
59=>"G.Melange [Ctn 94% - Viscose 6%]",
60=>"G.Melange [Viscose 60% - Ctn 40%]",
61=>"CVC [Ctn 58% - Polyester 38%]",
62=>"Cotton-Modal [Ctn 48% - Modal 52%]",
63=>"Polyester-Linen [Polyester 85% - Linen 15%]",
64=>"Polyester-Linen [Polyester 65% - Linen 35%]",
65=>"Rayon",
66=>"Polyester-Rayon [Polyester 94% - Rayon 6%]",
67=>"Polyester-Rayon [Polyester 2% - Rayon 98%]",
68=>"PC [Polyester 2% - Ctn 98%]",
69=>"CVC [Ctn 60% - Polyester 37%- Polyester Neps 3%]",
70=>"PVC [Poly-50%, Ctn-25%, Vis-25%]",
71=>"CVC [Org-50%, Poly-38%, Vis-12%]",
72=>"Acrylic",
73=>"Organic Cotton",
74=>"G.Melange [Organic 95% - Vis 5%]",
75=>"Organic Real Melange",
76=>"Organic Cotton 50% - Cotton 50%]",
77=>"CVC [60% Org Ctn-40% Poly slub]",
78=>"CVC [55% Ctn-34% Poly-11% Viscos]",
79=>"Cotton 70% - Linen 30%",
80=>"Core Spun",
81=>"Polyamaid",
82=>"Nylon",
83=>"P/C/Cationic [Polyester - 55% Ctn - 25% Cationic - 20%]",
84=>"P/C/Cationic [Polyester - 65% Ctn - 25% Cationic - 10%]",
85=>"PCR [Polyester - 50% Ctn - 38% Rayon - 12%]",
86=>"Benetton",
87=>"Glitter",
88=>"Vortex [Ctn - 27% Poly - 65% Viscose - 8%]",
89=>"PVC [Poly - 65% Ctn - 27% Viscose - 8%]",
90=>"PV [Poly 70% - Viscose 30%]",
91=>"Linen Lyocell [Linen 50% - Lyocell 50%]",
92=>"PVC [Poly 50% - Vis 25% - Ctn 25%]",
93=>"[45 % Ctn + 55% Silk]",
94=>"[92 % Modal + 8% Cashmere]",
95=>"Cupro",
96=>"[94 % Viscose-65 Woll]",
97=>"Winter linen",
98=>"Modal 50% - Viscose 30% - Poly 20%",
99=>"Modal 50% - Ctn 30% - Poly 20%",
100=>"Ctn 50% - Poly 50%",
101=>"PCV [50% Poly - 25% Ctn - 25% Vis]",
102=>"Excel",
103=>"Bross Melange",
104=>"100% Spun Polyester",
105=>"100% Filament Polyester",
106=>"Loop 100% Cotton",
107=>"Loop 60% Cotton 40% Polyester",
108=>"Loop 100% Polyester",
109=>"PCV [Poly 50% - Ctn 38% - Vis 12%]",
110=>"CVC [Ctn 97% - Poly 3%]",
111=>"PV [Poly 70% - White Viscose 30%]",
114=>"CM[Ctn 60% - Modal 40%]",
115=>"PV[Poly 50% - Vis 50%]",
116=>"Slub[BCI Ctn 60% - Poly 40%]",
117=>"60% BCI Ctn - 40% Poly Melange",
118=>"Poly 90% - Linen 10%",
119=>"VP[Vis 90% - Poly 10%]",
120=>"TL[Tencel 50% - Linen 50%]",
121=>"PV[Poly 65% - Viscose 35%]",
122=>"CVP[CTN 58%-Viscose 35%-Poly7%]",
123=>"CP[CTN 95%-Poly 5%]",
124=>"VP[Vis 70%-Poly 30%]",
125=>"PV[Poly 75%-Vis 25%]",
126=>"CVC[Ctn 97%-Poly Neps 3%]",
127=>"PCV[Poly 38%-Ctn 50%-Vis 12%]",
128=>"LPWC [Lenzing vis 70%-Poly 20%-Wool 5%-Cashmere 5%]",
129=>"CPV [50%Ctn,38% Poly,12%Vis]",
130=>" CPV [Cotton 80% Ploy 17% Viscose 3%]",
131=>" Micro Modal Unifeel",
132=>" RP [ Rayon 90%- Poly 10%]",
133=>" TP [ Tencel 62% - Poly 38%]",
134=>" Inject [98%ctn 2% polyester]",
135=>"[cotton 95% - Viscose 5%]",
136=>"Neps yarn [cotton 98% - Poly 2%]",
137=>"[Acrylic 62% - Poly 38%]",
138=>"[Acrylic 77% - Rayon 22% - Nylon 1%]",
139=>"[Viscose 83% - Ctn17%]",
140=>"Cotton Melange",
141=>"[ Polyester 50% Linen 50%]",
142=>"[ Viscose 70% Wool 30%]",
143=>"PVC[Polyester - 50% Cotton - 35% - Viscose - 15% ]",
144=>"PCR [Polyester - 52% Cotton - 35% Rayon - 13% ]",
145=>"[Cotton 97% Viscose 3%]",
146=>" Ecru Melange[BCI Ctn 99% Vis 1%]",
147=>"Poly-Modal [Polyester 65%  Modal 35%]",
148=>"CVP [Cotton 47%  Viscose 47%  polyester 6%]",
149=>"VP [Viscose 95% Poly 5% ]",
150=>"G.Melange[85%Organic cotton-15%Viscose]",
151=>"VP [65% Viscose 35% Poly]",
152=>"Poly-Linen [99% poly 1% linen]",
153=>"PV [Poly 68% Vis 32%]",
154=>"Bamboo",
155=>"PVC[Poly 52% Cotton 33% Viscose 15%]",
156=>"PV[Poly 97% -Viscose 3%]",
157=>"Spun Polyester",
158=>"100% Cotton",
159=>"VL[90 % Viscose - 10 % Linen]",
160=>"TS[90 % Tencel - 10 % Silk]",
161=>"VT[70 % Viscose - 30 % Tencel]",
162=>"PL[90 % Polyestere - 10 % Linen]",
163=>"ML[65 % Modal - 35 % Linen]",
164=>"PLV[Polyester 82%-Linen 13%-Viscose 5%]",
165=>"PV[Poly 3%- Viscose 97%]",
166=>"PVC [Poly 65%- Viscose 25%- Ctn 10%]",
167=>"[Cotton 85%- Linen 15%]",
168=>"CVC[Ctn 57% - Poly 43%]",
169=>"[Polyester 76%- Lyocell 24%]",
170=>"CPV [Ctn 67%-Polyester 30%-Viscose 3%]",
171=>"VP[Viscose 80%-Polyester 20%]",
172=>"VP[Viscose 60%-Poly 40%]",
173=>"CW [ Ctn 70%-Wool 30%]",
174=>"CPV [Ctn 50%-Poly 40%-Vis 10%]",
175=>"PCV[Poly 38%-Ctn 32%-Vis 30%]",
176=>"CV[Ctn 95%-Vis 5% ]",
177=>"Twisted",
178=>"Modal Polyester [Modal 75%-Poly 25%]",
179=>"PV[Poly 60%-Viscose 40%]",
180=>"Cotton 92.5%-Viscose 7.5%",
181=>"Pro-Viscose [ Lenzing Viscose 70%- Tencel LF 30%]",
182=>"GM Slub [ Ctn 90%- Vis10%]",
183=>"G.Melange [Ctn 98%- Vis 2%]",
184=>"VM [White Vis 95% - Black Vis 05%]",
185=>"CL[ Ctn 80% - Linen 20% ]",
186=>"VL [ Viscose 80%- Linen 20% ]",
187=>"VP[viscose 97%-Poly 3%]",
188=>"PC [ Polyster 57% - Cotton 43% ]",
189=>"G. Melange [Ctn 65% - Vis 35%] ",
190=>"CA [ Cotton 50% - Acrylic 50% ]",
191=>"PC [Recycled poly 65% Cotton 35% ]",
192=>"PCR [Polyester-50% Cotton- 35% Rayon-15%]",
193=>"Lurex",
194=>"Cotton Lurex",
195=>"Melange Lurex",
196=>"Pima Modal [Pima Cotton 60%- Modal 40%]",
197=>"Cotton-Melange[60% cotton 40% B.Cotton]",
198=>"Cotton-Melange[75% cotton 25% B.Cotton]",
199=>"Rayon Lurex [Rayon 75% - Lurex 25% ]",
200=>"Pes Rayon [ Pes 77% - Rayon 23% ]",
201=>"Linen Rayon Neps [ Linen 55% - Rayon Neps 45% ] ",
202=>"Rayon Poly [ Rayon 50 % - Poly 50% ]",
203=>"VCPL [Vis 40% - Ctn 35% - Poly 20% - Golden Lurex 5% ]",
204=>"Linen Viscose [ Linen 75% - Vis 25% ]",
205=>"[poly 81% - rayon 17 % - polyurethane 2%]",
206=>"Organic FT Cotton",
207=>"Lenzing Viscose",
208=>"Cotton Slub",
209=>"CVC [Loop Cotton 65% Polyester 35%]",
210=>"Modal-Cotton[ Modal 70%-Cotton 30%]",
211=>"CL [Cotton 85 %-Linen 15%]",
212=>"CVB[Ctn42%- Vis33%- Black Ctn25%]",
213=>"Organic Cotton 50% - Lenzing Modal 50%",
214=>"G.Melange[Organic Cotton 50% - Lenzing Modal 50%]",
215=>"Blue Melange[Organic Cotton 50% - Lenzing Modal 50%]",
216=>"[50% Organic CTN -30 CTN-20% Polyester]",
217=>"CVP [Cotton 50% - Viscose 25% - Polyester 25% ]",
218=>"Rayon Poly [Rayon 85% - Polyester 15%]",
219=>"[Polyester 70% - Rayon 25% - Elastane 5%]",
220=>"Ecru.Melange [Ctn 97%- Vis 3%]",
221=>"CV [ Ctn 80% - Vis 20% ]",
222=>"PV [ Poly 83 % - Vis 17 % ]",
223=>"CPL [ Ctn 50% - Poly 40% - Linen 10%]",
224=>"TC [Tencel 70% Cotton 30%]",
225=>"PCV [ Poly 30% - Ctn 53.2% - Vis 16.8]",
226=>"CV [ Ctn 60% - Vis 40% ]",
227=>"Organic Cotton 50%-Tencel 50%",
228=>"CV[Cotton65%- Viscose35%]",
229=>"[50%Org.CTN-40% POL-10% CTN]",
230=>"Pima Cotton",
231=>"VC [ Viscose 83% - Cotton 17 % ]",
232=>"CV[Cotton 55%-Viscose 45%]",
233=>"[Viscose 60% - Modal Melange 40%]",
234=>"PVL [Polyester 55%- Viscose 25%- Linen 20%]",
235=>"CVC [50% Org.Ctn-30% Conv.Ctn-20% Polyester]",
236=>"CVC [60% Ctn-40% Poly Slub]",
237=>"PL (Polyester 80% - Linen 20%)",
238=>"Polyester Cationic",
239=>"Melange [viscose 60%- Cotton 40%]",
240=>"CVC [Organic Cotton 80% Polyester 20%]",
241=>"MC[Lenzing Modal 50% Organic Cotton 50%]",
242=>"Slub [Rayon 60% - Cotton 40%]",
243=>"[Organic Cotton 50% - Poly 40% - Cotton 10%]",
244=>"Filament",
245=>"CVC[Cotton 60%-Viscose 40%]",
246=>"[Ctn 70%-Poly 25%-Vis 5%] ",
247=>"CVC [Ctn 87%-Poly 13%] ]",
248=>"[Ctn 82%-Poly 14%-Vis 4%]",
249=>"CVCK(96%-4%) NY",
250=>"CVCK(97%-3%) NAPY",
251=>"PCVK(2%-88-10%) NAPY",
252=>"PC[Polyester 96%-Cotton 4%]",
253=>"[ 50% Viscose, 48% Polyester, 2% Elastane]",
254=>"PVC[Polyester-50% Viscose-12% Snow-38%]",
255=>"PVC[Polyester-20% Viscose-65% Cotton-15%]",
256=>"Conventional Cotton",
257=>"[Ctn 83%-Poly 13%-Vis 4%]",
258=>"[Ctn 82%-Poly 13%-Vis 5%]",
259=>"40%linen -60% Lyocell",
260=>"90% Viscose 10% glitter",
261=>"PVC [Poly 50%-Cot 25%- Vis 25%]",
262=>"BC[ Bamboo 60%- Cotton 40%]",
263=>"EC [Excel 70%- Cotton 30%] ",
264=>"[Ctn 93% - Visc 4% - Lurex 3%]",
265=>"[Ctn 97% - Lurex 3%]",
266=>"[Organic 50% - Conventional Ctn 49% - Visc 1%]",
267=>"TC [ Cotton 60%- T 40%]",
268=>"PC [Poly 58%- Ctn42%]",
269=>"BCV [BCI Ctn 95% - Visc 5%]",
270=>"Cotton-Silk [Cotton 85%- Silk 15%]",
271=>"Cotton-Silk[Ctn 70% - Silk 30%]",
272=>"[Cotton95% - Injected Viscose5%]",
273=>"[Rayon 55%-Polyester 34%-Angelina 1%]",
274=>"RPS[Rayon 66% - Poly 27% - Spandex 7%]",
275=>"VPS[Viscose 66% - Poly 27% - Spandex 7%]",
276=>"RL [Rayon 85% - Linen 15%]",
277=>"CPV [Ctn80%-Poly10% -Vis10%]",
278=>"Ecro Melange[Org.Ctn99% - Visc 1%]",
279=>"[Cotton 92.5% - Viscose 7.5%]",
280=>"VC [Lenzing Viscose 50% - Organic Cotton 50%]",
281=>"Neps Yarn",
282=>"CVC [Ctn 98% - Poly 2%]",
283=>"VL [Viscose 70% - Linen30%]",
284=>"CVC [Ctn 91% - Polyester 9% ]",
285=>"[Cotton 96% - Viscose 4%]",
286=>"PV[Polyester 95% - Viscose 5%]",
287=>"VL [ Viscose 50% - linen 50% ]",
288=>"FT Cotton",
289=>"CVC [60% FT Ctn-40% Poly]",
290=>"CVC [80% FT Ctn-20% Poly]",
291=>"Poly",
292=>"Org Cotn 50%-Modal 50%",
293=>"[Cotton 75% - Viscose 25%]",
294=>"[Viscose 65%-Linen 30%-poly 5%]",
295=>"G.Melange [Ctn 92.5%-Vis 7.5%]",
296=>"[Cotton 50%- Modal 45% -Melange 5%]",
297=>"[Org ctn 50% - BCI ctn 48% - polyester 2%]",
298=>"G.Melange[Org Cotn 95% -Vis 5%]",
299=>"PC[Poly 52%- Cotn 48%]",
300=>"PV[Polyester 80% - Viscose 20%]",
301=>"[Viscose 94% -acrylic 5%- Poly 1%]",
302=>"[Poly 84% - Viscose 16% ]",
303=>"RP [Rayon 55%- Poly 45%]",
304=>"CVC[cotton 55%- Poly 45%]",
305=>"Inject[Poly 90.3% - Ctn 9.7%]",
306=>"CVC [Ctn 52% - Poly 48%]",
307=>"[Poly 47% - Visc 51% - Elast 2%]"
);

/*$unit_of_measurement=array(
"PIECES"=>array(
01=>"Pcs", 
02=>"Dzn",
03=>"Grs",
04=>"GG"
),
"WEIGHT"=>array(
10=>"Mg",
11=>"Gm",
12=>"Kg",
13=>"Quintal",
14=>"Ton"
),
"LENGTH"=>array(
20=>"Km",
21=>"Hm",
22=>"Dm",
23=>"Mtr",
24=>"Dcm",
25=>"CM",
26=>"MM",
27=>"Yds",
28=>"Feet",
29=>"Inch"
),
"LIQUID"=>array(
40=>"Ltr",
41=>"Ml"
),
"OTHERS"=>array(
50=>"Roll",
51=>"Coil",
52=>"Cone",
53=>"Bag",
54=>"Box",
55=>"Drum",
56=>"Bottle",
57=>"Pkt",
58=>"Set"
)
);*/
$unit_of_measurement=array(
1=>"Pcs", 
2=>"Dzn",
3=>"Grs",
4=>"GG",
10=>"Mg",
11=>"Gm",
12=>"Kg",
13=>"Quintal",
14=>"Ton",
15=>"Lbs",
20=>"Km",
21=>"Hm",
22=>"Dm",
23=>"Mtr",
24=>"Dcm",
25=>"CM",
26=>"MM",
27=>"Yds",
28=>"Feet",
29=>"Inch",
30=>"CFT",
31=>"SFT",
40=>"Ltr",
41=>"Ml",
50=>"Roll",
51=>"Coil",
52=>"Cone",
53=>"Bag",
54=>"Box",
55=>"Drum",
56=>"Bottle",
57=>"Pack",
58=>"Set",
59=>"Can",
60=>"Each",
61=>"Gallon",
62=>"Lachi",
63=>"Pair",
64=>"Lot",
65=>"Pocket",
66=>"Pot",
67=>"Book",
68=>"Culind",
69=>"Rim",
70=>"Cft",
71=>"Syp",
72=>"K.V",
73=>"CU-M3"
);
$pord_dept=array(1=>"Mens",2=>"Ladies",3=>"Teen Age-Girls",4=>"Teen Age-Boys",5=>"Kids",6=>"Infant",7=>"Intimates"); 

//merchandise variable settings
$order_tracking_module=array(12=>"Sales Year started",14=>"TNA Integrated",15=>"Pre Costing : Profit Calculative",18=>"Process Loss Method",19=>"Consumtion Basis",20=>"Copy Quotation",21=>"Conversion From Chart",22=>"CM Cost Predefined Method",23=>"Color From Library",24=>"Yarn Dyeing Charge (In WO) from Chart",25=>"Publish Shipment Date",26=>"Material Control",27=>"Commercial Cost Predefined Method",28=>"Gmt Number repeat style",29=>"Duplicate Ship Date",30=>"Image Mandatory",31=>"Tna Process type",32=>"Po Update Period",33=>"Po Receive Date",34=>"Inquery ID Mandatory",35=>"Trim Rate",36=>"CM Cost Predefined Method (Price Quatation)",37=>"Budget Validation",38=>"S.F. Booking Before M.F. 100%",39=>"Lab Test Rate Update",40=>"Colar Culff Percent",41=>"per-cost approval",42=>"Report Date Catagory"); 
$pre_cost_approval=array(1=>"Electronic Approval",2=>"Manual Approval");

$process_loss_method=array(1=>"Markup Method",2=>"Margin method");
$consumtion_basis=array(1=>"Cad Basis",2=>"Measurement Basis",3=>"Marker Basis");
//$wo_category = array(2=>"Knit Fabrics",3=>"Woven Fabrics",4=>"Accessories",13=>'Grey Fabric(Knitt)',14=>'Grey Fabric(woven)',12=>"Services");
$gmts_nature= array(1=>"Knit Garments",2=>"Woven Garments",3=>"Sweater");
$incoterm=array(1=>"FOB",2=>"CFR",3=>"CIF",4=>"FCA",5=>"CPT",6=>"EXW",7=>"FAS",8=>"CIP",9=>"DAF",10=>"DES",11=>"DEQ",12=>"DDU",13=>"DDP");
$fabric_source=array(1=>"Production",2=>"Purchase",3=>"Buyer Supplied",4=>"Stock");
$color_range=array(1=>"Dark Color",2=>"Light Color",3=>"Black Color",4=>"White Color",5=>"Average Color",6=>"Melange",7=>"Wash",8=>"Scouring",9=>"Extra Dark",10=>"Medium Color");
$costing_per=array(1=>"For 1 Dzn",2=>"For 1 Pcs",3=>"For 2 Dzn",4=>"For 3 Dzn",5=>"For 4 Dzn");
$delay_for=array(1=>"Sample Approval Delay",2=>"Lab Dip Approval Delay",3=>"Trims Approval Delay",4=>"Yarn In-House Delay",5=>"Knitting Delay",6=>"Dyeing Delay",7=>"Fabric In-House Delay",8=>"Trims In-House Delay",9=>"Print/Emb Delay",10=>"Line Insufficient",11=>"Worker Insufficient",12=>"Bulk Prod. Approval Delay",13=>"Traget Falilure",14=>"Inspection Fail",15=>"Production Problem",16=>"Quality Problem");
//$body_part=array(1=>"Main Fabric",2=>"Collar",3=>"Culf",4=>"Rib",5=>"Hood",6=>"Pocketing",7=>"Bottom Rib",8=>"Sleeve",9=>"Back Part",10=>"Front Part");

$body_part=array(1=>"Main Fabric Top",2=>"Collar",3=>"Cuff",4=>"Rib",5=>"Flap",6=>"Hood",7=>"Pocketing",8=>"Bottom Rib",9=>"Sleeve",10=>"Back Part",11=>"Front Part",12=>"Facing Fabric",13=>"Binding",14=>"Body part-1",15=>"Body part-2",16=>"Body part-3",17=>"Body part-4",18=>"Shoulder",19=>"Hood lining",20=>"Main Fabric Bottom",21=>"Pocketing Bottom",22=>"Bottom Rib Bottom",23=>"Waist belt insert",24=>"Waist belt",25=>"Back Loop",26=>"Back Yoke",27=>"Epaulate/Tab",28=>"Assembly",29=>"Fly",30=>"Back Pocket FCG",31=>"Back Flap",32=>"Loop",33=>"Side Flap",34=>"Side Pocket FCG",35=>"Side Pocket",36=>"Front Top",37=>"Front/Back Waist Band",38=>"Tape",39=>"Back Pocket",40=>"Placket",41=>"Neck Tape",42=>"Piping",43=>"Pocket binding",44=>"Neck band",45=>"Neck insert",46=>"Drawstring",47=>"Contrast insert",48=>"Mesh insert",49=>"Tricot mesh",50=>"Woven insert",51=>"Double layer",52=>"Side slit",53=>"Front Yoke",54=>"Applique fabrics",55=>"Neck Rib",56=>"Inner Neck",57=>"Sleeve Layer",58=>"Half Moon",59=>"Neck",60=>"Back Moon",61=>"Back Top",62=>"Carpenter Pocket",63=>"West Belt 1",64=>"Front Pocket",65=>"Coin Pocket",66=>"Welt Pocket",67=>"Front Welt Pkt",68=>"Collar Tai",69=>"Inner Front Body",70=>"Facg Tape",71=>"Sleeve Tape",72=>"Thigh Flap",73=>"Thigh Pocket",74=>"Waist Rib",75=>"Neck Binding",76=>"Bow",77=>"Half Moon+Under layer",78=>"Neck tape loops",79=>"Zipper Ups Binding",80=>"Bottom cuff and pocket binding",81=>"Back Tape",82=>"Fake Sleeve",83=>"Chest Piping",84=>"West Belt + Hem",85=>"Neck + Bottom + Cuff Rib",86=>"Drawstring + Pocket Binding",87=>"Neck + Bottom + Collar Rib",88=>"Neck + Cuff",89=>" Bottom + Cuff Rib",90=>"Tipping at Rib",91=>"Neck + Armhole Binding",92=>"Bottom Patch",93=>"Left Sleeve",94=>"Right Sleeve",95=>"Back Part + Front Part",96=>"Ruffles",97=>"Collar Stand",98=>"Back Hem",99=>"Back Facing",100=>"Back Part-2",101=>"Band",102=>"Bottom Hem",103=>"Box",104=>"Box Plaket",105=>"Coin Welt  Pkt",106=>"Facing",107=>"Front Flap",108=>"Front Hem",109=>"Front Part-2",110=>"Gambel",111=>"Harmoniam",112=>"Leg Hem",113=>"Lining Loop",114=>"Pannel",115=>"Pkt Piping",116=>"Rib Pkt",117=>"Sam",118=>"Sleeve Hem",119=>"Welt Pkt-2",120=>"Inner Bra",121=>"Sample Fabric",122=>"Frill",123=>"Neck and armhole strap",124=>"Printed Label",125=>"Main Fabric+Placket",126=>"Back Neck Patch",127=>"Embroidery Inside",128=>"Main Fabric Bottom Waist Belt",129=>"Insert Back Body",130=>"Bottom Insert",131=>"Main Fabric Top + Sleeve",132=>"Main Fabric Top + Neck",133=>"Collar Inside",134=>"Sleeve Panel",135=>"Back + Slv + N.Tape",136=>"Back Moon Placket",137=>"Key Hole Binding",138=>"Neck raw edge",139=>"Sleeve raw edge",140=>"Three Quarter Sleeve Tee",141=>"Shoulder tape",142=>"Gusset",143=>"Back Part + Slv",144=>"Herringbone tape",145=>"Inner Placket",146=>"Upper Placket",147=>"Raglan slv + Hood",148=>"Hood + slv",149=>"Main fabric + Placket + Foot",150=>"Front Lower part",151=>"Front Lower part + pocket",152=>"Back part + Pocket",153=>"Raglan Slv",154=>"Cuff Bind",155=>"Insert",156=>"Boon",157=>"Insert Bind",158=>"Crochet Lace",159=>"Hood Lining + Zipper Binding + Back Tape",160=>"Side piping",161=>"Neck Tape + Piping",162=>"Youk Shoulder");

$color_type=array(1=>"Solid",2=>"Stripe (Y/D)",3=>"Cross Over (Y/D)",4=>"Check (Y/D)",5=>"AOP",6=>"Solid (Y/D)",7=>"AOP Stripe", 20=>"Florecent",25=>"Reactive",26=>"Melange",27=>"Marl");

$dyeing_sub_process = array(1=>"Demineralisation",10=>"Pretreatment",20=>"Neutralisation-1",21=>"Neutralisation-2",22=>"Neutralisation-3",23=>"Neutralisation-4",30=>"Biopolish",40=>"Dyestuff",50=>"Dyeing Bath",60=>"After Treatment",70=>"Color Remove",90=>"Other",91=>"Leveling");

$dose_base = array(1=>"GPLL",2=>"% on BW");
//$conversion_cost_head_array=array(1=>"Knitting",2=>"Weaving",30=>"Yarn Dyeing",31=>"Fabric Dyeing",32=>"Tube Opening",33=>"Heat Setting",34=>"Stiching Back To Tube",35=>"All Over Printing",36=>"Stripe Printing",37=>"Cross Over Printing",60=>"Scouring",61=>"Color Dosing",62=>"Neutralization",63=>"Squeezing",64=>"Washing",65=>"Stentering",66=>"Compacting",67=>"Peach Finish",68=>"Brush",69=>"Peach+Brush",70=>"Heat+Peach",71=>"Peach+Brush+Heat",72=>"UV Prot",73=>"Odour Finish",74=>"Teflon Coating",75=>"Cool Touch",76=>"MM",77=>"Easy Care Finish",78=>"Water Repellent",79=>"Flame Resistant",80=>"Hydrophilics",81=>"Antistatic",82=>"Enzyme",83=>"Silicon", 84=>"Softener", 85=>"Brightener",86=>"Fixing/Binding Agent",87=>"Leveling Agent",101=>"Dyes & Chemical Cost");
$conversion_cost_head_array=array(
1=>"Knitting",
2=>"Weaving",
3=>"Collar and Cuff Knitting",
4=>"Feeder Stripe Knitting",
25=>"Dyeing+Enzyme+Silicon",
26=>"Dyeing+No Enzyme+Silicon",
30=>"Yarn Dyeing",
31=>"Fabric Dyeing",
32=>"Tube Opening/Sliting",
33=>"Heat Setting",
34=>"Stiching Back To Tube",
35=>"All Over Printing",
36=>"Stripe Printing",
37=>"Cross Over Printing",
38=>"Reversing",
39=>"Discharge Dyeing",
40=>"Discharge Print",
60=>"Scouring",
61=>"Color Dosing",
62=>"Neutralization",
63=>"Slitting/Squeezing",
64=>"Normal Wash",
65=>"Stentering",
66=>"Open Compacting",
67=>"Peach Finish",
68=>"Brush",
69=>"Peach+Brush",
70=>"Heat+Peach",
71=>"Peach+Brush+Heat",
72=>"UV Prot",
73=>"Odour Finish",
74=>"Teflon Coating",
75=>"Cool Touch",
76=>"MM",
77=>"Easy Care Finish",
78=>"Water Repellent",
79=>"Flame Resistant",
80=>"Hydrophilics",
81=>"Antistatic",
82=>"Enzyme",
83=>"Silicon Finish", 
84=>"Softener", 
85=>"Brightener",
86=>"Fixing/Binding Agent",
87=>"Leveling Agent",
88=>"Sueding",
89=>"Double Enzyme",
90=>"Tube Dryer",
91=>"Tube Compacting",
92=>"Carbon",
93=>"Trumble Dryer",
94=>"Singeing",
100=>"Bag Sewing",
101=>"Dyes & Chemical Cost",
120=>"Cutting",
121=>"Gmts. Printing",
122=>"Gmt. Embroidery",
123=>"Gmts. Washing",
124=>"Sewing",
125=>"Calendering",
127=>"Shearing",
128=>"Combing",
129=>"Burn Out",
130=>"Iron",
131=>"Gmts Finishing",
132=>"Peroxide Wash",
133=>"Curing",
134=>"Twisting",
135=>"Scouring + Enzyme + Silicon",
136=>"Double Stentering",
137=>"Double Dyeing",
138=>"Dyeing Enzyme Silicon Boi-Wash"
);

$mandatory_subprocess=array(33=>"Heat Setting",34=>"Stiching Back To Tube",94=>"Singeing",60=>"Scouring",63=>"Slitting/Squeezing",65=>"Stentering",66=>"Open Compacting",78=>"Water Repellent",82=>"Enzyme",90=>"Tube Dryer",91=>"Tube Compacting");//need to consult with sir

$conversion_cost_type=array(1=>"Knitting",10=>"Yarn Dyeing",11=>"Dyeing",12=>"AOP",13=>"Wash",20=>"Finishing",21=>"Chemical Finish",22=>"Special Finishing",40=>"Dyes & Chemical "); 
$emblishment_name_array=array(1=>"Printing",2=>"Embroidery",3=>"Wash",4=>"Special Works",5=>"Others");
$cost_heads_for_btb=array(1=>"Knitting",30=>"Yarn Dyeing",31=>"Fabric Dyeing",35=>"All Over Printing",75=>"Knit Fabric Purchase",78=>"Woven Fabric Purchase",101=>"Printing",102=>"Embroidery",103=>"Wash");//101 means 1, 102 means 2, 103 means 3,4=>"Feeder Stripe Knitting",64=>"Washing Charge",65=>"Stentering",68=>"Brush"

$emblishment_print_type=array(1=>"Rubber",2=>"Glitter",3=>"Flock",4=>"Puff",5=>"High D",6=>"Foil",7=>"Rubber+Foil",8=>"Rubber+Silver",9=>"Pigment",10=>"Rubber+Pearl",11=>"Rubber+Sugar",12=>"Transfer / Sel",13=>"Crack",14=>"Photo",15=>"Foil+Photo",16=>"Pigment+Stud",17=>"Rubber+Stud",18=>"Rubber+Glitter",19=>"Photo+Silicon",20=>"Rubber+Silicon",21=>"Rubber+Stud/Stone",22=>"Photo+Stud/Stone",23=>"Rubber+Flock",24=>"Photo+Flock",25=>"Discharge",26=>"Discharge+ Flock",	27=>" Discharge + Pigment",28=>"Pigment + Glitter",29=>"Pigment + Foil",30=>"pigment+ Plastisol",31=>"Plastisol",32=>"Flou color",33=>"Fluo +Pigment",34=>"Photo + Pigment",35=>"Reverse",36=>"Reverse + Pigment",37=>"Aop",38=>"Burnout",39=>"Sublimation",40=>"Heat Press",41=>"Pigment + Rubber",42=>"Emboss",43=>"Leaser Print",44=>"Glow In Dark");
$emblishment_embroy_type=array(1=>"Applique",2=>"Plain",3=>"Sequence",4=>"Patch Label ");
$emblishment_wash_type=array(1=>"Normal",2=>"Pigment",3=>"Acid",4=>"PP Spray/Dz",5=>"Enzyme",6=>"Enzyme+Silicon",7=>"Grinding",8=>"Cold Dye",9=>"Tie Dye",10=>"Batik Dye",11=>"Deep Dye",12=>"P.P Spray + Bleach Wash",13=>"Enzyme + Bleach wash",14=>"Burnout Wash",15=>"Crinkle Wash",16=>"Direct dyeing Acid Wash");
$emblishment_spwork_type=array(1=>"Stone",2=>"Bow",3=>"Ribbon",4=>"Beeds",5=>"H/Press");
$commission_particulars=array(1=>"Foreign",2=>"Local");
$commission_base_array=array(1=>" in Percentage",2=>"Per Pcs",3=>"Per Dzn");
$camarcial_items=array(1=>"LC Cost ",2=>"Port & Clearing",3=>"Transportation",4=>"All Togather");
$size_color_sensitive=array(1=>"As per Gmts. Color",2=>"Size Sensitive",3=>"Contrast Color",4=>"Color & Size Sensitive");
$shipment_status = array(0=>"ALL",1=>"Full Pending",2=>"Partial Shipment",3=>"Full Shipment/Closed");
$pay_mode=array(1=>"Credit",2=>"Import",3=>"In House",4=>"Cash");
$actual_cost_heads=array(1=>"Testing Cost",2=>"Freight Cost",3=>"Inspection Cost",4=>"Courier Cost",5=>"CM",6=>"Commercial");
$aop_nonor_fabric_source=array(1=>"Sample Booking", 2=>"Transfer From Order");
//---------------------------------------------------------------------Start production Module Array------------------------------------------------------//
$cause_type=array(1=>'Disorder',2=>'Routine Maintenance',3=>'Job Not Available',4=>'Job Not Assigned',5=>'Operator Not Available',6=>'Worker Unrest',7=>'Off-Day',8=>'Material Not Available',50=>'Others');
$production_type=array(1=>"Cutting",2=>"Printing",3=>"Print Received",4=>"Sweing In",5=>"Sewing Out",6=>"Finish Input",7=>"Iron Output",8=>"Gmts Finish",9=>"Cutting Delivery");
$batch_for=array(1=>"Fabric Dyeing",2=>"Yarn Dyeing");
$batch_against=array(1=>"Buyer Order",2=>"Re-Dyeing",3=>"Sample",4=>"External",5=>"Without Booking",6=>"Gmts Wash");
$inspection_status=array(1=>"Passed",2=>"Re- Check",3=>"Failed");
$inspection_cause=array(1=>"Major",2=>"Minor");
$loading_unloading=array(1=>'Loading',2=>'Un-loading');
$dyeing_result=array(1=>'Shade Matched',2=>'Re-Dyeing Needed',3=>'Fabric Damaged',4=>'Incomplete',5=>'Under Trial');
$dyeing_method=array(1=>"Black-B Process",10=>"Vacilis Process",20=>"Turquise (80-95-80)<1",30=>"Turquise (80-95-80)>1",40=>"All 60&#8451 Normal",50=>"All 60&#8451 Critical",60=>"40-60 Process",70=>"Polyestar Process",80=>"White Process",90=>"Viscose Process",100=>"CVC/PC Double Part (Vacilis Process)",110=>"CVC/PC Double Part (Separate Reduction Process)");
$batch_status_array=array(0=>"Incomplete", 1=>"Complete");

$worker_type=array(1=>"Salary Based Worker",2=>"Piece Rate Worker");
$piece_rate_wq_limit_arr=array(1=>"Up to Order Qty",2=>"Up to Plan Cut Qty");
$fabric_type_for_dyeing=array(1=>'Cotton',2=>'Polyster',3=>'Lycra',4=>'Both Part');
$inspected_by_arr=array(1=>'Buyer',2=>'3rd Party',3=>'Self');
$validation_type=array(1=>"Order Wise",2=>"Country Wise");
$defect_type=array(1=>"Alter",2=>"Spot",3=>"Reject");
$sew_fin_alter_defect_type=array(1=>"Fab Fault/ Colour Variation",2=>"Run of seam",3=>"Open Seam",4=>"Skip Stitch",5=>"Uneven Top Stitch",6=>"Broken Stitch",7=>"Loose Stitch",8=>"Irregular Stitch",9=>"Puckering",10=>"Label Wrong Mistake",11=>"Slanted At Label",12=>"Rawadge",13=>"Missing Tuck",14=>"Wrong Tacking",15=>"Up Down",16=>"Missing Lbl /Bartack",17=>"Shading",18=>"Pleat",19=>"Others");
$sew_fin_spot_defect_type=array(1=>"Dirty Stain",2=>"Oil Stain");
//---------------------------------------------------------------------Start production Module Array END --------------------------------------------------//



//---------------------------------------------------------------------Start Commercial Module Array------------------------------------------------------//
$source=array(1=>"Abroad",2=>"EPZ",3=>"Non-EPZ");
$pi_basis=array(1=>"Work Order Based",2=>"Independent");
$wo_basis=array(1=>"Requisition Based",2=>"Independent",3=>"Buyer PO");
$lc_basis=array(1=>"PI Basis",2=>"Independent");
$convertible_to_lc=array(1=>"LC/SC",2=>"No",3=>"Finance");
$pay_term=array(1=>"At Sight",2=>"Usance",3=>"Cash In Advance",4=>"Open Account");
$shipment_mode=array(1=>"Sea",2=>"Air",3=>"Road",4=>"Train",5=>"Sea/Air",6=>"Road/Air"); 
$contract_source=array(1=>"Foreign",2=>"Inland");

$yarn_type=array(1=>"Carded",2=>"Combed",3=>"Compact",4=>"Polyster",5=>"CVC",6=>"PC",7=>"Melange",8=>"Micro Poly",9=>"Rottor",10=>"Slub",11=>"Spandex",12=>"Viscose",13=>"Modal Cotton",14=>"BCI",15=>"Modal",16=>"Semi Combed",17=>"Special",18=>"Cotton Linen",19=>"Pima",20=>"Su-Pima",21=>"Lurex",22=>"PV",23=>"Tencel",24=>"Excel/Linen",25=>"CV",26=>"CVC Slub",27=>"Pmax",28=>"Mercerize",29=>"Organic",30=>"Twist",31=>"Melange Slub",32=>"Melange Neps",33=>"Neps",34=>"Ctn Melange",35=>"Inject",36=>"Cotton Lurex",37=>"Melange Lurex",38=>"Viscos/Linen",39=>"Vortex",40=>"Polyester/Linen",41=>"CB Slub",42=>"PC Slub",43=>"Carded Slub",44=>"Org-Melange",45=>"PVC",46=>"Acrylic",47=>"Spun",48=>"Viscose-Wool",49=>"Linen-Tencel",50=>"Viscose Melange",51=>"Ploy Filament",52=>"Spun Poly",53=>"Ring Spun",54=>"Poly Coolmax", 55=>"Poly HScool", 56=>"Poly Thermolit", 57=>"Poly Trevira", 58=>"Poly CD Yarn", 59=>"Cambric Viscose", 60=>"Ring Card", 61=>"CVC Melange", 62=>"PC Melange", 63=>"Modal Linen", 64=>"Siro", 65=>"Viscose Slub", 66=>"CPV ", 67=>"VC", 68=>"Cotton-Tencil", 69=>"Cotton-Rayon", 70=>"Siro Slub", 71=>"Inject Slub Melange",72=>"Pima Melange",73=>"Triblend",74=>"Space Slub",75=>"Carded Ring Spun",76=>"Combed Slub");

$service_type= array(1=>"Knitting",2=>"Collar and Cuff Knitting",3=>"Feeder Stripe Knitting",10=>"Yarn Dyeing",11=>"Fabric Dyeing",12=>"All Over Printing",20=>"Scouring",21=>"Brushing",22=>"Sueding",23=>"Washing",24=>"Stentering",25=>"Compacting",40=>"Cutting",41=>"Gmts. Printing",42=>"Gmt. Embroidery",43=>"Gmts. Washing",44=>"Sewing");
//$service_type= array(1=>"AOP",2=>"Yarn Dyeing",3=>"Gmt. Print",4=>"Gmt. Embroidery",5=>"Gmt. Wash",6=>"Scouring",7=>"Brushing",8=>"Sueding",9=>"Knitting",10=>"Dyeing",11=>"Collar and Cuff Knitting",12=>"Feeder Stripe Knitting",13=>"Stripe Print Charge",20=>"Others");
//$export_finance_loan_type=array(1=>"Packing Credit",2=>"Export Cash Credit");
 
$lc_type = array(
	1	=> "BTB LC",
	2	=> "Margin LC",
	3	=> "Fund Building" 
);


$source_pay_term = array(
	1	=> "01 Import LC - At sight",
	2	=> "02 Import LC - Usance",
	3	=> "03 BTB Inland - At sight",
	4	=> "04 BTB Inland - Usance",
	5	=> "05 BTB Foreign LC - At sight",
	6	=> "06 BTB Foreign LC - Usance",
	7	=> "10 Import LC EPZ - Usance",
	8	=> "11 BTB LC EPZ - At sight",
	9	=> "12 BTB LC EPZ - Usance",
	10	=> "99 Import From Inland to EPZ" 
);

$maturity_from = array(
	1	=> "Acceptance Date",
	2	=> "Shipment Date",
	3	=> "Negotiation Date",
	4	=> "B/L Date" 
);
 $credit_to_be_advised=array(1=>"Teletransmission",2=>"Airmail",3=>"Courier",4=>"Airmail/Courier",5=>"Telex",6=>"SWIFT");
 $increase_decrease=array(1=>"Increase",2=>"Decrease");
 
$export_item_category=array(1=>"Knit Garments",2=>"Woven Garments",3=>"Sweater Garments",4=>"Leather Garments",10=>"Knit Fabric ",11=>"Woven Fabric", 20=>"Knitting", 21=>"Weaving", 22=>"Dyeing & Finishing", 22=>"All Over Printing", 24=>"Fabric Washing", 30=>"Cutting", 31=>"Sewing", 35=>"Gmts Printing", 36=>"Gmts Embroidery", 37=>"Gmts Washing", 40=>"Yarn", 45=>"Trims", 50=>"Chemical", 51=>"Dyes", 55=>"Food Item", 60=>"Medicine", 65=>"Transportation", 66=>"C & F"); 

$commercial_head = array(1=>"Negotiation Loan/Liability", 5=>"BTB Margin/DFC/BLO A/C", 6=>"ERQ A/C", 10=>"CD Account", 11=>"STD A/C", 15=>"CC Account", 16=>"OD A/C20", 20=>"Packing Credit", 21=>"Bi-Salam/PC", 22=>"Export Cash Credit", 30=>"EDF A/C", 31=>"PAD", 32=>"LTR", 33=>"FTT/TR", 34=>"LIM", 35=>"Term Loan", 36=>"Force Loan", 40=>"IFDBC Liability", 45=>"Bank Charge", 46=>"SWIFT Charge", 47=>"Postage Charge", 48=>"Handling Charge", 49=>"Source Tax", 50=>"Excise Duty", 51=>"Foreign Collection Charge", 60=>"Other Charge", 61=>"Foreign Commision", 62=>"Local  Commision", 63=>"Penalty on Doc Descrepency", 64=>"Penalty on Goods Descrepency", 65=>"FDBC Commision", 70=>"Interest", 71=>"Import Margin A/C", 75=>"Discount A/C", 76=>"Advance A/C", 80=>"HPSM", 81=>"Sundry A/C", 82=>"MDA Special", 83=>"MDA UR", 84=>"Vat", 85=>"FDR Build up", 86=>"Miscellaneous Charge", 87=>"others Fund(sinking)", 88=>"Bank Commission", 89=>"Vat On Bank Commission", 90=>"Insurance Coverage", 91=>"Add Confirmation Change", 92=>"MDA Normal", 93=>"Settlement A/C", 94=>"Cash Security A/C", 95=>"Loan A/C", 96=>"Courier Charge", 97=>"Telex Charge", 98=>"Application Form Fee");

$acceptance_time=array(1=>"After Goods Receive",2=>"Before Goods Receive");
$document_status=array(1=>"Original",2=>"Copy");
$submited_to=array(1=>"Lien Bank",2=>"Buyer");
$submission_type=array(1=>"Collection",2=>"Negotiation");
$supply_source=array(01=>"01 Foreign Cash Sight",02=>"02 Foreign Deferred Cash",03=>"03 EDF Local",04=>"04 Local",05=>"05 EDF Foreign",06=>"06 Foreign",12=>"12 EPZ",99=>"99 Others");
//--------------------------------------------------------------------End Commercial Module Array--------------------------------------//

//--------------------------------------------------------------- Start Accounts Module Array ------------------------------------------------//

$accounts_main_group=array(1=>"OWNERS EQUITY",
						   2=>"NON-CURRENT LIABILITIES",
						   3=>"CURRENT LIABILITIES",
						   4=>"NON-CURRENT ASSETS",
						   5=>"CURRENT ASSETS",
						   6=>"REVENUE",
						   7=>"COST OF GOOD SOLD",
						   8=>"OPERATING EXPENSES",
						   9=>"FINANCIAL EXPENSES",
						   10=>"NON-OPERATING INCOME & EXPENSE",
						   11=>"EXTRA ORDINARY ITEMS",
						   12=>"TAX EXPENSE");

$accounts_statement_type=array(1=>"Balance Sheet",
							   2=>"Income Statement");//Profit & Loss 

$accounts_account_type=array( 1=>"Credit",
							  2=>"Debit");

$accounts_cash_flow_group=array(1=>"Operating Activities",
								2=>"Investing Activities",
								3=>"Financing Activities",
								4=>"Cash & Cash Equivalents");
								
								/* OLD
								1=>"Financing Activities",
									2=>"Operating Activities",
									3=>"Investing Activities",
									4=>"Cash & Cash Equivalents",
									5=>"Operating Activities");
									*/

$accounts_journal_type=array(
						   1=>"Opening/Closing Journal",
						   2=>"Credit Purchase Journal",
						   3=>"Credit Sales Journal",
						   4=>"Cash withdrawn Journal",
						   5=>"Cash Deposit Journal",
						   6=>"Cash Receive Journal",
						   7=>"Cheque Deposit Journal",
						   8=>"Cash Payment Journal",
						   9=>"Export Realization Journal",
						   10=>"Bank Payment Journal",
						   11=>"Adjustment Journal",
						   12=>"Provisional Journal",
						   13=>"Reverse Journal",
						   14=>"Rectifying Journal",
						   15=>"General Journal");	
						   
$control_accounts = array(1=>"AP",
						  2=>"AR",
						  3=>"Import Payable",
						  4=>"Export Receivable",
						  5=>"Advance Paid",
						  6=>"Advance Received",
						  7=>"Export Negotiation Liability",
						  8=>"Other Trade Finance",
						  9=>"Tax at source from Suppliers' Bill",
						  10=>"Tax at source from Sales Bill",
						  11=>"VAT at source from Suppliers' Bill",
						  12=>"VAT at source from Sales Bill",
						  13=>"Security at source from Suppliers' Bill",
						  14=>"Security at source from Sales Bill",
						  15=>"Tax at source from Employees' Salary",
						  16=>"Discount Allowed",
						  17=>"Discount Received",
						  18=>"Write-off Assets",
						  19=>"Write-off Liability",
						  20=>"Other Subsidiary",
						  21=>"Advance Paid-Employee",
						  22=>"Advance Paid-Others",
						  23=>"Advance Received-Employee",
						  24=>"Advance Received-Others",
						  25=>"Goods in Transit",
						  26=>"ILE Control Account",
						  27=>"Yarn Dyeing Charge");		

  $account_nature = array(1=>"Cash",
						  2=>"Bank",
						  3=>"OD/CC",
						  4=>"Foreign Sales",
						  5=>"Local Sales",
						  6=>"Project Sales",
						  7=>"Purchase",
						  8=>"Project Cost",
						  9=>"Interest",
						  10=>"Bank Charges",
						  11=>"Currency Exchange Gain/Loss - Export",
						  12=>"Currency Exchange Gain/Loss - Import",
						  13=>"Project Common Cost",
						  14=>"Depreciation, Amortization & Depletion");	
						  
	$instrument_type = array(1=>"Bearer Cheque",
						  2=>"Crossed Cheque",
						  3=>"Pay Order",
						  4=>"TT",
						  5=>"DD",
						  6=>"Special Crossed Cheque",
						  7=>"Deposit Slip");					  
 
 	$ac_loan_type = array(
						1=>"PAD",
						2=>"LTR",
						3=>"LIM",
						10=>"Packing Credit",
						11=>"ECC",
						20=>"Term Loan",
						50=>"Project Loan");
 
  $ratio_category  = array(
						1=>"Liquidity",
						2=>"Activity",
						3=>"Leverage",
						4=>"Profitability",
						5=>"Market");
 
//------------------------------------------------------ End Accounts Module Array -------------------------------------------------------------//

//---------------all day---------------------------------------sohel -------------------------------------------------------------//


//--------------------------TNA_task----------------------------------------
 
 $general_task=array(1=>"Order Placement Date",2=>"Order Evaluation",3=>"Acceptance to be given",4=>"Internal communication to be done");
 $test_approval_task=array(1=>"Fabric test to be done",2=>"Garments test to be done");
 $purchase_task=array(1=>"Fabric booking to be issued",2=>"Trims booking to be issued",3=>"Fabric service work order to be issued",4=>"Sample Fabric booking to be issued");
 $material_receive_task=array(1=>"Gray fabric to be in-house",2=>"Finished fabric to be in-house",3=>"Sewing trims to be in-house",4=>"Finishing trims to be in-house");
 
 $fabric_production_task=array(1=>"Gray fabric production to be done",2=>"Dyeing production to be done",3=>"Finish fabric production to be done",4=>"Yarn Send for Dyeing",5=>"Dyed Yarn Receive",6=>"Fabric Send for AOP",7=>"AOP Receive");
 
 $garments_production_task=array(1=>"PP meeting to be conducted",2=>"Trail cut to be done",3=>"Trail production to be submitted",4=>"Trail production approval to be received",5=>"PCD to be end",6=>"Print/Emb TOD  to be end",7=>"Sewing  to be end",8=>"Garments finishing to be done");
 
 $inspection_task=array(1=>"Inspection schedule to be offered",2=>"Inspection to be done");
 $export_task=array(1=>"Ex-Factory to be done",2=>"Document to be submited",3=>"Proceeds to be realized");
 $lapdip_task_name=array(1=>"Submission",2=>"Target Approval");
 $embelishment_approval_task=$emblishment_name_array;//array(1=>"Local",2=>"Imported");

$category_wise_task_array=array(1=>"General Task",5=>"Sample Approval Task",6=>"Lapdip Task Name",7=>"Trims Approval Task",8=>"Embelishment Approval Task",9=>"Test Approval Task",15=>"Purchase Task",20=>"Material Receive Task",25=>"Fabric Production Task",26=>"Garments Production Task",30=>"Inspection Task",35=>"Export Task");

$material_source=array(1=>"Local",2=>"Imported");
$test_approval_task=array(1=>"Fabric Approval",2=>"Garments Approval");
$inspection_task=array(1=>"Inspection Offered",2=>"Inspection Completed");
$knit_fabric_production_task=array(1=>"Knitting",2=>"Dyeing & Finishing"); 
$woven_fabric_production_task=array(1=>"Weaving",2=>"Dyeing",3=>"Finishing"); 
$material_purchase_task=array(1=>"Fabric Booking",2=>"Trims Booking",3=>"Embellishment Booking");

$knitting_source =array(1=>"In-house",2=>"In-bound Subcontract",3=>"Out-bound Subcontract");

$time_source =array(1=>"AM",2=>"PM");
$order_source=array(1=>"Self Order",2=>"Subcontract Order");

$tna_task_name = array(						 				
						1	=> "Order Placement Date",			
						2	=> "Order Evaluation",			
						3	=> "Acceptance To Be Given",			
						4	=> "Internal Communication To Be Done",			
						5	=> "SC/LC Received",
						7	=> "Fit Sample Submit",
						8	=> "PP Sample Submit",
						9	=> "Labdip Submit", 			
						10	=> "Labdip Approval",			
						11	=> "Trims Approval",			
						12	=> "PP Sample Approval",
						13	=> "Fit Sample Approval",
						14	=> "Size Set Submission",
						15	=> "Size Set Approval",
						16	=> "Production Sample Submission",
						17	=> "Production Sample Approval",
						19	=> "Embellishment Submission",			
						20	=> "Embellishment Approval",
						21	=> "Tag Sample Submission",
						22	=> "Tag Sample Approval",
						23	=> "Photo Sample Submission",
						24	=> "Photo Sample Approval",
						25	=> "Trims Submission",
						26	=> "Packing  Sample Submission",
						27	=> "Packing  Sample Approval",
						28	=> "Final  Sample Submission",
						29	=> "Final  Sample Approval",
						30	=> "Sample Fabric Booking To Be Issued",			
						31	=> "Fabric Booking To Be Issued",			
						32	=> "Trims Booking To Be Issued",			
						33	=> "Fabric Service Work Order To Be Issued",
						34	=> "Woven Fabric Work Order To Be Issued",			
						40	=> "Fabric Test To Be Done",			
						41	=> "Garments Test To Be Done",
						45	=> "Yarn purchase requisition",
						46	=> "Yarn purchase order",
						47	=> "Yarn Receive",
						48	=> "Yarn Allocating",		
						50	=> "Yarn Issue To Be Done",			
						51	=> "Yarn Send for Dyeing",			
						52	=> "Dyed Yarn Receive",			
						60	=> "Gray Fabric Production To Be Done",			
						61	=> "Dyeing Production To Be Done",			
						62	=> "Fabric Send for AOP",			
						63	=> "AOP Receive",			
						64	=> "Finish Fabric Production To Be Done",			
						70	=> "Sewing Trims To Be In-house",			
						71	=> "Finishing Trims To Be In-house",			
						72	=> "Gray fabric to be in-house",			
						73	=> "Finished fabric to be in-house",	
						74	=> "Finish Fabric Issue to Cut",		
						80	=> "PP Meeting To Be Conducted",			
						81	=> "Trail cut to be done",			
						82	=> "Trail production to be submitted",			
						83	=> "Trail production approval to be received",			
						84	=> "Cutting To Be Done",			
						85	=> "Print/Emb To Be Done",			
						86	=> "Sewing To Be Done",			
						87	=> "Iron To Be Done",			
						88	=> "Garments Finishing To Be Done",	
						89	=> "Garments sent for Wash",	
						90	=> "Garments Receive from Wash",	
						100	=> "Inspection Schedule To Be Offered",			
						101	=> "Inspection To Be Done",			
						110	=> "Ex-Factory To Be Done",			
						120	=> "Document to be submited",			
						121	=> "Proceeds to be realized",
						122	=> "Sewing Input To Be Done");	

$tna_common_task_name_to_process = array(						 				
						1	=> "Order Placement Date",			
						2	=> "Order Evaluation",			
						3	=> "Acceptance To Be Given",			
						4	=> "Internal Communication To Be Done",			
						5	=> "SC/LC Received",
						28	=> "Final Sample Submissinon",	
						29	=> "Final Sample Approval",			
						30	=> "Sample Fabric Booking To Be Issued",			
						31	=> "Fabric Booking To Be Issued",			
						32	=> "Trims Booking To Be Issued",			
						33	=> "Fabric Service Work Order To Be Issued",
						34	=> "Woven Fabric Work Order To Be Issued",			
						40	=> "Fabric Test To Be Done",			
						41	=> "Garments Test To Be Done",
						45	=> "Yarn purchase requisition",
						46	=> "Yarn purchase order",
						47	=> "Yarn Receive",
						48	=> "Yarn Allocating",			
						50	=> "Yarn Issue To Be Done",	
						60	=> "Gray Fabric Production To Be Done",			
						61	=> "Dyeing Production To Be Done",	
						64	=> "Finish Fabric Production To Be Done",			
						70	=> "Sewing Trims To Be In-house",			
						71	=> "Finishing Trims To Be In-house",			
						72	=> "Gray fabric to be in-house",			
						73	=> "Finished fabric to be in-house",	
						74	=> "Finish Fabric Issue to Cut",			
						80	=> "PP Meeting To Be Conducted",			
						81	=> "Trail cut to be done",			
						82	=> "Trail production to be submitted",			
						83	=> "Trail production approval to be received",			
						84	=> "Cutting To Be Done",	
						86	=> "Sewing To Be Done",			
						87	=> "Iron To Be Done",			
						88	=> "Garments Finishing To Be Done", 
						100	=> "Inspection Schedule To Be Offered",			
						101	=> "Inspection To Be Done",			
						110	=> "Ex-Factory To Be Done",			
						120	=> "Document to be submited",			
						121	=> "Proceeds to be realized",
						122	=> "Sewing Input To Be Done");
						
//--------------------------- Start Inventory ------------- 04_03_2013  --------------------

//Yarn Receive Basis
$receive_basis_arr=array(1=>"PI Based",2=>"WO/Booking Based",3=>"In-Bound Subcontract",4=>"Independent",5=>"Batch Based",6=>"Opening Balance",7=>"Requisition",8=>"Recipe Based",9=>"Production",10=>"Delivery");
//Yarn Issue Entry
$yarn_issue_purpose=array(1=>"Knitting",2=>"Yarn Dyeing",3=>"Sales",4=>"Sample With Order",5=>"Loan",6=>"Sample-material", 7=>"Yarn Test", 8=>"Sample Without Order", 9=>"Sewing Production", 10=>"Fabric Test",11=>"Fabric Dyeing",12=>"Reconning",13=>"Machine Wash",14=>"Topping",15=>"Twisting",16=>"Grey Yarn",26=>"Damage",27=>"Pilferage",28=>"Expired",29=>"Stolen",30=>"Unknown",31=>"Scrap Store",32=>"ETP");
//Inventory Variable List. Created by sohel
$inventory_module=array(8=>"ILE/Landed Cost Standard",9=>"Hide Opening Stock Flag",10=>"Item Rate Optional",11=>"Item QC",16=>"User given item code",17=>"Book Keeping Method",18=>"Allocated Quantity",19=>"Receive Control On Gate Entry",20=>"Receive basis controll");
//Transaction Type
$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return",5=>"Item Transfer Receive",6=>"Item Transfer Issue");
$issue_basis=array(1=>"Booking",2=>"Independent",3=>"Requisition");
$store_method=array(1=>"FIFO",2=>"LIFO");
$general_issue_purpose=array(1=>"Damage",2=>"Pilferage",3=>"Stolen",4=>"Unknown");

$quot_evaluation_factor=array(1=>"Quoted Item",2=>"Specification",3=>"Performance",4=>"Brand",5=>"Country of Origin",6=>"Delivery Days",7=>"Pay Term",8=>"Warranty",9=>"Service Agreement",10=>"online Support",11=>"Local Support Center",12=>"Price");
$get_pass_basis=array(1=>"Independent",2=>"Challan(Yarn)",3=>"Challan(Gray Fabric)",4=>"Challan(Finish Fabric)",5=>"Challan(General Item)",6=>"Challan(Trims)",6=>"Challan(Dyes & Chemical)",7=>"Challan(Trims)",8=>"Challan Subcon(grey fabric)",9=>"Challan Subcon (finish fabric)",10=>"Grey Fabric Delivery to Store",11=>"Finish Fabric Delivery to Store");
//-------------------------- End Inventory -------------------------------------------------

//------------------------- Start Sub. Bill ------------------ 09_03_2013 ------------------
$rate_type=array(1=>"External",2=>"Internal");	
$is_deleted=array(0=>"No",1=>"Yes");
$production_process=array(1=>"Cutting",2=>"Knitting",3=>"Dyeing",4=>"Finishing",5=>"Sewing",6=>"Fabric Printing",7=>"Washing",8=>"Gmts Printing",9=>"Embroidery",10=>"Iron",11=>"Gmts Finishing");

$bill_for=array(1=>"Order",2=>"Sample with order",3=>"Sample without order");

$instrument_payment=array(1=>"Cash",2=>"Cheque",3=>"Pay Order",4=>"LC",5=>"Non-Cash");

$adjustment_type=array(1=>"Discount",2=>"Bad Debts",3=>"Write Off",5=>"Others",6=>"Advance Adjustment");	
$payment_type=array(1=>"Due & Advance Rec.",2=>"Advance",3=>"Due Adjustment");
$bill_rate=array(1=>"Rate Manually",2=>"Rate from Order");

//------------------------- End Sub. Bill --------------------------------------------------
$buyer_quotation_status=array(1=>"Submitted",2=>"Confirm",3=>"Cancle",4=>"Inactive"); 
$clearance_method=array(1=>"First Come First Adjust",2=>"Manual Adjustment"); 

//=================Planning

//$complexity_level=array(1=>"Basic",2=>"Simply Complex", 3=>"Highly Complex");
$complexity_level=array(0=>"",1=>"Basic",2=>"Fancy", 3=>"Critical", 4=>"Average");

$complexity_level_data[0]['fdout']=0;
$complexity_level_data[0]['increment']=0;
$complexity_level_data[0]['target']=0; ///complexity_levels		
$complexity_level_data[1]['fdout']=1000;
$complexity_level_data[1]['increment']=100;
$complexity_level_data[1]['target']=1200;
$complexity_level_data[2]['fdout']=800;
$complexity_level_data[2]['increment']=100;
$complexity_level_data[2]['target']=1200;
$complexity_level_data[3]['fdout']=600;
$complexity_level_data[3]['increment']=100;
$complexity_level_data[3]['target']=1200; ///complexity_levels		
$complexity_level_data[4]['fdout']=880;
$complexity_level_data[4]['increment']=100;
$complexity_level_data[4]['target']=1100; ///complexity_levels		

$complexity_type_tmp=array( 1=>"Regular complexity",2=>"Percentage Based");

//report
$report_format=array(1=>"Fabric Booking Gr",2=>"Print Booking F 1",3=>"Print Booking F 2",4=>"Print For Cut 1",5=>"Print For Cut 2",6=>"Fabric Booking F 1",7=>"Fabric Booking F 2",8=>"Print Booking",9=>"Print Booking 2",10=>"Fabric Booking",11=>"Fabric Booking",12=>"Print Booking 1",13=>"Print Booking",14=>"Print Booking 1",15=>"Print Booking 2",16=>"Print Booking 3",17=>"Print Booking",18=>"Print Booking 1",19=>"Print Booking 2",20=>"Print Booking",21=>"Print Booking",22=>"Print Booking",23=>"Summary",24=>"Budget",25=>"Budget Report2",26=>"Quote Vs Budget",27=>"Budget On Shipout",28=>"Fabric booking AKH");
$report_name=array(1=>"Main Fabric Booking",2=>"Short Fabric Booking",3=>"Sample Fabric Booking -With order",4=>"Sample Fabric Booking -Without order",5=>"Multiple Order Wise Trims Booking",6=>"Country and Order Wise Trims Booking",7=>"Yarn Dyeing Work Order",8=>"Yarn Dyeing Work Order Without Order",9=>"Embellishment Work Order",10=>"Service Booking For AOP",11=>"Fabric Service Booking",12=>"Service Booking For Knitting",13=>"Yarn Dyeing Work Order",14=>"Yarn Dyeing Work Order Without Order",15=>"Short Trims Booking",16=>"Sample Trims Booking With Order",17=>"Sample Trims Booking Without Order",18=>"Order Wise Budget Report");

//22.09.2015 
$info_type=array(1=>"Recv Date Wise",2=>"Order Wise");
$report_date_catagory=array(1=>"Country Ship Date Wise",2=>"Pub Ship Date Wise");

//06.10.2015
$letter_type_arr=array(1=>"Shipping Guarantee Forwording", 2=>"Delivery of Consignment", 3=>"Sales Contact Lien", 4=>"Export LC Lien", 5=>"Export LC Amendment", 6=>"Export LC Replace");

?>