﻿<?php
/*-------------------------------------------- Comments
Version                  :  V1
Purpose			         : 	This form will create Short Fabric Booking
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 	27-12-2012
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 		
Update date		         : 		   
QC Performed BY	         :		
QC Date			         :	
Comments		         :From this version oracle conversion is start
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Woven Fabric Booking", "../../", 1, 1,$unicode,1,'');
?>	
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
var permission='<?php echo $permission; ?>';

function openmypage_order(page_link,title)
{
	if(document.getElementById('id_approved_id').value==1)
	{
		alert("This booking is approved")
		return;
	}
	if (form_validation('cbo_booking_month*cbo_booking_year*cbo_fabric_natu*cbo_fabric_source','Booking Month*Booking Year*Fabric Nature*Fabric Source')==false)
	{
		return;
	}
	var txt_booking_no=document.getElementById('txt_booking_no').value;
	var check_is_booking_used_id=return_global_ajax_value(txt_booking_no, 'check_is_booking_used', '', 'requires/short_fabric_booking_controller');
	if(trim(check_is_booking_used_id) !="")
	{
		alert("This booking used in PI Table. So Adding or removing order is not allowed")
		return;
	}
	else
	{
		if(txt_booking_no=="")
		{
		page_link=page_link+get_submitted_data_string('cbo_company_name*cbo_buyer_name*cbo_booking_month*cbo_booking_year','../../');
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px,height=470px,center=1,resize=1,scrolling=0','../')
		}
		else
		{
			var r=confirm("Existing Item against these Order  Will be Deleted")
			if(r==true)
			{
			var delete_booking_item=return_global_ajax_value(txt_booking_no, 'delete_booking_item', '', 'requires/short_fabric_booking_controller');
			show_list_view(txt_booking_no,'show_fabric_booking','booking_list_view','requires/short_fabric_booking_controller','setFilterGrid(\'list_view\',-1)');
			page_link=page_link+get_submitted_data_string('cbo_company_name*cbo_buyer_name*cbo_booking_month*cbo_booking_year','../../');
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=470px,center=1,resize=1,scrolling=0','../')
			}
			else
			{
				return;
			}
		}
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var id=this.contentDoc.getElementById("po_number_id");
			var po=this.contentDoc.getElementById("po_number");
			if (id.value!="")
			{
				freeze_window(5);
				reset_form('orderdetailsentry_2','booking_list_view','','','')
				document.getElementById('txt_order_no_id').value=id.value;
				document.getElementById('txt_order_no').value=po.value;
				var cbo_fabric_natu =document.getElementById('cbo_fabric_natu').value
				var cbo_fabric_source=document.getElementById('cbo_fabric_source').value
				get_php_form_data( id.value, "populate_order_data_from_search_popup", "requires/short_fabric_booking_controller" );
				load_drop_down( 'requires/short_fabric_booking_controller', id.value, 'load_drop_down_po_number', 'order_drop_down_td' )
                load_drop_down( 'requires/short_fabric_booking_controller', id.value+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_fabric_description', 'fabricdescription_id_td' )
				load_drop_down( 'requires/short_fabric_booking_controller', id.value+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_fabric_color', 'fabriccolor_id_id_td' )
				load_drop_down( 'requires/short_fabric_booking_controller', id.value+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_gmts_color', 'garmentscolor_id_id_td' )
				
				load_drop_down( 'requires/short_fabric_booking_controller', id.value+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_item_size', 'itemsize_id_td' )
				load_drop_down( 'requires/short_fabric_booking_controller', id.value+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_gmts_size', 'garmentssize_id_td' )
				release_freezing();
				fnc_generate_booking()
			}
		}
	}
}

function set_process_loss(str)
{
	var prosess_loss=return_global_ajax_value(str, 'prosess_loss_set', '', 'requires/short_fabric_booking_controller');
	document.getElementById('txt_process_loss').value=trim(prosess_loss);
	calculate_requirement()
}

function openmypage_booking(page_link,title)
{
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var theemail=this.contentDoc.getElementById("selected_booking");
		if (theemail.value!="")
		{
			freeze_window(5);
			reset_form('fabricbooking_1','booking_list_view','');
			get_php_form_data( theemail.value, "populate_data_from_search_popup", "requires/short_fabric_booking_controller" );
			reset_form('orderdetailsentry_2','booking_list_view','','','')
			var txt_order_no_id=document.getElementById('txt_order_no_id').value
			var cbo_fabric_natu =document.getElementById('cbo_fabric_natu').value
			var cbo_fabric_source=document.getElementById('cbo_fabric_source').value
			load_drop_down( 'requires/short_fabric_booking_controller', txt_order_no_id, 'load_drop_down_po_number', 'order_drop_down_td' )
            load_drop_down( 'requires/short_fabric_booking_controller', txt_order_no_id+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_fabric_description', 'fabricdescription_id_td' )
		    load_drop_down( 'requires/short_fabric_booking_controller', txt_order_no_id+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_fabric_color', 'fabriccolor_id_id_td' )
			load_drop_down( 'requires/short_fabric_booking_controller', txt_order_no_id+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_gmts_color', 'garmentscolor_id_id_td' )
			load_drop_down( 'requires/short_fabric_booking_controller', txt_order_no_id+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_item_size', 'itemsize_id_td' )
			load_drop_down( 'requires/short_fabric_booking_controller', txt_order_no_id+'_'+cbo_fabric_natu+'_'+cbo_fabric_source, 'load_drop_down_gmts_size', 'garmentssize_id_td' )
			show_list_view(theemail.value,'show_fabric_booking','booking_list_view','requires/short_fabric_booking_controller','setFilterGrid(\'list_view\',-1)');
			set_button_status(1, permission, 'fnc_fabric_booking',1);
			release_freezing();
		}
	}
}

function calculate_requirement()
{
	var cbo_company_name= document.getElementById('cbo_company_name').value;
	var cbo_fabric_natu= document.getElementById('cbo_fabric_natu').value
	var process_loss_method_id=return_global_ajax_value(cbo_company_name+'_'+cbo_fabric_natu, 'process_loss_method_id', '', 'requires/short_fabric_booking_controller');
	var txt_finish_qnty=(document.getElementById('txt_finish_qnty').value)*1;
	var processloss=(document.getElementById('txt_process_loss').value)*1;
	    var WastageQty='';
		if(process_loss_method_id==1)
		{
			WastageQty=txt_finish_qnty+txt_finish_qnty*(processloss/100);
		}
		else if(process_loss_method_id==2)
		{
			var devided_val = 1-(processloss/100);
			var WastageQty=parseFloat(txt_finish_qnty/devided_val);
		}
		else
		{
			WastageQty=0;
		}
		WastageQty= number_format_common( WastageQty, 5, 0) ;	
		document.getElementById('txt_grey_qnty').value= WastageQty;
		document.getElementById('txt_amount').value=number_format_common((document.getElementById('txt_rate').value)*1*WastageQty,5,0)
}


	


function fnc_fabric_booking( operation )
{
	if(operation==2)
	{
		alert("Delete Restricted")
		return;
	}
	if(document.getElementById('id_approved_id').value==1)
	{
		alert("This booking is approved")
		return;
	}
	if (form_validation('txt_order_no_id*txt_booking_date','Order No*Booking Date')==false)
	{
		return;
	}	
	else
	{
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_order_no_id*cbo_company_name*cbo_buyer_name*txt_job_no*txt_booking_no*cbo_fabric_natu*cbo_fabric_source*cbo_currency*txt_exchange_rate*cbo_pay_mode*txt_booking_date*cbo_booking_month*cbo_supplier_name*txt_attention*txt_delivery_date*cbo_source*cbo_booking_year*cbo_ready_to_approved',"../../");
		freeze_window(operation);
		http.open("POST","requires/short_fabric_booking_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_booking_reponse;
	}
}
	 
function fnc_fabric_booking_reponse()
{
	
	if(http.readyState == 4) 
	{
		 var reponse=trim(http.responseText).split('**');
		 show_msg(trim(reponse[0]));
		 document.getElementById('txt_booking_no').value=reponse[1];
		 set_button_status(1, permission, 'fnc_fabric_booking',1);
		 release_freezing();
	}
}


function fnc_fabric_booking_dtls( operation )
{
	if(operation==2)
	{
		alert("Delete Restricted")
		return;
	}
	if(document.getElementById('id_approved_id').value==1)
	{
		alert("This booking is approved")
		return;
	}
	if (form_validation('txt_order_no_id*txt_booking_date*txt_booking_no','Order No*Booking Date*Booking No')==false)
	{
		return;
	}	
	var data="action=save_update_delete_dtls&operation="+operation+get_submitted_data_string('txt_booking_no*txt_job_no*cbo_order_id*cbo_fabricdescription_id*cbo_fabriccolor_id*cbo_garmentscolor_id*cbo_itemsize_id*cbo_garmentssize_id*txt_dia_width*txt_finish_qnty*txt_process_loss*txt_grey_qnty*txt_rate*txt_amount*txt_rmg_qty*cbo_responsible_dept*cbo_responsible_person*txt_reason*update_id_details',"../../");
	freeze_window(operation);
	http.open("POST","requires/short_fabric_booking_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_fabric_booking_dtls_reponse;
}
	 
function fnc_fabric_booking_dtls_reponse()
{
	if(http.readyState == 4) 
	{
		 var reponse=http.responseText.split('**');
		 show_msg(trim(reponse[0]));
		 reset_form('orderdetailsentry_2','booking_list_view','','','')
		 set_button_status(0, permission, 'fnc_fabric_booking_dtls',2);
		 show_list_view(reponse[1],'show_fabric_booking','booking_list_view','requires/short_fabric_booking_controller','setFilterGrid(\'list_view\',-1)');
		 release_freezing();
	}
}


function open_terms_condition_popup(page_link,title)
{
	var txt_booking_no=document.getElementById('txt_booking_no').value;
	if (txt_booking_no=="")
	{
		alert("Save The Booking First")
		return;
	}	
	else
	{
	    page_link=page_link+get_submitted_data_string('txt_booking_no','../../');
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=720px,height=470px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
		}
	}
}

function enable_disable(value)
{
	if(value==2){
		document.getElementById('txt_rate').disabled=false;
	}
	else
	{
		document.getElementById('txt_rate').disabled=true;
	}
}

function generate_fabric_report(type)
{
if (form_validation('txt_booking_no','Booking No')==false)
	{
		return;
	}
	else
	{
		$report_title=$( "div.form_caption" ).html();
		var data="action="+type+get_submitted_data_string('txt_booking_no*cbo_company_name*txt_order_no_id*cbo_fabric_natu*cbo_fabric_source*id_approved_id*txt_job_no',"../../")+'&report_title='+$report_title;
		//freeze_window(5);
		http.open("POST","requires/short_fabric_booking_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = generate_fabric_report_reponse;
	}	
}

function generate_fabric_report_reponse()
{
	if(http.readyState == 4) 
	{
		$('#data_panel').html( http.responseText );
		var w = window.open("Surprise", "_blank");
		var d = w.document.open();
		d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel').innerHTML+'</body</html>');
		d.close();
	}
}

</script>
 
</head>
 
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
     <?php echo load_freeze_divs ("../../",$permission);  ?>
            	<form name="fabricbooking_1"  autocomplete="off" id="fabricbooking_1">
            	<fieldset style="width:950px;">
                <legend>Master</legend>
               
            		<table  width="900" cellspacing="2" cellpadding="0" border="0">
                    
                     <tr>
                            
                                <td align="right"></td>   
    						<td> 
                            	
                           </td>
                              <td  width="130" height="" align="right" class="must_entry_caption"> Booking No </td>              <!-- 11-00030  -->
                                <td  width="170" >
                                	<input class="text_boxes" type="text" style="width:160px" onDblClick="openmypage_booking('requires/short_fabric_booking_controller.php?action=fabric_booking_popup','fabric Booking Search')" readonly placeholder="Double Click for Booking" name="txt_booking_no" id="txt_booking_no"/>
                                
                                 
                                </td>
                               <td align="right" width="130" >
                              <input type="hidden" id="id_approved_id">
                                </td>
                                <td>	
                                    
                                </td>
							
                        </tr>
                    <tr>
                            
                                <td align="right" class="must_entry_caption">Booking Month</td>   
    						<td> 
                            	<?php 
							  	echo create_drop_down( "cbo_booking_month", 90, $months,"", 1, "-- Select --", "", "",0 );		
							  ?>
                              <?php 
							  	echo create_drop_down( "cbo_booking_year", 70, $year,"", 1, "-- Select --", date('Y'), "",0 );		
							  ?>
                           </td>
                              <td align="right" class="must_entry_caption">Fabric Nature</td>
                        	<td>
                            	  <?php 
									echo create_drop_down( "cbo_fabric_natu", 172, $item_category,"", 1, "-- Select --", 1,$onchange_func, $is_disabled, "2,3");		
								  ?>	
                            </td>
                               <td align="right" width="130" class="must_entry_caption">
                               Fabric Source
                                </td>
                                <td>	
                                    <?php 
									echo create_drop_down( "cbo_fabric_source", 172, $fabric_source,"", 1, "-- Select --", "","enable_disable(this.value);", "", "");		
								  ?>
                                </td>
							
                        </tr>
                       <tr>
                       <td  width="130" align="right" class="must_entry_caption">Booking Date</td>
                                <td width="170">
                                    <input class="datepicker" type="text" style="width:160px" name="txt_booking_date" id="txt_booking_date"/>	
                                </td>
                            <td height="" align="right" class="must_entry_caption">Selected Order No</td>   
                            <td colspan="3">
                                 <input class="text_boxes" type="text" style="width:97%;" placeholder="Double click for Order"  onDblClick="openmypage_order('requires/short_fabric_booking_controller.php?action=order_search_popup','Order Search')"   name="txt_order_no" id="txt_order_no"/>
                                 <input class="text_boxes" type="hidden" style="width:772px;"  name="txt_order_no_id" id="txt_order_no_id"/>
                            </td>                                
                        </tr>
                      
                        
                        <tr>
                        <td align="right">Currency</td>
                              <td>
                              <?php 
							  	echo create_drop_down( "cbo_currency", 172, $currency,"", 1, "-- Select --", 2, "",0 );		
							  ?>	
                               
                              </td>
                        	<td align="right">Exchange Rate</td>
                              <td>
                             <input style="width:160px;" type="text" class="text_boxes"  name="txt_exchange_rate" id="txt_exchange_rate"  />  
                              </td>
                       
                        	<td  align="right">Supplier Name</td>
                                <td>
                               <?php
							   		echo create_drop_down( "cbo_supplier_name", 172, "select id,supplier_name from lib_supplier where status_active =1 and is_deleted=0 order by supplier_name","id,supplier_name", 1, "-- Select Supplier --", $selected, "",0 );
							   ?> 
                                 </td> 
                              </tr>
                            
                             
                        
                        
                 
                        <tr>
                            <td  width="130" align="right">Delivery Date</td>
                                <td width="170">
                                    <input class="datepicker" type="text" style="width:160px" name="txt_delivery_date" id="txt_delivery_date"/>	
                                </td>
                                 <td  align="right">Pay Mode</td>
                                <td>
                               <?php
							   		echo create_drop_down( "cbo_pay_mode", 172, $pay_mode,"", 1, "-- Select Pay Mode --", 3, "","" );
							   ?> 
                                 </td>
                             <td  width="130" height="" align="right"> Source </td>              <!-- 11-00030  -->
                                <td  width="170" >
                                	<?php
							   		echo create_drop_down( "cbo_source", 172, $source,"", 1, "-- Select Source --", "", "","" );
							   ?>
                                
                                 
                                </td>
                                
                                                             
                        </tr>
                         <tr>
                            
                    <td  align="right">Company Name</td>
                              <td>
                              <?php 
							  	echo create_drop_down( "cbo_company_name", 172, "select id,company_name from lib_company where status_active=1 and is_deleted=0 order by company_name", "id,company_name",1, "-- Select Company --", $selected, "load_drop_down( 'requires/woven_order_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td' )",1,"" );
								?>	  
                              </td>
                         <td align="right" >Buyer Name</td>   
   						 <td id="buyer_td"> 
                             <?php  
							  	echo create_drop_down( "cbo_buyer_name", 172, "select id,buyer_name from lib_buyer where status_active =1 and is_deleted=0 order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",1,"" );
								?>
                         </td>
                               	<td align="right">Job No.</td>
                        	<td>
                            	  <input style="width:160px;" type="text" class="text_boxes"  name="txt_job_no" id="txt_job_no" disabled  /> 
                            </td>
                              
                        </tr>
                        <tr>
                        	<td align="right">Attention</td>   
                        	<td align="left" height="10" colspan="3">
                            	<input class="text_boxes" type="text" style="width:97%;"  name="txt_attention" id="txt_attention" />
                            	<input type="hidden" class="image_uploader" style="width:162px" value="Lab DIP No" onClick="openmypage('requires/short_fabric_booking_controller.php?action=lapdip_no_popup','Lapdip No','lapdip')">
                            <td align="right">Ready To Approved</td>  
                        	<td align="center" height="10">
                              <?php
							   		echo create_drop_down( "cbo_ready_to_approved", 172, $yes_no,"", 1, "-- Select--", 2, "","","" );
							   ?>
                            </td>
                            </td>
                             
                        </tr>
                        
                        <tr>
                        	<td align="center" height="10" colspan="6"><input type="button" id="set_button" class="image_uploader" style="width:160px;" value="Terms & Condition/Notes" onClick="open_terms_condition_popup('requires/short_fabric_booking_controller.php?action=terms_condition_popup','Terms Condition')" /></td>
                        </tr>
                        
                        
                        <tr>
                        	<td align="center" colspan="6" valign="top" id="app_sms2" style="font-size:18px; color:#F00">
                            	
                            </td>
                        </tr>
                        <tr>
                        	<td align="center" colspan="6" valign="middle" class="button_container">
                              <?php echo load_submit_buttons( $permission, "fnc_fabric_booking", 0,0 ,"reset_form('fabricbooking_1','','booking_list_view','cbo_pay_mode,3*cbo_booking_year,2014*cbo_currency,2*cbo_ready_to_approved,2')",1) ; ?>
                            </td>
                        </tr>
                        <tr>
                        	<td align="center" colspan="6" height="10">
                           
                           </td>
                        </tr>
                    </table>
                 
              </fieldset>
              </form>
              
              <form name="orderdetailsentry_2"  autocomplete="off" id="orderdetailsentry_2">
            	<fieldset style="width:950px;">
                <legend>Details</legend>
               
            		<table  width="900" cellspacing="2" cellpadding="0" border="0">
                    <tr>
                            
                            <td align="right" class="must_entry_caption">PO No</td>   
    						<td id="order_drop_down_td"> 
                            	<?php 
								 echo create_drop_down( "cbo_order_id", 172, $blank_array,"", 1, "--Select--", $selected, "" ); 
								 ?>
                           </td>
                              <td align="right" class="must_entry_caption" >Fabric Description</td>
                        	<td colspan="3" id="fabricdescription_id_td">
                            	   <?php  echo create_drop_down( "cbo_fabricdescription_id", 480, $blank_array,"", 1, "--Select--", $selected, "" ); ?>
                            </td>
                               
							
                        </tr>
                       <tr>
                       <td  width="130" align="right">Garments Color </td>
                                <td id="garmentscolor_id_id_td" >
                                <?php  echo create_drop_down( "cbo_garmentscolor_id", 172, $blank_array,"", 1, "--Select--", $selected, "" ); ?>
                       </td>
                       <td align="right" width="130" >
                               Fabric Color
                                </td>
                                <td id="fabriccolor_id_id_td">	
                                   <?php  echo create_drop_down( "cbo_fabriccolor_id", 172, $blank_array,"", 1, "--Select--", $selected, "" ); ?>
                                </td>
                                <td height="" align="right" width="130">Garments size</td>   
                                <td id="garmentssize_id_td">
                                <?php  echo create_drop_down( "cbo_garmentssize_id", 172, $blank_array,"", 1, "--Select--", $selected, "" ); ?>
                                </td>
                       
                              
                            
                                                        
                        </tr>
                      
                        
                        <tr>
                        <td  width="130" align="right">Item size</td>
                       <td id="itemsize_id_td">
                                
                              <?php  echo create_drop_down( "cbo_itemsize_id", 172, $blank_array,"", 1, "--Select--", $selected, "" ); ?>
                              </td>
                        <td align="right">Dia/ Width</td>
                              <td>
                             <input name="txt_dia_width" id="txt_dia_width" class="text_boxes" type="text" value=""  style="width:160px "/>
                               
                        </td>
                        <td align="right">Finish Fabric</td>
                              <td>
                             <input name="txt_finish_qnty" id="txt_finish_qnty" class="text_boxes_numeric" type="text" onChange="calculate_requirement()" value=""  style="width:160px "/>
                               
                              </td>  
                        
                        	
                       
                        	</tr>
                            
                             
                        
                        
                 
                        <tr>
                        <td align="right">Process loss </td>
                              <td>
                            <input name="txt_process_loss" id="txt_process_loss" class="text_boxes_numeric" type="text" value="" onChange="calculate_requirement()"   style="width:160px "/>
                               
                              </td>
                        <td align="right">Gray Fabric</td>
                              <td>
                             <input name="txt_grey_qnty" id="txt_grey_qnty" class="text_boxes_numeric" type="text" value=""  style="width:160px " readonly/>  
                              </td>
                        <td  align="right">Rate</td>
                                <td>
                              <input name="txt_rate" id="txt_rate" class="text_boxes_numeric" type="text" value="" onChange="calculate_requirement()"  style="width:160px " />
                                 </td> 
                              
                            
                                
                             
                                
                                                             
                        </tr>
                        <tr>
                        
                        <td  width="130" align="right">Amount</td>
                                <td width="170">
                                   <input name="txt_amount" id="txt_amount" class="text_boxes_numeric" type="text" value=""  style="width:160px " readonly/>
                                    <input type="hidden" id="update_id_details">
                                </td>
                                
                                <td  width="130" align="right">RMG Qty</td>
                                <td width="170">
                                   <input name="txt_rmg_qty" id="txt_rmg_qty" class="text_boxes_numeric" type="text" value=""  style="width:160px " />
                                    
                                </td>
                                
                                <td  width="130" align="right">Responsible Dept.</td>
                                <td width="170">
                                   <?php 
									echo create_drop_down( "cbo_responsible_dept", 172,"select id,department_name from  lib_department where status_active=1 and is_deleted=0 order by  department_name", "id,department_name", 0, "", '', '', $onchange_func_param_db,$onchange_func_param_sttc  ); 
								   ?>	
                                    
                                </td>
                                
                             
                                
                                                             
                        </tr>
                        
                        <tr>
                        
                       
                                
                                <td  width="130" align="right">Responsible person</td>
                                <td width="170">
                                   
                                   <input name="cbo_responsible_person" id="cbo_responsible_person" class="text_boxes" type="text" value=""  style="width:170px "/>
                                    
                                </td>
                                <td  width="130" align="right">Reason</td>
                                <td width="470" colspan="3">
                                   <input name="txt_reason" id="txt_reason" class="text_boxes" type="text" value=""  style="width:470px "/>
                                </td>
                                
                             
                                
                                                             
                        </tr>
                         
                        <tr>
                        	<td align="center" colspan="6" valign="middle" class="button_container">
                              <?php
									
									echo load_submit_buttons( $permission, "fnc_fabric_booking_dtls", 0,0 ,"reset_form('orderdetailsentry_2','','','','')",2) ; 
									?>
                                    <input type="button" value="Print Booking" onClick="generate_fabric_report('show_fabric_booking_report')"  style="width:100px" name="print" id="print" class="formbutton" />
                                    <input type="button" value="Print Booking2" onClick="generate_fabric_report('show_fabric_booking_report3')"  style="width:100px" name="print_booking3" id="print_booking3" class="formbutton" /> 
                            </td>
                        </tr>
                       
                        
                    </table>
                 
              </fieldset>
              </form>
              
              <fieldset style="width:1280px;">
                <legend>List View</legend>
                    <table style="border:none" cellpadding="0" cellspacing="2" border="0">
                            <tr align="center">
                                <td colspan="12" id="booking_list_view">
                                </td>	
                        	</tr>
                       </table>
                </fieldset>
              
              
                
	</div>
   <div style="display:none" id="data_panel"></div>
   

</body>
<script>
	set_multiselect('cbo_responsible_dept','0','0','','0');
</script>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>