<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Knit Garments Order Entry
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	07-05-2013
Updated by 		: 	Kausar	(Creating report)
Update date		: 	09-12-2013	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Receive Return Info","../", 1, 1, $unicode,1,1); 
$con = connect();
/*$updateID_array = $update_data = array();// avg_rate_per_unit <=0 and
$update_array	= "avg_rate_per_unit"; 
$dataArray=sql_select("select id from product_details_master where item_category_id=1");
foreach($dataArray as $row)
{
	$recvData=sql_select("select sum(cons_amount) as amnt, sum(cons_quantity) as qnty from inv_transaction where prod_id='$row[id]' and transaction_type=1");
	$avg_rate=$recvData[0][csf('amnt')]/$recvData[0][csf('qnty')];
	$updateID_array[]=$row[csf('id')];
	$update_data[$row[csf('id')]]=explode("*",($avg_rate));
}
$query=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array));//execute_query

$updateID_array = $update_data = array();// avg_rate_per_unit <=0 and
$update_array	= "current_stock*stock_value";
$prod_id='';
$sql="Select a.prod_id, b.current_stock, b.avg_rate_per_unit,
		sum(case when a.transaction_type in(1,4,5) then a.cons_quantity else 0 end) as rcv_qty,
		sum(case when a.transaction_type in(2,3,6) then a.cons_quantity else 0 end) as iss_qty
		from inv_transaction a, product_details_master b
		where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.order_id=0 and a.item_category=1 and b.item_category_id=1 and b.id in(331,381,542,543,558,571,596,620,685,686,736,740,744,757,758,759,760,
761,762,782,785,799,803,804,805,807,808,809,810,812,814,815,816,824,
828,829,830,845,852,853,863,889) group by a.prod_id order by b.id";//
$result=sql_select($sql);
foreach($result as $row)
{
	$current_qty=$row['rcv_qty']*1-$row['iss_qty']*1;
	$stock_qty=$row[csf('current_stock')];
	$stock_value=$current_qty*$row[csf('avg_rate_per_unit')];
	
	$updateID_array[]=$row[csf('prod_id')];
	$update_data[$row[csf('prod_id')]]=explode("*",($current_qty."*".$stock_value));
}
//echo substr($prod_id,0,-1);
echo bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array);
//$query=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array));	
//if($query) echo "Success"; else "Invalid";

disconnect($con);
die;*/
?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";


function open_mrrpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/yarn_receive_return_controller.php?action=mrr_popup&company='+company; 
	var title="Search MRR Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var mrrNumber=this.contentDoc.getElementById("hidden_recv_number").value; // mrr number
  		// master part call here
		$("#pi_id").val('');
		$("#txt_pi_no").val('');
		
		get_php_form_data(mrrNumber, "populate_data_from_data", "requires/yarn_receive_return_controller");  
 		$("#tbl_child").find('input,select').val('');
 	}
}
 
function fn_calculateAmount(qnty)
{
	var rate = $("#txt_rate").val();
	var rcvQnty = $("#txt_receive_qnty").val();
	
	if(qnty=="" || rate=="" || rcvQnty*1<qnty)
	{
		$('#txt_return_qnty').val(0);
		$('#txt_return_value').val(0);
		return;
	}
	else
	{		
		var amount = rate*qnty;
		$('#txt_return_value').val(number_format_common(amount,"","",1));
	}
}


function fnc_yarn_receive_return_entry(operation)
{
	if(operation==4)
	{
		 var report_title=$( "div.form_caption" ).html();
		 print_report( $('#cbo_company_name').val()+'*'+$('#txt_return_no').val()+'*'+report_title, "yarn_receive_return_print", "requires/yarn_receive_return_controller" ) 
		 return;
	}
	else if(operation==2)
	{
		show_msg('13');
		return;
	}
	else
	{
		if( form_validation('cbo_company_name*cbo_return_to*txt_return_date*txt_mrr_no*txt_item_description*txt_return_qnty*txt_rate','Company Name*Return To*Return Date*MRR Number*Item Description*Return Quantity*Rate')==false )
		{
			return;
		}	
		if($("#txt_return_qnty").val()*1>$("#txt_receive_qnty").val()*1)
		{
			alert("Return Quantity Can not be Greater Than Receive Quantity.");
			return;
		}
		var dataString = "txt_return_no*cbo_company_name*cbo_return_to*txt_return_date*txt_received_id*txt_mrr_no*txt_item_description*txt_prod_id*cbo_store_name*txt_return_qnty*txt_receive_qnty*cbo_uom*txt_rate*txt_return_value*before_prod_id*update_id*order_rate*order_ile_cost*pi_id";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
		freeze_window(operation);
		http.open("POST","requires/yarn_receive_return_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_yarn_receive_return_entry_reponse;
	}
}

function fnc_yarn_receive_return_entry_reponse()
{	
	if(http.readyState == 4) 
	{	  	
		//release_freezing(); return;	
		var reponse=trim(http.responseText).split('**');		
		if(reponse[0]==20)
		{
			alert(reponse[1]);
			release_freezing();
			return;
		} 
		
		show_msg(reponse[0]);
		 		
		if(reponse[0]==0 || reponse[0]==1)
		{
			$("#txt_return_no").val(reponse[1]);
 			//$("#tbl_master :input").attr("disabled", true);
			disable_enable_fields( 'txt_return_no', 0, "", "" ); // disable false
			show_list_view(reponse[1],'show_dtls_list_view','list_container_yarn','requires/yarn_receive_return_controller','');		
			//child form reset here after save data-------------//
			set_button_status(0, permission, 'fnc_yarn_receive_return_entry',1,1);
			$("#tbl_child").find('input,select').val('');
		}
		release_freezing();
	}
}

function open_returnpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/yarn_receive_return_controller.php?action=return_number_popup&company='+company; 
	var title="Search Return Number Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var returnNumber=this.contentDoc.getElementById("hidden_return_number").value; // mrr number
  		// master part call here
		get_php_form_data(returnNumber, "populate_master_from_data", "requires/yarn_receive_return_controller");  		
		//list view call here
		show_list_view(returnNumber,'show_dtls_list_view','list_container_yarn','requires/yarn_receive_return_controller','');
		set_button_status(0, permission, 'fnc_yarn_receive_return_entry',1,1);
		//$("#tbl_master").find('input,select').attr("disabled", true);	
		$("#tbl_child").find('input,select').val('');
		disable_enable_fields( 'txt_return_no', 0, "", "" ); // disable false
 	}
}

//form reset/refresh function here
function fnResetForm()
{
	$("#tbl_master").find('input,select').attr("disabled", false);	
	set_button_status(0, permission, 'fnc_yarn_receive_entry',1);
	reset_form('yarn_receive_return_1','list_container_yarn*list_product_container','','','','');
}

// popup for PI----------------------	
function openmypage_pi()
{
	if( form_validation('cbo_company_name*txt_mrr_no','Company Name*MRR No')==false )
	{
		return;
	}
	
	var company = $("#cbo_company_name").val();
	 
	page_link='requires/yarn_receive_return_controller.php?action=pi_popup&company='+company;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'PI Search', 'width=850px, height=370px, center=1, resize=0, scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var piID=this.contentDoc.getElementById("hidden_tbl_id").value; // pi table id
		var piNumber=this.contentDoc.getElementById("hidden_pi_number").value; // pi number
		
		$("#pi_id").val(piID);
		$("#txt_pi_no").val(piNumber);
	}		
}


</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../",$permission);  ?><br />    		 
    <form name="yarn_receive_return_1" id="yarn_receive_return_1" autocomplete="off" > 
    <div style="width:80%;">       
    <table width="80%" cellpadding="0" cellspacing="2" align="left">
     	<tr>
        	<td width="80%" align="center" valign="top">   
            	<fieldset style="width:1000px; float:left;">
                <legend>Yarn Receive Return</legend>
                <br />
                 	<fieldset style="width:900px;">                                       
                        <table  width="800" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                            <tr>
                           		<td colspan="3" align="right"><b>Return Number</b></td>
                           		<td colspan="3" align="left"><input type="text" name="txt_return_no" id="txt_return_no" class="text_boxes" style="width:160px" placeholder="Double Click To Search" onDblClick="open_returnpopup()" readonly /></td>
                   		   </tr>
                           <tr>
                           		<td colspan="6" align="center">&nbsp;</td>
                            </tr>
                           <tr>
                                <td  width="120" align="right" class="must_entry_caption">Company Name </td>
                                <td width="170">
									<?php 
                                     	echo create_drop_down( "cbo_company_name", 170, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "" );
                                    ?>
                                </td>
                                <td width="120" align="right" class="must_entry_caption">MRR Number<input type="hidden" name="txt_received_id" id="txt_received_id" readonly disabled /></td>
                                <td width="160" id="supplier_td"><input class="text_boxes" type="text" name="txt_mrr_no" id="txt_mrr_no" style="width:160px;" placeholder="Double Click To Search" onDblClick="open_mrrpopup()" readonly  /></td>
                                <td width="120" align="right" class="must_entry_caption">Return Date</td>
                                <td width="170"><input type="text" name="txt_return_date" id="txt_return_date" class="datepicker" style="width:160px;" placeholder="Select Date" /></td>
                          </tr>
                            <tr>
                                <td width="130" align="right" class="must_entry_caption">Returned To</td>
                                <td width="170">
									<?php 
										echo create_drop_down( "cbo_return_to", 170, "select id,supplier_name from lib_supplier order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",1 ); 
									?>
                               	</td>
                                <td align="right">PI NO </td>
                                <td>
                                	<input class="text_boxes" type="text" name="txt_pi_no" id="txt_pi_no" onDblClick="openmypage_pi()" placeholder="Double Click To Search" style="width:160px;" readonly />
                                    <input class="text_boxes" type="hidden" name="pi_id" id="pi_id" disabled/>
                                </td>
                                <td align="right">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </fieldset>
                    <br />
                    <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                    <tr>
                    <td width="49%" valign="top">
                    	<fieldset style="width:950px;">  
                        <legend>Return Item</legend>                                     
                            <table  width="950" cellspacing="0" cellpadding="0" border="1" class="rpt_table" rules="all">
                                <thead>
                                	<tr>
                                         <th width="250" class="must_entry_caption">Item Description</th>
                                         <th width="100">Store</th>
                                         <th width="100" class="must_entry_caption">Returned Qnty</th>
                                         <th width="100">Inv. Recv. Qnty</th>
                                         <th width="80">UOM</th>   
                                         <th width="80" class="must_entry_caption">Rate </th>
                                         <th width="100">Return Value</th>
                                  	</tr>
                                </thead>  
                                <tr align="center">
                                    <td>
                                   	  	<input type="text" name="txt_item_description" id="txt_item_description" class="text_boxes" style="width:250px" placeholder="Display" readonly />
                                    	<input type="hidden" name="txt_prod_id" id="txt_prod_id" readonly disabled />
                                    </td>
                                    <td>
										<?php 
                                       	 	echo create_drop_down( "cbo_store_name", 100, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 order by store_name","id,store_name", 1, "Display", "", "",1 );
                                        ?>
                                        
                                    </td>
                                    <td>
                                   	  <input type="text" name="txt_return_qnty" id="txt_return_qnty" class="text_boxes_numeric" style="width:100px;" placeholder="Entry" onKeyUp="fn_calculateAmount(this.value)" />
                                    </td>
                                    <td>
                                   	  <input type="text" name="txt_receive_qnty" id="txt_receive_qnty" class="text_boxes_numeric" style="width:100px" placeholder="Display" readonly />
                                    </td>                                    
                                    <td>
                                    	<?php
                                    		echo create_drop_down( "cbo_uom", 100, $unit_of_measurement,"", 1, "--Select--", 12, "",1 );
										?>
                                    </td>
                                    <td>
                                   	  <input type="text" name="txt_rate" id="txt_rate" class="text_boxes_numeric" style="width:80px" placeholder="Display" readonly />
                                    </td>
                                    <td>
                                   	 <input type="text" name="txt_return_value" id="txt_return_value" class="text_boxes_numeric" style="width:100px" placeholder="Display" readonly/>
                                    </td> 
                                </tr>
                                <input type="hidden" name="order_rate" id="order_rate" class="text_boxes_numeric" style="width:80px" placeholder="Display" readonly />
                                <input type="hidden" name="order_ile_cost" id="order_ile_cost" class="text_boxes_numeric" style="width:80px" placeholder="Display" readonly />
                            </table>
                    </fieldset>
                    </td>
                    </tr>
                </table>                
               	<table cellpadding="0" cellspacing="1" width="100%">
                	<tr> 
                       <td colspan="6" align="center"></td>				
                	</tr>
                	<tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <!-- details table id for update -->
                             <input type="hidden" id="before_prod_id" name="before_prod_id" value="" />
                             <input type="hidden" id="update_id" name="update_id" value="" />
                              <!-- -->
							 <?php echo load_submit_buttons( $permission, "fnc_yarn_receive_return_entry", 0,1,"fnResetForm()",1);?>
                        </td>
                   </tr> 
                </table>                 
              	</fieldset>
              	<fieldset>
    			<div style="width:990px;" id="list_container_yarn"></div>
    		  	</fieldset>
           </td>
         </tr>
    </table>
    </div>
    <div id="list_product_container" style="max-height:500px; width:20%; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>  
	</form>
</div>    
</body>  
<script src="../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
