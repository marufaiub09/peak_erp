﻿<?php
/*-------------------------------------------- Comments
Version                  :   V2
Purpose			         : 	This form will create  Shipment Schedule for Management Report
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 		Maruf	
Update date		         : 		08-12-2015
QC Performed BY	         :		
QC Date			         :	
Comments		         : From this version oracle conversion is start
*/
error_reporting('0');
include('../../../../includes/common.php');
session_start();
extract($_REQUEST);
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$date=date('Y-m-d');

$buyer_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$company_short_name_arr=return_library_array( "select id,company_short_name from lib_company",'id','company_short_name');
$company_team_name_arr=return_library_array( "select id,team_name from lib_marketing_team",'id','team_name');
$company_team_member_name_arr=return_library_array( "select id,team_member_name from  lib_mkt_team_member_info",'id','team_member_name');
$imge_arr=return_library_array( "select master_tble_id,image_location from   common_photo_library",'master_tble_id','image_location');
$cm_for_shipment_schedule_arr=return_library_array( "select job_no,cm_for_sipment_sche from  wo_pre_cost_dtls",'job_no','cm_for_sipment_sche');

$ex_factory_qty_arr=return_library_array( "select po_break_down_id,sum(ex_factory_qnty) as ex_factory_qnty from  pro_ex_factory_mst where  status_active=1 and is_deleted=0 group by po_break_down_id",'po_break_down_id','ex_factory_qnty');


if ($action=="load_drop_down_buyer")
{
echo create_drop_down( "cbo_buyer_name", 172, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );   	 
} 

if ($action=="load_drop_down_team_member")
{
echo create_drop_down( "cbo_team_member", 172, "select id,team_member_name 	 from lib_mkt_team_member_info  where team_id='$data' and status_active=1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-Select Team Member-", $selected, "" );   	 
}


if($type=="report_generate")
{
	$data=explode("_",$data);
	if($data[0]==0) $company_name="%%"; else $company_name=$data[0];
	//if($data[1]==0) $buyer_name="%%"; else $buyer_name=$data[1];
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";//.str_replace("'","",$cbo_buyer_name)
	}
	if(trim($data[2])!="") $start_date=$data[2];
	if(trim($data[3])!="") $end_date=$data[3];
	$cbo_order_status2=$data[4];
	if($data[4]==2) $cbo_order_status="%%"; else $cbo_order_status= "$data[4]";
	if(trim($data[5])=="0") $team_leader="%%"; else $team_leader="$data[5]";
	if(trim($data[6])=="0") $dealing_marchant="%%"; else $dealing_marchant="$data[6]";
	if(trim($data[8])!="") $pocond="and b.id in(".str_replace("'",'',$data[8]).")"; else  $pocond="";
	if($db_type==0)
	{
	$start_date=change_date_format($start_date,'yyyy-mm-dd','-');
	$end_date=change_date_format($end_date,'yyyy-mm-dd','-');
	}
	if($db_type==2)
	{
	$start_date=change_date_format($start_date,'yyyy-mm-dd','-',1);
	$end_date=change_date_format($end_date,'yyyy-mm-dd','-',1);
	}
	
	$cbo_category_by=$data[7]; $caption_date='';
	
	if($cbo_category_by==1)
	{
		if ($start_date!="" && $end_date!="")
		{
			$date_cond="and b.pub_shipment_date between '$start_date' and  '$end_date'";
		}
		else	
		{
			$date_cond="";
		}
	}
	else if($cbo_category_by==2)
	{
		if ($start_date!="" && $end_date!="")
		{
			$date_cond=" and b.po_received_date between '$start_date' and  '$end_date'";
		}
		else	
		{
			$date_cond="";
		}
	}
	else 
	{
		if ($start_date!="" && $end_date!="")
		{
			$date_cond=" and b.shipment_date between '$start_date' and  '$end_date'";
		}
		else	
		{
			$date_cond="";
		}
	}
	?>
	<div align="center">
        <div align="center">
            <table>
                <tr valign="top">
                    <td valign="top">
                    <h3 align="left" id="accordion_h2" class="accordion_h" onClick="accordion_menu( this.id,'content_summary1_panel', '')"> -Summary Panel</h3>
                        <div id="content_summary1_panel"> 
                            <fieldset>
                                <table width="750" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all">
                                    <thead>
                                    <th width="50">SL</th>
                                    <th width="130">Company Name</th><th width="200">Buyer Name</th>
                                    <th width="130">Quantity</th><th width="100">Value</th><th width="50">Value %</th>
                                    <th width="130"><strong>Full Shipped</strong></th><th width="130"><strong>Partial Shipped</strong></th> 
                                    <th width="130"><strong>Running</strong></th><th><strong>Ex-factory Percentage</strong></th>  
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    $total_po=0;
                                    $total_price=0;
                                    
                                    $po_qnty_array= array();
                                    $po_value_array= array();
                                    $po_full_shiped_array= array();
                                    $po_full_shiped_value_array= array();
                                    $po_partial_shiped_array= array();
                                    $po_partial_shiped_value_array= array();
                                    $po_running_array= array();
                                    $po_running_value_array= array();
									
                                    $data_array=sql_select("select a.company_name,a.buyer_name,sum(b.po_quantity*a.total_set_qnty) as po_quantity, sum(b.po_total_price) as po_total_price   from wo_po_details_master a, wo_po_break_down b   where a.job_no=b.job_no_mst   and a.company_name like '$company_name' $buyer_id_cond and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.company_name,a.buyer_name");//b.unit_price
                                    foreach ($data_array as $row)
                                    { 
									if ($i%2==0)  
									$bgcolor="#E9F3FF";
									else
									$bgcolor="#FFFFFF";	
                                    //$data_array_po=sql_select("select a.company_name, b.id, sum(b.po_quantity*a.total_set_qnty) as po_quantity , b.shiping_status  from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst   and a.company_name =$row[company_name] and a.buyer_name =$row[buyer_name] and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and b.status_active=1 ");
									$data_array_po=sql_select("select a.company_name, b.id, (b.po_quantity*a.total_set_qnty) as po_quantity , b.shiping_status  from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst   and a.company_name =".$row[csf('company_name')]." and a.buyer_name =".$row[csf('buyer_name')]." and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
                                    $full_shiped=0;$partial_shiped=0;
                                    foreach ($data_array_po as $row_po)
                                    {
                                   // $ex_factory_qnty=return_field_value( 'sum(ex_factory_qnty)','pro_ex_factory_mst', 'po_break_down_id="'.$row_po[id].'" and status_active=1 and is_deleted=0' );
								  $ex_factory_qnty=$ex_factory_qty_arr[$row_po[csf("id")]];
                                    if($row_po[csf('shiping_status')]==3)
                                    {
                                    $full_shiped+=$ex_factory_qnty;
                                    }
                                    if($row_po[csf('shiping_status')]==2)
                                    {
                                    $partial_shiped+=$ex_factory_qnty;
                                    }
                                    }
                                    ?>
                                    <tr bgcolor="<?php echo $bgcolor;?>">
                                    <td width="50"><?php echo $i;?></td>
                                    <td width="130"><?php echo $company_short_name_arr[$row[csf('company_name')]]; ?></td>
                                    <td width="200"><?php echo $buyer_short_name_arr[$row[csf('buyer_name')]];?></td>
                                    <td width="130" align="right">
                                    <?php 
                                    echo number_format($row[csf('po_quantity')],0); $total_po +=$row[csf('po_quantity')]; 
                                    if (array_key_exists($row[csf('company_name')], $po_qnty_array)) 
                                    {
                                    $po_qnty_array[$row[csf('company_name')]]+=$row[csf('po_quantity')];
                                    }
                                    else
                                    {
                                    $po_qnty_array[$row[csf('company_name')]]=$row[csf('po_quantity')];	
                                    }
                                    ?>
                                    </td>
                                    <td width="100" align="right">
                                    <?php 
                                    echo number_format($row[csf('po_total_price')],2); $total_price+= $row[csf('po_total_price')];
                                    if (array_key_exists($row[csf('company_name')], $po_value_array)) 
                                    {
                                    $po_value_array[$row[csf('company_name')]]+=$row[csf('po_total_price')];
                                    }
                                    else
                                    {
                                    $po_value_array[$row[csf('company_name')]]=$row[csf('po_total_price')];	
                                    }
                                    ?>
                                    <input type="hidden" id="value_<?php echo $i; ?>" value="<?php echo $row[csf('po_total_price')];?>"/>
                                    </td>
                                    <td width="50" id="value_percent_<?php echo $i; ?>" align="right"></td>
                                    <td width="130" align="right">
                                    <?php 
                                    echo number_format($full_shiped,0); $full_shipped_total+=$full_shiped;
                                    if (array_key_exists($row[csf('company_name')], $po_full_shiped_array)) 
                                    {
                                    $po_full_shiped_array[$row[csf('company_name')]]+=$full_shiped;
                                    }
                                    else
                                    {
                                    $po_full_shiped_array[$row[csf('company_name')]]=$full_shiped;	
                                    }
                                    /*if (array_key_exists($row[company_name], $po_full_shiped_value_array)) 
                                    {
                                    $po_full_shiped_value_array[$row[company_name]]+=$full_shiped_value;
                                    }
                                    else
                                    {
                                    $po_full_shiped_value_array[$row[company_name]]=$full_shiped_value;	
                                    }*/
                                    
                                    ?>
                                    </td>
                                    <td width="130" align="right">
                                    <?php 
                                    echo number_format($partial_shiped,0); $partial_shipped_total+=$partial_shiped;
                                    if (array_key_exists($row[csf('company_name')], $po_partial_shiped_array)) 
                                    {
                                    $po_partial_shiped_array[$row[csf('company_name')]]+=$partial_shiped;
                                    }
                                    else
                                    {
                                    $po_partial_shiped_array[$row[csf('company_name')]]=$partial_shiped;	
                                    }
                                    /*if (array_key_exists($row[company_name], $po_partial_shiped_value_array)) 
                                    {
                                    $po_partial_shiped_value_array[$row[company_name]]+=$partial_shipd_value;
                                    }
                                    else
                                    {
                                    $po_partial_shiped_value_array[$row[company_name]]=$partial_shipd_value;	
                                    }*/
                                    ?>
                                    </td> 
                                    <td width="130" align="right">
                                    <?php 
                                    $runing=$row[csf('po_quantity')]-($full_shiped+$partial_shiped); echo number_format($runing,0);$running_shipped_total+=$runing;
                                    if (array_key_exists($row[csf('company_name')], $po_running_array)) 
                                    {
                                    $po_running_array[$row[csf('company_name')]]+=$runing;
                                    }
                                    else
                                    {
                                    $po_running_array[$row[csf('company_name')]]=$runing;	
                                    }
                                    ?>
                                    </td>
                                    <td align="right"><?php $status=(($full_shiped+$partial_shiped)/$row[csf('po_quantity')])*100; $full_shipped_total_percent+=$status;  echo number_format($status,2); ?></td>  
                                    </tr>
                                    <?php
                                    $i++;
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <th width="50"></th>
                                    <th width="130"></th><th width="200"></th>
                                    <th width="130"><?php echo number_format($total_po,0); ?></th><th width="100"><?php  echo number_format($total_price,2); ?> <input type="hidden" id="total_value" value="<?php echo $total_price;?>"/></th><th width="50"></th>
                                    <th width="130"><?php echo number_format($full_shipped_total,0); ?></th><th width="130"><?php echo number_format($partial_shipped_total,0); ?></th> 
                                    <th width="130"><?php echo number_format($running_shipped_total,0); ?></th><th><input type="hidden" id="tot_row" value="<?php echo $i;?>"/></th>  
                                    </tfoot>
                                </table>
                            </fieldset>
                        </div>
                    </td>
                    <td valign="top">
                    <h3 align="left" id="accordion_h3" class="accordion_h" onClick="accordion_menu( this.id,'content_summary2_panel', '')"> -Summary Panel</h3>
                        <div id="content_summary2_panel"> 
                            <fieldset>
                                <table width="800" border="1" class="rpt_table" rules="all">
                                    <thead>
                                    <th>Company Name</th>
                                    <th>Particular Name</th>
                                    <th>Total Amount</th>
                                    <th>Full Shipped </th>
                                    <th>Partial Shipped </th>
                                    <th>Running </th>
                                    <th>Ex-factory Percentage</th>
                                    </thead>
									<?php
                                    $comp_po_total=0;
                                    $comp_po_total_value=0;
                                    $total_full_shiped_qnty=0;
                                    $total_par_qnty=0;
                                    $total_run_qnty=0;
                                    $total_full_shiped_val=0;
                                    $total_par_val=0;
                                    $total_run_val=0;
                                    foreach($po_qnty_array as $key=> $value)
                                    {
                                    ?>
                                    <tr>
                                    <td rowspan="2" align="center"><?php echo $company_short_name_arr[$key];//echo $company_name; ?></td>
                                    <td align="center">PO Quantity</td>
                                    <td align="right"><?php echo number_format($value+$po_qnty_array_projec[$key],0);$comp_po_total=$comp_po_total+$value+$po_qnty_array_projec[$key]; ?></td>
                                    <td align="right"><?php echo number_format($po_full_shiped_array[$key],0); $total_full_shiped_qnty+=$po_full_shiped_array[$key];?></td>
                                    <td align="right"><?php echo number_format($po_partial_shiped_array[$key],0); $total_par_qnty+=$po_partial_shiped_array[$key];?></td>
                                    <td align="right">
                                    <?php
                                    echo number_format($po_running_array[$key],0);
                                    $total_run_qnty+=$po_running_array[$key];
                                    ?>
                                    </td>
                                    <td align="right">
                                    <?php
                                    $ex_factory_per=(($po_full_shiped_array[$key]+$po_partial_shiped_array[$key])/($value))*100;
                                    echo number_format($ex_factory_per,2).' %';
                                    ?>
                                    </td>
                                    </tr>
                                    <tr bgcolor="white">
                                    <td align="center">LC Value</td>
                                    <td align="right"><?php echo number_format($po_value_array[$key],2);  $comp_po_total_value=$comp_po_total_value+$po_value_array[$key];?></td>
                                    <td align="right">
                                    <?php
                                    $full_shiped_value=($po_value_array[$key]/$value)*$po_full_shiped_array[$key];
                                    echo number_format($full_shiped_value,2);
                                    $total_full_shiped_val+=$full_shiped_value;
                                    
                                    //echo number_format($po_full_shiped_value_array[$key],2);
                                    //$total_full_shiped_val+=$po_full_shiped_value_array[$key];
                                    ?>
                                    </td>
                                    <td align="right">
                                    <?php
                                    $full_partial_shipeddd_value=($po_value_array[$key]/$value)*$po_partial_shiped_array[$key];
                                    echo number_format($full_partial_shipeddd_value,2);
                                    $total_par_val+=$full_partial_shipeddd_value;
                                    //echo number_format($po_partial_shiped_value_array[$key],2);
                                    // $total_par_val+=$po_partial_shiped_value_array[$key];
                                    ?>
                                    </td>
                                    
                                    <td align="right">
                                    <?php
                                    $full_running_value=($po_value_array[$key]/$value)*$po_running_array[$key];
                                    echo number_format($full_running_value,2);
                                    $total_run_val+=$full_running_value;
                                    //echo number_format($po_running_value_array[$key],2);
                                    //$total_run_val+=$po_running_value_array[$key];
                                    ?>
                                    </td>
                                    <td align="right">
                                    <?php
                                    $ex_factory_per_value=(($full_shiped_value+$full_partial_shipeddd_value)/($po_value_array[$key]))*100;
                                    echo number_format($ex_factory_per_value,2).' %';
                                    //$ex_factory_per_value=(($po_full_shiped_value_array[$key]+$po_partial_shiped_value_array[$key])/($po_value_array[$key]))*100;
                                    //echo number_format($ex_factory_per_value,2).' %';
                                    ?>
                                    </td>
                                    </tr>
									<?php
                                    }
                                    ?>
                                    <tfoot>
                                    <tr>
                                    <th align="center" rowspan="2"> Total:</th>
                                    <th align="center">Qnty Total:</th>
                                    <th align="right"><?php echo number_format($comp_po_total,0); ?></th>
                                    
                                    <th align="right">
                                    <?php
                                    
                                    echo number_format($total_full_shiped_qnty,2);
                                    ?>
                                    </th>
                                    <th align="right">
                                    <?php
                                    
                                    echo number_format($total_par_qnty,2);
                                    ?>
                                    </th>
                                    <th align="right">
                                    <?php
                                    
                                    echo number_format($total_run_qnty,2);
                                    ?>
                                    </th>
                                    <th align="right">
                                    <?php
                                    //echo number_format($ex_factory_per_value,2).' %';
                                    ?>
                                    </th>
                                    </tr>
                                    <tr bgcolor="#999999">
                                    <th align="center">Value Total:</th>
                                    <th align="right"><?php echo number_format($comp_po_total_value,2); ?></th>
                                    <th align="right">
                                    <?php
                                    echo number_format($total_full_shiped_val,2);
                                    ?>
                                    </th>
                                    <th align="right">
                                    <?php
                                    echo number_format($total_par_val,2);
                                    ?>
                                    </th>
                                    <th align="right">
                                    <?php
                                    echo number_format($total_run_val,2);
                                    ?>
                                    </th>
                                    <th align="right">
                                    <?php
                                    //echo number_format($ex_factory_per_value,2).' %';
                                    ?>
                                    </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </fieldset>
                        </div>
                    </td>
                    <td valign="top">
                    <h3 align="left" id="accordion_h4" class="accordion_h" onClick="accordion_menu( this.id,'content_summary3_panel', '')"> -Shipment Performance Summary</h3>
                    <div id="content_summary3_panel"> 
                    </div>
                    </td>
                </tr>
            </table>
            <h3 align="left" id="accordion_h4" class="accordion_h" onClick="accordion_menu( this.id,'content_report_panel', '')"> -Report Panel</h3>
            <div id="content_report_panel"> 
                <table width="2930" id="table_header_1" border="1" class="rpt_table" rules="all">
                    <thead>
                        <tr>
                            <th width="50">SL</th>
                            <th width="65" >Company</th>
                            <th width="70">Job No</th>
                            <th  width="50">Buyer</th>
                            <th  width="110">PO No</th>
                            <th  width="50">Agent</th>
                            <th width="60">Order Status</th>
                            <th width="70">Prod. Catg</th>
                            <th width="30">Img</th>
                            <th width="90">Style Ref</th>
                            <th width="150">Item</th>
                            <th width="80">Org.Ship Date</th>
                            <th width="80">Pub.Ship Date</th>
                            <th width="80">PO Rec. Date</th>
                            <th  width="50">Days in Hand</th>
                            <th width="90">Order Qnty(Pcs)</th>
                            <th width="90">Order Qnty</th>
                            <th width="30">Uom</th>
                            <th  width="50">Per Unit Price</th>
                            <th width="100">Order Value</th>
                            <th width="100">LC/SC No</th>
                            <th width="90">Ex-Fac Qnty </th>
                            <th  width="90">Short/Access Qnty</th>
                            <th width="120">Short/Access Value</th>
                            <th width="100">Yarn Req</th>
                            <th width="100">CM </th>
                            <th width="100" >Shipping Status</th>
                            <th width="150"> Team Member</th>
                            <th width="150">Team Name</th>
                            <th width="30">Id</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                </table>
                <div style=" max-height:400px; overflow-y:scroll; width:2930px"  align="left" id="scroll_body">
                    <table width="2910" border="1" class="rpt_table" rules="all" id="table-body">
                    <?php
                    $i=1;
                    $order_qnty_pcs_tot=0;
                    $order_qntytot=0;
                    $oreder_value_tot=0;
                    $total_ex_factory_qnty=0;
                    $total_short_access_qnty=0;
                    $total_short_access_value=0;
                    $yarn_req_for_po_total=0;
                    //$data_array=sql_select("select a.job_no,a.company_name,a.buyer_name,a.agent_name,a.style_ref_no,a.job_quantity,a.product_category,a.job_no,a.gmts_item_id,a.total_set_qnty,a.order_uom,a.team_leader,a.dealing_marchant,b.id,b.is_confirmed ,b.po_number,b.po_quantity,b.pub_shipment_date,b.po_received_date,DATEDIFF(b.pub_shipment_date,'$date') date_diff,b.unit_price,b.po_total_price,b.details_remarks,b.shiping_status,sum(c.ex_factory_qnty) as ex_factory_qnty,MAX(ex_factory_date) as ex_factory_date  from wo_po_details_master a, wo_po_break_down b,pro_ex_factory_mst c where a.job_no=b.job_no_mst and b.id=c.po_break_down_id  and a.company_name like '$company_name' and a.buyer_name like '$buyer_name' and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and b.status_active=1 group by b.id order by b.pub_shipment_date,a.job_no_prefix_num,b.id");
					$lc_number_arr=return_library_array( "select a.wo_po_break_down_id, b.export_lc_no from com_export_lc_order_info a, com_export_lc b where a.com_export_lc_id=b.id",'wo_po_break_down_id','export_lc_no');

					 $data_array_group=sql_select("select b.grouping  as grouping   from wo_po_details_master a, wo_po_break_down b LEFT JOIN pro_ex_factory_mst c on b.id = c.po_break_down_id where  a.job_no=b.job_no_mst   and a.company_name like '$company_name' $buyer_id_cond and a.team_leader like '$team_leader' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and b.status_active=1 group by b.grouping");
					 foreach ($data_array_group as $row_group)
                    { 
					$gorder_qnty_pcs_tot=0;
                    $gorder_qntytot=0;
					$goreder_value_tot=0;
					$gtotal_ex_factory_qnty=0;
					$gtotal_short_access_qnty=0;
					$gtotal_short_access_value=0;
                    $gyarn_req_for_po_total=0;
					?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="vertical-align:middle" height="25" >
                            <th width="50" align="center" > <?php echo $row_group[csf('grouping')]; ?> </th>
                            <th width="65" ></th>
                            <th  width="70"></th>
                            <th  width="50"></th>
                            <th width="110"></th>
                            <th  width="50"></th>
                            <th width="60"></th>
                            <th width="70"></th>
                            <th width="30"></th>
                            <th width="90"></th>
                            <th width="150"></th>
                            <th width="80"></th>
                            <th width="80"></th>
                            <th width="80"></th>
                            <th  width="50"></th>
                            <th width="90"></th>
                            <th width="90"></th>
                            <th width="30"></th>
                            <th  width="50"></th>
                            <th width="100"></th>
                            <th width="100"></th>
                            <th width="90"></th>
                            <th  width="90"></th>
                            <th width="120"></th>
                            <th width="100"></th>
                            <th width="100"></th>
                            <th width="100" ></th>
                            <th width="150"></th>
                            <th width="150"></th>
                            <th width="30"></th>
                            <th></th>
                            </tr>
					<?php 
					
					if($db_type==0)
					{
                    $data_array=sql_select("select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant, b.id, b.is_confirmed, b.po_number, b.po_quantity, b.pub_shipment_date, b.shipment_date, b.po_received_date, DATEDIFF(b.pub_shipment_date, '$date') date_diff_1, DATEDIFF(b.shipment_date,'$date') date_diff_2, b.unit_price, b.po_total_price, b.details_remarks, b.shiping_status, sum(c.ex_factory_qnty) as ex_factory_qnty, MAX(c.ex_factory_date) as ex_factory_date, DATEDIFF(b.pub_shipment_date,MAX(c.ex_factory_date)) date_diff_3, DATEDIFF(b.shipment_date, MAX(c.ex_factory_date)) date_diff_4 from wo_po_details_master a, wo_po_break_down b LEFT JOIN pro_ex_factory_mst c on b.id = c.po_break_down_id and c.status_active=1 and c.is_deleted=0 where  a.job_no=b.job_no_mst and a.company_name like '$company_name'  $buyer_id_cond and a.team_leader like '$team_leader' and b.grouping='".$row_group[csf('grouping')]."' and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and b.status_active=1  group by b.id order by b.pub_shipment_date,a.job_no_prefix_num,b.id");
					}
					
					if($db_type==2)
					{
						$date=date('d-m-Y');
						if($row_group[csf('grouping')]!="")
						{
						$grouping="and b.grouping='".$row_group[csf('grouping')]."'";
						}
						if($row_group[csf('grouping')]=="")
						{
						 $grouping	= "and b.grouping IS NULL";
						}
                    $data_array=sql_select("select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant, b.id, b.is_confirmed, b.po_number, b.po_quantity, b.pub_shipment_date, b.shipment_date, b.po_received_date, (b.pub_shipment_date - to_date('$date','dd-mm-yyyy')) date_diff_1, (b.shipment_date - to_date('$date','dd-mm-yyyy')) date_diff_2, b.unit_price, b.po_total_price, b.details_remarks, b.shiping_status, sum(c.ex_factory_qnty) as ex_factory_qnty, MAX(c.ex_factory_date) as ex_factory_date, (b.pub_shipment_date - MAX(c.ex_factory_date)) date_diff_3, (b.shipment_date - MAX(c.ex_factory_date)) date_diff_4 from wo_po_details_master a, wo_po_break_down b LEFT JOIN pro_ex_factory_mst c on b.id = c.po_break_down_id and c.status_active=1 and c.is_deleted=0 where  a.job_no=b.job_no_mst and a.company_name like '$company_name'  $buyer_id_cond and a.team_leader like '$team_leader' $grouping and a.dealing_marchant like '$dealing_marchant' $date_cond $pocond and a.status_active=1 and b.status_active=1 group by a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant, b.id, b.is_confirmed, b.po_number, b.po_quantity, b.pub_shipment_date, b.shipment_date, b.po_received_date,b.unit_price,b.po_total_price, b.details_remarks, b.shiping_status order by b.pub_shipment_date,a.job_no_prefix_num,b.id");
					}
					
					
                    foreach ($data_array as $row)
                    { 
						if ($i%2==0)  
						$bgcolor="#E9F3FF";
						else
						$bgcolor="#FFFFFF";	
						//--Calculation Yarn Required-------
						/*$data_array_yarn_cons=sql_select("select cons from  wo_pre_cos_fab_co_avg_con_dtls where  po_break_down_id='$row[id]'");
						$total_cons=0;
						$total_row=0;
						foreach($data_array_yarn_cons as $row_yarn_cons)
						{
						$total_cons=$total_cons+$row_yarn_cons['cons'];
						if($row_yarn_cons['cons'] !=0 || $row_yarn_cons['cons']!='' )
						{
						$total_row++;
						}
						}
						$yarn_req_for_po=($total_cons/$total_row)*$row['po_quantity'];*/
						$cons=0;
						$costing_per_pcs=0;
						$data_array_yarn_cons=sql_select("select yarn_cons_qnty from  wo_pre_cost_sum_dtls where  job_no='".$row[csf('job_no')]."'");
						$data_array_costing_per=sql_select("select costing_per from  wo_pre_cost_mst where  job_no='".$row[csf('job_no')]."'");
						list($costing_per)=$data_array_costing_per;
						if($costing_per[csf('costing_per')]==1)
						{
						  $costing_per_pcs=1*12;	
						}
						else if($costing_per[csf('costing_per')]==2)
						{
						 $costing_per_pcs=1*1;	
						}
						else if($costing_per[csf('costing_per')]==3)
						{
						 $costing_per_pcs=2*12;	
						}
						else if($costing_per[csf('costing_per')]==4)
						{
						 $costing_per_pcs=3*12;	
						}
						else if($costing_per[csf('costing_per')]==5)
						{
						 $costing_per_pcs=4*12;	
						}
						
						$yarn_req_for_po=0;
						foreach($data_array_yarn_cons as $row_yarn_cons)
						{
							$cons=$row_yarn_cons[csf('yarn_cons_qnty')];
							$yarn_req_for_po=($row_yarn_cons[csf('yarn_cons_qnty')]/ $costing_per_pcs)*$row[csf('po_quantity')];
						}
						
						//--Calculation Yarn Required-------
						//--Color Determination-------------
						//==================================
						$shipment_performance=0;
						if($row[csf('shiping_status')]==1 && $row[csf('date_diff_1')]>10 )
						{
						$color="";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row[csf('shiping_status')]==1 && ($row[csf('date_diff_1')]<=10 && $row[csf('date_diff_1')]>=0))
						{
						$color="orange";
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row[csf('shiping_status')]==1 &&  $row[csf('date_diff_1')]<0)
						{
						$color="red";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						//=====================================
						if($row[csf('shiping_status')]==2 && $row[csf('date_diff_1')]>10 )
						{
						$color="";	
						}
						if($row[csf('shiping_status')]==2 && ($row[csf('date_diff_1')]<=10 && $row[csf('date_diff_1')]>=0))
						{
						$color="orange";	
						}
						if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_1')]<0)
						{
						$color="red";	
						}
						if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_2')]>=0)
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;	
						}
						if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_2')]<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						//========================================
						if($row[csf('shiping_status')]==3 && $row[csf('date_diff_3')]>=0 )
						{
						$color="green";	
						}
						if($row[csf('shiping_status')]==3 &&  $row[csf('date_diff_3')]<0)
						{
						$color="#2A9FFF";	
						}
						if($row[csf('shiping_status')]==3 && $row[csf('date_diff_4')]>=0 )
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;
						}
						if($row[csf('shiping_status')]==3 &&  $row[csf('date_diff_4')]<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="vertical-align:middle" height="25" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i; ?>">
								<td width="50" align="center" bgcolor="<?php echo $color; ?>"> <?php echo $i; ?> </td>
								<td width="65" align="center" ><?php echo $company_short_name_arr[$row[csf('company_name')]];?></td>
								<td width="70" align="center"><?php echo $row[csf('job_no_prefix_num')];?></td>
								<td  width="50" align="center"><?php echo $buyer_short_name_arr[$row[csf('buyer_name')]];?></td>
								<td  width="110" align="center"><?php echo $row[csf('po_number')];?></td>
								<td  width="50" align="center"><?php echo $buyer_short_name_arr[$row[csf('agent_name')]];?></td>
								<td width="60" align="center"><?php echo $order_status[$row[csf('is_confirmed')]];?></td>
								<td width="70" align="center"><?php echo $product_category[$row[csf('product_category')]];?></td>
								<td width="30"><img  src='../../../<?php echo $imge_arr[$row[csf('job_no')]]; ?>' height='25' width='30' /></td>
								<td width="90" align="center"><?php echo $row[csf('style_ref_no')];?></td>
								<td width="150" align="center">
								<?php
								$gmts_item_id=explode(',',$row[csf('gmts_item_id')]);
								for($j=0; $j<=count($gmts_item_id); $j++)
								{
								echo $garments_item[$gmts_item_id[$j]];
								}
								?>
								</td>
                                <td width="80" align="center"><?php echo change_date_format($row[csf('shipment_date')],'dd-mm-yyyy','-');?></td>
								<td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')],'dd-mm-yyyy','-');?></td>
								<td width="80" align="center"><?php echo change_date_format($row[csf('po_received_date')],'dd-mm-yyyy','-');?></td>
								<td  width="50" align="center" bgcolor="<?php echo $color; ?>"> 
								<?php
								if($row[csf('shiping_status')]==1 || $row[csf('shiping_status')]==2)
								{
								echo $row[csf('date_diff_1')];
								}
								if($row[csf('shiping_status')]==3)
								{
								echo $row[csf('date_diff_3')];
								}
								?>
								</td>
								<td width="90" align="right">
								<?php 
								echo number_format(($row[csf('po_quantity')]*$row[csf('total_set_qnty')]),0);  
								$order_qnty_pcs_tot=$order_qnty_pcs_tot+($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
								$gorder_qnty_pcs_tot=$gorder_qnty_pcs_tot+($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
								?>
                                </td>
								<td width="90" align="right">
								<?php 
								echo number_format( $row[csf('po_quantity')],0);
								$order_qntytot=$order_qntytot+$row[csf('po_quantity')];
								$gorder_qntytot=$gorder_qntytot+$row[csf('po_quantity')];
								?>
								</td>
								<td width="30" align="center"><?php echo $unit_of_measurement[$row[csf('order_uom')]];?></td>
								<td  width="50" align="right"><?php echo number_format($row[csf('unit_price')],2);?></td>
								<td width="100" align="right">
								<?php 
								echo number_format($row[csf('po_total_price')],2);
								$oreder_value_tot=$oreder_value_tot+$row[csf('po_total_price')];
								$goreder_value_tot=$goreder_value_tot+$row[csf('po_total_price')];
								?>
                                </td>
								<td width="100" align="center"><?php echo $lc_number_arr[$row[csf('id')]]; ?></td>
								<td width="90" align="right">
								<?php 
								$ex_factory_qnty=$row[csf('ex_factory_qnty')]; 
								echo  number_format( $ex_factory_qnty,0); 
								$total_ex_factory_qnty=$total_ex_factory_qnty+$ex_factory_qnty ;
								$gtotal_ex_factory_qnty=$gtotal_ex_factory_qnty+$ex_factory_qnty ;;
								if ($shipment_performance==0)
								{
								$po_qnty['yet']+=($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
								$po_value['yet']+=100;
								}
								else if ($shipment_performance==1)
								{
								$po_qnty['ontime']+=$ex_factory_qnty;
								$po_value['ontime']+=((100*$ex_factory_qnty)/($row[csf('po_quantity')]*$row[csf('total_set_qnty')]));
								$po_qnty['yet']+=(($row[csf('po_quantity')]*$row[csf('total_set_qnty')])-$ex_factory_qnty);
								}
								else if ($shipment_performance==2)
								{
								$po_qnty['after']+=$ex_factory_qnty;
								$po_value['after']+=((100*$ex_factory_qnty)/($row[csf('po_quantity')]*$row[csf('total_set_qnty')]));
								$po_qnty['yet']+=(($row[csf('po_quantity')]*$row[csf('total_set_qnty')])-$ex_factory_qnty);
								}
								?> 
								</td>
								<td  width="90" align="right">
								<?php 
								$short_access_qnty=($row[csf('po_quantity')]-$ex_factory_qnty); 
								echo number_format($short_access_qnty,0);
								$total_short_access_qnty=$total_short_access_qnty+$short_access_qnty;
								$gtotal_short_access_qnty=$gtotal_short_access_qnty+$short_access_qnty;;
								?>
                                </td>
								<td width="120" align="right">
								<?php 
								$short_access_value=$short_access_qnty*$row[csf('unit_price')];
								echo  number_format($short_access_value,2);
								$total_short_access_value=$total_short_access_value+$short_access_value;
								$gtotal_short_access_value=$gtotal_short_access_value+$short_access_value;
								?>
                                </td>
								<td width="100" align="right" title="<?php echo "Cons:".$cons."Costing per:".$costing_per[csf('costing_per')];?>">
								<?php 
								echo number_format($yarn_req_for_po,2);
								$yarn_req_for_po_total=$yarn_req_for_po_total+$yarn_req_for_po;
								$gyarn_req_for_po_total=$gyarn_req_for_po_total+$yarn_req_for_po;
								?>
                                </td>
								<td width="100" align="right"><?php echo number_format ($cm_for_shipment_schedule_arr[$row[csf('job_no')]],2); ?></td>
								<td width="100" align="center"><?php echo $shipment_status[$row[csf('shiping_status')]]; ?></td>
								<td width="150" align="center"><?php echo $company_team_member_name_arr[$row[csf('dealing_marchant')]];?></td>
								<td width="150" align="center"><?php echo $company_team_name_arr[$row[csf('team_leader')]];?></td>
								<td width="30"><?php echo $row[csf('id')]; ?></td>
								<td><?php echo $row[csf('details_remarks')]; ?></td>
							</tr>
                    <?php
                    $i++;
                    }
					?>
                     <tr bgcolor="#CCCCCC" style="vertical-align:middle" height="25">
                            <td width="50" align="center" >  Total: </td>
                            <td width="65" ></td>
                            <td  width="70"></td>
                            <td  width="50"></td>
                            <td width="110"></td>
                            <td  width="50"></td>
                            <td width="60"></td>
                            <td width="70"></td>
                            <td width="30"></td>
                            <td width="90"></td>
                            <td width="150"></td>
                            <td width="80"></td>
                            <td width="80"></td>
                            <td width="80"></td>
                            <td  width="50"></td>
                            <td width="90" align="right"><?php echo number_format($gorder_qnty_pcs_tot,0); ?></td>
                            <td width="90" align="right"><?php echo number_format($gorder_qntytot,0); ?></td>
                            <td width="30"></td>
                            <td  width="50"></td>
                            <td width="100" align="right"><?php echo number_format($goreder_value_tot,2); ?></td>
                            <td width="100"></td>
                            <td width="90" align="right"><?php echo number_format($gtotal_ex_factory_qnty,0); ?></td>
                            <td  width="90" align="right"> <?php echo number_format($gtotal_short_access_qnty,0); ?></td>
                            <td width="120" align="right"> <?php echo number_format($gtotal_short_access_value,0); ?></td>
                            <td width="100" align="right"><?php echo number_format($gyarn_req_for_po_total,2); ?></td>
                            <td width="100"></td>
                            <td width="100" ></td>
                            <td width="150"></td>
                            <td width="150"></td>
                            <td width="30"></td>
                            <td></th>
                            </tr>
                    <?php
					}
                    ?>
                    </table>
                </div>
                <table width="2930" id="report_table_footer" border="1" class="rpt_table" rules="all">
                    <tfoot>
                        <tr>
                            <th width="50"></th>
                            <th width="65" ></th>
                            <th  width="70"></th>
                            <th  width="50"></th>
                            <th width="110"></th>
                            <th  width="50"></th>
                            <th width="60"></th>
                            <th width="70"></th>
                            <th width="30"></th>
                            <th width="90"></th>
                            <th width="150"></th>
                            <th width="80"></th>
                            <th width="80"></th>
                            <th width="80"></th>
                            <th  width="50"></th>
                            <th width="90" id="total_order_qnty_pcs"><?php echo number_format($order_qnty_pcs_tot,0); ?></th>
                            <th width="90" id="total_order_qnty"><?php echo number_format($order_qntytot,0); ?></th>
                            <th width="30"></th>
                            <th  width="50"></th>
                            <th width="100" id="value_total_order_value"><?php echo number_format($oreder_value_tot,2); ?></th>
                            <th width="100"></th>
                            <th width="90" id="total_ex_factory_qnty"> <?php echo number_format($total_ex_factory_qnty,0); ?></th>
                            <th  width="90" id="total_short_access_qnty"><?php echo number_format($total_short_access_qnty,0); ?></th>
                            <th width="120" id="value_total_short_access_value"><?php echo number_format($total_short_access_value,0); ?></th>
                            <th width="100" id="value_yarn_req_tot"><?php echo number_format($yarn_req_for_po_total,2); ?></th>
                            <th width="100"></th>
                            <th width="100" ></th>
                            <th width="150"> </th>
                            <th width="150"></th>
                            <th width="30"></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
                <div id="shipment_performance" style="visibility:hidden">
                    <fieldset>
                        <table width="600" border="1" cellpadding="0" cellspacing="1" class="rpt_table" rules="all" >
                        <thead>
                        <tr>
                        <th colspan="4"> <font size="4">Shipment Performance</font></th>
                        </tr>
                        <tr>
                        <th>Particulars</th><th>No of PO</th><th>PO Qnty</th><th> %</th>
                        </tr>
                        </thead>
                        <tr bgcolor="#E9F3FF">
                        <td>On Time Shipment</td><td><?php echo $number_of_order['ontime']; ?></td><td align="right"><?php echo number_format($po_qnty['ontime'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['ontime'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                        <td> Delivery After Shipment Date</td><td><?php echo $number_of_order['after']; ?></td><td align="right"><?php echo number_format($po_qnty['after'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['after'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        <tr bgcolor="#E9F3FF">
                        <td>Yet To Shipment </td><td><?php echo $number_of_order['yet']; ?></td><td align="right"><?php echo number_format($po_qnty['yet'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['yet'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        
                        <tr bgcolor="#E9F3FF">
                        <td> </td><td></td><td align="right"><?php echo number_format($po_qnty['yet']+$po_qnty['ontime']+$po_qnty['after'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['yet'])/$order_qnty_pcs_tot)+((100*$po_qnty['after'])/$order_qnty_pcs_tot)+((100*$po_qnty['ontime'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
	</div>
<?php
}
?>	
