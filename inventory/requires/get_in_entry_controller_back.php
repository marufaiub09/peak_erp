﻿<?
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

//

//load drop down supplier
if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier", 170, "select a.id, a.supplier_name from lib_supplier a,lib_supplier_party_type b,lib_supplier_tag_company c where a.id=b.supplier_id and a.id=c.supplier_id and b.party_type=20 and c.tag_company=$data and a.status_active=1 and
		a.is_deleted=0 order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

//load drop down store
if ($action=="load_drop_down_store")
{	  
	$expData=explode("_",str_replace("'","",$data) );
	$company=$expData[0];
	$itemCategory=$expData[1];
	if($itemCategory!="" && $itemCategory!='0') $cond = " and b.category_type=$itemCategory"; else $cond = "";
	//echo "select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id and a.company_id=$company  and a.status_active=1 and a.is_deleted=0  $cond order by a.store_name";
 	echo create_drop_down( "cbo_store_name", 170, "select distinct(a.id),a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id and a.company_id=$company and a.status_active=1 and a.is_deleted=0  $cond order by a.store_name","id,store_name", 1, "-- Select --", 0, "",0 ); 
	 	 
	exit();
}   
   
//wo/pi popup here----------------------// 
if ($action=="piworeq_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		//master part call here
		$("#hidden_tbl_id").val(str);
		parent.emailwindow.hide(); 
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?  
                            $search_by_arr=array(1=>'PI Number',2=>'WO Number',3=>'Requisition No');
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../')";							
							echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="250" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<? echo $company; ?>+'_'+<? echo $item_category; ?>, 'create_wopireq_search_list_view', 'search_div', 'get_in_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
					<? echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_tbl_id" value="" />
                    <!-- ---------END-------------> 
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}



if($action=="create_wopireq_search_list_view")
{
	
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
  	$item_category = $ex_data[5];
	
	$sql_cond="";	
	if(trim($txt_search_by)==1) // for pi
	{
		if(trim($txt_search_common)!="")
		{
			$sql_cond .= " and a.pi_number LIKE '$txt_search_common%'";	
		}
		if($db_type==0)
		{
		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and a.pi_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		if($db_type==2)
		{
		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and a.pi_date  between '".change_date_format($txt_date_from,'mm-dd-yyyy','/',1)."' and '".change_date_format($txt_date_to,'mm-dd-yyyy','/',1)."'";
		}
		if(trim($company)!="") $sql_cond .= " and a.importer_id='$company'";
		if(trim($item_category)!=0 ) $sql_cond .= " and a.item_category_id='$item_category'";
 	}
	else if(trim($txt_search_by)==2) // for wo
	{
		if(trim($txt_search_common)!="")
		{
			$sql_cond .= " and wo_number LIKE '$txt_search_common%'";
		}
		if($db_type==0)
		{								
		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and wo_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		if($db_type==2)
		{								
		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and wo_date  between '".change_date_format($txt_date_from,'mm-dd-yyyy','/',1)."' and '".change_date_format($txt_date_to,'mm-dd-yyyy','/',1)."'";
		}
		if(trim($company)!="") $sql_cond .= " and company_name='$company'";
		if(trim($item_category)!=0 ) $sql_cond .= " and item_category='$item_category'";
 	}
	else if(trim($txt_search_by)==3) // for requisition
	{
		if(trim($txt_search_common)!="")
		{
			$sql_cond .= " and requ_no LIKE '$txt_search_common%'";				
		}	
		if($db_type==0)
		{		
		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and requisition_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		if($db_type==2)
		{		
		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and wo_date  between '".change_date_format($txt_date_from,'mm-dd-yyyy','/',1)."' and '".change_date_format($txt_date_to,'mm-dd-yyyy','/',1)."'";
		}
		if(trim($company)!="") $sql_cond .= " and company_id='$company'";
		if(trim($item_category)!=0 ) $sql_cond .= " and item_category_id='$item_category'";
 	}
 	 
	if($txt_search_by==1 ) //pi
	{	if($db_type==0) $field_grp=""; 
		else if($db_type==2) $field_grp="";
		else $field_grp="";
		if($db_type==0)
		{
 		 $sql = "select a.id as id,a.pi_number as wopi_number,a.pi_number as wopi_prefix, b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a left join com_btb_lc_master_details b on FIND_IN_SET(a.id,b.pi_id)
				where 				 
 				a.status_active=1 and 
				a.is_deleted=0 				
				$sql_cond";
		}
		if($db_type==2)
		{
 		  $sql = "select a.id as id,a.pi_number as wopi_number,a.pi_number as wopi_prefix, b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a ,com_btb_lc_master_details b,com_btb_lc_pi c
				where
				a.id=c.pi_id and
				b.id=c.com_btb_lc_master_details_id		 
 				and a.status_active=1 and 
				a.is_deleted=0 				
				$sql_cond";
				/*echo "select a.id as id,a.pi_number as wopi_number,a.pi_number as wopi_prefix, b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a left join com_btb_lc_master_details b on FIND_IN_SET(a.id,b.pi_id) ',' || b.pi_id || ',' LIKE '%,a.id,%' 
				where 				 
 				a.status_active=1 and 
				a.is_deleted=0 				
				$sql_cond";*/
		}
	}
	else if($txt_search_by==2) //wo
	{
 		 $sql = "select id,wo_number as wopi_number,wo_number_prefix_num as wopi_prefix, ' ' as lc_number,wo_date as wopi_date,supplier_id as supplier_id,currency_id as currency_id,source as source 
				from wo_non_order_info_mst
				where 
				status_active=1 and is_deleted=0 and
				pay_mode!=2
				$sql_cond";
	}
	else if($txt_search_by==3) //requisition 
	{
 	 $sql = "select id,requ_no as wopi_number,requ_prefix_num as wopi_prefix,' ' as lc_number,requisition_date as wopi_date,' ' as supplier_id,cbo_currency as currency_id, source
				from inv_purchase_requisition_mst
				where 
				status_active=1 and is_deleted=0 and
				pay_mode=4
				$sql_cond";
	}
	//echo $sql; die;
	$result = sql_select($sql);
 	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(3=>$supplier_arr,4=>$currency,5=>$source);
	echo create_list_view("list_view", "WO/PI No, LC ,Date, Supplier, Currency, Source","120,120,120,200,120,120,120","900","230",0, $sql , "js_set_value", "id", "", 1, "0,0,0,supplier_id,currency_id,source", $arr, "wopi_prefix,lc_number,wopi_date,supplier_id,currency_id,source", "",'','0,0,0,0,0,0') ;	
	exit();	
	
}




if($action=="populate_main_from_data")
{
	
	$ex_data = explode("**",$data);
	$type = $ex_data[0];
	$wopireqID = $ex_data[1];
	
	if($type==1 ) //pi
	{
		if($db_type==2)
		{
 		$sql = "select a.id as id,a.pi_number as wopi_number,b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a ,com_btb_lc_master_details b,com_btb_lc_pi c
				where
				a.id=c.pi_id and
				b.id=c.com_btb_lc_master_details_id					
 				and a.status_active=1 and a.is_deleted=0 and
				a.id=$wopireqID";
		}
		if($db_type==0)
		{
 		$sql = "select a.id as id,a.pi_number as wopi_number,b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a left join com_btb_lc_master_details b on FIND_IN_SET(a.id,b.pi_id)
				where				
 				a.status_active=1 and a.is_deleted=0 and
				a.id=$wopireqID";
		}
	}
	else if($type==2) //wo
	{
 		$sql = "select id,wo_number as wopi_number,' ' as lc_number,wo_date as wopi_date,supplier_id as supplier_id,currency_id as currency_id,source as source 
				from wo_non_order_info_mst
				where 
				status_active=1 and is_deleted=0 and
				pay_mode!=2 and
				id=$wopireqID";
	}
	else if($type==3) //requisition
	{
 		$sql = "select id,requ_no as wopi_number,' ' as lc_number,requisition_date as wopi_date,' ' as supplier_id,cbo_currency as currency_id, source
				from inv_purchase_requisition_mst
				where 
				status_active=1 and is_deleted=0 and
				pay_mode=4 and
				id=$wopireqID";
	}
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{	

  		echo "$('#hidden_type').val(".$type.");\n";
		echo "$('#txt_pi_wo_req_id').val(".$row[csf("id")].");\n";
		echo "$('#txt_pi_wo_req').val('".$row[csf("wopi_number")]."');\n";
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
 		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n"; 		
  	}
	
	exit();	
}


//right side product list create here--------------------//
if($action=="show_product_listview")
{ 
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_req_ID = $ex_data[1];
	
	if($receive_basis==1) // pi basis
	{	
		if($db_type==2)
		{
		 $sql = "select a.item_category_id as item_category, b.id,b.item_group,b.item_description,b.color_id,b.item_color as color,b.size_id,b.item_size,b.count_name as yarn_count,b.yarn_composition_item1 as yarn_comp_type1st,
		b.yarn_composition_percentage1 as yarn_comp_percent1st,b.yarn_composition_item2 as yarn_comp_type2nd,b.yarn_composition_percentage2 as yarn_comp_percent2nd,b.fabric_composition,b.fabric_construction,b.yarn_type,b.gsm,b.dia_width,b.weight
		from com_pi_master_details a ,com_pi_item_details b,com_btb_lc_pi c
				where
				a.id=c.pi_id and
				b.pi_id=c.pi_id and a.id=$wo_pi_req_ID";
		}
		if($db_type==0)
		{
		$sql = "select a.item_category_id as item_category, b.id,b.item_group,b.item_description,b.color_id,b.item_color as color,b.size_id,b.item_size,b.count_name as yarn_count,b.yarn_composition_item1 as yarn_comp_type1st,
		b.yarn_composition_percentage1 as yarn_comp_percent1st,b.yarn_composition_item2 as yarn_comp_type2nd,b.yarn_composition_percentage2 as yarn_comp_percent2nd,b.fabric_composition,b.fabric_construction,b.yarn_type,b.gsm,b.dia_width,b.weight
		from com_pi_master_details a left join com_pi_item_details b on FIND_IN_SET(a.id,b.pi_id)
		where a.id=$wo_pi_req_ID";
		}
	}  
	else if($receive_basis==2) // wo basis
	{
		$sql = "select a.item_category,b.id,b.yarn_count, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color_name,
		c.product_name_details 
		from wo_non_order_info_mst a, wo_non_order_info_dtls b left join product_details_master c on b.item_id=c.id and b.item_id!=0
		where a.id=b.mst_id and a.id=$wo_pi_req_ID";
	}
	else if($receive_basis==3) // requisition basis
	{
		$sql = "select a.item_category_id as item_category, b.id,c.product_name_details
		from inv_purchase_requisition_mst a, inv_purchase_requisition_dtls b, product_details_master c
		where a.id=b.mst_id and a.id=$wo_pi_req_ID and b.product_id=c.id";
	}	
	
	//echo $sql;die;   
	$result = sql_select($sql);
	$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$item_group_arr=return_library_array( "select id, item_name from lib_item_group",'id','item_name');
	$size_name_arr=return_library_array( "select id, size_name from lib_size",'id','size_name');
	
	$i=1; 
	?>
     
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0">
        	<thead><tr><th>SL</th><th>Product Name</th></tr></thead>
            <tbody>
            	<? foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					
 					$productName = "";
					if($row[csf("item_category")]==1) //1=>"yarn"
					{
						$compositionPart = $composition[$row[csf("yarn_comp_type1st")]]." ".$row[csf("yarn_comp_percent1st")];
						
						if( trim($row[csf("yarn_comp_type2nd")]) != 0 )
						{
							$compositionPart .=" ".$composition[$row[csf("yarn_comp_type2nd")]]." ".$row[csf("yarn_comp_percent2nd")];
						}
 						$productName = $yarn_count_arr[$row[csf("yarn_count")]]." ".$compositionPart." ".$yarn_type[$row[csf("yarn_type")]]." ".$color_name_arr[$row[csf("color")]];

					}
					else if($row[csf("item_category")]==2) //2=>"Knit Fabrics"
					{
						$productName = $row[csf("fabric_construction")]." ".$row[csf("fabric_composition")]." ".$row[csf("gsm")]." ".$row[csf("dia_width")]." ".$color_name_arr[$row[csf("color")]];
					}
					else if($row[csf("item_category")]==3) //3=>"Woven Fabrics"
					{
						$productName = $row[csf("fabric_construction")]." ".$row[csf("fabric_composition")]." ".$row[csf("gsm")]." ".$row[csf("weight")]." ".$color_name_arr[$row[csf("color")]];
					}				
					else if($row[csf("item_category")]==4) //4=>"Accessories"
					{
						$productName = $item_group_arr[$row[csf("item_group")]]." ".$row[csf("item_description")]." ".$size_name_arr[$row[csf("item_size")]]." ".$color_name_arr[$row[csf("color")]];
					}
 					else if($row[csf("item_category")]==5 || $row[csf("item_category")]==6 || $row[csf("item_category")]==7) //5=>"Chemicals",6=>"Dyes",7=>"Auxilary Chemicals"
					{
						//$productName = $row[csf("item_group")]." ".$row[csf("item_description")]." ".$size_name_arr[$row[csf("item_size")]];
						$productName = $row[csf("product_name_details")];
						
					}
					else if($row[csf("item_category")]==8) //8=>"Spare Parts"
					{
						//$productName = $row[csf("item_group")]." ".$row[csf("item_description")]." ".$size_name_arr[$row[csf("item_size")]];
						$productName = $row[csf("product_name_details")];
					}
					else if($row[csf("item_category")]==9) //9=>"Machinaries"
					{
						//$productName = $row[csf("item_group")]." ".$row[csf("item_description")]." ".$size_name_arr[$row[csf("item_size")]];
						$productName = $row[csf("product_name_details")];
					}
					else if($row[csf("item_category")]==10) //10=>"Other Capital Items"
					{
						//$productName = $row[csf("item_group")]." ".$row[csf("item_description")]." ".$size_name_arr[$row[csf("item_size")]];
						$productName = $row[csf("product_name_details")];
					}
					else if($row[csf("item_category")]==11) //11=>"Stationaries"
					{
						//$productName = $item_group_arr[$row[csf("item_group")]]." ".$row[csf("item_description")]." ".$size_name_arr[$row[csf("item_size")]];
						$productName = $row[csf("product_name_details")];
					}
					
					
				?>
                	<tr bgcolor="<? echo $bgcolor; ?>" onClick='get_php_form_data("<? echo $receive_basis.'**'.$productName.'**'.$row[csf("id")].'**'.$row[csf("item_category")];?>","wo_pi_req_product_form_input","requires/get_in_entry_controller")' style="cursor:pointer" >
                		<td><? echo $i; ?></td>
                    	<td><? echo $productName; ?></td>
                    </tr>
                <? $i++; } ?>
            </tbody>
        </table>
     </fieldset>   
	<?	 
	exit();
}


  
  
if($action=="wo_pi_req_product_form_input")
{
	
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$product_name_details = $ex_data[1];
	$wo_pi_req_ID = $ex_data[2]; //pi,wo,req dtls table ID
 	$category = $ex_data[3];
	
	if($receive_basis==1) // pi basis
	{	
		$sql = "select uom,quantity,net_pi_rate as rate,amount from com_pi_item_details where id=$wo_pi_req_ID";
 	}  
	else if($receive_basis==2) // wo basis
	{
		$sql = "select uom,supplier_order_quantity as quantity,rate,amount from wo_non_order_info_dtls where id=$wo_pi_req_ID";
 	}
	else if($receive_basis==3) // requisition basis
	{
		$sql = "select cons_uom as uom,quantity,rate,amount from inv_purchase_requisition_dtls  where id=$wo_pi_req_ID";	
 	}	
	
 	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{ 
		echo "$('#txt_item_description').val('".$product_name_details."');\n";
		echo "$('#cbo_uom').val(".$row[csf("uom")].");\n";
		echo "$('#cbo_uom').attr('disabled',true);\n";
		echo "$('#txt_quantity').val(".$row[csf("quantity")].");\n";
		echo "$('#txt_rate').val('".number_format($row[csf("rate")],$dec_place[3],".","")."');\n";
		echo "$('#txt_amount').val('".number_format($row[csf("amount")],$dec_place[4],".","")."');\n";
  	}
	
	exit();	
}  
  
  


//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 		 
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
 		
		
		//---------------Check Duplicate product in Same system number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_gate_in_mst a, inv_gate_in_dtls b","a.id=b.mst_id and a.sys_number=$txt_system_id and b.item_description=$txt_item_description"); 
		if($duplicate==1 && str_replace("'","",$txt_system_id)!="") 
		{
			//check_table_status( $_SESSION['menu_id'],0);
			echo "20**Duplicate Product is Not Allow in Same  System ID";
			die;
		}
		//------------------------------Check Brand END---------------------------------------//
		 
		if(str_replace("'","",$txt_system_id)!="") //update
		{
			$new_sys_number[0] = str_replace("'","",$txt_system_id);
			$id=return_field_value("id","inv_gate_in_mst","sys_number=$txt_system_id");
			//inv_gate_in_mst master table UPDATE here START----------------------//		
			$field_array="item_category*sample*piworeq_type*pi_wo_req_id*supplier_name*challan_no*receive_date*currency*time_hour*time_minute*store_id*updated_by*update_date";
			$data_array="".$cbo_item_category."*".$cbo_sample."*".$hidden_type."*".$txt_pi_wo_req_id."*".$cbo_supplier."*".$txt_challan_no."*".$txt_receive_date."*".$cbo_currency."*".$txt_start_hours."*".$txt_start_minuties."*".$cbo_store_name."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_update("inv_gate_in_mst",$field_array,$data_array,"id",$id,0);	
			
			//inv_gate_in_mst master table UPDATE here END---------------------------------------//;
		}
		else // new insert
 		{			
			$id=return_next_id("id", "inv_gate_in_mst", 1);			
			// inv_gate_in_mst master table entry here START---------------------------------------//	
			 if($db_type==2)
				{
				
			$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GIE', date("Y",time()), 5, "select sys_number_prefix,sys_number_prefix_num from inv_gate_in_mst where company_name=$cbo_company_name and TO_CHAR(insert_date,'YYYY')=".date('Y',time())." order by id DESC ", "sys_number_prefix", "sys_number_prefix_num" ));
				}
				 if($db_type==0)
				{	
			$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GIE', date("Y",time()), 5, "select sys_number_prefix,sys_number_prefix_num from inv_gate_in_mst where company_name=$cbo_company_name and YEAR(insert_date)=".date('Y',time())." order by id DESC ", "sys_number_prefix", "sys_number_prefix_num" ));
				}
			$field_array="id,sys_number_prefix,sys_number_prefix_num,sys_number,company_name,item_category,sample,piworeq_type,pi_wo_req_id,pi_wo_req_number,supplier_name,challan_no,receive_date,currency,time_hour,time_minute,store_id,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_sys_number[1]."','".$new_sys_number[2]."','".$new_sys_number[0]."',".$cbo_company_name.",".$cbo_item_category.",".$cbo_sample.",".$hidden_type.",".$txt_pi_wo_req_id.",".$txt_pi_wo_req.",".$cbo_supplier.",".$txt_challan_no.",".$txt_receive_date.",".$cbo_currency.",".$txt_start_hours.",".$txt_start_minuties.",".$cbo_store_name.",'".$user_id."','".$pc_date_time."')";
			//echo $field_array."<br>".$data_array;die;
			 //echo "insert into inv_gate_in_mst (".$field_array.") values ".$data_array;	  		
			//$rID=sql_insert("inv_gate_in_mst",$field_array,$data_array,0);
			
			// inv_gate_in_mst master table entry here END---------------------------------------// 
		}		
 					
		//check master ID
		if($id == "" ){ echo "15"; exit(); }
		 
		//inv_gate_in_dtls details table entry here START---------------------------------------//		
		$dtlsid=return_next_id("id", "inv_gate_in_dtls", 1);		
  		$field_array_dtls="id,mst_id,item_description,uom,quantity,rate,amount,remarks,inserted_by,insert_date";
		$data_array_dtls="(".$dtlsid.",".$id.",".$txt_item_description.",".$cbo_uom.",".$txt_quantity.",".$txt_rate.",".$txt_amount.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
 		//echo $field_array."<br>".$data_array;die;
		//echo "insert into inv_gate_in_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;	
 		//$dtlsrID=sql_insert("inv_gate_in_dtls",$field_array_dtls,$data_array_dtls,1);
		
		//inv_gate_in_dtls details table entry here END---------------------------------------// 
		
			if(str_replace("'","",$txt_system_id)!="") //update
				{
				$rID=sql_update("inv_gate_in_mst",$field_array,$data_array,"id",$id,0);	
				}
				else
				{
				$rID=sql_insert("inv_gate_in_mst",$field_array,$data_array,0);
				}
			$dtlsrID=sql_insert("inv_gate_in_dtls",$field_array_dtls,$data_array_dtls,1);
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
			{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_sys_number[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2)
		{
			if($rID && $dtlsrID)
			{
			oci_commit($con);
			echo "0**".$new_sys_number[0];
			}
		}
		else
		{	oci_rollback($con);
			echo "10**".$new_sys_number[0];
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		//check_table_status( $_SESSION['menu_id'],0);
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		if( str_replace("'","",$update_id) == "" )
		{
			echo "15";exit(); 
		}
		
  		
		//inv_gate_in_mst master table UPDATE here START----------------------//		
		$field_array="item_category*sample*piworeq_type*pi_wo_req_id*supplier_name*challan_no*receive_date*currency*time_hour*time_minute*store_id*updated_by*update_date";
		$data_array="".$cbo_item_category."*".$cbo_sample."*".$hidden_type."*".$txt_pi_wo_req_id."*".$cbo_supplier."*".$txt_challan_no."*".$txt_receive_date."*".$cbo_currency."*".$txt_start_hours."*".$txt_start_minuties."*".$cbo_store_name."*'".$user_id."'*'".$pc_date_time."'";
 		//echo $field_array."<br>".$data_array;die;
 		//$rID=sql_update("inv_gate_in_mst",$field_array,$data_array,"sys_number",$txt_system_id,1);	
		//inv_gate_in_mst master table UPDATE here END---------------------------------------// 
		
 		//txt_start_hours*txt_start_minuties*cbo_store_name*txt_remarks	
		//inv_gate_in_dtls details table UPDATE here START-----------------------------------//			
 		$field_array2 = "item_description*uom*quantity*rate*amount*remarks*updated_by*update_date";
 		$data_array2 = "".$txt_item_description."*".$cbo_uom."*".$txt_quantity."*".$txt_rate."*".$txt_amount."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
		$rID=sql_update("inv_gate_in_mst",$field_array,$data_array,"sys_number",$txt_system_id,1);	
 		$dtlsrID=sql_update("inv_gate_in_dtls",$field_array2,$data_array2,"id",$update_id,1); 
		//inv_gate_in_dtls details table UPDATE here END-----------------------------------//
		
 		
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_system_id);
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID)
			{
				oci_commit($con);
			echo "1**".str_replace("'","",$txt_system_id);
			}
			else
			{
				oci_rollback($con);
			echo "10**".str_replace("'","",$txt_system_id);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		// master table delete here---------------------------------------
		$mst_id = return_field_value("id","inv_gate_in_mst","sys_number=$txt_system_id");	
		if($mst_id=="" || $mst_id==0){ echo "15**0"; die;}
		//$rID=1;
 		$rID = sql_update("inv_gate_in_mst",'status_active*is_deleted','0*1',"id",$mst_id,1);
		//$dtlsrID=1;
		$dtlsrID = sql_update("inv_gate_in_dtls",'status_active*is_deleted','0*1',"mst_id",$mst_id,1);
	
	if($db_type==0)
		{	
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_system_id);
			}
		}
		if($db_type==2 || $db_type==1 )
		{	if($rID && $dtlsrID)
			{
				oci_commit($con);
				echo "2**".str_replace("'","",$txt_system_id);
			}
		}
		else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_system_id);
			}
		disconnect($con);
		die;
	}		
}




if($action=="sys_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(sys_number)
	{
 		$("#hidden_sys_number").val(sys_number); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?  //select a.id, a.supplier_name from lib_supplier a,lib_supplier_party_type b,lib_supplier_tag_company c where a.id=b.supplier_id and a.id=c.supplier_id  and c.tag_company=$company and a.status_active=1 and a.is_deleted=0 order by a.supplier_name
 							 echo create_drop_down( "cbo_supplier", 150, "select distinct(a.id), a.supplier_name from lib_supplier a,lib_supplier_party_type b,lib_supplier_tag_company c where a.id=b.supplier_id and a.id=c.supplier_id  and c.tag_company=$company and a.status_active=1 and a.is_deleted=0 order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td>
                        <?  
                            $search_by = array(1=>'System Number',2=>'Challan No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 120, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<? echo $company; ?>, 'create_sys_search_list_view', 'search_div', 'get_in_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<? echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_sys_number" value="hidden_sys_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}




if($action=="create_sys_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
 	//echo $fromDate;die;
 	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for system number
		{
			$sql_cond .= " and sys_number LIKE '%$txt_search_common%'";				
		}
		else if(trim($txt_search_by)==2) // for chllan no
		{
			$sql_cond .= " and challan_no LIKE '%$txt_search_common%'";				
 		}		 
 	} 
	if($db_type==2) 
	{
	if( $fromDate!="" || $toDate!="" ) $sql_cond .= " and receive_date  between '".change_date_format($fromDate,'mm-dd-yyyy','/',1)."' and '".change_date_format($toDate,'mm-dd-yyyy','/',1)."'";
	}
	if($db_type==0) 
	{
	if( $fromDate!="" || $toDate!="" ) $sql_cond .= " and receive_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
	}
	
	if(trim($company)!="") $sql_cond .= " and company_name='$company'";
	 if($db_type==0) $grp_field="group by id";
	 if($db_type==2) $grp_field="group by id, sys_number_prefix_num, sys_number, supplier_name, challan_no, receive_date, piworeq_type, pi_wo_req_number,currency";
	  else $grp_field="";
	  
 	
	$sql = "select sys_number_prefix_num, sys_number, supplier_name, challan_no, receive_date, piworeq_type, pi_wo_req_number,currency 
			from inv_gate_in_mst where status_active=1 and is_deleted=0 $sql_cond $grp_field";
//	echo $sql;
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$supplier_arr,5=>$currency);
	echo create_list_view("list_view", "System No, Supplier Name, Challan No, Receive Date, PI/WO/REQ No, Currency","150,150,120,120,120,100","900","260",0, $sql , "js_set_value", "sys_number", "", 1, "0,supplier_name,0,0,0,currency", $arr, "sys_number_prefix_num,supplier_name,challan_no,receive_date,pi_wo_req_number,currency", "",'','0,0,0,0,0,0,1') ;	
	exit();
	
}

if($action=="populate_master_from_data")
{
	
	$sql="select sys_number,company_name,item_category,sample,piworeq_type,pi_wo_req_id,pi_wo_req_number,supplier_name,challan_no,receive_date,currency,time_hour,time_minute,store_id from inv_gate_in_mst where sys_number='$data'";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{	

 		echo "$('#cbo_sample').val(".$row[csf("sample")].");\n";
		echo "$('#cbo_item_category').val(".$row[csf("item_category")].");\n"; 		
		echo "$('#hidden_type').val(".$row[csf("piworeq_type")].");\n";
		echo "$('#txt_pi_wo_req_id').val(".$row[csf("pi_wo_req_id")].");\n";
		echo "$('#txt_pi_wo_req').val('".$row[csf("pi_wo_req_number")]."');\n";
		echo "$('#cbo_supplier').val(".$row[csf("supplier_name")].");\n";
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("receive_date")])."');\n";	
		echo "$('#txt_start_hours').val(".$row[csf("time_hour")].");\n";
		echo "$('#txt_start_minuties').val(".$row[csf("time_minute")].");\n";	
		echo "$('#cbo_currency').val(".$row[csf("currency")].");\n";
		echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
 		  	
		//right side list view 
		echo "show_list_view(".$row[csf("piworeq_type")]."+'**'+".$row[csf("pi_wo_req_id")].",'show_product_listview','list_product_container','requires/get_in_entry_controller','');\n";
  	}
	
	exit();	
}


if($action=="show_dtls_list_view")
{
	extract($data); 	
 $sql = "select b.id as id,b.item_description,b.uom,b.quantity,b.rate,b.amount from inv_gate_in_mst a,inv_gate_in_dtls b where a.id=b.mst_id and a.sys_number='$data'"; 
	//echo $sql;
	$arr=array(1=>$unit_of_measurement);
	
 	echo create_list_view("list_view", "Item Description,UOM,Qnty,Rate,Amount","220,90,100,90,150","700","260",0, $sql, "get_php_form_data", "id", "'child_form_input_data','requires/get_in_entry_controller'", 1, "0,uom,0,0,0", $arr, "item_description,uom,quantity,rate,amount", "","",'0,0,2,2,2',"3,quantity,'',amount");	
	exit();
		
} 


if($action=="child_form_input_data")
{
	
	//$data = details table ID 	
	$sql="select id,item_description,uom,quantity,rate,amount,remarks from inv_gate_in_dtls where id=$data"; 
	$result = sql_select($sql);
    
	foreach($result as $row)
	{
		
		echo "$('#txt_item_description').val('".$row[csf("item_description")]."');\n";
		echo "$('#cbo_uom').val(".$row[csf("uom")].");\n";
		echo "$('#txt_quantity').val(".$row[csf("quantity")].");\n";
		echo "$('#txt_rate').val(".$row[csf("rate")].");\n";
		echo "$('#txt_amount').val(".$row[csf("amount")].");\n";		
 		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		
		//update id here
		echo "$('#update_id').val(".$row[csf("id")].");\n";		
		//echo "show_list_view(".$row[csf("wo_po_type")]."+'**'+".$row[csf("wo_pi_no")].",'show_product_listview','list_product_container','requires/yarn_receive_controller','');\n";
		echo "set_button_status(1, permission, 'fnc_getin_entry',1,1);\n";
	}
	exit();
}

if ($action=="get_in_entry_print")
{
	extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from   lib_store_location", "id", "store_name"  );
	$sample_library=return_library_array( "select id, sample_name from  lib_sample", "id", "sample_name"  );
	
	$sql="select id, sys_number, item_category, sample, piworeq_type,pi_wo_req_number, supplier_name, challan_no, receive_date, currency, time_hour, time_minute, store_id from  inv_gate_in_mst where company_name='$data[0]' and sys_number='$data[1]' and status_active=1 and is_deleted=0 ";
	//echo $sql;
	$dataArray=sql_select($sql);
	
?>
<div style="width:930px;" align="center">
 <table width="900" cellspacing="0" align="center" border="0">
        <tr>
            <td colspan="7" align="center" style="font-size:xx-large"><strong><? echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
            <td colspan="7" align="center">
				<?
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <? echo $result['plot_no']; ?> 
						Level No: <? echo $result['level_no']?>
						Road No: <? echo $result['road_no']; ?> 
						Block No: <? echo $result['block_no'];?> 
						City No: <? echo $result['city'];?> 
						Zip Code: <? echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <? echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <? echo $result['email'];?> 
						Website No: <? echo $result['website'];
					}
                ?> 
            </td>
        </tr>
        <tr>
            <td colspan="7" align="center" style="font-size:x-large"><strong><u><? echo $data[2]; ?> Challan</u></strong></td>
        </tr>
        <tr>
            <td width="160"><strong>System ID:</strong></td> <td width="175px"><? echo $dataArray[0][csf('sys_number')]; ?></td>
            <td width="120"><strong>Sample:</strong></td><td width="175px" colspan="2"><? echo $sample_library[$dataArray[0][csf('sample')]]; ?></td>
            <td width="125"><strong>Item Category:</strong></td><td width="175px"><? echo  $item_category[$dataArray[0][csf('item_category')]]; ?></td>
        </tr>
        <tr>
            <td><strong>PI/WO/REQ:</strong></td> <td width="175px"><? echo $dataArray[0][csf('pi_wo_req_number')]; ?></td>
            <td><strong>Supplier:</strong></td><td width="175px" colspan="2"><? echo $supplier_library[$dataArray[0][csf('supplier_name')]]; ?></td>
            <td><strong>Challan No:</strong></td><td width="175px"><? echo $dataArray[0][csf('challan_no')]; ?></td>
        </tr>
        <tr>
            <td><strong>Receive Date:</strong></td><td width="175px"><? echo change_date_format($dataArray[0][csf('receive_date')]); ?></td>
            <td><strong>Time:</strong></td><td width="85px"><? echo $dataArray[0][csf('time_hour')]." HH"; ?></td><td width="85px"><? echo $dataArray[0][csf('time_minute')]." Min"; ?></td>
            <td><strong>Store Name</strong></td><td width="175px"><? echo $store_library[$dataArray[0][csf('store_id')]]; ?></td>
        </tr>
    </table>
     <br>
    <table align="center" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="180" align="center">Item Description</th>
            <th width="50" align="center">UOM</th>
            <th width="80" align="center">Quantity</th>
            <th width="80" align="center">Rate</th> 
            <th width="80" align="center">Amount </th>
            <th width="180" align="center">Remarks</th>
        </thead>
<?
    $i=1;
	$gate_id=$dataArray[0][csf('id')];
	$sql_dtls= " select id, item_description, uom, quantity, rate, amount, remarks from inv_gate_in_dtls where mst_id=$gate_id and status_active=1 and is_deleted=0 ";
	//echo $sql_dtls;
	$sql_result=sql_select($sql_dtls);
	
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			?>
			<tr bgcolor="<? echo $bgcolor; ?>">
                <td><? echo $i; ?></td>
                <td><?  echo $row[csf('item_description')]; ?></td>
                <td align="center"><? echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                <td align="right"><? echo $row[csf('quantity')]; ?></td>
                <td align="right"><? echo number_format($row[csf('rate')],2,'.',''); ?></td>
                <td align="right"><? echo number_format($row[csf('amount')],2,'.',''); ?></td>
                <td><? echo $row[csf('remarks')]; ?></td>
			</tr>
		<?php
        $uom_unit="Kg";
        $uom_gm="Grams";
    $i++;
    }
	?>
    </table>
</div>
<div>
    <?
         echo signature_table(33, $data[0], "900px");
    ?>
</div>
    
    
    <?
	   exit();
}
?>
