<?php
date_default_timezone_set("Asia/Dhaka");
require_once('includes/common.php');
require_once('mailer/class.phpmailer.php');

$user_library=return_library_array("select id,user_full_name from user_passwd where valid=1","id","user_full_name");
$company_library=return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name");
$buyer_library=return_library_array("select id,buyer_name from lib_buyer","id","buyer_name");
//$supplier_library = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
$supplier_library = return_library_array("select id,team_member_name from lib_mkt_team_member_info where status_active=1 and is_deleted=0","id","team_member_name");






	if($db_type==0)
	{
		$tomorrow = date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),1)));
		$day_after_tomorrow = date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),2)));
		$current_date = date("Y-m-d H:i:s",strtotime(add_time(date("Y-m-d",time()),0)));
		$prev_date = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($current_date))); 
	}
	else
	{
		$tomorrow = change_date_format(date("Y-m-d H:i:s",strtotime(add_time(date("Y-m-d",time()),1))),'','',1);
		$day_after_tomorrow = change_date_format(date("Y-m-d H:i:s",strtotime(add_time(date("Y-m-d",time()),2))),'','',1);
		$current_date = change_date_format(date("Y-m-d H:i:s",strtotime(add_time(date("Y-m-d",time()),0))),'','',1);
		$prev_date = change_date_format(date('Y-m-d H:i:s', strtotime('-1 day', strtotime($current_date))),'','',1); 
	}
	
//echo $prev_date="19-May-2015";	


foreach($company_library as $compid=>$compname)/// Order Received
{



ob_start();	
	?>
    
    <table width="1200">
        <tr>
            <td valign="top" align="center">
                <h2> Company Name: <?php echo $compname; ?></h2>
            </td>
        </tr>
        <tr>
            <td valign="top" align="left">
                 <table width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                 	<tr>
                 		<td colspan="14" height="35" align="center"><strong>P.O Qunatity Decrease/Increase Status</strong></td>
                 	</tr>
                    <tr>
                        <td width="30" align="center"><strong>SL</strong></td>
                        <td  width="70" align="center"><strong>Job No.</strong></td>
                        <td width="100" align="center"><strong>Buyer</strong></td>
                        <td width="100" align="center"><strong>Style</strong></td>
                        <td width="80" align="center"><strong>Particulars</strong></td>
                        <td width="80" align="center"><strong>Ord. Status</strong></td>
                        <td width="110" align="center"><strong>PO No</strong></td>
                        <td width="90" align="center"><strong>PO Rcv Date</strong></td>
                        <td width="90" align="center"><strong>Fac. Rcv Date</strong></td>
                        <td width="80" align="center"><strong>Ship Date</strong></td>
                        <td width="90" align="center"><strong>Order Qty</strong></td>
                        <td width="50" align="center"><strong>Rate</strong></td>
                        <td width="110" align="center"><strong>Order Value</strong></td>
                        <td width="60" align="center"><strong>Update By</strong></td>
                    </tr>
                    <?php
                    $i=0;$m =1; 
                   $sql = "select b.id, a.job_no, a.buyer_name,a.updated_by, b.po_number, b.po_quantity, b. 	pub_shipment_date as cur_ship_date, b.unit_price, a.dealing_marchant, c.shipment_date as pre_ship_date, c.previous_po_qty, c.previous_po_qty, a.style_ref_no, b.po_received_date as curr_rcv_date, b.factory_received_date, c.po_no, c.po_received_date as pre_rcv_date, c.shipment_date as pre_ship_date,c.order_status as pre_order_status,c.fac_receive_date as pre_factory_received_date,b.is_confirmed as order_status,c.avg_price,c.update_by as pre_update_by from wo_po_details_master a, wo_po_break_down b, wo_po_update_log c where a.job_no=b.job_no_mst and b.id=c.po_id and a.company_name like '$compid' and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.update_date='$prev_date' order by a.dealing_marchant,a.job_no"; // 
					$result = sql_select( $sql );
					foreach( $result as $row) 
					{
						$i++;
						if(!in_array($row[csf('dealing_marchant')],$new_subacc_code))
						{
							if($m!=1)
							{
							?>
								<tr bgcolor="#FFFFFF" style="font-weight:bold;">
								<td colspan="10" align="right">Group Total</td>
                                <td align="right"><?php echo number_format($sum_qty,0);   ?></td>
								<td align="right">&nbsp;</td>
								<td align="right"><?php echo number_format($sum_val,2);  $sum_qty=0;  $sum_val=0;  ?></td>
                                <td align="right">&nbsp;</td>
							 </tr> 
							 <?php
							}	
						 
							$new_subacc_code[]=$row[csf('dealing_marchant')];
					?>
                            <tr bgcolor="#FCF3FE">
                                        <td colspan="15" width="100%"><strong>Dealing Merchant : <?php echo $supplier_library[$row[csf('dealing_marchant')]];?></strong></td>
                             </tr>
                             <?php  
						}
					?>
                    <tr> 
                        <td align="center" rowspan="3"><?php echo $i; ?></td>
                        <td align="center" rowspan="3"><?php if(!in_array($row[csf('job_no')],$new_job)){ $new_job[]=$row[csf('job_no')]; echo $row[csf('job_no')];} else echo ""; ?></td>
                        <td rowspan="3"><?php if(!in_array($row[csf('buyer_name')],$new_buyer)){ $new_buyer[]=$row[csf('buyer_name')]; echo $buyer_library[$row[csf('buyer_name')]];} else echo ""; ?></td>
                        <td rowspan="3"><?php if(!in_array($row[csf('style_ref_no')],$new_style)){ $new_style[]=$row[csf('style_ref_no')]; echo $row[csf('style_ref_no')];} else echo ""; ?></td>
                        
                        <td>Current</td>
                        <td align="center"><?php echo $order_status[$row[csf('order_status')]]; ?></td>
                        <td><?php echo $row[csf('po_number')]; ?></td>
                        <td align="center"><?php echo change_date_format($row[csf('curr_rcv_date')]); ?></td>
                        <td align="center"><?php echo change_date_format($row[csf('factory_received_date')]); ?></td>
                        <td align="center"><?php echo change_date_format($row[csf('cur_ship_date')]); ?></td>
                        <td align="right"><?php echo number_format($row[csf('po_quantity')],0); ?></td>
                        <td align="right"><?php echo number_format($row[csf('unit_price')],2); ?></td>
                        <td align="right"><?php $current_val=$row[csf('po_quantity')]*$row[csf('unit_price')]; echo number_format($current_val,2); ?></td>
                        <td><?php echo $user_library[$row[csf('updated_by')]]; ?></td>
                    </tr>
                    <tr> 
                        <td>Previous</td>
                        <td align="center"><?php echo $order_status[$row[csf('pre_order_status')]]; ?></td>
                        <td><?php echo $row[csf('po_no')]; ?></td>
                        <td align="center"><?php echo change_date_format($row[csf('pre_rcv_date')]); ?></td>
                        <td align="center"><?php echo change_date_format($row[csf('pre_factory_received_date')]); ?></td>
                        <td align="center"><?php echo change_date_format($row[csf('pre_ship_date')]); ?></td>
                        <td align="right"><?php echo number_format($row[csf('previous_po_qty')],0); ?></td>
                        <td align="right"><?php echo number_format($row[csf('avg_price')],2); ?></td>
                        <td align="right"><?php $prev_val=$row[csf('previous_po_qty')]*$row[csf('avg_price')]; echo number_format($prev_val,2); ?></td>
                        <td><?php echo $user_library[$row[csf('pre_update_by')]]; ?></td>
                    </tr>
                    <tr> 
                        <td><b>Changed By</b></td>
                        <td align="center"><b><?php if($row[csf('order_status')]!=$row[csf('pre_order_status')])echo $order_status[$row[csf('order_status')]]; ?></b></td>
                        <td><b><?php if($row[csf('po_number')]!=$row[csf('po_no')])echo $row[csf('po_number')]; else ""; ?></b></td>
                        <td align="center"><b><?php if(change_date_format($row[csf('curr_rcv_date')])!=change_date_format($row[csf('pre_rcv_date')]))echo change_date_format($row[csf('curr_rcv_date')]); else ""; ?></b></td>
                        <td align="center"><b><?php if(change_date_format($row[csf('factory_received_date')])!=change_date_format($row[csf('pre_factory_received_date')]))echo change_date_format($row[csf('factory_received_date')]); ?></b></td>
                        
                        
                        <td align="center"><b><?php 
						if(change_date_format($row[csf('cur_ship_date')])!=change_date_format($row[csf('pre_ship_date')])) echo change_date_format($row[csf('cur_ship_date')]); else ""; ?></b></td>
                        
                        
                        <td align="right"><b><?php if($row[csf('po_quantity')]!=$row[csf('previous_po_qty')])echo number_format($row[csf('po_quantity')]-$row[csf('previous_po_qty')],0); else ""; ?></b></td>
                       	<td align="right"><b><?php 
						if($row[csf('unit_price')]!=$row[csf('avg_price')])echo number_format($row[csf('unit_price')]-$row[csf('previous_unit_price')],2); else "";
						?></b></td>
                        <td align="right"><b><?php if($current_val!=$prev_val)echo number_format($current_val-$prev_val,2); else ""; ?></b></td>
                        <td><?php if($row[csf('updated_by')]!=$row[csf('pre_update_by')]) echo $user_library[$row[csf('updated_by')]];else echo ""; ?></td>
                    </tr>
                    <?php
						$sum_qty+=$row[csf('po_quantity')]-$row[csf('previous_po_qty')];
						$sum_val+=$current_val-$prev_val;
						
						$grant_sum_qty += $row[csf('po_quantity')]-$row[csf('previous_po_qty')];
                        $grant_sum_val += $current_val-$prev_val;
								 
						if($i==$result_rows)
						{
						?>
							<tr bgcolor="#FFFFFF" style="font-weight:bold;">
							<td colspan="10" align="right">Group Total</td>
                            <td align="right"><?php echo number_format($sum_qty,0);   ?></td>
							<td align="right">&nbsp;</td>
							<td align="right"><?php echo number_format($sum_val,2);?></td>
                            <td align="right">&nbsp;</td>
						 </tr> 
						 <?php
						}
						$m++;
					}
					?>
                    	<tr class="tbl_bottom">
                            <td colspan="10" align="right"><b>Grand Total</b></td>
                            <td align="right"><b><?php echo number_format($grant_sum_qty);?></b></td>
                            <td align="right">&nbsp;</td>
                            <td align="right"><b><?php echo number_format($grant_sum_val,2);?></b></td>
                     	</tr>
                 </table>
            </td>
        </tr>
    </table>
    
<?php
		$sum_qty=0;
		$sum_val=0;
		$grant_sum_qty=0;
		$grant_sum_val=0;

		
	$to="";	
	$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=7 and b.mail_user_setup_id=c.id and a.company_id=$compid";
	$mail_sql=sql_select($sql);
	foreach($mail_sql as $row)
	{
		if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
	}
	
 	$subject = "Daily Revised Order";
	//echo $to; die;
	$message="";
	$message=ob_get_contents();
	ob_clean();
	$header=mail_header();
	
	if($to!="") echo send_mail_mailer( $to, $subject, $message, $from_mail);
//if($to!="") echo $message;
}
	
	




?> 