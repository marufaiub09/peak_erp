-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_roll_details`
--

DROP TABLE IF EXISTS `pro_roll_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_roll_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barcode_year` int(4) NOT NULL DEFAULT '0',
  `barcode_suffix_no` int(11) NOT NULL DEFAULT '0',
  `barcode_no` bigint(12) NOT NULL DEFAULT '0',
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `dtls_id` int(11) NOT NULL DEFAULT '0',
  `po_breakdown_id` int(11) NOT NULL DEFAULT '0',
  `entry_form` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'entry_form  array',
  `roll_no` int(11) NOT NULL DEFAULT '0',
  `roll_id` int(11) NOT NULL DEFAULT '0' COMMENT 'this table id',
  `plies` int(11) NOT NULL DEFAULT '0',
  `roll_used` int(11) NOT NULL DEFAULT '0' COMMENT '0=Roll Not Used; 1=Roll Used',
  `roll_split_from` int(11) NOT NULL DEFAULT '0',
  `qnty` double NOT NULL DEFAULT '0',
  `reject_qnty` double NOT NULL DEFAULT '0',
  `qc_pass_qnty` double NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `dtls_id` (`dtls_id`),
  KEY `po_breakdown_id` (`po_breakdown_id`),
  KEY `form_category` (`entry_form`),
  KEY `roll_no` (`roll_no`),
  KEY `qnty` (`qnty`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_roll_details`
--

LOCK TABLES `pro_roll_details` WRITE;
/*!40000 ALTER TABLE `pro_roll_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_roll_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
