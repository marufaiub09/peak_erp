﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$mrr_date_check="";
if($db_type==2 || $db_type==1 )
{
	$mrr_date_check="and to_char(insert_date,'YYYY')=".date('Y',time())."";
}
else if ($db_type==0)
{
	$mrr_date_check="and year(insert_date)=".date('Y',time())."";
}


//------------------------------------------------------------------------------------------------------
if ($action=="load_drop_down_supplier")
{
	if($data==4) $prty_type=5; else if($data==11) $prty_type=8;
	echo create_drop_down( "cbo_supplier", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b, lib_supplier_tag_company c where a.id=b.supplier_id and a.id=c.supplier_id and b.party_type in($prty_type) order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );     	 
}


if ($action=="load_details_container"){   //chemical & dyes

   	$explodeData = explode("**",$data);
	$woBasis = $explodeData[0];
	$company = $explodeData[1];
	$category = $explodeData[2];
	
	if($category==0) { echo ""; die; }
	
	if($woBasis==2) // independent
	{
		$i=1;
		
		$user_given_code_status=return_field_value("user_given_code_status","variable_settings_inventory","company_name='$company' and item_category_id='$category' and status_active=1 and is_deleted=0");
		$itemAcctDoubleClick="";$itemDescDoubleClick="";
		if($user_given_code_status==1)
			$itemAcctDoubleClick = 'onDblClick="itemDetailsPopup()" placeholder="Double Click To Search"';
		else
			$itemDescDoubleClick = 	'onDblClick="itemDetailsPopup()" placeholder="Double Click To Search"';
 	?>
	
	
			<table cellspacing="0" width="100%" class="rpt_table" id="tbl_details" >
					<thead> 
						<tr>
							<th>Item Account</th>
							<th class="must_entry_caption">Item Description</th>
							<th>Item Size</th>
							<th class="must_entry_caption">Item Group</th>
							<th>Cons UOM</th>
							<th class="must_entry_caption">Quantity</th>
							<th class="must_entry_caption">Rate</th>
							<th class="must_entry_caption">Amount</th>
                            <th>Action</th>
						</tr>    
					</thead>
                   <tbody>
					<tr class="general" id="<?php echo $i;?>">
						<td width="80">
 							<input type="text" name="txt_item_acct_<?php echo $i;?>" id="txt_item_acct_<?php echo $i;?>" class="text_boxes" style="width:100px"  <?php echo $itemAcctDoubleClick; ?> />
                            <input type="hidden" name="txt_item_id_<?php echo $i;?>" id="txt_item_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <input type="hidden" name="txt_req_dtls_id_<?php echo $i;?>" id="txt_req_dtls_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <!-- Only for show. not used for Independent -->
                            <input type="hidden" name="txt_req_no_<?php echo $i;?>" id="txt_req_no_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <input type="hidden" name="txt_req_no_id_<?php echo $i;?>" id="txt_req_no_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <input type="hidden" name="txt_req_qnty_<?php echo $i;?>" id="txt_req_qnty_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" readonly value="" />
                            <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value=""  />
                            <!-- END -->	
 						</td>
						<td width="80">
							<input type="text" name="txt_item_desc_<?php echo $i;?>" id="txt_item_desc_<?php echo $i;?>" class="text_boxes" style="width:100px" <?php echo $itemDescDoubleClick; ?> />
						</td>
						<td width="100">
							<input type="text" name="txt_item_size_<?php echo $i;?>" id="txt_item_size_<?php echo $i;?>" class="text_boxes" style="width:80px"/>
                        </td>
						<td width="50">
                        	<?php
								echo create_drop_down( "cbogroup_".$i, 80, "select id,item_name  from lib_item_group where status_active=1","id,item_name", 1, "Select", 0, "",1 );
							?> 
                        </td>
						<td width="100">
                        	<?php
								echo create_drop_down( "cbouom_".$i, 80, $unit_of_measurement,"", 1, "Select", 0, "",1 );
							?> 
						</td>
 						<td width="50">
							<input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;"/>
						</td>
						<td width="50">
							<input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" />
						</td>
						<td width="80">
							<input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" readonly />
						</td>						
                        <td width="80">
							 <input type="button" id="increaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'increase');" />
                             <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
						</td> 
					</tr>
                </tbody>
			</table>
           <!--<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>*/-->
			
	<?php
		exit();
	}	
	else //requisition container  header
	{
		?>
			<div style="width:1050px;">
                <table cellspacing="0" width="100%" class="rpt_table" id="tbl_details" >
                        <thead> 
                            <tr id="0">
                                <th>Requisition No</th>
                                <th>Item Account</th>
                                <th>Item Description</th>
                                <th>Item Size</th>
                                <th>Item Group</th>
                                <th>Cons UOM</th>
                                <th>Req.Qnty</th>
                                <th>WO.Qnty</th>
                                <th>Rate</th>
                                <th>Amount</th>
                                <th>Action</th> 
                            </tr>      
                        </thead>
                        <tbody>
                        	<!-- append here -->
                        </tbody> 
                </table>
                <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
			</div>	
		<?php
		exit();
	}

}
if ($action=="append_load_details_container"){   //Yarn details append table row
	
 	$explodeData = explode("**",$data);
	$i = $explodeData[0];
	$company = $explodeData[1];
	$category = $explodeData[2];
	
	$user_given_code_status=return_field_value("user_given_code_status","variable_settings_inventory","company_name='$company' and item_category_id='$category' and status_active=1 and is_deleted=0");
	$itemAcctDoubleClick="";$itemDescDoubleClick="";
	if($user_given_code_status==1)
		$itemAcctDoubleClick = 'onDblClick="itemDetailsPopup()" placeholder="Double Click To Search"';
	else
		$itemDescDoubleClick = 	'onDblClick="itemDetailsPopup()" placeholder="Double Click To Search"';
		
	?>
				<tr class="general" id="<?php echo $i;?>">
						<td width="80">
 							<input type="text" name="txt_item_acct_<?php echo $i;?>" id="txt_item_acct_<?php echo $i;?>" class="text_boxes" style="width:100px"  <?php echo $itemAcctDoubleClick; ?> />
                            <input type="hidden" name="txt_item_id_<?php echo $i;?>" id="txt_item_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <input type="hidden" name="txt_req_dtls_id_<?php echo $i;?>" id="txt_req_dtls_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <!-- Only for show. not used for Independent -->
                            <input type="hidden" name="txt_req_no_<?php echo $i;?>" id="txt_req_no_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <input type="hidden" name="txt_req_no_id_<?php echo $i;?>" id="txt_req_no_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                            <input type="hidden" name="txt_req_qnty_<?php echo $i;?>" id="txt_req_qnty_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" readonly value="" />
                            <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value=""  />
                            <!-- END -->	
 						</td>
						<td width="80">
							<input type="text" name="txt_item_desc_<?php echo $i;?>" id="txt_item_desc_<?php echo $i;?>" class="text_boxes" style="width:100px" <?php echo $itemDescDoubleClick; ?> />
						</td>
						<td width="100">
							<input type="text" name="txt_item_size_<?php echo $i;?>" id="txt_item_size_<?php echo $i;?>" class="text_boxes" style="width:80px"/>
                        </td>
						<td width="50">
                        	<?php
								echo create_drop_down( "cbogroup_".$i, 80, "select id,item_name  from lib_item_group where status_active=1","id,item_name", 1, "Select", 0, "",1 );
							?> 
                        </td>
						<td width="100">
                        	<?php
								echo create_drop_down( "cbouom_".$i, 80, $unit_of_measurement,"", 1, "Select", 0, "",1 );
							?> 
						</td>
 						<td width="50">
							<input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;"/>
						</td>
						<td width="50">
							<input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" />
						</td>
						<td width="80">
							<input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" readonly />
						</td>						
                        <td width="80">
							 <input type="button" id="increaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'increase');" />
                             <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
						</td> 
					</tr>

	<?php
	exit();
}




if ($action=="account_order_popup")
{
	echo load_html_head_contents("Item Creation popup", "../../../", 1, 1,'','1','');	
	extract($_REQUEST);
?>

<script>
	
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	 function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ ) {
				 eval($('#tr_'+i).attr("onclick"));  
			}
		}
		
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		//alert (id);
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		str=str[1];
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		
		$('#item_1').val( id );
		
	} 
	
	 
</script>

</head>	
<body>
	<div align="center" style="width:100%" >
		<form name="order_popup_1"  id="order_popup_1">
		<fieldset style="width:900px"> 
            <input type="hidden" id="item_1" />
     <?php
		  		   
		  if($itemIDS!="") $itemIDScond = " and a.id not in ($itemIDS)"; else $itemIDScond = "";
		  $arr=array (1=>$item_category,5=>$unit_of_measurement,9=>$row_status);//5=>$unit_of_measurement,
		  $sql="select a.id, a.item_account, a.item_category_id, a.item_description, a.item_size, a.item_group_id, a.unit_of_measure, a.current_stock, a.re_order_label, a.status_active, b.item_name from product_details_master a, lib_item_group b where a.item_group_id=b.id and a.is_deleted=0 and company_id='$company' and item_category_id='$itemCategory' $itemIDScond";
		  //echo $sql;die;		   
		  echo  create_list_view ( "list_view","Item Account,Item Category,Item Description,Item Size,Item Group,Order UOM,Stock,ReOrder Labale,Product ID,Status", "120,100,140,80,100,80,80,100,50,50","950","250",0, $sql, "js_set_value", "id", "", '', "0,item_category_id,0,0,0,unit_of_measure,0,0,0,status_active", $arr , "item_account,item_category_id,item_description,item_size,item_name,unit_of_measure,current_stock,re_order_label,id,status_active", "", 'setFilterGrid("list_view",-1);','0,0,0,0,0,0,1,1,1,0','',1 ); 
		
    ?>
      	</fieldset>
      </form>
	 </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>                                  
<?php 		

}


if ($action=="load_php_popup_to_form_asd")
{
	$explode_data = explode("**",$data);
	$data_id=$explode_data[0]; 
	//echo $total_row=$explode_data[1];
	$item=$explode_data[1];
	
	$i=$explode_data[2];
	
    if($data_id!="")
	{
		//echo "select a.id,a.item_account,a.item_category_id,a.item_description,a.item_size,a.item_group_id,a.unit_of_measure,a.current_stock,a.re_order_label,a.status_active,b.item_name from product_details_master a,lib_item_group b where a.id in ($data_id) and a.status_active=1 and a.item_group_id=b.id";
		//echo "select a.id,a.item_account,a.item_category_id,a.item_description,a.item_size,a.item_group_id,a.unit_of_measure,a.current_stock,a.re_order_label,a.status_active,b.item_name from product_details_master a,lib_item_group b where a.id in ($data_id) and a.status_active=1 and a.item_group_id=b.id";die;
		$nameArray=sql_select( "select a.id,a.item_account,a.item_category_id,a.item_description,a.item_size,a.item_group_id,a.unit_of_measure,a.current_stock,a.re_order_label,a.status_active,b.item_name from product_details_master a,lib_item_group b where a.id in ($data_id) and a.status_active=1 and a.item_group_id=b.id");
		//var_dump($nameArray);
		
		$user_given_code_status=return_field_value("user_given_code_status","variable_settings_inventory","company_name='$company' and item_category_id='$category' and status_active=1 and is_deleted=0");
		
		foreach ($nameArray as $val)
		{
		?>
    
			<tr class="general" id="<?php echo $i;?>">
                <td width="80">
                    <input type="text" name="txt_item_acct_<?php echo $i;?>" id="txt_item_acct_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("item_account")];?>" />
                    <input type="hidden" name="txt_item_id_<?php echo $i;?>" id="txt_item_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("id")];?>" readonly />
                    <input type="hidden" name="txt_req_dtls_id_<?php echo $i;?>" id="txt_req_dtls_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                     <!-- Only for show. not used for Independent -->
                    <input type="hidden" name="txt_req_no_<?php echo $i;?>" id="txt_req_no_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                    <input type="hidden" name="txt_req_no_id_<?php echo $i;?>" id="txt_req_no_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="" readonly />
                    <input type="hidden" name="txt_req_qnty_<?php echo $i;?>" id="txt_req_qnty_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" readonly value="" />
                    <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value="" />
                    <!----------------- END ------------------------>
                </td>
                <td width="80">
                    <input type="text" name="txt_item_desc_<?php echo $i;?>" id="txt_item_desc_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("item_description")];?>"  readonly />
                </td>
                <td width="100">
                    <input type="text" name="txt_item_size_<?php echo $i;?>" id="txt_item_size_<?php echo $i;?>" class="text_boxes" style="width:80px" value="<?php echo $val[csf("item_size")];?>" readonly />
                </td>
                
                <td width="50">
                    <?php
                        echo create_drop_down( "cbogroup_".$i,80,"select id,item_name  from lib_item_group","id,item_name", 1,"Select",$val[csf("item_group_id")], "",1 );
					   //echo $val[csf("item_group_id")];
                    ?> 
                </td>
                <td width="100">
                    <?php
                        echo create_drop_down( "cbouom_".$i, 80, $unit_of_measurement,"", 1, "Select", $val[csf("unit_of_measure")], "",1 );
                    ?> 
                </td>
                <td width="50">
                    <input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;" />
                </td>
                <td width="50">
                    <input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" />
                </td>
                <td width="80">
                    <input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" readonly />
                </td>                
                <td width="80">
                     <input type="button" id="increaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'increase');" />
                     <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
                </td> 
            </tr>
         <?php
			$i++;	
		}
	}
	exit();
}


// buyer order popoup here
if($action=="requitision_popup")
{
extract($_REQUEST); 
echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode,1);
?>
     
<script>			
	var selected_id = new Array;
	var selected_number = new Array;
	var selected_dtlsID = new Array;
 	function check_all_data() {
		var tbl_row_count = document.getElementById( 'table_body' ).rows.length;
		tbl_row_count = tbl_row_count - 1;
		for( var i = 1; i <= tbl_row_count; i++ ) {
			$('#tr_'+i).trigger('click');
		}
	}
	
	function set_all()
	{
		var old=document.getElementById('txt_row_data').value;
		if(old!="")
		{   
			old=old.split(",");
			for(var i=0; i<old.length; i++)
			{  
				js_set_value( old[i] ) 
			}
		}
	}
	
	function toggle( x, origColor ) {
		var newColor = 'yellow';
		if ( x.style ) { 
			x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
		}
	}
	
	function js_set_value( strParam ) 
	{		
			var splitArr = strParam.split("_");
			var str = splitArr[0];
			var numbers = splitArr[1];
			var ids = splitArr[2]; //requisition id
			var req_dtls_id = splitArr[3];  // item id
			js_set_value_for_item(req_dtls_id);
			 
			toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( ids, selected_id ) == -1 ) {
				selected_id.push( ids );
				selected_number.push( numbers );
  			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
 					if( selected_id[i] == numbers ) break;
				}
				selected_id.splice( i, 1 );
 				selected_number.splice( i, 1 );
  			} 
			
			var num =''; var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {				
				id += selected_id[i] + ',';
				num += selected_number[i] + ',';
 			}
			id = id.substr( 0, id.length - 1 );
			num = num.substr( 0, num.length - 1 );
 			 
 			$('#txt_selected_ids').val( id );
			$('#txt_selected_numbers').val( num );
 	}
	
	
	function js_set_value_for_item( strParam ) 
	{		
			 
			var req_dtls_id = strParam;  // item id  				 
			//------------------item id-------------
			if( jQuery.inArray( req_dtls_id, selected_dtlsID ) == -1 ) {
 				selected_dtlsID.push( req_dtls_id );
  			}
			else {
				for( var i = 0; i < selected_dtlsID.length; i++ ) {
 					if( selected_dtlsID[i] == req_dtls_id ) break;
				}
  				selected_dtlsID.splice( i, 1 );
  			}
			//--------------------------------------			
			var dtls_id = '';
			for( var i = 0; i < selected_dtlsID.length; i++ ) {				
 				dtls_id += selected_dtlsID[i] + ',';
 			}
 			dtls_id = dtls_id.substr( 0, dtls_id.length - 1 );
  			$('#txt_selected_dtls_id').val( dtls_id );
 	}
	
	
	function reset_hidden()
	{
		$('#txt_selected_ids').val('');
		$('#txt_selected_numbers').val('');
		$('#txt_selected_dtls_id').val('');
		
		/*if($("#txt_selected_ids").val()!="") 
		{
			var selectID = $('#txt_selected_ids').val().split(",");
			var selectName = $('#txt_selected_numbers').val().split(",");
 			var selectedDtlsID = $('#txt_selected_dtls_id').val().split(",");
 			for(var i=0;i<selectID.length;i++)
			{
				selected_id.push( selectID[i] );
				selected_number.push( selectName[i] );
   			}
			for(var i=0;i<selectedDtlsID.length;i++)
			{
 				selected_dtlsID.push( selectedDtlsID[i] );
  			}
		}*/
	}
	
    </script>

</head>

<body>
<div align="center" style="width:100%;" >

<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" class="rpt_table" align="center" border="1" rules="all">
    		<tr>
        		<td align="center" width="100%">
            		<table  cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
                   		<thead>
                        	<th width="150">Company Name</th>
                        	<th width="150">Category Name</th>
                        	<th width="200">Date Range</th>
                        	<th width="80"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
                    	</thead>
        				<tr>
                    		<td width="150">  
								<?php 
									echo create_drop_down( "cbo_company_name", 160, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select --", $company, "",1 );  							
								?>
                    		</td>
                            <td width="150">  
								<?php  
									echo create_drop_down( "cbo_item_category", 170, $item_category,"", 1, "-- Select --", $category, "",1,"" ); 							
								?>
                    		</td> 
                    		<td align="center">
                            	<input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px"> To
					  			<input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
                                <input type="hidden" id="txt_selected_ids" name="txt_selected_ids" value="<?php //echo $req_numbers_id; ?>" /> 
                                <input type="hidden" id="txt_selected_numbers" name="txt_selected_numbers" value="<?php //echo $req_numbers; ?>" /> 
                                <input type="hidden" id="txt_selected_dtls_id" name="txt_selected_dtls_id" value="<?php //echo $txt_req_dtls_id; ?>" /> 
                            </td> 
            		 		<td align="center">
                     			<input type="button" name="btn_show" id="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_item_category').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+'<?php echo $txt_req_dtls_id; ?>'+'_'+'<?php echo $garments_nature; ?>', 'create_requisition_search_list_view', 'search_div', 'stationary_work_order_controller', 'setFilterGrid(\'table_body\',-1)');reset_hidden();set_all();" style="width:100px;" />
                            </td>
        				</tr>
             		</table>
          		</td>
        	</tr>
        	<tr>
            	<td  align="center" height="40" valign="middle">
					<?php echo load_month_buttons(1);  ?>
          		</td>
            </tr>
        	<tr>
            	<td align="center" valign="top" id="search_div"></td>
        	</tr>
    </table> 
    </form>
 

</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_requisition_search_list_view")
{
	
 	extract($_REQUEST); 
	$ex_data = explode("_",$data);
	$companyName = $ex_data[0]; 
	$itemCategory = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$req_dtls_id = $ex_data[4];
  	$garments_nature = $ex_data[5]; // not used here
 			
	$sql_cond="";
 	if($companyName!=0)
		$sql_cond = " and a.company_id = '".$companyName."'";
	if($itemCategory!=0)
		$sql_cond .= " and a.item_category_id = '".$itemCategory."'";		
 	if($txt_date_from!="" || $txt_date_to!="") 
		$sql_cond .= " and a.requisition_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'";
  	//if($req_dtls_id!="") $sql_cond .= " and b.id NOT IN ($req_dtls_id)";
 	//print $company; $txt_pay_date=date("j-M-Y",strtotime($txt_pay_date));
	$sql = "select a.id, a.requ_no,a.requ_prefix_num, a.requisition_date, a.company_id, a.location_id, c.item_account, c.item_description, c.item_group_id, c.item_size, b.id as req_dtls_id
			from 
				inv_purchase_requisition_mst a, inv_purchase_requisition_dtls b left join product_details_master c on  b.product_id = c.id
			where
				a.pay_mode in (1,2,3) and			
				a.id=b.mst_id and
 				a.status_active=1 and 
				b.status_active=1 and 
				a.is_deleted=0 						
				$sql_cond
				order by requ_no,requisition_date";  
	//echo $sql;
	$result = sql_select($sql);
 	
	$company=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$location=return_library_array("select id,location_name from lib_location",'id','location_name');
 	$item_name=return_library_array("select id,item_name from lib_item_group",'id','item_name');
	
	?>
    <div style="margin-top:10px">	
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="100">Requisition No</th>
                <th width="90">Requisition Date</th>
                <th width="100">Company</th>
                <th width="100">Location</th>
                <th width="100">Item Account</th>
                <th width="120">Description</th>
                <th width="90">Item Group</th>
                <th>Item Size</th>
            </thead>
         </table> 
         <div style="width:900px; overflow-y:scroll; max-height:200px">  
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="882" class="rpt_table" id="table_body">
                <?php 
                 $i=1; $txt_row_data=""; $hidden_dtls_id=explode(",",$req_dtls_id);
                 $nameArray=sql_select( $sql );
                 foreach ($nameArray as $selectResult)
                 {
                    if ($i%2==0)  
                        $bgcolor="#E9F3FF";
                    else
                        $bgcolor="#FFFFFF";
					
					$data=$i."_".$selectResult[csf('requ_no')]."_".$selectResult[csf('id')]."_".$selectResult[csf('req_dtls_id')];
							
					if(in_array($selectResult[csf('req_dtls_id')],$hidden_dtls_id)) 
					{
						if($txt_row_data=="") $txt_row_data=$data; else $txt_row_data.=",".$data;
					}

            	?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="tr_<?php echo $i;?>" onClick="js_set_value('<?php echo $data; ?>')"> 
                        <td width="40" align="center"><?php echo "$i"; ?></td>	
                        <td width="100" align="center"><p><?php echo $selectResult[csf('requ_prefix_num')]; ?></p></td>
                        <td width="90"><?php echo change_date_format($selectResult[csf('requisition_date')]); ?></td> 
                        <td width="100"><p><?php echo $company[$selectResult[csf('company_id')]]; ?></p></td>
                        <td width="100"><p><?php echo $location[$selectResult[csf('location_id')]]; ?>&nbsp;</p></td>
                        <td width="100"><p><?php echo $selectResult[csf('item_account')]; ?>&nbsp;</p></td>
                        <td width="120"><p><?php echo $selectResult[csf('item_description')]; ?></p></td>	
                        <td width="90"><p><?php echo $item_name[$selectResult[csf('item_group_id')]];; ?></p></td>
                        <td><p><?php echo $selectResult[csf('item_size')]; ?></p></td>
                    </tr>
                <?php
                $i++;
                 }
                 ?>
                 <input type="hidden" name="txt_row_data" id="txt_row_data" value="<?php echo $txt_row_data; ?>"/>	
            </table>
        </div>
        <table width="880" cellspacing="0" cellpadding="0" border="1" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:45%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:55%; float:left" align="left">
                            <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>	
<?php	
	/*$arr=array (2=>$company,3=>$location,6=>$item_name);
	echo  create_list_view("table_body", "Requisition No, Requisition Date, Company, Location, Item Account, Description, Item Group, Item Size", "120,80,100,110,110,100,90,80","900","200", 0, $sql, "js_set_value", "requ_no,id,req_dtls_id", "",1,"0,0,company_id,location_id,0,0,item_group_id,0", $arr, "requ_no,requisition_date,company_id,location_id,item_account,item_description,item_group_id,item_size","stationary_work_order_controller","setFilterGrid('table_body',-1)",'0,3,0,0,0,0,0,0',"",1) ;*/
	
exit();	
}

if($action=="populate_pay_mode_data")
{
	$data=str_replace("'","",$data);
	$sql=sql_select("select id,pay_mode,source from  inv_purchase_requisition_mst where id in($data)");
	foreach($sql as $row)
	{
		echo "$('#cbo_pay_mode').val(".$row[csf("pay_mode")].");\n";
	}
}



if($action=="show_dtls_listview")
{
	extract($_REQUEST); 
	$explodeData = explode("**",$data);
 	$requisition_numberID = $explodeData[0];
	$reqDtlsID = $explodeData[1];
	$rowNo = $explodeData[2]; 
	
	if($reqDtlsID=="") return; // for empty request
	
	$sql=sql_select("select requisition_no,sum(supplier_order_quantity) as order_quantity, item_id from wo_non_order_info_dtls where requisition_no in ($requisition_numberID) group by requisition_no,item_id");
	$requisitionQnty=array();
	foreach($sql as $result)
	{
		$requisitionQnty[$result[csf("requisition_no")]][$result[csf("item_id")]]=$result[csf("order_quantity")];
	}
	
 	$sql = "select a.id as requisition_id,a.requ_no,b.id,c.item_account,c.id as item_id,c.item_description,c.item_size,c.item_group_id,b.cons_uom,b.quantity,b.rate,b.amount
			from 
				inv_purchase_requisition_mst a, inv_purchase_requisition_dtls b, product_details_master c
			where
 				a.status_active=1 and b.status_active=1 and c.status_active=1 and 
				a.is_deleted=0 and b.is_deleted=0 and c.is_deleted=0 and
				a.id=b.mst_id and b.product_id=c.id and				
				b.id in ($reqDtlsID)";				
	//echo $sql;
	$sqlResult = sql_select($sql);
	if( count($sqlResult)==0 ){ echo "No Data Found";die;}
 	 
	$i=$rowNo+1; // row no increse 1
	foreach($sqlResult as $key=>$val)
	{
 		?>		
				<tr class="general" id="<?php echo $i;?>">
						<td width="80">
 							<input type="text" name="txt_req_no_<?php echo $i;?>" id="txt_req_no_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("requ_no")];?>" readonly />
                            <input type="hidden" name="txt_item_id_<?php echo $i;?>" id="txt_item_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("item_id")];?>" readonly />
                            <input type="hidden" name="txt_req_dtls_id_<?php echo $i;?>" id="txt_req_dtls_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("id")];?>" readonly />
                            <input type="hidden" name="txt_req_no_id_<?php echo $i;?>" id="txt_req_no_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("requisition_id")];?>" readonly />
                            <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value="" />
 						</td>
                        <td width="80">
 							<input type="text" name="txt_item_acct_<?php echo $i;?>" id="txt_item_acct_<?php echo $i;?>" class="text_boxes" style="width:100px" readonly value="<?php echo $val[csf("item_account")];?>"  />
 						</td>
						<td width="80">
							<input type="text" name="txt_item_desc_<?php echo $i;?>" id="txt_item_desc_<?php echo $i;?>" class="text_boxes" style="width:100px" readonly value="<?php echo $val[csf("item_description")];?>"  />
						</td>
						<td width="100">
							<input type="text" name="txt_item_size_<?php echo $i;?>" id="txt_item_size_<?php echo $i;?>" class="text_boxes" style="width:80px" readonly value="<?php echo $val[csf("item_size")];?>" />
                        </td>
						<td width="50">
                        	<?php
								echo create_drop_down( "cbogroup_".$i, 80, "select id,item_name  from lib_item_group where status_active=1","id,item_name", 1, "Select", $val[csf("item_group_id")], "",1 );
							?> 
                        </td>
						<td width="100">
                        	<?php
								echo create_drop_down( "cbouom_".$i, 80, $unit_of_measurement,"", 1, "Select", $val[csf("cons_uom")], "",1 );
							?> 
						</td>
                        <td width="50">
                        	<?php $quantityRemaing = $val[csf("quantity")] - $requisitionQnty[$val[csf("requisition_id")]][$val[csf("item_id")]];?>
							<input type="text" name="txt_req_qnty_<?php echo $i;?>" id="txt_req_qnty_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" readonly value="<?php echo $quantityRemaing;?>" />
						</td>
 						<td width="50">
							<input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;" value="<?php echo $quantityRemaing;?>" />	<!-- This is wo qnty here -->
						</td>
						<td width="50">
							<input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" value="<?php echo $val[csf("rate")];?>" />
						</td>
						<td width="80">
							<input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" readonly value="<?php echo $val[csf("rate")]*$quantityRemaing;?>" />
						</td>						
                        <td width="80">
                              <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
						</td> 
					</tr>
                
                
	<?php 
		$i++;
		}
		  
	exit();
}



if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Order Search","../../../", 1, 1, $unicode,1);
	extract($_REQUEST);
	
	$terms_sql = sql_select("select id,terms from lib_terms_condition order by id");
	$terms_name = "";
	foreach( $terms_sql as $result )
	{ 
		$terms_name.= '{value:"'.$result[csf('terms')].'",id:'.$result[csf('id')]."},";
	}
	 
?>
<script>

function termsName(rowID)
{
	$("#termsconditionID_"+rowID).val('');
	 
	$(function() {
		var terms_name = [<?php echo substr($terms_name, 0, -1); ?>]; 
		$("#termscondition_"+rowID).autocomplete({
			source: terms_name,			
			select: function (event, ui) { 
				$("#termscondition_"+rowID).val(ui.item.value); // display the selected text
				$("#termsconditionID_"+rowID).val(ui.item.id); // save selected id to hidden input
 			} 
		});
	});
}

 
function add_break_down_tr(i) 
 {
	var row_num=$('#tbl_termcondi_details tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
	 	
		$("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});  
		  }).end().appendTo("#tbl_termcondi_details"); 
		$("#tbl_termcondi_details tr:last td:first").html(i);    
		$('#termscondition_'+i).removeAttr("onKeyPress").attr("onKeyPress","termsName("+i+");"); 
		$('#termscondition_'+i).removeAttr("onKeyUp").attr("onKeyUp","termsName("+i+");");   
		$('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
		$('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
		$('#termscondition_'+i).val("");
		$('#termsconditionID_'+i).val("");
	}
		  
}

function fn_deletebreak_down_tr(rowNo) 
{   
	
	var numRow = $('table#tbl_termcondi_details tbody tr').length; 
	if(numRow==rowNo && rowNo!=1)
	{
		$('#tbl_termcondi_details tbody tr:last').remove();
	}
	
}

/*function fnc_work_order_terms_condition( operation )
{
		var row_num=$('#tbl_termcondi_details tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{			
			if (form_validation('termscondition_'+i,'Term Condition')==false)
			{
				return;
			}
			
			data_all=data_all+get_submitted_data_string('txt_wo_number*termscondition_'+i+'*termsconditionID_'+i,"");
		}
		var data="action=save_update_delete_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
		//freeze_window(operation);
		http.open("POST","stationary_work_order_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_yarn_order_terms_condition_reponse;
}

function fnc_yarn_order_terms_condition_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
			if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				parent.emailwindow.hide();
			}
	}
}*/

	function fnc_work_order_terms_condition( operation )
		{
			var row_num=$('#tbl_termcondi_details tr').length-1;
			var data_all="";
			for (var i=1; i<=row_num; i++)
			{			
				if (form_validation('termscondition_'+i,'Term Condition')==false)
				{
					return;
				}
				
				data_all=data_all+get_submitted_data_string('txt_wo_number*termscondition_'+i+'*termsconditionID_'+i,"../../../");
			}
			var data="action=save_update_delete_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
			//freeze_window(operation);
			http.open("POST","stationary_work_order_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_yarn_order_terms_condition_reponse;
		}
	
		function fnc_yarn_order_terms_condition_reponse()
		{
			if(http.readyState == 4) 
			{
				//alert(http.responseText);
				var reponse=trim(http.responseText).split('**');
				if (reponse[0].length>2) reponse[0]=10;
				if(reponse[0]==0 || reponse[0]==1)
				{
					parent.emailwindow.hide();
				}
			}
		}

</script>
</head>

<body>
<div align="center" style="width:100%;" >
    <?php echo load_freeze_divs ("../../../",$permission,1); ?>
<fieldset>
       <form id="termscondi_1" autocomplete="off">
            <input type="hidden" id="txt_wo_number" name="txt_wo_number" value="<?php echo str_replace("'","",$update_id) ?>"/>
            <table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
                	<thead>
                    	<tr>
                        	<th width="50">Sl</th><th width="530">Terms</th><th ></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					//echo "select terms_and_condition from wo_non_order_info_mst where id = $update_id";
					$terms_and_conditionID = return_field_value("terms_and_condition","wo_non_order_info_mst","id = $update_id");  
					$flag=0;
					if($terms_and_conditionID=="")
					{
						$condd = " is_default=1"; 
					}
					else
					{ 
						$condd = " id in ($terms_and_conditionID)";
						$flag=1;
					}
					$data_array=sql_select("select id, terms from lib_terms_condition where $condd order by id");
					if( count($data_array)>0 )
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							?>
                            	<tr id="settr_1" align="center">
                                    <td>
                                    <?php echo $i;?>
                                    </td>
                                    <td>
                                    <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row[csf('terms')]; ?>" onKeyPress="termsName(<?php echo $i;?>)" onKeyUp="termsName(<?php echo $i;?>)" /> 
                                    <input type="hidden" id="termsconditionID_<?php echo $i;?>"  name="termsconditionID_<?php echo $i;?>" value="<?php echo $row[csf('id')]; ?>"  readonly />
                                    </td>
                                    <td> 
                                    <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                    <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?>);" />
                                    </td>
                                </tr>
                            <?php
						}
					}
					?>
                </tbody>
                </table>
                
                <table width="650" cellspacing="0" class="" border="0">
                	<tr>
                        <td align="center" height="15" width="100%"> </td>
                    </tr>
                	<tr>
                        <td align="center" width="100%" class="button_container">
						        <?php
									echo load_submit_buttons( $permission, "fnc_work_order_terms_condition", 0,0 ,"reset_form('termscondi_1','','','','')",1) ; 
								?>
                        </td> 
                    </tr>
                </table>
            </form>
        </fieldset>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="save_update_delete_terms_condition")
{
	$process = array( &$_POST );

	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0) mysql_query("BEGIN");
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 
		$terms_sql = sql_select("select id,terms from lib_terms_condition order by id");
		$terms_name = array();
		foreach( $terms_sql as $result )
		{ 
			$terms_name[$result[csf('terms')]] = $result[csf('id')];
		}
		 
		 $id=return_next_id( "id", "lib_terms_condition", 1 );
		 $field_array = "id,terms"; $data_array = "";
		 $idsArr = "";$j=0;
		 for ($i=1;$i<=$total_row;$i++)
		 {
			 $termscondition = "termscondition_".$i;
			 $termscondition = $$termscondition;
			 $termsconditionID = "termsconditionID_".$i;
			 $termsconditionID = $$termsconditionID;
			 if(str_replace("'","",$termsconditionID) == "")
			 {
				 $j++;
				 if ($j!=1){ $data_array .=",";}
				 $data_array .="(".$id.",".$termscondition.")";
				 $idsArr[]=$id;
				 $id=$id+1;				 
			 }
			 else
			 {
				 $idsArr[]=$termsconditionID;
			 }
		 }
		
 		if($data_array!="")
		{
			$CondrID=sql_insert("lib_terms_condition",$field_array,$data_array,0);
		}
		
		foreach($idsArr as &$value){
		   $value = str_replace("'","",$value);
		}
		$idsArr = implode(",", $idsArr);
		$rID = sql_update("wo_non_order_info_mst","terms_and_condition","'$idsArr'","id",$txt_wo_number,1);
		//echo $rID;die;
		//oci_commit($con); oci_rollback($con);			
		check_table_status( $_SESSION['menu_id'],0);		
		if($db_type==0)
		{
			if( $rID && $data_array!="" && $CondrID){
				mysql_query("COMMIT");  
				echo "0**";
			}
			else if($rID && $data_array==""){
				mysql_query("COMMIT");  
				echo "0**";
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if( $rID && $data_array!="" && $CondrID){
				oci_commit($con);  
				echo "0**";
			}
			else if($rID && $data_array==""){
				oci_commit($con);  
				echo "0**";
			}
			else{
				oci_rollback($con);	 
				echo "10**";
			}
		}
		disconnect($con);
		die;
	}	
}







if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if ($operation==0) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		
		//-----------------------------------------------wo_non_order_info_mst table insert START here----------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//
		//echo "select wo_number_prefix,wo_number_prefix_num from wo_non_order_info_mst where company_name=$cbo_company_name and item_category in(11,4) $mrr_date_check order by id desc";die; 
		$id=return_next_id("id", "wo_non_order_info_mst", 1);		
		$new_wo_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', '', date("Y",time()), 5, "select wo_number_prefix,wo_number_prefix_num from wo_non_order_info_mst where company_name=$cbo_company_name and item_category in(11,4) $mrr_date_check order by id desc", "wo_number_prefix", "wo_number_prefix_num" ));
		
		$field_array_mst="id, garments_nature, wo_number_prefix, wo_number_prefix_num, wo_number, company_name, requisition_no,delivery_place, wo_date, supplier_id, attention, wo_basis_id, item_category, currency_id, delivery_date, source, pay_mode, inserted_by, insert_date";
		$data_array_mst="(".$id.",".$garments_nature.",'".$new_wo_number[1]."','".$new_wo_number[2]."','".$new_wo_number[0]."',".$cbo_company_name.",".$txt_req_numbers_id.",".$txt_delivery_place.",".$txt_wo_date.",".$cbo_supplier.",".$txt_attention.",".$cbo_wo_basis.",".$cbo_item_category.",".$cbo_currency.",".$txt_delivery_date.",".$cbo_source.",".$cbo_pay_mode.",'".$user_id."','".$pc_date_time."')";
		//echo $field_array."<br>".$data_array;die;
 		//$rID=sql_insert("wo_non_order_info_mst",$field_array_mst,$data_array_mst,1);
 		//-----------------------------------------------wo_non_order_info_mst table insert END here-------------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//  
		
		 
		 	 	 	
		//-----------------------------------------------wo_non_order_info_dtls table insert START here----------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//  
		$total_row = str_replace("'","",$total_row);
		$field_array="id, mst_id, requisition_dtls_id, requisition_no, item_id, uom, req_quantity, supplier_order_quantity, rate, amount, inserted_by, insert_date";
		$dtlsid=return_next_id("id", "wo_non_order_info_dtls", 1);
		$dtlsid_check=return_next_id("id", "wo_non_order_info_dtls", 1);
		$data_array="";  
		for($i=1;$i<=$total_row;$i++)
		{
			if($i>1) $data_array.=","; 
			$req_no_id	 	= "txt_req_no_id_".$i;
			$req_dtls_id	= "txt_req_dtls_id_".$i;
			$item_id 	 	= "txt_item_id_".$i;
			$item_acct 	 	= "txt_item_acct_".$i;
			$item_desc	 	= "txt_item_desc_".$i;
			$item_size	 	= "txt_item_size_".$i;
			$cbogroup	 	= "cbogroup_".$i;
			$cbouom	 		= "cbouom_".$i;
			$txt_req_qnty   = "txt_req_qnty_".$i; 	//reuisition qnty
			$txt_quantity   = "txt_quantity_".$i;	//work order qnty
			$txt_rate    	= "txt_rate_".$i;
			$txt_amount  	= "txt_amount_".$i;
			
			if( str_replace("'","",$$txt_quantity) != "" )
			{
				$data_array.="(".$dtlsid.",".$id.",".$$req_dtls_id.",".$$req_no_id.",".$$item_id.",".$$cbouom.",".$$txt_req_qnty.",".$$txt_quantity.",".$$txt_rate.",".$$txt_amount.",'".$user_id."','".$pc_date_time."')";
				$dtlsid=$dtlsid+1;
			}
		}	
		//echo $field_array."<br>".$data_array;die;	
		$rID=sql_insert("wo_non_order_info_mst",$field_array_mst,$data_array_mst,1);	
		$dtlsrID=sql_insert("wo_non_order_info_dtls",$field_array,$data_array,1);
		//-----------------------------------------------wo_non_order_info_dtls table insert END here-----------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//
		
		
		//release lock table
		//oci_commit($con); oci_rollback($con);	
		check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_wo_number[0]."**".$id."**".$dtlsid_check;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID)
			{
				oci_commit($con);  
				echo "0**".$new_wo_number[0]."**".$id."**".$dtlsid_check;
			}
			else
			{
				oci_rollback($con); 
				echo "10**";
			}
			//echo $dtlsrID."**".$new_wo_number[0]."**".$id."**".$dtlsid_check;
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();
		$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		
		//-----------------------------------------------wo_non_order_info_mst table UPDATE START here----------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------// 
		$update_check=str_replace("'","",$update_id);
		if($update_check>0)
		{
			$mst_id = return_field_value("id","wo_non_order_info_mst","wo_number=$txt_wo_number");	 
			$field_array_mst="requisition_no*delivery_place*wo_date*supplier_id*attention*wo_basis_id*item_category*currency_id*delivery_date*source*pay_mode*updated_by*update_date";
			$data_array_mst="".$txt_req_numbers_id."*".$txt_delivery_place."*".$txt_wo_date."*".$cbo_supplier."*".$txt_attention."*".$cbo_wo_basis."*".$cbo_item_category."*".$cbo_currency."*".$txt_delivery_date."*".$cbo_source."*".$cbo_pay_mode."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br />".$data_array;die;
			//$rID=sql_update("wo_non_order_info_mst",$field_array_mst,$data_array_mst,"id",$mst_id,0);
		}
 		//-----------------------------------------------wo_non_order_info_mst table UPDATE END here-------------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//  
		
 		 
 		
		//-----------------------------------------------wo_non_order_info_dtls table UPDATE START here----------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//  
		$total_row = str_replace("'","",$total_row);
		$txt_delete_row = str_replace("'","",$txt_delete_row);
		/*if($txt_delete_row!="")
		{ 
			$sql_Delete = execute_query("UPDATE wo_non_order_info_dtls SET status_active=0,is_deleted=1 WHERE id in ($txt_delete_row)",0);
			//$sql_Delete = sql_multirow_update("wo_non_order_info_dtls","status_active*is_deleted","0*1","id",$txt_delete_row,1);
		}*/
		$data_array_insert="";
 		$field_array_insert="id, mst_id, requisition_dtls_id, requisition_no, item_id, uom, req_quantity, supplier_order_quantity, rate, amount, inserted_by, insert_date";
 		$field_array="requisition_dtls_id*requisition_no*item_id*uom*req_quantity*supplier_order_quantity*rate*amount*updated_by*update_date";
		$dtlsid=return_next_id("id", "wo_non_order_info_dtls", 1);
		$dtlsid_check=return_next_id("id", "wo_non_order_info_dtls", 1);
		$data_array=array();
		for($i=1;$i<=$total_row;$i++)
		{
			 
			$req_no_id	 	= "txt_req_no_id_".$i;
			$req_dtls_id	= "txt_req_dtls_id_".$i;
			$item_id 	 	= "txt_item_id_".$i;
			$item_acct 	 	= "txt_item_acct_".$i;
			$item_desc	 	= "txt_item_desc_".$i;
			$item_size	 	= "txt_item_size_".$i;
			$cbogroup	 	= "cbogroup_".$i;
			$cbouom	 		= "cbouom_".$i;
			$txt_req_qnty   = "txt_req_qnty_".$i; 	//reuisition qnty
			$txt_quantity   = "txt_quantity_".$i;	//work order qnty
			$txt_rate    	= "txt_rate_".$i;
			$txt_amount  	= "txt_amount_".$i;
						
			$dtls_ID  		= "txt_row_id_".$i;
			$dtlsID_up 		= str_replace("'","",$$dtls_ID);
			if($dtlsID_up>0) //update
			{
				$update_ID[]=$dtlsID_up; 
				$data_array[$dtlsID_up]=explode("*",("".$$req_dtls_id."*".$$req_no_id."*".$$item_id."*".$$cbouom."*".$$txt_req_qnty."*".$$txt_quantity."*".$$txt_rate."*".$$txt_amount."*'".$user_id."'*'".$pc_date_time."'"));
 			}
			else  // new insert
			{ 
				if( str_replace("'","",$$txt_quantity) != "" )
				{
					//echo $$txt_quantity;die;
					if($data_array_insert!="")$data_array_insert .=",";
					$data_array_insert.="(".$dtlsid.",".$update_id.",".$$req_dtls_id.",".$$req_no_id.",".$$item_id.",".$$cbouom.",".$$txt_req_qnty.",".$$txt_quantity.",".$$txt_rate.",".$$txt_amount.",'".$user_id."','".$pc_date_time."')";
					$dtlsid=$dtlsid+1;
				}
			}	
			 
		} 
		//echo $$txt_quantity;die;

		//echo $field_array_insert."<br>".$data_array_insert;die;
		//print_r($field_array);die;
		$rID=$sql_Delete=$dtlsrIDI=$dtlsrID=true;
		if($update_check>0)
		{
			$rID=sql_update("wo_non_order_info_mst",$field_array_mst,$data_array_mst,"id",$update_check,1);
		}
		if($txt_delete_row!="")
		{ 
			$sql_Delete = execute_query("UPDATE wo_non_order_info_dtls SET status_active=0,is_deleted=1 WHERE id in ($txt_delete_row)",1);
			//$sql_Delete = sql_multirow_update("wo_non_order_info_dtls","status_active*is_deleted","0*1","id",$txt_delete_row,1);
		}
		if($data_array_insert!="")
		{		 
			$dtlsrIDI=sql_insert("wo_non_order_info_dtls",$field_array_insert,$data_array_insert,1); 
		}
		//echo $dtlsrIDI;die;
		 
		if(count($update_ID)>0)
		{
			// bulk_update_sql_statement( $table, $id_column, $update_column, $data_values, $id_count )
			$dtlsrID=execute_query(bulk_update_sql_statement("wo_non_order_info_dtls","id",$field_array,$data_array,$update_ID),1);
		}
		//-----------------------------------------------wo_non_order_info_dtls table UPDATE END here-----------------------------------//
		//-------------------------------------------------------------------------------------------------------------------------------//
		  		
		//release lock table
		//oci_commit($con); oci_rollback($con);	
		check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_wo_number)."**".$update_check."**".$dtlsid_check;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID && $sql_Delete && $dtlsrIDI)
			{
				oci_commit($con); 
				echo "1**".str_replace("'","",$txt_wo_number)."**".$update_check."**".$dtlsid_check;
			}
			else
			{
				oci_rollback($con);
				echo "10**";
			}
			//echo "1**".$txt_wo_number."**".$update_check."**".$dtlsid_check;
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		/*$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		// master table delete here---------------------------------------
		$mst_id = return_field_value("id","wo_non_order_info_mst","wo_number like $txt_wo_number");	
		if($mst_id=="" || $mst_id==0){ echo "15**0"; die;}
 		$rID = sql_update("wo_non_order_info_mst",'status_active*is_deleted','0*1',"id",$mst_id,0);
		$dtlsrID = sql_update("wo_non_order_info_dtls",'status_active*is_deleted','0*1',"mst_id",$mst_id,1);
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_wo_number);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		//oci_commit($con); oci_rollback($con);
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID)
			{
				oci_commit($con);  
				echo "2**".str_replace("'","",$rID);
			}
			else
			{
				oci_rollback($con); 
				echo "10**";
			}
			//echo "2**".$rID;
		}
		disconnect($con);
		die;*/
	}
	
	
}



if($action=="wo_popup")
{
	extract($_REQUEST); 
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
?>
     
	<script>
		
		$(document).ready(function(e) {
            $("#txt_search_common").focus();
        });
		
		function search_populate(str)
		{
			//alert(str); 
			if(str==1) // wo number
			{		
				document.getElementById('search_by_th_up').innerHTML="Enter WO Number";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:140px " class="text_boxes" id="txt_search_common"	value=""  />';		 
			}
			else if(str==2) // supplier
			{
				var supplier_name = '<option value="0">--- Select ---</option>';
				<?php 
				$supplier_arr=return_library_array( "select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) order by supplier_name",'id','supplier_name');
				foreach($supplier_arr as $key=>$val)
				{
					echo "supplier_name += '<option value=\"$key\">".($val)."</option>';";
				} 
				?>
				document.getElementById('search_by_th_up').innerHTML="Select Supplier Name";
				document.getElementById('search_by_td').innerHTML='<select	name="txt_search_common" style="width:150px " class="combo_boxes" id="txt_search_common">'+ supplier_name +'</select>';
			}	
					 																																								
		}
			
	function js_set_value(wo_number)
	{
		$("#hidden_wo_number").val(wo_number);	
		parent.emailwindow.hide();
	}
			
    </script>

</head>

<body>
<div align="center" style="width:100%;" >

<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" class="rpt_table" align="center">
    		<tr>
        		<td align="center" width="100%">
            		<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                   		 <thead>
                         	<th width="100">Item Category</th>
                        	<th width="130">Search By</th>
                        	<th width="150" align="center" id="search_by_th_up">Enter Order Number</th>
                        	<th width="200">WO Date Range</th>
                        	<th width="80"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
                    	</thead>
        				<tr>
                        	<td width="100"> 
                        	<?php
							   	echo create_drop_down( "cboitem_category", 100, $item_category,"", 1, "-- Select --", $itemCategory, "",1);
  							?> 
                        	</td>
                    		<td width="130">  
							<?php 
							$searchby_arr=array(1=>"WO Number",2=>"Supplier");
							echo create_drop_down( "txt_search_by", 130, $searchby_arr,"", 0, "-- Select Sample --", $selected, "search_populate(this.value)",0 );
  							?>
                    		</td>
                   			<td width="150" align="center" id="search_by_td">				
								<input type="text" style="width:140px" class="text_boxes"  name="txt_search_common" id="txt_search_common" onKeyDown="if (event.keyCode == 13) document.getElementById('btn_show').click()" />			
            				</td>
                    		<td align="center">
                            	<input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px"> To
					  			<input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 		</td> 
            		 		<td align="center">
                     			<input type="button" name="btn_show" id="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cboitem_category').value+'_'+document.getElementById('txt_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $garments_nature; ?>, 'create_wo_search_list_view', 'search_div', 'stationary_work_order_controller', 'setFilterGrid(\'list_view\',-1)');$('#selected_id').val('')" style="width:100px;" />
                            </td>
        				</tr>
             		</table>
          		</td>
        	</tr>
        	<tr>
            	<td  align="center" height="40" valign="middle">
					<?php echo load_month_buttons(1);  ?>
                    <input type="hidden" id="hidden_wo_number" name="hidden_wo_number" value="" />
          		</td>
            </tr>
        	<tr>
            <td align="center" valign="top" id="search_div"> 
	
            </td>
        	</tr>
    </table> 
</form>
 

</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_wo_search_list_view")
{
	
 	extract($_REQUEST); 
	$ex_data = explode("_",$data);
	$itemCategory = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$txt_date_from = $ex_data[3];
	$txt_date_to = $ex_data[4];
	$company = $ex_data[5];
 	$garments_nature = $ex_data[6];
	 
			
	$sql_cond="";
	if(trim($itemCategory)!="") $sql_cond .= " and item_category='$itemCategory'";
	if(trim($txt_search_common)!="")
	{
		 
		if(trim($txt_search_by)==1)
			$sql_cond .= " and wo_number like '%".trim($txt_search_common)."'";
		else if(trim($txt_search_by)==2)
			$sql_cond .= " and supplier_id=trim('$txt_search_common')";		
 	}
	//print $company; $txt_pay_date=date("j-M-Y",strtotime($txt_pay_date));
	if($txt_date_from!="" || $txt_date_to!="") $sql_cond .= " and wo_date  between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'";
	if(trim($company)!="") $sql_cond .= " and company_name='$company'";
		
 	$sql = "select id,wo_number_prefix_num, wo_number,company_name,buyer_po,wo_date,supplier_id,attention,wo_basis_id,item_category,currency_id,delivery_date,source,pay_mode
			from 
				wo_non_order_info_mst
			where
				status_active=1 and
				is_deleted=0  				
				$sql_cond order by id"; //and garments_nature=$garments_nature
	//echo $sql;die;
	$result = sql_select($sql);
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
 	
	$arr=array(0=>$company_arr,3=>$pay_mode,4=>$supplier_arr,5=>$wo_basis,6=>$source);
	
	//function create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path, $filter_grid_fnc, $fld_type_arr, $summary_flds, $check_box_all )
	echo  create_list_view("list_view", "Company, WO Number, WO Date, Pay Mode, Supplier, WO Basis, Source", "150,100,100,100,150,100,100","900","250",0, $sql, "js_set_value", "wo_number,id", "", 1, "company_name,0,0,pay_mode,supplier_id,wo_basis_id,source", $arr , "company_name,wo_number_prefix_num,wo_date,pay_mode,supplier_id,wo_basis_id,source", "",'','0,0,0,0,0,0,0,0');
	
	
 	exit();	
}



if($action=="populate_data_from_search_popup")
{
	
	$sql = "select 
				id,requisition_no,delivery_place,company_name,buyer_po,wo_date,supplier_id,attention,wo_basis_id,item_category,currency_id,delivery_date,source,pay_mode,is_approved
			from 
				wo_non_order_info_mst
			where
				 id='$data'";  
	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $resultRow)
	{
		echo "$('#cbo_company_name').val('".$resultRow[csf("company_name")]."');\n";
		echo "$('#cbo_company_name').attr('disabled',true);\n";
		echo "$('#cbo_item_category').val('".$resultRow[csf("item_category")]."');\n";
		echo "$('#cbo_item_category').attr('disabled',true);\n";
		echo "$('#cbo_supplier').val('".$resultRow[csf("supplier_id")]."');\n";
		echo "$('#txt_wo_date').val('".change_date_format($resultRow[csf("wo_date")])."');\n";
		echo "$('#cbo_currency').val('".$resultRow[csf("currency_id")]."');\n";
		echo "$('#cbo_wo_basis').val('".$resultRow[csf("wo_basis_id")]."');\n";
		echo  "fn_disable_enable('".$resultRow[csf("wo_basis_id")]."');\n";
		echo "$('#cbo_wo_basis').attr('disabled',true);\n";
		echo "$('#cbo_pay_mode').val('".$resultRow[csf("pay_mode")]."');\n";
		echo "$('#cbo_source').val('".$resultRow[csf("source")]."');\n";
		echo "$('#txt_delivery_date').val('".change_date_format($resultRow[csf("delivery_date")])."');\n";
		echo "$('#txt_attention').val('".$resultRow[csf("attention")]."');\n";
		echo "$('#txt_req_numbers_id').val('".$resultRow[csf("requisition_no")]."');\n";
		echo "$('#txt_delivery_place').val('".$resultRow[csf("delivery_place")]."');\n";
		
		$requNumber="";$i=0;
		if($resultRow[csf("wo_basis_id")]==1) // requisition basis
		{
			$sqlResult = sql_select("select requ_no from inv_purchase_requisition_mst where id in (".$resultRow[csf("requisition_no")].")");
			//print_r($sqlResult);			
			foreach($sqlResult as $res)
			{
				if( $i>0 ) $requNumber .= ",";
				$requNumber .= $res[csf("requ_no")];
				$i++;
			}
		}
		echo "$('#txt_req_numbers').val('".$requNumber."');\n";		
		if($resultRow[csf("wo_basis_id")]!=1) echo "$('#txt_req_numbers').attr('disabled',true);\n";
		else echo "$('#txt_req_numbers').attr('disabled',false);\n";
		
		$requNumber="";$i=0;
		$sqlResult = sql_select("select requisition_dtls_id from wo_non_order_info_dtls where mst_id = (".$resultRow[csf("id")].")");
 		foreach($sqlResult as $res)
		{
			if( $i>0 ) $requNumber .= ",";
			$requNumber .= $res[csf("requisition_dtls_id")];
			$i++;
		}
		echo "$('#txt_req_dtls_id').val('".$requNumber."');\n";	
		
		
		
		echo "document.getElementById('is_approved').value = '".$resultRow[csf("is_approved")]."';\n";
		
		 if($resultRow[csf("is_approved")]==1)
	  {
		 echo "$('#approved').text('Approved');\n"; 
	  }
	  else
	  {
		 echo "$('#approved').text('');\n";
	  }
	  
	  
	}
	exit();
}
 

if($action=="show_dtls_listview_update")
{
	 
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sql = "select b.id,a.wo_basis_id, b.requisition_dtls_id, b.po_breakdown_id, b.requisition_no, b.item_id,p.item_account, p.item_description, p.item_size, p.item_group_id as item_group, b.req_quantity, b.color_name, b.supplier_order_quantity, b.uom, b.rate, b.amount, c.requ_no
			from 
				product_details_master p, wo_non_order_info_mst a, wo_non_order_info_dtls b left join  inv_purchase_requisition_mst c on b.requisition_no=c.id
			where
				a.id=$data and				
				a.id=b.mst_id and
				b.item_id=p.id and
				b.status_active=1 and
				b.is_deleted=0";				   
	//echo $sql; //b.item_category,
	$result = sql_select($sql);
	$i=1;
	foreach($result as $val)
	{
		if($i==1)
		{
		  ?>
        	<div style="width:1050px;">
				<table cellspacing="0" width="100%" class="rpt_table" id="tbl_details" >
					<thead>
						<tr id="0">
							<?php if($val[csf("wo_basis_id")]==1 ){?>
                            <th>Requisition No</th>
                            <?php } ?>
                            <th>Item Account</th>
                            <th>Item Description</th>
                            <th>Item Size</th>
                            <th>Item Group</th>
                            <th>Cons UOM</th>
                            <?php if($val[csf("wo_basis_id")]==1 ){?>
                            <th>Req.Qnty</th>
                            <?php } ?>
                            
                            <th>WO.Qnty</th>
                            <th>Rate</th>
                            <th>Amount</th>
                            <th>Action</th> 
						</tr>    
					</thead> 
         <?php } ?>        
                    <tr class="general" id="<?php echo $i;?>">
                    	
                        <!---- This is for requisition number selected in WO Basis START ---->
						<?php if($val[csf("wo_basis_id")]==1){
                        	echo "<td width=\"80\">";
                        } ?>
                            <input type="<?php if($val[csf("wo_basis_id")]==1)echo 'text'; else echo 'hidden';?>" name="txt_req_no_<?php echo $i;?>" id="txt_req_no_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("requ_no")];?>" readonly />
                             <input type="hidden" name="txt_item_id_<?php echo $i;?>" id="txt_item_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("item_id")];?>" readonly />
                             <input type="hidden" name="txt_req_dtls_id_<?php echo $i;?>" id="txt_req_dtls_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("requisition_dtls_id")];?>" readonly />
                            <input type="hidden" name="txt_req_no_id_<?php echo $i;?>" id="txt_req_no_id_<?php echo $i;?>" class="text_boxes" style="width:100px" value="<?php echo $val[csf("requisition_no")];?>" readonly />
                            <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value="<?php echo $val[csf("id")]; ?>" />
						<?php if($val[csf("wo_basis_id")]==1){
                        echo "</td>";
                        } ?>
                         <!---- This is for requisition number selected in WO Basis END ---->
                         
  						 
                        <td width="80">
 							<input type="text" name="txt_item_acct_<?php echo $i;?>" id="txt_item_acct_<?php echo $i;?>" class="text_boxes" style="width:100px" readonly value="<?php echo $val[csf("item_account")];?>"  />
 						</td>
						<td width="80">
							<input type="text" name="txt_item_desc_<?php echo $i;?>" id="txt_item_desc_<?php echo $i;?>" class="text_boxes" style="width:100px" readonly value="<?php echo $val[csf("item_description")];?>"  />
						</td>
						<td width="100">
							<input type="text" name="txt_item_size_<?php echo $i;?>" id="txt_item_size_<?php echo $i;?>" class="text_boxes" style="width:80px" readonly value="<?php echo $val[csf("item_size")];?>" />
                        </td>
						<td width="50">
                        	<?php
								echo create_drop_down( "cbogroup_".$i, 80, "select id,item_name  from lib_item_group","id,item_name", 1, "Select", $val[csf("item_group")], "",1 );
							?> 
                        </td>
						<td width="100">
                        	<?php
								echo create_drop_down( "cbouom_".$i, 80, $unit_of_measurement,"", 1, "Select", $val[csf("uom")], "",1 );
							?> 
						</td>
                        
                        <?php if($val[csf("wo_basis_id")]==1){
                        	echo "<td width=\"80\">";
                        } ?>
							<input type="<?php if($val[csf("wo_basis_id")]==1)echo 'text'; else echo 'hidden';?>" name="txt_req_qnty_<?php echo $i;?>" id="txt_req_qnty_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" readonly value="<?php echo $val[csf("req_quantity")];?>" />
                        <?php if($val[csf("wo_basis_id")]==1){
                        	echo "</td>";
                        } ?>	
                        					 
 						<td width="50">
							<input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;" value="<?php echo $val[csf("supplier_order_quantity")];?>" />	<!-- This is wo qnty here -->
						</td>
						<td width="50">
							<input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" value="<?php echo $val[csf("rate")];?>" />
						</td>
						<td width="80">
							<input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" readonly value="<?php echo $val[csf("amount")];?>" />
						</td>						
                        <?php if($val[csf("wo_basis_id")]==1){?>
                         <td width="80">
                              <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
						</td>
                        <?php }else{  ?>
                        <td width="80">
							 <input type="button" id="increaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'increase');" />
                             <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
						</td> 
                        <?php } ?>
                         
					</tr> 
                        
        	<?php
		$i++;
	}
	?>
		</table>
    <?php    
	
exit();
}

?>

<!--Station Work Order Report-->
<?php
 if ($action=="stationary_work_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	echo load_html_head_contents($data[16],"../../", 1, 1, $unicode,'',''); 
	//print_r ($data); die;
	$sql_supplier = sql_select("SELECT id,supplier_name,contact_no,country_id,web_site,email,address_1,address_2,address_3,address_4 FROM  lib_supplier WHERE id = $data[3]");

   foreach($sql_supplier as $supplier_data) 
			{//contact_no 	
				$row_mst[csf('supplier_id')];
				
				if($supplier_data[csf('address_1')]!='')$address_1 = $supplier_data[csf('address_1')].','.' ';else $address_1='';
				if($supplier_data[csf('address_2')]!='')$address_2 = $supplier_data[csf('address_2')].','.' ';else $address_2='';
				if($supplier_data[csf('address_3')]!='')$address_3 = $supplier_data[csf('address_3')].','.' ';else $address_3='';
				if($supplier_data[csf('address_4')]!='')$address_4 = $supplier_data[csf('address_4')].','.' ';else $address_4='';
				if($supplier_data[csf('contact_no')]!='')$contact_no = $supplier_data[csf('contact_no')].','.' ';else $contact_no='';
				if($supplier_data[csf('web_site')]!='')$web_site = $supplier_data[csf('web_site')].','.' ';else $web_site='';
				if($supplier_data[csf('email')]!='')$email = $supplier_data[csf('email')].','.' ';else $email='';
				//if($supplier_data[csf('country_id')]!=0)$country = $supplier_data[csf('country_id')].','.' ';else $country='';
				$country = $supplier_data['country_id'];
				
				$supplier_address = $address_1;
				$supplier_country =$country;
				$supplier_phone =$contact_no;
				$supplier_email = $email;
			}
?>

	<div id="table_row" style="width:930px;">
    <?php
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$location=return_field_value("location_name","lib_location","id=$data[0]" );
	$address=return_field_value("address","lib_location","id=$data[0]");
	$lib_country_arr=return_library_array( "select id,country_name from lib_country","id", "country_name"  );
	//$location=return_field_value("location_name","lib_location","company_id=$data[1]");
$item_name_arr=return_library_array("select id,item_name from lib_item_group", "id","item_name");
//$yarn_count = return_library_array('SELECT id,yarn_count FROM lib_yarn_count','id','yarn_count');
$supplier_name_library = return_library_array('SELECT id,supplier_name FROM lib_supplier','id','supplier_name');
$importer_name_library = return_library_array('SELECT id,company_name FROM lib_company','id','company_name');
//$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );


	 //$sql_mst = sql_select("select id,item_category_id,importer_id,supplier_id,pi_number,pi_date,last_shipment_date,pi_validity_date,currency_id,source,hs_code,internal_file_no ,intendor_name,pi_basis_id,remarks from  com_pi_master_details where id= $pi_mst_id"); 
	$image_location=return_field_value("image_location","common_photo_library","file_type=1 and form_name='company_details' and master_tble_id='$data[0]'","image_location");
	 	$i = 0;
		$total_ammount = 0;
		?>
		
            <table align="center" cellspacing="0" width="900" >
                <tr>
        			<td width="70"><img src="../../<?php echo $image_location; ?>" height="50" width="60"></td>
                    <td colspan="10" style="font-size:xx-large;" align="center"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
                </tr>
                <tr>
                    <td colspan="10" align="center"><strong><?php echo $location.",".$address; ?></strong></td>
                </tr>
                <tr>
                    <td colspan="10" align="center"><strong><?php echo $data[16] ;?></strong></td>
                </tr>
            </table>
                
            <table align="center" cellspacing="0" width="900" >
                <tr>
                    <td width="150" align="left"><strong>To</strong>,&nbsp;<?php echo $data[10]; ?></td>
                    <td width="80"><strong>PO :</strong></td>
                    <td width="100" align="left"><?php echo $data[1]; ?></td>
                    <td width="80" align="left" ><strong>Date :</strong></td>
                    <td width="100" align="left"><?php echo $data[4]; ?></td>
                </tr>
                <tr>
                    <td rowspan="4"><?php echo $supplier_name_library[$data[3]]; echo "<br>"; echo $supplier_address;  echo  $lib_country_arr[$country]; echo "<br>"; echo "Cell :".$supplier_phone; echo "<br>"; echo "Email :".$supplier_email; ?></td>
                    <td width="100"><strong>Delivery Date :</strong></td>
                    <td><?php echo $data[9]; ?></td>
                    <td align="left"><strong>Place of Delivery:</strong></td>
                	<td align="left" ><?php echo $data[14]; ?></td>
                </tr>
                <tr>
                	<td><strong>Currency:</strong></td>
                	<td align="left"><?php echo $currency[$data[5]]; ?></td>
                    <td align="left"><strong>Item Category:</strong></td>
                	<td align="left" ><?php echo $item_category[$data[2]]; ?></td>
                </tr>
                 <tr>
                	<td><strong>Pay Mode:</strong></td>
                	<td align="left" ><?php echo $pay_mode[$data[7]]; ?></td>
                    <td align="left" ><strong>WO Basis:</strong></td>
                	<td align="left" ><?php echo $wo_basis[$data[6]]; ?></td>
                </tr>
                <tr>
                	<td align="right" colspan="8" >&nbsp;</td>
                </tr>
                <tr>
                	<td align="right" colspan="8" >&nbsp;</td>
                </tr>
            </table>
          	<br>
            <table align="center" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
                <thead>
                    <th width="50">SL</th>
                    <th width="150" align="center">Requisition No</th>
                    <th width="80" align="center">Code</th>
                    <th width="180" align="center">Item Name & Description</th>
                    <th width="70" align="center">Item Size</th>
                    <th width="50" align="center">Cons UOM</th>
                    <th width="70" align="center">Req.Qty</th> 
                    <th width="70" align="center">WO.Qty</th>
                    <th width="80" align="center">Rate</th>
                    <th align="center">Amount</th>
                </thead>
				<?php
                
                $wopi_library=return_library_array( "select id,wo_number from  wo_non_order_info_mst", "id","wo_number"  );
                $requisition_library=return_library_array( "select id,requ_no from  inv_purchase_requisition_mst", "id","requ_no"  );
                $item_name_arr=return_library_array( "select id, item_name from lib_item_group",'id','item_name');
                $lib_terms_condition=return_library_array( "select id, terms from lib_terms_condition",'id','terms');
                //$reg_no=explode(',',$data[11]);
                $cond="";
                if($data[1]!="") $cond .= " and a.id='$data[15]'";
                //if($reg_no!="") $cond .= " and b.requisition_no='$reg_no'";
                $i=1;
                
                $sql_result= sql_select("select a.id,a.wo_number,a.currency_id,b.requisition_no,b.req_quantity,b.uom,b.supplier_order_quantity,b.amount,b.rate,d.item_description,d.item_size,d.item_group_id,d.item_account
                from wo_non_order_info_mst a,wo_non_order_info_dtls b,product_details_master d
                where a.id=b.mst_id and b.item_id=d.id and b.status_active=1 and b.is_deleted=0 $cond");
                
                foreach($sql_result as $row)
                {
                if ($i%2==0)  
                $bgcolor="#E9F3FF";
                else
                $bgcolor="#FFFFFF";
                
                $req_quantity=$row[csf('req_quantity')];
                $req_quantity_sum += $req_quantity;
                
                $supplier_order_quantity=$row[csf('supplier_order_quantity')];
                $supplier_order_quantityl_sum += $supplier_order_quantity;
                
                $amount=$row[csf('amount')];
                $total_amount+= $amount;
                ?>
                <tr bgcolor="<?php echo $bgcolor; ?>"> 
                    <td><?php echo $i; ?></td>
                    <td><?php echo $requisition_library[$row[csf('requisition_no')]]; ?></td>
                    <td><?php echo $row[csf('item_account')]; ?></td>
                    <td><?php echo $item_name_arr[$row[csf('item_group_id')]].', '.$row[csf('item_description')]; ?></td>
                    <td><?php echo $row[csf('item_size')]; ?></td>
                    <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                    <td align="right"><?php echo $row[csf('req_quantity')]; ?></td>
                    <td align="right"><?php echo $row[csf('supplier_order_quantity')]; ?></td>
                    <td align="right"><?php echo number_format($row[csf('rate')],4,".",""); ?></td>
                    <td align="right"><?php echo number_format($row[csf('amount')],2,".",""); ?></td>
                    <?php 
                    $carrency_id=$row[csf('currency_id')];
                    if($carrency_id==1){$paysa_sent="Paisa";} else if($carrency_id==2){$paysa_sent="CENTS";}
                    ?>
                    </tr>
                    <?php
                    $i++;
                    }
                    ?>
                <tr> 
                    <td align="right" colspan="6" >Total :</td>
                    <td align="right"><?php echo number_format($req_quantity_sum,0,'',',') ?></td>
                    <td align="right"><?php echo number_format($supplier_order_quantityl_sum,0,'',',') ?></td>
                    <td align="right" colspan="2"><?php echo $word_total_amount=number_format($total_amount, 2, '.', ''); ?></td>
                </tr>
            </table>
 			<br/>  
            <table width="900" align="right">
                <tr>
                <td colspan="10">&nbsp;  </td>
                </tr>
                <tr>
                <td colspan="10"> Amount in words:&nbsp;<?php echo number_to_words($word_total_amount,$currency[$carrency_id],$paysa_sent); ?>  </td>
                </tr>
                <tr>
                <td colspan="10">&nbsp;   </td>
                </tr>
                <tr>
                <td colspan="10">&nbsp;   </td>
                </tr>
            </table>  
            <table  width="900" class="rpt_table" border="0" cellpadding="0" cellspacing="0" align="center">
                <thead>
                    <tr style="border:1px solid black;">
                    <th width="3%" style="border:1px solid black;">Sl</th><th width="97%" style="border:1px solid black;">Terms & Condition/Note</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                //echo "select terms_and_condition from wo_non_order_info_mst where id=$data[15]"; die;
                $data_array=sql_select("select terms_and_condition from wo_non_order_info_mst where id=$data[15]");
                //echo $data_array[0][csf("terms_and_condition")]."jah";die;
                if ($data_array[0][csf("terms_and_condition")]!="")
                {
					$i=0;$k=0;
					foreach( $data_array as $row )
					{
						$term_id=array_unique(explode(",",$row[csf('terms_and_condition')]));
						//print_r($term_id);
						$i++;
						foreach($term_id as $row_term)
						{
							$k++;
							echo "<tr id='settr_1' style='border:1px solid black;'> <td style='border:1px solid black;'>
							$k<td style='border:1px solid black;'> $lib_terms_condition[$row_term]</td><br/> </tr>";
						
						}
					}
                }
                else
                {
					//echo "select id, terms from  lib_terms_condition where is_default=1";die;
					$i=0;
					$data_array=sql_select("select id, terms from  lib_terms_condition where is_default=1");// quotation_id='$data'
					//echo count($data_array)."jahid";
					foreach( $data_array as $row )
					{
						$i++;
						?>
							<tr id="settr_1" align="" style="border:1px solid black;">
								<td style="border:1px solid black;">
								<?php echo $i;?>
								</td>
								<td style="border:1px solid black;">
								<?php echo $row[csf('terms')]; ?>
								</td>
							</tr>
						<?php 
					}
                } 
                ?>
                </tbody>
            </table>
   			<br>
			 <?php
                echo signature_table(55, $data[0], "900px");
             ?>

<?php
}
exit();

?>


 