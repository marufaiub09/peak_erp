<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Woven Finish Fabric Receive
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	07-05-2013
Updated by 		: 	Kausar (Creating Report)	
Update date		: 	14-12-2013	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Woven Fabric Receive Info","../../", 1, 1, $unicode,1,1); 

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];

$(function() {	
	  $("#txt_color").autocomplete({
		 source: str_color
	  });
});


function rcv_basis_reset()
{
	document.getElementById('cbo_receive_basis').value=0;
} 
	
	
// popup for WO/PI----------------------	
function openmypage(page_link,title)
{
	if( form_validation('cbo_company_name*cbo_receive_basis','Company Name*Receive Basis')==false )
	{
		return;
	}
	
 		var company = $("#cbo_company_name").val();
		var receive_basis = $("#cbo_receive_basis").val();
		 
		page_link='requires/woven_finish_fabric_receive_controller.php?action=wopi_popup&company='+company+'&receive_basis='+receive_basis;
 		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=400px, center=1, resize=0, scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var rowID=this.contentDoc.getElementById("hidden_tbl_id").value; // wo/pi table id
			var wopiNumber=this.contentDoc.getElementById("hidden_wopi_number").value; // wo/pi number
			var hidden_is_non_ord_sample=this.contentDoc.getElementById("hidden_is_non_ord_sample").value; // wo/pi number
			if (rowID!="")
			{
 				freeze_window(5);
				$('#txt_wo_pi').val('');
				$('#save_data').val('');
				$('#txt_receive_qty').val('');
				$('#all_po_id').val('');
				$('#distribution_method_id').val('');
				$('#txt_deleted_id').val('');
				
				$('#txt_fabric_description').val('');
				$('#fabric_desc_id').val('');
				$('#txt_rate').val('');
				$('#txt_color').val('');
				$('#txt_width').val('');
				$("#txt_wo_pi").val(wopiNumber);
				$("#txt_wo_pi_id").val(rowID);
				$("#booking_without_order").val(hidden_is_non_ord_sample);
				get_php_form_data(receive_basis+"**"+rowID+"**"+hidden_is_non_ord_sample+"**"+wopiNumber, "populate_data_from_wopi_popup", "requires/woven_finish_fabric_receive_controller" );
				show_list_view(receive_basis+"**"+rowID+"**"+hidden_is_non_ord_sample+"**"+wopiNumber,'show_product_listview','list_product_container','requires/woven_finish_fabric_receive_controller','');
				release_freezing();	 
 			}
		}		
}

// enable disable field for independent
function fn_independent(val)
{
	$('#txt_wo_pi').val('');
	$('#save_data').val('');
	$('#txt_receive_qty').val('');
	$('#all_po_id').val('');
	$('#distribution_method_id').val('');
	$('#txt_deleted_id').val('');
	
	$('#txt_fabric_description').val('');
	$('#fabric_desc_id').val('');
	$('#txt_rate').val('');
	$('#txt_color').val('');
	$('#txt_width').val('');
	$('#txt_wo_pi').val('');
	$('#txt_wo_pi_id').val('');
	$('#txt_bla_order_qty').val('');
	$('#cbo_supplier').val(0);
	
	if(val==1)
	{
		$("#txt_lc_no").attr("disabled",true);
		$('#txt_lc_no').attr('placeholder','Display');
		$("#cbo_currency").attr("disabled",true);
		$("#cbo_source").attr("disabled",true);
		$("#txt_wo_pi").attr("disabled",false);
		$('#txt_wo_pi').removeAttr('placeholder','No Need');
		$('#txt_wo_pi').attr('placeholder','Double Click');
		$('#txt_fabric_description').attr('disabled','disabled');
		$('#txt_color').attr('disabled','disabled');
		$('#cbo_supplier').attr('disabled','disabled');
		$('#txt_rate').attr('disabled','disabled');
	}
	if(val==2)
	{
		$("#txt_lc_no").attr("disabled",true);
		$('#txt_lc_no').removeAttr('placeholder','Display');
		$("#cbo_currency").attr("disabled",true);
		$("#cbo_source").attr("disabled",true);
		$("#txt_wo_pi").attr("disabled",false);
		$('#txt_wo_pi').removeAttr('placeholder','No Need');
		$('#txt_wo_pi').attr('placeholder','Double Click');
		$('#txt_fabric_description').attr('disabled','disabled');
		$('#txt_color').attr('disabled','disabled');
		$('#cbo_supplier').attr('disabled','disabled');
		$('#txt_rate').attr('disabled','disabled');
	}
	
	if(val==4)
	{
		$("#txt_lc_no").attr("disabled",true);
		$('#txt_lc_no').removeAttr('placeholder','Display');
		$("#cbo_currency").attr("disabled",false);
		$('#txt_exchange_rate').removeAttr('disabled','disabled');
		$("#cbo_source").attr("disabled",false);
		$("#txt_wo_pi").attr("disabled",true);
		$('#txt_wo_pi').attr('placeholder','No Need');
		$('#txt_fabric_description').removeAttr('disabled','disabled');
		$('#txt_color').removeAttr('disabled','disabled');
		$('#cbo_supplier').removeAttr('disabled','disabled');
		$('#txt_rate').removeAttr('disabled','disabled');
	}
	if(val==6)
	{
		$("#txt_lc_no").removeAttr("disabled",true);
		$('#txt_lc_no').attr('placeholder','Click');
		$("#cbo_currency").attr("disabled",false);
		$('#txt_exchange_rate').removeAttr('disabled','disabled');
		$("#cbo_source").attr("disabled",false);
		$("#txt_wo_pi").attr("disabled",true);
		$('#txt_wo_pi').attr('placeholder','No Need');
		$('#txt_fabric_description').removeAttr('disabled','disabled');
		$('#txt_color').removeAttr('disabled','disabled');
		$('#cbo_supplier').removeAttr('disabled','disabled');
		$('#txt_rate').removeAttr('disabled','disabled');
	}
}

function set_exchange_rate(currency_id)
{
	if(currency_id==1)
	{
		$('#txt_exchange_rate').val(1);
		$('#txt_exchange_rate').attr('disabled','disabled');
	}
	if(currency_id!=1)
	{
		var response=return_global_ajax_value( currency_id, 'set_exchange_rate', '', 'requires/woven_finish_fabric_receive_controller');
		$('#txt_exchange_rate').val(response);
		$('#txt_exchange_rate').removeAttr('disabled','disabled')
	}
}


// LC pop up script here-----------------------------------
function popuppage_lc()
{
	
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/woven_finish_fabric_receive_controller.php?action=lc_popup&company='+company; 
	var title="Search LC Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px,height=370px,center=1,resize=0,scrolling=0','../ ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];//("search_order_frm"); //Access the form inside the modal window
		var rowID=this.contentDoc.getElementById("hidden_tbl_id").value; // lc table id
		var wopiNumber=this.contentDoc.getElementById("hidden_wopi_number").value; // lc number
		$("#txt_lc_no").val(wopiNumber);
		$("#hidden_lc_id").val(rowID);		  
	}
 	
}


// calculate ILE ---------------------------
function fn_calile()
{
	if( form_validation('cbo_company_name*cbo_source*txt_rate','Company Name*Source*Rate')==false )
	{
		return;
	}
	
	var company=$('#cbo_company_name').val();	
	var source=$('#cbo_source').val();	
	var rate=$('#txt_rate').val();	 
	var responseHtml = return_ajax_request_value(company+'**'+source+'**'+rate, 'show_ile', 'requires/woven_finish_fabric_receive_controller');
	var splitResponse="";
	if(responseHtml!="")
	{
		splitResponse = responseHtml.split("**");
		$("#ile_td").html('ILE% '+splitResponse[0]);
		$("#txt_ile").val(splitResponse[1]);
	}
	else
	{
		$("#ile_td").html('ILE% 0');
		$("#txt_ile").val(0);
	}
	
	//amount and book currency calculate--------------//
	var quantity 		= $("#txt_receive_qty").val();
	var exchangeRate 	= $("#txt_exchange_rate").val();
	var ile_cost 		= $("#txt_ile").val();
	var amount = quantity*1*(rate*1+ile_cost*1); 
	var bookCurrency = (rate*1+ile_cost*1)*exchangeRate*1*quantity*1;
	$("#txt_amount").val(number_format_common(amount,"","",1));
	$("#txt_book_currency").val(number_format_common(bookCurrency,"","",1));
	
}


function fn_room_rack_self_box()
{ 
	if( $("#txt_room").val()*1 > 0 )  
		disable_enable_fields( 'txt_rack', 0, '', '' ); 
	else
	{
		reset_form('','','txt_rack*txt_self*txt_binbox','','','');
		disable_enable_fields( 'txt_rack*txt_self*txt_binbox', 1, '', '' ); 
	}
	if( $("#txt_rack").val()*1 > 0 )  
		disable_enable_fields( 'txt_self', 0, '', '' ); 
	else
	{
		reset_form('','','txt_self*txt_binbox','','','');
		disable_enable_fields( 'txt_self*txt_binbox', 1, '', '' ); 	
	}
	if( $("#txt_self").val()*1 > 0 )  
		disable_enable_fields( 'txt_binbox', 0, '', '' ); 
	else
	{
		reset_form('','','txt_binbox','','','');
		disable_enable_fields( 'txt_binbox', 1, '', '' ); 	
	}
}


function fn_comp_new(val)
{	
	
	if(document.getElementById(val).value=='N') // when new(N) button click
	{											
		load_drop_down( 'requires/woven_finish_fabric_receive_controller', 1, 'load_drop_down_composition', 'composition_td' );		 		
	}
	else // When F button click
	{			
		load_drop_down( 'requires/woven_finish_fabric_receive_controller', 2, 'load_drop_down_composition', 'composition_td' );
	}
		
}



function fn_color_new(val)
{
	if(document.getElementById(val).value=='N') // when new(N) button click
	{											
		document.getElementById('color_td_id').innerHTML=' <input type="text" name="cbo_color" id="cbo_color" class="text_boxes" style="width:100px" /><input type="button" class="formbutton" name="btn_color" id="btn_color" width="15" onClick="fn_color_new(this.id)" value="F" />';	
	}
	else // When F button click
	{		
 		load_drop_down( 'requires/woven_finish_fabric_receive_controller', '', 'load_drop_down_color', 'color_td_id' );
 	}
}


function fnc_woben_finish_fab_receive_entry(operation)
{
	if(operation==4)
	{
		var report_title=$( "div.form_caption" ).html();
		print_report( $('#cbo_company_name').val()+'*'+$('#update_id').val()+'*'+report_title, "gwoven_finish_fabric_receive_print", "requires/woven_finish_fabric_receive_controller" ) 
		return;
	}
	else if(operation==2)
	{
		show_msg('13');
		return;
	}
	else
	{
		
		if( form_validation('cbo_company_name*cbo_receive_basis*txt_receive_date*txt_challan_no*cbo_store_name*cbo_supplier*cbo_currency*txt_exchange_rate*cbo_source*txt_fabric_description*txt_color*txt_batch_lot*txt_receive_qty*txt_rate','Company Name*Receive Basis*Receive Date*Challan No*Store Name*Supplier*Currency*Exchange Rate*Source*Fabric Desc*Color*Batch/Lot*Receive Quantity*Rate')==false )
		{
			return;
		}	
		
		else if( $("#txt_rate").val()=="" || $("#txt_rate").val()==0)
		{
			$("#txt_rate").val('');
			form_validation('txt_rate','Rate');
			return;
		}
		var dataString = "txt_mrr_no*update_id*cbo_company_name*cbo_receive_basis*txt_receive_date*txt_challan_no*cbo_location_name*cbo_store_name*txt_lc_no*hidden_lc_id*cbo_supplier*cbo_currency*txt_exchange_rate*cbo_source*txt_wo_pi*txt_wo_pi_id*booking_without_order*txt_fabric_type*txt_fabric_description*fabric_desc_id*txt_color*txt_width*txt_weight*txt_batch_lot*cbo_uom*txt_receive_qty*txt_rate*txt_ile*txt_amount*txt_book_currency*txt_bla_order_qty*txt_prod_code*txt_room*txt_rack*txt_self*txt_binbox*save_data*update_dtls_id*update_trans_id*previous_prod_id*hidden_receive_qnty*all_po_id*roll_maintained*distribution_method_id*txt_deleted_id";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../../");
		freeze_window(operation);
		http.open("POST","requires/woven_finish_fabric_receive_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_woben_finish_fab_receive_entry_reponse;
	}
}

function fnc_woben_finish_fab_receive_entry_reponse()
{	
	if(http.readyState == 4) 
	{
		//release_freezing();	
		 //alert(http.responseText);return;
		var reponse=trim(http.responseText).split('**');	
		show_msg(trim(reponse[0]));
		
		if((reponse[0]==0 || reponse[0]==1))
		{
			document.getElementById('update_id').value = reponse[1];
			document.getElementById('txt_mrr_no').value = reponse[2];
			$('#cbo_company_name').attr('disabled','disabled');

			show_list_view(reponse[2]+'**'+reponse[1],'show_dtls_list_view','list_container_yarn','requires/woven_finish_fabric_receive_controller','');
			set_button_status(reponse[3], permission, 'fnc_woben_finish_fab_receive_entry',1,1);
			
			reset_form('','','txt_fabric_type*txt_fabric_description*fabric_desc_id*txt_color*txt_width*txt_weight*txt_batch_lot*cbo_uom*txt_receive_qty*txt_rate*txt_ile*txt_amount*txt_book_currency*txt_bla_order_qty*txt_prod_code*txt_room*txt_rack*txt_self*txt_binbox*save_data*update_dtls_id*update_trans_id*previous_prod_id*hidden_receive_qnty*all_po_id','','','');
		}
		
		release_freezing();	
	}
}

function open_mrrpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/woven_finish_fabric_receive_controller.php?action=mrr_popup&company='+company; 
	var title="Search MRR Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=400px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		reset_form('yarn_receive_1','list_product_container','','','','cbo_company_name*roll_maintained');
		var theform=this.contentDoc.forms[0]; 
		var mrrNumber=this.contentDoc.getElementById("hidden_recv_number").value; // mrr number
 		$("#txt_mrr_no").val(mrrNumber);
		// master part call here
		get_php_form_data(mrrNumber, "populate_data_from_data", "requires/woven_finish_fabric_receive_controller");
		
		$("#tbl_master").find('input,select').attr("disabled", true);
		disable_enable_fields( 'txt_mrr_no', 0, "", "" );
		set_button_status(0, permission, 'fnc_woben_finish_fab_receive_entry',1,1);	
 	}
}

function fnResetForm()
{
	$("#tbl_master").find('input').attr("disabled", false);	
	disable_enable_fields( 'cbo_company_name*cbo_receive_basis*cbo_store_name', 0, "", "" );
	set_button_status(0, permission, 'fnResetForm',1);
	//reset_form('yarn_receive_1','list_container_yarn*list_product_container','','','','cbo_uom*cbo_currency*txt_exchange_rate*cbo_color');
}


function openmypage_po()
	{
		var receive_basis=$('#cbo_receive_basis').val();
		var cbo_source=$('#cbo_source').val();
		var booking_no=$('#txt_wo_pi').val();
		var cbo_company_id = $('#cbo_company_name').val();
		var dtls_id = $('#update_id').val();
		var roll_maintained = $('#roll_maintained').val();
		var save_data = $('#save_data').val();
		var all_po_id = $('#all_po_id').val();
		var txt_receive_qnty = $('#txt_receive_qty').val(); 
		var distribution_method = $('#distribution_method_id').val();
		var txt_deleted_id=$('#txt_deleted_id').val();
		
		if(receive_basis==0 )
		{
			alert("Please Select Receive Basis.");
			$('#cbo_receive_basis').focus();
			return false;
		}
		if(cbo_source==0 )
		{
			alert("Please Select Source");
			$('#cbo_receive_basis').focus();
			return false;
		}
			
		if(receive_basis==2 && booking_no=="")
		{
			alert("Please Select Booking No.");
			$('#txt_wo_pi').focus();
			return false;
		}
		if(receive_basis==1 && booking_no=="")
		{
			alert("Please Select PI No.");
			$('#txt_wo_pi').focus();
			return false;
		}
		else if((receive_basis==4 || receive_basis==6) && cbo_company_id==0)
		{
			alert("Please Select Company.");
			$('#cbo_company_id').focus();
			return false;
		}
		
		if(roll_maintained==1) 
		{
			popup_width='800px';
		}
		else
		{
			popup_width='650px';
		}
		
		var title = 'PO Info';	
		var page_link = 'requires/woven_finish_fabric_receive_controller.php?receive_basis='+receive_basis+'&cbo_company_id='+cbo_company_id+'&booking_no='+booking_no+'&dtls_id='+dtls_id+'&all_po_id='+all_po_id+'&roll_maintained='+roll_maintained+'&save_data='+save_data+'&txt_receive_qnty='+txt_receive_qnty+'&prev_distribution_method='+distribution_method+'&txt_deleted_id='+txt_deleted_id+'&action=po_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width='+popup_width+',height=370px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var save_string=this.contentDoc.getElementById("save_string").value;	 //Access form field with id="emailfield"
			var tot_grey_qnty=this.contentDoc.getElementById("tot_grey_qnty").value; //Access form field with id="emailfield"
			var number_of_roll=this.contentDoc.getElementById("number_of_roll").value; //Access form field with id="emailfield"
			var all_po_id=this.contentDoc.getElementById("all_po_id").value; //Access form field with id="emailfield"
			var distribution_method=this.contentDoc.getElementById("distribution_method").value;
			var hide_deleted_id=this.contentDoc.getElementById("hide_deleted_id").value;
			
			$('#save_data').val(save_string);
			$('#txt_receive_qty').val(tot_grey_qnty);
			$('#all_po_id').val(all_po_id);
			$('#distribution_method_id').val(distribution_method);
			
			if(roll_maintained==1)
			{
				$('#txt_roll_no').val(number_of_roll);
				$('#txt_deleted_id').val(hide_deleted_id);
			}
			else
			{
				$('#txt_deleted_id').val('');
			}
			
			fn_calile();
		}
	}
	
	function openmypage_fabricDescription()
	{
		var title = 'Fabric Description Info';	
		var page_link = 'requires/woven_finish_fabric_receive_controller.php?action=fabricDescription_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=370px,center=1,resize=1,scrolling=0','../');
		
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemail=this.contentDoc.getElementById("hidden_desc_id").value;	 //Access form field with id="emailfield"
			var theename=this.contentDoc.getElementById("hidden_desc_no").value; //Access form field with id="emailfield"
			var theegsm=this.contentDoc.getElementById("hidden_gsm").value; //Access form field with id="emailfield"
			
			$('#txt_fabric_description').val(theename);
			$('#fabric_desc_id').val(theemail);
			$('#txt_gsm').val(theegsm);
		}
	}
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../../",$permission);  ?><br />    		 
	<form name="yarn_receive_1" id="yarn_receive_1" autocomplete="off" > 
		<div style="width:880px;">       
			<fieldset style="width:880px; float:left;">
                <legend>Woven Finish Fabric Receive</legend>
                <br />
                <fieldset style="width:880px;">                                       
                    <table width="870" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                        <tr>
                            <td colspan="6" align="center">&nbsp;<b>MRR Number</b>
                                <input type="text" name="txt_mrr_no" id="txt_mrr_no" class="text_boxes" style="width:155px" placeholder="Double Click To Search" onDblClick="open_mrrpopup()" readonly /> <input type="hidden" name="update_id" id="update_id" />
                            </td>
                       </tr>
                       <tr>
                            <td width="130" align="right" class="must_entry_caption">Company Name </td>
                            <td width="170">
                                <?php 
                                 echo create_drop_down( "cbo_company_name", 170, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "rcv_basis_reset();load_drop_down( 'requires/woven_finish_fabric_receive_controller', this.value, 'load_drop_down_supplier', 'supplier' );load_drop_down( 'requires/woven_finish_fabric_receive_controller', this.value, 'load_drop_down_store', 'store_td' );load_drop_down( 'requires/woven_finish_fabric_receive_controller', this.value, 'load_drop_down_location', 'location_td');get_php_form_data(this.value,'roll_maintained','requires/woven_finish_fabric_receive_controller' )" );
                                ?>
                            </td>
                            <td width="94" align="right" class="must_entry_caption"> Receive Basis </td>
                            <td width="160">
                                <?php 
                                    echo create_drop_down( "cbo_receive_basis", 170, $receive_basis_arr,"", 1, "- Select Receive Basis -", $selected, "fn_independent(this.value)","","1,2,4,6" );
                                ?>
                            </td>
                            <td width="80" align="right">WO / PI </td>
                            <td width="140">
                               <input class="text_boxes"  type="text" name="txt_wo_pi" id="txt_wo_pi" onDblClick="openmypage('xx','Order Search')"  placeholder="Double Click" style="width:160px;"  readonly disabled /> 
                               <input type="hidden" id="txt_wo_pi_id" name="txt_wo_pi_id" value="" />
                               <input type="hidden" name="booking_without_order" id="booking_without_order"/>
                           </td>
                       </tr>
                        <tr>
                            <td width="130" align="right" class="must_entry_caption">Receive Date </td>
                            <td width="170">
                               <input type="text" name="txt_receive_date" id="txt_receive_date" class="datepicker" style="width:160px;" placeholder="Select Date" />
                            </td>
                            <td width="94" align="right" class="must_entry_caption"> Challan No </td>
                            <td width="160">
                               <input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:160px" >
                            </td>
                            <td  width="130" align="right" >Location </td>
                            <td width="170" id="location_td">
                               <?php 
                                   echo create_drop_down( "cbo_location_name", 170, $blank_array,"", 1, "-- Select Location --", 0, "" );
                               ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="130" align="right" class="must_entry_caption">Store Name</td>
                            <td width="170" id="store_td">
                                <?php 
                                    echo create_drop_down( "cbo_store_name", 170, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET(1,item_category_id) order by store_name","id,store_name", 1, "-- Select Store --", '', "" );
                                ?>
                            </td>
                            <td width="94" align="right" class="must_entry_caption"> Supplier </td>
                            <td id="supplier" width="160"> 
                                <?php
                                   echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",1 );
                                ?>
                            </td>
                            <td width="130" align="right"> L/C No </td>
                            <td id="lc_no" width="170">
                                <input class="text_boxes"  type="text" name="txt_lc_no" id="txt_lc_no" style="width:160px;" placeholder="Display" onDblClick="popuppage_lc()" readonly disabled  />  
                                <input type="hidden" name="hidden_lc_id" id="hidden_lc_id" />
                            </td>
                        </tr>
                        <tr>
                            <td width="130" align="right" class="must_entry_caption">Currency</td>
                            <td width="170" id="currency"> 
                                <?php
                                   echo create_drop_down( "cbo_currency", 170, $currency,"", 1, "-- Select Currency --", '', "set_exchange_rate(this.value)",1 );
                                ?>
                            </td>
                            <td  width="130" align="right" class="must_entry_caption">Exchange Rate</td>
                            <td width="170">
                                <input type="text" name="txt_exchange_rate" id="txt_exchange_rate" class="text_boxes_numeric" style="width:160px" value="" onBlur="fn_calile()"/>	
                            </td>
                            <td width="94" align="right" class="must_entry_caption">Source</td>
                            <td width="160" id="sources">  
                                <?php
                                    echo create_drop_down( "cbo_source", 170, $source,"", 1, "-- Select --", $selected, "",1 );
                                ?>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <br />
                <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                    <tr>
                        <td width="49%" valign="top">
                        <fieldset style="width:880px;">  
                        <legend>New Receive Item</legend>                                     
                        <table width="220" cellspacing="2" cellpadding="0" border="0" style="float:left">
                            <tr>    
                                <td align="right">Fabric Type</td>
                                <td>         
                                    <input type="text" name="txt_fabric_type" id="txt_fabric_type" class="text_boxes" style="width:120px;" maxlength="20" title="Maximum 20 Character"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="must_entry_caption">Fabric Desc </td>
                                <td colspan="3">
                                    <input type="text" name="txt_fabric_description" id="txt_fabric_description" class="text_boxes" style="width:120px; height:50px" onDblClick="openmypage_fabricDescription()" placeholder="Double Click To Search"  readonly/>
                                    <input type="hidden" name="fabric_desc_id" id="fabric_desc_id" class="text_boxes" style="width:397px">
                                </td>
                            </tr>
                            <tr>   
                                <td align="right" class="must_entry_caption">Color</td>
                                <td id="color_td_id">
                                    <input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:120px;" maxlength="20" title="Maximum 20 Character" disabled />
                                </td>
                            </tr> 
                            <tr>    
                                <td align="right">Width</td>
                                <td><input type="text" name="txt_width" id="txt_width" class="text_boxes" style="width:120px;" /></td>
                            </tr>   
                        </table>
                        <table width="220" cellspacing="2" cellpadding="0" border="0" style="float:left">
                            <tr>    
                                <td align="right">Weight</td>
                                <td><input type="text" name="txt_weight" id="txt_weight" class="text_boxes_numeric" style="width:120px;" /></td>
                            </tr>
                            <tr>
                                <td width="110" align="right" class="must_entry_caption">Batch/Lot</td>
                                <td width="130"><input type="text" name="txt_batch_lot" id="txt_batch_lot" class="text_boxes" style="width:120px;" /></td> 
                            </tr>
                            <tr>                 
                                <td width="140" align="right">UOM</td>
                                <td width="140">
                                    <?php
                                        echo create_drop_down( "cbo_uom", 130, $unit_of_measurement,"", 0, "--Select--", 27, "",1,27 );
                                    ?>
                                </td>
                            </tr>
                            <tr>    
                                <td align="right" class="must_entry_caption">Recv. Qnty.</td>   
                                <td>
                                    <input name="txt_receive_qty" id="txt_receive_qty"  class="text_boxes_numeric" type="text" style="width:120px;" onClick="openmypage_po()" placeholder="Single Click" onBlur="fn_calile()" readonly />
                                </td> 
                            </tr>
                            <tr>    
                                <td align="right" class="must_entry_caption">Rate</td>   
                                <td >
                                    <input name="txt_rate" id="txt_rate" class="text_boxes_numeric" type="text" style="width:120px;" onBlur="fn_calile()" value="0" disabled />
                                </td>
                            </tr>
                        </table>
                        <table width="240" cellspacing="2" cellpadding="0" border="0" style="float:left">
                            <tr>   
                                <td align="right" id="ile_td">ILE%</td>   
                                <td >
                                    <input name="txt_ile" id="txt_ile" class="text_boxes_numeric" type="text" style="width:120px;" placeholder="ILE COST" readonly />
                                </td>
                            </tr>  
                            <tr> 
                                <td align="right">Amount</td>
                                <td><input type="text" name="txt_amount" id="txt_amount" class="text_boxes_numeric" style="width:120px;" readonly disabled /></td>
                            </tr>
                            <tr> 
                                <td align="right">Book Currency.</td>
                                <td>
                                    <input type="text" name="txt_book_currency" id="txt_book_currency" class="text_boxes_numeric" style="width:120px;" readonly disabled />
                                </td>
                            </tr>
                            <tr> 
                                <td align="right">Balance PI/ WO </td>
                                <td><input class="text_boxes_numeric"  name="txt_bla_order_qty" id="txt_bla_order_qty" type="text" style="width:120px;" readonly /></td>
                            </tr>
                            <tr>                 
                                <td width="100" align="right">Product ID</td>
                                <td width="100"><input class="text_boxes"  name="txt_prod_code" id="txt_prod_code" type="text" style="width:120px;" readonly  /></td>
                            </tr>
                        </table>
                        <table width="200" cellspacing="2" cellpadding="0" border="0">
                            <tr> 
                                <td align="right">Room</td>
                                <td><input class="text_boxes_numeric"  name="txt_room" id="txt_room" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" /></td>
                            </tr>
                            <tr> 
                                <td align="right">Rack</td>
                                <td><input class="text_boxes_numeric"  name="txt_rack" id="txt_rack" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" disabled /></td>
                            </tr>
                            <tr> 
                                <td align="right">Self</td>
                                <td><input class="text_boxes_numeric"  name="txt_self" id="txt_self" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" disabled /></td>
                            </tr>
                            <tr> 
                                <td align="right">Bin/Box</td>
                                <td><input class="text_boxes_numeric"  name="txt_binbox" id="txt_binbox" type="text" style="width:100px;" disabled /></td>
                            </tr> 
                        </table>
                        </fieldset>
                        </td>
                    </tr>
                </table>                
                <table cellpadding="0" cellspacing="1" width="100%">
                    <tr> 
                       <td colspan="6" align="center"></td>				
                    </tr>
                    <tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <input type="hidden" name="save_data" id="save_data" readonly>
                             <input type="hidden" name="update_dtls_id" id="update_dtls_id" readonly>
                             <input type="hidden" name="update_trans_id" id="update_trans_id" readonly>
                             <input type="hidden" name="previous_prod_id" id="previous_prod_id" readonly>
                             <input type="hidden" name="hidden_receive_qnty" id="hidden_receive_qnty" readonly>
                             <input type="hidden" name="all_po_id" id="all_po_id" readonly>
                             <input type="hidden" name="roll_maintained" id="roll_maintained" readonly>
                             <input type="hidden" name="distribution_method_id" id="distribution_method_id" readonly />
                             <input type="hidden" name="txt_deleted_id" id="txt_deleted_id" readonly />
                             <?php echo load_submit_buttons( $permission, "fnc_woben_finish_fab_receive_entry", 0,1,"fnResetForm()",1);?>
                        </td>
                   </tr> 
                </table>
                <div style="width:870px;" id="list_container_yarn"></div>                 
            </fieldset>
        </div>
        <div id="list_product_container" style="max-height:500px; width:360px; overflow:auto; float:left; margin-left:5px; margin-top:5px; position:relative;"></div>  
	</form>
</div>    
</body>  
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
