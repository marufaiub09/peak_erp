<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Import CI Statement Report
				
Functionality	:	
JS Functions	:
Created by		:	Kausar
Creation date 	: 	22-12-2013
Updated by 		: 		
Update date		: 	Jahid	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Import CI Statement Report","../../", 1, 1, $unicode,1,1); 
?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

	var tableFilters = 
	{
		col_40: "none",
		col_operation: {
		id: ["value_tot_lc_value","value_tot_bill_value","value_gt_total_paid","value_total_out_standing","total_qnty","total_receive"],
		col: [12,17,18,19,34,38],
		operation: ["sum","sum","sum","sum","sum","sum"],
		write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
	} 

	function openmypage_pi_date(import_invoice_id,suppl_id,item_id,pi_id,curr_id,action,title)
	{
		var popup_width="";
		if(action=="pi_details") popup_width="900px"; else popup_width="850px";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/import_ci_statement_report_controller.php?import_invoice_id='+import_invoice_id+'&suppl_id='+suppl_id+'&item_id='+item_id+'&pi_id='+pi_id+'&curr_id='+curr_id+'&action='+action, title, 'width='+popup_width+',height=390px,center=1,resize=0,scrolling=0','../');
	}
	
	function openmypage_inHouse_date(pi_id,receive_value,receive_qnty,action,title)
	{
		var popup_width="";
		if(action=="pi_rec_details") popup_width="620px"; else popup_width="620px";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/import_ci_statement_report_controller.php?pi_id='+pi_id+'&receive_value='+receive_value+'&receive_qnty='+receive_qnty+'&action='+action, title, 'width='+popup_width+',height=390px,center=1,resize=0,scrolling=0','../');
	}
		
	
		
	function generate_report(operation)
	{
		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
			var report_title=$( "div.form_caption" ).html();
			var data="action=report_generate"+get_submitted_data_string('cbo_company_id*cbo_issue_banking*cbo_supplier_id*cbo_lc_type_id*cbo_currency_id*cbo_item_category_id*txt_date_from_c*txt_date_to_c*txt_date_from_b*txt_date_to_b*txt_date_from*txt_date_to*txt_date_from_p*txt_date_to_p*pending_type*txt_pending_date',"../../")+'&report_title='+report_title;
			freeze_window(3);
			http.open("POST","requires/import_ci_statement_report_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
	
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			var tot_rows=reponse[2];
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
			append_report_checkbox('table_header_1',1);
			setFilterGrid("tbl_body",-1,tableFilters);
	 		show_msg('3');
			release_freezing();
		}
	}
	
	
	
	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}
	
	function paid_amount_dtls(invoice_id,action,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/import_ci_statement_report_controller.php?invoice_id='+invoice_id+'&action='+action, title, 'width=620px,height=390px,center=1,resize=0,scrolling=0','../');
	}

</script>
</head>
<body onLoad="set_hotkey();">
    <div style="width:100%;" align="center">
    <?php echo load_freeze_divs ("../../","");  ?><br />    		 
        <form name="importcistatement_1" id="importcistatement_1" autocomplete="off" > 
         <h3 style="width:1320px; margin-top:10px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3> 
         <div id="content_search_panel" style="width:1340px" >      
            <fieldset>  
                <table class="rpt_table" width="1330" cellpadding="0" cellspacing="0">
                    <thead>
                        <th width="90" class="must_entry_caption">Company</th>
                        <th width="90">Issue Banking</th>
                        <th width="90">Supplier</th>
                        <th width="65">LC Type</th>
                        <th width="65">Currency</th>
                        <th width="90">Item Category</th>
                        <th width="160" >Company Accep. Date</th>
                        <th width="160" >Bank Accep. Date</th>
                        <th width="160" >Maturity Date</th>
                        <th width="160" >Paid Date</th>
                        <th width="65">Pending Type</th>
                       	<th width="70" >Pending As On</th>
                        <th ><input type="reset" name="res" id="res" value="Reset" style="width:50px" class="formbutton" onClick="reset_form('importcistatement_1','report_container*report_container2','','','')" /></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php 
                                    echo create_drop_down( "cbo_company_id", 90, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down( 'requires/import_ci_statement_report_controller', this.value, 'load_drop_down_supplier', 'supplier_td' );" );
                                ?>                            
                           </td>
                            <td>
                                <?php 
                                    echo create_drop_down( "cbo_issue_banking", 90, "select id,bank_name from  lib_bank where issusing_bank=1 and status_active=1 and is_deleted=0  order by bank_name","id,bank_name", 1, "--Select Bank--", $selected, "" );
                                ?>                            
                           </td>
                           <td id="supplier_td">
								<?php
									echo create_drop_down( "cbo_supplier_id", 90,$blank_array,"", 1, "-- Select Supplier--", $selected, "","","","","","");
                                ?> 
                          </td>
                          <td>
                            	<?php
                            		echo create_drop_down( "cbo_lc_type_id",65,$lc_type,'',1,'--Select LC Type--',0,"",0); 
                            	?>  
	                       </td>
                           <td>
								<?php
                                    echo create_drop_down( "cbo_currency_id", 65, $currency,"", 1, "--Select--", 0, "",0 );
                                ?>
                           </td>
                           <td>
								<?php
									echo create_drop_down( "cbo_item_category_id", 90,$item_category,"", 1, "-- Select Category--", $selected, "","","","","","");
                                ?> 
                          </td>
                          <td>
                                <input type="date" name="txt_date_from_c" id="txt_date_from_c" class="datepicker" style="width:55px;"/>                    							
                                To
                                <input type="date" name="txt_date_to_c" id="txt_date_to_c" class="datepicker" style="width:55px;"/>                        
                          </td>
                          <td>
                                <input type="date" name="txt_date_from_b" id="txt_date_from_b" class="datepicker" style="width:55px;"/>                    							
                                To
                                <input type="date" name="txt_date_to_b" id="txt_date_to_b" class="datepicker" style="width:55px;"/>                        
                          </td>
                          <td>
                                <input type="date" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:55px;"/>                    							
                                To
                                <input type="date" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:55px;"/>                        
                          </td>
                          <td>
                                <input type="date" name="txt_date_from_p" id="txt_date_from_p" class="datepicker" style="width:55px;"/>                    							
                                To
                                <input type="date" name="txt_date_to_p" id="txt_date_to_p" class="datepicker" style="width:55px;"/>                        
                          </td>
                          <td>
                            	<?php
								$ourstanding_array=array(1=>"All",2=>"Immaturity",3=>"Maturity");
                            		echo create_drop_down( "pending_type",65,$ourstanding_array,'',1,'Select',0,"",0); 
                            	?>  
	                       </td>
                          <td>
                                <input type="date" name="txt_pending_date" id="txt_pending_date" class="datepicker" style="width:60px;"/>                    							
                          </td>
                          <td>
                                <input type="button" name="search" id="search" value="Show" onClick="generate_report(3)" style="width:50px" class="formbutton" />
                          </td>
                      </tr>
                  </tbody>
                  <tfoot>
                      <tr>
                           <td colspan="9" align="center"><?php echo load_month_buttons(1);  ?></td>
                      </tr>
                    </tfoot>
                </table> 
            </fieldset>
        </div>
    <div style="margin-top:10px" id="report_container"></div>
    <div id="report_container2"></div>
 </form> 
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
