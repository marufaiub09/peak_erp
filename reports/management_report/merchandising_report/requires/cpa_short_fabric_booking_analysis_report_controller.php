<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$_SESSION['page_permission']=$permission;

if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$buyer_short_name_library=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$item_library=return_library_array( "select id, item_name from lib_item_group", "id", "item_name"  );
$costing_library=return_library_array( "select job_no, costing_date from wo_pre_cost_mst", "job_no", "costing_date"  );
$department_name_library=return_library_array( "select id,department_name from lib_department", "id", "department_name"  );
$order_arr=return_library_array( "select id, po_number from wo_po_break_down", "id", "po_number"  );
$color_library=return_library_array( "select id, color_name from  lib_color", "id", "color_name"  );

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 160, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );     	 
	exit();
}
if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<script>
		var selected_id = new Array; var selected_name = new Array;
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function js_set_value( str ) {
			if (str!="") str=str.split("_");
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			$('#hide_job_id').val( id );
			$('#hide_job_no').val( name );
		}
    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Job No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:100px;" onClick="reset_form('styleRef_form','search_div','','','','');">										 						<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                   		<input type="hidden" name="hide_job_no" id="hide_job_no" value="" /> 
                    </th> 
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+'<?php echo $cbo_year_id; ?>'+'**'+'<?php echo $cbo_month_id; ?>', 'create_job_no_search_list_view', 'search_div', 'cpa_short_fabric_booking_analysis_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}
if($action=="create_job_no_search_list_view")
				{
					$data=explode('**',$data);
					$company_id=$data[0];
					$year_id=$data[4];
					$month_id=$data[5];
					//echo $month_id;
					$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
					$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
					if($data[1]==0)
					{
						if ($_SESSION['logic_erp']["data_level_secured"]==1)
						{
							if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
						}
						else
						{
							$buyer_id_cond="";
						}
					}
					else
					{
						$buyer_id_cond=" and buyer_name=$data[1]";
					}
					$search_by=$data[2];
					$search_string="%".trim($data[3])."%";
					if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
					//$year="year(insert_date)";
					if($db_type==0) $year_field="YEAR(insert_date) as year"; 
					else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year";
					else $year_field="";
					if($db_type==0)
						{
					if($year_id!=0) $year_cond=" and year(insert_date)=$year_id"; else $year_cond="";	
						}
					else if($db_type==2)
						{
					$year_field_con=" and to_char(insert_date,'YYYY')";
					if($year_id!=0) $year_cond="$year_field_con=$year_id"; else $year_cond="";	
						}
					$arr=array (0=>$company_arr,1=>$buyer_arr);
					$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no, $year_field from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $year_cond  order by job_no";
					echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","600","240",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0,0','',1) ;
				   exit(); 
				} // Job Search end
if ($action=="booking_no_popup")
	{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
?>	
	<script>
	function js_set_value(booking_no)
	{
		document.getElementById('selected_booking').value=booking_no;
		//alert(booking_no);
		parent.emailwindow.hide();
	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="750" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
    	<tr>
        	<td align="center" width="100%">
            	<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="150">Company Name</th><th width="150">Buyer Name</th><th width="200">Date Range</th><th></th>           
                    </thead>
        			<tr>
                    	<td> <input type="hidden" id="selected_booking">
                       	 		<input type="hidden" id="job_no" value="<?php echo $data[2];?>">
							<?php 
								echo create_drop_down( "cbo_company_mst", 150, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", '', "load_drop_down( 'cpa_short_fabric_booking_analysis_report_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );");
							?>
                        </td>
                   	<td id="buyer_td">
                     <?php 
						echo create_drop_down( "cbo_buyer_name", 172, $blank_array,"", 1, "-- Select Buyer --" );
					?>	</td>
                    <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">
					  <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_mst').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('job_no').value, 'create_booking_search_list_view', 'search_div', 'cpa_short_fabric_booking_analysis_report_controller','setFilterGrid(\'list_view\',-1)')" style="width:100px;" /></td>
        		</tr>
             </table>
          </td>
        </tr>
        <tr>
            <td  align="center" height="40" valign="middle">
            <?php 
			echo create_drop_down( "cbo_year_selection", 70, $year,"", 1, "-- Select --", date('Y'), "",0 );		
			?>
			<?php echo load_month_buttons();  ?>
            </td>
            </tr>
        <tr>
            <td align="center"valign="top" id="search_div"> 
            </td>
        </tr>
    </table>    
    </form>
   </div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
	}
	if ($action=="create_booking_search_list_view")
	{
		$data=explode('_',$data);
		if ($data[0]!=0) $company="  company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
		if ($data[1]!=0) $buyer=" and buyer_id='$data[1]'"; else { echo "Please Select Buyer First."; die; }
		if ($data[4]!=0) $job_no=" and job_no='$data[4]'"; else $job_no='';
		if($db_type==0)
		{
		if ($data[2]!="" &&  $data[3]!="") $booking_date  = "and booking_date  between '".change_date_format($data[2], "yyyy-mm-dd", "-")."' and '".change_date_format($data[3], "yyyy-mm-dd", "-")."'"; else $booking_date ="";
		}
		if($db_type==2)
		{
		if ($data[2]!="" &&  $data[3]!="") $booking_date  = "and booking_date  between '".change_date_format($data[2], "yyyy-mm-dd", "-",1)."' and '".change_date_format($data[3], "yyyy-mm-dd", "-",1)."'"; else $booking_date ="";
		}
		$po_array=array();
		$sql_po= sql_select("select booking_no,po_break_down_id from wo_booking_mst  where $company $buyer $booking_date and booking_type=1 and is_short=2 and   status_active=1  and 	is_deleted=0 order by booking_no");
		foreach($sql_po as $row)
		{
			 $po_id=explode(",",$row[csf("po_break_down_id")]);
			//print_r( $po_id);
			$po_number_string="";
			foreach($po_id as $key=> $value )
			{
				$po_number_string.=$order_arr[$value].",";
			}
			$po_array[$row[csf("po_break_down_id")]]=rtrim($po_number_string,",");
		} //echo $po_array[$row[csf("po_break_down_id")]];
		 $approved=array(0=>"No",1=>"Yes");
		 $is_ready=array(0=>"No",1=>"Yes",2=>"No");
		$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
		$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
		$suplier=return_library_array( "select id, short_name from lib_supplier",'id','short_name');
		$po_num=return_library_array( "select job_no, job_no_prefix_num from wo_po_details_master",'job_no','job_no_prefix_num');
		$arr=array (2=>$comp,3=>$buyer_arr,4=>$po_num,5=>$po_array,6=>$item_category,7=>$fabric_source,8=>$suplier,9=>$approved,10=>$is_ready);
		  $sql= "select booking_no_prefix_num, booking_no,booking_date,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved from wo_booking_mst  where $company $buyer $booking_date and booking_type=1 and is_short=2 and  status_active=1  and 	is_deleted=0 order by booking_no"; 
		echo  create_list_view("list_view", "Booking No,Booking Date,Company,Buyer,Job No.,PO number,Fabric Nature,Fabric Source,Supplier,Approved,Is-Ready", "80,80,70,100,90,200,80,80,50,50","1020","320",0, $sql , "js_set_value", "booking_no_prefix_num", "", 1, "0,0,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", $arr , "booking_no_prefix_num,booking_date,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", '','','0,0,0,0,0,0,0,0,0,0,0','','');
		
exit(); 
}
	$tmplte=explode("**",$data);
	if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
	if ($template=="") $template=1;
	if ($action=="cpa_report_generate")
	{
		$process = array( &$_POST );
             
		extract(check_magic_quote_gpc( $process ));
		if(str_replace("'","",$cbo_buyer_name)==0)
		{
			if ($_SESSION['logic_erp']["data_level_secured"]==1)
			{
				if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
			}
			else
			{
				$buyer_id_cond="";
			}
		}
		else
		{
			$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
		}
		$date_cond='';
		if(str_replace("'","",$cbo_search_date)==1)
		{
			if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
			{
			 if($db_type==0)
				{
					$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd","");
					$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd","");
				}
				else if($db_type==2)
				{
					$start_date=change_date_format(str_replace("'","",$txt_date_from),"","",1);
					$end_date=change_date_format(str_replace("'","",$txt_date_to),"","",1);
				}
			$date_cond=" and b.pub_shipment_date between '$start_date' and '$end_date'";
			}
		}
		else if(str_replace("'","",$cbo_search_date)==2)
		{
		if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
			{
			 if($db_type==0)
				{
					$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd","");
					$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd","");
				}
				else if($db_type==2)
				{
					$start_date=change_date_format(str_replace("'","",$txt_date_from),"","",1);
					$end_date=change_date_format(str_replace("'","",$txt_date_to),"","",1);
				}
			$date_cond=" and c.booking_date between '$start_date' and '$end_date'";
			}
		}
		$job_no=str_replace("'","",$txt_job_no);
		$booking_no=str_replace("'","",$txt_booking_no);
		//echo $buyer_id_cond.'='.$end_date;
		if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in ($job_no) ";
		if ($booking_no=="") $booking_no_cond=""; else $booking_no_cond=" and c.booking_no_prefix_num in ($booking_no) ";
		if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
		else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
		if($template==1)
		{
		ob_start();
		$style1="#E9F3FF"; 
		$style="#FFFFFF";
		$company_name=str_replace("'","",$cbo_company_name);
	 ?>
     <div style="width:1810px;" id="content_search_panel2">
     <fieldset style="width:100%;">	
            <table width="1810">
                    <tr class="form_caption">
                        <td colspan="22" align="center"><strong>CPA/Short Fabric Booking Analysis Report</strong></td>
                    </tr>
                    <tr class="form_caption">
                        <td colspan="22" align="center"><strong><?php echo $company_library[$company_name]; ?></strong></td>
                    </tr>
            </table>
            <table id="table_header_1" class="rpt_table" width="1810" cellpadding="0" cellspacing="0" border="1" rules="all">
               <thead>
                <tr>
                 <th width="30">SL</th>
                 	<th width="60">Year</th>
                   	<th width="100">Job No</th>
                    <th width="100">Buyer</th>
                    <th width="100">Booking Date</th>
                    <th width="100">Booking No</th>
                    <th width="100">Order No</th>
                    <th width="100">Fabric Color</th>
                    <th width="70">Order Qty(pcs)</th>
                    <th width="80">Main Booking Qty</th>
                    <th width="70">CPA Grey Qty(kg)</th>
                    <th width="80">CPA Finish (Qty)</th>
                    <th width="80">Total Grey Qty</th>
                    <th width="80">Pre-Costing Cons/Dzn</th>
                    <th width="80">Quote Cons/Dzn</th>
                    <th width="80">Total Booking Con's/Doz</th>
                    <th width="80">Pre Cost Variance</th>
                    <th width="80">Quote Variance</th>
                    <th width="70">Excess Fabric(%)</th>
                    <th width="90">Responsible Dept.</th>
                    <th width="80">Responsible</th>
                    <th width="">Reason</th>
                </tr>
               </thead>
             </table>
             <div style="width:1830px; max-height:400px; overflow-y:scroll" id="scroll_body">
             <table class="rpt_table" width="1810" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
             <?php	
			 	$fabriccostArray=array();$date_string_arr=array(); $grey_qty_arr=array(); $responsible_dept_arr=array(); $responsible_person_arr=array(); $reason_arr=array();
				$main_booking_no_arr=array();$short_booking_no_arr=array(); $short_booking_no_large_arr=array();$main_booking_no_large_arr=array();
				$price_quotation_arr=array(); $price_qou_costArray=array();
				$pq_sql=sql_select("select quotation_id,fab_knit_req_kg from wo_pri_quo_sum_dtls where status_active=1 and is_deleted=0  ");
					foreach($pq_sql as $row_pq)
					{
					 $price_quotation_arr[$row_pq[csf('quotation_id')]]['fab_knit_req']=$row_pq[csf('fab_knit_req_kg')];
					
					} //var_dump( $price_quotation_arr);
				 $price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_qou_costArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					} //var_dump( $price_qou_costArray);
				 $fabriccostDataArray=sql_select("select job_no, costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost,certificate_pre_cost,currier_pre_cost from wo_pre_cost_dtls where status_active=1 and is_deleted=0  ");
					foreach($fabriccostDataArray as $fabRow)
					{
					 $fabriccostArray[$fabRow[csf('job_no')]]['costing_per_id']=$fabRow[csf('costing_per_id')];
					
					} 
					$sql_req_qty=sql_select("select job_no,fab_knit_req_kg from  wo_pre_cost_sum_dtls where status_active=1 and is_deleted=0 ");
					foreach($sql_req_qty as $row_data)
					{
					 $grey_qty_arr[$row_data[csf('job_no')]]['fab_knit']=$row_data[csf('fab_knit_req_kg')];
					} //var_dump($grey_qty_arr);
					$s_fab_book_arr=array();
					$item_fab_book_arr=array();
					$sfab_sql=sql_select("select c.job_no,c.fabric_source as short_fabric_source,c.item_category as short_item_category  from wo_po_details_master a, wo_booking_mst c,wo_booking_dtls d where c.booking_no=d.booking_no and c.booking_type=1 and c.is_short=1 and  a.job_no=c.job_no and c.company_id='$company_name' $job_no_cond and c.status_active=1 and c.is_deleted=0  ");
					foreach($sfab_sql as $s_data)
					{
					$s_fab_book_arr[$s_data[csf('job_no')]]['short_fabric_source']=$s_data[csf('short_fabric_source')];
					$s_fab_book_arr[$s_data[csf('job_no')]]['short_item_category']=$s_data[csf('short_item_category')];
					}// var_dump($s_fab_book_arr);
					$sm_fab_book_arr=array();
					$fab_sql=sql_select("select c.job_no,c.fabric_source as main_fabric_source,c.item_category as main_item_category from wo_po_details_master a, wo_booking_mst c,wo_booking_dtls d where c.booking_no=d.booking_no and c.booking_type=1 and c.is_short=2 and  a.job_no=c.job_no and c.company_id='$company_name' $job_no_cond and c.status_active=1 and c.is_deleted=0  ");
					foreach($fab_sql as $sm_data)
					{
					$sm_fab_book_arr[$sm_data[csf('job_no')]]['main_fabric_source']=$sm_data[csf('main_fabric_source')];
					$sm_fab_book_arr[$sm_data[csf('job_no')]]['main_item_category']=$sm_data[csf('main_item_category')];
					}  //var_dump($sm_fab_book_arr);
				if($db_type==2)
				{
				$sql_b_date=sql_select("select 
				a.job_no,c.booking_date,d.responsible_dept,d.responsible_person,d.reason,
				LISTAGG(CAST(d.responsible_dept AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY d.responsible_dept)  as responsible_dept,
				LISTAGG(CAST(d.responsible_person AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY d.responsible_person)  as responsible_person,
				LISTAGG(CAST(d.reason AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY d.reason)  as reason,  
				(case c.is_short when 2 then LISTAGG(CAST(c.booking_no_prefix_num AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.booking_no_prefix_num) end) as main_booking_no,
				(case c.is_short when 1 then LISTAGG(CAST(c.booking_no_prefix_num AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.booking_no_prefix_num) end) as short_booking_no,  
				(case c.is_short when 2 then LISTAGG(CAST(c.booking_no AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.booking_no) end) as main_booking_large,
				(case c.is_short when 1 then LISTAGG(CAST(c.booking_no AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.booking_no) end) as short_booking_large 
				from wo_po_details_master a, wo_po_break_down b,wo_booking_mst c,wo_booking_dtls d 
				where a.job_no=b.job_no_mst and a.job_no=c.job_no and a.job_no=d.job_no and 
				b.id=d.po_break_down_id and c.booking_no=d.booking_no and c.booking_type=1 and a.company_name='$company_name' and 
				a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and 
				c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 
				$date_cond 
				$buyer_id_cond   
				$job_no_cond
				$booking_no_cond
				group by a.job_no,c.booking_date,d.responsible_dept,d.responsible_person,d.reason,c.is_short
				");
				}
				else if($db_type==0)
				{
				$sql_b_date=sql_select("select 
				a.job_no,c.booking_date,d.responsible_dept,d.responsible_person,d.reason,
				group_concat(distinct d.responsible_dept) as responsible_dept,
				group_concat(distinct d.responsible_person)   as responsible_person,
				group_concat( distinct d.reason ) as reason,  
				group_concat(distinct(case when  c.is_short=2 then c.booking_no_prefix_num end))  as main_booking_no,
				group_concat(distinct(case  c.is_short when 1 then c.booking_no_prefix_num end))  as short_booking_no,
				group_concat(distinct(case  c.is_short when 1 then c.booking_no end))  as main_booking_large,
				group_concat(distinct(case  c.is_short when 2 then c.booking_no_prefix_num end))  as short_booking_large
				from wo_po_details_master a, wo_po_break_down b,wo_booking_mst c,wo_booking_dtls d 
				where a.job_no=b.job_no_mst and a.job_no=c.job_no and a.job_no=d.job_no and 
				b.id=d.po_break_down_id and c.booking_no=d.booking_no and c.booking_type=1 and a.company_name='$company_name' and 
				a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and 
				c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 
				$date_cond 
				$buyer_id_cond   
				$job_no_cond
				$booking_no_cond
				group by a.job_no,c.booking_date,d.responsible_dept,d.responsible_person,d.reason,c.is_short
				");
				}
				foreach($sql_b_date as $row_date)
				{
					//echo $row_date[csf('short_booking_no')];
				$date_string_arr[$row_date[csf('job_no')]][$row_date[csf('booking_date')]]=$row_date[csf('booking_date')];	
				$responsible_dept_arr[$row_date[csf('job_no')]][$row_date[csf('responsible_dept')]]=$row_date[csf('responsible_dept')];	
				$responsible_person_arr[$row_date[csf('job_no')]][$row_date[csf('responsible_person')]]=$row_date[csf('responsible_person')];	
				$reason_arr[$row_date[csf('job_no')]][$row_date[csf('reason')]]=$row_date[csf('reason')];
				$main_booking_no_arr[$row_date[csf('job_no')]][$row_date[csf('main_booking_no')]]=$row_date[csf('main_booking_no')];
				if(str_replace("'","",$row_date[csf('short_booking_no')])!="")
				{
					$short_booking_no_arr[$row_date[csf('job_no')]][$row_date[csf('short_booking_no')]]=$row_date[csf('short_booking_no')];	
				}
				$main_booking_no_large_arr[$row_date[csf('job_no')]][$row_date[csf('main_booking_large')]]=$row_date[csf('main_booking_large')];
				$short_booking_no_large_arr[$row_date[csf('job_no')]][$row_date[csf('short_booking_large')]]=$row_date[csf('short_booking_large')];		
				} //var_dump($short_booking_no_arr);
				$i=1;
			if($db_type==2)
			{
               $sql="select 
				a.job_no,
				$year_field,
				a.job_no_prefix_num, 
				a.company_name,
				a.total_set_qnty as ratio,
				a.buyer_name,
				a.quotation_id,
				sum(case c.is_short
				when 2 then 
				d.grey_fab_qnty end) as main_booking_qty,
				sum(case c.is_short
				when 1 then 
				d.grey_fab_qnty end) as cpa_grey_qty,
				sum(case c.is_short
				when 1 then 
				d.fin_fab_qnty end) as cpa_fin_qty,
				sum(d.grey_fab_qnty) as grey_fab_qnty,
				LISTAGG(CAST(d.fabric_color_id AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY d.fabric_color_id) as fabric_color_id ,
				LISTAGG(CAST(d.po_break_down_id AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY d.po_break_down_id) as po_break_down_id, 
				sum(distinct b.po_quantity) as po_quantity 
				from wo_po_details_master a, 
				wo_po_break_down b,
				wo_booking_mst c,
				wo_booking_dtls d 
				where 
				a.job_no=b.job_no_mst and 
				a.job_no=c.job_no and 
				a.job_no=d.job_no and 
				b.id=d.po_break_down_id and 
				c.booking_no=d.booking_no and
				c.booking_type=1 
				and 
				a.company_name='$company_name' and 
				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 and 
				c.status_active=1 and 
				c.is_deleted=0 and 
				d.status_active=1 and 
				d.is_deleted=0 
				$date_cond 
				$buyer_id_cond   
				$job_no_cond
				$booking_no_cond
				group by 
				a.job_no,
				a.job_no_prefix_num, 
				a.company_name,
				a.total_set_qnty,
				a.buyer_name,
				a.insert_date,
				a.quotation_id
				";
			}
			else if($db_type==0)
			{
                $sql="select 
				a.job_no,
				$year_field,
				a.job_no_prefix_num, 
				a.company_name,
				a.total_set_qnty as ratio,
				a.buyer_name,
				a.quotation_id,
				sum(case c.is_short
				when 2 then 
				d.grey_fab_qnty end) as main_booking_qty,
				sum(case c.is_short
				when 1 then 
				d.grey_fab_qnty end) as cpa_grey_qty,
				sum(case c.is_short
				when 1 then 
				d.fin_fab_qnty end) as cpa_fin_qty,
				sum(d.grey_fab_qnty) as grey_fab_qnty,
				group_concat( distinct d.fabric_color_id) as fabric_color_id ,
				group_concat(distinct d.po_break_down_id) as po_break_down_id, 
				sum(distinct b.po_quantity) as po_quantity 
				from wo_po_details_master a, 
				wo_po_break_down b,
				wo_booking_mst c,
				wo_booking_dtls d 
				where 
				a.job_no=b.job_no_mst and 
				a.job_no=c.job_no and 
				a.job_no=d.job_no and 
				b.id=d.po_break_down_id and 
				c.booking_no=d.booking_no and
				c.booking_type=1 and 
				a.company_name='$company_name' and 
				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 and 
				c.status_active=1 and 
				c.is_deleted=0 and 
				d.status_active=1 and 
				d.is_deleted=0 
				$date_cond 
				$buyer_id_cond   
				$job_no_cond
				$booking_no_cond
				group by 
				a.job_no,
				a.job_no_prefix_num, 
				a.company_name,
				a.total_set_qnty,
				a.buyer_name,
				a.insert_date,
				a.quotation_id
				";
			}
				$result=sql_select($sql);
				 $tot_rows=count($result);
				foreach($result as $row )
                {
				  if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				//print $row[csf('job_no')];
				$main_booking_no_prefix=implode(",",$main_booking_no_arr[$row[csf('job_no')]]);
				$short_booking_no_prefix=implode(",",$short_booking_no_arr[$row[csf('job_no')]]);
				$main_booking_no=implode(",",array_unique(explode(",",$main_booking_no_prefix)));
				$main_booking_data=explode(',', $main_booking_no); 
				$short_booking_no=implode(",",array_unique(explode(",",$short_booking_no_prefix)));
				$short_booking_data=explode(',', $short_booking_no); 
				$main_booking_no_large=array_unique(explode(",",implode(",",$main_booking_no_large_arr[$row[csf('job_no')]])));
				$short_booking_no_large=array_unique(explode(",",implode(",",$short_booking_no_large_arr[$row[csf('job_no')]])));
				$main_booking_no_large_data="";
				foreach($main_booking_no_large as $mb_large)
				{	
					if($mb_large>0)
					{
					if($main_booking_no_large_data=="") $main_booking_no_large_data=$mb_large; else $main_booking_no_large_data.=",".$mb_large;
					}
				}
				$short_booking_no_large_data="";
				foreach($short_booking_no_large as $sb_large)
				{	
					if($sb_large>0)
					{
					if($short_booking_no_large_data=="") $short_booking_no_large_data=$sb_large; else $short_booking_no_large_data.=",".$sb_large;
					}
				}
				$main_fab_source=$sm_fab_book_arr[$row[csf('job_no')]]['main_fabric_source'];
				$short_fab_source=$s_fab_book_arr[$row[csf('job_no')]]['short_fabric_source'];
				$main_fab_item_category=$sm_fab_book_arr[$row[csf('job_no')]]['main_item_category'];
				$short_fab_item_category=$s_fab_book_arr[$row[csf('job_no')]]['short_item_category'];
				$po_break_id=implode(",",array_unique(explode(",",$row[csf('po_break_down_id')]))); 
				$fabric_color=implode(",",array_unique(explode(",",$row[csf('fabric_color_id')])));
				$total_pri_costing_dzn="";
				$price_quo_costing_per=$price_qou_costArray[$row[csf('quotation_id')]]['costing_per'];//$fabriccostArray[$row[csf('job_no')]]['costing_per_id'];
				//echo $price_quo_costing_per;
				if($price_quo_costing_per==1)
				{
					$total_pri_costing_dzn=$price_quotation_arr[$row[csf('quotation_id')]]['fab_knit_req'];//$grey_qty_arr[$row[csf('job_no')]]['fab_knit'];
				}
				else if($price_quo_costing_per==2)
				{
					$total_pri_costing_dzn=$price_quotation_arr[$row[csf('quotation_id')]]['fab_knit_req']*12;
				}
				else if($price_quo_costing_per==3)
				{
					$total_pri_costing_dzn=$price_quotation_arr[$row[csf('quotation_id')]]['fab_knit_req']/2;
				}
				else if($price_quo_costing_per==4)
				{
					$total_pri_costing_dzn=$price_quotation_arr[$row[csf('quotation_id')]]['fab_knit_req']/3;
				}
				else if($price_quo_costing_per==5)
				{
					$total_pri_costing_dzn=$price_quotation_arr[$row[csf('quotation_id')]]['fab_knit_req']/4;
				}
					$total_pre_costing_dzn="";
					$dzn_qnty=0;
					$costing_per_id=$fabriccostArray[$row[csf('job_no')]]['costing_per_id'];
				if($costing_per_id==1)
				{
					$dzn_qnty=12*1;
					$total_pre_costing_dzn=$grey_qty_arr[$row[csf('job_no')]]['fab_knit'];
				}
				else if($costing_per_id==2)
				{
					$dzn_qnty=1;
					$total_pre_costing_dzn=$grey_qty_arr[$row[csf('job_no')]]['fab_knit']*12;
				}
				else if($costing_per_id==3)
				{
					$dzn_qnty=12*2;
					$total_pre_costing_dzn=$grey_qty_arr[$row[csf('job_no')]]['fab_knit']/2;
				}
				else if($costing_per_id==4)
				{
					$dzn_qnty=12*3;
					$total_pre_costing_dzn=$grey_qty_arr[$row[csf('job_no')]]['fab_knit']/3;
				}
				else if($costing_per_id==5)
				{
					$dzn_qnty=12*4;
					$total_pre_costing_dzn=$grey_qty_arr[$row[csf('job_no')]]['fab_knit']/4;
				}
				$po_qty_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
				$total_booking_cons_dzn=($row[csf('grey_fab_qnty')]/$po_qty_pcs)*12;
				$total_variance_cons_dzn=($total_pre_costing_dzn-$total_booking_cons_dzn);
				$total_price_quo_variance_dzn=($total_pri_costing_dzn-$total_booking_cons_dzn);
				$excess_percent=$row[csf('cpa_grey_qty')]/$row[csf('main_booking_qty')]*100;
				$dept_responsible=implode(",",$responsible_dept_arr[$row[csf('job_no')]]) ; 
				$person_responsible=implode(",",$responsible_person_arr[$row[csf('job_no')]]); 
				$reason=implode(",",$reason_arr[$row[csf('job_no')]]); 
				$reason_result=implode(",",array_unique(explode(",",$reason)));
				$reason_data_arr=explode(",",$reason_result);
				$reason_name_data='';
				foreach($reason_data_arr as $reason_data)
				{
					if($reason_name_data=="") $reason_name_data=$reason_data; else $reason_name_data.=",".$reason_data;
				}
				$dept_responsible_name_data='';
				$dept_responsible_pre_name1=implode(",",array_unique(explode(",",$dept_responsible)));
				$dept_responsible_pre_name=explode(",",$dept_responsible_pre_name1);
				foreach($dept_responsible_pre_name as $dept_response)
				{
					if($dept_responsible_name_data=="") $dept_responsible_name_data=$department_name_library[$dept_response]; else $dept_responsible_name_data.=",".$department_name_library[$dept_response];
				}
				$fabric_color_data='';
				$fabric_color_id=explode(",",$fabric_color);
				
				foreach($fabric_color_id as $fabcolor_id)
				{
					if($fabric_color_data=="") $fabric_color_data=$color_library[$fabcolor_id]; else $fabric_color_data.=", ".$color_library[$fabcolor_id];
				}
				$order_number_arr='';
				$order_number=explode(",",$po_break_id);
				foreach($order_number as $po_id)
				{	if($po_id>0)
					{
					if($order_number_arr=="") $order_number_arr=$order_arr[$po_id]; else $order_number_arr.=",".$order_arr[$po_id];
					}
				}
				$booking_date1=implode(",",$date_string_arr[$row[csf('job_no')]]);
				$booking_date=implode(",",array_unique(explode(",",$booking_date1)));
				$booking_date_data='';
				$booking_date_format=explode(",",$booking_date);
				foreach($booking_date_format as $booking_date_res)
				{
					if($booking_date_data=="") $booking_date_data=change_date_format($booking_date_res); else $booking_date_data.=",".change_date_format($booking_date_res);
				}
				if($total_pre_costing_dzn<$total_booking_cons_dzn)
				{
					$color_pre="red";
				}
				else if($total_pre_costing_dzn>$total_booking_cons_dzn)
				{
					$color_pre="green";	
				}
				else
				{
					$color_pre="";		
				}
				//print_r($short_booking_data);
                if($short_booking_data[0]!="")
				{     
			 ?>
             	<tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i; ?>">
                <td width="30"><?php echo $i; ?></td>
                <td width="60"><p><?php echo $row[csf('year')] ; ?></p></td>
                <td width="100" title="<?php echo $row[csf('job_no_prefix_num')] ; ?>"><p><?php echo $row[csf('job_no_prefix_num')] ; ?></p></td>
                <td width="100" title="<?php echo $buyer_short_name_library[$row[csf('buyer_name')]]; ?>"><p><?php echo $buyer_short_name_library[$row[csf('buyer_name')]] ; ?></p></td>
                <td width="100"><p><?php echo $booking_date_data; ?></p></td>
                <td width="100">
                <?php
				$main_booking_type=2;
				$short_booking_type=1;
                $short_booking_data_arr='';
				  foreach( $short_booking_data as $sb_row)
				  {
					 if($sb_row>0)
					 {	
					if($short_booking_data_arr=="") $short_booking_data_arr="<a href='##' onclick=\"ms_booking_no_popup('".$short_booking_type."','".$company_name."','".$sb_large."','".$po_id."','".$row[csf('job_no')]."','".$short_fab_source."','".$short_fab_item_category."')\">".$sb_row."</a>"; else 
					$short_booking_data_arr.=","."<a href='##' onclick=\"ms_booking_no_popup('".$short_booking_type."','".$company_name."','".$sb_large."','".$po_id."','".$row[csf('job_no')]."','".$short_fab_source."','".$short_fab_item_category."')\">".$sb_row."</a>";
					 }
				  }
				   $main_booking_data_arr='';
				    foreach( $main_booking_data as $mb_row)
				  {
					 if($mb_row>0)
					 {
					if($main_booking_data_arr=="")
						{ $main_booking_data_arr="<a href='##' onclick=\"ms_booking_no_popup('".$main_booking_type."','".$company_name."','".$mb_large."','".$po_id."','".$row[csf('job_no')]."','".$main_fab_source."','".$main_fab_item_category."')\">".$mb_row."</a>";
						}
					else 
						{
					$main_booking_data_arr.=","."<a href='##' onclick=\"ms_booking_no_popup('".$main_booking_type."','".$company_name."','".$mb_large."','".$po_id."','".$row[csf('job_no')]."','".$main_fab_source."','".$main_fab_item_category."')\">".$mb_row."</a>";
						}
					 }
				  }
				 	  ?>
                <p><?php echo  "MB: ".$main_booking_data_arr?></p> <p> <?php echo "SB: ".$short_booking_data_arr ; ?></p></td>
                <td width="100"><p><?php echo $order_number_arr ; ?></p></td>
                <td width="100" title="<?php echo $fabric_color_data; ?>"><p><?php
				 echo $fabric_color_data;	
				 ?></p></td>
                <td width="70" align="right"><p><?php echo $po_qty_pcs; ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($row[csf('main_booking_qty')],2); ?></p></td>
                <td width="70" align="right"><p><?php echo number_format($row[csf('cpa_grey_qty')],2); ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($row[csf('cpa_fin_qty')],2); ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($row[csf('grey_fab_qnty')],2); ?></p></td>
                <td width="80" align="right" title="<?php echo $total_pre_costing_dzn;?>"><p><?php echo number_format($total_pre_costing_dzn,2) ; ?></p></td>
                <td width="80" align="right" title="<?php //echo $total_pre_costing_dzn;?>"><p><?php echo number_format($total_pri_costing_dzn,2) ; ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($total_booking_cons_dzn,2); //echo $grey_fab_qty.'='.$po_qty_pcs.'='.$dzn_qnty;   ?></p></td>
                <td width="80" bgcolor="<?php echo $color_pre;?>"  align="right"><p><?php echo number_format($total_variance_cons_dzn,2); ?></p></td>
                <td width="80" bgcolor="<?php //echo $color_pre;?>"  align="right"><p><?php echo number_format($total_price_quo_variance_dzn,2); ?></p></td>
                <td width="70" align="right"><p><?php echo number_format($excess_percent,2); ?></p></td>
                <td width="90"><p><?php echo $dept_responsible_name_data; ?></p></td>
                <td width="80"><p><?php echo $person_responsible; ?></p></td>
                <td width=""><p><?php echo $reason_name_data; ?></p></td>
                </tr>
                <?php
				$total_main_booking_qty+=$row[csf('main_booking_qty')];
				$total_grey_qty_kg+=$row[csf('cpa_grey_qty')];
				$total_fin_qty+=$row[csf('cpa_fin_qty')];
				$total_grey_fab_qnty+=$row[csf('grey_fab_qnty')];
				$i++;
				}
				}
				?>
             </table>
             <table class="rpt_table" width="1810" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tfoot>
                     <th width="30"></th>
                     <th width="60"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="70"></th>
                     <th width="80"> <?php echo number_format($total_main_booking_qty,2) ?></th>
                     <th width="70"> <?php echo number_format($total_grey_qty_kg,2) ?> </th>
                     <th width="80"> <?php echo number_format($total_fin_qty,2) ?></th>
                     <th width="80"> <?php echo number_format($total_grey_fab_qnty,2) ?></th>
                     <th width="80"></th>
                     <th width="80"></th>
                     <th width="80"></th>
                     <th width="80"></th>
                     <th width="80"></th>
                     <th width="70"></th>
                     <th width="90"></th>
                     <th width="80"></th>
                     <th width=""></th>
                     </tfoot>
              </table>
             </div>
             </fieldset>
             </div>
<?php	
		}
		echo "$total_data****$filename";
		exit(); 
	}
?>