-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppl_entry_machine_datewise`
--

DROP TABLE IF EXISTS `ppl_entry_machine_datewise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppl_entry_machine_datewise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `dtls_id` int(11) NOT NULL DEFAULT '0',
  `machine_id` int(11) NOT NULL DEFAULT '0',
  `distribution_date` date NOT NULL,
  `fraction_date` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=Yes; 0=No;',
  `days_complete` double NOT NULL DEFAULT '0',
  `qnty` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppl_entry_machine_datewise`
--

LOCK TABLES `ppl_entry_machine_datewise` WRITE;
/*!40000 ALTER TABLE `ppl_entry_machine_datewise` DISABLE KEYS */;
INSERT INTO `ppl_entry_machine_datewise` VALUES (1,1,1,3,'2015-04-27',1,0.97,291),(5,4,4,1,'2015-05-04',1,0.01,5),(7,3,3,4,'2015-05-04',1,0.01,2),(8,3,5,1,'2015-05-01',1,0.02,7),(9,6,7,8,'2015-05-12',0,1,350),(10,6,7,8,'2015-05-13',1,0.86,302),(11,7,8,9,'2015-05-26',1,0.03,10),(12,8,9,3,'2015-05-27',1,0.46,147.48);
/*!40000 ALTER TABLE `ppl_entry_machine_datewise` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
