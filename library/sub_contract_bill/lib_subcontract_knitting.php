<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Marchandising team who will operate order 
					level entry and marketing. here 2 form is available where 1 is creating team leader 
					and 2nd is creating team member belongs to the team.
					
Functionality	:	First create team info and save then add multiple members one by one.
					select a team from list view for update.
JS Functions	:
Created by		:	Kausar 
Creation date 	: 	04-10-2012
Updated by 		: Maruf		
Update date		: 09-12-2015		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/


session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Knitting Charge Set up", "../../", 1, 1,$unicode,'','');

?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
	var permission='<?php echo $permission; ?>';
	
	var str_const_comp = [<?php echo substr(return_library_autocomplete( "select const_comp from lib_subcon_charge group by const_comp", "const_comp" ), 0, -1); ?>];
	$(document).ready(function(e)
	 {
            $("#text_construction_composition").autocomplete({
			 source: str_const_comp
		  });
     });
	 
	var str_yarn_desc = [<?php echo substr(return_library_autocomplete( "select yarn_description from lib_subcon_charge group by yarn_description", "yarn_description" ), 0, -1); ?>];
	$(document).ready(function(e)
	 {
            $("#text_yarn_description").autocomplete({
			 source: str_yarn_desc
		  });
     });

	function fnc_lib_subcontract_knitting( operation )
	{
	   if (form_validation('cbo_company_name*cbo_body_part*text_construction_composition*text_gsm*text_yarn_description*cbo_uom','Company Name*Body Part*Construction & Composition*GSM*Yarn Description*UOM')==false)
		{
			return;
		}
		
		else
		{
			eval(get_submitted_variables('cbo_company_name*cbo_body_part*text_construction_composition*text_gsm*text_yarn_description*cbo_uom*text_inhouse_rate*txt_customer_rate*cbo_buyer_id*cbo_status*update_id'));
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name*cbo_body_part*text_construction_composition*text_gsm*text_yarn_description*cbo_uom*text_inhouse_rate*txt_customer_rate*cbo_buyer_id*cbo_status*update_id',"../../");
			freeze_window(operation);
			http.open("POST","requires/lib_subcontract_knitting_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_lib_subcontract_knitting_reponse;
		}
	}

	function fnc_lib_subcontract_knitting_reponse()
	{
		if(http.readyState == 4) 
		{
			//alert(http.responseText);
			var reponse=trim(http.responseText).split('**');
			//if (reponse[0].length>2) reponse[0]=10;
			show_msg(reponse[0]);
			//document.getElementById('id').value  = reponse[1];
			document.getElementById('update_id').value  = reponse[2];
			show_list_view('','list_container_subcont','list_container_subcont','../sub_contract_bill/requires/lib_subcontract_knitting_controller','setFilterGrid("list_view",-1)');
			reset_form('libsubcontractknitting_1','','','');
			set_button_status(0, permission, 'fnc_lib_subcontract_knitting',1);
			release_freezing();
		}
	}	

</script>
</head>
<body onLoad="set_hotkey()">
    <div align="center" style="width:100%">
    <?php echo load_freeze_divs ("../../",$permission);  ?>
    <fieldset style="width:800px">
        <form  name="libsubcontractknitting_1" id="libsubcontractknitting_1"  autocomplete="off">
        <table width="700px" align="center" >
            <tr align="left">  
                <td class="must_entry_caption">Company Name</td>
                <td>                           
					<?php
						echo create_drop_down( "cbo_company_name", 171, "select id,company_name from lib_company comp where is_deleted=0  and status_active=1 $company_cond order by company_name","id,company_name", 1, "--Select Company--",  $selected, "load_drop_down( 'requires/lib_subcontract_knitting_controller', this.value, 'load_drop_down_buyer_name', 'buyer_td' );" );
                    ?> 
                </td>
                <td class="must_entry_caption">Body Part</td>
                <td>
					<?php 
						echo create_drop_down( "cbo_body_part", 171, $body_part,'', 1, "--- Select Body Part ---", $selected, "", "","" );
                    ?>
                </td>
            </tr>
            <tr align="left">
                <td class="must_entry_caption">Construction & Composition </td>
                <td>
                    <input class="text_boxes" style="width:161px"  name="text_construction_composition" id="text_construction_composition" type="text" maxlength="200" title="Maximum 200 Character" /> 
                </td>
                <td  class="must_entry_caption">GSM</td>
                <td class="must_entry_caption">                           
                    <input  style="width:161px"  name="text_gsm" id="text_gsm" type="number" class="text_boxes_numeric" /> 
                </td>
            </tr>
            <tr>
                <td class="must_entry_caption"> Yarn Description</td>
                <td>
                    <input class="text_boxes" style="width:161px"  name="text_yarn_description" id="text_yarn_description" type="text" maxlength="150" title="Maximum 150 Character" /> 
                </td>
                <td>In-House Rate</td>
                <td>
                    <input style="width:161px"  name="text_inhouse_rate" id="text_inhouse_rate" type="number" class="text_boxes_numeric" maxlength="100" title="Maximum 100 Character" /> 
                </td>
            </tr>
            <tr>
                <td>Customer Rate</td>
                <td><input type="text"  name="txt_customer_rate" id="txt_customer_rate" class="text_boxes_numeric" style="width:161px;" title="Maximum 30 Character" /></td>
                <td> Subcon Buyer </td>
                <td id="buyer_td"><?php echo create_drop_down( "cbo_buyer_id", 171, $blank_array,'', 1, "-- Select Buyer --", $selected, "","" ,"0" ); ?></td>
            </tr>
            <tr>
                <td class="must_entry_caption">UOM</td>
                <td>
					<?php 
						echo create_drop_down( "cbo_uom", 171, $unit_of_measurement,'', 1, "--- Select UOM ---", $selected, "", "","1,2,12" );
                    ?>
                </td>
                <td>Status </td>
                <td>
					<?php 
						echo create_drop_down( "cbo_status", 171, $row_status,'', '', '', 1, '', "",'','','','3' );
                    ?>
                    <input type="hidden" name="update_id" id="update_id" value=""/>
                </td>
            </tr>
            <tr>
                <td colspan="4" height="15"> </td>
            </tr>
            <tr>
                <td colspan="4" align="center" class="button_container">
					<?php 
						echo load_submit_buttons( $permission, "fnc_lib_subcontract_knitting", 0,0,"reset_form('libsubcontractknitting_1','','','')",1);
                    ?>
                </td>
            </tr>
        </table>
        </form>
    </fieldset>
    
    <fieldset style="width:800px;">
    <legend>List View</legend>
        <form>
        <table width="700px" cellpadding="0" cellspacing="3" border="0">
            <tr>
                <td colspan="5" id="list_container_subcont">
					<?php
						//$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
						$buyer_arr=return_library_array("select id, buyer_name from lib_buyer",'id','buyer_name');
						$arr=array (0=>$buyer_arr,1=>$body_part,6=>$unit_of_measurement,8=>$row_status);
						echo  create_list_view ( "list_view", "Buyer Name,Body Part,Construction & Composition,GSM,Yarn Description,In House Rate,UOM,Customer Rate,Status", "150,120,180,60,60,150,70,100,60","1040","220",1, "select id, body_part, const_comp, gsm, yarn_description, uom_id, status_active, customer_rate, buyer_id,in_house_rate from lib_subcon_charge where  is_deleted=0 and rate_type_id=2 order by id desc", "get_php_form_data", "id","'load_php_data_to_form'", 1, "buyer_id,body_part,0,0,0,0,uom_id,0,status_active", $arr , "buyer_id,body_part,const_comp,gsm,yarn_description,in_house_rate,uom_id,customer_rate,status_active", "../sub_contract_bill/requires/lib_subcontract_knitting_controller", 'setFilterGrid("list_view",-1);','0,0,0,0,0,2,0,0,0' ) ;	  
                    ?>
                </td>
            </tr>
        </table>                 
        </form>
    </fieldset>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
