-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `variable_settings_production`
--

DROP TABLE IF EXISTS `variable_settings_production`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable_settings_production` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` int(11) NOT NULL DEFAULT '0',
  `variable_list` int(11) NOT NULL DEFAULT '0',
  `cutting_update_hcode` varchar(50) DEFAULT NULL,
  `cutting_update` int(11) NOT NULL DEFAULT '0',
  `cutting_input_hcode` varchar(50) NOT NULL,
  `cutting_input` int(11) NOT NULL,
  `cut_panel_delevery_hcode` varchar(30) NOT NULL,
  `cut_panel_delevery` int(11) NOT NULL DEFAULT '0',
  `printing_emb_production_hcode` varchar(50) DEFAULT NULL,
  `printing_emb_production` int(11) NOT NULL,
  `sewing_production_hcode` varchar(30) NOT NULL DEFAULT '0',
  `sewing_production` int(11) NOT NULL DEFAULT '0',
  `iron_update_hcode` varchar(30) NOT NULL DEFAULT '0',
  `iron_update` int(11) NOT NULL DEFAULT '0',
  `finishing_update_hcode` varchar(30) NOT NULL DEFAULT '0',
  `finishing_update` int(11) NOT NULL DEFAULT '0',
  `ex_factory` int(11) NOT NULL DEFAULT '0',
  `fabric_roll_level_hcode` varchar(30) NOT NULL DEFAULT '0',
  `fabric_roll_level` int(11) NOT NULL DEFAULT '0',
  `fabric_machine_level_hcode` varchar(30) NOT NULL DEFAULT '0',
  `fabric_machine_level` int(11) NOT NULL DEFAULT '0',
  `batch_maintained_hcode` varchar(30) NOT NULL DEFAULT '0',
  `batch_maintained` int(11) NOT NULL DEFAULT '0',
  `iron_input` int(11) NOT NULL DEFAULT '0',
  `production_entry_hcode` varchar(100) NOT NULL,
  `production_entry` int(11) NOT NULL DEFAULT '0',
  `item_category_id` int(11) NOT NULL DEFAULT '0',
  `auto_update` tinyint(1) NOT NULL DEFAULT '0',
  `batch_no_creation` tinyint(4) NOT NULL DEFAULT '0',
  `smv_source` tinyint(4) NOT NULL DEFAULT '0',
  `shift_id` int(11) NOT NULL DEFAULT '0',
  `prod_start_time` time NOT NULL,
  `lunch_start_time` time NOT NULL,
  `piece_rate_wq_limit` int(3) NOT NULL,
  `cut_sefty_parcent` int(3) NOT NULL,
  `sewing_sefty_parcent` int(3) NOT NULL,
  `iron_sefty_parcent` int(3) NOT NULL,
  `finish_sefty_parcent` int(3) NOT NULL,
  `process_costing_maintain` int(3) NOT NULL DEFAULT '0',
  `inserted_by` int(3) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(3) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variable_settings_production`
--

LOCK TABLES `variable_settings_production` WRITE;
/*!40000 ALTER TABLE `variable_settings_production` DISABLE KEYS */;
INSERT INTO `variable_settings_production` VALUES (1,1,1,'Cutting Update',3,'Cutting Deloevary to Input',3,'',0,'Printing &amp; Embrd. Prodiction',3,'Sewing Production',3,'Iron Output',3,'Finishing Entry',3,1,'0',0,'0',0,'0',0,0,'Production Entry',2,0,0,0,0,0,'00:00:00','00:00:00',0,0,0,0,0,0,2,'2015-01-04 02:11:00',1,'2015-02-02 23:55:51',1,0,0),(2,1,23,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,0,1,0,0,0,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-01-14 00:53:26',1,'2015-03-07 14:02:20',1,0,0),(3,1,26,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,0,0,0,0,1,'08:30:00','13:00:00',0,0,0,0,0,0,1,'2015-02-03 00:14:36',0,'2015-07-30 09:57:45',1,0,0),(4,1,26,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,0,0,0,0,2,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-02-03 00:14:36',0,'2015-07-30 09:57:45',1,0,0),(5,1,26,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,0,0,0,0,3,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-02-03 00:14:36',0,'2015-07-30 09:57:45',1,0,0),(6,1,15,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,2,1,0,0,0,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-02-04 23:40:42',2,'2015-03-25 12:42:20',1,0,0),(7,1,15,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,13,2,0,0,0,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-02-04 23:40:42',2,'2015-03-25 12:42:20',1,0,0),(8,1,28,NULL,3,'',0,'',0,NULL,3,'0',3,'0',3,'0',3,0,'0',0,'0',0,'0',0,0,'',0,0,0,0,0,0,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-02-24 11:12:34',1,'2015-02-28 17:22:27',1,0,0),(9,1,25,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,0,0,0,1,0,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-02-25 12:20:25',1,'2015-02-28 17:33:07',1,0,0),(10,1,35,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,13,1,0,0,0,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-11-19 13:03:36',0,NULL,1,0,0),(11,1,35,NULL,0,'',0,'',0,NULL,0,'0',0,'0',0,'0',0,0,'0',0,'0',0,'0',0,0,'',0,100,0,0,0,0,'00:00:00','00:00:00',0,0,0,0,0,0,1,'2015-11-19 13:03:36',0,NULL,1,0,0);
/*!40000 ALTER TABLE `variable_settings_production` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
