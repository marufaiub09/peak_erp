-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pri_quo_fab_co_avg_con_dtls`
--

DROP TABLE IF EXISTS `wo_pri_quo_fab_co_avg_con_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pri_quo_fab_co_avg_con_dtls` (
  `id` int(11) NOT NULL DEFAULT '0',
  `wo_pri_quo_fab_co_dtls_id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `gmts_sizes` varchar(50) NOT NULL,
  `dia_width` varchar(50) DEFAULT NULL,
  `cons` double NOT NULL,
  `process_loss_percent` double NOT NULL,
  `requirment` double NOT NULL,
  `pcs` int(11) NOT NULL,
  `body_length` double NOT NULL DEFAULT '0',
  `body_sewing_margin` double NOT NULL,
  `body_hem_margin` double NOT NULL,
  `sleeve_length` double NOT NULL,
  `sleeve_sewing_margin` double NOT NULL,
  `sleeve_hem_margin` double NOT NULL,
  `half_chest_length` double NOT NULL,
  `half_chest_sewing_margin` double NOT NULL,
  `front_rise_length` double NOT NULL,
  `front_rise_sewing_margin` double NOT NULL,
  `west_band_length` double NOT NULL,
  `west_band_sewing_margin` double NOT NULL,
  `in_seam_length` double NOT NULL,
  `in_seam_sewing_margin` double NOT NULL,
  `in_seam_hem_margin` double NOT NULL,
  `half_thai_length` int(11) NOT NULL,
  `half_thai_sewing_margin` int(11) NOT NULL,
  `total` double NOT NULL,
  `marker_dia` double NOT NULL,
  `marker_yds` double NOT NULL,
  `marker_inch` double NOT NULL,
  `gmts_pcs` int(11) NOT NULL DEFAULT '0',
  `marker_length` double NOT NULL,
  `net_fab_cons` double NOT NULL,
  KEY `wo_pri_quo_fab_co_dtls_id` (`wo_pri_quo_fab_co_dtls_id`),
  KEY `quotation_id` (`quotation_id`),
  KEY `gmts_sizes` (`gmts_sizes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pri_quo_fab_co_avg_con_dtls`
--

LOCK TABLES `wo_pri_quo_fab_co_avg_con_dtls` WRITE;
/*!40000 ALTER TABLE `wo_pri_quo_fab_co_avg_con_dtls` DISABLE KEYS */;
INSERT INTO `wo_pri_quo_fab_co_avg_con_dtls` VALUES (11,1,1,'5','71 INCH',2.53,12,2.87,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(12,1,1,'6','71 INCH',2.53,12,2.87,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(13,1,1,'7','71 INCH',2.53,12,2.87,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(14,1,1,'8','71 INCH',2.53,12,2.87,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(15,1,1,'9','71 INCH',2.53,12,2.87,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(16,2,1,'5','28 TUBE',0.096,10,0.11,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(17,2,1,'6','28 TUBE',0.096,10,0.11,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(18,2,1,'7','28 TUBE',0.096,10,0.11,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(19,2,1,'8','28 TUBE',0.096,10,0.11,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(20,2,1,'9','28 TUBE',0.096,10,0.11,12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `wo_pri_quo_fab_co_avg_con_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
