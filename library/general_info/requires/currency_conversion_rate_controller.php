<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');
$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];
extract($_REQUEST);

//---------------------------------------------------------------------

if($action=="conversion_rate_entry_from_data")
{
$sql="select id,currency,conversion_rate,con_date from currency_conversion_rate where status_active=1 and is_deleted=0 and id=$data";
	$res = sql_select($sql);
	foreach($res as $row)
	{	
		echo "$('#update_id').val('".$row[csf("id")]."');\n";
		echo "$('#txt_currency').val('".$row[csf("currency")]."');\n";
		echo "$('#txt_conversion_rate').val('".$row[csf("conversion_rate")]."');\n";	
		echo "$('#txt_date').val('".change_date_format($row[csf("con_date")])."');\n";
		echo "set_button_status(1, permission, 'fn_conversion_rate_entry',1,1);";
		}
	exit();	
}



//--------------------------------------------------------------------------------------------
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if( $operation==0 ) // Insert
	{
		$con = connect();
		if($db_type==0)	{
			mysql_query("BEGIN");}
			
		
		if(str_replace("'","",$update_id)=="")
		{
			$id= return_next_id("id","currency_conversion_rate",1);
			$field_array_mst="id,currency,conversion_rate,con_date,inserted_by,insert_date,status_active,is_deleted";
			$data_array_mst="(".$id.",".$txt_currency.",".$txt_conversion_rate.",".$txt_date.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1,0)";
		}
		
		$rID=sql_insert("currency_conversion_rate",$field_array_mst,$data_array_mst,1);
		if($db_type==0)
		{
			if( $rID)
			{
			mysql_query("COMMIT");  
			echo "0**_".$id;
			}
			else
			{
			mysql_query("ROLLBACK"); 
			echo "10**_".$id;
			}
		}
		if($db_type==2 || $db_type==1 ){
			if( $rID)
				{
				oci_commit($con);  
				echo "0**_".$id;
				}
				else
				{
				oci_rollback($con);
				echo "10**_".$id;
				}
			}
			disconnect($con);
			die;
	}
	else if ($operation==1) // Update 
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$update_id=str_replace("'",'',$update_id);
		if($update_id!="")
		{
			$field_array_up="currency*conversion_rate*con_date*update_by*update_date";
			$data_array_up="".$txt_currency."*".$txt_conversion_rate."*".$txt_date."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		}
		
			$rID=sql_update("currency_conversion_rate",$field_array_up,$data_array_up,"id",$update_id,1);

		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "1**_".str_replace("'",'',$update_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**_".str_replace("'",'',$update_id);
			}
		}
		if($db_type==2 || $db_type==1 ){
			if($rID)
				{
					oci_commit($con);  
					echo "1**_".str_replace("'",'',$update_id);
				}
				else
				{
					oci_rollback($con); 
					echo "10**_".str_replace("'",'',$update_id);
				}
			}
			disconnect($con);
			die;
	}
	else if ($operation==2) // Delete
	{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$field_array="update_by*update_date*status_active*is_deleted";
			$data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*0*1";
			$rID=sql_update("currency_conversion_rate",$field_array,$data_array,"id","".$update_id."",1);
			
			if($db_type==0)
			{
				if($rID )
				{
					mysql_query("COMMIT");  
					echo "2**_".str_replace("'",'',$update_id);
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**_".str_replace("'",'',$update_id);
				}
			}
		
			if($db_type==2 || $db_type==1 )
			{
				if($rID )
				{
					oci_commit($con);  
					echo "2**_".str_replace("'",'',$update_id);
				}
				else
				{
					oci_rollback($con); 
					echo "10**_".str_replace("'",'',$update_id);
				}
			}
				disconnect($con);
				die;
	}

}//end action;



//--------------------------------------------------------------------------------------------

if($action=="load_list_view")
{ 
list($currency_id,$mst_id)=explode("_",$data);
if($currency_id!='')$con="currency='$currency_id' and"; else $con="id='$mst_id' and";
?>
<fieldset style="width:600px;">
<div style="width:590px;">
<table class="rpt_table" width="99%" border="1" cellspacing="0" cellpadding="0" rules="all">
    <thead>
        <th width="50">SL No</th>
        <th width="100">Currency</th>
        <th width="150">Conversion Rate</th>
        <th>Date</th>
    </thead>
</table>
</div>
<div style="width:590px; overflow-y: scroll; max-height:200px;">
<table class="rpt_table" width="99%" border="1" id="mail_setup" cellspacing="0" cellpadding="0" rules="all">
    <tbody>
<?php 


$result=sql_select("select id,currency,conversion_rate,con_date from currency_conversion_rate where $con  status_active=1 and is_deleted=0 order by id DESC");
$sl=1;
foreach($result as $list_rows){
$bgcolor=($sl%2==0)?"#E9F3FF":"#FFFFFF";
 ?>    
    <tr bgcolor="<?php echo $bgcolor; ?>" onClick="get_php_form_data(<?php echo $list_rows[csf('id')]; ?>,'conversion_rate_entry_from_data','requires/currency_conversion_rate_controller')" style="cursor:pointer;">
        <td width="50" align="center"><?php echo $sl; ?> </td>
        <td width="100"><?php echo $currency[$list_rows[csf('currency')]]; ?> </td>
        <td width="150" align="right"><?php echo $list_rows[csf('conversion_rate')]; ?></td>
        <td align="center"><?php echo change_date_format($list_rows[csf('con_date')]); ?> </td>
    </tr>
<?php $sl++; } ?>    
</tbody>  
</table>
</div>    
   
</fieldset>	
<?php
exit(); 
} 
?>