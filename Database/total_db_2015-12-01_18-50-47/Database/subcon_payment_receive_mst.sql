-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `subcon_payment_receive_mst`
--

DROP TABLE IF EXISTS `subcon_payment_receive_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcon_payment_receive_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix_no` varchar(20) NOT NULL,
  `prefix_no_num` int(11) NOT NULL DEFAULT '0',
  `receive_no` varchar(25) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `party_name` int(11) NOT NULL DEFAULT '0',
  `bill_type` int(2) NOT NULL DEFAULT '0',
  `payment_type` int(11) NOT NULL DEFAULT '1',
  `receipt_date` date NOT NULL,
  `instrument_id` int(11) NOT NULL DEFAULT '0',
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `net_amount` double NOT NULL DEFAULT '0',
  `adjustment_type` int(11) NOT NULL DEFAULT '0',
  `adjusted_amount` double NOT NULL DEFAULT '0',
  `bank_name` varchar(50) NOT NULL,
  `instrument_date` date DEFAULT NULL,
  `instrument_no` varchar(50) DEFAULT NULL,
  `clearance_method` int(11) NOT NULL DEFAULT '0',
  `advance_amount` double NOT NULL DEFAULT '0',
  `remarks` varchar(500) NOT NULL,
  `is_posted_account` int(5) NOT NULL,
  `inserted_by` tinyint(2) NOT NULL DEFAULT '0',
  `insert_date` date NOT NULL,
  `updated_by` tinyint(2) NOT NULL DEFAULT '0',
  `update_date` date NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcon_payment_receive_mst`
--

LOCK TABLES `subcon_payment_receive_mst` WRITE;
/*!40000 ALTER TABLE `subcon_payment_receive_mst` DISABLE KEYS */;
INSERT INTO `subcon_payment_receive_mst` VALUES (1,'PAL-15-',1,'PAL-15-00001',1,19,2,1,'2015-03-01',1,1,60,0,0,'','0000-00-00','',1,10,'',0,1,'2015-05-12',1,'2015-05-12',1,0);
/*!40000 ALTER TABLE `subcon_payment_receive_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
