<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if($db_type==2 || $db_type==1 )
{
	$mrr_date_check="and to_char(insert_date,'YYYY')=".date('Y',time())."";
	$concat="";
	$concat_coma="||";
}
else if ($db_type==0)
{
	$mrr_date_check="and year(insert_date)=".date('Y',time())."";
	$concat="concat";
	$concat_coma=",";

}

//--------------------------------------------------------------------------------------------
$trim_group_arr = return_library_array("select id, trim_uom from lib_item_group","id","trim_uom");
$machine_arr = return_library_array("select id, machine_no from lib_machine_name","id","machine_no");

//load drop down company location
if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location", 120, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select --", $selected, "load_drop_down( 'requires/general_item_issue_controller',document.getElementById('cbo_company_name').value+'_'+this.value, 'load_drop_down_floor', 'floor_td' );",0 );     	 
	exit();
}

if ($action=="load_drop_down_itemgroupPop")
{	   
	echo create_drop_down( "cbo_item_group", 200, "select id,item_name from lib_item_group where item_category=$data and status_active=1 and is_deleted=0 order by item_name","id,item_name", 1, "-- Select --", 0, "load_drop_down( 'requires/general_item_receive_controller', this.value, 'load_drop_down_uom', 'uom_td' );","" );  	 
	exit();
}

if ($action=="load_drop_down_floor")
{
	$data=explode("_",$data);
	$company_id=$data[0];
	$location_id=$data[1];
	if($location_id==0 || $location_id=="") $location_cond=""; else $location_cond=" and b.location_id=$location_id";
	
	echo create_drop_down( "cbo_floor_id", 130, "select a.id, a.floor_name from lib_prod_floor a, lib_machine_name b where a.id=b.floor_id and b.company_id=$company_id and b.status_active=1 and b.is_deleted=0 $location_cond group by a.id, a.floor_name order by a.floor_name","id,floor_name", 1, "-- Select Floor --", 0, "load_drop_down( 'requires/general_item_issue_controller',document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_machine_category').value+'_'+this.value, 'load_drop_machine', 'machine_td' );","" );
  exit();	 
}
if ($action=="load_drop_machine")
{
	$data=explode("_",$data);
	$company_id=$data[0];
	$machine_category=$data[1];
	$floor_id=$data[2];
	if($machine_category==0 || $machine_category=="") $machine_cond=""; else $machine_cond=" and category_id=$machine_category";
	if($floor_id==0 || $floor_id=="") $floor_cond=""; else $floor_cond=" and floor_id=$floor_id";
	
	echo create_drop_down( "cbo_machine_name", 120, "select id, machine_no as machine_name from lib_machine_name where  company_id=$company_id and status_active=1 and is_deleted=0 and is_locked=0 $floor_cond $machine_cond order by machine_no","id,machine_name", 1, "-- Select Machine --", 0, "","" );
	exit();
}

//load drop down company department
if ($action=="load_drop_down_department")
{
	//echo "select a.id,a.department_name from  lib_department a, lib_division b where b.id=a.division_id and a.status_active =1 and a.is_deleted=0 and b.company_id='$data' order by department_name";die;
	echo create_drop_down( "cbo_department", 120, "select a.id,a.department_name from  lib_department a, lib_division b where b.id=a.division_id and a.status_active =1 and a.is_deleted=0 and b.company_id='$data' order by department_name","id,department_name", 1, "-- Select --", $selected, "load_drop_down( 'requires/general_item_issue_controller', this.value, 'load_drop_down_section', 'section_td' );",0 );     	 
	exit();
}

//load drop down company section
if ($action=="load_drop_down_section")
{
	echo create_drop_down( "cbo_section", 120, "select id,section_name from lib_section where status_active =1 and is_deleted=0 and department_id='$data' order by section_name","id,section_name", 1, "-- Select --", $selected, "",0 );     	 
	exit();
}

//load drop down store
if ($action=="load_drop_down_store")
{	   
	//echo create_drop_down( "cbo_store_name", 130, "select a.id,a.store_name from lib_store_location a, lib_store_location_category b where a.id=b.store_location_id and b.category_type in (8,9,10,11) and a.status_active=1 and a.is_deleted=0 and FIND_IN_SET($data,a.company_id) group by a.id order by a.store_name","id,store_name", 1, "-- Select --", "", "","" ); 
	echo create_drop_down( "cbo_store_name", 130, "select a.id,a.store_name from lib_store_location a, lib_store_location_category b where a.id=b.store_location_id and b.category_type in (8,9,10,11) and a.status_active=1 and a.is_deleted=0 and a.company_id=$data group by a.id,a.store_name order by a.store_name","id,store_name", 1, "-- Select --", "", "","" ); 	 
	exit();
}
 
//load drop down item group
if ($action=="load_drop_down_itemgroup")
{	   
	echo create_drop_down( "cbo_item_group", 150, "select id,item_name from lib_item_group where item_category=$data and status_active=1 and is_deleted=0 order by item_name","id,item_name", 1, "-- Select --", 0, "load_drop_down( 'requires/general_item_issue_controller', this.value, 'load_drop_down_uom', 'uom_td' );","" );  	 
	exit();
}   

//load drop down uom
if ($action=="load_drop_down_uom")
{	   
	if($data==0) $uom=0; else $uom=$trim_group_arr[$data];
	echo create_drop_down( "cbo_uom", 130, $unit_of_measurement, "", 1, "-- Select --", $uom , "", 1);  	 
	exit();
} 

if ($action=="item_description_popup")
{
	echo load_html_head_contents("Item popup", "../../../", 1, 1,'','1','');	
	extract($_REQUEST);
?>
<script> 
	function js_set_value(item_description)
	{
  		 $("#item_description_all").val(item_description);
  		//$("#item_description_all").val('lktoilix sdoi;f il;of opod loiioo;potg09p pgsaos 1205 050');
 		parent.emailwindow.hide(); 
	} 
</script>
</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="200" class="must_entry_caption">Item Category</th>
                    <th width="200">Item Group</th>
                    <th width="200">Storo Name</th>
                    <th width="250"><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>                    
                    <td>
                        <?php  
                           // $search_by = array(1=>'Return Number');
							//$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							//function create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index )
							
							echo create_drop_down( "cbo_item_category", 200, $item_category,"", 1, "-- Select --", 0, "load_drop_down( 'general_item_issue_controller', this.value, 'load_drop_down_itemgroupPop', 'item_group_td' );", 0,"","","","1,2,3,5,6,7,12,13,14" );
                        ?>
                    </td>
                    <td width="" align="center" id="item_group_td">
                    	<?php  
                            //$search_by = array(1=>'Return Number');
							//$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_item_group", 200, $blank_array, "", 1, "-- Select --", 0, "", 0,"" );
                        ?>	
                    </td>
                    <td align="center">
                        <?php  
                            //$search_by = array(1=>'Return Number');
							//$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							$company_id=str_replace("'","",$company_id);
							echo create_drop_down( "cbo_store_name", 200, "select id,store_name from lib_store_location where company_id=$company_id and status_active=1 and is_deleted=0 order by store_name", "id,store_name", 1, "-- Select --", 0, "", 0,"" );
                        ?>
                    </td>
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_item_category').value+'_'+document.getElementById('cbo_item_group').value+'_'+document.getElementById('cbo_store_name').value+'_'+<?php echo $company_id; ?>, 'create_item_search_list_view', 'search_div', 'general_item_issue_controller', 'setFilterGrid(\'tbl_serial\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
                    <!-- Hidden field here-------->
                     
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
      <?php
		
}


if ($action=="create_item_search_list_view")
{
	
	//echo  create_list_view ( "list_view","Category,Group,Description,Store Name,Current Stock", "150,150,200,150,150","850","250",0, $sql, "js_set_value", "id,item_description,current_stock,item_category_id,item_group_id", "", 1, "item_category_id,item_group_id,0,0", $arr, "item_category_id,item_group_id,item_description,current_stock", "0,1", 'setFilterGrid("list_view",-1);','0,0,0,1','',0);
	?>
    <table width="850" cellspacing="0" cellpadding="0" border="0" class="rpt_table" id="" >
        <thead>
            <tr>                	 
                <th width="50">Serial No</th>
                <th width="70">Product Id</th>
                <th width="110">Category</th>
                <th width="110">Group</th>
                <th width="110">Sub Group</th>
                <th width="200">Description</th>
                <th width="110">Store Name</th>
                <th width="90">Current Stock</th> 
            </tr>
        </thead>
    </table>
    <table width="850" cellspacing="0" cellpadding="0" border="0" class="rpt_table" id="tbl_serial" >
        <tbody> 
            <?php
            $ex_data = explode("_",$data);
            $item_category_sql = $ex_data[0];
            $item_group = $ex_data[1];
            $store_name = $ex_data[2];
            $company = $ex_data[3];
            if ($item_category_sql!=0) $item_category_sql=" and a.item_category=$item_category_sql"; else { echo "Please Select item category."; die; };
			//echo $item_category;
            if( $item_group!=0 )  $item_group=" and b.item_group_id='$item_group'"; else $item_group="";
            if( $store_name!=0 )  $store_name=" and a.store_id='$store_name'"; else $store_name="";
            //echo $company;die;
            /*$sql="select a.store_id, b.id as id, sum(case when a.transaction_type in(1,4) then a.balance_qnty else 0 end) as receive, sum(case when a.transaction_type=2 then a.balance_qnty else 0 end) as issue, a.item_category, b.item_group_id,b.sub_group_name,$concat(b.item_description $concat_coma ',' $concat_coma b.item_size) as des, a.store_id
            from  inv_transaction a, product_details_master b
            where a.prod_id=b.id and a.company_id=$company $item_category_sql $item_group $store_name  
			group by 
					a.prod_id, b.id, b.item_group_id,b.sub_group_name,b.item_description, b.item_size, a.store_id, a.item_category";
					*/
			$sql="select  b.id as id, (sum(case when a.transaction_type in(1,4) then a.cons_quantity else 0 end) - sum(case when a.transaction_type in(2,3) then a.cons_quantity else 0 end)) as balance_stock, sum(case when a.transaction_type in(1,4) then a.balance_qnty else 0 end) as receive, sum(case when a.transaction_type=2 then a.balance_qnty else 0 end) as issue, b.current_stock, b.item_category_id, b.item_group_id,b.sub_group_name,$concat(b.item_description $concat_coma ',' $concat_coma b.item_size) as des, b.store_id
            from  inv_transaction a, product_details_master b
            where a.prod_id=b.id and a.company_id=$company $item_category_sql $item_group $store_name  
			group by 
					 b.id,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description, b.item_size,b.current_stock, b.store_id";
            //echo $sql;
            $itemgroup_arr = return_library_array("select id,item_name from lib_item_group where item_category not in (1,2,3,5,6,7,12,13,14) and status_active=1 and is_deleted=0",'id','item_name');
            $store_arr = return_library_array("select id,store_name from lib_store_location where company_id=$company and status_active=1 and is_deleted=0 order by store_name",'id','store_name');
            $arr=array(0=>$item_category,1=>$itemgroup_arr,3=>$store_arr);
            $result=sql_select($sql);
            $i=1;
            foreach($result as $row)
            {
            if ($i%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
            
            ?>
            <input type="hidden" id="item_description_all" value="" style=" width:300px;" />
            <tr bgcolor="<?php echo $bgcolor; ?>"  onClick='js_set_value("<?php echo $row[csf('id')]; ?>*<?php echo $row[csf('des')] ;?>*<?php echo $row[csf('balance_stock')];  //echo ($row[csf('receive')]-$row[csf('issue')]) ; ?>*<?php echo $row[csf('item_category_id')] ; ?>*<?php echo $row[csf('item_group_id')] ; ?>*<?php echo $row[csf('store_id')] ; ?>")' id="" style="cursor:pointer">
                <td width="50"><?php echo $i;  ?></td>
                <td align="center" width="70"><?php echo $row[csf('id')]; ?></td>
                <td width="110"><?php echo $item_category[$row[csf('item_category_id')]]; ?></td>
                <td width="110"><?php echo $itemgroup_arr[$row[csf('item_group_id')]] ; ?></td>
                <td width="110"><?php echo $row[csf('sub_group_name')] ; ?></td>
                <td width="200"><?php echo $row[csf('des')] ; ?></td>
                <td width="110"><?php echo $store_arr[$row[csf('store_id')]] ; ?></td>
                <td align="right" width="90"><?php  echo $row[csf('balance_stock')]; ; ?></td>
            </tr> 
            <?php 
            
            $i++;
            }
            
            ?>
        </tbody>
    </table>   
    <?php
    
		  
}






if($action=="serial_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST); 
	//echo $txt_received_id; die;
	//echo $current_prod_id; die;

 	$serialStringID = str_replace("'","",$serialStringID);
 	//$serialStringNo = str_replace("'","",$serialStringNo);
	$txt_received_id = str_replace("'","",$txt_received_id);
	$current_prod_id = str_replace("'","",$current_prod_id);
	
 	?>
	<script>
	var selected_id = new Array();
	var selected_no = new Array();	
	
	 
	var serialNoArr="<?php echo $serialStringID; ?>";
 	var chk_selected_no = new Array();
	var chk_selected_id = new Array();
	if(serialNoArr!=""){chk_selected_no=serialNoArr.split(",");}
	
	 
	
	function check_all_data() 
	{
		var tbl_row_count = document.getElementById( 'hidden_all_id' ).value.split(","); 
 		//tbl_row_count = tbl_row_count-1;
		for( var i = 0; i < tbl_row_count.length; i++ ) {
 			if( jQuery.inArray( $('#txt_serial_id' + tbl_row_count[i]).val(), chk_selected_id ) != -1 )
			js_set_value( tbl_row_count[i] );
		}
	}
	
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
				//x.style.backgroundColor = ( $serialStringID != "")? newColor : origColor;
			}
		} 
		
	function js_set_value( str ) { //alert(str);
		toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
		
		if( jQuery.inArray( $('#txt_serial_id' + str).val(), selected_id ) == -1 ) {
			selected_id.push( $('#txt_serial_id' + str).val() );
			selected_no.push( $('#txt_serial_no' + str).val() );
 		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == $('#txt_serial_id' + str).val() ) break;
			}
			selected_id.splice( i, 1 );
			selected_no.splice( i, 1 );
		}
		var id = '';	var no = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			no += selected_no[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		no = no.substr( 0, no.length - 1 );
  		$('#txt_string_id').val( id );
		$('#txt_string_no').val( no );
	}
	 
	function fn_onClosed()
	{
		var txt_string = $('#txt_string').val();
		if(txt_string==""){ alert("Please Select The Serial"); return;}
		parent.emailwindow.hide();
	}
	 
	</script>
	</head>
	<body>
	<div align="center" style="width:100%;" >
	<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
    	<table width="300" cellspacing="0" cellpadding="0" border="0" class="rpt_table" id="tbl_header" >
				<thead>
					<tr>                	 
						<th width="300">Serial No</th>
 					</tr>
				</thead>
        </table>        
        <div style="width:300px; min-height:220px">
		<table width="300" cellspacing="0" cellpadding="0" border="0" class="rpt_table" id="tbl_serial" style="overflow:scroll; min-height:200px" >
 				<tbody>
                	<?php
						$i=1;
						$sql="select id,serial_no from inv_serial_no_details where prod_id=$current_prod_id and is_issued=0";
						//echo $sql;die;
						$result = sql_select($sql);
						$count=count($result );
						foreach($result as $row) 
						{
							if ($i%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							if($new_data=="") $new_data=$row[csf("id")]; else $new_data .=",".$row[csf("id")];				
						?>	
							<tr bgcolor="<?php echo $bgcolor; ?>"  onClick="js_set_value(<?php echo $row[csf("id")]; ?>)" id="search<?php echo $row[csf("id")]; ?>" style="cursor:pointer">
								<td  width="300">
									<?php echo trim($row[csf("serial_no")]); ?> 
									<input type="hidden" id="txt_serial_id<?php echo $row[csf("id")]; ?>" value="<?php echo $row[csf("id")]; ?>" >
                                    <input type="hidden" id="txt_serial_no<?php echo $row[csf("id")]; ?>" value="<?php echo $row[csf("serial_no")]; ?>" >
								</td>
									<?php  
									
									if($count==$i)
									{
									?> 
                                    <input type="hidden" id="hidden_all_id" value="<?php echo $new_data; ?>" >
                                    <?php } ?>
							</tr> 
					<?php 
						
							$i++;
						}

				?>
				</tbody>         
			</table>  
            </div>
            <div><input type="button" name="btn_close" class="formbutton" style="width:100px" value="Close" onClick="fn_onClosed()" /></div>  
            <!-- Hidden field here-------->
			<input type="hidden" id="txt_string_id" value="" />
            <input type="hidden" id="txt_string_no" value="" />				 
			<!-- ---------END-------------> 
			</form>
	   </div>
	</body>           
	<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
    
    <script>
	//alert(serialNoArr);
		if( serialNoArr!="" )
		{
			serialNoArr=serialNoArr.split(",");
			for(var k=0;k<serialNoArr.length; k++)
			{
				js_set_value(serialNoArr[k] );
				//alert(serialNoArr[k]);
			}
		}
	</script>
	</html>
	<?php
}
if($action=="order_popup")
{
echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
extract($_REQUEST); 
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer where find_in_set($company,tag_company) and status_active=1 and is_deleted=0 order by buyer_name",'id','buyer_name');
?>
	<script>
		$(document).ready(function(e) {
            $("#txt_search_common").focus();
        });
		
		function search_populate(str)
		{
			if(str==0) 
			{		
				document.getElementById('search_by_th_up').innerHTML="Order No";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';		 
			}
			else if(str==1) 
			{
				document.getElementById('search_by_th_up').innerHTML="Style Ref. Number";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';
			}
			else //if(str==2)
			{
				var buyer_name = '<option value="0">--- Select Buyer ---</option>';
				<?php 			
				foreach($buyer_arr as $key=>$val)
				{
					echo "buyer_name += '<option value=\"$key\">".($val)."</option>';";
				} 
				?>
				document.getElementById('search_by_th_up').innerHTML="Select Buyer Name";
				document.getElementById('search_by_td').innerHTML='<select	name="txt_search_common" style="width:230px " class="combo_boxes" id="txt_search_common">'+ buyer_name +'</select>';
			}																																													
		}
	
	function js_set_value(id,po_no)
	{ 
		$("#hidden_string").val(id+"_"+po_no); 
   		parent.emailwindow.hide();
 	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="750" cellspacing="0" cellpadding="0" class="rpt_table" align="center">
    		<tr>
        		<td align="center" width="100%">
            		<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                   		 <thead>                	 
                        	<th width="130">Search By</th>
                        	<th  width="180" align="center" id="search_by_th_up">Enter Order Number</th>
                        	<th width="200">Date Range</th>
                        	<th width="80"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
                    	</thead>
        				<tr>
                    		<td width="130">  
							<?php 
							$searchby_arr=array(0=>"Order No",1=>"Style Ref. Number",2=>"Buyer Name");
							echo create_drop_down( "txt_search_by", 130, $searchby_arr,"", 1, "-- Select Sample --", $selected, "search_populate(this.value)",0 );
  							?>
                    		</td>
                   			<td width="180" align="center" id="search_by_td">				
								<input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" onKeyDown="if (event.keyCode == 13) document.getElementById('btn_show').click()" />			
            				</td>
                    		<td align="center">
                            	<input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px"> To
					  			<input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 		</td> 
            		 		<td align="center">
                     			<input type="button" name="btn_show" id="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_po_search_list_view', 'search_div', 'general_item_issue_controller', 'setFilterGrid(\'tbl_po_list\',-1)')" style="width:100px;" />
                            </td>
        				</tr>
             		</table>
          		</td>
        	</tr>
        	<tr>
            	<td  align="center" height="40" valign="middle">
					<?php echo load_month_buttons();  ?> 
                    <input type="hidden" id="hidden_string">
          		</td>
            </tr>
        	<tr>
            <td align="center" valign="top" id="search_div"> 
            </td>
        	</tr>
    </table>    
    </form>
	</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_po_search_list_view")
{
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	$garments_nature = $ex_data[5];
	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==0)
			$sql_cond = " and b.po_number like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==1)
			$sql_cond = " and a.style_ref_no like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==2)
			$sql_cond = " and a.buyer_name=trim('$txt_search_common')";		
 	}
	if($txt_date_from!="" || $txt_date_to!="") $sql_cond .= " and b.shipment_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if(trim($company)!="") $sql_cond .= " and a.company_name='$company'";
		
 	$sql = "select b.id,a.order_uom,a.buyer_name,a.company_name,a.total_set_qnty,a.set_break_down, a.job_no,a.style_ref_no,a.gmts_item_id,a.location_name,b.shipment_date,b.po_number,b.po_quantity ,b.plan_cut
			from wo_po_details_master a, wo_po_break_down b 
			where
			a.job_no = b.job_no_mst and
			a.status_active=1 and 
			a.is_deleted=0 
			$sql_cond"; 
	//echo $sql;die;
	$result = sql_select($sql);
 	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	?>
    <div style="width:820px;">
     	<table cellspacing="0" width="100%" class="rpt_table">
            <thead>
                <th width="50" >SL</th>
                <th width="100" >Shipment Date</th>
                <th width="100" >Order No</th>
                <th width="150" >Buyer</th>
                <th width="150" >Style</th>
                 <th width="100" >Order Qnty</th>
                <th>Company Name</th>
            </thead>
     	</table>
     </div>
     <div style="width:820px; max-height:220px;overflow-y:scroll;" >	 
        <table cellspacing="0" width="100%" class="rpt_table" id="tbl_po_list">
			<?php
			$i=1;
            foreach( $result as $row )
            {
                if ($i%2==0)  $bgcolor="#E9F3FF";
                else $bgcolor="#FFFFFF";
 					?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value(<?php echo $row[csf("id")];?>,'<?php echo $row[csf("po_number")];?>');" > 
							<td width="50" align="center"><?php echo $i; ?></td>
							<td width="100" align="center"><?php echo change_date_format($row[csf("shipment_date")]);?></td>		
							<td width="100" align="center"><?php echo $row[csf("po_number")]; ?></td>
							<td width="150"><?php echo $buyer_arr[$row[csf("buyer_name")]];  ?></td>	
							<td width="150"><?php echo $row[csf("style_ref_no")]; ?></td>
 							<td width="100" align="right"><?php echo $row[csf("po_quantity")];?> </td>
							<td><?php  echo $company_arr[$row[csf("company_name")]];?> </td> 	
						</tr>
					<?php 
				$i++;
            }
   		?>
			</table>
		</div> 
	<?php	
exit();	
}
 
//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	 	 
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 	 
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		//---------------Check Duplicate product in Same return number ------------------------//
		$txt_prod_id=str_replace("'","",$current_prod_id);
		$duplicate = is_duplicate_field("b.id","inv_issue_master a, inv_transaction b","a.id=b.mst_id and a.id=$txt_system_id and b.prod_id=$txt_prod_id and b.transaction_type=2"); 
		if($duplicate==1 && str_replace("'","",$txt_system_no)!="") 
		{
			check_table_status( $_SESSION['menu_id'],0);
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			die;
		}
		
 		//issue master table entry here START---------------------------------------//		
 		if( str_replace("'","",$txt_system_no) == "" ) //new insert
		{
			$id=return_next_id("id", "inv_issue_master", 1);		
			$new_mrr_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GIS', date("Y",time()), 5, "select issue_number_prefix,issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_name and entry_form=21 $mrr_date_check order by id DESC ", "issue_number_prefix", "issue_number_prefix_num" ));
			
			$field_array_master="id,issue_number_prefix, issue_number_prefix_num, issue_number, issue_purpose, entry_form, company_id, issue_date, challan_no, req_no,remarks, inserted_by, insert_date";
			$data_array_master="(".$id.",'".$new_mrr_number[1]."','".$new_mrr_number[2]."','".$new_mrr_number[0]."',".$cbo_issue_purpose.",21,".$cbo_company_name.",".$txt_issue_date.",".$txt_challan_no.",".$txt_issue_req_no.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
			//echo $field_array."<br>".$data_array;die;
			//$rID = sql_insert("inv_issue_master",$field_array_master,$data_array_master,1);
 		}
		else //update
		{
			$new_mrr_number[0]=str_replace("'","",$txt_system_no);
			$id=str_replace("'","",$txt_system_id);
			$field_array_master="issue_purpose*company_id*issue_date*challan_no*req_no*remarks*updated_by*update_date";
			$data_array_master="".$cbo_issue_purpose."*".$cbo_company_name."*".$txt_issue_date."*".$txt_challan_no."*".$txt_issue_req_no."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
			//echo "20**".$field_array."<br>".$txt_system_id;die;
			//$rID=sql_update("inv_issue_master",$field_array_master,$data_array_master,"id",$id,1);
 		}
		//issue master table entry here END---------------------------------------//
		 
		//product master table information
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value from product_details_master where id=$txt_prod_id and item_category_id=$cbo_item_category");
		$avg_rate=$stock_qnty=$stock_value=0;
		foreach($sql as $result)
		{
			$avg_rate = $result[csf("avg_rate_per_unit")];
			$stock_qnty = $result[csf("current_stock")];
			$stock_value = $result[csf("stock_value")];
		}
		
		//inventory TRANSACTION table data entry START----------------------------------------------------------//	
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$txt_issue_qnty = str_replace("'","",$txt_issue_qnty);
 		$issue_stock_value = $avg_rate*$txt_issue_qnty;		
		$transactionID = return_next_id("id", "inv_transaction", 1); 				
		$field_array_trans_insert = "id,mst_id,company_id,prod_id,item_category,transaction_type,transaction_date,store_id,order_id,cons_uom,cons_quantity,cons_rate,cons_amount,floor_id,machine_id,machine_category,room,rack,self,bin_box,location_id,department_id,section_id,inserted_by,insert_date";
 		$data_array_trans_insert = "(".$transactionID.",".$id.",".$cbo_company_name.",".$txt_prod_id.",".$cbo_item_category.",2,".$txt_issue_date.",".$cbo_store_name.",".$txt_order_id.",".$cbo_uom.",".$txt_issue_qnty.",".$avg_rate.",".$issue_stock_value.",".$cbo_floor_id.",".$cbo_machine_name.",".$cbo_machine_category.",".$cbo_room.",".$cbo_rack.",".$cbo_self.",".$cbo_binbox.",".$cbo_location.",".$cbo_department.",".$cbo_section.",'".$user_id."','".$pc_date_time."')"; 
		//echo $field_array."<br>".$data_array;die;
		
		//$transID = sql_insert("inv_transaction",$field_array_trans_insert,$data_array_trans_insert,1);
		
		//inventory TRANSACTION table data entry  END----------------------------------------------------------//
		
		//if LIFO/FIFO then START -----------------------------------------//
		$field_array_lifu_fifu = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array_lifu_fifu = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
		$data_array_lifu_fifu="";
		$updateID_array_lifu_fifu=array();
		$update_data_lifu_fifu=array();
		$issueQnty = $txt_issue_qnty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);
		if($db_type==0)
		{		
			$returnString=return_field_value("concat(store_method,'_',allocation)","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17 and item_category_id=$cbo_item_category and status_active=1 and is_deleted=0");
		}
		else
		{
			$returnString=return_field_value("(store_method || '_' || allocation) as store_data","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17 and item_category_id=$cbo_item_category and status_active=1 and is_deleted=0","store_data");
		}
		
		$expString = explode("_",$returnString); 
		$isLIFOfifo = $expString[0];
		$check_allocation = $expString[1];
		 
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC"; 
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in (1,4,5) and item_category=$cbo_item_category order by transaction_date $cond_lifofifo");			
		foreach($sql as $result)
		{				
			$recv_trans_id = $result[csf("id")]; // this row will be updated
			$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];
			$cons_rate = $result[csf("cons_rate")];
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array_lifu_fifu!="") $data_array_lifu_fifu .= ",";  
				$data_array_lifu_fifu .= "(".$mrrWiseIsID.",".$recv_trans_id.",".$transactionID.",21,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array_lifu_fifu[]=$recv_trans_id; 
				$update_data_lifu_fifu[$recv_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array_lifu_fifu!="") $data_array_lifu_fifu .= ",";  
				$data_array_lifu_fifu .= "(".$mrrWiseIsID.",".$recv_trans_id.",".$transactionID.",21,".$txt_prod_id.",".$balance_qnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array_lifu_fifu[]=$recv_trans_id; 
				$update_data_lifu_fifu[$recv_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
		}//end foreach
 		// LIFO/FIFO then END-----------------------------------------------//
		
		
		//mrr wise issue data insert here----------------------------//
		/*$mrrWiseIssueID=true;
		if($data_array_lifu_fifu!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array_lifu_fifu,$data_array_lifu_fifu,1);
		}
		//transaction table stock update here------------------------//
		$upTrID=true;
		if(count($updateID_array_lifu_fifu)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_lifu_fifu,$update_data_lifu_fifu,$updateID_array_lifu_fifu),1);
		}*/		
		 
 		//product master table data UPDATE START----------------------//
  		$currentStock   = $stock_qnty-$txt_issue_qnty;
		$StockValue	 	= $stock_value-($txt_issue_qnty*$avg_rate);
		$avgRate	 	= number_format($StockValue/$currentStock,$dec_place[3],'.',''); 
		
		$field_array_product	= "last_issued_qnty*current_stock*stock_value*updated_by*update_date"; 
		$data_array_product	= "".$txt_issue_qnty."*".$currentStock."*".number_format($StockValue,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"; 
		//------------------ product_details_master END--------------//
		
		if( str_replace("'","",$txt_system_no) == "" ) //new insert
		{
			$rID = sql_insert("inv_issue_master",$field_array_master,$data_array_master,1);
 		}
		else //update
		{
			$rID=sql_update("inv_issue_master",$field_array_master,$data_array_master,"id",$id,1);
 		}
		$transID = sql_insert("inv_transaction",$field_array_trans_insert,$data_array_trans_insert,1);
		$prodUpdate 	= sql_update("product_details_master",$field_array_product,$data_array_product,"id",$txt_prod_id,1);
		$mrrWiseIssueID=true;
		if($data_array_lifu_fifu!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array_lifu_fifu,$data_array_lifu_fifu,1);
		}
		//transaction table stock update here------------------------//
		$upTrID=true;
		if(count($updateID_array_lifu_fifu)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_lifu_fifu,$update_data_lifu_fifu,$updateID_array_lifu_fifu),1);
		}
		
		
		
		 
 		$txt_serial_id 	= trim(str_replace("'","",$txt_serial_id));
		$serialUpdate=true;
 		if($txt_serial_id!="")
		{
			if( strpos(trim($txt_serial_no), ",")>0)
			{
				$se_data=explode(",",str_replace("'","",$txt_serial_no));
				if( (count($se_data)<=str_replace("'","",$txt_issue_qnty)))
				{
					$serialUpdate = execute_query("update inv_serial_no_details set issue_trans_id=$transactionID , is_issued=1 where id in ($txt_serial_id)",1);
				}
				else
				{
					echo "50";
					die;
				}
			}
			else
			{
			$serialUpdate 	= execute_query("update inv_serial_no_details set issue_trans_id=$transactionID, is_issued=1 where id in ($txt_serial_id)",1);
			}
		}
		
		
		
		
		//echo "20**".$rID." && ".$transID." && ".$mrrWiseIssueID." && ".$prodUpdate." && ".$serialUpdate; 
		//mysql_query("ROLLBACK");die; 
 		 
		//release lock table   oci_commit($con); oci_rollback($con); 
		check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $transID && $mrrWiseIssueID && $upTrID && $prodUpdate && $serialUpdate)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_mrr_number[0]."**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_mrr_number[0]."**".$id;
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $transID && $mrrWiseIssueID && $upTrID && $prodUpdate && $serialUpdate)
			{
				oci_commit($con);  
				echo "0**".$new_mrr_number[0]."**".$id;
			}
			else
			{
				oci_rollback($con); 
				echo "10**".$new_mrr_number[0]."**".$id;
			}
		}
		disconnect($con);
		die;
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		if( str_replace("'","",$update_id) == "" ||  str_replace("'","",$txt_system_no)=="" )
		{
			echo "15";exit(); 
		}
		 
		//variable_list=17 is_allocated,  item_category_id=1 is yarn--------------------
		$returnString=return_field_value("$concat(store_method $concat_coma '_' $concat_coma allocation) as store_allocation","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17 and item_category_id=$cbo_item_category and status_active=1 and is_deleted=0","store_allocation");
		$expString = explode("_",$returnString); 
		$isLIFOfifo = $expString[0];
		$check_allocation = $expString[1];
		 
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
		//product master table information
		//before stock update
		$sql = sql_select( "select a.id,a.avg_rate_per_unit,a.current_stock,a.stock_value, b.cons_quantity, b.cons_amount from product_details_master a, inv_transaction b where a.id=b.prod_id and b.id=$update_id and a.item_category_id=$cbo_item_category and b.item_category=$cbo_item_category and b.transaction_type=2" );
		$before_prod_id=$before_issue_qnty=$before_stock_qnty=$before_stock_value=0;
		foreach($sql as $result)
		{
			$before_prod_id 	= $result[csf("id")];
 			$before_stock_qnty = $result[csf("current_stock")];
			$before_stock_value = $result[csf("stock_value")]; 
			$before_issue_qnty = $result[csf("cons_quantity")];
			$before_issue_value = $result[csf("cons_amount")]; 
		}
		
		//current product ID
		$txt_prod_id = str_replace("'","",$current_prod_id);
		$txt_issue_qnty = str_replace("'","",$txt_issue_qnty);
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value from product_details_master where id=$txt_prod_id and item_category_id=$cbo_item_category");
		$curr_avg_rate=$curr_stock_qnty=$curr_stock_value=0;
		foreach($sql as $result)
		{
			$curr_avg_rate 	   = $result[csf("avg_rate_per_unit")];
			$curr_stock_qnty 	 = $result[csf("current_stock")];
			$curr_stock_value 	= $result[csf("stock_value")]; 
		}
		
		//weighted and average rate START here------------------------//
		//product master table data UPDATE START----------------------//		
		$update_array	= "last_issued_qnty*current_stock*stock_value*updated_by*update_date"; //*allocated_qnty*available_qnty
		if($before_prod_id==$txt_prod_id)
		{
			$adj_stock_qnty = $curr_stock_qnty+$before_issue_qnty-$txt_issue_qnty; // CurrentStock + Before Issue Qnty - Current Issue Qnty 
 			$adj_stock_val  = $curr_stock_value+$before_issue_value-($txt_issue_qnty*$curr_avg_rate); // CurrentStockValue + Before Issue Value - Current Issue Value
			$adj_avgrate	= number_format($adj_stock_val/$adj_stock_qnty,$dec_place[3],'.','');
			 
			$data_array_same		= "".$txt_issue_qnty."*".$adj_stock_qnty."*".number_format($adj_stock_val,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"; 
 			//$query1 		= sql_update("product_details_master",$update_array,$data_array_same,"id",$before_prod_id,0);
			
			//now current stock
			$curr_avg_rate 		= $adj_avgrate;
			$curr_stock_qnty 	= $adj_stock_qnty;
			$curr_stock_value 	= $adj_stock_val;
		}
		else
		{
			$updateID_array = $update_data = array();
			//before product adjust
			$adj_before_stock_qnty 	= $before_stock_qnty+$before_issue_qnty; // CurrentStock + Before Issue Qnty
			$adj_before_stock_val  	 = $before_stock_value+$before_issue_value; // CurrentStockValue + Before Issue Value
			$adj_before_avgrate	   = number_format($adj_before_stock_val/$adj_before_stock_qnty,$dec_place[3],'.','');
			 
			$updateID_array_before[]=$before_prod_id;
			$update_data_before[$before_prod_id]=explode("*",("".$adj_before_avgrate."*".$txt_issue_qnty."*".$adj_before_stock_qnty."*".number_format($adj_before_stock_val,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
			//$query_beofore_product=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data_before,$updateID_array_before),0);
  			
 			//current product adjust
			$adj_curr_stock_qnty  = $curr_stock_qnty-$txt_issue_qnty; // CurrentStock + Before Issue Qnty
			$adj_curr_stock_val   = $curr_stock_value-($txt_issue_qnty*$curr_avg_rate); // CurrentStockValue + Before Issue Value
			$adj_curr_avgrate	 = number_format($adj_curr_stock_val/$adj_curr_stock_qnty,$dec_place[3],'.','');
			//for current product------------- 
			
			$availableChk = $curr_stock_qnty>=$txt_issue_qnty ?  true : false;
			$msg="Issue Quantity is exceed the current Stock Quantity";
			
			if($availableChk==false)
			{
				//check_table_status( $_SESSION['menu_id'],0);
				if($db_type==0) {mysql_query("ROLLBACK");}
				else {oci_rollback($con);}
				echo "11**".$msg;
				exit();
			}
			
			$updateID_array_current[]=$txt_prod_id;
			$update_data_current[$txt_prod_id]=explode("*",("".$txt_issue_qnty."*".$adj_curr_stock_qnty."*".number_format($adj_curr_stock_val,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
			//$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data_current,$updateID_array_current),0);
			
			//now current stock
			$curr_avg_rate 		= $adj_curr_avgrate;
			$curr_stock_qnty 	= $adj_curr_stock_qnty;
			$curr_stock_value 	= $adj_curr_stock_val;
		}
  		//------------------ product_details_master END--------------//
		//weighted and average rate END here-------------------------//
		 		
 		//transaction table START--------------------------//
		$update_array_trans = "balance_qnty*balance_amount*updated_by*update_date";
		$sql = sql_select("select a.id,a.balance_qnty,a.balance_amount,b.issue_qnty,b.rate,b.amount from inv_transaction a, inv_mrr_wise_issue_details b where a.id=b.recv_trans_id and b.issue_trans_id=$update_id and b.entry_form=21"); 
		$updateID_array_trans = array();
		$update_data_trans = array();
		foreach($sql as $result)
		{
			$adjBalance = $result[csf("balance_qnty")]+$result[csf("issue_qnty")];
			$adjAmount = $result[csf("balance_amount")]+$result[csf("amount")];
			$updateID_array_trans[]=$result[csf("id")]; 
			$update_data_trans[$result[csf("id")]]=explode("*",("".$adjBalance."*".$adjAmount."*'".$user_id."'*'".$pc_date_time."'"));
			
			$trans_data_array[$result[csf("id")]]['qnty']=$adjBalance;
			$trans_data_array[$result[csf("id")]]['amnt']=$adjAmount;
		}
		
		

		/*$query2=true; 
		if(count($updateID_array_trans)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_trans,$update_data_trans,$updateID_array_trans),0);
		}*/
		//transaction table END----------------------------//
		
		//LIFO/FIFO  START here------------------------//
		/*$query3=true;
		if(count($update_data_trans)>0)
		{
			 $updateIDArray = implode(",",$update_data_trans);
			 $query3 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=21",0);
		}*/
		
		//############## SAVE POINT START  ###################
		$savepoint="updatesql";
		mysql_query("SAVEPOINT $savepoint");
		//############## SAVE POINT END    ###################
		
		//****************************************** NEW ENTRY START *****************************************//
		
		//issue master update START--------------------------------------//
		$field_array_update_issue="issue_purpose*company_id*issue_date*challan_no*req_no*remarks*updated_by*update_date";
		$data_array_update_issue="".$cbo_issue_purpose."*".$cbo_company_name."*".$txt_issue_date."*".$txt_challan_no."*".$txt_issue_req_no."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
		/*if(trim(str_replace("'","",$txt_system_id))!="")
		{
			$rID=sql_update("inv_issue_master",$field_array_update_issue,$data_array_update_issue,"id",$txt_system_id,1);
		}*/
		//echo $field_array."<br>".$data_array;."-".;
		//issue master update END---------------------------------------// 
 		 
		//inventory TRANSACTION table data UPDATE START----------------------------------------------------------//	
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$txt_issue_qnty = str_replace("'","",$txt_issue_qnty);
		$avg_rate = $curr_avg_rate; // asign current rate
 		$issue_stock_value = $avg_rate*$txt_issue_qnty;
		
		$field_array_again = "company_id*prod_id*item_category*transaction_type*transaction_date*store_id*order_id*cons_uom*cons_quantity*cons_rate*cons_amount*floor_id*machine_id*machine_category*room*rack*self*bin_box*location_id*department_id*section_id*inserted_by*insert_date";
 		$data_array_again = "".$cbo_company_name."*".$txt_prod_id."*".$cbo_item_category."*2*".$txt_issue_date."*".$cbo_store_name."*".$txt_order_id."*".$cbo_uom."*".$txt_issue_qnty."*".$avg_rate."*".$issue_stock_value."*".$cbo_floor_id."*".$cbo_machine_name."*".$cbo_machine_category."*".$cbo_room."*".$cbo_rack."*".$cbo_self."*".$cbo_binbox."*".$cbo_location."*".$cbo_department."*".$cbo_section."*'".$user_id."'*'".$pc_date_time."'"; 
		//echo $field_array."<br>".$data_array;."-".;
		
		
		//$transID = sql_update("inv_transaction",$field_array_again,$data_array_again,"id",$update_id,0);
 		//inventory TRANSACTION table data UPDATE  END----------------------------------------------------------//
		
		//if LIFO/FIFO then START -----------------------------------------//
		$field_array_mrr = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array_tran = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
/*		$updateID_array_tran_up=array();
		$update_data_tran_up=array();
		$update_data_mrr_insert=array();
*/		$issueQnty = $txt_issue_qnty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);
		
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC";		 
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in (1,4,5) and item_category=$cbo_item_category order by transaction_date $cond_lifofifo");
			
 		foreach($sql as $result)
		{				
			$issue_trans_id = $result[csf("id")]; // this row will be updated
			if($trans_data_array[$issue_trans_id]['qnty']=="")
			{
				$balance_qnty = $result[csf("balance_qnty")];
				$balance_amount = $result[csf("balance_amount")];
			}
			else
			{
				$balance_qnty = $trans_data_array[$issue_trans_id]['qnty'];
				$balance_amount = $trans_data_array[$issue_trans_id]['amnt'];
			}
			
			/*$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];*/
			$cons_rate = $result[csf("cons_rate")]; 
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($update_data_mrr_insert!="") $update_data_mrr_insert .= ",";  
				$update_data_mrr_insert .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",21,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array_tran_up[]=$issue_trans_id; 
				$update_data_tran_up[$issue_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				
				//for insert
				if($update_data_mrr_insert!="") $update_data_mrr_insert .= ",";  
				$update_data_mrr_insert .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",21,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//echo "20**".$data_array;die;
				//for update
				$updateID_array_tran_up[]=$issue_trans_id; 
				$update_data_tran_up[$issue_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
		}//end foreach
 		// LIFO/FIFO then END-----------------------------------------------//
		//echo "insert into inv_mrr_wise_issue_details ($field_array_mrr) values $update_data_mrr_insert";die;
		//mrr wise issue data insert here----------------------------//
		/*$mrrWiseIssueID=true;
		if($update_data_mrr_insert!="")
		{	 
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array_mrr,$update_data_mrr_insert,0);
		}
		 
		//transaction table stock update here------------------------//
		$upTrID=true;
		if(count($updateID_array_tran_up)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_tran,$update_data_tran_up,$updateID_array_tran_up),0);
		}*/
				
 		//****************************************** NEW ENTRY END *****************************************//
		
		//echo "20**".$query1." && ".$query2." && ".$query3." && ".$rID." && ".$transID." && ".$upTrID." && ".$mrrWiseIssueID." && ".$serialUpdate; 
		//mysql_query("ROLLBACK");die;
		
		
		//****************************************** All query execute Bellow*****************************************//
		$query1=$query_beofore_product=$query2=$query3=$rID=$transID=$mrrWiseIssueID=$upTrID=$serialUpdate=$serialDelete=true;
		if($before_prod_id==$txt_prod_id)
		{
 			$query1 		= sql_update("product_details_master",$update_array,$data_array_same,"id",$before_prod_id,0);
		}
		else
		{
			$query_beofore_product=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data_before,$updateID_array_before),0);
			$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data_current,$updateID_array_current),0);
		}
		
		
		
		if(count($updateID_array_trans)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_trans,$update_data_trans,$updateID_array_trans),0);
		}
		
		
		if(count($update_data_trans)>0)
		{
			 $updateIDArray = implode(",",$update_data_trans);
			 $query3 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=21",0);
		}
		
		
		if(trim(str_replace("'","",$txt_system_id))!="")
		{
			$rID=sql_update("inv_issue_master",$field_array_update_issue,$data_array_update_issue,"id",$txt_system_id,1);
		}
		
		
		
		$transID = sql_update("inv_transaction",$field_array_again,$data_array_again,"id",$update_id,0);
		
		if($update_data_mrr_insert!="")
		{	 
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array_mrr,$update_data_mrr_insert,0);
		}
		//transaction table stock update here------------------------//
		if(count($updateID_array_tran_up)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_tran,$update_data_tran_up,$updateID_array_tran_up),0);
		}
		
		$txt_serial_id 	= trim(str_replace("'","",$txt_serial_id));
 		if($txt_serial_id!="")
		{
			$before_serial_id=trim(str_replace("'","",$before_serial_id));$txt_serial_id=trim(str_replace("'","",$txt_serial_id));$update_id=trim(str_replace("'","",$update_id));
			if( strpos(trim($txt_serial_no), ",")>0)
			{
				$se_data=explode(",",str_replace("'","",$txt_serial_no));
				if( (count($se_data)<=str_replace("'","",$txt_issue_qnty)))
				{
					if($before_serial_id !="")
					{
					$serialDelete=execute_query("update inv_serial_no_details set issue_trans_id=0 , is_issued=0 where id in ($before_serial_id)",0);
					}
					$serialUpdate = execute_query("update inv_serial_no_details set issue_trans_id=$update_id , is_issued=1 where id in ($txt_serial_id)",0);
				}
				else
				{
					echo "50";
					die;
				}
			}
			else
			{
				if($before_serial_id !="")
				{
				$serialDelete	=execute_query("update inv_serial_no_details set issue_trans_id=0 , is_issued=0 where id in ($before_serial_id)",0);
				}
				
				$serialUpdate 	= execute_query("update inv_serial_no_details set issue_trans_id=$update_id , is_issued=1 where id in ($txt_serial_id)",0);
			}
		}
		
		
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($query1 && $query_beofore_product &&  $query2 && $query3 && $rID && $transID && $mrrWiseIssueID && $upTrID && $serialUpdate  && $serialDelete)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$txt_system_id);
			}
			else
			{
				mysql_query("ROLLBACK");
				mysql_query("ROLLBACK TO $savepoint"); 
				echo "10**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$txt_system_id);
			}
		}
		//$query1=$query_beofore_product=$query2=$query3=$rID=$transID=$mrrWiseIssueID=$upTrID=$serialUpdate=$serialDelete    
		if($db_type==2 || $db_type==1 )
		{
			if($query1 && $query_beofore_product &&  $query2 && $query3 && $rID && $transID && $mrrWiseIssueID && $upTrID && $serialUpdate  && $serialDelete)
			{
				oci_commit($con);  
				echo "1**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$txt_system_id);
			}
			else
			{
				oci_rollback($con);  
				echo "10**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$txt_system_id);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		//check update id
		if( str_replace("'","",$update_id) == "" ||  str_replace("'","",$txt_system_no)=="" )
		{
			echo "15";exit(); 
		}
		/*
		//master table delete here---------------------------------------
  		$rID = sql_update("inv_issue_master",'status_active*is_deleted','0*1',"issue_number",$txt_system_no,1);
		$dtlsrID = sql_update("inv_transaction",'status_active*is_deleted','0*1',"id",$update_id,1);
 		*/
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "2**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$txt_system_id);
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$txt_system_id);
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "2**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$txt_system_id);
		}
		disconnect($con);
		die;
	}		
}

if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
<script>
	function js_set_value(sys_id)
	{
 		$("#hidden_sys_id").val(sys_id); // mrr number
		parent.emailwindow.hide();
	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="780" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter Issue No</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr> 
                    <td> 
                        <?php  
 							$search_by = array(1=>'Issue No',2=>'Req No',3=>'Challan No');
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view (document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'general_item_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_sys_id" value="hidden_sys_id" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
 	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$fromDate = $ex_data[2];
	$toDate = $ex_data[3];
	$company = $ex_data[4];
 	$sql_cond="";
	if( $txt_date_from!="" && $txt_date_to!="" ) $sql_cond .= " and issue_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
 	if($company!="" && $company*1!=0) $sql_cond .= " and company_id='$company'";
	
 	$company_arr = return_library_array("select id, company_name from lib_company",'id','company_name');
 	$store_arr = return_library_array("select id, store_name from lib_store_location",'id','store_name');
	
 	if($txt_search_common!="" || $txt_search_common!=0)
	{
		if($txt_search_by==1)
		{	
			$sql_cond .= " and issue_number_prefix_num like '%$txt_search_common%'";			
		}
		else if($txt_search_by==2)
		{		
			$sql_cond .= " and req_no like '%$txt_search_common%'";	
 		}
		else if($txt_search_by==3)
		{
			$sql_cond .= " and challan_no like '%$txt_search_common%'";	
		} 
	}
		
	$sql = "select id,issue_number,issue_basis,issue_purpose,entry_form,company_id,location_id,supplier_id,store_id,issue_date
			from inv_issue_master 
			where status_active=1 and entry_form=21 $sql_cond order by issue_number";
	//echo $sql;
	$result = sql_select( $sql );
	?>
    	<div>
            <div style="width:700px;">
                <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table">
                    <thead>
                        <th width="50">SL</th>
                        <th width="120">Issue No</th>				
                        <th width="120">Date</th>              
                        <th width="120">Purpose</th>               
                        <th width="120">Req No</th>
                        <th width="">Issue Qnty</th> 
                    </thead>
                </table>
             </div>
            <div style="width:700px;overflow-y:scroll; min-height:210px; max-height:210px;" id="search_div" >
                <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" id="list_view">
        <?php	
            $i=1;   
            foreach( $result as $row ){
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
					
				$issuQnty = return_field_value("sum(cons_quantity) as cons_quantity","inv_transaction","mst_id=".$row[csf("id")]." and transaction_type=2 and item_category not in (1,2,3,5,6,7,12,13,14) group by mst_id","cons_quantity");	
        ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer"  onclick="js_set_value('<?php echo $row[csf("id")];?>');"> 
                            <td width="50"><?php echo $i; ?></td>	
                            <td width="120"><p><?php echo $row[csf("issue_number")];?></p></td>              	            			
                            <td width="120"><p><?php echo $row[csf("issue_date")]; ?></p></td>								
                            <td width="120"><p><?php echo $general_issue_purpose[$row[csf("issue_purpose")]]; ?></p></td>					
                            <td width="120"><p><?php echo $row[csf("req_no")]; ?></p></td>
                            <td width="" align="right"><p><?php echo $issuQnty; ?></p></td> 
                        </tr>
                        <?php
                        $i++;
                        }
                        ?>
                </table>
            </div>
        </div>
    <?php
	exit();
}


if($action=="populate_data_from_data")
{
	$sql = "select id,issue_number,issue_purpose,company_id,issue_date,challan_no,req_no,remarks
			from inv_issue_master 
			where id='$data' and entry_form=21";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#txt_system_no').val('".$row[csf("issue_number")]."');\n";
		echo "$('#txt_system_id').val(".$row[csf("id")].");\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
  		echo "$('#cbo_issue_purpose').val('".$row[csf("issue_purpose")]."');\n";
 		echo "$('#txt_issue_date').val('".change_date_format($row[csf("issue_date")])."');\n";
 		echo "$('#txt_issue_req_no').val('".$row[csf("req_no")]."');\n";
 		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
  		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		//clear child form
		echo "$('#tbl_child').find('select,input').val('');\n";
  	}
	exit();	
}

if($action=="show_dtls_list_view")
{
	$ex_data = explode("**",$data);
	$issue_number_id = $ex_data[0];
 	
	$cond="";
	if($issue_number_id!="") $cond .= " and a.id='$issue_number_id'";
 	
	$location_arr=return_library_array("select id,location_name from lib_location",'id','location_name');
	$department_arr=return_library_array("select id,department_name from lib_department",'id','department_name');
	$section_arr=return_library_array("select id,section_name from lib_section",'id','section_name');
	$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
	$po_no_arr=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
 	$group_arr=return_library_array( "select id,item_name from lib_item_group where item_category not in (1,2,3,4,5,6,7,12,13,14) and status_active=1 and is_deleted=0",'id','item_name');
	
	$sql = "select a.issue_number, b.id, b.store_id, b.cons_uom, b.cons_quantity, b.machine_category, b.machine_id, b.prod_id, b.location_id, b.department_id, b.section_id, c.item_category_id, c.item_description, c.item_group_id, b.order_id 
			from inv_issue_master a, inv_transaction b, product_details_master c
			where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=2 and a.entry_form=21 $cond";
			//echo $sql;
			
	$result = sql_select($sql);
	$i=1;
	$total_qnty=0;
	?> 
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:1000px" >
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Category</th>
                    <th>Group</th>
                    <th>Description</th>
                    <th>Store</th>
                    <th>Issue Qnty</th>
                    <th>UOM</th>
                    <th>Serial No</th>
                    <th>Machine Categ.</th>
                    <th>Machine No</th>
                    <th>Buyer Order</th>
                    <th>Loc./Dept./Sec.</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF"; 
					if($db_type==0)
					{
						$serialNo=return_field_value("group_concat(serial_no)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
						$serialID=return_field_value("group_concat(id)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
					}
					else
					{
						$serialNo=return_field_value("LISTAGG(CAST(serial_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY serial_no) as sr","inv_serial_no_details","issue_trans_id=".$row[csf("id")],"sr");
						$serialID=return_field_value("LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as id","inv_serial_no_details","issue_trans_id=".$row[csf("id")],"id");
					}
					$total_qnty +=	$row[csf("cons_quantity")];
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>","child_form_input_data","requires/general_item_issue_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $item_category[$row[csf("item_category_id")]]; ?></p></td>
                        <td width="100"><p><?php echo $group_arr[$row[csf("item_group_id")]]; ?></p></td>
                        <td width="100"><p><?php echo $row[csf("item_description")]; ?></p></td>
                        <td width="90"><p><?php echo $store_arr[$row[csf("store_id")]]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("cons_quantity")]; ?></p></td>
                        <td width="50"><p><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></p></td>
                        <td width="50"><p><?php echo $serialNo; ?></p></td>
                        <td width="70"><p><?php echo $machine_category[$row[csf("machine_category")]]; ?></p></td>
                        <td width="50"><p><?php echo $machine_arr[$row[csf("machine_id")]]; ?></p></td>
                        <td width="80"><p><?php echo $po_no_arr[$row[csf("order_id")]]; ?></p></td>
                        <td width="170"><p><?php echo $location_arr[$row[csf("location_id")]].', '.$department_arr[$row[csf("department_id")]].', '.$section_arr[$row[csf("section_id")]]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                            <th colspan="5" align="right">Total :</th>
                            <th><?php echo $total_qnty; ?></th>
                            <th colspan="6">&nbsp;</th>                            
                     </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}

if($action=="child_form_input_data")
{
	$rcv_dtls_id = $data;	
 	
	$sql = "select b.id, b.location_id, b.company_id, c.id as prod_id, c.item_description, c.item_category_id, c.item_group_id, b.cons_quantity, c.current_stock, b.store_id, b.cons_uom, b.order_id, b.floor_id, b.machine_id, b.machine_category, b.location_id, b.department_id, b.section_id, b.room, b.rack, b.self, b.bin_box 
			from inv_transaction b, product_details_master c
			where b.prod_id=c.id and b.id='$rcv_dtls_id' and b.transaction_type=2 and b.item_category not in (1,2,3,5,6,7,12,13,14)"; 
	//echo $sql;die;
	$result = sql_select($sql);
    
	foreach($result as $row)
	{
		echo "$('#txt_item_desc').val('".$row[csf("item_description")]."');\n";
		echo "$('#cbo_item_category').val(".$row[csf("item_category_id")].");\n";
		echo "$('#cbo_item_group').val(".$row[csf("item_group_id")].");\n";
		echo "$('#txt_issue_qnty').val(".$row[csf("cons_quantity")].");\n";
		echo "$('#hidden_p_issue_qnty').val(".$row[csf("cons_quantity")].");\n"; 
 		echo "$('#txt_current_stock').val(".($row[csf("current_stock")]+$row[csf("cons_quantity")]).");\n";
		echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
 		echo "$('#cbo_uom').val(".$row[csf("cons_uom")].");\n";
		
		
		//$serialNo=return_field_value("group_concat(serial_no)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
		//$serialID=return_field_value("group_concat(id)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
		if($db_type==0)
		{
			$serialNo=return_field_value("group_concat(serial_no)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
			$serialID=return_field_value("group_concat(id)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
		}
		else
		{
			$serialNo=return_field_value("LISTAGG(CAST(serial_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY serial_no) as sr","inv_serial_no_details","issue_trans_id=".$row[csf("id")],"sr");
			$serialID=return_field_value("LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as id","inv_serial_no_details","issue_trans_id=".$row[csf("id")],"id");
		}
		echo "$('#txt_serial_no').val('".$serialNo."');\n";
		echo "$('#txt_serial_id').val('".$serialID."');\n";
		echo "$('#before_serial_id').val('".$serialID."');\n";
		echo "load_drop_down( 'requires/general_item_issue_controller',".$row[csf("company_id")]."+'_'+".$row[csf("location_id")].", 'load_drop_down_floor', 'floor_td' );\n";
		echo "$('#cbo_floor_id').val(".$row[csf("floor_id")].");\n";
		echo "$('#cbo_machine_category').val(".$row[csf("machine_category")].");\n";
		echo "load_drop_down( 'requires/general_item_issue_controller',".$row[csf("company_id")]."+'_'+".$row[csf("machine_category")]."+'_'+".$row[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
		echo "$('#cbo_machine_name').val(".$row[csf("machine_id")].");\n";
		echo "$('#txt_order_id').val(".$row[csf("order_id")].");\n";
		$buyer_order=return_field_value("po_number","wo_po_break_down","id=".$row[csf("order_id")]);
		echo "$('#txt_buyer_order').val('".$buyer_order."');\n";
		echo "$('#cbo_location').val(".$row[csf("location_id")].");\n";
		echo "$('#cbo_department').val(".$row[csf("department_id")].");\n";
		//echo "load_drop_down( 'requires/general_item_issue_controller',document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_department').value, 'load_drop_down_section', 'section_td');\n";
		echo "load_drop_down( 'requires/general_item_issue_controller', ".$row[csf("department_id")].", 'load_drop_down_section', 'section_td' );\n";
		echo "$('#cbo_section').val(".$row[csf("section_id")].");\n";
		//echo "$('#cbo_section').val(".$row[csf("section_id")].");\n"; 
 		echo "$('#cbo_room').val(".$row[csf("room")].");\n";
		echo "fn_room_rack_self_box();\n";
		echo "$('#cbo_rack').val('".$row[csf("rack")]."');\n";
		echo "fn_room_rack_self_box();\n";
 		echo "$('#cbo_self').val(".$row[csf("self")].");\n";
		echo "fn_room_rack_self_box();\n";
		echo "$('#cbo_binbox').val(".$row[csf("bin_box")].");\n";
		echo "$('#current_prod_id').val(".$row[csf("prod_id")].");\n";
		echo "$('#update_id').val(".$row[csf("id")].");\n"; 
		echo "set_button_status(1, permission, 'fnc_general_item_issue_entry',1,1);\n";
		echo "$('#tbl_master').find('input,select').attr('disabled', false);\n";
	}
	exit();
}
//################################################# function Here #########################################//

//function for domestic rate find--------------//
//parameters rate,ile cost,exchange rate,conversion factor
function return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor){
	$rate_ile=$rate+$ile_cost;
	$rate_ile_exchange=$rate_ile*$exchange_rate;
	$doemstic_rate=$rate_ile_exchange/$conversion_factor;
	return $doemstic_rate;	
}

if ($action=="general_item_issue_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);

	$sql=" select id, issue_number,issue_purpose,issue_date, req_no, challan_no, remarks from inv_issue_master where id='$data[1]'";
	$dataArray=sql_select($sql);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$floor_arr = return_library_array("select id, floor_name from  lib_prod_floor","id","floor_name");
?>
<div style="width:1000px;">
    <table width="980" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u>Goods Issue Note</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>System ID :</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="130"><strong>Issue Purpose :</strong></td> <td width="175px"><?php echo $general_issue_purpose[$dataArray[0][csf('issue_purpose')]]; ?></td>
            <td width="125"><strong>Issue Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Issue Req. No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('req_no')]; ?></td>
            <td><strong>Challan No :</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Remarks:</strong></td><td width="175px"><?php echo $dataArray[0][csf('remarks')]; ?></td>
        </tr>
         <tr>
               <td><strong>Bar Code:</strong></td><td  colspan="3" id="barcode_img_id"></td>
               
       </tr>
    </table>
         <br>
	<div style="width:100%;">
    <table align="right" cellspacing="0" width="980"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="80" align="center">Item Category</th>
            <th width="90" align="center">Item Group</th>
            <th width="160" align="center">Item Description</th>
            <th width="60" align="center">Store</th> 
            <th width="60" align="center">Issue Qnty</th>
            <th width="50" align="center">UOM</th>
            <th width="80" align="center">Serial No</th>
            <th width="80" align="center">Machine Categ.</th> 
            <th width="80" align="center">Floor</th>
            <th width="50" align="center">Machine No</th>
            <th width="60" align="center">Buyer Order</th>
            <th width="80" align="center">Loc./Dept./Sec.</th>                
        </thead>
        <tbody>
<?php
	//$mrr_no=$dataArray[0][csf('issue_number')];
	$cond="";
	if($data[1]!="") $cond .= " and a.id='$data[1]'";
	$location_arr=return_library_array("select id,location_name from lib_location",'id','location_name');
	$department_arr=return_library_array("select id,department_name from lib_department",'id','department_name');
	$section_arr=return_library_array("select id,section_name from lib_section",'id','section_name');
	$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
	$po_number_arr=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
 	$group_arr=return_library_array( "select id,item_name from lib_item_group where item_category not in (1,2,3,4,5,6,7,12,13,14) and status_active=1 and is_deleted=0",'id','item_name');
	
	$i=1;
	$sql_result = sql_select("select a.issue_number, b.id, b.store_id, b.cons_uom, b.cons_quantity, b.floor_id, b.machine_category, b.machine_id, b.prod_id, b.location_id, b.department_id, b.section_id, c.item_category_id, c.item_description, c.item_group_id, b.order_id from inv_issue_master a, inv_transaction b, product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=2 and a.entry_form=21 $cond");
			
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			if($db_type==0)
			{
				$serialNo=return_field_value("group_concat(serial_no)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
				$serialID=return_field_value("group_concat(id)","inv_serial_no_details","issue_trans_id=".$row[csf("id")]);
				$order_num=return_field_value("group_concat(po_number)","wo_po_break_down","id=".$row[csf("order_id")]);
			}
			else
			{
				$serialNo=return_field_value("LISTAGG(CAST(serial_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY serial_no) as sr","inv_serial_no_details","issue_trans_id=".$row[csf("id")],"sr");
				$serialID=return_field_value("LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as id","inv_serial_no_details","issue_trans_id=".$row[csf("id")],"id");
				$order_num=return_field_value("LISTAGG(CAST(po_number AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY po_number) as po_number","wo_po_break_down","id=".$row[csf("order_id")],"po_number");
			}
			
			
			
			$cons_quantity=$row[csf('cons_quantity')];
			$cons_quantity_sum += $cons_quantity;
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <td><?php echo $item_category[$row[csf("item_category_id")]]; ?></td>
                <td><?php echo $group_arr[$row[csf("item_group_id")]]; ?></td>
                <td><?php echo $row[csf("item_description")]; ?></td>
                <td align="center"><?php echo $store_arr[$row[csf("store_id")]]; ?></td>
                <td align="right"><?php echo $row[csf("cons_quantity")]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></td>
                <td><?php echo $serialNo; ?></td>
                <td><?php echo $machine_category[$row[csf("machine_category")]]; ?></td>
                <td ><?php echo $floor_arr[$row[csf("floor_id")]]; ?></td>
                <td align="center"><?php echo $machine_arr[$row[csf("machine_id")]]; ?></td>
                <td align="center"><?php echo $order_num; ?></td>
                <td><?php echo $location_arr[$row[csf("location_id")]].', '.$department_arr[$row[csf("department_id")]].', '.$section_arr[$row[csf("section_id")]]; ?></td>
			</tr>
			<?php $i++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5" align="right">Total :</td>
                <td align="right"><?php echo $cons_quantity_sum; ?></td>
                <td colspan="7">&nbsp;</td>
            </tr>                           
        </tfoot>
    </table>
        <br>
		 <?php
            echo signature_table(12, $data[0], "900px");
         ?>
	</div>
	</div>
     <script type="text/javascript" src="../../../js/jquery.js"></script>
      <script type="text/javascript" src="../../../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess ){
		   
			var value = valuess;//$("#barcodeValue").val();
		 // alert(value)
			var btype = 'code39';//$("input[name=btype]:checked").val();
			var renderer ='bmp';// $("input[name=renderer]:checked").val();
			 
			var settings = {
			  output:renderer,
			  bgColor: '#FFFFFF',
			  color: '#000000',
			  barWidth: 1,
			  barHeight: 30,
			  moduleSize:5,
			  posX: 10,
			  posY: 20,
			  addQuietZone: 1
			};
			$("#barcode_img_id").html('11');
			 value = {code:value, rect: false};
			
			$("#barcode_img_id").show().barcode(value, btype, settings);
		  
		} 
  
	 generateBarcode('<?php echo $data[2]; ?>');
	 
	 
	 </script>
            
    
<?php
exit();
}

?>
