﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------- Start-------------------------------------//

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 152, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  
	exit();
}

if($action=="populate_acc_loan_no_data")
{
	$data=explode("**",$data);
	$acc_type=$data[0];
	$rowID=$data[1];
	$company_id=$data[2];
	
	$sql="select account_no from lib_bank_account where account_type=$acc_type and company_id=$company_id and status_active=1 and is_deleted=0";
	$nameArray=sql_select($sql,1);
	echo "$('#acLoanNo_".$rowID."').removeAttr('readonly');\n";
 	echo "$('#acLoanNo_".$rowID."').val('');\n";
	foreach($nameArray as $row)
	{
		echo "$('#acLoanNo_".$rowID."').attr('readonly','readonly');\n";
		echo "$('#acLoanNo_".$rowID."').val('".$row[csf("account_no")]."');\n";
	}
	exit();
}

if($action=="invoice_bill_popup_search")
{
	echo load_html_head_contents("Export Proceeds Realization Form", "../../../", 1, 1,'','1','');
	extract($_REQUEST);
	?>
     
	<script>
	
		function js_set_value(id,type)
		{
			$('#hidden_invoice_bill_id').val(id);
			$('#hidden_is_invoiceBill').val(type);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:740px;">
	<form name="searchexportinformationfrm"  id="searchexportinformationfrm">
		<fieldset style="width:720px;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="630" class="rpt_table">
                <thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th width="180" id="search_by_td_up">Enter Bill No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="hidden_invoice_bill_id" id="hidden_invoice_bill_id" value="" />
                        <input type="hidden" name="hidden_is_invoiceBill" id="hidden_is_invoiceBill" value="" />
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <?php
						if($buyerID!=0)
						{
                           echo create_drop_down( "cbo_buyer_name", 152, "select buy.id, buy.buyer_name from lib_buyer buy where buy.status_active =1 and buy.is_deleted=0  $buyer_cond order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $buyerID, "",0 ); 
						}
						else
						{
							echo create_drop_down( "cbo_buyer_name", 152, "select buy.id, buy.buyer_name from lib_buyer buy where buy.status_active =1 and buy.is_deleted=0  $buyer_cond order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "",0 ); 
						}
                        ?>                        
                    </td>
                    <td> 
                        <?php
                            $arr=array(1=>'Bill No',2=>'Invoice No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $arr,"",0, "--Select--", "",$dd,0 );
                        ?> 
                    </td>						
                    <td id="search_by_td">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>                       
                     <td>
                        <input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_buyer_name').value+'**'+<?php echo $beneficiary_name; ?>+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value, 'invoice_bill_search_list_view', 'search_div', 'export_proceed_realization_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                     </td>
                </tr>
           </table>
            <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="invoice_bill_search_list_view")
{
	$data=explode('**',$data); 
	
	if($data[0]==0) $buyer_id="%%"; else $buyer_id=$data[0];
	$company_id=$data[1];
	$search_by=$data[2];
	$search_text=trim($data[3]);
	
	if($db_type==0)
	{
		if($search_text=="")
		{
			$sql = "select a.id, a.company_id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, a.submit_type, group_concat(distinct(b.invoice_id)) as invoice_id, 0 as is_lc, 0 as lc_sc_id, 1 as type from com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_id and a.buyer_id like '$buyer_id' and a.id not in (select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=1 and status_active=1 and is_deleted=0) and a.entry_form=40 group by a.id
			union all
			select c.id, c.benificiary_id as company_id, c.buyer_id, c.invoice_no as bank_ref_no, c.invoice_date as bank_ref_date, 0 as submit_type, '' as invoice_id, c.is_lc, c.lc_sc_id, 2 as type from com_export_invoice_ship_mst c where c.status_active=1 and c.is_deleted=0 and c.benificiary_id=$company_id and c.buyer_id like '$buyer_id' and c.id not in(select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=2 and status_active=1 and is_deleted=0) 
			";
		}
		else
		{
			if($search_by==1)
			{
				$sql = "select a.id, a.company_id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, a.submit_type, group_concat(distinct(b.invoice_id)) as invoice_id, 0 as is_lc, 0 as lc_sc_id, 1 as type from com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_id and a.buyer_id like '$buyer_id' and a.bank_ref_no like '%".$search_text."%' and a.id not in (select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=1 and status_active=1 and is_deleted=0) and a.entry_form=40 group by a.id";
			}
			else 
			{
				$sql = "select c.id, c.benificiary_id as company_id, c.buyer_id, c.invoice_no as bank_ref_no, c.invoice_date as bank_ref_date, 0 as submit_type, '' as invoice_id, c.is_lc, c.lc_sc_id, 2 as type from com_export_invoice_ship_mst c where c.status_active=1 and c.is_deleted=0 and c.benificiary_id=$company_id and c.buyer_id like '$buyer_id' and c.invoice_no like '%".$search_text."%' and c.id not in(select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=2 and status_active=1 and is_deleted=0)";
			}
		}
	}
	else
	{
		if($search_text=="")
		{
			$sql = "select a.id, a.company_id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, a.submit_type, LISTAGG(b.invoice_id, ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id, 0 as is_lc, 0 as lc_sc_id, 1 as type from com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_id and a.buyer_id like '$buyer_id' and a.id not in (select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=1 and status_active=1 and is_deleted=0) and a.entry_form=40 group by a.id, a.company_id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, a.submit_type
				union all
				select c.id, c.benificiary_id as company_id, c.buyer_id, c.invoice_no as bank_ref_no, c.invoice_date as bank_ref_date, 0 as submit_type, NULL as invoice_id, c.is_lc, c.lc_sc_id, 2 as type from com_export_invoice_ship_mst c where c.status_active=1 and c.is_deleted=0 and c.benificiary_id=$company_id and c.buyer_id like '$buyer_id' and c.id not in(select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=2 and status_active=1 and is_deleted=0) 
				";	
		}
		else
		{
			if($search_by==1)
			{
				$sql = "select a.id, a.company_id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, a.submit_type, LISTAGG(b.invoice_id, ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id, 0 as is_lc, 0 as lc_sc_id, 1 as type from com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_id and a.buyer_id like '$buyer_id' and a.bank_ref_no like '%".$search_text."%' and a.id not in (select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=1 and status_active=1 and is_deleted=0) and a.entry_form=40 group by a.id, a.company_id, a.buyer_id, a.bank_ref_no, a.bank_ref_date, a.submit_type";
			}
			else 
			{
				$sql = "select c.id, c.benificiary_id as company_id, c.buyer_id, c.invoice_no as bank_ref_no, c.invoice_date as bank_ref_date, 0 as submit_type, NULL as invoice_id, c.is_lc, c.lc_sc_id, 2 as type from com_export_invoice_ship_mst c where c.status_active=1 and c.is_deleted=0 and c.benificiary_id=$company_id and c.buyer_id like '$buyer_id' and c.invoice_no like '%".$search_text."%' and c.id not in(select invoice_bill_id from com_export_proceed_realization where is_invoice_bill=2 and status_active=1 and is_deleted=0)";
			}
		}	
	}
	
	//echo $sql;die;

	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$dom_curr_arr=return_library_array( "select doc_submission_mst_id, sum(dom_curr) as dom_curr from com_export_doc_sub_trans where status_active=1 and is_deleted=0 group by doc_submission_mst_id",'doc_submission_mst_id','dom_curr');
	$lcsc_arr=return_library_array( "select id, export_lc_no from com_export_lc",'id','export_lc_no');
	
	$ScRes_arr=array();
	$ScRes=sql_select("select id, contract_no, pay_term from com_sales_contract");
	foreach($ScRes as $row)
	{
		$ScRes_arr[$row[csf('id')]]['contract_no']=$row[csf('contract_no')];
		$ScRes_arr[$row[csf('id')]]['pay_term']=$row[csf('pay_term')];
	}
	
	$invoiceDataArr=array();
	$sql_invoice="select id,is_lc,lc_sc_id from com_export_invoice_ship_mst where status_active=1 and is_deleted=0";
	$data_array_invoice=sql_select($sql_invoice);
	foreach($data_array_invoice as $row_invoice)
	{
		$invoiceDataArr[$row_invoice[csf('id')]]=$row_invoice[csf('is_lc')]."**".$row_invoice[csf('lc_sc_id')];
	}
	?>
	<table width="700" cellpadding="0" cellspacing="0" border="1" class="rpt_table" rules="all">
        <thead> 
            <th width="40">SL</th>
            <th width="120">Bill/ Invoice No</th>
            <th width="70">Bill/Invoice</th>
            <th width="80">Bill/ Invoice Date</th>
            <th width="70">Beneficiary</th>
            <th width="60">Buyer</th>
            <th width="150">LC/SC No</th>
            <th>Negotiated Amnt</th>
        </thead>
	</table>
    <div style="width:720px; max-height:270px; overflow-y:scroll"> 
     	<table width="700" cellpadding="0" cellspacing="0" border="1" class="rpt_table" rules="all" id="list_view">
		<?php
			$data_array=sql_select($sql);
            $i = 1; 
            foreach($data_array as $row)
            { 
                if ($i%2==0)  
                    $bgcolor="#FFFFFF";
                else
                    $bgcolor="#E9F3FF";	
				
				$lc_sc_no=''; $negotiated_amnt=0;
				if($row[csf('type')]==1)
				{
					//$negotiated_amnt=return_field_value("sum(dom_curr)","com_export_doc_sub_trans","doc_submission_mst_id=".$row[csf('id')]);
					$negotiated_amnt=$dom_curr_arr[$row[csf('id')]];
					
					/*$sql_invoice="select is_lc, lc_sc_id from com_export_invoice_ship_mst where id in(".$row[csf('invoice_id')].") and status_active=1 and is_deleted=0 group by is_lc, lc_sc_id";
					$data_array_invoice=sql_select($sql_invoice);
					foreach($data_array_invoice as $row_invoice)*/
					$all_invoice_id=explode(",",$row[csf('invoice_id')]);
					foreach($all_invoice_id as $invoice_id)
					{
						$invData=explode("**",$invoiceDataArr[$invoice_id]);
						//if($row_invoice[csf('is_lc')]==1)
						if($invData[0]==1)
						{
							//$lc_no=return_field_value("export_lc_no","com_export_lc","id=".$row_invoice[csf('lc_sc_id')]);
							$lc_no=$lcsc_arr[$invData[1]];
							if($lc_sc_no=="") $lc_sc_no=$lc_no; else $lc_sc_no.=",".$lc_no;
						}
						else
						{
							//$sc_no=return_field_value("contract_no","com_sales_contract","id=".$row_invoice[csf('lc_sc_id')]);
							$sc_no=$ScRes_arr[$invData[1]]['contract_no'];
							if($lc_sc_no=="") $lc_sc_no=$sc_no; else $lc_sc_no.=",".$sc_no;
						}
					}
					$print_cond=1;
				}
				else
				{
					$negotiated_amnt=0;
					if($row[csf('is_lc')]==1)
					{
						//$lc_no=return_field_value("export_lc_no","com_export_lc","id=".$row[csf('lc_sc_id')]);
						$lc_no=$lcsc_arr[$row[csf('lc_sc_id')]];
						if($lc_sc_no=="") $lc_sc_no=$lc_no; else $lc_sc_no.=",".$lc_no;
						$print_cond=0;
					}
					else
					{
						/*if($db_type==0)
						{
							$sc_payterm=return_field_value("concat_ws('**',contract_no,pay_term)","com_sales_contract","id=".$row[csf('lc_sc_id')]);
						}
						else
						{
							$sc_payterm=return_field_value("contract_no || '**' || pay_term as sc_payterm","com_sales_contract","id=".$row[csf('lc_sc_id')]);
						}
						
						$sc_payterm=explode("**",$sc_payterm);
						$sc_no=$sc_payterm[0];
						$pay_term=$sc_payterm[1];*/
						
						$sc_no=$ScRes_arr[$row[csf('lc_sc_id')]]['contract_no'];
						$pay_term=$ScRes_arr[$row[csf('lc_sc_id')]]['pay_term'];
						
						if($pay_term==3) $print_cond=1; else $print_cond=0;
						
						if($lc_sc_no=="") $lc_sc_no=$sc_no; else $lc_sc_no.=",".$sc_no;
					}
				}
				if($print_cond==1)
				{
					$lc_sc_no=implode(",",array_unique(explode(",",$lc_sc_no)));
				?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer;" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $row[csf('id')].','.$row[csf('type')]; ?>);" >                		
                    	<td width="40"><?php echo $i; ?></td>
                        <td width="120"><p><?php echo $row[csf('bank_ref_no')]; ?>&nbsp;</p></td>
                        <td width="70" align="center"><?php if($row[csf('type')]==1) echo "Bill"; else echo "Invoice"; ?></td>
                        <td width="80" align="center"><?php echo change_date_format($row[csf('bank_ref_date')]); ?>&nbsp;</td>
                        <td width="70"><?php echo $comp[$row[csf('company_id')]]; ?></td>
                        <td width="60"><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></td>
                        <td width="150"><p><?php echo $lc_sc_no; ?></p></td>
                        <td align="right"><?php echo number_format($negotiated_amnt,2); ?>&nbsp;</td>
                    </tr>
                <?php
                $i++;
				}
            }		
			?>
		</table>
	</div>
	<?php	
	exit();
}

if($action=="populate_data_from_invoice_bill")
{
	$data=explode("**",$data);
	$invoice_bill_id=$data[0];
	$is_invoiceBill=$data[1];
	$realization_id=$data[2];
	
	if($is_invoiceBill==1)
	{
		if($db_type==0)
		{
			$sql = "select a.bank_ref_no, a.bank_ref_date, a.buyer_id, a.submit_type, group_concat(distinct(b.invoice_id)) as invoice_id, sum(b.net_invo_value) as bill_amnt from com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.id=$invoice_bill_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.bank_ref_no, a.bank_ref_date, a.buyer_id, a.submit_type";
		}
		else
		{
			$sql = "select a.bank_ref_no, a.bank_ref_date, a.buyer_id, a.submit_type, LISTAGG(b.invoice_id, ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id, sum(b.net_invo_value) as bill_amnt from com_export_doc_submission_mst a, com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id and a.id=$invoice_bill_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.bank_ref_no, a.bank_ref_date, a.buyer_id, a.submit_type";	
		}
	}
	else
	{
		$sql = "select invoice_no as bank_ref_no, buyer_id, invoice_date as bank_ref_date, is_lc, lc_sc_id, invoice_value as bill_amnt from com_export_invoice_ship_mst where id=$invoice_bill_id and status_active=1 and is_deleted=0";	
	}
	//echo $sql;die;
	$data_array=sql_select($sql);
	
 	foreach ($data_array as $row)
	{
		$currency='';
		if($is_invoiceBill==1)
		{
			$negotiation_array=sql_select("select sum(dom_curr) as dom_curr, sum(lc_sc_curr) as lc_sc_curr from com_export_doc_sub_trans where doc_submission_mst_id=$invoice_bill_id and status_active=1 and is_deleted=0");
			
			$negotiated_amnt=$negotiation_array[0][csf('dom_curr')];
			$lc_sc_currency=$negotiation_array[0][csf('lc_sc_curr')];
			$conv_rate=$negotiated_amnt/$lc_sc_currency;
			
			$sql_invoice="select is_lc, lc_sc_id from com_export_invoice_ship_mst where id in(".$row[csf('invoice_id')].") and status_active=1 and is_deleted=0 group by is_lc, lc_sc_id";
			$data_array_invoice=sql_select($sql_invoice);
			
			foreach($data_array_invoice as $row_invoice)
			{
				if($row_invoice[csf('is_lc')]==1)
				{
					if($db_type==0)
					{
						$lc_sc=return_field_value("concat_ws('**',export_lc_no,currency_name)","com_export_lc","id=".$row_invoice[csf('lc_sc_id')]."");
					}
					else
					{
						$lc_sc=return_field_value("export_lc_no || '**' || currency_name as EX_CURR","com_export_lc","id='".$row_invoice[csf('lc_sc_id')]."'","EX_CURR");
					}
				}
				else
				{
					if($db_type==0)
					{
						$lc_sc=return_field_value("concat_ws('**',contract_no,currency_name)","com_sales_contract","id=".$row_invoice[csf('lc_sc_id')]."");
					}
					else
					{
						$lc_sc=return_field_value("contract_no || '**' || currency_name as CON_CURR","com_export_lc","id='".$row_invoice[csf('lc_sc_id')]."'","CON_CURR");
					}
				}
				
				$lc_sc=explode("**",$lc_sc);
				
				if($lc_sc_no=="") $lc_sc_no=$lc_sc[0]; else $lc_sc_no.=",".$lc_sc[0];
				if($currency=="") $currency=$lc_sc[1]; else $currency=$currency;
			}
		}
		else
		{
			$negotiated_amnt=0;
			if($row[csf('is_lc')]==1)
			{
				if($db_type==0)
				{
					$lc_sc=return_field_value("concat_ws('**',export_lc_no,currency_name)","com_export_lc","id=".$row[csf('lc_sc_id')]."");
				}
				else
				{
					$lc_sc=return_field_value("export_lc_no || '**' || currency_name as EX_CURR","com_export_lc","id='".$row[csf('lc_sc_id')]."'","EX_CURR");
				}
			}
			else
			{
				if($db_type==0)
				{
					$lc_sc=return_field_value("concat_ws('**',contract_no,currency_name)","com_sales_contract","id=".$row[csf('lc_sc_id')]."");
				}
				else
				{
					$lc_sc=return_field_value("contract_no || '**' || currency_name as CON_CURR","com_export_lc","id='".$row[csf('lc_sc_id')]."'","CON_CURR");
				}
			}
			
			$lc_sc=explode("**",$lc_sc);
			$lc_sc_no=$lc_sc[0];
			$currency=$lc_sc[1];
		}
		
		if($row[csf('submit_type')]==2)
		{
			echo "document.getElementById('cbodistributionHead_1').value 					= '1';\n";
			echo "document.getElementById('distributionDocumentCurrency_1').value 			= '".$lc_sc_currency."';\n";
			echo "document.getElementById('distributionConversionRate_1').value 			= '".$conv_rate."';\n";
			echo "document.getElementById('distributionDomesticCurrency_1').value 			= '".$negotiated_amnt."';\n";
			echo "disable_enable_fields('cbodistributionHead_1*acLoanNo_1*distributionDocumentCurrency_1*distributionConversionRate_1*distributionDomesticCurrency_1',1);\n";
		}
		else
		{
			echo "disable_enable_fields('cbodistributionHead_1*acLoanNo_1*distributionDocumentCurrency_1*distributionConversionRate_1*distributionDomesticCurrency_1',0);\n";
		}
		
		echo "document.getElementById('txt_invoice_bill_no').value 		= '".$row[csf("bank_ref_no")]."';\n";
		echo "document.getElementById('invoice_bill_id').value 			= '".$invoice_bill_id."';\n";
		echo "document.getElementById('is_invoice_bill').value 			= '".$is_invoiceBill."';\n";
		echo "document.getElementById('txt_lc_sc_no').value 			= '".$lc_sc_no."';\n";
		echo "document.getElementById('cbo_buyer_name').value 			= '".$row[csf("buyer_id")]."';\n";
		echo "document.getElementById('cbo_currency_name').value 		= '".$currency."';\n";
		echo "document.getElementById('txt_bill_invoice_date').value 	= '".change_date_format($row[csf("bank_ref_date")])."';\n";
		echo "document.getElementById('txt_bill_invoice_amnt').value 	= '".$row[csf("bill_amnt")]."';\n";
		echo "document.getElementById('txt_negotiated_amount').value 	= '".$negotiated_amnt."';\n";
		echo "document.getElementById('submit_type').value 				= '".$row[csf('submit_type')]."';\n";
		
		if($realization_id!="")
		{
			$sql_realization="select benificiary_id, buyer_id, received_date, remarks from com_export_proceed_realization where id=$realization_id";
			$data_array_realization=sql_select($sql_realization);
			
			echo "document.getElementById('cbo_beneficiary_name').value 	= '".$data_array_realization[0][csf("benificiary_id")]."';\n";
			echo "document.getElementById('cbo_buyer_name').value 			= '".$data_array_realization[0][csf("buyer_id")]."';\n";
			echo "document.getElementById('txt_received_date').value 		= '".change_date_format($data_array_realization[0][csf("received_date")])."';\n";
			echo "document.getElementById('txt_remarks').value 				= '".$data_array_realization[0][csf("remarks")]."';\n";
			echo "document.getElementById('update_id').value 				= '".$realization_id."';\n";	
			echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_export_proceed_realization',1);\n"; 
		}
		else
		{
			echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_export_proceed_realization',1);\n";	
		}
		
		exit();
	}
}

if($action=="proceed_realization_popup_search")
{
	echo load_html_head_contents("Export Proceeds Realization Form", "../../../", 1, 1,'','1','');
	extract($_REQUEST);
	?>
     
	<script>
	
		function js_set_value(data)
		{
			var data=data.split("_");
			$('#hidden_realization_id').val(data[0]);
			$('#hidden_invoice_bill_id').val(data[1]);
			$('#hidden_is_invoiceBill').val(data[2]);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:740px;">
	<form name="searchexportinformationfrm"  id="searchexportinformationfrm">
		<fieldset style="width:720px;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="630" class="rpt_table">
                <thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th width="180" id="search_by_td_up">Enter Bill No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="hidden_realization_id" id="hidden_realization_id" value="" />
                        <input type="hidden" name="hidden_invoice_bill_id" id="hidden_invoice_bill_id" value="" />
                        <input type="hidden" name="hidden_is_invoiceBill" id="hidden_is_invoiceBill" value="" />
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <?php
						if($buyerID!=0)
						{
                           echo create_drop_down( "cbo_buyer_name", 152, "select buy.id, buy.buyer_name from lib_buyer buy where buy.status_active =1 and buy.is_deleted=0  $buyer_cond order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $buyerID, "",0 ); 
						}
						else
						{
						  echo create_drop_down( "cbo_buyer_name", 152, "select buy.id, buy.buyer_name from lib_buyer buy where buy.status_active =1 and buy.is_deleted=0  $buyer_cond order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "",0 );
						}
                        ?>                        
                    </td>
                    <td> 
                        <?php
                            $arr=array(1=>'Bill No',2=>'Invoice No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $arr,"",0, "--Select--", "",$dd,0 );
                        ?> 
                    </td>						
                    <td id="search_by_td">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>                       
                     <td>
                        <input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_buyer_name').value+'**'+<?php echo $beneficiary_name; ?>+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value, 'proceed_realization_search_list_view', 'search_div', 'export_proceed_realization_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                     </td>
                </tr>
           </table>
            <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="proceed_realization_search_list_view")
{
	$data=explode('**',$data); 
	
	$buyer_id=$data[0];
	$company_id=$data[1];
	$search_by=$data[2];
	$search_text=trim($data[3]);
	if($buyer_id!=0) $byer_cond="and a.buyer_id=$buyer_id"; else $byer_cond="";
	
	if($search_text=="")
	{
		$sql = "select a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, b.bank_ref_no, b.bank_ref_date, sum(c.net_invo_value) as bill_amnt from com_export_proceed_realization a, com_export_doc_submission_mst b, com_export_doc_submission_invo c where a.invoice_bill_id=b.id and a.is_invoice_bill=1 and b.id=c.doc_submission_mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.benificiary_id=$company_id $byer_cond group by a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, b.bank_ref_no, b.bank_ref_date
		union all
		select a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, e.invoice_no as bank_ref_no, e.invoice_date as bank_ref_date, sum(e.invoice_value) as bill_amnt from com_export_proceed_realization a, com_export_invoice_ship_mst e where a.invoice_bill_id=e.id and a.is_invoice_bill=2 and a.status_active=1 and a.is_deleted=0 and e.status_active=1 and e.is_deleted=0 and a.benificiary_id=$company_id $byer_cond group by a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, e.invoice_no, e.invoice_date";
	}
	else 
	{
		if($search_by==1)
		{
			$sql = "select a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, b.bank_ref_no, b.bank_ref_date, sum(c.net_invo_value) as bill_amnt from com_export_proceed_realization a, com_export_doc_submission_mst b, com_export_doc_submission_invo c where a.invoice_bill_id=b.id and a.is_invoice_bill=1 and b.id=c.doc_submission_mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.benificiary_id=$company_id $byer_cond and b.bank_ref_no like '%".$search_text."%' group by a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, b.bank_ref_no, b.bank_ref_date";
		}
		else 
		{
			$sql = "select a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, e.invoice_no as bank_ref_no, e.invoice_date as bank_ref_date, sum(e.invoice_value) as bill_amnt from com_export_proceed_realization a, com_export_invoice_ship_mst e where a.status_active=1 and a.is_deleted=0 and e.status_active=1 and e.is_deleted=0 and a.benificiary_id=$company_id $byer_cond and e.invoice_no like '%".$search_text."%' group by a.id, a.benificiary_id, a.buyer_id, a.invoice_bill_id, a.is_invoice_bill, e.invoice_no, e.invoice_date";
		}
	}
	//echo $sql;
	$is_invoiceBill_arr=array(1=>"Bill",2=>"Invoice");
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	
	$arr=array (2=>$is_invoiceBill_arr,3=>$comp,4=>$buyer_arr);
	
	echo create_list_view("list_view", "System Id,Bill/ Invoice No,Bill/ Invoice,Benificiary,Buyer,Bill/ Invoice Amnt,Received Date", "70,120,70,80,100,110","720","280",0, $sql, "js_set_value", "id,invoice_bill_id,is_invoice_bill", "", 1, "0,0,is_invoice_bill,benificiary_id,buyer_id,0,0", $arr , "id,bank_ref_no,is_invoice_bill,benificiary_id,buyer_id,bill_amnt,bank_ref_date", "",'','0,0,0,0,0,2,3');
	exit();
}

if($action=="details_list_view")
{
	$data=explode("**",$data);
	$realization_id=$data[0];
	$type=$data[1];
	$submit_type=$data[2];
	
	$nameArray=sql_select( "select id, account_head, ac_loan_no, document_currency, conversion_rate, domestic_currency from com_export_proceed_rlzn_dtls where mst_id='$realization_id' and type='$type' and status_active=1 and is_deleted=0" );
	$num_row=count($nameArray);
	$i=1;
	
	if($type==0)
	{
		if($num_row>0)
		{
			foreach($nameArray as $row) 
			{
			?>
				<tr class="general" id="deduction_<?php echo $i; ?>">
					<td>
                    	<select class="combo_boxes" id="cbodeductionHead_<?php echo $i; ?>" name="cbodeductionHead[]" style="width:172px">
                            <option value="0">-- Select Account Head --</option>
                                <?php
                                    foreach($commercial_head as $key=>$value)
                                    {
                                    ?>
                                        <option value=<?php echo $key; if($row[csf('account_head')]==$key) { ?> selected <?php } ?>><?php echo $value; ?></option>
                                    <?php	
                                    }
                                ?>
                        </select>
					</td>
					<td>
						<input type="text" name="deductionDocumentCurrency[]" id="deductionDocumentCurrency_<?php echo $i; ?>" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(<?php echo $i; ?>,'DocumentCurrency_','tbl_deduction','deduction')" value="<?php echo $row[csf('document_currency')]; ?>"/>
					</td>
					<td>
						<input type="text" name="deductionConversionRate[]" id="deductionConversionRate_<?php echo $i; ?>" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(<?php echo $i; ?>,'ConversionRate_','tbl_deduction','deduction')" value="<?php echo $row[csf('conversion_rate')]; ?>"/>
					</td>
					<td>
						<input type="text" name="deductionDomesticCurrency[]" id="deductionDomesticCurrency_<?php echo $i; ?>" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(<?php echo $i; ?>,'DomesticCurrency_','tbl_deduction','deduction')" value="<?php echo $row[csf('domestic_currency')]; ?>" />
					</td>
					<td width="65">
						<input type="button" id="deductionincrease_<?php echo $i; ?>" name="deductionincrease[]" style="width:30px" class="formbuttonplasminus" value="+" onClick="fnc_addRow(<?php echo $i; ?>,'tbl_deduction','deduction_')" />
						<input type="button" id="deductiondecrease_<?php echo $i; ?>" name="deductiondecrease[]" style="width:30px" class="formbuttonplasminus" value="-" onClick="fnc_deleteRow(<?php echo $i; ?>,'tbl_deduction','deduction_');" />
					</td>
				</tr>
			<?php
			$i++;
			}
		}
		else
		{
		?>
        	<tr class="general" id='deduction_1' align="center">
                <td>
                	<select class="combo_boxes" id="cbodeductionHead_1" name="cbodeductionHead[]" style="width:172px">
                        <option value="0">-- Select Account Head --</option>
                            <?php
                                foreach($commercial_head as $key=>$value)
                                {
                                ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php	
                                }
                            ?>
                    </select>
                </td>
                <td>
                    <input type="text" name="deductionDocumentCurrency[]" id="deductionDocumentCurrency_1" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(1,'DocumentCurrency_','tbl_deduction','deduction')"/>
                </td>
                <td>
                    <input type="text" name="deductionConversionRate[]" id="deductionConversionRate_1" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(1,'ConversionRate_','tbl_deduction','deduction')"/>
                </td>
                <td>
                    <input type="text" name="deductionDomesticCurrency[]" id="deductionDomesticCurrency_1" class="text_boxes_numeric" style="width:177px;" onKeyUp="calculate(1,'DomesticCurrency_','tbl_deduction','deduction')" />
                </td>
                <td width="65">
                    <input type="button" id="deductionincrease_1" name="deductionincrease[]" style="width:30px" class="formbuttonplasminus" value="+" onClick="fnc_addRow(1,'tbl_deduction','deduction_')" />
                    <input type="button" id="deductiondecrease_1" name="deductiondecrease[]" style="width:30px" class="formbuttonplasminus" value="-" onClick="fnc_deleteRow(1,'tbl_deduction','deduction_');" />
                </td>
            </tr>
        <?php
		}
	}
	else
	{
		if($num_row>0)
		{
			foreach($nameArray as $row) 
			{
				if($submit_type==2 && $row[csf('account_head')]==1)
				{
					$disable="disabled='disabled'";
				}
				else
				{
					$disable="";
				}
			?>
				<tr class="general" id="distribution_<?php echo $i; ?>">
					<td>
                    	<select class="combo_boxes" id="cbodistributionHead_<?php echo $i; ?>" name="cbodistributionHead[]" onChange="get_php_form_data( this.value+'**<?php echo $i; ?>**'+$('#cbo_beneficiary_name').val(), 'populate_acc_loan_no_data', 'requires/export_proceed_realization_controller' );check_duplication(<?php echo $i; ?>)" <?php echo $disable; ?>>
                            <option value="0">-- Select Account Head --</option>
                                <?php
                                    foreach($commercial_head as $key=>$value)
                                    {
                                    ?>
                                        <option value=<?php echo $key; if($row[csf('account_head')]==$key) { ?> selected <?php } ?>><?php echo $value; ?></option>
                                    <?php	
                                    }
									//echo create_drop_down( "cbodistributionHead_$i", 172, $commercial_head,"", 1, "-- Select Account Head --", $row[csf('account_head')], "" );
                                ?>
                        </select>
					</td>
					<td>
						<input type="text" name="acLoanNo[]" id="acLoanNo_<?php echo $i; ?>" class="text_boxes" style="width:130px;" value="<?php echo $row[csf('ac_loan_no')]; ?>" <?php echo $disable; ?>/>
					</td>
					<td>
						<input type="text" name="distributionDocumentCurrency[]" id="distributionDocumentCurrency_<?php echo $i; ?>" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(<?php echo $i; ?>,'DocumentCurrency_','tbl_distribution','distribution')" value="<?php echo $row[csf('document_currency')]; ?>" <?php echo $disable; ?>/>
					</td>
					<td>
						<input type="text" name="distributionConversionRate[]" id="distributionConversionRate_<?php echo $i; ?>" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(<?php echo $i; ?>,'ConversionRate_','tbl_distribution','distribution')" value="<?php echo $row[csf('conversion_rate')]; ?>" <?php echo $disable; ?>/>
					</td>
					<td>
						<input type="text" name="distributionDomesticCurrency[]" id="distributionDomesticCurrency_<?php echo $i; ?>" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(<?php echo $i; ?>,'DomesticCurrency_','tbl_distribution','distribution')" value="<?php echo $row[csf('domestic_currency')]; ?>" <?php echo $disable; ?>/>
					</td>
					<td width="65">
						<input type="button" id="distributionincrease_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbuttonplasminus" value="+" onClick="fnc_addRow(<?php echo $i; ?>,'tbl_distribution','distribution_')"/>
						<input type="button" id="distributiondecrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbuttonplasminus" value="-" onClick="fnc_deleteRow(<?php echo $i; ?>,'tbl_distribution','distribution_');"/>
					</td>
				</tr>
			<?php
			$i++;
			}
		}
		else
		{
		?>
            <tr class="general" id='distribution_1' align="center">
                <td> 
					<select class="combo_boxes" id="cbodistributionHead_1" name="cbodistributionHead[]" onChange="get_php_form_data( this.value+'**1**'+$('#cbo_beneficiary_name').val(), 'populate_acc_loan_no_data', 'requires/export_proceed_realization_controller' );check_duplication(1)">
                        <option value="0">-- Select Account Head --</option>
                            <?php
                                foreach($commercial_head as $key=>$value)
                                {
                                ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php	
                                }
                            ?>
					</select>
                </td>
                <td>
                    <input type="text" name="acLoanNo[]" id="acLoanNo_1" class="text_boxes" style="width:130px;" />
                </td>
                <td>
                    <input type="text" name="distributionDocumentCurrency[]" id="distributionDocumentCurrency_1" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(1,'DocumentCurrency_','tbl_distribution','distribution')"/>
                </td>
                <td>
                    <input type="text" name="distributionConversionRate[]" id="distributionConversionRate_1" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(1,'ConversionRate_','tbl_distribution','distribution')"/>
                </td>
                <td>
                    <input type="text" name="distributionDomesticCurrency[]" id="distributionDomesticCurrency_1" class="text_boxes_numeric" style="width:130px;" onKeyUp="calculate(1,'DomesticCurrency_','tbl_distribution','distribution')" />
                </td>
                <td width="65">
                    <input type="button" id="distributionincrease_1" name="increase[]" style="width:30px" class="formbuttonplasminus" value="+" onClick="fnc_addRow(1,'tbl_distribution','distribution_')" />
                    <input type="button" id="distributiondecrease_1" name="decrease[]" style="width:30px" class="formbuttonplasminus" value="-" onClick="fnc_deleteRow(1,'tbl_distribution','distribution_');" />
                </td>
            </tr>
        <?php
		}
	}
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$flag=1;
		$id=return_next_id( "id", "com_export_proceed_realization", 1 ) ;
				 
		$field_array="id, invoice_bill_id, is_invoice_bill, buyer_id, benificiary_id, received_date, remarks, inserted_by, insert_date";
		
		$data_array="(".$id.",".$invoice_bill_id.",".$is_invoice_bill.",".$cbo_buyer_name.",".$cbo_beneficiary_name.",".$txt_received_date.",".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
		/*$rID=sql_insert("com_export_proceed_realization",$field_array,$data_array,0);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} */
		
		$field_array_dtls="id, mst_id, type, account_head, ac_loan_no, document_currency, conversion_rate, domestic_currency, inserted_by, insert_date";
		$realization_id = return_next_id( "id", "com_export_proceed_rlzn_dtls", 1 );
		for($j=1;$j<=$deduction_tot_row;$j++)
		{ 	
			$type="0";
			$account_head="cbodeductionHead_".$j;
			$ac_loan_no="";
			$document_currency="deductionDocumentCurrency_".$j;
			$conversion_rate="deductionConversionRate_".$j;
			$domestic_currency="deductionDomesticCurrency_".$j;
			
			if($data_array_dtls!="") $data_array_dtls.=",";
			
			$data_array_dtls.="(".$realization_id.",".$id.",".$type.",'".$$account_head."','".$ac_loan_no."','".$$document_currency."','".$$conversion_rate."','".$$domestic_currency."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$realization_id = $realization_id+1;
		}
		
		for($j=1;$j<=$distribution_tot_row;$j++)
		{ 	
			$type="1";
			$account_head="cbodistributionHead_".$j;
			$ac_loan_no="acLoanNo_".$j;
			$document_currency="distributionDocumentCurrency_".$j;
			$conversion_rate="distributionConversionRate_".$j;
			$domestic_currency="distributionDomesticCurrency_".$j;
			
			if($data_array_dtls!="") $data_array_dtls.=",";
			
			$data_array_dtls.="(".$realization_id.",".$id.",".$type.",'".$$account_head."','".$$ac_loan_no."','".$$document_currency."','".$$conversion_rate."','".$$domestic_currency."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$realization_id = $realization_id+1;
		}
		
		$rID=sql_insert("com_export_proceed_realization",$field_array,$data_array,0);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} 
		$rID2=sql_insert("com_export_proceed_rlzn_dtls",$field_array_dtls,$data_array_dtls,1);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$id."**1";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "0**".$id."**1";
			}
			else
			{
				oci_rollback($con);
				echo "5**0**0";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$flag=1;
				 
		$field_array="invoice_bill_id*is_invoice_bill*buyer_id*benificiary_id*received_date*remarks*updated_by*update_date";
		
		$data_array=$invoice_bill_id."*".$is_invoice_bill."*".$cbo_buyer_name."*".$cbo_beneficiary_name."*".$txt_received_date."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$rID=sql_update("com_export_proceed_realization",$field_array,$data_array,"id",$update_id,0);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} 
		
		$delete_dtls=execute_query( "delete from com_export_proceed_rlzn_dtls where mst_id=$update_id",0);
		if($flag==1) 
		{
			if($delete_dtls) $flag=1; else $flag=0; 
		} */
		
		$field_array_dtls="id, mst_id, type, account_head, ac_loan_no, document_currency, conversion_rate, domestic_currency, inserted_by, insert_date";
		$realization_id = return_next_id( "id", "com_export_proceed_rlzn_dtls", 1 );
		for($j=1;$j<=$deduction_tot_row;$j++)
		{ 	
			$type="0";
			$account_head="cbodeductionHead_".$j;
			$ac_loan_no="";
			$document_currency="deductionDocumentCurrency_".$j;
			$conversion_rate="deductionConversionRate_".$j;
			$domestic_currency="deductionDomesticCurrency_".$j;
			
			if($data_array_dtls!="") $data_array_dtls.=",";
			
			$data_array_dtls.="(".$realization_id.",".$update_id.",".$type.",'".$$account_head."','".$ac_loan_no."','".$$document_currency."','".$$conversion_rate."','".$$domestic_currency."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$realization_id = $realization_id+1;
		}
		
		for($j=1;$j<=$distribution_tot_row;$j++)
		{ 	
			$type="1";
			$account_head="cbodistributionHead_".$j;
			$ac_loan_no="acLoanNo_".$j;
			$document_currency="distributionDocumentCurrency_".$j;
			$conversion_rate="distributionConversionRate_".$j;
			$domestic_currency="distributionDomesticCurrency_".$j;
			
			if($data_array_dtls!="") $data_array_dtls.=",";
			
			$data_array_dtls.="(".$realization_id.",".$update_id.",".$type.",'".$$account_head."','".$$ac_loan_no."','".$$document_currency."','".$$conversion_rate."','".$$domestic_currency."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$realization_id = $realization_id+1;
		}
		
		$rID=sql_update("com_export_proceed_realization",$field_array,$data_array,"id",$update_id,0);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} 
		
		$delete_dtls=execute_query( "delete from com_export_proceed_rlzn_dtls where mst_id=$update_id",0);
		if($flag==1) 
		{
			if($delete_dtls) $flag=1; else $flag=0; 
		} 
		//echo "insert into com_export_proceed_rlzn_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;
		$rID2=sql_insert("com_export_proceed_rlzn_dtls",$field_array_dtls,$data_array_dtls,1);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'", '', $update_id)."**1";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**".str_replace("'", '', $update_id)."**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "1**".str_replace("'", '', $update_id)."**1";
			}
			else
			{
				oci_rollback($con);
				echo "6**".str_replace("'", '', $update_id)."**1";
			}
		}
		disconnect($con);
		die;
	}
	
}

?>


 