﻿<?
/*-------------------------------------------- Comments

Purpose			: 	
Functionality	:	
JS Functions	:
Created by		:	Ashraful 
Creation date 	: 	6-8-2012
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Quotation Inquery Entry", "../../", 1, 1,$unicode,'','');
?>	
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<? echo $permission; ?>';
	 
function fnc_quotation_inquery( operation )
{
	
	if (form_validation('cbo_company_name*cbo_buyer_name*txt_style_ref*txt_inquery_date','Company*Buyer*Style Ref*Inquery Date')==false)
	{
		return;
	}

	else // Save Here
	{
				var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name*cbo_buyer_name*txt_style_ref*txt_inquery_date*txt_season*cbo_status*txt_request_no*txt_remarks*cbo_dealing_merchant*txt_system_id*update_id',"../../");
			//alert(data);	
		//freeze_window(operation);
		http.open("POST","requires/quotation_inquery_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_quotation_inquery_reponse;
	}
}

function fnc_quotation_inquery_reponse()
{
	if(http.readyState == 4) 
	{
		//alert(http.responseText);
		var reponse=trim(http.responseText).split('**');
		if(reponse[0]==0 )
		{
		   show_msg(reponse[0]);
		   $("#txt_system_id").val(reponse[1]);
		   $("#update_id").val(reponse[2]);
		   set_button_status(1, permission, 'fnc_quotation_inquery',1,1);
		}
		if(reponse[0]==1 )
			{
				show_msg(reponse[0]);
			}
		if(reponse[0]==2)
		{
		reset_form('quotationinquery_1','','');
		}
		
		
		//release_freezing();
	}
} 

	function open_mrrpopup()
	{
		//reset_form('','list_container_recipe_items*recipe_items_list_view','','','','');
	
		if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}
		var company = $("#cbo_company_name").val();	
		var page_link='requires/quotation_inquery_controller.php?action=mrr_popup&company='+company; 
		var title="Search  Popup";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=980px,height=400px,center=1,resize=0,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]; 
			
			var mrrNumber=this.contentDoc.getElementById("hidden_issue_number").value; // mrr number
			mrrNumber = mrrNumber.split("_"); 
			//var mrrId=this.contentDoc.getElementById("issue_id").value; // mrr number
	
			$("#txt_system_id").val(mrrNumber[0]);
			$("#update_id").val(mrrNumber[1]);
			
			get_php_form_data(mrrNumber[0], "populate_data_from_data", "requires/quotation_inquery_controller");
			
			
			set_button_status(1, permission, 'fnc_quotation_inquery',1,1);	
			
		}
	}


</script>
</head>

<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
      
        <? echo load_freeze_divs ("../../",$permission);  ?>
        <fieldset style="width:910px;">
            <legend>Quotation Inquery</legend>
            <form name="quotationinquery_1" id="quotationinquery_1" autocomplete="off">
                <table  width="810" cellspacing="2" cellpadding="0" border="0">
                    <tr>
                        <td  width="100" height="" align="right"></td>              
                        <td  width="170" >
                        </td>
                        <td  width="100" align="right">System ID </td>
                        
                        <td width="170">
                        <input style="width:160px;" type="text" title="Double Click to Search" onDblClick="open_mrrpopup()" class="text_boxes" placeholder="System ID" name="txt_system_id" id="txt_system_id" readonly />
                       <input type="hidden" name="update_id" id="update_id" /> 
                        </td>
                        <td width="100" align="right"></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td  width="100" height="" class="must_entry_caption" align="right">Company</td>              
                        <td  width="170" >
                              <?
                                 echo create_drop_down( "cbo_company_name", 172, "select comp.id,comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/quotation_inquery_controller', this.value, 'load_drop_down_buyer', 'buyer_td');",0);
                              ?> 
                        </td>
                        <td  width="100" class="must_entry_caption" align="right">Buyer </td>
                        <td width="170" id="buyer_td">
                           <? 
                        echo create_drop_down( "cbo_buyer_name", 172, "select id,buyer_name from lib_buyer  where status_active =1 and is_deleted=0  order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" ,0);   
                           ?>
                        </td>
                        <td width="100" class="must_entry_caption" align="right">Style Ref</td>
                        <td id="location" width="170">
                            <input class="text_boxes" type="text" style="width:160px"  name="txt_style_ref" id="txt_style_ref"/>		
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Season</td>
                        <td id="">
                         <input class="text_boxes" type="text" style="width:160px"  name="txt_season" id="txt_season"/>	  	  
                        </td>
                        <td align="right" class="must_entry_caption">Inquery Date</td>
                        <td>
                           <input name="txt_inquery_date" id="txt_inquery_date" class="datepicker" type="text" value="" style="width:160px; "  />	
                        </td>
                        <td align="right">Status</td>
                        <td>	
                            <?
							echo create_drop_down( "cbo_status", 172, $row_status,"", "", "", 1, "" );
						     ?>  
                        </td>
                    </tr>
                    <tr>
                        <td height="" align="right">Request No</td>   
                        <td  >
                               <input class="text_boxes" type="text" style="width:160px"  name="txt_request_no" id="txt_request_no"/>	 
                        </td>
                        <td height="" align="right">Remarks</td>   
                        <td  >
                               <input class="text_boxes" type="text" style="width:160px"  name="txt_remarks" id="txt_remarks"/>	 
                        </td>
                        <td align="right">Dealing Merchant</td>   
                        <td > 
                        <? 
                        	echo create_drop_down( "cbo_dealing_merchant", 172, "select id,team_member_name from lib_mkt_team_member_info where status_active =1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-- Select Merchant --", $selected, "" );
                        ?>	
                        </td>
                       
                      
                    </tr>
                   
                    <tr>
                         <td id="agent_td" colspan="2" align="right">
                          <input type="button" class="image_uploader" style="width:172px" value=" ADD File" onClick="file_uploader ( '../../', document.getElementById('update_id').value,'', 'quotation_inquery', 2 ,1)">
                        </td>
                        <td id="agent_td" colspan="4">
                          
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6" valign="middle" style="max-height:380px; min-height:15px;" id="size_color_breakdown11">
                        <? 
                        echo load_submit_buttons( $permission, "fnc_quotation_inquery", 0,0 ,"reset_form('quotationinquery_1','','')",1) ;                        ?>
                        </td>
                   </tr>
                </table>
            </form>
        </fieldset>
        <div id="size_color_breakdown">
        </div>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>