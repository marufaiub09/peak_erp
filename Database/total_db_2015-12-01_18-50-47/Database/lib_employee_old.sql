-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_employee_old`
--

DROP TABLE IF EXISTS `lib_employee_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_employee_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `first_name` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `middle_name` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `last_name` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `full_name_bangla` varchar(64) CHARACTER SET utf8 NOT NULL,
  `father_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `father_name_ban` varchar(50) CHARACTER SET utf8 NOT NULL,
  `mother_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `mother_name_ban` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `birth_place` varchar(70) CHARACTER SET utf8 NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `religion` int(11) NOT NULL,
  `marital_status` int(11) NOT NULL COMMENT '0=single; 1=married; 2=separated; 3=widow',
  `blood_group` varchar(5) CHARACTER SET utf8 NOT NULL,
  `nationality` varchar(32) CHARACTER SET utf8 NOT NULL,
  `national_id` varchar(64) CHARACTER SET utf8 NOT NULL,
  `passport_id` varchar(30) CHARACTER SET utf8 NOT NULL,
  `emp_catagory` varchar(20) CHARACTER SET utf8 NOT NULL,
  `designation_level` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `designation_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `functional_sup` int(11) NOT NULL,
  `admin_sup` int(11) NOT NULL,
  `id_card_no` varchar(20) CHARACTER SET utf8 NOT NULL,
  `joining_date` date NOT NULL,
  `category` int(1) NOT NULL DEFAULT '0',
  `confirmation_date` date NOT NULL,
  `punch_card_no` varchar(20) CHARACTER SET utf8 NOT NULL,
  `remark` text CHARACTER SET utf8 NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `company_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `division_id` int(11) NOT NULL DEFAULT '0',
  `division_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL DEFAULT '0',
  `department_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `section_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `line_no` varchar(50) CHARACTER SET utf8 NOT NULL,
  `line_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subsection_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `contract_start_date` date NOT NULL,
  `contract_end_date` date NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `emp_code` (`emp_code`),
  KEY `designation_level` (`designation_level`),
  KEY `joining_date` (`joining_date`),
  KEY `company_id` (`company_id`),
  KEY `location_id` (`location_id`),
  KEY `division_id` (`division_id`),
  KEY `department_id` (`department_id`),
  KEY `section_id` (`section_id`),
  KEY `subsection_id` (`line_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_employee_old`
--

LOCK TABLES `lib_employee_old` WRITE;
/*!40000 ALTER TABLE `lib_employee_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `lib_employee_old` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
