<?
/*-------------------------------------------- Comments

Purpose			: 	This form will create Order Tracking Variable Settings
					Select company and select Variable List that onchange will change content
					
Functionality	:	Must fill Company, Variable List

JS Functions	:

Created by		:	Bilas 
Creation date 	: 	12-10-2012
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Tracking Variable Settings", "../../", 1, 1,$unicode,'','');

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
?>
 
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
var permission='<? echo $permission; ?>';


function fnc_order_tracking_variable_settings( operation )
{
	
	var cbo_sales_year_started_date="";
	var cbo_tna_integrated="";
	var cbo_profit_calculative="";
	var cbo_consumption_basis="";
	var cbo_copy_quotation="";
 		
	if (document.getElementById('cbo_variable_list_wo').value*1==12)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_sales_year_started_date','Company Name*Sales Year started')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
					
				var sales_year_started	= escape(document.getElementById('sales_year_started').innerHTML);
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*cbo_sales_year_started_date*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+sales_year_started+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*cbo_sales_year_started_date*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 12
	
	
	if (document.getElementById('cbo_variable_list_wo').value*1==14)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_tna_integrated','Company Name*TNA Integrated')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
					
				var tna_integrated_td	= escape(document.getElementById('tna_integrated_td').innerHTML);
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*cbo_tna_integrated*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+tna_integrated_td+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*cbo_tna_integrated*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 14
	
	if (document.getElementById('cbo_variable_list_wo').value*1==15)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_profit_calculative','Company Name*Profit Calculative')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
					
				var profit_calculative_td	= escape(document.getElementById('profit_calculative_td').innerHTML);
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*cbo_profit_calculative*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+profit_calculative_td+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*cbo_profit_calculative*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 15
	
	
	if (document.getElementById('cbo_variable_list_wo').value*1==18)
	{
		
		if ( form_validation('cbo_company_name_wo','Company Name')==0 )
		{
			return;
		}
		else
		{					
				
				var item_category_id='';
				var process_loss_method='';
 				var countrow='';
				$(document).ready(function() {
                   	countrow = $('.rpt_table tbody tr').length; 
                });
				 
				for(i=1;i<=countrow;i++)
				{
					if(i==1) 
					{
						item_category_id=$('#item_category_id_'+i).val();
						process_loss_method=$('#process_loss_method_'+i).val();
					}
					else 
					{
						item_category_id+=","+$('#item_category_id_'+i).val();
						process_loss_method+=","+$('#process_loss_method_'+i).val();
					}
					
				}
				
				nocache = Math.random();
				var update_id = $('#update_id').val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete_process_loss_method&operation="+operation+"&item_category_id="+item_category_id+"&process_loss_method="+process_loss_method+"&update_id="+update_id+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 18
	
	
	if (document.getElementById('cbo_variable_list_wo').value*1==19)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_consumption_basis','Company Name*Consumption Basis')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var consumption_td	= escape(document.getElementById('consumption_td').innerHTML);
				var cbo_consumption_basis	= $("#cbo_consumption_basis").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+consumption_td+'&cbo_consumption_basis='+cbo_consumption_basis+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 19
	
	if (document.getElementById('cbo_variable_list_wo').value*1==20)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_copy_quotation','Company Name*Consumption Basis')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var copy_quotation_td	= escape(document.getElementById('copy_quotation_td').innerHTML);
				var cbo_copy_quotation	= $("#cbo_copy_quotation").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+copy_quotation_td+'&cbo_copy_quotation='+cbo_copy_quotation+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 20
	
	if (document.getElementById('cbo_variable_list_wo').value*1==21)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_conversion_from_chart','Company Name*Conversion From Chart')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var conversion_from_chart_td	= escape(document.getElementById('conversion_from_chart_td').innerHTML);
				var cbo_conversion_from_chart	= $("#cbo_conversion_from_chart").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+conversion_from_chart_td+'&cbo_conversion_from_chart='+cbo_conversion_from_chart+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 21
	
	if (document.getElementById('cbo_variable_list_wo').value*1==22)
	{
		
		if ( form_validation('cbo_company_name_wo','Company Name')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var cbo_cm_cost_method_td	= escape(document.getElementById('cbo_cm_cost_method_td').innerHTML);
				var cbo_cm_cost_method	= $("#cbo_cm_cost_method").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+cbo_cm_cost_method_td+'&cbo_cm_cost_method='+cbo_cm_cost_method+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 22
	
	if (document.getElementById('cbo_variable_list_wo').value*1==23)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_color_from_library','Company Name*Color From Library')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var color_from_library_td	= escape(document.getElementById('color_from_library_td').innerHTML);
				var cbo_color_from_library	= $("#cbo_color_from_library").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+color_from_library_td+'&cbo_color_from_library='+cbo_color_from_library+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 23
	
	
	if (document.getElementById('cbo_variable_list_wo').value*1==24)
	{
		//alert("xxx"); return;
		
		if ( form_validation('cbo_company_name_wo*cbo_yarn_dyeing_charge','Company Name*Yarn Dyeing Charge')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var color_from_library_td	= escape(document.getElementById('yarn_dyeing_charge_td').innerHTML);
				var cbo_color_from_library	= $("#cbo_yarn_dyeing_charge").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+color_from_library_td+'&cbo_color_from_library='+cbo_color_from_library+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 24
	
	if (document.getElementById('cbo_variable_list_wo').value*1==25)
	{
		//alert("xxx"); return;
		
		if ( form_validation('cbo_company_name_wo*publish_shipment_date','Company Name*Publish Shipment Date')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var publish_shipment_date_td	= escape(document.getElementById('publish_shipment_date_td').innerHTML);
				var publish_shipment_date	= $("#publish_shipment_date").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+publish_shipment_date_td+'&publish_shipment_date='+publish_shipment_date+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 24
	if (document.getElementById('cbo_variable_list_wo').value*1==26)
	{
		
		if ( form_validation('cbo_company_name_wo','Company Name')==0 )
		{
			return;
		}
		else
		{					
				
				var item_category_id='';
				var process_loss_method='';
 				var countrow='';
				$(document).ready(function() {
                   	countrow = $('.rpt_table tbody tr').length; 
                });
				 
				for(i=1;i<=countrow;i++)
				{
					if(i==1) 
					{
						item_category_id=$('#item_category_id_'+i).val();
						exeed_budget_qty=$('#txt_exeed_qty_'+i).val();
						exeed_budget_amt=$('#txt_exeed_amount_'+i).val();
						amt_exceed_lavel=$('#cbo_exceed_level_'+i).val();
					}
					else 
					{
						item_category_id+=","+$('#item_category_id_'+i).val();
						exeed_budget_qty+=","+$('#txt_exeed_qty_'+i).val();
						exeed_budget_amt+=","+$('#txt_exeed_amount_'+i).val();
						amt_exceed_lavel+=","+$('#cbo_exceed_level_'+i).val();
					}
					
				}
				
				nocache = Math.random();
				var update_id = $('#update_id').val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete_material_control&operation="+operation+"&item_category_id="+item_category_id+"&exeed_budget_qty="+exeed_budget_qty+"&exeed_budget_amt="+exeed_budget_amt+"&amt_exceed_lavel="+amt_exceed_lavel+"&update_id="+update_id+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	}
	
	if (document.getElementById('cbo_variable_list_wo').value*1==27)
	{
		
		if ( form_validation('cbo_company_name_wo','Company Name')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var cbo_commercial_cost_method_td	= escape(document.getElementById('cbo_commercial_cost_method_td').innerHTML);
				var cbo_commercial_cost_method	= $("#cbo_commercial_cost_method").val();
				var txt_commercial_cost_percent	= $("#txt_commercial_cost_percent").val();
				var cbo_editable	= $("#cbo_editable").val();
				//eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+cbo_commercial_cost_method_td+'&cbo_commercial_cost_method='+cbo_commercial_cost_method+'&txt_commercial_cost_percent='+txt_commercial_cost_percent+'&cbo_editable='+cbo_editable+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	}
	
	
	if (document.getElementById('cbo_variable_list_wo').value*1==28)
	{
		
		if ( form_validation('cbo_company_name_wo*txt_size_wise_repeat','Company Name*Consumption Basis')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
 				var copy_quotation_td	= escape(document.getElementById('copy_quotation_td').innerHTML);
				var txt_size_wise_repeat	= $("#txt_size_wise_repeat").val();
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				var data="action=save_update_delete&operation="+operation+"&sales_year_started="+copy_quotation_td+'&txt_size_wise_repeat='+txt_size_wise_repeat+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 20
	



	if (document.getElementById('cbo_variable_list_wo').value*1==29)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_duplicate_ship_date','Company Name*Duplicate Ship')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
				var cbo_duplicate_ship_date	= $("#cbo_duplicate_ship_date").val();
				
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				
				var data="action=save_update_delete&operation="+operation+"&cbo_duplicate_ship_date="+cbo_duplicate_ship_date+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 20
	
	if (document.getElementById('cbo_variable_list_wo').value*1==30)
	{
		
		if ( form_validation('cbo_company_name_wo*cbo_image_mandatory','Company Name*Mandatory')==0 )
		{
			return;
		}
		else
		{				
				nocache = Math.random();
				var image_mandatory	= $("#cbo_image_mandatory").val();
				
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				
				var data="action=save_update_delete&operation="+operation+"&image_mandatory="+image_mandatory+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				
				
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	} // end cbo_variable_list_wo type 30



	
	if (document.getElementById('cbo_variable_list_wo').value*1==31)
	{
		
		if ( form_validation('cbo_company_name_wo*txt_tna_process_type','Company Name*TNA Process Type')==0 )
		{
			return;
		}
		else
		{				
				nocache = Math.random();
				var txt_tna_process_type = $("#txt_tna_process_type").val();
				
				eval(get_submitted_variables('cbo_company_name_wo*cbo_variable_list_wo*update_id'));
				
				var data="action=save_update_delete&operation="+operation+"&tna_process_type="+txt_tna_process_type+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				
				
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	}
	
	if (document.getElementById('cbo_variable_list_wo').value*1==33) //cbo_po_current_date
	{
		
		
		if ( form_validation('cbo_company_name_wo*cbo_po_current_date','Company Name*Po Current Date Type')==0 )
		{
			return;
		}
		else
		{				
			
				var po_current_date = $("#cbo_po_current_date").val();
				
				var data="action=save_update_delete&operation="+operation+"&po_current_date="+po_current_date+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				//alert(data);
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	}
	if (document.getElementById('cbo_variable_list_wo').value*1==32) //Update Period
	{
		
		
		if ( form_validation('cbo_company_name_wo*update_period','Company Name*Update Period Type')==0 )
		{
			return;
		}
		else
		{				
			
				var update_period = $("#update_period").val();
				
				var data="action=save_update_delete&operation="+operation+"&update_period="+update_period+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				//alert(data);
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	}
	if (document.getElementById('cbo_variable_list_wo').value*1==34)
	{
		
		if (form_validation('cbo_company_name_wo*cbo_inquery_id_mandatory','Company Name*Inquery ID Mandatory')==0 )
		{
			return;
		}
		else
		{				
				var inquery_id_mandatory = $("#cbo_inquery_id_mandatory").val();
				var data="action=save_update_delete&operation="+operation+"&inquery_id_mandatory="+inquery_id_mandatory+get_submitted_data_string('cbo_company_name_wo*cbo_variable_list_wo*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/merchandising_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_order_tracking_variable_settings_reponse;
		}
	
	}//34 
	
	



}	

function fnc_order_tracking_variable_settings_reponse()
{
	if(http.readyState == 4) 
	{
	  
		var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		document.getElementById('update_id').value  = reponse[2];
		set_button_status(0, permission, 'fnc_order_tracking_variable_settings',1);
		reset_form('ordertrackingvariablesettings_1','variable_settings_container','');
		release_freezing();
	}
}	

function fnc_move_cursor(val,id, field_id,lnth,max_val)
	{
		var str_length=val.length;
		if(str_length==lnth)
		{
			$('#'+field_id).select();
			$('#'+field_id).focus();
		}
		if(val>max_val)
		{
			document.getElementById(id).value=max_val;
		}
	}
	
	
</script>

</head>

<body  onLoad="set_hotkey()">
	<div align="center" style="width:100%;">
         
				<? echo load_freeze_divs ("../../",$permission);  ?>
               
	<fieldset style="width:850px;">
		<legend>Merchandising Variable Settings</legend>
		<form name="ordertrackingvariablesettings_1" id="ordertrackingvariablesettings_1" autocomplete="off">	
      			<table  width="750" cellspacing="2" cellpadding="0" border="0">
            		<tr>
                		<td width="200" align="left" class="must_entry_caption">Company Name</td>
                        <td width="250">
                           			<? 
										echo create_drop_down( "cbo_company_name_wo", 250, "select company_name,id from lib_company where is_deleted=0  and status_active=1 $company_name order by company_name",'id,company_name', 1, '--- Select Company ---', 0, "" );
									?>
                        </td>
                		<td width="200" align="center">Variable List</td>
                        <td width="250">
                            		<? 
										echo create_drop_down( "cbo_variable_list_wo", 250, $order_tracking_module,'', '1', '---- Select ----', '',"show_list_view(this.value+'_'+document.getElementById('cbo_company_name_wo').value,'on_change_data','variable_settings_container','../variable/requires/merchandising_settings_controller','')",''); //data, action, div, path, extra_func 
									?>
                        </td>
            		</tr>
        		</table>
            <div style="width:895px; float:left; min-height:40px; margin:auto" align="center" id="variable_settings_container">
            </div>
		</form>	
	</fieldset>

    </div>
 </body>
</html>    

