﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
$store_arr=return_library_array( "select id, store_name from lib_store_location", "id", "store_name"  );
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$other_party_arr=return_library_array( "select id,other_party_name from lib_other_party", "id", "other_party_name"  );

	
if ($action=="job_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
    <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#job_no_id').val( id );
		$('#job_no_val').val( ddd );
	} 
		  
	</script>
     <input type="hidden" id="job_no_id" />
     <input type="hidden" id="job_no_val" />
 <?php
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and buyer_name=$data[1]";
	
	$sql="select id,job_no_prefix_num,job_no,buyer_name,style_ref_no,style_description from wo_po_details_master where company_name=$data[0] and is_deleted=0 $buyer_name ORDER BY job_no; ";	
	$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$arr=array(1=>$buyer);
	
	echo  create_list_view("list_view", "Job No,Buyer,Style Ref.,Style Desc.", "110,110,150,200","620","400",0, $sql, "js_set_value", "id,job_no", "", 1, "0,buyer_name,0,0,0", $arr , "job_no_prefix_num,buyer_name,style_ref_no,style_description", "daily_yarn_issue_report_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0','',1) ;
	disconnect($con);
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if(str_replace("'","",$cbo_yarn_type)!="") $yarn_type_cond=" and c.yarn_type in(".str_replace("'","",$cbo_yarn_type).")";
	if(str_replace("'","",$cbo_yarn_count)!="") $yarn_count_cond=" and c.yarn_count_id in(".str_replace("'","",$cbo_yarn_count).")";
	if(str_replace("'","",trim($txt_lot_no))=="") $lot_no="%%"; else $lot_no="%".str_replace("'","",trim($txt_lot_no))."%";
	if(str_replace("'","",$cbo_issue_purpose)!="" && str_replace("'","",$cbo_issue_purpose)!=0) $issue_purpose_cond=" and a.issue_purpose=$cbo_issue_purpose";
	
	
	if($db_type==0)
	{
		$from_date=change_date_format(str_replace("'","",$txt_date_from),'yyyy-mm-dd');
		$to_date=change_date_format(str_replace("'","",$txt_date_to),'yyyy-mm-dd');
	}
	if($db_type==2)
	{
		$from_date=change_date_format(str_replace("'","",$txt_date_from),'','',1);
		$to_date=change_date_format(str_replace("'","",$txt_date_to),'','',1);
	}
	
	$booking_arr=array();
	$dataArray=sql_select( "select a.id, a.booking_no, a.job_no, sum(b.grey_fab_qnty) as qnty from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and b.status_active=1 and b.is_deleted=0 group by a.id, a.booking_no, a.job_no " );// and a.company_id=$cbo_company_name	$job_no_cond
	foreach($dataArray as $row)
	{
		$booking_arr[$row[csf('booking_no')]]['qnty']=$row[csf('qnty')];
		$booking_arr[$row[csf('booking_no')]]['job']=$row[csf('job_no')];
	}
	//var_dump($booking_arr);
	
	$yarn_booking_array=array();
	$data_yarn=sql_select( "select a.id, a.ydw_no, b.job_no, sum(b.yarn_wo_qty) as qnty from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.ydw_no, b.job_no " );// and a.company_id=$cbo_company_name	$job_no_cond
	foreach($data_yarn as $row)
	{
		$yarn_booking_array[$row[csf('ydw_no')]]['qnty']=$row[csf('qnty')];
		$yarn_booking_array[$row[csf('ydw_no')]]['job']=$row[csf('job_no')];
	}
	
	$booking_without_array=array();
	$data_without=sql_select( "select a.id, a.booking_no, a.buyer_id, sum(b.grey_fabric) as qnty from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.booking_no, a.buyer_id" );
	foreach($data_without as $row)
	{
		$booking_without_array[$row[csf('booking_no')]]['qnty']=$row[csf('qnty')];
		$booking_without_array[$row[csf('booking_no')]]['booking_no']=$row[csf('booking_no')];
		$booking_without_array[$row[csf('booking_no')]]['buyer_id']=$row[csf('buyer_id')];
	}
	
	$job_array=array();
	if($db_type==0)
	{
		$sql_job="select a.job_no, a.buyer_name, a.style_ref_no, group_concat(b.po_number) as po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.job_no, a.buyer_name, a.style_ref_no";
	}
	else if($db_type==2)
	{
		$sql_job="select a.job_no, a.buyer_name, a.style_ref_no, listagg(CAST(po_number as varchar2(4000)),',') within group (order by po_number) as po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.job_no, a.buyer_name, a.style_ref_no";
	}
	$sql_job_result=sql_select($sql_job);
	foreach($sql_job_result as $row)
	{
		$po_number=implode(",",array_unique(explode(",",$row[csf('po_number')])));
		
		$job_array[$row[csf('job_no')]]['job_no']=$row[csf('job_no')];
		$job_array[$row[csf('job_no')]]['buyer_name']=$row[csf('buyer_name')];
		$job_array[$row[csf('job_no')]]['style_ref_no']=$row[csf('style_ref_no')];
		$job_array[$row[csf('job_no')]]['po_number']=$po_number;
	}
	//var_dump($job_array);
	
	/*$styleRef_arr=return_library_array( "select job_no,style_ref_no from wo_po_details_master", "job_no", "style_ref_no" );// where company_name=$cbo_company_name
	if($db_type==0)
	{
		$order_array=return_library_array( "select job_no_mst,group_concat(distinct(po_number)) as po_number from wo_po_break_down group by job_no_mst", "job_no_mst", "po_number" );
	
	}
	else if($db_type==2)
	{
		$order_array=return_library_array( "select job_no_mst,listagg(CAST(po_number as varchar2(4000)),',') within group (order by po_number) as po_number from wo_po_break_down group by job_no_mst", "job_no_mst", "po_number" );
		$order=explode(",",$order_array);
		
	$order_arr=$order_array;//implode(",",array_unique(explode(",",$order_array))); 

	}
	//print_r($order);die;*/
	$planning_arr=return_library_array("select b.id, a.booking_no from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b where a.id=b.mst_id", "id", "booking_no");
	
	$requisition_arr=array();
	$datareqsnArray=sql_select( "select requisition_no, knit_id, sum(yarn_qnty) as qnty from ppl_yarn_requisition_entry group by requisition_no,knit_id" );
	foreach($datareqsnArray as $row)
	{
		$requisition_arr[$row[csf('requisition_no')]]['qnty']=$row[csf('qnty')];
		$requisition_arr[$row[csf('requisition_no')]]['knit_id']=$row[csf('knit_id')];
	}
	
	if($zero_val==1) 
	{
		$value_width=1920;
		$span=20;
		$column='';
	}
	else 
	{
		$value_width=2120;
		$span=22;
		$column='<th rowspan="2" width="90">Avg. Rate (Tk)</th><th rowspan="2" width="110">Stock Value</th>';
	}	

	ob_start();
	?>
    <fieldset style="width:<?php echo $value_width-10; ?>px;">
        <table cellpadding="0" cellspacing="0" width="<?php echo $value_width; ?>">
            <tr>
               <td align="center" width="100%" colspan="<?php echo $span; ?>" style="font-size:16px"><strong><?php echo $report_title; ?></strong></td>
            </tr>
            <tr>
               <td align="center" width="100%" colspan="<?php echo $span; ?>" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
            </tr>
        </table>
        <table width="<?php echo $value_width-15; ?>" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
            <thead>
                <th width="30">SL</th>
                <th width="90">Job No</th>
                <th width="100">Buyer Name</th>
                <th width="135">Style No</th>
                <th width="150">Order No</th>
                <th width="100">Issue Basis</th>
                <th width="110">Issue No</th>
                <th width="70">Issue Date</th>
                <th width="80">Challan No</th>
                <th width="60">Count</th>
                <th width="70">Yarn Brand</th>
                <th width="80">Type</th>
                <th width="90">Color</th>
                <th width="70">Lot No</th>
                <th width="90">Issue Qty</th>
                <?php echo $column; ?>
                <th width="100">Issue Purpose</th> 
                <th width="110">Booking/ Reqn. No</th>
                <th width="100">Booking/ Reqn. Qty</th>
                <th width="130">Issue To</th>  
                <th>Store</th>
            </thead>
        </table>
        <div style="width:<?php echo $value_width-15; ?>px; overflow-y: scroll; max-height:380px;" id="scroll_body">
			<table width="<?php echo $value_width-35; ?>" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">    
                <tbody>
                <?php
					$i=1; $total_iss_qnty=0; $issue_qnty=0; $caption=''; $knitting_party=''; $total_amount=0; $grand_total_amount=0;
					$inside_outside_array=array(); $issue_purpose_array=array();
					
					$sql="select a.issue_number, a.issue_basis, a.issue_purpose, a.booking_id, a.booking_no, a.buyer_id, a.issue_date, a.knit_dye_source, a.knit_dye_company, a.challan_no, b.requisition_no, b.store_id, b.brand_id, b.cons_quantity as issue_qnty, c.yarn_type, c.yarn_count_id, c.lot, c.color, c.avg_rate_per_unit from inv_issue_master a, inv_transaction b, product_details_master c where a.item_category=1 and a.entry_form=3 and a.company_id=$cbo_company_name and a.issue_purpose in (1,2,4) and a.issue_date between '$from_date' and '$to_date' and a.id=b.mst_id and b.item_category=1 and b.transaction_type=2 and b.prod_id=c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.lot like '$lot_no' $yarn_type_cond $yarn_count_cond $issue_purpose_cond order by a.knit_dye_source,a.issue_number, a.issue_date";
					$result=sql_select($sql);
					foreach($result as $row)
					{
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							
						if($row[csf('knit_dye_source')]==1)
						{
							$knitting_party=$company_arr[$row[csf('knit_dye_company')]];
							$caption='Inside'; 
						}
						else if($row[csf('knit_dye_source')]==3)
						{
							$knitting_party=$supplier_arr[$row[csf('knit_dye_company')]];
							$caption='Outside';
						}
						else
						{
							$knitting_party="&nbsp;";
							$caption='&nbsp;';
						}	
						
						if(in_array($row[csf('knit_dye_source')],$inside_outside_array))
						{
							$print_caption=0;
						}
						else
						{
							$print_caption=1;
							$inside_outside_array[$i]=$row[csf('knit_dye_source')];
						}
						
						if($print_caption==1 && $i!=1)
						{
						?>
                        	<tr class="tbl_bottom"><td colspan="14" align="right"><b>Total</b></td><td align="right"><b><?php echo number_format($issue_qnty,2);?></b></td>
                            <?php
								if($zero_val==0)
								{
							?>
                                    <td align="right">&nbsp;</td><td align="right"><b><?php echo number_format($total_amount,2);?></b></td>
                            <?php
								}							?>
                            <td colspan="5">&nbsp;</td></tr>	
						<?php
							$issue_qnty=0;
							$total_amount=0;
						}
						if($print_caption==1)
						{
						?>
                        	<tr><td colspan="<?php echo $span; ?>" style="font-size:14px;" bgcolor="#CCCCCC">&nbsp;<b><?php echo $caption; ?></b></td></tr>	
						<?php
						}
                    	?>
                        <tr bgcolor="<?php echo $bgcolor;?>" onclick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                            <td width="30"><?php echo $i; ?></td>
                            <?php 
								$buyer=''; $job_no=''; $styRef=''; $booking_reqsn_qty=0; $knit_id=''; $order_no='';
								if($row[csf('issue_basis')]==1)
								{
									$booking_req_no=$row[csf('booking_no')];
									if($row[csf('issue_purpose')]==1 || $row[csf('issue_purpose')]==4)
									{
										$job_no=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['job_no'];
										$buyer=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['buyer_name'];
										$styRef=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['style_ref_no'];
										$order_no=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['po_number'];
										$booking_reqsn_qty=$booking_arr[$row[csf('booking_no')]]['qnty'];
									}
									else if($row[csf('issue_purpose')]==2)
									{
										$job_no=$job_array[$yarn_booking_array[$row[csf('booking_no')]]['job']]['job_no'];
										$buyer=$job_array[$yarn_booking_array[$row[csf('booking_no')]]['job']]['buyer_name'];
										$styRef=$job_array[$yarn_booking_array[$row[csf('booking_no')]]['job']]['style_ref_no'];
										$order_no=$job_array[$yarn_booking_array[$row[csf('booking_no')]]['job']]['po_number'];
										$booking_reqsn_qty=$yarn_booking_array[$row[csf('booking_no')]]['qnty'];
									}
									else 
									{
										$job_no='';
										$buyer='';
										$styRef='';
										$order_no='';
										$booking_reqsn_qty='';
									}
								}
								else if($row[csf('issue_basis')]==3)
								{
									$booking_req_no=$row[csf('requisition_no')];
									$knit_id=$requisition_arr[$row[csf('requisition_no')]]['knit_id'];
									$job_no=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['job_no'];
									$buyer=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['buyer_name'];
									$styRef=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['style_ref_no'];
									$order_no=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['po_number'];
									$booking_reqsn_qty=$requisition_arr[$row[csf('requisition_no')]]['qnty'];
								}
								else
								{
									$booking_req_no="&nbsp;";
									$buyer=$row[csf('buyer_id')];
									$job_no='';
									$styRef='';
									$booking_reqsn_qty=0;
									$order_no='';
								}
							?>
                            <td width="90" align="center"><p><?php echo $job_no; ?>&nbsp;</p></td>
                            <td width="100"><p><?php echo $buyer_arr[$buyer]; ?>&nbsp;</p></td>	
                            <td width="135"><p><?php echo $styRef; ?>&nbsp;</p></td>
                            <td width="150"><p><?php $order_n=implode(",",array_unique(explode(",",$order_no)));  echo $order_n; ?>&nbsp;</p></td>
                            <td width="100"><p><?php echo $issue_basis[$row[csf('issue_basis')]]; ?></p></td>
                            <td width="110"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="70" align="center"><?php echo change_date_format($row[csf('issue_date')]); ?>&nbsp;</td>
                            <td width="80"><p><?php echo $row[csf('challan_no')]; ?>&nbsp;</p></td>
                            <td width="60" align="center"><p><?php echo $count_arr[$row[csf('yarn_count_id')]]; ?>&nbsp;</p></td>
                            <td width="70"><p><?php echo $brand_arr[$row[csf('brand_id')]]; ?>&nbsp;</p></td>
                            <td width="80"><p><?php echo $yarn_type[$row[csf('yarn_type')]]; ?>&nbsp;</p></td>
                            <td width="90"><p><?php echo $color_arr[$row[csf('color')]]; ?>&nbsp;</p></td>
                            <td width="70"><p><?php echo $row[csf('lot')]; ?>&nbsp;</p></td>
                            <td width="90" align="right">
							<?php 
                                echo number_format($row[csf('issue_qnty')],2); 
                                $total_iss_qnty+=$row[csf('issue_qnty')];
                                $issue_qnty+=$row[csf('issue_qnty')];
                            ?>
                            </td>
                            <?php
								if($zero_val==0)
								{
								?>
                                    <td width="90" align="right">
									<?php
                                        $avgg=$row[csf('avg_rate_per_unit')]; 
                                        $avg_rate=$avgg/78;
                                        echo number_format($avg_rate,4); 
                                    ?>
                                    </td> 
                                    <td width="110" align="right">
									<?php
                                        $amount=$row[csf('issue_qnty')]*$avg_rate;
                                        echo number_format($amount,2);
                                        $total_amount+=$amount;
                                        $grand_total_amount+=$amount;
                                    ?>
                                    </td> 
                            	<?php
								}
							?>
                            <td width="100"><p><?php echo $yarn_issue_purpose[$row[csf('issue_purpose')]]; ?>&nbsp;</p></td> 
                            <td width="110" align="center"><?php echo $booking_req_no; ?></td>
                            <td width="100" align="right"><?php echo number_format($booking_reqsn_qty,2); ?></td>
                            <td width="130"><p><?php echo $knitting_party; ?>&nbsp;</p></td>
                            <td><p><?php echo $store_arr[$row[csf('store_id')]]; ?></td>
                        </tr>   
               	<?php  
					$i++; 
					}
					if(count($result)>0)
					{
					?>
						<tr class="tbl_bottom"><td colspan="14" align="right"><b>Total</b></td><td align="right"><b><?php echo number_format($issue_qnty,2);?></b></td>
                            <?php
							if($zero_val==0)
							{
							?>
                            	<td align="right">&nbsp;</td><td align="right"><b><?php echo number_format($total_amount,2);?></b></td>
                            <?php
							}
							?>
                        <td colspan="5">&nbsp;</td></tr>	
					<?php
					} 
					
					$k=1; $issue_qnty=0; $knitting_party='';$total_amount=0;
					$query="select a.issue_number, a.issue_basis, a.issue_purpose, a.booking_id, a.booking_no, a.buyer_id, a.issue_date, a.knit_dye_source, a.knit_dye_company, a.challan_no, a.other_party, b.requisition_no, b.store_id, b.brand_id, b.cons_quantity as issue_qnty, c.yarn_type, c.yarn_count_id, c.yarn_type, c.lot, c.color, c.avg_rate_per_unit from inv_issue_master a, inv_transaction b, product_details_master c where a.item_category=1 and a.entry_form=3 and a.company_id=$cbo_company_name and a.issue_purpose not in (1,2,4) and a.issue_date between '$from_date' and '$to_date' and a.id=b.mst_id and b.item_category=1 and b.transaction_type=2 and b.prod_id=c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.lot like '$lot_no' $yarn_type_cond $yarn_count_cond $issue_purpose_cond order by a.issue_purpose, a.issue_date";
					
					$nameArray=sql_select($query);
					foreach($nameArray as $row)
					{
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							
						if($row[csf('knit_dye_source')]==1)
						{
							$knitting_party=$company_arr[$row[csf('knit_dye_company')]];
						}
						else if($row[csf('knit_dye_source')]==3)
						{
							$knitting_party=$supplier_arr[$row[csf('knit_dye_company')]];
						}
						else
						{
							$knitting_party="&nbsp;";
						}	
						
						if(in_array($row[csf('issue_purpose')],$issue_purpose_array))
						{
							$print_caption=0;
						}
						else
						{
							$print_caption=1;
							$issue_purpose_array[$i]=$row[csf('issue_purpose')];
						}
						
						if($print_caption==1 && $k!=1)
						{
						?>
                        	<tr class="tbl_bottom"><td colspan="14" align="right"><b>Total</b></td><td align="right"><b><?php echo number_format($issue_qnty,2);?></b></td>
                            	<?php
								if($zero_val==0)
								{
								?>
                                    <td align="right">&nbsp;</td><td align="right"><b><?php echo number_format($total_amount,2);?></b></td>
                            	<?php
								}							
								?>
                            	<td colspan="5">&nbsp;</td></tr>	
						<?php
							$issue_qnty=0;
							$total_amount=0;
						}
						if($print_caption==1)
						{
						?>
                        	<tr><td colspan="<?php echo $span; ?>" style="font-size:14px;" bgcolor="#CCCCCC">&nbsp;<b><?php echo $yarn_issue_purpose[$row[csf('issue_purpose')]]; ?></b></td></tr>	
						<?php
						}
                    	?>
                        <tr bgcolor="<?php echo $bgcolor;?>" onclick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                            <td width="30"><?php echo $i; ?></td>
							 <?php 
                                $buyer=''; $job_no=''; $styRef=''; $booking_reqsn_qty=0; $knit_id=''; $order_no='';
                                if($row[csf('issue_basis')]==1)
                                {
                                    $booking_req_no=$row[csf('booking_no')];
                                    
                                    if($row[csf('issue_purpose')]==8)
                                    {
                                        $buyer=$booking_without_array[$row[csf('booking_no')]]['buyer_id'];
                                        $booking_reqsn_qty=$booking_without_array[$row[csf('booking_no')]]['qnty'];
                                        $job_no=''; 
                                        $styRef='';
                                        $order_no="";
                                    }
                                    else
                                    {
                                        $job_no=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['job_no'];
                                        $buyer=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['buyer_name'];
                                        $styRef=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['style_ref_no'];
                                        $order_no=$job_array[$booking_arr[$row[csf('booking_no')]]['job']]['po_number'];
                                        $booking_reqsn_qty=$booking_arr[$row[csf('booking_no')]]['qnty'];
                                    }
                                }
                                else if($row[csf('issue_basis')]==3)
                                {
                                    $booking_req_no=$row[csf('requisition_no')];
                                    $knit_id=$requisition_arr[$row[csf('requisition_no')]]['knit_id'];
                                    $job_no=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['job_no'];
                                    $buyer=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['buyer_name'];
                                    $styRef=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['style_ref_no'];
                                    $order_no=$job_array[$booking_arr[$planning_arr[$knit_id]]['job']]['po_number'];
                                    $booking_reqsn_qty=$requisition_arr[$row[csf('requisition_no')]]['qnty'];
                                }
                                else
                                {
                                    $booking_req_no="&nbsp;";
                                    $buyer=$row[csf('buyer_id')];
                                    $job_no='';
                                    $styRef='';
                                    $order_no='';
                                    $booking_reqsn_qty=0;
                                }
                                
                                if($row[csf('issue_purpose')]==5)
                                {
                                    $knitting_party=$other_party_arr[$row[csf('other_party')]];
                                }
                                else if($row[csf('issue_purpose')]==3)
                                {
                                    $knitting_party=$buyer_arr[$row[csf('buyer_id')]];
                                    $buyer='&nbsp;';
                                }
                            ?>                            
                            <td width="90" align="center"><p><?php echo $job_no; ?>&nbsp;</p></td>
                            <td width="100"><p><?php echo $buyer_arr[$buyer]; ?>&nbsp;</p></td>	
                            <td width="135"><p><?php echo $styRef; ?>&nbsp;</p></td>
                            <td width="150"><p><?php $order_n=implode(",",array_unique(explode(",",$order_no))); echo $order_n; ?>&nbsp;</p></td>
                            <td width="100"><p><?php echo $issue_basis[$row[csf('issue_basis')]]; ?></p></td>
                            <td width="110"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="70" align="center"><?php echo change_date_format($row[csf('issue_date')]); ?>&nbsp;</td>
                            <td width="80"><p><?php echo $row[csf('challan_no')]; ?>&nbsp;</p></td>
                            <td width="60" align="center"><p><?php echo $count_arr[$row[csf('yarn_count_id')]]; ?>&nbsp;</p></td>
                            <td width="70"><p><?php echo $brand_arr[$row[csf('brand_id')]]; ?>&nbsp;</p></td>
                            <td width="80"><p><?php echo $yarn_type[$row[csf('yarn_type')]]; ?>&nbsp;</p></td>
                            <td width="90"><p><?php echo $color_arr[$row[csf('color')]]; ?>&nbsp;</p></td>
                            <td width="70"><p><?php echo $row[csf('lot')]; ?>&nbsp;</p></td>
                            <td width="90" align="right">
							<?php 
                                echo number_format($row[csf('issue_qnty')],2); 
                                $total_iss_qnty+=$row[csf('issue_qnty')];
                                $issue_qnty+=$row[csf('issue_qnty')];
                            ?>
                            </td>
                            <?php
							if($zero_val==0)
							{
							?>
                                <td width="90" align="right">
                                    <?php
                                        $avgg=$row[csf('avg_rate_per_unit')]; 
                                        $avg_rate=$avgg/78;
                                        echo number_format($avg_rate,4); 
                                    ?>
                                </td> 
                                <td width="110" align="right">
                                    <?php
                                        $amount=$row[csf('issue_qnty')]*$avg_rate;
                                        echo number_format($amount,2);
                                        $total_amount+=$amount;
                                        $grand_total_amount+=$amount;
                                    ?>
                                </td> 
                            <?php
							}
							?>
                            <td width="100"><p><?php echo $yarn_issue_purpose[$row[csf('issue_purpose')]]; ?>&nbsp;</p></td> 
                            <td width="110" align="center"><?php echo $booking_req_no; ?>&nbsp;</td>
                            <td width="100" align="right"><?php echo number_format($booking_reqsn_qty,2); ?></td>
                            <td width="130"><p><?php echo $knitting_party; ?>&nbsp;</p></td>
                            <td><p><?php echo $store_arr[$row[csf('store_id')]]; ?></td>
                        </tr>   
               	<?php  
					$k++;
					$i++; 
					}
					
					if(count($nameArray)>0)
					{
					?>
						<tr class="tbl_bottom"><td colspan="14" align="right"><b>Total</b></td><td align="right"><b><?php echo number_format($issue_qnty,2);?></b></td>
                            <?php
							if($zero_val==0)
							{
							?>
                           		<td align="right">&nbsp;</td><td align="right"><b><?php echo number_format($total_amount,2);?></b></td>
                            <?php
							}
							?>
                            <td colspan="5">&nbsp;</td>
                         </tr>	
					<?php
					} 
				?>
                </tbody>    
                <tfoot>
                	<th colspan="14" align="right">Grand Total</th>
                    <th align="right"><?php echo number_format($total_iss_qnty,2); ?></th>
					<?php
					if($zero_val==0)
					{
                    ?>
                        <th>&nbsp;</th>
                        <th align="right"><?php echo number_format($grand_total_amount,2); ?></th>
					<?php
					}
                    ?>
                    <th colspan="5" align="right">&nbsp;</th>
                </tfoot>
            </table>
        </div>
    </fieldset>      
	<?php
    foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
}
?>
