<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$userid = $_SESSION['logic_erp']['user_id'];
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="on_change_data")
{
	  //echo $data; die;
	  $explode_data = explode("_",$data);
	  $type = $explode_data[0];
	  $company_id = $explode_data[1];
	  
	  
	  
	  if($type=="16") // User given item code-----------------------------------------------------------------------------------
	  {
  	?>
		  <fieldset style="width:100%; margin-bottom:10px">
		  <legend>User Given Code</legend>
				<table cellspacing="0" cellpadding="0" width="400" class="rpt_table">
					<thead> 
						<th class="must_entry_caption">Item Category</th>
						<th class="must_entry_caption">Code Required</th>
					</thead>
					<tr align="center">
						<td>
						<? 
						echo create_drop_down( "cbo_item_category", 180, $item_category,'', 1, '---- Select ----', "0", "" );
						?>
						</td>
						<td>
						 <? 
						echo create_drop_down( "cbo_item_status", 180, $yes_no,'', 1, '---- Select ----',"0", "" );
						 ?>	
						</td>
					</tr>
                    <tr>
                        <td colspan="2" valign="bottom" align="center" class="button_container">
                        <input  type="hidden"name="update_id" id="update_id" value="">
                        <? 
                         echo load_submit_buttons( $permission, "fnc_variable_settings_inventory", 0,0 ,"reset_form('inventoryvariablesettings_1','','')",1);
                        ?>
                        </td>					
                   </tr>
				</table>
               
			<div id="list_view_con" style="margin-top:15px"> 
				<?
			  $company_name_arr=return_library_array( "select company_name,id from lib_company", "id", "company_name"  );
			  $arr=array (0=>$company_name_arr,1=>$item_category,2=>$yes_no);
			  echo  create_list_view ( "list_view", "Company Name,Item Category,Status", "150,150,150","470","220",0, "select id,company_name,item_category_id,user_given_code_status from  variable_settings_inventory where company_name='$company_id' and variable_list='$type' and status_active=1 and is_deleted=0 ","get_php_form_data","id","'load_php_data_to_form'",1,"company_name,item_category_id,user_given_code_status", $arr , "company_name,item_category_id,user_given_code_status", "../variable/requires/inventory_settings_controller",'setFilterGrid("list_view",-1);' );
				  ?>  
				</div>
	  </fieldset>		  
  	<?
	  } // ----------------------------------- User given item code END ----------------------------------------------------------------------------
	  
	  
	
	else if($type==8){	//Inventory_ILE/Landed Cost Standard--------------------------------------------------------------------------------------------

	?>

         <fieldset>
         <legend>ILE/Landed Cost Standard</legend>
                 
                    <table cellpadding="2" cellspacing="0" width="100%" class="rpt_table" id="tbl_variable_list" >
                        <thead>
                        	<tr>
                                <th width="170">Category</th>
                                <th width="170">Item Group</th>
                                <th width="170">Source</th>
                                <th width="170">Standard %</th>
                        	</tr>
                       	</thead>     
                    	<tbody>             
                    
                    <?php 
					   $itemGroupArr = return_library_array("select id,item_name from lib_item_group","id","item_name");
                       $sqlResult= sql_select("select category,item_group,source,standard from variable_inv_ile_standard where company_name='$company_id' and variable_list=8 order by id");
                       $row_count=count($sqlResult);  
					   $i=1;             
                       foreach( $sqlResult as $rows ) 
                       {
							?>
							<tr> 
								<td width="170" align="center">
									<?
										echo create_drop_down( "cbo_category".$i, 170, $item_category,"", 1, "-- Select --", $rows[csf("category")], "",0 );
									?>  
								</td>
								<td width="170" align="center">
									<?
										echo create_drop_down( "cbo_item_group".$i, 170, $itemGroupArr,"", 1, "-- Select --", $rows[csf("item_group")], "",0 );
									?> 
								</td>
								<td width="170" align="center">
									<?
										echo create_drop_down( "cbo_source".$i, 170, $source,"", 1, "-- Select --", $rows[csf("source")],"",0 );
									?>
								</td>
								<td width="170" align="center"><input type="text" name="txt_standard<? echo $i; ?>" id="txt_standard<? echo $i; ?>" value="<? echo $rows[csf("standard")]; ?>" class="text_boxes_numeric" onfocus="add_variable_row(<? echo $i; ?>)"   style="width:160px"/></td>
							</tr>
                        <?
							$i++;
					   	}
						if($row_count==0 || $row_count==""){ //----------for new save--------
                        ?>
                        	<tr> 
								<td width="170" align="center">
									<?
										echo create_drop_down( "cbo_category".$i, 170, $item_category,"", 1, "-- Select --", "", "",0 );
									?>  
								</td>
								<td width="170" align="center">
									<?
										echo create_drop_down( "cbo_item_group".$i, 170, $itemGroupArr,"", 1, "-- Select --", "", "",0 );
									?> 
								</td>
								<td width="170" align="center">
									<?
										echo create_drop_down( "cbo_source".$i, 170, $source,"", 1, "-- Select --", 0, "",0 );
									?>
								</td>
								<td width="170" align="center"><input type="text" name="txt_standard<? echo $i; ?>" id="txt_standard<? echo $i; ?>" class="text_boxes_numeric" onfocus="add_variable_row(<? echo $i; ?>)"   style="width:160px"/></td>
							</tr>                               
                                
                        <? } ?>  
                        </tbody>                      	
                    </table>
                    <table>   
                        <tr>
                            <td colspan="4">&nbsp;  </td>                                    
                        </tr>
                        <tr>
                            <td colspan="4" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="">
                                <? 
								 if($row_count>0) $flag=1; else $flag=0;
                                 echo load_submit_buttons( $permission, "fnc_variable_settings_inventory_ile", $flag,0,"reset_form('inventoryvariablesettings_1','variable_settings_container','')",1);
                                ?>
                            </td>					
                       </tr>              
                   </table>    
            </fieldset>    
                    
         <?
        }
		
		
		else if($type==17){	 //Book keeping mmethod---------------------------------------------------------------------------------

	?>

            <fieldset style="width:100%; margin-bottom:10px">
              <legend>Allocated Quantity</legend>
                    <table cellspacing="0" cellpadding="0" width="400" class="rpt_table">
                        <thead> 
                            <th class="must_entry_caption">Item Category</th>
                            <th class="must_entry_caption">Method</th>
                        </thead>
                        <tr align="center">
                            <td>
								<? 
                                    echo create_drop_down( "cbo_item_category", 180, $item_category,'', 1, '---- Select ----', "0", "" );
                                ?>
                            </td>
                            <td>
								 <? 
                                	echo create_drop_down( "cbo_store_method", 180, $store_method,'', 1, '---- Select ----',"0", "" );
                                 ?>	
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="bottom" align="center" class="button_container">                                
                                <? 
                                 	echo load_submit_buttons( $permission, "fnc_variable_settings_inventory_store_method", 0,0 ,"reset_form('inventoryvariablesettings_1','','')",1);
                                ?>
                                <input  type="hidden"name="update_id" id="update_id" value="">
                            </td>					
                        </tr>
                    </table>
                   
                    <div id="list_view_con" style="margin-top:15px;"> 
                        <?
							  $company_name_arr=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
							  $arr=array (0=>$company_name_arr,1=>$item_category,2=>$store_method);
							  echo  create_list_view ( "list_view", "Company Name,Item Category,Method", "200,200,200","650","220",0, "select id,company_name,item_category_id,store_method from  variable_settings_inventory where company_name='$company_id' and variable_list='$type' and status_active=1 and is_deleted=0 ","get_php_form_data","id","'load_data_for_store_method_form'",1,"company_name,item_category_id,store_method", $arr , "company_name,item_category_id,store_method", "../variable/requires/inventory_settings_controller",'setFilterGrid("list_view",-1);' );
                         ?>  
                   </div>
          	</fieldset>	 
                    
         <?
		 
        }// END Book keeping mmethod---------------------------------------------------------------------------------
	  
	  
	  else if($type==18){	 //Allocated Quantity-------------------------------------------------------------------

	?>

            <fieldset style="width:100%; margin-bottom:10px">
              <legend>User Given Code</legend>
                    <table cellspacing="0" cellpadding="0" width="400" class="rpt_table">
                        <thead> 
                            <th class="must_entry_caption">Item Category</th>
                            <th class="must_entry_caption">Allocated</th>
                        </thead>
                        <tr align="center">
                            <td>
								<? 
                                    echo create_drop_down( "cbo_item_category", 180, $item_category,'', 1, '---- Select ----', "0", "" );
                                ?>
                            </td>
                            <td>
								 <? 
                                	echo create_drop_down( "cbo_allocated", 180, $yes_no,'', 1, '---- Select ----',"0", "" );
                                 ?>	
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="bottom" align="center" class="button_container">                                
                                <? 
                                 	echo load_submit_buttons( $permission, "fnc_variable_settings_inventory_allocation", 0,0 ,"reset_form('inventoryvariablesettings_1','','')",1);
                                ?>
                                <input  type="text"name="update_id" id="update_id" value="">
                            </td>					
                        </tr>
                    </table>
                   
                    <div id="list_view_con" style="margin-top:15px;"> 
                        <?
							  $company_name_arr=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
							  $arr=array (0=>$company_name_arr,1=>$item_category,2=>$yes_no);
							  echo  create_list_view ( "list_view", "Company Name,Item Category,Allocation", "200,200,200","650","220",0, "select id,company_name,item_category_id,allocation from  variable_settings_inventory where company_name='$company_id' and variable_list='$type' and status_active=1 and is_deleted=0 ","get_php_form_data","id","'load_data_for_allocation_form'",1,"company_name,item_category_id,allocation", $arr , "company_name,item_category_id,allocation", "../variable/requires/inventory_settings_controller",'setFilterGrid("list_view",-1);' );
                         ?>  
                   </div>
          	</fieldset>	 
        
         <?
		 
        }// END Allocated Method---------------------------------------------------------------------------------
	  
  }


//-------------------------------------------------------------------
//-------------------------------------------------------------------
if($action=="append_load_details_container")
{
	$itemGroupArr = return_library_array("select id,item_name from lib_item_group","id","item_name");
	$i=$data+1;
 	?>
            <tr> 
                <td width="170" align="center">
                    <?
                        echo create_drop_down( "cbo_category".$i, 170, $item_category,"", 1, "-- Select --", 0, "",0 );
                    ?>  
                </td>
                <td width="170" align="center">
                    <?
                        echo create_drop_down( "cbo_item_group".$i, 170, $itemGroupArr,"", 1, "-- Select --", 0, "",0 );
                    ?> 
                </td>
                <td width="170" align="center">
                    <?
                        echo create_drop_down( "cbo_source".$i, 170, $source,"", 1, "-- Select --", 0, "",0 );
                    ?>
                </td>
                <td width="170" align="center"><input type="text" name="txt_standard<? echo $i; ?>" id="txt_standard<? echo $i; ?>" value="<? echo $rows[csf("standard")]; ?>" class="text_boxes_numeric" onfocus="add_variable_row(<? echo $i; ?>)"   style="width:160px"/></td>
            </tr>
           
     <?   
	 exit();                    
}
//-------------------------------------------------------------------
//-------------------------------------------------------------------


if ($action=="on_change_data_list")
{
		$ex_data = explode("_",$data);
		$type = $ex_data[0];
		$company_id = $ex_data[1];
		
		$company_name_arr=return_library_array( "select company_name,id from lib_company", "id", "company_name"  );
		$arr=array (0=>$company_name_arr,1=>$item_category,2=>$yes_no);
		
		echo  create_list_view ( "list_view", "Company Name,Item Category,Status", "150,150,150","470","220",0, "select id,company_name,item_category_id,user_given_code_status from  variable_settings_inventory where company_name='$company_id' and variable_list='$type' and status_active=1 and is_deleted=0 ","get_php_form_data","id","'load_php_data_to_form'",1,"company_name,item_category_id,user_given_code_status", $arr , "company_name,item_category_id,user_given_code_status", "../variable/requires/inventory_settings_controller",'setFilterGrid("list_view",-1);' );
		  
}

if ($action=="load_php_data_to_form")
{
	  $nameArray=sql_select( "select id,company_name,variable_list,item_category_id,user_given_code_status from variable_settings_inventory where id='$data'" );
	  foreach ($nameArray as $inf)
	  {
		  echo "document.getElementById('cbo_company_name').value = '".($inf[csf("company_name")])."';\n"; 
		  echo "document.getElementById('cbo_variable_list').value = '".($inf[csf("variable_list")])."';\n";
		  echo "document.getElementById('cbo_item_category').value = '".($inf[csf("item_category_id")])."';\n"; 
		  echo "document.getElementById('cbo_item_status').value = '".($inf[csf("user_given_code_status")])."';\n";
		  echo "document.getElementById('update_id').value = '".($inf[csf("id")])."';\n";    
		  echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_variable_settings_inventory',1);\n";  
	  }
}
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here==============================================================================
	{
		
		if(is_duplicate_field( "item_category_id", " variable_settings_inventory", "item_category_id=$cbo_item_category and company_name=$cbo_company_name and variable_list=$cbo_variable_list and is_deleted=0" ) == 1)
		{
				echo "11**0"; die;
		}
		else
		{
		  	$con = connect();
			  if($db_type==0)
			  {
				  mysql_query("BEGIN");
			  }
			  $id=return_next_id( "id", "variable_settings_inventory", 1 );
			  
			  $field_array="id,company_name,variable_list,item_category_id,user_given_code_status,inserted_by,insert_date,status_active,is_deleted";
			  $data_array="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",".$cbo_item_category.",".$cbo_item_status.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1,'0')";
			  //print_r($data_array);die;
			  $rID=sql_insert("variable_settings_inventory",$field_array,$data_array,1);
			  if($db_type==0)
			  {
				  if($rID ){
					  mysql_query("COMMIT");  
					  echo "0**".$rID;
				  }
				  else{
					  mysql_query("ROLLBACK"); 
					  echo "10**".$rID;
				  }
			  }
			  
			if($db_type==2 || $db_type==1 )
			{
			
			if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			
			}
			disconnect($con);
			die;
		}
	  }
	  else if ($operation==1)   // Update Here=========================================================
	  {
		  
		 //  echo "select item_category_id from  variable_settings_inventory where item_category_id=$cbo_item_category and company_name=$cbo_company_name and variable_list=$cbo_variable_list and is_deleted=0";die;
		  if(is_duplicate_field( "item_category_id", " variable_settings_inventory", "item_category_id=$cbo_item_category and company_name=$cbo_company_name   and variable_list=$cbo_variable_list and is_deleted=0 and id!=$update_id" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			  $con = connect();
			  if($db_type==0)
			  {
				  mysql_query("BEGIN");
			  }
			  
			  $field_array="company_name*variable_list*item_category_id*user_given_code_status*updated_by*update_date*status_active*is_deleted";
			  $data_array="".$cbo_company_name."*".$cbo_variable_list."*".$cbo_item_category."*".$cbo_item_status."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*".'1'."*'0'";
			 // print_r($data_array);die;
			  $rID=sql_update("variable_settings_inventory",$field_array,$data_array,"id","".$update_id."",1);
			  //print_r($rID);die;
			  if($db_type==0)
			  {
				  if($rID ){
					  mysql_query("COMMIT");  
					  echo "1**".$rID;
				  }
				  else{
					  mysql_query("ROLLBACK"); 
					  echo "10**".$rID;
				  }
			  }
			  
			  if($db_type==2 || $db_type==1 )
			    {
			     if($rID )
					{
						oci_commit($con);   
						echo "1**".$rID;
					}
					else{
						oci_rollback($con);
						echo "10**".$rID;
					}
				
			    }
			  disconnect($con);
			  die;
		}
	  }
	  else if ($operation==2)   // Delete Here===================================================
	  {
		  $con = connect();
		  if($db_type==0)
		  {
			  mysql_query("BEGIN");
		  }
		  
		  $field_array="updated_by*update_date*status_active*is_deleted";
		  $data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
		  
		  $rID=sql_delete("variable_settings_inventory",$field_array,$data_array,"id","".$update_id."",1);
		  
		  if($db_type==0)
		  {
			  if($rID ){
				  mysql_query("COMMIT");  
				  echo "2**".$rID;
			  }
			  else{
				  mysql_query("ROLLBACK"); 
				  echo "10**".$rID;
			  }
		  }
		   if($db_type==2 || $db_type==1 )
			  {
			
			if($rID )
			    {
					oci_commit($con);   
					echo "2**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
				
			  }
		  	disconnect($con);
		  	die;
	}
}


//---------------------------------------------ile standard save here-----------------------------------// 
if ($action=="save_update_delete_ile")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here==============================================================================
	{
 	
		$con = connect();
		if($db_type==0)
		{
		  mysql_query("BEGIN");
		}
		
		$id=return_next_id( "id", "variable_inv_ile_standard", 1 );		
		$field_array="id,company_name, variable_list, category, item_group, source, standard,inserted_by,insert_date";
		$rows = str_replace("'","",$row);
		$data_array="";
		for($i=1;$i<=$rows;$i++)
		{ 
			
			$cbo_category 	= 'cbo_category'.$i;
			$cbo_item_group = 'cbo_item_group'.$i;
			$cbo_source 	= 'cbo_source'.$i;
			$txt_standard 	= 'txt_standard'.$i;
			if( $$cbo_category!=0 || $$cbo_item_group!=0 || $$cbo_source!=0 ||  $$txt_standard!="" )
			{ 
				if(trim($data_array)!="") $data_array .= ",";
				$data_array.="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",".$$cbo_category.",".$$cbo_item_group.",".$$cbo_source.",".$$txt_standard.",".$userid.",'".$pc_date_time."')";
				$id=$id+1;
			}
		}
		
		//echo $data_array."#####".$field_array;die;
		$rID=sql_insert("variable_inv_ile_standard",$field_array,$data_array,1);
		if($db_type==0)
		{
		  if($rID ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1 )
		{
		
			 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		
		}
		disconnect($con);
		die;		
	}
 	else if ($operation==1)   // Update Here=========================================================
	{
		 
		$con = connect();
		if($db_type==0)
		{
		  mysql_query("BEGIN");
		}
		
		// delete here--------------- 
		$deleteSQL = execute_query("DELETE FROM variable_inv_ile_standard WHERE company_name=$cbo_company_name",0);
		
		$id=return_next_id( "id", "variable_inv_ile_standard", 1 );		
		$field_array="id,company_name, variable_list, category, item_group, source, standard,inserted_by,insert_date";
		$rows = str_replace("'","",$row);
		$data_array="";
		for($i=1;$i<=$rows;$i++)
		{ 
			
			$cbo_category 	= 'cbo_category'.$i;
			$cbo_item_group = 'cbo_item_group'.$i;
			$cbo_source 	= 'cbo_source'.$i;
			$txt_standard 	= 'txt_standard'.$i;
			if( $$cbo_category!=0 || $$cbo_item_group!=0 || $$cbo_source!=0 ||  $$txt_standard!="" )
			{ 
				if(trim($data_array)!="") $data_array .= ",";
				$data_array.="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",".$$cbo_category.",".$$cbo_item_group.",".$$cbo_source.",".$$txt_standard.",".$userid.",'".$pc_date_time."')";
				$id=$id+1;
			}
		}
		
		//print_r($data_array);die;
		$rID=sql_insert("variable_inv_ile_standard",$field_array,$data_array,1);
		if($db_type==0)
		{
		  if( $rID ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1 )
		{   
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		
		}
		disconnect($con);
		die;
	 }
	 else if ($operation==2)   // Delete Here===================================================
	 {
		  //no operation
	 }
	  
}
 
 
 
 
 
//save update delete for store method
if ($action=="save_update_delete_store_method")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here==============================================================================
	{
 	
		$con = connect();
		if($db_type==0)
		{
		  mysql_query("BEGIN");
		}
		
		$check = is_duplicate_field( "id", "variable_settings_inventory", "company_name=$cbo_company_name and variable_list=$cbo_variable_list and item_category_id=$cbo_item_category" );
		if($check==1)
		{
			echo"11";
			exit();
		}
		
		$id=return_next_id( "id", "variable_settings_inventory", 1 );		
		$field_array="id, company_name, variable_list, item_category_id, store_method, inserted_by, insert_date";		
		$data_array="";		 
		$data_array.="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",".$cbo_item_category.",".$cbo_store_method.",".$userid.",'".$pc_date_time."')";
		$id=$id+1;
						
		//echo $field_array."#####".$data_array;die;
		$rID=sql_insert("variable_settings_inventory",$field_array,$data_array,1);
		if($db_type==0)
		{
		  if($rID ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1 )
		{
		
			 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			
		}
		disconnect($con);
		die;		
	}
 	else if ($operation==1)   // Update Here=========================================================
	{
		 
		$con = connect();
		if($db_type==0)
		{
		  mysql_query("BEGIN");
		}
		
		if(str_replace("'",$update_id)=="")
		{
			echo "10";
		}
		
		$check = is_duplicate_field( "id", "variable_settings_inventory", "company_name=$cbo_company_name and variable_list=$cbo_variable_list and item_category_id=$cbo_item_category and id!=$update_id" );
		if($check==1)
		{
			echo"11";
			exit();
		}
		 
		$field_array="company_name*variable_list*item_category_id*store_method*updated_by*update_date";		
		$data_array="";
		$data_array.="".$cbo_company_name."*".$cbo_variable_list."*".$cbo_item_category."*".$cbo_store_method."*".$userid."*'".$pc_date_time."'"; 		
		//print_r($data_array);die;
		$rID=sql_update("variable_settings_inventory",$field_array,$data_array,"id",$update_id,1);
		if($db_type==0)
		{
		  if( $rID ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1 )
		{
		
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			
		}
		disconnect($con);
		die;
	 }
	 else if ($operation==2)   // Delete Here===================================================
	 {
		 $deleteSQL = execute_query("DELETE FROM variable_settings_inventory WHERE id=update_id");
		 if($db_type==0)
		{
		  if( $deleteSQL ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1 )
		{
			 if($deleteSQL )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		}
		disconnect($con);
		die;
	 }	  
} 



//save update delete for allocated quantity
if ($action=="save_update_delete_allocated")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here==============================================================================
	{
 	
		$con = connect();
		if($db_type==0)
		{
		  mysql_query("BEGIN");
		}
		
		$check = is_duplicate_field( "id", "variable_settings_inventory", "company_name=$cbo_company_name and variable_list=$cbo_variable_list and item_category_id=$cbo_item_category" );
		if($check==1)
		{
			echo"11";
			exit();
		}
		
		$id=return_next_id( "id", "variable_settings_inventory", 1 );		
		$field_array="id, company_name, variable_list, item_category_id, allocation, inserted_by, insert_date";		
		$data_array="";		 
		$data_array.="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",".$cbo_item_category.",".$cbo_allocated.",".$userid.",'".$pc_date_time."')";
		$id=$id+1;
						
		//echo $field_array."#####".$data_array;die;
		$rID=sql_insert("variable_settings_inventory",$field_array,$data_array,1);
		if($db_type==0)
		{
		  if($rID ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1 )
		{
		
			 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			
		}
		disconnect($con);
		die;		
	}
 	else if ($operation==1)   // Update Here=========================================================
	{
		
		$con = connect();
		if($db_type==0)
		{
		  mysql_query("BEGIN");
		}
	
		if(str_replace("'","",$update_id)=="")
		{
			echo "10";
		}
		
		$check = is_duplicate_field( "id", "variable_settings_inventory", "company_name=$cbo_company_name and variable_list=$cbo_variable_list and item_category_id=$cbo_item_category and id!=$update_id" );
		if($check==1)
		{
			echo"11";
			exit();
		}
		 
		$field_array="company_name*variable_list*item_category_id*allocation*updated_by*update_date";		
		$data_array="";
		$data_array.="".$cbo_company_name."*".$cbo_variable_list."*".$cbo_item_category."*".$cbo_allocated."*".$userid."*'".$pc_date_time."'"; 		
		//print_r($data_array);die;
		$rID=sql_update("variable_settings_inventory",$field_array,$data_array,"id",$update_id,1);
		if($db_type==0)
		{
		  if( $rID ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1 )
		{
		
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			
		}
		disconnect($con);
		die;
	 }
	 else if ($operation==2)   // Delete Here===================================================
	 {
		$con = connect();
		if($db_type==0)
		{
		  mysql_query("BEGIN");
		}
		 $deleteSQL=execute_query("DELETE FROM variable_settings_inventory WHERE id=$update_id",1);
		 if($db_type==0)
		{
		  if( $deleteSQL ){
			  mysql_query("COMMIT");  
			  echo "0**".$rID;
		  }
		  else{
			  mysql_query("ROLLBACK"); 
			  echo "10**".$rID;
		  }
		}
		
		if($db_type==2 || $db_type==1)
		{
			 if($deleteSQL )
			    {
					oci_commit($con);   
					echo "2**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		}
		disconnect($con);
		die;
	 }	  
}
//for update get data
if($action=="load_data_for_store_method_form")
{
	 $nameArray=sql_select( "select id,company_name,variable_list,item_category_id,user_given_code_status, store_method from variable_settings_inventory where id='$data'" );
	  foreach ($nameArray as $inf)
	  {
		  echo "document.getElementById('cbo_company_name').value = '".($inf[csf("company_name")])."';\n"; 
		  echo "document.getElementById('cbo_variable_list').value = '".($inf[csf("variable_list")])."';\n";
		  echo "document.getElementById('cbo_item_category').value = '".($inf[csf("item_category_id")])."';\n"; 
		  echo "document.getElementById('cbo_store_method').value = '".($inf[csf("store_method")])."';\n";
		  echo "document.getElementById('update_id').value = '".($inf[csf("id")])."';\n";    
		  echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_variable_settings_inventory_store_method',1);\n";  
	  }
	  exit();

}



//for update get data allocation------------------
if($action=="load_data_for_allocation_form")
{
	//echo "select id,company_name,variable_list,item_category_id,user_given_code_status, allocation from variable_settings_inventory where id='$data'";
	 $nameArray=sql_select( "select id,company_name,variable_list,item_category_id,user_given_code_status, allocation from variable_settings_inventory where id='$data'",1 );
	  foreach ($nameArray as $inf)
	  {
		  echo "document.getElementById('cbo_company_name').value = '".($inf[csf("company_name")])."';\n"; 
		  echo "document.getElementById('cbo_variable_list').value = '".($inf[csf("variable_list")])."';\n";
		  echo "document.getElementById('cbo_item_category').value = '".($inf[csf("item_category_id")])."';\n"; 
		  echo "document.getElementById('cbo_allocated').value = '".($inf[csf("allocation")])."';\n";
		  echo "document.getElementById('update_id').value = '".($inf[csf("id")])."';\n";    
		  echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_variable_settings_inventory_allocation',1);\n";  
	  }
	  exit();

}




 
?>