<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_buyer")
{
	//$data=explode('_',$data);
	echo create_drop_down( "cbo_buyer_id", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",0);  
	exit();
}

if ($action=="job_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
    <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#job_no_id').val( id );
		$('#job_no_val').val( ddd );
	} 
		  
	</script>
     <input type="hidden" id="job_no_id" />
     <input type="hidden" id="job_no_val" />
 <?php
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and buyer_name=$data[1]";
	if($db_type==0)
	{
		$year_cond= "year(insert_date)as year";
	}
	else if($db_type==2)
	{
		$year_cond= "TO_CHAR(insert_date,'YYYY') as year";
	}
	$sql="select id, job_no_prefix_num, job_no, $year_cond, buyer_name, style_ref_no, style_description from wo_po_details_master where company_name=$data[0] and is_deleted=0 $buyer_name ORDER BY job_no DESC";	
	$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$arr=array(2=>$buyer);

	echo  create_list_view("list_view", "Job No,Year,Buyer,Style Ref.,Style Desc.", "70,70,110,150,180","620","350",0, $sql, "js_set_value", "id,job_no", "", 1, "0,0,buyer_name,0,0,0", $arr , "job_no_prefix_num,year,buyer_name,style_ref_no,style_description", "grey_fabric_issue_status_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
	disconnect($con);
	exit();
}

if ($action=="batch_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
    <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#batch_no_id').val( id );
		$('#batch_no_val').val( ddd );
	} 
		  
	</script>
     <input type="hidden" id="batch_no_id" />
     <input type="hidden" id="batch_no_val" />
 <?php
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and buyer_name=$data[1]";
	$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
	$sql="select id, batch_no, extention_no, batch_for, booking_no, color_id, batch_weight from pro_batch_create_mst where entry_form=0 and company_id=$data[0] and is_deleted=0 ";//entry_form=16 and
	$arr=array(2=>$color_library);
	
	echo  create_list_view("list_view", "Batch No,Batch Ext.,Color,Booking No, Batch For,Batch Weight ", "100,60,100,130,100,80","640","350",0, $sql, "js_set_value", "id,batch_no", "", 1, "0,0,color_id,0,0,0", $arr , "batch_no,extention_no,color_id,booking_no,batch_for,batch_weight", "employee_info_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0,2','',1) ;
	disconnect($con);
	exit();
}//BatchNumberShow;

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_buyer=str_replace("'","",$cbo_buyer_id);
	$job_no_mst=str_replace("'","",$txt_job_no);
	$job_no_id=str_replace("'","",$txt_job_no_id);
	$batch_no_id=str_replace("'","",$txt_batch_no_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$constArr=return_library_array( "select id,construction from  lib_yarn_count_determina_mst", "id", "construction"  );
	$countArr=return_library_array( "select id,yarn_count from  lib_yarn_count", "id", "yarn_count"  );
	$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$batch_arr = return_library_array( "select id, batch_no from pro_batch_create_mst",'id','batch_no');
	ob_start();	
	?>
    <div>
    <table width="1230px" cellpadding="0" cellspacing="0" id="caption" align="center">
        <tr>
           <td align="center" width="100%" colspan="13" class="form_caption"><strong style="font-size:18px">Company Name:<?php echo $company_library[$cbo_company]; ?></strong></td>
        </tr> 
        <tr>  
           <td align="center" width="100%" colspan="13" class="form_caption" ><strong style="font-size:14px"><?php echo $report_title; ?></strong></td>
        </tr>
        <tr>  
           <td align="center" width="100%" colspan="13" class="form_caption" ><strong style="font-size:14px"> <?php echo "From : ".change_date_format(str_replace("'","",$txt_date_from))." To : ".change_date_format(str_replace("'","",$txt_date_to))."" ;?></strong></td>
        </tr>  
    </table>
    <table width="1230px " border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all"> 
        <thead>
            <tr>
                <th width="35">SL</th>
                <th width="120">Body Part</th>
                <th width="120">Construction</th>
                <th width="150">Composition</th>
                <th width="80">Dia/Width </th>
                <th width="80">GSM</th>
                <th width="80">Issue Qty.</th>
                <th width="80">Yarn Lot</th>
                <th width="80">Yrn. Count</th>
                <th width="100">Order No</th>
                <th width="120">Buyer</th>
                <th width="70">Job No</th>
                <th>Batch No</th>
            </tr>
        </thead>
    </table>
    <div style="width:1250px; max-height:300px; overflow-y:scroll" id="scroll_body" > 
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1230px" class="rpt_table" id="tbl_issue_status" >
	<?php
	$composition_arr=array();
 	$sql_deter="select a.id, a.construction, b.copmposition_id from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')];
			}
		}
	}	
	//var_dump($composition_arr);
	if ($cbo_buyer==0) $buyer_id=""; else $buyer_id=" and a.buyer_id=$cbo_buyer";
	if ($job_no_id=="") $buyer_job_no=""; else $buyer_job_no=" and a.order_id in ( $job_no_id )";
	if ($batch_no_id=="") $batch_no=""; else $batch_no=" and a.batch_no in ( $batch_no_id )";
	if($db_type==0)
		{
		if( $date_from==0 && $date_to==0 ) $issue_date=""; else $issue_date= " and a.issue_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
		}
  if($db_type==2)
		{
		if( $date_from==0 && $date_to==0 ) $issue_date=""; else $issue_date= " and a.issue_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
		}
	$determ_arr = return_library_array( "select mst_id, copmposition_id from lib_yarn_count_determina_dtls",'mst_id','copmposition_id');
	$body_part_arr = return_library_array( "select id, body_part_id from wo_pre_cost_fabric_cost_dtls",'id','body_part_id');
	
	$job_array=array();
	$job_sql=sql_select("select b.id, b.po_number, a.job_no_prefix_num  from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0");
	foreach( $job_sql as $job_row )
	{
		$job_array[$job_row[csf('id')]]['job']=$job_row[csf('job_no_prefix_num')];
		$job_array[$job_row[csf('id')]]['po']=$job_row[csf('po_number')];
	}
if($db_type==0)
	{	
    $dtls_sql="SELECT a.id, a.company_id, a.buyer_id, a.booking_no, a.batch_no, a.order_id, a.issue_basis, 
     b.prod_id, b.issue_qnty, b.yarn_lot  as yarn_lot , b.yarn_count,
     d.gsm, d.dia_width, d.detarmination_id
     from  inv_issue_master a, inv_grey_fabric_issue_dtls b, product_details_master d
     where a.id=b.mst_id and b.prod_id=d.id and a.entry_form=16 and a.item_category=13 and a.company_id=$cbo_company and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $buyer_id $buyer_job_no $batch_no $issue_date order by a.id ";
	}
	
if($db_type==2)
	{	
    $dtls_sql="SELECT a.id, a.company_id, a.buyer_id, a.booking_no, a.batch_no, a.order_id, a.issue_basis, 
     b.prod_id, b.issue_qnty, CAST(b.yarn_lot as varchar2(500)) as yarn_lot , b.yarn_count,
     d.gsm, d.dia_width, d.detarmination_id
     from  inv_issue_master a, inv_grey_fabric_issue_dtls b, product_details_master d
     where a.id=b.mst_id and b.prod_id=d.id and a.entry_form=16 and a.item_category=13 and a.company_id=$cbo_company and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $buyer_id $buyer_job_no $batch_no $issue_date order by a.id ";
	}
	
	//echo $dtls_sql;
	$dtls_sql_result=sql_select($dtls_sql);
	$i=1;
	foreach ( $dtls_sql_result as $row )
	{
		if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
		$detarm_id=$row[csf("detarmination_id")];
		?>
		<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
             <td width="35" ><p><?php echo $i; ?></p></td>
             <td width="120" ><p><?php echo $body_part[$body_part_arr[$detarm_id]]; ?></p></td>
             <td width="120" ><p><?php echo $constArr[$row[csf("detarmination_id")]]; ?></p></td>
             <td width="150" ><p><?php echo $composition[$determ_arr[$detarm_id]]; ?></p></td>
             <td width="80" ><p><?php echo $row[csf("dia_width")]; ?></p></td>
             <td width="80" ><p><?php echo $row[csf("gsm")]; ?></p></td>
             <td width="80" align="right" ><?php echo number_format($row[csf("issue_qnty")],2); ?></td>
             <td width="80" ><p><?php echo $row[csf("yarn_lot")]; ?></p></td>
             <td width="80" ><p><?php echo $countArr[$row[csf("yarn_count")]]; ?></p></td>
             <td width="100" ><p><?php echo $job_array[$row[csf('order_id')]]['po']; ?></p></td>
             <td width="120" ><p><?php echo $buyer_arr[$row[csf("buyer_id")]]; ?></p></td>
             <td width="70" align="center" ><p><?php echo $job_array[$row[csf('order_id')]]['job']; ?></p></td>
             <td width="" ><p><?php echo $batch_arr[$row[csf("batch_no")]]; ?></p></td>
         </tr>
 		<?php
		$i++;
		$tot_qnty+=$row[csf("issue_qnty")];
	}
	?>
    </table>
    <table width="1230px " border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all"> 
        <thead>
            <th width="35">&nbsp;</th>
            <th width="120">&nbsp;</th>
            <th width="120">&nbsp;</th>
            <th width="150">&nbsp;</th>
            <th width="80">&nbsp;</th>
            <th width="80">&nbsp;</th>
            <th width="80" id="tot_qnty"><?php echo number_format($tot_qnty,2); ?></th>
            <th width="80">&nbsp;</th>
            <th width="80">&nbsp;</th>
            <th width="100">&nbsp;</th>
            <th width="120">&nbsp;</th>
            <th width="70">&nbsp;</th>
            <th>&nbsp;</th>
        </thead>
    </table>
    </div>
    </div>
    <?php
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	//echo "$total_data####$filename";
	exit();      
	
}
?>