<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will create planning Info Entry
					
Functionality	:	
				

JS Functions	:

Created by		:	Fuad Shahriar 
Creation date 	: 	27-07-2013
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

session_start(); 
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Planning Info Entry", "../", 1, 1,'','','');

?>	
<script>

	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
	var permission='<?php echo $permission; ?>';


	function show_details()
	{
		if(form_validation('cbo_company_name','Company')==false)
		{
			return;
		}
		
		var data="action=booking_item_details"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*hide_job_id*cbo_planning_status*txt_order_no*hide_order_id',"../");
		
		freeze_window(5);
		http.open("POST","requires/planning_info_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fn_show_details_reponse;
		//show_list_view(cbo_company_id, 'booking_item_details', 'list_container_fabric_desc', 'requires/planning_info_entry_controller', '');
	}

	function fn_show_details_reponse()
	{
		if(http.readyState == 4) 
		{
			var response=trim(http.responseText);
			$('#list_container_fabric_desc').html(response);
			show_msg('18');
			release_freezing();
		}
	}
		
	function openmypage_style()
	{
		if(form_validation('cbo_company_name','Company Name')==false)
		{
			return;
		}
		
		var companyID = $("#cbo_company_name").val();
		var page_link='requires/planning_info_entry_controller.php?action=style_ref_search_popup&companyID='+companyID;
		var title='Style Ref. Search';
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=630px,height=370px,center=1,resize=1,scrolling=0','');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var style_ref=this.contentDoc.getElementById("hide_style_ref").value;
			var job_id=this.contentDoc.getElementById("hide_job_id").value;
			
			$('#txt_style_ref').val(style_ref);
			$('#hide_job_id').val(job_id);	 
		}
	}
	
	function openmypage_order()
	{
		if(form_validation('cbo_company_name','Company Name')==false)
		{
			return;
		}
		
		var companyID = $("#cbo_company_name").val();
		var page_link='requires/planning_info_entry_controller.php?action=order_no_search_popup&companyID='+companyID;
		var title='Order No Search';
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=790px,height=390px,center=1,resize=1,scrolling=0','');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var order_no=this.contentDoc.getElementById("hide_order_no").value;
			var order_id=this.contentDoc.getElementById("hide_order_id").value;
			
			$('#txt_order_no').val(order_no);
			$('#hide_order_id').val(order_id);	 
		}
	}
	
	function openmypage_prog()
	{
		var tot_row=$('#tbl_list_search tbody tr').length;
		var data=''; var i=0; var selected_row=0; var currentRowColor=''; var booking_no=''; var body_part_id=''; var fabric_typee=''; var buyer_id=''; var po_id=''; var dia='';
		var gsm=''; var desc=''; var start_date=''; var end_date=''; var booking_qnty=0; var desc_of_yarn=''; var plan_id=''; var determination_id=''; var pre_cost_id=''; var color_type_id='';
		
		var companyID=$('#company_id').val();
		
		for(var j=1; j<=tot_row; j++)
		{
			currentRowColor=document.getElementById('tr_' + j ).style.backgroundColor;
			check=$('#check_'+j).val();
			if(check==1 && currentRowColor=='yellow')
			{
				i++;
				selected_row++;
				
				if(data=='')
				{
					data=$('#booking_no_'+j).text()+"**"
					+$('#start_date_'+j).text()+"**"
					+$('#end_date_'+j).text()+"**"
					+$('#po_id_'+j).text()+"**"
					+$('#buyer_id_'+j).val()+"**"
					+$('#body_part_id_'+j).val()+"**"
					+$('#fabric_typee_'+j).val()+"**"
					+$('#pre_cost_id_'+j).val()+"**"
					+$('#desc_'+j).text()+"**"
					+$('#gsm_weight_'+j).text()+"**"
					+$('#dia_width_'+j).text()+"**"
					+$('#determination_id_'+j).val()+"**"
					+$('#booking_qnty_'+j).text()+"**"
					+$('#color_type_id_'+j).val()+"**"
					+$('#updateId_'+j).val();
				}
				else
				{
					data+="_"+$('#booking_no_'+j).text()+"**"
					+$('#start_date_'+j).text()+"**"
					+$('#end_date_'+j).text()+"**"
					+$('#po_id_'+j).text()+"**"
					+$('#buyer_id_'+j).val()+"**"
					+$('#body_part_id_'+j).val()+"**"
					+$('#fabric_typee_'+j).val()+"**"
					+$('#pre_cost_id_'+j).val()+"**"
					+$('#desc_'+j).text()+"**"
					+$('#gsm_weight_'+j).text()+"**"
					+$('#dia_width_'+j).text()+"**"
					+$('#determination_id_'+j).val()+"**"
					+$('#booking_qnty_'+j).text()+"**"
					+$('#color_type_id_'+j).val()+"**"
					+$('#updateId_'+j).val();
				}
				
				booking_no=$('#booking_no_'+j).text();
				gsm=$('#gsm_weight_'+j).text();
				dia=$('#dia_width_'+j).text();
				desc=$('#desc_'+j).text();
				start_date=$('#start_date_'+j).text();
				end_date=$('#end_date_'+j).text();
				desc_of_yarn=$('#yarn_desc_'+j).text();
				
				if(plan_id=='')	plan_id=$('#plan_id_'+j).text();
				
				if(po_id=='') po_id=$('#po_id_'+j).text(); else po_id+=","+$('#po_id_'+j).text();
				if(pre_cost_id=='') pre_cost_id=$('#pre_cost_id_'+j).val(); else pre_cost_id+=","+$('#pre_cost_id_'+j).val();
				
				determination_id=$('#determination_id_'+j).val();
				body_part_id=$('#body_part_id_'+j).val();
				color_type_id=$('#color_type_id_'+j).val();
				fabric_typee=$('#fabric_typee_'+j).val();
				buyer_id=$('#buyer_id_'+j).val();
				
				booking_qnty=booking_qnty*1+$('#booking_qnty_'+j).text()*1;
			}
		}
		
		if(selected_row<1)
		{
			alert("Please Select At Least One Item");
			return;
		}
		//alert(booking_qnty);
		var page_link='requires/planning_info_entry_controller.php?action=prog_qnty_popup&gsm='+gsm+'&dia='+dia+'&desc='+desc+'&start_date='+start_date+'&end_date='+end_date+'&booking_qnty='+booking_qnty+'&companyID='+companyID+'&data="'+data+'"&desc_of_yarn='+desc_of_yarn+'&plan_id='+plan_id+'&determination_id='+determination_id+'&booking_no='+booking_no+'&body_part_id='+body_part_id+'&fabric_type='+fabric_typee+'&buyer_id='+buyer_id+'&po_id='+po_id+'&pre_cost_id='+pre_cost_id+'&color_type_id='+color_type_id;
		var title='Program Qnty Info';
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=930px,height=430px,center=1,resize=1,scrolling=0','');
		/*emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var program_qnty=this.contentDoc.getElementById("txt_program_qnty").value;
			
		}*/
	}
	
	function selected_row(rowNo)
	{
		var color=document.getElementById('tr_' + rowNo ).style.backgroundColor;
		var bookingNo=$('#booking_no_'+rowNo).text();
		var determinationId=$('#determination_id_'+rowNo).val();
		var widthDiaType=$('#fabric_typee_'+rowNo).val();
		var gsm=$('#gsm_weight_'+rowNo).text();
		var fabricDia=$('#dia_width_'+rowNo).text();
		var plan_id=$('#plan_id_'+rowNo).text();
		var color_type_id=$('#color_type_id_'+rowNo).val();
		
		var stripe_or_not='';
		
		if(color_type_id==2 || color_type_id==3 || color_type_id==4)
		{
			stripe_or_not=1;//1 means stripe yes
		}
		else
		{
			stripe_or_not=0;//0 means stripe no
		}
		
		var currentRowColor=''; var check='';
		if(color!='yellow')
		{
			var tot_row=$('#tbl_list_search tbody tr').length;
			for(var i=1; i<=tot_row; i++)
			{ 
				if(i!=rowNo)
				{
					check=$('#check_'+i).val();
					currentRowColor=document.getElementById('tr_' + i ).style.backgroundColor;
					if(check==1 && currentRowColor=='yellow')
					{
						var bookingNoCur=$('#booking_no_'+i).text();
						var determinationIdCur=$('#determination_id_'+i).val();
						var widthDiaTypeCur=$('#fabric_typee_'+i).val();
						var gsmCur=$('#gsm_weight_'+i).text();
						var fabricDiaCur=$('#dia_width_'+i).text();
						var plan_idCur=$('#plan_id_'+i).text();
						var color_type_idCur=$('#color_type_id_'+i).val();
						
						var stripe_or_notCur='';
						if(color_type_idCur==2 || color_type_idCur==3 || color_type_idCur==4)
						{
							stripe_or_notCur=1;//1 means stripe yes
						}
						else
						{
							stripe_or_notCur=0;//0 means stripe no
						}
						
						if(plan_id=="" || plan_idCur=="")
						{
							if(!(bookingNo==bookingNoCur && determinationId==determinationIdCur && widthDiaType==widthDiaTypeCur && gsm==gsmCur && fabricDia==fabricDiaCur && stripe_or_not==stripe_or_notCur))
							{
								alert("Please Select Same Description and Same Plan ID");
								return;
							}
						}
						else
						{
							if(!(plan_id==plan_idCur && bookingNo==bookingNoCur && determinationId==determinationIdCur && widthDiaType==widthDiaTypeCur && gsm==gsmCur && fabricDia==fabricDiaCur))
							{
								alert("Please Select Same Description and Same Plan ID");
								return;
							}
						}
					}
				}
			}

			$('#tr_' + rowNo).css('background-color','yellow');
		}
		else
		{
			var reqsn_found_or_not=$('#reqsn_found_or_not_'+rowNo).val();
			if(reqsn_found_or_not==0)
			{
				$('#tr_' + rowNo).css('background-color','#FFFFCC');
			}
			else
			{
				alert("Requisition Found Against This Planning. So Change Not Allowed");
				return;
			}
		}
	}
	
	function generate_worder_report(type,booking_no,company_id,order_id,fabric_nature,fabric_source,job_no,approved)
	{
		var data="action=show_fabric_booking_report"+
					'&txt_booking_no='+"'"+booking_no+"'"+
					'&cbo_company_name='+"'"+company_id+"'"+
					'&txt_order_no_id='+"'"+order_id+"'"+
					'&cbo_fabric_natu='+"'"+fabric_nature+"'"+
					'&cbo_fabric_source='+"'"+fabric_source+"'"+
					'&id_approved_id='+"'"+approved+"'"+
					'&txt_job_no='+"'"+job_no+"'";
		
		if(type==1)	
		{			
			http.open("POST","../order/woven_order/requires/short_fabric_booking_controller.php",true);
		}
		else if(type==2)
		{
			http.open("POST","../order/woven_order/requires/fabric_booking_controller.php",true);
		}
		else
		{
			http.open("POST","../order/woven_order/requires/sample_booking_controller.php",true);
		}
		
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = generate_fabric_report_reponse;
	}
	
	function generate_fabric_report_reponse()
	{
		if(http.readyState == 4) 
		{
			var w = window.open("Surprise", "#");
			var d = w.document.open();
			d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title></head><body>'+http.responseText+'</body</html>');
			d.close();
		}
	}
	
</script>
</head>

<body onLoad="set_hotkey();">
	<div style="width:100%;" align="center">
	<?php echo load_freeze_divs ("../",$permission); ?>
		 <form name="palnningEntry_1" id="palnningEntry_1"> 
         <h3 style="width:900px;margin-top:10px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu(this.id,'content_search_panel','')">-Search Panel</h3> 
         <div id="content_search_panel">      
             <fieldset style="width:900px;">
                 <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
                        <thead>
                            <th class="must_entry_caption">Company Name</th>
                            <th>Buyer Name</th>
                            <th>Style Ref.</th>
                            <th>Order No</th>
                            <th>Planning Status</th>
                            <th><input type="reset" name="res" id="res" value="Reset" onClick="reset_form('palnningEntry_1','list_container_fabric_desc','','','')" class="formbutton" style="width:100px" /></th>
                        </thead>
                        <tbody>
                            <tr class="general">
                                <td> 
                                    <?php
                                        echo create_drop_down( "cbo_company_name", 160, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/planning_info_entry_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                                    ?>
                                </td>
                                <td id="buyer_td">
                                    <?php 
                                        echo create_drop_down( "cbo_buyer_name", 150, $blank_array,"", 1, "-- All Buyer --", $selected, "",0,"" );
                                    ?>
                                </td>
                                <td>
                                    <input type="text" name="txt_style_ref" id="txt_style_ref" class="text_boxes" style="width:140px" placeholder="Browse" onDblClick="openmypage_style();" readonly>
                                    <input type="hidden" name="hide_job_id" id="hide_job_id" readonly>
                                </td>
                                <td>
                                    <input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:140px" placeholder="Browse Or Write" onDblClick="openmypage_order();" onChange="$('#hide_order_id').val('');" autocomplete="off">
                                    <input type="hidden" name="hide_order_id" id="hide_order_id" readonly>
                                </td>
                                <td> 
                                    <?php
                                        echo create_drop_down( "cbo_planning_status", 120, $planning_status,"", 0, "", $selected,"","", "1,2" );
                                        
                                    ?>
                                </td>
                                <td><input type="button" value="Show" name="show" id="show" class="formbutton" style="width:100px" onClick="show_details()"/></td>                	
                            </tr>
                        </tbody>
                    </table>
                </fieldset>
            </div>
            <div style="width:100%;margin-top:10px;">
                <input type="button" value="Click For Program" name="generate" id="generate" class="formbutton" style="width:150px" onClick="openmypage_prog()"/>
            </div>
		</form>
	</div>
    <div id="list_container_fabric_desc" style="margin-left:10px;margin-top:10px"></div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>