-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_marketing_team`
--

DROP TABLE IF EXISTS `lib_marketing_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_marketing_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(50) DEFAULT NULL,
  `team_leader_name` varchar(50) DEFAULT NULL,
  `team_leader_desig` varchar(50) DEFAULT NULL,
  `team_leader_email` varchar(50) NOT NULL,
  `lib_mkt_team_member_info_id` int(11) NOT NULL,
  `total_member` int(11) NOT NULL DEFAULT '1',
  `capacity_smv` int(11) NOT NULL DEFAULT '0',
  `capacity_basic` double NOT NULL DEFAULT '0',
  `team_contact_no` varchar(64) NOT NULL,
  `inserted_by` int(11) NOT NULL COMMENT 'employee code will be add here',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'employee code will be add here',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `team_name` (`team_name`),
  KEY `team_leader_name` (`team_leader_name`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_marketing_team`
--

LOCK TABLES `lib_marketing_team` WRITE;
/*!40000 ALTER TABLE `lib_marketing_team` DISABLE KEYS */;
INSERT INTO `lib_marketing_team` VALUES (1,'Md.Zaheed','Md.Zaheed','GM','',1,11,0,0,'',2,'2015-01-04 00:21:25',2,'2015-11-12 09:37:16',1,0),(2,'Md.abu naser sobuz','Md.abu naser sobuz','AGM','',13,1,0,0,'',2,'2015-11-12 09:33:16',2,'2015-11-12 09:36:54',3,0);
/*!40000 ALTER TABLE `lib_marketing_team` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
