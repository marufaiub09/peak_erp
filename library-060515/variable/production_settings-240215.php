<?
/*-------------------------------------------- Comments

Purpose			: 	This form will create Order Tracking Variable Settings
					Select company and select Variable List that onchange will change content
					
Functionality	:	Must fill Company, Variable List

JS Functions	:

Created by		:	Bilas 
Creation date 	: 	12-10-2012
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Tracking Variable Settings", "../../", 1, 1,$unicode,'','');

$smv_source_arr=array(1=>"From Order Entry",2=>"From Pre-Costing",3=>"From GSD Entry");

?>
 

<script language="javascript">

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
var permission='<? echo $permission; ?>';

function fnc_production_variable_settings(operation){
	
	if(operation==2)
	{
		show_msg('13');
		return;
	}
	//var update_id						= escape(document.getElementById('update_id').value);
	var cbo_company_name_production		= escape(document.getElementById('cbo_company_name_production').value);
	var cbo_variable_list_production	= escape(document.getElementById('cbo_variable_list_production').value);
	
	if ( form_validation('cbo_company_name_production*cbo_variable_list_production','Company Name*Variable List')==0 )
	{
		return;
	}
	
	if (cbo_variable_list_production==1)
	{
 				
			var cutting_update				= escape(document.getElementById('cutting_update').innerHTML);
			var printing_emb_production		= escape(document.getElementById('printing_emb_production').innerHTML);
			var sewing_production			= escape(document.getElementById('sewing_production').innerHTML);
			var iron_update					= escape(document.getElementById('iron_update').innerHTML);
			var finishing_update			= escape(document.getElementById('finishing_update').innerHTML);
			var production_entry			= escape(document.getElementById('production_entry').innerHTML);
			var cutting_delevary_input		= escape(document.getElementById('cutting_delevery_entry').innerHTML);
			var html_data ='&cutting_delevary_input_html='+cutting_delevary_input+'&cutting_update_html='+cutting_update+'&printing_emb_production_html='+printing_emb_production+'&sewing_production_html='+sewing_production+'&iron_update_html='+iron_update+'&finishing_update_html='+finishing_update+'&production_entry_html='+production_entry;
			nocache = Math.random();					
			
			eval(get_submitted_variables('cbo_company_name_production*cbo_variable_list_production*cbo_cutting_update*cbo_printing_emb_production*cbo_sewing_production*cbo_iron_update*cbo_finishing_update*cbo_ex_factory*cbo_production_entry*update_id'));
			var data="action=save_update_delete&operation="+operation+html_data+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_cutting_update_to_input*cbo_cutting_update*cbo_printing_emb_production*cbo_sewing_production*cbo_iron_update*cbo_finishing_update*cbo_ex_factory*cbo_production_entry*update_id',"../../");
			//alert(data)
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse;
  	
	}
	else if(cbo_variable_list_production==2)
	{
		
 		var data_part="";
		var ct=0;
		$("#tbl_slab tbody tr").each(function() {
            ct++;
          	data_part = data_part+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*update_id*txt_slab_rang_start'+ct+'*txt_slab_rang_end'+ct+'*txt_excess_percent'+ct,"../../");
   		});
  		var data="action=save_update_delete&operation="+operation+'&counter='+ct+data_part;
 
 		http.open("POST","requires/production_settings_controller.php", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_production_variable_settings_reponse;
	}
	else if(cbo_variable_list_production==3)
	{
			var fabric_roll_level_html	= escape(document.getElementById('fabric_roll_level').innerHTML);
 			html_data = '&fabric_roll_level_html='+fabric_roll_level_html;
			
			var data="action=save_update_delete&operation="+operation+html_data+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_fabric_roll_level*update_id',"../../");
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse;
		
	}
 	else if(cbo_variable_list_production==4)
	{
			var fabric_machine_level_html	= escape(document.getElementById('fabric_machine_level').innerHTML);
 			html_data = '&fabric_machine_level_html='+fabric_machine_level_html;
			
			var data="action=save_update_delete&operation="+operation+html_data+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_fabric_machine_level*update_id',"../../");
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse;
		
	}
	else if(cbo_variable_list_production==13)
	{
			var batch_maintained_html	= escape(document.getElementById('batch_maintained').innerHTML);
 			html_data = '&batch_maintained_html='+batch_maintained_html;
			
			var data="action=save_update_delete&operation="+operation+html_data+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_batch_maintained*update_id',"../../");
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse;
	}
	
	else if(cbo_variable_list_production==15)
	{
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_item_category_1*cbo_auto_update_1*update_id_1*cbo_item_category_2*cbo_auto_update_2*update_id_2',"../../");
			
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse_for_auto_update;
	}
	else if(cbo_variable_list_production==23)
	{
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_prod_resource*update_id',"../../");
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse_for_auto_update;
	}
	else if(cbo_variable_list_production==24)
	{
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_batch_no*update_id',"../../");
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse_for_auto_update;
	}
	else if(cbo_variable_list_production==25)
	{
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production*cbo_source*update_id',"../../");
			//freeze_window(operation);
			http.open("POST","requires/production_settings_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_variable_settings_reponse_for_auto_update;
	}
	else if(cbo_variable_list_production==26)
	{
		var row_num=$('#tbl_list_search tr').length-1;
		
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			data_all=data_all+get_submitted_data_string('shift_id'+i+'*update_id'+i+'*txt_prod_start_time'+i+'*txt_lunch_start_time'+i,"../../",i)	
		} //alert(data_all);return;
		
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name_production*cbo_variable_list_production',"../../",i)+data_all+'&total_row='+row_num;
		freeze_window(operation);
		http.open("POST","requires/production_settings_controller.php", true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_production_variable_settings_reponse_for_auto_update;
	}
}

function fnc_production_variable_settings_reponse()
{
	if(http.readyState == 4) 
	{
		var cbo_variable_list=$('#cbo_variable_list_production').val();
		//alert(http.responseText);
		var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		
		if(cbo_variable_list_production==26)
		{
			if(reponse[0]==0 || reponse[0]==1)
			{
				set_button_status(1, permission, 'fnc_production_variable_settings',1);
				show_list_view(cbo_variable_list_production+'_'+document.getElementById('cbo_company_name_production').value,'on_change_data','variable_settings_container','../variable/requires/production_settings_controller','');
			}
		}
		else
		{
			document.getElementById('update_id').value  = reponse[2];
			set_button_status(0, permission, 'fnc_production_variable_settings',1);
			reset_form('productionVariableSettings','variable_settings_container','');
		}
		release_freezing();
	}
}	

function fnc_production_variable_settings_reponse_for_auto_update()
{
	if(http.readyState == 4) 
	{
		
		var reponse=trim(http.responseText).split('**');
		
		show_msg(reponse[0]);
		if(reponse[0]==0 || reponse[0]==1)
		{
			set_button_status(1, permission, 'fnc_production_variable_settings',1);
			show_list_view(document.getElementById('cbo_variable_list_production').value+'_'+document.getElementById('cbo_company_name_production').value,'on_change_data','variable_settings_container','../variable/requires/production_settings_controller','');
		}
		
		release_freezing();
	}
}	



function next_number(i)
	{
		var j=i-1;
		if(i!=1)
		{
			var aa = document.getElementById('txt_slab_rang_start'+i).value;
			var bb = document.getElementById('txt_slab_rang_end'+j).value;
			var k=aa*1-bb*1;
			//alert(aa+'k='+k);
			if(k>1 || k<=0) 
			{
				document.getElementById('txt_slab_rang_start'+i).value="";
				alert('Please Enter Next Slab Range End Number');
			}
		}
			
	}
	
		
function add_variable_row( counter_id ) 
	{
		var rowCount = document.getElementById('tbl_slab').rows.length;
 		if(counter_id<=10)
		{
			if (counter_id!=rowCount)
			{
				return false;
			}			
			counter_id++;			
			$('#tbl_slab tbody').append(
				'<tr id="po_' + counter_id + '">'
					+ '<td><input	type="text"	name="txt_slab_rang_start' + counter_id + '" autocomplete="off" style="width:150px;" onchange="next_number('+counter_id+')" class="text_boxes_numeric" id="txt_slab_rang_start' + counter_id + '"		value="" 	/></td>'
					+ '<td><input	type="text"	name="txt_slab_rang_end' + counter_id + '" 	autocomplete="off" style="width:150px; " class="text_boxes_numeric" id="txt_slab_rang_end' + counter_id + '"		value="" 	/></td>'
					+ '<td><input	type="text"	name="txt_excess_percent' + counter_id + '"	autocomplete="off" style="width:150px " class="text_boxes_numeric" id="txt_excess_percent' + counter_id + '"		value=""	onfocus="add_variable_row( ' + counter_id + ' );" 	/></td>'
 				+ '</tr>'
			);
		}
	}
	
	function fnc_valid_time(val,field_id)
	{
		var colon_contains=val.contains(":");
		if(colon_contains==false)
		{
			if(val>23)
			{
				document.getElementById(field_id).value='23:';
			}
		}
		else
		{
			var data=val.split(":");
			var minutes=data[1];
			var str_length=minutes.length;
			
			if(str_length>2)
			{
				minutes= minutes.substr(0, 2);
				if(minutes*1>59)
				{
					minutes=59;
				}

				var valid_time=data[0]+":"+minutes;
				document.getElementById(field_id).value=valid_time;
			}
		}
	}
</script>
</head>

<body  onLoad="set_hotkey()">
	<div align="center" style="width:100%;">
         
		<? echo load_freeze_divs ("../../",$permission);  ?>
        
        <fieldset style="width:850px;">
            <legend>Production Variable Settings</legend>
            <form name="productionVariableSettings" id="productionVariableSettings" >
                    <table  width="850px" cellspacing="2" cellpadding="0" border="0">
                        <tr>
                            <td width="150" align="center" class="must_entry_caption">Company</td>
                            <td width="300">
                                <? 
                                    echo create_drop_down( "cbo_company_name_production", 250, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, '--- Select Company ---', 0, "" );
                                ?>
                            </td> 
                            <td width="150" align="center" class="must_entry_caption">Variable List</td>
                            <td width="300">
                                 <? 
                                    echo create_drop_down( "cbo_variable_list_production", 250, $production_module,'', '1', '---- Select ----', '',"show_list_view(this.value+'_'+document.getElementById('cbo_company_name_production').value,'on_change_data','variable_settings_container','../variable/requires/production_settings_controller','');",''); //data, action, div, path, extra_func 
                                ?>
                            </td>
                        </tr>
                    </table>
                 
                 <div style="width:895px; float:left; min-height:40px; margin:auto" align="center" id="variable_settings_container"></div>
                
            </form>
        </fieldset>
	</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
