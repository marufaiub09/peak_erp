<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
if($db_type==2 || $db_type==1 )
{
	$mrr_date_check="and to_char(insert_date,'YYYY')=".date('Y',time())."";
	$group_concat="wm_concat";
}
else if ($db_type==0)
{
	$mrr_date_check="and year(insert_date)=".date('Y',time())."";
	$group_concat="group_concat";
}

//--------------------------------------------------------------------------------------------
$trim_group_arr = return_library_array("select id, order_uom from lib_item_group","id","order_uom");

//load drop down supplier
if ($action=="load_drop_down_supplier")
{	 
	//echo create_drop_down( "cbo_supplier", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where FIND_IN_SET($data,a.tag_company) and a.id=b.supplier_id and b.party_type in (1,6,7,8,90) and a.status_active=1 and a.is_deleted=0 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", "", "" ); select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and  b.party_type=2 and a.status_active=1 and a.is_deleted=0  group by a.id,a.supplier_name order by a.supplier_name
	echo create_drop_down( "cbo_supplier", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b, lib_supplier_tag_company c where a.id=b.supplier_id and a.id=c.supplier_id and b.party_type in(1,5,6,7,8) and c.tag_company in($data) and a.status_active=1 and a.is_deleted=0 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	exit();
}

//load drop down store
if ($action=="load_drop_down_store")
{
	echo create_drop_down( "cbo_store_name", 170, "select a.id,a.store_name from lib_store_location a, lib_store_location_category b where a.id=b.store_location_id and b.category_type in (8,9,10,11,15,16,17,18,19,20,21,22) and a.status_active=1 and a.is_deleted=0 and a.company_id in($data) group by a.id ,a.store_name order by a.store_name","id,store_name", 1, "-- Select --", "", "","" );  	 
	exit();
}
 
 //load drop down item group
if ($action=="load_drop_down_itemgroup")
{	   
	echo create_drop_down( "cbo_item_group", 130, "select id,item_name from lib_item_group where item_category=$data and status_active=1 and is_deleted=0 order by item_name","id,item_name", 1, "-- Select --", 0, "load_drop_down( 'requires/general_item_receive_controller', this.value, 'load_drop_down_uom', 'uom_td' );","" );  	 
	exit();
}
 
  //load drop down uom
if ($action=="load_drop_down_uom")
{	
	if($data==0) $uom=0; else $uom=$trim_group_arr[$data];
	//echo $data;die;
	echo create_drop_down( "cbo_uom", 130, $unit_of_measurement, "", 1, "-- Select --", $uom , "", 1); 	 
	exit();
} 
 
// wo/pi popup here----------------------// 
if ($action=="wopi_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
<script>
	function js_set_value(str)
	{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
		$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
		parent.emailwindow.hide();
	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="150">Search By</th>
                <th width="150" align="center" id="search_by_th_up">Enter WO/PI/Req Number</th>
                <th width="200">Date Range</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            echo create_drop_down( "cbo_search_by", 170, $receive_basis_arr,"",1, "--Select--", $receive_basis,"",1 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_wopi_search_list_view', 'search_div', 'general_item_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            	</tr>
                <tr>                  
                    <td align="center" height="40" valign="middle" colspan="4">
                        <?php echo load_month_buttons(1);  ?>
                        <!-- Hidden field here-------->
                        <input type="hidden" id="hidden_tbl_id" value="" />
                        <input type="hidden" id="hidden_wopi_number" value="hidden_wopi_number" />
                        <!-- ---------END------------->
                    </td>
                </tr>    
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_wopi_search_list_view")
{
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for pi
		{
			$sql_cond .= " and a.pi_number LIKE '%$txt_search_common%'";	
			if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and a.pi_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			if(trim($company)!="") $sql_cond .= " and a.importer_id='$company'";
		}
		else if(trim($txt_search_by)==2) // for wo
		{
			$sql_cond .= " and wo_number LIKE '%$txt_search_common%'";				
 			if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and wo_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			if(trim($company)!="") $sql_cond .= " and company_name='$company'";
		}
		/*else if(trim($txt_search_by)==6) // for requisition
		{
			$sql_cond .= " and requ_no LIKE '%$txt_search_common%'";				
 			if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and requisition_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			if(trim($company)!="") $sql_cond .= " and company_id='$company'";
		}*/
		else if(trim($txt_search_by)==7) // for requisition
		{
			$sql_cond .= " and requ_no LIKE '%$txt_search_common%'";				
 			if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and requisition_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			if(trim($company)!="") $sql_cond .= " and company_id='$company'";
		}
 	} 
 	
	if($txt_search_by==1 ) //pi base
	{
		if($db_type==0)
		{
 		$sql = "select a.id as id,a.pi_number as wopi_number,b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source ,a.item_category_id as item_category
				from com_pi_master_details a left join com_btb_lc_master_details b on FIND_IN_SET(a.id,b.pi_id)
				where  
				a.item_category_id not in (1,2,3,5,6,7,12,13,14) and
				a.status_active=1 and a.is_deleted=0 and
				a.importer_id=$company 
				$sql_cond";//a.supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
		}
				
		if($db_type==1 || $db_type==2)
		{
			$sql = "select a.id as id,a.pi_number as wopi_number,b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source,a.item_category_id as item_category 
				from com_pi_master_details a left join com_btb_lc_pi c on a.id=c.pi_id left join com_btb_lc_master_details b on c.com_btb_lc_master_details_id=b.id
				where  
				a.item_category_id not in (1,2,3,5,6,7,12,13,14) and
				a.status_active=1 and a.is_deleted=0 and
				a.importer_id=$company 
				$sql_cond";
		}
				
	}
	else if($txt_search_by==2) // wo base
	{
 		$sql = "select id,wo_number as wopi_number,' ' as lc_number,wo_date as wopi_date,supplier_id as supplier_id,currency_id as currency_id,source as source,item_category
				from wo_non_order_info_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category not in (1,2,3,5,6,7,12,13,14) and pay_mode in (1,4) and 
				company_name=$company
				$sql_cond";//supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
	}
	else if($txt_search_by==7) // requisition base
	{
 		$sql = "select id, requ_no as wopi_number,' ' as lc_number,requisition_date as wopi_date,'' as supplier_id, cbo_currency as currency_id,source as source , item_category_id as item_category
				from inv_purchase_requisition_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category_id not in (1,2,3,5,6,7,12,13,14) and pay_mode=4 and 
				company_id=$company
				$sql_cond"; 
	}
	//echo $sql;
	$result = sql_select($sql);
 	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(3=>$supplier_arr,4=>$currency,5=>$source,6=>$item_category);
	echo  create_list_view("list_view", "WO/PI No, LC ,Date, Supplier, Currency, Source,Item Category","100,100,100,200,100,120,120","900","260",0, $sql , "js_set_value", "id,wopi_number", "", 1, "0,0,0,supplier_id,currency_id,source,item_category", $arr, "wopi_number,lc_number,wopi_date,supplier_id,currency_id,source,item_category", "",'','0,0,0,0,0,0,0') ;	
	exit();	
}

//after select wo/pi number get form data here---------------------------//
if($action=="populate_data_from_wopi_popup")
{
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	
	if($receive_basis==1 ) //PI
	{
		if($db_type==0)
		{
 		$sql = "select b.id as id, b.lc_number as lc_number,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source,'' as pay_mode 
				from com_pi_master_details a left join com_btb_lc_master_details b on FIND_IN_SET(a.id,b.pi_id)
				where  
				a.item_category_id not in (1,2,3,5,6,7,12,13,14) and
				a.status_active=1 and a.is_deleted=0 and
				a.id=$wo_pi_ID";
		}
		if($db_type==1 || $db_type==2)
		{
			$sql ="select b.id as id, b.lc_number as lc_number,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source,'' as pay_mode 
				from com_pi_master_details a left join com_btb_lc_pi c on a.id=c.pi_id left join com_btb_lc_master_details b on c.com_btb_lc_master_details_id=b.id
				where  
				a.item_category_id not in (1,2,3,5,6,7,12,13,14) and
				a.status_active=1 and a.is_deleted=0 and
				a.id=$wo_pi_ID";
		}
	}
	else if($receive_basis==2) //WO
	{
 		$sql = "select id,'' as lc_number,supplier_id as supplier_id,currency_id as currency_id,source as source,pay_mode  
				from wo_non_order_info_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category not in (1,2,3,5,6,7,12,13,14) and 
				id=$wo_pi_ID";
	}
	else if($receive_basis==7) //Requisition
	{
 		$sql = "select id, requ_no as wopi_number,' ' as lc_number,requisition_date as wopi_date,'' as supplier_id, cbo_currency as currency_id,source as source,pay_mode as pay_mode 
				from inv_purchase_requisition_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category_id not in (1,2,3,5,6,7,12,13,14) and pay_mode=4 and 
				id=$wo_pi_ID";
	}
	//echo $sql;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		if($row[csf("currency_id")]==1)
		{
			echo "$('#txt_exchange_rate').val(1);\n";
		}
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		echo "$('#cbo_pay_mode').val(".$row[csf("pay_mode")].");\n";
		echo "$('#txt_lc_no').val('".$row[csf("lc_number")]."');\n";
		if($row[csf("lc_number")]!="")
		{
			echo "$('#hidden_lc_id').val(".$row[csf("id")].");\n";
		}
	}
	exit();	
}

//right side product list create here--------------------//
if($action=="show_product_listview")
{ 
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	
	if($receive_basis==1) // pi basis
	{	
		$sql = "select a.importer_id as company_id, a.supplier_id,b.id,b.item_description as product_name_details, sum(b.quantity) as quantity  
		from com_pi_master_details a, com_pi_item_details b, product_details_master c
		where a.id=$wo_pi_ID and a.id=b.pi_id and b.item_prod_id=c.id 
		group by c.id,a.importer_id,a.supplier_id,b.id,b.item_description"; 
	}  
	else if($receive_basis==2) // wo basis
	{
		$sql = "select a.company_name as company_id,a.supplier_id,b.id, c.product_name_details, sum(b.supplier_order_quantity) as quantity
		from wo_non_order_info_mst a, wo_non_order_info_dtls b, product_details_master c
		where a.id=b.mst_id and a.id=$wo_pi_ID and a.pay_mode in (1,4) and b.item_id=c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by c.id,a.company_name,a.supplier_id, c.product_name_details,b.id";
	}
	else if($receive_basis==7) // requisition basis
	{
		$sql = "select a.company_id, '' as supplier_id, b.id, c.product_name_details, sum(b.quantity) as quantity
		from inv_purchase_requisition_mst a, inv_purchase_requisition_dtls b, product_details_master c 
		where a.id=b.mst_id and b.product_id=c.id and a.id=$wo_pi_ID and a.pay_mode=4 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 
		group by c.id, c.product_name_details,a.company_id, b.id";
	}	
	//echo $sql;
	$result = sql_select($sql); 
	$i=1; 
	?>
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" width="300" rules="all">
        	<thead><tr><th width="30">SL</th><th>Product Name</th></tr></thead>
            <tbody>
            	<?php foreach($result as $row)
				{  
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					$productName=$row[csf("product_name_details")];
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $receive_basis."**".$row[csf("id")]."**".$row[csf("company_id")]."**".$row[csf("supplier_id")]."**".$row[csf("quantity")];?>","wo_pi_product_form_input","requires/general_item_receive_controller")' style="cursor:pointer" >
                        <td><?php echo $i; ?></td>
                        <td><?php echo $productName; ?></td>
					</tr>
					<?php 
					$i++; 
				} 
				?>
            </tbody>
        </table>
     </fieldset>   
	<?php	 
	exit();
}

// get form data from product click in right side
if($action=="wo_pi_product_form_input")
{
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$company_id = $ex_data[2];
	$supplier_id = $ex_data[3];
	$wo_po_qnty = $ex_data[4];
	if($receive_basis==1) // pi basis
	{	
  		$sql = "select a.importer_id as company_id, a.supplier_id,c.id,c.item_category_id as item_category, c.item_group_id as item_group, c.item_description as item_description, b.uom as cons_uom, b.rate, sum(b.quantity) as quantity  
		from com_pi_master_details a, com_pi_item_details b, product_details_master c
		where b.id=$wo_pi_ID and a.id=b.pi_id and b.item_prod_id=c.id 
		group by c.id,c.item_category_id, c.item_group_id,c.item_description,a.importer_id, a.supplier_id,b.uom,b.rate";		
	}
	else if($receive_basis==2) // wo basis
	{
 		$sql = "select a.company_name as company_id,a.supplier_id,c.id,c.item_category_id as item_category, c.item_group_id as item_group, c.item_description as item_description, b.uom as cons_uom, b.rate, sum(b.supplier_order_quantity) as quantity
		from wo_non_order_info_mst a, wo_non_order_info_dtls b, product_details_master c
		where a.id=b.mst_id and b.id=$wo_pi_ID and a.pay_mode in (1,4) and b.item_id=c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 
		group by c.id,c.item_category_id, c.item_group_id,c.item_description,a.company_name,a.supplier_id,b.uom, b.rate";	
	}
	else if($receive_basis==7) // requisition basis
	{
		$sql = "select a.company_id, '' as supplier_id, c.id, a.item_category_id as item_category, c.item_group_id as item_group, c.item_description as item_description, b.cons_uom, b.rate, sum(b.quantity) as quantity
		from inv_purchase_requisition_mst a, inv_purchase_requisition_dtls b, product_details_master c 
		where a.id=b.mst_id and b.product_id=c.id and b.id=$wo_pi_ID and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 
		group by c.id,c.item_category_id, c.item_group_id,c.item_description,a.company_id,a.item_category_id,b.cons_uom, b.rate";
	}	
	//echo $sql; 
	$result = sql_select($sql); 
	foreach($result as $row)
	{ 
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";	 
	 	echo "$('#cbo_item_category').val(".$row[csf("item_category")].");\n";
		echo "load_drop_down( 'requires/general_item_receive_controller',".$row[csf("item_category")].", 'load_drop_down_itemgroup', 'item_group_td' );";		
		echo "$('#cbo_item_group').val(".$row[csf("item_group")].");\n";
		echo "$('#txt_item_desc').val('".$row[csf("item_description")]."');\n";
		echo "$('#current_prod_id').val('".$row[csf("id")]."');\n";
		//echo "$('#txt_serial_no').val(".$row[csf("")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("cons_uom")].");\n";
 		echo "$('#txt_rate').val('".$row[csf("rate")]."');\n";
 		$totalRcvQnty = return_field_value("current_stock","product_details_master","id=".$row[csf("id")]);
		$orderQnty = round($wo_po_qnty)-round($totalRcvQnty);
		echo "$('#txt_order_qty').val('".$orderQnty."');\n";
	} 
	exit();	 
}

// LC popup here----------------------// 
if ($action=="lc_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
<script>
function js_set_value(str)
{
	var splitData = str.split("_");		 
	$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
	$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
	parent.emailwindow.hide();
}
	
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th>
                    	<input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  />
                   		<!-- Hidden field here-------->
                        <input type="hidden" id="hidden_tbl_id" value="" />
                        <input type="hidden" id="hidden_wopi_number" value="hidden_wopi_number" />
                        <!-- ---------END------------->
                    </th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            $search_by_arr=array(0=>'LC Number',1=>'Supplier Name');
							$dd="change_search_event(this.value, '0*1', '0*select id, supplier_name from lib_supplier', '../../../') ";							
							echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+<?php echo $company; ?>, 'create_lc_search_list_view', 'search_div', 'general_item_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
           	 	</tr> 
            </tbody>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_lc_search_list_view")
{
	$ex_data = explode("_",$data);
	$cbo_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$company = $ex_data[2];
	
	if($cbo_search_by==1 && $txt_search_common!="") // lc number
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where lc_number LIKE '%$search_string%' and importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	} 
	else if($cbo_search_by==1 && $txt_search_common!="") //supplier
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where supplier_id='$search_string' and importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	} 
	else
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	}
	
	$company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$company_arr,2=>$supplier_arr,3=>$item_category);
	echo  create_list_view("list_view", "LC No,Importer,Supplier Name,Item Category,Value","120,150,150,120,120","750","260",0, $sql , "js_set_value", "id,lc_number", "", 1, "0,importer_id,supplier_id,item_category_id,0", $arr, "lc_number,importer_id,supplier_id,item_category_id,lc_value", "",'','0,0,0,0,0,1') ;	
	exit();
}

if($action=="show_ile")
{
	$ex_data = explode("**",$data);
	$company = $ex_data[0];
	$source = $ex_data[1];
	$rate = $ex_data[2];
	$category = $ex_data[3];
	$group = $ex_data[4];
	
	$sql="select standard from variable_inv_ile_standard where source='$source' and company_name='$company' and category=$category and item_group=$group and status_active=1 and is_deleted=0 order by id limit 1";
	
	//echo $sql;
	$result=sql_select($sql);
	foreach($result as $row)
	{
		// NOTE :- ILE=standard, ILE% = standard/100*rate
		$ile = $row[csf("standard")];
		$ile_percentage = ( $row[csf("standard")]/100 )*$rate;
		echo $ile."**".number_format($ile_percentage,$dec_place[3],".","");
		exit();
	}
	exit();
}

if($action=="serial_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
 	if(str_replace("'","",$serialString)!="")
	{
		$mainEx = explode("**",str_replace("'","",$serialString)); 
		$serialArr = explode(",",$mainEx[0]);
		$qntyArr = explode(",",$mainEx[1]);
	}
	
	?>
	<script>
	function add_break_down_tr(i) 
	 {
		var row_num=$('#tbl_serial tr').length-1;
		if (row_num!=i)
		{
			return false;
		}
		else
		{
			i++;			
			$("#tbl_serial tr:last").clone().find("input,select").each(function() {
				$(this).attr({
				  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
				  'name': function(_, name) { return name + i },
				  'value': function(_, value) { return value }              
				});  
			  }).end().appendTo("#tbl_serial"); 
			$('#txtSerialNo_'+i).val('');
			$('#txtQuantity_'+i).removeClass("class").addClass("class","text_boxes_numeric");
   			$('#btnIncrease_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
			$('#btnDecrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
			$('#txtSerialNo_'+i).removeAttr("onBlur").attr("onBlur","fn_check_serial("+i+")");	
 		}
	}
	
	function fn_deletebreak_down_tr(rowNo) 
	{   
		var numRow = $('table#tbl_serial tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_serial tbody tr:last').remove();
		}
 	}
	
	function fnClosed() 
	{   
		var numRow = $('table#tbl_serial tr').length;  
		var serialS="";
		var qntyS="";
		for(var i=1;i<numRow;i++)
		{
 			if(i*1>1){ serialS+=","; qntyS+=","; }
			serialS+=$("#txtSerialNo_"+i).val();
			qntyS+=$("#txtQuantity_"+i).val();
			if( form_validation('txtSerialNo_'+i,'Serial')==false )
			{
				return;
			}
		}
		var txtString = serialS;//+"**"+qntyS;
		$("#txt_string").val(txtString);
		$("#txt_qty").val(qntyS);
		parent.emailwindow.hide();
 	}
	
	function fn_check_serial(rowNo) 
	{
		if(rowNo!=1)
		{
			var table_length = $('#tbl_serial tr').length;
			for(var i=1; i<=rowNo-1; i++)
			{
				if(($('#txtSerialNo_'+i).val()*1)==($('#txtSerialNo_'+rowNo).val()*1))
				{
					$('#txtSerialNo_'+rowNo).val("");
				}
			}
		}
 	}
	</script>
	</head>
	<body>
	<div align="center" style="width:100%;" >
	<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
		<table width="450" cellspacing="0" cellpadding="0" border="0" class="rpt_table" id="tbl_serial" >
				<thead>
					<tr>                	 
						<th width="260" class="must_entry_caption">Serial No</th>
                        <th width="80">Quantity</th>
 						<th width="120">Action</th> 
					</tr>
				</thead>
				<tbody>
                	<?php
 						$chkNo = sizeof($serialArr);
						if(!empty($serialArr[0]))
						{ 
 							for($j=1;$j<=$chkNo;$j++)
							{
								?>
								<tr>
									<td>
										<input type="text" id="txtSerialNo_<?php echo $j;?>" name="txtSerialNo_<?php echo $j;?>" style="width:250px" class="text_boxes" value="<?php echo $serialArr[$j-1]; ?>" onBlur="fn_check_serial(<?php echo $j;?>)" />
									</td>
									<td>
										<input type="text" id="txtQuantity_<?php echo $j;?>" name="txtQuantity_<?php echo $j;?>" style="width:70px" class="text_boxes_numeric" value="<?php echo $qntyArr[$j-1]; ?>" disabled />
									</td>
									<td>				
										<input type="button" id="btnIncrease_<?php echo $j;?>" name="btnIncrease_<?php echo $j;?>" class="formbutton" style="width:40px" onClick="add_break_down_tr(<?php echo $j;?>)" value="+" />
										<input type="button" id="btnDecrease_<?php echo $j;?>" name="btnDecrease_<?php echo $j;?>" class="formbutton" style="width:40px" onClick="fn_deletebreak_down_tr(<?php echo $j;?>)" value="-" />
									</td> 
								</tr> 
					<?php	
							}
 						}
						else
						{
					?>	
                            <tr>
                                <td>
                                    <input type="text" id="txtSerialNo_1" name="txtSerialNo_1" style="width:250px" class="text_boxes" value=""  onBlur="fn_check_serial(1)" />
                                </td>
                                <td>
                                    <input type="text" id="txtQuantity_1" name="txtQuantity_1" style="width:70px" class="text_boxes_numeric" value="1" disabled />
                                </td>
                                <td>				
                                    <input type="button" id="btnIncrease_1" name="btnIncrease_1" class="formbutton" style="width:40px" onClick="add_break_down_tr(1)" value="+" />
                                    <input type="button" id="btnDecrease_1" name="btnDecrease_1" class="formbutton" style="width:40px" onClick="fn_deletebreak_down_tr(1)" value="-" />
                                </td> 
                            </tr> 
                    <?php } ?>
				</tbody>         
			</table>  
            <div><input type="button" name="btn_close" class="formbutton" style="width:100px" value="Close" onClick="fnClosed()" /></div>  
            <!-- Hidden field here-------->
			<input type="hidden" id="txt_string" value="" />
            <input type="hidden" id="txt_qty" value="" />				 
			<!-- ---------END-------------> 
			</form>
	   </div>
	</body>           
	<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
}

if ($action=="item_description_popup")
{
	echo load_html_head_contents("Item popup", "../../../", 1, 1,'','1','');	
	extract($_REQUEST);
?>
<script> 
	function js_set_value(item_description)
	{
		var splitArr = item_description.split("_");
		$("#product_id_td").val(splitArr[0]);
		$("#item_description_td").val(splitArr[1]);
		parent.emailwindow.hide(); 
	} 
</script>
</head>	
<body>
	<div align="center" style="width:100%" >
	<form name="item_popup_1"  id="item_popup_1">
      <?php
 		  $sql="select id, item_description, item_size from product_details_master where status_active=1 and is_deleted=0 and company_id=$company_id and item_category_id=$item_category and item_group_id=$item_group";
		  
 		  echo  create_list_view ( "list_view","Item Account,Item Description,Item Size", "110,170","480","250",0, $sql, "js_set_value", "id,item_description", "", 1, "", $arr, "item_account,item_description,item_size", "", 'setFilterGrid("list_view",-1);','0,0','',0 ); 
    ?>
     <input type="hidden" id="item_description_td" />
     <input type="hidden" id="product_id_td" />
   </form>
   </div>  
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>                                  
<?php 		
}

//data save update delete here------------------------------//
if($action=="save_update_delete")
{	  
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		//table lock here  
 		
 		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_receive_master a, inv_transaction b","a.id=b.mst_id and a.recv_number=$txt_mrr_no and b.prod_id=$current_prod_id and b.transaction_type=1 and a.item_category=1"); 
		if($duplicate==1 && str_replace("'","",$txt_mrr_no)=="") 
		{
			echo "20**Duplicate Product is Not Allow in Same MRR Number.";
			die;
		}
		//------------------------------Check product END---------------------------------------//
				
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value,available_qnty from product_details_master where id=$current_prod_id");
		$presentStock=$presentStockValue=$presentAvgRate=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock		=$result[csf("current_stock")];
			$presentStockValue	=$result[csf("stock_value")];
			$presentAvgRate		=$result[csf("avg_rate_per_unit")];
			$product_name_details 	=$result[csf("product_name_details")];	
			$available_qnty			=$result[csf("available_qnty")];	
		}		 
		//----------------Check Product ID END---------------------//
		 
		if(str_replace("'","",$txt_mrr_no)!="")
		{
			$new_recv_number[0] = str_replace("'","",$txt_mrr_no);
			$id=return_field_value("id","inv_receive_master","recv_number=$txt_mrr_no");
			//master table UPDATE here START----------------------//		
			$field_array1="item_category*receive_basis*receive_date*booking_id*challan_no*store_id*exchange_rate*currency_id*supplier_id*lc_no*pay_mode*source*remarks*updated_by*update_date";
			$data_array1="".$cbo_item_category."*".$cbo_receive_basis."*".$txt_receive_date."*".$txt_wo_pi_req_id."*".$txt_challan_no."*".$cbo_store_name."*".$txt_exchange_rate."*".$cbo_currency."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_pay_mode."*".$cbo_source."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_update("inv_receive_master",$field_array,$data_array,"id",$id,1);	
			//master table UPDATE here END---------------------------------------// 
		}
		else  	
		{	
			//master table entry here START---------------------------------------//txt_remarks		
			$id=return_next_id("id", "inv_receive_master", 1);		
			$new_recv_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GIR', date("Y",time()), 5, "select recv_number_prefix,recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='20' $mrr_date_check order by id DESC ", "recv_number_prefix", "recv_number_prefix_num" )); 
			
 			$field_array1="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, company_id, receive_basis, receive_date, booking_id, challan_no, store_id, exchange_rate, currency_id, supplier_id, lc_no, pay_mode, source, remarks, inserted_by, insert_date";
			$data_array1="(".$id.",'".$new_recv_number[1]."','".$new_recv_number[2]."','".$new_recv_number[0]."',20,".$cbo_item_category.",".$cbo_company_name.",".$cbo_receive_basis.",".$txt_receive_date.",".$txt_wo_pi_req_id.",".$txt_challan_no.",".$cbo_store_name.",".$txt_exchange_rate.",".$cbo_currency.",".$cbo_supplier.",".$hidden_lc_id.",".$cbo_pay_mode.",".$cbo_source.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
			//echo "20**".$field_array."<br>".$data_array;die;
			//$rID=sql_insert("inv_receive_master",$field_array,$data_array,1); 		
			//master table entry here END---------------------------------------// 
		}
		
		
		//details table entry here START-----------------------------------//
		//echo $cbo_item_group;die;		
		$rate = str_replace("'","",$txt_rate);
		$txt_ile = str_replace("'","",$txt_ile);
		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$ile = ($txt_ile/$rate)*100; // ile cost to ile
		$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
		$exchange_rate = str_replace("'","",$txt_exchange_rate);
		if($db_type==0)
		{
		$concattS = explode(",",return_field_value(" concat(trim_uom,',',conversion_factor) as cons_uom","lib_item_group","id=$cbo_item_group","cons_uom")); 
		}
		if($db_type==2)
		{
		$concattS = explode(",",return_field_value("(trim_uom || ',' ||conversion_factor) as cons_uom","lib_item_group","id=$cbo_item_group","cons_uom")); 
		}
		$cons_uom = $concattS[0];
		$conversion_factor = $concattS[1];
		$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
 		$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
		$con_quantity = $conversion_factor*$txt_receive_qty;
		$con_amount = $cons_rate*$con_quantity;
		$con_ile = $ile/$conversion_factor;//($ile/$domestic_rate)*100;
		$con_ile_cost = ($con_ile/100)*$cons_rate;
		
		if($ile_cost=="") $ile_cost =0;
		if($cons_uom=="") $cons_uom =0;
		if($con_ile=="") $con_ile =0;
			
		$dtlsid = return_next_id("id", "inv_transaction", 1);		 
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$field_array_trams = "id,mst_id,receive_basis,pi_wo_batch_no,company_id,supplier_id,prod_id,item_category,transaction_type,transaction_date,store_id,order_uom,order_qnty,order_rate,order_ile,order_ile_cost,order_amount,cons_uom,cons_quantity,cons_rate,cons_ile,cons_ile_cost,cons_amount,balance_qnty,balance_amount,room,rack,self,bin_box,expire_date,inserted_by,insert_date";
 		$data_array_trans = "(".$dtlsid.",".$id.",".$cbo_receive_basis.",".$txt_wo_pi_req_id.",".$cbo_company_name.",".$cbo_supplier.",".$current_prod_id.",".$cbo_item_category.",1,".$txt_receive_date.",".$cbo_store_name.",".$cbo_uom.",".$txt_receive_qty.",".$txt_rate.",".$ile.",".$ile_cost.",".$txt_amount.",".$cons_uom.",".$con_quantity.",".$cons_rate.",".$con_ile.",".$con_ile_cost.",".$con_amount.",".$con_quantity.",".$con_amount.",".$txt_room.",".$txt_rack.",".$txt_self.",".$txt_binbox.",".$txt_warranty_date.",'".$user_id."','".$pc_date_time."')";
		//echo "INSERT INTO inv_transaction (".$field_array.") VALUES ".$data_array.""; die;
		//echo "**".$field_array."<br>".$data_array;die;
		//$dtlsrID = sql_insert("inv_transaction",$field_array_trams,$data_array_trans,1);
		//yarn details table entry here END-----------------------------------//
		
		//product master table data UPDATE START----------------------------------------------------------//	
		$stock_value 	= $domestic_rate*$con_quantity;
  		$currentStock   = $presentStock+$con_quantity;
		$available_qnty = $available_qnty+$con_quantity;
		$StockValue	 = $presentStockValue+$stock_value;
		$avgRate		= $StockValue/$currentStock;
 		$field_array3="avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*available_qnty*updated_by*update_date";
 		$data_array3="".number_format($avgRate,$dec_place[3],".","")."*".$con_quantity."*".$currentStock."*".number_format($StockValue,$dec_place[4],".","")."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'";
		//echo "**".$field_array."<br>".$data_array;die;
		//$prodUpdate = sql_update("product_details_master",$field_array,$data_array,"id",$current_prod_id,1);
		
				

		//------------------ product_details_master END---------------------------------------------------//
		
		//serial no save---------------
 		$serialID = return_next_id("id", "inv_serial_no_details", 1);		 
 		$serial_field_array = "id,recv_trans_id,prod_id,serial_no,is_issued,inserted_by,insert_date,serial_qty";
		$expSerial = explode(",",str_replace("'","",$txt_serial_no));
		$expSerialqty = explode(",",str_replace("'","",$txt_serial_qty));
		//print_r($current_prod_id);die;
		$serial_data_array=="";
		for($i=0;$i<count($expSerial);$i++)
		{
 			if($i>0){ $serial_data_array .=","; }
			$serial_data_array .= "(".$serialID.",".$dtlsid.",".$current_prod_id.",'".$expSerial[$i]."',0,'".$user_id."','".$pc_date_time."','".$expSerialqty[$i]."')";
			$serialID++;
		}
		
		//for test 
		if(str_replace("'","",$txt_mrr_no)!="")
		{
			$rID=sql_update("inv_receive_master",$field_array1,$data_array1,"id",$id,1);	
		}
		else  	
		{	
			$rID=sql_insert("inv_receive_master",$field_array1,$data_array1,1); 		
		}
		$dtlsrID = sql_insert("inv_transaction",$field_array_trams,$data_array_trans,1);
		$prodUpdate = sql_update("product_details_master",$field_array3,$data_array3,"id",$current_prod_id,1);
		$serial_dtlsrID=1;
		if(str_replace("'","",$txt_serial_no)!="")
		{
			$serial_dtlsrID = sql_insert("inv_serial_no_details",$serial_field_array,$serial_data_array,1);
		}
		if($db_type==0)
		{
			if( $rID && $dtlsrID && $prodUpdate && $serial_dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_recv_number[0]."**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_recv_number[0]."**".$id;
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if( $rID && $dtlsrID && $prodUpdate && $serial_dtlsrID)
			{
				oci_commit($con); 
				echo "0**".$new_recv_number[0]."**".$id;
			}
			else
			{
				oci_rollback($con); 
				echo "10**".$new_recv_number[0]."**".$id;
			}
		}
		disconnect($con);
		die;
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		if( str_replace("'","",$update_id) == "" )
		{
			echo "15";exit(); 
		}
		
		//check update id
		
				
		//previous product stock adjust here--------------------------//
		//product master table UPDATE here START ---------------------//
		$sql = sql_select("select a.prod_id,a.cons_quantity,a.cons_rate,a.cons_amount,b.avg_rate_per_unit,b.current_stock,b.stock_value from inv_transaction a, product_details_master b where a.id=$update_id and a.prod_id=b.id");
		$before_prod_id=$before_receive_qnty=$before_rate=$beforeAmount=$before_brand="";
		$beforeStock=$beforeStockValue=$beforeAvgRate=0;
		foreach( $sql as $row)
		{
			$before_prod_id 		= $row[csf("prod_id")]; 
			$before_receive_qnty 	= $row[csf("cons_quantity")]; //stock qnty
			$before_rate 			= $row[csf("cons_rate")]; 
			$beforeAmount			= $row[csf("cons_amount")]; //stock value
 			$beforeStock			=$row[csf("current_stock")];
			$beforeStockValue		=$row[csf("stock_value")];
			$beforeAvgRate			=$row[csf("avg_rate_per_unit")];	
		}
		
		
		//stock value minus here---------------------------//
		$adj_beforeStock			=$beforeStock-$before_receive_qnty;
		$adj_beforeStockValue		=$beforeStockValue-$beforeAmount;
		$adj_beforeAvgRate			=number_format(($adj_beforeStockValue/$adj_beforeStock),$dec_place[3],'.','');	
  		 
 		//current product stock-------------------------//
		$current_prod_id=str_replace("'","",$current_prod_id);
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value,available_qnty from product_details_master where id=$current_prod_id");
		$presentStock=$presentStockValue=$presentAvgRate=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock		  =$result[csf("current_stock")];
			$presentStockValue	 =$result[csf("stock_value")];
			$presentAvgRate		=$result[csf("avg_rate_per_unit")];
			$product_name_details  =$result[csf("product_name_details")];
			$available_qnty		=$result[csf("available_qnty")];
		}	 
		//----------------Check Product ID END---------------------//
		
		//yarn master table UPDATE here START----------------------//booking_id$txt_wo_pi_req_id
		if($update_id!="")
		{		
			$field_array_receive="item_category*receive_basis*receive_date*booking_id*challan_no*store_id*exchange_rate*currency_id*supplier_id*lc_no*pay_mode*source*remarks*updated_by*update_date";
			$data_array_receive="".$cbo_item_category."*".$cbo_receive_basis."*".$txt_receive_date."*".$txt_wo_pi_req_id."*".$txt_challan_no."*".$cbo_store_name."*".$txt_exchange_rate."*".$cbo_currency."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_pay_mode."*".$cbo_source."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array."==".$hidden_mrr_id."--";die;
			//$rID=sql_update("inv_receive_master",$field_array_receive,$data_array_receive,"id",$hidden_mrr_id,1);
			
			//yarn master table UPDATE here END---------------------------------------// 
			
			// yarn details table UPDATE here START-----------------------------------//		
			$rate = str_replace("'","",$txt_rate);
			$txt_ile = str_replace("'","",$txt_ile);
			$txt_receive_qty = str_replace("'","",$txt_receive_qty);
			$ile = ($txt_ile/$rate)*100; // ile cost to ile
	
			$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
			$exchange_rate = str_replace("'","",$txt_exchange_rate);
			
			if($db_type==0)
			{
				$concattS = explode(",",return_field_value("concat(trim_uom,',',conversion_factor) as concat_val","lib_item_group","id=$cbo_item_group","concat_val"));
			}
			else
			{
				$concattS = explode(",",return_field_value("(trim_uom || ',' || conversion_factor) as concat_val","lib_item_group","id=$cbo_item_group","concat_val"));
			}
			$cons_uom = $concattS[0];
			$conversion_factor = $concattS[1];
			$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
			$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
			
			$con_quantity = $conversion_factor*$txt_receive_qty;
			$con_amount = $cons_rate*$con_quantity;
			$con_ile = $ile/$conversion_factor;  
			$con_ile_cost = ($con_ile/100)*($cons_rate);
			//echo "20**".$con_ile_cost; mysql_query("ROLLBACK"); die; 
			if($ile_cost=="") $ile_cost=0;
			if($con_ile=="") $con_ile=0;
			if($cons_uom=="") $cons_uom=0;
			 
			$field_array_trans = "receive_basis*pi_wo_batch_no*company_id*supplier_id*prod_id*item_category*transaction_type*transaction_date*store_id*order_uom*order_qnty*order_rate*order_ile*order_ile_cost*order_amount*cons_uom*cons_quantity*cons_rate*cons_ile*cons_ile_cost*cons_amount*balance_qnty*balance_amount*room*rack*self*bin_box*expire_date*updated_by*update_date";
			$data_array_trans = "".$cbo_receive_basis."*".$txt_wo_pi_req_id."*".$cbo_company_name."*".$cbo_supplier."*".$current_prod_id."*".$cbo_item_category."*1*".$txt_receive_date."*".$cbo_store_name."*".$cbo_uom."*".$txt_receive_qty."*".$txt_rate."*".$ile."*".$ile_cost."*".$txt_amount."*".$cons_uom."*".$con_quantity."*".$cons_rate."*".$con_ile."*".$con_ile_cost."*".$con_amount."*".$con_quantity."*".$con_amount."*".$txt_room."*".$txt_rack."*".$txt_self."*".$txt_binbox."*".$txt_warranty_date."*'".$user_id."'*'".$pc_date_time."'";
			//echo "**".$field_array."<br>".$data_array;die;
			//$dtlsrID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
		}
		 
		//yarn details table UPDATE here END-----------------------------------//
    		
		//product master table data UPDATE START----------------------------------------------------------// 
		$field_array_product="avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*available_qnty*updated_by*update_date";
		if($before_prod_id==$current_prod_id)
		{
			$currentStock	= $adj_beforeStock+$con_quantity;
			$available_qnty  = $available_qnty+$con_quantity;
			$StockValue	  = $adj_beforeStockValue+$con_amount;
			$avgRate		 = number_format($StockValue/$currentStock,$dec_place[3],'.','');
 			$data_array_product = "".$avgRate."*".$con_quantity."*".$currentStock."*".number_format($StockValue,$dec_place[4],'.','')."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'";
			//$prodUpdate = sql_update("product_details_master",$field_array_product,$data_array_product,"id",$current_prod_id,1);
		}
		else
		{ 
			//before
			$updateID_array=$update_data=array();
			$updateID_array[]=$before_prod_id;
			$update_data[$before_prod_id]=explode("*",("".$adj_beforeAvgRate."*0*".$adj_beforeStock."*".number_format($adj_beforeStockValue,$dec_place[4],'.','')."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'"));
			//current			 
 			$presentStock 		= $presentStock+$con_quantity;
 			$available_qnty  	= $available_qnty+$con_quantity;
			$presentStockValue	= $presentStockValue+$con_amount;
			$presentAvgRate		= number_format($presentStockValue/$presentStock,$dec_place[3],'.','');
			$updateID_array[]=$current_prod_id;
			$update_data[$current_prod_id]=explode("*",("".$presentAvgRate."*0*".$presentStock."*".number_format($presentStockValue,$dec_place[4],'.','')."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'"));
			//$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$field_array_product,$update_data,$updateID_array),1);
		}
		//------------------ product_details_master END---------------------------------------------------//
		
		//serial no save---------------
		$deleteSerial=execute_query("delete from inv_serial_no_details where recv_trans_id=".$update_id,0);
  		$serialID = return_next_id("id", "inv_serial_no_details", 1);		 
 		$serial_field_array = "id,recv_trans_id,prod_id,serial_no,inserted_by,insert_date,serial_qty";
		$expSerial = explode(",",str_replace("'","",$txt_serial_no));
		$expSerialqty = explode(",",str_replace("'","",$txt_serial_qty));
		$serial_data_array=="";
		for($i=0;$i<count($expSerial);$i++)
		{
 			if($i>0){ $serial_data_array .=","; }
			$serial_data_array .= "(".$serialID.",".$update_id.",".$current_prod_id.",'".$expSerial[$i]."','".$user_id."','".$pc_date_time."','".$expSerialqty[$i]."')";
			$serialID++;
		}
		
		//all query execute here
		
		if($update_id!="")
		{
			$rID=sql_update("inv_receive_master",$field_array_receive,$data_array_receive,"id",$hidden_mrr_id,1);
			$dtlsrID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
		}
		if($before_prod_id==$current_prod_id)
		{
			$prodUpdate = sql_update("product_details_master",$field_array_product,$data_array_product,"id",$current_prod_id,1);
		}
		else
		{
			$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$field_array_product,$update_data,$updateID_array),1);
		}
		$serial_dtlsrID=true;
		if(str_replace("'","",$txt_serial_no)!="")
		{
			$serial_dtlsrID = sql_insert("inv_serial_no_details",$serial_field_array,$serial_data_array,1);		
		}
		
		if($db_type==0)
		{
			if($rID && $dtlsrID && $prodUpdate && $deleteSerial && $serial_dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_mrr_no)."**".str_replace("'","",$hidden_mrr_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_mrr_no)."**".str_replace("'","",$hidden_mrr_id);
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID && $prodUpdate && $deleteSerial && $serial_dtlsrID)
			{
				oci_commit($con);
				echo "1**".str_replace("'","",$txt_mrr_no)."**".str_replace("'","",$hidden_mrr_id);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_mrr_no)."**".str_replace("'","",$hidden_mrr_id);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		// master table delete here---------------------------------------
		$mst_id = str_replace("'","",$hidden_mrr_id);
		//echo $mst_id ;die;
		if($mst_id=="" || $mst_id==0)
		{ 
			echo "15**0"; die;
		}
		else
		{
			$rID = sql_update("inv_receive_master",'status_active*is_deleted','0*1',"id*item_category","$mst_id*$cbo_item_category",0);
			$dtlsrID = sql_update("inv_transaction",'status_active*is_deleted','0*1',"mst_id*item_category","$mst_id*$cbo_item_category",0);
			$srID = sql_update("inv_serial_no_details",'status_active*is_deleted','0*1',"mst_id*entry_form","$mst_id*20",1);
		}
		if($db_type==0)
		{
			if($rID && $dtlsrID && $srID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_mrr_no)."**".str_replace("'","",$hidden_mrr_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_mrr_no)."**".str_replace("'","",$hidden_mrr_id);
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "2**".str_replace("'","",$txt_mrr_no)."**".str_replace("'","",$hidden_mrr_id);
		}
		disconnect($con);
		die;
	}		
}

if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter MRR Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
 							 echo create_drop_down( "cbo_supplier", 150, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and  b.party_type in(1,6,7,8) and a.status_active=1 and a.is_deleted=0  group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td>
                        <?php  
                            $search_by = array(1=>'MRR No',2=>'Challan No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 120, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'general_item_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="hidden_recv_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
	 
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for mrr
		{
			$sql_cond .= " and a.recv_number LIKE '%$txt_search_common%'";	
			
		}
		else if(trim($txt_search_by)==2) // for chllan no
		{
			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";				
 		}		 
 	} 
	
	if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	if(trim($supplier)!=0) $sql_cond .= " and a.supplier_id='$supplier'";
	
	$sql = "select a.recv_number,a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis,sum(b.cons_quantity) as receive_qnty from inv_transaction b, inv_receive_master a left join com_btb_lc_master_details c on a.lc_no=c.id where a.id=b.mst_id and a.entry_form=20 and a.status_active=1 and a.is_deleted=0 $sql_cond 
	group by 
			b.mst_id,a.recv_number,a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis order by b.mst_id";
	//echo $sql;
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$supplier_arr,5=>$receive_basis_arr);
	echo create_list_view("list_view", "MRR No, Supplier Name, Challan No, LC No, Receive Date, Receive Basis, Receive Qnty","120,120,120,120,120,100,100","900","260",0, $sql , "js_set_value", "recv_number", "", 1, "0,supplier_id,0,0,0,receive_basis,0", $arr, "recv_number,supplier_id,challan_no,lc_number,receive_date,receive_basis,receive_qnty", "",'','0,0,0,0,0,0,1') ;	
	exit();
}

if($action=="populate_data_from_data")
{
	$sql = "select id,recv_number,company_id,receive_basis,receive_purpose,receive_date,booking_id,challan_no,store_id,lc_no,supplier_id,exchange_rate,currency_id,lc_no,pay_mode,source,remarks 
			from inv_receive_master 
			where recv_number='$data' and entry_form=20";
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#hidden_mrr_id').val(".$row[csf("id")].");\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		echo"load_drop_down( 'requires/general_item_receive_controller', ".$row[csf("company_id")].", 'load_drop_down_supplier', 'supplier' );\n";
		echo "$('#cbo_receive_basis').val(".$row[csf("receive_basis")].");\n";
		echo "$('#cbo_receive_purpose').val(".$row[csf("receive_purpose")].");\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("receive_date")])."');\n";
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		if($row[csf("currency_id")]==1)
		{
			echo "$('#txt_exchange_rate').val(1);\n";
			echo "$('#txt_exchange_rate').attr('disabled',true);\n";
		}
		else
		{
			echo "$('#txt_exchange_rate').attr('disabled',false);\n";
		}
		echo "$('#txt_exchange_rate').val(".$row[csf("exchange_rate")].");\n";
		echo "$('#cbo_pay_mode').val(".$row[csf("pay_mode")].");\n";
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		
		if($row[csf("receive_basis")]==1)
			$wopireq=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("booking_id")]."");	
		else if($row[csf("receive_basis")]==2)
			$wopireq=return_field_value("wo_number","wo_non_order_info_mst","id=".$row[csf("booking_id")]."");
		else if($row[csf("receive_basis")]==7)
			$wopireq=return_field_value("requ_no","inv_purchase_requisition_mst","id=".$row[csf("booking_id")]."");	
		echo "$('#txt_wo_pi_req').val('".$wopireq."');\n";
		echo "$('#txt_wo_pi_req_id').val(".$row[csf("booking_id")].");\n";
		
		echo "$('#hidden_lc_id').val(".$row[csf("lc_no")].");\n";
		$lcNumber = return_field_value("lc_number","com_btb_lc_master_details","id=".$row[csf("lc_no")]."");
		echo "$('#txt_lc_no').val('".$lcNumber."');\n";
		
		//right side list view
		echo "show_list_view('".$row[csf("recv_number")]."**".$row[csf("id")]."','show_dtls_list_view','list_container','requires/general_item_receive_controller','');\n";
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_general_item_receive_entry',1);\n";
 	}
	exit();	
}

if($action=="show_dtls_list_view")
{
	$ex_data = explode("**",$data);
	$recv_number = $ex_data[0];
	$rcv_mst_id = $ex_data[1];
	
	$cond="";
	if($recv_number!="") $cond .= " and a.recv_number='$recv_number'";
	if($rcv_mst_id!="") $cond .= " and a.id='$rcv_mst_id'";
	
	$sql = "select a.recv_number, b.id, b.receive_basis,b.pi_wo_batch_no,c.product_name_details,c.lot,b.order_uom,b.order_qnty,b.order_rate,b.order_ile_cost,b.order_amount,b.cons_amount   
			from inv_receive_master a, inv_transaction b,  product_details_master c 
			where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category not in (1,2,3,5,6,7,12,13,14) and a.entry_form=20 $cond";
	//echo $sql;die;		
	$result = sql_select($sql);
	$i=1;
	$totalQnty=0;
	$totalAmount=0;
	$totalbookCurr=0;
	?>
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" width="950" rules="all">
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Item Details</th>
                    <th>UOM</th>
                    <th>Receive Qty</th>
                    <th>Rate</th>
                    <th>ILE Cost</th>
                    <th>Amount</th>
                    <th>Book Currency</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					
					$wopireq="";
					if($row[csf("receive_basis")]==1)
						$wopireq=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("pi_wo_batch_no")]."");	
					else if($row[csf("receive_basis")]==2)
						$wopireq=return_field_value("wo_number","wo_non_order_info_mst","id=".$row[csf("pi_wo_batch_no")]."");
					else if($row[csf("receive_basis")]==7)
						$wopireq=return_field_value("requ_no","inv_purchase_requisition_mst","id=".$row[csf("pi_wo_batch_no")]."");		
					
					$totalQnty +=$row[csf("order_qnty")];
					$totalAmount +=$row[csf("order_amount")];
					$totalbookCurr +=$row[csf("cons_amount")];	
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>","child_form_input_data","requires/general_item_receive_controller")' style="cursor:pointer" >
                        <td width="50"><?php echo $i; ?></td>
                        <td width="300"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td width="100"><p><?php echo $unit_of_measurement[$row[csf("order_uom")]]; ?></p></td>
                        <td width="100" align="right"><p><?php echo $row[csf("order_qnty")]; ?></p></td>
                        <td width="100" align="right"><p><?php echo $row[csf("order_rate")]; ?></p></td>
                        <td width="100" align="right"><p><?php echo $row[csf("order_ile_cost")]; ?></p></td>                       
                        <td width="100" align="right"><p><?php echo $row[csf("order_amount")]; ?></p></td>
                        <td width="" align="right"><p><?php echo $row[csf("cons_amount")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                        <th colspan="3">Total</th>                         
                        <th><?php echo $totalQnty; ?></th>
                        <th colspan="2"></th>
                        <th><?php echo $totalAmount; ?></th>
                        <th><?php echo $totalbookCurr; ?></th>
                        <th></th>
                  </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}

if($action=="child_form_input_data")
{
	$rcv_dtls_id = $data;	
 	
	$sql = "select a.currency_id, a.booking_id, a.exchange_rate, b.id, b.pi_wo_batch_no, b.prod_id, b.brand_id, c.lot, b.order_uom, b.order_qnty, b.order_rate, b.order_ile_cost, b.order_amount, b.cons_amount, b.expire_date, b.room,b.rack, b.self,b.bin_box,c.item_category_id,c.item_group_id,c.item_description
			from inv_receive_master a, inv_transaction b, product_details_master c  
			where a.id=b.mst_id and b.prod_id=c.id and b.id='$rcv_dtls_id'";
			//echo $sql;
	$result = sql_select($sql);
    
	foreach($result as $row)
	{
		echo "$('#cbo_item_category').val(".$row[csf("item_category_id")].");\n";
		echo "load_drop_down( 'requires/general_item_receive_controller', ".$row[csf("item_category_id")].", 'load_drop_down_itemgroup', 'item_group_td' );\n";
		echo "$('#cbo_item_group').val(".$row[csf("item_group_id")].");\n";
		echo "$('#txt_item_desc').val('".$row[csf("item_description")]."');\n";
		echo "$('#txt_warranty_date').val('".change_date_format($row[csf("expire_date")])."');\n";
		if($db_type==0)
		{
			$serialString = return_field_value("$group_concat(serial_no)","inv_serial_no_details","recv_trans_id=".$row[csf("id")]." group by recv_trans_id");
			$serialqty = return_field_value("$group_concat(serial_qty)","inv_serial_no_details","recv_trans_id=".$row[csf("id")]." group by recv_trans_id");
		}
		else
		{
			$serialString = return_field_value("LISTAGG(CAST(serial_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY serial_no) as serial_no","inv_serial_no_details","recv_trans_id=".$row[csf("id")]." group by recv_trans_id","serial_no");
			$serialqty = return_field_value("LISTAGG(CAST(serial_qty AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY serial_qty) as serial_qty","inv_serial_no_details","recv_trans_id=".$row[csf("id")]." group by recv_trans_id","serial_qty");

		}
		
		
		echo "$('#txt_serial_no').val('".$serialString."');\n";
		echo "$('#txt_serial_qty').val('".$serialqty."');\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
 		if($row[csf("receive_basis")]==1)
			$wopireq=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("booking_id")]."");	
		else if($row[csf("receive_basis")]==2)
			$wopireq=return_field_value("wo_number","wo_non_order_info_mst","id=".$row[csf("booking_id")]."");
		else if($row[csf("receive_basis")]==7)
			$wopireq=return_field_value("requ_no","inv_purchase_requisition_mst","id=".$row[csf("booking_id")]."");
		echo "$('#txt_wo_pi').val('".$wopireq."');\n";
		echo "$('#txt_wo_pi_id').val(".$row[csf("booking_id")].");\n";
 		echo "$('#txt_receive_qty').val(".$row[csf("order_qnty")].");\n";
		echo "$('#txt_rate').val(".$row[csf("order_rate")].");\n";
		echo "$('#txt_ile').val(".$row[csf("order_ile_cost")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("order_uom")].");\n";
		echo "$('#txt_amount').val(".$row[csf("order_amount")].");\n";
		echo "$('#txt_book_currency').val(".$row[csf("cons_amount")].");\n";
		echo "$('#txt_order_qty').val(0);\n";
		echo "$('#txt_room').val(".$row[csf("room")].");\n";
		echo "$('#txt_rack').val(".$row[csf("rack")].");\n";
		echo "$('#txt_self').val(".$row[csf("self")].");\n";
		echo "$('#txt_binbox').val(".$row[csf("bin_box")].");\n";
		echo "$('#update_id').val(".$row[csf("id")].");\n";
		echo "$('#current_prod_id').val(".$row[csf("prod_id")].");\n";
 		echo "set_button_status(1, permission, 'fnc_general_item_receive_entry',1);\n";
		echo "disable_enable_fields( 'cbo_receive_basis*txt_receive_date*txt_challan_no*cbo_store_name*txt_exchange_rate', 0, '', '');\n";
		echo "fn_calile();\n";
	}
	exit();
}
if($action=="load_exchange_rate")
{
	if($data==1)
	{
		echo "$('#txt_exchange_rate').val(1);\n";
		echo "$('#txt_exchange_rate').attr('disabled',true);\n";
	}
	else
	{
		$last_exchange_rate=return_field_value("exchange_rate","inv_receive_master","currency_id=$data order by id limit 0,1");
		
		echo "$('#txt_exchange_rate').val(".$last_exchange_rate.");\n";
		echo "$('#txt_exchange_rate').attr('disabled',false);\n";
	}
	exit();
}
//################################################# function Here #########################################//
//function for domestic rate find--------------//
//parameters rate,ile cost,exchange rate,conversion factor
function return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor){
	$rate_ile=$rate+$ile_cost;
	$rate_ile_exchange=$rate_ile*$exchange_rate;
	$doemstic_rate=$rate_ile_exchange/$conversion_factor;
	return $doemstic_rate;	
}

if ($action=="general_item_receive_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql=" select id, recv_number,receive_basis,receive_date, challan_no, lc_no, store_id, supplier_id, currency_id, exchange_rate, pay_mode,source,booking_id from inv_receive_master where id='$data[1]'";
	$dataArray=sql_select($sql);

	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$lc_arr=return_library_array( "select id, lc_number from  com_btb_lc_master_details", "id", "lc_number"  );
	$pi_arr=return_library_array( "select id, pi_number from  com_pi_master_details", "id", "pi_number"  );
	$wo_arr=return_library_array( "select id, wo_number from  wo_non_order_info_mst", "id", "wo_number"  );
	$req_arr=return_library_array( "select id, requ_no from  inv_purchase_requisition_mst", "id", "requ_no"  );
?>
	<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u>Material Receiving & Inspection Report</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>MRIR Number:</strong></td><td width="175px"><?php echo $dataArray[0][csf('recv_number')]; ?></td>
            <td width="130"><strong>Receive Basis :</strong></td> <td width="175px"><?php echo $receive_basis_arr[$dataArray[0][csf('receive_basis')]]; ?></td>
            <td width="125"><strong>Receive Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('receive_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Challan No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>L/C No:</strong></td><td width="175px"><?php echo $lc_arr[$dataArray[0][csf('lc_no')]]; ?></td>
            <td><strong>Store Name:</strong></td><td width="175px"><?php echo $store_library[$dataArray[0][csf('store_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Supplier:</strong></td> <td width="175px"><?php echo $supplier_library[$dataArray[0][csf('supplier_id')]]; ?></td>
            <td><strong>Currency:</strong></td><td width="175px"><?php echo $currency[$dataArray[0][csf('currency_id')]]; ?></td>
            <td><strong>Exchange Rate:</strong></td><td width="175px"><?php echo $dataArray[0][csf('exchange_rate')]; ?></td>
        </tr>
        <tr>
            <td><strong>Pay Mode:</strong></td> <td width="175px"><?php echo $pay_mode[$dataArray[0][csf('pay_mode')]]; ?></td>
            <td><strong>Source:</strong></td><td width="175px"><?php echo $source[$dataArray[0][csf('source')]]; ?></td>
            <td><strong>WO/PI/Req.No:</strong></td> <td width="175px"><?php if ($dataArray[0][csf('receive_basis')]==1) echo $pi_arr[$dataArray[0][csf('booking_id')]]; else if ($dataArray[0][csf('receive_basis')]==2) echo $wo_arr[$dataArray[0][csf('booking_id')]]; else if ($dataArray[0][csf('receive_basis')]==7) echo $req_arr[$dataArray[0][csf('booking_id')]]; ?></td>
        </tr>
    </table>
         <br>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" style="margin-bottom:15px;">
            <thead bgcolor="#dddddd" align="center">
                <th width="40">SL</th>
                <th width="80" align="center">Item Category</th>
                <th width="150" align="center">Item Group</th>
                <th width="200" align="center">Item Description</th>
                <th width="50" align="center">UOM</th> 
                <th width="80" align="center">Recv. Qnty.</th>
                <th width="50" align="center">Rate</th>
                <th width="70" align="center">Amount</th>
                <th width="80" align="center">PI/Ord/Req Qnty Bal.</th> 
                <th width="80" align="center">Warranty Exp. Date</th>                  
            </thead>
<?php
	$mrr_no =$dataArray[0][csf('recv_number')];;
	$up_id =$data[1];
	$cond="";
	if($mrr_no!="") $cond .= " and a.recv_number='$mrr_no'";
	if($up_id!="") $cond .= " and a.id='$up_id'";
	 $i=1;
	 $item_name_arr=return_library_array( "select id, item_name from  lib_item_group", "id", "item_name"  );

	$sql_result= sql_select("select a.id, a.receive_basis, b.order_uom, b.order_qnty, b.order_rate, b.order_amount, b.cons_amount,b.balance_qnty,b.expire_date, c.item_category_id, c.item_group_id, c.item_description, c.product_name_details, c.lot, c.item_size from inv_receive_master a, inv_transaction b,  product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category not in (1,2,3,5,6,7,12,13,14) and a.entry_form=20 $cond");
	
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			$order_qnty=$row[csf('order_qnty')];
			$order_qnty_sum += $order_qnty;
			
			$order_amount=$row[csf('order_amount')];
			$order_amount_sum += $order_amount;
			
			$balance_qnty=$row[csf('balance_qnty')];
			$balance_qnty_sum += $balance_qnty;
			
			$desc=$row[csf('item_description')];
			
			if($row[csf('item_size')]!="")
			{
				$desc.=", ".$row[csf('item_size')];
			}
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $item_category[$row[csf('item_category_id')]]; ?></td>
                <td><?php echo $item_name_arr[$row[csf('item_group_id')]]; ?></td>
                <td><?php echo $desc; ?></td>
                <td><?php echo $unit_of_measurement[$row[csf('order_uom')]]; ?></td>
                <td align="right"><?php echo $row[csf('order_qnty')]; ?></td>
                <td align="right"><?php echo $row[csf('order_rate')]; ?></td>
                <td align="right"><?php echo $row[csf('order_amount')]; ?></td>
                <td align="right"><?php  echo $row[csf('balance_qnty')]; ?></td>
                <td><?php echo change_date_format($row[csf('expire_date')]); ?></td>
			</tr>
			<?php
			$i++;
			}
			?>
        	<tr> 
                <td align="right" colspan="5" >Total</td>
                <td align="right"><?php echo number_format($order_qnty_sum,0,'',','); ?></td>
                <td align="right" colspan="2" ><?php echo $order_amount_sum; ?></td>
                <td align="right" ><?php echo number_format($balance_qnty_sum,0,'',','); ?></td>
                <td align="right">&nbsp;</td>
			</tr>
		</table>
        
		<div style="margin-left:27px;">
        <?php
			$remarks=return_field_value("remarks","inv_receive_master","company_id=$data[0] and id='$data[1]'");
			echo "Remarks : ".$remarks;
		?>
        </div>

		 <?php
            echo signature_table(11, $data[0], "900px");
         ?>
	</div>
	</div>
<?php
exit();
}
?>
