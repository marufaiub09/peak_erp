-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_booking_dtls_hstry`
--

DROP TABLE IF EXISTS `wo_booking_dtls_hstry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_booking_dtls_hstry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) NOT NULL DEFAULT '0',
  `booking_dtls_id` int(11) NOT NULL DEFAULT '0',
  `job_no` varchar(25) NOT NULL,
  `po_break_down_id` int(11) NOT NULL DEFAULT '0',
  `pre_cost_fabric_cost_dtls_id` int(11) NOT NULL,
  `color_size_table_id` int(11) NOT NULL,
  `booking_no` varchar(25) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `is_short` int(11) NOT NULL COMMENT '1=yes,2=no',
  `fabric_color_id` int(11) NOT NULL,
  `item_size` varchar(50) NOT NULL,
  `fin_fab_qnty` double NOT NULL,
  `grey_fab_qnty` double NOT NULL,
  `rate` double NOT NULL,
  `amount` double NOT NULL,
  `color_type` int(11) NOT NULL,
  `construction` varchar(100) NOT NULL,
  `copmposition` varchar(200) NOT NULL,
  `gsm_weight` int(11) NOT NULL,
  `dia_width` varchar(50) NOT NULL,
  `process_loss_percent` double NOT NULL,
  `trim_group` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `brand_supplier` varchar(150) NOT NULL,
  `uom` int(11) NOT NULL,
  `process` int(11) NOT NULL,
  `sensitivity` int(11) NOT NULL,
  `wo_qnty` double NOT NULL,
  `delivery_date` date NOT NULL,
  `cons_break_down` text NOT NULL,
  `rmg_qty` int(11) NOT NULL,
  `gmt_item` int(11) NOT NULL,
  `responsible_dept` varchar(150) NOT NULL,
  `responsible_person` varchar(150) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `gmts_size` int(11) NOT NULL DEFAULT '0',
  `gmts_color_id` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_booking_dtls_hstry`
--

LOCK TABLES `wo_booking_dtls_hstry` WRITE;
/*!40000 ALTER TABLE `wo_booking_dtls_hstry` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_booking_dtls_hstry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
