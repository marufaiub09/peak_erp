<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="on_change_data")
{
	$explode_data = explode("_",$data);
	$type = $explode_data[0];
	$company_id = $explode_data[1];
		
	$nameArray= sql_select("select id, dyeing_fin_bill from  variable_settings_subcon where company_id='$company_id' and variable_list=1 order by id");
	if(count($nameArray)>0)$is_update=1;else $is_update=0;
	?>
	<fieldset>
	<legend>Bill On</legend>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td width="130" align="left" id="bill_on">Bill On</td>
                    <td width="190">
						<?php 
							echo create_drop_down( "cbo_bill_on", 170, $dyeing_finishing_bill,'', 1, '---- Select ----', $nameArray[0][csf('dyeing_fin_bill')], "",'','' );
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
            <table cellspacing="0" width="100%" >
                <tr> 
                    <td align="center" width="320">&nbsp;</td>						
                </tr>						 
                <tr>
                    <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                    <input  type="hidden"name="update_id" id="update_id" value="<?php echo $nameArray[0][csf('id')]; ?>">
						<?php 
							echo load_submit_buttons( $permission, "fnc_subcontract_variable_settings", $is_update,0 ,"reset_form('subcontractVariable','','')",1);
                        ?>
                    </td>					
                </tr>
            </table>
        </div>
	</fieldset>
	<?php
	exit();
}
	
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if (is_duplicate_field( "company_id", "variable_settings_subcon", "company_id=$cbo_company_id and variable_list=$cbo_variable_list") == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$id=return_next_id( "id", "variable_settings_subcon", 1);
			
			$field_array="id, company_id, variable_list, dyeing_fin_bill, inserted_by, insert_date, status_active"; 
			$data_array="(".$id.",".$cbo_company_id.",".$cbo_variable_list.",".$cbo_bill_on.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
			//echo "insert into variable_settings_subcon (".$field_array.") values ".$data_array."";die;
			$rID=sql_insert("variable_settings_subcon",$field_array,$data_array,1);
		}
		if($db_type==0)
		{
			if($rID )
			{
				mysql_query("COMMIT");  
				echo "0**".$rID;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$rID;
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID )
			{
				oci_commit($con);   
				echo "0**".$rID;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$rID;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="company_id*variable_list*dyeing_fin_bill*updated_by*update_date*status_active"; 
		$data_array="".$cbo_company_id."*".$cbo_variable_list."*".$cbo_bill_on."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
		$rID=sql_update("variable_settings_subcon",$field_array,$data_array,"id","".$update_id."",1);
		 
		if($db_type==0)
		{
			if($rID )
			{
				mysql_query("COMMIT");  
				echo "1**".$rID;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$rID;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID )
			{
				oci_commit($con);   
				echo "1**".$rID;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$rID;
			}
		}
		disconnect($con);
		die;
	}	
}
?>