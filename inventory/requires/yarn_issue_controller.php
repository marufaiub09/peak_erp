<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------

//load drop down supplier
if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier", 170, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data' and b.party_type =2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
  	 
	exit();
}

//load drop down company location
if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location", 170, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select Location --", $selected, "",0 );     	 
	exit();
}

//load drop down company Store location
if ($action=="load_drop_down_store")
{
	echo create_drop_down( "cbo_store_name", 142, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=1 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "-- Select Store --", $selected, "",0 );     	 
	exit();
}

if($action=="load_drop_down_buyer")
{
	$data=explode("_",$data);
	if($data[1]==1) $party="1,3,21,90"; else $party="80";
	
	echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[0]' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in ($party)) group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",$data[1] );  
	exit();
	
}

if($action=="load_drop_down_dyeing_color")
{
	$data=explode("_",$data);
	$booking_id=$data[0];
	$color_id=$data[1];
	
	echo create_drop_down( "cbo_dyeing_color", 142, "select a.id, a.color_name from lib_color a, wo_yarn_dyeing_dtls b where a.id=b.yarn_color and b.status_active =1 and b.is_deleted=0 and b.mst_id='$booking_id' group by a.id, a.color_name order by a.color_name","id,color_name", 1, "-- Select --", $selected, "",$color_id );  
	exit();
}

//load_drop_down_purpose
if ($action=="load_drop_down_purpose")
{
	if($data==1)
	{
		echo create_drop_down( "cbo_issue_purpose", 170, $yarn_issue_purpose,"", 1, "-- Select Purpose --", $selected, "active_inactive(this.value)","","1,4" );
	}
	else if($data==2)
	{
		echo create_drop_down( "cbo_issue_purpose", 170, $yarn_issue_purpose,"", 1, "-- Select Purpose --", $selected, "active_inactive(this.value)","","2,5,6,7" );
	}
	else
	{
		echo create_drop_down( "cbo_issue_purpose", 170, $yarn_issue_purpose,"", 1, "-- Select Purpose --", $selected, "active_inactive(this.value)","","" );
	}
	exit();
}

//load drop down knitting company
if ($action=="load_drop_down_knit_com")
{
	$exDataArr = explode("**",$data);	
	$knit_source=$exDataArr[0];
	$company=$exDataArr[1];
	$issuePurpose=$exDataArr[2];
	if($company=="" || $company==0) $company_cod = ""; else $company_cod = " and id=$company";
	if($knit_source==1)
		echo create_drop_down( "cbo_knitting_company", 170, "select id,company_name from lib_company where status_active=1 and is_deleted=0 $company_cod order by company_name","id,company_name", 1, "-- Select --", $company, "" );
	else if($knit_source==3 && $issuePurpose==1)
		echo create_drop_down( "cbo_knitting_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,20) and a.status_active=1 group by a.id, a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3 && $issuePurpose==2)
		echo create_drop_down( "cbo_knitting_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,21,24) and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3)		
		echo create_drop_down( "cbo_knitting_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==0)		
		echo create_drop_down( "cbo_knitting_company", 170, $blank_array,"", 1, "-- Select --", $selected, "","","" );	
	exit();	
}
 
// wo/pi popup here----------------------// 
if ($action=="fabbook_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
<script>
	
	function fn_check()
	{
		/*if(form_validation('cbo_buyer_name','Buyer Name')==false )
			return;
		else*/
			show_list_view ( document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $issue_purpose; ?>, 'create_fabbook_search_list_view', 'search_div', 'yarn_issue_controller', 'setFilterGrid(\'list_view\',-1)');
	}
		
	function js_set_value(booking_dtls)
	{ 
		$("#hidden_booking_number").val(booking_dtls);  
		parent.emailwindow.hide();
	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Buyer Name</th>
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
						   echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select --", $selected, "" );
                        ?>
                    </td>
                    <td>
                        <?php  
                            $search_by = array(1=>'Booking No', 2=>'Buyer Order', 3=>'Job No');
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../')";
							echo create_drop_down( "cbo_search_by", 120, $search_by, "", 0, "--Select--", "", $dd, 0);
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="fn_check()" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_booking_id" value="" />
                    <input type="hidden" id="hidden_booking_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_fabbook_search_list_view")
{
 	$ex_data = explode("_",$data);
	$buyer = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = trim($ex_data[2]);
	$txt_date_from = $ex_data[3];
	$txt_date_to = $ex_data[4];
	$company = $ex_data[5];
	$booking_type = $ex_data[6];
 	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for Booking No
		{
			$sql_cond .= " and a.booking_no LIKE '%$txt_search_common'";	
 		}
		else if(trim($txt_search_by)==2) // for buyer order
		{
			$sql_cond .= " and b.po_number LIKE '%$txt_search_common%'";	// wo_po_break_down			
 		}
		else if(trim($txt_search_by)==3) // for job no
		{
			$sql_cond .= " and a.job_no LIKE '%$txt_search_common%'";				
 		}
 	} 
 	
	if($txt_date_from!="" && $txt_date_to!="" )
	{
		if($db_type==0)
		{ 
			$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
	
	if( trim($buyer)!=0 ) $sql_cond .= " and a.buyer_id='$buyer'";
	if( trim($company)!=0 ) $sql_cond .= " and a.company_id='$company'";
	if( trim($booking_type)==1 ) $sql_cond .= " and a.booking_type!=4";
	else if( trim($booking_type)==4 ) $sql_cond .= " and a.booking_type=4";
 	
  	if( trim($booking_type)==8 )
	{
			$sql = "select a.id, a.booking_no_prefix_num, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no as job_no_mst
			from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b 
			where 
				a.booking_no=b.booking_no and 
 				a.item_category=2 and 
 				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 
				$sql_cond 
				group by a.id, a.booking_no_prefix_num, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no"; 
	}
	else if( trim($booking_type)==2)
	{
		$sql_cond='';
		if(trim($txt_search_by)==1) // for Booking No
		{
			$sql_cond .= " and a.ydw_no LIKE '%$txt_search_common'";	
 		}
		else if(trim($txt_search_by)==3) // for job no
		{
			$sql_cond .= " and c.job_no LIKE '%$txt_search_common%'";				
 		}
		
		if( $txt_date_from!="" && $txt_date_to!="" ) 
		{
			if($db_type==0)
			{
				$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			}
			else
			{
				$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
			}
		}
		if( trim($buyer)!=0 ) $sql_cond .= " and c.buyer_name='$buyer'";
		if( trim($company)!=0 ) $sql_cond .= " and a.company_id='$company'";
		
		if($db_type==0)
		{
			$sql = "select a.id, a.yarn_dyeing_prefix_num as booking_no_prefix_num, a.ydw_no as booking_no, a.booking_date, 1 as item_category, a.delivery_date, c.buyer_name as buyer_id, group_concat(distinct(c.job_no)) as job_no_mst, group_concat(distinct(c.id)) as job_no_id
			from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b, wo_po_details_master c 
			where 
				a.id=b.mst_id and 
				b.job_no_id=c.id and
 				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 
				$sql_cond 
				group by a.id"; 
		}
		else
		{
			$sql = "select a.id, a.yarn_dyeing_prefix_num as booking_no_prefix_num, a.ydw_no as booking_no, a.booking_date, a.delivery_date, 1 as item_category, max(c.buyer_name) as buyer_id,LISTAGG(c.job_no, ',') WITHIN GROUP (ORDER BY c.job_no) as job_no_mst, LISTAGG(c.id, ',') WITHIN GROUP (ORDER BY c.id) as job_no_id
			from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b, wo_po_details_master c 
			where 
				a.id=b.mst_id and 
				b.job_no_id=c.id and
 				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 
				$sql_cond 
				group by a.id, a.yarn_dyeing_prefix_num, a.ydw_no, a.booking_date,  a.delivery_date"; 
		}
	}
	else
	{
		$sql = "select a.id, a.booking_no_prefix_num, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no as job_no_mst 
			from wo_booking_mst a, wo_po_break_down b 
			where 
				a.job_no=b.job_no_mst and 
 				a.item_category=2 and 
 				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 
				$sql_cond 
				group by a.id, a.booking_no_prefix_num, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no"; 
	}
	$result = sql_select($sql);
	$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	?>
    <div align="left">
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" >
        <thead>
            <th width="30">SL</th>
            <th width="105">Booking No</th>
            <th width="90">Book. Date</th>               
            <th width="100">Buyer</th>
            <th width="90">Item Cat.</th>
            <th width="90">Job No</th>
            <th width="90">Order Qnty</th>
            <th width="80">Ship. Date</th>
            <th >Order No</th>
        </thead>
	</table>
    
	<div style="width:990px; max-height:240px; overflow-y:scroll" id="list_container_batch" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" id="list_view">  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	 
                 
                $po_qnty_in_pcs=0; $po_no=''; $min_shipment_date='';
                if( trim($booking_type)!=8 )
				{
					if(trim($booking_type)==2 )
					{
						$po_sql = "select a.style_ref_no, b.po_number, b.pub_shipment_date, b.po_quantity, a.total_set_qnty as ratio from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.id in(".$row[csf('job_no_id')].") group by b.id, b.po_number, b.pub_shipment_date, b.po_quantity, a.style_ref_no, a.total_set_qnty";	
					}
					else
					{
						$po_sql = "select a.style_ref_no, b.po_number, b.pub_shipment_date, b.po_quantity, a.total_set_qnty as ratio from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in (".$row[csf('po_break_down_id')].") group by b.id, b.po_number, b.pub_shipment_date, b.po_quantity, a.style_ref_no, a.total_set_qnty";	
					}
					$nameArray=sql_select($po_sql);
					$style_ref_no="";
					foreach ($nameArray as $po_row)
					{
						if($po_no=="") $po_no=$po_row[csf('po_number')]; else $po_no.=",".$po_row[csf('po_number')];
						
						if($min_shipment_date=='')
						{
							$min_shipment_date=$po_row[csf('pub_shipment_date')];
						}
						else
						{
							if($po_row[csf('pub_shipment_date')]<$min_shipment_date) $min_shipment_date=$po_row[csf('pub_shipment_date')]; else $min_shipment_date=$min_shipment_date;
						}
						$po_qnty_in_pcs+=$po_row[csf('po_quantity')]*$po_row[csf('ratio')];
						$style_ref_no = $po_row[csf('style_ref_no')];
					}
				}
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]; ?>_<?php echo $row[csf('booking_no')]; ?>_<?php echo $row[csf('buyer_id')]; ?>_<?php echo $row[csf('job_no_mst')]; ?>_<?php echo $style_ref_no; ?>');"> 
                    <td width="30"><?php echo $i; ?></td>
                    <td width="105"><p><?php echo $row[csf('booking_no_prefix_num')]; ?></p></td>
                    <td width="90" align="center"><?php echo change_date_format($row[csf('booking_date')]); ?></td>               
                    <td width="100"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                    <td width="90"><p><?php echo $item_category[$row[csf('item_category')]]; ?></p></td>
                    <td width="90"><p><?php echo implode(",",array_unique(explode(",",$row[csf('job_no_mst')]))); ?></p></td>
                    <td width="90" align="right"><?php echo $po_qnty_in_pcs; ?></td>
                    <td width="80" align="center"><?php echo change_date_format($min_shipment_date); ?></td>
                    <td><p><?php echo $po_no; ?></p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
      </div>
    </div>
	<?php	
	exit();		
}

//yarn LOT POP UP Search Here----------------------------------//  
if($action=="yarnLot_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
	?>
	<script>
		function fn_check_lot()
		{ 
			if(form_validation('cbo_supplier','Supplier Name')==false )
				return;
			else
				show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+<?php echo $company; ?>+'_'+<?php echo $issue_purpose; ?>, 'create_lot_search_list_view', 'search_div', 'yarn_issue_controller', 'setFilterGrid(\'list_view\',-1)');
		}
			
		function js_set_value(prod_id)
		{ 
 			$("#hidden_prod_id").val(prod_id);  
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
	<div align="center" style="width:100%;" >
	<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
		<table width="800" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
				<thead>
					<tr>                	 
						<th>Supplier Name</th>
						<th>Search By</th>
						<th align="center" width="200" id="search_by_td_up">Enter Lot Number</th>
 						<th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
					</tr>
				</thead>
				<tbody>
					<tr align="center">
						<td>
							<?php  
							  echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where status_active=1 order by supplier_name","id,supplier_name", 1, "-- Select --", $supplier, "",1 );
							?>
						</td>
						<td align="center">
							<?php  
								$search_by = array(1=>'Lot No', 2=>'Yarn Count');
								$dd="change_search_event(this.value, '0*0', '0*0', '../../')";
								echo create_drop_down( "cbo_search_by", 150, $search_by, "", 0, "--Select--", "", $dd, 0);
							?>
						</td>
						<td width="180" align="center" id="search_by_td">				
							<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
						</td> 
						<td align="center">
							<input type="button" name="btn_show" class="formbutton" value="Show" onClick="fn_check_lot()" style="width:100px;" />				
						</td>
					</tr>
						<!-- Hidden field here-------->
						<input type="hidden" id="hidden_prod_id" value="" />
 						<!-- ---------END------------->
 				</tbody>
			 </tr>         
			</table>    
			<div align="center" valign="top" style="margin-top:5px" id="search_div"> </div> 
			</form>
	   </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
}

if($action=="create_lot_search_list_view")
{
 	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = trim($ex_data[2]);
	$company = $ex_data[3];
 	$issue_purpose=$ex_data[4];
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for LOT NO
		{
			$sql_cond .= " and b.lot LIKE '%$txt_search_common%'";	
 		}
		else if(trim($txt_search_by)==2) // for Yarn Count
		{
			$sql_cond .= " and b.yarn_count_id LIKE '%$txt_search_common%'";	 	
 		} 
 	} 
 		
	if( trim($supplier)!=0 ) $sql_cond .= " and b.supplier_id='$supplier'";
	if( trim($company)!=0 ) $sql_cond .= " and b.company_id='$company'";
	
	if($db_type==0)
	{
		$sql = "select b.id, b.company_id, b.supplier_id, b.lot, b.current_stock, b.allocated_qnty, b.available_qnty, b.yarn_count_id, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color, group_concat(distinct (a.store_id)) as store_id
				from product_details_master b, inv_transaction a
				where
					a.prod_id=b.id and 
					a.item_category=1 and 
					a.transaction_type=1 and 
					b.item_category_id=1 and				 
					b.status_active=1 and 
					b.current_stock>0
					$sql_cond 
					group by b.id, b.company_id, b.supplier_id, b.lot, b.current_stock, b.allocated_qnty, b.available_qnty, b.yarn_count_id, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color order by b.lot"; 
	}
	else
	{
		$sql = "select b.id, b.company_id, b.supplier_id, b.lot, b.current_stock, b.allocated_qnty, b.available_qnty, b.yarn_count_id, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color, LISTAGG(a.store_id, ',') WITHIN GROUP (ORDER BY a.store_id) as store_id
				from product_details_master b, inv_transaction a
				where
					a.prod_id=b.id and 
					a.item_category=1 and 
					a.transaction_type=1 and 
					b.item_category_id=1 and				 
					b.status_active=1 and 
					b.current_stock>0
					$sql_cond 
					group by b.id, b.company_id, b.supplier_id, b.lot, b.current_stock, b.allocated_qnty, b.available_qnty, b.yarn_count_id, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color order by b.lot"; 	
	}
 	//echo $sql;
	$result = sql_select($sql);
	$color_arr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$yarn_count_arr = return_library_array( "select id,yarn_count from lib_yarn_count",'id','yarn_count');
	$store_arr = return_library_array( "select id,store_name from lib_store_location",'id','store_name');
	$check_allocation_arr = return_library_array( "select company_name,allocation from variable_settings_inventory",'company_name','allocation');
  	?> 
    <div align="left" style="margin-left:20px">    
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="950" class="rpt_table" >
        <thead>
            <th width="40">SL</th>
            <th width="100">Yarn Lot</th>
            <th width="100">Yarn Count</th>               
            <th width="200">Composition</th>
            <th width="100">Yarn Type</th>
            <th width="160">Color</th>
            <th width="130">Store</th>
            <th >Current Stock</th>
        </thead>
	</table>
    
	<div style="width:970px; max-height:240px; overflow-y:scroll" id="list_container_batch" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="950" class="rpt_table" id="list_view">  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if ($i%2==0)  $bgcolor="#E9F3FF";
                else $bgcolor="#FFFFFF";
				 
				$composition_string = $composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%";
				if($row[csf('yarn_comp_type2nd')]!=0) $composition_string .= " ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
				 
				$store='';
				$store_id=explode(",",$row[csf('store_id')]);
				foreach($store_id as $val)
				{
					if($store=='') $store=$store_arr[$val]; else $store.=",".$store_arr[$val];
				}
				 
				/*$check_allocation=$check_allocation_arr[$row[csf('company_id')]]; $stock_qnty=0;
				if($check_allocation==1)
				{
					if($issue_purpose==1 || $issue_purpose==2)
					{
						$stock_qnty=$row[csf("allocated_qnty")];
					}
					else
					{
						$stock_qnty=$row[csf("available_qnty")];
					}
				}
				else
				{
					$stock_qnty=$row[csf("current_stock")];
				}*/
				
				$stock_qnty=$row[csf("current_stock")];
          		?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]; ?>');"> 
                    <td width="40"><?php echo $i; ?></td>
                    <td width="100"><p>&nbsp;<?php echo $row[csf('lot')]; ?></p></td>
                    <td width="100"><p><?php echo $yarn_count_arr[$row[csf('yarn_count_id')]]; ?>&nbsp;</p></td>               
                    <td width="200"><p><?php echo $composition_string; ?></p></td>
                    <td width="100"><p><?php echo $yarn_type[$row[csf('yarn_type')]]; ?>&nbsp;</p></td>
                    <td width="160"><p><?php echo $color_arr[$row[csf('color')]]; ?>&nbsp;</p></td>
                    <td width="130"><p><?php echo $store; ?>&nbsp;</p></td>
                    <td align="right"><p><?php echo number_format($stock_qnty,2); ?>&nbsp;</p></td>
                 </tr>
				<?php
                $i++;
            }
        	?>
        </table>
      </div>
    </div>
	<?php	
	exit();	
}

// child form data populate after lot pop up search
if($action == "populate_data_child_from")
{
	$data=explode("**",$data);
	$prodID = $data[0];
	$issue_purpose = $data[1];
	
	$sql = "select id,company_id,supplier_id,store_id,item_category_id,detarmination_id,sub_group_code,sub_group_name,item_group_id,item_description,product_name_details,lot,item_code,unit_of_measure,re_order_label,minimum_label,maximum_label,item_account,packing_type,avg_rate_per_unit,last_purchased_qnty,current_stock,last_issued_qnty,stock_value,yarn_count_id,yarn_comp_type1st,yarn_comp_percent1st,yarn_comp_type2nd,yarn_comp_percent2nd,yarn_type,color,brand,allocated_qnty,available_qnty
			from product_details_master
			where 
				id='$prodID' and 
				item_category_id=1 and
				status_active=1 and 
				is_deleted=0";
	
	$result = sql_select($sql);
	foreach($result as $row)
	{
		$composition_string = $composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')];
		if($row[csf('yarn_comp_type2nd')]!=0) $composition_string .=" ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')];
		
		if($db_type==0)
		{
			$store_id=return_field_value("group_concat(distinct(store_id)) as store_id","inv_transaction","prod_id=".$row[csf('id')]." and item_category=1 and transaction_type=1","store_id");
		}
		else
		{
			$store_id=return_field_value("LISTAGG(store_id, ',') WITHIN GROUP (ORDER BY store_id) as store_id","inv_transaction","prod_id=".$row[csf('id')]." and item_category=1 and transaction_type=1","store_id");
			$store_id=implode(",",array_unique(explode(",",$store_id)));
		}
		
		$stock_qnty=$row[csf("current_stock")];
		
		echo "$('#cbo_store_name').val('".$store_id."');\n";
		echo "$('#txt_lot_no').val('".$row[csf("lot")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("id")]."');\n";
		echo "$('#txt_current_stock').val(".$stock_qnty.");\n";
		echo "$('#cbo_yarn_count').val(".$row[csf("yarn_count_id")].");\n";
		echo "$('#txt_composition').val('".$composition_string."');\n";
		echo "$('#cbo_yarn_type').val('".$row[csf("yarn_type")]."');\n";
 		echo "$('#cbo_uom').val('".$row[csf("unit_of_measure")]."');\n";
		echo "$('#cbo_color').val('".$row[csf("color")]."');\n";
		echo "$('#cbo_brand').val('".$row[csf("brand")]."');\n";
	}
	exit();
}
 

 
if ($action=="po_popup")
{
	echo load_html_head_contents("PO Info", "../../", 1, 1,'','','');
	extract($_REQUEST); 
	//receive_basis='+receive_basis+'&cbo_company_id='+cbo_company_id+'&booking_no='+booking_no+'&all_po_id='+all_po_id+'&save_data='+save_data+'&action=po_popup'+distribution_method+issueQnty
	$data=explode("_",$data); 
	$po_id=$data[0]; //order ID 
	if($data[1])$type=$data[1]; else $type=0; //is popup search or not	
	$prevQnty=$data[2]; //previous input qnty po wise
	if($data[3]!="")$prev_method=$data[3];  
	else $prev_method=$distribution_method;  
	if($data[4]!="")$issueQnty=$data[4];  
	else $issueQnty=$issueQnty; 
	if($data[5]!="") $retnQnty=$data[5];  
?> 
	<script>
		var receive_basis=<?php echo $receive_basis; ?>;
		function fn_show_check()
		{
			if(form_validation('cbo_buyer_name','Buyer Name')==false )
			{
				return;
			}			
			show_list_view ( $('#txt_search_common').val()+'_'+$('#cbo_search_by').val()+'_'+<?php echo $cbo_company_id; ?>+'_'+$('#cbo_buyer_name').val()+'_'+'<?php echo $all_po_id; ?>'+'_'+<?php echo $receive_basis;?>, 'create_po_search_list_view', 'search_div', 'yarn_issue_controller', 'setFilterGrid(\'tbl_list_search\',-1);hidden_field_reset();');
			set_all();
		}
		
		function distribute_qnty(str)
		{
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_grey_qnty=$('#txt_prop_grey_qnty').val()*1;
				var tblRow = $("#tbl_list_search tr").length;
				var len=totalGrey=0;
				$("#tbl_list_search").find('tr').each(function()
				{
					len=len+1;
					var txtOrginal=$(this).find('input[name="txtOrginal[]"]').val()*1;
					if(txtOrginal==0)
					{
						$(this).remove();
					}
					else
					{
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;						
						var grey_qnty=(perc*txt_prop_grey_qnty)/100;
						
						totalGrey = totalGrey*1+grey_qnty*1;
						totalGrey = totalGrey.toFixed(2);						
						if(tblRow==len)
						{
							var balance = txt_prop_grey_qnty-totalGrey;
							if(balance!=0) grey_qnty=grey_qnty+(balance);							
						}
						$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
					}
				});
			}
			else
			{
				$('#txt_prop_grey_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					$(this).find('input[name="txtGreyQnty[]"]').val('');
				});
			}
		}
		
		function distribute_retn_qnty(str)
		{
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_retn_qnty=$('#txt_prop_retn_qnty').val()*1;
				var tblRow = $("#tbl_list_search tr").length;
				var len=totalGreyRetn=0;
				$("#tbl_list_search").find('tr').each(function()
				{
					len=len+1;
					var txtOrginal=$(this).find('input[name="txtOrginal[]"]').val()*1;
					if(txtOrginal==0)
					{
						$(this).remove();
					}
					else
					{
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;						
						var grey_retn_qnty=(perc*txt_prop_retn_qnty)/100;
						
						totalGreyRetn = totalGreyRetn*1+grey_retn_qnty*1;
						totalGreyRetn = totalGreyRetn.toFixed(2);						
						if(tblRow==len)
						{
							var balance = txt_prop_retn_qnty-totalGreyRetn;
							if(balance!=0) grey_retn_qnty=grey_retn_qnty+(balance);							
						}
						$(this).find('input[name="txtReturnQnty[]"]').val(grey_retn_qnty.toFixed(2));
					}
				});
			}
			else
			{
				$('#txt_prop_retn_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					$(this).find('input[name="txtReturnQnty[]"]').val('');
				});
			}
		}
		
		var selected_id = new Array();
		 function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_po_row_id').value;
			if(old!="")
			{   
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{  
					js_set_value( old[i] ) 
				}
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#po_id').val( id );
		}
		
		function show_grey_prod_recv() 
		{ 
 			var po_id=$('#po_id').val();
			var prev_save_string=$('#prev_save_string').val();
			var prev_method=$('#prev_method').val();
			var prev_total_qnty=$('#prev_total_qnty').val();
			var prev_retn_qnty=$('#prev_retn_qnty').val();			
			show_list_view ( po_id+'_'+'1'+'_'+prev_save_string+'_'+prev_method+'_'+prev_total_qnty+'_'+prev_retn_qnty, 'po_popup', 'search_div', 'yarn_issue_controller', '');
		}
		
		function hidden_field_reset()
		{
			$('#po_id').val('');
			$('#save_string').val( '' );
			$('#tot_grey_qnty').val( '' );
			selected_id = new Array();
		}
		
		function fnc_close()
		{
			var save_string='';	var tot_grey_qnty=''; var tot_retn_qnty=''; var no_of_roll=''; 
			var po_id_array = new Array();
			$("#tbl_list_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtGreyQnty=$(this).find('input[name="txtGreyQnty[]"]').val();	
				var txtReturnQnty=$(this).find('input[name="txtReturnQnty[]"]').val();				
				tot_grey_qnty=tot_grey_qnty*1+txtGreyQnty*1;
				tot_retn_qnty=tot_retn_qnty*1+txtReturnQnty*1;
								
				if(txtGreyQnty*1>0)
				{
					if(save_string=="")
					{
						save_string=txtPoId+"**"+txtGreyQnty+"**"+txtReturnQnty;
					}
					else
					{
						save_string+=","+txtPoId+"**"+txtGreyQnty+"**"+txtReturnQnty;
					}
					
					if( jQuery.inArray( txtPoId, po_id_array) == -1 ) 
					{
						po_id_array.push(txtPoId);
					}
				}
			});
			
			$('#save_string').val( save_string );
			$('#tot_grey_qnty').val( tot_grey_qnty );
			$('#tot_retn_qnty').val( tot_retn_qnty );
			$('#all_po_id').val( po_id_array );
			$('#distribution_method').val( $('#cbo_distribiution_method').val() );			
			parent.emailwindow.hide();
		}
    </script>

</head>
<body>
	
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:10px">
        <?php if($type!=1){?>
        	<!-- previous data here---->
            <input type="hidden" name="prev_save_string" id="prev_save_string" class="text_boxes" value="<?php echo $save_data; ?>">
            <input type="hidden" name="prev_total_qnty" id="prev_total_qnty" class="text_boxes" value="<?php echo $issueQnty; ?>">
            <input type="hidden" name="prev_retn_qnty" id="prev_retn_qnty" class="text_boxes" value="<?php echo $retnQnty; ?>">
            <input type="hidden" name="prev_method" id="prev_method" class="text_boxes" value="<?php echo $distribution_method; ?>">
            <!--- END-------->
        	<input type="hidden" name="save_string" id="save_string" class="text_boxes" value="<?php echo $save_data; ?>">
            <input type="hidden" name="tot_grey_qnty" id="tot_grey_qnty" class="text_boxes" value="">
            <input type="hidden" name="tot_retn_qnty" id="tot_retn_qnty" class="text_boxes" value="">
            <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
            <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
		<?php }  ?>
	<?php   
	if($receive_basis==2 || $receive_basis==3)
	{
		if($receive_basis!=3)
		{
	?>
		<table cellpadding="0" cellspacing="0" width="620" class="rpt_table">
			<thead>
				<th>Buyer</th>
				<th>Search By</th>
				<th>Search</th>
				<th>
					<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
					<input type="hidden" name="po_id" id="po_id" value="">
				</th> 
			</thead>
			<tr class="general">
				<td align="center">
					<?php
						echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "","" ); 
					?>       
				</td>
				<td align="center">	
					<?php
						$search_by_arr=array(1=>"PO No",2=>"Job No");
						echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
					?>
				</td>                 
				<td align="center">	 
					<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
 				</td> 						
				<td align="center">
					<input type="button" name="button2" class="formbutton" value="Show" onClick="fn_show_check()" style="width:100px;" />
				</td>
			</tr>
		</table>
        <?php } ?>
		<div id="search_div" style="margin-top:10px">
        <?php 
			if($all_po_id!="" || $po_id!="")
			{ 
			?>    
				<div style="width:600px; margin-top:10px" align="center">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
						<thead>
							<th>Total Issue Qnty</th>
                            <th>Total Returnable Qnty</th>
							<th>Distribution Method</th>
						</thead>
						<tr class="general">
							<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
                            <td><input type="text" name="txt_prop_retn_qnty" id="txt_prop_retn_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $retnQnty; ?>" style="width:120px"  onBlur="distribute_retn_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
							<td>
								<?php
									$distribiution_method=array(1=>"Proportionately",2=>"Manually");
									echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);distribute_retn_qnty(this.value);",0 );
								?>
							</td>
						</tr>
					</table>
				</div>
				<div style="margin-left:10px; margin-top:10px">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
						<thead>
							<th width="150">PO No</th>
							<th width="110">PO Qnty</th>
							<th width="140">Issue Qnty</th>
                            <th>Returnable Qnty</th>
						</thead>
					</table>
					<div style="width:600px; max-height:280px; overflow-y:scroll" id="list_container" align="left">  
						<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">   
					<?php
						if($po_id=="") $po_id=$all_po_id; else $po_id=$po_id;
						$i=1; $tot_po_qnty=0;
						if($po_id!="")
						{
							$po_sql="select b.id, b.po_number, b.po_quantity, a.total_set_qnty 
							from wo_po_details_master a, wo_po_break_down b 
							where a.job_no=b.job_no_mst and b.id in ($po_id) group by b.id, b.po_number, b.po_quantity, a.total_set_qnty";	
						}				 
						//echo $po_sql; 						
						$explSaveData = explode(",",$save_data); 
						$po_dataArray=array();
						foreach($explSaveData as $val)
						{
							$woQnty = explode("**",$val);
							$po_dataArray[$woQnty[0]]['qnty']=$woQnty[1];
							$po_dataArray[$woQnty[0]]['retQnty']=$woQnty[2];
						}
						$nameArray=sql_select($po_sql);
						foreach($nameArray as $row)
						{  
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$po_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('total_set_qnty')];
							$tot_po_qnty+=$po_qnty_in_pcs;
							
							/*$woQnty = explode("**",$explSaveData[$i-1]);
							if($woQnty[0]==$row[csf('id')]) $qnty = $woQnty[1]; else $qnty = "";*/
								
							$qnty=$po_dataArray[$row[csf('id')]]['qnty'];
							$returnQnty=$po_dataArray[$row[csf('id')]]['retQnty'];						
						 ?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="150">
									<p><?php echo $row[csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
									<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
								</td>
								<td width="110" align="right">
									<?php echo $po_qnty_in_pcs; ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_qnty_in_pcs; ?>">
								</td>
								<td width="140" align="center">
									<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i;?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $qnty; ?>">
								</td>
                                <td align="center">
									<input type="text" name="txtReturnQnty[]" id="txtReturnQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $returnQnty; ?>">
								</td>					
							</tr>
						<?php 
						$i++; 
						} 
						?>
						<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
					</table>                
				</div>
				<table width="620" id="table_id">
						 <tr>
							<td align="center" >
								<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
							</td>
						</tr>
					</table>
				</div>
          <?php } ?>  
       </div>     
	<?php
	}
	else
	{   
	?>
		<div style="width:600px; margin-top:10px" align="center">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
				<thead>
					<th>Total Issue Qnty</th>
                    <th>Total Returnable Qnty</th>
					<th>Distribution Method</th>
				</thead>
				<tr class="general">
					<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
                    <td><input type="text" name="txt_prop_retn_qnty" id="txt_prop_retn_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $retnQnty; ?>" style="width:120px"  onBlur="distribute_retn_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
					<td>
						<?php
							$distribiution_method=array(1=>"Proportionately",2=>"Manually");
                            echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);distribute_retn_qnty(this.value);",0 );
						?>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:10px; margin-top:10px">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
				<thead>
					<th width="150">PO No</th>
					<th width="110">PO Qnty</th>
					<th width="140">Issue Qnty</th>
                    <th>Returnable Qnty</th>
				</thead>
			</table>
			<div style="width:600px; max-height:150px; overflow-y:scroll" id="list_container" align="left"> 
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">  
					<?php 
					$i=1; $tot_po_qnty=0;
					 
 					if($type==1 && $po_id!="")
					{ 
						if($booking_no=="")
						{
							$po_sql="select b.id, b.po_number, b.po_quantity, a.total_set_qnty from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id) group by b.id, b.po_number, b.po_quantity, a.total_set_qnty";
						}
						else
						{
							$po_sql="select b.id, b.po_number, b.po_quantity, a.total_set_qnty from wo_po_details_master a, wo_booking_dtls c, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' and b.id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by b.id, b.po_number, b.po_quantity, a.total_set_qnty";	
						}
					}
					else
					{
						if($issue_purpose==2)
						{
							$po_sql="select b.id, b.po_number, b.po_quantity, a.total_set_qnty from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.job_no='$job_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.id, b.po_number, b.po_quantity, a.total_set_qnty";	
						}
						else
						{
							if($booking_no=="")
							{
								$po_sql="select b.id, b.po_number, b.po_quantity, a.total_set_qnty from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.id, b.po_number, b.po_quantity, a.total_set_qnty";
							}
							else
							{
								$po_sql="select b.id, b.po_number, b.po_quantity, a.total_set_qnty from wo_po_details_master a, wo_booking_dtls c, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by b.id, b.po_number, b.po_quantity, a.total_set_qnty";	
							}
						}
					} 
					 
					if($save_string=="" && $type==1) $save_data=$prevQnty;
					$explSaveData = explode(",",$save_data);
					
					$po_dataArray=array();
					foreach($explSaveData as $val)
					{
						$woQnty = explode("**",$val);
						$po_dataArray[$woQnty[0]]['qnty']=$woQnty[1];
						$po_dataArray[$woQnty[0]]['retQnty']=$woQnty[2];
					}
					
 					$nameArray=sql_select($po_sql);
					foreach($nameArray as $row)
					{  
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
							
						$po_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('total_set_qnty')];
						$tot_po_qnty+=$po_qnty_in_pcs;
						
						//$woQnty = explode("**",$explSaveData[$i-1]);
						//if($woQnty[0]==$row[csf('id')]) $qnty = $woQnty[1]; else $qnty = "";	
						
						$qnty=$po_dataArray[$row[csf('id')]]['qnty'];
						$returnQnty=$po_dataArray[$row[csf('id')]]['retQnty'];				
						
					 ?>
						<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
							<td width="150">
								<p><?php echo $row[csf('po_number')]; ?></p>
								<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
								<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
							</td>
							<td width="110" align="right">
								<?php echo $po_qnty_in_pcs; ?>
								<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_qnty_in_pcs; ?>">
							</td>
							<td width="140" align="center">
								<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $qnty; ?>">
							</td>
                            <td align="center">
                                <input type="text" name="txtReturnQnty[]" id="txtReturnQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $returnQnty; ?>">
                            </td>					
						</tr>
					<?php 
					$i++; 
					} 
					?>
					<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
				</table>
			</div>
			<table width="620" id="table_id">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
		</div>
	<?php
	}	
	?>
		</fieldset>
	</form>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="create_po_search_list_view")
{
	$data = explode("_",$data);
	$search_string=trim($data[0]);
	$search_by=$data[1];
	$search_con="";
	if($search_by==1 && $search_string!="")
		$search_con = " and b.po_number like '%$search_string%'";
	else if($search_by==2 && $search_string!="")
		$search_con =" and a.job_no like '%$search_string%'"; 
		
	$company_id =$data[2];
	$buyer_id =$data[3];	
	$all_po_id=$data[4];
	$receiveBasis=$data[5];
	
	if($all_po_id!="")
		$po_id_cond=" or b.id in($all_po_id)";
	else 
		$po_id_cond="";
	
	$hidden_po_id=explode(",",$all_po_id);
	if($buyer_id==0) { echo "<b>Please Select Buyer First</b>"; die; }
	
	if($receiveBasis==1)
	{
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date 
		from wo_po_details_master a, wo_po_break_down b, wo_booking_dtls c 
		where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and a.company_name=$company_id and a.buyer_name=$buyer_id $search_con and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date, a.job_no, a.style_ref_no, a.order_uom";
	}
	else
	{
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date 
		from wo_po_details_master a, wo_po_break_down b
		where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name=$buyer_id $search_con and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date, a.job_no, a.style_ref_no, a.order_uom"; 
	}
	//echo $sql;die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Job No</th>
                <th width="110">Style No</th>
                <th width="110">PO No</th>
                <th width="90">PO Quantity</th>
                <th width="50">UOM</th>
                <th>Shipment Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:220px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1; $po_row_id='';
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					if(in_array($selectResult[csf('id')],$hidden_po_id)) 
					{
						if($po_row_id=="") $po_row_id=$i; else $po_row_id.=",".$i;
					}
					
					$po_qnty_in_pcs=$selectResult[csf('po_quantity')]*$selectResult[csf('total_set_qnty')];
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
                            <td width="40" align="center"><?php echo "$i"; ?>
                             <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>	
                            </td>	
                            <td width="100"><p><?php echo $selectResult[csf('job_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                            <td width="90" align="right"><?php echo $po_qnty_in_pcs; ?></td> 
                            <td width="50" align="center"><p><?php echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                            <td align="center"><?php echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                        </tr>
                    <?php
                    $i++;
				}
			?>
				<input type="hidden" name="txt_po_row_id" id="txt_po_row_id" value="<?php echo $po_row_id; ?>"/>
            </table>
        </div>
         <table width="620" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                            <input type="button" name="close" onClick="show_grey_prod_recv();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>           
<?php
exit();
}

//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	 //echo $cbo_ready_to_approved;die;
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 	 
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		//---------------Check Duplicate product in Same return number ------------------------//
		$requigitionCond="";
		if( str_replace("'","",$cbo_basis)==3) $requigitionCond=" and a.buyer_id=".$cbo_buyer_name." and b.requisition_no=".$txt_req_no."";
		
		if( str_replace("'","",$cbo_issue_purpose)!=2) 
		{
			$duplicate = is_duplicate_field("b.id","inv_issue_master a, inv_transaction b","a.id=b.mst_id and a.issue_number=$txt_system_no and b.prod_id=$txt_prod_id and b.transaction_type=2 $requigitionCond"); 
			
			if( $duplicate==1 && str_replace("'","",$txt_system_no)!="") 
			{
				//check_table_status( $_SESSION['menu_id'],0);
				echo "20**Duplicate Product is Not Allow in Same Return Number.";
				die;
			}
		}
		//------------------------------Check Brand END---------------------------------------//
				
		//yarn issue master table entry here START---------------------------------------//		
 		if( str_replace("'","",$txt_system_no) == "" ) //new insert cbo_ready_to_approved
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$id=return_next_id("id", "inv_issue_master", 1);		
			$new_mrr_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'YIS', date("Y",time()), 5, "select issue_number_prefix,issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_name and entry_form=3 and $year_cond=".date('Y',time())." order by id DESC", "issue_number_prefix", "issue_number_prefix_num" ));
			
			$field_array_mst="id,issue_number_prefix, issue_number_prefix_num, issue_number, issue_basis, issue_purpose, entry_form, item_category, company_id, location_id, supplier_id, store_id, buyer_id, buyer_job_no, style_ref, booking_id, booking_no, issue_date, sample_type, knit_dye_source, knit_dye_company, challan_no, other_party, remarks,ready_to_approve, inserted_by, insert_date";
			$data_array_mst="(".$id.",'".$new_mrr_number[1]."','".$new_mrr_number[2]."','".$new_mrr_number[0]."',".$cbo_basis.",".$cbo_issue_purpose.",3,1,".$cbo_company_name.",".$cbo_location.",".$cbo_supplier.",".$cbo_store_name.",".$cbo_buyer_name.",".$txt_buyer_job_no.",".$txt_style_ref.",".$txt_booking_id.",".$txt_booking_no.",".$txt_issue_date.",".$cbo_sample_type.",".$cbo_knitting_source.",".$cbo_knitting_company.",".$txt_challan_no.",".$cbo_other_party.",".$txt_remarks.",".$cbo_ready_to_approved.",'".$user_id."','".$pc_date_time."')";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_insert("inv_issue_master",$field_array_mst,$data_array_mst,0); 
 		}
		else //update
		{
			$new_mrr_number[0]=str_replace("'","",$txt_system_no);
			$id=return_field_value("id","inv_issue_master","issue_number=$txt_system_no");
			$field_array_mst="issue_basis*issue_purpose*entry_form*item_category*company_id*location_id*supplier_id*store_id*buyer_id*buyer_job_no*style_ref*booking_id*booking_no*issue_date*sample_type*knit_dye_source*knit_dye_company*challan_no*other_party*remarks*ready_to_approve*updated_by*update_date";
			$data_array_mst="".$cbo_basis."*".$cbo_issue_purpose."*3*1*".$cbo_company_name."*".$cbo_location."*".$cbo_supplier."*".$cbo_store_name."*".$cbo_buyer_name."*".$txt_buyer_job_no."*".$txt_style_ref."*".$txt_booking_id."*".$txt_booking_no."*".$txt_issue_date."*".$cbo_sample_type."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$txt_challan_no."*".$cbo_other_party."*".$txt_remarks."*".$cbo_ready_to_approved."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"id",$id,0);
 		}
		//yarn issue master table entry here END---------------------------------------//
		//product master table information
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value,allocated_qnty,available_qnty from product_details_master where id=$txt_prod_id and item_category_id=1");
		$avg_rate=$stock_qnty=$stock_value=$allocated_qnty=$available_qnty=0;
		foreach($sql as $result)
		{
			$avg_rate = $result[csf("avg_rate_per_unit")];
			$stock_qnty = $result[csf("current_stock")];
			$stock_value = $result[csf("stock_value")];
			$allocated_qnty = $result[csf("allocated_qnty")];
			$available_qnty = $result[csf("available_qnty")];
		}
		//inventory TRANSACTION table data entry START----------------------------------------------------------//	
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$txt_issue_qnty = str_replace("'","",$txt_issue_qnty);
 		$issue_stock_value = $avg_rate*$txt_issue_qnty;		
		$transactionID = return_next_id("id", "inv_transaction", 1); 				
		$field_array_trans= "id,mst_id,requisition_no,receive_basis,company_id,supplier_id,prod_id,dyeing_color_id,item_category,transaction_type,transaction_date,store_id,brand_id,cons_uom,cons_quantity,return_qnty,cons_rate,cons_amount,no_of_bags,cone_per_bag,weight_per_bag,weight_per_cone,room,rack,self,bin_box,job_no,inserted_by,insert_date";
 		$data_array_trans= "(".$transactionID.",".$id.",".$txt_req_no.",".$cbo_basis.",".$cbo_company_name.",".$cbo_supplier.",".$txt_prod_id.",".$cbo_dyeing_color.",1,2,".$txt_issue_date.",".$cbo_store_name.",".$cbo_brand.",".$cbo_uom.",".$txt_issue_qnty.",".$txt_returnable_qty.",".$avg_rate.",".$issue_stock_value.",".$txt_no_bag.",".$txt_no_cone.",".$txt_weight_per_bag.",".$txt_weight_per_cone.",".$cbo_room.",".$cbo_rack.",".$cbo_self.",".$cbo_binbox.",".$job_no.",'".$user_id."','".$pc_date_time."')"; 
		//echo $field_array."<br>".$data_array;die;
		//$transID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
		
		//inventory TRANSACTION table data entry  END----------------------------------------------------------//
		//if LIFO/FIFO then START -----------------------------------------//
		$field_array = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
		$data_array="";
		$updateID_array=array();
		$update_data=array();
		$issueQnty = $txt_issue_qnty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);
				
		/*$returnString=return_field_value("concat(store_method,'_',allocation)","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17 and item_category_id=1 and status_active=1 and is_deleted=0");
		$expString = explode("_",$returnString); 
		$isLIFOfifo = $expString[0];
		$check_allocation = $expString[1];*/
		
		$isLIFOfifo=''; $check_allocation='';
		$sql_variable=sql_select("select store_method,allocation,variable_list from variable_settings_inventory where company_name=$cbo_company_name and variable_list in(17,18) and item_category_id=1 and status_active=1 and is_deleted=0");		
		foreach($sql_variable as $row)
		{
			if($row[csf('variable_list')]==17)
			{
				$isLIFOfifo=$row[csf('store_method')];
			}
			else if($row[csf('variable_list')]==18)
			{
				$check_allocation=$row[csf('allocation')];
			}
		}
		
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC"; 
		
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in (1,4,5) and item_category=1 order by transaction_date $cond_lifofifo");			
		foreach($sql as $result)
		{				
			$recv_trans_id = $result[csf("id")]; // this row will be updated
			$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];
			$cons_rate = $result[csf("cons_rate")];
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$recv_trans_id.",".$transactionID.",3,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$recv_trans_id; 
				$update_data[$recv_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				//$issueQntyBalance = $balance_qnty+$issueQntyBalance; // adjust issue qnty
				//$issueQntyBalance = $issueQntyBalance-$balance_qnty;
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$recv_trans_id.",".$transactionID.",3,".$txt_prod_id.",".$balance_qnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$recv_trans_id; 
				$update_data[$recv_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
		}//end foreach
 		// LIFO/FIFO then END-----------------------------------------------//
		
		//mrr wise issue data insert here----------------------------//
		$mrrWiseIssueID=true; $upTrID=true;
		
		//weighted and average rate START here------------------------//
		//product master table data UPDATE START----------------------//
  		$currentStock   = $stock_qnty-$txt_issue_qnty;
		$StockValue	 = $stock_value-($txt_issue_qnty*$avg_rate);
		//$avgRate	 	= number_format($StockValue/$currentStock,$dec_place[3],'.','');
		$avgRate	 	= number_format($avg_rate,'.','');
		//newly added code here =============================================
		//item allocation----------------------------------------------------
		
		$availableChk = $stock_qnty>=$txt_issue_qnty ?  true : false;
		$msg="Issue Quantity is exceed the current Stock Quantity";
		
		if($availableChk==false)
		{
			//check_table_status( $_SESSION['menu_id'],0);
			
			if($db_type==0)
			{
				mysql_query("ROLLBACK"); 
			}
			else
			{
				oci_rollback($con);
			}
			echo "11**".$msg;
			exit();
		}
		
		$allocated_qnty_balance=0;
		$available_qnty_balance=0;
		if(str_replace("'","",$cbo_issue_purpose)==1 || str_replace("'","",$cbo_issue_purpose)==2)
		{
			$allocated_qnty_balance = $allocated_qnty-$txt_issue_qnty;
			$available_qnty_balance = $available_qnty;
		}
		else  
		{
			$allocated_qnty_balance = $allocated_qnty;
			$available_qnty_balance = $available_qnty-$txt_issue_qnty;
		}
		
		$field_array_prod = "last_issued_qnty*current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";//*avg_rate_per_unit*
		$data_array_prod	= "".$txt_issue_qnty."*".$currentStock."*".number_format($StockValue,$dec_place[4],'.','')."*".$allocated_qnty_balance."*".$available_qnty_balance."*'".$user_id."'*'".$pc_date_time."'";//*".$allocated_qnty_balance."*".$available_qnty_balance."$avgRate."*".
		//$prodUpdate 	= sql_update("product_details_master",$field_array_prod,$data_array_prod,"id",$txt_prod_id,0);
		
		//------------------ product_details_master END--------------//
		//weighted and average rate END here-------------------------// 
		
		$proportQ=true; 
		$data_array_prop="";
		$save_string=explode(",",str_replace("'","",$save_data));
		if(count($save_string)>0 && str_replace("'","",$save_data)!="")  
		{		 
 			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,po_breakdown_id,prod_id,quantity,issue_purpose,returnable_qnty,inserted_by,insert_date";
 			$po_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				$returnable_qnty=$order_dtls[2];
				
				if( array_key_exists($order_id,$po_array) )
				{
					$po_array[$order_id]+=$order_qnty;
					$po_rt_array[$order_id]+=$returnable_qnty;
				}
				else
				{
					$po_array[$order_id]=$order_qnty;
					$po_rt_array[$order_id]=$returnable_qnty;
				}
			}
			$i=0; 
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$returnable_qnty=$po_rt_array[$key];
				$data_array_prop.="(".$id_proport.",".$transactionID.",2,3,".$order_id.",".$txt_prod_id.",".$order_qnty.",".$cbo_issue_purpose.",'".$returnable_qnty."',".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			/*if($data_array_prop!="")
			{
				//echo "10**INSERT INTO order_wise_pro_details (".$field_array_proportionate.") VALUES ".$data_array_prop.""; die;
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
			}*/
		}//end if
		//order_wise_pro_details table data insert END -----//

		//echo "20**".$rID." && ".$transID." && ".$prodUpdate." && ".$proportQ; 
		//mysql_query("ROLLBACK");die; 
 		 
		if( str_replace("'","",$txt_system_no) == "" )
		{
			 $rID=sql_insert("inv_issue_master",$field_array_mst,$data_array_mst,0); 
		}
		else
		{
			$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"id",$id,0); 
		}
		
		$transID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
		if($data_array!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array,$data_array,0);
		}
		
		//transaction table stock update here------------------------//
		if(count($updateID_array)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array));
		}		

		$prodUpdate= sql_update("product_details_master",$field_array_prod,$data_array_prod,"id",$txt_prod_id,0);
		
		if($data_array_prop!="")
		{
			//echo "10**INSERT INTO order_wise_pro_details (".$field_array_proportionate.") VALUES ".$data_array_prop.""; die;
			$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
		}
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{		
			if($rID && $transID && $prodUpdate && $proportQ && $upTrID)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_mrr_number[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_mrr_number[0];
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $transID && $prodUpdate && $proportQ && $upTrID)
			{
				oci_commit($con);   
				echo "0**".$new_mrr_number[0];
			}
			else
			{
				oci_rollback($con);
				echo "10**0";
			}
		}
		disconnect($con);
		die;
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		//check update id
		if( str_replace("'","",$update_id) == "" ||  str_replace("'","",$txt_system_no)=="" )
		{
			echo "15";
			exit(); 
		}
		//variable_list=17 is_allocated,  item_category_id=1 is yarn--------------------
		/*$returnString=return_field_value("concat(store_method,'_',allocation)","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17 and item_category_id=1 and status_active=1 and is_deleted=0");
		$expString = explode("_",$returnString); 
		$isLIFOfifo = $expString[0];
		$check_allocation = $expString[1];*/
		
		$isLIFOfifo=''; $check_allocation='';
		$sql_variable=sql_select("select store_method,allocation,variable_list from variable_settings_inventory where company_name=$cbo_company_name and variable_list in(17,18) and item_category_id=1 and status_active=1 and is_deleted=0");		
		foreach($sql_variable as $row)
		{
			if($row[csf('variable_list')]==17)
			{
				$isLIFOfifo=$row[csf('store_method')];
			}
			else if($row[csf('variable_list')]==18)
			{
				$check_allocation=$row[csf('allocation')];
			}
		}
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
		//product master table information
		//before stock update
		$sql = sql_select( "select a.id,a.avg_rate_per_unit,a.current_stock,a.stock_value, b.cons_quantity, b.cons_amount,a.allocated_qnty,a.available_qnty from product_details_master a, inv_transaction b where a.id=b.prod_id and b.id=$update_id and a.item_category_id=1 and b.item_category=1 and b.transaction_type=2" );
		$before_prod_id=$before_issue_qnty=$before_stock_qnty=$before_stock_value=0;
		foreach($sql as $result)
		{
			$before_prod_id 	= $result[csf("id")];
 			$before_stock_qnty = $result[csf("current_stock")];
			$before_stock_value = $result[csf("stock_value")];
			//before quantity and stock value
			$before_issue_qnty = $result[csf("cons_quantity")];
			$before_issue_value = $result[csf("cons_amount")];
			$before_allocated_qnty = $result[csf("allocated_qnty")];
			$before_available_qnty	=$result[csf("available_qnty")];	
		}
		//current product ID
		$txt_prod_id = str_replace("'","",$txt_prod_id);
		$txt_issue_qnty = str_replace("'","",$txt_issue_qnty);
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value,allocated_qnty,available_qnty from product_details_master where id=$txt_prod_id and item_category_id=1");
		$curr_avg_rate=$curr_stock_qnty=$curr_stock_value=$allocated_qnty=$available_qnty=0;
		foreach($sql as $result)
		{
			$curr_avg_rate 	   = $result[csf("avg_rate_per_unit")];
			$curr_stock_qnty 	 = $result[csf("current_stock")];
			$curr_stock_value 	= $result[csf("stock_value")];
			$allocated_qnty	  = $result[csf("allocated_qnty")];
			$available_qnty	  = $result[csf("available_qnty")];
		}
		//weighted and average rate START here------------------------//
		//product master table data UPDATE START----------------------//		
		$update_array_prod	= "last_issued_qnty*current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";
		if($before_prod_id==$txt_prod_id)
		{
			$adj_stock_qnty = $curr_stock_qnty+$before_issue_qnty-$txt_issue_qnty; // CurrentStock + Before Issue Qnty - Current Issue Qnty
			//item allocation----------------------------------------------------
			
			$availableChk = $curr_stock_qnty+$before_issue_qnty>=$txt_issue_qnty ?  true : false;
			$msg="Issue Quantity is exceed the current Stock Quantity";
			
			if($availableChk==false)
			{
				//check_table_status( $_SESSION['menu_id'],0);
				if($db_type==0) {mysql_query("ROLLBACK");}
				else {oci_rollback($con);} 
				echo "11**".$msg;
				exit();
			}
			
			$allocated_qnty_balance=0;
			$available_qnty_balance=0;
			if(str_replace("'","",$cbo_issue_purpose)==1 || str_replace("'","",$cbo_issue_purpose)==2)
			{
				$allocated_qnty_balance = $allocated_qnty+$before_issue_qnty-$txt_issue_qnty;
				$available_qnty_balance = $available_qnty;
			}
			else  
			{
				$allocated_qnty_balance = $allocated_qnty;
				$available_qnty_balance = $available_qnty+$before_issue_qnty-$txt_issue_qnty;
			}
			
			$adj_stock_val  = $curr_stock_value+$before_issue_value-($txt_issue_qnty*$curr_avg_rate); // CurrentStockValue + Before Issue Value - Current Issue Value
			//$adj_avgrate	= number_format($adj_stock_val/$adj_stock_qnty,$dec_place[3],'.','');
			$adj_avgrate	= number_format($curr_avg_rate,$dec_place[3],'.','');
			 
			$data_array_prod	= "".$txt_issue_qnty."*".$adj_stock_qnty."*".number_format($adj_stock_val,$dec_place[4],'.','')."*".$allocated_qnty_balance."*".$available_qnty_balance."*'".$user_id."'*'".$pc_date_time."'"; //*".$allocated_qnty_balance."*".$available_qnty_balance."$adj_avgrate."*".
 			//$query1 		= sql_update("product_details_master",$update_array_prod,$data_array_prod,"id",$before_prod_id,0);
			
			//now current stock
			$curr_avg_rate 		= $adj_avgrate;
			$curr_stock_qnty 	= $adj_stock_qnty;
			$curr_stock_value 	= $adj_stock_val;
		}
		else
		{
			$updateID_array_prod = $update_data_prod = array();
			//before product adjust
			$adj_before_stock_qnty 	= $before_stock_qnty+$before_issue_qnty; // CurrentStock + Before Issue Qnty
			//$before_allocated_qnty	= $before_allocated_qnty+$before_issue_qnty;
			//$before_available_qnty	= $adj_before_stock_qnty-$before_allocated_qnty;			
			$allocated_qnty_balance=0; $available_qnty_balance=0;
			if(str_replace("'","",$cbo_issue_purpose)==1 || str_replace("'","",$cbo_issue_purpose)==2)
			{
				$before_allocated_qnty = $before_allocated_qnty+$before_issue_qnty;
				$before_available_qnty = $before_available_qnty;
				
				$allocated_qnty_balance = $allocated_qnty-$txt_issue_qnty;
				$available_qnty_balance = $available_qnty;
			}
			else  
			{
				$before_allocated_qnty = $before_allocated_qnty;
				$before_available_qnty = $before_available_qnty+$before_issue_qnty;
				
				$allocated_qnty_balance = $allocated_qnty;
				$available_qnty_balance = $available_qnty-$txt_issue_qnty;
			}
			
			$adj_before_stock_val  	= $before_stock_value+$before_issue_value; // CurrentStockValue + Before Issue Value
			$adj_before_avgrate	   = number_format($adj_before_stock_val/$adj_before_stock_qnty,$dec_place[3],'.','');
			 
			$updateID_array_prod[]=$before_prod_id;
			$update_data_prod[$before_prod_id]=explode("*",("".$txt_issue_qnty."*".$adj_before_stock_qnty."*".number_format($adj_before_stock_val,$dec_place[4],'.','')."*".$before_allocated_qnty."*".$before_available_qnty."*'".$user_id."'*'".$pc_date_time."'"));//$adj_before_avgrate."*".
  			
			//current product adjust
			$adj_curr_stock_qnty  = $curr_stock_qnty-$txt_issue_qnty; // CurrentStock + Before Issue Qnty
			$adj_curr_stock_val   = $curr_stock_value-($txt_issue_qnty*$curr_avg_rate); // CurrentStockValue + Before Issue Value
			//$adj_curr_avgrate	 = number_format($adj_curr_stock_val/$adj_curr_stock_qnty,$dec_place[3],'.','');
			$adj_curr_avgrate	 = number_format($curr_avg_rate,$dec_place[3],'.','');
			//for current product-------------
			//item allocation----------------------------------------------------
			
			$availableChk = $curr_stock_qnty>=$txt_issue_qnty ?  true : false;
			$msg="Issue Quantity is exceed the current Stock Quantity";
			
			if($availableChk==false)
			{
				//check_table_status( $_SESSION['menu_id'],0);
				if($db_type==0) {mysql_query("ROLLBACK");}
				else {oci_rollback($con);}
				echo "11**".$msg;
				exit();
			}
			
			$updateID_array_prod[]=$txt_prod_id;
			$update_data_prod[$txt_prod_id]=explode("*",("".$txt_issue_qnty."*".$adj_curr_stock_qnty."*".number_format($adj_curr_stock_val,$dec_place[4],'.','')."*".$allocated_qnty_balance."*".$available_qnty_balance."*'".$user_id."'*'".$pc_date_time."'"));//.$adj_curr_avgrate."*"
			
			//$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array_prod,$update_data_prod,$updateID_array_prod));
			
			//now current stock
			$curr_avg_rate 		= $adj_curr_avgrate;
			$curr_stock_qnty 	= $adj_curr_stock_qnty;
			$curr_stock_value 	= $adj_curr_stock_val;
		}
  		//------------------ product_details_master END--------------//
		//weighted and average rate END here-------------------------//
 		//transaction table START--------------------------//
		$trans_data_array=array();
		$update_array_trans = "balance_qnty*balance_amount*updated_by*update_date";
		$sql = sql_select("select a.id,a.balance_qnty,a.balance_amount,b.issue_qnty,b.rate,b.amount from inv_transaction a, inv_mrr_wise_issue_details b where a.id=b.recv_trans_id and b.issue_trans_id=$update_id and b.entry_form=3 and a.item_category=1"); 
		$updateID_array_trans = array();
		$update_data_trans = array();
		foreach($sql as $result)
		{
			$adjBalance = $result[csf("balance_qnty")]+$result[csf("issue_qnty")];
			$adjAmount = $result[csf("balance_amount")]+$result[csf("amount")];
			$updateID_array_trans[]=$result[csf("id")]; 
			$update_data_trans[$result[csf("id")]]=explode("*",("".$adjBalance."*".$adjAmount."*'".$user_id."'*'".$pc_date_time."'"));
			
			$trans_data_array[$result[csf("id")]]['qnty']=$adjBalance;
			$trans_data_array[$result[csf("id")]]['amnt']=$adjAmount;
		}
		
		$query2=true; $query3=true;
		/*if(count($updateID_array_trans)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_trans,$update_data_trans,$updateID_array_trans));
		}
		
		//transaction table END----------------------------//
		//LIFO/FIFO  START here------------------------//
		
		if(count($updateID_array_trans)>0)
		{
			 $updateIDArray = implode(",",$updateID_array);
			 $query3 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=3");
		}*/
		//PROPORTIONATE table here-------------------------// 
 		//$query4 = execute_query("DELETE FROM  order_wise_pro_details WHERE trans_id=$update_id and entry_form=3");
		//****************************************** BEFORE ENTRY ADJUST END *****************************************//
		//############## SAVE POINT START  ###################
		if($db_type==0)
		{
			$savepoint="updatesql";
			mysql_query("SAVEPOINT $savepoint");
		}
		//############## SAVE POINT END    ################### 
		//****************************************** NEW ENTRY START *****************************************//
		//issue master update START--------------------------------------//
		$field_array_mst="issue_basis*issue_purpose*entry_form*item_category*company_id*location_id*supplier_id*store_id*buyer_id*buyer_job_no*style_ref*booking_id*booking_no*issue_date*sample_type*knit_dye_source*knit_dye_company*challan_no*other_party*remarks*ready_to_approve*updated_by*update_date";
		$data_array_mst="".$cbo_basis."*".$cbo_issue_purpose."*3*1*".$cbo_company_name."*".$cbo_location."*".$cbo_supplier."*".$cbo_store_name."*".$cbo_buyer_name."*".$txt_buyer_job_no."*".$txt_style_ref."*".$txt_booking_id."*".$txt_booking_no."*".$txt_issue_date."*".$cbo_sample_type."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$txt_challan_no."*".$cbo_other_party."*".$txt_remarks."*".$cbo_ready_to_approved."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;."-".;
		//$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"issue_number",$txt_system_no,0);
		
		//issue master update END---------------------------------------// 
		//inventory TRANSACTION table data UPDATE START----------------------------------------------------------//	
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$txt_issue_qnty = str_replace("'","",$txt_issue_qnty);
		$avg_rate = $curr_avg_rate; // asign current rate
 		$issue_stock_value = $avg_rate*$txt_issue_qnty;
		$field_array_trans= "requisition_no*receive_basis*company_id*supplier_id*prod_id*dyeing_color_id*item_category*transaction_type*transaction_date*store_id*brand_id*cons_uom*cons_quantity*return_qnty*cons_rate*cons_amount*no_of_bags*cone_per_bag*weight_per_bag*weight_per_cone*room*rack*self*bin_box*job_no*updated_by*update_date";
 		$data_array_trans= "".$txt_req_no."*".$cbo_basis."*".$cbo_company_name."*".$cbo_supplier."*".$txt_prod_id."*".$cbo_dyeing_color."*1*2*".$txt_issue_date."*".$cbo_store_name."*".$cbo_brand."*".$cbo_uom."*".$txt_issue_qnty."*".$txt_returnable_qty."*".$avg_rate."*".$issue_stock_value."*".$txt_no_bag."*".$txt_no_cone."*".$txt_weight_per_bag."*".$txt_weight_per_cone."*".$cbo_room."*".$cbo_rack."*".$cbo_self."*".$cbo_binbox."*".$job_no."*'".$user_id."'*'".$pc_date_time."'"; 
		//echo $field_array."<br>".$data_array;."-".;
		//$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
 		//inventory TRANSACTION table data UPDATE  END----------------------------------------------------------//
		//if LIFO/FIFO then START -----------------------------------------//
		$field_array = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
		$data_array="";
		$updateID_array=array();
		$update_data=array();
		$issueQnty = $txt_issue_qnty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);
		
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC";		 
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in (1,4,5) and item_category=1 order by transaction_date $cond_lifofifo");			
 		foreach($sql as $result)
		{				
			$issue_trans_id = $result[csf("id")]; // this row will be updated
			if($trans_data_array[$issue_trans_id]['qnty']=="")
			{
				$balance_qnty = $result[csf("balance_qnty")];
				$balance_amount = $result[csf("balance_amount")];
			}
			else
			{
				$balance_qnty = $trans_data_array[$issue_trans_id]['qnty'];
				$balance_amount = $trans_data_array[$issue_trans_id]['amnt'];
			}
			$cons_rate = $result[csf("cons_rate")]; 
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",3,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",3,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//echo "20**".$data_array;die;
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
		}//end foreach
 		// LIFO/FIFO then END-----------------------------------------------//
		//echo "20**".$data_array;mysql_query("ROLLBACK");die;
		//mrr wise issue data insert here----------------------------//
		$mrrWiseIssueID=true; $upTrID=true;
		
		//----------order_wise_pro_details table data insert Start --------------------------------// 
		$proportQ=true; 
		$save_string=explode(",",str_replace("'","",$save_data));
		if(count($save_string)>0 && str_replace("'","",$save_data)!="")  
		{
			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,po_breakdown_id,prod_id,quantity,issue_purpose,returnable_qnty,inserted_by,insert_date";
 			$po_array=array(); $po_rt_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				$returnable_qnty=$order_dtls[2];
				
				if( array_key_exists($order_id,$po_array) )
				{
					$po_array[$order_id]+=$order_qnty;
					$po_rt_array[$order_id]+=$returnable_qnty;
				}
				else
				{
					$po_array[$order_id]=$order_qnty;
					$po_rt_array[$order_id]=$returnable_qnty;
				}
			}
			$i=0;
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$returnable_qnty=$po_rt_array[$key];
				$data_array_prop.="(".$id_proport.",".$update_id.",2,3,".$order_id.",".$txt_prod_id.",".$order_qnty.",".$cbo_issue_purpose.",'".$returnable_qnty."',".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			/*if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);	
				
			}*/
		} 
		//----------order_wise_pro_details table data insert END --------------------------------//
		//****************************************** NEW ENTRY END *****************************************//
		//echo "10**"."insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;
		
		if($before_prod_id==$txt_prod_id)
		{
			$query1= sql_update("product_details_master",$update_array_prod,$data_array_prod,"id",$before_prod_id,0);
		}
		else
		{
			$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array_prod,$update_data_prod,$updateID_array_prod));
		}
		
		if(count($updateID_array_trans)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_trans,$update_data_trans,$updateID_array_trans));
			$query3=execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=3");
		}
		
		$query4 = execute_query("DELETE FROM order_wise_pro_details WHERE trans_id=$update_id and entry_form=3");
		$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"issue_number",$txt_system_no,0);
		$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
		
		if($data_array!="") 
		{	 
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array,$data_array,0);
		} 
		if(count($updateID_array)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array));
		}		
	
		if($data_array_prop!="")
		{
			$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);	
		}
		//mysql_query("ROLLBACK");
		/*echo "10**".$transID;
		die;*/
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($query1 && $query2 && $query3 && $query4 && $rID && $transID && $upTrID && $mrrWiseIssueID && $proportQ)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_system_no);
			}
			else
			{
				mysql_query("ROLLBACK");
				mysql_query("ROLLBACK TO $savepoint"); 
				echo "10**".str_replace("'","",$txt_system_no);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($query1 && $query2 && $query3 && $query4 && $rID && $transID && $upTrID && $mrrWiseIssueID && $proportQ)
			{
				oci_commit($con);    
				echo "1**".str_replace("'","",$txt_system_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_system_no);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Not Used Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//check update id
		if( str_replace("'","",$update_id) == "" ||  str_replace("'","",$txt_system_no)=="" )
		{
			echo "15";exit(); 
		}
		/*
		//master table delete here---------------------------------------
  		$rID = sql_update("inv_issue_master",'status_active*is_deleted','0*1',"issue_number",$txt_system_no,1);
		$dtlsrID = sql_update("inv_transaction",'status_active*is_deleted','0*1',"id",$update_id,1);
		$dtlsrID = sql_update("wo_non_order_info_dtls",'status_active*is_deleted','0*1',"mst_id",$mst_id,1);
		$dtlsrID = sql_update("wo_non_order_info_dtls",'status_active*is_deleted','0*1',"mst_id",$mst_id,1);
		*/
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "2**".str_replace("'","",$txt_system_no);
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**".str_replace("'","",$txt_system_no);
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "2**".str_replace("'","",$txt_system_no);
		}
		disconnect($con);
		die;
	}		
}

if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
<script>
	function js_set_value(sys_number)
	{
 		$("#hidden_sys_number").val(sys_number); // mrr number
		parent.emailwindow.hide();
	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter Issue Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
 							 echo create_drop_down( "cbo_supplier", 150, "select c.supplier_name,c.id from lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and b.party_type=2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td> 
                        <?php  
                            $search_by = array(1=>'Issue No',2=>'Challan No',3=>'In House',4=>'Out Bound Subcontact',5=>'Job No',6=>'Wo No',7=>'Buyer');
							$dd="change_search_event(this.value, '0*0*1*1*0*0*1', '0*0*select id,company_name from lib_company where status_active=1 and is_deleted=0 order by company_name*select c.supplier_name,c.id from lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and b.party_type=2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name*0*0*select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$company $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name', '../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'yarn_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_sys_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common =trim($ex_data[2]);
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
 	$sql_cond="";
	
	if($fromDate!="" && $toDate!="")
	{
		if($db_type==0)
		{
			$sql_cond .= " and issue_date between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and issue_date between '".change_date_format($fromDate,'','',1)."' and '".change_date_format($toDate,'','',1)."'";	
		}
	}
	
	if($supplier!="" && $supplier*1!=0) $sql_cond .= " and supplier_id='$supplier'";
	if($company!="" && $company*1!=0) $sql_cond .= " and company_id='$company'";
	
 	$supplier_arr = return_library_array("select id, supplier_name from lib_supplier",'id','supplier_name');
	$company_arr = return_library_array("select id, company_name from lib_company",'id','company_name');
	$buyer_arr = return_library_array("select id, buyer_name from lib_buyer",'id','buyer_name');
	$store_arr = return_library_array("select id, store_name from lib_store_location",'id','store_name');
	$issuQnty_arr = return_library_array("select mst_id, sum(cons_quantity) as qnty from inv_transaction where transaction_type=2 and item_category=1 and status_active=1 and is_deleted=0 group by mst_id",'mst_id','qnty');
	
 	if($txt_search_common!="" || $txt_search_common!=0)
	{
		if($txt_search_by==1)
		{	
			$sql_cond .= " and issue_number like '%$txt_search_common%'";			
		}
		else if($txt_search_by==2)
		{		
			$sql_cond .= " and challan_no like '%$txt_search_common%'";	
 		}
		else if($txt_search_by==3)
		{
			$sql_cond .= " and knit_dye_source=1 and knit_dye_company='$txt_search_common'";
		}
		else if($txt_search_by==4)
		{
			$sql_cond .= " and knit_dye_source=2 and knit_dye_company='$txt_search_common'";
		}
		else if($txt_search_by==5)
		{
			$sql_cond .= " and buyer_job_no like '%$txt_search_common%'";	
		}
		else if($txt_search_by==6)
		{
			$sql_cond .= " and booking_no like '%$txt_search_common%'";	
		}
		else if($txt_search_by==7)
		{
			$sql_cond .= " and buyer_id = '$txt_search_common'";	
		}
	}
	
	if($db_type==0) $year_field="YEAR(insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
		
	$sql = "select id, issue_number_prefix_num, issue_number, issue_basis, issue_purpose, entry_form, item_category, company_id, location_id, supplier_id, store_id, buyer_id, buyer_job_no, style_ref, booking_id, booking_no, issue_date, sample_type, knit_dye_source, knit_dye_company, challan_no, loan_party, lap_dip_no, gate_pass_no, item_color, color_range, remarks, ready_to_approve, other_party, $year_field is_approved
			from inv_issue_master 
			where status_active=1 and entry_form=3 $sql_cond order by issue_number";
	//echo $sql;
	$result = sql_select( $sql );
	?>
    	<div style="margin-top:10px">
            <div style="width:1015px;">
                <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" border="1" rules="all">
                    <thead>
                        <th width="30">SL</th>
                        <th width="70">Issue No</th>
                        <th width="50">Year</th>				
                        <th width="70">Date</th>              
                        <th width="100">Purpose</th>               
                        <th width="70">Challan No</th>
                        <th width="60">Issue Qnty</th>
                        <th width="100">Booking No</th>
                        <th width="100">knitting Comp.</th>
                        <th width="100">Buyer</th>
                        <th width="90">Job No.</th>                        
                        <th width="90">Store</th> 
                        <th>Ready to Approve</th>              
                    </thead>
                </table>
             </div>
            <div style="width:1015px;overflow-y:scroll; min-height:210px; max-height:210px;" id="search_div" >
                <table cellspacing="0" cellpadding="0" width="997" class="rpt_table" id="list_view" border="1" rules="all">
        <?php	
            $i=1;   
            foreach( $result as $row ){
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
					
				//$issuQnty = return_field_value("sum(cons_quantity)","inv_transaction","mst_id=".$row[csf("id")]." and transaction_type=2 and item_category=1 and status_active=1 and is_deleted=0");	
				$issuQnty =$issuQnty_arr[$row[csf("id")]];
        ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer"  onclick="js_set_value('<?php echo $row[csf("issue_number")];?>,<?php echo $row[csf("is_approved")];?>');"> 
                            <td width="30"><?php echo $i; ?></td>	
                            <td width="70"><p><?php echo $row[csf("issue_number_prefix_num")];?></p></td>
                            <td width="50"><p><?php echo $row[csf("year")];?></p></td>              	            			
                            <td width="70"><p><?php echo change_date_format($row[csf("issue_date")]); ?></p></td>								
                            <td width="100"><p><?php echo $yarn_issue_purpose[$row[csf("issue_purpose")]]; ?></p></td>					
                            <td width="70"><p><?php echo $row[csf("challan_no")]; ?></p></td>
                            <td width="60" align="right"><p><?php echo $issuQnty; ?></p></td>
                            <td width="100"><p><?php echo $row[csf("booking_no")]; ?></p></td>
                            <td width="100">
                                <p>
                                    <?php 
                                        if($row[csf("knit_dye_source")]==1) $knit_com=$company_arr[$row[csf("knit_dye_company")]]; 
                                        else $knit_com=$supplier_arr[$row[csf("knit_dye_company")]];
                                        echo $knit_com; 
                                    ?>
                                 </p>
                            </td>
                            <td width="100"><p><?php echo $buyer_arr[$row[csf("buyer_id")]]; ?></p></td>
                            <td width="90"><p><?php echo $row[csf("buyer_job_no")]; ?></p></td>
                            <td width="90"><p><?php echo $store_arr[$row[csf("store_id")]]; ?></p></td>
                            <td><p><?php echo $yes_no[$row[csf("ready_to_approve")]]; //if($row[csf("ready_to_approve")]!=1) else echo ""; ?>&nbsp;</p></td>		
                        </tr>
                        <?php
                        $i++;
                        }
                        ?>
                </table>
            </div>
        </div>
    <?php
	exit();
}

if($action=="populate_data_from_data")
{
	$sql = "select id,issue_number,issue_basis,issue_purpose,entry_form,item_category,company_id,location_id,supplier_id,store_id,buyer_id,buyer_job_no,style_ref,booking_id,booking_no,issue_date,sample_type,knit_dye_source,knit_dye_company,challan_no,loan_party,lap_dip_no,gate_pass_no,item_color,color_range,remarks,ready_to_approve,other_party,is_approved
			from inv_issue_master 
			where issue_number='$data' and entry_form=3";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		
		echo"load_drop_down( 'requires/yarn_issue_controller', ".$row[csf("company_id")].", 'load_drop_down_supplier', 'supplier' );\n";
		echo"load_drop_down( 'requires/yarn_issue_controller', ".$row[csf("company_id")].", 'load_drop_down_location', 'location_td' );\n";
		echo"load_drop_down( 'requires/yarn_issue_controller', ".$row[csf("company_id")].", 'load_drop_down_store', 'store_td' );\n";
 		
		echo "$('#cbo_basis').val(".$row[csf("issue_basis")].");\n";
		echo "$('#cbo_issue_purpose').val(".$row[csf("issue_purpose")].");\n";
		
		echo "active_inactive();\n";
		echo "disable_enable_fields( 'cbo_company_name*cbo_basis', 1, '', '');\n";
		echo "$('#txt_issue_date').val('".change_date_format($row[csf("issue_date")])."');\n";
		echo "$('#txt_booking_id').val(".$row[csf("booking_id")].");\n";
		echo "$('#txt_booking_no').val('".$row[csf("booking_no")]."');\n";
		echo "$('#cbo_location').val(".$row[csf("location_id")].");\n";
		echo "$('#cbo_knitting_source').val(".$row[csf("knit_dye_source")].");\n";
		
		if($row[csf("knit_dye_source")]!=0)
		{
			echo "load_drop_down( 'requires/yarn_issue_controller', ".$row[csf("knit_dye_source")]."+'**'+".$row[csf("company_id")].", 'load_drop_down_knit_com', 'knitting_company_td' );\n";
		}
		
		if($row[csf("issue_purpose")]==2)
		{
			echo "show_list_view('".$row[csf("booking_id")]."','show_yarn_dyeing_list_view','requisition_item','requires/yarn_issue_controller','');\n";
			echo "load_drop_down( 'requires/yarn_issue_controller','".$row[csf("booking_id")]."','load_drop_down_dyeing_color', 'dyeingColor_td' );\n";
		}
		else
		{
			echo "load_drop_down( 'requires/yarn_issue_controller','0','load_drop_down_dyeing_color', 'dyeingColor_td' );\n";
		}
		//echo "load_drop_down( 'requires/yarn_issue_controller', ".$row[csf("knit_dye_source")]."+'**'+".$row[csf("company_id")].", 'load_drop_down_knit_com', 'knitting_company_td' );\n";
		
		echo "$('#cbo_knitting_company').val(".$row[csf("knit_dye_company")].");\n";
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";		
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_other_party').val(".$row[csf("other_party")].");\n";		
		echo "$('#cbo_sample_type').val(".$row[csf("sample_type")].");\n";
		echo "$('#cbo_buyer_name').val(".$row[csf("buyer_id")].");\n";
		echo "$('#txt_style_ref').val('".$row[csf("style_ref")]."');\n";
		echo "$('#txt_buyer_job_no').val('".$row[csf("buyer_job_no")]."');\n";
 		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		echo "$('#cbo_ready_to_approved').val(".$row[csf("ready_to_approve")].");\n";
		//clear child form
		echo "$('#tbl_child').find('select,input').val('');\n";
		if($row[csf("is_approved")]==1)
		{
			 echo "$('#approved').text('Approved');\n"; 
		}
		else
		{
			 echo "$('#approved').text('');\n";
		}
  	}
	exit();	
}

if($action=="show_dtls_list_view")
{
	$ex_data = explode("**",$data);
	$issue_number = $ex_data[0];
 	
	$cond="";
	if($issue_number!="") $cond .= " and a.issue_number='$issue_number'";
	$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
	$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	
	$sql = "select b.requisition_no, a.challan_no, c.yarn_count_id, c.yarn_comp_type1st, c.yarn_comp_percent1st, c.yarn_comp_type2nd, c.yarn_comp_percent2nd, c.yarn_type, c.color, c.lot, b.id, b.store_id, b.cons_uom, b.cons_quantity, b.cons_amount, b.rack, b.self
			from inv_issue_master a, inv_transaction b, product_details_master c 
			where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=2 and b.item_category=1 and a.entry_form=3 $cond";
	$result = sql_select($sql);
	$i=1;
	$total_qnty=0;
	?> 
    	<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" width="970" rules="all">
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Challan No</th>
                    <th>Lot No</th>                    
                    <th>Yarn Count</th>
                    <th>Composition</th>
                    <th>Yarn Type</th>
                    <th>Color</th>
                    <th>Store</th>
                    <th>Issue Qnty</th>
                    <th>UOM</th>
                    <th>Req. No</th>
                    <th>Rack</th>
                    <th>Shelf</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF"; 
					
					$composition_string = $composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')];
					if($row[csf('yarn_comp_type2nd')]!=0) $composition_string.=" ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')];
					
					$total_qnty +=	$row[csf("cons_quantity")];
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>","child_form_input_data","requires/yarn_issue_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="80"><p><?php echo $row[csf("challan_no")]; ?></p></td>
                        <td width="80"><p><?php echo $row[csf("lot")]; ?></p></td>
                        <td width="70"><p><?php echo $yarn_count_arr[$row[csf("yarn_count_id")]]; ?></p></td>
                        <td width="130"><p><?php echo $composition_string; ?></p></td>
                        <td width="80"><p><?php echo $yarn_type[$row[csf("yarn_type")]]; ?></p></td>
                        <td width="80"><p><?php echo $color_name_arr[$row[csf("color")]]; ?></p></td>
                        <td width="100"><p><?php echo $store_arr[$row[csf("store_id")]]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("cons_quantity")]; ?></p></td>
                        <td width="50"><p><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></p></td>
                        <td width="60"><p><?php echo $row[csf("requisition_no")]; ?></p></td>
                        <td width="50"><p><?php echo $row[csf("rack")]; ?></p></td>
                        <td><p><?php echo $row[csf("self")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                            <th colspan="8" align="right">Sum</th>
                            <th><?php echo $total_qnty; ?></th>
                            <th>&nbsp;</th>   
                            <th>&nbsp;</th>
                            <th>&nbsp;</th> 
                            <th>&nbsp;</th>                            
                     </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}


if($action=="child_form_input_data")
{
	$rcv_dtls_id = $data;	
	$sql = "select a.company_id, a.issue_basis, a.issue_purpose, b.requisition_no,b.id,b.store_id,b.cons_uom,b.cons_quantity,b.return_qnty,b.no_of_bags, b.cone_per_bag, b.weight_per_bag, b.weight_per_cone, b.dyeing_color_id,b.room,b.rack,b.self,b.bin_box,b.job_no,c.current_stock, c.allocated_qnty, c.available_qnty, c.yarn_count_id, c.yarn_comp_type1st, c.yarn_comp_percent1st, c.yarn_comp_type2nd, c.yarn_comp_percent2nd, c.yarn_type, c.color, c.brand, c.lot, c.id as prod_id
			from inv_issue_master a, inv_transaction b, product_details_master c  
			where a.id=b.mst_id and b.prod_id=c.id and b.id='$rcv_dtls_id' and b.transaction_type=2 and b.item_category=1"; 
	//echo $sql;
	$result = sql_select($sql);
    
	foreach($result as $row)
	{
		echo "$('#txt_req_no').val(".$row[csf("requisition_no")].");\n";
		if($row[csf("issue_basis")]==3)
		{
			echo "show_list_view('".$row[csf("requisition_no")].",".$row[csf("company_id")]."','show_req_list_view','requisition_item','requires/yarn_issue_controller','');\n";
		}
		echo "$('#txt_lot_no').val('".$row[csf("lot")]."');\n";
   		echo "$('#txt_prod_id').val(".$row[csf("prod_id")].");\n";
		echo "$('#txt_issue_qnty').val(".$row[csf("cons_quantity")].");\n";
		echo "$('#hidden_p_issue_qnty').val(".$row[csf("cons_quantity")].");\n"; 
		echo "$('#txt_returnable_qty').val(".$row[csf("return_qnty")].");\n";
		//$curr_stock = $row[csf("current_stock")]+$row[csf("cons_quantity")];
		
		$stock_qnty=$row[csf("current_stock")];
		
		echo "$('#txt_current_stock').val(".$stock_qnty.");\n";
 		echo "$('#txt_no_bag').val(".$row[csf("no_of_bags")].");\n";
		echo "$('#txt_no_cone').val(".$row[csf("cone_per_bag")].");\n";	
		echo "$('#txt_weight_per_bag').val(".$row[csf("weight_per_bag")].");\n";	
		echo "$('#txt_weight_per_cone').val(".$row[csf("weight_per_cone")].");\n";		
 		echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
		echo "$('#cbo_yarn_count').val(".$row[csf("yarn_count_id")].");\n";
		
		$composition_string = $composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')];
		if($row[csf('yarn_comp_type2nd')]!=0) $composition_string.=" ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')];
					
 		echo "$('#txt_composition').val('".$composition_string."');\n";
		echo "$('#cbo_yarn_type').val('".$row[csf("yarn_type")]."');\n";
 		echo "$('#cbo_color').val(".$row[csf("color")].");\n";
		echo "$('#cbo_brand').val(".$row[csf("brand")].");\n";
		echo "$('#cbo_dyeing_color').val('".$row[csf("dyeing_color_id")]."');\n";
		echo "$('#cbo_uom').val(".$row[csf("cons_uom")].");\n";
		echo "$('#cbo_room').val(".$row[csf("room")].");\n";
		echo "$('#cbo_rack').val(".$row[csf("rack")].");\n";
 		echo "$('#cbo_self').val(".$row[csf("self")].");\n";
		echo "$('#cbo_binbox').val(".$row[csf("bin_box")].");\n";
		echo "$('#job_no').val('".$row[csf("job_no")]."');\n";
  		
		//issue qnty popup data arrange 
		$sqlIN = sql_select("select po_breakdown_id, quantity, returnable_qnty from order_wise_pro_details where trans_id=".$row[csf("id")]." and entry_form=3 and trans_type=2");
		//echo "select po_breakdown_id, quantity, returnable_qnty from order_wise_pro_details where trans_id=".$row[csf("id")]." and entry_form=3 and trans_type=2";
		$poWithValue="";
		$poID="";
		foreach($sqlIN as $res)
		{
			if($poWithValue!="") $poWithValue .=",";
			if($poID!="") $poID .=",";
			$poWithValue .= $res[csf("po_breakdown_id")]."**".$res[csf("quantity")]."**".$res[csf("returnable_qnty")];
			$poID .=$res[csf("po_breakdown_id")];
		}
		echo "$('#save_data').val('".$poWithValue."');\n"; 
		echo "$('#all_po_id').val('".$poID."');\n"; 
		
		//update id here
		echo "$('#update_id').val(".$row[csf("id")].");\n"; 
		echo "set_button_status(1, permission, 'fnc_yarn_issue_entry',1);\n";
		/*echo "$('#tbl_master').find('input,select').attr('disabled', false);\n";
		echo "disable_enable_fields( 'cbo_company_name*cbo_basis', 1, '', '');\n";
		echo "disable_enable_fields( 'cbo_company_name*cbo_basis', 1, '', '');\n";
		if($row[csf("issue_basis")]==2)
		{
			echo "disable_enable_fields( 'txt_booking_no*txt_req_no', 1, '', '');\n";
			echo "disable_enable_fields( 'txt_lot_no', 0, '', '');\n";			
		}
		else if($row[csf("issue_basis")]==3)
		{
			echo "disable_enable_fields( 'txt_booking_no*txt_lot_no*cbo_other_party*cbo_sample_type*cbo_buyer_name', 1, '', '');\n";
			echo "disable_enable_fields( 'txt_req_no', 0, '', '');\n";			
		}
		else
		{
			echo "disable_enable_fields( 'txt_booking_no*txt_lot_no', 0, '', '');\n";
			echo "disable_enable_fields( 'txt_req_no', 1, '', '');\n";
		}*/
		
	}
	exit();
}

//newly addedd--- requisition popup
if($action=="requis_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
	?>
	<script> 
		function fn_show_check()
		{ 
 			show_list_view(document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_search_common').value+'_'+<?php echo $company; ?>, 'create_req_search_list', 'search_divs', 'yarn_issue_controller', 'setFilterGrid(\'list_view\',-1)');
 		}
			
		function js_set_value(req_no)
		{ 
 			$("#hidden_req_no").val(req_no);  
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
	<div align="center" style="width:100%;" >
	<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
		<table width="400" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
				<thead>
					<tr>                	 
						<th width="150">Buyer Name</th>
						<th width="150" align="center">Requisition No</th>
 						<th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php 
							  	echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select --", $selected, "" );
							?>
						</td> 
						<td width="180" align="center">				
							<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
						</td> 
						<td align="center">
							<input type="button" name="btn_show" class="formbutton" value="Show" onClick="fn_show_check()" style="width:100px;" />				
						</td>
					</tr>
						<!-- Hidden field here-------->
						<input type="hidden" id="hidden_req_no" value="" />
 						<!-- ---------END------------->
 				</tbody>
			 </tr>         
			</table>    
			<div align="center" valign="top" id="search_divs"> </div> 
			</form>
	   </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if($action=="create_req_search_list")
{ 
	$ex_data = explode("_",$data); 
	$buyer	 = str_replace("'","",$ex_data[0]);
	$req_no	= str_replace("'","",$ex_data[1]);
 	$company   = str_replace("'","",$ex_data[2]);
	
	$po_array=array();
	$po_sql=sql_select("select a.job_no,a.style_ref_no,b.id,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company");
	foreach($po_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')];
		$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')]; 
	}
	
	if($db_type==0)
	{
		$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company group by dtls_id", "dtls_id", "po_id"  );
	}
	else
	{
		$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company group by dtls_id", "dtls_id", "po_id"  );
	}
	
	$cond="";
	if($buyer!="" && $buyer!=0) $cond .= " and a.buyer_id=$buyer";
	if($req_no!="" && $req_no!=0) $cond .= " and c.requisition_no=$req_no";
	if($company!="" && $company!=0) $cond .= " and a.company_id=$company"; 
	 
	$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
 	
	$sql = "select a.company_id, a.buyer_id, b.knitting_source, b.knitting_party, c.requisition_no, c.prod_id, c.requisition_date, c.knit_id as knit_id, sum(c.yarn_qnty) as yarn_qnty
			from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c 
			where a.id=b.mst_id and b.id=c.knit_id $cond group by a.company_id, a.buyer_id, b.knitting_source, b.knitting_party, c.requisition_no, c.prod_id, c.requisition_date, c.knit_id";
	//echo $sql;
	$result = sql_select($sql);
	$i=1; 
	?> 
    <div align="center">	
        <div style="width:770px;">
            <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" width="100%" rules="all">
                <thead>
                    <tr>
                        <th width="30">SL</th>
                        <th width="110">Buyer</th>
                        <th width="300">Order No</th>
                        <th width="90">Req. No</th>
                        <th width="90">Req. Date</th>
                        <th width="">Quantity</th>
                    </tr>
                </thead>
              </table>  
          </div>
          <div style="width:770px; overflow-y:scroll; max-height:250px;" >
            <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" width="100%" rules="all" id="list_view">	
                <tbody>
					<?php foreach($result as $row){
                        if ($i%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";					
						
						$po_id=array_unique(explode(",",$plan_details_array[$row[csf('knit_id')]]));
						$po_no='';  $style_no=''; $job_no='';
						
						foreach($po_id as $val)
						{
							if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
							if($job_no=='') $job_no=$po_array[$val]['job_no'];
							if($style_no=='') $style_no=$po_array[$val]['style_ref'];
						}

                    ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value('<?php echo $row[csf('requisition_no')].','.$row[csf('company_id')].','.$row[csf('buyer_id')]; ?>')" style="cursor:pointer" >
                            <td width="30"><?php echo $i; ?></td>
                            <td width="110"><p><?php echo $buyer_arr[$row[csf("buyer_id")]]; ?></p></td>
                            <td width="300"><p><?php echo $po_no; ?></p></td>
                            <td width="90" align="center"><p><?php echo $row[csf("requisition_no")]; ?></p></td>
                            <td width="90" align="center"><p><?php echo change_date_format($row[csf("requisition_date")]); ?></p></td>
                            <td width="" align="right"><p><?php echo $row[csf("yarn_qnty")]; ?></p></td>
                       </tr>
                    <?php 
						$i++; 
					} 
					?>
            	</tbody>
        	</table>
       </div>
    </div>    
    <?php
	exit();
}

if($action=="show_req_list_view")
{
	
	$ex_data=explode(",",$data);
	$reqsation_no=$ex_data[0];
	$company=$ex_data[1];
	$buyer=$ex_data[2];
	
	$product_array=array();
	$color_arr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$productDataArray=sql_select( "select id, product_name_details, color from product_details_master where item_category_id=1");
	foreach($productDataArray as $rowProd)
	{
		$product_array[$rowProd[csf('id')]]['desc']=$rowProd[csf('product_name_details')];
		$product_array[$rowProd[csf('id')]]['color']=$rowProd[csf('color')];
	}

	if ($reqsation_no!='' || $company!=0)
	{
		$sql = "select a.buyer_id,c.requisition_no,c.prod_id,c.requisition_date,sum(c.yarn_qnty) as yarn_qnty from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c where a.id=b.mst_id and b.id=c.knit_id and requisition_no='$reqsation_no' and a.company_id='$company' group by c.requisition_no,c.prod_id,c.requisition_date,a.buyer_id";
		//echo $sql;
	}
	
	$result = sql_select($sql);
	//$yes_no_req=count($result);
	$i=1;
	?>
    	<fieldset style="width:300px;">	
        <table width="100%" class="rpt_table" cellpadding="0" cellspacing="0">
            <thead><th>SL</th><th>Product</th><th>Color</th><th>Qnty</th></thead>
            <tbody>
            <?php
				foreach($result as $key=>$val)
				{
					if ($i%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
					
					$product_name=$product_array[$val[csf("prod_id")]]['desc'];
					$color=$product_array[$val[csf("prod_id")]]['color'];
				?>	
					<tr bgcolor="<?php echo $bgcolor; ?>" onClick="get_php_form_data('<?php echo $val[csf("requisition_no")]."_".$val[csf("prod_id")]; ?>', 'populate_child_from_data', 'requires/yarn_issue_controller');" style="cursor:pointer" >
						<td width="20"><?php echo $i; ?></td>
                        <td width="180"><?php echo $product_name; ?></td>
                        <td width="60"><?php echo $color; ?></td>
                        <td width="50" align="right"><?php echo $val[csf("yarn_qnty")]; ?></td>
					</tr>
				<?php	
					$i++;	
				}
			?>
			</tbody>
        </table>
        </fieldset>
   <?php
   exit();    
}

if($action=="populate_child_from_data")
{
	$exp_data = explode("_",str_replace("'","",$data));
	$req_no=$exp_data[0];
	$prod_id=$exp_data[1];
	
	$sql = "select a.requisition_no,a.prod_id,b.*
			from ppl_yarn_requisition_entry a, product_details_master b
			where a.prod_id=b.id and a.requisition_no=$req_no and a.prod_id=$prod_id";
	//echo $sql;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		$composition_string = $composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')];
		if($row[csf('yarn_comp_type2nd')]!=0) $composition_string .= $composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')];
		
		echo "$('#cbo_supplier').val('".$row[csf("supplier_id")]."');\n";
		//echo "$('#cbo_store_name').val('".$row[csf("store_id")]."');\n";
		echo "$('#txt_lot_no').val('".$row[csf("lot")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("id")]."');\n";
		echo "$('#txt_current_stock').val(".$row[csf("current_stock")].");\n";
		echo "$('#cbo_yarn_count').val(".$row[csf("yarn_count_id")].");\n";
		echo "$('#txt_composition').val('".$composition_string."');\n";
		echo "$('#cbo_yarn_type').val('".$row[csf("yarn_type")]."');\n";
 		echo "$('#cbo_uom').val('".$row[csf("unit_of_measure")]."');\n";
		echo "$('#cbo_color').val('".$row[csf("color")]."');\n";
		echo "$('#cbo_brand').val('".$row[csf("brand")]."');\n";
		
		/*$sqsl = "select a.po_id
			from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c 
			where a.id=b.mst_id and b.id=c.knit_id and requisition_no=".$row[csf("requisition_no")];*/
		if($db_type==0)
		{	
			$poID=return_field_value("group_concat(distinct(a.po_id)) as poid","ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c","b.id=a.dtls_id and b.id=c.knit_id and requisition_no='".$row[csf("requisition_no")]."'","poid");
		}
		else
		{
			$poID=return_field_value("LISTAGG(a.po_id, ',') WITHIN GROUP (ORDER BY a.po_id) as poid","ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c","b.id=a.dtls_id and b.id=c.knit_id and requisition_no='".$row[csf("requisition_no")]."'","poid");	
		}
		
		echo "$('#all_po_id').val('".$poID."');\n";
	}
	exit();
}

if($action=="show_yarn_dyeing_list_view")
{
	$product_array=return_library_array( "select id, lot from product_details_master where item_category_id=1",'id','lot');
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');

	$sql = "select id, job_no, product_id, count, yarn_description, yarn_wo_qty from wo_yarn_dyeing_dtls where mst_id=$data and status_active=1 and is_deleted=0";
	//echo $sql;die;
	$result = sql_select($sql);
	$i=1;
	?>
    	<fieldset style="width:310px;">	
        <table width="100%" class="rpt_table" cellpadding="0" cellspacing="0" rules="all" border="1">
            <thead><th>SL</th><th>Job</th><th>Lot</th><th>Product</th><th>Qnty</th></thead>
            <tbody>
            <?php
				foreach($result as $row)
				{
					if ($i%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
					
					if($row[csf("product_id")]==0)
					{
						$product_name=$count_arr[$row[csf("count")]]." ".$row[csf("yarn_description")];
						$lot="&nbsp;";
					}
					else
					{
						$product_name=$row[csf("yarn_description")];
						$lot=$product_array[$row[csf("product_id")]];
					}
				?>	
					<tr bgcolor="<?php echo $bgcolor; ?>" onClick="get_php_form_data(<?php echo $row[csf("id")]; ?>, 'populate_child_from_yarn_dyeing_data', 'requires/yarn_issue_controller');" style="cursor:pointer" >
						<td width="20"><?php echo $i; ?></td>
                        <td width="80"><?php echo $row[csf("job_no")]; ?></td>
                        <td width="50"><p><?php echo $lot; ?></p></td>
                        <td><p><?php echo $product_name; ?></p></td>
                        <td width="50" align="right"><?php echo $row[csf("yarn_wo_qty")]; ?></td>
					</tr>
				<?php	
					$i++;	
				}
			?>
			</tbody>
        </table>
        </fieldset>
   <?php
   exit();    
}

if($action=="populate_child_from_yarn_dyeing_data")
{
	$sql = "select job_no, product_id, count, yarn_description, yarn_wo_qty from wo_yarn_dyeing_dtls where id=$data";
	//echo $sql;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		echo "$('#job_no').val('".$row[csf("job_no")]."');\n";
		$style_ref_no=return_field_value("style_ref_no","wo_po_details_master","job_no='".$row[csf("job_no")]."'");
		echo "$('#txt_style_ref').val('".$style_ref_no."');\n";
		echo "$('#txt_buyer_job_no').val('".$row[csf("job_no")]."');\n";
		
		$prodData=sql_select("select id, unit_of_measure, supplier_id, lot, current_stock, yarn_count_id, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd,  yarn_type, color, brand, item_code from product_details_master where id=".$row[csf("product_id")]);
		
		$composition_string = $composition[$prodData[0][csf('yarn_comp_type1st')]]." ".$prodData[0][csf('yarn_comp_percent1st')]."%";
		if($prodData[0][csf('yarn_comp_type2nd')]!=0) $composition_string .= $composition[$prodData[0][csf('yarn_comp_type2nd')]]." ".$prodData[0][csf('yarn_comp_percent2nd')]."%";
		
		echo "$('#cbo_supplier').val('".$prodData[0][csf("supplier_id")]."');\n";
		echo "$('#txt_lot_no').val('".$prodData[0][csf("lot")]."');\n";
		echo "$('#txt_prod_id').val('".$prodData[0][csf("id")]."');\n";
		echo "$('#txt_current_stock').val(".$prodData[0][csf("current_stock")].");\n";
		echo "$('#cbo_yarn_count').val(".$prodData[0][csf("yarn_count_id")].");\n";
		echo "$('#txt_composition').val('".$composition_string."');\n";
		echo "$('#cbo_yarn_type').val('".$prodData[0][csf("yarn_type")]."');\n";
		echo "$('#cbo_uom').val('".$prodData[0][csf("unit_of_measure")]."');\n";
		echo "$('#cbo_color').val('".$prodData[0][csf("color")]."');\n";
		echo "$('#cbo_brand').val('".$prodData[0][csf("brand")]."');\n";
		
	}
	exit();
}

if ($action=="yarn_issue_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	
	//echo $data[5];
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$other_party_arr=return_library_array( "select id, other_party_name from  lib_other_party",'id','other_party_name');
	$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
	$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');

	$sql=" select id, issue_number, issue_basis, booking_id, booking_no, other_party, knit_dye_source, challan_no, issue_purpose, buyer_id, issue_date, knit_dye_company, gate_pass_no, remarks from inv_issue_master where issue_number='$data[1]'";
	//echo $sql;die;
	$dataArray=sql_select($sql);

	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );

?>
<div style="width:930px;">
    <table width="930" cellspacing="0" align="right" border="0">
        <tr>
            <td colspan="5" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td><td  style="font-size:xx-large; font-style:italic;" align="right" ><div style="color:#FF0000"><?php if ($data[4]==1) echo "<b>Approved</b>"; ?></div></td>
        </tr>
        <tr class="form_caption">
            <td colspan="5" align="center">
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td><td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5" align="center" style="font-size:x-large"><strong><u><?php echo $data[2]; ?> Note/Challan</u></strong></center></td><td>&nbsp;</td>
        </tr>
        <tr>
            <td width="160"><strong>Issue No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="120"><strong>Booking/Req. N:</strong></td><td width="175px"><?php echo $dataArray[0][csf('booking_no')]; ?></td>
            <td width="125"><strong>Knitting Source:</strong></td><td width="175px"><?php echo  $knitting_source[$dataArray[0][csf('knit_dye_source')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Challan/Program No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Issue Purpose:</strong></td><td width="175px"><?php if ($dataArray[0][csf('issue_purpose')]==3) echo "Knitting Purpose"; else echo $yarn_issue_purpose[$dataArray[0][csf('issue_purpose')]];// ?></td>
            <td><strong>Issue Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
        </tr>
        <tr>
             <td><strong>Issue To:</strong></td> 
             
				<?php
					$supp_add=$dataArray[0][csf('knit_dye_company')];
					$nameArray=sql_select( "select address_1,web_site,email,country_id from lib_supplier where id=$supp_add"); 
					foreach ($nameArray as $result)
					{ 
                    	$address="";
						if($result!="") $address=$result['address_1'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
					}
					
					$other_party_add=$dataArray[0][csf('other_party')];
					$otherPartyArray=sql_select( "select address, email, web_site, country_id from  lib_other_party where id=$other_party_add"); 
					foreach ($otherPartyArray as $result)
					{ 
                    	$addressParty="";
						if($result!="") $addressParty=$result['address'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
					}
					
                ?> 
             <td width="175px" colspan="5">
			 	<?php 
					if ($dataArray[0][csf('issue_purpose')]==3)
					{ 
						echo $buyer_arr[$dataArray[0][csf('buyer_id')]];
					}
					else if ($dataArray[0][csf('issue_purpose')]==5) 
					{
						echo $other_party_arr[$dataArray[0][csf('other_party')]].' : Address :- '.$addressParty;
					}
					else
					{
						if($dataArray[0][csf('knit_dye_source')]==1) 
							echo $company_library[$dataArray[0][csf('knit_dye_company')]];
						else 
							echo $supplier_arr[$dataArray[0][csf('knit_dye_company')]].' : Address :- '.$address; 
					}
					
					
					?></td>
                </tr>
        <tr>
            <td><strong>Demand No.:</strong></td><td width="175px"><?php //echo $dataArray[0][csf('booking_id')]; ?></td>
            <td><strong>Gate Pass No.:</strong></td><td width="175px"><?php echo $dataArray[0][csf('gate_pass_no')]; ?></td>
            <td><strong>Do No:</strong></td><td width="175px"><?php //echo $dataArray[0][csf('remarks')]; ?></td>
        </tr>
        <tr>
        	<td valign="top"><strong>Remarks :</strong></td><td ><p><?php echo $dataArray[0][csf('remarks')]; ?></p></td>
            <td><strong>Bar Code:</strong></td><td  colspan="3" id="barcode_img_id"></td>
        </tr>
    </table>
         <br>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="30">SL</th>
                <th width="50" align="center">Req. No</th>
                <th width="50" align="center">Lot No</th>
                <th width="150" align="center">Item Details</th>
                <th width="70" align="center">Dye. Color</th>
                <th width="60" align="center">Supp.</th> 
                <th width="60" align="center">Issue Qty(kg)</th>
                <th width="60" align="center">Returnable Qty(kg)</th>
                <th width="70" align="center">No.Of Bag & Cone</th>
                <th width="80" align="center">Store</th>
                <th width="110" align="center">Buyer & Job</th>                   
                <th width="" align="center">Order & Style</th>
            </thead>
   <?php
	$wopi_library=return_library_array( "select id,pi_number from com_pi_master_details", "id","pi_number"  );

	$cond="";
	if($data[1]!="") $cond .= " and c.issue_number='$data[1]'";

	$po_array=array();
	$costing_sql=sql_select("select a.job_no, a.style_ref_no, a.buyer_name, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$data[0]");
	foreach($costing_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')];
		$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')];
		$po_array[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')]; 
	}
	
	//var_dump($po_array);
    $i=1;
	if($db_type==0)
	{
		$sql_result= sql_select("select a.id, a.lot, a.product_name_details, a.detarmination_id, b.dyeing_color_id, b.id, b.requisition_no, b.cons_uom, b.cons_quantity, b.return_qnty as returnable_qnty, b.store_id, b.no_of_bags, b.cone_per_bag, c.issue_basis, c.booking_no, c.buyer_id, c.buyer_job_no, a.supplier_id, group_concat(d.po_breakdown_id) as po_id, sum(d.returnable_qnty) as returnable_qnty_pro from product_details_master a, inv_issue_master c, inv_transaction b left join order_wise_pro_details d on b.id=d.trans_id where c.id=b.mst_id and b.prod_id=a.id and b.transaction_type=2 and b.item_category=1 and c.entry_form=3 $cond group by a.id, a.lot, a.product_name_details, a.detarmination_id, a.supplier_id, b.id, b.dyeing_color_id, b.requisition_no, b.cons_uom, b.cons_quantity, b.return_qnty, b.store_id, b.no_of_bags, b.cone_per_bag, c.issue_basis, c.booking_no, c.buyer_id, c.buyer_job_no");
	}
	else
	{
		$sql_result= sql_select("select a.id, a.lot, a.product_name_details, a.detarmination_id, b.dyeing_color_id, b.id, b.requisition_no, b.cons_uom, b.cons_quantity, b.return_qnty as returnable_qnty, b.store_id, b.no_of_bags, b.cone_per_bag, c.issue_basis, c.booking_no, c.buyer_id, c.buyer_job_no, a.supplier_id, LISTAGG(d.po_breakdown_id, ',') WITHIN GROUP (ORDER BY d.po_breakdown_id) as po_id, sum(d.returnable_qnty) as returnable_qnty_pro from product_details_master a, inv_issue_master c, inv_transaction b left join order_wise_pro_details d on b.id=d.trans_id where c.id=b.mst_id and b.prod_id=a.id and b.transaction_type=2 and b.item_category=1 and c.entry_form=3 $cond group by a.id, a.lot, a.product_name_details, a.detarmination_id, a.supplier_id, b.id, b.dyeing_color_id, b.requisition_no, b.cons_uom, b.cons_quantity, b.return_qnty, b.store_id, b.no_of_bags, b.cone_per_bag, c.issue_basis, c.booking_no, c.buyer_id, c.buyer_job_no");	
	}
 	$all_req_no=''; $all_prod_id='';
	foreach($sql_result as $row)
	{
		if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
		if($row[csf('requisition_no')]!='')
		{
			if ($all_req_no=='') $all_req_no=$row[csf('requisition_no')]; else $all_req_no.=','.$row[csf('requisition_no')];
			if ($all_prod_id=='') $all_prod_id=$row[csf('po_id')]; else $all_prod_id.=','.$row[csf('po_id')];
		}
			$order_qnty_val=$row[csf('cons_quantity')];
			$order_qnty_val_sum += $order_qnty_val;
			
			$order_amount_val=$row[csf('order_amount')];
			$order_amount_val_sum += $order_amount_val;
			
			$no_of_bags_val=$row[csf('no_of_bags')];
			$no_of_bags_val_sum += $no_of_bags_val;
			
			$po_id=explode(",",$row[csf('po_id')]);
			$po_no=''; $style_ref=''; $job_no=''; $buyer='';
			//$data=explode('*',$data);
			$productdtls=$row[csf('product_name_details')];
			$productArr=explode(' ',$productdtls);
			
			$proddtls='';
			if ($productArr[0]!='' && $productArr[1]!='' && $productArr[2]!='' && $productArr[3]!='' && $productArr[4]!=''&& $productArr[5]!='')
			{
				$proddtls=$productArr[0].', '.$productArr[1].' '.$productArr[2].' %, '.$productArr[3].' '.$productArr[4]."%, ".$productArr[5].", ".$productArr[6]; 
			}
			else if ($productArr[0]!='' && $productArr[1]!='' && $productArr[2]!='' && $productArr[3]!='' && $productArr[4]!='')
			{
				$proddtls=$productArr[0].', '.$productArr[1].' '.$productArr[2].' %, '.$productArr[3].', '.$productArr[4]; 
			}
			else if($productArr[0]!='' && $productArr[1]!='' && $productArr[2]!='' && $productArr[3]!='' )
			{
				$proddtls=$productArr[0].', '.$productArr[1].' '.$productArr[2].' %, '.$productArr[3]; 
			}
			else if($productArr[0]!='' && $productArr[1]!='' && $productArr[2]!='')
			{
				$proddtls=$productArr[0].', '.$productArr[1].' '.$productArr[2].' %, '; 
			}
			else if($productArr[0]!='' && $productArr[1]!='' )
			{
				$proddtls=$productArr[0].', '.$productArr[1]; 
			}
			else if($productArr[0]!='')
			{
				$proddtls=$productArr[0]; 
			}
			else
			{
				$proddtls=''; 
			}
			
			foreach($po_id as $val)
			{
				if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=", ".$po_array[$val]['no'];
				if($job_no=='') $job_no=$po_array[$val]['job_no'];
				if($style_ref=='') $style_ref=$po_array[$val]['style_ref'];
				if($buyer=='') $buyer_name=$po_array[$val]['buyer_name'];
			}
			
			$buyer_job='';
			if($row[csf('issue_basis')]==1)
			{
				$buyer_job=$buyer_arr[$row[csf('buyer_id')]]; if($row[csf('buyer_job_no')]!="") { $buyer_job=$buyer_arr[$row[csf('buyer_id')]].' & '.$row[csf('buyer_job_no')];}
			}
			elseif($row[csf('issue_basis')]==2)
			{
				$buyer_job='';
			}
			elseif($row[csf('issue_basis')]==3)
			{
				if($db_type==0)
				{
					$poID=return_field_value("group_concat(distinct(a.po_id)) as poid","ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c","b.id=a.dtls_id and b.id=c.knit_id and requisition_no='".$row[csf("requisition_no")]."'","poid");
				}
				else
				{
					$poID=return_field_value("LISTAGG(a.po_id, ',') WITHIN GROUP (ORDER BY a.po_id) as poid","ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c","b.id=a.dtls_id and b.id=c.knit_id and requisition_no='".$row[csf("requisition_no")]."'","poid");	
				}
				
				$ex_data = array_unique(explode(",",$poID));
				//print_r($ex_data);
				if ($job_no!='')
				{
					$buyer_job=$buyer_arr[$buyer_name].' & '.$job_no;
				}
				else
				{
					$buyer_job=$buyer_arr[$buyer_name];
				}
			}
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td width="30"><?php echo $i; ?></td>
                <td width="50"><?php if($row[csf('requisition_no')]!=0) echo $row[csf('requisition_no')]; ?></td>
                <td width="50"><?php echo $row[csf('lot')]; ?></td>
                <td width="150"><?php echo $proddtls; ?></td>
                <td width="70"><?php echo $color_name_arr[$row[csf('dyeing_color_id')]]; ?></td>
                <td width="60"><?php echo $supplier_arr[$row[csf('supplier_id')]]; ?></td>
                <td align="right" width="60"><?php echo number_format($row[csf('cons_quantity')],3,'.',''); ?></td>
                <td align="right" width="60"><?php echo number_format($row[csf('returnable_qnty')],2,'.',''); ?></td>
                <td align="right" width="70"><?php if ($row[csf('cone_per_bag')]=="" || $row[csf('cone_per_bag')]==0) echo $no_of_bags_val; else echo $no_of_bags_val.' & '.$row[csf('cone_per_bag')]; ?>&nbsp;</td>
                <td width="80"><?php echo $store_arr[$row[csf('store_id')]]; ?></td>
                <td width="110"><p><?php echo $buyer_job; ?></p></td>
                <td width=""><p><?php echo $po_no; if($style_ref!="") echo ' & '.$style_ref; ?></p></td>
			</tr>
			<?php
				$uom_unit="Kg";
				$uom_gm="Grams";
				$tot_return_qty+=$row[csf('returnable_qnty')];
				$tot_bag+=$no_of_bags_val;
				$tot_cone+=$row[csf('cone_per_bag')];
				$req_no=$row[csf('requisition_no')];
			$i++;
			}
		?>
        	<tr> 
                <td align="right" colspan="6" >Total</td>
                <td align="right"><?php echo $format_total_amount=number_format($order_qnty_val_sum,3,'.',''); ?></td>
                <td align="right"><?php echo number_format($tot_return_qty,2,'.','');  ?></td>
                <td align="right"><?php echo $tot_bag; if ($tot_cone!="") echo ' & '.$tot_cone;  ?></td>
                <td align="right" colspan="3" ></td>
			</tr>
            <tr>
                <td colspan="12" align="left"><b>In Word: <?php echo number_to_words($format_total_amount,$uom_unit,$uom_gm); ?></b></td>
            </tr>
		</table>
        <br>
        <br>&nbsp;
        <!--================================================================-->
        <?php 
		if($dataArray[0][csf('issue_basis')]==3)
		{
			
			//echo $all_req_no.'=='.$all_prod_id;die;
		?>
        <div style="">
        <table width="850" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
            	<tr>
                	<th colspan="7" align="center">Requisition Details</th>
                </tr>
                <tr>
                    <th width="40">SL</th>
                    <th width="100">Requisition No</th>
                    <th width="110">Lot No</th>
                    <th width="220">Yarn Description</th>
                    <th width="110">Brand</th>
                    <th width="90">Requisition Qty</th>
                    <th>Remarks</th>
                </tr>
            </thead>
            <?php
				$i=1; $tot_reqsn_qnty=0;
				$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
				$product_details_array=array();
				$sql="select id, supplier_id, lot, current_stock, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color, brand from product_details_master where item_category_id=1 and company_id='$data[0]' and status_active=1 and is_deleted=0";
				$result = sql_select($sql);
				
				foreach($result as $row)
				{
					$compos='';
					if($row[csf('yarn_comp_percent2nd')]!=0)
					{
						$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
					}
					else
					{
						$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
					}
					
					$product_details_array[$row[csf('id')]]['count']=$count_arr[$row[csf('yarn_count_id')]];
					$product_details_array[$row[csf('id')]]['comp']=$compos;
					$product_details_array[$row[csf('id')]]['type']=$yarn_type[$row[csf('yarn_type')]];
					$product_details_array[$row[csf('id')]]['lot']=$row[csf('lot')];
					$product_details_array[$row[csf('id')]]['brand']=$brand_arr[$row[csf('brand')]];
				}	
							
				$sql="select knit_id, requisition_no, prod_id, sum(yarn_qnty) as yarn_qnty from ppl_yarn_requisition_entry where requisition_no in ($all_req_no) and status_active=1 and is_deleted=0 group by requisition_no, prod_id, knit_id";
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					?>
                    <tr>
                        <td width="40" align="center"><?php echo $i; ?></td>
                        <td width="100" align="center">&nbsp;<?php echo $selectResult[csf('requisition_no')]; ?></td>
                        <td width="110" align="center">&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['lot']; ?></td>
                        <td width="220">&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['count']." ".$product_details_array[$selectResult[csf('prod_id')]]['comp']." ".$product_details_array[$selectResult[csf('prod_id')]]['type']; ?></td>
                        <td width="110" align="center">&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['brand']; ?></td>
                        <td width="90" align="right"><?php echo number_format($selectResult[csf('yarn_qnty')],2); ?>&nbsp;</td>
                        <td><?php //echo $selectResult[csf('requisition_no')]; ?></td>	
                    </tr>
					<?php
					$tot_reqsn_qnty+=$selectResult[csf('yarn_qnty')];
					$i++;
				}
			?>
            <tfoot>
                <th colspan="5" align="right"><b>Total</b></th>
                <th align="right"><?php echo number_format($tot_reqsn_qnty,2); ?>&nbsp;</th>
                <th>&nbsp;</th>
            </tfoot>
        </table>
        <?php
		if($data[5]!=1)
		{
			$z=1; $k=1;
			$colorArray=sql_select("select a.id, a.mst_id, a.knitting_source, a.knitting_party, a.program_date,a. color_range, a.stitch_length, a.machine_dia, a.machine_gg, a.program_qnty, a.remarks, b.knit_id, c.body_part_id, c.fabric_desc, c.gsm_weight, c.dia from ppl_planning_info_entry_dtls a, ppl_yarn_requisition_entry b, ppl_planning_info_entry_mst c where a.mst_id= c.id and a.id=b.knit_id and b.requisition_no in ($all_req_no) and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.is_deleted=0");
			?> 
			<table width="710" cellpadding="0" cellspacing="0" border="1" rules="all" style="margin-top:20px;" class="rpt_table">
				<thead>
					<th width="40">SL</th>
					<th width="250">Fabrication</th>
					<th width="120">Color</th>
					<th width="120">GGSM OR S/L</th>
					<th>FGSM</th>
				 </thead>
					<?php
					/*foreach ($colorArray as $row)
					{s*/
						?>
						<tr>
							<td width="40" align="center"><?php echo $z; ?></td>
							<td width="250" align="center"><?php echo $body_part[$colorArray[0][csf('body_part_id')]].', '.$colorArray[0][csf('fabric_desc')];//$color_range[$row[csf('color_range')]]; ?></td>
							<td width="120" align="center"><?php echo $color_range[$colorArray[0][csf('color_range')]];//$color_range[$row[csf('color_range')]]; ?></td>
							<td width="120" align="center"><?php echo $colorArray[0][csf('stitch_length')];//$row[csf('stitch_length')]; ?></td>
							<td align="center"><?php echo $colorArray[0][csf('gsm_weight')];//$row[csf('gsm_weight')]; ?></td>
						</tr>
						<?php
						/*$z++;
					}*/
					?>
			</table>
			<table style="margin-top:20px;" width="650" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
				<thead>
					<th width="40">SL</th>
					<th width="50">Prog. No</th>
					<th width="110">Finish Dia</th>
					<th width="170">Machine Dia & Gauge</th>
					<th width="110">Program Qty</th>
					<th>Remarks</th>
				</thead>
					<?php
					//foreach ($colorArray as $row)
					//{
						?>
						<tr>
							<td width="40" align="center"><?php echo $k; ?></td>
							<td width="50" align="center">&nbsp;<?php echo $colorArray[0][csf('knit_id')]; ?></td>
							<td width="110" align="center"><?php echo $colorArray[0][csf('dia')];//$row[csf('dia')]; ?></td>
							<td width="170" align="center"><?php echo $colorArray[0][csf('machine_dia')]."X".$colorArray[0][csf('machine_gg')]; //$row[csf('machine_dia')]."X".$row[csf('machine_gg')]; ?></td>
							<td width="110" align="right"><?php echo number_format($colorArray[0][csf('program_qnty')],2);//number_format($row[csf('program_qnty')],2); ?>&nbsp;</td>
							<td><?php echo $colorArray[0][csf('remarks')]//$row[csf('remarks')]; ?></td>
						</tr>
						<?php
						$tot_prog_qty+=$colorArray[0][csf('program_qnty')];
						//$k++;
					//}
					?>
					<tr>
						<td colspan="4" align="right"><strong>Total : </strong></td>
						<td align="right"><?php echo number_format($tot_prog_qty,2); ?></td>
						<td>&nbsp;</td>
					</tr>
			</table> 
			<?php
			}
		}
		else if($dataArray[0][csf('issue_basis')]==1)
		{
			?>
             <table style="margin-top:20px;" width="500" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
             	<thead>
                    <tr><th colspan="4" align="center">Comments (Booking)</th></tr>
                    <tr>
                        <th>Req. Qty</th>
                        <th>Cuml. Qty</th>
                        <th>Balance Qty</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
				 <?php
                    if( $dataArray[0][csf('issue_purpose')]==8 )
                        {
                            $sql = "select a.id, sum(b.grey_fabric) as fabric_qty from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.booking_no='".$dataArray[0][csf('booking_no')]."' group by a.id"; 
                        }
                        else if( $dataArray[0][csf('issue_purpose')]==2)
                        {					
                            $sql = "select a.id, sum(b.yarn_wo_qty) as fabric_qty from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.ydw_no='".$dataArray[0][csf('booking_no')]."' group by a.id"; 
                        }
                        else
                        {
                            $sql = "select a.id, sum(b.grey_fab_qnty) as fabric_qty from wo_booking_mst a,  wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.booking_no='".$dataArray[0][csf('booking_no')]."' group by a.id"; 
                        }
                        $result = sql_select($sql);
						
						$total_issue_qty=return_field_value("sum(a.cons_quantity) as total_issue_qty"," inv_transaction a, inv_issue_master b","b.id=a.mst_id and b.booking_no='".$dataArray[0][csf('booking_no')]."' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.transaction_type=2 and b.item_category=1","total_issue_qty");		 
						$total_return_qty=return_field_value("sum(a.cons_quantity) as total_issue_qty"," inv_transaction a, inv_receive_master b","b.id=a.mst_id and b.booking_no='".$dataArray[0][csf('booking_no')]."' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.transaction_type=4 and b.item_category=1","total_issue_qty");		 
                 ?>
                 <tbody>
                     <tr>
                         <td align="center">
                             <?php echo number_format($result[0][csf('fabric_qty')],3); ?>
                         </td>
                         <td align="center">
                             <?php $cumulative_qty=$total_issue_qty-$total_return_qty; echo number_format($cumulative_qty,3); ?>
                         </td>
                         <td align="center">
                             <?php $balance_qty=$result[0][csf('fabric_qty')]-$cumulative_qty; echo number_format($balance_qty,3);?>
                         </td>
                         <td align="center">
                             <?php if ($result[0][csf('fabric_qty')]>$cumulative_qty)  echo "Less"; else if ($result[0][csf('fabric_qty')]<$cumulative_qty)  echo "Over"; else echo ""; ?>
                         </td>
                     </tr>
                 </tbody>
             </table>
            <?php
		}
		?>       
        <br>
		 <?php
            echo signature_table(49, $data[0], "930px");
         ?>
	</div>
	</div>
   <script type="text/javascript" src="../../js/jquery.js"></script>
     <script type="text/javascript" src="../../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess ){
		   
			var value = valuess;//$("#barcodeValue").val();
		 
			var btype = 'code39';//$("input[name=btype]:checked").val();
			var renderer ='bmp';// $("input[name=renderer]:checked").val();
			 
			var settings = {
			  output:renderer,
			  bgColor: '#FFFFFF',
			  color: '#000000',
			  barWidth: 1,
			  barHeight: 30,
			  moduleSize:5,
			  posX: 10,
			  posY: 20,
			  addQuietZone: 1
			};
			$("#barcode_img_id").html('11');
			 value = {code:value, rect: false};
			
			$("#barcode_img_id").show().barcode(value, btype, settings);
		  
		} 
  
	   generateBarcode('<?php echo $data[1]; ?>');
	
	</script>
<?php

exit();
}

if ($action=="requisition_print")
{
    extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data[1]);die;
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
	$yarn_count_details=return_library_array( "select id,yarn_count from lib_yarn_count", "id", "yarn_count"  );
	$brand_details=return_library_array( "select id, brand_name from lib_brand", "id", "brand_name"  );
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
	
	$order_id = str_replace("'","",$hide_order_id);
	$company_name=$cbo_company_name;

	$po_array=array();
	$costing_sql=sql_select("select a.job_no, a.style_ref_no, a.buyer_name, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$data[0]");
	foreach($costing_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')]; 
		$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')]; 
		$po_array[$row[csf('id')]]['buyer_name']=$buyer_arr[$row[csf('buyer_name')]]; 
	}
	
	$product_details_arr=array();
	$pro_sql=sql_select("select id, yarn_count_id, lot, color, brand from product_details_master where company_id=$data[0] and item_category_id=1");
	foreach($pro_sql as $row)
	{
		$product_details_arr[$row[csf('id')]]['count']=$yarn_count_details[$row[csf('yarn_count_id')]];
		$product_details_arr[$row[csf('id')]]['lot']=$row[csf('lot')]; 
		$product_details_arr[$row[csf('id')]]['color']=$color_library[$row[csf('color')]];
		$product_details_arr[$row[csf('id')]]['brand']=$brand_details[$row[csf('brand')]];
	}
	//var_dump($product_details_arr);
	$reqsn_details_arr=array();
	$sql=sql_select("select prod_id, requisition_no, sum(yarn_demand_qnty) as demand_qnty from ppl_yarn_demand_reqsn_dtls group by prod_id, requisition_no");
	foreach($sql as $row)
	{
		$reqsn_details_arr[$row[csf('prod_id')]][$row[csf('requisition_no')]]=$row[csf('demand_qnty')];
	}	
	?>
    <div>
    <label><strong>Bar Code:</strong></label><label id="barcode_img_id"></label>
    <br> <br> 
        <table id="" cellspacing="0" cellpadding="0" border="1" rules="all" width="1040" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <tr>
                    <th colspan="12">Requisition Details</th>
                </tr>
                <tr>
                    <th width="30">SL</th>
                    <th width="60">Buyer</th>
                    <th width="130">Order No</th>
                    <th width="70">Reqsn. No.</th>
                    <th width="70">Yarn Count</th>
                    <th width="100">Yarn Brand</th>
                    <th width="100">Lot No</th>
                    <th width="100">Color</th>
                    <th width="80">Reqsn. Qty</th>
                    <th width="80">Demand Qty</th>
                    <th width="80">Delivery Qty</th>
                    <th width="80">Balance Qty</th>
                </tr>
            </thead>
        </table>
        <div style="width:1057px;" id="scroll_body">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1040" class="rpt_table" id="table_body">
            <tbody>
                <?php 
                    $i=1; $tot_reqsn_qnty=0; $tot_demand_qnty=0; $tot_delivery_qnty=0; $tot_balance=0;
					if($db_type==0)
					{
                    	$sql="select c.id, c.requisition_no as reqs_no, c.prod_id, c.no_of_cone, c.yarn_qnty as reqsn_qnty, group_concat(distinct(a.po_id)) as po_id from ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c where b.id=a.dtls_id and b.id=c.knit_id and c.requisition_no in ($data[1]) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.id, c.requisition_no, c.prod_id, c.no_of_cone, c.yarn_qnty order by c.requisition_no";
					}
					else
					{
						$sql="select c.id, c.requisition_no as reqs_no, c.prod_id, c.no_of_cone, c.yarn_qnty as reqsn_qnty, LISTAGG(a.po_id, ',') WITHIN GROUP (ORDER BY a.po_id) as po_id from ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c where b.id=a.dtls_id and b.id=c.knit_id and c.requisition_no in ($data[1]) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.id, c.requisition_no, c.prod_id, c.no_of_cone, c.yarn_qnty order by c.requisition_no";	
					}
                    //echo $sql;
                    $nameArray=sql_select( $sql );
                    foreach ($nameArray as $row)
                    {
                        if ($i%2==0)  
                            $bgcolor="#E9F3FF";
                        else
                            $bgcolor="#FFFFFF";
                        
                        $yarn_iss_data=sql_select("select sum(b.cons_quantity) as delivery_qnty, sum(b.no_of_bags) as no_of_bags from inv_issue_master a, inv_transaction b where a.id=b.mst_id and a.issue_basis=3 and a.item_category=1 and a.entry_form=3 and b.receive_basis=3 and b.transaction_type=2 and b.item_category=1 and b.requisition_no=".$row[csf('reqs_no')]." and b.prod_id=".$row[csf('prod_id')]." and b.status_active=1 and b.is_deleted=0");
                        
                        $delivery_qnty=$yarn_iss_data[0][csf('delivery_qnty')];
                        $no_of_bags=$yarn_iss_data[0][csf('no_of_bags')];
                        
                        $demand_qnty=$reqsn_details_arr[$row[csf('prod_id')]][$row[csf('reqs_no')]];
                        $balance_qnty=$row[csf('reqsn_qnty')]-$delivery_qnty;
                        
                        $po_id=array_unique(explode(",",$row[csf('po_id')]));
                        $po_no=''; $buyer='';
                        
                        foreach($po_id as $val)
                        {
                            if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=", ".$po_array[$val]['no'];
                            if($buyer=='') $buyer=$po_array[$val]['buyer_name'];
                        }
                        
                        ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
                            <td width="30"><?php echo $i; ?></td>
                            <td width="60"><p><?php echo  $buyer; ?></p></td>
                            <td width="130"><p><?php echo $po_no; ?></p></td>
                            <td width="70" align="center"><?php echo $row[csf('reqs_no')]; ?></td>
                            <td width="70"><p><?php echo $product_details_arr[$row[csf('prod_id')]]['count']; ?></p></td>
                            <td width="100"><?php echo $product_details_arr[$row[csf('prod_id')]]['brand']; ?></td>
                            <td width="100"><p><?php echo $product_details_arr[$row[csf('prod_id')]]['lot']; ?></p></td>
                            <td width="100"><p><?php echo $product_details_arr[$row[csf('prod_id')]]['color']; ?></p></td>
                            <td align="right" width="80"><?php echo number_format($row[csf('reqsn_qnty')],2); ?></td>
                            <td align="right" width="80"><?php echo number_format($demand_qnty,2); ?></td>
                            <td align="right" width="80"><?php echo number_format($delivery_qnty,2); ?></td>
                            <td align="right" width="80"><?php echo number_format($balance_qnty,2); ?></td>
                        </tr>
                        <?php
                        $tot_reqsn_qnty+=$row[csf('reqsn_qnty')];
                        $tot_demand_qnty+=$demand_qnty;
                        $tot_delivery_qnty+=$delivery_qnty;
                        $tot_balance+=$balance_qnty;
                        
                        $i++;
                    }
                    ?>
            </tbody>
        </table> 
        <table class="rpt_table" width="1040" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
            <tfoot>
                <th width="30">&nbsp;</th>
                <th width="60">&nbsp;</th>
                <th width="130">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100" align="right">Total</th>
                <th width="80" align="right" id="value_tot_reqsn_qnty"><?php echo number_format($tot_reqsn_qnty,2,'.',''); ?></th>
                <th width="80" align="right" id="value_tot_demand_qnty"><?php echo number_format($tot_demand_qnty,2,'.',''); ?></th>
                <th width="80" align="right" id="value_tot_delivery_qnty"><?php echo number_format($tot_delivery_qnty,2,'.',''); ?></th>
                <th width="80" align="right" id="value_tot_balance"><?php echo number_format($tot_balance,2,'.',''); ?></th>
            </tfoot>
        </table>
        </div>
	</div>
    
     <script type="text/javascript" src="../../js/jquery.js"></script>
     <script type="text/javascript" src="../../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess ){
		   
			var value = valuess;//$("#barcodeValue").val();
		  //alert(value)
			var btype = 'code39';//$("input[name=btype]:checked").val();
			var renderer ='bmp';// $("input[name=renderer]:checked").val();
			 
			var settings = {
			  output:renderer,
			  bgColor: '#FFFFFF',
			  color: '#000000',
			  barWidth: 1,
			  barHeight: 30,
			  moduleSize:5,
			  posX: 10,
			  posY: 20,
			  addQuietZone: 1
			};
			//$("#barcode_img_id").html('11');
			 value = {code:value, rect: false};
			
			$("#barcode_img_id").show().barcode(value, btype, settings);
		  
		} 
  
	 generateBarcode('<?php echo $data[2]; ?>');
	 
	 
	 </script>
<?php
exit();
}

?>