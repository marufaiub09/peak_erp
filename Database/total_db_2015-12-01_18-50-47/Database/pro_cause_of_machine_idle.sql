-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_cause_of_machine_idle`
--

DROP TABLE IF EXISTS `pro_cause_of_machine_idle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_cause_of_machine_idle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `machine_entry_tbl_id` int(11) NOT NULL DEFAULT '0',
  `machine_no` varchar(25) NOT NULL,
  `from_date` date NOT NULL,
  `from_hour` int(11) NOT NULL DEFAULT '0',
  `from_minute` int(11) NOT NULL DEFAULT '0',
  `to_date` date NOT NULL,
  `to_hour` int(11) NOT NULL DEFAULT '0',
  `to_minute` int(11) NOT NULL DEFAULT '0',
  `machine_idle_cause` int(11) NOT NULL,
  `remarks` varchar(1000) NOT NULL,
  `status_active` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  `inserted_by` tinyint(1) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` tinyint(1) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_cause_of_machine_idle`
--

LOCK TABLES `pro_cause_of_machine_idle` WRITE;
/*!40000 ALTER TABLE `pro_cause_of_machine_idle` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_cause_of_machine_idle` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
