<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_name=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_library=return_library_array( "select id, company_short_name from lib_company", "id", "company_short_name"  );
$supplier_details=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$color_type_arr=return_library_array( "select id, color_type_id from wo_pre_cost_fabric_cost_dtls",'id','color_type_id');
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$machine_arr=return_library_array( "select id, machine_no from lib_machine_name",'id','machine_no');
$feeder=array(1=>"Full Feeder",2=>"Half Feeder");
//--------------------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 120, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "- All Buyer -", $selected, "" );     	 
	exit();
}

if ($action=="load_drop_down_party_type")
{
	$explode_data = explode("**",$data);
	$data = $explode_data[0];
	$selected_company = $explode_data[1];
	//print_r ($selected_company);
	if($data==3)
	{
		echo create_drop_down( "cbo_party_type", 120, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$selected_company' and b.party_type =20 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "--- Select ---", $selected, "" );
	}
	else if($data==1)
	{
		echo create_drop_down( "cbo_party_type", 120, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--- Select ---", $selected_company, "",0,0 );
	}
	else
	{
		echo create_drop_down( "cbo_party_type", 120, $blank_array,"", 1, "--- Select ---", $selected, "",1);
	}
	exit();
}

if($action=="order_no_search_popup")
{
	echo load_html_head_contents("Order No Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_order_id').val( id );
			$('#hide_order_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:780px;">
            <table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Order No</th>
                    <th>Shipment Date</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_order_no" id="hide_order_no" value="" />
                    <input type="hidden" name="hide_order_id" id="hide_order_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",0,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Order No",2=>"Style Ref",3=>"Job No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 110, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
                            <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
                        </td>	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value, 'create_order_no_search_list_view', 'search_div', 'knitting_status_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    	</td>
                    </tr>
                    <tr>
                        <td colspan="5" height="20" valign="middle"><?php echo load_month_buttons(1); ?></td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_order_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	
	$company_short_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==1) 
		$search_field="b.po_number"; 
	else if($search_by==2) 
		$search_field="a.style_ref_no"; 	
	else 
		$search_field="a.job_no";
		
	$start_date =$data[4];
	$end_date =$data[5];	
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format(trim($start_date),"yyyy-mm-dd")."' and '".change_date_format(trim($end_date),"yyyy-mm-dd")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format(trim($start_date),'','',1)."' and '".change_date_format(trim($end_date),'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	
	$arr=array (0=>$company_short_arr,1=>$buyer_arr);
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
	else $year_field="";//defined Later
	
	$sql= "select b.id, a.job_no, $year_field, a.job_no_prefix_num, a.company_name, a.buyer_name, a.style_ref_no, b.po_number, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $date_cond order by b.id, b.pub_shipment_date";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No, Po No, Shipment Date", "100,100,50,70,140,130","760","220",0, $sql , "js_set_value", "id,po_number","",1,"company_name,buyer_name,0,0,0,0,0",$arr,"company_name,buyer_name,year,job_no_prefix_num,style_ref_no,po_number,pub_shipment_date","",'','0,0,0,0,0,0,3','',1) ;
   exit(); 
}

$tmplte=explode("**",$data);

if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	$cbo_knitting_status=str_replace("'","",$cbo_knitting_status);

 	if($template==1)
	{
		$type = str_replace("'","",$cbo_type);
		$company_name=$cbo_company_name;
		if(str_replace("'","",$cbo_party_type)==0) $party_type="%%"; else $party_type=str_replace("'","",$cbo_party_type);
		if(str_replace("'","",$cbo_buyer_name)==0)
		{
			if ($_SESSION['logic_erp']["data_level_secured"]==1)
			{
				if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
			}
			else
			{
				$buyer_id_cond="";
			}
		}
		else
		{
			$buyer_id_cond=" and a.buyer_id=$cbo_buyer_name"; 	
		}
		
		if(str_replace("'","",trim($txt_order_no))=="")
		{
			$po_id_cond="";
		}
		else
		{
			if(str_replace("'","",$hide_order_id)!="")
			{
				$po_id=str_replace("'","",$hide_order_id);
			}
			else
			{
				$po_number=trim(str_replace("'","",$txt_order_no))."%";
				if($db_type==0)
				{
					$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");
				}
				else
				{
					$po_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");
				}	
				if($po_id=="") $po_id=0;
			}
			
			$po_id_cond="and c.po_id in(".$po_id.")";
		}
		//echo $po_id_cond;
		
		if(str_replace("'","",trim($txt_date_from))!="" && str_replace("'","",trim($txt_date_to))!="")
		{
			$date_cond=" and b.program_date between ".trim($txt_date_from)." and ".trim($txt_date_to)."";
		}
		else
		{
			$date_cond="";
		}
		
		if(str_replace("'","",$txt_machine_dia)=="") $machine_dia="%%"; else $machine_dia="%".str_replace("'","",$txt_machine_dia)."%";
		
		if(str_replace("'","",$txt_program_no)=="") $program_no="%%"; else $program_no=str_replace("'","",$txt_program_no);
		
		$po_array=array();
		$costing_sql=sql_select("select a.job_no, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company_name");
		foreach($costing_sql as $row)
		{
			$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
			$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')]; 
			$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')]; 
		}
		
		$product_details_arr=array();
		$pro_sql=sql_select("select id, product_name_details, lot from product_details_master where company_id=$company_name and item_category_id=1");
		foreach($pro_sql as $row)
		{
			$product_details_arr[$row[csf('id')]]['desc']=$row[csf('product_name_details')];
			$product_details_arr[$row[csf('id')]]['lot']=$row[csf('lot')]; 
		}
		
		if($db_type==0)
		{
			$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_name group by dtls_id", "dtls_id", "po_id"  );
		}
		else
		{
			$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_name group by dtls_id", "dtls_id", "po_id"  );
		}
		
		$yarn_iss_arr=array();
		$yarnIssueData=sql_select( "select requisition_no, prod_id, sum(cons_quantity) as qnty from inv_transaction where item_category=1 and transaction_type=2 and receive_basis=3 and status_active=1 and is_deleted=0 group by requisition_no, prod_id");
		foreach($yarnIssueData as $row)
		{
			$yarn_iss_arr[$row[csf('requisition_no')]][$row[csf('prod_id')]]=$row[csf('qnty')];
		}
		
		$yarn_IssRtn_arr=array();
		$yarnIssueRtnData=sql_select( "select a.booking_id as reqsn_no, b.prod_id, sum(b.cons_quantity) as qnty from inv_receive_master a, inv_transaction b where a.id=b.mst_id and a.receive_basis=3 and a.entry_form=9 and b.item_category=1 and b.transaction_type=4 and a.status_active=1 and a.is_deleted=0 group by a.booking_id, b.prod_id");
		foreach($yarnIssueRtnData as $row)
		{
			$yarn_IssRtn_arr[$row[csf('reqsn_no')]][$row[csf('prod_id')]]=$row[csf('qnty')];
		}
		
		$reqsDataArr=array();
		if($db_type==0)
		{
			$reqsData=sql_select("select knit_id, requisition_no as reqs_no, group_concat(distinct(prod_id)) as prod_id from ppl_yarn_requisition_entry where status_active=1 and is_deleted=0 group by knit_id");
		}
		else
		{
			$reqsData=sql_select("select knit_id, max(requisition_no) as reqs_no, LISTAGG(prod_id, ',') WITHIN GROUP (ORDER BY prod_id) as prod_id from ppl_yarn_requisition_entry where status_active=1 and is_deleted=0 group by knit_id,requisition_no");	
		}
		foreach($reqsData as $row)
		{
			$reqsDataArr[$row[csf('knit_id')]]['reqs_no']=$row[csf('reqs_no')];
			$reqsDataArr[$row[csf('knit_id')]]['prod_id']=$row[csf('prod_id')];
		}
		
		$knitDataArr=array();
		if($db_type==0)
		{
			$knitting_dataArray=sql_select("select a.booking_id, group_concat(distinct(a.id)) as knit_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and item_category=13 and a.entry_form=2 and a.receive_basis=2 and b.status_active=1 and b.is_deleted=0 group by a.booking_id");
		}
		else
		{
			$knitting_dataArray=sql_select("select a.booking_id, LISTAGG(a.id, ',') WITHIN GROUP (ORDER BY a.id) as knit_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and item_category=13 and a.entry_form=2 and a.receive_basis=2 and b.status_active=1 and b.is_deleted=0 group by a.booking_id");	
		}
		foreach($knitting_dataArray as $row)
		{
			$knitDataArr[$row[csf('booking_id')]]['qnty']=$row[csf('knitting_qnty')];
			$knitDataArr[$row[csf('booking_id')]]['knit_id']=$row[csf('knit_id')];
		}

		$knitting_recv_qnty_array=return_library_array( "select a.booking_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and a.item_category=13 and a.entry_form=22 and a.receive_basis=9 and b.status_active=1 and b.is_deleted=0 group by a.booking_id", "booking_id","knitting_qnty");
		
		if($type==3)
		{
			$colspan=26;
			$tbl_width=3310;
		}
		else 
		{
			$colspan=25; 
			$tbl_width=3210;
		}
		
		ob_start();
		?>
        <fieldset style="width:<?php echo $tbl_width; ?>px;">
        	<table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                   <td align="center" width="100%" colspan="<?php echo $colspan+7; ?>" style="font-size:16px"><strong>Knitting Program</strong></td>
                </tr>
            </table>	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $tbl_width-20; ?>" class="rpt_table" >
                <thead>
                	<th width="40"></th>
                    <th width="40">SL</th>
                    <?php
						if($type==3) echo "<th width='100'>Party Name</th>";
					?>
                    <th width="60">Program No</th>
                    <th width="80">Program Date</th>
                    <th width="80">Start Date</th>
                    <th width="80">T.O.D</th>
                    <th width="80">Buyer</th>
                    <th width="130">Order No</th>
                    <th width="110"><?php echo $company_library[str_replace("'","",$company_name)]; ?></th>
                    <th width="120">Style</th>
                    <th width="70">Req. No</th>
                    <th width="80">Dia / GG</th>
                    <th width="100">Distribution Qnty</th>
                    <th width="80">M/C no</th>
                    <th width="70">Status</th>
                    <th width="140">Fabric Desc.</th>
                    <th width="170">Desc.Of Yarn</th>
                    <th width="70">Lot</th>
                    <th width="100">Color Range</th>
                    <th width="100">Stitch Length</th>
                    <th width="100">Sp. Stitch Length</th>
                    <th width="100">Draft Ratio</th>
                    <th width="70">Fabric Gsm</th>
                    <th width="70">Fabric Dia</th>
                    <th width="80">Width/Dia Type</th>
                    <th width="100">Program Qnty</th>
                    <th width="100">Yarn Issue Qnty</th>
                    <th width="100">Issue. Bal. Qnty</th>
                    <th width="100">Knitting Qnty</th>
                    <th width="100">Knit Balance Qnty</th>
                    <th width="100">Received Qnty</th>
                    <th width="100">Recv. Bal. Qnty</th>
                    <th width="80">Complete T.O.D</th>
                    <th>Remarks</th>
                </thead>
            </table>
			<div style="width:<?php echo $tbl_width-20; ?>px; overflow-y:scroll; max-height:330px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $tbl_width-37; ?>" class="rpt_table" id="tbl_list_search">
                    <tbody>
                        <?php 
							//ppl_planning_info_entry_dtls
							$status_cond="";
							if($cbo_knitting_status!="") $status_cond="and b.status in($cbo_knitting_status)";
							$i=1;$k=1;$tot_program_qnty=0; $tot_knitting_qnty=0; $tot_balance=0; $tot_balance_recv_qnty=0; $tot_knitting_recv_qnty=0; 
							$machine_dia_gg_array=array();
							$sql="select a.company_id, a.buyer_id, a.body_part_id, a.fabric_desc, a.gsm_weight, a.dia, a.width_dia_type, b.id, b.knitting_source, b.knitting_party, b.color_id, b.color_range, b.machine_dia, b.machine_gg, b.program_qnty, b.program_date, b.stitch_length, b.spandex_stitch_length, b.draft_ratio, b.machine_id, b.distribution_qnty, b.status, b.start_date, b.end_date, b.remarks from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id=$company_name and b.knitting_source=$type and b.knitting_party like '$party_type' and b.machine_dia like '$machine_dia' and b.id like '$program_no' and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $buyer_id_cond $po_id_cond $date_cond $status_cond group by b.id, a.company_id, a.buyer_id, a.body_part_id, a.fabric_desc, a.gsm_weight, a.dia, a.width_dia_type, b.knitting_source, b.knitting_party, b.color_id, b.color_range, b.machine_dia, b.machine_gg, b.program_qnty, b.program_date, b.stitch_length, b.spandex_stitch_length, b.draft_ratio, b.machine_id, b.distribution_qnty, b.status, b.start_date, b.end_date, b.remarks order by b.machine_dia,b.machine_gg";
							
                            $nameArray=sql_select( $sql );
							
                            foreach ($nameArray as $row)
                            {
                                if ($i%2==0)  
                                    $bgcolor="#E9F3FF";
                                else
                                    $bgcolor="#FFFFFF";
                               
								$machine_dia_gg=$row[csf('machine_dia')].'X'.$row[csf('machine_gg')];
								
								$machine_no='';
								$machine_id=explode(",",$row[csf("machine_id")]);
								foreach($machine_id as $val)
								{
									if($machine_no=='') $machine_no=$machine_arr[$val]; else $machine_no.=",".$machine_arr[$val];
								}
								
								$yarn_issue_qnty=0;
								/*if($db_type==0)
								{
									$sql_reqs="select requisition_no as reqs_no, group_concat(distinct(prod_id)) as prod_id from ppl_yarn_requisition_entry where knit_id=".$row[csf('id')]." and status_active=1 and is_deleted=0 group by requisition_no";
								}
								else
								{
									$sql_reqs="select requisition_no as reqs_no, LISTAGG(prod_id, ',') WITHIN GROUP (ORDER BY prod_id) as prod_id from ppl_yarn_requisition_entry where knit_id=".$row[csf('id')]." and status_active=1 and is_deleted=0 group by requisition_no";	
								}
								$result_reqs=sql_select($sql_reqs);
								$prod_id=array_unique(explode(",",$result_reqs[0][csf('prod_id')]));*/
								
								$prod_id=array_unique(explode(",",$reqsDataArr[$row[csf('id')]]['prod_id']));
								$yarn_desc=''; $lot='';
								foreach($prod_id as $val)
								{
									$yarn_desc.=$product_details_arr[$val]['desc'].",";
									$lot.=$product_details_arr[$val]['lot'].",";
									
									//$yarnRtnQty=$yarn_IssRtn_arr[$result_reqs[0][csf('reqs_no')]][$val];
									//$yarn_issue_qnty+=$yarn_iss_arr[$result_reqs[0][csf('reqs_no')]][$val]-$yarnRtnQty;
									
									$yarnRtnQty=$yarn_IssRtn_arr[$reqsDataArr[$row[csf('id')]]['reqs_no']][$val];
									$yarn_issue_qnty+=$yarn_iss_arr[$reqsDataArr[$row[csf('id')]]['reqs_no']][$val]-$yarnRtnQty;
								}

								$yarn_desc=explode(",",substr($yarn_desc,0,-1));
								$lot=explode(",",substr($lot,0,-1));
								
								$po_id=array_unique(explode(",",$plan_details_array[$row[csf('id')]]));
								$po_no=''; $style_ref=''; $job_no='';
								
								foreach($po_id as $val)
								{
									if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
									if($style_ref=='') $style_ref=$po_array[$val]['style_ref'];
									if($job_no=='') $job_no=$po_array[$val]['job_no'];
								}
								
								/*if($db_type==0)
								{
									$knitting_dataArray=sql_select("select group_concat(distinct(a.id)) as knit_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and item_category=13 and a.entry_form=2 and a.receive_basis=2 and a.booking_id=".$row[csf('id')]." and b.status_active=1 and b.is_deleted=0");
								}
								else
								{
									$knitting_dataArray=sql_select("select LISTAGG(a.id, ',') WITHIN GROUP (ORDER BY a.id) as knit_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and item_category=13 and a.entry_form=2 and a.receive_basis=2 and a.booking_id=".$row[csf('id')]." and b.status_active=1 and b.is_deleted=0");	
								}
								$knitting_qnty=$knitting_dataArray[0][csf('knitting_qnty')];
								$knit_id=$knitting_dataArray[0][csf('knit_id')];
								$knit_id=array_unique(explode(",",$knit_id));*/
								
								$knitting_qnty=$knitDataArr[$row[csf('id')]]['qnty'];
								$knit_id=$knitDataArr[$row[csf('id')]]['knit_id'];
								$knit_id=array_unique(explode(",",$knit_id));

								$knitting_recv_qnty=0;
								foreach($knit_id as $val)
								{
									$knitting_recv_qnty+=$knitting_recv_qnty_array[$val];
								}
								/*if(!$knit_id=="")
								{
									$knitting_recv_qnty=return_field_value("sum(b.grey_receive_qnty) as knitting_qnty","inv_receive_master a, pro_grey_prod_entry_dtls b","a.id=b.mst_id and item_category=13 and a.entry_form=22 and a.receive_basis=9 and a.booking_id in($knit_id) and b.status_active=1 and b.is_deleted=0","knitting_qnty");
								}*/
								
								$balance_qnty=$row[csf('program_qnty')]-$knitting_qnty;
								$balance_recv_qnty=$knitting_qnty-$knitting_recv_qnty;
								$yarn_issue_bl_qnty=$row[csf('program_qnty')]-$yarn_issue_qnty;
								
								if(!in_array($machine_dia_gg, $machine_dia_gg_array))
								{
									if ($k!=1)
									{
									?>
										<tr bgcolor="#CCCCCC">
											<td colspan="<?php echo $colspan; ?>" align="right"><b>Sub Total</b></td>
											<td align="right"><b><?php echo number_format($sub_tot_program_qnty,2,'.',''); ?></b></td>
                                            <td align="right"><b><?php echo number_format($sub_yarn_issue_qnty,2,'.',''); ?></b></td>
                                            <td align="right"><b><?php echo number_format($sub_yarn_issue_bl_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($sub_tot_knitting_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($sub_tot_balance,2,'.',''); ?></b></td>
                                            <td align="right"><b><?php echo number_format($sub_tot_knitting_recv_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($sub_tot_recv_balance,2,'.',''); ?></b></td>
											<td>&nbsp;</td>
                                            <td>&nbsp;</td>
										</tr>
									<?php
										$sub_tot_program_qnty = 0;
										$sub_tot_knitting_qnty = 0;
										$sub_tot_balance = 0;
										$sub_yarn_issue_qnty=0;
										$sub_yarn_issue_bl_qnty=0;
									}
									
								?>
									<tr bgcolor="#EFEFEF">
										<td colspan="<?php echo $colspan+9; ?>"><b>Machine Dia:- <?php echo $machine_dia_gg; ?></b></td>
									</tr>
								<?php
									$machine_dia_gg_array[]=$machine_dia_gg;
									$k++;
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
                                	<td width="40" align="center" valign="middle">
                                        <input type="checkbox" id="tbl_<?php echo $i;?>" onClick="selected_row(<?php echo $i; ?>);" />
                                        <input id="promram_id_<?php echo $i;?>" name="promram_id[]" type="hidden" value="<?php echo $row[csf('id')]; ?>" />
                                        <input id="job_no_<?php echo $i;?>" name="job_no[]" type="hidden" value="<?php echo $job_no; ?>" />
                                    </td> 
									<td width="40"><?php echo $i; ?></td>
                                    <?php if($type==3) echo "<td width='100'><a href='##' onclick=\"generate_report(".$row[csf('company_id')].",".$row[csf('id')].")\">".$supplier_details[$row[csf('knitting_party')]]."</a></td>"; ?>
                                    <td width="60" align="center"><?php echo $row[csf('id')]; ?>&nbsp;</td>
									<td width="80" align="center"><?php echo change_date_format($row[csf('program_date')]); ?>&nbsp;</td>
									<td width="80" align="center"><?php echo change_date_format($row[csf('start_date')]); ?>&nbsp;</td>
									<td width="80" align="center"><?php echo change_date_format($row[csf('end_date')]); ?>&nbsp;</td>
									<td width="80"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
									<td width="130">
                                    	<p>
											<?php if($type==3) echo $po_no; else echo "<a href='##' onclick=\"generate_report(".$row[csf('company_id')].",".$row[csf('id')].")\">$po_no</a>"; ?>
                                    	</p>
                                    </td>
									<td width="110"><p><?php echo implode(",",array_unique(explode(",",$plan_details_array[$row[csf('id')]]))); ?></p></td>
									<td width="120"><p><?php echo $style_ref; ?></p></td>
									<td width="70" align="center"><?php echo $reqsDataArr[$row[csf('id')]]['reqs_no']; ?>&nbsp;</td>
									<td width="80"><p><?php echo $machine_dia_gg; ?></p></td>
                                    <td align="right" width="100"><?php echo number_format($row[csf('distribution_qnty')],2); ?></td>
                                    <td width="80"><p><?php echo $machine_no; ?></p></td>
                                    <td width="70"><p><?php echo $knitting_program_status[$row[csf('status')]]; ?></p></td>
                                    <td width="140"><p><?php echo $row[csf('fabric_desc')]; ?></p></td>
                                    <td width="170"><p><?php echo join(",",array_unique($yarn_desc)); ?></p></td>
                    				<td width="70"><p><?php echo join(",",array_unique($lot)); ?></p></td>
                                    <td width="100"><p><?php echo $color_range[$row[csf('color_range')]] ?>&nbsp;</p></td>
                                    <td width="100"><p><?php echo $row[csf('stitch_length')]; ?>&nbsp;</p></td>
                                    <td width="100"><p><?php echo $row[csf('spandex_stitch_length')]; ?>&nbsp;</p></td>
                                    <td align="right" width="100"><?php echo number_format($row[csf('draft_ratio')],2); ?>&nbsp;</td>
									<td width="70"><p><?php echo $row[csf('gsm_weight')]; ?>&nbsp;</p></td>
									<td width="70"><p><?php echo $row[csf('dia')]; ?>&nbsp;</p></td>
									<td width="80"><?php echo $fabric_typee[$row[csf('width_dia_type')]]; ?>&nbsp;</td>
									<td align="right" width="100"><?php echo number_format($row[csf('program_qnty')],2); ?></td>
                                    <td align="right" width="100"><?php echo number_format($yarn_issue_qnty,2); ?></td>
                                    <td align="right" width="100"><?php echo number_format($yarn_issue_bl_qnty,2); ?></td>
                                    <td align="right" width="100"><?php echo number_format($knitting_qnty,2); ?></td>
                                    <td align="right" width="100"><?php echo number_format($balance_qnty,2); ?></td>
                                    <td align="right" width="100"><?php echo number_format($knitting_recv_qnty,2); ?></td>
                                    <td align="right" width="100"><?php echo number_format($balance_recv_qnty,2); ?></td>
                                    <td align="center" width="80">Complete</td>
									<td><p><?php echo $row[csf('remarks')]; ?>&nbsp;</p></td>
								</tr>
								<?php
								
								$sub_tot_program_qnty+=$row[csf('program_qnty')];
								$sub_tot_knitting_qnty+=$knitting_qnty;
								$sub_tot_balance+=$balance_qnty;
								$sub_tot_knitting_recv_qnty+=$knitting_recv_qnty;
								$sub_tot_recv_balance+=$balance_recv_qnty;
								$sub_yarn_issue_qnty+=$yarn_issue_qnty;
								$sub_yarn_issue_bl_qnty+=$yarn_issue_bl_qnty;
								
								$tot_program_qnty+=$row[csf('program_qnty')];
								$tot_knitting_qnty+=$knitting_qnty;
								$tot_balance+=$balance_qnty;
								$tot_knitting_recv_qnty+=$knitting_recv_qnty;
								$tot_balance_recv_qnty+=$balance_recv_qnty;
								$tot_yarn_issue_qnty+=$yarn_issue_qnty;
								$tot_yarn_issue_bl_qnty+=$yarn_issue_bl_qnty;
								
								$i++;
							}
							if($i>1)
                            {
                            ?>
                                <tr bgcolor="#CCCCCC">
                                    <td colspan="<?php echo $colspan; ?>" align="right"><b>Sub Total</b></td>
                                    <td align="right"><b><?php echo number_format($sub_tot_program_qnty,2,'.',''); ?></b></td>
                                    <td align="right"><b><?php echo number_format($sub_yarn_issue_qnty,2,'.',''); ?></b></td>
                                    <td align="right"><b><?php echo number_format($sub_yarn_issue_bl_qnty,2,'.',''); ?></b></td>
                                    <td align="right"><b><?php echo number_format($sub_tot_knitting_qnty,2,'.',''); ?></b></td>
                                    <td align="right"><b><?php echo number_format($sub_tot_balance,2,'.',''); ?></b></td>
                                    <td align="right"><b><?php echo number_format($sub_tot_knitting_recv_qnty,2,'.',''); ?></b></td>
									<td align="right"><b><?php echo number_format($sub_tot_recv_balance,2,'.',''); ?></b></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            <?php
                            }
                        	?>
                    </tbody>
                    <tfoot>
                        <th colspan="<?php echo $colspan; ?>" align="right">Grand Total</th>
                        <th align="right"><?php echo number_format($tot_program_qnty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($tot_yarn_issue_qnty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($tot_yarn_issue_bl_qnty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($tot_knitting_qnty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($tot_balance,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($tot_knitting_recv_qnty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($tot_balance_recv_qnty,2,'.',''); ?></th>
                        <th></th>
                        <th></th>
                    </tfoot>
                </table>
			</div>
      	</fieldset>      
	<?php
	}
	

	foreach (glob("$user_name*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_name."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename="requires/".$user_name."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
 	
}

if($action=="print")
{
	echo load_html_head_contents("Program Qnty Info", "../../", 1, 1,'','','');
	extract($_REQUEST);	
	$data = explode('*',$data);
	$company_id = $data[0];
	$program_id = $data[1];
	
	$company_details=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$country_arr=return_library_array( "select id,country_name from lib_country",'id','country_name');
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
	$buyer_details=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );

	
	if($db_type==0)
	{
		$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_id group by dtls_id", "dtls_id", "po_id"  );
	}
	else
	{
		$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_id group by dtls_id", "dtls_id", "po_id"  );
	}
	
	$po_dataArray=sql_select("select id, po_number, job_no_mst from wo_po_break_down");
	foreach($po_dataArray as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no_mst')];
	}
	
	$product_details_array=array();
	$sql="select id, supplier_id, lot, current_stock, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color, brand from product_details_master where item_category_id=1 and company_id=$company_id and status_active=1 and is_deleted=0";
	$result = sql_select($sql);
	
	foreach($result as $row)
	{
		$compos='';
		if($row[csf('yarn_comp_percent2nd')]!=0)
		{
			$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
		}
		else
		{
			$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
		}
		
		$product_details_array[$row[csf('id')]]['count']=$count_arr[$row[csf('yarn_count_id')]];
		$product_details_array[$row[csf('id')]]['comp']=$compos;
		$product_details_array[$row[csf('id')]]['type']=$yarn_type[$row[csf('yarn_type')]];
		$product_details_array[$row[csf('id')]]['lot']=$row[csf('lot')];
		$product_details_array[$row[csf('id')]]['brand']=$brand_arr[$row[csf('brand')]];
		$product_details_array[$row[csf('id')]]['color']=$row[csf('color')];
	}
	
	?>
	<div style="margin-left:20px; width:850px">
    	<div style="width:100px;float:left;position:relative;margin-top:10px">
        	<?php $image_location=return_field_value("image_location","common_photo_library","master_tble_id=$company_id and form_name='company_details' and is_deleted=0"); ?>
            <img src='../../<?php echo $image_location; ?>' height='100%' width='100%' />
        </div>
        <div style="width:50px;float:left;position:relative;margin-top:10px"></div>
    	<div style="width:710px;float:left;position:relative;">   
        <table width="100%" style="margin-top:10px">
            <tr>
                <td align="center" style="font-size:16px;">
                  <?php      
                        echo $company_details[$company_id];
                  ?>
                </td>
            </tr>
            <tr>
                <td align="center" style="font-size:14px">  
                <?php
                $nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$company_id"); 
                foreach ($nameArray as $result)
                { 
                ?>
                    Plot No: <?php echo $result['plot_no']; ?> 
                    Level No: <?php echo $result['level_no']?>
                    Road No: <?php echo $result['road_no']; ?> 
                    Block No: <?php echo $result['block_no'];?> 
                    City No: <?php echo $result['city'];?> 
                    Zip Code: <?php echo $result['zip_code']; ?> 
                    Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
                    Email Address: <?php echo $result['email'];?> 
                    Website No: <?php echo $result['website'];
                }
                ?>   
               </td> 
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td width="100%" align="center" style="font-size:14px;"><b><u>Knitting Program</u></b></td>
            </tr>
        </table>
        </div>
	</div>
    <div style="margin-left:10px;float:left; width:850px">
        <?php 
			$dataArray=sql_select("select id, mst_id, knitting_source, knitting_party, program_date, color_range, stitch_length,spandex_stitch_length, feeder, machine_dia, machine_gg, program_qnty, remarks from ppl_planning_info_entry_dtls where id=$program_id"); 
			
			$mst_dataArray=sql_select("select booking_no, buyer_id, fabric_desc, gsm_weight, dia from ppl_planning_info_entry_mst where id=".$dataArray[0][csf('mst_id')]); 
			$booking_no=$mst_dataArray[0][csf('booking_no')];
			$buyer_id=$mst_dataArray[0][csf('buyer_id')];
			$fabric_desc=$mst_dataArray[0][csf('fabric_desc')];
			$gsm_weight=$mst_dataArray[0][csf('gsm_weight')];
			$dia=$mst_dataArray[0][csf('dia')];
		?>
        <table width="100%" style="margin-top:20px" cellspacing="7">
            <tr>
                <td width="140"><b>Program No:</b></td><td width="170"><?php echo $dataArray[0][csf('id')]; ?></td>
                <td width="170"><b>Program Date:</b></td><td><?php echo change_date_format($dataArray[0][csf('program_date')]); ?></td>
            </tr>
            <tr>
                <td><b>Factory:</b></td>
                <td>
                    <?php 
                        if($dataArray[0][csf('knitting_source')]==1) echo $company_details[$dataArray[0][csf('knitting_party')]]; 
                        else if($dataArray[0][csf('knitting_source')]==3) echo $supplier_details[$dataArray[0][csf('knitting_party')]];
                    ?>
                </td>
                <td><b>Fabrication & FGSM:</b></td><td><?php echo $fabric_desc." & ".$gsm_weight; ?></td>
            </tr>
            <tr>
            	<td><b>Address:</b></td>
                <td colspan="3">
                    <?php 
						$address='';
                        if($dataArray[0][csf('knitting_source')]==1)
						{
							$addressArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,city from lib_company where id=$company_id");
							foreach ($nameArray as $result)
							{ 
							?>
								Plot No: <?php echo $result['plot_no']; ?> 
								Level No: <?php echo $result['level_no']?>
								Road No: <?php echo $result['road_no']; ?> 
								Block No: <?php echo $result['block_no'];?> 
								City No: <?php echo $result['city'];?> 
								Country: <?php echo $country_arr[$result['country_id']]; 
							}
						}
                        else if($dataArray[0][csf('knitting_source')]==3)
						{
							$address=return_field_value("address_1","lib_supplier","id=".$dataArray[0][csf('knitting_party')]);
							echo $address;
						}
                    ?>
                </td>
            </tr>
            <tr>
                <td><b>Buyer Name:</b></td>
                <td>
                    <?php 
                        echo $buyer_details[$buyer_id];
						
						$po_id=array_unique(explode(",",$plan_details_array[$dataArray[0][csf('id')]]));
						$po_no=''; $job_no=''; 

						foreach($po_id as $val)
						{
							if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
							if($job_no=='') $job_no=$po_array[$val]['job_no'];
						}
								
                    ?>
                </td>
                <td><b>Order No:</b></td><td><?php echo $po_no; ?></td>
            </tr>
            <tr>
                <td><b>Booking No:</b></td><td><b><?php echo $booking_no; ?></b></td>
                <td><b>Job No:</b></td><td><b><?php echo $job_no; ?></b></td>
            </tr>
                <tr>   
                    <td><b>Style Ref :</b></td>
                    <td><?php 
					if($job_no!='')
					{
						$style_val=return_field_value("style_ref_no","wo_po_details_master","job_no='$job_no'","style_ref_no");
					}
					
					echo $style_val; ?></td>
                </tr>
        </table>
        
        <table style="margin-top:10px;" width="850" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="80">Requisition No</th>
                <th width="80">Lot No</th>
                <th width="220">Yarn Description</th>
                 <th width="100">Color</th>
                <th width="110">Brand</th>
                <th width="80">Requisition Qnty</th>
                <th>No of Cone</th>
            </thead>
            <?php
				$i=1; $tot_reqsn_qnty=0;
				$sql="select requisition_no, prod_id,no_of_cone, yarn_qnty from ppl_yarn_requisition_entry where knit_id='".$dataArray[0][csf('id')]."' and status_active=1 and is_deleted=0";
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					?>
                    <tr>
                        <td width="40" align="center"><?php echo $i; ?></td>
                        <td width="80">&nbsp;&nbsp;<?php echo $selectResult[csf('requisition_no')]; ?></td>
                        <td width="80">&nbsp;&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['lot']; ?></td>
                        <td width="220">&nbsp;&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['count']." ".$product_details_array[$selectResult[csf('prod_id')]]['comp']." ".$product_details_array[$selectResult[csf('prod_id')]]['type']; ?></td>
                         <td width="100">&nbsp;&nbsp;<?php  echo $color_library[$product_details_array[$selectResult[csf('prod_id')]]['color']]; ?></td>
                        <td width="110">&nbsp;&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['brand']; ?></td>
                        <td width="80" align="right"><?php echo number_format($selectResult[csf('yarn_qnty')],2); ?>&nbsp;&nbsp;</td>
                        <td align="right"><?php echo number_format($selectResult[csf('no_of_cone')]); ?></td>	
                    </tr>
					<?php
					$tot_reqsn_qnty+=$selectResult[csf('yarn_qnty')];
					$i++;
				}
			?>
            <tfoot>
                <th colspan="6" align="right"><b>Total</b></th>
                <th align="right"><?php echo number_format($tot_reqsn_qnty,2); ?>&nbsp;&nbsp;</th>
                <th>&nbsp;</th>
            </tfoot>
        </table>
        <table width="850" cellpadding="0" cellspacing="0" border="1" rules="all" style="margin-top:20px;" class="rpt_table">
			<tr>
                <td width="100">&nbsp;&nbsp;<b>Colour:</b></td>
                <td width="120">&nbsp;&nbsp;<?php echo $color_range[$dataArray[0][csf('color_range')]]; ?></td>
                <td width="100">&nbsp;&nbsp;<b>GGSM OR S/L:</b></td>
                <td width="120">&nbsp;&nbsp;<?php echo $dataArray[0][csf('stitch_length')]; ?></td>
                <td width="100">&nbsp;&nbsp;<b>Spandex S/L:</b></td>
                <td width="110">&nbsp;&nbsp;<?php echo $dataArray[0][csf('spandex_stitch_length')]; ?></td>
             
                <td width="100">&nbsp;&nbsp;<b>FGSM:</b></td>
                <td>&nbsp;&nbsp;<?php echo $gsm_weight; ?></td>
            </tr>
        </table>
        <table style="margin-top:20px;" width="850" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th width="100">Finish Dia</th>
                <th width="230">Machine Dia & Gauge</th>
                 <th width="80">Feeder</th>
                 <th width="110">Program Qnty</th>
                
               
                <th>Remarks</th>
            </thead>
            <tr>
                <td width="100">&nbsp;&nbsp;<?php echo $dia; ?></td>
                <td width="230">&nbsp;&nbsp;<?php echo $dataArray[0][csf('machine_dia')]."X".$dataArray[0][csf('machine_gg')]; ?></td>
                 <td width="80">&nbsp;&nbsp;<?php echo $feeder[$dataArray[0][csf('feeder')]]; ?></td>
                <td width="110" align="right">&nbsp;&nbsp;<?php echo number_format( $dataArray[0][csf('program_qnty')],2); ?>&nbsp;&nbsp;</td>
                <td><?php echo $dataArray[0][csf('remarks')]; ?></td>	
            </tr>
            <tr height="70" valign="middle">
            	<td colspan="5"><b>Advice:</b></td>
            </tr>
        </table>
        <table width="850"> 
        	<tr>
				<td width="100%" height="90" colspan="5"></td>
			</tr> 
            <tr>
				<td width="25%" align="center"><strong style="text-decoration:overline">Checked By</strong></td>
                <td width="25%" align="center"><strong style="text-decoration:overline">Store Incharge</strong></td>
                <td width="25%" align="center"><strong style="text-decoration:overline">Knitting Manager</strong></td>
                <td width="25%" align="center"><strong style="text-decoration:overline">Authorised By</strong></td>
			</tr> 
        </table>
    </div>
<?php 
exit();   
}

if($action=="requisition_print")
{
	echo load_html_head_contents("Program Qnty Info", "../../", 1, 1,'','','');
	extract($_REQUEST);	
	
	$program_ids = $data;
	
	$company_details=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
	if($db_type==0)
	{
		$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where dtls_id in($program_ids) group by dtls_id", "dtls_id", "po_id" );
	}
	else
	{
		$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where dtls_id in($program_ids) group by dtls_id", "dtls_id", "po_id"  );
	}
	
	
	$po_dataArray=sql_select("select id, po_number, job_no_mst from wo_po_break_down");
	foreach($po_dataArray as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no_mst')];
	}
	
	$product_details_array=array();
	$sql="select id, supplier_id, lot, current_stock, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color, brand from product_details_master where item_category_id=1 and status_active=1 and is_deleted=0";
	$result = sql_select($sql);
	
	foreach($result as $row)
	{
		$compos='';
		if($row[csf('yarn_comp_percent2nd')]!=0)
		{
			$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
		}
		else
		{
			$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
		}
		
		$product_details_array[$row[csf('id')]]['desc']=$count_arr[$row[csf('yarn_count_id')]]." ".$compos." ".$yarn_type[$row[csf('yarn_type')]];
		$product_details_array[$row[csf('id')]]['lot']=$row[csf('lot')];
		$product_details_array[$row[csf('id')]]['brand']=$brand_arr[$row[csf('brand')]];
		$product_details_array[$row[csf('id')]]['color']=$color_library[$row[csf('color')]];
	}
	
	
	$knit_id_array=array(); $prod_id_array=array(); $rqsn_array=array();
	$reqsn_dataArray=sql_select("select knit_id, requisition_no, prod_id,sum(no_of_cone) as no_of_cone , sum(yarn_qnty) as yarn_qnty from ppl_yarn_requisition_entry where knit_id in($program_ids) and status_active=1 and is_deleted=0 group by knit_id, prod_id, requisition_no");
	foreach($reqsn_dataArray as $row)
	{
		$prod_id_array[$row[csf('knit_id')]][$row[csf('prod_id')]]=$row[csf('yarn_qnty')];
		$knit_id_array[$row[csf('knit_id')]].=$row[csf('prod_id')].",";
		$rqsn_array[$row[csf('prod_id')]]['reqsn'].=$row[csf('requisition_no')].",";
		$rqsn_array[$row[csf('prod_id')]]['qnty']+=$row[csf('yarn_qnty')];
		$rqsn_array[$row[csf('prod_id')]]['no_of_cone']+=$row[csf('no_of_cone')];
	}
	
	$order_no=''; $buyer_name=''; $knitting_factory=''; $job_no=''; $booking_no=''; $company='';
	if($db_type==0)
	{
		$dataArray=sql_select("select a.id, a.knitting_source, a.knitting_party, b.buyer_id, b.booking_no, b.company_id, group_concat(distinct(b.po_id)) as po_id from ppl_planning_info_entry_dtls a, ppl_planning_entry_plan_dtls b where a.id=b.dtls_id and a.id in ($program_ids) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.knitting_source, a.knitting_party, b.buyer_id, b.booking_no, b.company_id"); 
	}
	else
	{
		$dataArray=sql_select("select a.id, a.knitting_source, a.knitting_party, b.buyer_id, b.booking_no, b.company_id, LISTAGG(cast(b.po_id as varchar2(4000)), ',') WITHIN GROUP (ORDER BY b.po_id) as po_id from ppl_planning_info_entry_dtls a, ppl_planning_entry_plan_dtls b where a.id=b.dtls_id and a.id in ($program_ids) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.knitting_source, a.knitting_party, b.buyer_id, b.booking_no, b.company_id"); 	
	}
	foreach($dataArray as $row)
	{
		if($duplicate_arr[$row[csf('knitting_source')]][$row[csf('knitting_party')]]=="")
		{
			$duplicate_arr[$row[csf('knitting_source')]][$row[csf('knitting_party')]]=$row[csf('knitting_party')];
			
			if($row[csf('knitting_source')]==1) $knitting_factory.=$company_details[$row[csf('knitting_party')]].",";
			else if($row[csf('knitting_source')]==3) $knitting_factory.=$supplier_details[$row[csf('knitting_party')]].",";
		}
		
		if($buyer_name=="") $buyer_name=$buyer_arr[$row[csf('buyer_id')]];
		if($booking_no=="") $booking_no=$row[csf('booking_no')];
		if($company=="") $company=$company_details[$row[csf('company_id')]];
				
		$po_id=explode(",",$row[csf('po_id')]);
		
		foreach($po_id as $val)
		{
			$order_no.=$po_array[$val]['no'].",";
			if($job_no=="") $job_no=$po_array[$val]['job_no'];
		}
	}
	
	$order_no=array_unique(explode(",",substr($order_no,0,-1)));
	?>
    <div style="width:1200px; margin-left:5px">
        <table width="100%" style="margin-top:10px">
        	<tr>
                <td width="100%" align="center" style="font-size:20px;"><b><?php echo $company; ?></b></td>
            </tr>
            <tr>
                <td width="100%" align="center" style="font-size:20px;"><b><u>Knitting Program</u></b></td>
            </tr>
        </table>
        <div style="border:1px solid;margin-top:10px; width:950px">
            <table width="100%" cellpadding="2" cellspacing="5">
                <tr>
                    <td width="140"><b>Knitting Factory </b></td>
                    <td>:</td>
                    <td><?php echo substr($knitting_factory,0,-1); ?></td>
                </tr>
                <tr>   
                    <td><b>Buyer Name </b></td>
                    <td>:</td>
                    <td><?php echo $buyer_name; ?></td>
                </tr>
                <tr>   
                    <td><b>Style </b></td>
                    <td>:</td>    
                    <td><?php 
					if($job_no!='')
					{
						$style_val=return_field_value("style_ref_no","wo_po_details_master","job_no='$job_no'","style_ref_no");
					}
					
					echo $style_val; ?></td>
                </tr>
                <tr>   
                    <td><b>Order No </b></td>
                    <td>:</td>    
                    <td><?php echo implode(",",$order_no); ?></td>
                </tr>
                <tr>    
                    <td><b>Job No </b></td>
                    <td>:</td>
                    <td><?php echo $job_no; ?></td>
                </tr> 
                <tr>     
                    <td><b>Booking No </b></td>
                    <td>:</td>
                    <td><?php echo $booking_no; ?></td>
                </tr>
            </table>
        </div>
        <table width="950" style="margin-top:10px" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
             <thead>
                <th width="30">SL</th>
                <th width="100">Requisition No</th>
                <th width="100">Brand</th>
                <th width="100">Lot No</th>
                <th width="200">Yarn Description</th>
                <th width="100">Color</th>
                <th width="100">Requisition Qty.</th>
                <th>No Of Cone</th>
            </thead>
            <?php
			$j=1; $tot_reqsn_qty=0;
			foreach($rqsn_array as $prod_id=>$data)
			{
				if($j%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			?>
            	<tr bgcolor="<?php echo $bgcolor; ?>">
                	<td width="30"><?php echo $j; ?></td>
                    <td width="100"><?php echo substr($data['reqsn'],0,-1); ?></td>
                    <td width="100"><p><?php echo $product_details_array[$prod_id]['brand']; ?>&nbsp;</p></td>
                    <td width="100"><p><?php echo $product_details_array[$prod_id]['lot']; ?></p></td>
                    <td width="200"><p><?php echo $product_details_array[$prod_id]['desc']; ?></p></th>
                    <td width="100"><p><?php echo $product_details_array[$prod_id]['color']; ?>&nbsp;</p></td>
                    <td width="100" align="right"><p><?php echo number_format($data['qnty'],2,'.',''); ?></p></td>
                    <td align="right"><?php echo number_format($data['no_of_cone']); ?></td>
                </tr>
            <?php	
				$tot_reqsn_qty+=$data['qnty'];
				$tot_no_of_cone+=$data['no_of_cone'];
				$j++;
			}
			?>
            <tfoot>
            	<th colspan="6" align="right">Total</th>
                <th align="right"><?php echo number_format($tot_reqsn_qty,2,'.',''); ?></th>
                <th><?php echo number_format($tot_no_of_cone); ?></th>
            </tfoot>
        </table>
        
        <table style="margin-top:10px;" width="100%" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th width="25">SL</th>
                <th width="60">Program No & Date</th>
                <th width="120">Fabrication</th>
                <th width="50">GSM</th>
                <th width="50">F. Dia</th>
                <th width="60">Dia Type</th>
                <th width="50">S/L</th>
                <th width="50">Spandex S/L</th>
                <th width="50">Feeder</th>
                <th width="60">Color</th>
                <th width="60">Color Range</th>
                <th width="60">Machine No</th> 
                <th width="70">Machine Dia & GG</th>
                <th width="70">Knit Plan Date</th>
                <th width="70">Prpgram Qty.</th>
                <th width="110">Yarn Description</th>
                <th width="50">Lot</th>
                <th width="70">Yarn Qty.(KG)</th>
                <th>Remarks</th>
            </thead>
            <?php //stitch_length,spandex_stitch_length, feeder, machine_dia, machine_gg, program_qnty, remarks from ppl_planning_info_entry_dtls
               $i=1; $s=1; $tot_program_qnty=0; $tot_yarn_reqsn_qnty=0; $company_id='';
               $sql="select a.company_id, a.fabric_desc, a.gsm_weight, a.dia, a.width_dia_type, b.id as program_id, b.color_id, b.color_range, b.machine_dia, b.width_dia_type as diatype, b.machine_gg, b.fabric_dia, b.program_qnty, b.program_date, b.stitch_length,b.spandex_stitch_length,b.feeder, b.machine_id, b.start_date, b.end_date, b.remarks from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b where a.id=b.mst_id and b.id in($program_ids) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0";
                $nameArray=sql_select( $sql );
                foreach ($nameArray as $row)
                {
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					$color='';
					$color_id=explode(",",$row[csf('color_id')]);

					foreach($color_id as $val)
					{
						if($color=='') $color=$color_library[$val]; else $color.=",".$color_library[$val];
					}
					
					if($company_id=='') $company_id=$row[csf('company_id')];
					
					$machine_no='';
					$machine_id=explode(",",$row[csf('machine_id')]);

					foreach($machine_id as $val)
					{
						if($machine_no=='') $machine_no=$machine_arr[$val]; else $machine_no.=",".$machine_arr[$val];
					}
					
					if($knit_id_array[$row[csf('program_id')]]!="")
					{
						$all_prod_id=explode(",",substr($knit_id_array[$row[csf('program_id')]],0,-1));
						$row_span=count($all_prod_id);
						$z=0;
						foreach($all_prod_id as $prod_id)
						{
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>">
								<?php
                                if($z==0)
                                {
                                ?>
                                    <td width="25" rowspan="<?php echo $row_span; ?>"><?php echo $i; ?></td>
                                    <td width="60" rowspan="<?php echo $row_span; ?>" align="center"><?php echo $row[csf('program_id')].'<br>'.change_date_format($row[csf('program_date')]); ?></td>
                                    <td width="120" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('fabric_desc')]; ?></p></td>
                                    <td width="50" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('gsm_weight')]; ?></p></th>
                                    <td width="50" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('fabric_dia')]; ?></p></td>
                                    <td width="60" rowspan="<?php echo $row_span; ?>"><p><?php echo $fabric_typee[$row[csf('diatype')]]; ?></p></td>
                                    <td width="50" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('stitch_length')]; ?></p></td>
                                    <td width="50" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('spandex_stitch_length')]; ?></p></td>
                                    <td width="50" rowspan="<?php echo $row_span; ?>"><p><?php echo $feeder[$row[csf('feeder')]]; ?></p></td>
                                    <td width="60" rowspan="<?php echo $row_span; ?>"><p><?php echo $color; ?></p></td>
                                    <td width="60" rowspan="<?php echo $row_span; ?>"><p><?php echo $color_range[$row[csf('color_range')]]; ?></p></td>
                                    <td width="60" rowspan="<?php echo $row_span; ?>"><p><?php echo $machine_no; ?></p></td> 
                                    <td width="70" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('machine_dia')]."X".$row[csf('machine_gg')]; ?></p></td>
                                    <td width="70" rowspan="<?php echo $row_span; ?>"><?php echo change_date_format($row[csf('start_date')])." to ".change_date_format($row[csf('end_date')]); ?></td>
                                    <td width="70" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($row[csf('program_qnty')],2,'.',''); ?>&nbsp;</td>
                                <?php
									$tot_program_qnty+=$row[csf('program_qnty')];
									$i++;
                                }
                                ?>
								<td width="110"><p><?php echo $product_details_array[$prod_id]['desc']; ?>&nbsp;</p></td>
								<td width="50"><p><?php echo $product_details_array[$prod_id]['lot']; ?>&nbsp;</p></td>
								<td width="70" align="right"><?php echo number_format($prod_id_array[$row[csf('program_id')]][$prod_id],2,'.',''); ?></td>
                                <?php
                                if($z==0)
                                {
                                ?>
									<td rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('remarks')]; ?>&nbsp;</p></td>
                                <?php
                                }
                                ?>
							</tr>
							<?php
							$tot_yarn_reqsn_qnty+=$prod_id_array[$row[csf('program_id')]][$prod_id];
							$z++;
						}
					}
					else
					{
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="25"><?php echo $i; ?></td>
							<td width="60" align="center"><?php echo $row[csf('program_id')].'<br>'.change_date_format($row[csf('program_date')]); ?></td>
							<td width="120"><p><?php echo $row[csf('fabric_desc')]; ?></p></td>
							<td width="50"><p><?php echo $row[csf('gsm_weight')]; ?></p></th>
							<td width="50"><p><?php echo $row[csf('fabric_dia')]; ?></p></td>
							<td width="60"><p><?php echo $fabric_typee[$row[csf('diatype')]]; ?></p></td>
							<td width="50"><p><?php echo $row[csf('stitch_length')]; ?></p></td>
                            <td width="50"><p><?php echo $row[csf('spandex_stitch_length')]; ?></p></td>
                            <td width="50"><p><?php echo $feeder[$row[csf('feeder')]]; ?></p></td>
							<td width="60"><p><?php echo $color; ?></p></td>
							<td width="60"><p><?php echo $color_range[$row[csf('color_range')]]; ?></p></td>
							<td width="60"><p><?php echo $machine_no; ?></p></td> 
							<td width="70"><p><?php echo $row[csf('machine_dia')]."X".$row[csf('machine_gg')]; ?></p></td>
							<td width="70"><?php echo change_date_format($row[csf('start_date')])." to ".change_date_format($row[csf('end_date')]); ?></td>
							<td width="70" align="right"><?php echo number_format($row[csf('program_qnty')],2,'.',''); ?>&nbsp;</td>
							<td width="110"><p>&nbsp;</p></td>
							<td width="50"><p>&nbsp;</p></td>
							<td width="70" align="right">&nbsp;</td>
							<td><p><?php echo $row[csf('remarks')]; ?>&nbsp;</p></td>
						</tr>
						<?php
						$tot_program_qnty+=$row[csf('program_qnty')];
						$i++;
					}
                }
            ?>
            <tfoot>
                <th colspan="14" align="right"><b>Total</b></th>
                <th align="right"><?php echo number_format($tot_program_qnty,2,'.',''); ?>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th align="right"><?php echo number_format($tot_yarn_reqsn_qnty,2,'.',''); ?></th>
                <th>&nbsp;</th>
            </tfoot>
        </table>
        <br>
        <?php
		$sql_strip="select a.color_number_id, a.stripe_color, a.measurement, a.uom, b.dtls_id, b.no_of_feeder as no_of_feeder from wo_pre_stripe_color a, ppl_planning_feeder_dtls b where a.pre_cost_fabric_cost_dtls_id=b.pre_cost_id and a.color_number_id=b.color_id and a.stripe_color=b.stripe_color_id and b.dtls_id in($program_ids) and b.no_of_feeder>0 and a.status_active=1 and a.is_deleted=0";
		$result_stripe = sql_select($sql_strip);
		if (count($result_stripe)>0)
		{
		?>
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
				<thead>
					<tr>
						<th colspan="7">Stripe Measurement</th>
					</tr>
					<tr>
						<th width="30">SL</th>
						<th width="60">Prog. no</th>
						<th width="140">Color</th> 
						<th width="130">Stripe Color</th> 
						<th width="70">Measurement</th>               
						<th width="50">UOM</th>
						<th>No Of Feeder</th> 
					</tr>
				</thead>
				<?php
					$i=1; $tot_feeder=0;
					foreach($result_stripe as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$tot_feeder+=$row[csf('no_of_feeder')];
						?>
						<tr valign="middle" bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>"> 
							<td width="30" align="center"><?php echo $i; ?></td>
							<td width="50" align="center"><?php echo $row[csf('dtls_id')]; ?></td> 	
							<td width="140"><p><?php echo $color_library[$row[csf('color_number_id')]]; ?></p></td> 
							<td width="130"><p><?php echo $color_library[$row[csf('stripe_color')]]; ?></p></td>     
							<td width="70" align="center"><?php echo $row[csf('measurement')]; ?></td>               
							<td width="50" align="center"><p><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></p></td>
							<td align="right" style="padding-right:10px"><?php echo $row[csf('no_of_feeder')]; ?>&nbsp;</td>
						</tr>
					<?php
					$tot_masurement+=$row[csf('measurement')];
					$i++;
					}
				?>
				</tbody>
				<tfoot>
					<th colspan="4">Total</th>
					<th>&nbsp;</th> 
					<th>&nbsp;</th>
					<th style="padding-right:10px"><?php echo $tot_feeder; ?>&nbsp;</th> 
				</tfoot>
			</table>          
		<?php
        }
        echo signature_table(41, $company_id, "1180px");
        ?>
    </div>
<?php 
exit();   
}
?>