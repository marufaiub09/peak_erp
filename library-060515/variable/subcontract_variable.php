<?
/*-------------------------------------------- Comments
Purpose			: 	This form will create Dyeing & Finishing Bill Variable Settings
					Select company and select Variable List that onchange will change content
					
Functionality	:	Must fill Company, Variable List
JS Functions	:
Created by		:	Kausar 
Creation date 	: 	09-08-2014
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Subcontract Variable Settings", "../../", 1, 1,$unicode,'','');
?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<? echo $permission; ?>';

	function fnc_subcontract_variable_settings(operation)
	{
		if(operation==2)
		{
			show_msg('13');
			return;
		}
		
		if ( form_validation('cbo_company_id*cbo_variable_list','Company Name*Variable List')==0 )
		{
			return;
		}
		else
		{
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_id*cbo_variable_list*cbo_bill_on*update_id',"../../");
			//freeze_window(operation);
			http.open("POST","requires/subcontract_variable_controller.php", true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_subcontract_variable_settings_reponse;
		}
	 }

	function fnc_subcontract_variable_settings_reponse()
	{
		if(http.readyState == 4) 
		{
			//alert(http.responseText);
			var reponse=trim(http.responseText).split('**');
			//if (reponse[0].length>2) reponse[0]=10;
			show_msg(reponse[0]);
			document.getElementById('update_id').value  = reponse[2];
			set_button_status(0, permission, 'fnc_subcontract_variable_settings',1);
			reset_form('subcontractVariable','variable_settings_container','');
			release_freezing();
		}
	}	

</script>
</head>
<body  onLoad="set_hotkey()">
	<div align="center" style="width:100%;">
		<? echo load_freeze_divs ("../../",$permission);  ?>
        <fieldset style="width:850px;">
            <legend>Subcontract Variable Settings</legend>
            <form name="subcontractVariable" id="subcontractVariable" >
                <table  width="850px" cellspacing="2" cellpadding="0" border="0">
                    <tr>
                        <td width="150" align="center" class="must_entry_caption">Company</td>
                        <td width="300">
							<? 
								echo create_drop_down( "cbo_company_id", 250, "select company_name,id from lib_company where is_deleted=0 and status_active=1 $company_name order by company_name",'id,company_name', 1, '---Select Company---', 0, "" );
                            ?>
                        </td> 
                        <td width="150" align="center" class="must_entry_caption">Variable List</td>
                        <td width="300">
							<? 
								echo create_drop_down( "cbo_variable_list", 250, $subcon_variable,'', '1', '---Select---', '',"show_list_view(this.value+'_'+document.getElementById('cbo_company_id').value,'on_change_data','variable_settings_container','requires/subcontract_variable_controller','')",''); 
                            ?>
                        </td>
                    </tr>
                </table>
                 <div style="width:895px; float:left; min-height:40px; margin:auto" align="center" id="variable_settings_container"></div>
            </form>
        </fieldset>
	</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
