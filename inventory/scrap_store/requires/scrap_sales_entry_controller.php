<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$user_id=$_SESSION['logic_erp']['user_id'];

 //-------------------START ----------------------------------------
$company_arr = return_library_array("select id, company_name from lib_company","id","company_name");
$supplier_arr = return_library_array("select id, short_name from lib_supplier","id","short_name");
$brand_arr = return_library_array("select id, brand_name from lib_brand","id","brand_name");

if ($action=="itemDescription_popup")
{
	echo load_html_head_contents("Item Description Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 
	<script>
		function js_set_value(id)
		{
			//alert (id);
			$('#hidden_prod_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
	<input type="hidden" name="hidden_prod_id" id="hidden_prod_id" />
	<?php
	$sales_qty_arr=array();
	$sql_qty="select prod_id, rej_uom, sum(sales_qty) as sales_qty from inv_scrap_sales_dtls where is_deleted=0 and status_active=1 group by prod_id, rej_uom";
	$result_sql_qty = sql_select($sql_qty);
	foreach ($result_sql_qty as $row)
	{
		$sales_qty_arr[$row[csf('prod_id')]][$row[csf('rej_uom')]]=$row[csf('sales_qty')];
	}
	//echo $cbo_category_id;
	if($cbo_category_id==1)
	{
		$sql_yarn="select a.id, a.supplier_id, a.product_name_details, a.lot, a.unit_of_measure, a.brand, sum(b.cons_reject_qnty) as rej_qty from  product_details_master a, inv_transaction b, inv_receive_master c where c.company_id=$cbo_company_id and a.item_category_id=1 and b.item_category=1 and b.transaction_type=4 and c.item_category=1 and c.entry_form=9 and b.cons_reject_qnty!=0 and a.id=b.prod_id and b.mst_id=c.id and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.supplier_id, a.product_name_details, a.lot, a.unit_of_measure, a.brand order by a.id";
		$result_yarn = sql_select($sql_yarn); $i=1;
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="510" class="rpt_table">
            <thead>
                <th width="25">SL</th>
                <th width="50">Prod ID</th>
                <th width="150">Product Nmae</th>
                <th width="60">Lot</th>
                <th width="60">Brand</th>
                <th width="80">Supplier</th>
                <th>Reject Qty</th>                
            </thead>
            <?php
            $i=1;
            foreach ($result_yarn as $row)
            {  
				$sales_qty=$sales_qty_arr[$row[csf('id')]][$row[csf('unit_of_measure')]];
				$available_qty=$row[csf('rej_qty')]-$sales_qty;
				if($available_qty>0)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					//$data=
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick='js_set_value("<?php echo $row[csf('id')].'__'.$row[csf('product_name_details')].'__'.$row[csf('unit_of_measure')].'__'.$available_qty; ?>")' > 
							<td width="25"><?php echo $i; ?></td>
							<td width="50"><p><?php echo $row[csf('id')]; ?></p></td>  
							<td width="150"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
							<td width="60"><p><?php echo $row[csf('lot')]; ?></p></td>   
							<td width="60"><p><?php echo $brand_arr[$row[csf('brand')]]; ?></p></td>  
							<td width="80"><p><?php echo $supplier_arr[$row[csf('supplier_id')]]; ?></p></td>             
							<td align="right"><?php echo number_format($available_qty,2,'.',''); ?></td>
						</tr>
					<?php
					$i++;
				}
            }
            ?>
        </table>
	<?php
	}
	else if($cbo_category_id==2)
	{
		$sql_grey="select a.id, a.supplier_id, a.product_name_details, a.lot, a.unit_of_measure, a.brand, sum(b.cons_reject_qnty) as rej_qty from  product_details_master a, inv_transaction b, inv_receive_master c where c.company_id=$cbo_company_id and a.item_category_id=2 and b.item_category=2 and b.transaction_type=1 and c.item_category=2 and c.entry_form in (7,37) and b.cons_reject_qnty!=0 and a.id=b.prod_id and b.mst_id=c.id and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.supplier_id, a.product_name_details, a.lot, a.unit_of_measure, a.brand order by a.id";
		$result_grey = sql_select($sql_grey); $i=1;
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="510" class="rpt_table">
            <thead>
                <th width="25">SL</th>
                <th width="50">Prod ID</th>
                <th width="150">Product Nmae</th>
                <th width="60">Lot</th>
                <th width="60">Brand</th>
                <th width="80">Supplier</th>
                <th>Reject Qty</th>                
            </thead>
            <?php
            $i=1;
            foreach ($result_grey as $row)
            {  
				$sales_qty=$sales_qty_arr[$row[csf('id')]][$row[csf('unit_of_measure')]];
				$available_qty=$row[csf('rej_qty')]-$sales_qty;
				if($available_qty>0)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick='js_set_value("<?php echo $row[csf('id')].'__'.$row[csf('product_name_details')].'__'.'12'.'__'.$available_qty; ?>")' > 
							<td width="25"><?php echo $i; ?></td>
							<td width="50"><p><?php echo $row[csf('id')]; ?></p></td>  
							<td width="150"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
							<td width="60"><p><?php echo $row[csf('lot')]; ?></p></td>   
							<td width="60"><p><?php echo $brand_arr[$row[csf('brand')]]; ?></p></td>  
							<td width="80"><p><?php echo $supplier_arr[$row[csf('supplier_id')]]; ?></p></td>             
							<td align="right"><?php echo number_format($available_qty,2,'.',''); ?></td>
						</tr>
					<?php
                	$i++;
				}
            }
            ?>
        </table>
	<?php
	}
	else if($cbo_category_id==13)
	{
		$sql_grey="select a.id, a.supplier_id, a.product_name_details, a.lot, a.unit_of_measure, a.brand, sum(b.cons_reject_qnty) as rej_qty from  product_details_master a, inv_transaction b, inv_receive_master c where c.company_id=$cbo_company_id and a.item_category_id=13 and b.item_category=13 and b.transaction_type=1 and c.item_category=13 and c.entry_form in (2,22) and b.cons_reject_qnty!=0 and a.id=b.prod_id and b.mst_id=c.id and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.id, a.supplier_id, a.product_name_details, a.lot, a.unit_of_measure, a.brand order by a.id";
		$result_grey = sql_select($sql_grey); $i=1;
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="510" class="rpt_table">
            <thead>
                <th width="25">SL</th>
                <th width="50">Prod ID</th>
                <th width="150">Product Nmae</th>
                <th width="60">Lot</th>
                <th width="60">Brand</th>
                <th width="80">Supplier</th>
                <th>Reject Qty</th>                
            </thead>
            <?php
            $i=1;
            foreach ($result_grey as $row)
            {  
				$sales_qty=$sales_qty_arr[$row[csf('id')]][$row[csf('unit_of_measure')]];
				$available_qty=$row[csf('rej_qty')]-$sales_qty;
				if($available_qty>0)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick='js_set_value("<?php echo $row[csf('id')].'__'.$row[csf('product_name_details')].'__'.$row[csf('unit_of_measure')].'__'.$available_qty; ?>")' > 
							<td width="25"><?php echo $i; ?></td>
							<td width="50"><p><?php echo $row[csf('id')]; ?></p></td>  
							<td width="150"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
							<td width="60"><p><?php echo $row[csf('lot')]; ?></p></td>   
							<td width="60"><p><?php echo $brand_arr[$row[csf('brand')]]; ?></p></td>  
							<td width="80"><p><?php echo $supplier_arr[$row[csf('supplier_id')]]; ?></p></td>             
							<td align="right"><?php echo number_format($available_qty,2,'.',''); ?></td>
						</tr>
					<?php
                	$i++;
				}
            }
            ?>
        </table>
	<?php
	}
	else if($cbo_category_id==30)
	{
		$sql_gmts="select item_number_id, production_type, sum(reject_qnty) as reject_qnty from pro_garments_production_mst where status_active=1 and is_deleted=0 and reject_qnty!=0 group by item_number_id, production_type order by production_type, item_number_id";
		$result_gmts = sql_select($sql_gmts); $i=1;
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="510" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="50">Item ID</th>
                <th width="200">Item Name</th>
                <th width="120">Reject Type</th>                
                <th>Reject Qty.</th>
            </thead>
            <?php
            $i=1;
            foreach ($result_gmts as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				//$data=
                ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick='js_set_value("<?php echo $row[csf('item_number_id')]; ?>")' > 
                        <td width="40"><?php echo $i; ?></td>
                        <td width="50"><p><?php echo $row[csf('item_number_id')]; ?></p></td>  
                        <td width="200"><p><?php echo $garments_item[$row[csf('item_number_id')]]; ?></p></td> 
                        <td width="120"><p><?php echo $production_type[$row[csf('production_type')]]; ?></p></td>             
                        <td align="right"><?php echo number_format($row[csf('reject_qnty')],2,'.',''); ?></td>
                    </tr>
                <?php
                $i++;
            }
            ?>
        </table>
	<?php
	}
	exit();
}

if($action=="customer_search_popup")
{
	echo load_html_head_contents("Customer Name Info Popup", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 
	<script>
		function js_set_value(id,name)
		{
			//alert (id);
			$('#hidden_customer_id').val(id);
			$('#hidden_customer_no').val(name);
			parent.emailwindow.hide();
		}
    </script>
</head>
	<input type="hidden" name="hidden_customer_id" id="hidden_customer_id" />
    <input type="hidden" name="hidden_customer_no" id="hidden_customer_no" />
	<?php
		$sql_customer="select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (80)) order by buy.buyer_name";
		$result_customer = sql_select($sql_customer); $i=1;
		?>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="510" class="rpt_table">
            <thead>
                <th width="30">SL</th>
                <th width="80">Customer ID</th>
                <th>Customer Nmae</th>
            </thead>
            <?php
            $i=1;
            foreach ($result_customer as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick='js_set_value("<?php echo $row[csf('id')]; ?>","<?php echo $row[csf('buyer_name')]; ?>")' > 
                        <td width="30"><?php echo $i; ?></td>
                        <td width="80"><p><?php echo $row[csf('id')]; ?></p></td>  
                        <td><p><?php echo $row[csf('buyer_name')]; ?></p></td>
                    </tr>
                <?php
                $i++;
            }
            ?>
        </table>
	<?php
	exit();
}

if($action=="show_dtls_listview")
{
	?>	
	<div>
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="680" class="rpt_table">
            <thead>
                <th width="30">SL</th>
                <th width="50">Prod. ID</th>
                <th width="150">Item Description</th>
                <th width="50">Reject UOM</th>
                <th width="80">Sales Qty As Reject UOM</th>                    
                <th width="50">Sales UOM</th>
                <th width="80">Sales Qty</th>
                <th width="70">Sales Rate</th>
                <th>Amount</th>
            </thead>
		</table>
	<div style="width:680;max-height:180px; overflow-y:scroll" id="scrap_list_view" align="left">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="660" class="rpt_table">
		<?php 
			$product_name_arr = return_library_array("select id, product_name_details from product_details_master","id","product_name_details"); 
			$i=1;
			$sqlResult =sql_select("select id, prod_id, rej_uom, sales_qty_as_rej_uom, sales_uom, sales_qty, sales_rate, sales_amount from inv_scrap_sales_dtls where mst_id='$data' and status_active=1 and is_deleted=0 order by id Desc");
			
			foreach($sqlResult as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="get_php_form_data('<?php echo $row[csf('id')]; ?>','populate_scrap_form_data','requires/scrap_sales_entry_controller');" > 
                    <td width="30" align="center"><?php echo $i; ?></td>
                    <td width="50" align="center"><p><?php echo $row[csf('prod_id')]; ?></p></td>
                    <td width="150"><p><?php echo $product_name_arr[$row[csf('prod_id')]]; ?></p></td>
                    <td width="50" align="center"><?php echo $unit_of_measurement[$row[csf('rej_uom')]]; ?></td>
                    <td width="80" align="right"><?php echo number_format($row[csf('sales_qty_as_rej_uom')],2); ?></td>
                    <td width="50" align="right"><?php echo $unit_of_measurement[$row[csf('sales_uom')]]; ?></td>
                    <td width="80" align="right"><?php echo number_format($row[csf('sales_qty')],2); ?></td>
                    <td width="70" align="right"><?php echo number_format($row[csf('sales_rate')],2); ?></td>
                    <td align="right"><?php echo number_format($row[csf('sales_amount')],2); ?></td>
                </tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
    </div>
<?php
	exit();
}

if ($action=="populate_scrap_form_data")
{
	$product_name_arr = return_library_array("select id, product_name_details from product_details_master","id","product_name_details");
	$nameArray=sql_select( "select id, prod_id, trans_id, rej_uom, sales_qty_as_rej_uom, sales_uom, sales_qty, sales_rate, sales_amount from inv_scrap_sales_dtls where id='$data'");
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_prodid').value		= '".$row[csf("prod_id")]."';\n"; 
		echo "document.getElementById('trans_id').value			= '".$row[csf("trans_id")]."';\n"; 
		echo "document.getElementById('txt_itemdes').value		= '".$product_name_arr[$row[csf("prod_id")]]."';\n";
		echo "document.getElementById('cbo_rejuomid').value		= '".$row[csf("rej_uom")]."';\n";
		echo "document.getElementById('txt_rejqty').value		= '".$row[csf("sales_qty_as_rej_uom")]."';\n";  
		echo "document.getElementById('cbo_salesuomid').value	= '".$row[csf("sales_uom")]."';\n";
		echo "document.getElementById('txt_salesqty').value		= '".$row[csf("sales_qty")]."';\n";
		echo "document.getElementById('txt_salesrate').value	= '".$row[csf("sales_rate")]."';\n";
		echo "document.getElementById('txt_salesamount').value	= '".$row[csf("sales_amount")]."';\n";
		echo "document.getElementById('dtls_update_id').value	= '".$row[csf("id")]."';\n";
		
		echo "set_button_status(1,'".$_SESSION['page_permission']."', 'fnc_scrap_sales',1,1);\n";	
	}
	exit();	
}

if ($action=="scrap_sales_popup_search")
{
	echo load_html_head_contents("System Challan Info Popup", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<script>
		function js_set_value(id)
		{ 
			$("#hidden_mst_id").val(id);
			//document.getElementById('selected_job').value=id;
			parent.emailwindow.hide();
		}		
	</script>
	</head>
        <div align="center" style="width:100%;" >
            <form name="searchreceivefrm_1"  id="searchreceivefrm_1" autocomplete="off">
                <table width="700" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>
                    	<tr>                	 
                            <th width="150">Company Name</th>
                            <th width="150">Customer Name</th>
                            <th width="80">Sys Challan ID</th>
                            <th width="170">Selling Range</th>
                            <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>
                        </tr>         
                    </thead>
                    <tbody>
                        <tr>
                            <td>
								<?php 
									echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $company_id, ""); //load_drop_down( 'scrap_sales_entry_controller', this.value, 'load_drop_down_buyer_pop', 'buyer_td' ); ?>
                            </td>
                            <td id="buyer_td">
								<?php 
									echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (80)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $data[2], "" ); 
                                ?>
                            </td>
                            <td>
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes_numeric" style="width:75px" placeholder="Receive ID" />
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:65px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:65px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value, 'create_receive_search_list_view', 'search_div', 'scrap_sales_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" /></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" height="40" valign="middle">
								<?php echo load_month_buttons(1);  ?>
                                <input type="hidden" name="hidden_mst_id" id="hidden_mst_id" class="datepicker" style="width:70px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                    </tbody>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if($action=="create_receive_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company=" and company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $buyer_cond=" customer_id='$data[1]'"; else $buyer_cond="";
	
	if($db_type==0)
	{ 
		if ($data[2]!="" &&  $data[3]!="") $selling_date = "and selling_date between '".change_date_format($data[2],'yyyy-mm-dd')."' and '".change_date_format($data[3],'yyyy-mm-dd')."'"; else $selling_date ="";
	}
	else
	{
		if ($data[2]!="" &&  $data[3]!="") $selling_date = "and selling_date between '".change_date_format($data[2], "", "",1)."' and '".change_date_format($data[3], "", "",1)."'"; else $selling_date ="";
	}
	
	if ($data[4]!='') $sys_id_cond=" and challan_no_prefix_num='$data[4]'"; else $sys_id_cond="";
		
	
	$party_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$arr=array (2=>$item_category,4=>$party_arr,5=>$currency);
	//id, challan_no_prefix, challan_no_prefix_num, sys_challan_no, company_id, item_category, selling_date, pay_term, customer_id, currency_id, remarks
	if($db_type==0)
	{
		$year_cond= "year(insert_date)";
	}
	else if($db_type==2)
	{
		$year_cond= "TO_CHAR(insert_date,'YYYY')";
	}
	$sql= "select id, challan_no_prefix_num, $year_cond as year, item_category, selling_date, pay_term, customer_id, currency_id, remarks from inv_scrap_sales_mst where status_active=1 and is_deleted=0 order by id DESC ";
		//echo $sql; 
	echo  create_list_view("list_view", "System No,Year,Item Category,Selling Date,Customer,Currency", "70,70,120,80,130,100","680","250",0, $sql , "js_set_value", "id", "", 1, "0,0,item_category,0,customer_id,currency_id", $arr , "challan_no_prefix_num,year,item_category,selling_date,customer_id,currency_id", "scrap_sales_entry_controller","",'0,0,0,3,0,0') ; 
	
	exit();
}

if ($action=="populate_data_from_scrap_sales")
{
	$nameArray=sql_select( "select id, sys_challan_no, company_id, item_category, selling_date, pay_term, customer_id, currency_id, remarks from inv_scrap_sales_mst where id='$data'" ); 
	$party_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_system_id').value 		= '".$row[csf("sys_challan_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 		= '".$row[csf("company_id")]."';\n";
		echo "$('#cbo_company_id').attr('disabled','true')".";\n"; 
		echo "document.getElementById('cbo_category_id').value		= '".$row[csf("item_category")]."';\n";  
		echo "document.getElementById('txt_selling_date').value		= '".change_date_format($row[csf("selling_date")])."';\n"; 
		echo "document.getElementById('cbo_pay_term').value			= '".$row[csf("pay_term")]."';\n"; 
		echo "document.getElementById('txt_customer_no').value 		= '".$party_arr[$row[csf("customer_id")]]."';\n"; 
		echo "document.getElementById('txt_customer_id').value 		= '".$row[csf("customer_id")]."';\n"; 
		echo "document.getElementById('cbo_currency_id').value 		= '".$row[csf("currency_id")]."';\n";
		echo "document.getElementById('txt_remarks').value 			= '".$row[csf("remarks")]."';\n"; 
	    echo "document.getElementById('update_id').value            = '".$row[csf("id")]."';\n";
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_scrap_sales',1,1);\n";
	}
	exit();
}


if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if(str_replace("'","",$update_id)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$new_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'SSE', date("Y",time()), 5, "select challan_no_prefix, challan_no_prefix_num from inv_scrap_sales_mst where company_id=$cbo_company_id and $year_cond=".date('Y',time())." order by id desc ", "challan_no_prefix", "challan_no_prefix_num"));
		 	
			$id=return_next_id( "id", "inv_scrap_sales_mst", 1) ;
			$field_array="id, challan_no_prefix, challan_no_prefix_num, sys_challan_no, company_id, item_category, selling_date, pay_term, customer_id, currency_id, remarks, inserted_by, insert_date";
			
			$data_array="(".$id.",'".$new_system_id[1]."',".$new_system_id[2].",'".$new_system_id[0]."',".$cbo_company_id.",".$cbo_category_id.",".$txt_selling_date.",".$cbo_pay_term.",".$txt_customer_id.",".$cbo_currency_id.",".$txt_remarks.",".$user_id.",'".$pc_date_time."')";
			
			//echo "insert into inv_scrap_sales_mst (".$field_array.") values ".$data_array;die;
			/*$rID=sql_insert("inv_issue_master",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0; */
			
			$sys_challan_no=$new_system_id[0];
			$row_id=$id;
		}
		else
		{
			$field_array_update="item_category*selling_date*pay_term*customer_id*currency_id*remarks*updated_by*update_date";
			$data_array_update=$cbo_category_id."*".$txt_selling_date."*".$cbo_pay_term."*".$txt_customer_id."*".$cbo_currency_id."*".$txt_remarks."*".$user_id."*'".$pc_date_time."'";
			
			/*$rID=sql_update("inv_issue_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($rID) $flag=1; else $flag=0; */
			
			$sys_challan_no=str_replace("'","",$txt_system_id);
			$row_id=str_replace("'","",$update_id);
		}
		
		$id_dtls=return_next_id( "id", "inv_scrap_sales_dtls", 1) ;
		
		$field_array_dtls="id, mst_id, prod_id, trans_id, rej_uom, sales_qty_as_rej_uom, sales_uom, sales_qty, sales_rate, sales_amount, inserted_by, insert_date";
		
		$data_array_dtls="(".$id_dtls.",".$row_id.",".$txt_prodid.",".$trans_id.",".$cbo_rejuomid.",".$txt_rejqty.",".$cbo_salesuomid.",".$txt_salesqty.",".$txt_salesrate.",".$txt_salesamount.",".$user_id.",'".$pc_date_time."')";
		
		//echo "insert into inv_scrap_sales_dtls (".$field_array_dtls.") values ".$data_array_dtls; die;
		
		if(str_replace("'","",$update_id)=="")
		{
			$rID=sql_insert("inv_scrap_sales_mst",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$rID=sql_update("inv_scrap_sales_mst",$field_array_update,$data_array_update,"id",$row_id,1);
			if($rID) $flag=1; else $flag=0; 
		}
		$rID2=sql_insert("inv_scrap_sales_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
				
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$sys_challan_no."**".$row_id."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "0**".$sys_challan_no."**".$row_id."**0";
			}
			else
			{
				oci_rollback($con);
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array_update="item_category*selling_date*pay_term*customer_id*currency_id*remarks*updated_by*update_date";
		$data_array_update=$cbo_category_id."*".$txt_selling_date."*".$cbo_pay_term."*".$txt_customer_id."*".$cbo_currency_id."*".$txt_remarks."*".$user_id."*'".$pc_date_time."'";
		
		$sys_challan_no=str_replace("'","",$txt_system_id);
		$row_id=str_replace("'","",$update_id);
		
		$field_array_update_dtls="prod_id*trans_id*rej_uom*sales_qty_as_rej_uom*sales_uom*sales_qty*sales_rate*sales_amount*updated_by*update_date";
		$data_array_update_dtls=$txt_prodid."*".$trans_id."*".$cbo_rejuomid."*".$txt_rejqty."*".$cbo_salesuomid."*".$txt_salesqty."*".$txt_salesrate."*".$txt_salesamount."*".$user_id."*'".$pc_date_time."'";
		
		//echo $field_array_update_dtls.'=='.$data_array_update_dtls; die;
		
		$rID=sql_update("inv_scrap_sales_mst",$field_array_update,$data_array_update,"id",$update_id,0);
		if($rID) $flag=1; else $flag=0;
		//echo $dtls_update_id;die;
		$rID1=sql_update("inv_scrap_sales_dtls",$field_array_update_dtls,$data_array_update_dtls,"id",$dtls_update_id,0);
		if($rID1) $flag=1; else $flag=0; 
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".$sys_challan_no."**".$row_id."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**0**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "1**".$sys_challan_no."**".$row_id."**0";
			}
			else
			{
				oci_rollback($con);
				echo "6**0**0**1";
			}
		}
		disconnect($con);
		die;
	}
}

if($action=="scrap_sales_challan_print")
{
	extract($_REQUEST);
	$data=explode('*',$data);
	
	$company_library=return_library_array("select id, company_name from lib_company", "id", "company_name");
	$buyer_library=return_library_array("select id, buyer_name from lib_buyer", "id", "buyer_name");
	$productNameArr=return_library_array("select id, product_name_details from product_details_master", "id", "product_name_details");
	
	$sql_mst="select sys_challan_no, item_category, selling_date, pay_term, customer_id, currency_id, remarks from inv_scrap_sales_mst where id='$data[1]' and company_id='$data[0]' and status_active=1 and is_deleted=0";
	$dataArrayMst=sql_select($sql_mst);
	?>
	<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:24px"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px"> 
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0] and status_active=1 and is_deleted=0"); 
					foreach ($nameArray as $result)
					{ 
					?>
						<?php if ($result[csf('plot_no')]!="") echo $result[csf('plot_no')].',&nbsp;&nbsp;';
						if ($result[csf('level_no')]!="") echo $result[csf('level_no')].',&nbsp;&nbsp;'; 
						if ($result[csf('road_no')]!="") echo $result[csf('road_no')].',&nbsp;&nbsp;'; 
						if ($result[csf('block_no')]!="") echo $result[csf('block_no')].',&nbsp;&nbsp;'; 
						if ($result[csf('city')]!="") echo $result[csf('city')].',&nbsp;&nbsp;'; 
						if ($result[csf('zip_code')]!="") echo $result[csf('zip_code')].',&nbsp;&nbsp;'; 
						if ($result[csf('province')]!="") echo $result[csf('province')].',&nbsp;&nbsp;'; 
						if ($result[csf('country_id')]!=0) echo $country_arr[$result[csf('country_id')]]; ?><br> 
						Email : <?php echo $result[csf('email')].',&nbsp;&nbsp;';?>
						Web : <?php echo $result[csf('website')];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:20px"><u><strong>Scrap Out Challan/Gate Pass</strong></u></td>
        </tr>
        <tr>
			<?php
                $buyer_add=$dataArrayMst[0][csf('customer_id')];
                $nameArray=sql_select( "select address_1, web_site, buyer_email, country_id from lib_buyer where id=$buyer_add"); 
                foreach ($nameArray as $result)
                { 
                    $address="";
                    if($result!="") $address=$result[csf('address_1')];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
                }
				//echo $address;
            ?> 
        	<td width="100" rowspan="4" valign="top" colspan="2"><p><strong>Issue To : <?php echo $buyer_library[$buyer_add].'<br>'.$address;  ?></strong></p></td>
            <td width="125"><strong>Challan No:</strong></td> <td width="175px"><?php echo $dataArrayMst[0][csf('sys_challan_no')]; ?></td>
            <td width="125"><strong>Selling Date :</strong></td><td width="175px"><?php echo change_date_format($dataArrayMst[0][csf('selling_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Item Category :</strong></td><td><?php echo $item_category[$dataArrayMst[0][csf('item_category')]]; ?></td>
        	<td><strong>Pay Term:</strong></td><td><?php echo $pay_mode[$dataArrayMst[0][csf('pay_term')]]; ?></td>
        </tr>
        <tr>
        	<td><strong>Currency:</strong></td><td><?php echo $currency[$dataArrayMst[0][csf('currency_id')]]; ?></td>
        	<td>&nbsp;</td><td>&nbsp;</td>
        </tr>
    </table>
    <br>
	<?php
	$sql_dtls="select prod_id, rej_uom, sales_qty_as_rej_uom, sales_uom, sales_qty, sales_rate, sales_amount from inv_scrap_sales_dtls where mst_id='$data[1]' and status_active=1 and is_deleted=0";
	$result_dtls=sql_select($sql_dtls);
	?>
	<div>
    <table cellspacing="0" width="800"  border="1" rules="all" class="rpt_table" align="left">
        <thead bgcolor="#dddddd">
            <th width="30">SL</th>
            <th width="80">Prod. ID</th>
            <th width="220">Item Description</th>
            <th width="60">Sales Uom</th>
            <th width="80">Sales Qty.</th>
            <th width="70">Rate</th>
            <th>Total Sales Amount</th>
        </thead>
        <tbody>
        <?php
		$i=1;
		foreach($result_dtls as $row)
		{
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			?>
            <tr bgcolor="<?php echo $bgcolor; ?>">
            	<td width="30"><?php echo $i;  ?></td>
                <td width="80"><?php echo $row[csf('prod_id')]; ?></td>
                <td width="220"><?php echo $productNameArr[$row[csf('prod_id')]]; ?></td>
                <td width="60"><?php echo $unit_of_measurement[$row[csf('sales_uom')]]; ?></td>
                <td width="80" align="right"><?php echo number_format($row[csf('sales_qty')],2,'.',''); ?></td>
                <td width="70" align="right"><?php echo number_format($row[csf('sales_rate')],3,'.',''); ?></td>
                <td align="right"><?php echo number_format($row[csf('sales_amount')],2,'.',''); ?></td>
            </tr>
            <?php
			$tot_sales_qty+=$row[csf('sales_qty')];
			$tot_sales_amount+=$row[csf('sales_amount')];
			$i++;
		}
		?>
        </tbody>
        <tfoot>
        	<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="right" colspan="4" ><strong>Total:</strong></td>
                <td align="right"><?php echo number_format($tot_sales_qty,2,'.',''); ?></td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_sales_amount,2,'.',''); ?></td>
            </tr>
        </tfoot>
    </table>
    <br>
		 <?php
            echo signature_table(78, $data[0], "900px");
         ?>
     </div>
     </div>
     <?php
	 exit();
}
?>
