﻿<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

extract ($_REQUEST);

//Task Details
	$sql = "SELECT id,task_catagory,task_name,task_short_name,task_type,module_name,link_page,penalty,completion_percent FROM  lib_tna_task WHERE is_deleted = 0 and status_active=1 ";
	$result = sql_select( $sql ) ;
	$i=0;
	$tna_task_details = array();
	$tna_task_name=array();
	foreach( $result as $row ) 
	{	
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_catagory']=  $row[csf('task_catagory')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['id']=  $row[csf('id')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_name']=  $row[csf('task_name')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_short_name']=  $row[csf('task_short_name')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_type']=  $row[csf('task_type')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['module_name']=  $row[csf('module_name')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['link_page']=  $row[csf('link_page')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['penalty']=  $row[csf('penalty')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['completion_percent']=  $row[csf('completion_percent')];
		
		$tna_task_name[$row[csf('id')]]['task_catagory']=  $row[csf('task_catagory')];
		$tna_task_name[$row[csf('id')]]['task_name']=  $row[csf('task_name')];
		$tna_task_name[$row[csf('id')]]['task_short_name']=  $row[csf('task_short_name')];
		$tna_task_name[$row[csf('id')]]['task_type']=  $row[csf('task_type')];
		$tna_task_name[$row[csf('id')]]['module_name']=  $row[csf('module_name')];
		$tna_task_name[$row[csf('id')]]['link_page']=  $row[csf('link_page')];
		$tna_task_name[$row[csf('id')]]['penalty']=  $row[csf('penalty')];
		$tna_task_name[$row[csf('id')]]['completion_percent']=  $row[csf('completion_percent')]; 
		$i++;
	}
// Sample List 
	$sql = "SELECT id,sample_type FROM  lib_sample WHERE sample_type in ( 2,3,4 ) and status_active =1 and is_deleted = 0";
	$result = sql_select( $sql );
	$sample_list = array();
	foreach( $result as $row ) 
	{		
		$sample_list[] = $row[csf('id')];
	}
// Item List 
	$sql = "SELECT id,item_name FROM  lib_item_group WHERE trim_type in ( 1,2 ) and status_active =1 and is_deleted = 0";
	$result = sql_select( $sql );
	$accessories_list = array();
	foreach( $result as $row ) 
	{		
		$accessories_list[] = $row[csf('id')];
	}
//Template Details
	$sql_task = "SELECT a.id,task_template_id,lead_time,material_source,total_task,tna_task_id,deadline,execution_days,notice_before,sequence_no,for_specific,b.task_catagory,b.task_name FROM  tna_task_template_details a, lib_tna_task b WHERE  a.is_deleted = 0 and a.status_active=1 and a.tna_task_id=b.id order by for_specific,lead_time";
	$result = sql_select( $sql_task ) ;
	$tna_task_template = array();
	$tna_task_template_new=array();
	$tna_template = array();
	$tna_template_buyer = array(); 
	$i=0;
	$k=0;
	$j=0;
	foreach( $result as $row ) 
	{
		if (!in_array($row[csf("task_template_id")],$template))
		{
			$template[]=$row[csf("task_template_id")];
			if ( $row[csf("for_specific")]==0 )
			{
				$tna_template[$i]['lead']=$row[csf('lead_time')];
				$tna_template[$i]['id']=$row[csf('task_template_id')];
				$i++;
			}
			else
			{
				if(!in_array($row[csf('for_specific')],$tna_template_spc)) { $j=0; $tna_template_spc[]=$row[csf("for_specific")]; }
				$tna_template_buyer[$row[csf('for_specific')]][$j]['lead']=$row[csf('lead_time')];
				$tna_template_buyer[$row[csf('for_specific')]][$j]['id']=$row[csf('task_template_id')];
				$j++;
			}
			$k++;
		}
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['lead_time']=$row[csf("lead_time")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['material_source']=$row[csf("material_source")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['total_task']=$row[csf("total_task")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['tna_task_id']=$row[csf("tna_task_id")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['deadline']=$row[csf("deadline")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['execution_days']=$row[csf("execution_days")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['notice_before']=$row[csf("notice_before")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['sequence_no']=$row[csf("sequence_no")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['for_specific']=$row[csf("for_specific")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['id']=$row[csf("id")];
		 $tna_task_template[$row[csf("task_template_id")]][$row[csf("tna_task_id")]]['task_template_id']=$row[csf("task_template_id")];
		 if (!in_array($row[csf("task_template_id")],$templatesss))
		 	{ $templatesss[]=$row[csf("task_template_id")]; $g=0; }
			 
		 $tna_task_template_new[$row[csf("task_template_id")]][$row[csf("task_catagory")]][$row[csf("task_name")]]=$row[csf("task_name")];
		 $tna_task_template_type[$row[csf("task_template_id")]][$row[csf("task_catagory")]]['type']=$row[csf("task_type")];
		 
		 /*$tna_task_template_new[$row[csf("task_template_id")]][$g]['material_source']=$row[csf("material_source")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['total_task']=$row[csf("total_task")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['tna_task_id']=$row[csf("tna_task_id")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['deadline']=$row[csf("deadline")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['execution_days']=$row[csf("execution_days")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['notice_before']=$row[csf("notice_before")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['sequence_no']=$row[csf("sequence_no")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['for_specific']=$row[csf("for_specific")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['id']=$row[csf("id")];
		 $tna_task_template_new[$row[csf("task_template_id")]][$g]['task_template_id']=$row[csf("task_template_id")];*/
		 $g++;
		 $i++;
	}

if ( $action=="tna_process" )
{
	// Reprocess Check
	/*if (trim($job_no)=="") $strcond=""; 
	else 
	{
		$job_no=explode("*",$job_no);
		
		for ($i=0; $i<count($job_no);$i++)
		{
			if($job_nos=="")$job_nos="'".$job_no[$i]."'"; else $job_nos=$job_nos.","."'".$job_no[$i]."'";
		}
		mysql_query("delete a.*,b.* FROM tna_process_mst a, tna_process_dtls_mail b  WHERE a.id=b.mst_id and a.job_no in ( $job_nos )") or die (mysql_error()); 
		$strcond=" and job_no in ( $job_nos )";
		$strcond_break=" and job_no_mst in ( $job_nos )";
	}*/
	
	// PO Details List
	if ($strcond=="")
	{
		$sql = "SELECT (pub_shipment_date) as shipment_date,job_no_mst,po_received_date,b.id,po_quantity,a.buyer_name,a.garments_nature,is_confirmed,tna_task_from_upto FROM wo_po_break_down b, wo_po_details_master a WHERE b.is_deleted = 0 and b.status_active=1 and a.job_no=b.job_no_mst and b.shipment_date!='0000-00-00' and b.po_received_date!='0000-00-00'   ORDER BY b.shipment_date asc";
	}
	else
	{
		$sql = "SELECT (pub_shipment_date) as shipment_date,job_no_mst,po_received_date,b.id,po_quantity,a.buyer_name,a.garments_nature,is_confirmed,tna_task_from_upto FROM wo_po_break_down b, wo_po_details_master a WHERE b.is_deleted = 0 and b.status_active=1 and a.job_no=b.job_no_mst and b.shipment_date!='0000-00-00' and b.po_received_date!='0000-00-00' and shiping_status!=3 and job_no_mst  in ($job_nos)   ORDER BY b.shipment_date asc";
	}
	 
	$data_array=sql_select($sql);
	$job_no_array=array();
	$order_id_array=array();
	$po_order_template=array();
	$po_order_details=array();
	$job_nature = array();
	$i=0;
	foreach($data_array as $row)
	{
		$remain_days=datediff( "d", $row[csf("po_received_date")], $row[csf("shipment_date")] );
		$template_id=get_tna_template($remain_days,$tna_template,$row[csf("buyer_name")]);
		
		if ($template_id=="" || $template_id==0)
		{
			if(count($tna_template_buyer[$row[csf("buyer_name")]])>0) $template_id=$tna_template_buyer[$row[csf("buyer_name")]][count($tna_template_buyer[$row[csf("buyer_name")]])-1]['id'];
			else $template_id= $tna_template[count($tna_template)-1]['id'];
		}
		//,is_confirmed,tna_task_from_upto 
		
		if (!in_array( $row[csf("job_no_mst")],$job_no_array)) $job_no_array[]= $row[csf("job_no_mst")] ;
		$order_id_array[$i]=$row[csf("id")];
		$po_order_template[$row[csf("id")]]=  $template_id; 
		$po_order_details[$row[csf("id")]]['po_received_date']=$row[csf("po_received_date")];
		$po_order_details[$row[csf("id")]]['shipment_date']=$row[csf("shipment_date")];
		$po_order_details[$row[csf("id")]]['job_no_mst']=$row[csf("job_no_mst")];
		$po_order_details[$row[csf("id")]]['po_quantity']=$row[csf("po_quantity")];
		$po_order_details[$row[csf("id")]]['is_confirmed']=$row[csf("is_confirmed")];
		$po_order_details[$row[csf("id")]]['tna_task_from_upto']=$row[csf("tna_task_from_upto")];
		$job_nature[$row[csf('job_no')]] = $row[csf('garments_nature')];
		$i++;
	}
	
	// Knit Fabric Update Data
	$po_ids=implode(",",$order_id_array);
	$sql = "SELECT b.po_breakdown_id,b.entry_form, min(a.transaction_date) mindate, max(a.transaction_date) maxdate, sum(quantity) as prod_qntry 
FROM inv_transaction a,  order_wise_pro_details b where  b.trans_id=a.id and b.entry_form in (2,7) and b.po_breakdown_id in ( $po_ids ) group by b.po_breakdown_id,b.entry_form order by b.po_breakdown_id";
	$knit_fabric_transaction_update=array();
	$knit_tmp=array(1=>"2",2=>"6",3=>"7");
	$data_array=sql_select($sql);
	foreach($data_array as $row)
	{
		$knit_fabric_transaction_update[$row[csf("po_breakdown_id")]][$row[csf("entry_form")]]['start_date']=$row[csf("mindate")];
		$knit_fabric_transaction_update[$row[csf("po_breakdown_id")]][$row[csf("entry_form")]]['end_date']=$row[csf("maxdate")];
		$knit_fabric_transaction_update[$row[csf("po_breakdown_id")]][$row[csf("entry_form")]]['quantity']=$row[csf("prod_qntry")];
	}
	
	$job_no_list="'".implode("','",$job_no_array)."'";
// Inspection Data for Update
	$sql = "SELECT job_no,po_break_down_id,min(inspection_date) as mind,max(inspection_date) as maxd,sum(inspection_qnty) as sumtot FROM pro_buyer_inspection WHERE job_no in ( $job_no_list ) and status_active =1 and is_deleted = 0 group by po_break_down_id ";
	$result = sql_select( $sql );
	$inspection_status_array = array();
	foreach( $result as $row ) 
	{
		$inspection_status_array[$row[csf('po_break_down_id')]]['min'] = $row[csf('mind')];
		$inspection_status_array[$row[csf('po_break_down_id')]]['max'] = $row[csf('maxd')];
		$inspection_status_array[$row[csf('po_break_down_id')]]['qnty'] = $row[csf('sumtot')];
	}
	
	// TNA Processs List for Update
	$sql = "SELECT id,job_no,po_number_id,task_category,task_number,actual_start_date,actual_finish_date FROM   tna_process_mst WHERE job_no in ( $job_no_list ) and status_active =1 and is_deleted = 0";
	$result = sql_select( $sql );
	$tna_process_list = array();
	$tna_process_details = array();
	foreach( $result as $row ) 
	{
		$tna_process_list[$row[csf('task_category')]][$row[csf('po_number_id')]][$row[csf('task_number')]]= $row[csf('id')];
		$tna_process_details[$row[csf('id')]]['start']=$row[csf('actual_start_date')];
		$tna_process_details[$row[csf('id')]]['finish']=$row[csf('actual_finish_date')];
	}
 
	$job_no=$job_no_list;
	$sample_ids=implode(",",$sample_list);
	
	$field_array_tna_process="id,template_id,job_no,po_number_id,po_receive_date,shipment_date,task_category,task_number,target_date,task_start_date,task_finish_date,notice_date_start,notice_date_end,process_date,sequence_no,status_active,is_deleted";
	$field_array_tna_process_up="actual_start_date*actual_finish_date";

/*
	foreach($tna_task_template[1] as $test_task)
	{
		echo $tna_task_name[$test_task['tna_task_id']]['task_catagory']; 
		print_r($test_task); die;
	}
	if( $tna_task_template_new[$po_order_template[$poid]][26][$value]!=""  )
			{
	die;*/
	
	foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	{
		foreach($garments_production_task as $value=>$test_task)
		{
			if( $tna_task_template_new[$po_order_template[$poid]][26][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[26][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',26,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					// Need to be updated by Task wise
					//tna_task_template_type[$row[csf("task_template_id")]][$row[csf("task_catagory")]]['type']
					$process_id_up=$tna_process_list[26][$poid][$value];
					if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="" ) $start_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']; else $start_date="0000-00-00";
					if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="" ) $finish_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']; else $finish_date="0000-00-00";
					$process_id_up_array[]=$process_id_up;
					$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
				}
			}
		}
	} echo "ss";
die;
	
	// Sample Approval Start Here
	$sql_task = "SELECT job_no_mst,po_break_down_id,color_number_id,sample_type_id,target_approval_date,send_to_factory_date,submitted_to_buyer,approval_status,approval_status_date FROM wo_po_sample_approval_info WHERE job_no_mst in ( $job_no ) and sample_type_id in ( $sample_ids ) and is_deleted = 0 and status_active=1 group by sample_type_id,po_break_down_id order by job_no_mst,po_break_down_id";
	$data_array=sql_select($sql_task);
	foreach($data_array as $row)
	{
		if($po_order_details[$row[csf("po_break_down_id")]]['shipment_date']!="")
		{
			$target_date=add_date($po_order_details[$row[csf("po_break_down_id")]]['shipment_date'] ,- $tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("sample_type_id")]]['deadline']);
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("sample_type_id")]]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("sample_type_id")]]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			
			 if ($tna_process_list[5][$row[csf('po_break_down_id')]][$row[csf('sample_type_id')]]=="")
			 {
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$row[csf("po_break_down_id")]].",'".$row[csf("job_no_mst")]."','".$row[csf("po_break_down_id")]."','".$po_order_details[$row[csf("po_break_down_id")]]['po_received_date']."','".$po_order_details[$row[csf("po_break_down_id")]]['shipment_date']."',5,".$row[csf('sample_type_id')].",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("sample_type_id")]]['sequence_no']."',1,0)";
			}
			else  // Update Process
			{
				$process_id_up=$tna_process_list[5][$row[csf('po_break_down_id')]][$row[csf('sample_type_id')]];
				if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; else $start_date=$row[csf("submitted_to_buyer")];
				if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; else $finish_date=$row[csf("approval_status_date")];
				$process_id_up_array[]=$process_id_up;
				$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
			}
		}
	}
	
	// Acessories Approval Start Here 
 	$accessories_type_ids=implode(",",$accessories_list);
	$sql_task = "SELECT job_no_mst,po_break_down_id,accessories_type_id,target_approval_date,sent_to_supplier,submitted_to_buyer,approval_status,approval_status_date FROM wo_po_trims_approval_info WHERE job_no_mst in ( $job_no ) and accessories_type_id in ( $accessories_type_ids ) and is_deleted = 0 and status_active=1 and current_status!=0 group by accessories_type_id,po_break_down_id order by job_no_mst,po_break_down_id";
	$data_array=sql_select($sql_task);
	foreach($data_array as $row)
	{
		if($po_order_details[$row[csf("po_break_down_id")]]['shipment_date']!="")
		{
			$target_date=add_date($po_order_details[$row[csf("po_break_down_id")]]['shipment_date'] ,- $tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("accessories_type_id")]]['deadline']);
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("accessories_type_id")]]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("accessories_type_id")]]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			
			 if ($tna_process_list[7][$row[csf('po_break_down_id')]][$row[csf('accessories_type_id')]]=="")
			 {
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$row[csf("po_break_down_id")]].",'".$row[csf("job_no_mst")]."','".$row[csf("po_break_down_id")]."','".$po_order_details[$row[csf("po_break_down_id")]]['po_received_date']."','".$po_order_details[$row[csf("po_break_down_id")]]['shipment_date']."',7,".$row[csf('accessories_type_id')].",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("accessories_type_id")]]['sequence_no']."',1,0)";
			}
			else  // Update Process
			{
				$process_id_up=$tna_process_list[7][$row[csf('po_break_down_id')]][$row[csf('accessories_type_id')]];
				if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; else $start_date=$row[csf("submitted_to_buyer")];
				if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; else $finish_date=$row[csf("approval_status_date")];
				$process_id_up_array[]=$process_id_up;
				$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
			}
		}
	}
	
	// Embellishment Approval Start Here 
	$sql_task = "SELECT job_no_mst,po_break_down_id,embellishment_id,target_approval_date,sent_to_supplier,submitted_to_buyer,approval_status,approval_status_date FROM wo_po_embell_approval WHERE job_no_mst in ( $job_no ) and embellishment_id in ( 1,2,3,4 ) and is_deleted = 0 and status_active=1 and current_status!=0 group by embellishment_id,po_break_down_id order by job_no_mst,po_break_down_id";
	$data_array=sql_select($sql_task);
	foreach($data_array as $row)
	{
		if($po_order_details[$row[csf("po_break_down_id")]]['shipment_date']!="")
		{
			$target_date=add_date($po_order_details[$row[csf("po_break_down_id")]]['shipment_date'] ,- $tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("embellishment_id")]]['deadline']);
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("embellishment_id")]]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("embellishment_id")]]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			
			 if ($tna_process_list[8][$row[csf('po_break_down_id')]][$row[csf('embellishment_id')]]=="")
			 {
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$row[csf("po_break_down_id")]].",'".$row[csf("job_no_mst")]."','".$row[csf("po_break_down_id")]."','".$po_order_details[$row[csf("po_break_down_id")]]['po_received_date']."','".$po_order_details[$row[csf("po_break_down_id")]]['shipment_date']."',8,".$row[csf('embellishment_id')].",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][$row[csf("embellishment_id")]]['sequence_no']."',1,0)";
			}
			else  // Update Process
			{
				$process_id_up=$tna_process_list[8][$row[csf('po_break_down_id')]][$row[csf('embellishment_id')]];
				if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; else $start_date=$row[csf("submitted_to_buyer")];
				if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; else $finish_date=$row[csf("approval_status_date")];
				$process_id_up_array[]=$process_id_up;
				$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
			}
		}
	}
	
	// Lapdip Approval Start Here	
	$sql_task = "SELECT job_no_mst,po_break_down_id,lapdip_target_approval_date,submitted_to_buyer,approval_status,approval_status_date FROM wo_po_lapdip_approval_info WHERE job_no_mst in ( $job_no ) and is_deleted = 0 and status_active=1 and current_status!=0 group by po_break_down_id order by job_no_mst,po_break_down_id";
	$data_array=sql_select($sql_task);
	foreach($data_array as $row)
	{
		if($po_order_details[$row[csf("po_break_down_id")]]['shipment_date']!="")
		{
			$target_date=add_date($po_order_details[$row[csf("po_break_down_id")]]['shipment_date'] ,- $tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][1]['deadline']);
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][1]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][1]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			 
			 if ($tna_process_list[6][$row[csf('po_break_down_id')]][1]=="")  //target
			 {
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$row[csf("po_break_down_id")]].",'".$row[csf("job_no_mst")]."','".$row[csf("po_break_down_id")]."','".$po_order_details[$row[csf("po_break_down_id")]]['po_received_date']."','".$po_order_details[$row[csf("po_break_down_id")]]['shipment_date']."',6,1,'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][1]['sequence_no']."',1,0)";
			}
			else  // Update Process
			{
				$process_id_up=$tna_process_list[6][$row[csf('po_break_down_id')]][1];
				if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; else $start_date=$row[csf("approval_status_date")];
				if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; else $finish_date=$row[csf("approval_status_date")];
				$process_id_up_array[]=$process_id_up;
				$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
			}
			/// Submission
			$target_date=add_date($po_order_details[$row[csf("po_break_down_id")]]['shipment_date'] ,- $tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][2]['deadline']);
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][2]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][2]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			
			 if ($tna_process_list[6][$row[csf('po_break_down_id')]][2]=="")  //Submission
			 {
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$row[csf("po_break_down_id")]].",'".$row[csf("job_no_mst")]."','".$row[csf("po_break_down_id")]."','".$po_order_details[$row[csf("po_break_down_id")]]['po_received_date']."','".$po_order_details[$row[csf("po_break_down_id")]]['shipment_date']."',6,2,'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$row[csf("po_break_down_id")]]][2]['sequence_no']."',1,0)";
			}
			else  // Update Process
			{
				$process_id_up=$tna_process_list[6][$row[csf('po_break_down_id')]][2];
				if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; else $start_date=$row[csf("submitted_to_buyer")];
				if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; else $finish_date=$row[csf("submitted_to_buyer")];
				$process_id_up_array[]=$process_id_up;
				$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
			}
		}
	}
	
	// Test Approval No Update
	foreach($order_id_array as $poid)
	{
		foreach($test_approval_task as $value=>$test_task)
		{
			$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][$value]['deadline']);
			 
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			if ($tna_process_list[9][$poid][$value]=="")
			 {
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',9,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][$value]['sequence_no']."',1,0)";
			}
		}
	}
	 
	// Inspection
	foreach($order_id_array as $poid)
	{
		foreach($inspection_task as $value=>$test_task)
		{
			$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][$value]['deadline']);
			 
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			if ($tna_process_list[30][$poid][$value]=="")
			 {
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',30,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][$value]['sequence_no']."',1,0)";
			}
			else // Update
			{
				//print_r($tna_process_list[30]); die;
				//Update from Inspection table  total pass qnty to be compared with task completion %
				$process_id_up=$tna_process_list[30][$poid][$value];
				// $perc=ceil(($inspection_status_array[$poid]['qnty']*100)/$po_order_details[$poid]['po_quantity']);
					//if($tna_task_details[30][$value]['completion_percent']>$perc) $inspection_status_array[$poid]['maxd']="0000-00-00";
					 
				if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; 
				else $start_date= ($inspection_status_array[$poid]['min'] == "" ? "0000-00-00" : $inspection_status_array[$poid]['min']); 
				if (get_percent($inspection_status_array[$poid]['qnty'], $po_order_details[$poid]['po_quantity'])>=$tna_task_details[30][$value]['completion_percent'])
				{
					if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; 
					else $finish_date=($inspection_status_array[$poid]['maxd'] == "" ? "0000-00-00" : $inspection_status_array[$poid]['maxd']); 
				}
				else $finish_date="0000-00-00";
				
				$process_id_up_array[]=$process_id_up;
				$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
			}
		}
	}
	 
	//  Fabric Production  
	foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	{
		if( $job_nature[$po_order_details[$poid]['job_no_mst']]==1)  // Knit garments
		{
			foreach($knit_fabric_production_task as $value=>$test_task)
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[25][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',25,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					$process_id_up=$tna_process_list[25][$poid][$value];
					if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="" ) $start_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']; else $start_date="0000-00-00";
					if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="" ) $finish_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']; else $finish_date="0000-00-00";
					$process_id_up_array[]=$process_id_up;
					$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
				}
			}
		}
		/*else if( $job_nature[$po_order_details[$poid]['job_no_mst']]==2)  // Woven garments Temporary OFFFFFFFFFFf
		{
			foreach($woven_fabric_production_task as $value=>$test_task)
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[25][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',25,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					$process_id_up=$tna_process_list[25][$poid][$value];
					if ( $knit_fabric_transaction_update[$poid][2]['start_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][2]['start_date']!="" ) $start_date=$knit_fabric_transaction_update[$poid][2]['start_date']; else $start_date="0000-00-00";
					if ( $knit_fabric_transaction_update[$poid][2]['end_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][2]['end_date']!="" ) $finish_date=$knit_fabric_transaction_update[$poid][2]['end_date']; else $finish_date="0000-00-00";
					$process_id_up_array[]=$process_id_up;
					$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
				}
			}
		}*/
	}
	
	//  Material Purchase   No Update
	foreach($order_id_array as $poid)
	{
		foreach($material_recieve_task as $value=>$test_task)
		{
			$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][$value]['deadline']);
			 
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			if ($tna_process_list[20][$poid][$value]=="")
			{
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',20,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][$value]['sequence_no']."',1,0)";
			}
			else // Update
			{
				if ($value==1)// Knitting
				{
					
				}
			}
		}
	}
	 

//  Garments Production  
	foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	{
		foreach($garments_production_task as $value=>$test_task)
		{
			$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][$value]['deadline']);
			
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['execution_days']-1;
			$start_date=add_date($target_date ,-$to_add_days);
			$finish_date=$target_date;
			$to_add_days=$tna_task_template[$po_order_template[$poid]][$value]['notice_before'];
			$notice_date_start=add_date($start_date ,-$to_add_days);
			$notice_date_end=add_date($finish_date ,-$to_add_days);
			if ($tna_process_list[26][$poid][$value]=="")
			{
				if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
				if ($data_array_tna_process!="") $data_array_tna_process .=",";
				$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',26,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][$value]['sequence_no']."',1,0)";
			}
			else // Update
			{
				// Need to be updated by Task wise
				$process_id_up=$tna_process_list[26][$poid][$value];
				if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="" ) $start_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']; else $start_date="0000-00-00";
				if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="" ) $finish_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']; else $finish_date="0000-00-00";
				$process_id_up_array[]=$process_id_up;
				$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
			}
		}
	}
	
	echo "Sumon".$data_array_tna_process;
	die;



	$con = connect();
	if($db_type==0)
	{
		mysql_query("BEGIN");
	}
	
	$rID=sql_insert("tna_process_mst",$field_array_tna_process,$data_array_tna_process,1);
	mysql_query("COMMIT"); die;
//	$rID_up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_up, $process_id_up_array ));die;
	if($db_type==0)
	{
		if($rID ){
			mysql_query("COMMIT");  
			echo "0**".$rID;
		}
		else{
			mysql_query("ROLLBACK"); 
			echo "10**".$rID;
		}
	}
	
	if($db_type==2 || $db_type==1 )
	{
		echo '0**0';
	}
	disconnect($con);
	
	
	
	echo "222"; die;
	
	
}

function get_tna_template( $remain_days, $tna_template, $buyer )
{
	global $tna_template_buyer;
	if(count($tna_template_buyer[$buyer])>0)
	{ 
		$n=count($tna_template_buyer[$buyer]); 
		for($i=0;$i<$n;$i++)
		{ 
			if($remain_days<=$tna_template_buyer[$buyer][$i]['lead']) 
			{
				if ($i!=0)
				{
					$up_day=$tna_template_buyer[$buyer][$i]['lead']-$remain_days;
					$low_day=$remain_days-$tna_template_buyer[$buyer][$i-1]['lead'];
					if ($up_day>=$low_day)
						return $tna_template_buyer[$buyer][$i-1]['id'];
					else
						return $tna_template_buyer[$buyer][$i]['id'];
				}
				else
				{
					return $tna_template_buyer[$buyer][$i]['id'];
				}
			}
		}
	}
	else
	{
		$n=count($tna_template); 
		for($i=0;$i<$n;$i++)
		{
			if($remain_days<=$tna_template[$i]['lead']) 
			{
				if ($i!=0)
				{
					$up_day=$tna_template[$i]['lead']-$remain_days;
					$low_day=$remain_days-$tna_template[$i-1]['lead'];
					if ($up_day>=$low_day)
						return $tna_template[$i-1]['id'];
					else
						return $tna_template[$i]['id'];
				}
				else
				{
					return $tna_template[$i]['id'];
				}
			}
		}
	}
}

function get_percent($completed, $actual)
{
	return number_format((($completed*100)/$actual),0);
}

?>