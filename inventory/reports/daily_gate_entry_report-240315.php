﻿<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Date Wise Item Receive Issue Report
				
Functionality	:	
JS Functions	:
Created by		:	Ashraful 
Creation date 	: 	24/03/2014
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Item Ledger","../../", 1, 1, $unicode,1,1); 
?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";



	function openmypage_chalan()
	{
		if( form_validation('cbo_item_cat*cbo_company_name','Item Category*Company Name')==false )
		{
			return;
		}
		var company = $("#cbo_company_name").val();	
		var category= $("#cbo_item_cat").val();	
	
		var page_link='requires/daily_gate_entry_report_contorller.php?action=chalan_surch&company='+company+'&category='+category;
		var title="Search Item Popup";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px,height=500px,center=1,resize=0,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var system_id=this.contentDoc.getElementById("hidden_chalan_id").value; // product ID
			var system_no=this.contentDoc.getElementById("hidden_chalan_no").value; // product ID
			var search_by=this.contentDoc.getElementById("hidden_search_number").value; // product Description
		
			$("#txt_chalan_no").val(system_no);
			$("#txt_chalan_id").val(system_id);
			$("#txt_search_id").val(search_by); 
			
		}
	}
	
	
	function openmypage_order()
	{
		if( form_validation('cbo_item_cat*cbo_company_name','Item Category*Company Name*')==false )
		{
			return;
		}
		var company = $("#cbo_company_name").val();	
		var category= $("#cbo_item_cat").val();	
	
		var page_link='requires/daily_gate_entry_report_contorller.php?action=pi_search&company='+company+'&category='+category;  
		var title="Search Chalan no/System Popup";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=520px,height=370px,center=1,resize=0,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]; 
			var pi_id=this.contentDoc.getElementById("txt_selected_id").value; // product ID
			var pi_des=this.contentDoc.getElementById("txt_selected").value; // product Description
		
			//alert(style_des_no);
			$("#txt_pi_no").val(pi_des);
			$("#txt_pi_id").val(pi_id); 
		
		}
	}



		
/*	
function reset_field()
{
	reset_form('item_receive_issue_1','report_container2','','','','');
}
function job_order_per()
{
	var item_val=$('#cbo_item_cat').val();
	if((item_val==2)||(item_val==3)||(item_val==4)||(item_val==13)||(item_val==14))
	{
		$('#txt_style_ref').attr("disabled",false);
		$('#txt_order').attr("disabled",false);
		$('#cbo_buyer_name').attr("disabled",false);
	}
	
	else
	{
		$('#txt_style_ref').attr("disabled",true);
		$('#txt_order').attr("disabled",true);
		$('#cbo_buyer_name').attr("disabled",true);
	}
	
}
*/
function  generate_report(operation)
	{
		/*if( form_validation('cbo_item_cat*cbo_company_name*txt_date_from*txt_date_to','Item Cetagory*Company Name*Date From*Date To')==false )
		{
			return;
		} */
		
			//alert("xx");
			var cbo_item_cat = $("#cbo_item_cat").val();
			var cbo_company_name = $("#cbo_company_name").val();
			var cbo_gate_type = $("#cbo_gate_type").val();
		
			var txt_pi_no = $("#txt_pi_no").val();
			var cbo_supplier= $("#cbo_supplier").val();	
			var txt_challan = $("#txt_chalan_no").val();
			var txt_search_item = $("#txt_search_id").val();
			var txt_date_from = $("#txt_date_from").val();
			var txt_date_to = $("#txt_date_to").val();	
			
			var dataString = "&cbo_item_cat="+cbo_item_cat+"&cbo_company_name="+cbo_company_name+"&cbo_gate_type="+cbo_gate_type+"&txt_pi_no="+txt_pi_no+"&txt_challan="+txt_challan+"&txt_search_item="+txt_search_item+"&txt_date_from="+txt_date_from+"&txt_date_to="+txt_date_to+"&cbo_supplier="+cbo_supplier;
			var data="action=generate_report"+dataString;
			//alert(data);
			freeze_window(3);
			http.open("POST","requires/daily_gate_entry_report_contorller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = generate_report_reponse; 
		
	}

	function generate_report_reponse()
	{	
		if(http.readyState == 4) 
		{
			//alert(http.responseText);	 
			
			
				var reponse=trim(http.responseText).split("####");
		$("#report_container2").html(reponse[0]);  
		document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
		setFilterGrid("table_body",-1);
		
		show_msg('3');
		release_freezing();
			
			
			
			
		/*	var reponse=trim(http.responseText).split("**");
			//alert(reponse[2]);
			$("#report_container2").html(reponse[0]);
			document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
			if(reponse[2]==2)
			{
				setFilterGrid("table_body",-1,tableFilters);
				setFilterGrid("table_body_1",-1,tableFilters);
			}
			else if(reponse[2]==13)
			{
				setFilterGrid("table_body",-1,tableFilters2);
				setFilterGrid("table_body_1",-1,tableFilters2);
			}
			else if(reponse[2]==4)
			{
				setFilterGrid("table_body",-1,tableFilters3);
				setFilterGrid("table_body_1",-1,tableFilters2);
			}
			else
			{
				setFilterGrid("table_body",-1);
				setFilterGrid("table_body_1",-1);
			}
			release_freezing();
			show_msg('3');*/
			//document.getElementById('report_container').innerHTML=report_convert_button('../../');
		}
	} 

	function new_window()
	{
		
		//document.getElementById('caption').style.visibility='visible';
		$('#scroll_body tr:first').hide();
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');

	//document.getElementById('caption').style.visibility='hidden';
	d.close(); 
	$('#scroll_body tr:first').show();
		
	/*	 
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		$('#table_body tr:first').hide(); 
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');
		d.close(); 
		document.getElementById('scroll_body').style.overflow="auto"; 
		document.getElementById('scroll_body').style.maxHeight="250px";
		$('#table_body tr:first').show();
	}*/

	}
</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
	<?php echo load_freeze_divs ("../../",$permission);  ?><br />    		 
    <form name="item_receive_issue_1" id="item_receive_issue_1" autocomplete="off" >
    <h3 align="left" id="accordion_h1" style="width:1050px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
    <div style="width:1100px;" align="center" id="content_search_panel">
        <fieldset style="width:1050px;">
             <table class="rpt_table" width="1030" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                    	<th width="130" class="must_entry_caption"><p>Item Category</p>
                   	    <p>&nbsp;</p></th>
                        <th width="130" class="must_entry_caption">Company</th>                                
                        
                        <th width="150">Challan No./System ID</th>
                        <th width="130" >PI/WO/REQ</th>
                        <th width="130" > Supplier</th>
						 <th width="130" > Type</th>
                        <th width="130" class="must_entry_caption">Date From</th>
                        <th width="130" class="must_entry_caption">Date To</th>
                        <th width="100"><input type="reset" name="res" id="res" value="Reset" style="width:90px" class="formbutton" onClick="reset_field()" /></th>
                    </tr>
                </thead>
                <tr class="general">
                	<td>
						<?php
                        
							
					   echo create_drop_down( "cbo_item_cat", 120, $item_category,"", 1, "--- Select ---", $selected, "","","",0 );

                        ?>
                    </td>
                    <td>
                            <?php
                        echo create_drop_down( "cbo_company_name", 130, "select comp.id, comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/daily_gate_entry_report_contorller', this.value, 'load_drop_down_supplier', 'suplier_td');" );
							//load_drop_down( 'requires/daily_gate_entry_report_contorller', this.value, 'load_drop_down_supplier', 'supplier_td' );
                        ?>                          
                    </td>
                   
                    <td align="center">
                        <input style="width:120px;"  name="txt_chalan_no" id="txt_chalan_no"  ondblclick="openmypage_chalan()"  class="text_boxes" placeholder="Browse "   />   
                        <input type="hidden" name="txt_chalan_id" id="txt_chalan_id"/>    
                        <input type="hidden" name="txt_search_id" id="txt_search_id"/>            
                    </td> 
                    
                     <td align="center">
                        <input type="text" style="width:120px;"  name="txt_pi_no" id="txt_pi_no"  ondblclick="openmypage_order()"  class="text_boxes" placeholder="Browse or Write"   />         
                    </td>
                    <td id="suplier_td">
                    	<?php
                                echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                         ?>
                    </td>
					 <td>
								<?php //load_drop_down('requires/dyeing_production_report_controller',document.getElementById('cbo_company_name').value+'_'+this.value, 'load_drop_down_buyer', 'cbo_buyer_name_td' );
									$gate_type_arr=array(1=>"Gate In",2=>"Gate Out");
									echo create_drop_down( "cbo_gate_type",70, $gate_type_arr,"",1, "--All--", 0,"",0 );
                                ?>
                            </td>
                      <td>
                    	<input type="date" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:95px;" readonly/> 
                    </td>
                    <td>
                    	<input type="date" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:95px;" readonly/>
                    </td>
                    
                    <td>
                        <input type="button" name="search" id="search" value="Show" onClick="generate_report(3)" style="width:90px" class="formbutton" />
                    </td>
                </tr>
                <tr>
                	<td colspan="10" align="center" valign="bottom"><?php echo load_month_buttons(1);  ?></td>
                </tr>
                
            </table>  
        </fieldset> 
           
    </div>
        <!-- Result Contain Start-------------------------------------------------------------------->
        	<div id="report_container" align="center"></div>
            <div id="report_container2"></div> 
        <!-- Result Contain END-------------------------------------------------------------------->
    
    
    </form>    
</div>    
</body>  
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
