<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_store")
{
	$data=explode('_',$data);
	echo create_drop_down( "cbo_store_name", 120, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET($data[0],company_id) and FIND_IN_SET($data[1],item_category_id) order by store_name","id,store_name", 1, "--Select Store--", 1, "",0 );
	exit();
}

if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_account_id').val( id );
		$('#item_account_val').val( ddd );
	} 
		  
	</script>
     <input type="hidden" id="item_account_id" />
     <input type="hidden" id="item_account_val" />
 <?php
	
	$sql="SELECT id, item_category_id, product_name_details from product_details_master where item_category_id=$data[1] and status_active=1 and is_deleted=0"; 
	$arr=array(0=>$item_category);
	echo  create_list_view("list_view", "Item Category,Fabric Description,Product ID", "120,250","490","300",0, $sql , "js_set_value", "id,product_name_details", "", 1, "item_category_id,0,0", $arr , "item_category_id,product_name_details,id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	//echo $from_date;
	if ($cbo_company_name==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_name'";
	if ($cbo_item_category_id==0) $item_category_id=""; else $item_category_id=" and a.item_category=$cbo_item_category_id";
	if ($item_account_id==0) $item_account=""; else $item_account=" and a.prod_id in ($item_account_id)";
	if ($cbo_store_name==0){ $store_id="";}else{$store_id=" and a.store_id='$cbo_store_name'";}

	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$determinaArr = return_library_array("select id,construction from lib_yarn_count_determina_mst where status_active=1 and is_deleted=0","id","construction");

	/*$sql="Select a.id,a.prod_id,b.detarmination_id,b.gsm,b.dia_width,
		sum(case when a.transaction_type in(1,3,5) and a.transaction_date<'".change_date_format($from_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type in(2,4,6) and a.transaction_date<'".change_date_format($from_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as iss_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as receive,
		sum(case when a.transaction_type=3 and a.transaction_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as receive_return,
		sum(case when a.transaction_type=5 and a.transaction_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."' then a.cons_quantity else 0 end) as transfer_in
		from inv_transaction a, product_details_master b, inv_receive_master c 
		where a.prod_id=b.id and a.cons_quantity >0 and a.mst_id=c.id and a.transaction_type in (1,3,5) and a.company_id=c.company_id and c.item_category=a.item_category and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.order_id=0 $company_id $item_category_id $group_id $store_id $item_account group by a.prod_id order by a.prod_id, b.store_id ASC";*/
	$sql="Select a.prod_id, b.detarmination_id, b.gsm, b.dia_width,
		sum(case when a.transaction_type in(1,3,5) and a.transaction_date<'".change_date_format($from_date,'','',1)."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type in(2,4,6) and a.transaction_date<'".change_date_format($from_date,'','',1)."' then a.cons_quantity else 0 end) as iss_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as receive,
		sum(case when a.transaction_type=3 and a.transaction_date between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as receive_return,
		sum(case when a.transaction_type=5 and a.transaction_date between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as transfer_in
		from inv_transaction a, product_details_master b
		where a.prod_id=b.id and a.cons_quantity >0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.order_id=0 $company_id $item_category_id $store_id $item_account group by a.prod_id, b.detarmination_id, b.gsm, b.dia_width order by a.prod_id ASC";	
		//echo $sql;//die;// and a.transaction_type in (1,3,5)
	$result = sql_select($sql);
	$i=1;
	ob_start();	
	?>
    <div >
        <table style="width:1480px" border="1" cellpadding="2" cellspacing="0"  id="caption" rules="all"> 
            <thead>
                <tr class="form_caption" style="border:none;">
                    <td colspan="17" align="center" style="border:none;font-size:16px; font-weight:bold" ><?php echo $report_title; ?></td> 
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td colspan="17" align="center" style="border:none; font-size:14px;">
                       <b>Company Name : <?php echo $companyArr[$cbo_company_name]; ?></b>                               
                    </td>
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td colspan="17" align="center" style="border:none;font-size:12px; font-weight:bold">
                        <?php if($from_date!="" || $to_date!="")echo "From : ".change_date_format($from_date,'','',1)." To : ".change_date_format($to_date,'','',1)."" ;?>
                    </td>
                </tr>
            </thead>
        </table>
        <table style="width:1480px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="">
        	<thead>
                <tr>
                    <th rowspan="2" width="40">SL</th>
                    <th rowspan="2" width="60">Prod.ID</th>
                    <th colspan="4">Description</th>
                    <th rowspan="2" width="110">Opening Stock</th>
                    <th colspan="4">Receive</th>
                    <th colspan="4">Issue</th>
                    <th rowspan="2" width="100">Closing Stock</th>
                    <th rowspan="2" width="60">DOH</th>
                </tr> 
                <tr>                         
                    <th width="120">Construction</th>
                    <th width="180">Composition</th>
                    <th width="70">GSM</th>
                    <th width="100">Dia/Width</th>
                    
                    <th width="80">Receive</th>
                    <th width="60">Receive Return</th>
                    <th width="80">Transfer In</th>
                    <th width="100">Total Receive</th>
                    
                    <th width="80">Issue</th>
                    <th width="60">Issue Return</th>
                    <th width="80">Transfer Out</th>
                    <th width="100">Total Issue</th> 
                </tr> 
            </thead>
        </table>
        <div style="width:1500px; max-height:200px; overflow-y:scroll" id="scroll_body" > 
        <table style="width:1482px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body">
        <?php
			$composition_arr=array();
			$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
			$data_array=sql_select($sql_deter);
			if(count($data_array)>0)
			{
				foreach( $data_array as $row )
				{
					if(array_key_exists($row[csf('id')],$composition_arr))
					{
						$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
					}
					else
					{
						$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
					}
				}
			}
			
            foreach($result as $row)
            {
                if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 

                $issue_sql = "select 
						sum(case when a.transaction_type=2 and a.transaction_date between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as issue,
						sum(case when a.transaction_type=4 and a.transaction_date between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as issue_return,
						sum(case when a.transaction_type=6 and a.transaction_date between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."' then a.cons_quantity else 0 end) as transfer_out
						from inv_transaction a
                        where a.cons_quantity>0 and a.transaction_type in (2,4,6) and a.prod_id=".$row[csf("prod_id")]." and a.item_category in (13,14) and a.status_active=1 and a.is_deleted=0 and a.order_id=0";
						
				$issue_result = sql_select($issue_sql);
				
				$opening=$row[csf("rcv_total_opening")]-$row[csf("iss_total_opening")];
				
				$totalReceive = $row[csf("receive")]+$row[csf("receive_return")]+$row[csf("transfer_in")];
				$totalIssue = $issue_result[0][csf('issue')]+$issue_result[0][csf('issue_return')]+$issue_result[0][csf('transfer_out')];
				$closingStock=$opening+$totalReceive-$totalIssue;
				$totalStockValue+=$closingStock;
				
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
						<td width="40"><?php echo $i; ?></td>	
                        <td width="60" align="center"><p><?php echo $row[csf("prod_id")]; ?></p></td>
						<td width="120"><?php echo $determinaArr[$row[csf("detarmination_id")]]; ?></td>                                 
						<td width="180"><p><?php echo $composition_arr[$row[csf('detarmination_id')]]; ?></p></td>
						<td width="70"><p><?php echo $row[csf("gsm")]; ?></p></td> 
						<td width="100"><p><?php echo $row[csf("dia_width")]; ?></p></td> 
						<td width="110" align="right"><p><?php echo number_format($opening,2); $tot_opening+=$opening; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($row[csf("receive")],2); $tot_receive+=$row[csf("receive")]; ?></p></td>
						<td width="60" align="right"><p><?php echo number_format($row[csf("receive_return")],2); $tot_rec_return+=$row[csf("receive_return")]; ?></p></td>
                        <td width="80" align="right"><p><?php echo number_format($row[csf("transfer_in")],2); $tot_transfer_in+=$row[csf("transfer_in")]; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalReceive,2); $total_receive+=$totalReceive; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($issue_result[0][csf('issue')],2); $tot_issue+=$issue_result[0]['issue']; ?></p></td>
						<td width="60" align="right"><p><?php echo number_format($issue_result[0][csf('issue_return')],2); $tot_issue_return+=$issue_result[0]['issue_return']; ?></p></td>
                        <td width="80" align="right"><p><?php echo number_format($issue_result[0][csf('transfer_out')],2); $tot_transfer_out+=$issue_result[0]['transfer_out']; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalIssue,2); $total_issue+=$totalIssue; ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($closingStock,2); ?></p></td>
						<?php
						if ($db_type==0)
						{
							$returnRes=explode(",",return_field_value("concat(min(transaction_date),',',max(transaction_date))","inv_transaction","prod_id=".$row[csf("prod_id")]));
						}
						else if ($db_type==2)
						{
							$returnRes=explode(",",return_field_value("(min(transaction_date) || ',' || max(transaction_date)) as trns_date","inv_transaction","prod_id=".$row[csf("prod_id")],'trns_date'));
						}
							//echo $returnRes;die;
							$daysOnHand = datediff("d",change_date_format($returnRes[1],'','',1),change_date_format(date("Y-m-d"),'','',1));
						?>
						<td width="60" align="center"><?php echo $daysOnHand; ?></td>
					</tr>
                <?php 												
                 $i++; 				
			}
		?>
        	</tbody>
        </table>
        <table style="width:1480px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" id="table_header_1" rules="all"> 
        	<tfoot>
               <tr>
                    <th width="40" align="right">&nbsp;</th>
                    <th width="60" align="right">&nbsp;</th>  
                    <th width="120" align="right">&nbsp;</th>
                    <th width="180" align="right">&nbsp;</th>
                    <th width="70" align="right">&nbsp;</th>
                    <th width="100" align="right"><strong>Total : </strong></th>

                    <th width="110" align="right" id="tot_opening"><?php echo number_format($tot_opening,2);  ?></th>
                    <th width="80" align="right" id="tot_receive"><?php echo number_format($tot_receive,2);  ?></th>
                    <th width="60" align="right" id="tot_rec_return"><?php echo number_format($tot_rec_return,2);  ?></th>
                    <th width="80" align="right" id="tot_transfer_in"><?php echo number_format($tot_transfer_in,2);  ?></th>
                    <th width="100" align="right" id="total_receive"><?php echo number_format($total_receive,2);  ?></th>
                    
                    <th width="80" align="right" id="tot_issue"><?php echo number_format($tot_issue,2);  ?></th>
                    <th width="60" align="right" id="tot_issue_return"><?php echo number_format($tot_issue_return,2);  ?></th>
                    <th width="80" align="right" id="tot_transfer_out"><?php echo number_format($tot_transfer_out,2);  ?></th>
                    <th width="100" align="right" id="total_issue"><?php echo number_format($total_issue,2);  ?></th>
                    <th width="100" align="right" id="totalStockValue"><strong><?php echo number_format($totalStockValue,2); ?></strong></th>
                    <th width="60" align="right">&nbsp;</th>
                </tr>
            </tfoot>
        </table>
        </div>  
        </div>
    <?php
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}
?>