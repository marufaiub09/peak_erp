<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Supplier List Report
				
Functionality	:	
JS Functions	:
Created by		:	Kausar 
Creation date 	: 	15/12/2013
Updated by 		: Maruf		
Update date		: 09-12-2015		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Supplier List", "../../", 1, 1, $unicode,1,'');
?>	
<script>
var permission='<?php echo $permission; ?>';
	
	function openmypage_supplier()
	{
		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
			var data = $("#cbo_company_id").val();
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/supplier_list_controller.php?data='+data+'&action=supplier_popup', 'Supplier Search', 'width=600px,height=420px,center=1,resize=0,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var theemailid=this.contentDoc.getElementById("supplier_id");
				var theemailval=this.contentDoc.getElementById("supplier_val");
				if (theemailid.value!="" || theemailval.value!="")
				{
					freeze_window(5);
					$("#txt_supplier").val(theemailval.value);
					$("#txt_supplier_id").val(theemailid.value);
					release_freezing();
				}
			}
		}
	}
	
	function fn_report_generated()
	{
		if(form_validation('cbo_company_id','Company Name')==false)
		{
			return;
		}
		else
		{	
			var report_title=$( "div.form_caption" ).html();
			var data="action=report_generate"+get_submitted_data_string('cbo_company_id*cbo_party_type*txt_supplier_id',"../../")+'&report_title='+report_title;
			freeze_window(3);
			http.open("POST","requires/supplier_list_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
		
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			var tot_rows=reponse[2];
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
	 		show_msg('3');
			release_freezing();
		}
	}

</script>
</head>
<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
		<?php echo load_freeze_divs ("../../");  ?>
        <form id="supplierList_1">
         <h3 style="width:850px; margin-top:20px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3> 
         <div id="content_search_panel" style="width:850px" >      
            <fieldset>  
                <table cellpadding="0" cellspacing="2" width="800px">
                    <tr>
                        <td width="100" class="must_entry_caption"><strong>Company </strong></td>
                        <td width="150">
							<?php 
								echo create_drop_down( "cbo_company_id", 145, "select id,company_name from lib_company where status_active=1 and is_deleted=0 order by company_name","id,company_name", 1, "-- Select Company --", $selected,"" );
                            ?>
                        </td>
                        <td width="100"><strong>Party Type </strong></td>
                        <td width="145">
							<?php 
								echo create_drop_down( "cbo_party_type", 140, $party_type_supplier, "", 0, "", '', 'set_value_supplier_nature(this.value)', $onchange_func_param_db,$onchange_func_param_sttc  ); 
                            ?>				
<!--                        	<input type="text" name="txt_party_type" id="txt_party_type" class="text_boxes" style="width:140px" value="" placeholder="Double Click" onDblClick="openmypage_partyType();" /><input type="text" name="txt_part_id" id="txt_part_id" class="text_boxes" style="width:140px" />
-->                        </td>
                        <td width="100"><strong>Supplier </strong></td>
                        <td width="140">
                        	<input type="text" name="txt_supplier" id="txt_supplier" class="text_boxes" style="width:140px" placeholder="Double Click" onDblClick="openmypage_supplier();" /><input type="hidden" name="txt_supplier_id" id="txt_supplier_id" class="text_boxes" style="width:140px" />
                        </td>
                        <td>
                            <input type="button" name="search" id="search" value="Show" onClick="fn_report_generated()" style="width:80px" class="formbutton" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            </div>
        </form>
        <div id="report_container" align="center"></div>
        <div id="report_container2"> 
    </div>
</body>
<script>
	set_multiselect('cbo_party_type','0*0','0','','0');
</script>

<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>