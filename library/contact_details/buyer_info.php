<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//----------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Buyer Info","../../",1 ,1 ,$unicode,1,'' );
?>
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission='<?php echo $permission; ?>';
 
function fnc_buyer_info( operation )
{
	if(operation==2)
		{
	var delt=confirm('Reday to Delete ?');
	if(delt)
	{
		var buyer_id_check=$("#buyer_id").val();
		var cbo_party_type=document.getElementById('cbo_party_type').value.split(',');
		
		if (form_validation('txt_buyer_name*txt_short_name*cbo_party_type*cbo_buyer_company','Buyer Name*Short Name*Party Type*Tag Company*')==false)
		{
			return;
		}
		else if($.inArray('90',cbo_party_type)> -1 && form_validation('cbo_buyer_supplier','Link to Supplier')==false )
		{
			alert(" Select Link To Supplier");
			return;
		}
		else // Save Here
		{
			
			//eval(get_submitted_variables('txt_buyer_name*txt_short_name*txt_contact_person*txt_contact_no*cbo_party_type*txt_desination*cbo_buyer_company*cbo_country*txt_web_site*txt_buyer_email*txt_address_1st*txt_address_2nd*txt_address_3rd*txt_address_4th*cbo_buyer_supplier*cbo_status*txt_remark*txt_credit_limit_days*txt_credit_limit_amount*cbo_credit_limit_amount_curr*cbo_discount_method*cbo_security_deducted*cbo_vat_to_be_deducted*cbo_ait_to_be_deducted*update_id'));
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_buyer_name*txt_short_name*txt_contact_person*txt_exporter_ref*cbo_party_type*txt_desination*cbo_buyer_company*cbo_country*txt_web_site*txt_buyer_email*txt_address_1st*txt_address_2nd*txt_address_3rd*txt_address_4th*cbo_buyer_supplier*cbo_status*txt_remark*txt_credit_limit_days*txt_credit_limit_amount*cbo_credit_limit_amount_curr*cbo_discount_method*cbo_security_deducted*cbo_vat_to_be_deducted*cbo_ait_to_be_deducted*txt_sewing_effi_mkt*txt_sewing_effi_planing*update_id*cbo_marketing_team*hidden_buyer_id',"../../");
			//alert(data);return;
			freeze_window(operation);
			http.open("POST","requires/buyer_info_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_on_submit_reponse;
		}
	}
	else
	{
	return false;	
	}
		}
	else if(operation==0 || operation==1)
	{
		var buyer_id_check=$("#buyer_id").val();
		var cbo_party_type=document.getElementById('cbo_party_type').value.split(',');
		
		if (form_validation('txt_buyer_name*txt_short_name*cbo_party_type*cbo_buyer_company','Buyer Name*Short Name*Party Type*Tag Company*')==false)
		{
			return;
		}
		else if($.inArray('90',cbo_party_type)> -1 && form_validation('cbo_buyer_supplier','Link to Supplier')==false )
		{
			alert(" Select Link To Supplier");
			return;
		}
		else // Save Here
		{
			
			//eval(get_submitted_variables('txt_buyer_name*txt_short_name*txt_contact_person*txt_contact_no*cbo_party_type*txt_desination*cbo_buyer_company*cbo_country*txt_web_site*txt_buyer_email*txt_address_1st*txt_address_2nd*txt_address_3rd*txt_address_4th*cbo_buyer_supplier*cbo_status*txt_remark*txt_credit_limit_days*txt_credit_limit_amount*cbo_credit_limit_amount_curr*cbo_discount_method*cbo_security_deducted*cbo_vat_to_be_deducted*cbo_ait_to_be_deducted*update_id'));
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_buyer_name*txt_short_name*txt_contact_person*txt_exporter_ref*cbo_party_type*txt_desination*cbo_buyer_company*cbo_country*txt_web_site*txt_buyer_email*txt_address_1st*txt_address_2nd*txt_address_3rd*txt_address_4th*cbo_buyer_supplier*cbo_status*txt_remark*txt_credit_limit_days*txt_credit_limit_amount*cbo_credit_limit_amount_curr*cbo_discount_method*cbo_security_deducted*cbo_vat_to_be_deducted*cbo_ait_to_be_deducted*txt_sewing_effi_mkt*txt_sewing_effi_planing*update_id*cbo_marketing_team*hidden_buyer_id',"../../");
			//alert(data);return;
			freeze_window(operation);
			http.open("POST","requires/buyer_info_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_on_submit_reponse;
		}	
	}
}

function fnc_on_submit_reponse()
{
	if(http.readyState == 4) 
	{
		var reponse=trim(http.responseText).split('**');
		if(reponse[0]==50)
		{
			alert (reponse[1]);
			release_freezing();
			return;
		}
		//alert(reponse[0]);
		show_msg(reponse[0]);
		show_list_view(reponse[1],'show_buyer_list_view','list_view_div','../contact_details/requires/buyer_info_controller','setFilterGrid("list_view",-1)');
		reset_form('buyerinfo_1','','');
		set_button_status(0, permission, 'fnc_buyer_info');
		release_freezing();
	}
} 
/*function suplier_anable()
 {
	if($("#cbo_party_type").val()==90)
	alert($("#cbo_party_type").val())
    
};*/
</script>
</head>	
<body onLoad="set_hotkey()">

<div align="center" style="width:100%;">
	<?php echo load_freeze_divs ("../../",$permission);  ?>
	<fieldset style="width:1010px;">
    <legend>Contact Info</legend>
		
		 <form name="buyerinfo_1" id="buyerinfo_1" autocomplete="off">	
			<table cellpadding="0" cellspacing="2" border="0" width="100%">
			  <tr>
                    <td width="130" class="must_entry_caption">Contact Name  </td>
                    <td width="180">
                   		<input type="text" name="txt_buyer_name" id="txt_buyer_name" class="text_boxes" style="width:180px" maxlength="100" title="Maximum 100 Character" />
                        <input type="hidden" name="hidden_buyer_id" id="hidden_buyer_id" />  

                    </td>
                    <td width="130" class="must_entry_caption">
                        Short Name 
                    </td>
                    <td width="180">
                        <input type="text" name="txt_short_name" id="txt_short_name" class="text_boxes" style="width:180px" maxlength="10" title="Maximum 10 Character"/>						
                    </td>
                     
                    <td>
                        Contact Person
                    </td>
                    <td>
                        <input type="text" name="txt_contact_person" id="txt_contact_person" class="text_boxes" style="width:180px"  maxlength="100" title="Maximum 100 Character"/>						
                    </td>
                </tr>			
				<tr>
                    <td width="130">
                        Designation</td>
                    <td width="180">
                        <input type="text" name="txt_designation" id="txt_desination" class="text_boxes" style="width:180px" maxlength="50" title="Maximum 50 Character"/>						
                    </td>
                	
                    <td>
                        Exporters Ref.
                    </td>
                    <td>
                        <input type="text" name="txt_exporter_ref" id="txt_exporter_ref" class="text_boxes" style="width:180px" maxlength="20" title="Maximum 20 Character"/>						
                    </td>
                    <td>
                        Email
                    </td>
                    <td>
                        <input type="text" name="txt_buyer_email" id="txt_buyer_email" class="text_boxes" style="width:180px;" maxlength="100" title="Maximum 100 Character"/>						
                    </td>

                </tr>
                <tr>
                    <td>
                        http://www.
                    </td>
                    <td>
                        <input type="text" name="txt_web_site" id="txt_web_site" class="text_boxes" style="width:180px" maxlength="30" title="Maximum 30 Character"/>						
                    </td>
                    <td>
                        Address1
                    </td>
                    <td>
                        <textarea name="txt_address_1st" id="txt_address_1st" class="text_area" style="width:180px;" maxlength="500" title="Maximum 500 Character"></textarea>
                                        
                    </td>
                    <td>
                        Address2
                    </td>
                    <td>
                        <textarea name="txt_address_2nd" id="txt_address_2nd" class="text_area" style="width:180px;" maxlength="500" title="Maximum 500 Character"></textarea>
                                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Address3
                    </td>
                    <td>
                        <textarea name="txt_address_3rd" id="txt_address_3rd" class="text_area" style="width:180px;" maxlength="500" title="Maximum 500 Character"></textarea>
                                        
                    </td>
                    <td>
                        Address4
                    </td>
                    <td>
                        <textarea name="txt_address_4th" id="txt_address_4th" class="text_area" style="width:180px;" maxlength="500" title="Maximum 500 Character"></textarea>
                                        
                    </td>
                    <td>
                        Country
                    </td>
                    <td>
                    <?php echo create_drop_down( "cbo_country", 190, "select id,country_name from  lib_country where is_deleted=0 and status_active=1 order by country_name", "id,country_name", 1, "-- Select Country --", $selected_index, $onchange_func, $onchange_func_param_db,$onchange_func_param_sttc  ); ?>
                                    
                    </td>
                </tr> 
                <tr>
                	<td class="must_entry_caption">
                        Party Type
                    </td>
                    <td>
						<?php
                        echo create_drop_down( "cbo_party_type", 190, $party_type, "", 0, "", '', 'set_value_supplier_nature(this.value)', $onchange_func_param_db,$onchange_func_param_sttc  ); 
                        ?>				
                    </td>
                    <td class="must_entry_caption">
                        Tag Company
                    </td>
                    <td>
						<?php
                        echo create_drop_down( "cbo_buyer_company", 190, "select id,company_name from lib_company comp where is_deleted=0 and status_active=1 $company_cond order by company_name", "id,company_name", 0, "", '', ''  ); 
                        ?>				
                    </td>
                    <td>
                       Link to Supplier 
                    </td>
                    <td >
						<?php
						 echo create_drop_down( "cbo_buyer_supplier", 190, "select id,supplier_name from  lib_supplier where is_deleted=0 and status_active=1 order by supplier_name", "id,supplier_name", 1, "-- Select Supplier --", $selected_index, $onchange_func, $onchange_func_param_db,$onchange_func_param_sttc  ); 
                        ?>
                	</td>
                </tr>
                <tr>
                	<td>
                        Credit Limit (Days)
                    </td>
                    <td>
					    <input type="text" name="txt_credit_limit_days" id="txt_credit_limit_days" class="text_boxes_numeric" style="width:180px" />						
                    </td>
                    <td>
                        Credit Limit (Amount)
                    </td>
                    <td>
                    <input type="text" name="txt_credit_limit_amount" id="txt_credit_limit_amount" class="text_boxes_numeric" style="width:100px" />	
                    					

						<?php
                        echo create_drop_down( "cbo_credit_limit_amount_curr",75, $currency, "", 0, "", '', ''  ); 
                        ?>				
                    </td>
                    <td>
                        Discount Method
                    </td>
                    <td >
						<?php
						 echo create_drop_down( "cbo_discount_method", 190, $currency, "", 1, "-- Select Method --", $selected_index, $onchange_func, $onchange_func_param_db,$onchange_func_param_sttc  ); 
                        ?>
                	</td>
                </tr>
                <tr>
                <tr>
                	<td>
                        Security deducted
                    </td>
                    <td>
					    <?php
						 echo create_drop_down( "cbo_security_deducted", 190, $yes_no, "", 0, "", "", "", "",""); 
                        ?>					
                    </td>
                    <td>
                        VAT to be deducted
                    </td>
                    <td>
						<?php
                        echo create_drop_down( "cbo_vat_to_be_deducted", 190,  $yes_no, "", 0, "", "", "", "",""); 
                        ?>				
                    </td>
                    <td>
                        AIT to be deducted
                    </td>
                    <td >
						<?php
						 echo create_drop_down( "cbo_ait_to_be_deducted",190, $yes_no, "", 0, "", "", "", "",""); 
                        ?>
                	</td>
                </tr>
                <tr>
                    
                    <td>
                        Remark
                    </td>
                    <td colspan="3">
                        <input type="text" name="txt_remark" id="txt_remark" class="text_boxes" style="width:503px" maxlength="500" title="Maximum 500 Character"999/>
                    </td>
                     <td>
                        Marketing Team
                    </td>
                    <td >
						
                         <?php
						 echo create_drop_down( "cbo_marketing_team", 190, "select id,team_name from  lib_marketing_team where is_deleted=0 and status_active=1 order by team_name", "id,team_name", 1, "-- Select Marketing Team --", $selected_index, $onchange_func, $onchange_func_param_db,$onchange_func_param_sttc  ); 
						 ?>

                	</td>
                </tr>	
				<tr>
                     <td>
                       Sewing Effi Mkt. %
                    </td>
                    <td>
					    <input type="text" name="txt_sewing_effi_mkt" id="txt_sewing_effi_mkt" class="text_boxes_numeric" style="width:180px" />						
                    </td>
                    <td>
                       Sewing Effi Planing %
                    </td>
                    <td>
					    <input type="text" name="txt_sewing_effi_planing" id="txt_sewing_effi_planing" class="text_boxes_numeric" style="width:180px" />						
                    </td>
                    <td>
                        Status
                    </td>
                    <td  >
						
                         <?php
						 echo create_drop_down( "cbo_status", 190, $row_status,'', $is_select, $select_text, 1, $onchange_func, "",'','' );
						 ?>

                	</td>
                </tr>
                <tr>
                   
                    <td>
                        Image
                    </td>
                    <td height="25" valign="middle">
                    	<input type="button" class="image_uploader" style="width:192px" value="CLICK TO ADD/VIEW IMAGE" onClick="file_uploader ( '../../', document.getElementById('update_id').value,'', 'buyer_info', 0 ,1)">
                    </td>
                </tr>
                		  
				 <tr>
                    <td colspan="4" align="center" height="20" valign="middle" > 
						<input type="hidden" name="update_id" id="update_id" />  
                       <!--  <input type="button" value="asasd" onClick="set_multiselect('cbo_party_type','0','1','2,3,90','__set_supplier_status__../contact_details/requires/buyer_info_controller');" > -->
                     <!--function set_multiselect( fld_id, max_selection, is_update, update_values, on_close_fnc_param )-->
               
                    </td>					
                </tr>	 
            
                <tr>
                    <td colspan="6" align="center" height="40" valign="middle" class="button_container"> 
                    <?php
                        echo load_submit_buttons($permission, "fnc_buyer_info", 0,0 ,"reset_form('buyerinfo_1','','')",1);
                    ?> 
                    </td>					
                </tr>				
			</table>
		  </form>
	</fieldset>	
	
	<div style="width:100%; float:left; margin:auto" align="center" id="search_container">
		<fieldset style="width:1010px; margin-top:10px">
            	<table width="1010" cellspacing="2" cellpadding="0" border="0">
                    
					<tr>
						<td colspan="3">
                        <div id="list_view_div">
                        	<?php
				$arr=array (2=>$party_type,7=>$currency,8=>$row_status);
	echo  create_list_view ( "list_view", "Contact Name,Short Name, Sewing Effi Mkt. %,Contact Person,Designation,Credit Limit(Days),Credit Limit (Amount),Currency, Status", "150,100,150,100,120,100,100,70","1010","220",0, "select buyer_name,short_name,sewing_effi_mkt_percent,contact_person,designation,credit_limit_days,credit_limit_amount, 	credit_limit_amount_currency,status_active,id from lib_buyer where is_deleted=0", "get_php_form_data", "id", "'load_php_data_to_form'", 1, "0,0,0,0,0,0,0,credit_limit_amount_currency,status_active", $arr , "buyer_name,short_name,sewing_effi_mkt_percent,contact_person,designation,credit_limit_days,credit_limit_amount,credit_limit_amount_currency,status_active", "../contact_details/requires/buyer_info_controller", 'setFilterGrid("list_view",-1);','0,0,1,0,0,1,1,0,0');
	
							/*$arr=array (7=>$currency,8=>$row_status);
							echo  create_list_view ( "list_view", "Contact Name,Contact Person,Designation,Contact NO,Email,Credit Limit(Days),Credit Limit (Amount),Currency, Status", "150,100,100,100,120,100,100,70","1010","220",0, "select  buyer_name,contact_person,designation,contact_no,buyer_email,credit_limit_days,credit_limit_amount, 	credit_limit_amount_currency,status_active,id from lib_buyer where is_deleted=0", "get_php_form_data", "id", "'load_php_data_to_form'", 1, "0,0,0,0,0,0,0,credit_limit_amount_currency,status_active", $arr , "buyer_name,contact_person,designation,contact_no,buyer_email,credit_limit_days,credit_limit_amount,credit_limit_amount_currency,status_active", "../contact_details/requires/buyer_info_controller", 'setFilterGrid("list_view",-1);','0,0,0,0,0,1,1,0,0') ;*/
							
							 ?>
                        </div>
						</td>
					</tr>
				</table>
		</fieldset>	
	</div>
</div>
</body>

<script>
	set_multiselect('cbo_party_type*cbo_buyer_company','0*0','0','','0*0');
</script>

<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>