<? 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../includes/common.php');


$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($_SESSION['logic_erp']["data_level_secured"]==1)
{
	if($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_cond=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_cond="";
	if($_SESSION['logic_erp']["company_id"]!=0) $company_cond=" and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_cond="";
}
else
{
	$buyer_cond="";	$company_cond="";
}
$permission=$_SESSION['page_permission'];

//---------------------------------------------------- Start---------------------------------------------------------------------------
$po_number=return_library_array( "select id,po_number from wo_po_break_down where job_no_mst='".$txt_job_no."'", "id", "po_number"  );
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$size_library=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );

if ($txt_order_no_id=="") $action="auto_generate_booking"; 

if ($action=="auto_generate_booking")
{
	if ($txt_order_no_id!="")
	{
		//delete old Process data
		
		 $str_cond=" and c.po_break_down_id in (".$txt_order_no_id.") "; 
	}
	else $str_cond="";
	
	$nameArray=sql_select( "select  a.id as pre_cost_fabric_cost_dtls_id,a.item_number_id,a.body_part_id,a.color_type_id,a.gsm_weight,a.color_size_sensitive,a.rate,a.amount, a.costing_per,b.po_break_down_id,b.dia_width,b.cons,b.requirment,b.pcs,c.id as color_size_table_id,c.size_number_id,c.color_number_id,c.plan_cut_qnty 
	
	FROM wo_pre_cost_fabric_cost_dtls a,wo_pre_cos_fab_co_avg_con_dtls b, wo_po_color_size_breakdown c
WHERE a.job_no=b.job_no and a.id=b.pre_cost_fabric_cost_dtls_id  and c.job_no_mst=a.job_no and c.id=b.color_size_table_id  $str_cond" );  
	
	$i=1;
	$id=return_next_id( "id", "wo_fabric_booking_auto_gen", 1 ) ;
	$field_array="id,po_break_down_id,pre_cost_fabric_cost_dtls_id,color_size_table_id,fin_fab_qnty,grey_fab_qnty,rate,amount";
	foreach ($nameArray as $result)
	{
		$costing_per_qnty=0;
		if($result[csf("costing_per")]==1)
		{
			$costing_per_qnty=1*12;
		}
		if($result[csf("costing_per")]==2)
		{
			$costing_per_qnty=1*1;
		}
		if($result[csf("costing_per")]==3)
		{
			$costing_per_qnty=2*12;
		}
		if($result[csf("costing_per")]==4)
		{
			$costing_per_qnty=3*12;
		}
		if($result[csf("costing_per")]==5)
		{
			$costing_per_qnty=4*12;
		}
		$finish_fab_qnty=(($result[csf("plan_cut_qnty")]/$costing_per_qnty)*$result[csf("cons")]);
		$grey_fab_qnty=(($result[csf("plan_cut_qnty")]/$costing_per_qnty)*$result[csf("requirment")]);
		$amount= $grey_fab_qnty*$result[csf("rate")];
		if ($i!=1) $data_array .=",";
		$data_array .="(".$id.",".$result[csf("po_break_down_id")].",".$result[csf("pre_cost_fabric_cost_dtls_id")].",".$result[csf("color_size_table_id")].",".$finish_fab_qnty.",".$grey_fab_qnty.",".$result[csf("rate")].",".$amount.")";
		$id++;
		$i++;
	} // for each name arra
	$rID=sql_insert("wo_fabric_booking_auto_gen",$field_array,$data_array,1);
}
?>