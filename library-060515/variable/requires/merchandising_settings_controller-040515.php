<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if ($action=="on_change_data")
{
	
	$explode_data = explode("_",$data);
	$type = $explode_data[0];
	$company_id = $explode_data[1];
	
	if($type=="12") // sales year started
	{
	
			$nameArray=sql_select( "select sales_year_started,id from  variable_order_tracking where company_name='$company_id' and variable_list=12 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
				<legend>Sales Year started</legend>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td width="100" align="left" id="sales_year_started">Sales Year started</td>
								<td width="190">
                                     <? 
										echo create_drop_down( "cbo_sales_year_started_date", 170, $months,'', 1, '---- Select ----', $nameArray[0][csf('sales_year_started')], "" );
									?>    
 							</td>
						</tr>
					</table>
				</div>
				<div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td align="center" width="320">&nbsp;</td>						
 						</tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','')",1);
                                ?>
                            </td>					
                        </tr>
					</table>
				</div>
			</fieldset>
<?    				
			
	}
    
    
	
	if( $type==14 ) //Order Tracking_TNA Integrated
	{
			 
			$nameArray=sql_select( "select tna_integrated,id from  variable_order_tracking where company_name='$company_id' and variable_list=14 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
			<legend>TNA Integrated</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="100" align="left"  id="tna_integrated_td">TNA Integrated</td>
                                <td width="190">
                                 
                                <? 
									echo create_drop_down( "cbo_tna_integrated", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('tna_integrated')], "",'','' );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>						
                         </tr>
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
        	</div>
<?    				
			
	}
	


	if( $type==15 ) //Order Tracking_Pre_Costing_Profit_Calculative
	{
	
			$nameArray=sql_select( "select profit_calculative,id from  variable_order_tracking where company_name='$company_id' and variable_list=15 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Pre Costing : Profit Calculative</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="100" align="left" id="profit_calculative_td" class="must_entry_caption">Profit Calculative</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_profit_calculative", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('profit_calculative')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}
	
	
	if( $type==18 )//Commercial Possible heads for BTB
	{
			
			$nameArray=sql_select( "select process_loss_method,item_category_id,id from  variable_order_tracking where company_name='$company_id' and variable_list=18 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
                <div>
                   <fieldset>
                        <legend>Process Loss Method</legend>
                        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                            <table cellspacing="0" width="100%" class="rpt_table">
                                <thead> 
                                    <th width="200" align="left">Item Category</th>
                                    <th width="190" align="left">Process Loss Method</th>
                                </thead>
                                <?
                                $i=1;
								$id_req=array(2,3,4,12,13,14);
                                foreach($item_category as $key=>$value)
                                {
                                  	if (in_array($key,$id_req))
                                    {
                      						 $resultRow= sql_select("select id,item_category_id,process_loss_method from variable_order_tracking where company_name='$company_id' and item_category_id='$key' and variable_list=18 order by id");
                                				 
								?>
                                        <tr align="center">
                                            <td>
                                               	 	<? 
														 
								echo create_drop_down( "item_category_id_".$i, 170, $item_category,'', 0, '', $resultRow[0][csf('item_category_id')], "",'',$key );
								 					?>
                                            </td>
                                            <td>
                                                    <? 
														echo create_drop_down( "process_loss_method_".$i, 150, $process_loss_method,'', 0, '', $resultRow[0][csf('process_loss_method')], "" );
													?>												
                                            </td>
                                        </tr>                                       
                                <?
                                	
									if($i==1) $updids .= $resultRow[0][csf('id')]; else $updids .= ",".$resultRow[0][csf('id')];
									$i++;
									}
                                }
                                ?>
                            </table>
                        </div>
                        <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                            <table cellspacing="0" width="100%" >
                                <tr> 
                                    <td align="center" width="320">&nbsp;</td>
                                </tr>
                                <tr>
                                   <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                        <input  type="hidden"name="update_id" id="update_id" value="<? echo $updids; ?>">
										<? 
                                            //$permission=$_SESSION['page_permission'];
                                            echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                        ?>
                                    </td>					
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
<?    				
			
	}



if( $type==19 ) //Consumption Basis
	{
	
			$nameArray=sql_select( "select consumption_basis,id from  variable_order_tracking where company_name='$company_id' and variable_list=19 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Consumption Basis</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="100" align="left" id="consumption_td">Consumption Basis</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_consumption_basis", 170, $consumtion_basis,'', 1, '---- Select ----', $nameArray[0][csf('consumption_basis')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}


if( $type==20 ) //Consumption Basis
	{
	
			$nameArray=sql_select( "select copy_quotation,id from  variable_order_tracking where company_name='$company_id' and variable_list=20 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Copy Quotation</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="100" align="left" id="copy_quotation_td">Copy Quotation</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_copy_quotation", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('copy_quotation')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}
	
	if( $type==21 ) //Consumption Basis
	{
	
			$nameArray=sql_select( "select conversion_from_chart,id from  variable_order_tracking where company_name='$company_id' and variable_list=21 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Conversion From Chart</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="100" align="left" id="conversion_from_chart_td">Conversion From Chart</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_conversion_from_chart", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('conversion_from_chart')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}
	
	if( $type==22 ) //Consumption Basis
	{
	
			$nameArray=sql_select( "select cm_cost_method,id from  variable_order_tracking where company_name='$company_id' and variable_list=22 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Conversion From Chart</legend>
                <div style="width:800px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="100" align="left" id="cbo_cm_cost_method_td">CM Cost Predefined Method</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_cm_cost_method", 700, $cm_cost_predefined_method,'', 1, '---- Select ----', $nameArray[0][csf('cm_cost_method')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}

if( $type==23 ) //Consumption Basis
	{
	
			$nameArray=sql_select( "select color_from_library,id from  variable_order_tracking where company_name='$company_id' and variable_list=23 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Conversion From Chart</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="100" align="left" id="color_from_library_td">Color From Library</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_color_from_library", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('color_from_library')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}
	

if( $type==24 ) //Consumption Basis
	{
	
			$nameArray=sql_select( "select color_from_library,id from  variable_order_tracking where company_name='$company_id' and variable_list=24 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Conversion From Chart</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="150" align="left" id="yarn_dyeing_charge_td">Yarn Dyeing Charge (In WO) from Chart</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_yarn_dyeing_charge", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('color_from_library')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}
	
	
	
	if( $type==25 ) //Consumption Basis
	{
	
			$nameArray=sql_select( "select publish_shipment_date,id from  variable_order_tracking where company_name='$company_id' and variable_list=25 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Conversion From Chart</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="150" align="left" id="publish_shipment_date_td">Publish Shipment Date</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "publish_shipment_date", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('publish_shipment_date')], "" );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}
	
	if( $type==26 )//Commercial Possible heads for BTB
	{
			
			$nameArray=sql_select( "select exeed_budge_qty,exeed_budge_amount,amount_exceed_level,item_category_id,id from  variable_order_tracking where company_name='$company_id' and variable_list=26 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
                <div>
                   <fieldset>
                        <legend>Process Loss Method</legend>
                        <div style="width:600px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                            <table cellspacing="0" width="100%" class="rpt_table">
                                <thead> 
                                    <th width="150" align="left">Item Category</th>
                                    <th width="150" align="left">Exceed Budget Qty(%)</th>
                                    <th width="150" align="left">Exceed Budget Amount(%)</th>
                                    <th width="150" align="left">Amount Exceed Level</th>
                                </thead>
                                <?
                                $i=1;
								//$id_req=array(2,3,4,12,13,14);
                              //  foreach($item_category as $key=>$value)
                              //  {
                                //  	if (in_array($key,$id_req))
                               //     {
                      						// $resultRow= sql_select("select id,item_category_id,process_loss_method from variable_order_tracking where company_name='$company_id' and item_category_id='$key' and variable_list=18 order by id");
								$id_req=array(4);
								foreach($item_category as $key=>$value)
								{
								  if (in_array($key,$id_req))
								  {
                                	$resultRow= sql_select("select id,item_category_id, exeed_budge_qty,exeed_budge_amount,amount_exceed_level from variable_order_tracking where company_name='$company_id' and item_category_id='$key' and variable_list=26 order by id");			 
							    	?>
                                        <tr align="center">
                                            <td>
                                               	 	<? 
														 
							          	echo create_drop_down( "item_category_id_".$i, 150, $item_category,'', 0, '', $resultRow[0][csf('item_category_id')], "",1,$key );
								 					?>
                                            </td>
                                            <td>
                                            <input type="text" name="txt_exeed_qty_<? echo $i; ?>" id="txt_exeed_qty_<? echo $i; ?>"  class="text_boxes_numeric" value="<? echo $resultRow[0][csf('exeed_budge_qty')]; ?>" />
                                                    											
                                            </td>
                                            <td>
                                            <input type="text" name="txt_exeed_amount_<? echo $i; ?>" id="txt_exeed_amount_<? echo $i; ?>"  class="text_boxes_numeric" value="<? echo $resultRow[0][csf('exeed_budge_amount')]; ?>" />
                                                    											
                                            </td>
                                             <td>
                                               	 <? 
												  $amount_exeed_lavel=array(1=>"Total Amount",2=>"Item Amount");
							            	      echo create_drop_down( "cbo_exceed_level_".$i, 150, $amount_exeed_lavel,'', 1, '---------Select---------', $resultRow[0][csf('amount_exceed_level')], "",0,"");
								 				?>
                                            </td>
                                        </tr>                                       
                                <?
                                	
									if($i==1) $updids .= $resultRow[0][csf('id')]; else $updids .= ",".$resultRow[0][csf('id')];
									$i++;
								}
                              }
                                ?>
                            </table>
                        </div>
                        <div style="width:600px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                            <table cellspacing="0" width="100%" >
                                <tr> 
                                    <td align="center" width="100%">&nbsp;</td>
                                </tr>
                                <tr>
                                   <td colspan="4" height="40" valign="bottom" align="center" class="button_container">
                                        <input  type="hidden"name="update_id" id="update_id" value="<? echo $updids; ?>">
										<? 
                                            //$permission=$_SESSION['page_permission'];
                                            echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                        ?>
                                    </td>					
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
<?    				
			
	}
	if( $type==27 ) //Consumption Basis
	{
			$nameArray=sql_select( "select commercial_cost_method,id,commercial_cost_percent,editable from  variable_order_tracking where company_name='$company_id' and variable_list=27 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
            <legend>Commercial Cost Predefined Method</legend>
                <div style="width:800px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="150" align="left" id="cbo_commercial_cost_method_td">Commercial Cost Predefined Method</td>
                                <td width="190">
                                 <? 
									echo create_drop_down( "cbo_commercial_cost_method", 200, $commercial_cost_predefined_method,'', 1, '---- Select ----', $nameArray[0][csf('commercial_cost_method')], "" );
								?>
                            </td>
                            
                            <td width="150" align="left" id="">Commercial Cost  Percent</td>
                                <td width="190">
                                 <input type="text" name="txt_commercial_cost_percent" id="txt_commercial_cost_percent"  class="text_boxes_numeric" value="<? echo $nameArray[0][csf('commercial_cost_percent')]; ?>" />
								
                            </td>
                            <td width="150" align="left" id="">Editable</td>
                                <td width="190">
                                <? 
									echo create_drop_down( "cbo_editable", 200, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('editable')], "" );
								?>
								
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>
                        </tr>
                       <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
                                    //$permission=$_SESSION['page_permission'];
									echo load_submit_buttons( $permission, "fnc_order_tracking_variable_settings", $is_update,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                ?>
                            </td>					
                        </tr>
                    </table>
                </div>
            </fieldset>
       	 </div>
<?    				
			
	}
	exit();
}

if ($action=="save_update_delete_material_control")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$item_category_id=explode(",",$item_category_id);
	$exeed_budget_qty=explode(",",$exeed_budget_qty);
	$exeed_budget_amt=explode(",",$exeed_budget_amt);
	$amt_exceed_lavel=explode(",",$amt_exceed_lavel);
	
	if ($operation==0)  // Insert Here
	{
		 
		if (is_duplicate_field( "company_name", "variable_order_tracking", "company_name=$cbo_company_name_wo and variable_list=$cbo_variable_list_wo" ) == 1)
		{
			echo 11; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			$data_array="";
			$field_array="id,company_name,variable_list,exeed_budge_qty,exeed_budge_amount,amount_exceed_level,item_category_id,inserted_by,insert_date,status_active,is_deleted";
			for($i=0;$i<count($item_category_id);$i++)
			{			 
				if( $id=="" ) $id = return_next_id( "id", "variable_order_tracking", 1 ); else $id = $id+1;
				if($i==0)
					$data_array .= "(".$id.",".$cbo_company_name_wo.",".$cbo_variable_list_wo.",'".$exeed_budget_qty[$i]."','".$exeed_budget_amt[$i]."',".$amt_exceed_lavel[$i].",'".$item_category_id[$i]."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1,0)";
				else
					$data_array .= ",(".$id.",".$cbo_company_name_wo.",".$cbo_variable_list_wo.",'".$exeed_budget_qty[$i]."','".$exeed_budget_amt[$i]."',".$amt_exceed_lavel[$i].",'".$item_category_id[$i]."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1,0)";
			}	
			//echo $data_array;		
			$rID=sql_insert("variable_order_tracking",$field_array,$data_array,1);
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo 0;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo 10;
				}
			}			
			if($db_type==2 || $db_type==1 )
			{
				 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		}
	}
	
	else if ($operation==1)   // Update Here
	{
			
			$update_id=explode(",",$update_id); 
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			for($i=0;$i<count($item_category_id);$i++) 	 
			{						
				$field_array="company_name*variable_list*exeed_budge_qty*exeed_budge_amount*amount_exceed_level*item_category_id*updated_by*update_date";
				$data_array="".$cbo_company_name_wo."*".$cbo_variable_list_wo."*'".$exeed_budget_qty[$i]."'*'".$exeed_budget_amt[$i]."'*".$amt_exceed_lavel[$i]."*".$item_category_id[$i]."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				$rID=sql_update("variable_order_tracking",$field_array,$data_array,"id","".$update_id[$i]."",1);
			} 
			
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo 1;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo 10;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
		disconnect($con);
		die;
		
	}	
}



	



if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		 
		if (is_duplicate_field( "company_name", "variable_order_tracking", "company_name=$cbo_company_name_wo and variable_list=$cbo_variable_list_wo" ) == 1)
		{
			echo 11; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			 			 
			$id=return_next_id( "id", "variable_order_tracking", 1 ) ;
			
			$field_array="id,company_name,variable_list,sales_year_started,tna_integrated,profit_calculative,process_loss_method,item_category_id,consumption_basis,copy_quotation,conversion_from_chart,cm_cost_method,color_from_library,publish_shipment_date,commercial_cost_method,commercial_cost_percent,editable,inserted_by,insert_date,status_active"; 

			$data_array="(".$id.",".$cbo_company_name_wo.",".$cbo_variable_list_wo.",'".str_replace("'",'',$cbo_sales_year_started_date)."','".str_replace("'",'',$cbo_tna_integrated)."','".str_replace("'",'',$cbo_profit_calculative)."','".str_replace("'",'',$process_loss_methods)."','".str_replace("'",'',$item_category_ids)."','".str_replace(",",'',$cbo_consumption_basis)."','".str_replace(",",'',$cbo_copy_quotation)."','".str_replace("'",'',$cbo_conversion_from_chart)."','".str_replace("'",'',$cbo_cm_cost_method)."','".str_replace("'",'',$cbo_color_from_library)."','".str_replace("'",'',$publish_shipment_date)."','".str_replace("'",'',$cbo_commercial_cost_method)."','".str_replace("'",'',$txt_commercial_cost_percent)."','".str_replace("'",'',$cbo_editable)."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";   
  
		 $rID=sql_insert("variable_order_tracking",$field_array,$data_array,1);
			
		if($db_type==0)
			{
				if($rID )
				{
					mysql_query("COMMIT");  
					echo 0;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo 10;
				}
			}
			
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		}
	}
	
	else if ($operation==1)   // Update Here
	{
		
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			$field_array="company_name*variable_list*sales_year_started*tna_integrated*profit_calculative*process_loss_method*item_category_id*consumption_basis*copy_quotation*conversion_from_chart*cm_cost_method*color_from_library*publish_shipment_date*commercial_cost_method*commercial_cost_percent*editable*updated_by*update_date";
			
			
			$data_array="".$cbo_company_name_wo."*".$cbo_variable_list_wo."*'".str_replace("'",'',$cbo_sales_year_started_date)."'*'".str_replace("'",'',$cbo_tna_integrated)."'*'".str_replace("'",'',$cbo_profit_calculative)."'*'".str_replace("'",'',$process_loss_methods)."'*'".str_replace("'",'',$item_category_ids)."'*'".str_replace("'",'',$cbo_consumption_basis)."'*'".str_replace("'",'',$cbo_copy_quotation)."'*'".str_replace("'",'',$cbo_conversion_from_chart)."'*'".str_replace("'",'',$cbo_cm_cost_method)."'*'".str_replace("'",'',$cbo_color_from_library)."'*'".str_replace("'",'',$publish_shipment_date)."'*'".str_replace("'",'',$cbo_commercial_cost_method)."'*'".str_replace("'",'',$txt_commercial_cost_percent)."'*'".str_replace("'",'',$cbo_editable)."'*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$rID=sql_update("variable_order_tracking",$field_array,$data_array,"id","".$update_id."",1);  //   '".$cbo_color_from_library."'*'".$publish_shipment_date."'*			 
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo 1;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo 10;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
	    	disconnect($con);
		    die;
		
	}	
	
}
if ($action=="save_update_delete_process_loss_method")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$item_category_id=explode(",",$item_category_id);
	$process_loss_method=explode(",",$process_loss_method);
	
	if ($operation==0)  // Insert Here
	{
		 
		if (is_duplicate_field( "company_name", "variable_order_tracking", "company_name=$cbo_company_name_wo and variable_list=$cbo_variable_list_wo" ) == 1)
		{
			echo 11; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			$data_array="";
			$field_array="id,company_name,variable_list,sales_year_started_hcode,sales_year_started,tna_integrated,profit_calculative,process_loss_method,item_category_id,inserted_by,insert_date,status_active";
			for($i=0;$i<count($item_category_id);$i++)
			{			 
				if( $id=="" ) $id = return_next_id( "id", "variable_order_tracking", 1 ); else $id = $id+1;
				if($i==0)
					$data_array .= "(".$id.",".$cbo_company_name_wo.",".$cbo_variable_list_wo.",'".$sales_year_started."','".$cbo_sales_year_started_date."','".$cbo_tna_integrated."','".$cbo_profit_calculative."','".$process_loss_method[$i]."','".$item_category_id[$i]."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				else
					$data_array .= ",(".$id.",".$cbo_company_name_wo.",".$cbo_variable_list_wo.",'".$sales_year_started."','".$cbo_sales_year_started_date."','".$cbo_tna_integrated."','".$cbo_profit_calculative."','".$process_loss_method[$i]."','".$item_category_id[$i]."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
			}	
			//echo $data_array;		
			$rID=sql_insert("variable_order_tracking",$field_array,$data_array,1);
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo 0;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo 10;
				}
			}			
			if($db_type==2 || $db_type==1 )
			{
				 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		}
	}
	
	else if ($operation==1)   // Update Here
	{
			
			$update_id=explode(",",$update_id); 
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			for($i=0;$i<count($item_category_id);$i++) 	 
			{						
				$field_array="company_name*variable_list*process_loss_method*item_category_id*updated_by*update_date";
				$data_array="".$cbo_company_name_wo."*".$cbo_variable_list_wo."*'".$process_loss_method[$i]."'*".$item_category_id[$i]."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				$rID=sql_update("variable_order_tracking",$field_array,$data_array,"id","".$update_id[$i]."",1);
			} 
			
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo 1;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo 10;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
		disconnect($con);
		die;
		
	}	
}



?>