<?php

	date_default_timezone_set("Asia/Dhaka");
	
	require_once('mailer/class.phpmailer.php');
	require_once('includes/common.php');
	$from_mail="PLATFORM-ERP@asrotex.com";

	$company_library=return_library_array( "select id, company_name from lib_company where  status_active=1 and is_deleted=0", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$supplier_library = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	$dealing_marchant = return_library_array("select id,team_member_name from lib_mkt_team_member_info where status_active=1 and is_deleted=0","id","team_member_name");
	$task_short_name = return_library_array("select task_name,task_short_name from lib_tna_task where status_active=1 and is_deleted=0","task_name","task_short_name");
	$po_number = return_library_array("select id,po_number from wo_po_break_down where status_active=1 and is_deleted=0","id","po_number");
	
/*	
	$current_date=add_date(date("Y-m-d"),0);
	$tomorrow=add_date(date("Y-m-d"),1);
	$day_after_tomorrow=add_date(date("Y-m-d"),2);
	
	$prev_date=add_date(date("Y-m-d"),-1);
	
*/	
	
	
	if($db_type==0)
	{
		$tomorrow = date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),1)));
		$day_after_tomorrow = date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),2)));
		$current_date = date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),0)));
		$prev_date = date('Y-m-d H:i:s', strtotime('-1 day', strtotime($current_date))); 
		$actual_date="='0000-00-00'";
	}
	else
	{
		$tomorrow = change_date_format(date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),1))),'','',1);
		$day_after_tomorrow = change_date_format(date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),2))),'','',1);
		$current_date = change_date_format(date("Y-m-d H:i:s",strtotime(add_date(date("Y-m-d",time()),0))),'','',1);
		$prev_date = change_date_format(date('Y-m-d H:i:s', strtotime('-1 day', strtotime($current_date))),'','',1); 
		$actual_date="is null";
	}
	
	
	
	
foreach($company_library as $compid=>$company_name)
{
	ob_start();
	$flag=0;
?>
<table width="1020">
    <tr>
    	<td height="30" valign="top" align="left">Dear Sir,</td>
    </tr>
    <tr>
    	<td valign="top" align="left"></td>
    </tr>
    <tr>
    	<td height="40" valign="top" align="left">As per TNA Schedule, following task(s) is/are supposed to start. Please check everything is ready or not.</td>
    </tr>

    <tr>
    	<td valign="top" align="left">
    		<table width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tr>
                    <td colspan="10" height="40" align="center"><span><?php echo $company_name; ?></span><br /><strong>Task Starting Reminder</strong></td>
                </tr>
				<?php  
					$sql_mst="select b.dealing_marchant from tna_process_mst a, wo_po_details_master b where a.job_no=b.job_no and a.task_start_date between '".$current_date."' and '".$day_after_tomorrow."' and a.actual_start_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.dealing_marchant";
					$nameArray_mst=sql_select($sql_mst);
					$tot_rows1=count($nameArray_mst);
					
					foreach($nameArray_mst as $row)
					{
						$j++;
                ?>
                <tr><td height="30" valign="top" colspan="10">&nbsp;</td></tr>
				<tr><td colspan="2"><strong>Dealing Merchant :</strong></td><td colspan="8">&nbsp;&nbsp;&nbsp;<strong><?php echo $dealing_marchant[$row[csf('dealing_marchant')]]; ?></strong></td></tr>
                <tr>
                    <td width="30" align="center"><strong>SL</strong></td>
                    <td width="130" align="center"><strong>Task Name</strong></td>
                    <td width="110" align="center"><strong>Buyer</strong></td>
                    <td width="90" align="center"><strong>Job No</strong></td>
                    <td width="100" align="center"><strong>Style Ref.</strong></td>
                    <td width="100" align="center"><strong>Order</strong></td>
                    <td width="75" align="center"><strong>Ship Date</strong></td>
                    <td width="75" align="center"><strong><?php echo change_date_format($current_date); ?></strong></td>
                    <td width="75" align="center"><strong><?php echo change_date_format($tomorrow); ?></strong></td>
                    <td width="75" align="center"><strong><?php echo change_date_format($day_after_tomorrow); ?></strong></td>
                </tr>
				<?php
                	$i=0;
					
                	$sql_dtls="select a.id,a.task_number,b.buyer_name,a.job_no,a.po_number_id,a.shipment_date,a.task_start_date,b.style_ref_no from tna_process_mst a, wo_po_details_master b, lib_tna_task c, wo_po_break_down d where a.job_no=b.job_no and c.task_name=a.task_number and b.job_no=d.job_no_mst and a.task_start_date between '".$current_date."' and '".$day_after_tomorrow."' and a.actual_start_date $actual_date and b.dealing_marchant='".$row[csf('dealing_marchant')]."' and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and d.shiping_status !=3 order by c.task_sequence_no";
					$nameArray_dtls=sql_select($sql_dtls);
					foreach($nameArray_dtls as $row_dtls)
					{
					$i++;
                ?>
                <tr>
                    <td align="center"><?php echo $i; ?></td>
                    <td><?php echo $task_short_name[$row_dtls[csf('task_number')]]; ?></td>
                    <td><?php echo $buyer_library[$row_dtls[csf('buyer_name')]]; ?></td>
                    <td><?php echo $row_dtls[csf('job_no')]; ?></td>
                    <td><?php echo $row_dtls[csf('style_ref_no')]; ?></td>
                    <td><?php echo $po_number[$row_dtls[csf('po_number_id')]]; ?></td> 
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('shipment_date')]); ?></td>
                    <td align="center">
						<?php
							if(change_date_format($current_date)==change_date_format($row_dtls[csf('task_start_date')]))  
							echo change_date_format($row_dtls[csf('task_start_date')]); 
                        ?>
                    </td>
                    <td align="center">
						<?php
							if(change_date_format($tomorrow)==change_date_format($row_dtls[csf('task_start_date')]))  
							echo change_date_format($row_dtls[csf('task_start_date')]); 
                        ?>
                    </td>
                    <td align="center">
						<?php
							if(change_date_format($day_after_tomorrow)==change_date_format($row_dtls[csf('task_start_date')]))  
							echo change_date_format($row_dtls[csf('task_start_date')]); 
                        ?>
                    </td>
                </tr>
				<?php
					}
					$flag=1;
					}
					if($tot_rows1==0)
					{
					?>
						<tr><td colspan="10" align="center"><font size="+1"; color="#FF0000"><strong>NO ENTRY FOUND</strong></font></td></tr>
					<?php	
					}
                ?>
			</table>
		</td>
	</tr>
</table>
<?php
	
	$to="";
	$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=3 and b.mail_user_setup_id=c.id and a.company_id=$compid";
	$mail_sql=sql_select($sql);
	foreach($mail_sql as $row)
	{
		if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
	}
	
	$subject="Task Starting Reminder as per TNA Schedule.";
	 
	$message="";
	$message=ob_get_contents();
	ob_clean();
	$header=mail_header();
	
	//$to="beeresh.apu1974@gmail.com,muktobani@gmail.com";
	if($to!="" && $flag==1){echo send_mail_mailer( $to, $subject, $message, $from_mail );}
	//echo $message;
		
} // End Company
	
	
	
	
	
foreach($company_library as $compid=>$company_name)
{
ob_start();
$flag=0;
?> 

<table width="1020">
    <tr>
    	<td height="30" valign="top" align="left">Dear Sir,</td>
    </tr>
    <tr>
    	<td valign="top" align="left"></td>
    </tr>
    <tr>
    	<td height="40" valign="top" align="left">As per TNA Schedule, following task(s) is/are supposed to complete. Please check work progress. You can take help of work progress report available in ERP (Report-> Merchandising report->Work progress report).</td>
    </tr>
    <tr>
    	<td valign="top" align="left">
    		<table width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tr>
                    <td colspan="10" height="40" align="center"><span><?php echo $company_name; ?></span><br /><strong>Task Complition Reminder</strong></td>
                </tr>
				<?php
                	$sql_mst="select b.dealing_marchant from tna_process_mst a, wo_po_details_master b where a.job_no=b.job_no and a.task_finish_date between '".$current_date."' and '".$day_after_tomorrow."' and a.actual_finish_date $actual_date  and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.dealing_marchant";
					$nameArray_mst=sql_select($sql_mst);
					$tot_rows1=count($nameArray_mst);
					foreach($nameArray_mst as $row)
					{
						$j++;
                ?>
                <tr><td height="30" valign="top" colspan="10">&nbsp;</td></tr>
				<tr><td colspan="2"><strong>Dealing Merchant :</strong></td><td colspan="8">&nbsp;&nbsp;&nbsp;<strong><?php echo $dealing_marchant[$row[csf('dealing_marchant')]]; ?></strong></td></tr>
                <tr>
                    <td width="30" align="center"><strong>SL</strong></td>
                    <td width="130" align="center"><strong>Task Name</strong></td>
                    <td width="110" align="center"><strong>Buyer</strong></td>
                    <td width="90" align="center"><strong>Job No</strong></td>
                    <td width="100" align="center"><strong>Style Ref.</strong></td>
                    <td width="100" align="center"><strong>Order</strong></td>
                    <td width="75" align="center"><strong>Ship Date</strong></td>
                    <td width="75" align="center"><strong><?php echo change_date_format($current_date); ?></strong></td>
                    <td width="75" align="center"><strong><?php echo change_date_format($tomorrow); ?></strong></td>
                    <td width="75" align="center"><strong><?php echo change_date_format($day_after_tomorrow); ?></strong></td>
                </tr>
				<?php
                	$i=0;
                	$sql_dtls="select a.id,a.task_number,b.buyer_name,a.job_no,a.po_number_id,a.shipment_date,a.task_finish_date,b.style_ref_no from tna_process_mst a, wo_po_details_master b, lib_tna_task c, wo_po_break_down d where a.job_no=b.job_no and c.task_name=a.task_number and b.job_no=d.job_no_mst and a.task_finish_date between '".$current_date."' and '".$day_after_tomorrow."' and b.dealing_marchant='".$row[csf('dealing_marchant')]."' and b.company_name=$compid and a.actual_finish_date $actual_date and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and d.shiping_status !=3 order by c.task_sequence_no";
					$nameArray_dtls=sql_select($sql_dtls);
					foreach($nameArray_dtls as $row_dtls)
					{
					$i++;
                ?>
                <tr>
                    <td align="center"><?php echo $i; ?></td>
                    <td><?php echo $task_short_name[$row_dtls[csf('task_number')]]; ?></td>
                    <td><?php echo $buyer_library[$row_dtls[csf('buyer_name')]]; ?></td>
                    <td><?php echo $row_dtls[csf('job_no')]; ?></td>
                    <td><?php echo $row_dtls[csf('style_ref_no')]; ?></td>
                    <td><?php echo $po_number[$row_dtls[csf('po_number_id')]]; ?></td> 
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('shipment_date')]); ?></td>
                    <td align="center">
						<?php
							if(change_date_format($current_date)==change_date_format($row_dtls[csf('task_start_date')]))  
							echo change_date_format($row_dtls[csf('task_start_date')]); 
                        ?>
                    </td>
                    <td align="center">
						<?php
							if(change_date_format($tomorrow)==change_date_format($row_dtls[csf('task_start_date')]))  
							echo change_date_format($row_dtls[csf('task_start_date')]); 
                        ?>
                    </td>
                    <td align="center">
						<?php
							if(change_date_format($day_after_tomorrow)==change_date_format($row_dtls[csf('task_start_date')]))
							echo change_date_format($row_dtls[csf('task_start_date')]); 
                        ?>
                    </td>
                </tr>
				<?php
					}
					$flag=1;
					}
					if($tot_rows1==0)
					{
					?>
						<tr><td colspan="10" align="center"><font size="+1"; color="#FF0000"><strong>NO ENTRY FOUND</strong></font></td></tr>
					<?php	
					}
                ?>
			</table>
		</td>
	</tr>
</table>
<?php

	$to="";
	$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=3 and b.mail_user_setup_id=c.id and a.company_id=$compid";
	$mail_sql=sql_select($sql);
	foreach($mail_sql as $row)
	{
		if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
	}
	
	$subject="Task completion reminder as per TNA Schedule.";
	
	$message="";
	$message=ob_get_contents();
	ob_clean();
	$header=mail_header();
	//$to="beeresh.apu1974@gmail.com,muktobani@gmail.com";
	if($to!="" && $flag==1){echo send_mail_mailer( $to, $subject, $message, $from_mail);}
	//echo $message;
} // End Company

		
foreach($company_library as $compid=>$company_name)
{
ob_start();
$flag=0;
?> 

<table width="1020">
    <tr>
    	<td height="30" valign="top" align="left">Dear Sir,</td>
    </tr>
    <tr>
    	<td valign="top" align="left"></td>
    </tr>
    <tr>
    	<td height="40" valign="top" align="left">As per TNA Schedule, following task(s) of &nbsp;(&nbsp;Date :&nbsp;<?php echo change_date_format($prev_date); ?>&nbsp;) has/have got pending to start. Please find out the root cause of pending and take action to overcome.</td>
    </tr>
    <tr>
    	<td valign="top" align="left">
    		<table width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tr>
                    <td colspan="7" height="40" align="center"><span><?php echo $company_name; ?></span><br /><strong>Task Start Pending Of &nbsp;(&nbsp;Date :&nbsp;<?php echo change_date_format($prev_date); ?>&nbsp;)</strong></td>
                </tr>
				<?php
                	$sql_mst="select b.dealing_marchant from tna_process_mst a, wo_po_details_master b where a.job_no=b.job_no and a.task_start_date='".$prev_date."' and a.actual_start_date $actual_date  and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.dealing_marchant";
					$nameArray_mst=sql_select($sql_mst);
					$tot_rows1=count($nameArray_mst);
					foreach($nameArray_mst as $row)
					{
						$j++;
                ?>
                <tr><td height="30" valign="top" colspan="7">&nbsp;</td></tr>
				<tr><td colspan="2"><strong>Dealing Merchant :</strong></td><td colspan="5">&nbsp;&nbsp;&nbsp;<strong><?php echo $dealing_marchant[$row[csf('dealing_marchant')]]; ?></strong></td></tr>
                <tr>
                    <td width="30" align="center"><strong>SL</strong></td>
                    <td width="200" align="center"><strong>Task Name</strong></td>
                    <td width="200" align="center"><strong>Buyer</strong></td>
                    <td width="100" align="center"><strong>Job No</strong></td>
                    <td width="100" align="center"><strong>Style Ref.</strong></td>
                    <td width="155" align="center"><strong>Order</strong></td>
                    <td width="75" align="center"><strong>Ship Date</strong></td>
                </tr>
				<?php
                	$i=0;
                	$sql_dtls="select a.id,a.task_number,b.buyer_name,a.job_no,a.po_number_id,a.shipment_date,b.style_ref_no from tna_process_mst a, wo_po_details_master b, lib_tna_task c, wo_po_break_down d where a.job_no=b.job_no and c.task_name=a.task_number and b.job_no=d.job_no_mst and a.task_start_date='".$prev_date."' and b.dealing_marchant='".$row[csf('dealing_marchant')]."' and a.actual_start_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and d.shiping_status !=3 order by c.task_sequence_no";
					$nameArray_dtls=sql_select($sql_dtls);
					foreach($nameArray_dtls as $row_dtls)
					{
					$i++;
                ?>
                <tr>
                    <td align="center"><?php echo $i; ?></td>
                    <td><?php echo $task_short_name[$row_dtls[csf('task_number')]]; ?></td>
                    <td><?php echo $buyer_library[$row_dtls[csf('buyer_name')]]; ?></td>
                    <td><?php echo $row_dtls[csf('job_no')]; ?></td>
                    <td><?php echo $row_dtls[csf('style_ref_no')]; ?></td>
                    <td><?php echo $po_number[$row_dtls[csf('po_number_id')]]; ?></td> 
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('shipment_date')]); ?></td>
                </tr>
				<?php
					}
					$flag=1;
					}
					if($tot_rows1==0)
					{
					?>
						<tr><td colspan="7" align="center"><font size="+1"; color="#FF0000"><strong>NO ENTRY FOUND</strong></font></td></tr>
					<?php	
					}
                ?>
			</table>
		</td>
	</tr>
</table>
<?php
	$to="";
	$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=3 and b.mail_user_setup_id=c.id and a.company_id=$compid";
	$mail_sql=sql_select($sql);
	foreach($mail_sql as $row)
	{
		if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
	}
	
	$subject="Task Start pending for ( Date :".change_date_format($prev_date).") as per TNA Schedule.";
	
	$message="";
	$message=ob_get_contents();
	ob_clean();
	$header=mail_header();
	//$to="beeresh.apu1974@gmail.com,muktobani@gmail.com";
	if($to!="" && $flag==1){echo send_mail_mailer( $to, $subject, $message, $from_mail);}
	
	//echo $message;

} // End Company

		
foreach($company_library as $compid=>$company_name)
{
ob_start();
$flag=0;
?> 

<table width="1020">
    <tr>
    	<td height="30" valign="top" align="left">Dear Sir,</td>
    </tr>
    <tr>
    	<td valign="top" align="left"></td>
    </tr>
    <tr>
    	<td height="40" valign="top" align="left">As per TNA schedule, following task(s) of &nbsp;(&nbsp;Date :&nbsp;<?php echo change_date_format($prev_date); ?>&nbsp;) has/have got pending to complete. Please find out the root cause of pending and take action to overcome.</td>
    </tr>
    <tr>
    	<td valign="top" align="left">
    		<table width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tr>
                    <td colspan="7" height="40" align="center"><strong><span><?php echo $company_name; ?></span><br />Task Completion Pending Of &nbsp;(&nbsp;Date :&nbsp;<?php echo change_date_format($prev_date); ?>&nbsp;)</strong></td>
                </tr>
				<?php
                	$sql_mst="select b.dealing_marchant from tna_process_mst a, wo_po_details_master b where a.job_no=b.job_no and a.task_finish_date='".$prev_date."' and a.actual_finish_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.dealing_marchant";
					$nameArray_mst=sql_select($sql_mst);
					$tot_rows1=count($nameArray_mst);
					foreach($nameArray_mst as $row)
					{
						$j++;
                ?>
                <tr><td height="30" valign="top" colspan="7">&nbsp;</td></tr>
				<tr><td colspan="2"><strong>Dealing Merchant :</strong></td><td colspan="5">&nbsp;&nbsp;&nbsp;<strong><?php echo $dealing_marchant[$row[csf('dealing_marchant')]]; ?></strong></td></tr>
                <tr>
                    <td width="30" align="center"><strong>SL</strong></td>
                    <td width="200" align="center"><strong>Task Name</strong></td>
                    <td width="200" align="center"><strong>Buyer</strong></td>
                    <td width="100" align="center"><strong>Job No</strong></td>
                    <td width="100" align="center"><strong>Style Ref.</strong></td>
                    <td width="155" align="center"><strong>Order</strong></td>
                    <td width="75" align="center"><strong>Ship Date</strong></td>
                </tr>
				<?php
                	$i=0;
                	$sql_dtls="select a.id,a.task_number,b.buyer_name,a.job_no,a.po_number_id,a.shipment_date,b.style_ref_no from tna_process_mst a, wo_po_details_master b, lib_tna_task c, wo_po_break_down d where a.job_no=b.job_no and c.task_name=a.task_number and b.job_no=d.job_no_mst and a.task_finish_date='".$prev_date."' and b.dealing_marchant='".$row[csf('dealing_marchant')]."' and a.actual_finish_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and d.shiping_status !=3 order by c.task_sequence_no";
					$nameArray_dtls=sql_select($sql_dtls);
					foreach($nameArray_dtls as $row_dtls)
					{
					$i++;
                ?>
                <tr>
                    <td align="center"><?php echo $i; ?></td>
                    <td><?php echo $task_short_name[$row_dtls[csf('task_number')]]; ?></td>
                    <td><?php echo $buyer_library[$row_dtls[csf('buyer_name')]]; ?></td>
                    <td><?php echo $row_dtls[csf('job_no')]; ?></td>
                    <td><?php echo $row_dtls[csf('style_ref_no')]; ?></td>
                    <td><?php echo $po_number[$row_dtls[csf('po_number_id')]]; ?></td> 
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('shipment_date')]); ?></td>
                </tr>
				<?php
					}
					$flag=1;
					}
					if($tot_rows1==0)
					{
					?>
						<tr><td colspan="7" align="center"><font size="+1"; color="#FF0000"><strong>NO ENTRY FOUND</strong></font></td></tr>
					<?php	
					}
                ?>
			</table>
		</td>
	</tr>
</table>
<?php

	$to="";
	$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=3 and b.mail_user_setup_id=c.id and a.company_id=$compid";
	$mail_sql=sql_select($sql);
	foreach($mail_sql as $row)
	{
		if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
	}
	
	$subject="Task completion pending for ( Date :".change_date_format($prev_date).")  as per TNA schedule.";
	
	$message="";
	$message=ob_get_contents();
	ob_clean();
	$header=mail_header();
	//$to="beeresh.apu1974@gmail.com,muktobani@gmail.com";
	if($to!="" && $flag==1){echo send_mail_mailer( $to, $subject, $message, $from_mail);}
	//echo $message;

} // End Company

foreach($company_library as $compid=>$company_name)
{
ob_start();
$flag=0;
?> 

<table width="1020">
    <tr>
    	<td height="30" valign="top" align="left">Dear Sir,</td>
    </tr>
    <tr>
    	<td valign="top" align="left"></td>
    </tr>
    <tr>
    	<td height="40" valign="top" align="left">As per TNA schedule, all the pending tasks to start have been mentioned below</td>
    </tr>
    <tr>
    	<td valign="top" align="left">
    		<table width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tr>
                    <td colspan="9" height="40" align="center"><span><?php echo $company_name; ?></span><br /><strong>Total Task Start Pending As On Today</strong></td>
                </tr>
				<?php
                	$sql_mst="select b.dealing_marchant from tna_process_mst a, wo_po_details_master b where a.job_no=b.job_no and a.task_start_date <='".$current_date."' and a.actual_start_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.dealing_marchant";
					$nameArray_mst=sql_select($sql_mst);
					$tot_rows1=count($nameArray_mst);
					foreach($nameArray_mst as $row)
					{
						$j++;
                ?>
                <tr><td height="30" valign="top" colspan="9">&nbsp;</td></tr>
				<tr><td colspan="2"><strong>Dealing Merchant :</strong></td><td colspan="7">&nbsp;&nbsp;&nbsp;<strong><?php echo $dealing_marchant[$row[csf('dealing_marchant')]]; ?></strong></td></tr>
                <tr>
                    <td width="30" align="center"><strong>SL</strong></td>
                    <td width="200" align="center"><strong>Task Name</strong></td>
                    <td width="200" align="center"><strong>Buyer</strong></td>
                    <td width="100" align="center"><strong>Job No</strong></td>
                    <td width="100" align="center"><strong>Style Ref.</strong></td>
                    <td width="155" align="center"><strong>Order</strong></td>
                    <td width="75" align="center"><strong>Ship Date</strong></td>
                    
                    <td width="100" align="center"><strong>Plan Start Date</strong></td>
                    <td width="75" align="center"><strong>Over Due Days</strong></td>
                </tr>
				<?php
                	$i=0;
					
                	$sql_dtls="select a.id,a.task_number,b.buyer_name,a.job_no,a.po_number_id,a.shipment_date,b.style_ref_no,a.task_start_date from tna_process_mst a, wo_po_details_master b, lib_tna_task c, wo_po_break_down d where a.job_no=b.job_no and c.task_name=a.task_number and b.job_no=d.job_no_mst and a.task_start_date <='".$current_date."' and b.dealing_marchant='".$row[csf('dealing_marchant')]."' and a.actual_start_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and d.shiping_status !=3 order by c.task_sequence_no";
					$nameArray_dtls=sql_select($sql_dtls);
					foreach($nameArray_dtls as $row_dtls)
					{
						$i++;
						$current_date=date("Y-m-d");
						$current_date1 = strtotime("$current_date");
						
						$task_start_date = strtotime($row_dtls[csf('task_start_date')]);
						$dateDiff = $current_date1-$task_start_date;
						$fullDays = floor($dateDiff/(60*60*24));
                ?>
                <tr>
                    <td align="center"><?php echo $i; ?></td>
                    <td><?php echo $task_short_name[$row_dtls[csf('task_number')]]; ?></td>
                    <td><?php echo $buyer_library[$row_dtls[csf('buyer_name')]]; ?></td>
                    <td><?php echo $row_dtls[csf('job_no')]; ?></td>
                    <td><?php echo $row_dtls[csf('style_ref_no')]; ?></td>
                    <td><?php echo $po_number[$row_dtls[csf('po_number_id')]]; ?></td> 
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('shipment_date')]); ?></td>
                    
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('task_start_date')]); ?></td>
                    <td align="center"><?php  echo $fullDays; ?></td>
                </tr>
				<?php
					}
					$flag=1;
					}
					if($tot_rows1==0)
					{
					?>
						<tr><td colspan="9" align="center"><font size="+1"; color="#FF0000"><strong>NO ENTRY FOUND</strong></font></td></tr>
					<?php	
					}
                ?>
			</table>
		</td>
	</tr>
</table>
<?php
	$to="";
	$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=3 and b.mail_user_setup_id=c.id and a.company_id=$compid";
	$mail_sql=sql_select($sql);
	foreach($mail_sql as $row)
	{
		if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
	}
	
	$subject="Task start pending as on today as per TNA schedule";
	
	$message="";
	$message=ob_get_contents();
	ob_clean();
	$header=mail_header();
	//$to="beeresh.apu1974@gmail.com,muktobani@gmail.com";
	if($to!="" && $flag==1){echo send_mail_mailer( $to, $subject, $message, $from_mail);}
	//echo $message;

} // End Company

foreach($company_library as $compid=>$company_name)
{
ob_start();
$flag=1;
?> 


<table width="1020">
    <tr>
    	<td height="30" valign="top" align="left">Dear Sir,</td>
    </tr>
    <tr>
    	<td valign="top" align="left"></td>
    </tr>
    <tr>
    	<td height="40" valign="top" align="left">As per TNA schedule, all the pending tasks to complete have been mentioned below.</td>
    </tr>
    <tr>
    	<td valign="top" align="left">
    		<table width="100%" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tr>
                    <td colspan="9" height="40" align="center"><span><?php echo $company_name; ?></span><br /><strong>Total Task Completion Pending As On Today</strong></td>
                </tr>
				<?php
                	$sql_mst="select b.dealing_marchant from tna_process_mst a, wo_po_details_master b where a.job_no=b.job_no and a.task_finish_date <='".$current_date."' and a.actual_finish_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.dealing_marchant";
					$nameArray_mst=sql_select($sql_mst);
					$tot_rows1=count($nameArray_mst);
					foreach($nameArray_mst as $row)
					{
						$j++;
                ?>
                <tr><td height="30" valign="top" colspan="9">&nbsp;</td></tr>
				<tr><td colspan="2"><strong>Dealing Merchant :</strong></td><td colspan="7">&nbsp;&nbsp;&nbsp;<strong><?php echo $dealing_marchant[$row[csf('dealing_marchant')]]; ?></strong></td></tr>
                <tr>
                    <td width="30" align="center"><strong>SL</strong></td>
                    <td width="200" align="center"><strong>Task Name</strong></td>
                    <td width="200" align="center"><strong>Buyer</strong></td>
                    <td width="100" align="center"><strong>Job No</strong></td>
                    <td width="100" align="center"><strong>Style Ref.</strong></td>
                    <td width="155" align="center"><strong>Order</strong></td>
                    <td width="75" align="center"><strong>Ship Date</strong></td>
                    
                    <td width="100" align="center"><strong>Plan Finish Date</strong></td>
                    <td width="75" align="center"><strong>Over Due Days</strong></td>
                </tr>
				<?php
                	$i=0;
                	$sql_dtls="select a.id,a.task_number,b.buyer_name,a.job_no,a.po_number_id,a.shipment_date,b.style_ref_no,a.task_finish_date from tna_process_mst a, wo_po_details_master b, lib_tna_task c, wo_po_break_down d where a.job_no=b.job_no and c.task_name=a.task_number and b.job_no=d.job_no_mst and a.task_finish_date <='".$current_date."' and b.dealing_marchant='".$row[csf('dealing_marchant')]."' and a.actual_finish_date $actual_date and b.company_name=$compid and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and d.shiping_status !=3  order by c.task_sequence_no";
					$nameArray_dtls=sql_select($sql_dtls);
					foreach($nameArray_dtls as $row_dtls)
					{
						$i++;
						
						$current_date=date("Y-m-d");
						$current_date1 = strtotime("$current_date");
						
						$task_finish_date = strtotime($row_dtls[csf('task_finish_date')]);
						$dateDiff = $current_date1-$task_finish_date;
						$fullDays = floor($dateDiff/(60*60*24));
                ?>
                <tr>
                    <td align="center"><?php echo $i; ?></td>
                    <td><?php echo $task_short_name[$row_dtls[csf('task_number')]]; ?></td>
                    <td><?php echo $buyer_library[$row_dtls[csf('buyer_name')]]; ?></td>
                    <td><?php echo $row_dtls[csf('job_no')]; ?></td>
                    <td><?php echo $row_dtls[csf('style_ref_no')]; ?></td>
                    <td><?php echo $po_number[$row_dtls[csf('po_number_id')]]; ?></td> 
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('shipment_date')]); ?></td>
                    
                    <td align="center"><?php  echo change_date_format($row_dtls[csf('task_finish_date')]); ?></td>
                    <td align="center"><?php  echo $fullDays; ?></td>
                </tr>
				<?php
					}
					$flag=1;
					}
					if($tot_rows1==0)
					{
					?>
						<tr><td colspan="9" align="center"><font size="+1"; color="#FF0000"><strong>NO ENTRY FOUND</strong></font></td></tr>
					<?php	
					}
                ?>
			</table>
		</td>
	</tr>
</table>
<?php
	$to="";
	$sql = "SELECT c.email_address FROM mail_group_mst a, mail_group_child b, user_mail_address c where b.mail_group_mst_id=a.id and a.mail_item=3 and b.mail_user_setup_id=c.id and a.company_id=$compid";
	$mail_sql=sql_select($sql);
	foreach($mail_sql as $row)
	{
		if ($to=="")  $to=$row[csf('email_address')]; else $to=$to.", ".$row[csf('email_address')]; 
	}
	
	$subject="Task completion pending as on today as per TNA schedule";
	
	$message="";
	$message=ob_get_contents();
	ob_clean();
	$header=mail_header();
	//$to="beeresh.apu1974@gmail.com,muktobani@gmail.com";
	if($to!="" && $flag==1){echo send_mail_mailer( $to, $subject, $message, $from_mail);}
	//echo $message;


} // End Company

?> 