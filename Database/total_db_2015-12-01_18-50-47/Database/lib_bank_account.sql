-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_bank_account`
--

DROP TABLE IF EXISTS `lib_bank_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_bank_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `account_type` int(11) DEFAULT NULL,
  `account_no` varchar(50) DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `loan_limit` int(11) DEFAULT NULL,
  `loan_type` int(1) NOT NULL DEFAULT '0',
  `company_id` int(11) DEFAULT NULL,
  `po_status` varchar(10) DEFAULT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `account_type` (`account_type`),
  KEY `account_no` (`account_no`),
  KEY `company_id` (`company_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_bank_account`
--

LOCK TABLES `lib_bank_account` WRITE;
/*!40000 ALTER TABLE `lib_bank_account` DISABLE KEYS */;
INSERT INTO `lib_bank_account` VALUES (1,4,10,'11100005680',1,0,1,1,'',13,'2015-08-26 18:14:20',NULL,NULL,1,0),(2,1,10,'204.100.13002',1,0,0,0,'',13,'2015-08-26 18:19:06',NULL,NULL,1,0),(3,2,10,'111-13535',1,0,0,0,'',13,'2015-08-26 18:24:38',NULL,NULL,1,0),(4,0,10,'0102100000000672',1,0,1,1,'',13,'2015-10-21 13:03:37',NULL,NULL,1,0),(5,0,10,'0102100000000672',1,0,1,1,'',13,'2015-11-19 10:55:31',NULL,NULL,1,0),(6,5,10,'01021000000006721',1,0,1,1,'',13,'2015-11-19 11:06:23',NULL,NULL,1,0);
/*!40000 ALTER TABLE `lib_bank_account` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
