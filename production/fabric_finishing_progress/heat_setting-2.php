<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Dyeing Production Entry

Functionality	:	
JS Functions	:
Created by		:	Aziz
Creation date 	: 	15-04-2014
Updated by 		: 		
Update date		: 	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/ 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Heat / Setting  Entry Info","../../", 1, 1, "",'1','');
?>

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";

	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];

	$(document).ready(function(e)
	 {
            $("#txt_color").autocomplete({
			 source: str_color
		  });
     });

	function openmypage_batchnum()
	{
		var cbo_company_id = $('#cbo_company_id').val();
			

		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{
			var page_link='requires/heat_setting_controller.php?cbo_company_id='+cbo_company_id+'&action=batch_number_popup';
			var title='Batch Number Popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=820px,height=390px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var batch_id=this.contentDoc.getElementById("hidden_batch_id").value;
				//alert( batch_id)
				if(batch_id!="")
				{
					freeze_window(5);
					get_php_form_data(batch_id, "populate_data_from_batch", "requires/heat_setting_controller" );
					show_list_view(batch_id,'show_fabric_desc_listview','list_fabric_desc_container','requires/heat_setting_controller','');
					
					release_freezing();
				}
				get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/heat_setting_controller' );
				//get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/heat_setting_controller' );
			}
		}
	}
	
	function check_batch()
	{
		var batch_no=$('#txt_batch_no').val();
		var cbo_company_id = $('#cbo_company_id').val();
		
		//alert(update_id_dtl);
		if(batch_no!="")
		{
			if (form_validation('cbo_company_id','Company')==false)
			{
				return;
			}
			
			var response=return_global_ajax_value( cbo_company_id+"**"+batch_no, 'check_batch_no', '', 'requires/heat_setting_controller');
			var response=response.split("_");
			
			if(response[0]==0)
			{
				alert('Batch no not found.');
				$('#txt_batch_no').val('');
				$('#hidden_batch_id').val(''); 
				//$('#cbo_company_id').val(''); 
				$('#txt_update_id').val(''); 
				$('#cbo_sub_process').val('');
				$('#txt_process_end_date').val('');
				$('#txt_end_hours').val('');
				$('#txt_end_minutes').val('');
				$('#cbo_machine_name').val('');
				$('#txt_remarks').val('');
			reset_form('slittingsqueezing_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
				
			}
			else
			{
				$('#hidden_batch_id').val(response[1]);
				var update_id_dtl=$('#txt_update_id').val(); 
				
				//get_php_form_data(response[1], "populate_data_from_batch", "requires/heat_setting_controller" );
				get_php_form_data(response[1]+'_'+batch_no, "populate_data_from_batch", "requires/heat_setting_controller" );
			
				get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/heat_setting_controller' );
				show_list_view(response[1]+'_'+batch_no,'show_fabric_desc_listview','list_fabric_desc_container','requires/heat_setting_controller','');
				//get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/heat_setting_controller' );
			}
		
		}
	}
	
	function fnc_move_cursor(val,id, field_id,lnth,max_val)
	{
		var str_length=val.length;
		if(str_length==lnth)
		{
			$('#'+field_id).select();
			$('#'+field_id).focus();
		}
		
		if(val>max_val)
		{
			document.getElementById(id).value=max_val;
		}
	}
	
		function fnc_pro_fab_subprocess( operation )
		{
			if( form_validation('cbo_company_id*txt_batch_no*txt_process_end_date*txt_end_hours*txt_end_minutes','Company*Batch No*End Date*End Hour*End Minute')==false )
				{
					return;
				} 
				var row_num=$('#tbl_item_details tbody tr').length-1;
				//alert(row_num);return;
				var data_all="";
				for (var i=1; i<=row_num; i++)
				{	
					data_all+=get_submitted_data_string('txtconscomp_'+i+'*txtgsm_'+i+'*txtbodypart_'+i+'*txtdiawidth_'+i+'*txtbatchqnty_'+i+'*txtprodid_'+i+'*updateiddtls_'+i,"../../",i);
					//data_all_mm+=get_submitted_data_string('txtconscomp_'+i,"../../");
					//data_all_mm+='txtconscomp_'+i+'*';
					//data_all+=get_submitted_data_string('txtconscomp_'+i,"../../",i);updateiddtls_1
				}
				 
			
			
			
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_id*txt_batch_no*hidden_batch_id*cbo_sub_process*txt_process_end_date*txt_end_hours*txt_end_minutes*cbo_machine_name*txt_remarks*txt_ext_id*txt_update_id*cbo_floor*txt_temparature*txt_stretch*txt_feed*txt_feed_in*txt_pinning*txt_speed',"../../")+data_all+'&total_row='+row_num;
		//alert(data);return;
	  freeze_window(operation);
	  http.open("POST","requires/heat_setting_controller.php",true);
	  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  http.send(data);
	  http.onreadystatechange = fnc_pro_fab_subprocess_response;
			
	}
		
	 function fnc_pro_fab_subprocess_response()
	 {
		if(http.readyState == 4) 
		{
			alert (http.responseText);
			var reponse=trim(http.responseText).split('**');
			show_msg(reponse[0]);
			//document.getElementById('txt_batch_no').value = reponse[1];
			document.getElementById('txt_update_id').value = reponse[1];
/*			var reponse2=reponse[2].split(',');
			var row_num=$('#tbl_item_details tbody tr').length-1;
			for (var i=1; i<=row_num; i++)
				{
					document.getElementById('updateiddtls_'+i).value = reponse2[i];	
				}
*/			
			
			set_button_status(1, permission, 'fnc_pro_fab_subprocess',1,1);
		}
		release_freezing();
	 }
	 
	function fnResetForm()
	{
		reset_form('slittingsqueezing_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
	}
	
	function scan_batchnumber(str)
	{
	var cbo_company_id = $('#cbo_company_id').val();
	var response=return_global_ajax_value( cbo_company_id+"**"+str, 'check_batch_no', '', 'requires/heat_setting_controller');
	var response=response.split("_");
	if(response[0]==0)
		{
			alert('Batch no not found.');
			$('#txt_batch_no').val('');
			$('#hidden_batch_id').val(''); 
			//$('#cbo_company_id').val(''); 
			$('#txt_update_id').val(''); 
			$('#cbo_sub_process').val('');
			$('#txt_process_end_date').val('');
			$('#txt_end_hours').val('');
			$('#txt_end_minutes').val('');
			$('#cbo_machine_name').val('');
			$('#txt_remarks').val('');
		reset_form('slittingsqueezing_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
			
		}
		else
		{
	$('#hidden_batch_id').val(response[1]);
	get_php_form_data(response[1]+'_'+str, "populate_data_from_batch", "requires/heat_setting_controller" );
	show_list_view(response[1]+'_'+str,'show_fabric_desc_listview','list_fabric_desc_container','requires/heat_setting_controller','');
	get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/heat_setting_controller' );
		}
	}
	
$('#txt_batch_no').live('keydown', function(e) {
   
    if (e.keyCode === 13) {
        e.preventDefault();
		 scan_batchnumber(this.value); 
    }
});	
</script>
</head>
<body onLoad="set_hotkey(); $('#txt_batch_no').focus();">
<div style="width:100%;" align="center">
<?php echo load_freeze_divs ("../../",$permission); ?>
    <form name="slittingsqueezing_1" id="slittingsqueezing_1" autocomplete="off" >
    <div style="width:1200px; float:left;">   
        <fieldset style="width:950px;">
        <table cellpadding="0" cellspacing="1" width="950" border="0" align="left">
            <tr>
                <td width="30%" valign="top">
                    <fieldset>
                    <legend>Input Area</legend>
                    <table cellpadding="0" cellspacing="2" width="100%" id="main_tbl">
                    	<tr>
                         <td class="must_entry_caption" width="130">Company</td>
                            <td>
                                <?php
                                    echo create_drop_down( "cbo_company_id", 135, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down('requires/heat_setting_controller', this.value, 'load_drop_floor', 'floor_td' );" );
                                ?>
                                <input type="text" name="txt_update_id" id="txt_update_id" style="width:100px;" class="text_boxes" readonly />
                            </td>
                        </tr>
                        <tr> 
                            <td width="" class="must_entry_caption">Batch No.</td>
                            <td>
                                <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:122px;" placeholder="Write/Browse" maxlength="20" title="Maximum 20 Character" onDblClick="openmypage_batchnum();" onChange="check_batch();" />
                                <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" readonly />
                                <!--<input type="text" name="txt_ext_id" id="txt_ext_id" style="width:100px;" class="text_boxes" readonly />-->
                            </td>
                        </tr>
                        <tr>
                            <td class="">Process </td>
                            <td>
                               <?php
								   echo create_drop_down( "cbo_sub_process", 135, $conversion_cost_head_array,"", 0, "-- Select --", 0, "","","33" );
                               ?>
                            </td>
                        </tr>
                        <tr>
                            <td  class="must_entry_caption">Process En Date</td>
                            <td>
                                <input type="text" name="txt_process_end_date" id="txt_process_end_date" class="datepicker" style="width:122px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td class="">Temparature</td>
                            <td>
                                <input type="text" name="txt_temparature" id="txt_temparature"  class="text_boxes_numeric" style="width:120px;"   />
                            </td>
                        </tr>
                         <tr>
                            <td class="">Stretch</td>
                            <td>
                                <input type="text" name="txt_stretch" id="txt_stretch" class="text_boxes_numeric" style="width:120px;" />
                            </td>
                        </tr>
                         <tr>
                            <td class="">Over Feed</td>
                            <td>
                                <input type="text" name="txt_feed" id="txt_feed" class="text_boxes_numeric" style="width:120px;"  />
                            </td>
                        </tr>
                         <tr>
                            <td class="">Feed-In</td>
                            <td>
                                <input type="text" name="txt_feed_in" id="txt_feed_in" class="text_boxes_numeric" style="width:120px;"   />
                            </td>
                        </tr>
                         <tr>
                            <td class="">Pinning</td>
                            <td>
                                <input type="text" name="txt_pinning" id="txt_pinning" class="text_boxes_numeric" style="width:120px;"   />
                            </td>
                        </tr>
                         <tr>
                            <td class="">Speed (M/Min)</td>
                            <td>
                                <input type="text" name="txt_speed" id="txt_speed" class="text_boxes_numeric" style="width:120px;"   />
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process Time</td>
                            <td>
                                 <input type="text" name="txt_end_hours" id="txt_end_hours" class="text_boxes_numeric" placeholder="Hours" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_hours','txt_end_minutes',2,23)" />
                                <input type="text" name="txt_end_minutes" id="txt_end_minutes" class="text_boxes_numeric" placeholder="Minutes" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_minutes','txt_end_date',2,59)" />
                            </td>
                        </tr>
                        <tr>
                            <td>Floor</td>
                            <td id="floor_td">
								<?php
									 echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 order by floor_name limit 0 ,0","id,floor_name", 1, "-- Select Floor --", $selected, "",0,"","","","",4 );
                                ?>
                            </td>
                        <tr>
                        <tr>
                            <td>Machine Name</td>
                            <td id="machine_td">
								<?php
									echo create_drop_down("cbo_machine_name", 135, $blank_array,"", 1, "-- Select Machine --", 0, "",0 ); 
                                ?>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="40%" valign="top">
                    <table cellpadding="0" cellspacing="1" width="80%" border="0" align="left">
                        <tr>
                            <td colspan="3"> <center> <legend>Batch Reference Display</legend></center> </td>
                        </tr>
                        <tr>
                            <td width="35%" valign="top">
                                <fieldset>
                                    <table width="700" align="left" id="tbl_body1">
                                        <tr>
                                            <td width="70">Batch ID</td>
                                            <td width="110">
                                             <input type="text" name="txt_batch_ID" id="txt_batch_ID" class="text_boxes" style="width:100px;" readonly />
                                            </td>
                                            <td width="90">Ext. No </td>
                                            <td width="110">
                                             <input type="text" name="txt_ext_id" id="txt_ext_id" class="text_boxes" style="width:100px;" readonly />
                                            </td>
                                            <td width="100">Color</td>
                                            <td>
                                             <input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:100px;"  readonly />
                                            </td>
                                             <td width="100">Color Ran.</td>
                                            <td>
                                            <input type="text" name="txt_color_range" id="txt_color_range" class="text_boxes" style="width:100px;"  readonly />
                                            </td>
                                            </tr>
                                            <tr>
                                            <td>Job No</td>
                                            <td>
                                            <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:100px;"  readonly />
                                            </td>
                                            <td>Buyer </td>
                                            <td> 
                                            <input type="text" name="txt_buyer" id="txt_buyer" class="text_boxes" style="width:100px;"  readonly />
                                            </td>
                                            <td>Order No.</td>
                                            <td >
                                             <input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:100px;" readonly />
                                            </td>
                                            <td>Batch Date</td>
                                            <td>
                                             <input type="text" name="txt_batch_date" id="txt_batch_date" class="text_boxes" style="width:100px;" readonly  />
                                            </td>
                                         </tr>
                                        <tr>
                                            <td width="100"> Batch Time </td>
                                            <td id="">
                                            <input type="text" name="txt_batch_time" id="txt_batch_time" class="text_boxes" style="width:100px;" readonly  />
                                            </td>
                                            <td width="100">Bat. Against </td>
                                            <td>
                                             <input type="text" name="txt_batch_against" id="txt_batch_against" class="text_boxes" style="width:100px;" readonly />
                                            </td>
                                            <td>M/C Floor </td>
                                            <td>
                                             <input type="text" name="txt_machine_no" id="txt_machine_no" class="text_boxes" style="width:100px;" readonly />
                                            </td>
                                            <td width="90">M/C Group </td>
                                            <td width="110" id="machine_fg_td">
                                            <input type="text" name="txt_mc_group" id="txt_mc_group" class="text_boxes" style="width:100px;" value="<?php echo $data;?>" readonly />
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                              </td>
                              </tr>
                                 <table width="550" align="left" class="rpt_table" rules="all" id="tbl_item_details" >
								 
                                      <caption> <legend> Fabric Details </legend></caption>
                                        <thead>
                                            <th>Const/Composition</th> 
                                            <th>GSM</th>
                                            <th>Dia/Width</th> 
                                            <th>D/W Type</th>
                                            <th>Batch Qty</th>
                                        </thead>
										 
                                        <tbody id="list_fabric_desc_container">
                                            <tr class="general" id="row_1">
                                                <td><input type="text" name="txtconscomp_1" id="txtconscomp_1" class="text_boxes" style="width:250px;" readonly disabled /></td>
                                                <td><input type="text" name="txtgsm_1" id="txtgsm_1" class="text_boxes" style="width:60px;"readonly disabled /> </td>
                                                <td><input type="text" name="txtbodypart_1" id="txtbodypart_1" class="text_boxes" style="width:120px;" readonly  disabled/></td> 
                                                <td><input type="text" name="txtdiawidth_1" id="txtdiawidth_1" class="text_boxes" style="width:120px;" readonly disabled/></td>
                                                <td><input type="text" name="txtbatchqnty_1" id="txtbatchqnty_1" class="text_boxes" style="width:90px;" readonly disabled/>
                                                  <input type="hidden" name="txtprodid_1" id="txtprodid_1" />
                                                  <input type="text" name="updateiddtls_1" id="updateiddtls_1" class="text_boxes" readonly />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" align="right"><b>Sum:</b> <?php //echo $b_qty; ?> </td>
                                                <td align="right"></td>
                                            </tr>
                                        </tbody>
                                  </table>
                    </table>
                </td>
            </tr>
            <tr align="left">
                <td colspan="3" align="left"  width="30%">
                    <fieldset style="width:880px; float:left;">
                        <table style="width:880" align="left">
                            <tr>
                                <td width="100">Remarks:</td>
                                <td>
                                    <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:880px;"  />
                                </td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                              
                                <td align="center">
                                 <?php
									echo load_submit_buttons($permission, "fnc_pro_fab_subprocess", 0,0,"",1);
								?>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td align="right">&nbsp;</td>
            </tr>
        </table>
        </fieldset>
        </div>
	</form>
</div>
</body>
<script> set_multiselect('cbo_sub_process','0','0','','0'); </script>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>