-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_tna_task`
--

DROP TABLE IF EXISTS `lib_tna_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_tna_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_catagory` int(11) NOT NULL,
  `task_name` varchar(250) NOT NULL,
  `task_short_name` varchar(15) NOT NULL,
  `task_type` int(1) NOT NULL DEFAULT '0' COMMENT 'submission:1, Approval:2',
  `module_name` int(11) NOT NULL DEFAULT '0',
  `link_page` int(11) NOT NULL DEFAULT '0',
  `penalty` double NOT NULL DEFAULT '0',
  `completion_percent` tinyint(3) NOT NULL DEFAULT '0',
  `row_status` int(11) NOT NULL,
  `task_sequence_no` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` datetime DEFAULT NULL,
  `status_active` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_tna_task`
--

LOCK TABLES `lib_tna_task` WRITE;
/*!40000 ALTER TABLE `lib_tna_task` DISABLE KEYS */;
INSERT INTO `lib_tna_task` VALUES (1,0,'1','Order Entry',0,0,0,2,100,1,1,2,'2015-10-08 09:36:59',2,'2015-11-18 11:14:49',0,1),(2,0,'9','Labdip Sub',0,0,0,2,100,1,2,2,'2015-10-08 09:37:44',0,NULL,1,0),(3,0,'10','Labdip Approval',0,0,0,5,100,1,9,2,'2015-10-08 09:38:09',0,NULL,1,0),(4,0,'7','Fit Sam. Sub.',0,0,0,7,100,1,3,2,'2015-10-08 09:38:37',0,NULL,1,0),(5,0,'8','PP Sam Sub',0,0,0,3,100,1,11,2,'2015-10-08 09:39:13',0,NULL,1,0),(6,0,'14','Size set Sub',0,0,0,3,100,1,15,2,'2015-10-08 09:39:52',0,NULL,1,0),(7,0,'50','Yarn Issue',0,0,0,0,100,1,6,2,'2015-10-10 12:43:35',0,NULL,1,0),(8,0,'25','Trims Booking',0,0,0,0,0,1,7,2,'2015-10-10 12:45:54',2,'2015-10-10 14:46:38',0,1),(9,0,'31','Fab. Boking',0,0,0,0,0,1,4,2,'2015-10-10 12:46:38',0,NULL,1,0),(10,0,'33','Fab. Ser. Bokin',0,0,0,0,0,1,7,2,'2015-10-10 12:47:19',0,NULL,1,0),(11,0,'70','Trims In-house',0,0,0,0,0,1,13,2,'2015-10-10 12:48:04',0,NULL,1,0),(12,0,'72','Gray Fab In-hou',0,0,0,0,0,1,14,2,'2015-10-10 12:48:37',0,NULL,1,0),(13,0,'73','Fin. Fab In-hou',0,0,0,0,0,1,16,2,'2015-10-10 12:49:05',0,NULL,1,0),(14,0,'84','Cutting',0,0,0,0,0,1,17,2,'2015-10-10 12:49:34',0,NULL,1,0),(15,0,'86','Sew. Out',0,0,0,0,0,1,19,2,'2015-10-10 12:50:03',0,NULL,1,0),(16,0,'87','Iron Complete',0,0,0,0,0,1,20,2,'2015-10-10 12:50:32',0,NULL,1,0),(17,0,'88','Gar. Finishing',0,0,0,0,0,1,21,2,'2015-10-10 12:50:51',0,NULL,1,0),(18,0,'101','Inspection Done',0,0,0,0,0,1,22,2,'2015-10-10 12:51:25',0,NULL,1,0),(19,0,'110','Ex-Factory',0,0,0,0,0,1,23,2,'2015-10-10 12:51:45',0,NULL,1,0),(20,0,'13','Fit Sam App',0,0,0,0,100,1,8,2,'2015-10-10 13:37:03',0,NULL,1,0),(21,0,'64','Sam Fab Bok',0,0,0,0,0,1,9,2,'2015-10-10 13:37:24',2,'2015-10-10 14:50:24',0,1),(22,0,'12','PP Sam App',0,0,0,0,0,1,12,2,'2015-10-10 13:38:04',0,NULL,1,0),(23,0,'32','Trims Bok',0,0,0,0,100,1,10,2,'2015-10-10 13:41:48',0,NULL,1,0),(24,0,'85','Print/Emb Done',0,0,0,0,100,1,18,2,'2015-10-10 14:33:09',0,NULL,1,0),(25,0,'30','Sam Fab Booking',0,0,0,0,0,1,5,2,'2015-10-10 14:51:52',0,NULL,1,0);
/*!40000 ALTER TABLE `lib_tna_task` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
