<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------------------------------
if ($action=="load_drop_down_location")
{
	//echo $data;
	echo create_drop_down( "cbo_location", 110, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select --", $selected, "load_drop_down( 'requires/date_wise_prod_without_cm_report_controller', this.value, 'load_drop_down_floor', 'floor_td' );",0 );
}

if ($action=="load_drop_down_floor")
{
	echo create_drop_down( "cbo_floor", 110, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 and location_id='$data' order by floor_name","id,floor_name", 1, "-- Select --", $selected, "",0 );     	 
}

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 110, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- Select --", $selected, "" );     	 
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$company_short_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name"  );
 	$buyer_short_library=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
 	$location_library=return_library_array( "select id,location_name from lib_location", "id", "location_name"  ); 
	$floor_library=return_library_array( "select id,floor_name from lib_prod_floor", "id", "floor_name"  ); 
	$line_library=return_library_array( "select id,line_name from lib_sewing_line", "id", "line_name"  ); 
	$prod_reso_arr=return_library_array( "select id, line_number from prod_resource_mst",'id','line_number'); 	

	if(str_replace("'","",trim($cbo_subcon))==1)//NO
	{
		$garments_nature=str_replace("'","",$cbo_garments_nature);
		if($garments_nature==1)$garments_nature="";
		$type = str_replace("'","",$cbo_type);
		if(str_replace("'","",$cbo_company_name)==0)$company_name=""; else $company_name=" and b.company_name=$cbo_company_name";
		if(str_replace("'","",$cbo_buyer_name)==0)$buyer_name="";else $buyer_name=" and b.buyer_name=$cbo_buyer_name";
		if(str_replace("'","",$cbo_floor)==0)$floor_name="";else $floor_name=" and c.floor_id=$cbo_floor";
		
		if(str_replace("'","",$cbo_floor)==0)$floor_id="";else $floor_id=" and floor_id=$cbo_floor";
		
		if(str_replace("'","",trim($txt_date_from))=="" || str_replace("'","",trim($txt_date_to))=="")$txt_date="";
		else $txt_date=" and c.production_date between $txt_date_from and $txt_date_to";
		$fromDate = change_date_format( str_replace("'","",trim($txt_date_from)) );
		$toDate = change_date_format( str_replace("'","",trim($txt_date_to)) );
		//cbo_garments_nature
		
		
		
		if($type==1) //--------------------------------------------Show Date Wise
		{
			
			
			ob_start();
			?>
			<div style="width:1720px" id="scroll_body">
                <table width="1700"  cellspacing="0"   >
                    <tr class="form_caption" style="border:none;">
                        <td colspan="19" align="center" style="border:none;font-size:14px; font-weight:bold" > Date Wise Production Report</td>
                     </tr>
                    <tr style="border:none;">
                        <td colspan="19" align="center" style="border:none; font-size:16px; font-weight:bold">
                            Company Name:<?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?>                                
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td colspan="19" align="center" style="border:none;font-size:12px; font-weight:bold">
                            <?php echo "From $fromDate To $toDate" ;?>
                        </td>
                    </tr>
                </table>
                <div>
                    <table width="910" cellspacing="0" border="1" class="rpt_table" rules="all" id="" >
                        <thead>
                            <tr>
                                <th width="30">Sl.</th>    
                                <th width="80">Buyer Name</th>
                                <th width="80">Order Qnty.(Pcs)</th>
                                <th width="80">PO Value</th>
                                <th width="80">Cut Qty</th>
                                <th width="80">Sent Embl.</th>
                                <th width="80">Rec. From Embl.</th>
                                <th width="80">Sew Input</th>
                                <th width="80">Sew Output</th>
                                <th width="80">Total Iron</th>
                                <th width="80">Total Re-Iron</th>
                                <th width="80">Total Finish</th>
                             </tr>
                        </thead>
                    </table>
                    <div style="max-height:425px; width:930px" >
                        <table cellspacing="0" border="1" class="rpt_table"  width="910" rules="all" id="" >
                        <?php
						
						$cutting_array=array();
						$printing_array=array();
						$printreceived_array=array();
						$sewingin_array=array();
						$sewingout_array=array();
						$iron_array=array();
						$re_iron_array=array();
						$finish_array=array();
						$finish_rej_array=array();
						$sewingout_rej_array=array();
						
						$sql_order=sql_select("SELECT b.buyer_name,
						sum(CASE WHEN production_type ='1' THEN production_quantity ELSE 0 END) AS cutting_qnty,
						sum(CASE WHEN production_type ='2' THEN production_quantity ELSE 0 END) AS printing_qnty,
						sum(CASE WHEN production_type ='3' THEN production_quantity ELSE 0 END) AS printreceived_qnty,
						sum(CASE WHEN production_type ='4' THEN production_quantity ELSE 0 END) AS sewingin_qnty,
						sum(CASE WHEN production_type ='5' THEN production_quantity ELSE 0 END) AS sewingout_qnty,                                          
						sum(CASE WHEN production_type ='7' THEN production_quantity ELSE 0 END) AS iron_qnty,
						sum(CASE WHEN production_type ='7' THEN re_production_qty ELSE 0 END) AS re_iron_qnty,
						sum(CASE WHEN production_type ='8' THEN production_quantity ELSE 0 END) AS finish_qnty,
						sum(CASE WHEN production_type ='8' THEN reject_qnty ELSE 0 END) AS finish_rej_qnty,
						sum(CASE WHEN production_type ='5' THEN reject_qnty ELSE 0 END) AS sewingout_rej_qnty
									from 
                                        wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                    where  
                                        a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $floor_name $garmentsNature group by b.buyer_name");
						foreach($sql_order as $sql_result)
						{
							$cutting_array[$sql_result[csf("buyer_name")]]['1']=$sql_result[csf("cutting_qnty")];
							$printing_array[$sql_result[csf("buyer_name")]]['2']=$sql_result[csf("printing_qnty")];
							$printreceived_array[$sql_result[csf("buyer_name")]]['3']=$sql_result[csf("printreceived_qnty")];
							$sewingin_array[$sql_result[csf("buyer_name")]]['4']=$sql_result[csf("sewingin_qnty")];
							$sewingout_array[$sql_result[csf("buyer_name")]]['5']=$sql_result[csf("sewingout_qnty")];
							$sewingout_rej_array[$sql_result[csf("buyer_name")]]['5']=$sql_result[csf("sewingout_rej_qnty")];
							$iron_array[$sql_result[csf("buyer_name")]]['7']=$sql_result[csf("iron_qnty")];
							$re_iron_array[$sql_result[csf("buyer_name")]]['7']=$sql_result[csf("re_iron_qnty")];
							$finish_array[$sql_result[csf("buyer_name")]]['8']=$sql_result[csf("finish_qnty")];
							$finish_rej_array[$sql_result[csf("buyer_name")]]['8']=$sql_result[csf("finish_rej_qnty")];
						}

                         $total_po_quantity=0;$total_po_value=0;
                         $total_cut=0;$total_sent_embl=0;
                         $total_re_from_embl=0; $total_sew_input=0;
                         $total_sew_out=0;$total_iron=0;$total_re_iron=0;
                         $total_finish=0;
                         $i=1;
                         
                         // garments nature here -------------------------------							
                         if($garments_nature==1 || $garments_nature=="") $garmentsNature="";else $garmentsNature=" and b.garments_nature=$garments_nature";
						 
                         $exfactory_sql= "select b.buyer_name,sum(c.production_quantity) as production_quantity 
                                        from wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                        where a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and c.company_id=$cbo_company_name  and c.production_date between $txt_date_from and $txt_date_to $buyer_name $location $floor_name $garmentsNature group by b.buyer_name";
						//echo $exfactory_sql;die;
						$exfactory_sql_result=sql_select($exfactory_sql);	
                        $exfactory_arr=array(); 
                        foreach($exfactory_sql_result as $resRow)
						{
                            $exfactory_arr[$resRow[csf("buyer_name")]] = $resRow[csf("production_quantity")];
                        }
									
						/*if($db_type==0)
						{                          
                        	$pro_date_sql_query="SELECT b.company_name, b.buyer_name,sum(a.po_quantity*b.total_set_qnty) as po_quantity,sum(a.po_total_price) as po_total_price,a.id
									from lib_buyer d, wo_po_details_master b, wo_po_break_down a 
                                    where a.job_no_mst=b.job_no and b.buyer_name=d.id and a.status_active=1 and b.status_active=1 and b.company_name=$cbo_company_name and a.pub_shipment_date between $txt_date_from and $txt_date_to $company_name $buyer_name  $garmentsNature group by b.buyer_name order by d.buyer_name ASC"; 
						}
						else
						{
							$pro_date_sql_query="SELECT b.buyer_name,sum(a.po_quantity*b.total_set_qnty) as po_quantity,sum(a.po_total_price) as po_total_price
									from lib_buyer d, wo_po_details_master b, wo_po_break_down a 
                                    where a.job_no_mst=b.job_no and b.buyer_name=d.id and a.status_active=1 and b.status_active=1 and b.company_name=$cbo_company_name and a.pub_shipment_date between $txt_date_from and $txt_date_to $company_name $buyer_name  $garmentsNature group by b.buyer_name order by b.buyer_name ASC"; 
							
						}*/
						if($db_type==0)
						{                           
                        	$pro_date_sql_query="SELECT b.company_name, b.buyer_name,sum(a.po_quantity*b.total_set_qnty) as po_quantity,sum(a.po_total_price) as po_total_price
									from lib_buyer d, wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                    where a.job_no_mst=b.job_no and a.id=c.po_break_down_id and b.buyer_name=d.id and a.status_active=1 and b.status_active=1 and b.company_name=$cbo_company_name and c.production_date between $txt_date_from and $txt_date_to $buyer_name $floor_name $garmentsNature group by b.buyer_name order by d.buyer_name ASC"; 
						}
						else
						{
							$pro_date_sql_query="SELECT b.buyer_name,sum(a.po_quantity*b.total_set_qnty) as po_quantity,sum(a.po_total_price) as po_total_price
									from lib_buyer d, wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                    where a.job_no_mst=b.job_no and a.id=c.po_break_down_id and b.buyer_name=d.id and a.status_active=1 and b.status_active=1 and b.company_name=$cbo_company_name and c.production_date between $txt_date_from and $txt_date_to $buyer_name $floor_name $garmentsNature group by b.buyer_name order by b.buyer_name ASC"; 
							
						}
                     // echo $pro_date_sql_query;//die; 
					   $pro_date_sql=sql_select($pro_date_sql_query);
					   
                        foreach($pro_date_sql as $pro_date_sql_row)
                        {
                            if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                            
                        ?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                                <td width="30"><?php echo $i;?></td>
                                <td width="80"><?php echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                                <td width="80" align="right"><?php echo number_format($pro_date_sql_row[csf("po_quantity")]);?></td>
                                <td width="80" align="right"><?php echo number_format($pro_date_sql_row[csf("po_total_price")],2);?></td>
                                
                                <td width="80" align="right"><?php echo number_format($cutting_array[$pro_date_sql_row[csf("buyer_name")]]['1']); ?></td>
                                <td width="80" align="right"><?php echo number_format($printing_array[$pro_date_sql_row[csf("buyer_name")]]['2']); ?></td>
                                <td width="80" align="right"><?php echo number_format($printreceived_array[$pro_date_sql_row[csf("buyer_name")]]['3']);  ?></td>
                                <td width="80" align="right"><?php echo number_format($sewingin_array[$pro_date_sql_row[csf("buyer_name")]]['4']); ?></td>
                                <td width="80" align="right"><?php echo number_format($sewingout_array[$pro_date_sql_row[csf("buyer_name")]]['5']); ?></td>
                                <td width="80" align="right"><?php echo number_format($iron_array[$pro_date_sql_row[csf("buyer_name")]]['7']); ?></td>
                                <td width="80" align="right"><?php echo number_format($re_iron_array[$pro_date_sql_row[csf("buyer_name")]]['7']); ?></td>
                                <td width="80" align="right"><?php echo number_format($finish_array[$pro_date_sql_row[csf("buyer_name")]]['8']); ?></td>
                            </tr>	
                            <?php		
                                $total_po_quantity+=$pro_date_sql_row[csf("po_quantity")];
                                $total_po_value+=$pro_date_sql_row[csf("po_total_price")];
                                $total_cut+=$cutting_array[$pro_date_sql_row[csf("buyer_name")]]['1'];
								$total_sent_embl+=$printing_array[$pro_date_sql_row[csf("buyer_name")]]['2'];
                                $total_re_from_embl+=$printreceived_array[$pro_date_sql_row[csf("buyer_name")]]['3'];
                                $total_sew_input+=$sewingin_array[$pro_date_sql_row[csf("buyer_name")]]['4'];
                                $total_sew_out+=$sewingout_array[$pro_date_sql_row[csf("buyer_name")]]['5'];
                                $total_iron+=$iron_array[$pro_date_sql_row[csf("buyer_name")]]['7'];
								$total_re_iron+=$re_iron_array[$pro_date_sql_row[csf("buyer_name")]]['7'];
                                $total_finish+=$finish_array[$pro_date_sql_row[csf("buyer_name")]]['8'];                                    
                          
                        $i++;
                    }//end foreach 1st
                        $chart_data_qnty="Order Qty;".$total_po_quantity."\n"."Cutting;".$total_cut."\n"."Sew In;".$total_sew_input."\n"."Sew Out ;".$total_sew_out."\n"."Iron ;".$total_iron."\n"."Finish ;".$total_finish."\n"."Ex-Fact;".$total_ex_factory."\n";
                    ?>
                    </table>
                    <input type="hidden" id="graph_data" value="<?php echo substr($chart_data_qnty,0,-1); ?>"/>
                     <table border="1" class="tbl_bottom"  width="910" rules="all" id="" >
                             <tr> 
                                <td width="30">&nbsp;</td> 
                                <td width="80">Total</td> 
                                <td width="80" id="tot_po_quantity"><?php echo number_format($total_po_quantity); ?></td> 
                                <td width="80" id="tot_po_value"><?php echo number_format($total_po_value); ?></td> 
                                <td width="80" id="tot_cutting"><?php echo number_format($total_cut); ?></td>
                                <td width="80" id="total_sent_embl"><?php echo number_format($total_sent_embl); ?></td>
                                <td width="80" id="total_re_from_embl"><?php echo number_format($total_re_from_embl); ?></td> 
                                <td width="80" id="tot_sew_in"><?php echo number_format($total_sew_input); ?></td> 
                                <td width="80" id="tot_sew_out"><?php echo number_format($total_sew_out); ?></td>   
                                <td width="80" id="tot_iron"><?php echo number_format($total_iron); ?></td>
                                <td width="80" id="tot_re_iron"><?php echo number_format($total_re_iron); ?></td> 
                                <td width="80" id="tot_finish"><?php echo number_format($total_finish); ?></td>
                             </tr>
                     </table>
                   </div>
                 </div>
                <div style="clear:both"></div>
                 <br />
                                
                <table width="2280" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                   <thead>
                        <th width="30">Sl.</th>    
                        <th width="100">Working Factory</th>
                        <th width="100">Job No</th>
                        <th width="130">Order Number</th>
                        <th width="100">Buyer Name</th>
                        <th width="130">Style Name</th>
                        <th width="130">Item Name</th>
                        <th width="100">Production Date</th>
                        <th width="80">Cutting</th>
                        <th width="80">Sent to prnt</th>
                        <th width="80">Rev prn/Emb</th>
                        
                        <th width="80">Sent to Emb</th>
                        <th width="80">Rev Emb</th>
                        <th width="80">Sent to Wash</th>
                        <th width="80">Rev Wash</th>
                        <th width="80">Sent to Sp. Works</th>
                        <th width="80">Rev Sp. Works</th>
                        
                        <th width="80">Sewing Input</th>
                        <th width="80">Sewing Output</th>
                        <th width="80">Iron Qty </th>
                        <th width="80">Re-Iron Qty </th>
                        <th width="80">Finish Qty </th>
                        <th width="80">Today Carton</th>
                        <th width="80">Prod/Dzn</th>
                        <th width="80">Reject Qty</th>
                     <th width="">Remarks</th>
                     </thead>
                </table>
                <div style="width:2280px; overflow-y: scroll; max-height:300px;" id="scroll_body2">
                    <table cellspacing="0" border="1" class="rpt_table"  width="2262" rules="all" id="table_body" >
                    <?php
					
					$cutting_array=array();
					$printing_array=array();
					$printreceived_array=array();
					
					$emb_array=array();
					$embreceived_array=array();
					$wash_array=array();
					$washreceived_array=array();
					$sp_array=array();
					$washreceived_array=array();
					
					$sewingin_array=array();
					$sewingout_array=array();
					$iron_array=array();
					$re_iron_array=array();
					$finish_array=array();
					$finish_rej_array=array();
					$sewingout_rej_array=array();
					$carton_qty_array=array();
					$sql_order=sql_select("SELECT c.po_break_down_id,c.production_date,c.item_number_id,
					sum(CASE WHEN c.production_type ='1' THEN c.production_quantity ELSE 0 END) AS cutting_qnty,
					sum(CASE WHEN c.production_type ='2' THEN c.production_quantity ELSE 0 END) AS printing_qnty,
					sum(CASE WHEN c.production_type ='3' THEN c.production_quantity ELSE 0 END) AS printreceived_qnty,
					
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS emb_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS embreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS wash_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS washreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS sp_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS spreceived_qnty,
					
					sum(CASE WHEN c.production_type ='4' THEN c.production_quantity ELSE 0 END) AS sewingin_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.production_quantity ELSE 0 END) AS sewingout_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.production_quantity ELSE 0 END) AS iron_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.re_production_qty ELSE 0 END) AS re_iron_qnty,  
					sum(CASE WHEN c.production_type ='8' THEN c.production_quantity ELSE 0 END) AS finish_qnty,
					sum(CASE WHEN c.production_type ='8' THEN c.reject_qnty ELSE 0 END) AS finish_rej_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.reject_qnty ELSE 0 END) AS sewingout_rej_qnty,
					sum(c.carton_qty) as carton_qty
								from 
									wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
								where 
									a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature 
									group by c.po_break_down_id,c.production_date,c.item_number_id");
					foreach($sql_order as $sql_result)
					{
						$cutting_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['1']=$sql_result[csf("cutting_qnty")];
						$printing_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("printing_qnty")];
						$printreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("printreceived_qnty")];
						
						$emb_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("emb_qnty")];
						$embreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("embreceived_qnty")];
						
						$wash_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("wash_qnty")];
						$washreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("washreceived_qnty")];
						
						$sp_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("sp_qnty")];
						$spreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("spreceived_qnty")];
						
						$sewingin_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['4']=$sql_result[csf("sewingin_qnty")];
						$sewingout_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['5']=$sql_result[csf("sewingout_qnty")];
						$sewingout_rej_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['5']=$sql_result[csf("sewingout_rej_qnty")];
						$iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['7']=$sql_result[csf("iron_qnty")];
						$re_iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['7']=$sql_result[csf("re_iron_qnty")];
						$finish_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['8']=$sql_result[csf("finish_qnty")];
						$finish_rej_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['8']=$sql_result[csf("finish_rej_qnty")];
						$carton_qty_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['carton_qty']=$sql_result[csf("carton_qty")];
					}
					
					
                    $total_cut=0;$total_print_iss=0;$total_print_re=0;$total_sew_input=0;$total_sew_out=0;$total_iron=0;$total_finish=0;$total_carton=0;$total_prod_dzn=0;$rej_value=0;$total_rej_value=0;
                    //$total_cm_value=0;
                    
                    $i=1;
					if($garments_nature!="") $garments_nature=" and c.garments_nature=$garments_nature";
					
						
					if($db_type==0)
					{
                    	$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.job_no_mst, a.po_number, a.po_quantity, b.job_no_prefix_num, b.order_uom, b.buyer_name, b.style_ref_no as style, b.company_name as company_name, c.garments_nature, c.po_break_down_id,c.item_number_id,c.production_source,c.serving_company,c.location,c.embel_name,c.embel_type, c.production_date,c.production_quantity,c.production_type,c.entry_break_down_type,c.production_hour,c.sewing_line,c.supervisor,c.remarks,c.floor_id,c.alter_qnty,c.reject_qnty 
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature group by c.po_break_down_id,c.production_date order by c.production_date");
					}
					else
					{
						$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.po_number, b.buyer_name, b.style_ref_no as style, b.company_name as company_name,  c.po_break_down_id,c.item_number_id, c.production_date,c.garments_nature
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature 
						group by c.po_break_down_id, b.job_no_prefix_num, a.po_number, b.buyer_name, b.style_ref_no , b.company_name, c.item_number_id, c.production_date,c.garments_nature 
						order by c.production_date");
						/*echo "SELECT b.job_no_prefix_num, a.po_number, b.buyer_name, b.style_ref_no as style, b.company_name as company_name, c.po_break_down_id,c.item_number_id, c.production_date, c.garments_nature
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature  and c.po_break_down_id=4257
						group by c.po_break_down_id, b.job_no_prefix_num, a.po_number, b.buyer_name, b.style_ref_no , b.company_name, c.item_number_id, c.production_date,c.garments_nature 
						order by c.production_date";
						*/
					}
                    //echo $pro_date_sql;//;die;  
 					foreach($pro_date_sql as $pro_date_sql_row)
                    {
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
                   	 	
						?>
                    	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $i; ?>">
                            <td width="30"><?php echo $i;?></td>
                            <td width="100"><p><?php echo $company_short_library[$pro_date_sql_row[csf("company_name")]]; ?></p></td>
                            <td width="100"><p><?php echo $pro_date_sql_row[csf("job_no_prefix_num")];?></p></td>
                            <td width="130"><p><a href="##" onclick="openmypage_order(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'orderQnty_popup');" ><?php echo $pro_date_sql_row[csf("po_number")]; ?></a></p></td>
                            <td width="100"><p><?php echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></p></td>
                            <td width="130"><p><?php echo $pro_date_sql_row[csf("style")]; ?></p></td>
                            <td width="130"><p><?php echo $garments_item[$pro_date_sql_row[csf("item_number_id")]]; ?></p></td>
                            <td width="100"><p><?php echo change_date_format($pro_date_sql_row[csf("production_date")]); ?></p></td>
                            
                            
                            <td width="80" align="right">
								<?php
									echo $cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['1'];  
									$total_cut+=$cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['1'];
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_print_iss+=$printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_print_re+=$printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            
                            
                            <td width="80" align="right">
								<?php
									echo $emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_emb_iss+=$emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_emb_re+=$embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_wash_iss+=$wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_wash_re+=$washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_sp_iss+=$sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_sp_re+=$spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            
                            
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'sewingQnty_input_popup');" >
                            	<?php
									echo $sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['4'];  
									$total_sew_input+=$sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['4'];  
								?>
                                </a>
                            </td>
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'sewingQnty_popup');" >
                            	<?php
									echo $sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['5'];  
									$total_sew_out+=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['5'];  
								?>
                            </a>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7'];  
									$total_iron+=$iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7'];  
									$total_re_iron+=$re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7']; 
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['8'];  
									$total_finish+=$finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['8'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $carton_qty_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]]['carton_qty'][$pro_date_sql_row[csf("item_number_id")]];  
									$total_carton+=$carton_qty_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]]['carton_qty'][$pro_date_sql_row[csf("item_number_id")]]; 
								?>
                            </td>
                            
                            
                            <?php $prod_dzn=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]]['5'] / 12 ; $total_prod_dzn+=$prod_dzn; ?>
                            <td width="80" align="right"><?php if($prod_dzn!=0) echo number_format($prod_dzn,2); else echo "0"; ?></td>
 							<?php
                            //$cm_per_dzn=return_field_value("cm_for_sipment_sche","wo_pre_cost_dtls","job_no='".$pro_date_sql_row[csf("job_no_mst")]."' and is_deleted=0 and status_active=1");
                            ?>
                            <td width="80" align="right" ><?php $rej_value=$finish_rej_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['8']+$sewingout_rej_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['5']; echo number_format($rej_value,2); $total_rej_value+=$rej_value; ?></td>
                            
                            <td width="">
                            	<a href="##"  onclick="openmypage_remark(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,'date_wise_production_report');" > Veiw </a>
                            </td>
                   	 </tr>
						<?php
					$i++;
				}//end foreach 1st
				?>
                </table> 
                
           		</div>
                <table width="2280" cellspacing="0" border="1" class="rpt_table" rules="all" id="report_table_footer" >
                    <tfoot>
                            <th width="30" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right">Total</th> 
                            <th width="80" align="right" id="total_cut_td" ><?php echo $total_cut;?></th> 
                            <th width="80" align="right" id="total_printissue_td"><?php echo $total_print_iss; ?> </th> 
                            <th width="80" align="right" id="total_printrcv_td"><?php  echo $total_print_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_emb_iss"><?php echo $total_emb_iss; ?> </th> 
                            <th width="80" align="right" id="total_emb_re"><?php  echo $total_emb_re;  ?>  </th>
                            <th width="80" align="right" id="total_wash_iss"><?php echo $total_wash_iss; ?> </th> 
                            <th width="80" align="right" id="total_wash_re"><?php  echo $total_wash_re;  ?>  </th>
                            <th width="80" align="right" id="total_sp_iss"><?php echo $total_sp_iss; ?> </th> 
                            <th width="80" align="right" id="total_sp_re"><?php  echo $total_sp_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_sewin_td"><?php echo $total_sew_input;  ?> </th> 
                            <th width="80" align="right" id="total_sewout_td"><?php echo $total_sew_out;  ?> </th>
                            <th width="80" align="right" id="total_iron_td"><?php  echo $total_iron; ?> </th> 
                            <th width="80" align="right" id="total_iron_td"><?php  echo $total_re_iron; ?> </th> 
                            <th width="80" align="right" id="total_finish_td"><?php  echo $total_finish; ?>  </th>   
                            <th width="80" align="right" id="total_carton_td"><?php echo $total_carton; ?> </th> 
                            <th width="80" align="right" id="total_prod_dzn_td"><?php  echo number_format($total_prod_dzn,2); ?> </th>
                            <th width="80" align="right" id="total_rej_value_td"><?php echo number_format($total_rej_value,2); ?> </th >
                            <th width="">&nbsp;</th>
                       </tfoot>
                </table>
       		</div>    
			<?php
		}// end if condition of type
		//-------------------------------------------END Show Date Wise------------------------
		//-------------------------------------------Show Date Location Floor & Line Wise------------------------	
		if($type==2)
		{
			
			ob_start();
		?>
        	<div style="max-height:425px; overflow-y:scroll; width:2200px" id="scroll_body">
                <table width="2100px"  cellspacing="0"   >
                    <tr class="form_caption" style="border:none;">
                            <td colspan="23" align="center" style="border:none;font-size:14px; font-weight:bold"> Date Location Floor & Line Wise Production Report</td>
                     </tr>
                    <tr style="border:none;">
                            <td colspan="23" align="center" style="border:none; font-size:16px; font-weight:bold">
                                Company Name:<?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?>                                
                            </td>
                      </tr>
                      <tr style="border:none;">
                            <td colspan="23" align="center" style="border:none;font-size:12px; font-weight:bold">
                            	<?php echo "From $fromDate To $toDate" ;?>
                            </td>
                      </tr>
                </table>
                <table width="2680px" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                   <thead>
                        <th width="30">Sl.</th>    
                        <th width="100">Working Factory</th>
                        <th width="100">Job No</th>
                        <th width="130">Order Number</th>
                        <th width="100">Buyer Name</th>
                        <th width="130">Style Name</th>
                        <th width="130">Item Name</th>
                        <th width="100">Production Date</th>
                        <th width="100">Status</th>
                        <th width="100">Location</th>
                        <th width="100">Floor</th>
                        <th width="100">Sewing Line No</th>
                        <th width="80">Cutting</th>
                        <th width="80">Sent to prnt</th>
                        <th width="80">Rev prn/Emb</th>
                        
                        <th width="80">Sent to Emb</th>
                        <th width="80">Rev Emb</th>
                        <th width="80">Sent to Wash</th>
                        <th width="80">Rev Wash</th>
                        <th width="80">Sent to Sp. Works</th>
                        <th width="80">Rev Sp. Works</th>
                        
                        <th width="80">Sewing Input</th>
                        <th width="80">Sewing Output</th>
                        <th width="80">Iron Qty </th>
                        <th width="80">Re-Iron Qty </th>
                        <th width="80">Finish Qty </th>
                        <th width="80">Today Carton</th>
                        <th width="80">Prod/Dzn</th>
                        <th width="80">CM Cost</th>
                        <th width="">Remarks</th>
                       </thead>
                </table>
                <div style="max-height:425px; overflow-y:scroll; width:2680px" id="scroll_body2">
                    <table cellspacing="0" border="1" class="rpt_table"  width="2662px" rules="all" id="table_body" >
                    <?php
					$cutting_array=array();
					$printing_array=array();
					$printreceived_array=array();
					
					$emb_array=array();
					$embreceived_array=array();
					$wash_array=array();
					$washreceived_array=array();
					$sp_array=array();
					$washreceived_array=array();
					
					$sewingin_array=array();
					$sewingout_array=array();
					$iron_array=array();
					$re_iron_array=array();
					$finish_array=array();
					$carton_array=array();
					
					$sql_order=sql_select("SELECT c.po_break_down_id, c.production_date, c.item_number_id, c.location, c.floor_id, c.sewing_line,
					sum(CASE WHEN c.production_type ='1' THEN c.production_quantity ELSE 0 END) AS cutting_qnty,
					sum(CASE WHEN c.production_type ='2' THEN c.production_quantity ELSE 0 END) AS printing_qnty,
					sum(CASE WHEN c.production_type ='3' THEN c.production_quantity ELSE 0 END) AS printreceived_qnty,
					
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS emb_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS embreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS wash_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS washreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS sp_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS spreceived_qnty,
					
					sum(CASE WHEN c.production_type ='4' THEN c.production_quantity ELSE 0 END) AS sewingin_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.production_quantity ELSE 0 END) AS sewingout_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.production_quantity ELSE 0 END) AS iron_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.re_production_qty ELSE 0 END) AS re_iron_qnty, 
					sum(CASE WHEN c.production_type ='8' THEN c.production_quantity ELSE 0 END) AS finish_qnty, 
					sum(c.carton_qty) as carton_qty
								from 
									wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
								where 
									a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature 
									group by c.po_break_down_id, c.production_date, c.item_number_id, c.location, c.floor_id, c.sewing_line");

					foreach($sql_order as $sql_result)
					{
						$cutting_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['1']=$sql_result[csf("cutting_qnty")];
						$printing_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("printing_qnty")];
						$printreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("printreceived_qnty")];
						
						$emb_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("emb_qnty")];
						$embreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("embreceived_qnty")];
						
						$wash_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("wash_qnty")];
						$washreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("washreceived_qnty")];
						
						$sp_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("sp_qnty")];
						$spreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("spreceived_qnty")];
						
						$sewingin_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['4']=$sql_result[csf("sewingin_qnty")];
						$sewingout_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['5']=$sql_result[csf("sewingout_qnty")];
						$iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['7']=$sql_result[csf("iron_qnty")];
						$re_iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['7']=$sql_result[csf("re_iron_qnty")];
						$finish_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['8']=$sql_result[csf("finish_qnty")];
						$carton_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]=$sql_result[csf("carton_qty")];
						
					}
					
                    $total_cut=0; $total_print_iss=0; $total_print_re=0; $total_sew_input=0; $total_sew_out=0; $total_iron=0; $total_re_iron=0; $total_finish=0; $total_carton=0;  $total_prod_dzn=0;
                    //$total_cm_value=0;
                    
                    $i=1;
					if($garments_nature!="") $garments_nature=" and c.garments_nature=$garments_nature";
					
					if($db_type==0)
					{
                    	$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.job_no_mst, a.po_number, a.po_quantity, b.order_uom, b.buyer_name, b.style_ref_no as style, b.company_name as company_name, c.garments_nature, c.po_break_down_id, c.item_number_id, c.production_source, c.serving_company, c.location, c.embel_name, c.embel_type, c.production_date, c.production_quantity, c.production_type, c.entry_break_down_type, c.production_hour, c.sewing_line, c.supervisor, c.carton_qty, c.remarks, c.floor_id, c.alter_qnty, c.reject_qnty, c.prod_reso_allo
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id  and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature group by c.po_break_down_id,c.production_date,c.location,c.floor_id,c.sewing_line order by c.production_date");
					}
					else
					{
						$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.po_number,b.buyer_name,b.style_ref_no as style ,b.company_name as company_name, c.garments_nature, c.po_break_down_id, c.item_number_id, c.production_source, c.location, c.production_date, c.sewing_line, c.carton_qty, c.floor_id, c.prod_reso_allo
						from wo_po_break_down a, wo_po_details_master b, pro_garments_production_mst c 
						where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature 
						group by b.job_no_prefix_num, a.po_number, b.buyer_name, b.style_ref_no, b.company_name, c.garments_nature,
c.po_break_down_id, c.item_number_id, c.production_source, c.location, c.production_date, c.sewing_line, c.carton_qty, c.floor_id, c.prod_reso_allo 
						order by c.production_date");
					}
                    //echo $pro_date_sql;die;
 					foreach($pro_date_sql as $pro_date_sql_row)
                    {
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						
						$sewing_line='';
						if($pro_date_sql_row[csf('prod_reso_allo')]==1)
						{
							$line_number=explode(",",$prod_reso_arr[$pro_date_sql_row[csf('sewing_line')]]);
							foreach($line_number as $val)
							{
								if($sewing_line=='') $sewing_line=$line_library[$val]; else $sewing_line.=", ".$line_library[$val];
							}
						}
						else $sewing_line=$line_library[$pro_date_sql_row[csf('sewing_line')]];                   	 	
                    ?>
                    	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $i; ?>">
                            <td width="30"><?php echo $i;?></td>
                            <td width="100"><p><?php echo $company_short_library[$pro_date_sql_row[csf("company_name")]]; ?></p></td>
                            <td width="100"><p><?php echo $pro_date_sql_row[csf("job_no_prefix_num")];?></p></td>
                            <td width="130"><p><a href="##" onclick="openmypage_order(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,<?php echo $pro_date_sql_row[csf("item_number_id")]; ?>,'orderQnty_popup');" ><?php echo $pro_date_sql_row[csf("po_number")]; ?></a></p></td>
                            <td width="100"><p><?php echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></p></td>
                            <td width="130"><p><?php echo $pro_date_sql_row[csf("style")]; ?></p></td>
                            <td width="130"><p><?php echo $garments_item[$pro_date_sql_row[csf("item_number_id")]]; ?></p></td>
                            <td width="100"><p><?php echo change_date_format($pro_date_sql_row[csf("production_date")]); ?></p></td>
                            <td width="100"><p><?php echo $knitting_source[$pro_date_sql_row[csf("production_source")]]; ?></p></td>
                            
                            <td width="100"><p><?php echo $location_library[$pro_date_sql_row[csf("location")]]; ?></p></td>
                            <td width="100"><p><?php echo $floor_library[$pro_date_sql_row[csf("floor_id")]]; ?></p></td>
                            <td width="100" align="center"><p><?php echo $sewing_line; ?></p></td>
                            
                            <td width="80" align="right">
                            	<?php
									echo $cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['1'];  
									$total_cut+=$cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['1'];
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_print_iss+=$printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_print_re+=$printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
								?>
                            </td>
                            
                            <td width="80" align="right">
								<?php
									echo $emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_emb_iss+=$emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_emb_re+=$embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_wash_iss+=$wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_wash_re+=$washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_sp_iss+=$sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_sp_re+=$spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3']; 
								?>
                            </td>
                            
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'sewingQnty_input_popup');" >
                            	<?php
									echo $sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['4'];  
									$total_sew_input+=$sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['4'];  
								?>
                                </a>
                            </td>
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'sewingQnty_popup');" >
                            	<?php
									echo $sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['5'];  
									$total_sew_out+=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['5'];  
								?>
                            </a></td>
                            <td width="80" align="right">
                            	<?php
									echo $iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
									$total_iron+=$iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
									$total_re_iron+=$re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['8'];  
									$total_finish+=$finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['8']; 
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $carton_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]];  
									$total_carton+=$carton_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]; 
								?>
                            </td>
                            
                            
                            <?php $prod_dzn=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['5'] / 12 ; $total_prod_dzn+=$prod_dzn; ?>
                            <td width="80" align="right"><?php if($prod_dzn!=0) echo number_format($prod_dzn,2); else echo "0"; ?></td>
 							<?php
                            //$cm_per_dzn=return_field_value("cm_for_sipment_sche","wo_pre_cost_dtls","job_no='".$pro_date_sql_row[csf("job_no_mst")]."' and is_deleted=0 and status_active=1");
                            ?>
<!--                            <td align="right" width="80"><?php //$total_cm_value+=$cm_per_dzn*$prod_dzn; if($cm_per_dzn*$prod_dzn==-0)echo "0"; else echo number_format($cm_per_dzn*$prod_dzn,2); ?></td>
-->                            <td width="">
                             <a href="##"  onclick="openmypage_remark(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,'date_wise_production_report');" > Veiw </a>
                            </td>
                   	 </tr>
						<?php
						
					$i++;
				}//end foreach 1st
				
				?>
                </table>
                <table width="2680" cellspacing="0" border="1" class="rpt_table" rules="all" id="report_table_footer" >
                    <tfoot>
                        <tr>
                            <th width="30" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th> 
                            <th width="100" align="right">Total</th>
                            <th width="80" align="right" id="total_cut_td"><?php echo $total_cut;?></th> 
                            <th width="80" align="right" id="total_printissue_td"><?php echo $total_print_iss; ?> </th> 
                            <th width="80" align="right" id="total_printrcv_td"><?php  echo $total_print_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_emb_iss"><?php echo $total_emb_iss; ?> </th> 
                            <th width="80" align="right" id="total_emb_re"><?php  echo $total_emb_re;  ?>  </th>
                            <th width="80" align="right" id="total_wash_iss"><?php echo $total_wash_iss; ?> </th> 
                            <th width="80" align="right" id="total_wash_re"><?php  echo $total_wash_re;  ?>  </th>
                            <th width="80" align="right" id="total_sp_iss"><?php echo $total_sp_iss; ?> </th> 
                            <th width="80" align="right" id="total_sp_re"><?php  echo $total_sp_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_sewin_td"><?php echo $total_sew_input;  ?> </th> 
                            <th width="80" align="right" id="total_sewout_td"><?php echo $total_sew_out;  ?> </th>
                            <th width="80" align="right" id="total_iron_td"><?php  echo $total_iron; ?>  </th>
                            <th width="80" align="right" id="total_re_iron_td"><?php  echo $total_re_iron; ?>  </th> 
                            <th width="80" align="right" id="total_finish_td"><?php  echo $total_finish; ?>  </th>   
                            <th width="80" align="right" id="total_carton_td"><?php echo $total_carton; ?> </th> 
                            <th width="80" align="right" id="total_prod_dzn_td"><?php  echo number_format($total_prod_dzn,2); ?> </th>
                            <th width="" >&nbsp;</th>
                         </tr>
                     </tfoot>
                </table>
              	</div>
          	</div>
		<?php
		}// end if condition of type
	}
	if(str_replace("'","",trim($cbo_subcon))==2) //yes
	{
		$garments_nature=str_replace("'","",$cbo_garments_nature);
		if($garments_nature==1)$garments_nature="";
		$type = str_replace("'","",$cbo_type);
		//echo $txt_date_from."<br>".$txt_date_to;
		if(str_replace("'","",$cbo_company_name)==0)$company_name=""; else $company_name=" and b.company_name=$cbo_company_name";
		if(str_replace("'","",$cbo_buyer_name)==0)$buyer_name="";else $buyer_name=" and b.buyer_name=$cbo_buyer_name";
		if(str_replace("'","",trim($txt_date_from))=="" || str_replace("'","",trim($txt_date_to))=="")$txt_date="";
		else $txt_date=" and c.production_date between $txt_date_from and $txt_date_to";
		$fromDate = change_date_format( str_replace("'","",trim($txt_date_from)) );
		$toDate = change_date_format( str_replace("'","",trim($txt_date_to)) );
		//cbo_garments_nature
		
		if($type==1) //--------------------------------------------Show Date Wise
		{
			ob_start();
			?>
			<div id="scroll_body">
                <table width="1700"  cellspacing="0"   >
                    <tr class="form_caption" style="border:none;">
                        <td colspan="19" align="center" style="border:none;font-size:14px; font-weight:bold" > Date Wise Production Report</td>
                     </tr>
                    <tr style="border:none;">
                        <td colspan="19" align="center" style="border:none; font-size:16px; font-weight:bold">
                            Company Name:<?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?>                                
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td colspan="19" align="center" style="border:none;font-size:12px; font-weight:bold">
                            <?php echo "From $fromDate To $toDate" ;?>
                        </td>
                    </tr>
                </table>
                <!--<div style="float:left; width:1020px">In-House Order Production</div>-->
                <div style="float:left; width:1420px">
					<table width="1410" cellspacing="0" border="1" class="" rules="all" id="" >
                    <tr>
                    <td width="910" align="left" valign="top"><div align="left" style="width:200px"><strong>Production-Regular Order </strong></div>
                    <table width="910" cellspacing="0" border="1" class="rpt_table" rules="all" id="" >
                        <thead>
                            <tr>
                                <th width="30">Sl.</th>    
                                <th width="80">Buyer Name</th>
                                <th width="80">Cut Qty</th>
                                <th width="80">Sent Embl.</th>
                                <th width="80">Rec. From Embl.</th>
                                <th width="80">Sew Input</th>
                                <th width="80">Sew Output</th>
                                <th width="80">Total Iron</th>
                                <th width="80">Total Re-Iron</th>
                                <th width="80">Total Finish</th>
                             </tr>
                        </thead>
                    </table>
                    <div style="max-height:425px; width:930px" >
                        <table cellspacing="0" border="1" class="rpt_table"  width="910" rules="all" id="" >
                        <?php
						
						$cutting_array=array();
						$printing_array=array();
						$printreceived_array=array();
												
						$sewingin_array=array();
						$sewingout_array=array();
						$iron_array=array();
						$re_iron_array=array();
						$finish_array=array();
						
						$sql_order=sql_select("SELECT b.buyer_name,
						sum(CASE WHEN production_type ='1' THEN production_quantity ELSE 0 END) AS cutting_qnty,
						sum(CASE WHEN production_type ='2' THEN production_quantity ELSE 0 END) AS printing_qnty,
						sum(CASE WHEN production_type ='3' THEN production_quantity ELSE 0 END) AS printreceived_qnty,
						sum(CASE WHEN production_type ='4' THEN production_quantity ELSE 0 END) AS sewingin_qnty,
						sum(CASE WHEN production_type ='5' THEN production_quantity ELSE 0 END) AS sewingout_qnty,                                          
						sum(CASE WHEN production_type ='7' THEN production_quantity ELSE 0 END) AS iron_qnty,
						sum(CASE WHEN production_type ='7' THEN re_production_qty ELSE 0 END) AS re_iron_qnty,
						sum(CASE WHEN production_type ='8' THEN production_quantity ELSE 0 END) AS finish_qnty
									from 
                                        wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                    where  
                                        a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $floor_name $garmentsNature group by b.buyer_name");
						foreach($sql_order as $sql_result)
						{
							$cutting_array[$sql_result[csf("buyer_name")]]['1']=$sql_result[csf("cutting_qnty")];
							$printing_array[$sql_result[csf("buyer_name")]]['2']=$sql_result[csf("printing_qnty")];
							$printreceived_array[$sql_result[csf("buyer_name")]]['3']=$sql_result[csf("printreceived_qnty")];
							$sewingin_array[$sql_result[csf("buyer_name")]]['4']=$sql_result[csf("sewingin_qnty")];
							$sewingout_array[$sql_result[csf("buyer_name")]]['5']=$sql_result[csf("sewingout_qnty")];
							$iron_array[$sql_result[csf("buyer_name")]]['7']=$sql_result[csf("iron_qnty")];
							$re_iron_array[$sql_result[csf("buyer_name")]]['7']=$sql_result[csf("re_iron_qnty")];
							$finish_array[$sql_result[csf("buyer_name")]]['8']=$sql_result[csf("finish_qnty")];
						}
						
                         $total_po_quantity=0;$total_po_value=0;
                         $total_cut=0;$total_sent_embl=0;
                         $total_re_from_embl=0; $total_sew_input=0;
                         $total_sew_out=0;$total_iron=0;$total_re_iron=0;
                         $total_finish=0;
                         $i=1;
                         
                         // garments nature here -------------------------------							
                         if($garments_nature==1 || $garments_nature=="") $garmentsNature=""; else $garmentsNature=" and b.garments_nature=$garments_nature";
						 
						 
                         /*$exfactory_sql= "select b.buyer_name,sum(c.production_quantity) as production_quantity 
                                        from wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                        where a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and c.company_id=$cbo_company_name $txt_date $buyer_name $location $floor_name $garmentsNature group by b.buyer_name";
						//echo $exfactory_sql;//die;
						$exfactory_sql_result=sql_select($exfactory_sql);	
                        $exfactory_arr=array(); 
                        foreach($exfactory_sql_result as $resRow)
						{
                            $exfactory_arr[$resRow[csf("buyer_name")]] = $resRow[csf("production_quantity")];
                        }*/
						
						
						if($db_type==0)
						{                           
                        	$pro_date_sql_query="SELECT b.company_name, b.buyer_name,sum(a.po_quantity*b.total_set_qnty) as po_quantity,sum(a.po_total_price) as po_total_price,a.id
									from lib_buyer d, wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                    where a.job_no_mst=b.job_no and a.id=c.po_break_down_id and b.buyer_name=d.id and a.status_active=1 and b.status_active=1 and b.company_name=$cbo_company_name and c.production_date between $txt_date_from and $txt_date_to $buyer_name $floor_name $garmentsNature group by b.buyer_name order by d.buyer_name ASC"; 
						}
						else
						{
							$pro_date_sql_query="SELECT b.buyer_name,sum(a.po_quantity*b.total_set_qnty) as po_quantity,sum(a.po_total_price) as po_total_price
									from lib_buyer d, wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                    where a.job_no_mst=b.job_no and a.id=c.po_break_down_id and b.buyer_name=d.id and a.status_active=1 and b.status_active=1 and b.company_name=$cbo_company_name and c.production_date between $txt_date_from and $txt_date_to $buyer_name $floor_name $garmentsNature group by b.buyer_name order by b.buyer_name ASC"; 
							
						}
                       //echo $pro_date_sql_query;//die; 
					   $pro_date_sql=sql_select($pro_date_sql_query);
					   
                        foreach($pro_date_sql as $pro_date_sql_row)
                        {
                            if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                            
                        ?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                                <td width="30"><?php echo $i; ?></td>
                                <td width="80"><?php echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                                <td width="80" align="right"><?php echo number_format($cutting_array[$pro_date_sql_row[csf("buyer_name")]]['1']); ?></td>
                                <td width="80" align="right"><?php echo number_format($printing_array[$pro_date_sql_row[csf("buyer_name")]]['2']); ?></td>
                                <td width="80" align="right"><?php echo number_format($printreceived_array[$pro_date_sql_row[csf("buyer_name")]]['3']);  ?></td>
                                <td width="80" align="right"><?php echo number_format($sewingin_array[$pro_date_sql_row[csf("buyer_name")]]['4']); ?></td>
                                <td width="80" align="right"><?php echo number_format($sewingout_array[$pro_date_sql_row[csf("buyer_name")]]['5']); ?></td>
                                <td width="80" align="right"><?php echo number_format($iron_array[$pro_date_sql_row[csf("buyer_name")]]['7']); ?></td>
                                <td width="80" align="right"><?php echo number_format($re_iron_array[$pro_date_sql_row[csf("buyer_name")]]['7']); ?></td>
                                <td width="80" align="right"><?php echo number_format($finish_array[$pro_date_sql_row[csf("buyer_name")]]['8']); ?></td>
                            </tr>	
                            
                            <?php
							
							$total_po_quantity+=$pro_date_sql_row[csf("po_quantity")];
							$total_po_value+=$pro_date_sql_row[csf("po_total_price")];
							$total_cut+=$cutting_array[$pro_date_sql_row[csf("buyer_name")]]['1'];
							$total_sent_embl+=$printing_array[$pro_date_sql_row[csf("buyer_name")]]['2'];
							$total_re_from_embl+=$printreceived_array[$pro_date_sql_row[csf("buyer_name")]]['3'];
							$total_sew_input+=$sewingin_array[$pro_date_sql_row[csf("buyer_name")]]['4'];
							$total_sew_out+=$sewingout_array[$pro_date_sql_row[csf("buyer_name")]]['5'];
							$total_iron+=$iron_array[$pro_date_sql_row[csf("buyer_name")]]['7'];
							$total_re_iron+=$re_iron_array[$pro_date_sql_row[csf("buyer_name")]]['7'];
							$total_finish+=$finish_array[$pro_date_sql_row[csf("buyer_name")]]['8']; 
							
                        $i++;
                        
                    }//end foreach 1st
                        $chart_data_qnty="Order Qty;".$total_po_quantity."\n"."Cutting;".$total_cut."\n"."Sew In;".$total_sew_input."\n"."Sew Out ;".$total_sew_out."\n"."Iron ;".$total_iron."\n"."Finish ;".$total_finish."\n"."Ex-Fact;".$total_ex_factory."\n";
                    ?>
                    </table>
                    <input type="hidden" id="graph_data" value="<?php echo substr($chart_data_qnty,0,-1); ?>"/>
                     <table border="1" class="tbl_bottom"  width="910" rules="all" id="" >
                             <tr> 
                                <td width="30">&nbsp;</td> 
                                <td width="80">Total</td> 
                                <td width="80" id="tot_cutting"><?php echo number_format($total_cut); ?></td>
                                <td width="80" id="total_sent_embl"><?php echo number_format($total_sent_embl); ?></td>
                                <td width="80" id="total_re_from_embl"><?php echo number_format($total_re_from_embl); ?></td> 
                                <td width="80" id="tot_sew_in"><?php echo number_format($total_sew_input); ?></td> 
                                <td width="80" id="tot_sew_out"><?php echo number_format($total_sew_out); ?></td>   
                                <td width="80" id="tot_iron"><?php echo number_format($total_iron); ?></td> 
                                <td width="80" id="tot_re_iron"><?php echo number_format($total_re_iron); ?></td> 
                                <td width="80" id="tot_finish"><?php echo number_format($total_finish); ?></td>
                             </tr>
                     </table>
                   </div>
                   </td>
                   <td width="520" align="left" valign="top"><div align="left" style="width:200px"><strong>Production-Subcontract Order </strong></div>
                   <div style="float:left; width:750px">
                   
            <table width="430" cellspacing="0" border="1" class="rpt_table" rules="all" id="" >
                <thead>
                    <tr>
                        <th width="40">Sl.</th>    
                        <th width="150">Buyer</th>
                        <th width="100">Total Cut Qty</th>
                        <th width="100">Total Sew Qty</th>
                    </tr>
                </thead>
            </table>
            <div style="max-height:425px; width:430px" >
                <table cellspacing="0" border="1" class="rpt_table"  width="430" rules="all" id="" >
					<?php  $total_po_quantity=0;$total_po_value=0;$total_cut_subcon=0;$total_sew_out_subcon=0;$total_ex_factory=0;
						$i=1;
						
						if($db_type==0)
						{
							$ex_factory_sql="select a.party_id, sum(c.order_quantity) as order_quantity 
									from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
									where a.subcon_job=c.job_no_mst and c.id=b.order_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1  and b.production_date between $txt_date_from and $txt_date_to and a.company_id=$cbo_company_name $location $floor group by a.party_id";
						}
						else
						{
							$ex_factory_sql="select a.party_id, sum(c.order_quantity) as order_quantity 
									from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
									where a.subcon_job=c.job_no_mst and c.id=b.order_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1  and b.production_date between $txt_date_from and $txt_date_to  and a.company_id=$cbo_company_name $location $floor group by a.party_id";
							
						}
						//echo  $exfactory_sql;
						$ex_factory_sql_result=sql_select($ex_factory_sql);
						$ex_factory_arr=array(); 
						foreach($ex_factory_sql_result as $resRow)
						{
							$ex_factory_arr[$resRow[csf("party_id")]] = $resRow[csf("order_quantity")];
						}
						//var_dump($exfactory_arr);die;
						//print_r($ex_factory_arr);die;
						
						//@@@@@@@@@@@@@@@@@@@@@
						$sub_cut_sew_array=array();
						
						if($db_type==0)
						{
							$production_mst_sql= sql_select("SELECT  a.party_id,
									sum(CASE WHEN production_type ='1' THEN production_qnty ELSE 0 END) AS cutting_qnty,
									sum(CASE WHEN production_type ='2' THEN production_qnty ELSE 0 END) AS sewingout_qnty 
								from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
								where c.job_no_mst=a.subcon_job and c.id=b.order_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and  c.is_deleted=0 and c.status_active=1 and b.production_date between $txt_date_from and $txt_date_to and a.company_id=$cbo_company_name $location $floor group by a.party_id");
						}
						else
						{
							$production_mst_sql=sql_select("SELECT  a.party_id,
											sum(CASE WHEN production_type ='1' THEN production_qnty ELSE 0 END) AS cutting_qnty,
											sum(CASE WHEN production_type ='2' THEN production_qnty ELSE 0 END) AS sewingout_qnty 
										from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
										where c.job_no_mst=a.subcon_job and c.id=b.order_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and b.production_date between $txt_date_from and $txt_date_to and a.company_id=$cbo_company_name $location $floor group by a.party_id");
						}
						foreach($production_mst_sql as $sql_result)
						{
							$sub_cut_sew_array[$sql_result[csf("party_id")]]['1']=$sql_result[csf("cutting_qnty")];
							$sub_cut_sew_array[$sql_result[csf("party_id")]]['2']=$sql_result[csf("sewingout_qnty")];
						}
						//var_dump($cutting_array);
						//@@@@@@@@@@@@@@@@@@@@@
						if($db_type==0)
						{
							$production_date_sql="SELECT a.party_id, sum(c.order_quantity) as po_quantity, sum(c.amount) as po_total_price       
									from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c 
									where c.job_no_mst=a.subcon_job and c.id=b.order_id and b.production_type in (1,2) and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and b.production_date between $txt_date_from and $txt_date_to  and a.company_id=$cbo_company_name $location $floor group by a.party_id order by a.party_id ASC";
						}
						else
						{
							$production_date_sql="SELECT a.party_id, sum(c.order_quantity) as po_quantity, sum(c.amount) as po_total_price       
									from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c 
									where c.job_no_mst=a.subcon_job and c.id=b.order_id and b.production_type in (1,2) and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and b.production_date between $txt_date_from and $txt_date_to and a.company_id=$cbo_company_name $location $floor group by a.party_id order by a.party_id ASC";
							
						}
						//echo $production_date_sql;//die;
						$pro_sql_result=sql_select($production_date_sql);	
						foreach($pro_sql_result as $pro_date_sql_row)
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                            
								?>
                                <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                                    <td width="40"><?php echo $i;?></td>
                                    <td width="150"><?php echo $buyer_short_library[$pro_date_sql_row[csf("party_id")]]; ?></td>
                                    <td width="100" align="right"><?php echo number_format($sub_cut_sew_array[$pro_date_sql_row[csf("party_id")]]['1']); ?></td>
                                    <td width="100" align="right"><?php echo number_format($sub_cut_sew_array[$pro_date_sql_row[csf("party_id")]]['2']); ?></td>
                                </tr>	
                                <?php		
                                    $total_cut_subcon+=$sub_cut_sew_array[$pro_date_sql_row[csf("party_id")]]['1'];
                                    $total_sew_out_subcon+=$sub_cut_sew_array[$pro_date_sql_row[csf("party_id")]]['2'];
                            $i++;
                        }//end foreach 1st
						//$chart_data_qnty="Order Qty;".$total_po_quantity."\n"."Cutting;".$total_cut."\n"."Sew Out ;".$total_sew_out."\n"."Ex-Fact;".$total_ex_factory."\n";
                        ?>
                    </table>
                    <table border="1" class="tbl_bottom"  width="430" rules="all" id="" >
                        <tr> 
                            <td width="40">&nbsp;</td> 
                            <td width="150" align="right">Total</td> 
                            <td width="100" id="tot_cutting"><?php echo number_format($total_cut_subcon); ?></td>
                            <td width="100" id="tot_sew_out"><?php echo number_format($total_sew_out_subcon); ?></td>   
                        </tr>
                    </table>
                    <br />
                    <div style="background-color:#FF9; width:400px; font-size:16px"><strong>Total Cutting: <?php echo number_format($all_production_cutt=$total_cut+$total_cut_subcon,0); ?> (Pcs)&nbsp;&nbsp; & Total Sewing: <?php echo number_format($all_production_sewing=$total_sew_out+$total_sew_out_subcon,0); ?> (Pcs)</strong></div>
                 </div>
                 </div>
                   </td>
                   </tr>
                   <tr>
                       <td colspan="2">&nbsp;</td>
                   </tr>
                 </table>  
                 </div>
                 <br />
                  <div style="clear:both"></div>

                 <div align="left" style="width:200px"><strong>Production-Regular Order </strong></div>               
                <table width="2280" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                   <thead>
                        <th width="30">Sl.</th>    
                        <th width="100">Working Factory</th>
                        <th width="100">Job No</th>
                        <th width="130">Order Number</th>
                        <th width="100">Buyer Name</th>
                        <th width="130">Style Name</th>
                        <th width="130">Item Name</th>
                        <th width="100">Production Date</th>
                        <th width="80">Cutting</th>
                        <th width="80">Sent to prnt</th>
                        <th width="80">Rev prn/Emb</th>
                        
                        <th width="80">Sent to Emb</th>
                        <th width="80">Rev Emb</th>
                        <th width="80">Sent to Wash</th>
                        <th width="80">Rev Wash</th>
                        <th width="80">Sent to Sp. Works</th>
                        <th width="80">Rev Sp. Works</th>
                        
                        <th width="80">Sewing Input</th>
                        <th width="80">Sewing Output</th>
                        <th width="80">Iron Qty </th>
                        <th width="80">Re-Iron Qty </th>
                        <th width="80">Finish Qty </th>
                        <th width="80">Today Carton</th>
                        <th width="80">Prod/Dzn</th>
                        <th width="80">Reject Qty</th>
                     <th width="">Remarks</th>
                     </thead>
                </table>
                <div style="width:2280px; overflow-y: scroll; max-height:300px;" id="scroll_body2">
                    <table cellspacing="0" border="1" class="rpt_table"  width="2262" rules="all" id="table_body" >
                    <?php
					
					$cutting_array=array();
					$printing_array=array();
					$printreceived_array=array();
					
					$emb_array=array();
					$embreceived_array=array();
					$wash_array=array();
					$washreceived_array=array();
					$sp_array=array();
					$washreceived_array=array();
					
					$sewingin_array=array();
					$sewingout_array=array();
					$iron_array=array();
					$re_iron_array=array();
					$finish_array=array();
					$finish_rej_array=array();
					$sewingout_rej_array=array();
					$carton_qty_array=array();
					
					
										/*echo "SELECT c.po_break_down_id, c.production_date,
					sum(CASE WHEN c.production_type ='1' THEN c.production_quantity ELSE 0 END) AS cutting_qnty,
					sum(CASE WHEN c.production_type ='2' THEN c.production_quantity ELSE 0 END) AS printing_qnty,
					sum(CASE WHEN c.production_type ='3' THEN c.production_quantity ELSE 0 END) AS printreceived_qnty,
					sum(CASE WHEN c.production_type ='4' THEN c.production_quantity ELSE 0 END) AS sewingin_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.production_quantity ELSE 0 END) AS sewingout_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.production_quantity ELSE 0 END) AS iron_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.re_production_qty ELSE 0 END) AS re_iron_qnty, 
					sum(CASE WHEN c.production_type ='8' THEN c.production_quantity ELSE 0 END) AS finish_qnty,
					sum(CASE WHEN c.production_type ='8' THEN c.reject_qnty ELSE 0 END) AS finish_rej_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.reject_qnty ELSE 0 END) AS sewingout_rej_qnty,
					sum(c.carton_qty) as carton_qty
								from 
									wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
								where 
									a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $garments_nature group by c.po_break_down_id,c.production_date";*/

					
					$sql_order=sql_select("SELECT c.po_break_down_id, c.production_date,c.item_number_id,
					sum(CASE WHEN c.production_type ='1' THEN c.production_quantity ELSE 0 END) AS cutting_qnty,
					sum(CASE WHEN c.production_type ='2' THEN c.production_quantity ELSE 0 END) AS printing_qnty,
					sum(CASE WHEN c.production_type ='3' THEN c.production_quantity ELSE 0 END) AS printreceived_qnty,
					
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS emb_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS embreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS wash_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS washreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS sp_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS spreceived_qnty,
					
					sum(CASE WHEN c.production_type ='4' THEN c.production_quantity ELSE 0 END) AS sewingin_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.production_quantity ELSE 0 END) AS sewingout_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.production_quantity ELSE 0 END) AS iron_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.re_production_qty ELSE 0 END) AS re_iron_qnty, 
					sum(CASE WHEN c.production_type ='8' THEN c.production_quantity ELSE 0 END) AS finish_qnty,
					sum(CASE WHEN c.production_type ='8' THEN c.reject_qnty ELSE 0 END) AS finish_rej_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.reject_qnty ELSE 0 END) AS sewingout_rej_qnty,
					sum(c.carton_qty) as carton_qty
								from 
									wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
								where 
									a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $garments_nature 
									group by c.po_break_down_id,c.production_date,c.item_number_id");
					foreach($sql_order as $sql_result)
					{
						$cutting_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['1']=$sql_result[csf("cutting_qnty")];
						$printing_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("printing_qnty")];
						$printreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("printreceived_qnty")];
						
						$emb_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("emb_qnty")];
						$embreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("embreceived_qnty")];
						
						$wash_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("wash_qnty")];
						$washreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("washreceived_qnty")];
						
						$sp_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['2']=$sql_result[csf("sp_qnty")];
						$spreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['3']=$sql_result[csf("spreceived_qnty")];
						
						$sewingin_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['4']=$sql_result[csf("sewingin_qnty")];
						$sewingout_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['5']=$sql_result[csf("sewingout_qnty")];
						$sewingout_rej_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['5']=$sql_result[csf("sewingout_rej_qnty")];
						$iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['7']=$sql_result[csf("iron_qnty")];
						$re_iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['7']=$sql_result[csf("re_iron_qnty")];
						$finish_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['8']=$sql_result[csf("finish_qnty")];
						$finish_rej_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['8']=$sql_result[csf("finish_rej_qnty")];
						$carton_qty_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]]['carton_qty']=$sql_result[csf("carton_qty")];
					}
					
					  
                    $total_cut=0;$total_print_iss=0;$total_print_re=0;$total_sew_input=0;$total_sew_out=0;$total_iron=0;$total_re_iron=0;$total_finish=0;$total_carton=0;$total_prod_dzn=0;$total_rej_value=0;$rej_value=0;
                    //$total_cm_value=0;
                    
                    $i=1;
					if($garments_nature!="") $garments_nature=" and c.garments_nature=$garments_nature";
					
					if($db_type==0)
					{
                    	$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.job_no_mst,a.po_number,a.po_quantity,b.order_uom,b.buyer_name,b.style_ref_no as style ,b.company_name as company_name ,c.garments_nature,c.po_break_down_id,c.item_number_id,c.production_source,c.serving_company,c.location,c.embel_name,c.embel_type, c.production_date,c.production_quantity,c.production_type,c.entry_break_down_type,c.production_hour,c.sewing_line,c.supervisor,c.remarks,c.floor_id,c.alter_qnty,c.reject_qnty 
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $garments_nature group by c.po_break_down_id,c.production_date order by c.production_date");
						
					}
					else
					{
						$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.job_no_mst,a.po_number,a.po_quantity,b.order_uom,b.buyer_name,b.style_ref_no as style ,b.company_name as company_name ,c.po_break_down_id,c.item_number_id,c.production_date 
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $garments_nature group by a.job_no_mst, b.job_no_prefix_num,a.po_number,a.po_quantity,b.order_uom,b.buyer_name,b.style_ref_no ,b.company_name ,c.garments_nature,c.po_break_down_id,c.item_number_id,c.production_date");
						
					}
                    //echo $pro_date_sql;die;  
 					foreach($pro_date_sql as $pro_date_sql_row)
                    {
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
                   	 	
						?>
                    	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $i; ?>">
                            <td width="30"><?php echo $i;?></td>
                            <td width="100"><p><?php echo $company_short_library[$pro_date_sql_row[csf("company_name")]]; ?></p></td>
                            <td width="100"><p><?php echo $pro_date_sql_row[csf("job_no_prefix_num")];?></p></td>
                            <td width="130"><p><a href="##" onclick="openmypage_order(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'orderQnty_popup');" ><?php echo $pro_date_sql_row[csf("po_number")]; ?></a></p></td>
                            <td width="100"><p><?php echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></p></td>
                            <td width="130"><p><?php echo $pro_date_sql_row[csf("style")]; ?></p></td>
                            <td width="130"><p><?php echo $garments_item[$pro_date_sql_row[csf("item_number_id")]]; ?></p></td>
                            <td width="100"><p><?php echo change_date_format($pro_date_sql_row[csf("production_date")]); ?></p></td>
                            
                            <td width="80" align="right">
								<?php
									echo $cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['1'];  
									$total_cut+=$cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['1'];
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_print_iss+=$printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_print_re+=$printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            
                            <td width="80" align="right">
								<?php
									echo $emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_emb_iss+=$emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_emb_re+=$embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_wash_iss+=$wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_wash_re+=$washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];  
									$total_sp_iss+=$sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3'];  
									$total_sp_re+=$spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['3']; 
								?>
                            </td>
                            
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'sewingQnty_input_popup');" >
                            	<?php
									echo $sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['4'];  
									$total_sew_input+=$sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['4'];  
								?>
                                </a>
                            </td>
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'sewingQnty_popup');" >
                            	<?php
									echo $sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['5'];  
									$total_sew_out+=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['5'];  
								?>
                            </a>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7'];  
									$total_iron+=$iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7'];  
									$total_re_iron+=$re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['7']; 
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['8'];  
									$total_finish+=$finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['8'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $carton_qty_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['carton_qty'];
									$total_carton+=$carton_qty_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['carton_qty']; 
								?>
                            </td>
                            
                            <?php $prod_dzn=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['5'] / 12 ; $total_prod_dzn+=$prod_dzn; ?>
                            <td width="80" align="right"><?php if($prod_dzn!=0) echo number_format($prod_dzn,2); else echo "0"; ?></td>
 							<?php
                            //$cm_per_dzn=return_field_value("cm_for_sipment_sche","wo_pre_cost_dtls","job_no='".$pro_date_sql_row[csf("job_no_mst")]."' and is_deleted=0 and status_active=1");
                            ?>
                            <td width="80" align="right" ><?php $rej_value=$finish_rej_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['8']+$sewingout_rej_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]]['5']; echo number_format($rej_value,2); $total_rej_value+=$rej_value; ?></td>                    
                            
                            <td width="">
                            	<a href="##"  onclick="openmypage_remark(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,'date_wise_production_report');" > Veiw </a>
                            </td>
                   	 </tr>
						<?php	
					$i++;
				}//end foreach 1st
				?>
                </table> 
                
           </div> 
           	<table width="2280" cellspacing="0" border="1" class="rpt_table" rules="all" id="report_table_footer" >
                    <tfoot>
                            <th width="30" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right">Total</th> 
                            <th width="80" align="right" id="total_cut_td" ><?php echo $total_cut;?></th> 
                            <th width="80" align="right" id="total_printissue_td"><?php echo $total_print_iss; ?> </th> 
                            <th width="80" align="right" id="total_printrcv_td"><?php  echo $total_print_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_emb_iss"><?php echo $total_emb_iss; ?> </th> 
                            <th width="80" align="right" id="total_emb_re"><?php  echo $total_emb_re;  ?>  </th>
                            <th width="80" align="right" id="total_wash_iss"><?php echo $total_wash_iss; ?> </th> 
                            <th width="80" align="right" id="total_wash_re"><?php  echo $total_wash_re;  ?>  </th>
                            <th width="80" align="right" id="total_sp_iss"><?php echo $total_sp_iss; ?> </th> 
                            <th width="80" align="right" id="total_sp_re"><?php  echo $total_sp_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_sewin_td"><?php echo $total_sew_input;  ?> </th> 
                            <th width="80" align="right" id="total_sewout_td"><?php echo $total_sew_out;  ?> </th>
                            <th width="80" align="right" id="total_iron_td"><?php  echo $total_iron; ?> </th>
                            <th width="80" align="right" id="total_re_iron_td"><?php  echo $total_re_iron; ?> </th>  
                            <th width="80" align="right" id="total_finish_td"><?php  echo $total_finish; ?>  </th>   
                            <th width="80" align="right" id="total_carton_td"><?php echo $total_carton; ?> </th> 
                            <th width="80" align="right" id="total_prod_dzn_td"><?php  echo number_format($total_prod_dzn,2); ?> </th>
                            <th width="80" align="right" id="total_cm_value_td"><?php echo number_format($total_rej_value,2); ?> </th >
                            <th width="">&nbsp;</th>
                       </tfoot>
                </table>
           </div>    
			<?php
		}// end if condition of type
		//-------------------------------------------END Show Date Wise------------------------
		//-------------------------------------------Show Date Location Floor & Line Wise------------------------	
		if($type==2)
		{
			ob_start();
		?>
        	<div style="max-height:425px; width:2120px; overflow-y: scroll;" id="scroll_body">
                <table width="2100px"  cellspacing="0"   >
                    <tr class="form_caption" style="border:none;">
                            <td colspan="23" align="center" style="border:none;font-size:14px; font-weight:bold"> Date Location Floor & Line Wise Production Report</td>
                     </tr>
                    <tr style="border:none;">
                            <td colspan="23" align="center" style="border:none; font-size:16px; font-weight:bold">
                                Company Name:<?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?>                                
                            </td>
                      </tr>
                      <tr style="border:none;">
                            <td colspan="23" align="center" style="border:none;font-size:12px; font-weight:bold">
                            	<?php echo "From $fromDate To $toDate" ;?>
                            </td>
                      </tr>
                </table>
                <table width="2600" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                   <thead>
                        <th width="30">Sl.</th>    
                        <th width="100">Working Factory</th>
                        <th width="100">Job No</th>
                        <th width="130">Order Number</th>
                        <th width="100">Buyer Name</th>
                        <th width="130">Style Name</th>
                        <th width="130">Item Name</th>
                        <th width="100">Production Date</th>
                        <th width="100">Status</th>
                        <th width="100">Location</th>
                        <th width="100">Floor</th>
                        <th width="100">Sewing Line No</th>
                        <th width="80">Cutting</th>
                        <th width="80">Sent to prnt</th>
                        <th width="80">Rev prn/Emb</th>
                        
                        <th width="80">Sent to Emb</th>
                        <th width="80">Rev Emb</th>
                        <th width="80">Sent to Wash</th>
                        <th width="80">Rev Wash</th>
                        <th width="80">Sent to Sp. Works</th>
                        <th width="80">Rev Sp. Works</th>
                        
                        <th width="80">Sewing Input</th>
                        <th width="80">Sewing Output</th>
                        <th width="80">Iron Qty </th>
                        <th width="80">Re-Iron Qty </th>
                        <th width="80">Finish Qty </th>
                        <th width="80">Today Carton</th>
                        <th width="80">Prod/Dzn</th>
                      	<th width="">Remarks</th>
                    </thead>
                </table>
                <div style="max-height:425px;  width:2600px" id="scroll_body2">
                    <table cellspacing="0" border="1" class="rpt_table"  width="2582" rules="all" id="table_body" >
                    <?php
					
					$cutting_array=array();
					$printing_array=array();
					$printreceived_array=array();
					
					$emb_array=array();
					$embreceived_array=array();
					$wash_array=array();
					$washreceived_array=array();
					$sp_array=array();
					$washreceived_array=array();
					
					$sewingin_array=array();
					$sewingout_array=array();
					$iron_array=array();
					$re_iron_array=array();
					$finish_array=array();
					$carton_array=array();
					
					$sql_order=sql_select("SELECT c.po_break_down_id,c.production_date, c.item_number_id,c.location,c.floor_id,c.sewing_line,
					sum(CASE WHEN c.production_type ='1' THEN c.production_quantity ELSE 0 END) AS cutting_qnty,
					sum(CASE WHEN c.production_type ='2' THEN c.production_quantity ELSE 0 END) AS printing_qnty,
					sum(CASE WHEN c.production_type ='3' THEN c.production_quantity ELSE 0 END) AS printreceived_qnty,
					
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS emb_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=2 THEN c.production_quantity ELSE 0 END) AS embreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS wash_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=3 THEN c.production_quantity ELSE 0 END) AS washreceived_qnty,
					sum(CASE WHEN c.production_type ='2' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS sp_qnty,
					sum(CASE WHEN c.production_type ='3' AND c.embel_name=4 THEN c.production_quantity ELSE 0 END) AS spreceived_qnty,
					
					sum(CASE WHEN c.production_type ='4' THEN c.production_quantity ELSE 0 END) AS sewingin_qnty,
					sum(CASE WHEN c.production_type ='5' THEN c.production_quantity ELSE 0 END) AS sewingout_qnty,
					sum(CASE WHEN c.production_type ='7' THEN c.production_quantity ELSE 0 END) AS iron_qnty, 
					sum(CASE WHEN c.production_type ='7' THEN c.re_production_qty ELSE 0 END) AS re_iron_qnty, 
					sum(CASE WHEN c.production_type ='8' THEN c.production_quantity ELSE 0 END) AS finish_qnty, 
					sum(c.carton_qty) as carton_qty
								from 
									wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
								where 
									a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $floor_name $garments_nature 
									group by c.po_break_down_id,c.production_date,c.item_number_id,c.location,c.floor_id,c.sewing_line");
					foreach($sql_order as $sql_result)
					{
						$cutting_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['1']=$sql_result[csf("cutting_qnty")];
						$printing_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("printing_qnty")];
						$printreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("printreceived_qnty")];
						
						$emb_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("emb_qnty")];
						$embreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("embreceived_qnty")];
						
						$wash_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("wash_qnty")];
						$washreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("washreceived_qnty")];
						
						$sp_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['2']=$sql_result[csf("sp_qnty")];
						$spreceived_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['3']=$sql_result[csf("spreceived_qnty")];
						
						$sewingin_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['4']=$sql_result[csf("sewingin_qnty")];
						$sewingout_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['5']=$sql_result[csf("sewingout_qnty")];
						$iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['7']=$sql_result[csf("iron_qnty")];
						$re_iron_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['7']=$sql_result[csf("re_iron_qnty")];
						$finish_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]['8']=$sql_result[csf("finish_qnty")];
						$carton_array[$sql_result[csf("po_break_down_id")]][$sql_result[csf("production_date")]][$sql_result[csf("item_number_id")]][$sql_result[csf("location")]][$sql_result[csf("floor_id")]][$sql_result[csf("sewing_line")]]=$sql_result[csf("carton_qty")];
						
					}
					
                    $total_cut=0;
                    $total_print_iss=0;
                    $total_print_re=0;
                    $total_sew_input=0;
                    $total_sew_out=0;
					$total_iron=0;
					$total_re_iron=0;
                    $total_finish=0;
                    $total_carton=0;
                    $total_prod_dzn=0;
                    //$total_cm_value=0;
                    
                    $i=1;
					
					
					if($garments_nature!="") $garments_nature=" and c.garments_nature=$garments_nature";
					
					
					if($db_type==0)
					{
                    	$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.job_no_mst, a.po_number, a.po_quantity, b.order_uom, b.buyer_name, b.style_ref_no as style, b.company_name as company_name, c.garments_nature, c.po_break_down_id, c.item_number_id, c.production_source, c.serving_company, c.location, c.embel_name, c.embel_type, c.production_date, c.production_quantity, c.production_type, c.entry_break_down_type, c.production_hour, c.sewing_line, c.supervisor, c.carton_qty, c.remarks, c.floor_id, c.alter_qnty, c.reject_qnty, c.prod_reso_allo
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $garments_nature group by c.po_break_down_id,c.production_date,c.location,c.floor_id,c.sewing_line order by c.production_date");
					}
					else
					{
						$pro_date_sql=sql_select("SELECT b.job_no_prefix_num, a.po_number, b.buyer_name, b.style_ref_no as style, b.company_name as company_name, c.garments_nature, c.po_break_down_id, c.item_number_id, c.production_source, c.location, c.production_date, c.sewing_line, c.carton_qty, c.floor_id, c.prod_reso_allo
					from 
						wo_po_break_down a,wo_po_details_master b, pro_garments_production_mst c 
					where 
						a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $garments_nature group by b.job_no_prefix_num, a.po_number, b.buyer_name, b.style_ref_no, b.company_name, c.garments_nature, c.po_break_down_id, c.item_number_id, c.production_source, c.location, c.production_date, c.sewing_line, c.carton_qty, c.floor_id, c.prod_reso_allo");
					}
                    //echo $pro_date_sql;die;
 					foreach($pro_date_sql as $pro_date_sql_row)
                    {
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
                   	 	
						$sewing_line='';
						if($pro_date_sql_row[csf('prod_reso_allo')]==1)
						{
							$line_number=explode(",",$prod_reso_arr[$pro_date_sql_row[csf('sewing_line')]]);
							foreach($line_number as $val)
							{
								if($sewing_line=='') $sewing_line=$line_library[$val]; else $sewing_line.=", ".$line_library[$val];
							}
						}
						else $sewing_line=$line_library[$pro_date_sql_row[csf('sewing_line')]];  
						
                    ?>
                    	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $i; ?>">
                            <td width="30"><?php echo $i;?></td>
                            <td width="100"><p><?php echo $company_short_library[$pro_date_sql_row[csf("company_name")]]; ?></p></td>
                            <td width="100"><p><?php echo $pro_date_sql_row[csf("job_no_prefix_num")];?></p></td>
                            <td width="130"><p><a href="##" onclick="openmypage_order(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,<?php echo $pro_date_sql_row[csf("item_number_id")]; ?>,'orderQnty_popup');" ><?php echo $pro_date_sql_row[csf("po_number")]; ?></a></p></td>
                            <td width="100"><p><?php echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></p></td>
                            <td width="130"><p><?php echo $pro_date_sql_row[csf("style")]; ?></p></td>
                            <td width="130"><p><?php echo $garments_item[$pro_date_sql_row[csf("item_number_id")]]; ?></p></td>
                            <td width="100"><p><?php echo change_date_format($pro_date_sql_row[csf("production_date")]); ?></p></td>
                            <td width="100"><p><?php echo $knitting_source[$pro_date_sql_row[csf("production_source")]]; ?></p></td>
                            
                            <td width="100"><p><?php echo $location_library[$pro_date_sql_row[csf("location")]]; ?></p></td>
                            <td width="100"><p><?php echo $floor_library[$pro_date_sql_row[csf("floor_id")]]; ?></p></td>
                            <td width="100" align="center"><p><?php echo $sewing_line; ?></p></td>
                            
                            <td width="80" align="right">
                            	<?php
									echo $cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['1'];  
									$total_cut+=$cutting_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['1'];
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_print_iss+=$printing_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_print_re+=$printreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
								?>
                            </td>
                            
                            <td width="80" align="right">
								<?php
									echo $emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_emb_iss+=$emb_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_emb_re+=$embreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_wash_iss+=$wash_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_wash_re+=$washreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3']; 
								?>
                            </td>
                            <td width="80" align="right">
								<?php
									echo $sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];  
									$total_sp_iss+=$sp_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['2'];
								?>
							</td>
                            <td width="80" align="right">
                            	<?php
									echo $spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3'];  
									$total_sp_re+=$spreceived_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['3']; 
								?>
                            </td>
                            
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")];?>,'sewingQnty_input_popup');" >
                            	<?php
									echo $sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['4'];  
									$total_sew_input+=$sewingin_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['4'];  
								?>
                                </a>
                            </td>
                            <td width="80" align="right"><a href="##" onclick="openmypage_sew_output(<?php echo $pro_date_sql_row[csf("po_break_down_id")]; ?>,'<?php echo $pro_date_sql_row[csf("production_date")]; ?>',<?php echo $pro_date_sql_row[csf("item_number_id")]; ?>,'sewingQnty_popup');" >
                            	<?php
									echo $sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['5'];  
									$total_sew_out+=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['5'];  
								?>
                            </a></td>
                            <td width="80" align="right">
                            	<?php
									echo $iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
									$total_iron+=$iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
									$total_re_iron+=$re_iron_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['7'];  
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['8'];  
									$total_finish+=$finish_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['8']; 
								?>
                            </td>
                            <td width="80" align="right">
                            	<?php
									echo $carton_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]];  
									$total_carton+=$carton_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]; 
								?>
                            </td>
                            <?php $prod_dzn=$sewingout_array[$pro_date_sql_row[csf("po_break_down_id")]][$pro_date_sql_row[csf("production_date")]][$pro_date_sql_row[csf("item_number_id")]][$pro_date_sql_row[csf("location")]][$pro_date_sql_row[csf("floor_id")]][$pro_date_sql_row[csf("sewing_line")]]['5'] / 12 ; $total_prod_dzn+=$prod_dzn; ?>
                            
                            <td width="80" align="right"><?php if($prod_dzn!=0) echo number_format($prod_dzn,2); else echo "0"; ?></td>
                            <td width=""><a href="##"  onclick="openmypage_remark(<?php echo $pro_date_sql_row[csf("po_break_down_id")];?>,'date_wise_production_report');" > Veiw </a>
                            </td>
                   	 </tr>
						<?php
						
					$i++;
				}//end foreach 1st
				
				?>
                </table>
                <table width="2600" cellspacing="0" border="1" class="rpt_table" rules="all" id="report_table_footer" >
                    <tfoot>
                            <th width="30" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="130" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th>
                            <th width="100" align="right"></th> 
                            <th width="100" align="right">Total</th>
                            <th width="80" align="right" id="total_cut_td"><?php echo $total_cut;?></th> 
                            <th width="80" align="right" id="total_printissue_td"><?php echo $total_print_iss; ?> </th> 
                            <th width="80" align="right" id="total_printrcv_td"><?php  echo $total_print_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_emb_iss"><?php echo $total_emb_iss; ?> </th> 
                            <th width="80" align="right" id="total_emb_re"><?php  echo $total_emb_re;  ?>  </th>
                            <th width="80" align="right" id="total_wash_iss"><?php echo $total_wash_iss; ?> </th> 
                            <th width="80" align="right" id="total_wash_re"><?php  echo $total_wash_re;  ?>  </th>
                            <th width="80" align="right" id="total_sp_iss"><?php echo $total_sp_iss; ?> </th> 
                            <th width="80" align="right" id="total_sp_re"><?php  echo $total_sp_re;  ?>  </th>
                            
                            <th width="80" align="right" id="total_sewin_td"><?php echo $total_sew_input;  ?> </th> 
                            <th width="80" align="right" id="total_sewout_td"><?php echo $total_sew_out;  ?> </th>
                            <th width="80" align="right" id="total_iron_td"><?php  echo $total_iron; ?>  </th> 
                            <th width="80" align="right" id="total_re_iron_td"><?php  echo $total_re_iron; ?>  </th> 
                            <th width="80" align="right" id="total_finish_td"><?php  echo $total_finish; ?>  </th>   
                            <th width="80" align="right" id="total_carton_td"><?php echo $total_carton; ?> </th> 
                            <th width="80" align="right" id="total_prod_dzn_td"><?php  echo number_format($total_prod_dzn,2); ?> </th>
                            <th width="" >&nbsp;</th>
                     </tfoot>
                </table>
              </div>
          </div>
		<?php
		}// end if condition of type
		?>
            <br />
            <div align="left" style="width:200px"><strong>Production-Subcontract Order </strong></div>
            <div>
                <table width="1400" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                    <thead>
                        <th width="30">Sl.</th>    
                        <th width="100">Working Factory</th>
                        <th width="100">Job No</th>
                        <th width="130">Order No</th>
                        <th width="100">Buyer </th>
                        <th width="130">Style </th>
                        <th width="130">Item Name</th>
                        <th width="75">Production Date</th>
                        <th width="100">Sewing Line</th>
                        <th width="90">Cutting</th>
                        <th width="90">Sewing Output</th>
                        <th width="90">Iron Output</th>
                        <th width="90">Gmts. Finishing</th>
                        <th width="">Remarks</th>
                    </thead>
                </table>
            <div style="max-height:300px; overflow-y:scroll; width:1420px" id="scroll_body">
                <table border="1" class="rpt_table"  width="1400" rules="all" id="sub_list_view" >
                      <?php 
                        //sql_select
                       // if($type==1) //--------------------------------------------Show Order Wise  $type==1 
                       // {
                        $item_id_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');
						
						$production_array=array();
						if($db_type==0)
						{
							$prod_sql= "SELECT c.order_id, c.production_date,
								sum(CASE WHEN c.production_type ='1' THEN  c.production_qnty  ELSE 0 END) AS cutting_qnty,
								sum(CASE WHEN c.production_type ='2' THEN  c.production_qnty  ELSE 0 END) AS sewingout_qnty,
								sum(CASE WHEN c.production_type ='3' THEN  c.production_qnty  ELSE 0 END) AS ironout_qnty,
								sum(CASE WHEN c.production_type ='4' THEN  c.production_qnty  ELSE 0 END) AS gmts_fin_qnty
							from 
								subcon_gmts_prod_dtls c
							where c.status_active=1 and c.is_deleted=0 and c.production_date between $txt_date_from and $txt_date_to $location_mst $floor_mst group by c.order_id, c.production_date";
						}
						else
						{
							$prod_sql= "SELECT c.order_id, c.production_date,
								NVL(sum(CASE WHEN c.production_type ='1' THEN  c.production_qnty  ELSE 0 END),0) AS cutting_qnty,
								NVL(sum(CASE WHEN c.production_type ='2' THEN  c.production_qnty  ELSE 0 END),0) AS sewingout_qnty,
								NVL(sum(CASE WHEN c.production_type ='3' THEN  c.production_qnty  ELSE 0 END),0) AS ironout_qnty,
								NVL(sum(CASE WHEN c.production_type ='4' THEN  c.production_qnty  ELSE 0 END),0) AS gmts_fin_qnty
							from 
								subcon_gmts_prod_dtls c
							where c.status_active=1 and c.is_deleted=0 and c.production_date between $txt_date_from and $txt_date_to $location_mst $floor_mst group by c.order_id, c.production_date";
						}
						$prod_sql_result= sql_select($prod_sql);
						//echo $prod_sql;//die;
						foreach($prod_sql_result as $proRes)
						{
							$production_array[$proRes[csf("order_id")]][$proRes[csf("production_date")]]['cutting_qnty']=$proRes[csf("cutting_qnty")];
							$production_array[$proRes[csf("order_id")]][$proRes[csf("production_date")]]['sewingout_qnty']=$proRes[csf("sewingout_qnty")];
							$production_array[$proRes[csf("order_id")]][$proRes[csf("production_date")]]['ironout_qnty']=$proRes[csf("ironout_qnty")];
							$production_array[$proRes[csf("order_id")]][$proRes[csf("production_date")]]['gmts_fin_qnty']=$proRes[csf("gmts_fin_qnty")];
						}
						
						if($db_type==0)
						{	
                        	$order_sql= "select c.id, c.order_no, c.cust_style_ref, a.company_id, a.party_id, a.location_id, a.job_no_prefix_num, b.gmts_item_id, b.line_id, sum(CASE WHEN b.order_id=c.id THEN b.production_qnty ELSE 0 END) AS production_qnty, b.production_date, b.prod_reso_allo
                        from 
                            subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
                        where 
                             b.order_id=c.id and b.production_date between $txt_date_from and $txt_date_to and a.company_id=$cbo_company_name and a.subcon_job=c.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by b.order_id, b.production_date order by b.production_date";
						}
						else
						{
							$order_sql= "select c.id, c.order_no, c.cust_style_ref, a.company_id, a.party_id, a.location_id, a.job_no_prefix_num, b.gmts_item_id, b.line_id, sum(CASE WHEN b.order_id=c.id THEN b.production_qnty ELSE 0 END) AS production_qnty,  b.production_date, b.prod_reso_allo
                        from 
                            subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
                        where 
                             b.order_id=c.id and b.production_date between $txt_date_from and $txt_date_to  and a.company_id=$cbo_company_name 
and a.subcon_job=c.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by b.order_id, c.id, c.order_no, c.cust_style_ref, a.company_id, a.job_no_prefix_num, a.party_id, a.company_id, a.party_id, a.location_id, b.gmts_item_id, b.line_id, b.production_date, b.prod_reso_allo order by c.id";
						}

                   // echo $order_sql;//die;
                       
						$order_sql_result=sql_select($order_sql);
                           $j=0;$k=0;
                           foreach($order_sql_result as $orderRes)
                           {
                               $j++;
                               if ($j%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
							   
                                $sewing_line='';
								if($pro_date_sql_row[csf('prod_reso_allo')]==1)
								{
									$line_number=explode(",",$prod_reso_arr[$orderRes[csf("line_id")]]);
									foreach($line_number as $val)
									{
										if($sewing_line=='') $sewing_line=$line_library[$val]; else $sewing_line.=", ".$line_library[$val];
									}
								}
								else $sewing_line=$line_library[$orderRes[csf("line_id")]]; 
								
                                ?>
                                <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_3nd<?php echo $j; ?>','<?php echo $bgcolor; ?>')" id="tr_3nd<?php echo $j; ?>" style="height:20px">
                                    <td width="30" ><?php echo $j; ?></td>    
                                    <td width="100"><p><?php echo $company_short_library[$orderRes[csf("company_id")]]; ?></p></td>
                                    <td width="100" align="center"><p><?php echo $orderRes[csf("job_no_prefix_num")]; ?></p></td>
                                    <td width="130"><p><?php echo $orderRes[csf("order_no")]; ?></p></td>
                                    <td width="100"><?php echo $buyer_short_library[$orderRes[csf("party_id")]]; ?></td>
                                    <td width="130"><p><?php echo $orderRes[csf("cust_style_ref")]; ?></p></td>
                                    <td width="130"><p><?php echo $garments_item[$orderRes[csf("gmts_item_id")]];?></p></td>
                                    <td width="75" bgcolor="<?php echo $color; ?>"><?php echo change_date_format($orderRes[csf("production_date")]);  ?></td>
                                    <td width="100" align="center"><?php echo $sewing_line; ?></td>
                                    <td width="90" align="right"><?php echo $production_array[$orderRes[csf("id")]][$orderRes[csf("production_date")]]['cutting_qnty']; $total_cutt+=$production_array[$orderRes[csf("id")]][$orderRes[csf("production_date")]]['cutting_qnty']; ?></td>
                                    <td width="90" align="right"><?php echo $production_array[$orderRes[csf("id")]][$orderRes[csf("production_date")]]['sewingout_qnty']; $total_sew+=$production_array[$orderRes[csf("id")]]['sewingout_qnty']; ?></td>
                                    <td width="90" align="right"><?php echo $production_array[$orderRes[csf("id")]][$orderRes[csf("production_date")]]['ironout_qnty']; $total_iron_sub+=$production_array[$orderRes[csf("id")]]['ironout_qnty']; ?></td>
                                    <td width="90" align="right"><?php echo $production_array[$orderRes[csf("id")]][$orderRes[csf("production_date")]]['gmts_fin_qnty']; $total_gmtfin+=$production_array[$orderRes[csf("id")]]['gmts_fin_qnty']; ?></td>

                                    <td width="">&nbsp;</td>
                                 </tr>
                            <?php
                           }
                          ?>  
                        </table>	
                        <table border="1" class="tbl_bottom"  width="1400" rules="all" id="report_table_footer_1" >
                            <tr>
                                <td width="30"></td>
                                <td width="100"></td>
                                <td width="100"></td>
                                <td width="130"></td>
                                <td width="100"></td>
                                <td width="130">Total</td>
                                <td width="130" id="total_ord_quantity"><?php echo $total_ord_quantity; ?></td>
                                <td width="75"></td>
                                <td width="100"></td>
                                <td width="90" id="total_cutt"><?php echo $total_cutt; ?></td>
                                <td width="90" id="total_sew"><?php echo $total_sew; ?></td>
                                <td width="90" id="total_iron_sub"><?php echo $total_iron_sub; ?></td>
                                <td width="90" id="total_gmtfin"><?php echo $total_gmtfin; ?></td>
                                <td width=""></td>
                             </tr>
                     </table>
                    </div>
			  </div>		
		<?php
	}
		//-------------------------------------------END Show Date Location Floor & Line Wise------------------------
		//---------end------------//
	$html = ob_get_contents();
	ob_clean();
	$new_link=create_delete_report_file( $html, 1, 1, "../../../" );
	echo "$html";
	exit();	
}

if($action=="orderQnty_popup")
{
	echo load_html_head_contents("Date Wise Production Report", "../../../", 1, 1,$unicode,'','');
 	extract($_REQUEST);
	
	$sql= "SELECT b.id,b.po_number,b.pub_shipment_date,b.po_quantity*(select from wo_po_details_mas_set_details set where set.job_no=a.job_no and set.gmts_item_id=$gmts_item_id) as po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id='$po_break_down_id' and a.garments_nature='$garments_nature' and a.is_deleted=0 and a.status_active=1";
	//echo $sql;
	echo "<br />". create_list_view ( "list_view", "Order No,Order Qnty,Pub Shipment Date", "200,120,220","540","220",1, "SELECT b.id,b.po_number,b.pub_shipment_date,b.po_quantity*a.total_set_qnty as po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id='$po_break_down_id' and a.is_deleted=0 and a.status_active=1", "", "","", 1, '0,0,0', $arr, "po_number,po_quantity,pub_shipment_date","../requires/date_wise_prod_without_cm_report_controller", '','0,1,3');
exit();
}

if($action=='date_wise_production_report') 
{	
	extract($_REQUEST); 
 	echo load_html_head_contents("Remarks", "../../../", 1, 1,$unicode,'','');
 ?>
	<fieldset>
    <legend>Cutting</legend>
    	<?php 
			 $sql= "SELECT id,production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and production_type='1' and is_deleted=0 and status_active=1";
 			 //echo $sql;
			 echo  create_list_view ( "list_view_1", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
			
 		?>
    </fieldset>
    <fieldset>
    <legend>Print/Embr Issue</legend>
    	<?php 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id'  and production_type='2' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_2", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    <fieldset>
    <legend>Print/Embr Receive</legend>
    	<?php 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id'  and production_type='3' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_3", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    <fieldset>
    <legend>Sewing Input</legend>
    	<?php 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id'  and production_type='4' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_4", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    <fieldset>
    <legend>Sewing Output</legend>
    	<?php 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id'  and production_type='5' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_5", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    <fieldset>
    <legend>Finish Input</legend>
    	<?php 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id'  and production_type='6' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_6", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    <fieldset>
    <legend>Finish Output</legend>
    	<?php 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id'  and production_type='8' and is_deleted=0 and status_active=1";
			 
			  echo  create_list_view ( "list_view_7", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
<?php
}//end if 




if($action=="sewingQnty_popup")
{
	echo load_html_head_contents("Date Wise Production Report", "../../../", 1, 1,$unicode,'','');
 	extract($_REQUEST);
	//echo $po_break_down_id;die;
	$sizearr=return_library_array("select id,size_name from lib_size ","id","size_name");
	$colorarr=return_library_array("select id,color_name from  lib_color ","id","color_name");
	$country_library=return_library_array( "select id,country_name from  lib_country", "id", "country_name"  ); 
	$order_library=return_library_array( "select id,po_number from  wo_po_break_down", "id", "po_number"  ); 
	$buyer_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$floor_library=return_library_array( "select id,floor_name from  lib_prod_floor", "id", "floor_name"  );
	$sewing_line_library=return_library_array( "select id,line_name from  lib_sewing_line", "id", "line_name"  );
	$prod_reso_arr=return_library_array( "select id, line_number from prod_resource_mst",'id','line_number');
	
	$sizearr_order=return_library_array("select size_number_id,size_number_id from wo_po_color_size_breakdown where po_break_down_id=$po_break_down_id","size_number_id","size_number_id");
	$buyer_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	//var_dump();die;
	
	$po_details_sql=sql_select("select a.job_no, a.buyer_name, a.style_ref_no, a.gmts_item_id from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.id=$po_break_down_id group by a.job_no, a.buyer_name, a.style_ref_no, a.gmts_item_id");
	//echo $po_details_sql;die;
	
	if($db_type==2)
	{
		$sql= "SELECT  a.challan_no, a.floor_id, a.sewing_line, a.country_id, a.production_source, c.color_number_id, LISTAGG(CAST(c.size_number_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.size_number_id) as size_number_id, a.prod_reso_allo  
		from pro_garments_production_mst a,  pro_garments_production_dtls b,  wo_po_color_size_breakdown c 
		where a.production_type=5 and a.item_number_id=$gmts_item_id and a.production_date='$production_date' and a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.po_break_down_id='$po_break_down_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 
		group by  a.country_id,a.challan_no, a.floor_id, a.sewing_line, a.production_source, a.prod_reso_allo,c.color_number_id
		order by a.country_id,a.challan_no, a.floor_id, a.sewing_line";
	}
	else if($db_type==0)
	{
		$sql= "SELECT a.challan_no,a.floor_id, a.sewing_line,a.country_id,a.production_source, c.color_number_id, group_concat(c.size_number_id) as size_number_id , a.prod_reso_allo  
		from pro_garments_production_mst a,  pro_garments_production_dtls b,  wo_po_color_size_breakdown c 
		where a.production_type=5 and a.item_number_id=$gmts_item_id and a.production_date='$production_date' and a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.po_break_down_id='$po_break_down_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0
		group by  a.country_id,a.challan_no, a.floor_id, a.sewing_line, a.production_source,a.prod_reso_allo,c.color_number_id
		order by a.country_id,a.challan_no, a.floor_id, a.sewing_line";
	}
	//echo $sql;die;
	
	
	//echo $sql; //and a.production_date='$production_date'
	$result=sql_select($sql);
	
	$sql_color_size= sql_select("SELECT a.challan_no,a.floor_id, a.sewing_line,a.country_id,a.production_source, sum(b.production_qnty) as production_qnty, c.color_number_id, c.size_number_id 
	from pro_garments_production_mst a,  pro_garments_production_dtls b,  wo_po_color_size_breakdown c 
	where a.production_type=5 and a.item_number_id=$gmts_item_id and a.production_date='$production_date' and a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.po_break_down_id='$po_break_down_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 
	group by   a.country_id,a.challan_no, a.floor_id, a.sewing_line, a.production_source,c.color_number_id,c.size_number_id");
	foreach($sql_color_size as $row)
	{
		$production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]] +=$row[csf('production_qnty')];
		$summery_data[$row[csf('color_number_id')]][$row[csf('size_number_id')]] +=$row[csf('production_qnty')];
	}
	
	$col_width=60*count($sizearr_order);
	$table_width=630+$col_width;
	$summer_table_width=230+$col_width;


?>	
<script>

	function print_window()
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
		d.close();
	}
	
	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<fieldset style="width:<?php echo $table_width; ?>  margin-left:10px" >
		<input type="button" value="Print" onClick="print_window()" style="width:100px; margin-left:400px;"  class="formbutton" /><br />
    <div style="100%" id="report_container">
    
        <table cellpadding="0" cellspacing="0" border="0" rules="all" width="<?php echo $table_width; ?>" style="margin-bottom:10px; margin-top:10px;">
        	<tr>
            	<td style="font-size:14px; font-weight:bold;">
                Buyer Name : <?php echo $buyer_library[$po_details_sql[0][csf("buyer_name")]]; ?>&nbsp;&nbsp;&nbsp;&nbsp; Job No : <?php echo $po_details_sql[0][csf("job_no")]; ?>&nbsp;&nbsp;&nbsp;&nbsp; Style No : <?php echo $po_details_sql[0][csf("style_ref_no")]; ?>&nbsp;&nbsp;&nbsp;&nbsp; Garments Item : 
				<?php
                    $item_data=""; 
                    $garments_item_arr=array_unique(explode(",",$po_details_sql[0][csf("gmts_item_id")])); 
                    foreach($garments_item_arr as $item_id)
                    {
                        if($item_data!="") $item_data .=", ";
                        $item_data .=$garments_item[$item_id];
                    }
                    echo $item_data;
                ?>
                <br />
                Order No : <?php echo $order_library[$po_break_down_id]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Date : <?php echo change_date_format($production_date); ?> 
                <br />
                Summary
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="<?php echo $summer_table_width; ?>" style="margin-bottom:20px;">
            <thead>
                <tr>
                	<th width="40" rowspan="2">SI</th>
                    <th width="100" rowspan="2">Color</th>
                	<th width="<?php echo $col_width; ?>" colspan="<?php echo count($sizearr_order); ?>">Size</th>
                    <th width="80" rowspan="2" >Total</th>
                </tr>
                <tr>
                <?php
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th width="60"><?php echo $sizearr[$size_id]; ?></th>
                    <?php
				}
				?>
                </tr>
                
            </thead>
            <tbody>
            <?php
			$i=1;
			//var_dump($result);die;
			foreach($summery_data as $color_id=>$row)
			{
				//print_r($row);die;
				
				?>
                <tr>
                    <td align="center"><?php echo $i;  ?></td>
                    <td ><?php echo $colorarr[$color_id];  ?></td>
                    <?php
					$summry_color_total_in =0;
                    foreach($sizearr_order as $size_id)
                    {
                        ?>
                        <td align="right"><?php echo number_format($row[$size_id],0) ; $summry_color_total_in+= $row[$size_id]; $summry_color_size_in [$size_id]+=$row[$size_id];?></td>
                        <?php
                    }
                    ?>
                    <td align="right"><?php echo  number_format( $summry_color_total_in,0); $grand_tot_in+=$summry_color_total_in; ?></td>
                </tr>
                <?php
				$i++;
			}
			?>
            </tbody>
            <tfoot>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <?php
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th ><?php echo $summry_color_size_in[$size_id]; ?></th>
                    <?php
				}
				?>
                <th ><?php echo $grand_tot_in; ?></th>
                
            </tfoot>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" rules="all" width="<?php echo $table_width; ?>" style="margin-bottom:10px;">
        	<tr>
            	<td style="font-size:14px; font-weight:bold;">Details</td>
            </tr>
        </table>
        
        <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="<?php echo $table_width; ?>">
            <thead>
                <tr>
                	<th width="40" rowspan="2">SI</th>
                    <th width="100" rowspan="2">Country Name</th>
                    <th width="80" rowspan="2">Source</th>
                    <th width="70" rowspan="2">Challan</th>
                    <th width="90" rowspan="2">Sewing Unit</th>
                    <th width="70" rowspan="2">Sewing Line</th>
                    <th width="100" rowspan="2">Color</th>
                	<th width="<?php echo $col_width; ?>" colspan="<?php echo count($sizearr_order); ?>">Size</th>
                    <th width="80" rowspan="2" >Total</th>
                </tr>
                <tr>
                <?php
				$grand_tot_in=0;
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th width="60"><?php echo $sizearr[$size_id]; ?></th>
                    <?php
				}
				?>
                </tr>
                
            </thead>
            <tbody>
            <?php
			$i=1;
			$k=1;
			//var_dump($result);die;
			foreach($result as $row)
			{
				if(!in_array($row[csf("sewing_line")],$temp_arr[$row[csf("country_id")]]))
				{
					$temp_arr[$row[csf("country_id")]][]=$row[csf("sewing_line")];
					if($k!=1)
					{
						?>
						<tr bgcolor="#CCCCCC">
							<td >&nbsp;</td>
							<td>&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<?php
							foreach($sizearr_order as $size_id)
							{
								?>
								<td align="right"><?php echo number_format($line_color_size_in [$size_id],0); ?></td>
								<?php
							}
							?>
							<td align="right"><?php echo number_format($line_color_total_in,0); ?></td>
						</tr>
						<?php
						$line_color_size_in = $line_color_total_in ="";
					}
					$k++;
				}
				
				$sewing_line='';
				if($row[csf('prod_reso_allo')]==1)
				{
					$line_number=explode(",",$prod_reso_arr[$row[csf('sewing_line')]]);
					foreach($line_number as $val)
					{
						if($sewing_line=='') $sewing_line=$sewing_line_library[$val]; else $sewing_line.=",".$sewing_line_library[$val];
					}
				}
				else $sewing_line=$sewing_line_library[$row[csf('sewing_line')]];
				
				?>
                <tr>
                    <td align="center"><?php echo $i;  ?></td>
                    <td ><p><?php echo $country_library[$row[csf("country_id")]];  ?></p></td>
                    <td ><p><?php echo $knitting_source[$row[csf("production_source")]]; ?><p></td>
                    <td ><p><?php echo $row[csf("challan_no")];  ?></p></td>
                    <td ><p><?php echo $floor_library[$row[csf("floor_id")]];  ?></p></td>
                    <td align="center"><p><?php echo $sewing_line;  ?></p></td>
                    <td ><p><?php echo $colorarr[$row[csf("color_number_id")]];  ?></p></td>
                    <?php
					$color_total_in=0;
                    foreach($sizearr_order as $size_id)
                    {
                        ?>
                        <td align="right"><p>
						<?php
						 	echo number_format($production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id],0) ;
							 $color_total_in+= $production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id]; 
							 $color_size_in [$size_id]+=$production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id];
							 $line_color_total_in+= $production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id]; 
							 $line_color_size_in [$size_id]+=$production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id];
						 ?>
                        </p></td>
                        <?php
                    }
                    ?>
                    <td align="right"><p><?php echo  number_format( $color_total_in,0); $grand_tot_in+=$color_total_in; ?></p></td>
                </tr>
                <?php
				$i++;
			}
			?>
            <tr bgcolor="#CCCCCC">
                <td >&nbsp;</td>
                <td>&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <?php
                foreach($sizearr_order as $size_id)
                {
                    ?>
                    <td align="right"><?php echo number_format($line_color_size_in [$size_id],0); ?></td>
                    <?php
                }
                ?>
                <td align="right"><?php echo number_format($line_color_total_in,0); ?></td>
            </tr>
            </tbody>
            <tfoot>
                <th >&nbsp;</th>
                <th>&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <?php
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th ><?php echo $color_size_in[$size_id]; ?></th>
                    <?php
				}
				?>
                <th ><?php echo $grand_tot_in; ?></th>
                
            </tfoot>
        </table>
        </div>
    </fieldset>
<?php	
}

if($action=="sewingQnty_input_popup")
{
	echo load_html_head_contents("Date Wise Production Report", "../../../", 1, 1,$unicode,'','');
 	extract($_REQUEST);
	//echo $po_break_down_id;die;
	$sizearr=return_library_array("select id,size_name from lib_size ","id","size_name");
	$colorarr=return_library_array("select id,color_name from  lib_color ","id","color_name");
	$country_library=return_library_array( "select id,country_name from  lib_country", "id", "country_name"  ); 
	$order_library=return_library_array( "select id,po_number from  wo_po_break_down", "id", "po_number"  ); 
	$buyer_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$floor_library=return_library_array( "select id,floor_name from  lib_prod_floor", "id", "floor_name"  );
	$sewing_line_library=return_library_array( "select id,line_name from  lib_sewing_line", "id", "line_name"  );
	$prod_reso_arr=return_library_array( "select id, line_number from prod_resource_mst",'id','line_number');
	
	$sizearr_order=return_library_array("select size_number_id,size_number_id from wo_po_color_size_breakdown where po_break_down_id=$po_break_down_id","size_number_id","size_number_id");
	$buyer_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	//var_dump();die;
	
	$po_details_sql=sql_select("select a.job_no, a.buyer_name, a.style_ref_no, a.gmts_item_id from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.id=$po_break_down_id group by a.job_no, a.buyer_name, a.style_ref_no, a.gmts_item_id");
	//echo $po_details_sql;die;
	
	if($db_type==2)
	{
		$sql= "SELECT  a.challan_no, a.floor_id, a.sewing_line, a.country_id, a.production_source, c.color_number_id, LISTAGG(CAST(c.size_number_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.size_number_id) as size_number_id , a.prod_reso_allo  
		from pro_garments_production_mst a,  pro_garments_production_dtls b,  wo_po_color_size_breakdown c 
		where a.production_type=4 and a.item_number_id=$gmts_item_id and a.production_date='$production_date' and a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.po_break_down_id='$po_break_down_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 
		group by  a.country_id,a.challan_no, a.floor_id, a.sewing_line, a.production_source, a.prod_reso_allo,c.color_number_id
		order by a.country_id,a.challan_no, a.floor_id, a.sewing_line";
	}
	else if($db_type==0)
	{
		$sql= "SELECT a.challan_no,a.floor_id, a.sewing_line,a.country_id,a.production_source, c.color_number_id, group_concat(c.size_number_id) as size_number_id, a.prod_reso_allo 
		from pro_garments_production_mst a,  pro_garments_production_dtls b,  wo_po_color_size_breakdown c 
		where a.production_type=4 and a.item_number_id=$gmts_item_id and a.production_date='$production_date' and a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.po_break_down_id='$po_break_down_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0
		group by  a.country_id,a.challan_no, a.floor_id, a.sewing_line, a.production_source, a.prod_reso_allo,c.color_number_id
		order by a.country_id,a.challan_no, a.floor_id, a.sewing_line";
	}
	//echo $sql;die;
	
	
	//echo $sql; //and a.production_date='$production_date'
	$result=sql_select($sql);
	
	$sql_color_size= sql_select("SELECT a.challan_no,a.floor_id, a.sewing_line,a.country_id,a.production_source, sum(b.production_qnty) as production_qnty, c.color_number_id, c.size_number_id 
	from pro_garments_production_mst a,  pro_garments_production_dtls b,  wo_po_color_size_breakdown c 
	where a.production_type=4 and a.item_number_id=$gmts_item_id and a.production_date='$production_date' and a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.po_break_down_id='$po_break_down_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 
	group by   a.country_id,a.challan_no, a.floor_id, a.sewing_line, a.production_source,c.color_number_id,c.size_number_id
	order by a.country_id");
	foreach($sql_color_size as $row)
	{
		$production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]] +=$row[csf('production_qnty')];
		$summery_data[$row[csf('color_number_id')]][$row[csf('size_number_id')]] +=$row[csf('production_qnty')];
	}
	
	$col_width=60*count($sizearr_order);
	$table_width=630+$col_width;
	$summer_table_width=230+$col_width;


?>	
<script>

	function print_window()
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
		d.close();
	}
	
	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<fieldset style="width:<?php echo $table_width; ?>  margin-left:10px" >
		<input type="button" value="Print" onClick="print_window()" style="width:100px; margin-left:400px;"  class="formbutton" /><br />
    <div style="100%" id="report_container">
    
        <table cellpadding="0" cellspacing="0" border="0" rules="all" width="<?php echo $table_width; ?>" style="margin-bottom:10px;  margin-top:10px;">
        	<tr>
            	<td style="font-size:14px; font-weight:bold;">
                Buyer Name : <?php echo $buyer_library[$po_details_sql[0][csf("buyer_name")]]; ?>&nbsp;&nbsp;&nbsp;&nbsp; Job No : <?php echo $po_details_sql[0][csf("job_no")]; ?>&nbsp;&nbsp;&nbsp;&nbsp; Style No : <?php echo $po_details_sql[0][csf("style_ref_no")]; ?>&nbsp;&nbsp;&nbsp;&nbsp; Garments Item : 
				<?php
                    $item_data=""; 
                    $garments_item_arr=array_unique(explode(",",$po_details_sql[0][csf("gmts_item_id")])); 
                    foreach($garments_item_arr as $item_id)
                    {
                        if($item_data!="") $item_data .=", ";
                        $item_data .=$garments_item[$item_id];
                    }
                    echo $item_data;
                ?>
                <br />
                Order No : <?php echo $order_library[$po_break_down_id]; ?>&nbsp;&nbsp;&nbsp;&nbsp;Date : <?php echo change_date_format($production_date); ?> 
                <br />
                Summary
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="<?php echo $summer_table_width; ?>" style="margin-bottom:20px;">
            <thead>
                <tr>
                	<th width="40" rowspan="2">SI</th>
                    <th width="100" rowspan="2">Color</th>
                	<th width="<?php echo $col_width; ?>" colspan="<?php echo count($sizearr_order); ?>">Size</th>
                    <th width="80" rowspan="2" >Total</th>
                </tr>
                <tr>
                <?php
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th width="60"><?php echo $sizearr[$size_id]; ?></th>
                    <?php
				}
				?>
                </tr>
                
            </thead>
            <tbody>
            <?php
			$i=1;
			//var_dump($result);die;
			foreach($summery_data as $color_id=>$row)
			{
				//print_r($row);die;
				
				?>
                <tr>
                    <td align="center"><?php echo $i;  ?></td>
                    <td ><?php echo $colorarr[$color_id];  ?></td>
                    <?php
					$summry_color_total_in =0;
                    foreach($sizearr_order as $size_id)
                    {
                        ?>
                        <td align="right"><?php echo number_format($row[$size_id],0) ; $summry_color_total_in+= $row[$size_id]; $summry_color_size_in [$size_id]+=$row[$size_id];?></td>
                        <?php
                    }
                    ?>
                    <td align="right"><?php echo  number_format( $summry_color_total_in,0); $grand_tot_in+=$summry_color_total_in; ?></td>
                </tr>
                <?php
				$i++;
			}
			?>
            </tbody>
            <tfoot>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <?php
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th ><?php echo $summry_color_size_in[$size_id]; ?></th>
                    <?php
				}
				?>
                <th ><?php echo $grand_tot_in; ?></th>
                
            </tfoot>
        </table>
        <table cellpadding="0" cellspacing="0" border="0" rules="all" width="<?php echo $table_width; ?>" style="margin-bottom:10px;">
        	<tr>
            	<td style="font-size:14px; font-weight:bold;">Details</td>
            </tr>
        </table>
        
        <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="<?php echo $table_width; ?>">
            <thead>
                <tr>
                	<th width="40" rowspan="2">SI</th>
                    <th width="100" rowspan="2">Country Name</th>
                    <th width="80" rowspan="2">Source</th>
                    <th width="70" rowspan="2">Challan</th>
                    <th width="90" rowspan="2">Sewing Unit</th>
                    <th width="70" rowspan="2">Sewing Line</th>
                    <th width="100" rowspan="2">Color</th>
                	<th width="<?php echo $col_width; ?>" colspan="<?php echo count($sizearr_order); ?>">Size</th>
                    <th width="80" rowspan="2" >Total</th>
                </tr>
                <tr>
                <?php
				$grand_tot_in=0;
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th width="60"><?php echo $sizearr[$size_id]; ?></th>
                    <?php
				}
				?>
                </tr>
                
            </thead>
            <tbody>
            <?php
			$i=1;
			$k=1;
			//var_dump($result);die;
			foreach($result as $row)
			{
				if(!in_array($row[csf("sewing_line")],$temp_arr[$row[csf("country_id")]]))
				{
					$temp_arr[$row[csf("country_id")]][]=$row[csf("sewing_line")];
					if($k!=1)
					{
						?>
						<tr bgcolor="#CCCCCC">
							<td >&nbsp;</td>
							<td>&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<?php
							foreach($sizearr_order as $size_id)
							{
								?>
								<td align="right"><?php echo number_format($line_color_size_in [$size_id],0); ?></td>
								<?php
							}
							?>
							<td align="right"><?php echo number_format($line_color_total_in,0); ?></td>
						</tr>
						<?php
						$line_color_size_in = $line_color_total_in ="";
					}
					$k++;
				}
				
				$sewing_line='';
				if($row[csf('prod_reso_allo')]==1)
				{
					$line_number=explode(",",$prod_reso_arr[$row[csf('sewing_line')]]);
					foreach($line_number as $val)
					{
						if($sewing_line=='') $sewing_line=$sewing_line_library[$val]; else $sewing_line.=",".$sewing_line_library[$val];
					}
				}
				else $sewing_line=$sewing_line_library[$row[csf('sewing_line')]];
				
				?>
                <tr>
                    <td align="center"><?php echo $i;  ?></td>
                    <td ><p><?php echo $country_library[$row[csf("country_id")]];  ?></p></td>
                    <td ><p><?php echo $knitting_source[$row[csf("production_source")]]; ?><p></td>
                    <td ><p><?php echo $row[csf("challan_no")];  ?></p></td>
                    <td ><p><?php echo $floor_library[$row[csf("floor_id")]];  ?></p></td>
                    <td align="center"><p><?php echo $sewing_line;  ?></p></td>
                    <td ><p><?php echo $colorarr[$row[csf("color_number_id")]];  ?></p></td>
                    <?php
					$color_total_in=0;
                    foreach($sizearr_order as $size_id)
                    {
                        ?>
                        <td align="right"><p>
						<?php
						 	echo number_format($production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id],0) ;
							 $color_total_in+= $production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id]; 
							 $color_size_in [$size_id]+=$production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id];
							 $line_color_total_in+= $production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id]; 
							 $line_color_size_in [$size_id]+=$production_break_qnty[$row[csf('country_id')]][$row[csf('challan_no')]][$row[csf('floor_id')]][$row[csf('sewing_line')]][$row[csf('production_source')]][$row[csf('color_number_id')]][$size_id];
						 ?>
                        </p></td>
                        <?php
                    }
                    ?>
                    <td align="right"><p><?php echo  number_format( $color_total_in,0); $grand_tot_in+=$color_total_in; ?></p></td>
                </tr>
                <?php
				$i++;
			}
			?>
            <tr bgcolor="#CCCCCC">
                <td >&nbsp;</td>
                <td>&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <?php
                foreach($sizearr_order as $size_id)
                {
                    ?>
                    <td align="right"><?php echo number_format($line_color_size_in [$size_id],0); ?></td>
                    <?php
                }
                ?>
                <td align="right"><?php echo number_format($line_color_total_in,0); ?></td>
            </tr>
            </tbody>
            <tfoot>
                <th >&nbsp;</th>
                <th>&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                <?php
				foreach($sizearr_order as $size_id)
				{
					?>
                	<th ><?php echo $color_size_in[$size_id]; ?></th>
                    <?php
				}
				?>
                <th ><?php echo $grand_tot_in; ?></th>
                
            </tfoot>
        </table>
        </div>
    </fieldset>
<?php	
}
?>