<?
/*-------------------------------------------- Comments

Purpose			: 	This form will create for buyer sales contract entry
					
Functionality	:	
				

JS Functions	:

Created by		:	Bilas 
Creation date 	: 	04-11-2012
Updated by 		: 	Fuad Shahriar	
Update date		: 	20-03-2013	   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Sales Contract Form", "../../", 1, 1,'','1','');
?>	

<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  

var permission='<? echo $permission; ?>';

var str_port_of_entry 		= [<? echo substr(return_library_autocomplete( "select distinct(port_of_entry) from com_sales_contract", "port_of_entry"  ), 0, -1); ?>];
var str_port_of_loading 	= [<? echo substr(return_library_autocomplete( "select distinct(port_of_loading) from com_sales_contract", "port_of_loading"  ), 0, -1); ?>];
var str_port_of_discharge 	= [<? echo substr(return_library_autocomplete( "select distinct(port_of_discharge) from com_sales_contract", "port_of_discharge"  ), 0, -1); ?>];
var str_inco_term_place 	= [<? echo substr(return_library_autocomplete( "select distinct(inco_term_place) from com_sales_contract", "inco_term_place"  ), 0, -1); ?>];

$(document).ready(function(e)
{
	$("#txt_port_of_entry").autocomplete({
		 source: str_port_of_entry
	});
	$("#txt_port_of_loading").autocomplete({
		source: str_port_of_loading
	});
	$("#txt_port_of_discharge").autocomplete({
		source: str_port_of_discharge
	});
	$("#txt_inco_term_place").autocomplete({
		source: str_inco_term_place
	});
	
});


function party_loading_dischage_field(str)
{
	 
	if(str==1)
	{
		reset_form("","","txt_port_of_entry*txt_port_of_loading*txt_port_of_discharge");
	}
	else if(str==2)
	{
		$("#txt_port_of_entry").val("From Supplier Factory");
		$("#txt_port_of_loading").val("From Supplier Factory");
		$("#txt_port_of_discharge").val("To Buyer Factory");
	}
}

function fnc_sales_contract(operation) 
{ 
	if(operation==2)
	{
		show_msg('13');
		return;
	}
	
	if ( form_validation('cbo_beneficiary_name*txt_internal_file_no*txt_year*txt_contract_no*txt_contract_value*txt_contract_date*cbo_buyer_name*txt_last_shipment_date*cbo_export_item_category','Beneficiary Name*Internal File No*Year*Contract No*Contract Value*Contract Date*Buyer Name*Shipment Date*Export Item Category')==false )
	{
		return;
	}
	else
	{
					
		var data="action=save_update_delete_mst&operation="+operation+get_submitted_data_string('cbo_beneficiary_name*txt_internal_file_no*txt_bank_file_no*txt_year*txt_contract_no*txt_contract_value*cbo_currency_name*txt_contract_date*cbo_convertible_to_lc*cbo_buyer_name*txt_applicant_name*cbo_notifying_party*cbo_consignee*cbo_lien_bank*txt_lien_date*txt_last_shipment_date*txt_expiry_date*txt_tolerance*cbo_shipping_mode*cbo_pay_term*txt_tenor*cbo_inco_term*txt_inco_term_place*cbo_contract_source*txt_port_of_entry*txt_port_of_loading*txt_port_of_discharge*txt_shipping_line*txt_doc_presentation_days*txt_max_btb_limit*txt_foreign_comn*txt_local_comn*txt_discount_clauses*txt_claim_adjustment*txt_converted_from*txt_converted_from_id*txt_converted_btb_lc*txt_converted_btb_id*txt_remarks*txt_bl_clause*txt_system_id*txt_attach_row_id*contact_system_id*cbo_export_item_category',"../../");
		
		freeze_window(operation);
		
		http.open("POST","requires/sales_contract_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = sales_contract_mst_Reply_info;
	}

}


function sales_contract_mst_Reply_info()
{
	if(http.readyState == 4) 
	{
		// alert(http.responseText);
		var reponse=trim(http.responseText).split('**');	
			
		show_msg(trim(reponse[0]));
		
		if((reponse[0]==0 || reponse[0]==1))
		{
			document.getElementById('txt_system_id').value = reponse[1];
			document.getElementById('contact_system_id').value = reponse[2];
			$('#cbo_beneficiary_name').attr('disabled','disabled');
			set_button_status(1, permission, 'fnc_sales_contract',1);	
		}
		release_freezing();
	}
}


function fnc_po_selection_save(operation)
{
	if(operation==2)
	{
		show_msg('13');
		return;
	}
	
	if (form_validation('txt_system_id','Sales Contract No')==false )
	{
		return;
	}
	var row_num = $('table#tbl_order_list tbody tr').length;
	var submit_data="";
	for(var j=1;j<=row_num;j++)  
	{
		if(trim($("#txtordernumber_"+j).val())!="")
		{
			if($("#txtattachedqnty_"+j).val()*1 <= 0)
			{
				alert("Please Insert Attach Qnty");
				$("#txtattachedqnty_"+j).focus();
				return;
			}
			submit_data += "*hiddenwopobreakdownid_"+j+"*txtattachedqnty_"+j+"*hiddenunitprice_"+j+"*txtattachedvalue_"+j+"*cbopostatus_"+j;
		}
	}
	if(submit_data=="")
	{
		alert("Please Select Order No");
		return;
	}
	var data="action=save_update_delete_contract_order_info&noRow="+row_num+"&operation="+operation+get_submitted_data_string('txt_system_id*hiddensalescontractorderid'+submit_data,"../../");
	
	freeze_window(operation);
	
	http.open("POST","requires/sales_contract_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_po_selection_save_Reply_info;
}

function fnc_po_selection_save_Reply_info()
{
	if(http.readyState == 4) 
	{
		var reponse=http.responseText.split('**');	

		show_msg(trim(reponse[0]));	
		
		if((reponse[0]==0 || reponse[0]==1))
		{	
			reset_form('salescontractfrm_2','','','txt_tot_row,0','$(\'#tbl_order_list tbody tr:not(:first)\').remove();','hidden_selectedID');
			show_list_view(reponse[1],'show_po_active_listview','po_list_view','requires/sales_contract_controller','');
			set_button_status(0, permission, 'fnc_po_selection_save',2);	
			load_po_id();
		}
		else if(reponse[0]==13)
		{
			alert('Bellow Invoice Found. Detach Not Allowed.\n Invoice No: '+reponse[1]+"\n");
		}
		
		release_freezing(); 
	}
}

function openmypage(page_link,title,row_num)
{
	if( form_validation('txt_system_id','System ID')==false )
	{
		$('#contact_system_id').focus();
		return;
	}
	else
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=900px,height=360px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var order_id=this.contentDoc.getElementById("txt_selected_id").value; 
			//alert(order_id);
			if(order_id!="")
			{
				var pre_selectID = $("#hidden_selectedID").val();
				
				if(trim(pre_selectID)=="") $("#hidden_selectedID").val(order_id);else $("#hidden_selectedID").val(pre_selectID+","+order_id); 
				
				var tot_row=$('#txt_tot_row').val();
				
				var data=order_id+"**"+tot_row;
				var list_view_orders = return_global_ajax_value( data, 'order_list_for_attach', '', 'requires/sales_contract_controller');				 
				var order_no=$('#txtordernumber_'+row_num).val();
				
				if(order_no=="")
				{
					$("#tr_"+row_num).remove();
				}
				
				$("#tbl_order_list tbody:last").append(list_view_orders);	
				
				var numRow = $('table#tbl_order_list tbody tr').length; 
				$('#txt_tot_row').val(numRow);
				
				var ddd={ dec_type:2, comma:0, currency:''}
				math_operation( "totalOrderqnty", "txtorderqnty_", "+", numRow );
				math_operation( "totalOrdervalue", "txtordervalue_", "+", numRow, ddd);
				math_operation( "totalAttachedqnty", "txtattachedqnty_", "+", numRow );
				math_operation( "totalAttachedvalue", "txtattachedvalue_", "+", numRow, ddd );
			}
 	 
		}
		
	}//end else
}

function convertible_to_lc_display()
{
	var myTest = document.getElementById("cbo_convertible_to_lc").value;
	if(myTest=='1')
	{
		$('#convert_btb_lc_list').show();
		$('#convert_btb_lc_list_cap').show();
		//$('#salescontractfrm_2').hide();
	}	
	else if(myTest=='2')
	{
	
		$('#convert_btb_lc_list').show();
		$('#convert_btb_lc_list_cap').show();
		//$('#salescontractfrm_2').show();
	}
	else if(myTest=='3')
	{
		$('#convert_btb_lc_list').hide();
		$('#convert_btb_lc_list_cap').hide();
		//$('#salescontractfrm_2').hide();
	}

}

function fn_add_sales_contract(type)
{ 
	if (type==1) //Converted From 
	{
		var beneficiary=document.getElementById('cbo_beneficiary_name').value;
		if (beneficiary==0)
		{
			alert("Please Select Beneficiary Name "); 
			return;
		}
		var page_link='requires/sales_contract_controller.php?beneficiary='+beneficiary+'&action=fake_sc';
		var title='Sales Contract Selection Form';
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=350px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]
			var fc_contract_no=this.contentDoc.getElementById("hidden_contract_no").value;
			var fc_contract_id=this.contentDoc.getElementById("hidden_contract_id").value;
			document.getElementById('txt_converted_from').value=fc_contract_no;
			document.getElementById('txt_converted_from_id').value=fc_contract_id;
			
		}		
		
	}	
	else if (type==2) // BTB
	{
		var txt_converted_from=document.getElementById('txt_converted_from_id').value;
		var txt_converted_btb_id=document.getElementById('txt_converted_btb_id').value;
		if (txt_converted_from=="")
		{
			alert("Please Select Converted from Contract No Name "); 
			return;
		}
		
		var page_link='requires/sales_contract_controller.php?sales_contract='+txt_converted_from+'&txt_converted_btb_id='+txt_converted_btb_id+'&action=fake_btb';
		var title='BTB LC Selection Form';
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=350px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]
			var btb_lc_number=this.contentDoc.getElementById("txt_selected").value;
			var btb_lc_id=this.contentDoc.getElementById("txt_selected_id").value;
			var btb_attach_id=this.contentDoc.getElementById("txt_attach_id").value;
			
			var txt_converted_btb_lc=document.getElementById('txt_converted_btb_lc').value;
			var txt_converted_btb_id=document.getElementById('txt_converted_btb_id').value;
			var txt_attach_row_id=document.getElementById('txt_attach_row_id').value;
			
			if(txt_attach_row_id!=btb_attach_id && btb_attach_id!="")
			{
				if(txt_attach_row_id=="")
				{
					document.getElementById('txt_converted_btb_lc').value=btb_lc_number;
					document.getElementById('txt_converted_btb_id').value=btb_lc_id;
					document.getElementById('txt_attach_row_id').value=btb_attach_id;
				}
				else
				{
					document.getElementById('txt_converted_btb_lc').value=txt_converted_btb_lc+"*"+btb_lc_number;
					document.getElementById('txt_converted_btb_id').value=txt_converted_btb_id+","+btb_lc_id;
					document.getElementById('txt_attach_row_id').value=txt_attach_row_id+","+btb_attach_id;
				}
			}
			
		}
		
	}
	else if (type==3) //system id
	{
		var page_link='requires/sales_contract_controller.php?action=sales_contact_search';
		var title='Sales Contract Form';
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var sales_contract_id=this.contentDoc.getElementById("hidden_sales_contract_id").value;
			if(trim(sales_contract_id)!="")
			{
				freeze_window(5);
				reset_form('salescontractfrm_2','','','txt_tot_row,0','$(\'#tbl_order_list tbody tr:not(:first)\').remove();');
				get_php_form_data( sales_contract_id, "populate_data_from_sales_contract", "requires/sales_contract_controller" );
				show_list_view(sales_contract_id,'show_po_active_listview','po_list_view','requires/sales_contract_controller','');
				release_freezing();
			}
						 
		}
	}
}

function validate_attach_qnty(row_id)
{
	if( form_validation('txtordernumber_'+row_id,'Order Number')==false )
	{
		$('#txtattachedqnty_'+row_id).val('');
		return;
	}
	else
	{
		var attached_qnty=0; 
		var txt_rate=parseFloat(Number($('#hiddenunitprice_'+row_id).val()));
		var txt_attach_order_qnty=parseInt(Number($('#txtattachedqnty_'+row_id).val()));
		var order_attached_qnty=parseInt(Number($('#order_attached_qnty_'+row_id).val()));
		var txt_order_qnty=parseInt(Number($('#txtorderqnty_'+row_id).val()));
		var hide_attached_qnty=parseInt(Number($('#hideattachedqnty_'+row_id).val()));
		
		var pre_att_value=hide_attached_qnty*txt_rate;
		
		var txt_lc_no=$('#order_attached_lc_no_'+row_id).val();
		var txt_lc_qnty=parseInt(Number($('#order_attached_lc_qty_'+row_id).val()));
		var txt_sc_no=$('#order_attached_sc_no_'+row_id).val();
		var txt_sc_qnty=parseInt(Number($('#order_attached_sc_qty_'+row_id).val()));
		
		attached_qnty=txt_attach_order_qnty+order_attached_qnty;
		
		var msg=''; 
		
		if(attached_qnty>txt_order_qnty)
		{
			if(txt_lc_no=="" && txt_sc_no=="")
			{
				msg='';
			}
			else if(txt_lc_no!="" && txt_sc_no=="")
			{
				msg="\nPrevious Attached Info:\nLC NO: "+txt_lc_no+"; Attached Qty: "+txt_lc_qnty;
			}
			else if(txt_lc_no=="" && txt_sc_no!="")
			{
				msg="\nPrevious Attached Info:\nSC NO: "+txt_sc_no+"; Attached Qty: "+txt_sc_qnty;
			}
			else
			{
				msg="\nPrevious Attached Info:\nLC NO: "+txt_sc_no+"; Attached Qty: "+txt_sc_qnty+"\nSC NO: "+txt_sc_no+"; Attached Qty: "+txt_sc_qnty;
			}
			
			alert("Attached Qnty Exceeded Order Qnty"+msg);
			
			$('#txtattachedqnty_'+row_id).val(hide_attached_qnty);
			$('#txtattachedvalue_'+row_id).val(pre_att_value.toFixed(2));
			calculate_attach_val(row_id);
		}
		else
		{
			calculate_attach_val(row_id);
		}
	}
}

function calculate_attach_val(row_id)
{
	if( form_validation('txtordernumber_'+row_id,'Order Number')==false )
	{
		$('#hiddenunitprice_'+row_id).val('');
		return;
	}
	var attached_val=0;
	var txt_rate=parseFloat(Number($('#hiddenunitprice_'+row_id).val()));
	var txt_attach_order_qnty=parseInt(Number($('#txtattachedqnty_'+row_id).val()));
	attached_val=txt_attach_order_qnty*txt_rate;
	$('#txtattachedvalue_'+row_id).val(attached_val.toFixed(2));
	
	var numRow = $('table#tbl_order_list tbody tr').length; 
	
	var ddd={ dec_type:2, comma:0, currency:''}
	math_operation( "totalAttachedqnty", "txtattachedqnty_", "+", numRow );
	math_operation( "totalAttachedvalue", "txtattachedvalue_", "+", numRow, ddd );
}

function load_po_id()
{
	var sales_cotract_id=$('#txt_system_id').val();
	if(sales_cotract_id!="")
	{
		get_php_form_data(sales_cotract_id, 'populate_attached_po_id', 'requires/sales_contract_controller');
	}
}

function fn_add_date_field()
{
	$("#txt_expiry_date").val(add_days($('#txt_last_shipment_date').val(),'15'));
}
	
</script>

<style>
#salescontractfrm_1 input:not([type=checkbox]) input:not([class=flt]{
	width:152px;
}
 
/*#salescontractfrm_1 input[type=checkbox]{
	width:10px; Both Works Perfectly*/
}
</style> 

</head>
 
<body onLoad="set_hotkey()">
	<div style="width:100%;" align="center">																	
     	<? echo load_freeze_divs ("../../",$permission); ?>
     	
        <fieldset style="width:930px; margin-bottom:10px;">
            <legend>Sales Contract Entry</legend>
            <form name="salescontractfrm_1" id="salescontractfrm_1" autocomplete="off" method="POST" action="" >
                <table cellpadding="0" cellspacing="1" width="100%">
                	<tr>
                    	<td colspan="6" align="center" ><b>System ID</b> 
                        	<input type="hidden" name="txt_system_id" id="txt_system_id"  readonly class="text_boxes">
                        	<input type="text" name="contact_system_id" id="contact_system_id"  placeholder="Double Click" onDblClick="fn_add_sales_contract(3)" readonly class="text_boxes">
                        </td>                        
                    </tr>
                    <tr><td height="5" colspan="6"></td></tr>
                  	<tr>
                    	<td class="must_entry_caption">Beneficiary</td>
                        <td>
                        	<?
							   	echo create_drop_down( "cbo_beneficiary_name", 162, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Beneficiary --", 0, "load_drop_down( 'requires/sales_contract_controller', this.value, 'load_drop_down_buyer_search', 'buyer_td_id' );load_drop_down( 'requires/sales_contract_controller', this.value, 'load_drop_down_applicant_name', 'applicant_name_td' );load_drop_down( 'requires/sales_contract_controller', this.value, 'load_drop_down_notifying_party', 'notifying_party_td' );load_drop_down( 'requires/sales_contract_controller', this.value, 'load_drop_down_consignee', 'consignee_td' );get_php_form_data( this.value, 'get_btb_limit', 'requires/sales_contract_controller' );get_php_form_data( this.value, 'eval_multi_select', 'requires/sales_contract_controller' );" );
 							
							?>                          
                        </td>
                        <td class="must_entry_caption">Internal File No</td>
                    	<td><input type="text" name="txt_internal_file_no" id="txt_internal_file_no" style="width:150px" class="text_boxes_numeric" /></td>
                        <td width="130">Bank File No</td>
                        <td width="170"><input type="text" name="txt_bank_file_no" id="txt_bank_file_no" style="width:150px" class="text_boxes"  /></td>
                    </tr>                    
                    <tr>
                        <td width="130" class="must_entry_caption">Year</td>
                        <td width="170"><input name="txt_year" id="txt_year" style="width:150px" class="text_boxes" maxlength="10" title="Maximum Character 10" ></td>
                        
                        <td width="130" class="must_entry_caption">Contract Number</td>
                        <td width="170"><input type="text" name="txt_contract_no" style="width:150px" id="txt_contract_no" class="text_boxes">
                        <input type="hidden" name="txt_contract_id" id="txt_contract_id" style="width:150px" class="text_boxes" ></td>
                        <td width="130" class="must_entry_caption">Contract Value</td>
                        <td width="170"><input type="text" name="txt_contract_value" style="width:150px" id="txt_contract_value" class="text_boxes_numeric" ></td>
                    </tr>
                    <tr>
                        <td>Currency</td>
                        <td> 
                         	<?
							   	echo create_drop_down( "cbo_currency_name", 162, $currency,"", 0, "", 2, "" );
							?> 
                        </td>
                        <td class="must_entry_caption">Contract Date</td>
                        <td><input type="text" name="txt_contract_date" id="txt_contract_date" style="width:150px" class="datepicker" readonly /></td>
                        <td>Convertible to</td>
                        <td>
                            <?
							   	echo create_drop_down( "cbo_convertible_to_lc", 162, $convertible_to_lc,"", 0, "", 1, "convertible_to_lc_display()" );
							?>   
                        </td>
                    </tr>
                    <tr>
                        <td class="must_entry_caption">Buyer Name</td>
                        <td id="buyer_td_id"> 
                        	<?
							   echo create_drop_down( "cbo_buyer_name", 162, $blank_array,"", 1, "---- Select ----", 0, "" );
 							?>
                        </td>
                        <td>Applicant Name</td>
                        <td id="applicant_name_td"> 
                            <?
							   	echo create_drop_down( "txt_applicant_name", 162, $blank_array,"", 1, "---- Select ----", 0, "" );
							?>
                        </td>
                        <td>Notifying Party</td>
                        <td id="notifying_party_td">
                        	<?
							   	echo create_drop_down( "cbo_notifying_party", 162, $blank_array,"", 0, "---- Select ----", 0, "" );
							?>
                        </td>
                    </tr>
                    <tr>
                      <td>Consignee</td>
                      <td id="consignee_td">
                      		<?
							   	echo create_drop_down( "cbo_consignee", 162, $blank_array,"", 0, "---- Select ----", 0, "" );
							?>
                      </td>
                      <td>Lien Bank</td>
                      <td>
					  		<?
							   	echo create_drop_down( "cbo_lien_bank", 162, "select bank_name,id from lib_bank where is_deleted=0 and status_active=1 and lien_bank=1 order by bank_name","id,bank_name", 1, "-- Select Lien Bank --", 0, "" );
							?>
                       </td>
                      <td>Lien Date</td>
                      <td><input type="text" name="txt_lien_date" id="txt_lien_date" class="datepicker" style="width:150px" readonly ></td>
                    </tr>
                    <tr>
                        <td class="must_entry_caption">Last Shipment Date</td>
                        <td><input type="text" name="txt_last_shipment_date" style="width:150px" id="txt_last_shipment_date" class="datepicker" readonly  onChange="fn_add_date_field();" ></td>
                        <td>Expiry Date</td>
                        <td><input type="text" name="txt_expiry_date" style="width:150px" id="txt_expiry_date" class="datepicker" readonly ></td>
                        <td>Tolerance %</td>
                        <td><input type="text" name="txt_tolerance" id="txt_tolerance" style="width:150px" class="text_boxes_numeric" value="5" ></td>
                    </tr>
                    <tr>
                        <td>Shipping Mode</td>
                        <td>
							<?
							   	echo create_drop_down( "cbo_shipping_mode", 162, $shipment_mode,"", 0, "", 0, "" );
							?>
                        </td>
                        <td>Pay Term</td>
                        <td>
							<?
							   	echo create_drop_down( "cbo_pay_term", 162, $pay_term,"", 1, "--- Select ---", 0, "" );
							?>
                        </td>
                        <td>Tenor</td>
                        <td><input type="text" name="txt_tenor" id="txt_tenor" class="text_boxes_numeric" style="width:150px" /></td>
                    </tr>
                    <tr>
                        <td>Inco Term</td>
                        <td>
							<?
							   	echo create_drop_down( "cbo_inco_term", 162, $incoterm,"", 0, "", 0, "" );
							?>
                        </td>
                        <td>Inco Term Place</td>
                        <td><input type="text" name="txt_inco_term_place" id="txt_inco_term_place" style="width:150px" class="text_boxes" value="" /></td>
                        <td>Contract Source</td>
                        <td>
							<?
							   	echo create_drop_down( "cbo_contract_source", 162, $contract_source,"",1,"--- Select ---", 0, "party_loading_dischage_field(this.value)");
							?>
                        </td>
                    </tr>
                    <tr>
                        <td>Port of Entry</td>
                        <td><input type="text" name="txt_port_of_entry" id="txt_port_of_entry" style="width:150px" class="text_boxes" value="Ctg" /></td>
                        <td>Port of Loading</td>
                        <td><input type="text" name="txt_port_of_loading" id="txt_port_of_loading" style="width:150px" class="text_boxes" value="" /></td>
                        <td>Port of Discharge</td>
                        <td><input type="text" name="txt_port_of_discharge" id="txt_port_of_discharge" style="width:150px" class="text_boxes" /></td>
                    </tr>
                    <tr>
                        <td>Shipping Line</td>
                        <td><input type="text" name="txt_shipping_line" id="txt_shipping_line" style="width:150px" class="text_boxes" /></td>
                        <td>Doc Present Days</td>
                        <td><input type="text" name="txt_doc_presentation_days" id="txt_doc_presentation_days" style="width:150px" class="text_boxes" /></td>
                        <td>Claim Adjustment</td>
                        <td><input type="text" name="txt_claim_adjustment" id="txt_claim_adjustment" style="width:150px" class="text_boxes_numeric" /></td>
                    </tr>
                    <tr>
                        <td>BTB Limit %</td>
                        <td> 
                        	<input type="text" name="txt_max_btb_limit" id="txt_max_btb_limit" style="width:150px" class="text_boxes_numeric" value="">
                        </td>
                        <td>Foreign Comn%</td>
                        <td>
                            <input type="text" name="txt_foreign_comn" id="txt_foreign_comn" style="width:150px" class="text_boxes_numeric"  />                           
                        </td>
                        <td>Local Comn%</td>
                        <td>
                            <input type="text" name="txt_local_comn" id="txt_local_comn" style="width:150px" class="text_boxes_numeric"  />
                        </td>
                    </tr>
                    <tr>
                        <td>Converted From
                        <input type="hidden" name="txt_converted_from_id" id="txt_converted_from_id" class="text_boxes" ></td>
                        <td><input type="text" name="txt_converted_from" id="txt_converted_from" placeholder="Double Click" onDblClick="fn_add_sales_contract(1)" readonly class="text_boxes" style="width:150px" ></td>
                        <td id="convert_btb_lc_list_cap">Transfered BTB LC</td>
                        <input type="hidden" name="txt_converted_btb_id" id="txt_converted_btb_id" class="text_boxes" >
                        <input type="hidden" name="txt_attach_row_id" id="txt_attach_row_id" class="text_boxes" >
                        <td colspan="3" id="convert_btb_lc_list"><input type="text" readonly placeholder="Double Click" name="txt_converted_btb_lc" onDblClick="fn_add_sales_contract(2)" id="txt_converted_btb_lc" class="text_boxes" style="width:465px"></td>
                    </tr>
                    <tr>
                      <td>Discount Clauses</td>
                      <td colspan="5"><input type="text" name="txt_discount_clauses" id="txt_discount_clauses" class="text_boxes" style="width:773px" maxlength="500"></td>
                    </tr>
                    <tr>
                      <td>BL Clause</td>
                      <td colspan="5"><input type="text" name="txt_bl_clause" id="txt_bl_clause" class="text_boxes" style="width:773px" maxlength="250"></td>
                    </tr>
                    <tr>
                    	<td class="must_entry_caption">Export Item Category</td>
               			<td>
                        	<?
                                echo create_drop_down( "cbo_export_item_category", 162, $export_item_category,"", 1, "--- Select ---", 0, "" );
                            ?>
                        </td>
                    	<td>Remarks</td>
               			<td colspan="3"><input name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:465px"></td>
                    </tr>
                    <tr>
                        <td colspan="6" height="40" valign="middle" align="center" class="button_container">
							<? echo load_submit_buttons( $permission, "fnc_sales_contract", 0,0 ,"reset_form('salescontractfrm_1*salescontractfrm_2','po_list_view','','txt_port_of_entry,Ctg*txt_tot_row,0*txt_tolerance,5*cbo_currency_name,2','disable_enable_fields(\'cbo_beneficiary_name*txt_contract_value*txt_last_shipment_date*txt_expiry_date*cbo_shipping_mode*cbo_inco_term*txt_inco_term_place*txt_port_of_entry*txt_port_of_loading*txt_port_of_discharge*cbo_pay_term*txt_tenor*txt_claim_adjustment*txt_discount_clauses*bl_clause*txt_remarks\',0)');convertible_to_lc_display();$('#tbl_order_list tbody tr:not(:first)').remove();",1); ?>
                        </td>
                    </tr>
                </table>
            </form>
		</fieldset>
        <input type="button" value="Click for already attached PO" name="Attached PO" id="Attached PO" class="formbutton" onClick="openmypage('requires/sales_contract_controller.php?action=order_popup&types=attached_po_status&buyer_id='+document.getElementById('cbo_buyer_name').value+'&selectID='+document.getElementById('hidden_selectedID').value+'&sales_contractID='+document.getElementById('txt_system_id').value+'&company_id='+document.getElementById('cbo_beneficiary_name').value,'Attached PO','')" style="width:210px"/>
		<form name="salescontractfrm_2" id="salescontractfrm_2" method="POST" action="" >
			<fieldset style="width:1000px; margin:5px">
                <table width="100%" cellspacing="0" cellpadding="0" class="rpt_table" id="tbl_order_list">
                    <thead>
                    	<tr>
                            <th class="must_entry_caption">Order Number</th>
                            <th>Order Qty</th>
                            <th>Order Value</th>
                            <th class="must_entry_caption">Attach. Qty</th>
                            <th>Rate</th>
                            <th>Attach. Val.</th>
                            <th>Style Ref</th>
                            <th>Item</th>
                            <th>Job No.</th>
                            <th>Status</th> 
                      	</tr>                         
                    </thead>
                    <tbody>
                        <tr class="general" id="tr_1">
                            <td><input type="text" name="txtordernumber_1" id="txtordernumber_1" class="text_boxes" style="width:100px"  onDblClick= "openmypage('requires/sales_contract_controller.php?action=order_popup&types=order_select_popup&buyer_id='+document.getElementById('cbo_buyer_name').value+'&selectID='+document.getElementById('hidden_selectedID').value+'&sales_contractID='+document.getElementById('txt_system_id').value+'&company_id='+document.getElementById('cbo_beneficiary_name').value,'PO Selection Form',1)" readonly= "readonly" placeholder="Double Click" /></td>
                            <td><input type="text" name="txtorderqnty_1" id="txtorderqnty_1" class="text_boxes_numeric" style="width:70px;" readonly= "readonly" /></td>
                            <td><input type="text" name="txtordervalue_1" id="txtordervalue_1" class="text_boxes_numeric" style="width:80px;" readonly= "readonly"/></td>
                            <td><input type="text" name="txtattachedqnty_1" id="txtattachedqnty_1" class="text_boxes_numeric" style="width:70px" onKeyUp="validate_attach_qnty(1)" />
                            	<input type="hidden" name="hideattachedqnty_1" id="hideattachedqnty_1" class="text_boxes_numeric" style="width:80px; text-align:right"/>
                            </td>
                            <td>
                                <input type="text" name="hiddenunitprice_1" id="hiddenunitprice_1" class="text_boxes_numeric" style="width:50px" onKeyUp="calculate_attach_val(1)" >
                            </td>
                            <td><input type="text" name="txtattachedvalue_1" id="txtattachedvalue_1" class="text_boxes_numeric" style="width:80px" readonly= "readonly"/></td>
                            <td><input type="text" name="txtstyleref_1" id="txtstyleref_1" class="text_boxes" style="width:120px" readonly= "readonly"/></td>
                            <td><input type="text" name="txtitemname_1" id="txtitemname_1" class="text_boxes" style="width:120px" readonly= "readonly"/></td>
                            <td><input type="text" name="txtjobno_1" id="txtjobno_1" class="text_boxes" style="width:90px" readonly= "readonly"/></td>
                            
                                <input type="hidden" name="hiddenwopobreakdownid_1" id="hiddenwopobreakdownid_1" readonly= "readonly" />
                                <input type="hidden" name="order_attached_qnty_1" id="order_attached_qnty_1" readonly= "readonly" />
                                <input type="hidden" name="order_attached_lc_no_1" id="order_attached_lc_no_1" readonly= "readonly" />
                                <input type="hidden" name="order_attached_lc_qty_1" id="order_attached_lc_qty_1" readonly= "readonly" />
                                <input type="hidden" name="order_attached_sc_no_1" id="order_attached_sc_no_1" readonly= "readonly" />
                                <input type="hidden" name="order_attached_sc_qty_1" id="order_attached_sc_qty_1" readonly= "readonly" />
                                
                            <td>                             
                                <? 
                                    echo create_drop_down( "cbopostatus_1", 80, $attach_detach_array,"", 0, "", 1, "" );
                                ?>
                            </td> 
                        </tr>
                    </tbody>
                    <tfoot>
                    	<tr class="tbl_bottom">
                          <td>Total</td>
                          <td><input type="text" name="totalOrderqnty" id="totalOrderqnty" class="text_boxes_numeric" style="width:70px;" readonly= "readonly" /></td>
                          <td><input type="text" name="totalOrdervalue" id="totalOrdervalue" class="text_boxes_numeric" style="width:80px;" readonly= "readonly" /></td>
                          <td><input type="text" name="totalAttachedqnty" id="totalAttachedqnty" class="text_boxes_numeric" style="width:70px;" readonly= "readonly" /></td>
                          <td>&nbsp;</td>
                          <td><input type="text" name="totalAttachedvalue" id="totalAttachedvalue" class="text_boxes_numeric" style="width:80px;" readonly= "readonly" /></td>
                          <td colspan="4">&nbsp;</td>
                        </tr>
                    	<tr>
                        	<td colspan="10" height="50" valign="middle" align="center" class="button_container">
							<? echo load_submit_buttons( $permission, "fnc_po_selection_save", 0,0 ,"reset_form('salescontractfrm_2','','','txt_tot_row,0','$(\'#tbl_order_list tbody tr:not(:first)\').remove();load_po_id();','hidden_selectedID')",2) ; ?>
                            <input type="hidden" name="hiddensalescontractorderid" id="hiddensalescontractorderid" readonly= "readonly" /> <!-- for update --> 
                            <input type="hidden" id="hidden_selectedID" readonly= "readonly" />
                            <input type="hidden" name="txt_tot_row" id="txt_tot_row" class="text_boxes_numeric"  readonly= "readonly" value="0" />
                        	</td>
                    	</tr>
                    </tfoot>
                </table>
				<div style="width:100%; margin-top:10px" id="po_list_view" align="left"></div>
			</fieldset>
		</form>
	</div>
</body>
<script>
	set_multiselect('cbo_notifying_party*cbo_consignee','0*0','0','','0*0');
</script>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>