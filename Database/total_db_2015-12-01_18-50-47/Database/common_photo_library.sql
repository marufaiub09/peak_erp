-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `common_photo_library`
--

DROP TABLE IF EXISTS `common_photo_library`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_photo_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_tble_id` varchar(25) NOT NULL,
  `details_tble_id` varchar(25) NOT NULL,
  `form_name` varchar(50) NOT NULL,
  `image_location` varchar(150) NOT NULL,
  `pic_size` int(1) NOT NULL DEFAULT '0',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `file_type` int(1) NOT NULL DEFAULT '1',
  `real_file_name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `master_tble_id` (`master_tble_id`),
  KEY `details_tble_id` (`details_tble_id`),
  KEY `form_name` (`form_name`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_photo_library`
--

LOCK TABLES `common_photo_library` WRITE;
/*!40000 ALTER TABLE `common_photo_library` DISABLE KEYS */;
INSERT INTO `common_photo_library` VALUES (1,'PAL-15-00025','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00025_1.pdf',0,0,2,'FE6976T BLU.pdf'),(2,'PAL-15-00025','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00025_2.jpg',0,0,1,'FE6975T-FE6976T-FE6977T.jpg'),(3,'PAL-15-00059','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00059_3.pdf',0,0,2,'0002_001.pdf'),(4,'PAL-15-00059','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00059_4.xlsx',0,0,2,'8865WLEGG- FULL LENGTH LEGGING S-XL.XLSX'),(5,'PAL-15-00059','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00059_5.pdf',0,0,2,'SFG-PRINT_PRT-LOGISTIC-01-OUTPUT_3902_001.pdf'),(6,'PAL-15-00060','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00060_6.pdf',0,0,2,'2005015- 5970MPOLO COTTON SOURCING.pdf'),(7,'PAL-15-00060','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00060_7.pdf',0,0,2,'2005015- 5970MPOLO COTTON SOURCING.pdf'),(8,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_8.pdf',0,0,2,'FE6975TBLU.pdf'),(9,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_9.pdf',0,0,2,'FE6975TGIALLO.pdf'),(10,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_10.pdf',0,0,2,'FE6975TROSSO.pdf'),(11,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_11.pdf',0,0,2,'FE6976T BLU.pdf'),(12,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_12.pdf',0,0,2,'FE6976TROSSO.pdf'),(13,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_13.pdf',0,0,2,'FE6977TGRIGIO.pdf'),(14,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_14.pdf',0,0,2,'FE6977TROSSO.pdf'),(15,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_15.jpg',0,0,1,'FE6977T.jpg'),(16,'PAL-15-00022','','knit_order_entry','file_upload/knit_order_entry_PAL-15-00022_16.jpg',0,0,1,'FE6975T-FE6976T-FE6977T.jpg'),(17,'1','','company_details','file_upload/company_details_1_17.jpg',0,0,1,'header_logo.jpg'),(18,'3','','yarn_dyeing_wo_booking','file_upload/yarn_dyeing_wo_booking_3_18.jpg',0,0,1,'111.jpg'),(19,'3','','yarn_dyeing_wo_booking','file_upload/yarn_dyeing_wo_booking_3_19.jpg',0,0,1,'images.jpg'),(20,'PAL-SB-15-00005','','service_booking','file_upload/service_booking_PAL-SB-15-00005_20.jpg',0,0,1,'Untitled.jpg'),(21,'PAL-SB-15-00006','','service_booking','file_upload/service_booking_PAL-SB-15-00006_21.jpg',0,0,1,'11.jpg'),(22,'6','','export_invoice','file_upload/export_invoice_6_22.xls',0,0,2,'INVOICE NO#85MGB.xls'),(23,'13','','proforma_invoice','file_upload/proforma_invoice_13_23.pdf',0,0,2,'scan0001 (2).pdf'),(24,'14','','proforma_invoice','file_upload/proforma_invoice_14_24.pdf',0,0,2,'scan0002.pdf'),(25,'16','','proforma_invoice','file_upload/proforma_invoice_16_25.pdf',0,0,2,'HANIF SPPINNING MILL.pdf'),(26,'5','','proforma_invoice','file_upload/proforma_invoice_5_26.pdf',0,0,2,'HANIF SPPINNING MILL.pdf'),(27,'17','','proforma_invoice','file_upload/proforma_invoice_17_27.pdf',0,0,2,'ARIF SPPINNING MILL.pdf'),(28,'8','','export_invoice','file_upload/export_invoice_8_28.xls',0,0,2,'INVOICE NO#98MGB.xls'),(29,'9','','export_invoice','file_upload/export_invoice_9_29.xls',0,0,2,'INVOICE NO#97MGB.xls'),(30,'10','','export_invoice','file_upload/export_invoice_10_30.xls',0,0,2,'INVOICE NO#111MGB.xls'),(31,'7','','export_invoice','file_upload/export_invoice_7_31.xls',0,0,2,'INVOICE NO#102 PEP & CO.xls'),(32,'12','','export_invoice','file_upload/export_invoice_12_32.xlsx',0,0,2,'INVOICE NO#103 PEP & CO.xlsx'),(33,'14','','export_invoice','file_upload/export_invoice_14_33.xlsx',0,0,2,'INVOICE NO#115 PEP & CO.xlsx'),(34,'15','','export_invoice','file_upload/export_invoice_15_34.xls',0,0,2,'INVOICE NO#107 PEP & CO.xls'),(35,'7','','export_invoice','file_upload/export_invoice_7_35.xls',0,0,2,'INVOICE NO#102 PEP & CO.xls'),(36,'16','','export_invoice','file_upload/export_invoice_16_36.xls',0,0,2,'INVOICE NO#117.xls'),(37,'17','','export_invoice','file_upload/export_invoice_17_37.xls',0,0,2,'INVOICE NO#122.xls'),(38,'18','','export_invoice','file_upload/export_invoice_18_38.xls',0,0,2,'INVOICE NO#119MGB-R.xls'),(39,'20','','export_invoice','file_upload/export_invoice_20_39.xls',0,0,2,'Invoice-256.xls'),(40,'21','','export_invoice','file_upload/export_invoice_21_40.xls',0,0,2,'Invoice-253.xls'),(41,'22','','export_invoice','file_upload/export_invoice_22_41.xls',0,0,2,'Invoice-254.xls'),(42,'23','','export_invoice','file_upload/export_invoice_23_42.xls',0,0,2,'INVOICE NO#130 NORWEST.xls'),(43,'24','','export_invoice','file_upload/export_invoice_24_43.xls',0,0,2,'INVOICE NO#120MGB.xls'),(44,'25','','export_invoice','file_upload/export_invoice_25_44.xls',0,0,2,'INVOICE NO#125MGB.xls'),(45,'27','','export_invoice','file_upload/export_invoice_27_45.xls',0,0,2,'INVOICE NO#110MGB.xls'),(46,'28','','export_invoice','file_upload/export_invoice_28_46.xls',0,0,2,'INVOICE NO#131 NORWEST-NZ.xls'),(47,'29','','export_invoice','file_upload/export_invoice_29_47.xls',0,0,2,'INVOICE NO#123MGB.xls');
/*!40000 ALTER TABLE `common_photo_library` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
