<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="supplier_popup")
{
	echo load_html_head_contents("Popup Info", "../../../", 1, 1,'',1,'');
	$data=explode('_',$data);
	//print_r ($data);
?>
	<script>
        var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
        function toggle( x, origColor ) {
                var newColor = 'yellow';
                if ( x.style ) {
                    x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
                }
            }
            
        function js_set_value(id)
        {
            var str=id.split("_");
            toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
            var strdt=str[2];
            str=str[1];
        
            if( jQuery.inArray(  str , selected_id ) == -1 ) {
                selected_id.push( str );
                selected_name.push( strdt );
            }
            else {
                for( var i = 0; i < selected_id.length; i++ ) {
                    if( selected_id[i] == str  ) break;
                }
                selected_id.splice( i, 1 );
                selected_name.splice( i,1 );
            }
            var id = '';
            var ddd='';
            for( var i = 0; i < selected_id.length; i++ ) {
                id += selected_id[i] + ',';
                ddd += selected_name[i] + ',';
            }
            id = id.substr( 0, id.length - 1 );
            ddd = ddd.substr( 0, ddd.length - 1 );
           $('#supplier_id').val( id );
            $('#supplier_val').val( ddd );
        } 
    </script>  
    <input type="hidden" id="supplier_id" />
    <input type="hidden" id="supplier_val" />
 <?php
	$countryArr = return_library_array("select id,country_name from  lib_country where status_active=1 and is_deleted=0","id","country_name");
	
	$sql="select a.id, a.supplier_name, a.short_name, a.country_id from lib_supplier a where FIND_IN_SET($data[0],tag_company) and a.status_active=1 order by a.supplier_name "; 
	$arr=array(2=>$party_type_supplier,3=>$countryArr);
	echo  create_list_view("list_view", "Supplier,Short Name,Party Type,Country", "160,100,170,120","580","360",0, $sql , "js_set_value", "id,supplier_name", "", 1, "0,0,party_type,country_id", $arr , "supplier_name,short_name,party_type,country_id", "",'setFilterGrid("list_view",-1);','0,0,0,0','',1) ;
	exit();	
}

if ($action=="report_generate")
{
	extract($_REQUEST);
	//$data=explode('*',$data);
	$party_id=str_replace("'","",$cbo_party_type);
	$supplier_id=str_replace("'","",$txt_supplier_id);
	//print_r ($supplier_id);
	
	if ($party_id==0) $part_id =""; else $part_id =" and b.party_type in ( $party_id )";
	if ($supplier_id==0) $suppl_id =""; else $suppl_id =" and a.id in ( $supplier_id )";
	
	?>
	<div id="scroll_body" align="center" style="height:auto; width:1270px; margin:0 auto; padding:0;">
	<?php
	$company_library=sql_select("select id, company_name, plot_no, level_no,road_no,city from lib_company where id=".$cbo_company_id."");
	
	foreach( $company_library as $row)
	{
?>
		<span style="font-size:20px"><center><b><?php echo $row[csf('company_name')];?></b></center></span>
<?php
	}
?>
    <table width="1260px" align="center">
        <tr>
            <td colspan="6" align="center" style="font-size:18px"><center><strong><u><?php echo $report_title; ?> Report</u></strong></center></td>
        </tr>
    </table>
    <?php
		$sql_con="select a.id, a.supplier_name, a.short_name, a.contact_person, a.contact_no, a.designation, a.address_1, a.email, a.web_site, a.country_id, a.status_active, 
		a.credit_limit_days, a.credit_limit_amount, a.credit_limit_amount_currency, b.party_type, c.tag_company 
		from lib_supplier a,lib_supplier_party_type b, lib_supplier_tag_company c 
		where a.id=c.supplier_id and c.tag_company=$cbo_company_id and a.id=b.supplier_id and a.is_deleted=0  and a.status_active=1 $part_id $suppl_id order by b.party_type, a.supplier_name ";
		//echo $sql_con;			
		$sql_data=sql_select($sql_con);
		
		$item_category_array=array();
		$country_arr = return_library_array("select id,country_name from  lib_country where status_active=1 and is_deleted=0","id","country_name");
	?>
        <div style="width:1270px; height:auto">
        <table align="right" cellspacing="0" width="1260px"  border="1" rules="all" class="rpt_table" id="tbl_suppler_list" >
            <?php
		foreach( $sql_data as $row)
		{
			if (!in_array($row[csf("party_type")],$item_category_array) )
			{
				$item_category_array[]=$row[csf('party_type')];
			?>
            <thead bgcolor="#dddddd" align="center">
                <tr>
                    <td colspan="13" align="left"><b>Category : <?php echo $party_type_supplier[$row[csf("party_type")]]; ?></b></td>
                </tr>
                <tr>
                    <th width="40">SL</th>
                    <th width="150" align="center">Supplier Name</th>
                    <th width="75" align="center">Short Name</th>
                    <th width="100" align="center">Contact Person</th>
                    <th width="90" align="center">Designation</th>
                    <th width="75" align="center">Contact No.</th>
                    <th width="150" align="center">Address</th>
                    <th width="100" align="center">Email</th>
                    <th width="100" align="center">Country</th>
                    <th width="100" align="center">Website</th>
                    <th width="80" align="center">Credit Limit (Days)</th>
                    <th width="80" align="center">Credit Limit (Amount)</th>
                    <th width="80" align="center">Currency</th>
                </tr>         
            </thead>
            <tbody>
            <?php
			$i=1;
			}
				if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
			?>
            <tr bgcolor="<?php echo $bgcolor ; ?>">
            	<td><?php echo $i; ?></td>
                <td><?php echo $row[csf("supplier_name")]; ?></td>
                <td><?php echo $row[csf("short_name")]; ?></td>
                <td><?php echo $row[csf("contact_person")]; ?></td>
                <td><?php echo $row[csf("designation")]; ?></td>
                <td><?php echo $row[csf("contact_no")]; ?></td>
                <td><?php echo $row[csf("address_1")]; ?></td>
                <td><?php echo $row[csf("email")]; ?></td>
                <td><?php echo $country_arr[$row[csf("country_id")]]; ?></td>
                <td><?php echo $row[csf("web_site")]; ?></td>
                <td><?php echo $row[csf("credit_limit_days")]; ?></td>
                <td><?php echo $row[csf("credit_limit_amount")]; ?></td>
                <td><?php echo $currency[$row[csf("credit_limit_amount_currency")]]; ?></td>
            </tr>
            </tbody>
		<?php	
        $i++;
        }
?>
        </table>
	</div>
	</div>
<?php
}
?>
