-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pre_cost_mst_histry`
--

DROP TABLE IF EXISTS `wo_pre_cost_mst_histry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pre_cost_mst_histry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) DEFAULT NULL,
  `pre_cost_mst_id` int(11) DEFAULT NULL,
  `garments_nature` int(1) NOT NULL DEFAULT '0',
  `job_no` varchar(25) NOT NULL,
  `costing_date` date NOT NULL,
  `incoterm` int(11) NOT NULL DEFAULT '0',
  `incoterm_place` varchar(100) NOT NULL,
  `machine_line` varchar(100) NOT NULL,
  `prod_line_hr` varchar(100) NOT NULL,
  `costing_per` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(500) NOT NULL,
  `copy_quatation` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=yes,2=no',
  `cm_cost_predefined_method_id` int(11) NOT NULL DEFAULT '0',
  `exchange_rate` double NOT NULL,
  `sew_smv` double NOT NULL,
  `cut_smv` double NOT NULL,
  `sew_effi_percent` double NOT NULL,
  `cut_effi_percent` double NOT NULL,
  `efficiency_wastage_percent` double NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '2',
  `approved_by` int(11) NOT NULL DEFAULT '0',
  `approved_date` date NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_no` (`job_no`),
  KEY `costing_date` (`costing_date`),
  KEY `incoterm` (`incoterm`),
  KEY `incoterm_place` (`incoterm_place`),
  KEY `costing_per` (`costing_per`),
  KEY `copy_quatation` (`copy_quatation`),
  KEY `approved` (`approved`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pre_cost_mst_histry`
--

LOCK TABLES `wo_pre_cost_mst_histry` WRITE;
/*!40000 ALTER TABLE `wo_pre_cost_mst_histry` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_pre_cost_mst_histry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
