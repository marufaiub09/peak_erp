<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Buyer Inquery VS Quatation Report
				
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	01/09/2014
Updated by 		: 	Maruf	
Update date		: 	08-12-2015	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
error_reporting('0');
include('../../../../includes/common.php');

session_start();
extract($_REQUEST);
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$date=date('Y-m-d');
$user_id=$_SESSION['logic_erp']['user_id'];

$buyer_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$company_short_name_arr=return_library_array( "select id,company_short_name from lib_company",'id','company_short_name');
$company_team_name_arr=return_library_array( "select id,team_name from lib_marketing_team",'id','team_name');
$ffl_merchandiser_arr=return_library_array( "select id,team_member_name from  lib_mkt_team_member_info",'id','team_member_name');
$weak_of_year=return_library_array( "select week_date,week from  week_of_year",'week_date','week');
$inquery_confirm_date=return_library_array( "select a.inquery_id,b.confirm_date from  wo_price_quotation a, wo_price_quotation_costing_mst b where a.id=b.quotation_id and a.inquery_id>0",'inquery_id','confirm_date');
$yarn_count_array=return_library_array( "Select id, yarn_count from  lib_yarn_count where  status_active=1",'id','yarn_count');
//select id,team_member_name from lib_mkt_team_member_info where status_active =1 and is_deleted=0 order by team_member_name 

if ($action=="load_drop_down_buyer")
{
	//echo "yes";
	echo create_drop_down( "cbo_buyer_name", 180, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}


if($action=="season_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	$company=str_replace("'","",$company);
	$buyer=str_replace("'","",$buyer);
	//echo $buyer;
	?>
    <script>
	function js_set_value(str)
	{
		$('#hidden_session').val(str);
		parent.emailwindow.hide();
	}
	</script>
    <input type="hidden" id="hidden_session" name="hidden_session"  />
    <table width="400" class="rpt_table" border="1" rules="all">
        <thead>
            <th width="50">SL</th>
            <th width="200">Buyer Name</th>
            <th>Season</th>
        </thead>
    </table>
    <table width="400" class="rpt_table" border="1" rules="all" id="list_view">
        <tbody>
        <?php
		$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
        if($buyer!=0) $buyer_cond="and buyer_id='$buyer'"; else  $buyer_cond="";
        $sql_inquery=sql_select("select  buyer_id, season from  wo_quotation_inquery where status_active=1 and is_deleted=0 and company_id='$company' $buyer_cond group by buyer_id,season   order by season");
		$i=1;
		foreach($sql_inquery as $row)
		{
			if ($k%2==0)
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			?>
            <tr bgcolor="<? echo $bgcolor; ?>" style="cursor:pointer;" onclick="js_set_value('<? echo $row[csf("season")]; ?>_<?php echo $row[csf("buyer_id")]; ?>')">
            	<td width="50" align="center"><? echo $i; ?></td>
                <td width="200"><p><? echo $buyer_arr[$row[csf("buyer_id")]]; ?></p></td>
                <td><p><? echo $row[csf("season")]; ?></p></td>
            </tr>
            <?php
			$i++;
		}
        ?>
        </tbody>
        <script>setFilterGrid('list_view',-1);</script>
    
        <?php
	exit();

}

if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_company_name=str_replace(",","",$cbo_company_name);
	
	$pc_current_date=date("Y-m",strtotime($pc_date));
	
	/*if($db_type==0)
	{
		$start_date = return_field_value("min(insert_date)" ," wo_quotation_inquery","status_active=1 and is_deleted=0 and company_id=$cbo_company_name and DATE_FORMAT(insert_date, '%Y%m')='$pc_current_date'");
	}
	else if($db_type==2)
	{
		$start_date = return_field_value("min(insert_date)" ," wo_quotation_inquery","status_active=1 and is_deleted=0 and company_id=$cbo_company_name and to_char(insert_date,'YYYY-MM')='$pc_current_date'");
	}*/
	
	$start_date = return_field_value("min(insert_date)" ," wo_quotation_inquery","status_active=1 and is_deleted=0 and company_id=$cbo_company_name");
	//echo $start_date;die;

	$end_date=date("Y-m-01");
	

	$start_month=date("Y-m",strtotime($start_date));
	//$end_month=date("Y-m");
	$end_month=date("Y-m");
	$end_date2=date("Y-m-d");
	if($db_type==2)
	{
		$start_date=change_date_format($start_date,'yyyy-mm-dd','-',1);
		$end_date2=change_date_format($end_date2,'yyyy-mm-dd','-',1);
	}
	else if($db_type==0)
	{
	}
	
	//$diff = abs(strtotime($start_month) - strtotime($end_month));
	//$total_months = floor($diff / (30*60*60*24));
	$total_months=datediff("m",$start_month,$end_month);
	
	$last_month=date("Y-m", strtotime("+1 Months", strtotime($end_month)));
	
	$previous_month_year=date("Y-m",strtotime("-1 Months", strtotime($end_month)));
	$array_previous_month_year=explode("-",$previous_month_year);
	$number_of_dayes_prev_moth=cal_days_in_month(CAL_GREGORIAN, $array_previous_month_year[1], $array_previous_month_year[0]);
	$previous_month_end_date=$previous_month_year."-".$number_of_dayes_prev_moth;
	
	if($db_type==2)
	{
		$previous_month_end_date=change_date_format($previous_month_end_date,'yyyy-mm-dd','-',1);
	}
	
	$month_identify=explode("-",$end_date2);
	$month=$month_identify[1];
	$year=$month_identify[0];
	$num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
	//$current_month_end_date=$year."-".$month."-".$num_days;
	//$current_month_end_date=$year."-".$month;
	$current_month_end_date=date("Y-m-d");
	if($db_type==2)
	{
		$current_month_end_date=change_date_format($current_month_end_date,'yyyy-mm-dd','-',1);
	}
	
	if($end_date!="")
	{
		$str_cond="and insert_date between '$start_date' and '".$previous_month_end_date." 11:59:59 PM'";
		
		$end_date3=date("Y-m-01",strtotime($end_date2));
		if($db_type==2)
		{
			$end_date3=change_date_format($end_date3,'yyyy-mm-dd','-',1);
		}
		$str_cond_curr="and insert_date between '".$end_date3."' and '".$current_month_end_date." 11:59:59 PM'";
	}
	else
	{
		$str_cond="";
		$str_cond_curr="";
	}
		ob_start();
?>
<!--=============================================================Total Summary Start=============================================================================================-->
<div style="width:790px">
    <table width="100%"  cellspacing="0">
        <tr>
            <td colspan="7" align="center" ><font size="3"><strong><?php echo $company_details[$company_name];  ?> </strong> </font></td>
        </tr>
        <tr class="form_caption">
            <td colspan="7" align="center"><font size="3"><strong>Total Pending Inquiry Summary </strong></font></td>
        </tr>
    </table>
	<table border="1" rules="all" class="rpt_table" width="800">
        <thead>
            <th width="30">SL</th>
            <th width="220"> Particullars </th>
            <th width="100">No of Inquiry </th>
            <th width="100">No of Buyer</th>
            <th width="120">Submission Pending </th>
            <th >Confirmation Pending</th>
        </thead>

<?php
		//echo "SELECT id, buyer_id from wo_quotation_inquery where status_active=1 and is_deleted=0 and company_id=$cbo_company_name  $str_cond ";
		$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
		$txt_session=str_replace("'","",$txt_session);
		$hidden_inquery_id=str_replace("'","",$hidden_inquery_id);
        if($cbo_buyer_name!=0) $buyer_cond="and buyer_id='$cbo_buyer_name'"; else  $buyer_cond="";
		if($txt_session!="") $session_cond="and season='$txt_session'"; else  $session_cond="";
		
		$sql_summary=sql_select( "SELECT id, buyer_id from wo_quotation_inquery where status_active=1 and is_deleted=0 and company_id=$cbo_company_name  $str_cond $buyer_cond $session_cond");
		$inquery_id="";
		foreach( $sql_summary as $row_summary)
		{
			if($inquery_confirm_date[$row_summary[csf("id")]] =='' || $inquery_confirm_date[$row_summary[csf("id")]] =='0000-00-00')
			{
				if($inquery_id=="") $inquery_id=$row_summary[csf('id')]; else $inquery_id=$inquery_id.",".$row_summary[csf('id')];
				$num_of_buyer[$row_summary[csf('buyer_id')]]=$row_summary[csf('buyer_id')];
				$num_of_inquery[$row_summary[csf('id')]]=$row_summary[csf('id')];
			}
		}
		//echo "SELECT id, buyer_id from wo_quotation_inquery where status_active=1 and is_deleted=0 and company_id=$cbo_company_name  $str_cond_curr";
		$sql_summary2=sql_select("SELECT id, buyer_id from wo_quotation_inquery where status_active=1 and is_deleted=0 and company_id=$cbo_company_name  $str_cond_curr $buyer_cond $session_cond");
		$inquery_id_cur="";
		foreach($sql_summary2 as $row_summary2)
		{
			if($inquery_confirm_date[$row_summary2[csf("id")]] =='' || $inquery_confirm_date[$row_summary2[csf("id")]] =='0000-00-00')
			{
				if($inquery_id_cur=="") $inquery_id_cur=$row_summary2[csf('id')]; else $inquery_id_cur=$inquery_id_cur.",".$row_summary2[csf('id')];
				$num_of_buyer_currnet[$row_summary2[csf('buyer_id')]]=$row_summary2[csf('buyer_id')];
				$num_of_inquery_currnet[$row_summary2[csf('id')]]=$row_summary2[csf('id')];
			}
		}
		//echo count($num_of_inquery_currnet);die;
		//echo "SELECT id,inquery_id from wo_price_quotation where status_active=1 and is_deleted=0 and company_id=$cbo_company_name and inquery_id in($inquery_id)";
		if($inquery_id!="")
		{
			$sql_summary3=sql_select("SELECT id,inquery_id from wo_price_quotation where status_active=1 and is_deleted=0 and company_id=$cbo_company_name and inquery_id in($inquery_id)");
		}
		$inquery_id_sub="";
		foreach($sql_summary3 as $row)
		{
			if($inquery_id_sub=="") $inquery_id_sub=$row[csf('id')]; else $inquery_id_sub=$inquery_id_sub.",".$row[csf('id')];
		}
		//echo "SELECT id,inquery_id from wo_price_quotation where status_active=1 and is_deleted=0 and company_id=$cbo_company_name and inquery_id in($inquery_id_cur)";
		if($inquery_id_cur!="")
		{
			$sql_summary4=sql_select("SELECT id,inquery_id from wo_price_quotation where status_active=1 and is_deleted=0 and company_id=$cbo_company_name and inquery_id in($inquery_id_cur)");
		}
		$inquery_id_sub_curr="";
		foreach($sql_summary4 as $row)
		{
			if($inquery_id_sub_curr=="") $inquery_id_sub_curr=$row[csf('id')]; else $inquery_id_sub_curr=$inquery_id_sub_curr.",".$row[csf('id')];
		}
		
		
		if($db_type==0) $confirm_date_cond="and confirm_date!='0000-00-00'"; else if($db_type==2) $confirm_date_cond="and confirm_date IS NOT NULL ";
		if($inquery_id_sub!="")
		{
			//echo "SELECT id,quotation_id from wo_price_quotation_costing_mst where status_active=1 and is_deleted=0 and quotation_id in($inquery_id_sub) $confirm_date_cond";
			$sql_summary5=sql_select("SELECT id,quotation_id from wo_price_quotation_costing_mst where status_active=1 and is_deleted=0 and quotation_id in($inquery_id_sub) $confirm_date_cond");
		}
		$inquery_id_con="";
		foreach($sql_summary5 as $row)
		{
			if($inquery_id_con=="") $inquery_id_con=$row[csf('id')]; else $inquery_id_con=$inquery_id_con.",".$row[csf('id')];
		}
		//echo "SELECT id,inquery_id from wo_price_quotation where status_active=1 and is_deleted=0 and company_id=$cbo_company_name and inquery_id in($inquery_id_cur)";
		if($inquery_id_sub_curr!="")
		{
			//echo "SELECT id,quotation_id from wo_price_quotation_costing_mst where status_active=1 and is_deleted=0 and quotation_id in($inquery_id_sub_curr) $confirm_date_cond";
			$sql_summary6=sql_select("SELECT id,quotation_id from wo_price_quotation_costing_mst where status_active=1 and is_deleted=0 and quotation_id in($inquery_id_sub_curr) $confirm_date_cond");
		}
		$inquery_id_con_curr="";
		foreach($sql_summary6 as $row)
		{
			if($inquery_id_con_curr=="") $inquery_id_con_curr=$row[csf('id')]; else $inquery_id_con_curr=$inquery_id_con_curr.",".$row[csf('id')];
		}
		
		
		$curr_month=date("F",strtotime($end_month)).", ".date("Y",strtotime($end_month));
		//wo_price_quotation_costing_mst  wo_price_quotation
?>
        <tr bgcolor="<? echo "#E9F3FF"; ?>">
            <td rowspan="2" valign="middle" align="center">1</td>
            <td rowspan="2" valign="middle">Previous To Current Month</td>
            <td  align="right" rowspan="2" valign="middle"><? $no_inquery=count($num_of_inquery); echo $no_inquery; $summary_grand_total_inquery=$no_inquery; ?></td>
            <td align="right" rowspan="2" valign="middle"><? $no_of_buyer=count($num_of_buyer); echo $no_of_buyer; ?></td>
            <td  align="right"><? $no_inquery_sub=(count($num_of_inquery)-count($sql_summary3)); echo $no_inquery_sub;  $summary_grand_total_submission=$no_inquery_sub; ?></td>
            <td align="right"><? $no_inquery_confirm=(count($sql_summary3)-count($sql_summary5)); echo $no_inquery_confirm;  $summary_grand_total_confirm=$no_inquery_confirm; ?></td>
        </tr>
        <tr>
        	<td align="right"><? $sub_percent=($no_inquery_sub/$no_inquery)*100; echo number_format($sub_percent,2)."%"; ?></td>
            <td align="right"><? $confirm_percent=($no_inquery_confirm/$no_inquery)*100; echo number_format($confirm_percent,2)."%"; ?></td>
        </tr>
        <tr bgcolor="<? echo "#FFFFFF"; ?>">
            <td align="center" valign="middle" rowspan="2">2</td>
            <td valign="middle" rowspan="2" > <?php echo $curr_month; ?> </td>
            <td align="right" valign="middle" rowspan="2"><? $no_inquery_cur=count($num_of_inquery_currnet); echo $no_inquery_cur; $summary_grand_total_inquery+=$no_inquery_cur; ?></td>
            <td align="right" valign="middle" rowspan="2"><? $no_of_buyer_cur=count($num_of_buyer_currnet); echo $no_of_buyer_cur;  ?></td>
            <td align="right"><? $no_inquery_cur_sub=(count($num_of_inquery_currnet)-count($sql_summary4)); echo $no_inquery_cur_sub; $summary_grand_total_submission+=$no_inquery_cur_sub; ?></td>
            <td align="right"><? $no_inquery_cur_confirm=(count($sql_summary4)-count($sql_summary6)); echo $no_inquery_cur_confirm; $summary_grand_total_confirm+=$no_inquery_cur_confirm; ?></td>
        </tr>
        <tr>
        	<td align="right"><? $sub_percent_curr=($no_inquery_cur_sub/$no_inquery_cur)*100; echo number_format($sub_percent_curr,2)."%"; ?></td>
            <td align="right"><? $confirm_percent_curr=($no_inquery_cur_confirm/$no_inquery_cur)*100; echo number_format($confirm_percent_curr,2)."%"; ?></td>
        </tr>
        <tfoot>
        	<tr>
                <th colspan="2" align="right">Total</th>
                <th><? echo $summary_grand_total_inquery; ?></th>
                <th ><? //echo $summary_grand_total_lc_value; ?></th>
                <th ><? echo $summary_grand_total_submission; ?></th>
                <th><? echo $summary_grand_total_confirm; ?></th>
            </tr>
            <tr>
            	<th colspan="4" align="right">Total Percentage:</th>
                <th align="center"><? $total_percent_sub =($summary_grand_total_submission/$summary_grand_total_inquery)*100; echo number_format($total_percent_sub,2)."%"; ?></th>
                <th align="center"><? $total_percent_confirm =($summary_grand_total_confirm/$summary_grand_total_inquery)*100; echo number_format($total_percent_confirm,2)."%"; ?></th>
            </tr>
        </tfoot>
    </table> 
</div>

<br/>      
<!--=============================================================Total Summary End=============================================================================================-->


<!--=============================================================Total Current Month Strat=======================================================================================-->
<fieldset style="width:1250px">
<legend>Month Wise Total Summary</legend>
    <table width="1200">
        <tr>
        <?php	$s=0;
			for($i=0;$i<=$total_months;$i++)
			{
				$last_month=date("Y-m", strtotime("-1 Months", strtotime($last_month)));
				$month_query=$last_month."-"."%%"; 
				//echo $month_query,"jahid";die;
				if($db_type==2)
				{
					if($i==0)
					{
						$month_query_cond_sub="and b.insert_date between '$month_query_start_date' and '".$month_query_end_date." 11:59:59 PM'";
					}
					$month_inquery_id= return_field_value("LISTAGG(CAST(id as varchar(4000)),',')WITHIN GROUP (ORDER BY id) as inquery_id" ," wo_quotation_inquery","status_active=1 and is_deleted=0 and company_id=$cbo_company_name and to_char(insert_date,'YYYY-MM-DD') like '$month_query'","inquery_id");
					$month_inquery_id=implode(",",array_unique(explode(",",$month_inquery_id)));

					$month_query_cond_sub="and to_char(b.insert_date,'YYYY-MM-DD') like '$month_query'";
				}
				else if($db_type==0)
				{
					if($i==0)
					{
						$month_query_cond_sub="and b.insert_date between '$month_query_start_date' and '".$month_query_end_date." 11:59:59 PM'";
					}
					//echo "select group_concat(distinct id) as inquery_id from  wo_quotation_inquery where status_active=1 and is_deleted=0 and company_id=$cbo_company_name and insert_date like '$month_query' jahid";
					$month_inquery_id= return_field_value("group_concat(distinct id) as inquery_id" ," wo_quotation_inquery","status_active=1 and is_deleted=0 and company_id=$cbo_company_name and insert_date like '$month_query'","inquery_id");
					$month_inquery_id=implode(",",array_unique(explode(",",$month_inquery_id)));
					
					$month_query_cond_sub="and b.insert_date like '$month_query'";
				}
				
				if($i==0)
				{
					$month_query_start_date=$last_month."-01"; 
					$month_query_end_date=date("d");
					$month_query_end_date=$last_month."-".$month_query_end_date; 
					if($db_type==2)
					{
						$month_query_start_date=change_date_format($month_query_start_date,'yyyy-mm-dd','-',1);
						$month_query_end_date=change_date_format($month_query_end_date,'yyyy-mm-dd','-',1);
					}
					$month_query_cond="and insert_date between '$month_query_start_date' and '".$month_query_end_date." 11:59:59 PM'";
				}
				else
				{
					if($db_type==0)
					{
						$month_query_cond="and insert_date like '$month_query'";
					}
					else
					{
						$month_query_cond="and to_char(insert_date,'YYYY-MM-DD') like '$month_query'";
					}
				}
				//echo $month_query_cond."jahid";die;
				if($month_inquery_id=="") $month_inquery_id=0;
				if($db_type==0) $confirm_date_cond="and b.confirm_date='0000-00-00'"; else if($db_type==2) $confirm_date_cond="and b.confirm_date IS NULL ";
				//echo "SELECT count(a.id) as no_quatation_confirm,a.buyer_id from wo_price_quotation a, wo_price_quotation_costing_mst b where a.id=b.quotation_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_id=$cbo_company_name and a.inquery_id in($month_inquery_id) $confirm_date_cond group by a.buyer_id jahid";
				$sql_quation_month=sql_select("SELECT count(a.id) as no_quatation_confirm,a.buyer_id from wo_price_quotation a, wo_price_quotation_costing_mst b where a.id=b.quotation_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_id=$cbo_company_name and a.inquery_id in($month_inquery_id) $confirm_date_cond group by a.buyer_id");
				foreach($sql_quation_month as $row)
				{
					$break_confirm[$row[csf("buyer_id")]][$last_month]=$row[csf("no_quatation_confirm")];
				}
				$sql_month="SELECT id,buyer_id from wo_quotation_inquery where status_active=1 and is_deleted=0 and company_id=$cbo_company_name $month_query_cond $buyer_cond $session_cond";
				//echo $sql_month."jahud";
				$sql_result=sql_select($sql_month);
				$count=0;
				foreach($sql_result as $row)
				{
					if($inquery_confirm_date[$row[csf("id")]] =='' || $inquery_confirm_date[$row[csf("id")]] =='0000-00-00')
					{
						 $res_month[$last_month][$row[csf("buyer_id")]][$row[csf("id")]] =$row[csf("id")];
						 $count++;
						 
					}
				}
				if($count>0) 
				{
					if($s%3==0) $tr="</tr><tr>"; else $tr="";
					echo $tr;
					?>
					<td valign="top">
                        <div style="width:400px">
                            <table width="400"  cellspacing="0"  class="display">
                                <tr>
                                    <td colspan="5" align="center">
                                    <font size="3"><strong>Total Summary 
                                    <?php $month_name=date("F",strtotime($last_month)).", ".date("Y",strtotime($last_month));
                                    echo $month_name;
                                    ?>
                                    </strong></font>
                                    </td>
                                </tr>
                            </table>
                            <table width="400" class="rpt_table" border="1" rules="all">
                                <thead>
                                    <th width="50">SL</th>
                                    <th width="90">Buyer Name</th>
                                    <th width="90">No Of Inquiry</th>
                                    <th width="90">Submission Pending</th>
                                    <th width="90">Confirm Pending</th>
                                </thead>
                            <?php
                            $d=1; $tot_po_qnty=0; $tot_po_val=0; 
                            foreach( $res_month[$last_month] as $buyer_id=>$inq_id)
                            {
								$no_of_inq=count($inq_id);
								$month_sub_pendin=$no_of_inq-$break_confirm[$buyer_id][$last_month];
								$month_confirm_pendin =$break_confirm[$buyer_id][$last_month];
								if ($d%2==0)  
								$bgcolor="#E9F3FF";
								else
								$bgcolor="#FFFFFF";
								?>
									<tr bgcolor="<? echo $bgcolor; ?>">
										<td><? echo $d; ?></td>
										<td><div style="word-wrap:break-word; width:150px"><? echo $buyer_short_name_arr[$buyer_id]; ?></div></td>
										<td align="right"><? echo $no_of_inq; $tot_inqyery_qnty+=$no_of_inq; ?></td>
										<td align="right"><?  echo number_format($month_sub_pendin,0);    $total_sub_pendin +=$month_sub_pendin; ?></td>
										<td align="right"><?  echo number_format($month_confirm_pendin,0); $total_confirm_pendin +=$month_confirm_pendin; ?></td>
									</tr>
								<?php
								$d++;
                            
                            }
                            ?>
                            <tfoot>
                                <tr>
                                    <th colspan="2" align="right">Total</th>
                                    <th ><? echo number_format($tot_inqyery_qnty,0); ?></th>
                                    <th ><? echo number_format($total_sub_pendin,0); ?></th>
                                    <th ><? echo number_format($total_confirm_pendin,0); ?></th>											
                                </tr>
                            </tfoot>
                            </table>
                        </div>  
					</td> 
					<?php
					$tot_inqyery_qnty="";$total_sub_pendin="";$total_confirm_pendin="";
					$s++;
				}
			}		 
		?>
    	</tr>
    </table>
    </fieldset>
    <br/>
    <div id="detail_report" style="height:auto; width:1650px; margin:0 auto; padding:0;">
    <fieldset>
    <legend> Details Report</legend>
    <!--<div style="width:1670px;">-->
        <table border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="1650" align="left">
            <tr>
            	<td  colspan="23" width="1505"><span style="height:15px; width:15px; background-color:#FF3; float:left; margin-left:400px; margin-right:10px;"></span><span style="float:left;">2 Days but quotation not submited</span><span style="height:15px; width:15px; background-color:#FF0000; float:left;  margin-left:10px; margin-right:10px;""></span><span style="float:left;">3 Days but quotation not submited</span></td>
            </tr>
        </table>
        <table border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="1650" align="left">

        	<thead>
            	<tr>
                    <th width="30">SL</th>
                    <th width="60">Month</th>
                    <th width="40">Year</th>
                    <th width="40">Inq. Id</th>
                    <th width="40">Qut. Id</th> 
                    <th width="70" >Season</th>
                    <th width="80" >Buyer Request No</th>
                    <th width="100" >Style Name</th>
                    <th width="70" >M-List</th>
                    <th width="100" >Customer/BH Merchandiser</th>
                    <th width="100" >Department/FFL Merchandiser</th>
                    <th width="60">Status</th>
                    <th width="65">Status Date</th>
                    <th width="65">Request Receive Date</th>
                    <th width="60">Days To Sub.</th>
                    <th width="60">Delay To Conf.</th>
                    <th width="70">Approx Forecast Qty</th>
                    <th width="50">TOD</th>
                    <th width="150">Fabrication & Composition</th>
                    <th width="70">Yarn Count</th>
                    <th width="80">Quoted Price</th>
                    <th width="80">Target Price</th>
                    <th >Remarks</th>
                </tr>
        	</thead>
		</table>
<!--        </div>
-->     <div style="width:1650px; max-height:410px; overflow-y: scroll;" id="scroll_body">
        <table cellspacing="0" cellpadding="0"  width="1632"  border="1" rules="all" class="rpt_table" id="tbl_ship_pending" >
        	<tbody>
			<?php
			if($db_type==0) $confirm_date_cond="and b.confirm_date!='0000-00-00'"; else if($db_type==2) $confirm_date_cond="and b.confirm_date IS NOT NULL ";
			
			
			$sql_quatation=sql_select("SELECT id, inquery_id, product_code, m_list_no, bh_marchant, company_id, buyer_id, quot_date, est_ship_date,offer_qnty
			from  wo_price_quotation 
			where company_id =$cbo_company_name and status_active=1 and is_deleted=0 and inquery_id>0");
			
			foreach($sql_quatation as $row)
			{
				$quatation_mst_arr[$row[csf("inquery_id")]]['inquery_id']=$row[csf("inquery_id")];
				$quatation_mst_arr[$row[csf("inquery_id")]]['id']=$row[csf("id")];
				$quatation_mst_arr[$row[csf("inquery_id")]]['product_code']=$row[csf("product_code")];
				$quatation_mst_arr[$row[csf("inquery_id")]]['m_list_no']=$row[csf("m_list_no")];
				$quatation_mst_arr[$row[csf("inquery_id")]]['bh_marchant']=$row[csf("bh_marchant")];
				$quatation_mst_arr[$row[csf("inquery_id")]]['quot_date']=$row[csf("quot_date")];
				$quatation_mst_arr[$row[csf("inquery_id")]]['est_ship_date']=$row[csf("est_ship_date")];
				$quatation_mst_arr[$row[csf("inquery_id")]]['offer_qnty']=$row[csf("offer_qnty")];
			}
			//var_dump($quatation_mst_arr);
			
			$sql_quatation_costing=sql_select("SELECT a.id as quation_id, a.inquery_id, b.id as quation_costing_id, b.a1st_quoted_price, b.a1st_quoted_price_date, b.revised_price, b.revised_price_date, b.confirm_price,b.terget_qty,b.confirm_date
			from  wo_price_quotation a, wo_price_quotation_costing_mst b
			where a.id=b.quotation_id and a.company_id =$cbo_company_name and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 and a.inquery_id>0");
			
			foreach($sql_quatation_costing as $row)
			{
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['inquery_id']=$row[csf("inquery_id")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['quation_id']=$row[csf("quation_id")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['quation_costing_id']=$row[csf("quation_costing_id")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['a1st_quoted_price']=$row[csf("a1st_quoted_price")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['a1st_quoted_price_date']=$row[csf("a1st_quoted_price_date")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['revised_price']=$row[csf("revised_price")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['revised_price_date']=$row[csf("revised_price_date")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['confirm_price']=$row[csf("confirm_price")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['terget_qty']=$row[csf("terget_qty")];
				$quatation_costing_mst_arr[$row[csf("inquery_id")]]['confirm_date']=$row[csf("confirm_date")];
			}
			//var_dump($quatation_costing_mst_arr);
			$sql_fabrication_composition=sql_select("SELECT a.id as quation_id, a.inquery_id, b.construction, b.composition
			from  wo_price_quotation a, wo_pri_quo_fabric_cost_dtls b
			where a.id=b.quotation_id and a.company_id =$cbo_company_name and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0");
			
			foreach($sql_fabrication_composition as $row)
			{
				$fabrication_composition[$row[csf("inquery_id")]].=$row[csf("construction")]." ".$row[csf("composition")].",";
			}
			
			if($db_type==2)
			{
				$sql_yarn_count=sql_select("SELECT  a.inquery_id, LISTAGG(CAST(b.count_id as varchar(4000)),',')WITHIN GROUP (ORDER BY b.count_id) as count_id 
			from  wo_price_quotation a, wo_pri_quo_fab_yarn_cost_dtls b
			where a.id=b.quotation_id and a.company_id =$cbo_company_name and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 group by a.inquery_id");
			}
			else if($db_type==0)
			{
				$sql_yarn_count=sql_select("SELECT  a.inquery_id, group_concat(distinct b.count_id ) as count_id 
			from  wo_price_quotation a, wo_pri_quo_fab_yarn_cost_dtls b
			where a.id=b.quotation_id and a.company_id =$cbo_company_name and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 group by a.inquery_id");
			}
			//echo $sql_yarn_count;die;
			foreach($sql_yarn_count as $row)
			{
				$count_id_arr[$row[csf("inquery_id")]]=implode(",",array_unique(explode(",",$row[csf("count_id")])));
			}
			
			
			
            if ($start_date!="")
            {
                $str_cond3="and insert_date between '$start_date' and '$current_month_end_date 11:59:59 PM' ";
            }
            else
            {
                $str_cond3="";
            }
			$i=1;
            $sql_inquery="SELECT id, system_number_prefix_num, company_id, buyer_id, season, inquery_date, buyer_request, remarks,dealing_marchant, style_refernce, insert_date
			from  wo_quotation_inquery 
			where company_id =$cbo_company_name and status_active=1 and is_deleted=0  $str_cond3 $buyer_cond $session_cond order by inquery_date DESC";
			//echo $sql_inquery;
			$sql_result=sql_select($sql_inquery);
            foreach($sql_result as $row)
            {
				if($inquery_confirm_date[$row[csf("id")]] =='' || $inquery_confirm_date[$row[csf("id")]] =='0000-00-00')
				{
					if($quatation_costing_mst_arr[$row[csf("id")]]['confirm_date'] !='0000-00-00' && $quatation_costing_mst_arr[$row[csf("id")]]['confirm_date']!='')
					{ 
						$con_sub_date=change_date_format($quatation_costing_mst_arr[$row[csf("id")]]['confirm_date']);
						//echo $quatation_costing_mst_arr[$row[csf("id")]]['confirm_date'];
					}
					else
					{ 
						if($quatation_mst_arr[$row[csf("id")]]['inquery_id']!=0)
						{
							$con_sub_date=change_date_format($quatation_mst_arr[$row[csf("id")]]['quot_date']);
							
							$dateToSub=datediff( 'd', $row[csf('inquery_date')], $quatation_mst_arr[$row[csf("id")]]['quot_date']);
							$dateToConf=datediff( 'd',$quatation_mst_arr[$row[csf("id")]]['quot_date'],$pc_date_time);
						}
						else 
						{
							$dateToSub=datediff( 'd', $row[csf('inquery_date')], $pc_date_time);
							$dateToConf=datediff( 'd',$row[csf('inquery_date')],$pc_date_time);
							//echo change_date_format($row[csf('inquery_date')]); 
							//echo $pc_date_time;
						}
					}
					
					$month_year=date("Y-m",strtotime($row[csf("insert_date")]));
					$month_year_arr=(explode("-",$month_year));
					$month_val=($month_year_arr[1]*1);
					if ($i%2==0)  
					$bgcolor="#E9F3FF";
					else
					$bgcolor="#FFFFFF";
					?>
					<tr bgcolor="<? echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i;?>','<? echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
						<td width="30"><? echo $i; ?></td>
                        <td width="60" align="center"><p><? echo $months[$month_val]; ?></p></td>
                        <td width="40" align="center"><p><? echo $month_year_arr[0]; ?></p></td>
                        <td width="40" align="center"><p><? echo $row[csf("system_number_prefix_num")]; ?></p></td>
                        <td width="40" align="center"><p><? echo $quatation_mst_arr[$row[csf("id")]]['id']; ?></p></td>
                        <td width="70"><p><? echo $row[csf('season')]; ?></p></td>
						<td width="80"><p><? echo $row[csf('buyer_request')]; ?></p></td>
						<td width="100"><p><? echo $row[csf('style_refernce')]; ?></p></td>
						<td width="70"><p><? echo $quatation_mst_arr[$row[csf("id")]]['m_list_no']; ?></p></td>
						<td width="100"><p><? echo $buyer_short_name_arr[$row[csf('buyer_id')]]."<br>".$quatation_mst_arr[$row[csf("id")]]['bh_marchant']; ?></p></td>
						<td width="100"><p><? echo $quatation_mst_arr[$row[csf("id")]]['product_code']."<br>".$ffl_merchandiser_arr[$row[csf("dealing_marchant")]]; ?></p></td>
                        <?php
						if($quatation_costing_mst_arr[$row[csf("id")]]['confirm_date'] !='0000-00-00' && $quatation_costing_mst_arr[$row[csf("id")]]['confirm_date']!='')
						{ 
							$status="Confirmed";
							//echo $quatation_costing_mst_arr[$row[csf("id")]]['confirm_date'];
						}
						else
						{ 
							if($quatation_mst_arr[$row[csf("id")]]['inquery_id']!=0) $status="Submitted";
							else $status="Not Submitted";
						}
						if($status=='Submitted')
						{
							?>
							<td width="60" align="center" ><p><? echo $status; ?></p></td>
							<?php
                        }
						else if($status=='Not Submitted')
						{
							if($dateToSub>2)
							{
								?>
								<td width="60" align="center" bgcolor="#FF0000"><p><? echo $status; ?></p></td>
								<?php
							}
							else
							{
								?>
								<td width="60" align="center" bgcolor="#FFFF00"><p><? echo $status; ?></p></td>
								<?php
							}
                        }
						?>
						<td width="65"><p><? echo $con_sub_date; ?></p>
						</td>
						<td align="center" width="65"><p><? if($row[csf('inquery_date')]!='0000-00-00' && $row[csf('inquery_date')] !="")  echo change_date_format($row[csf('inquery_date')]);  ?></p></td>
                        <td width="60" align="center"><p><? echo $dateToSub." Days"; ?></p></td>
                        <td width="60" align="center"><p><? echo $dateToConf." Days"; ?></p></td>
						<td align="right" width="70" ><p><? echo number_format($quatation_mst_arr[$row[csf("id")]]['offer_qnty'],0,"",""); $total_offer_qty +=$quatation_mst_arr[$row[csf("id")]]['offer_qnty']; ?></p></td>
						<td width="50"><p><? if($weak_of_year[$quatation_mst_arr[$row[csf("id")]]['est_ship_date']]!="") echo "W ".$weak_of_year[$quatation_mst_arr[$row[csf("id")]]['est_ship_date']]; ?></p></td>
						<td width="150"><p>
						<?php
						$fabrication_all="";
						$k=1;
						$fabrication_arr=array_filter(explode(",",substr($fabrication_composition[$row[csf("id")]],0,-1)));
						
						if(count($fabrication_arr)>0)
						{
							foreach($fabrication_arr as $val)
							{
								if($fabrication_all !="") $fabrication_all .="<br>";
								$fabrication_all .=$k.". ".$val;
								$k++;
							}
						}
						echo $fabrication_all; 
						?>
                        </p></td>
						<td width="70"><p>
						<?php 
						$yarn_count_all="";
						$yarn_count_arr_all=explode(",",$count_id_arr[$row[csf("id")]]);
						foreach($yarn_count_arr_all as $yarn_id)
						{
							if($yarn_count_all!="") $yarn_count_all.=", ";
							 $yarn_count_all.=$yarn_count_array[$yarn_id];
						}
						echo $yarn_count_all; 
						?>
                        </p></td>
                        <?php 
						if($quatation_costing_mst_arr[$row[csf("id")]]['confirm_price'] !=0)
						{
							$quted_price=$quatation_costing_mst_arr[$row[csf("inquery_id")]]['confirm_price'];
						}
						else
						{
							if($quatation_costing_mst_arr[$row[csf("id")]]['revised_price'] !=0)
							{
								$quted_price=$quatation_costing_mst_arr[$row[csf("id")]]['revised_price'];
							}
							else
							{
								$quted_price=$quatation_costing_mst_arr[$row[csf("id")]]['a1st_quoted_price'];
							}
						}
						if($quted_price<$quatation_costing_mst_arr[$row[csf("id")]]['terget_qty'])
						{
							?>
							<td width="80" align="right" bgcolor="#FFFF00"><p><? echo number_format($quted_price,2); $total_qtd_price +=$quted_price; ?> </p></td>
							<?php
						}
						else
						{
							?>
							<td width="80" align="right"><p><? echo number_format($quted_price,2); $total_qtd_price +=$quted_price; ?></p></td>
							<?php
						}
						?>
						<td align="right" width="80" style="padding-right:3px;"><p><? echo number_format($quatation_costing_mst_arr[$row[csf("id")]]['terget_qty'],2); $total_target_qty +=$quatation_costing_mst_arr[$row[csf("id")]]['terget_qty']; ?></p> </td>
                        <td ><p><? echo $row[csf("remarks")]; ?></p></td>
					</tr>
					<?php
					$i++;
				}
        
			}
        	?>
            </tbody>
            <tfoot>
                 <tr>
                    <th colspan="16" align="right">Grand Total</th>
                    <th align="right"><? echo  number_format($total_offer_qty,0);?></th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th align="right"><? echo number_format($total_qtd_price,2); ?></th>
                    <th align="right"><? echo number_format($total_target_qty,2); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </tfoot>
        </table> 
    </div>
    </fieldset> 
</div>   

<?php
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data****$filename";
	disconnect($con);
		
}  // end if($type=="sewing_production_summary")
exit();	


?>