-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_store_location`
--

DROP TABLE IF EXISTS `lib_store_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_store_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `store_location` varchar(150) NOT NULL,
  `item_category_id` varchar(50) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `status_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `store_name` (`store_name`),
  KEY `company_id` (`company_id`),
  KEY `store_location` (`store_location`),
  KEY `is_deleted` (`is_deleted`),
  KEY `status_active` (`status_active`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_store_location`
--

LOCK TABLES `lib_store_location` WRITE;
/*!40000 ALTER TABLE `lib_store_location` DISABLE KEYS */;
INSERT INTO `lib_store_location` VALUES (1,'Yarn Store',1,'Ground Floor','1',2,'2015-02-05 00:16:26',0,NULL,0,1),(2,'Grey Fabric Store',1,'Ground Floor','13',2,'2015-02-05 00:18:51',0,NULL,0,1),(3,'General Store',1,'Ground Floor','8,9,11,15,16,17,18,19,20,21,10,5,23,6,7,22',1,'2015-06-01 16:20:08',0,NULL,0,1),(4,'Accessories Store',1,'Ground Floor','4',2,'2015-01-29 04:49:26',0,NULL,0,1),(5,'Finish Fabric Store',1,'Ground Floor','2,3',2,'2015-03-11 09:47:05',2,'2015-03-11 09:48:15',1,0),(6,'Finish Fabric Store',1,'Ground Floor','2,3,30',2,'2015-11-07 15:30:05',0,NULL,0,1);
/*!40000 ALTER TABLE `lib_store_location` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
