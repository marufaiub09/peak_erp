-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_trim_book_con_dtls_hstry`
--

DROP TABLE IF EXISTS `wo_trim_book_con_dtls_hstry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_trim_book_con_dtls_hstry` (
  `id` int(11) NOT NULL DEFAULT '0',
  `approved_no` int(11) NOT NULL,
  `wo_trim_book_con_dtl_id` int(11) NOT NULL,
  `wo_trim_booking_dtls_id` int(11) NOT NULL,
  `booking_no` varchar(30) NOT NULL,
  `job_no` varchar(25) NOT NULL,
  `po_break_down_id` int(11) NOT NULL DEFAULT '0',
  `color_number_id` int(11) NOT NULL,
  `gmts_sizes` int(11) NOT NULL,
  `item_color` int(11) NOT NULL DEFAULT '0',
  `item_size` varchar(50) NOT NULL,
  `cons` double NOT NULL,
  `process_loss_percent` double NOT NULL,
  `requirment` double NOT NULL,
  `pcs` int(11) NOT NULL,
  `color_size_table_id` int(11) NOT NULL DEFAULT '0',
  KEY `po_break_down_id` (`po_break_down_id`),
  KEY `wo_trim_booking_dtls_id` (`wo_trim_booking_dtls_id`),
  KEY `job_no` (`job_no`),
  KEY `color_number_id` (`color_number_id`),
  KEY `gmts_sizes` (`gmts_sizes`),
  KEY `item_color` (`item_color`),
  KEY `item_size` (`item_size`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_trim_book_con_dtls_hstry`
--

LOCK TABLES `wo_trim_book_con_dtls_hstry` WRITE;
/*!40000 ALTER TABLE `wo_trim_book_con_dtls_hstry` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_trim_book_con_dtls_hstry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:47
