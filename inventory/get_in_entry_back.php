﻿<?
/*-------------------------------------------- Comments
Purpose			: 	This form will create Knit Garments Order Entry
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	12-05-2013
Updated by 		: 	Kausar 	(Creating print report)
Update date		: 	11-01-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Receive Info","../", 1, 1, $unicode,1,1); 
?>	

<script>
var permission='<? echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
 

  	
// popup for WO/PI----------------------
function open_syspopup(page_link,title)
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	
	var company = $("#cbo_company_name").val();
	page_link='requires/get_in_entry_controller.php?action=sys_popup&company='+company;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=400px, center=1, resize=0, scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var sysNumber=this.contentDoc.getElementById("hidden_sys_number").value; // wo/pi number
		if (sysNumber!="")
		{
			freeze_window(5);
			$("#txt_system_id").val(sysNumber);
			reset_form('','','txt_item_description*cbo_uom*txt_quantity*txt_rate*txt_amount','','','');
			get_php_form_data(sysNumber, "populate_master_from_data", "requires/get_in_entry_controller" );
			//function show_list_view( data, action, div, path, extra_func, is_append ) 
			show_list_view(sysNumber,'show_dtls_list_view','list_container','requires/get_in_entry_controller','');
			//function disable_enable_fields( flds, operation, loop_flds, loop_leng )
			disable_enable_fields( 'cbo_company_name*cbo_item_category*txt_pi_wo_req*cbo_supplier', 1, "", "" );
			set_button_status(0, permission, 'fnc_getin_entry',1,1);
			release_freezing();	 
		}
	}		
}

function fnc_getin_entry(operation)
{
	if(operation==4)
	{
		 var report_title=$( "div.form_caption" ).html();
		 print_report( $('#cbo_company_name').val()+'*'+$('#txt_system_id').val()+'*'+report_title, "get_in_entry_print", "requires/get_in_entry_controller" ) 
		 return;
	}
	else if(operation==0 || operation==1 || operation==2)
	{
		if( form_validation('cbo_company_name*txt_challan_no*txt_receive_date*txt_item_description*cbo_uom*txt_quantity','Company Name*Challan No*Receive Date*Item Description*UOM*Quantity')==false )
		{
			return;
		}	
				 
		var dataString = "txt_system_id*cbo_company_name*cbo_sample*cbo_item_category*hidden_type*txt_pi_wo_req_id*txt_pi_wo_req*cbo_supplier*txt_challan_no*txt_receive_date*cbo_currency*txt_start_hours*txt_start_minuties*cbo_store_name*txt_item_description*cbo_uom*txt_quantity*txt_rate*txt_amount*txt_remarks*update_id";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
		//alert(data);
		freeze_window(operation);
		http.open("POST","requires/get_in_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_getin_entry_reponse;
	}
}

function fnc_getin_entry_reponse()
{	
	if(http.readyState == 4) 
	{	 	
		//alert(http.responseText);
		var reponse=trim(http.responseText).split('**');
 		if(reponse[0]==0)
		{
			show_msg(trim(reponse[0]));
			$("#txt_system_id").val(reponse[1]);
		}
		else if(reponse[0]==2)
		{
			reset_form('get_in_1','list_container*list_product_container','','','','');
			disable_enable_fields( 'cbo_company_name*cbo_item_category*txt_pi_wo_req*cbo_supplier', 0, "", "" );
			set_button_status(0, permission, 'fnc_getin_entry',1,1);
			show_msg(trim(reponse[0]));
			release_freezing();
			return;
		}
		else if(reponse[0]==10 || reponse[0]==15)
		{
			show_msg(trim(reponse[0]));
			release_freezing();
			return;
		}
		else if(reponse[0]==20)
		{
			alert(reponse[1]);
			release_freezing();
			return;
		}
		
		show_list_view(reponse[1],'show_dtls_list_view','list_container','requires/get_in_entry_controller','');
		reset_form('','','txt_item_description*cbo_uom*txt_quantity*txt_rate*txt_amount*txt_remarks','','','');
		set_button_status(0, permission, 'fnc_getin_entry',1,1);
		release_freezing();
 	}
}

function open_piworeq()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}	
	var company = $("#cbo_company_name").val();	
	var item_category = $("#cbo_item_category").val();	
	var page_link='requires/get_in_entry_controller.php?action=piworeq_popup&company='+company+'&item_category='+item_category; 
	var title="Search PI/WO/REQ Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		
		var theform=this.contentDoc.forms[0];
 		var type=this.contentDoc.getElementById("cbo_search_by").value; // type
		var pi_wo_req_id=this.contentDoc.getElementById("hidden_tbl_id").value; // pi wo req id
		//form data
		get_php_form_data(type+"**"+pi_wo_req_id, "populate_main_from_data", "requires/get_in_entry_controller");
		//right side list view 
		show_list_view( type+"**"+pi_wo_req_id,'show_product_listview','list_product_container','requires/get_in_entry_controller','');
   	}
}


//amount calculate
function fn_calculate_amount()
{
	var quantity = $("#txt_quantity").val();
	var rate = $("#txt_rate").val();
	var amount = quantity*rate*1;
	$("#txt_amount").val(amount); 
}

function fnc_move_cursor(val,id, field_id,lnth,max_val)
{
	var str_length=val.length;
	if(str_length==lnth)
	{
		$('#'+field_id).select();
		$('#'+field_id).focus();
	}
	
	if(val>max_val)
	{
		document.getElementById(id).value=max_val;
	}
}


//form reset/refresh function here
function fnResetForm()
{
	$("#tbl_master").find('input,select').attr("disabled", false);	
	disable_enable_fields( 'cbo_uom', 0, "", "" );
	set_button_status(0, permission, 'fnc_getin_entry',1,0);
	reset_form('get_in_1','list_container*list_product_container','','','','');
	
}


</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<? echo load_freeze_divs ("../",$permission);  ?><br />    		 
    <form name="get_in_1" id="get_in_1" autocomplete="off" > 
    <div style="width:80%;">       
    <table width="80%" cellpadding="0" cellspacing="2" align="left">
     	<tr>
        	<td width="80%" align="center" valign="top">   
            	<fieldset style="width:900px; float:left;">
                <legend>Get IN</legend>
                  	<fieldset style="width:850px;">                                       
                        <table  width="850" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                            <tr>
                           		<td colspan="6" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>System ID</b>
                                	<input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:155px" placeholder="Double Click To Search" onDblClick="open_syspopup()" readonly />
                                </td>
                           </tr>
                           <tr>
                                <td  width="130" align="right" class="must_entry_caption">Company Name </td>
                                <td width="170">
                                      <? 
                                       echo create_drop_down( "cbo_company_name", 170, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/get_in_entry_controller', this.value, 'load_drop_down_supplier', 'supplier_td' );load_drop_down( 'requires/get_in_entry_controller', this.value, 'load_drop_down_store', 'store_td' );" );
                                      ?>
                               	 </td>
                                 <td width="150" align="right" > Sample </td>
                                 <td width="160">
                                 	<? 
										
                                       	echo create_drop_down( "cbo_sample", 170, "select id,sample_name from lib_sample where status_active=1 order by sample_name","id,sample_name",1, "-- Select --", 0, "" );
                                    ?>
                                 </td>
                                 <td width="100" align="right">Item Category</td>
                                 <td width="170"><? 
                                        echo create_drop_down( "cbo_item_category", 170, $item_category,"", 1, "--- Select ---", $selected, "load_drop_down( 'requires/get_in_entry_controller', $('#cbo_company_name').val()+'_'+this.value, 'load_drop_down_store', 'store_td' );","","",0 );
                                       ?></td>
                            </tr>
                            <tr>
                                <td  width="130" align="right">PI/WO/REQ</td>
                                <td width="170">
                                	<input type="text" name="txt_pi_wo_req" id="txt_pi_wo_req" class="text_boxes" style="width:160px" placeholder="Double Click To Search" onDblClick="open_piworeq()" readonly />
                                	<input type="hidden" name="txt_pi_wo_req_id" id="txt_pi_wo_req_id" />
                                    <input type="hidden" name="hidden_type" id="hidden_type" />
                                </td>
                                <td width="94" align="right" >Supplier</td>
                                <td width="160" id="supplier_td">
									<?
                                       echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                                    ?>
                                </td>
                                <td width="130" align="right" class="must_entry_caption">Challan No</td>
                                <td width="170"><input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:160px" ></td>
                            </tr>
                            <tr>
                                <td width="130" align="right" class="must_entry_caption">Receive Date</td>
                                    <td id="lc_no" width="170"><input type="text" name="txt_receive_date" id="txt_receive_date" class="datepicker" style="width:160px;" placeholder="Select Date" /></td>
                                    <td width="94" align="right" >Currency</td>
                                    <td id="supplier" width="160">
								   		<?
                                      	 echo create_drop_down( "cbo_currency", 170, $currency,"", 1, "-- Select Currency --", $currencyID, "",0 );
                                    	?>
                                    </td>
                                    <td width="130" align="right">Time</td>
                                    <td width="170">
                                    	 	<input type="text" name="txt_start_hours" id="txt_start_hours" class="text_boxes_numeric" placeholder="Hours" style="width:70px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_hours','txt_start_minuties',2,23);" />
                                 			<input type="text" name="txt_start_minuties" id="txt_start_minuties" class="text_boxes_numeric" placeholder="Minutes" style="width:70px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_minuties','txt_start_date',2,59)" />
                                    </td>
                            </tr>
                            <tr>
                            		<td width="130" align="right">Store Name</td>
                                    <td width="170" id="store_td">
                                    	<? 
                                        echo create_drop_down( "cbo_store_name", 170, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET(1,item_category_id) order by store_name","id,store_name", 1, "-- Select Store --", $storeName, "" );
                                        ?>
                                    </td>
                            </tr>
                        </table>
                    </fieldset>
                  <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                    <tr>
                    <td width="49%" valign="top">
                    	<fieldset style="width:850px;">  
                        <legend>Item Part</legend>                                     
                            <table  width="700" cellspacing="2" cellpadding="0" border="0">
                                <tr>                               
                                    <td width="150" align="right" class="must_entry_caption">Item Description</td>
                                    <td width=""><input type="text" name="txt_item_description" id="txt_item_description" class="text_boxes" style="width:400px" ></td>
                                 </tr>
                                <tr>
                                    <td align="right" class="must_entry_caption">UOM</td>
                                    <td>
                                    	<? 
                                      	 	echo create_drop_down( "cbo_uom", 130, $unit_of_measurement,"", 1, "-- Select UOM --", $selected, "",0 );
                                      	?>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="must_entry_caption">Quantity</td>
                                    <td id="composition_td"><input type="text" name="txt_quantity" id="txt_quantity" class="text_boxes_numeric" style="width:120px" ></td>
                                </tr>
                                <tr>
                                     <td align="right">Rate</td>
                                    <td><input type="text" name="txt_rate" id="txt_rate" class="text_boxes_numeric" style="width:120px" onKeyUp="fn_calculate_amount()" ></td>
                                </tr>
                                <tr>
                                     <td align="right">Amount</td>
                                     <td id="color_td_id"><input type="text" name="txt_amount" id="txt_amount" class="text_boxes_numeric" style="width:120px" readonly disabled ></td>
                                 </tr>
                                <tr>
                                    <td align="right">Remarks</td>
                                    <td><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:400px" ></td>
                                </tr>
                                
                            </table>
                    </fieldset>
                    </td>
                    </tr>
                </table>                
               	<table cellpadding="0" cellspacing="1" width="100%">
                	<tr> 
                       <td colspan="6" align="center"></td>				
                	</tr>
                	<tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <!-- details table id for update -->
                             <input type="hidden" id="update_id" name="update_id" value="" />
                              <!-- -->
							 <? echo load_submit_buttons( $permission, "fnc_getin_entry", 0,1,"fnResetForm()",1);?>
                        </td>
                   </tr> 
                </table>                 
              	</fieldset>
              	<fieldset>
    			<div style="width:850px;" id="list_container"></div>
    		  	</fieldset>
           </td>
         </tr>
    </table>
    </div>
    <div id="list_product_container" style="max-height:500px; width:20%; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>  
	</form>
</div>    
</body>  
<script src="../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
