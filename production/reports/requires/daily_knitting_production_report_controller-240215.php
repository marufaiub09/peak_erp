<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_floor")
{
	echo create_drop_down( "cbo_floor_id", 130, "select a.id, a.floor_name from lib_prod_floor a, lib_machine_name b where a.id=b.floor_id and b.category_id=1 and b.company_id=$data and b.status_active=1 and b.is_deleted=0  group by a.id,a.floor_name order by a.floor_name","id,floor_name", 1, "-- Select Floor --", 0, "","" );//load_drop_down( 'requires/daily_knitting_production_report_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_machine', 'machine_td' );$location_cond
  exit();	 
}

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$supplier_arr=return_library_array( "select id, short_name from lib_supplier", "id", "short_name"  );
$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$yarn_count_details=return_library_array( "select id,yarn_count from lib_yarn_count", "id", "yarn_count"  );
$brand_details=return_library_array( "select id, brand_name from lib_brand", "id", "brand_name"  );
$machine_details=return_library_array( "select id, machine_no from lib_machine_name", "id", "machine_no"  );
$floor_details=return_library_array( "select id, floor_name from lib_prod_floor", "id", "floor_name"  );
$reqsn_details=return_library_array( "select knit_id, requisition_no from ppl_yarn_requisition_entry group by knit_id", "knit_id", "requisition_no"  );
//--------------------------------------------------------------------------------------------------------------------

$tmplte=explode("**",$data);

if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 

 	if($template==1)
	{
		if (str_replace("'","",$cbo_floor_id)==0) $floor_id=''; else $floor_id=" and b.floor_id=$cbo_floor_id";
		
		$from_date=$txt_date_from;
		if(str_replace("'","",$txt_date_to)=="") $to_date=$from_date; else $to_date=$txt_date_to;
		
		if(str_replace("'","",$cbo_knitting_source)==0) $source="%%"; else $source=str_replace("'","",$cbo_knitting_source);
		
		$machine_details=array();
		$machine_data=sql_select("select id, machine_no, dia_width from lib_machine_name");
		foreach($machine_data as $row)
		{
			$machine_details[$row[csf('id')]]['no']=$row[csf('machine_no')];
			$machine_details[$row[csf('id')]]['dia']=$row[csf('dia_width')];
		}

		$po_array=array();
		$po_data=sql_select("select a.job_no, a.job_no_prefix_num, a.style_ref_no from wo_po_details_master a where a.company_name=$cbo_company_name");
		foreach($po_data as $row)
		{
			//$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
			//$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')];
			$po_array[$row[csf('job_no')]]['style_ref_no']=$row[csf('style_ref_no')];
		}
		
		$po_sub_array=array();
		$po_data=sql_select("select a.job_no, a.job_no_prefix_num, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$cbo_company_name group by a.job_no, a.job_no_prefix_num, a.style_ref_no, b.id, b.po_number");
		foreach($po_data as $row)
		{
			$po_sub_array[$row[csf('id')]]['job_no']=$row[csf('job_no')];
			$po_sub_array[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
			$po_sub_array[$row[csf('id')]]['po_number']=$row[csf('po_number')];
		}
		//var_dump($po_sub_array);
		$composition_arr=array();
		$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
		$data_array=sql_select($sql_deter);
		if(count($data_array)>0)
		{
			foreach( $data_array as $row )
			{
				if(array_key_exists($row[csf('id')],$composition_arr))
				{
					$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
				}
				else
				{
					$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
				}
			}
		}
		
		$knit_plan_arr=array();
		$plan_data=sql_select("select id, color_range, stitch_length from ppl_planning_info_entry_dtls");
		foreach($plan_data as $row)
		{
			$knit_plan_arr[$row[csf('id')]]['cr']=$row[csf('color_range')];
			$knit_plan_arr[$row[csf('id')]]['sl']=$row[csf('stitch_length')]; 
		}
		
		$tbl_width=2000+count($shift_name)*155;
		ob_start();
		?>
        <fieldset style="width:<?php echo $tbl_width+20; ?>px;">
        	<table cellpadding="0" cellspacing="0" width="<?php echo $tbl_width; ?>">
                <tr>
                   <td align="center" width="100%" colspan="30" class="form_caption" style="font-size:18px"><?php echo $report_title; ?></td>
                </tr>
                <tr>
                   <td align="center" width="100%" colspan="30" class="form_caption" style="font-size:16px"><?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?></td>
                </tr>
                <tr> 
                   <td align="center" width="100%" colspan="30" class="form_caption" style="font-size:12px" ><strong><?php echo "From ".str_replace("'","",$txt_date_from)." To ".str_replace("'","",$txt_date_to); ?></strong></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="450px" class="rpt_table" >
                <thead>
                	<tr>
                    	<th colspan="5">Knit Production Summary</th>
                    </tr>
                    <tr>
                        <th width="40">SL</th>
                        <th width="100">Buyer</th>
                        <th width="90">Inhouse</th>
                        <th width="90">Outbound-Subcon</th>
                        <th width="100">Total</th>
                    </tr>
                </thead>
                <tbody>
                <?php
				
					$sql_qty="Select a.buyer_id, sum(case when a.knitting_source=1 and b.machine_no_id>0 $floor_id  then c.quantity end ) as qtyinhouse, sum(case when a.knitting_source=3 then c.quantity end ) as qtyoutbound from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.item_category=13 and a.id=b.mst_id and b.id=c.dtls_id and c.entry_form=2 and c.trans_type=1 and a.company_id=$cbo_company_name and a.knitting_source like '$source' and a.receive_date between $from_date and $to_date and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0  group by a.buyer_id ";
					//echo $sql_qty; 
					$k=1;
					$sql_result=sql_select( $sql_qty);
					foreach($sql_result as $rows)
					{
					   if ($k%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
					?>
                    	<tr bgcolor="<?php echo $bgcolor; ?>">
                            <td width="40"><?php echo  $k; ?></td>
                            <td width="100"><?php echo  $buyer_arr[$rows[csf('buyer_id')]]; ?></td>
                            <td width="90" align="right"><?php echo  number_format($rows[csf('qtyinhouse')],2,'.',''); ?>&nbsp;</td>
                            <td width="90" align="right"><?php echo  number_format($rows[csf('qtyoutbound')],2,'.',''); $tot_summ=$rows[csf('qtyinhouse')]+$rows[csf('qtyoutbound')]; ?>&nbsp;</td>
                            <td width="100" align="right"><?php echo  number_format($tot_summ,2,'.',''); ?>&nbsp;</td>
                        </tr>
					<?php	
						$tot_qtyinhouse+=$rows[csf('qtyinhouse')];
						$tot_qtyoutbound+=$rows[csf('qtyoutbound')];
						$total_summ+=$tot_summ;
						$k++;
					}
					?>
                </tbody>
                <tfoot>
                	<tr>
                    	<th colspan="2" align="right"><strong>Total</strong></th>
                        <th align="right"><?php echo number_format($tot_qtyinhouse,2,'.',''); ?>&nbsp;</th>
                        <th align="right"><?php echo number_format($tot_qtyoutbound,2,'.',''); ?>&nbsp;</th>
                        <th align="right"><?php echo number_format($total_summ,2,'.',''); ?>&nbsp;</th>
                    </tr>
                	<tr>
                    	<th colspan="2"><strong>In %</strong></th>
                        <th align="right"><?php $qtyinhouse_per=($tot_qtyinhouse/$total_summ)*100; echo number_format($qtyinhouse_per,2).' %'; ?>&nbsp;</th>
                        <th align="right"><?php $qtyoutbound_per=($tot_qtyoutbound/$total_summ)*100; echo number_format($qtyoutbound_per,2).' %'; ?>&nbsp;</th>
                        <th align="right"><?php echo "100 %"; ?></th>
                    </tr>
                </tfoot>
            </table>
            <br />	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $tbl_width; ?>" class="rpt_table" >
                <thead>
                	<tr>
                    	<th width="40" rowspan="2"></th>
                        <th width="30" rowspan="2">SL</th>
                        <th width="55" rowspan="2">Knitting Party</th>
                        <th width="60" rowspan="2">M/C No</th>
                        <th width="90" rowspan="2">Job No</th>
                        <th width="70" rowspan="2">Buyer</th>
                        <th width="110" rowspan="2">Order No</th>
                        <th width="100" rowspan="2">Style</th>
                        <th width="90" rowspan="2">Prod. Basis</th>
                        <th width="110" rowspan="2">Booking No / Prog. No</th>
                        <th width="60" rowspan="2">Prod. No</th>
                        <th width="80" rowspan="2">Req. No.</th>
                        <th width="80" rowspan="2">Yarn Count</th>
                        <th width="90" rowspan="2">Yarn Brand</th>
                        <th width="60" rowspan="2">Lot No</th>
                        <th width="100" rowspan="2">Average Color</th>
                        <th width="150" rowspan="2">Fabric Type</th>
                        <th width="50" rowspan="2">M/C Dia</th>
                        <th width="50" rowspan="2">Fab. Dia</th>
                        <th width="50" rowspan="2">Stich</th>
                        <th width="60" rowspan="2">Fin GSM</th>
                        <?php
						foreach($shift_name as $val)
						{
						?>
                            <th width="150" colspan="2"><?php echo $val; ?></th>
                        <?php	
						}
						?>
                        <th width="150" colspan="2">No Shift</th>
                        <th width="150" colspan="2">Total</th>
                        <th rowspan="2">Remarks</th>
                    </tr>
                    <tr>
                    	<?php
						foreach($shift_name as $val)
						{
						?>
                            <th width="50" rowspan="2">Roll</th>
                        	<th width="100" rowspan="2">Qnty</th>
                        <?php	
						}
						?>
                        <th width="50" rowspan="2">Roll</th>
                        <th width="100" rowspan="2">Qnty</th>
                        <th width="50" rowspan="2">Roll</th>
                        <th width="100" rowspan="2">Qnty</th>
                    </tr>
                </thead>
            </table>
			<div style="width:<?php echo $tbl_width+20; ?>px; overflow-y:scroll; max-height:330px;" id="scroll_body">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $tbl_width; ?>" class="rpt_table" id="table_body">
					<?php 
                        $i=1; $tot_rolla=''; $tot_rollb=''; $tot_rollc=''; $tot_rolla_qnty=0; $tot_rollb_qnty=0; $tot_rollc_qnty=0; $grand_tot_roll=''; $grand_tot_qnty=0;$tot_subcontract=0;
						$inside_outside_array=array(); $floor_array=array(); $receive_basis=array(0=>"Independent",1=>"Fabric Booking No",2=>"Knitting Plan");
						
                        $sql_inhouse="select b.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_no, a.buyer_id, a.remarks, b.prod_id, b.febric_description_id, b.gsm, b.width,  b.yarn_lot, b.yarn_count, b.brand_id, b.machine_no_id, b.floor_id, c.po_breakdown_id, d.machine_no as machine_name, e.job_no_mst, e.po_number, sum(case when b.shift_name=0 then c.quantity else 0 end ) as qntynoshift, sum(case when b.shift_name=0 then  b.no_of_roll end ) as rollnoshift";
						foreach($shift_name as $key=>$val)
						{
							$sql_inhouse.=", sum(case when b.shift_name=$key then b.no_of_roll end ) as roll".strtolower($val)."	
							, sum(case when b.shift_name=$key then c.quantity else 0 end ) as qntyshift".strtolower($val);
						}
						$sql_inhouse.=" from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c, lib_machine_name d, wo_po_break_down e where c.po_breakdown_id=e.id and a.entry_form=2 and a.item_category=13 and a.id=b.mst_id and b.id=c.dtls_id and c.entry_form=2 and c.trans_type=1 and a.knitting_source=1 and a.company_id=$cbo_company_name and a.knitting_source like '$source' and a.receive_date between $from_date and $to_date and b.machine_no_id=d.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $floor_id group by a.recv_number, a.receive_basis, a.receive_date, a.booking_id, b.prod_id, b.floor_id, b.machine_no_id, b.yarn_lot, b.yarn_count, b.brand_id, e.job_no_mst, e.po_number, b.id, a.recv_number_prefix_num, a.knitting_source, a.knitting_company, a.booking_no, a.buyer_id, a.remarks, b.febric_description_id, b.gsm, b.width, c.po_breakdown_id, d.machine_no, d.seq_no order by b.floor_id,a.receive_date, d.seq_no
						";	
						
                         $sql_subcontract="select c.id, b.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_id, a.booking_no, a.buyer_id, a.remarks, b.prod_id, b.febric_description_id, b.gsm, b.width,  b.yarn_lot, b.yarn_count, b.brand_id, b.machine_no_id, b.floor_id, c.po_breakdown_id, e.job_no_mst, e.po_number, sum(case when c.entry_form=2 then c.quantity else 0 end)  as outqntyshift  from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c, wo_po_break_down e where c.po_breakdown_id=e.id and a.knitting_source=3 and a.item_category=13 and a.id=b.mst_id and b.id=c.dtls_id and c.entry_form=2 and c.trans_type=1 and a.company_id=$cbo_company_name and a.knitting_source=3 and a.knitting_source like '$source' and a.receive_date between $from_date and $to_date and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0  group by a.recv_number, a.receive_basis, a.receive_date, a.booking_id, b.prod_id, b.floor_id, b.yarn_lot, b.yarn_count, b.brand_id, e.job_no_mst, e.po_number, c.id, b.id, a.recv_number_prefix_num, a.knitting_source, a.knitting_company, a.booking_no, a.buyer_id, a.remarks, b.febric_description_id, b.gsm, b.width, b.machine_no_id, c.po_breakdown_id order by b.floor_id,a.receive_date";			
						
                     	//echo $sql_subcontract;
                        $nameArray_inhouse=sql_select( $sql_inhouse);
						$nameArray_subcontract=sql_select( $sql_subcontract);
						
						if (count($nameArray_inhouse)>0)
						{
						?>
                            <tr  bgcolor="#CCCCCC">
                                <td colspan="32" align="left" ><b>In-House</b></td>
                            </tr>    
						<?php
							foreach ($nameArray_inhouse as $row)
							{
								if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
									
								$count='';
								$yarn_count=explode(",",$row[csf('yarn_count')]);
								foreach($yarn_count as $count_id)
								{
									if($count=='') $count=$yarn_count_details[$count_id]; else $count.=",".$yarn_count_details[$count_id];
								}
								
								$reqsn_no=""; $stich_length=""; $color="";
								if($row[csf('receive_basis')]==2)
								{
									$reqsn_no=$reqsn_details[$row[csf('booking_id')]]; 
									$stich_length=$knit_plan_arr[$row[csf('booking_id')]]['sl']; 
									$color=$color_range[$knit_plan_arr[$row[csf('booking_id')]]['cr']];
								}
	
								if($row[csf('knitting_source')]==1)
									$knitting_party=$company_arr[$row[csf('knitting_company')]];
								else if($row[csf('knitting_source')]==3)
									$knitting_party=$supplier_arr[$row[csf('knitting_company')]];
								else
									$knitting_party="&nbsp;";
								
								if(!in_array($row[csf('floor_id')],$floor_array))
								{
									if($i!=1)
									{
									?>
										<tr class="tbl_bottom">
											<td colspan="20" align="right"><b>Floor Total</b></td>
											<?php
											$floor_tot_qnty_row=0;
											foreach($shift_name as $key=>$val)
											{
												$floor_tot_qnty_row+=$floor_tot_roll[$key]['qty'];
											?>
												<td align="right">&nbsp;</td>
												<td align="right"><?php echo number_format($floor_tot_roll[$key]['qty'],2,'.',''); ?></td>
											<?php
											}
											?>
                                            <td align="right">&nbsp;</td>
                                            <td align="right"><?php echo number_format($noshift_total,2,'.',''); ?></td>
											<td align="right">&nbsp;</td>
											<td align="right"><?php echo number_format($floor_tot_qnty_row+$noshift_total,2,'.',''); ?></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
								<?php
										unset($noshift_total);
										unset($floor_tot_roll);
									}	
								?>
									<tr><td colspan="32" style="font-size:14px" bgcolor="#CCCCAA">&nbsp;<b><?php echo $floor_details[$row[csf('floor_id')]]; ?></b></td></tr>
								<?php	
									$floor_array[$i]=$row[csf('floor_id')];
								}	
														?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
									<td width="40" align="center" valign="middle">
										<input type="checkbox" id="tbl_<?php echo $i;?>" onClick="selected_row(<?php echo $i; ?>);" />
										<input id="promram_id_<?php echo $i;?>" name="promram_id[]" type="hidden" value="<?php echo $row[csf('booking_id')]; ?>" />
										<input id="production_id_<?php echo $i;?>" name="production_id[]" type="hidden" value="<?php echo $row[csf('recv_number_prefix_num')]; ?>" />
										<input id="job_no_<?php echo $i;?>" name="job_no[]" type="hidden" value="<?php echo $po_array[$row[csf('po_breakdown_id')]]['job_no']; ?>" />
										<input type="hidden" id="source_<?php echo $i; ?>" name="source[]" value="<?php echo "1"; ?>" />
									<td width="30"><?php echo $i; ?></td>
									<td width="55"><p><?php echo $knitting_party; ?>&nbsp;</p></td>
									<td width="60"><p>&nbsp;<?php echo $row[csf('machine_name')]; ?></p></td>
									<td align="center" width="90"><p>&nbsp;<?php echo $row[csf('job_no_mst')]; //$po_array[$row[csf('po_breakdown_id')]]['job_no']; ?></p></td>
									<td width="70" id="buyer_id_<?php echo $i; ?>"><p>&nbsp;<?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
									<td width="110"><p>&nbsp;<?php echo $row[csf('po_number')];//$po_array[$row[csf('po_breakdown_id')]]['no']; ?></p></td>
									<td width="100"><p>&nbsp;<?php echo $po_array[$row[csf('job_no_mst')]]['style_ref_no']; ?></p></td>
									<td width="90" id="prog_id_<?php echo $i; ?>"><P><?php echo $receive_basis[$row[csf('receive_basis')]]; ?></P></td>
									<td width="110" id="booking_no_<?php echo $i; ?>"><P><?php echo $row[csf('booking_no')]; ?></P></td>
									<td width="60" id="prod_id_<?php echo $i; ?>"><P><?php echo $row[csf('recv_number_prefix_num')]; ?></P></td>
									<td width="80" align="center"><?php echo $reqsn_no; ?>&nbsp;</td>
									<td width="80" id="yarn_count_<?php echo $i; ?>"><p><?php echo $count; ?>&nbsp;</p></td>
									<td width="90" id="brand_id_<?php echo $i; ?>"><p>&nbsp;<?php echo $brand_details[$row[csf('brand_id')]]; ?></p></td>
									<td width="60" id="yarn_lot_<?php echo $i; ?>"><p>&nbsp;<?php echo $row[csf('yarn_lot')]; ?></p></td>
									<td width="100" id="color_<?php echo $i; ?>"><p>&nbsp;<?php echo $color; ?></p></td>
									<td width="150" id="feb_type_<?php echo $i; ?>"><p><?php echo $composition_arr[$row[csf('febric_description_id')]]; ?>&nbsp;</p></td>
									<td width="50" id="mc_dia_<?php echo $i; ?>"><p>&nbsp;<?php echo $machine_details[$row[csf('machine_no_id')]]['dia']; ?></p></td>
									<td width="50" id="fab_dia_<?php echo $i; ?>"><p>&nbsp;<?php echo $row[csf('width')]; ?></p></td>
									<td width="50" id="stich_<?php echo $i; ?>"><p>&nbsp;<?php echo $stich_length; ?></p></td>
									<td width="60" id="fin_gsm_<?php echo $i; ?>"><p>&nbsp;<?php echo $row[csf('gsm')]; ?></p></td>
									<?php
									$row_tot_roll=0; 
									$row_tot_qnty=0; 
									foreach($shift_name as $key=>$val)
									{
										$tot_roll[$key]['roll']+=$row[csf('roll'.strtolower($val))];
										$tot_roll[$key]['qty']+=$row[csf('qntyshift'.strtolower($val))];
										
										$source_tot_roll[$key]['roll']+=$row[csf('roll'.strtolower($val))];
										$source_tot_roll[$key]['qty']+=$row[csf('qntyshift'.strtolower($val))];
										
										$floor_tot_roll[$key]['roll']+=$row[csf('roll'.strtolower($val))];
										$floor_tot_roll[$key]['qty']+=$row[csf('qntyshift'.strtolower($val))];
										
										$row_tot_roll+=$row[csf('roll'.strtolower($val))]; 
										$row_tot_qnty+=$row[csf('qntyshift'.strtolower($val))]; 
									?>
										<td width="50" align="right" ><?php echo $row[csf('roll'.strtolower($val))]; ?></td>
										<td width="100" align="right" ><?php echo number_format($row[csf('qntyshift'.strtolower($val))],2); ?></td>
									<?php
									}
									?>
                                    <td width="50" align="right" id="noqty_<?php echo $i; ?>"><?php echo number_format($row[csf('rollnoshift')],2); ?></td>
                                    <td width="100" align="right" id="noqty_<?php echo $i; ?>"><?php echo number_format($row[csf('qntynoshift')],2); ?></td>
									<td width="50" align="right" id="roll_<?php echo $i; ?>"><?php echo $row_tot_roll; ?></td>
									<td width="100" align="right" id="qty_<?php echo $i; ?>"><?php echo number_format($row_tot_qnty+$row[csf('qntynoshift')],2,'.',''); ?></td>
									<td><p><?php echo $row[csf('remarks')]; ?>&nbsp;</p></td>
								</tr>
								</tbody>
								<?php
	
								$grand_tot_roll+=$row_tot_roll; 
								$grand_tot_qnty+=$row_tot_qnty;
								
								$source_grand_tot_roll+=$row_tot_roll; 
								$source_grand_tot_qnty+=$row_tot_qnty;
								
								$noshift_total+=$row[csf('qntynoshift')];
								
								$grand_tot_floor_roll+=$row_tot_roll; 
								$grand_tot_floor_qnty+=$row_tot_qnty;
								$total_qty_noshift+=$row[csf('qntynoshift')];
								
								$i++;
							}
						
						?>
							<tr class="tbl_bottom">
                                <td colspan="21" align="right"><b>Floor Total</b></td>
                                <?php
                                $floor_tot_qnty_row=0;
                                foreach($shift_name as $key=>$val)
                                {
                                    $floor_tot_qnty_row+=$floor_tot_roll[$key]['qty'];
                                ?>
                                    <td align="right">&nbsp;</td>
                                    <td align="right"><?php echo number_format($floor_tot_roll[$key]['qty'],2,'.',''); ?></td>
                                <?php
                                }
                                ?>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php echo number_format($noshift_total,2,'.',''); ?></td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php echo number_format($floor_tot_qnty_row+$noshift_total,2,'.',''); ?></td>
                                <td>&nbsp;</td>
                            </tr>	
                            <tr class="tbl_bottom">
                                <td colspan="21" align="right"><b>Total</b></td>
                                <?php
                                //$source_tot_qnty_row=0;
                                foreach($shift_name as $key=>$val)
                                {
									$source_tot_qnty+=$source_tot_roll[$key]['qty'];
                                    $source_tot_qnty_row+=$source_tot_roll[$key]['qty'];
                                ?>
                                    <td align="right">&nbsp;</td>
                                    <td align="right"><?php echo number_format($source_tot_qnty_row,2,'.',''); ?></td>
                                <?php
								unset($source_tot_qnty_row);
                                }
								
								
                                ?>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php echo number_format($total_qty_noshift,2,'.',''); ?></td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php echo number_format($source_tot_qnty+$total_qty_noshift,2,'.',''); ?></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php
							
						}
						if(count($nameArray_subcontract)>0)
						{
						?>
                            <tr  bgcolor="#CCCCCC">
                                <td colspan="32" align="left"><b>Outbound-Subcontract</b></td>
                            </tr>    
						<?php
							foreach ($nameArray_subcontract as $row)
							{
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
									
								$count='';
								$yarn_count=explode(",",$row[csf('yarn_count')]);
								foreach($yarn_count as $count_id)
								{
									if($count=='') $count=$yarn_count_details[$count_id]; else $count.=",".$yarn_count_details[$count_id];
								}
								
								$reqsn_no=""; $stich_length=""; $color="";
								if($row[csf('receive_basis')]==2)
								{
									$reqsn_no=$reqsn_details[$row[csf('booking_id')]]; 
									$stich_length=$knit_plan_arr[$row[csf('booking_id')]]['sl']; 
									$color=$color_range[$knit_plan_arr[$row[csf('booking_id')]]['cr']];
								}
	
								if($row[csf('knitting_source')]==1)
									$knitting_party=$company_arr[$row[csf('knitting_company')]];
								else if($row[csf('knitting_source')]==3)
									$knitting_party=$supplier_arr[$row[csf('knitting_company')]];
								else
									$knitting_party="&nbsp;";
								
							?>  
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
									<td width="40" align="center" valign="middle">
										<input type="checkbox" id="tbl_<?php echo $i;?>" onClick="selected_row(<?php echo $i; ?>);" />
										<input id="promram_id_<?php echo $i;?>" name="promram_id[]" type="hidden" value="<?php echo $row[csf('booking_id')]; ?>" />
										<input id="production_id_<?php echo $i;?>" name="production_id[]" type="hidden" value="<?php echo $row[csf('prod_id')]; ?>" />
										<input id="job_no_<?php echo $i;?>" name="job_no[]" type="hidden" value="<?php echo $po_array[$row[csf('po_breakdown_id')]]['job_no']; ?>" />
										<input type="hidden" id="source_<?php echo $i; ?>" name="source[]" value="<?php echo "3"; ?>" />
									<td width="30"><?php echo $i; ?></td>
									<td width="55"><p><?php echo $knitting_party; ?>&nbsp;</p></td>
									<td width="60"><p>&nbsp;<?php echo $row[csf('machine_name')]; ?></p></td>
									<td align="center" width="90"><p>&nbsp;<?php echo $po_sub_array[$row[csf('po_breakdown_id')]]['job_no']; ?></p></td>
									<td width="70"><p>&nbsp;<?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
									<td width="110"><p>&nbsp;<?php echo $po_sub_array[$row[csf('po_breakdown_id')]]['po_number']; ?></p></td>
									<td width="100"><p>&nbsp;<?php echo $po_sub_array[$row[csf('po_breakdown_id')]]['style_ref_no']; ?></p></td>
									<td width="90"><P><?php echo $receive_basis[$row[csf('receive_basis')]]; ?></P></td>
									<td width="110"><P><?php echo $row[csf('booking_no')]; ?></P></td>
									<td width="60"><P><?php echo $row[csf('prod_id')]; ?></P></td>
									<td width="80" align="center"><?php echo $reqsn_no; ?>&nbsp;</td>
									<td width="80"><p><?php echo $count; ?>&nbsp;</p></td>
									<td width="90"><p>&nbsp;<?php echo $brand_details[$row[csf('brand_id')]]; ?></p></td>
									<td width="60"><p>&nbsp;<?php echo $row[csf('yarn_lot')]; ?></p></td>
									<td width="100"><p>&nbsp;<?php echo $color; ?></p></td>
									<td width="150"><p><?php echo $composition_arr[$row[csf('febric_description_id')]]; ?>&nbsp;</p></td>
									<td width="50"><p>&nbsp;<?php echo $machine_details[$row[csf('machine_no_id')]]['dia']; ?></p></td>
									<td width="50"><p>&nbsp;<?php echo $row[csf('width')]; ?></p></td>
									<td width="50"><p>&nbsp;<?php echo $stich_length; ?></p></td>
									<td width="60"><p>&nbsp;<?php echo $row[csf('gsm')]; ?></p></td>
									<?php

									$row_tot_roll=0; 
									$row_tot_qnty=0; 
									foreach($shift_name as $key=>$val)
									{
										$tot_roll[$key]['roll']+=$row[csf('roll'.strtolower($val))];
										$tot_roll[$key]['qty']+=$row[csf('qntyshift'.strtolower($val))];
										
										$source_tot_roll[$key]['roll']+=$row[csf('roll'.strtolower($val))];
										$source_tot_roll[$key]['qty']+=$row[csf('qntyshift'.strtolower($val))];
										
										$floor_tot_roll[$key]['roll']+=$row[csf('roll'.strtolower($val))];
										$floor_tot_roll[$key]['qty']+=$row[csf('qntyshift'.strtolower($val))];
										
										$row_tot_roll+=$row[csf('roll'.strtolower($val))]; 
										$row_tot_qnty+=$row[csf('qntyshift'.strtolower($val))]; 
									?>
										<td width="50" align="right"><?php //echo $row[csf('roll'.strtolower($val))]; ?></td>
										<td width="100" align="right"><?php //echo number_format($row[csf('outqntyshift'.strtolower($val))],2); ?></td>
									<?php
									}
									?>
									<td width="50" align="right"><?php //echo $row_tot_roll; ?></td>
                                    <td width="100" align="right"><?php //echo number_format($row[csf('outqntyshift')],2); ?></td>
									<td width="50" align="right"><?php echo $row_tot_roll; ?></td>
									<td width="100" align="right"><?php echo number_format($row[csf('outqntyshift')],2); ?></td>
									<td><p><?php echo $row[csf('remarks')]; ?>&nbsp;</p></td>
								</tr>
								<?php
	
								$grand_tot_roll+=$row_tot_roll; 
								$grand_tot_qnty+=$row_tot_qnty;
								
								$source_grand_tot_roll+=$row_tot_roll; 
								$source_grand_tot_qnty+=$row_tot_qnty;
								
								$tot_subcontract+=$row[csf('outqntyshift')]; 
								$grand_tot_floor_qnty+=$row_tot_qnty;
								
								$i++;
							}
							
						//if(count($nameArray)>0)
						//{
						?>
                            <tr class="tbl_bottom">
                                <td colspan="21" align="right"><b>Total</b></td>
                                <?php
                                $source_tot_qnty_row=0;
                                foreach($shift_name as $key=>$val)
                                {
                                    //$source_tot_qnty_row+=$source_tot_roll[$key]['qty'];
                                ?>
                                    <td align="right">&nbsp;</td>
                                    <td align="right"><?php //echo number_format($source_tot_roll[$key]['qty'],2,'.',''); ?></td>
                                <?php
                                }
                                ?>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php echo number_format($tot_subcontract,2,'.',''); ?></td>
                                <td>&nbsp;</td>
                            </tr>                            		
						<?php
						} 
					?>
                    <tfoot>
                    	<th colspan="21" align="right">Grand Total</th>
                        <?php
						$grand_tot_qnty=0;
						foreach($shift_name as $key=>$val)
						{
							$source_tot_qnty_row+=$source_tot_roll[$key]['qty'];
							$grand_tot_qnty=$source_tot_qnty_row+$tot_subcontract+$total_qty_noshift;
						?>
							<th align="right">&nbsp;</th>
							<th align="right"><?php echo number_format($tot_roll[$key]['qty'],2,'.',''); ?></th>
						<?php
						}
						?>
                        <th align="right">&nbsp;</th>
                        <th align="right"><?php echo number_format($total_qty_noshift,2,'.',''); ?></th>
                        <th align="right">&nbsp;</th>
                        <th align="right"><?php echo number_format($grand_tot_qnty,2,'.',''); ?></th>
                        <th>&nbsp;</th>
                    </tfoot>
				</table> 
			</div>
      	</fieldset>      
	<?php
	}
	
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	
	disconnect($con);
	exit();
}

if($action=="delivery_challan_print")
{
	echo load_html_head_contents("Delivery Challan Info", "../../", 1, 1,'','','');
	extract($_REQUEST);	
	
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$datas=explode('_',$data);
	$program_ids = $datas[0];
	$source_ids = $datas[1];
	$company = $datas[2];
	$from_date = $datas[3];
	$to_date = $datas[4];
	$in_out_data=explode(',',$datas[1]);
	//echo $from_date;
	$company_details=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$country_arr=return_library_array( "select id,country_name from lib_country", "id", "country_name");
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
	//$poNumber_arr=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
	
	$machine_details=array();
	$machine_data=sql_select("select id, machine_no, dia_width from lib_machine_name");
	foreach($machine_data as $row)
	{
		$machine_details[$row[csf('id')]]['no']=$row[csf('machine_no')];
		$machine_details[$row[csf('id')]]['dia']=$row[csf('dia_width')];
	}
	
	$po_array=array();
	$po_data=sql_select("select a.job_no, a.job_no_prefix_num, a.style_ref_no, b.id, b.po_number as po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company group by b.po_number, a.job_no, a.job_no_prefix_num, a.style_ref_no, b.id");
	foreach($po_data as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no_prefix_num')];
		$po_array[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
	}
	
	$composition_arr=array();
	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}
	
	$knit_plan_arr=array();
	$plan_data=sql_select("select id, color_range, stitch_length from ppl_planning_info_entry_dtls");
	foreach($plan_data as $row)
	{
		$knit_plan_arr[$row[csf('id')]]['cr']=$row[csf('color_range')];
		$knit_plan_arr[$row[csf('id')]]['sl']=$row[csf('stitch_length')]; 
	}	
	
	?>
	<div style="width:1360px;">
		<table width="1350" cellspacing="0" align="center" border="0">
			<tr>
				<td colspan="17" align="center" style="font-size:x-large"><strong><?php echo $company_details[$company]; ?></strong></td>
			</tr>
			<tr class="form_caption">
				<td colspan="17" align="center">
					<?php
						$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$company"); 
						foreach ($nameArray as $result)
						{ 
						?>
							Plot No: <?php echo $result['plot_no']; ?> 
							Level No: <?php echo $result['level_no']?>
							Road No: <?php echo $result['road_no']; ?> 
							Block No: <?php echo $result['block_no'];?> 
							City No: <?php echo $result['city'];?> 
							Zip Code: <?php echo $result['zip_code']; ?> 
							Province No: <?php echo $result['province'];?> 
							Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
							Email Address: <?php echo $result['email'];?> 
							Website No: <?php echo $result['website'];
						}
					?> 
				</td>
			</tr>
			<tr>
				<td colspan="17" align="center" style="font-size:18px"><strong><u>Delivery Challan</u></strong></center></td>
			</tr>
			<tr>
				<td colspan="17" align="center" style="font-size:16px"><strong><u>Knitting Section</u></strong></center></td>
			</tr>
            <tr >
				<td colspan="17"  style="font-size:14px"><strong><?php echo "Date Range :"." ". $from_date." "."To"." ".$to_date; ?></strong></center></td>
			</tr>
        </table>
    </div>
    <div style="width:100%;">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1350" class="rpt_table" >
            <thead>
                <tr>
                    <th width="30" >SL</th>
                    <th width="60" >Job No</th>
                    <th width="90" >Order No</th>
                    <th width="60" >Buyer</th>
                    <th width="50" >Prod. ID</th>
                    <th width="60" >M/C No</th>
                    <th width="60" >Req. No</th>
                    <th width="90" >Booking No/ Prog. No</th>
                    <th width="60" >Yarn Count</th>
                    <th width="70" >Yarn Brand</th>
                    <th width="70" >Lot No</th>
                    <th width="100" >Color</th>
                    <th width="" >Fabric Type</th>
                    <th width="50" >Stich</th>
                    <th width="50" >Fin GSM</th>
                    <th width="50" >Fab. Dia</th>
                    <th width="50" >M/C Dia</th>
                    <th width="50" >Total Roll</th>
                    <th width="70" >Total Qty</th>
                </tr>
            </thead>
        </table>
    <div style="width:1350px">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1350" class="rpt_table" >
          
	<?php	
	$machine_arr=return_library_array( "select id, machine_no from lib_machine_name", "id", "machine_no" );
	$reqsn_details=return_library_array( "select knit_id, requisition_no from ppl_yarn_requisition_entry group by knit_id", "knit_id", "requisition_no"  );
	
	if($db_type==2) $date_cond="'".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."'";
	if($db_type==0) $date_cond="'".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'";
	if($in_out_data[0]==1)
	{
		$sql="select c.id, b.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_id, a.booking_no, a.buyer_id, a.remarks, b.prod_id, b.febric_description_id, b.gsm, b.width,  b.yarn_lot, b.yarn_count, b.brand_id, b.machine_no_id, c.po_breakdown_id,sum(case when c.entry_form=2 then b.no_of_roll else 0 end)  as roll_no, sum(case when c.entry_form=2 then c.quantity else 0 end)  as outqntyshift  
		from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c 
		where a.item_category=13 and a.id=b.mst_id and b.id=c.dtls_id and c.entry_form=2 and c.trans_type=1 and a.company_id=$company and a.knitting_source=1 and a.receive_date between $date_cond and a.recv_number_prefix_num in ($program_ids) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0  
		group by a.recv_number, a.receive_basis, a.receive_date, a.booking_id, b.prod_id, b.yarn_lot, b.yarn_count, b.brand_id, c.po_breakdown_id, c.id, b.id, a.recv_number_prefix_num, a.knitting_source, a.knitting_company, a.booking_no, a.buyer_id, a.remarks, b.febric_description_id, b.gsm, b.width, b.machine_no_id	
		order by a.receive_date";	
	}
	else
	{
		$sql="select c.id, b.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_id, a.booking_no, a.buyer_id, a.remarks, b.prod_id, b.febric_description_id, b.gsm, b.width,  b.yarn_lot, b.yarn_count, b.brand_id, b.machine_no_id, c.po_breakdown_id, sum(case when c.entry_form=2 then c.quantity else 0 end)  as outqntyshift  from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.knitting_source=3 and a.item_category=13 and a.id=b.mst_id and b.id=c.dtls_id and c.entry_form=2 and c.trans_type=1 and a.company_id=$company and a.receive_date between $date_cond and a.recv_number_prefix_num in ($program_ids) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0  
		group by a.recv_number, a.receive_basis, a.receive_date, a.booking_id, b.prod_id, b.yarn_lot, b.yarn_count, b.brand_id, c.po_breakdown_id, c.id, b.id, a.recv_number_prefix_num, a.knitting_source, a.knitting_company, a.booking_no, a.buyer_id, a.remarks, b.febric_description_id, b.gsm, b.width, b.machine_no_id 
		order by b.floor_id,a.receive_date";	
	}
	//echo $sql;
	$nameArray=sql_select( $sql); $i=1; $tot_roll=0; $tot_qty=0;
	foreach($nameArray as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
		$count='';
		$yarn_count=explode(",",$row[csf('yarn_count')]);
		foreach($yarn_count as $count_id)
		{
			if($count=='') $count=$yarn_count_details[$count_id]; else $count.=",".$yarn_count_details[$count_id];
		}
		
		$reqsn_no=""; $stich_length=""; $color="";
		if($row[csf('receive_basis')]==2)
		{
			$reqsn_no=$reqsn_details[$row[csf('booking_id')]]; 
			$stich_length=$knit_plan_arr[$row[csf('booking_id')]]['sl']; 
			$color=$color_range[$knit_plan_arr[$row[csf('booking_id')]]['cr']];
		}
	?>
        <tr bgcolor="<?php echo $bgcolor; ?>">
        	<td width="30"><div style="word-wrap:break-word; width:30px;"><?php echo $i; ?></div></td>
            <td width="60"><div style="word-wrap:break-word; width:60px;"><?php echo $po_array[$row[csf('po_breakdown_id')]]['job_no']; ?></div></td>
            <td width="90"><div style="word-wrap:break-word; width:90px;"><?php echo $po_array[$row[csf('po_breakdown_id')]]['no']; ?></div></td>
            <td width="60"><div style="word-wrap:break-word; width:60px;"><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></div></td>
            <td width="50"><div style="word-wrap:break-word; width:50px;"><?php echo $row[csf('recv_number_prefix_num')]; ?></div></td>
            <td width="60" align="center"><div style="word-wrap:break-word; width:60px;"><?php echo $machine_arr[$row[csf('machine_no_id')]]; ?></div></td>
            <td width="60"><div style="word-wrap:break-word; width:60px;"><?php echo $reqsn_no; ?></div></td>
            <td width="90"><div style="word-wrap:break-word; width:90px;"><?php echo $row[csf('booking_no')]; ?></div></td>
            <td width="60"><div style="word-wrap:break-word; width:60px;"><?php echo $count; ?></div></td>
            <td width="70"><div style="word-wrap:break-word; width:70px;"><?php echo $brand_details[$row[csf('brand_id')]]; ?></div></td>
            <td width="70"><div style="word-wrap:break-word; width:70px;"><?php echo $row[csf('yarn_lot')]; ?></div></td>
            <td width="100"><div style="word-wrap:break-word; width:100px;"><?php echo $color; ?></div></td>
            <td width=""><div style="word-wrap:break-word; width:210px;"><?php echo $composition_arr[$row[csf('febric_description_id')]];; ?></div></td>
            <td width="50"><div style="word-wrap:break-word; width:50px;"><?php echo $stich_length; ?></div></td>
            <td width="50"><div style="word-wrap:break-word; width:50px;"><?php echo $row[csf('gsm')]; ?></div></td>
            <td width="50"><div style="word-wrap:break-word; width:50px;"><?php echo $row[csf('width')]; ?></div></td>
            <td width="50"><div style="word-wrap:break-word; width:50px;"><?php echo $machine_details[$row[csf('machine_no_id')]]['dia']; ?></div></td>
            <td width="50" align="right"><div style="word-wrap:break-word; width:50px;"><?php echo $row[csf('roll_no')]; $tot_roll+=$row[csf('roll_no')]; ?>&nbsp;</div></td>
            <td width="70" align="right"><div style="word-wrap:break-word; width:70px;"><?php echo $row[csf('outqntyshift')]; $tot_qty+=$row[csf('outqntyshift')]; ?>&nbsp;</div></td>
        </tr>
    <?php
		$i++;
	}
	?>
        	<tr> 
                <td align="right" colspan="17" ><strong>Total:</strong></td>
                <td align="right"><?php echo number_format($tot_roll,2,'.',''); ?>&nbsp;</td>
                <td align="right" ><?php echo number_format($tot_qty,2,'.',''); ?>&nbsp;</td>
			</tr>
            <tr>
                <td colspan="2" align="left"><b>Remarks: </b></td>
                <td colspan="17" ><?php //echo number_to_words($tot_qty); ?>&nbsp;</td>
            </tr>
		</table>
        <br>
		 <?php
            echo signature_table(44, $company, "1340px");
         ?>
	</div>
	</div>
	<?php
    exit();
}
?>