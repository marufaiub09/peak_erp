-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_pre_export_lc_wise_dtls`
--

DROP TABLE IF EXISTS `com_pre_export_lc_wise_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_pre_export_lc_wise_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pre_export_dtls_id` int(11) NOT NULL DEFAULT '0',
  `export_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: LC, 2: SC',
  `lc_sc_id` int(11) NOT NULL DEFAULT '0',
  `currency_id` tinyint(3) NOT NULL DEFAULT '0',
  `amount` double NOT NULL,
  `conversion_rate` double NOT NULL,
  `equivalent_fc` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_pre_export_lc_wise_dtls`
--

LOCK TABLES `com_pre_export_lc_wise_dtls` WRITE;
/*!40000 ALTER TABLE `com_pre_export_lc_wise_dtls` DISABLE KEYS */;
INSERT INTO `com_pre_export_lc_wise_dtls` VALUES (1,1,1,29,2,1,78,0.0128),(2,2,1,21,2,621882,77.08,8068.0073),(3,3,1,21,2,621882,77.08,8068.0073),(4,4,1,21,2,621882,77.08,8068.0073);
/*!40000 ALTER TABLE `com_pre_export_lc_wise_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
