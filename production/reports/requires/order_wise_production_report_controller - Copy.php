<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location", 110, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select --", $selected, "load_drop_down( 'requires/date_wise_production_report_controller', this.value, 'load_drop_down_floor', 'floor_td' );",0 );     	 
}

if ($action=="load_drop_down_floor")
{
	echo create_drop_down( "cbo_floor", 110, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 and location_id='$data' order by floor_name","id,floor_name", 1, "-- Select --", $selected, "",0 );     	 
}

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 110, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- Select --", $selected, "" );     	 
	exit();
}

if($action=="report_generate")
{ 
	
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$company_short_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name"  );
 	$buyer_short_library=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
 	//$location_library=return_library_array( "select id,location_name from lib_location", "id", "location_name"  ); 
	//$floor_library=return_library_array( "select id,floor_name from lib_prod_floor", "id", "floor_name"  ); 
	$line_library=return_library_array( "select id,line_name from lib_sewing_line", "id", "line_name"  ); 
 	$country_library=return_library_array( "select id,country_name from lib_country", "id", "country_name"  );

 	/*if($template==1)
	{
	*/	
		
		//print_r($_REQUEST);die;
		$garments_nature=str_replace("'","",$cbo_garments_nature);
		if($garments_nature==1)$garments_nature="";
		$type = str_replace("'","",$cbo_type);
		if(str_replace("'","",$cbo_company_name)==0)$company_name=""; else $company_name=" and b.company_name=$cbo_company_name";
		if(str_replace("'","",$cbo_buyer_name)==0)$buyer_name="";else $buyer_name=" and b.buyer_name=$cbo_buyer_name";
		
		if(str_replace("'","",$cbo_location)==0)$location="";else $location=" and c.location=$cbo_location";
		if(str_replace("'","",$cbo_floor)==0)$floor="";else $floor=" and c.floor_id=$cbo_floor";
		
		if(str_replace("'","",trim($txt_order_no))!="") $search_string="%".str_replace("'","",trim($txt_order_no))."%"; else $search_string="%%";
		
		if(str_replace("'","",trim($txt_date_from))=="" || str_replace("'","",trim($txt_date_to))=="")$txt_date="";
		else $txt_date=" and a.pub_shipment_date between $txt_date_from and $txt_date_to";
		$fromDate = change_date_format( str_replace("'","",trim($txt_date_from)) );
		$toDate = change_date_format( str_replace("'","",trim($txt_date_to)) );
		
		
		if($type==2 || $type==4) //------------------------------------Show Date Location Floor & Line Wise $type==2
		{
 			$groupByCond = "group by a.id,c.location,c.floor_id order by a.id,c.location,c.floor_id";
  		}
		else //--------------------------------------------Show Order Wise  $type==1
		{ 
 			$groupByCond = "group by a.id order by a.pub_shipment_date,a.id";
 		}
			ob_start(); 
		?>
                <div style="width:1850px"> 
                    <table width="1000"  cellspacing="0" >
                        <tr class="form_caption" style="border:none;">
                                <td align="center" style="border:none;font-size:16px; font-weight:bold" >
                                <?php 
									if($type==1) echo "Order Wise Production Report";
									else if($type==2) echo "Order Location & Floor Wise Production Report";
									else if($type==3) echo "Order Country Wise Production Report";
									else echo "Order Country Location & Floor Wise Production Report";
																	  
								?>    
                                </td>
                         </tr>
                        <tr style="border:none;">
                                <td align="center" style="border:none; font-size:14px;">
                                    Company Name : <?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?>                                
                                </td>
                          </tr>
                          <tr style="border:none;">
                                <td align="center" style="border:none;font-size:12px; font-weight:bold">
                                    <?php echo "From $fromDate To $toDate" ;?>
                                </td>
                          </tr>
                    </table>
                
                     <div style="float:left; width:1150px">
                    	<table width="1100" cellspacing="0" border="1" class="rpt_table" rules="all" id="" >
                            <thead>
                            	<tr>
                                	<th colspan="14" >In-House Order Production </th>
                                </tr>
                                <tr>
                                    <th width="30">Sl.</th>    
                                    <th width="80">Buyer Name</th>
                                    <th width="80">Order Qty.(Pcs)</th>
                                    <th width="80">PO Value</th>
                                    <th width="80">Total Cut Qty</th>
                                    <th width="80">Total  Emb. Rcv. Qty</th>
                                    <th width="80">Total Sew Input Qty</th>
                                    <th width="80">Total Sew Qty</th>
                                    <th width="80">Total Iron Qty</th>
                                    <th width="80">Total Re-Iron Qty</th>
                                    <th width="80">Total Finish Qty</th>
                                    <th width="80">Fin Goods Status</th>
                                    <th width="80">Ex-Fac</th>
                                    <th width="">Ex-Fac%</th>
                                 </tr>
                            </thead>
                        </table>
                        <div style="max-height:425px; overflow-y:scroll; width:1120px" >
                            <table cellspacing="0" border="1" class="rpt_table"  width="1100" rules="all" id="" >
                            <?php
                             $total_po_quantity=0;$total_po_value=0;
                             $total_cut=0;$total_print_iss=0;
                             $total_print_re=0; $total_sew_input=0;
                             $total_sew_out=0;$total_iron=0; $total_re_iron=0;
                             $total_finish=0;$total_ex_factory=0;
                             $i=1;
							 
							 if($type==1 || $type==3) $location_cond=''; else $location_cond=$location;
                             
 							 // garments nature here -------------------------------							
							 if($garments_nature==1 || $garments_nature=="") $garmentsNature="";else $garmentsNature=" and b.garments_nature=$garments_nature";
							 
							 $exfactory_res = sql_select("select b.buyer_name,sum(c.ex_factory_qnty) as ex_factory_qnty 
							 					from  
													wo_po_details_master b, wo_po_break_down a, pro_ex_factory_mst c
												where 
													a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $txt_date $company_name $buyer_name $location_cond $garmentsNature group by b.buyer_name");
                             $exfactory_arr=array(); 
                             foreach($exfactory_res as $resRow){
                                $exfactory_arr[$resRow[csf("buyer_name")]] = $resRow[csf("ex_factory_qnty")];
                            }
                            //print_r($exfactory_arr);die;
							                           
							$pro_date_sql=sql_select("SELECT b.company_name, b.buyer_name,sum(a.po_quantity*b.total_set_qnty) as po_quantity,sum(a.po_total_price) as po_total_price,a.id         
                                        from 
                                            lib_buyer d, wo_po_details_master b, wo_po_break_down a 
                                        where 
                                            a.job_no_mst=b.job_no and b.buyer_name=d.id and a.status_active=1 and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature group by b.buyer_name order by d.buyer_name ASC"); 
                           //echo $pro_date_sql;die; $location $floor
                            foreach($pro_date_sql as $pro_date_sql_row)
                            {
                                 
                                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                                $production_mst_sql= sql_select("SELECT  
                                            sum(CASE WHEN production_type ='1' THEN production_quantity ELSE 0 END) AS cutting_qnty,
                                            sum(CASE WHEN production_type ='2' THEN production_quantity ELSE 0 END) AS printing_qnty,
                                            sum(CASE WHEN production_type ='3' THEN production_quantity ELSE 0 END) AS printreceived_qnty,
                                            sum(CASE WHEN production_type ='4' THEN production_quantity ELSE 0 END) AS sewingin_qnty,
                                            sum(CASE WHEN production_type ='5' THEN production_quantity ELSE 0 END) AS sewingout_qnty,                                          
                                            sum(CASE WHEN production_type ='7' THEN production_quantity ELSE 0 END) AS iron_qnty,
											sum(CASE WHEN production_type ='7' THEN re_production_qty ELSE 0 END) AS re_iron_qnty,
											sum(CASE WHEN production_type ='8' THEN production_quantity ELSE 0 END) AS finish_qnty 
                                        from 
                                            wo_po_details_master b, wo_po_break_down a, pro_garments_production_mst c
                                        where  
                                            a.job_no_mst=b.job_no and a.id=c.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.buyer_name=".$pro_date_sql_row[csf("buyer_name")]." $txt_date $company_name $garmentsNature");
                            //echo $production_mst_sql;die;
                            foreach($production_mst_sql as $row)
                            {
                             
                            ?>
                           
                                <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                                    <td width="30"><?php echo $i;?></td>
                                    <td width="80"><?php echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                                    <td width="80" align="right"><?php echo number_format($pro_date_sql_row[csf("po_quantity")]);?></td>
                                    <td width="80" align="right"><?php echo number_format($pro_date_sql_row[csf("po_total_price")],2);?></td>
                                    
                                    <td width="80" align="right"><?php echo number_format($row[csf("cutting_qnty")]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($row[csf("printreceived_qnty")]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($row[csf("sewingin_qnty")]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($row[csf("sewingout_qnty")]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($row[csf("iron_qnty")]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($row[csf("re_iron_qnty")]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($row[csf("finish_qnty")]); ?></td>
                                    <?php $finish_gd_status = ($row[csf("finish_qnty")]/$pro_date_sql_row[csf("po_quantity")])*100; ?>
                                    <td width="80" align="right"><?php echo number_format($finish_gd_status,2)." %"; ?></td>
                                    <td width="80" align="right"><?php echo number_format($exfactory_arr[$pro_date_sql_row[csf("buyer_name")]]); ?></td>
                                    <?php $ex_gd_status = ($exfactory_arr[$pro_date_sql_row[csf("buyer_name")]]/$pro_date_sql_row[csf("po_quantity")])*100; ?>
                                    <td width="" align="right"><?php echo  number_format($ex_gd_status,2)." %"; ?></td>
                                </tr>	
                                
                                <?php		
                                    
                                    $total_po_quantity+=$pro_date_sql_row[csf("po_quantity")];
                                    $total_po_value+=$pro_date_sql_row[csf("po_total_price")];
                                    $total_cut+=$row[csf("cutting_qnty")];
                                    $total_print_re+=$row[csf("printreceived_qnty")];
                                    $total_sew_input+=$row[csf("sewingin_qnty")];
                                    $total_sew_out+=$row[csf("sewingout_qnty")];
                                    $total_iron+=$row[csf("iron_qnty")];
									$total_re_iron+=$row[csf("re_iron_qnty")];
                                    $total_finish+=$row[csf("finish_qnty")];
                                    $total_ex_factory+=$exfactory_arr[$pro_date_sql_row[csf("buyer_name")]];
                                      	
                               } //end foreach 2nd
                            
                            $i++;
                            
                        }//end foreach 1st
                            $chart_data_qnty="Order Qty;".$total_po_quantity."\n"."Cutting;".$total_cut."\n"."Sew In;".$total_sew_input."\n"."Sew Out ;".$total_sew_out."\n"."Iron ;".$total_iron."\n"."Finish ;".$total_finish."\n"."Ex-Fact;".$total_ex_factory."\n";
                        ?>
                        </table>
                        <input type="hidden" id="graph_data" value="<?php echo substr($chart_data_qnty,0,-1); ?>"/>
                         <table border="1" class="tbl_bottom"  width="1100" rules="all" id="" >
                                 <tr> 
                                    <td width="30">&nbsp;</td> 
                                    <td width="80">Total</td> 
                                    <td width="80" id="tot_po_quantity"><?php echo number_format($total_po_quantity); ?></td> 
                                    <td width="80" id="tot_po_value"><?php echo number_format($total_po_value); ?></td> 
                                    <td width="80" id="tot_cutting"><?php echo number_format($total_cut); ?></td>
                                    <td width="80" id="tot_emb_rcv"><?php echo number_format($total_print_re); ?></td> 
                                    <td width="80" id="tot_sew_in"><?php echo number_format($total_sew_input); ?></td> 
                                    <td width="80" id="tot_sew_out"><?php echo number_format($total_sew_out); ?></td>   
                                    <td width="80" id="tot_iron"><?php echo number_format($total_iron); ?></td> 
                                    <td width="80" id="tot_re_iron"><?php echo number_format($total_re_iron); ?></td> 
                                    <td width="80" id="tot_finish"><?php echo number_format($total_finish); ?></td>
                                    <?php $total_finish_gd_status = ($total_finish/$total_po_quantity)*100; ?>
                                    <td width="80"><?php echo number_format($total_finish_gd_status,2); ?></td >
                                    <td width="80"><?php echo number_format($total_ex_factory); ?></td >
                                    <?php $total_ex_status = ($total_ex_factory/$total_po_quantity)*100; ?>
                                    <td width=""><?php echo number_format($total_ex_status,2); ?></td>
                                 </tr>
                         </table>
                     </div>
                     </div>
                     <div style="float:left; width:600px">   
                        <table>
                        	<tr>
                        		<td height="21" width="600"><div id="chartdiv"> </div></td>
                          </tr>    
                        </table>
                      </div>
                      <div style="clear:both"></div>
                      <br />
                      <?php
					  if($type==1 || $type==2)
					  {
					  ?>
                      	<div>
                      		<table width="2280" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                                <thead>
                                    <tr>
                                        <th width="30">SL</th>    
                                        <th width="80">Order Number</th>
                                        <th width="80">Buyer Name</th>
                                        <th width="90">Job Number</th>
                                        <th width="100">Style Name</th>
                                        <th width="130">Item Name</th>
                                        <th width="80">Order Qty.</th>
                                        <th width="80">Ship Date</th>
                                        <?php 
										if($type==2) 
										{ 
										?>
                                        	<th width="80">Location</th>
                                            <th width="80">Floor</th>
                                        <?php 
										} 
										?>
                                        <th width="80">Ex-Factory Date</th>
                                        <th width="80">Delay</th>
                                        <th width="80">Stan. Exc. Cut %</th>
                                        <th width="80">Total Cut Qty</th>
                                        <th width="80">Actual Exc. Cut %</th>
                                        <th width="80">Total Emb. Issue Qty</th>
                                        <th width="80">Total Emb. Rcv. Qty</th>
                                        <th width="80">Total Sew Input Qty</th>
                                        <th width="80">Total Sew Output Qty</th>
                                        <th width="80">Total Iron Qty</th>
                                        <th width="80">Total Re-Iron Qty</th>
                                        <th width="80">Total Finish Qty</th>
                                        <th width="80">Fin Goods Status</th>
                                        <th width="80">Reject Qty</th>
                                        <th width="80">Total Out</th>
                                        <th width="80">Shortage/ Excess</th>
                                        <th width="80">Status</th>
                                        <th width="">Remarks</th>
                                     </tr>
                                </thead>
                            </table>
                            <div style="max-height:425px; overflow-y:scroll; width:2300px" id="scroll_body">
                                <table border="1" class="rpt_table"  width="2280" rules="all" id="table_body" >
                                	
                                  <?php 
								  	//sql_select
									if($type==1) //--------------------------------------------Show Order Wise  $type==1 
									{
										$order_sql= sql_select("select a.id, b.job_no_prefix_num, a.po_number, a.po_quantity, a.job_no_mst, a.pub_shipment_date as shipment_date,a.shiping_status,a.excess_cut,a.plan_cut, b.company_name, b.buyer_name, b.set_break_down, b.style_ref_no,
											sum(CASE WHEN c.po_break_down_id=a.id THEN c.ex_factory_qnty ELSE 0 END) AS ex_factory_qnty,
											MAX(c.ex_factory_date) AS ex_factory_date
										from 
											wo_po_details_master b,wo_po_break_down a left join pro_ex_factory_mst c on c.po_break_down_id=a.id and c.status_active=1 
										where 
											a.job_no_mst=b.job_no and a.status_active=1 and a.po_number like '$search_string' and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature $groupByCond ");
											
/*											echo "select a.id, a.po_number, a.po_quantity, a.job_no_mst, a.pub_shipment_date as shipment_date,a.shiping_status,a.excess_cut,a.plan_cut, b.company_name, b.buyer_name, b.set_break_down, b.style_ref_no,
											sum(CASE WHEN c.po_break_down_id=a.id THEN c.ex_factory_qnty ELSE 0 END) AS ex_factory_qnty,
											MAX(c.ex_factory_date) AS ex_factory_date
										from 
											wo_po_details_master b,wo_po_break_down a left join pro_ex_factory_mst c on c.po_break_down_id=a.id and c.status_active=1 
										where 
											a.job_no_mst=b.job_no and a.status_active=1 and a.po_number like '$search_string' and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature $groupByCond";
*/									}
								    else if($type==2)  //------------------------------------Show Date Location Floor & Line Wise $type==2
									{
										 
										$location_library=return_library_array( "select id, location_name from lib_location",'id','location_name');
										$floor_library=return_library_array( "select id, floor_name from lib_prod_floor",'id','floor_name');
										
										$order_sql= sql_select("select a.id, b.job_no_prefix_num, a.po_number, a.po_quantity, a.job_no_mst, a.pub_shipment_date as shipment_date,a.shiping_status,a.excess_cut,a.plan_cut, b.company_name, b.buyer_name, b.set_break_down, b.style_ref_no,c.location,c.floor_id
										from 
											wo_po_details_master b,wo_po_break_down a left join pro_garments_production_mst c on c.po_break_down_id=a.id and c.status_active=1 $location $floor
										where 
											a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature $groupByCond");
 										
									}
								
								  // echo $order_sql;die;
								   $i=0;$k=0;
								   foreach($order_sql as $orderRes)
								   {
									   $i++;
									   if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
									   
									   $setArr = explode("__",$orderRes[csf("set_break_down")] );
									   $countArr = count($setArr); 
									   if($countArr==0)$countArr=1; 
									   for($j=0;$j<$countArr;$j++)
									   {
										   $setItemArr = explode("_",$setArr[$j]);
										   $item_id=$setItemArr[0];
										   $set_qnty=$setItemArr[1];
										   if($item_id>0)
										   {
											   $k++;
											   $po_quantity_in_pcs = $orderRes[csf("po_quantity")]*$set_qnty;
											   
											   ?>
												<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $k; ?>" style="height:20px">
													<td width="30" ><?php if($j>0) echo "<p style='display:none' >".$i."</p>";else echo $i; ?></td>    
													<td width="80"><p><?php if($j>0) echo "<p style='display:none' >".$orderRes[csf("po_number")]."</p>";else echo $orderRes[csf("po_number")]; ?></p></td>
													<td width="80"><?php if($j>0) echo "<p style='display:none' >".$buyer_short_library[$orderRes[csf("buyer_name")]]."</p>";else echo $buyer_short_library[$orderRes[csf("buyer_name")]]; ?></td>
													<td width="90" align="center"><p><?php if($j>0) echo "<p style='display:none' >".$orderRes[csf("job_no_prefix_num")]."</p>";else echo $orderRes[csf("job_no_prefix_num")]; ?></p></td>
													<td width="100"><p><?php if($j>0) echo "<p style='display:none' >".$orderRes[csf("style_ref_no")]."</p>";else echo $orderRes[csf("style_ref_no")]; ?></p></td>
													<td width="130"><p><?php echo $garments_item[$item_id];?></p></td>
													<td width="80" align="right"><a href="##" onclick="openmypage_order(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,'0','OrderPopup')"><?php echo $po_quantity_in_pcs;  ?></a></td>
													
													<?php
													//------------------------------------------
													   if($type==1)// order wise
													   {
														   $sqlEx = sql_select("select MAX(ex_factory_date) AS ex_factory_date,sum(ex_factory_qnty) AS ex_factory_qnty from pro_ex_factory_mst where status_active=1 and po_break_down_id=".$orderRes[csf("id")]." and item_number_id=$item_id");
														   $orderRes[ex_factory_date]=$sqlEx[0][csf('ex_factory_date')];
														   $orderRes[ex_factory_qnty]=$sqlEx[0][csf('ex_factory_qnty')];
														  // echo "select MAX(ex_factory_date) AS ex_factory_date,sum(ex_factory_qnty) AS ex_factory_qnty from pro_ex_factory_mst where status_active=1 and po_break_down_id=".$orderRes[csf("id")]." and item_number_id=$item_id";
													   }
													   if($type==2)// order location floor wise
													   { 
														   $sqlEx = sql_select("select MAX(ex_factory_date) AS ex_factory_date,sum(ex_factory_qnty) AS ex_factory_qnty from pro_ex_factory_mst where status_active=1 and po_break_down_id=".$orderRes[csf("id")]." and item_number_id=$item_id" );// and location='".$orderRes[csf("location")]."'." and floor_id=".$orderRes[csf("floor_id")]
														   $orderRes[ex_factory_date]=$sqlEx[0][csf('ex_factory_date')];
														   $orderRes[ex_factory_qnty]=$sqlEx[0][csf('ex_factory_qnty')];
													   }
													   //------------------------------------------
													$ex_factory_date = $orderRes[csf("ex_factory_date")];
													$date=date("Y-m-d");$color="";$days_remian="";
													if($orderRes[csf("shiping_status")]==1 || $orderRes[csf("shiping_status")]==2)
													{
															$days_remian=datediff("d",$date,$orderRes[csf("shipment_date")]); 
															if($orderRes[csf("shipment_date")] > $date) 
															{
																$color="";
															}
															else if($row1[shipment_date] < $date) 
															{
																$color="red";
															}														
															else if($orderRes[csf("shipment_date")] >= $date && $days_remian<=5 ) 
															{
																$color="orange";
															}
															
													} 
													else if($orderRes[csf("shiping_status")]==3)
													{
														$days_remian=datediff("d",$ex_factory_date,$orderRes[csf("shipment_date")]);
														if($orderRes[csf("shipment_date")] >= $ex_factory_date) 
														{ 
															$color="green";
														}
														else if($orderRes[csf("shipment_date")] < $ex_factory_date) 
														{ 
															$color="#2A9FFF";
														}
														
													}//end if condition
													?>
													<td width="80" bgcolor="<?php echo $color; ?>"><?php echo change_date_format($orderRes[csf("shipment_date")]);  ?></td>
													
													<?php if($type==2) { ?>
														<td width="80"><?php echo $location_library[$orderRes[csf("location")]]; ?></td>
														<td width="80"><?php echo $floor_library[$orderRes[csf("floor_id")]]; ?></td>
													<?php 
														$location_mst=" and c.location='".$orderRes[csf("location")]."'";
														$floor_mst=" and c.floor_id='".$orderRes[csf("floor_id")]."'";
														} ?>
													
													<td width="80"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,'<?php echo $item_id; ?>','exfactory','','','','0')">
													<?php if(!($ex_factory_date=="" || $ex_factory_date=="0000-00-00")) echo change_date_format($ex_factory_date);  ?></a></td>
													<td width="80" align="center" title="<?php echo $days_remian; ?>"><?php if($orderRes[csf("shiping_status")]==3)echo $days_remian; else echo "---"; ?></td>
													<?php $orderRes[csf("excess_cut")] = number_format($orderRes[csf("excess_cut")],2);?>
													<td width="80"><?php echo $orderRes[csf("excess_cut")]." %"; ?></td>
													<?php
													 
													$prod_sql= sql_select("SELECT c.id,c.location,c.floor_id, 
														IFNULL(sum(CASE WHEN c.production_type ='1' THEN  c.production_quantity  ELSE 0 END),0) AS cutting_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='2' THEN  c.production_quantity  ELSE 0 END),0) AS printing_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='3' THEN  c.production_quantity  ELSE 0 END),0) AS printreceived_qnty, 
														IFNULL(sum(CASE WHEN c.production_type ='4' THEN  c.production_quantity  ELSE 0 END),0) AS sewingin_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='5' THEN  c.production_quantity  ELSE 0 END),0) AS sewingout_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.production_quantity  ELSE 0 END),0) AS iron_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.re_production_qty  ELSE 0 END),0) AS re_iron_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='8' THEN  c.production_quantity  ELSE 0 END),0) AS finish_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='8' THEN c.reject_qnty ELSE 0 END),0) AS finish_rej_qnty,
														IFNULL(sum(CASE WHEN c.production_type ='5' THEN c.reject_qnty ELSE 0 END),0) AS sewingout_rej_qnty
													 
													from 
														pro_garments_production_mst c
													where  
														c.po_break_down_id =".$orderRes[csf("id")]." and c.item_number_id='$item_id' and c.status_active=1 $location_mst $floor_mst ");
													//echo $prod_sql."-------------------------";die;
													foreach($prod_sql as $proRes);
														$actual_exces_cut = $proRes[csf("cutting_qnty")];//$proRes[csf("cutting_qnty")]+($proRes[csf("cutting_qnty")]*($orderRes[csf("excess_cut")]/100));
														if($actual_exces_cut < $po_quantity_in_pcs) $actual_exces_cut=""; else $actual_exces_cut=number_format( (($actual_exces_cut-$po_quantity_in_pcs)/$po_quantity_in_pcs)*100,2)."%";
													
														//$embl_issue_total = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$orderRes[csf("id")]." and production_type=2 and status_active=1 group by embel_name order by id ASC limit 1");
														//$embl_receive_total = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$orderRes[csf("id")]." and production_type=3 and status_active=1 group by embel_name order by id ASC limit 1");
														$sql=sql_select("select sum(production_quantity) as production_quantity,
															SUM(CASE WHEN embel_name=1 THEN production_quantity ELSE 0 END) AS print,  
															SUM(CASE WHEN embel_name=2 THEN production_quantity ELSE 0 END) AS emb,
															SUM(CASE WHEN embel_name=3 THEN production_quantity ELSE 0 END) AS wash,
															SUM(CASE WHEN embel_name=4 THEN production_quantity ELSE 0 END) AS special 
															from pro_garments_production_mst where po_break_down_id=".$orderRes[csf("id")]." and item_number_id='$item_id' and production_type=2 and status_active=1 order by id ASC limit 1");
														//print_r($sql);	
														foreach($sql as $res);
														$issue_print = $res[csf("print")];
														$issue_emb = $res[csf("emb")];
														$issue_wash = $res[csf("wash")];
														$issue_special = $res[csf("special")];
														$embl_issue_total="";$total_rej_value=0;$rej_value=0;	
														if($issue_print!=0) $embl_issue_total .= 'PR='.$issue_print;
														if($issue_emb!=0) $embl_issue_total .= ', EM='.$issue_emb;
														if($issue_wash!=0) $embl_issue_total .= ', WA='.$issue_wash;
														if($issue_special!=0) $embl_issue_total .= ', SP='.$issue_special;
														
														
														$sql=sql_select("select sum(production_quantity) as production_quantity,
															SUM(CASE WHEN embel_name=1 THEN production_quantity ELSE 0 END) AS print,  
															SUM(CASE WHEN embel_name=2 THEN production_quantity ELSE 0 END) AS emb,
															SUM(CASE WHEN embel_name=3 THEN production_quantity ELSE 0 END) AS wash,
															SUM(CASE WHEN embel_name=4 THEN production_quantity ELSE 0 END) AS special 
															from pro_garments_production_mst where po_break_down_id=".$orderRes[csf("id")]." and item_number_id='$item_id' and production_type=3 and status_active=1 order by id ASC limit 1");
														foreach($sql as $res);
														$rcv_print = $res[csf("print")];
														$rcv_emb = $res[csf("emb")];
														$rcv_wash = $res[csf("wash")];
														$rcv_special = $res[csf("special")];
														$embl_receive_total="";	
														if($rcv_print!=0) $embl_receive_total .= 'PR='.$rcv_print;
														if($rcv_emb!=0) $embl_receive_total .= ', EM='.$rcv_emb;
														if($rcv_wash!=0) $embl_receive_total .= ', WA='.$rcv_wash;
														if($rcv_special!=0) $embl_receive_total .= ', SP='.$rcv_special;	
													?>
														 
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,1,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $proRes[csf("cutting_qnty")]; ?></a></td>
														<td width="80" align="right" <?php if(round($actual_exces_cut) > round($orderRes[csf("excess_cut")])) echo "bgcolor='#FF0000'"; ?>><?php echo $actual_exces_cut; ?></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,2,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $embl_issue_total;//$proRes[csf("printing_qnty")] ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,3,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $embl_receive_total;//$proRes[csf("printreceived_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,4,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $proRes[csf("sewingin_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,5,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $proRes[csf("sewingout_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,7,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $proRes[csf("iron_qnty")]; ?></a></td>
                                                        <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,9,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $proRes[csf("re_iron_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,8,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,'')"><?php echo $proRes[csf("finish_qnty")]; ?></a></td>
														<?php $finish_status = $proRes[csf("finish_qnty")]*100/$po_quantity_in_pcs; ?>
														<td width="80" align="right"><?php echo number_format($finish_status,2)." %"; ?></td>
                                                        <td width="80" align="right" ><a href="##" onclick="openmypage_rej(<?php echo $orderRes[csf("id")]; ?>,<?php echo $item_id;?>,'reject_qty')"><?php $rej_value=$proRes[csf("finish_rej_qnty")]+$proRes[csf("sewingout_rej_qnty")]; echo $rej_value; $total_rej_value+=$rej_value; ?></a></td>
														<td width="80" align="right"><?php echo $orderRes[csf("ex_factory_qnty")]; ?></td>
														<?php $shortage = $po_quantity_in_pcs-$orderRes[csf("ex_factory_qnty")]; ?>
														<td width="80" align="right"><?php echo $shortage; ?></td>
														<td width="80"><?php echo $shipment_status[$orderRes[csf("shiping_status")]]; ?></td>
														<td width="">
														<?php
															echo "<a href='##'  onclick=\"openmypage_remark(".$orderRes[csf("id")].",".$item_id.",0,'date_wise_production_report');\">Veiw</a>";
														?>
														</td>
												 </tr>
												 
												<?php
										   }
											
									   } //end for loop
									   
								   }// end main foreach 
								   
								  ?>  
                                    </table>	
                                    <table border="1" class="tbl_bottom"  width="2380" rules="all" id="report_table_footer_1" >
                                         	<tr>
                                        		<td width="30"></td>
                                                <td width="80"></td>
                                                <td width="80"></td><td width="90"></td><td width="100"></td>
                                                <td width="130">Total</td>
                                                <td width="80" id="total_order_quantity"></td>
												<td width="80"></td>
                                                <?php if($type==2) { ?>
                                                    <td width="80"></td>
                                                    <td width="80"></td>
                                                <?php } ?>
                                                <td width="80"></td>
												<td width="80"></td>
                                                <td width="80"></td>
												<td width="80" id="total_cutting"></td>
												<td width="80"></td>
 												<td width="80" id="total_emb_issue"></td>
												<td width="80" id="total_emb_receive"></td>
												<td width="80" id="total_sewing_input"></td>
												<td width="80" id="total_sewing_out"></td>
												<td width="80" id="total_iron_qnty"></td>
                                                <td width="80" id="total_re_iron_qnty"></td>
												<td width="80" id="total_finish_qnty"></td>
												<td width="80"></td>
                                                <td width="80" align="right" id="total_rej_value_td"><?php //echo number_format($total_rej_value,2); ?> </td >
												<td width="80" id="total_out"></td>
												<td width="80" id="total_shortage"></td>
                                                <td width="80"></td>
 												<td width=""></td>
 											 </tr>
                                 </table>
                            </div>
                     	<?php
					  	}
					  	else
					  	{
						?>
                            <div>
                      		<table width="2380" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                                <thead>
                                    <tr>
                                        <th width="30">SL</th>    
                                        <th width="100">Order Number</th>
                                        <th width="100">Buyer Name</th>
                                        <th width="100">Job Number</th>
                                        <th width="100">Style Name</th>
                                        <th width="150">Item Name</th>
                                        <th width="80">Country</th>
                                        <th width="80">Order Qty.</th>
                                        <th width="80">Ship Date</th>
                                        <?php 
										if($type==4) 
										{ 
										?>
                                        	<th width="80">Location</th>
                                            <th width="80">Floor</th>
                                        <?php 
										} 
										?>
                                        <th width="80">Ex-Factory Date</th>
                                        <th width="80">Delay</th>
                                        <th width="80">Stan. Exc. Cut %</th>
                                        <th width="80">Total Cut Qty</th>
                                        <th width="80">Actual Exc. Cut %</th>
                                        <th width="80">Total Emb. Issue Qty</th>
                                        <th width="80">Total Emb. Rcv. Qty</th>
                                        <th width="80">Total Sew Input Qty</th>
                                        <th width="80">Total Sew Output Qty</th>
                                        <th width="80">Total Iron Qty</th>
                                        <th width="80">Total Re-Iron Qty</th>
                                        <th width="80">Total Finish Qty</th>
                                        <th width="80">Fin Goods Status</th>
                                        <th width="80">Reject Qty</th>
                                        <th width="80">Total Out</th>
                                        <th width="80">Shortage/ Excess</th>
                                        <th width="80">Status</th>
                                        <th width="">Remarks</th>
                                     </tr>
                                </thead>
                            </table>
                            <div style="max-height:425px; overflow-y:scroll; width:2400px" id="scroll_body">
                                <table border="1" class="rpt_table"  width="2380" rules="all" id="table_body" >
                                	
                                  <?php 
								  	$po_country_arr=return_library_array( "select po_break_down_id, group_concat(distinct(country_id)) as country from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id",'po_break_down_id','country');
									
									$po_country_data_arr=array();
									$poCountryData=sql_select( "select po_break_down_id, item_number_id, country_id, sum(order_quantity) as qnty from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id, item_number_id, country_id");
									
									foreach($poCountryData as $row)
									{
										$po_country_data_arr[$row[csf('po_break_down_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]=$row[csf('qnty')];
									}
								  	//sql_select
									if($type==3) //--------------------------------------------Show Order Country Wise  $type==3 
									{
										$order_sql= sql_select("select a.id, a.po_number, a.po_quantity, a.job_no_mst, a.pub_shipment_date as shipment_date,a.shiping_status,a.excess_cut,a.plan_cut, b.company_name, b.job_no_prefix_num, b.buyer_name, b.set_break_down, b.style_ref_no,
											sum(CASE WHEN c.po_break_down_id=a.id THEN c.ex_factory_qnty ELSE 0 END) AS ex_factory_qnty,
											MAX(c.ex_factory_date) AS ex_factory_date
										from 
											wo_po_details_master b,wo_po_break_down a left join pro_ex_factory_mst c on c.po_break_down_id=a.id and c.status_active=1 
										where 
											a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature $groupByCond");
									}
								    else if($type==4)  //------------------------------------Show Order Country Location Floor & Line Wise $type==4
									{
										 
										$location_library=return_library_array( "select id, location_name from lib_location",'id','location_name');
										$floor_library=return_library_array( "select id, floor_name from lib_prod_floor",'id','floor_name');
										
										$order_sql= sql_select("select a.id, a.po_number, a.po_quantity, a.job_no_mst, a.pub_shipment_date as shipment_date,a.shiping_status,a.excess_cut,a.plan_cut, b.company_name, b.job_no_prefix_num, b.buyer_name, b.set_break_down, b.style_ref_no,c.location,c.floor_id
										from 
											wo_po_details_master b,wo_po_break_down a left join pro_garments_production_mst c on c.po_break_down_id=a.id and c.status_active=1 $location $floor
										where 
											a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature $groupByCond");
 										
									}
								
								  // echo $order_sql;die;
								   $i=0;$k=0;
								   foreach($order_sql as $orderRes)
								   {
									     
									   $i++;
									   if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
									   
									   $setArr = explode("__",$orderRes[csf("set_break_down")] );
									   $countArr = count($setArr); 
									   if($countArr==0) $countArr=1; $s=0;
									   for($j=0;$j<$countArr;$j++)
									   {
										   $setItemArr = explode("_",$setArr[$j]);
										   $item_id=$setItemArr[0];
										   $set_qnty=$setItemArr[1];
										   if($item_id>0)
										   {
											   $country=explode(",",$po_country_arr[$orderRes[csf("id")]]);
											   //$numOfCountry = count($country)."=".$orderRes[csf("id")]."<br>";
											   
											   foreach($country as $country_id)
											   {
												   $k++;
												   //$po_quantity_in_pcs = $orderRes[csf("po_quantity")]*$set_qnty;
												   $po_quantity_in_pcs = $po_country_data_arr[$orderRes[csf("id")]][$item_id][$country_id]
												   
												   
												   ?>
													<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $k; ?>" style="height:20px">
														<td width="30" ><?php if($s>0) echo "<p style='display:none' >".$i."</p>";else echo $i; ?></td>    
														<td width="100"><p><?php if($s>0) echo "<p style='display:none' >".$orderRes[csf("po_number")]."</p>";else echo $orderRes[csf("po_number")]; ?></p></td>
														<td width="100"><?php if($s>0) echo "<p style='display:none' >".$buyer_short_library[$orderRes[csf("buyer_name")]]."</p>";else echo $buyer_short_library[$orderRes[csf("buyer_name")]]; ?></td>
														<td width="100" align="center"><p><?php if($s>0) echo "<p style='display:none' >".$orderRes[csf("job_no_mst")]."</p>";else echo $orderRes[csf("job_no_prefix_num")]; ?></p></td>
														<td width="100"><p><?php if($s>0) echo "<p style='display:none' >".$orderRes[csf("style_ref_no")]."</p>";else echo $orderRes[csf("style_ref_no")]; ?></p></td>
														<td width="150"><p><?php echo $garments_item[$item_id];?></p></td>
														<td width="80"><p><?php echo $country_library[$country_id]; ?></p></td>
														<td width="80" align="right"><a href="##" onclick="openmypage_order(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,<?php echo $country_id;?>,'OrderPopupCountry')"><?php echo $po_quantity_in_pcs;  ?></a></td>
														
													<?php
													//------------------------------------------
													   if($type==3)// order wise
													   {
														   $sqlEx = sql_select("select MAX(ex_factory_date) AS ex_factory_date,sum(ex_factory_qnty) AS ex_factory_qnty from pro_ex_factory_mst where status_active=1 and po_break_down_id=".$orderRes[csf("id")]." and item_number_id=$item_id and country_id='$country_id'");
														   $orderRes[ex_factory_date]=$sqlEx[0][csf('ex_factory_date')];
														   $orderRes[ex_factory_qnty]=$sqlEx[0][csf('ex_factory_qnty')];
													   }
													   if($type==4)// order location floor wise
													   { 
														   $sqlEx = sql_select("select MAX(ex_factory_date) AS ex_factory_date,sum(ex_factory_qnty) AS ex_factory_qnty from pro_ex_factory_mst where status_active=1 and po_break_down_id=".$orderRes[csf("id")]." and item_number_id=$item_id and country_id='$country_id'" );// and location='".$orderRes[csf("location")]."'." and floor_id=".$orderRes[csf("floor_id")]
														   $orderRes[ex_factory_date]=$sqlEx[0][csf('ex_factory_date')];
														   $orderRes[ex_factory_qnty]=$sqlEx[0][csf('ex_factory_qnty')];
													   }
													   //------------------------------------------
														$ex_factory_date = $orderRes[csf("ex_factory_date")];
														$date=date("Y-m-d");$color="";$days_remian="";
														if($orderRes[csf("shiping_status")]==1 || $orderRes[csf("shiping_status")]==2)
														{
															$days_remian=datediff("d",$date,$orderRes[csf("shipment_date")]); 
															if($orderRes[csf("shipment_date")] > $date) 
															{
																$color="";
															}
															else if($row1[shipment_date] < $date) 
															{
																$color="red";
															}														
															else if($orderRes[csf("shipment_date")] >= $date && $days_remian<=5 ) 
															{
																$color="orange";
															}
															
														} 
														else if($orderRes[csf("shiping_status")]==3)
														{
															$days_remian=datediff("d",$ex_factory_date,$orderRes[csf("shipment_date")]);
															if($orderRes[csf("shipment_date")] >= $ex_factory_date) 
															{ 
																$color="green";
															}
															else if($orderRes[csf("shipment_date")] < $ex_factory_date) 
															{ 
																$color="#2A9FFF";
															}
															
														}//end if condition
														?>
														<td width="80" bgcolor="<?php echo $color; ?>"><?php echo change_date_format($orderRes[csf("shipment_date")]);  ?></td>
														
														<?php if($type==4) { ?>
															<td width="80"><?php echo $location_library[$orderRes[csf("location")]]; ?></td>
															<td width="80"><?php echo $floor_library[$orderRes[csf("floor_id")]]; ?></td>
														<?php 
															$location_mst=" and c.location='".$orderRes[csf("location")]."'";
															$floor_mst=" and c.floor_id='".$orderRes[csf("floor_id")]."'";
															} ?>
														
														<td width="80"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,'<?php echo $item_id; ?>','exfactoryCountry','','','',<?php echo $country_id; ?>)">
														<?php if(!($ex_factory_date=="" || $ex_factory_date=="0000-00-00")) echo change_date_format($ex_factory_date);  ?></a></td>
														<td width="80" align="center" title="<?php echo $days_remian; ?>"><?php if($orderRes[csf("shiping_status")]==3)echo $days_remian; else echo "---"; ?></td>
														<?php $orderRes[csf("excess_cut")] = number_format($orderRes[csf("excess_cut")],2);?>
														<td width="80"><?php echo $orderRes[csf("excess_cut")]." %"; ?></td>
														<?php
														 
														$prod_sql= sql_select("SELECT c.id, c.location,c.floor_id, 
															IFNULL(sum(CASE WHEN c.production_type ='1' THEN  c.production_quantity  ELSE 0 END),0) AS cutting_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='2' THEN  c.production_quantity  ELSE 0 END),0) AS printing_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='3' THEN  c.production_quantity  ELSE 0 END),0) AS printreceived_qnty, 
															IFNULL(sum(CASE WHEN c.production_type ='4' THEN  c.production_quantity  ELSE 0 END),0) AS sewingin_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='5' THEN  c.production_quantity  ELSE 0 END),0) AS sewingout_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.production_quantity  ELSE 0 END),0) AS iron_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.re_production_qty  ELSE 0 END),0) AS re_iron_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='8' THEN  c.production_quantity  ELSE 0 END),0) AS finish_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='8' THEN c.reject_qnty ELSE 0 END),0) AS finish_rej_qnty,
															IFNULL(sum(CASE WHEN c.production_type ='5' THEN c.reject_qnty ELSE 0 END),0) AS sewingout_rej_qnty
													 
														from 
															pro_garments_production_mst c
														where  
															c.po_break_down_id =".$orderRes[csf("id")]." and c.item_number_id='$item_id' and c.country_id='$country_id' and c.status_active=1 $location_mst $floor_mst ");
														//echo $prod_sql."-------------------------";die;
														foreach($prod_sql as $proRes);
														$actual_exces_cut = $proRes[csf("cutting_qnty")];//$proRes[csf("cutting_qnty")]+($proRes[csf("cutting_qnty")]*($orderRes[csf("excess_cut")]/100));
														if($actual_exces_cut < $po_quantity_in_pcs) $actual_exces_cut=""; else $actual_exces_cut=number_format( (($actual_exces_cut-$po_quantity_in_pcs)/$po_quantity_in_pcs)*100,2)."%";
													
														//$embl_issue_total = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$orderRes[csf("id")]." and production_type=2 and status_active=1 group by embel_name order by id ASC limit 1");
														//$embl_receive_total = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$orderRes[csf("id")]." and production_type=3 and status_active=1 group by embel_name order by id ASC limit 1");
														$sql=sql_select("select sum(production_quantity) as production_quantity,
															SUM(CASE WHEN embel_name=1 THEN production_quantity ELSE 0 END) AS print,  
															SUM(CASE WHEN embel_name=2 THEN production_quantity ELSE 0 END) AS emb,
															SUM(CASE WHEN embel_name=3 THEN production_quantity ELSE 0 END) AS wash,
															SUM(CASE WHEN embel_name=4 THEN production_quantity ELSE 0 END) AS special 
															from pro_garments_production_mst where po_break_down_id=".$orderRes[csf("id")]." and item_number_id='$item_id' and country_id='$country_id' and production_type=2 and status_active=1 order by id ASC limit 1");
														//print_r($sql);	
														foreach($sql as $res);
														$issue_print = $res[csf("print")];
														$issue_emb = $res[csf("emb")];
														$issue_wash = $res[csf("wash")];
														$issue_special = $res[csf("special")];
														$embl_issue_total="";	
														if($issue_print!=0) $embl_issue_total .= 'PR='.$issue_print;
														if($issue_emb!=0) $embl_issue_total .= ', EM='.$issue_emb;
														if($issue_wash!=0) $embl_issue_total .= ', WA='.$issue_wash;
														if($issue_special!=0) $embl_issue_total .= ', SP='.$issue_special;
														
														
														$sql=sql_select("select sum(production_quantity) as production_quantity,
															SUM(CASE WHEN embel_name=1 THEN production_quantity ELSE 0 END) AS print,  
															SUM(CASE WHEN embel_name=2 THEN production_quantity ELSE 0 END) AS emb,
															SUM(CASE WHEN embel_name=3 THEN production_quantity ELSE 0 END) AS wash,
															SUM(CASE WHEN embel_name=4 THEN production_quantity ELSE 0 END) AS special 
															from pro_garments_production_mst where po_break_down_id=".$orderRes[csf("id")]." and item_number_id='$item_id' and country_id='$country_id' and production_type=3 and status_active=1 order by id ASC limit 1");
														foreach($sql as $res);
														$rcv_print = $res[csf("print")];
														$rcv_emb = $res[csf("emb")];
														$rcv_wash = $res[csf("wash")];
														$rcv_special = $res[csf("special")];
														$embl_receive_total="";	
														if($rcv_print!=0) $embl_receive_total .= 'PR='.$rcv_print;
														if($rcv_emb!=0) $embl_receive_total .= ', EM='.$rcv_emb;
														if($rcv_wash!=0) $embl_receive_total .= ', WA='.$rcv_wash;
														if($rcv_special!=0) $embl_receive_total .= ', SP='.$rcv_special;	
													?>
														 
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,1,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("cutting_qnty")]; ?></a></td>
														<td width="80" align="right" <?php if(round($actual_exces_cut) > round($orderRes[csf("excess_cut")])) echo "bgcolor='#FF0000'"; ?>><?php echo $actual_exces_cut; ?></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,2,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $embl_issue_total;//$proRes[csf("printing_qnty")] ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,3,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $embl_receive_total;//$proRes[csf("printreceived_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,4,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("sewingin_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,5,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("sewingout_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,7,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("iron_qnty")]; ?></a></td>
                                                        <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,9,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("re_iron_qnty")]; ?></a></td>
														<td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,8,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("finish_qnty")]; ?></a></td>
														<?php $finish_status = $proRes[csf("finish_qnty")]*100/$po_quantity_in_pcs; ?>
														<td width="80" align="right"><?php echo number_format($finish_status,2)." %"; ?></td>
                                                        <td width="80" align="right" ><a href="##" onclick="openmypage_rej(<?php echo $proRes[csf("id")]; ?>,'reject_qty')"><?php $rej_value=$proRes[csf("finish_rej_qnty")]+$proRes[csf("sewingout_rej_qnty")]; echo $rej_value; $total_rej_value+=$rej_value; ?></a></td>
														<td width="80" align="right"><?php echo $orderRes[csf("ex_factory_qnty")]; ?></td>
														<?php $shortage = $po_quantity_in_pcs-$orderRes[csf("ex_factory_qnty")]; ?>
														<td width="80" align="right"><?php echo $shortage; ?></td>
														<td width="80"><?php echo $shipment_status[$orderRes[csf("shiping_status")]]; ?></td>
														<td width="">
														<?php
															echo "<a href='##'  onclick=\"openmypage_remark(".$orderRes[csf("id")].",".$item_id.",'".$country_id."','date_wise_production_report_country');\">Veiw</a>";
														?>
														</td>
												 </tr>
												 
												<?php
												$s++;
											   }//end foreach loop
										   }
									   } //end for loop
									   
								   }// end main foreach 
								   
								  ?>  
                                    </table>	
                                    <table border="1" class="tbl_bottom"  width="2380" rules="all" id="report_table_footer_1" >
                                         	<tr>
                                        		<td width="30"></td><td width="100"></td><td width="100"></td><td width="100"></td><td width="100"></td>
                                                <td width="150"></td>
                                                <td width="80">Total</td>
                                                <td width="80" id="total_order_quantity"></td>
												<td width="80"></td>
                                                <?php if($type==4) { ?>
                                                    <td width="80"></td>
                                                    <td width="80"></td>
                                                <?php } ?>
                                                <td width="80"></td>
												<td width="80"></td>
                                                <td width="80"></td>
												<td width="80" id="total_cutting"></td>
												<td width="80"></td>
 												<td width="80" id="total_emb_issue"></td>
												<td width="80" id="total_emb_receive"></td>
												<td width="80" id="total_sewing_input"></td>
												<td width="80" id="total_sewing_out"></td>
												<td width="80" id="total_iron_qnty"></td>
                                                <td width="80" id="total_re_iron_qnty"></td>
												<td width="80" id="total_finish_qnty"></td>
												<td width="80"></td>
                                                <td width="80" align="right" id="total_rej_value_td"><?php //echo number_format($total_rej_value,2); ?> </td >
												<td width="80" id="total_out"></td>
												<td width="80" id="total_shortage"></td>
                                                <td width="80"></td>
 												<td width=""></td>
 											 </tr>
                                 </table>
                            </div> 
                        <?php
						}
						?>     
                      </div>
                      
                      
              <div style="display:none">
                <table width="2460" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                    <thead>
                        <tr>
                            <th width="30">SL</th>    
                            <th width="100">Order Number</th>
                            <th width="100">Buyer Name</th>
                            <th width="100">Job Number</th>
                            <th width="100">Style Name</th>
                            <th width="150">Item Name</th>
                            <th width="80">Country</th>
                            <th width="80">Order Qty.</th>
                            <th width="80">Ship Date</th>
                            <?php 
                            if($type==4) 
                            { 
                            ?>
                                <th width="80">Location</th>
                                <th width="80">Floor</th>
                            <?php 
                            } 
                            ?>
                            <th width="80">Ex-Factory Date</th>
                            <th width="80">Delay</th>
                            <th width="80">Stan. Exc. Cut %</th>
                            <th width="80">Total Cut Qty</th>
                            <th width="80">Actual Exc. Cut %</th>
                            <th width="80">Total Emb. Issue Qty</th>
                            <th width="80">Total Emb. Rcv. Qty</th>
                            <th width="80">Total Sew Input Qty</th>
                            <th width="80">Total Sew Output Qty</th>
                            <th width="80">Total Iron Qty</th>
                            <th width="80">Total Re-Iron Qty</th>
                            <th width="80">Total Finish Qty</th>
                            <th width="80">Fin Goods Status</th>
                            <th width="80">Reject Qty</th>
                            <th width="80">Total Out</th>
                            <th width="80">Shortage/ Excess</th>
                            <th width="80">Status</th>
                            <th width="">Remarks</th>
                         </tr>
                    </thead>
                </table>
                <div style="max-height:425px; overflow-y:scroll; width:2380px" id="scroll_body">
                    <table border="1" class="rpt_table"  width="2362" rules="all" id="table_body" >
                        
                      <?php 
                        $po_country_arr=return_library_array( "select po_break_down_id, group_concat(distinct(country_id)) as country from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id",'po_break_down_id','country');
                        
                        $po_country_data_arr=array();
                        $poCountryData=sql_select( "select po_break_down_id, item_number_id, country_id, sum(order_quantity) as qnty from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id, item_number_id, country_id");
                        
                        foreach($poCountryData as $row)
                        {
                            $po_country_data_arr[$row[csf('po_break_down_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]=$row[csf('qnty')];
                        }
                        //sql_select
                        if($type==3) //--------------------------------------------Show Order Country Wise  $type==3 
                        {
                            $order_sql= sql_select("select a.id, a.po_number, a.po_quantity, a.job_no_mst, a.pub_shipment_date as shipment_date,a.shiping_status,a.excess_cut,a.plan_cut, b.company_name, b.job_no_prefix_num, b.buyer_name, b.set_break_down, b.style_ref_no,
                                sum(CASE WHEN c.po_break_down_id=a.id THEN c.ex_factory_qnty ELSE 0 END) AS ex_factory_qnty,
                                MAX(c.ex_factory_date) AS ex_factory_date
                            from 
                                wo_po_details_master b,wo_po_break_down a left join pro_ex_factory_mst c on c.po_break_down_id=a.id and c.status_active=1 
                            where 
                                a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature $groupByCond");
                        }
                        else if($type==4)  //------------------------------------Show Order Country Location Floor & Line Wise $type==4
                        {
                             
                            $location_library=return_library_array( "select id, location_name from lib_location",'id','location_name');
                            $floor_library=return_library_array( "select id, floor_name from lib_prod_floor",'id','floor_name');
                            
                            $order_sql= sql_select("select a.id, a.po_number, a.po_quantity, a.job_no_mst, a.pub_shipment_date as shipment_date,a.shiping_status,a.excess_cut,a.plan_cut, b.company_name, b.job_no_prefix_num, b.buyer_name, b.set_break_down, b.style_ref_no,c.location,c.floor_id
                            from 
                                wo_po_details_master b,wo_po_break_down a left join pro_garments_production_mst c on c.po_break_down_id=a.id and c.status_active=1 $location $floor
                            where 
                                a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 $txt_date $company_name $buyer_name $garmentsNature $groupByCond");
                            
                        }
                    
                      // echo $order_sql;die;
                       $i=0;$k=0;
                       foreach($order_sql as $orderRes)
                       {
                             
                           $i++;
                           if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
                           
                           $setArr = explode("__",$orderRes[csf("set_break_down")] );
                           $countArr = count($setArr); 
                           if($countArr==0) $countArr=1; $s=0;
                           for($j=0;$j<$countArr;$j++)
                           {
                               $setItemArr = explode("_",$setArr[$j]);
                               $item_id=$setItemArr[0];
                               $set_qnty=$setItemArr[1];
                               if($item_id>0)
                               {
                                   $country=explode(",",$po_country_arr[$orderRes[csf("id")]]);
                                   //$numOfCountry = count($country)."=".$orderRes[csf("id")]."<br>";
                                   
                                   foreach($country as $country_id)
                                   {
                                       $k++;
                                       //$po_quantity_in_pcs = $orderRes[csf("po_quantity")]*$set_qnty;
                                       $po_quantity_in_pcs = $po_country_data_arr[$orderRes[csf("id")]][$item_id][$country_id]
                                       
                                       
                                       ?>
                                        <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $k; ?>" style="height:20px">
                                            <td width="30" ><?php if($s>0) echo "<p style='display:none' >".$i."</p>";else echo $i; ?></td>    
                                            <td width="100"><p><?php if($s>0) echo "<p style='display:none' >".$orderRes[csf("po_number")]."</p>";else echo $orderRes[csf("po_number")]; ?></p></td>
                                            <td width="100"><?php if($s>0) echo "<p style='display:none' >".$buyer_short_library[$orderRes[csf("buyer_name")]]."</p>";else echo $buyer_short_library[$orderRes[csf("buyer_name")]]; ?></td>
                                            <td width="100" align="center"><p><?php if($s>0) echo "<p style='display:none' >".$orderRes[csf("job_no_mst")]."</p>";else echo $orderRes[csf("job_no_prefix_num")]; ?></p></td>
                                            <td width="100"><p><?php if($s>0) echo "<p style='display:none' >".$orderRes[csf("style_ref_no")]."</p>";else echo $orderRes[csf("style_ref_no")]; ?></p></td>
                                            <td width="150"><p><?php echo $garments_item[$item_id];?></p></td>
                                            <td width="80"><p><?php echo $country_library[$country_id]; ?></p></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage_order(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,<?php echo $country_id;?>,'OrderPopupCountry')"><?php echo $po_quantity_in_pcs;  ?></a></td>
                                            
                                        <?php
                                        //------------------------------------------
                                           if($type==3)// order wise
                                           {
                                               $sqlEx = sql_select("select MAX(ex_factory_date) AS ex_factory_date,sum(ex_factory_qnty) AS ex_factory_qnty from pro_ex_factory_mst where status_active=1 and po_break_down_id=".$orderRes[csf("id")]." and item_number_id=$item_id and country_id='$country_id'");
                                               $orderRes[ex_factory_date]=$sqlEx[0][csf('ex_factory_date')];
                                               $orderRes[ex_factory_qnty]=$sqlEx[0][csf('ex_factory_qnty')];
                                           }
                                           if($type==4)// order location floor wise
                                           { 
                                               $sqlEx = sql_select("select MAX(ex_factory_date) AS ex_factory_date,sum(ex_factory_qnty) AS ex_factory_qnty from pro_ex_factory_mst where status_active=1 and po_break_down_id=".$orderRes[csf("id")]." and item_number_id=$item_id and country_id='$country_id'" );// and location='".$orderRes[csf("location")]."'." and floor_id=".$orderRes[csf("floor_id")]
                                               $orderRes[ex_factory_date]=$sqlEx[0][csf('ex_factory_date')];
                                               $orderRes[ex_factory_qnty]=$sqlEx[0][csf('ex_factory_qnty')];
                                           }
                                           //------------------------------------------
                                            $ex_factory_date = $orderRes[csf("ex_factory_date")];
                                            $date=date("Y-m-d");$color="";$days_remian="";
                                            if($orderRes[csf("shiping_status")]==1 || $orderRes[csf("shiping_status")]==2)
                                            {
                                                $days_remian=datediff("d",$date,$orderRes[csf("shipment_date")]); 
                                                if($orderRes[csf("shipment_date")] > $date) 
                                                {
                                                    $color="";
                                                }
                                                else if($row1[shipment_date] < $date) 
                                                {
                                                    $color="red";
                                                }														
                                                else if($orderRes[csf("shipment_date")] >= $date && $days_remian<=5 ) 
                                                {
                                                    $color="orange";
                                                }
                                                
                                            } 
                                            else if($orderRes[csf("shiping_status")]==3)
                                            {
                                                $days_remian=datediff("d",$ex_factory_date,$orderRes[csf("shipment_date")]);
                                                if($orderRes[csf("shipment_date")] >= $ex_factory_date) 
                                                { 
                                                    $color="green";
                                                }
                                                else if($orderRes[csf("shipment_date")] < $ex_factory_date) 
                                                { 
                                                    $color="#2A9FFF";
                                                }
                                                
                                            }//end if condition
                                            ?>
                                            <td width="80" bgcolor="<?php echo $color; ?>"><?php echo change_date_format($orderRes[csf("shipment_date")]);  ?></td>
                                            
                                            <?php if($type==4) { ?>
                                                <td width="80"><?php echo $location_library[$orderRes[csf("location")]]; ?></td>
                                                <td width="80"><?php echo $floor_library[$orderRes[csf("floor_id")]]; ?></td>
                                            <?php 
                                                $location_mst=" and c.location='".$orderRes[csf("location")]."'";
                                                $floor_mst=" and c.floor_id='".$orderRes[csf("floor_id")]."'";
                                                } ?>
                                            
                                            <td width="80"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,'<?php echo $item_id; ?>','exfactoryCountry','','','',<?php echo $country_id; ?>)">
                                            <?php if(!($ex_factory_date=="" || $ex_factory_date=="0000-00-00")) echo change_date_format($ex_factory_date);  ?></a></td>
                                            <td width="80" align="center" title="<?php echo $days_remian; ?>"><?php if($orderRes[csf("shiping_status")]==3)echo $days_remian; else echo "---"; ?></td>
                                            <?php $orderRes[csf("excess_cut")] = number_format($orderRes[csf("excess_cut")],2);?>
                                            <td width="80"><?php echo $orderRes[csf("excess_cut")]." %"; ?></td>
                                            <?php
                                             
                                            $prod_sql= sql_select("SELECT c.location,c.floor_id, 
                                                IFNULL(sum(CASE WHEN c.production_type ='1' THEN  c.production_quantity  ELSE 0 END),0) AS cutting_qnty,
                                                IFNULL(sum(CASE WHEN c.production_type ='2' THEN  c.production_quantity  ELSE 0 END),0) AS printing_qnty,
                                                IFNULL(sum(CASE WHEN c.production_type ='3' THEN  c.production_quantity  ELSE 0 END),0) AS printreceived_qnty, 
                                                IFNULL(sum(CASE WHEN c.production_type ='4' THEN  c.production_quantity  ELSE 0 END),0) AS sewingin_qnty,
                                                IFNULL(sum(CASE WHEN c.production_type ='5' THEN  c.production_quantity  ELSE 0 END),0) AS sewingout_qnty,
                                                IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.production_quantity  ELSE 0 END),0) AS iron_qnty,
												IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.re_production_qty  ELSE 0 END),0) AS re_iron_qnty,
                                                IFNULL(sum(CASE WHEN c.production_type ='8' THEN  c.production_quantity  ELSE 0 END),0) AS finish_qnty,
												IFNULL(sum(CASE WHEN c.production_type ='8' THEN c.reject_qnty ELSE 0 END),0) AS finish_rej_qnty,
												IFNULL(sum(CASE WHEN c.production_type ='5' THEN c.reject_qnty ELSE 0 END),0) AS sewingout_rej_qnty
													 
                                            from 
                                                pro_garments_production_mst c
                                            where  
                                                c.po_break_down_id =".$orderRes[csf("id")]." and c.item_number_id='$item_id' and c.country_id='$country_id' and c.status_active=1 $location_mst $floor_mst ");
                                            //echo $prod_sql."-------------------------";die;
                                            foreach($prod_sql as $proRes);
                                            $actual_exces_cut = $proRes[csf("cutting_qnty")];//$proRes[csf("cutting_qnty")]+($proRes[csf("cutting_qnty")]*($orderRes[csf("excess_cut")]/100));
                                            if($actual_exces_cut < $po_quantity_in_pcs) $actual_exces_cut=""; else $actual_exces_cut=number_format( (($actual_exces_cut-$po_quantity_in_pcs)/$po_quantity_in_pcs)*100,2)."%";
                                        
                                            //$embl_issue_total = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$orderRes[csf("id")]." and production_type=2 and status_active=1 group by embel_name order by id ASC limit 1");
                                            //$embl_receive_total = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$orderRes[csf("id")]." and production_type=3 and status_active=1 group by embel_name order by id ASC limit 1");
                                            $sql=sql_select("select sum(production_quantity) as production_quantity,
                                                SUM(CASE WHEN embel_name=1 THEN production_quantity ELSE 0 END) AS print,  
                                                SUM(CASE WHEN embel_name=2 THEN production_quantity ELSE 0 END) AS emb,
                                                SUM(CASE WHEN embel_name=3 THEN production_quantity ELSE 0 END) AS wash,
                                                SUM(CASE WHEN embel_name=4 THEN production_quantity ELSE 0 END) AS special 
                                                from pro_garments_production_mst where po_break_down_id=".$orderRes[csf("id")]." and item_number_id='$item_id' and country_id='$country_id' and production_type=2 and status_active=1 order by id ASC limit 1");
                                            //print_r($sql);	
                                            foreach($sql as $res);
                                            $issue_print = $res[csf("print")];
                                            $issue_emb = $res[csf("emb")];
                                            $issue_wash = $res[csf("wash")];
                                            $issue_special = $res[csf("special")];
                                            $embl_issue_total="";	
                                            if($issue_print!=0) $embl_issue_total .= 'PR='.$issue_print;
                                            if($issue_emb!=0) $embl_issue_total .= ', EM='.$issue_emb;
                                            if($issue_wash!=0) $embl_issue_total .= ', WA='.$issue_wash;
                                            if($issue_special!=0) $embl_issue_total .= ', SP='.$issue_special;
                                            
                                            
                                            $sql=sql_select("select sum(production_quantity) as production_quantity,
                                                SUM(CASE WHEN embel_name=1 THEN production_quantity ELSE 0 END) AS print,  
                                                SUM(CASE WHEN embel_name=2 THEN production_quantity ELSE 0 END) AS emb,
                                                SUM(CASE WHEN embel_name=3 THEN production_quantity ELSE 0 END) AS wash,
                                                SUM(CASE WHEN embel_name=4 THEN production_quantity ELSE 0 END) AS special 
                                                from pro_garments_production_mst where po_break_down_id=".$orderRes[csf("id")]." and item_number_id='$item_id' and country_id='$country_id' and production_type=3 and status_active=1 order by id ASC limit 1");
                                            foreach($sql as $res);
                                            $rcv_print = $res[csf("print")];
                                            $rcv_emb = $res[csf("emb")];
                                            $rcv_wash = $res[csf("wash")];
                                            $rcv_special = $res[csf("special")];
                                            $embl_receive_total="";	
                                            if($rcv_print!=0) $embl_receive_total .= 'PR='.$rcv_print;
                                            if($rcv_emb!=0) $embl_receive_total .= ', EM='.$rcv_emb;
                                            if($rcv_wash!=0) $embl_receive_total .= ', WA='.$rcv_wash;
                                            if($rcv_special!=0) $embl_receive_total .= ', SP='.$rcv_special;	
                                        ?>
                                             
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,1,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("cutting_qnty")]; ?></a></td>
                                            <td width="80" align="right" <?php if(round($actual_exces_cut) > round($orderRes[csf("excess_cut")])) echo "bgcolor='#FF0000'"; ?>><?php echo $actual_exces_cut; ?></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,2,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $embl_issue_total;//$proRes[csf("printing_qnty")] ?></a></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,3,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $embl_receive_total;//$proRes[csf("printreceived_qnty")]; ?></a></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,4,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("sewingin_qnty")]; ?></a></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,5,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("sewingout_qnty")]; ?></a></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,7,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("iron_qnty")]; ?></a></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,9,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("re_iron_qnty")]; ?></a></td>
                                            <td width="80" align="right"><a href="##" onclick="openmypage(<?php echo $orderRes[csf("id")];?>,<?php echo $item_id;?>,8,'<?php echo $proRes[csf("location")];?>','<?php echo $proRes[csf("floor_id")];?>',<?php echo $type; ?>,<?php echo $country_id; ?>)"><?php echo $proRes[csf("finish_qnty")]; ?></a></td>
                                            <?php $finish_status = $proRes[csf("finish_qnty")]*100/$po_quantity_in_pcs; ?>
                                            <td width="80" align="right"><?php echo number_format($finish_status,2)." %"; ?></td>
                                            <td width="80" align="right" ><?php $rej_value=$proRes[csf("finish_rej_qnty")]+$proRes[csf("sewingout_rej_qnty")]; echo $rej_value; $total_rej_value+=$rej_value; ?></td>
                                            <td width="80" align="right"><?php echo $orderRes[csf("ex_factory_qnty")]; ?></td>
                                            <?php $shortage = $po_quantity_in_pcs-$orderRes[csf("ex_factory_qnty")]; ?>
                                            <td width="80" align="right"><?php echo $shortage; ?></td>
                                            <td width="80"><?php echo $shipment_status[$orderRes[csf("shiping_status")]]; ?></td>
                                            <td width="">
                                            <?php
                                                echo "<a href='##'  onclick=\"openmypage_remark(".$orderRes[csf("id")].",".$item_id.",'".$country_id."','date_wise_production_report_country');\">Veiw</a>";
                                            ?>
                                            </td>
                                     </tr>
                                     
                                    <?php
                                    $s++;
                                   }//end foreach loop
                               }
                           } //end for loop
                           
                       }// end main foreach 
                       
                      ?>  
                        </table>	
                        <table border="1" class="tbl_bottom"  width="2362" rules="all" id="report_table_footer_1" >
                                <tr>
                                    <td width="30"></td><td width="100"></td><td width="100"></td><td width="100"></td><td width="100"></td>
                                    <td width="150"></td>
                                    <td width="80">Total</td>
                                    <td width="80" id="total_order_quantity"></td>
                                    <td width="80"></td>
                                    <?php if($type==4) { ?>
                                        <td width="80"></td>
                                        <td width="80"></td>
                                    <?php } ?>
                                    <td width="80"></td>
                                    <td width="80"></td>
                                    <td width="80"></td>
                                    <td width="80" id="total_cutting"></td>
                                    <td width="80"></td>
                                    <td width="80" id="total_emb_issue"></td>
                                    <td width="80" id="total_emb_receive"></td>
                                    <td width="80" id="total_sewing_input"></td>
                                    <td width="80" id="total_sewing_out"></td>
                                    <td width="80" id="total_iron_qnty"></td>
                                    <td width="80" id="total_re_iron_qnty"></td>
                                    <td width="80" id="total_finish_qnty"></td>
                                    <td width="80"></td>
                                    <td width="80" align="right" id="total_rej_value_td"><?php //echo number_format($total_rej_value,2); ?> </td >
                                    <td width="80" id="total_out"></td>
                                    <td width="80" id="total_shortage"></td>
                                    <td width="80"></td>
                                    <td width=""></td>
                                 </tr>
                     </table>
                </div>                     
		<br /><br />		
		</div><!-- end main div -->

<?php

 		//-------------------------------------------END Show Date Wise------------------------
  		//-------------------------------------------END Show Date Location Floor & Line Wise------------------------
		//-------------------------------------------end-----------------------------------------------------------------------------//
		
					
		$html = ob_get_contents();
		ob_clean();
		$new_link=create_delete_report_file( $html, 1, 1, "../../../" );
		
 		echo "$html";
		exit();	
 	
}

 
if($action=='date_wise_production_report') 
{	
	extract($_REQUEST); 
 	echo load_html_head_contents("Remarks", "../../../", 1, 1,$unicode,'',''); 
	?>
	<fieldset>
    <legend>Cutting</legend>
    	<?php 
			 
			 $sql= "SELECT id,production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and production_type='1' and is_deleted=0 and status_active=1";
 			 //echo $sql;
			 echo  create_list_view ( "list_view_1", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
			
 		?>
    </fieldset>
    
    <fieldset>
    <legend>Print/Embr Issue</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and production_type='2' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_2", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    <fieldset>
    <legend>Print/Embr Receive</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and production_type='3' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_3", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    
    <fieldset>
    <legend>Sewing Input</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and production_type='4' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_4", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    
    <fieldset>
    <legend>Sewing Output</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and production_type='5' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_5", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    
    <fieldset>
    <legend>Finish Input</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and production_type='6' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_6", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    <fieldset>
    <legend>Finish Output</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and production_type='8' and is_deleted=0 and status_active=1";
			 
			  echo  create_list_view ( "list_view_7", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
   
<?php
exit();
}//end if 


if($action=='date_wise_production_report_country') 
{	
	extract($_REQUEST); 
 	echo load_html_head_contents("Remarks", "../../../", 1, 1,$unicode,'','');
 ?>
	<fieldset>
    <legend>Cutting</legend>
    	<?php 
			 
			 $sql= "SELECT id,production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id  and country_id='$country_id' and production_type='1' and is_deleted=0 and status_active=1";
 			 //echo $sql;
			 echo  create_list_view ( "list_view_1", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
			
 		?>
    </fieldset>
    
    <fieldset>
    <legend>Print/Embr Issue</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and country_id='$country_id' and production_type='2' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_2", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    <fieldset>
    <legend>Print/Embr Receive</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and country_id='$country_id' and production_type='3' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_3", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    
    <fieldset>
    <legend>Sewing Input</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and country_id='$country_id' and production_type='4' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_4", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    
    <fieldset>
    <legend>Sewing Output</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and country_id='$country_id' and production_type='5' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_5", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    
    <fieldset>
    <legend>Finish Input</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and country_id='$country_id' and production_type='6' and is_deleted=0 and status_active=1";
			  
			 echo  create_list_view ( "list_view_6", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
    
    <fieldset>
    <legend>Finish Output</legend>
    	<?php 
			 
			  $sql= "SELECT production_date,production_quantity,remarks from pro_garments_production_mst where po_break_down_id='$po_break_down_id' and item_number_id=$item_id and country_id='$country_id' and production_type='8' and is_deleted=0 and status_active=1";
			 
			  echo  create_list_view ( "list_view_7", "Date,Production Qnty,Remarks", "100,120,280","500","220",1, $sql, "", "","", 1, '0,0,0', $arr, "production_date,production_quantity,remarks", "../requires/order_wise_production_report_controller", '','3,1,0');
		?>
    </fieldset>
   
<?php
exit();
}//end if 

  
if ($action=='OrderPopup')
{
	echo load_html_head_contents("Order Wise Production Report", "../../../", 1, 1,$unicode,'','');
	$po_break_down_id=$_REQUEST['po_break_down_id'];
	$item_id=$_REQUEST['item_id'];
	
?>

	
 <div id="data_panel" align="center" style="width:100%">
         <script>
		 	function new_window()
			 {
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('details_reports').innerHTML);
				d.close();
			 }
         </script>
 	<input type="button" value="Print" id="print" class="formbutton" style="width:100px;" onclick="new_window()" />
 </div>
  
<div style="width:700px" align="center" id="details_reports"> 
 	<fieldset style="width:700px">
  	<legend>Color And Size Wise Summary</legend>
    <table id="tbl_id" class="rpt_table" width="600" border="1" rules="all" >
    	<thead>
        	<tr>
            	<th width="100">Buyer</th>
                <th width="100">Job Number</th>
                <th width="100">Style Name</th>
                <th width="100">Order Number</th>
                <th width="100">Ship Date</th>
                <th width="100">Order Qty.</th>
            </tr>
        </thead>
       	<?php
        	$buyer_short_library=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
 			$sql = sql_select("select a.job_no_mst,a.po_number,a.pub_shipment_date,a.po_quantity,a.packing,b.set_break_down, b.company_name, b.order_uom, b.buyer_name, b.style_ref_no,c.set_item_ratio 
					from wo_po_break_down a, wo_po_details_master b, wo_po_details_mas_set_details c 
					where a.job_no_mst=b.job_no and b.job_no=c.job_no and a.id='$po_break_down_id' and c.gmts_item_id=$item_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
			//echo $sql;
			foreach($sql as $resultRow);
			
			$cons_embr=return_field_value("sum(cons_dzn_gmts) as cons_dzn_gmts","wo_pre_cost_embe_cost_dtls","job_no='".$resultRow[csf("job_no_mst")]."' and status_active=1 and is_deleted=0","cons_dzn_gmts");
			
 		?> 
        <tr>
        	<td><?php echo $buyer_short_library[$resultRow[csf("buyer_name")]]; ?></td>
            <td><p><?php echo $resultRow[csf("job_no_mst")]; ?></p></td>
            <td><p><?php echo $resultRow[csf("style_ref_no")]; ?></p></td>
            <td><?php echo $resultRow[csf("po_number")]; ?></td>
            <td><?php echo change_date_format($resultRow[csf("pub_shipment_date")]); ?></td>
            <td><?php echo $resultRow[csf("po_quantity")]*$resultRow[csf("set_item_ratio")]; ?></td>
        </tr>
         <?php
         $prod_sewing_sql=sql_select("SELECT sum(alter_qnty) as alter_qnty, sum(reject_qnty) as reject_qnty from pro_garments_production_mst where production_type=5 and po_break_down_id ='$po_break_down_id' and is_deleted=0 and status_active=1");
		 foreach($prod_sewing_sql as $sewingRow);
		?> 	
        <tr>
        	<td colspan="2">Total Alter Sewing Qty : <b><?php echo $sewingRow[csf("alter_qnty")]; ?></b></td>
        	<td colspan="2">Total Reject Sewing Qty : <b><?php echo $sewingRow[csf("reject_qnty")]; ?></b></td>
            <td colspan="2">Pack Assortment: <b><?php echo $packing[$resultRow[csf("packing")]]; ?></b></td>
        </tr>
    </table>
	</fieldset>
    
    <?php
				  
	  $size_Arr_library=return_library_array( "select id,size_name from lib_size", "id", "size_name" );
	  $color_Arr_library=return_library_array( "select id,color_name from lib_color", "id", "color_name" );	
	  
	  $color_library=sql_select("select distinct(color_number_id) from wo_po_color_size_breakdown where po_break_down_id='$po_break_down_id' and color_mst_id!=0 and status_active=1 ");
	  $size_library=sql_select("select distinct(size_number_id) from wo_po_color_size_breakdown where po_break_down_id='$po_break_down_id' and size_mst_id!=0 and status_active=1");
	  $count = count($size_library);	
	  $width= $count*70+350; 		
	  	  
	?>
    <fieldset style="width:700px">
    <table id="tblDtls_id" class="rpt_table" width="<?php echo $width; ?>" border="1" rules="all" >
	 	<thead>
        	<tr>
            	<th width="100">Color Name</th>
                <th width="200">Production Type</th>
 				<?php
				foreach($size_library as $sizeRes)
				{
				 	?><th width="80"><?php echo $size_Arr_library[$sizeRes[csf("size_number_id")]]; ?></th><?php
				}
				?>
     		    <th width="60">Total</th>
           </tr>
        </thead>
        <?php
		  
		  foreach($color_library as $colorRes)
		  {
			?>	  
			<tr>
				<td rowspan="10"><?php echo $color_Arr_library[$colorRes[csf("color_number_id")]]; ?></td>
			
 			<?php
            	  $i=0;$j=0;$sqlPart="";
				  foreach($size_library as $sizeRes)
				  {
					  $i++;$j++;
					  if($i>1) $sqlPart .=",";
					  $sqlPart .= 'SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' and size_number_id='.$sizeRes[csf("size_number_id")].' THEN order_quantity ELSE 0 END ) as '."col".$i;
					  $sqlPart .= ',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' and size_number_id='.$sizeRes[csf("size_number_id")].' THEN plan_cut_qnty ELSE 0 END ) as '."pcut".$i;
					  $sqlPart .= ',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' and size_number_id='.$sizeRes[csf("size_number_id")].' THEN excess_cut_perc ELSE 0 END ) as '."excess_cut".$i;
				  }
				  if($j>1)
				  {
					 $sqlPart .=',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' THEN order_quantity ELSE 0 END ) as totalorderqnty';
					 $sqlPart .=',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' THEN plan_cut_qnty ELSE 0 END ) as totalplancutqnty';
				  }
		  
				$sql = sql_select("select avg(excess_cut_perc) as avg_excess_cut_perc,excess_cut_perc,". $sqlPart ." from wo_po_color_size_breakdown where status_active=1 and po_break_down_id='$po_break_down_id' and item_number_id=$item_id");
				//echo $sql;die;
				foreach($sql as $resRow); 
 					$bgcolor1="#E9F3FF"; 
					$bgcolor2="#FFFFFF";
				?>
					 
					<tr bgcolor="<?php echo $bgcolor1; ?>">
						<td><b>Order Quantity</b></td>	
                        <?php for($k=1;$k<=$i;$k++) {	$col = 'col'.$k; ?>	
                         	<td><?php echo $resRow[csf($col)]; ?></td>
						<?php } ?>
                         <td><?php echo $resRow[csf("totalorderqnty")]; ?></td> 
					</tr>
                    <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Plan To Cut (AVG <?php echo $resRow[csf("avg_excess_cut_perc")]; ?>)% </b></td>	
                        <?php for($k=1;$k<=$i;$k++){ $col = 'pcut'.$k;$excess_cut = 'excess_cut'.$k;	?>	
                         	<td title="Excess Cut <?php echo $resRow[csf($excess_cut)]; ?>%"><?php echo $resRow[csf($col)]; ?></td>
						<?php } ?>
                         <td><?php echo $resRow[csf("totalplancutqnty")]; ?></td> 
                    </tr>
					
                <?php
 				$total_cutting=0;$total_emb_issue=0;$total_emb_rcv=0;$total_sew_in=0;$total_sew_out=0;$total_fin_in=0;$total_fin_out=0;$total_iron_out=0;
				$cutting_html='';$embiss_html='';$embrcv_html='';$sewin_html='';$sewout_html='';$finisin_html='';$finisout_html='';$iron_html='';
				$k=0;
				foreach($size_library as $sizeRes)
				{
					$k++;
					$prod_sql= sql_select("SELECT  
							IFNULL(sum(CASE WHEN c.production_type ='1' THEN  c.production_qnty  ELSE 0 END),0) AS cutting_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='2' THEN  c.production_qnty  ELSE 0 END),0) AS printing_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='3' THEN  c.production_qnty  ELSE 0 END),0) AS printreceived_qnty, 
							IFNULL(sum(CASE WHEN c.production_type ='4' THEN  c.production_qnty  ELSE 0 END),0) AS sewingin_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='5' THEN  c.production_qnty  ELSE 0 END),0) AS sewingout_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='6' THEN  c.production_qnty  ELSE 0 END),0) AS finishin_qnty, 
							IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.production_qnty  ELSE 0 END),0) AS iron_qnty,
							
							IFNULL(sum(CASE WHEN c.production_type ='8' THEN  c.production_qnty  ELSE 0 END),0) AS finish_qnty 
						from 
							pro_garments_production_dtls c,wo_po_color_size_breakdown d
						where  
							d.po_break_down_id =".$po_break_down_id." and d.item_number_id='$item_id' and d.color_number_id=".$colorRes[csf("color_number_id")]." and d.size_number_id=".$sizeRes[csf("size_number_id")]." and c.color_size_break_down_id=d.id and c.status_active=1 $location $floor ");

					foreach($prod_sql as $prodRow);  
					$col = 'col'.$k;
                    if($prodRow[csf("cutting_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("cutting_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("cutting_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
					$cutting_html .='<td '.$bgCol.'>'.$prodRow[csf("cutting_qnty")].'</td>';
                    $total_cutting+=$prodRow[csf("cutting_qnty")];
                 	
					if($cons_embr>0)
					{
						if($prodRow[csf("printing_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
						else if($prodRow[csf("printing_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
						else if($prodRow[csf("printing_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'";
					}
					else $bgCol='';
                    $embiss_html .='<td '.$bgCol.'>'.$prodRow[csf("printing_qnty")].'</td>';
                    $total_emb_issue+=$prodRow[csf("printing_qnty")];
                    
					if($cons_embr>0)
					{
						if($prodRow[csf("printreceived_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
						else if($prodRow[csf("printreceived_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
						else if($prodRow[csf("printreceived_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'";
					}
					else $bgCol='';
					
                    $embrcv_html .='<td '.$bgCol.'>'.$prodRow[csf("printreceived_qnty")].'</td>';
                    $total_emb_rcv+=$prodRow[csf("printreceived_qnty")];
                    
					if($prodRow[csf("sewingin_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("sewingin_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("sewingin_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $sewin_html .='<td '.$bgCol.'>'.$prodRow[csf("sewingin_qnty")].'</td>';
                    $total_sew_in+=$prodRow[csf("sewingin_qnty")];
                    
					if($prodRow[csf("sewingout_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("sewingout_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("sewingout_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'";  
                    $sewout_html .='<td '.$bgCol.'>'.$prodRow[csf("sewingout_qnty")].'</td>';
                    $total_sew_out+=$prodRow[csf("sewingout_qnty")];
                    
					/*if($prodRow[csf("finishin_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("finishin_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("finishin_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $finisin_html .='<td '.$bgCol.'>'.$prodRow[csf("finishin_qnty")].'</td>';
                    $total_fin_in+=$prodRow[csf("finishin_qnty")];*/
                    
					if($prodRow[csf("finish_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("finish_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("finish_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $finisout_html .='<td '.$bgCol.'>'.$prodRow[csf("finish_qnty")].'</td>';
                    $total_fin_out+=$prodRow[csf("finish_qnty")];
					
/*					if($prodRow[csf("iron_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("iron_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("iron_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $iron_html .='<td '.$bgCol.'>'.$prodRow[csf("iron_qnty")].'</td>';
                    $total_iron_in+=$prodRow[csf("iron_qnty")];
*/                    
					if($prodRow[csf("iron_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("iron_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("iron_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $iron_html .='<td '.$bgCol.'>'.$prodRow[csf("iron_qnty")].'</td>';
                    $total_iron_out+=$prodRow[csf("iron_qnty")];
					
/*					if($prodRow[csf("re_iron_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("re_iron_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("re_iron_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $re_iron_html .='<td '.$bgCol.'>'.$prodRow[csf("re_iron_qnty")].'</td>';
                    $total_re_iron_out+=$prodRow[csf("re_iron_qnty")];
*/
 				 
				}// end size foreach loop	
				
				?>
					<tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Cutting</b></td>
                        <?php echo $cutting_html; ?> 
                        <td><?php echo $total_cutting; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Print/Embro Issue</b></td>
                        <?php echo $embiss_html; ?> 
                        <td><?php echo $total_emb_issue; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Print/Embro Received</b></td>
                        <?php echo $embrcv_html; ?> 
                        <td><?php echo $total_emb_rcv; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Sewing Input</b></td>
                       <?php echo $sewin_html; ?> 
                        <td><?php echo $total_sew_in; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Sewing Output</b></td>
                        <?php echo $sewout_html; ?> 
                        <td><?php echo $total_sew_out; ?></td> 
                    </tr>
                      <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Iron Output</b></td>
                        <?php echo $iron_html; ?> 
                        <td><?php echo  $total_iron_out; ?></td> 
                    </tr>
<!--                      <tr bgcolor="<?php// echo $bgcolor2; ?>">
                    	<td><b>Re-Iron Output</b></td>
                        <?php// echo $re_iron_html; ?> 
                        <td><?php// echo  $total_re_iron_out; ?></td> 
                    </tr>
-->                    <tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Finishing Output</b></td>
                       <?php echo $finisout_html; ?> 
                        <td><?php echo $total_fin_out; ?></td> 
                    </tr>
			<?php	
			}// end color foreach loop
			?>
           
		 
 </table>
</fieldset>
    
</div>    


<?php
exit();

}// end if condition

if ($action=='OrderPopupCountry')
{
	echo load_html_head_contents("Order Wise Production Report", "../../../", 1, 1,$unicode,'','');
	$po_break_down_id=$_REQUEST['po_break_down_id'];
	$item_id=$_REQUEST['item_id'];
	$country_id=$_REQUEST['country_id'];
	
?>

	
 <div id="data_panel" align="center" style="width:100%">
         <script>
		 	function new_window()
			 {
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('details_reports').innerHTML);
				d.close();
			 }
         </script>
 	<input type="button" value="Print" id="print" class="formbutton" style="width:100px;" onclick="new_window()" />
 </div>
  
<div style="width:700px" align="center" id="details_reports"> 
 	<fieldset style="width:700px">
  	<legend>Color And Size Wise Summary</legend>
    <table id="tbl_id" class="rpt_table" width="680" border="1" rules="all" >
    	<thead>
        	<tr>
            	<th width="100">Buyer</th>
                <th width="100">Job Number</th>
                <th width="100">Style Name</th>
                <th width="100">Order Number</th>
                <th width="100">Ship Date</th>
                <th width="100">Order Qnty.</th>
                <th>Country</th>
            </tr>
        </thead>
       	<?php
        	$buyer_short_library=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
			$country_library=return_library_array( "select id,country_name from lib_country", "id", "country_name"  );
 			$sql = sql_select("select a.job_no_mst,a.po_number,a.pub_shipment_date,a.po_quantity,a.packing,b.set_break_down, b.company_name, b.order_uom, b.buyer_name, b.style_ref_no,c.set_item_ratio 
					from wo_po_break_down a, wo_po_details_master b, wo_po_details_mas_set_details c 
					where a.job_no_mst=b.job_no and b.job_no=c.job_no and a.id='$po_break_down_id' and c.gmts_item_id=$item_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
			//echo $sql;
			foreach($sql as $resultRow);
			
			$cons_embr=return_field_value("sum(cons_dzn_gmts) as cons_dzn_gmts","wo_pre_cost_embe_cost_dtls","job_no='".$resultRow[csf("job_no_mst")]."' and status_active=1 and is_deleted=0","cons_dzn_gmts");
			
			$po_qnty=return_field_value("sum(order_quantity) as qnty","wo_po_color_size_breakdown","po_break_down_id='$po_break_down_id' and item_number_id='$item_id' and country_id='$country_id' and status_active=1 and is_deleted=0","qnty");	
 		?> 
        <tr>
        	<td><?php echo $buyer_short_library[$resultRow[csf("buyer_name")]]; ?></td>
            <td><p><?php echo $resultRow[csf("job_no_mst")]; ?></p></td>
            <td><p><?php echo $resultRow[csf("style_ref_no")]; ?></p></td>
            <td><?php echo $resultRow[csf("po_number")]; ?></td>
            <td><?php echo change_date_format($resultRow[csf("pub_shipment_date")]); ?></td>
            <td><?php echo $po_qnty; ?></td>
            <td><?php echo $country_library[$country_id]; ?></td>
        </tr>
         <?php
         $prod_sewing_sql=sql_select("SELECT sum(alter_qnty) as alter_qnty, sum(reject_qnty) as reject_qnty from pro_garments_production_mst where production_type=5 and po_break_down_id ='$po_break_down_id' and is_deleted=0 and status_active=1");
		 foreach($prod_sewing_sql as $sewingRow);
		?> 	
        <tr>
        	<td colspan="2">Total Alter Sewing Qnty : <b><?php echo $sewingRow[csf("alter_qnty")]; ?></b></td>
        	<td colspan="2">Total Reject Sewing Qnty : <b><?php echo $sewingRow[csf("reject_qnty")]; ?></b></td>
            <td colspan="2">Pack Assortment: <b><?php echo $packing[$resultRow[csf("packing")]]; ?></b></td>
        </tr>
    </table>
	</fieldset>
    
    <?php
				  
	  $size_Arr_library=return_library_array( "select id,size_name from lib_size", "id", "size_name" );
	  $color_Arr_library=return_library_array( "select id,color_name from lib_color", "id", "color_name" );	
	  
	  $color_library=sql_select("select distinct(color_number_id) from wo_po_color_size_breakdown where po_break_down_id='$po_break_down_id' and color_mst_id!=0 and status_active=1 ");
	  $size_library=sql_select("select distinct(size_number_id) from wo_po_color_size_breakdown where po_break_down_id='$po_break_down_id' and size_mst_id!=0 and status_active=1");
	  $count = count($size_library);	
	  $width= $count*70+350; 		
	  	  
	?>
    <fieldset style="width:700px">
    <table id="tblDtls_id" class="rpt_table" width="<?php echo $width; ?>" border="1" rules="all" >
	 	<thead>
        	<tr>
            	<th width="100">Color Name</th>
                <th width="200">Production Type</th>
 				<?php
				foreach($size_library as $sizeRes)
				{
				 	?><th width="80"><?php echo $size_Arr_library[$sizeRes[csf("size_number_id")]]; ?></th><?php
				}
				?>
     		    <th width="60">Total</th>
           </tr>
        </thead>
        <?php
		  
		  foreach($color_library as $colorRes)
		  {
			?>	  
			<tr>
				<td rowspan="10"><?php echo $color_Arr_library[$colorRes[csf("color_number_id")]]; ?></td>
			
 			<?php
            	  $i=0;$j=0;$sqlPart="";
				  foreach($size_library as $sizeRes)
				  {
					  $i++;$j++;
					  if($i>1) $sqlPart .=",";
					  $sqlPart .= 'SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' and size_number_id='.$sizeRes[csf("size_number_id")].' THEN order_quantity ELSE 0 END ) as '."col".$i;
					  $sqlPart .= ',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' and size_number_id='.$sizeRes[csf("size_number_id")].' THEN plan_cut_qnty ELSE 0 END ) as '."pcut".$i;
					  $sqlPart .= ',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' and size_number_id='.$sizeRes[csf("size_number_id")].' THEN excess_cut_perc ELSE 0 END ) as '."excess_cut".$i;
				  }
				  if($j>1)
				  {
					 $sqlPart .=',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' THEN order_quantity ELSE 0 END ) as totalorderqnty';
					 $sqlPart .=',SUM( CASE WHEN color_number_id='.$colorRes[csf("color_number_id")].' THEN plan_cut_qnty ELSE 0 END ) as totalplancutqnty';
				  }
		  
				$sql = sql_select("select avg(excess_cut_perc) as avg_excess_cut_perc,excess_cut_perc,". $sqlPart ." from wo_po_color_size_breakdown where status_active=1 and po_break_down_id='$po_break_down_id' and item_number_id=$item_id and country_id='$country_id'");
				//echo $sql;die;
				foreach($sql as $resRow); 
 					$bgcolor1="#E9F3FF"; 
					$bgcolor2="#FFFFFF";
				?>
					 
					<tr bgcolor="<?php echo $bgcolor1; ?>">
						<td><b>Order Quantity</b></td>	
                        <?php for($k=1;$k<=$i;$k++) {	$col = 'col'.$k; ?>	
                         	<td><?php echo $resRow[csf($col)]; ?></td>
						<?php } ?>
                         <td><?php echo $resRow[csf("totalorderqnty")]; ?></td> 
					</tr>
                    <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Plan To Cut (AVG <?php echo $resRow[csf("avg_excess_cut_perc")]; ?>)% </b></td>	
                        <?php for($k=1;$k<=$i;$k++){ $col = 'pcut'.$k;$excess_cut = 'excess_cut'.$k;	?>	
                         	<td title="Excess Cut <?php echo $resRow[csf($excess_cut)]; ?>%"><?php echo $resRow[csf($col)]; ?></td>
						<?php } ?>
                         <td><?php echo $resRow[csf("totalplancutqnty")]; ?></td> 
                    </tr>
					
                <?php
 				$total_cutting=0;$total_emb_issue=0;$total_emb_rcv=0;$total_sew_in=0;$total_sew_out=0;$total_fin_in=0;$total_fin_out=0;$total_iron_out=0;
				$cutting_html='';$embiss_html='';$embrcv_html='';$sewin_html='';$sewout_html='';$finisin_html='';$finisout_html=''; $iron_html='';
				$k=0;
				foreach($size_library as $sizeRes)
				{
					$k++;
					$prod_sql= sql_select("SELECT  
							IFNULL(sum(CASE WHEN c.production_type ='1' THEN  c.production_qnty  ELSE 0 END),0) AS cutting_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='2' THEN  c.production_qnty  ELSE 0 END),0) AS printing_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='3' THEN  c.production_qnty  ELSE 0 END),0) AS printreceived_qnty, 
							IFNULL(sum(CASE WHEN c.production_type ='4' THEN  c.production_qnty  ELSE 0 END),0) AS sewingin_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='5' THEN  c.production_qnty  ELSE 0 END),0) AS sewingout_qnty,
							IFNULL(sum(CASE WHEN c.production_type ='6' THEN  c.production_qnty  ELSE 0 END),0) AS finishin_qnty, 
							IFNULL(sum(CASE WHEN c.production_type ='7' THEN  c.production_qnty  ELSE 0 END),0) AS iron_qnty,
							
							IFNULL(sum(CASE WHEN c.production_type ='8' THEN  c.production_qnty  ELSE 0 END),0) AS finish_qnty 
						from 
							pro_garments_production_dtls c,wo_po_color_size_breakdown d
						where  
							d.po_break_down_id =".$po_break_down_id." and d.item_number_id='$item_id' and d.country_id='$country_id' and d.color_number_id=".$colorRes[csf("color_number_id")]." and d.size_number_id=".$sizeRes[csf("size_number_id")]." and c.color_size_break_down_id=d.id and c.status_active=1 $location $floor ");
					
					foreach($prod_sql as $prodRow);  
					$col = 'col'.$k;
                    if($prodRow[csf("cutting_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("cutting_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("cutting_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
					$cutting_html .='<td '.$bgCol.'>'.$prodRow[csf("cutting_qnty")].'</td>';
                    $total_cutting+=$prodRow[csf("cutting_qnty")];
                 	
					if($cons_embr>0)
					{
						if($prodRow[csf("printing_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
						else if($prodRow[csf("printing_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
						else if($prodRow[csf("printing_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'";
					}
					else $bgCol='';
                    $embiss_html .='<td '.$bgCol.'>'.$prodRow[csf("printing_qnty")].'</td>';
                    $total_emb_issue+=$prodRow[csf("printing_qnty")];
                    
					if($cons_embr>0)
					{
						if($prodRow[csf("printreceived_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
						else if($prodRow[csf("printreceived_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
						else if($prodRow[csf("printreceived_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'";
					}
					else $bgCol='';
					
                    $embrcv_html .='<td '.$bgCol.'>'.$prodRow[csf("printreceived_qnty")].'</td>';
                    $total_emb_rcv+=$prodRow[csf("printreceived_qnty")];
                    
					if($prodRow[csf("sewingin_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("sewingin_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("sewingin_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $sewin_html .='<td '.$bgCol.'>'.$prodRow[csf("sewingin_qnty")].'</td>';
                    $total_sew_in+=$prodRow[csf("sewingin_qnty")];
                    
					if($prodRow[csf("sewingout_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("sewingout_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("sewingout_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'";  
                    $sewout_html .='<td '.$bgCol.'>'.$prodRow[csf("sewingout_qnty")].'</td>';
                    $total_sew_out+=$prodRow[csf("sewingout_qnty")];
                    
					/*if($prodRow[csf("finishin_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("finishin_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("finishin_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $finisin_html .='<td '.$bgCol.'>'.$prodRow[csf("finishin_qnty")].'</td>';
                    $total_fin_in+=$prodRow[csf("finishin_qnty")];*/
					
					if($prodRow[csf("iron_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("iron_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("iron_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $iron_html .='<td '.$bgCol.'>'.$prodRow[csf("iron_qnty")].'</td>';
                    $total_iron_out+=$prodRow[csf("iron_qnty")];
					
/*					if($prodRow[csf("re_iron_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("re_iron_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("re_iron_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $re_iron_html .='<td '.$bgCol.'>'.$prodRow[csf("re_iron_qnty")].'</td>';
                    $total_re_iron_out+=$prodRow[csf("re_iron_qnty")];
*/					
					
					if($prodRow[csf("finish_qnty")]==0)$bgCol="bgcolor='#FF0000'"; 
					else if($prodRow[csf("finish_qnty")] < $resRow[csf($col)]) $bgCol="bgcolor='#FFFF00'"; 
					else if($prodRow[csf("finish_qnty")] >= $resRow[csf($col)]) $bgCol="bgcolor='#00FF00'"; 
                    $finisout_html .='<td '.$bgCol.'>'.$prodRow[csf("finish_qnty")].'</td>';
                    $total_fin_out+=$prodRow[csf("finish_qnty")];
					
					
                    
				}// end size foreach loop	
				
				?>
					<tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Cutting</b></td>
                        <?php echo $cutting_html; ?> 
                        <td><?php echo $total_cutting; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Print/Embro Issue</b></td>
                        <?php echo $embiss_html; ?> 
                        <td><?php echo $total_emb_issue; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Print/Embro Received</b></td>
                        <?php echo $embrcv_html; ?> 
                        <td><?php echo $total_emb_rcv; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Sewing Input</b></td>
                       <?php echo $sewin_html; ?> 
                        <td><?php echo $total_sew_in; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Sewing Output</b></td>
                        <?php echo $sewout_html; ?> 
                        <td><?php echo $total_sew_out; ?></td> 
                    </tr>
                    <tr bgcolor="<?php echo $bgcolor2; ?>">
                    	<td><b>Iron Output</b></td>
                        <?php echo $iron_html; ?> 
                        <td><?php echo  $total_iron_out; ?></td> 
                    </tr>
<!--                    <tr bgcolor="<?php// echo $bgcolor2; ?>">
                    	<td><b>Re-Iron Output</b></td>
                        <?php// echo $re_iron_html; ?> 
                        <td><?php// echo  $total_re_iron_out; ?></td> 
                    </tr>
-->                    
                    <tr bgcolor="<?php echo $bgcolor1; ?>">
                    	<td><b>Finishing Output</b></td>
                       <?php echo $finisout_html; ?> 
                        <td><?php echo $total_fin_out; ?></td> 
                    </tr> 
			<?php	
			}// end color foreach loop
			?>
           
		 
 </table>
</fieldset>
    
</div>    


<?php
exit();

}// end if condition


//cutting-1,sewing ouput-5--------------------popup-----------//
if ($action==1 || $action==5) 
{
	 
 	extract($_REQUEST);
 	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode,1,1);
	
	if($country_id=='') $country_cond=""; else $country_cond=" and country_id='$country_id'";
	
 	?>
    <fieldset>
    <div style="margin-left:50px">
        <table width="620" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1" >
            <thead>
                 <?php if($action==1){ ?>
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="100">Cutting Date</th>
                        <th width="160">Cutt. Qty(In-house)</th>
                        <th width="160">Cutt. Qty(Out-bound)</th>
                        <th width="">Cutting Company</th>
                 	</tr>
				<?php } else if($action==5){ ?>
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="100">Sew.Output Date</th>
                        <th width="160">Sew.Qty(In-house)</th>
                        <th width="160">Sew.Qty(Out-bound)</th>
                        <th width="">Sewing Company</th>
                    </tr>
 				<?php } ?>
                
            </thead>
        </table>
        <div style="max-height:425px; overflow-y:scroll; width:638px;" id="scroll_body">
            <table cellspacing="0" border="1" class="rpt_table"  width="620" rules="all" id="table_body" >
            <?php
             $total_in_quantity=0;$total_out_quantity=0;
             $i=1;
 			 $company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
 			 $supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id", "supplier_name" );
			 
			 $location="";$floor="";
			 if($dateOrLocWise==2 || $dateOrLocWise==4) // only for location floor and line wise
			 {
				 if($location_id!="") $location=" and location=$location_id";
				 if($floor_id!="") $floor=" and floor_id=$floor_id";
			 }
			 
			
             $sql=sql_select("select po_break_down_id,item_number_id,production_date,production_type,production_source,serving_company,
				  SUM(CASE WHEN production_source=1 THEN production_quantity ELSE 0 END) as in_house_cut_qnty,
				  SUM(CASE WHEN production_source=3 THEN production_quantity ELSE 0 END) as out_bound_cut_qnty
				  from pro_garments_production_mst where po_break_down_id=$po_break_down_id and item_number_id=$item_id and production_type=$action and status_active=1 $location $floor $country_cond group by serving_company,production_date"); 
				  
            foreach($sql as $resultRow)
            {
                 if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
             	?>
                 <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                    <td width="50"><?php echo $i;?></td>
                    <td width="100"><?php echo change_date_format($resultRow[csf("production_date")]); ?></td>
                    <td width="160" align="right"><?php echo number_format($resultRow[csf("in_house_cut_qnty")]); ?></td>
                    <td width="160" align="right"><?php echo number_format($resultRow[csf("out_bound_cut_qnty")]); ?></td>
                    <?php
                    	$source= $resultRow[csf('production_source')];
					    if($source==3)
						{
							$serving_company= $supplier_library[$resultRow[csf('serving_company')]];
						}
						else
						{
							$serving_company= $company_library[$resultRow[csf('serving_company')]];
						}
					?>
                    <td width=""><p><?php echo $serving_company; ?></p></td>
                 </tr>	
                 <?php		
 
                    $total_in_quantity+=$resultRow[csf("in_house_cut_qnty")];
					$total_out_quantity+=$resultRow[csf("out_bound_cut_qnty")];
            	 	$i++;
            
        }//end foreach 1st
            
        ?>
        </table>
         <table border="1" class="tbl_bottom"  width="620" rules="all" id="body_bottom" >
                 <tr> 
                    <td width="50">&nbsp;</td> 
                    <td width="100">Total</td> 
                    <td width="160"><?php echo number_format($total_in_quantity); ?> </td>
                    <td width="160"><?php echo number_format($total_out_quantity); ?></td>
                    <td width="">&nbsp;</td> 
                 </tr>
         </table>
       </div>
     </div>
     </fieldset>
    <?php
 	
 exit();
 
}


//---- sewing input-4, iron input-7, finish-8, re_iron input-9-----------popup--------// 
if ($action==4 || $action==7 || $action==8 || $action==9) // popup
{
	 
	extract($_REQUEST);
 	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode,1,1);
	
	if($country_id=='') $country_cond=""; else $country_cond=" and country_id='$country_id'";
	
 	?>
    <fieldset>
    <div style="margin-left:60px">
        <table width="500" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1" >
            <thead>
				<?php if($action==2){ ?>
                
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Print/ Emb. Issue Date</th>
                        <th width="">Print/ Emb. Issue Qnty</th>
                    </tr>
                
				<?php } else if($action==3){ ?>
               
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Print/ Emb. Receive Date</th>
                        <th width="">Print/ Emb. Receive Qnty</th>
                    </tr>
                
				<?php } else if($action==4){ ?>
                
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Sewing Input Date</th>
                        <th width="">Sewing Input Qnty</th>
                    </tr>
                <?php } else if($action==7){ ?>
                
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Iron Output Date</th>
                        <th width="">Iron Output Qnty</th>
                    </tr>
                <?php } else if($action==8){ ?>
                
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Finish Date</th>
                        <th width="">Finish Qty</th>
                    </tr>
                <?php } else if($action==9){ ?>
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Iron Output Date</th>
                        <th width="">Re-Iron Output Qty</th>
                    </tr>
               <?php } ?> 
            </thead>
        </table>
        <div style="max-height:425px; overflow-y:scroll; width:518px;" id="scroll_body">
            <table cellspacing="0" border="1" class="rpt_table"  width="500" rules="all" id="table_body" >
            <?php
             $total_quantity=0;
             $i=1;
			 $location="";$floor="";
			 if($dateOrLocWise==2 || $dateOrLocWise==4) // only for location floor and line wise
			 {
				 if($location_id!="") $location=" and location=$location_id";
				 if($floor_id!="") $floor=" and floor_id=$floor_id";
			 }
			 if ($action==9)
			 {
				 $sql=sql_select("select po_break_down_id,item_number_id,production_date,production_type, sum(re_production_qty) as production_quantity	  
				  from pro_garments_production_mst where po_break_down_id=$po_break_down_id and re_production_qty!=0 and item_number_id=$item_id and status_active=1 and is_deleted=0 and production_type=7 $location $floor $country_cond group by serving_company,production_date");
			 }
			 else
			 {
				 $sql=sql_select("select po_break_down_id,item_number_id,production_date,production_type,production_source,sum(production_quantity) as production_quantity
				  from pro_garments_production_mst where po_break_down_id=$po_break_down_id and item_number_id=$item_id and status_active=1 and is_deleted=0 and production_type=$action $location $floor $country_cond group by serving_company,production_date"); 
			 }
 
            foreach($sql as $resultRow)
            {
                 if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
             	?>
                 <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                    <td width="50"><?php echo $i;?></td>
                    <td width="200" align="right"><?php echo change_date_format($resultRow[csf("production_date")]); ?></td>
                    <td width="" align="right"><?php echo number_format($resultRow[csf("production_quantity")],2); ?>&nbsp;</td>
                 </tr>	
                 <?php		
                    $total_quantity+=$resultRow[csf("production_quantity")];
            	 	$i++;
            
        }//end foreach 1st
        ?>
        </table>
        <table cellspacing="0" border="1" class="tbl_bottom"  width="500" rules="all" id="body_bottom" >
                 <tr> 
                    <td width="50">&nbsp;</td> 
                    <td width="200">Total</td> 
                    <td width=""><?php echo number_format($total_quantity,2); ?>&nbsp;</td>
                  </tr>
         </table>
       </div>
     </div>
     </fieldset>
    <?php
 	
 exit();
 
}



if ($action=='exfactory')  // exfactory date popup
{
	 
	extract($_REQUEST);
 	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode,1,1);
	
 	?>
    <fieldset>
    <div style="margin-left:60px">
        <table width="500" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1" >
            <thead>
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Ex Factory Date</th>
                        <th width="">Ex Factory Qnty</th>
               		</tr>
            </thead>
        </table>
        <div style="max-height:425px; overflow-y:scroll; width:518px;" id="scroll_body">
            <table cellspacing="0" border="1" class="rpt_table"  width="500" rules="all" id="table_body" >
            <?php
             $total_quantity=0;
             
             $sql=sql_select("select sum(ex_factory_qnty) as ex_factory_qnty, ex_factory_date 		  
				  from pro_ex_factory_mst where po_break_down_id=$po_break_down_id and item_number_id=$item_id group by ex_factory_date"); 
            //echo $sql; 
            foreach($sql as $resultRow)
            {
                 if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
             	?>
                 <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                    <td width="50"><?php echo $i;?></td>
                    <td width="200" align="right"><?php if($resultRow[csf("ex_factory_date")]!="0000-00-00") echo change_date_format($resultRow[csf("ex_factory_date")]); ?></td>
                    <td width="" align="right"><?php echo number_format($resultRow[csf("ex_factory_qnty")]); ?></td>
                 </tr>	
                 <?php		
                    $total_quantity+=$resultRow[csf("ex_factory_qnty")];
            	 	$i++;
            
        }//end foreach 1st
        ?>
        </table>
        <table cellspacing="0" border="1" class="tbl_bottom"  width="500" rules="all" id="body_bottom" >
                 <tr> 
                    <td width="50">&nbsp;</td> 
                    <td width="200">Total</td> 
                    <td width=""><?php echo number_format($total_quantity); ?></td>
                  </tr>
         </table>
       </div>
     </div>
     </fieldset>
    <?php
 	
 exit();
 
}

if ($action=='exfactoryCountry')  // exfactory date popup
{
	 
	extract($_REQUEST);
 	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode,1,1);
	
	if($location_id=="") $location_mst=""; else $location_mst=" and location='$location_id'";
	if($floor_id=="") $floor_mst=""; else $floor_mst=" and floor_id='$floor_id'";
	
 	?>
    <fieldset>
    <div style="margin-left:60px">
        <table width="500" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1" >
            <thead>
                    <tr>
                        <th width="50">Sl.</th>    
                        <th width="200">Ex Factory Date</th>
                        <th width="">Ex Factory Qnty</th>
               		</tr>
            </thead>
        </table>
        <div style="max-height:425px; overflow-y:scroll; width:518px;" id="scroll_body">
            <table cellspacing="0" border="1" class="rpt_table"  width="500" rules="all" id="table_body" >
            <?php
             $total_quantity=0;
             
             $sql=sql_select("select sum(ex_factory_qnty) as ex_factory_qnty, ex_factory_date 		  
				  from pro_ex_factory_mst where po_break_down_id=$po_break_down_id and item_number_id=$item_id and country_id='$country_id' group by ex_factory_date"); // $location_mst $floor_mst
            //echo $sql; 
            foreach($sql as $resultRow)
            {
                 if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
             	?>
                 <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                    <td width="50"><?php echo $i;?></td>
                    <td width="200" align="right"><?php if($resultRow[csf("ex_factory_date")]!="0000-00-00") echo change_date_format($resultRow[csf("ex_factory_date")]); ?></td>
                    <td width="" align="right"><?php echo number_format($resultRow[csf("ex_factory_qnty")]); ?></td>
                 </tr>	
                 <?php		
                    $total_quantity+=$resultRow[csf("ex_factory_qnty")];
            	 	$i++;
            
        }//end foreach 1st
        ?>
        </table>
        <table cellspacing="0" border="1" class="tbl_bottom"  width="500" rules="all" id="body_bottom" >
                 <tr> 
                    <td width="50">&nbsp;</td> 
                    <td width="200">Total</td> 
                    <td width=""><?php echo number_format($total_quantity); ?></td>
                  </tr>
         </table>
       </div>
     </div>
     </fieldset>
    <?php
 	
 exit();
 
}

//--print/emb issue-2,print/emb receive-3,
if ($action==2 || $action==3)
{
	 
	extract($_REQUEST);
 	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode,1,1);
	
	if($country_id=='') $country_cond=""; else $country_cond=" and country_id='$country_id'";
	
 	?>
    <div id="data_panel" align="center" style="width:100%">
         <script>
		 	function new_window()
			 {
				var w = window.open("Surprise", "#");
				var d = w.document.open();
				d.write(document.getElementById('details_reports').innerHTML);
				d.close();
			 }
          </script>
 	<input type="button" value="Print" id="print" class="formbutton" style="width:100px;" onclick="new_window()" />
 	</div>
    <div id="details_reports">
        <table width="1000" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1" >
            <thead>
                   
                 <?php if ($action==2) { ?>  
                   <tr>
                        <th width="30" rowspan="2">Sl.</th>    
                        <th width="110" rowspan="2">Date</th>
                        <th colspan="3">Printing Issue</th>
                        <th colspan="3">Embroidery Issue</th>
                        <th colspan="3">Wash Issue</th>
                        <th colspan="3">Special Work Issue</th>
                    </tr> 
                 <?php } else {?>
                 	<tr>
                        <th width="30" rowspan="2">Sl.</th>    
                        <th width="110" rowspan="2">Date</th>
                        <th colspan="3">Printing Receive</th>
                        <th colspan="3">Embroidery Receive</th>
                        <th colspan="3">Wash Receive</th>
                        <th colspan="3">Special Work Receive</th>
                    </tr> 
                 <?php } ?>   
                    
                    <tr>
                      
                      <th width="80">In-house</th>
                      <th width="80">Outside</th>
                      <th width="80">Embl. Company</th>
                      
                      <th width="80">In-house</th>
                      <th width="80">Outside</th>
                      <th width="80">Embl. Company</th>
                      
                      <th width="80">In-house</th>
                      <th width="80">Outside</th>
                      <th width="80">Embl. Company</th>
                      
                      <th width="80">In-house</th>
                      <th width="80">Outside</th>
                      <th width="80">Embl. Company</th>
                    </tr>
            </thead>
        </table>
        <div style="max-height:425px; overflow-y:scroll; width:1020px;" id="scroll_body">
            <table cellspacing="0" border="1" class="rpt_table"  width="1000" rules="all" id="table_body" >
            <?php
			$company_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name" );
 			$supplier_library=return_library_array( "select id,short_name from  lib_supplier", "id", "short_name" );	
 			 
			$sql = sql_select("SELECT id,production_date,production_source,serving_company,
						SUM(CASE WHEN production_source =1 AND embel_name=1 THEN production_quantity ELSE 0 END) AS prod11,  
						SUM(CASE WHEN production_source =1 AND embel_name=2 THEN production_quantity ELSE 0 END) AS prod12,
						SUM(CASE WHEN production_source =1 AND embel_name=3 THEN production_quantity ELSE 0 END) AS prod13,
						SUM(CASE WHEN production_source =1 AND embel_name=4 THEN production_quantity ELSE 0 END) AS prod14,
						
						SUM(CASE WHEN production_source =3 AND embel_name=1 THEN production_quantity ELSE 0 END) AS prod31,  
						SUM(CASE WHEN production_source =3 AND embel_name=2 THEN production_quantity ELSE 0 END) AS prod32,
						SUM(CASE WHEN production_source =3 AND embel_name=3 THEN production_quantity ELSE 0 END) AS prod33,
						SUM(CASE WHEN production_source =3 AND embel_name=4 THEN production_quantity ELSE 0 END) AS prod34
 					FROM
						pro_garments_production_mst 
					WHERE 
						production_type=$action and po_break_down_id=$po_break_down_id and item_number_id=$item_id $country_cond
					GROUP BY production_date,production_source,serving_company");
			// echo $sql; die;
		   	$printing_in_qnty=0;$emb_in_qnty=0;$wash_in_qnty=0;$special_in_qnty=0;
			$printing_out_qnty=0;$emb_out_qnty=0;$wash_out_qnty=0;$special_out_qnty=0;
			$dataArray=array();$companyArray=array();
            $i=1;
			foreach($sql as $resultRow)
            {
                 if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				 
				 if($resultRow[csf('production_source')]==3)
					$serving_company= $supplier_library[$resultRow[csf('serving_company')]];
				else
					$serving_company= $company_library[$resultRow[csf('serving_company')]];
				$td_count = 2;	
             	?>
                 <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_1nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_1nd<?php echo $i; ?>">
                    <td width="30"><?php echo $i;?></td>
                    <td width="85" align="right"><?php echo change_date_format($resultRow[csf("production_date")]); ?></td>
                    
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==1){ echo $resultRow[csf("prod11")];$printing_in_qnty+=$resultRow[csf("prod11")];}else echo "0"; ?></td>
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==3){ echo $resultRow[csf("prod31")];$printing_out_qnty+=$resultRow[csf("prod31")];}else echo "0"; ?></td>
                    <td width="80"><?php if($resultRow[csf('prod11')]>0 || $resultRow[csf('prod31')]>0) echo $serving_company; ?></td>
                    <?php 
					$companyArray[$serving_company]=$serving_company;
					$dataArray[1][$serving_company]+=$resultRow[csf("prod11")]+$resultRow[csf("prod31")] ?>
                    
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==1){ echo $resultRow[csf("prod12")];$emb_in_qnty+=$resultRow[csf("prod12")];}else echo "0"; ?></td>
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==3){ echo $resultRow[csf("prod32")];$emb_out_qnty+=$resultRow[csf("prod32")];}else echo "0"; ?></td>
                    <td width="80"><?php if($resultRow[csf('prod12')]>0 || $resultRow[csf('prod32')]>0) echo $serving_company; ?></td>
                    <?php 
 					$dataArray[2][$serving_company]+=$resultRow[csf("prod12")]+$resultRow[csf("prod32")] ?>
                    
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==1){ echo $resultRow[csf("prod13")];$wash_in_qnty+=$resultRow[csf("prod13")];}else echo "0"; ?></td>
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==3){ echo $resultRow[csf("prod33")];$wash_out_qnty+=$resultRow[csf("prod33")];}else echo "0"; ?></td>
                    <td width="80"><?php if($resultRow[csf('prod13')]>0 || $resultRow[csf('prod33')]>0) echo $serving_company; ?></td>
                    <?php 
 					$dataArray[3][$serving_company]+=$resultRow[csf("prod13")]+$resultRow[csf("prod33")] ?>
                    
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==1){ echo $resultRow[csf("prod14")];$special_in_qnty+=$resultRow[csf("prod14")];}else echo "0"; ?></td>
                    <td width="80" align="right"><?php if($resultRow[csf('production_source')]==3){ echo $resultRow[csf("prod34")];$special_out_qnty+=$resultRow[csf("prod34")];}else echo "0"; ?></td>
                    <td width="80"><?php if($resultRow[csf('prod14')]>0 || $resultRow[csf('prod34')]>0) echo $serving_company; ?></td>
                    <?php 
 					$dataArray[4][$serving_company]+=$resultRow[csf("prod14")]+$resultRow[csf("prod34")] ?>
                  </tr> 
 				 <?php		
             	$i++;
            
        }//end foreach 1st
        ?>
        		<tfoot>
                    <tr>
                       <th align="right" colspan="2">Grand Total</th>
                       <th align="right"><?php echo $printing_in_qnty; ?></th>
                       <th align="right"><?php echo $printing_out_qnty; ?></th>
                       <th align="right">&nbsp;</th>
                       <th align="right"><?php echo $emb_in_qnty; ?></th>
                       <th align="right"><?php echo $emb_out_qnty; ?></th>
                       <th align="right">&nbsp;</th>
                       <th align="right"><?php echo $wash_in_qnty; ?></th>
                       <th align="right"><?php echo $wash_out_qnty; ?></th>
                       <th align="right">&nbsp;</th>
                       <th align="right"><?php echo $special_in_qnty; ?></th>
                       <th align="right"><?php echo $special_out_qnty; ?></th>
                       <th align="right">&nbsp;</th>
                     </tr>
               </tfoot>      
        </table>
       </div>
       
       <div style="clear:both">&nbsp;</div>
       
       <div style="width:450px; float:left"> 
       <table width="400" cellspacing="0" border="1" class="rpt_table" rules="all" > 
       		<?php if($action==2){?> <label><h3>Issue Summary</h3></label><?php } else {?> <label><h3>Receive Summary</h3></label> <?php } ?>               	
             <thead> 
                <tr>
                    <th>SL</th>
                    <th>Emb.Company</th>
                    <th>Print</th>
                    <th>Embroidery</th>
                    <th>Emb	Wash</th>
                    <th>Special Work</th>
                 </tr>
              </thead>  
			 <?php
			 $printing_total=0;$emb_total=0;$wash_total=0;$special_total=0;
			 $i=1;	 
			 foreach($companyArray as $com){
			 	if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
			 ?>
                 <tr bgcolor="<?php echo $bgcolor; ?>">
                 		<td><?php echo $i; ?></td>
                        <td><?php echo $com; ?></td>
                        <td align="right"><?php echo number_format($dataArray[1][$com]);$printing_total+=$dataArray[1][$com]; ?></td>
                        <td align="right"><?php echo number_format($dataArray[2][$com]);$emb_total+=$dataArray[2][$com]; ?></td>
                        <td align="right"><?php echo number_format($dataArray[3][$com]);$wash_total+=$dataArray[3][$com]; ?></td>
                        <td align="right"><?php echo number_format($dataArray[4][$com]);$special_total+=$dataArray[4][$com]; ?></td>
                 </tr>   
              <?php $i++; } ?>
              <tfoot>
                    <tr>
                       <th align="right" colspan="2">Grand Total</th>
                       <th align="right"><?php echo number_format($printing_total); ?></th>
                       <th align="right"><?php echo number_format($emb_total); ?></th>
                       <th align="right"><?php echo number_format($wash_total); ?></th>
                       <th align="right"><?php echo number_format($special_total); ?></th>
                    </tr>
              </tfoot>          
    	 </table>
     </div>
     
     <div style="width:450px; float:left; "> 
     	<?php if($action!=2) //only for receive
		 { 
			?> 	
			<table width="400" cellspacing="0" border="1" class="rpt_table" rules="all" > 
            <label><h3>Balance</h3></label>
              <thead> 
                <tr>
                    <th>SL</th>
                    <th>Particulers</th>
                    <th>Print</th>
                    <th>Embroidery</th>
                    <th> Wash</th>
                    <th>Special Work</th>
                 </tr>
              </thead>  
 			<?php
 				$sql_order = sql_select("SELECT  a.job_no_mst,
						SUM(CASE WHEN b.emb_name=1 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS print,  
						SUM(CASE WHEN b.emb_name=2 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS emb,
						SUM(CASE WHEN b.emb_name=3 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS wash,
						SUM(CASE WHEN b.emb_name=4 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS special
   					FROM
						wo_po_break_down a, wo_pre_cost_embe_cost_dtls b 
					WHERE
						a.id=$po_break_down_id and a.job_no_mst=b.job_no");
				foreach($sql_order as $resultRow);	
						
				$sql_mst = sql_select("SELECT 
						SUM(CASE WHEN embel_name=1 THEN production_quantity ELSE 0 END) AS print_issue,  
						SUM(CASE WHEN embel_name=2 THEN production_quantity ELSE 0 END) AS emb_issue,
						SUM(CASE WHEN embel_name=3 THEN production_quantity ELSE 0 END) AS wash_issue,
						SUM(CASE WHEN embel_name=4 THEN production_quantity ELSE 0 END) AS special_issue
 					FROM
						pro_garments_production_mst
					WHERE
						po_break_down_id=$po_break_down_id and item_number_id=$item_id and production_type=2 $country_cond
					GROUP BY 
						po_break_down_id,item_number_id");		
				//echo $sql_mst;die;
				foreach($sql_mst as $resultMst);
				//echo $sql;die;
				$i=1;		
				 
					 ?>
						 <tr bgcolor="#E9F3FF">
								<td><?php echo $i++; ?></td>
								<td>Req Qnty</td>
								<td align="right"><?php echo number_format($resultRow[csf('print')]); ?></td>
								<td align="right"><?php echo number_format($resultRow[csf('emb')]); ?></td>
								<td align="right"><?php echo number_format($resultRow[csf('wash')]); ?></td>
								<td align="right"><?php echo number_format($resultRow[csf('special')]); ?></td>
						 </tr> 
                         <tr bgcolor="#FFFFFF">
								<td><?php echo $i++; ?></td>
                                <td>Total Sent for</td>
 								<td align="right"><?php echo number_format($resultMst[csf('print_issue')]); ?></td>
								<td align="right"><?php echo number_format($resultMst[csf('emb_issue')]); ?></td>
								<td align="right"><?php echo number_format($resultMst[csf('wash_issue')]); ?></td>
								<td align="right"><?php echo number_format($resultMst[csf('special_issue')]); ?></td>
						 </tr>
                         <tr bgcolor="#E9F3FF">
								<td><?php echo $i++; ?></td>
                                <td>Total Receive</td>
 								<td align="right"><?php echo number_format($printing_total); ?></td>
								<td align="right"><?php echo number_format($emb_total); ?></td>
								<td align="right"><?php echo number_format($wash_total); ?></td>
								<td align="right"><?php echo number_format($special_total); ?></td>
						 </tr>
                         <tr bgcolor="#FFFFFF">
								<td><?php echo $i++; ?></td>
                                <td>Receive Balance</td>
                                <?php $rcv_print_balance = $resultMst[csf('print_issue')]-$printing_total; ?>
 								<td align="right"><?php echo number_format($rcv_print_balance); ?></td>
								<?php $rcv_emb_balance = $resultMst[csf('emb_issue')]-$emb_total; ?>
 								<td align="right"><?php echo number_format($rcv_emb_balance); ?></td>
								<?php $rcv_wash_balance = $resultMst[csf('wash_issue')]-$wash_total; ?>
 								<td align="right"><?php echo number_format($rcv_wash_balance); ?></td>
								<?php $rcv_special_balance = $resultMst[csf('special_issue')]-$special_total; ?>
 								<td align="right"><?php echo number_format($rcv_special_balance); ?></td>
						 </tr> 
                         <tr bgcolor="#E9F3FF">
								<td><?php echo $i++; ?></td>
 								<td>Issue Balance</td>
 								<td align="right"><?php echo  number_format($resultRow[csf('print')]-$resultMst[csf('print_issue')]); ?></td>
								<td align="right"><?php echo  number_format($resultRow[csf('emb')]-$resultMst[csf('emb_issue')]); ?></td>
                                <td align="right"><?php echo  number_format($resultRow[csf('wash')]-$resultMst[csf('wash_issue')]); ?></td>
                                <td align="right"><?php echo  number_format($resultRow[csf('special')]-$resultMst[csf('special_issue')]); ?></td>
 						 </tr>  
					 <?php 
 				} 
			?>
            </table> 
        
     </div>
     <div style="clear:both"></div>
	<br />
 </div>    
    
<?php
  exit();
 
}

if ($action=="reject_qty")
{
	extract($_REQUEST);
 	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode,1,1);
	//echo $po_id;
	?>
     <div style="width:450px;" align="center"> 
       <table width="380" cellspacing="0" border="1" class="rpt_table" rules="all" > 
             <thead>
             	<tr>
                	<th colspan="4">Reject Qty Details</th>
                </tr>
                <tr>
                    <th width="30">SL</th>
                    <th width="110">Sew. Out Rej. Qty</th>
                    <th width="110">Fin. Rej. Qty.</th>
                    <th width="110">Total Rej. Qty.</th>
                 </tr>
              </thead>
              <tbody> 
			 <?php
			 $sql_qry="Select sum(CASE WHEN production_type ='8' THEN reject_qnty ELSE 0 END) AS finish_rej_qnty,
						sum(CASE WHEN production_type ='5' THEN reject_qnty ELSE 0 END) AS sewingout_rej_qnty
							from pro_garments_production_mst 
							where po_break_down_id =$po_id and item_number_id='$item_id' and status_active=1 and is_deleted=0 group by po_break_down_id";
			//echo $sql_qry;
			$sql_result=sql_select($sql_qry);

			 $i=1;	 
			 foreach($sql_result as $row)
			 {
			 	if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
			 ?>
                 <tr bgcolor="<?php echo $bgcolor; ?>">
                    <td><?php echo $i; ?></td>
                    <td align="right"><?php echo $row[csf('sewingout_rej_qnty')]; ?>&nbsp;</td>
                    <td align="right"><?php echo $row[csf('finish_rej_qnty')]; ?>&nbsp;</td>
                    <td align="right"><?php $total_reject=$row[csf('sewingout_rej_qnty')]+$row[csf('finish_rej_qnty')]; echo $total_reject; ?>&nbsp;</td>
                 </tr>   
              <?php $i++; 
			  } ?> 
             </tbody>
         </table>
     </div>    
	<?php
	exit();
}
?>