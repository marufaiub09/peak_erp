﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$color_arr=return_library_array( "select id, color_name from  lib_color", "id", "color_name"  );
$line_arr=return_library_array( "select id, line_name from  lib_sewing_line", "id", "line_name");

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_short_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.id=b.buyer_id and b.tag_company=$data and a.status_active=1 and a.is_deleted=0 order by a.buyer_name","id,buyer_name", 1, "-- Select buyer --", 0, "","" );//load_drop_down( 'requires/daily_knitting_production_report_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_machine', 'machine_td' );$location_cond
  exit();	 
}

//item style------------------------------//
if($action=="style_wise_search")
{		  
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	if($company==0) $company_name=""; else $company_name="and a.company_name=$company";
	if($buyer==0) $buyer_name=""; else $buyer_name="and a.buyer_name=$buyer";
	if(str_replace("'","",$job_id)!="")  $job_cond="and b.id in(".str_replace("'","",$job_id).")";
    else  if (str_replace("'","",$job_no)=="") $job_cond=""; else $job_cond="and b.job_no_mst='".$job_no."'";

	$sql = "select a.id,a.style_ref_no,a.job_no_prefix_num,SUBSTRING_INDEX(a.insert_date, '-', 1) as year from wo_po_details_master a,wo_po_break_down b where a.job_no=b.job_no_mst $company_name $buyer_name $job_cond group by a.id"; 
	//echo $sql;die;
	echo create_list_view("list_view", "Style Refference,Job no,Year","190,100,100","440","310",0, $sql , "js_set_value", "id,style_ref_no", "", 1, "0", $arr, "style_ref_no,job_no_prefix_num,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}



//order wise browse------------------------------//
if($action=="job_wise_search")
{		  
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	if($company==0) $company_name=""; else $company_name=" and b.company_name=$company";//job_no
	if($buyer==0) $buyer_name=""; else $buyer_name="and b.buyer_name=$buyer";
	
	$sql = "select distinct a.id,a.po_number,a.job_no_mst,b.style_ref_no,b.job_no_prefix_num,SUBSTRING_INDEX(b.insert_date, '-', 1) as year from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company_name  $buyer_name";
	echo create_list_view("list_view", "Order Number,Job No,Year,Style Ref","150,100,100,100","500","310",0, $sql , "js_set_value", "id,job_no_mst", "", 1, "0", $arr, "po_number,job_no_prefix_num,year,style_ref_no", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}






//order wise browse------------------------------//
if($action=="order_wise_search")
{		  
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	if($company==0) $company_name=""; else $company_name=" and b.company_name=$company";
	if($buyer==0) $buyer_name=""; else $buyer_name="and b.buyer_name=$buyer";
	if(str_replace("'","",$style_id)!="")  $style_cond="and b.id in(".str_replace("'","",$style_id).")";
    else  if (str_replace("'","",$style_no)=="") $style_cond=""; else $style_cond="and b.style_ref_no='".$style_no."'";
	$sql = "select distinct a.id,a.po_number,b.style_ref_no,b.job_no_prefix_num,SUBSTRING_INDEX(b.insert_date, '-', 1) as year from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company_name  $buyer_name $style_cond";
	echo create_list_view("list_view", "Style Ref,Order Number,Job No, Year","150,150,100,100,","550","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "style_ref_no,po_number,job_no_prefix_num,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}



if($action=="generate_report")
{ 
   $process = array( &$_POST );
   extract(check_magic_quote_gpc( $process )); 
   $product_array=array();
					$sql_product="select id, color from product_details_master where item_category_id=2 and status_active=1 and is_deleted=0";
					$sql_product_result=sql_select($sql_product);
					foreach( $sql_product_result as $row )
					{
						$product_array[$row[csf('id')]]=$row[csf('color')];
					}

					$transfer_in_arr=array();
					$sql_transfer_in="select a.id, a.to_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_in_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_name and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=2 and a.transfer_date=".$txt_date_from." group by a.to_order_id,b.from_prod_id";
					$data_transfer_in_array=sql_select($sql_transfer_in);
					if(count($data_transfer_in_array)>0)
					{
						foreach( $data_transfer_in_array as $row )
						{
							$transfer_in_arr[$row[csf('to_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_in_qnty')];
						}
					}
					$transfer_out_arr=array(); 
					$sql_transfer_out="select a.id, a.from_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_out_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_name and a.id=b.mst_id  and a.transfer_date=".$txt_date_from." and a.transfer_criteria=4 and a.item_category=2 group by a.from_order_id,b.from_prod_id";
					$data_transfer_out_array=sql_select($sql_transfer_out);
					if(count($data_transfer_out_array)>0)
					{
						foreach( $data_transfer_out_array as $row )
						{
							$transfer_out_arr[$row[csf('from_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_out_qnty')];
						}
					}
					
					
					$transfer_prein_arr=array();
					$sql_transfer_prein="select a.id, a.to_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_in_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_name and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=2 and a.transfer_date<".$txt_date_from." group by a.to_order_id,b.from_prod_id";
					$data_transfer_prein_array=sql_select($sql_transfer_prein);
					if(count($data_transfer_prein_array)>0)
					{
						foreach( $data_transfer_prein_array as $row )
						{
							$transfer_prein_arr[$row[csf('to_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_in_qnty')];
						}
					}
					$data_tran_pre_out_arr=array(); 
					$sql_transfer_pre_out="select a.id, a.from_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_out_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_name and a.id=b.mst_id  and a.transfer_date<".$txt_date_from." and a.transfer_criteria=4 and a.item_category=2 group by a.from_order_id,b.from_prod_id";
					$data_tran_pre_out_arr=sql_select($sql_transfer_out);
					if(count($data_transfer_out_array)>0)
					{
						foreach( $data_transfer_out_array as $row)
						{
							$data_tran_pre_out_arr[$row[csf('from_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_out_qnty')];
						}
					}
   
 ?>
  <fieldset style="width:2410px;">
        	<table cellpadding="0" cellspacing="0" width="1120">
        
            </table>
             <br />	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="2360" class="rpt_table">
                <thead>
                	<tr>
                        <th width="30" rowspan="2">SL</th>
                        <th width="100" rowspan="2">Buyer</th>
                        <th width="50" rowspan="2">Job No</th>
                        <th width="50" rowspan="2">Year</th>
                        <th width="100" rowspan="2">Order No</th>
                        <th width="70" rowspan="2">Cut. Inch.</th>
                        <th width="70" rowspan="2">Tod</th>
                        <th width="70" rowspan="2">Style</th>
                        <th width="70" rowspan="2">Color</th>
                        <th width="70" rowspan="2">Order Qty.</th>
                        <th width="70" rowspan="2">Plan Cut Qty.</th>
                        <th width="70" rowspan="2">Fabric Qty</th>
                        <th width="200" colspan="4">Fabric Receive Qty.</th>
                        <th width="200" colspan="4">Cutting</th>
                        <th width="200" colspan="4">Delivery to Embellishment</th>
                        <th width="150" colspan="3">Receive from Embel</th>
                        <th width="60" rowspan="2">WIP in Print</th>
                        <th width="200" colspan="4">Receive From Cut</th>
                        <th width="250" colspan="5">Input</th>
                        <th width="60" rowspan="2">Inhand</th>
                        <th width="60" rowspan="2">Ready For Input</th>
                        <th width="60" rowspan="2">Line No</th>
                        <th width="100" rowspan="2">Remarks</th>
                    </tr>
                    <tr>
                        <th width="50" rowspan="2">Prev.</th>
                        <th width="50" rowspan="2">Today </th>
                        <th width="50" rowspan="2">Total </th>
                        <th width="50" rowspan="2">Bal.</th>
                        <th width="50" rowspan="2">Prev.</th>
                        <th width="50" rowspan="2">Today </th>
                        <th width="50" rowspan="2">Total </th>
                        <th width="50" rowspan="2">Bal.</th>
                        <th width="50" rowspan="2">Prev.</th>
                        <th width="50" rowspan="2">Today </th>
                        <th width="50" rowspan="2">Total </th>
                        <th width="50" rowspan="2">Bal.</th>
                        <th width="50" rowspan="2">Prev.</th>
                        <th width="50" rowspan="2">Today </th>
                        <th width="50" rowspan="2">Total </th>
                        <th width="50" rowspan="2">Prev.</th>
                        <th width="50" rowspan="2">Today </th>
                        <th width="50" rowspan="2">Total </th>
                        <th width="50" rowspan="2">Bal.</th>
                        <th width="50" rowspan="2">Prev.</th>
                        <th width="50" rowspan="2">Today</th>
                        <th width="50" rowspan="2">Total</th>
                        <th width="50" rowspan="2">%</th>
                        <th width="50" rowspan="2">Balance</th>
                    </tr>
                </thead>
            </table>
             <div style="max-height:425px; overflow-y:scroll; width:2380px; margin-left:15px" id="scroll_body">
                    <table cellspacing="0" border="1" class="rpt_table"  width="2360" rules="all" id="table_body" >
                    <?php
                     $total_cut=0;
					
                      $total_print_iss=0;
                      $total_print_rec=0;
                      $total_sew_input=0;
                      $total_sew_out=0;
					  $total_delivery_cut=0;
                      $cutting_balance=0;
					  $print_issue_balance=0;
					  $print_rec_balance=0;
				  	  $deliv_cut_bal=0;
					  $total_sew_input_balance=0;
					  $input_percentage=0;
					  $inhand=0;
					  $buyer_total_order=0;
					  $buyer_total_plan=0;
					  $buyer_total_fabric_qty=0;
					  $buyer_total_fabric_pre=0;
					  $buyer_fabric_total=0;
					  $buyer_fabric_today_total=0;
					  $buyer_fabric_bal=0;
					  $buyer_pre_cut=0;
					  $buyer_today_cut=0;
					  $buyer_total_cut=0;
					  $buyer_cutting_balance=0;
					  $buyer_priv_print_iss=0;
					  $buyer_today_print_iss=0;
					  $buyer_print_issue_balance=0;
					  $buyer_priv_print_rec=0;
					  $buyer_today_print_rec=0;
					  $buyer_total_print_rec=0;
					  $buyer_print_rec_balance=0;
					  $buyer_priv_deliv_cut=0;
					  $buyer_today_deliv_cut=0;
					  $buyer_total_delivery_cut=0;
					  $buyer_deliv_cut_bal=0;
					  $buyer_priv_sew=0;
					  $buyer_today_sew=0;
					  $buyer_total_sew_=0;
					  $buyer_total_sew__bal=0;
					  $buyer_inhand=0;
					  $buyer_arr=array();
					  $job_arr=array();
                      $i=1;
					
					if(str_replace("'","",$cbo_company_name)==0) $company_name=""; else $company_name=" and b.company_name=".str_replace("'","",$cbo_company_name)."";
	                if(str_replace("'","",$cbo_buyer_name)==0)  $buyer_name=""; else $buyer_name="and b.buyer_name=".str_replace("'","",$cbo_buyer_name)."";
					if (str_replace("'","",$hidden_order_id)!="") $order_cond="and a.id in (".str_replace("'","",$hidden_order_id).")"; 
					else if (str_replace("'","",$txt_order_no)=="") $order_cond=""; else $order_cond="and a.po_number='".str_replace("'","",$txt_order_no)."'"; 
					if(str_replace("'","",$hidden_job_id)!="")  $job_cond="and a.id in(".str_replace("'","",$hidden_job_id).")";
                    else  if (str_replace("'","",$txt_job_no)=="") $job_cond=""; else $job_cond="and a.job_no_mst='".str_replace("'","",$txt_job_no)."'";
					if(str_replace("'","",$hidden_style_id)!="")  $style_cond="and b.id in(".str_replace("'","",$hidden_style_id).")";
                    else  if (str_replace("'","",$txt_style_no)=="") $style_cond=""; else $style_cond="and b.style_ref_no='".$txt_style_no."'";
				//if(str_replace("'","",$txt_date_from)!="")  $date_cond="and SUBSTRING_INDEX(a.insert_date, ' ', 1)=".$txt_date_from."";
					
                   $pro_date_sql=sql_select ("SELECT a.id,a.job_no_mst,a.po_number,
					sum(d.order_quantity) as order_qty,sum(d.plan_cut_qnty) as plan_qty,b.buyer_name,b.style_ref_no as style,b.job_no_prefix_num,SUBSTRING_INDEX(b.insert_date, '-', 1) as year ,d.color_number_id,d.id as color_size_break_id
					from 
						wo_po_break_down a,wo_po_details_master b, wo_po_color_size_breakdown d
					where 
						a.job_no_mst=b.job_no and a.id=d.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1   $company_name $buyer_name $style_cond $order_cond $job_cond $date_cond  group by d.po_break_down_id,d.color_number_id order by b.buyer_name");
					foreach($pro_date_sql as $sql_row)
						{ 
						 $po_number_arr[$sql_row[csf('id')]]['po_id']=$sql_row[csf('id')];
						 $po_number_arr[$sql_row[csf('id')]]['job_no']=$sql_row[csf('job_no_mst')];
						 $po_number_arr[$sql_row[csf('id')]]['po_no']=$sql_row[csf('po_number')];
						 $po_number_arr[$sql_row[csf('id')]]['order_qty']=$sql_row[csf('order_qty')]; 
					     $po_number_arr[$sql_row[csf('id')]]['plan_qty']=$sql_row[csf('plan_qty')];
					     $po_number_arr[$sql_row[csf('id')]]['buyer_id']=$sql_row[csf('buyer_name')];
						 $po_number_arr[$sql_row[csf('id')]]['style']=$sql_row[csf('style')];
						 $po_number_arr[$sql_row[csf('id')]]['year']=$sql_row[csf('year')];
						 $po_number_arr[$sql_row[csf('id')]]['color_no_id']=$sql_row[csf('color_number_id')];
						 $po_number_arr[$sql_row[csf('id')]]['color_break_id']=$sql_row[csf('color_size_break_id')];
						}
	          
 					//foreach($pro_date_sql as $pro_date_sql_row)
                   // {  
					  		$transfer_prein_arr=$transfer_in_arr[$row[csf('id')]][$row[csf('color_number_id')]];
				            $data_tran_pre_out_arr=$transfer_out_arr[$row[csf('id')]][$row[csf('color_number_id')]];		  
							$transfer_in_qty=$transfer_in_arr[$row[csf('id')]][$row[csf('color_number_id')]];
				            $transfer_out_qty=$transfer_out_arr[$row[csf('id')]][$row[csf('color_number_id')]];
						    $sql_fabric=sql_select( "SELECT 
										  sum(CASE WHEN b.transaction_date = ".$txt_date_from." THEN a.quantity ELSE 0 END ) AS fabric_qty,
										  sum(CASE WHEN b.transaction_date <".$txt_date_from." THEN quantity ELSE 0 END ) AS fabric_qty_pre
										  FROM order_wise_pro_details a,  inv_transaction b
										  WHERE a.trans_id = b.id
										  AND a.po_breakdown_id =".$po_number_arr[$sql_row[csf('id')]]['po_id']."
										  AND a.entry_form =7
										  AND a.color_id =".$po_number_arr[$sql_row[csf('id')]]['color_no_id']."
										  AND a.trans_type =1 and b.status_active=1 and b.is_deleted=0");
						
						    foreach($sql_fabric as $row_fab)	
									{
									 $fabric_pre=$row_fab[csf("fabric_qty_pre")]+$transfer_prein_arr-$data_tran_pre_out_arr;	
									 $fabric_today=$row_fab[csf("fabric_qty")]+$transfer_in_qty-$transfer_out_qty;
									 $total_fabric=$fabric_pre+$fabric_today;	
									 $fabric_balance=$total_fabric-$fabric_qty;		
									}

							foreach($sql_sewing as $row_sew)
							       {
								   $con_per_peice= str_replace("'","",$row_sew[csf("cons")])/str_replace("'","",$row_sew[csf("pcs")]);
							       }
			
					  if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
                    
									   $con_avg=0;
									   $sewing_line="";
									   $cutting_sql=sql_select("select a.id,
									   sum(CASE WHEN b.production_type ='1' and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cutting_qnty,
									   sum(CASE WHEN b.production_type ='1' and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cutting_qnty_pre,                                       sum(CASE WHEN b.production_type ='2' and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printing_qnty,
									   sum(CASE WHEN b.production_type ='2' and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printing_qnty_pre ,
									   sum(CASE WHEN b.production_type =3 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printreceived_qnty,
									   sum(CASE WHEN b.production_type ='3' and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printreceived_qnty_pre,
									   sum(CASE WHEN b.production_type =9 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cut_delivery_qnty,
									   sum(CASE WHEN b.production_type =9 and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cut_delivery_qnty_pre,
									   sum(CASE WHEN b.production_type =4 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS sewingin_qnty,
									   sum(CASE WHEN b.production_type =4 and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS sewingin_qnty_pre 
									   from  pro_garments_production_dtls b,pro_garments_production_mst a,wo_po_color_size_breakdown c where a.id=b.mst_id and b.color_size_break_down_id=c.id and a.po_break_down_id='".$po_number_arr[$sql_row[csf('id')]]['po_id']."' and a.po_break_down_id=c.po_break_down_id and c.color_number_id=".$po_number_arr[$sql_row[csf('id')]]['color_no_id']."");
									
									   foreach($cutting_sql as $cut_row)
										   {
										     $priv_cut=$cut_row[csf("cutting_qnty_pre")]; 
										     $today_cut=$cut_row[csf("cutting_qnty")]; 
											 $priv_print_iss=$print_row[csf("printing_qnty_pre")]; 
										     $today_print_iss=$print_row[csf("printing_qnty")]; 
											 $priv_print_rec=$prec_row[csf("printreceived_qnty_pre")]; 
										     $today_print_rec=$prec_row[csf("printreceived_qnty")];
											 $priv_deliv_cut=$cut_deli_row[csf("cut_delivery_qnty_pre")]; 
										     $today_deliv_cut=$cut_deli_row[csf("cut_delivery_qnty")]; 
											 $priv_sew=$sew_row[csf("sewingin_qnty_pre")]; 
										     $today_sew=$sew_row[csf("sewingin_qnty")];  
										   }
					            
						
						
							
						  $sql_sewing=sql_select("SELECT sum( b.cons ) / count( b.color_number_id ) AS conjunction,pcs
                                       FROM wo_pre_cos_fab_co_avg_con_dtls b, wo_pre_cost_fabric_cost_dtls a
                                       WHERE a.id = b.pre_cost_fabric_cost_dtls_id
                                       AND b.job_no = '".$po_number_arr[$sql_row[csf('id')]]['job_no']."'
                                       AND b.po_break_down_id =".$po_number_arr[$sql_row[csf('id')]]['po_id']."
                                       AND b.color_number_id =".$po_number_arr[$sql_row[csf('id')]]['color_no_id']."
                                       GROUP BY a.body_part_id");	
					  	  foreach($sql_sewing as $row_sew)
								   {
									  $con_avg+= str_replace("'","",$row_sew[csf("conjunction")]);///str_replace("'","",$row_sew[csf("pcs")]);
									  $con_per_pcs=$con_avg/str_replace("'","",$row_sew[csf("pcs")]);
								       
								   }
				
								//  echo $con_per_pcs;die; 
		       $fabric_qty=$po_number_arr[$sql_row[csf('id')]]['plan_qty']*$con_per_pcs;
			  // echo $fabric_qty;
				if($today_cut!=0 || $today_print_iss!=0 || $today_print_rec!=0 || $today_deliv_cut!=0 || $today_sew!=0 || $fabric_today!=0 )
				   {
					    if($i!=1)
					  {
					     if(!in_array($pro_date_sql_row[csf("job_no_mst")],$job_arr))
							    {
								  
								?>
							      <tr bgcolor="#D3D3D3" id="">
                                        <td width="30"><?php // echo $i;?></td>
                                        <td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                                        <td width="50"></td>
                                        <td width="50"></td>
                                        <td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
                                        <td width="70"></td>
                                        <td width="70"></td>
                                        <td width="65"><strong>Job</strong></td>
                                        <td width="70"><strong>Total:</strong></td>
                                        <td width="70"><?php echo $job_total_order; ?></td>
                                        <td width="70" align="right"><?php  echo $job_total_plan; ?></td>
                                        <td width="70" align="right"><?php  echo number_format($job_total_fabric_qty,2); ?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_total_fabric_pre,2); ?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_fabric_today_total,2); ?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_fabric_total,2);?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_fabric_bal,2); ?></td>
                                        <td width="50" align="right"><?php  echo $job_pre_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_cutting_balance; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_print_iss; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_print_iss; ?></td>
                                        <td width="50" align="right"><?php  echo $job_print_issue_balance; ?></td>
                                        <td width="50" align="right"><?php  echo $job_print_issue_balance; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_print_rec; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_print_rec; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_print_rec; ?></td>
                                        <td width="60" align="right"><?php  echo $job_print_rec_balance;; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_deliv_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_deliv_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_delivery_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_deliv_cut_bal; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_sew; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_sew; ?></td>
                                        <td width="50" align="right"> <?php echo $job_total_sew_input; ?></td>
                                        <td width="50" align="right"><?php //echo $input_percentage; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_sew__bal; ?></td>
                                        <td width="60" align="right"><?php echo $job_inhand; ?></td>
                                        <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                                        <td width="60" align="right"><?php //echo  $sewing_line ?></td>
                                        <td width="100" align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
					              </tr>
								<?php  
								  $job_total_order=0;
								  $job_total_plan=0;
								  $job_total_fabric_qty=0;
								  $job_total_fabric_pre=0;
								  $job_fabric_total=0;
								  $job_fabric_today_total=0;
								  $job_fabric_bal=0;
								  $job_pre_cut=0;
								  $job_today_cut=0;
								  $job_total_cut=0;
								  $job_cutting_balance=0;
								  $job_priv_print_iss=0;
								  $job_today_print_iss=0;
								  $job_total_print_iss=0;
								  $job_print_issue_balance=0;
								  $job_priv_print_rec=0;
								  $job_today_print_rec=0;
								  $job_total_print_rec=0;
								  $job_print_rec_balance=0;
								  $job_priv_deliv_cut=0;
								  $job_today_deliv_cut=0;
								  $job_total_delivery_cut=0;
								  $job_deliv_cut_bal=0;
								  $job_priv_sew=0;
								  $job_today_sew=0;
								  $job_total_sew_input=0;
								  $job_total_sew__bal=0;
							  }
							
					  }
					
					 
					   if($i!=1)
					     	      {	
								  
								  		  
							     if( !in_array($pro_date_sql_row[csf("buyer_name")],$buyer_arr))
										{
											
											
									  
										?>
                                        <tbody>
											<tr bgcolor="#999999" style=" height:15px">
												<td width="30"><?php // echo $i;?></td>
												<td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
												<td width="50"></td>
                                                <td width="50"></td>
												<td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
												<td width="70"></td>
												<td width="70"></td>
												<td width="65"><strong> Buyer</strong></td>
												<td width="70"><strong>Total:</strong></td>
												<td width="70" align="right"><?php echo $buyer_total_order; ?></td>
												<td width="70" align="right"><?php  echo $buyer_total_plan; ?></td>
												<td width="70" align="right"><?php  echo number_format($buyer_total_fabric_qty,2); ?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_total_fabric_pre,2); ?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_fabric_today_total,2); ?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_fabric_total,2);?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_fabric_bal,2); ?></td>
												<td width="50" align="right"><?php  echo $buyer_pre_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_cutting_balance; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_print_iss; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_print_iss; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_print_iss; ?></td>
												<td width="50" align="right"><?php  echo $buyer_print_issue_balance; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_print_rec; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_print_rec; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_print_rec; ?></td>
												<td width="60" align="right"><?php  echo $buyer_print_rec_balance;; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_deliv_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_deliv_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_delivery_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_deliv_cut_bal; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_sew; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_sew; ?></td>
												<td width="50" align="right"> <?php echo $buyer_total_sew_; ?></td>
												<td width="50" align="right"><?php //echo $input_percentage; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_sew_bal; ?></td>
												<td width="60" align="right"><?php echo $buyer_inhand; ?></td>
												<td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
												<td width="60" align="right"><?php //echo  $sewing_line ?></td>
												<td width="100" align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
										  </tr>
                                          </tbody>
										<?php  
										  $buyer_total_order=0;
										  $buyer_total_plan=0;
										  $buyer_total_fabric_qty=0;
										  $buyer_total_fabric_pre=0;
										  $buyer_fabric_total=0;
										  $buyer_fabric_today_total=0;
										  $buyer_fabric_bal=0;
										  $buyer_pre_cut=0;
										  $buyer_today_cut=0;
										  $buyer_total_cut=0;
										  $buyer_cutting_balance=0;
										  $buyer_priv_print_iss=0;
										  $buyer_today_print_iss=0;
										  $buyer_total_print_iss=0;
										  $buyer_print_issue_balance=0;
										  $buyer_priv_print_rec=0;
										  $buyer_today_print_rec=0;
										  $buyer_total_print_rec=0;
										  $buyer_print_rec_balance=0;
										  $buyer_priv_deliv_cut=0;
										  $buyer_today_deliv_cut=0;
										  $buyer_total_delivery_cut=0;
										  $buyer_deliv_cut_bal=0;
										  $buyer_priv_sew=0;
										  $buyer_today_sew=0;
										  $buyer_total_sew_=0;
										  $buyer_total_sew__bal=0;
										  $buyer_inhand=0;
									  }
							    } 
					   
					   
						$total_cut=$priv_cut+$today_cut;
						$cutting_balance=$total_cut-$pro_date_sql_row[csf("plan_qty")];
						$total_print_iss=$priv_print_iss+$today_print_iss;
						$print_issue_balance=$total_print_iss-$total_cut;
						$total_print_rec=$priv_print_rec+$today_print_rec;
						$print_rec_balance=$total_print_rec-$total_print_iss;
                        $total_delivery_cut=$today_deliv_cut+$priv_deliv_cut;
						$deliv_cut_bal=($total_cut-$total_print_iss+$total_print_rec)-$total_delivery_cut;
						$total_sew_input=$today_sew+$priv_sew;
						$total_sew_input_balance=$total_sew_input-$pro_date_sql_row[csf("plan_qty")];
						$input_percentage=($total_sew_input/$pro_date_sql_row[csf("order_qty")])*100;
						$inhand=$total_delivery_cut-$total_sew_input;
						//for job total *******************************************************************************************************
						$job_total_order+=$pro_date_sql_row[csf("order_qty")];
						$job_total_plan+=$pro_date_sql_row[csf("plan_qty")];
						$job_total_fabric_qty+=$fabric_qty;
						$job_total_fabric_pre+=$fabric_pre;
						$job_fabric_today_total+=$fabric_today;
						$job_fabric_total+=$total_fabric;
						$job_fabric_bal+=$fabric_balance;
						$job_pre_cut+=$priv_cut;
						$job_today_cut+=$today_cut;
						$job_total_cut+=$total_cut;
						$job_cutting_balance+=$cutting_balance;
						$job_priv_print_iss+=$priv_print_iss;
						$job_today_print_iss+=$today_print_iss;
						$job_total_print_iss+=$total_print_iss;
						$job_print_issue_balance+=$print_issue_balance;
						$job_priv_print_rec+=$priv_print_rec;
						$job_today_print_rec+=$today_print_rec;
						$job_total_print_rec+=$total_print_rec;
						$job_print_rec_balance+=$print_rec_balance;
						$job_priv_deliv_cut+=$priv_deliv_cut;
						$job_today_deliv_cut+=$today_deliv_cut;
						$job_total_delivery_cut+=$total_delivery_cut;
						$job_deliv_cut_bal+=$deliv_cut_bal;
						$job_priv_sew+=$priv_sew;
						$job_today_sew+=$today_sew;
						$job_total_sew_input+=$total_sew_input;
						$job_total_sew_bal+=$total_sew_input_balance;
						$job_inhand+=$inhand;
						//buyer sub total **************************************************************************************************
					    $buyer_total_order+=$pro_date_sql_row[csf("order_qty")];
						$buyer_total_plan+=$pro_date_sql_row[csf("plan_qty")];
						$buyer_total_fabric_qty+=$fabric_qty;
						$buyer_total_fabric_pre+=$fabric_pre;
						$buyer_fabric_today_total+=$fabric_today;
						$buyer_fabric_total+=$total_fabric;
						$buyer_fabric_bal+=$fabric_balance;
						$buyer_pre_cut+=$priv_cut;
						$buyer_today_cut+=$today_cut;
						$buyer_total_cut+=$total_cut;
						$buyer_cutting_balance+=$cutting_balance;
						$buyer_priv_print_iss+=$priv_print_iss;
						$buyer_today_print_iss+=$today_print_iss;
						$buyer_total_print_iss+=$total_print_iss;
						$buyer_print_issue_balance+=$job_print_issue_balance;
						$buyer_priv_print_rec+=$priv_print_rec;
						$buyer_today_print_rec+=$today_print_rec;
						$buyer_total_print_rec+=$total_print_rec;
						$buyer_print_rec_balance+=$print_rec_balance;
						$buyer_priv_deliv_cut+=$priv_deliv_cut;
						$buyer_today_deliv_cut+=$today_deliv_cut;
						$buyer_total_delivery_cut+=$total_delivery_cut;
						$buyer_deliv_cut_bal+=$deliv_cut_bal;
						$buyer_priv_sew+=$priv_sew;
						$buyer_today_sew+=$today_sew;
						$buyer_total_sew_+=$total_sew_input;
						$buyer_total_sew_bal+=$total_sew_input_balance;
						$buyer_inhand+=$inhand;
						// for grand total ********************************************************************************************************************
					    $grand_total_order+=$pro_date_sql_row[csf("order_qty")];
						$grand_total_plan+=$pro_date_sql_row[csf("plan_qty")];
						$grand_total_fabric_qty+=$fabric_qty;
						$grand_total_fabric_pre+=$fabric_pre;
						$grand_fabric_today_total+=$fabric_today;
						$grand_fabric_total+=$total_fabric;
						$grand_fabric_bal+=$fabric_balance;
						$grand_pre_cut+=$priv_cut;
						$grand_today_cut+=$today_cut;
						$grand_total_cut+=$total_cut;
						$grand_cutting_balance+=$cutting_balance;
						$grand_priv_print_iss+=$priv_print_iss;
						$grand_today_print_iss+=$today_print_iss;
						$grand_total_print_iss+=$total_print_iss;
						$grand_print_issue_balance+=$job_print_issue_balance;
						$grand_priv_print_rec+=$priv_print_rec;
						$grand_today_print_rec+=$today_print_rec;
						$grand_total_print_rec+=$total_print_rec;
						$grand_print_rec_balance+=$print_rec_balance;
						$grand_priv_deliv_cut+=$priv_deliv_cut;
						$grand_today_deliv_cut+=$today_deliv_cut;
						$grand_total_delivery_cut+=$total_delivery_cut;
						$grand_deliv_cut_bal+=$deliv_cut_bal;
						$grand_priv_sew+=$priv_sew;
						$grand_today_sew+=$today_sew;
						$grand_total_sew_+=$total_sew_input;
						$grand_total_sew_bal+=$total_sew_input_balance;
						$grand_inhand+=$inhand;
						//$sewing_line=return_field_value("sewing_line","pro_garments_production_mst","po_break_down_id='".$pro_date_sql_row[csf("id")]."' and production_type ='4' ");
						
						//echo "select distinct sewing_line from pro_garments_production_mst where po_break_down_id='".$pro_date_sql_row[csf("id")]."' and production_type='4' ";die;
						$sql_line=sql_select("select distinct sewing_line from pro_garments_production_mst where po_break_down_id='".$pro_date_sql_row[csf("id")]."' and production_type='4' ");
						
						$j=1;
						foreach($sql_line as $row_line)
							{
							 if($j==1)
								 {
									$sewing_line=$line_arr[$row_line[csf("sewing_line")]];
									 
								 }
							 if($j!=1)
								 {
									$sewing_line.=",".$line_arr[$row_line[csf("sewing_line")]];
									 
								 }
							
						$j++;
							
							}
							//echo $sewing_line."***";
                    ?>
                    	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $i; ?>">
                            <td width="30"><?php echo $i;?></td>
                            <td width="100"><?php echo $buyer_short_library[$po_number_arr[$sql_row[csf('id')]]['buyer_id']]; ?></td>
                            <td width="50" align="right"><?php echo $pro_date_sql_row[csf("job_no_prefix_num")];?></td>
                            <td width="50" align="right"><?php echo $pro_date_sql_row[csf("year")];?></td>
                            <td width="100" align="center"><?php echo $pro_date_sql_row[csf("po_number")];?></td>
                            <td width="70"></td>
                            <td width="70"></td>
                            <td width="70"><?php echo $pro_date_sql_row[csf("style")]; ?></td>
                            <td width="70"><?php echo $color_arr[$pro_date_sql_row[csf("color_number_id")]]; ?></td>
                            <td width="70" align="right"><?php echo $pro_date_sql_row[csf("order_qty")]; ?></td>
                            <td width="70" align="right"><?php  echo $pro_date_sql_row[csf("plan_qty")]; ?></td>
                            <td width="70" align="right"><?php  echo number_format($fabric_qty,2); ?></td>
                            <td width="50" align="right"><?php  echo number_format($fabric_pre,2); ?></td>
                            <td width="50" align="right"><?php  echo number_format($fabric_today,2); ?></td>
                            <td width="50" align="right"><?php  echo number_format($total_fabric,2);?></td>
                            <td width="50" align="right"><?php  echo number_format($fabric_balance,2); ?></td>
                            <td width="50" align="right"><?php  echo $priv_cut; ?></td>
                            <td width="50" align="right"><?php  echo $today_cut; ?></td>
                            <td width="50" align="right"><?php  echo $total_cut; ?></td>
                            <td width="50" align="right"><?php  echo $cutting_balance; ?></td>
                            <td width="50" align="right"><?php  echo $priv_print_iss; ?></td>
                            <td width="50" align="right"><?php  echo $today_print_iss; ?></td>
                            <td width="50" align="right"><?php  echo $total_print_iss; ?></td>
                            <td width="50" align="right"><?php  echo $print_issue_balance; ?></td>
                            <td width="50" align="right"><?php  echo $priv_print_rec; ?></td>
                            <td width="50" align="right"><?php  echo $today_print_rec; ?></td>
                            <td width="50" align="right"><?php  echo $total_print_rec; ?></td>
                            <td width="60" align="right"><?php  echo $print_rec_balance;; ?></td>
                            <td width="50" align="right"><?php  echo $priv_deliv_cut; ?></td>
                            <td width="50" align="right"><?php  echo $today_deliv_cut; ?></td>
                            <td width="50" align="right"><?php  echo $total_delivery_cut; ?></td>
                            <td width="50" align="right"><?php  echo $deliv_cut_bal; ?></td>
                            <td width="50" align="right"><?php  echo $priv_sew; ?></td>
                            <td width="50" align="right"><?php  echo $today_sew; ?></td>
                            <td width="50" align="right"> <?php echo $total_sew_input; ?></td>
                            <td width="50" align="right"><?php  echo number_format($input_percentage,2); ?></td>
                            <td width="50" align="right"><?php  echo $total_sew_input_balance; ?></td>
                            <td width="60" align="right"><?php  echo $inhand; ?></td>
                            <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                            <td width="60" align="right"><?php  echo  $sewing_line ?></td>
                            <td width="100" align="right"><?php if($total_delivery_cut-$pro_date_sql_row[csf("plan_qty")]>0) echo "Receive Ok"; if($total_sew_input_balance-$pro_date_sql_row[csf("plan_qty")]>0) echo "Input Ok";  ?></td>
                 </tr>
						<?php	
						 $job_arr[]=$pro_date_sql_row[csf("job_no_mst")];
						 $buyer_arr[]=$pro_date_sql_row[csf("buyer_name")];
				     	 $i++;					
                        } //end foreach 2nd
						
							  
				
					
				//}//end foreach 1st
			
				?>
			                  	 <tr bgcolor="#D3D3D3" id="">
                                        <td width="30"><?php // echo $i;?></td>
                                        <td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                                        <td width="50"></td>
                                        <td width="50"></td>
                                        <td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
                                        <td width="70"></td>
                                        <td width="70"></td>
                                        <td width="70"><strong>Job</strong></td>
                                        <td width="70"><strong>Total:</strong></td>
                                        <td width="70" align="right"><?php echo $job_total_order; ?></td>
                                        <td width="70" align="right"><?php  echo $job_total_plan; ?></td>
                                        <td width="70" align="right"><?php  echo number_format($job_total_fabric_qty,2); ?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_total_fabric_pre,2); ?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_fabric_today_total,2); ?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_fabric_total,2);?></td>
                                        <td width="50" align="right"><?php  echo number_format($job_fabric_bal,2); ?></td>
                                        <td width="50" align="right"><?php  echo $job_pre_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_cutting_balance; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_print_iss; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_print_iss; ?></td>
                                        <td width="50" align="right"><?php  echo $job_print_issue_balance; ?></td>
                                        <td width="50" align="right"><?php  echo $job_print_issue_balance; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_print_rec; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_print_rec; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_print_rec; ?></td>
                                     
                                        <td width="60" align="right"><?php  echo $job_print_rec_balance;; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_deliv_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_deliv_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_delivery_cut; ?></td>
                                        <td width="50" align="right"><?php  echo $job_deliv_cut_bal; ?></td>
                                        <td width="50" align="right"><?php  echo $job_priv_sew; ?></td>
                                        <td width="50" align="right"><?php  echo $job_today_sew; ?></td>
                                        <td width="50" align="right"> <?php echo $job_total_sew_input; ?></td>
                                        <td width="50" align="right"><?php //echo $input_percentage; ?></td>
                                        <td width="50" align="right"><?php  echo $job_total_sew__bal; ?></td>
                                        <td width="60" align="right"><?php echo $job_inhand; ?></td>
                                        <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                                        <td width="60" align="right"><?php //echo  $sewing_line ?></td>
                                        <td width="100" align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
					              </tr>
                                   <tr bgcolor="#999999" id="">
												<td width="30"><?php // echo $i;?></td>
												<td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
												<td width="50"></td>
                                                <td width="50"></td>
												<td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
												<td width="70"></td>
												<td width="70"></td>
												<td width="70"><strong> Buyer</strong></td>
												<td width="70"><strong>Total:</strong></td>
												<td width="70" align="right"><?php echo $buyer_total_order; ?></td>
												<td width="70" align="right"><?php  echo $buyer_total_plan; ?></td>
												<td width="70" align="right"><?php  echo number_format($buyer_total_fabric_qty,2); ?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_total_fabric_pre,2); ?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_fabric_today_total,2); ?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_fabric_total,2);?></td>
												<td width="50" align="right"><?php  echo number_format($buyer_fabric_bal,2); ?></td>
												<td width="50" align="right"><?php  echo $buyer_pre_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_cutting_balance; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_print_iss; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_print_iss; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_print_iss; ?></td>
												<td width="50" align="right"><?php  echo $buyer_print_issue_balance; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_print_rec; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_print_rec; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_print_rec; ?></td>
												
												<td width="60" align="right"><?php  echo $buyer_print_rec_balance;; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_deliv_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_deliv_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_delivery_cut; ?></td>
												<td width="50" align="right"><?php  echo $buyer_deliv_cut_bal; ?></td>
												<td width="50" align="right"><?php  echo $buyer_priv_sew; ?></td>
												<td width="50" align="right"><?php  echo $buyer_today_sew; ?></td>
												<td width="50" align="right"> <?php echo $buyer_total_sew_; ?></td>
												<td width="50" align="right"><?php //echo $input_percentage; ?></td>
												<td width="50" align="right"><?php  echo $buyer_total_sew_bal; ?></td>
												<td width="60" align="right"><?php echo $buyer_inhand; ?></td>
												<td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
												<td width="60" align="right"><?php //echo  $sewing_line ?></td>
												<td width="100" align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
										  </tr>
                                      <tfoot>
                                            <tr>
                                            <td width="30"><?php // echo $i;?></td>
                                            <td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                                            <td width="50"></td>
                                            <td width="50"></td>
                                            <td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
                                            <td width="70"></td>
                                            <td width="70"></td>
                                            <td width="70"> <strong>Grand</strong></td>
                                            <td width="70"><strong>Total:</strong></td>
                                            <td width="70" align="right"><strong><?php echo $grand_total_order; ?></strong></td>
                                            <td width="70" align="right"><strong><?php  echo $grand_total_plan; ?></strong></td>
                                            <td width="70" align="right"><strong><?php  echo number_format($grand_total_fabric_qty,2); ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo number_format($grand_total_fabric_pre,2); ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo number_format($grand_fabric_today_total,2); ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo number_format($grand_fabric_total,2);?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo number_format($grand_fabric_bal,2); ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_pre_cut; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_today_cut; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_total_cut; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_cutting_balance; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_priv_print_iss; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_today_print_iss; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_total_print_iss; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_print_issue_balance; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_priv_print_rec; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_today_print_rec; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_total_print_rec; ?></strong></td>
                                            <td width="60" align="right"><strong><?php  echo $grand_print_rec_balance; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_priv_deliv_cut; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_today_deliv_cut; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_total_delivery_cut; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_deliv_cut_bal; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_priv_sew; ?></strong></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_today_sew; ?></strong></td>
                                            <td width="50" align="right"><strong><?php echo $grand_total_sew_; ?></strong></td>
                                            <td width="50" align="right"><?php //echo $input_percentage; ?></td>
                                            <td width="50" align="right"><strong><?php  echo $grand_total_sew_bal; ?></strong></td>
                                            <td width="60" align="right"><strong><?php echo $grand_inhand; ?></strong></td>
                                            <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                                            <td width="60" align="right"><?php //echo  $sewing_line ?></td>
                                            <td width="100" align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
                                     </tr> 
                                   </tfoot>
                                     <?php
								
								     ?>
                </table> 
           </div>     
  	</div>
 
<?php

foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
		}
		?>
  </fieldset>
 <?php	

?>