
<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//year(a.insert_date)
if($db_type==2 || $db_type==1 )
{
	$mrr_date_check=" to_char(a.insert_date,'YYYY')";
}
else if ($db_type==0)
{
	$mrr_date_check=" year(a.insert_date)";
}


 //-------------------START ----------------------------------------
$company_arr=return_library_array( "select id, company_name from  lib_company",'id','company_name');
$location_arr=return_library_array( "select id, location_name from  lib_location",'id','location_name');
$buyer_array = return_library_array("select id, short_name from  lib_buyer","id","short_name");
$brand_details=return_library_array( "select id, brand_name from lib_brand", "id", "brand_name"  );
$yarn_count_details=return_library_array( "select id,yarn_count from lib_yarn_count", "id", "yarn_count"  );
$order_no_arr=return_library_array( "select id, po_number from  wo_po_break_down",'id','po_number');
$supplier_arr=return_library_array( "select id, supplier_name from  lib_supplier",'id','supplier_name');
	

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_id", 120, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_id", 100, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  
	exit();
}
if($action=="load_drop_down_buyer_form")
{
	echo create_drop_down( "cbo_buyer_id", 120, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  
	exit();
}

$composition_arr=array();
$construction_arr=array();
$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
$data_array=sql_select($sql_deter);
if(count($data_array)>0)
{
/*foreach( $data_array as $row )
{
	if(array_key_exists($row[csf('id')],$composition_arr))
	{
		$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]].$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
	}
	else
	{
		$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
	}
}*/
	foreach( $data_array as $row )
	{
		if(array_key_exists($row[csf('id')],$construction_arr))
		{
			$construction_arr[$row[csf('id')]]=$construction_arr[$row[csf('id')]];
		}
		else
		{
			$construction_arr[$row[csf('id')]]=$row[csf('construction')];
		}
		if(array_key_exists($row[csf('id')],$composition_arr))
		{
			$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]];
		}
		else
		{
			$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
		}
	}
}



if($action=='list_generate')
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$permission=$_SESSION['page_permission'];
	$cbo_company_id=str_replace("'","",$cbo_company_id);
	$cbo_location_id=str_replace("'","",$cbo_location_id);
	$cbo_buyer_id=str_replace("'","",$cbo_buyer_id);
	$txt_prog_no=str_replace("'","",$txt_prog_no);
	$cbo_year=str_replace("'","",$cbo_year);
	$txt_job_no=str_replace("'","",$txt_job_no);
	$txt_ord_no=str_replace("'","",$txt_ord_no);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	$cbo_status=str_replace("'","",$cbo_status);
	$update_mst_id=str_replace("'","",$update_mst_id);
	
	$hidden_receive_id=str_replace("'","",$hidden_receive_id);
	$hidden_product_id=str_replace("'","",$hidden_product_id);
	$hidden_order_id=str_replace("'","",$hidden_order_id);
	if($hidden_receive_id=="") $hidden_receive_id=0;
	if($hidden_product_id=="") $hidden_product_id=0;
	if($hidden_order_id=="") $hidden_order_id=0;
	
	$sql_roll=sql_select("select mst_id,prod_id,order_id,no_of_roll as no_of_roll from pro_grey_prod_entry_dtls where status_active=1 and is_deleted=0");
	foreach($sql_roll as $row)
	{
		$order_arr=explode(",",$row[csf("order_id")]);
		foreach($order_arr as $ord_id)
		{
			$roll_arr[$row[csf("mst_id")]][$row[csf("prod_id")]][$ord_id] +=$row[csf("no_of_roll")];
		}
	}
	//echo $hidden_receive_id."***".$hidden_product_id."***".$hidden_order_id;die;
	
	if($txt_prog_no>0)
	{
		$program_cond="and a.booking_id='$txt_prog_no' and a.receive_basis=2";
	}
	else
	{
		$program_cond="";
	}
	if($txt_job_no!="")
	{
		if($cbo_year!="") $cbo_year="and $mrr_date_check='$cbo_year'"; else $cbo_year="";
		if($db_type==0)
		{
			$job_no_po_id=return_field_value("group_concat(distinct b.id) as po_id","wo_po_details_master a, wo_po_break_down b","a.job_no=b.job_no_mst and a.job_no_prefix_num='$txt_job_no' and a.status_active=1 $cbo_year","po_id");
		}
		else if($db_type==2)
		{
			$job_no_po_id=return_field_value(" LISTAGG(CAST( b.id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.id) as po_id","wo_po_details_master a, wo_po_break_down b","a.job_no=b.job_no_mst and a.job_no_prefix_num='$txt_job_no' and a.status_active=1 $cbo_year","po_id");
		}
	}
	//echo $job_no_po_id;die;
	if($txt_ord_no!="")
	{
		if($db_type==0)
		{
			$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '%$txt_ord_no' and status_active=1","po_id");
		}
		else if($db_type==2)
		{
			$po_id=return_field_value("LISTAGG(CAST( id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as po_id","wo_po_break_down","po_number like '%$txt_ord_no' and status_active=1","po_id");
		}
	
	}
	//echo $txt_date_from;
	if($cbo_location_id!=0) $location_cond="and a.location_id=$cbo_location_id"; else $location_cond=""; 
	if($cbo_buyer_id!=0) $buyer_cond="and a.buyer_id=$cbo_buyer_id"; else $buyer_cond="";
	if(trim($job_no_po_id)!="") $job_cond="and c.po_breakdown_id in(".trim($job_no_po_id).")"; else $job_cond="";
	if(trim($po_id)!="") $order_cond="and c.po_breakdown_id in(".$po_id.")"; else $order_cond="";
	if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.receive_date between '$txt_date_from' and '$txt_date_to'"; else $date_cond="";
	
	if($update_mst_id=="")
	{
		$sql="select 
					a.id,
					a.recv_number,
					a.receive_date,
					a.booking_id as prog_no,
					a.buyer_id,
					a.receive_basis,
					a.knitting_source,
					b.febric_description_id as determination_id,
					b.gsm,
					b.width as dia,
					b.prod_id,
					c.po_breakdown_id,
					c.quantity as quantity
				from
					inv_receive_master a,  pro_grey_prod_entry_dtls b, order_wise_pro_details c
				where
					a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and c.entry_form=2 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and c.trans_type=1 and a.company_id=$cbo_company_id and c.po_breakdown_id!=0 $location_cond $program_cond $buyer_cond $job_cond $order_cond $date_cond order by a.id";
	}
	else if($update_mst_id>0)
	{
		$sql="select 
					a.id,
					a.recv_number,
					a.receive_date,
					a.booking_id as prog_no,
					a.buyer_id,
					a.receive_basis,
					a.knitting_source,
					b.febric_description_id as determination_id,
					b.gsm,
					b.width as dia,
					b.prod_id,
					c.po_breakdown_id,
					c.quantity as quantity
				from
					inv_receive_master a,  pro_grey_prod_entry_dtls b, order_wise_pro_details c
				where
					a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and c.entry_form=2 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and c.trans_type=1 and a.company_id=$cbo_company_id and c.po_breakdown_id!=0 $location_cond $program_cond $buyer_cond $job_cond $order_cond $date_cond and a.id in($hidden_receive_id) and b.prod_id in($hidden_product_id) and c.po_breakdown_id in($hidden_order_id)
					order by a.id";
	}
		
	//echo $sql;die;
	$result=sql_select($sql);
	$product_id=0;$po_break_id=0;$detarminate_id=0;$result_arr=array();
	foreach($result as $row)
	{
		//if($product_id==0) $product_id=$row[csf("prod_id")]; else $product_id=$product_id.",".$row[csf("prod_id")];
		if($po_break_id==0) $po_break_id=$row[csf("po_breakdown_id")]; else $po_break_id=$po_break_id.",".$row[csf("po_breakdown_id")];
		//if($detarmination_id==0) $detarmination_id=$row[csf("detarmination_id")]; else $detarmination_id=$detarmination_id.",".$row[csf("detarmination_id")];
		
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['prod_id']= $row[csf("prod_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['po_breakdown_id']= $row[csf("po_breakdown_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['receive_id']= $row[csf("id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['recv_number']= $row[csf("recv_number")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['receive_date']= $row[csf("receive_date")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['receive_basis']= $row[csf("receive_basis")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['knitting_source']= $row[csf("knitting_source")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['prog_no']= $row[csf("prog_no")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['buyer_id']= $row[csf("buyer_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['detarmination_id']= $row[csf("determination_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['gsm']= $row[csf("gsm")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['dia_width']= $row[csf("dia")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['current_stock'] += $row[csf("quantity")];
		
	}
	//echo $sql;
	//print_r($result_arr);die;
	$po_break_id=array_chunk(array_unique(explode(",",$po_break_id)),999);
	if($db_type==2) $select_year="to_char(a.insert_date,'YYYY') as job_year"; else if($db_type==0)	$select_year="year(a.insert_date) as job_year";			
	$sql_job="select 
					b.id as po_id,
					b.po_number,
					a.job_no,
					a.job_no_prefix_num,
					$select_year
				from
					wo_po_details_master a, wo_po_break_down b
				where
					a.job_no=b.job_no_mst  ";
	$m=1;
	foreach($po_break_id as $id)
	{
		if($m==1) $sql_job .="and (b.id in(".implode(',',$id).")"; else $sql_job .=" or b.id in(".implode(',',$id).")";
		$m++;
	}
	$sql_job .=")";
	//echo $sql_job;
	$job_po_arr=array();
	$sql_job_result=sql_select($sql_job);
	//echo $sql_job;die;
	
	foreach($sql_job_result as $row)
	{
		$job_po_arr[$row[csf("po_id")]]["po_id"]=$row[csf("po_id")];
		$job_po_arr[$row[csf("po_id")]]["job_no"]=$row[csf("job_no_prefix_num")];
		$job_po_arr[$row[csf("po_id")]]["po_number"]=$row[csf("po_number")];
		$job_po_arr[$row[csf("po_id")]]["job_year"]=$row[csf("job_year")];
	}
	//var_dump($job_po_arr);die;
	$update_row_check=array();
	if($update_mst_id!="")
	{
		$sql_update=sql_select("select id,grey_sys_id,product_id,job_no,order_id,current_delivery,roll from pro_grey_prod_delivery_dtls where mst_id=$update_mst_id");
		foreach($sql_update as $row)
		{
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["current_delivery"] =$row[csf("current_delivery")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["id"] =$row[csf("id")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["roll"] =$row[csf("roll")];
		}
	}
	
	$sql_production=sql_select("Select grey_sys_id,product_id,order_id,current_delivery from pro_grey_prod_delivery_dtls where status_active=1 and is_deleted=0");
	foreach($sql_production as $row)
	{
		$sql_production_arr[$row[csf("grey_sys_id")]][$row[csf("product_id")]][$row[csf("order_id")]]['prodcut_qty'] +=$row[csf("current_delivery")];
	}
	
	
	//var_dump($update_row_check);die;
	ob_start();
	?>

<div style="width:1400px;" id="">
        <form name="delivery_details" id="delivery_details" autocomplete="off" > 
	<div id="report_print" style="width:1310px;">
    <table width="1380" class="rpt_table" id="tbl_header" cellpadding="0" cellspacing="1" rules="all">
    	<thead>
        	<th width="30">Sl</th>
            <th width="120">System Id</th>
            <th width="90">Progm/ Booking No</th>
            <th width="75">Knitting Source</th>
            <th width="70">Prd. date</th>
            <th width="50">Prod. Id</th>
            <th width="40">Year</th>
            <th width="60">Job No</th>
            <th width="50">Buyer</th>
            <th width="90">Order No</th>
            <th width="110">Construction </th>
            <th width="110">Composition</th>
            <th width="40">GSM</th>
            <th width="40">Dia</th>
            <th width="40">Roll</th>
            <th width="70">Prod. qty</th>
            <th width="70">Total Delivery</th>
            <th width="70">Balance</th>
            <th width="75" >Current Delv.</th>
            <th >Roll</th>
        </thead>
    </table>
    <div style="width:1398px; overflow-y:scroll; max-height:200px;font-size:12px; overflow-x:hidden;" id="scroll_body">
    <table width="1380" class="rpt_table" id="table_body" cellpadding="0" cellspacing="1" rules="all">
    	<tbody>
        <?php
		//var_dump($update_mst_id);
		$i=1;
		$current_row_array=array();
		foreach($result_arr as $recev_key=>$value)
		{
			foreach($value as $prod_key=>$val)
			{
				foreach($val as $po_key=>$row)
				{
					if ($i%2==0)
					$bgcolor="#E9F3FF";
					else
					$bgcolor="#FFFFFF";
					
					$tot_delivery=$sql_production_arr[$row[("receive_id")]][$row[("prod_id")]][$row[("po_breakdown_id")]]['prodcut_qty'];
					
					$index_pk=$row["receive_id"]."*".$row["prod_id"]."*".$row["po_breakdown_id"];
					$current_stock="$row[current_stock]";
					//$tot_delivery=number_format($tot_delivery,2);
					//$current_stock=number_format($row['current_stock'],2);
					//if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["id"]."xx";
					
					
					if($update_mst_id=="")
					{
						if($cbo_status==1)
						{
							//$tot_delivery=$tot_delivery*1;
							//var_dump($current_stock);
							//var_dump($tot_delivery);
							if( $current_stock > $tot_delivery)
							{
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>">
									<td width="30" align="center"><p><?php echo $i; ?>
									<input type="hidden" id="hidesysid_<?php echo $i;?>" name="hidesysid_<?php echo $i;?>" value="<?php echo $row["receive_id"];?>"  />&nbsp;
									</p></td>
									<td width="120"><p><input type="hidden" id="hidesysnum_<?php echo $i;?>" name="hidesysnum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
									<?php
									 echo $row["recv_number"]; 
									?>&nbsp;
									</p></td>
									<td width="90" align="center"><p>
									<input type="hidden" id="hideprogrum_<?php echo $i;?>" name="hideprogrum_<?php echo $i;?>" value="<?php echo $row["prog_no"];?>"  />
									<?php if($row["prog_no"]==0) echo "Independent"; else echo $row["prog_no"]; ?>&nbsp;
									</p></td>
									<td width="75" align="center"><p>
									<?php
									  if($row["knitting_source"]==1)  echo "In-House"; if($row["knitting_source"]==3) echo "Sub-Contract"; 
									?>&nbsp;
									</p></td>
									<td width="70" align="center"><p><?php  if($row["receive_date"]!='0000-00-00')  echo change_date_format($row["receive_date"]); else echo ""; ?>&nbsp;</p></td>
									<td width="50" align="center"><p>
									<input type="hidden" id="hideprodid_<?php echo $i;?>" name="hideprodid_<?php echo $i;?>" value="<?php echo $row["prod_id"];?>"  />
									<?php echo $row["prod_id"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p><?php echo $job_po_arr[$po_key]["job_year"]; ?>&nbsp;</p></td>
									<td width="60" align="center"><p>
									<input type="hidden" id="hidejob_<?php echo $i;?>" name="hidejob_<?php echo $i;?>" value="<?php echo $job_po_arr[$po_key]["job_no"];?>"  />
									<?php echo $job_po_arr[$po_key]["job_no"]; ?>&nbsp;
									</p></td>
									<td width="50"><p><?php echo $buyer_array[$row["buyer_id"]]; ?>&nbsp;</p></td>
									<td width="90"><p>
									<input type="hidden" id="hideorder_<?php echo $i;?>" name="hideorder_<?php echo $i;?>" value="<?php echo $row["po_breakdown_id"];?>"  />
									<?php echo $job_po_arr[$po_key]["po_number"]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									 <input type="hidden" id="hideconstruction_<?php echo $i;?>" name="hideconstruction_<?php echo $i;?>" value="<?php echo $construction_arr[$row["detarmination_id"]];?>"  />
									<?php echo $construction_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									<input type="hidden" id="hidecomposition_<?php echo $i;?>" name="hidecomposition_<?php echo $i;?>" value="<?php echo $composition_arr[$row["detarmination_id"]];?>"  />
									<?php echo $composition_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidegsm_<?php echo $i;?>" name="hidegsm_<?php echo $i;?>" value="<?php echo $row["gsm"]; ?>"  />
									<?php echo $row["gsm"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidedia_<?php echo $i;?>" name="hidedia_<?php echo $i;?>" value="<?php echo $row["dia_width"]; ?>"  />
									<?php echo $row["dia_width"]; ?>&nbsp;
									</p></td>
                                    <td width="40" align="center"><p>
									<?php echo $roll_arr[$row["receive_id"]][$row["prod_id"]][$row["po_breakdown_id"]]; ?>&nbsp;
									</p></td>
									<td width="70" align="right" ><p><?php echo number_format($row["current_stock"],2); $total_stock+=$row["current_stock"]; ?>&nbsp;</p></td>
									<td width="70" align="right" ><p>
									<?php
									echo number_format($tot_delivery,2);
									$gt_tot_delivery+=$tot_delivery;
									//echo $job_po_arr[$po_key]["po_id"];
									?>&nbsp;</p>
									</td>
									<td width="70"align="right" id="totalqtyTd_<?php echo $i; ?>"><p><?php $balance=($row["current_stock"]-$tot_delivery); echo number_format($balance,2); $total_balance+=$balance; ?>&nbsp;</p></td>
									<td width="75">
									<p><input type="hidden" id="hiddendtlsid_<?php echo $i;?>" name="hiddendtlsid_<?php echo $i;?>"  style="width:60px;" value="" />
										<input type="text" id="txtcurrentdelivery_<?php echo $i;?>" name="txtcurrentdelivery_<?php echo $i;?>" class="text_boxes_numeric" style="width:60px;" value="" onBlur="setHideval(<?php echo $i; ?>)" />
                                <input type="hidden" id="hidden_current_val_<?php echo $i;?>" value="">
									&nbsp;</p></td>
									<td ><p>
										<input type="text" id="txtroll_<?php echo $i;?>" name="txtroll_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" value="" onBlur="total_roll(<?php echo $i; ?>)"  />
                                <input type="hidden" id="hideroll_<?php echo $i;?>" value=""  >
									&nbsp;</p></td>
								</tr>
								<?php
								$i++;
							}
						}
						
						else
						{
							if($current_stock<=$tot_delivery)
							{
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>">
									<td width="30" align="center"><p><?php echo $i; ?>
									<input type="hidden" id="hidesysid_<?php echo $i;?>" name="hidesysid_<?php echo $i;?>" value="<?php echo $row["receive_id"];?>"  />&nbsp;
									</p></td>
									<td width="120"><p><input type="hidden" id="hidesysnum_<?php echo $i;?>" name="hidesysnum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
									<?php
									 echo $row["recv_number"]; 
									?>&nbsp;
									</p></td>
									<td width="90" align="center"><p>
									<input type="hidden" id="hideprogrum_<?php echo $i;?>" name="hideprogrum_<?php echo $i;?>" value="<?php echo $row["prog_no"];?>"  />
									<?php if($row["prog_no"]==0) echo "Independent"; else echo $row["prog_no"]; ?>&nbsp;
									</p></td>
									<td width="75" align="center"><p>
									<?php
									  if($row["knitting_source"]==1)  echo "In-House"; if($row["knitting_source"]==3) echo "Sub-Contract"; 
									?>&nbsp;
									</p></td>
									<td width="70" align="center"><p><?php  if($row["receive_date"]!='0000-00-00')  echo change_date_format($row["receive_date"]); else echo ""; ?>&nbsp;</p></td>
									<td width="50" align="center"><p>
									<input type="hidden" id="hideprodid_<?php echo $i;?>" name="hideprodid_<?php echo $i;?>" value="<?php echo $row["prod_id"];?>"  />
									<?php echo $row["prod_id"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p><?php echo $job_po_arr[$po_key]["job_year"]; ?>&nbsp;</p></td>
									<td width="60" align="center"><p>
									<input type="hidden" id="hidejob_<?php echo $i;?>" name="hidejob_<?php echo $i;?>" value="<?php echo $job_po_arr[$po_key]["job_no"];?>"  />
									<?php echo $job_po_arr[$po_key]["job_no"]; ?>&nbsp;
									</p></td>
									<td width="50"><p><?php echo $buyer_array[$row["buyer_id"]]; ?>&nbsp;</p></td>
									<td width="90"><p>
									<input type="hidden" id="hideorder_<?php echo $i;?>" name="hideorder_<?php echo $i;?>" value="<?php echo $row["po_breakdown_id"];?>"  />
									<?php echo $job_po_arr[$po_key]["po_number"]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									 <input type="hidden" id="hideconstruction_<?php echo $i;?>" name="hideconstruction_<?php echo $i;?>" value="<?php echo $construction_arr[$row["detarmination_id"]];?>"  />
									<?php echo $construction_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									<input type="hidden" id="hidecomposition_<?php echo $i;?>" name="hidecomposition_<?php echo $i;?>" value="<?php echo $composition_arr[$row["detarmination_id"]];?>"  />
									<?php echo $composition_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidegsm_<?php echo $i;?>" name="hidegsm_<?php echo $i;?>" value="<?php echo $row["gsm"]; ?>"  />
									<?php echo $row["gsm"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidedia_<?php echo $i;?>" name="hidedia_<?php echo $i;?>" value="<?php echo $row["dia_width"]; ?>"  />
									<?php echo $row["dia_width"]; ?>&nbsp;
									</p></td>
                                    <td width="40" align="center"><p>
									<?php echo $roll_arr[$row["receive_id"]][$row["prod_id"]][$row["po_breakdown_id"]]; ?>&nbsp;
									</p></td>
									<td width="70" align="right" ><p><?php echo number_format($row["current_stock"],2); $total_stock+=$row["current_stock"]; ?>&nbsp;</p></td>
									<td width="70" align="right" ><p>
									<?php
									echo number_format($tot_delivery,2);
									$gt_tot_delivery+=$tot_delivery;
									//echo $job_po_arr[$po_key]["po_id"];
									?>&nbsp;</p>
									</td>
									<td width="70"align="right" id="totalqtyTd_<?php echo $i; ?>"><p><?php $balance=($row["current_stock"]-$tot_delivery); echo number_format($balance,2); $total_balance+=$balance; ?>&nbsp;</p></td>
									<td width="75">
									<p><input type="hidden" id="hiddendtlsid_<?php echo $i;?>" name="hiddendtlsid_<?php echo $i;?>"  style="width:60px;" value="" />
										<input type="text" id="txtcurrentdelivery_<?php echo $i;?>" name="txtcurrentdelivery_<?php echo $i;?>" class="text_boxes_numeric" style="width:60px;" value="" onBlur="setHideval(<?php echo $i; ?>)"  />
                                <input type="hidden" id="hidden_current_val_<?php echo $i;?>" value="">
									&nbsp;</p></td>
									<td ><p>
										<input type="text" id="txtroll_<?php echo $i;?>" name="txtroll_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" value="" onBlur="total_roll(<?php echo $i; ?>)"  />
                                <input type="hidden" id="hideroll_<?php echo $i;?>" value=""  >
									&nbsp;</p></td>
								</tr>
								<?php
								$i++;
							}
						}
					}
					else
					{
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="background-color:#FF6;">
							<td width="30" align="center"><p><?php echo $i; ?>
							<input type="hidden" id="hidesysid_<?php echo $i;?>" name="hidesysid_<?php echo $i;?>" value="<?php echo $row["receive_id"];?>"  />&nbsp;
							</p></td>
							<td width="120"><p><input type="hidden" id="hidesysnum_<?php echo $i;?>" name="hidesysnum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
							<?php
							echo $row["recv_number"]; 
							?>&nbsp;
							</p></td>
							<td width="90" align="center"><p>
							<input type="hidden" id="hideprogrum_<?php echo $i;?>" name="hideprogrum_<?php echo $i;?>" value="<?php echo $row["prog_no"];?>"  />
							<?php if($row["prog_no"]==0) echo "Independent"; else echo $row["prog_no"]; ?>&nbsp;
							</p></td>
							<td width="75" align="center"><p>
							<?php
							if($row["knitting_source"]==1)  echo "In-House"; if($row["knitting_source"]==3) echo "Sub-Contract"; 
							?>&nbsp;
							</p></td>
							<td width="70" align="center"><p><?php  if($row["receive_date"]!='0000-00-00')  echo change_date_format($row["receive_date"]); else echo ""; ?>&nbsp;</p></td>
							<td width="50" align="center"><p>
							<input type="hidden" id="hideprodid_<?php echo $i;?>" name="hideprodid_<?php echo $i;?>" value="<?php echo $row["prod_id"];?>"  />
							<?php echo $row["prod_id"]; ?>&nbsp;
							</p></td>
							<td width="40" align="center"><p><?php echo $job_po_arr[$po_key]["job_year"]; ?>&nbsp;</p></td>
							<td width="60" align="center"><p>
							<input type="hidden" id="hidejob_<?php echo $i;?>" name="hidejob_<?php echo $i;?>" value="<?php echo $job_po_arr[$po_key]["job_no"];?>"  />
							<?php echo $job_po_arr[$po_key]["job_no"]; ?>&nbsp;
							</p></td>
							<td width="50"><p><?php echo $buyer_array[$row["buyer_id"]]; ?>&nbsp;</p></td>
							<td width="90"><p>
							<input type="hidden" id="hideorder_<?php echo $i;?>" name="hideorder_<?php echo $i;?>" value="<?php echo $row["po_breakdown_id"];?>"  />
							<?php echo $job_po_arr[$po_key]["po_number"]; ?>&nbsp;
							</p></td>
							<td width="110"><p>
							<input type="hidden" id="hideconstruction_<?php echo $i;?>" name="hideconstruction_<?php echo $i;?>" value="<?php echo $construction_arr[$row["detarmination_id"]];?>"  />
							<?php  echo $construction_arr[$row["detarmination_id"]]; ?>&nbsp;
							</p></td>
							<td width="110"><p>
							<input type="hidden" id="hidecomposition_<?php echo $i;?>" name="hidecomposition_<?php echo $i;?>" value="<?php echo $composition_arr[$row["detarmination_id"]];?>"  />
							<?php echo $composition_arr[$row["detarmination_id"]]; ?>&nbsp;
							</p></td>
							<td width="40" align="center"><p>
							<input type="hidden" id="hidegsm_<?php echo $i;?>" name="hidegsm_<?php echo $i;?>" value="<?php echo $row["gsm"]; ?>"  />
							<?php echo $row["gsm"]; ?>&nbsp;
							</p></td>
							<td width="40" align="center"><p>
							<input type="hidden" id="hidedia_<?php echo $i;?>" name="hidedia_<?php echo $i;?>" value="<?php echo $row["dia_width"]; ?>"  />
							<?php echo $row["dia_width"]; ?>&nbsp;
							</p></td>
                            <td width="40" align="center"><p>
							<?php echo $roll_arr[$row["receive_id"]][$row["prod_id"]][$row["po_breakdown_id"]]; ?>&nbsp;
							</p></td>
							<td width="70" align="right" ><p><?php echo number_format($row["current_stock"],2); $total_stock+=$row["current_stock"]; ?>&nbsp;</p></td>
							<td width="70" align="right" ><p>
							<?php
							$tot_delivery=$tot_delivery-$update_row_check[$index_pk]["current_delivery"]; 
							echo number_format($tot_delivery,2);
							$gt_tot_delivery+=$tot_delivery;
							//echo $job_po_arr[$po_key]["po_id"];  onKeyUp=" setHideval(<?php echo $i; )"  onChange="total_current_val(<?php echo $i;  )"
							?>&nbsp;</p>
							</td>
							<td width="70"align="right" id="totalqtyTd_<?php echo $i; ?>"><p><?php $balance=($row["current_stock"]-$tot_delivery); echo number_format($balance,2); $total_balance+=$balance; ?>&nbsp;</p></td>
							<td width="75">
							<p><input type="hidden" id="hiddendtlsid_<?php echo $i;?>" name="hiddendtlsid_<?php echo $i;?>"  style="width:60px;" value="<?php echo $update_row_check[$index_pk]["id"]; ?>" />
							<input type="text" id="txtcurrentdelivery_<?php echo $i;?>" name="txtcurrentdelivery_<?php echo $i;?>" class="text_boxes_numeric" style="width:60px;" value="<?php echo $update_row_check[$index_pk]["current_delivery"]; $to_delivey+=$update_row_check[$index_pk]["current_delivery"]; ?>" onBlur="setHideval(<?php echo $i;?>)" />
							<input type="hidden" id="hidden_current_val_<?php echo $i;?>" value="<?php echo $update_row_check[$index_pk]["current_delivery"]; ?>">
							&nbsp;</p></td>
							<td ><p>
							<input type="text" id="txtroll_<?php echo $i;?>" name="txtroll_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" value="<?php echo $update_row_check[$index_pk]["roll"]; $to_roll+=$update_row_check[$index_pk]["roll"]; ?>" onBlur="total_roll(<?php echo $i;?>)" />
							<input type="hidden" id="hideroll_<?php echo $i;?>" value="<?php echo $update_row_check[$index_pk]["roll"]; ?>"  >
							&nbsp;</p></td>
						</tr>
						<?php
						$i++;
					}
					
				}
			}
			
		}
		?>
        </tbody>
	</table>
    </div>
    <table width="1380" class="rpt_table" id="tbl_footer" cellpadding="0" cellspacing="1" rules="all">
    	<tfoot>
        	<th colspan="15" align="right">Total:</th>
            <th width="70"><?php echo number_format($total_stock,2); ?></th>
            <th width="70"><?php echo number_format($gt_tot_delivery,2); ?></th>
            <th width="70"><?php echo number_format($total_balance,2); ?></th>
            <th width="75" id="total_current_val" align="right"> <?php echo number_format($to_delivey,2); ?></th>
            <th width="62" id="total_roll" align="right"><?php echo number_format($to_roll,0); ?></th>
        </tfoot>
    </table>
    
    </div>
    <table width="1380" class="rpt_table" id="tbl_foot" cellpadding="0" cellspacing="1" rules="all">
    	<tr>
        	<td colspan="20" height="30" valign="middle" align="center" class="button_container">
				<?php 
                	echo load_submit_buttons( $permission, "fnc_prod_delivery",0,1 ,"",1) ; 
                ?>
            </td>
        </tr>
    </table>
    </form>
</div>
<!--<script src="../includes/functions_bottom.js" type="text/javascript"></script>-->
	<?php
foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename####$update_mst_id";
	
	exit();	
}







if ($action=="save_update_delete")
{
		
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	//extract( $process );
	//echo $cbo_buyer_id;die;
	if($operation==0) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 	 
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
				
		//yarn issue master table entry here START---------------------------------------//	
 		if( str_replace("'","",$update_mst_id) == "" ) //new insert cbo_ready_to_approved
		{
			$id=return_next_id("id", " pro_grey_prod_delivery_mst", 1);		
			$new_mrr_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'GDS', date("Y",time()), 5, "select a.sys_number_prefix,a.sys_number_prefix_num from pro_grey_prod_delivery_mst a where a.company_id=$cbo_company_id and $mrr_date_check =".date('Y',time())." order by a.id DESC", "sys_number_prefix", "sys_number_prefix_num" ));
		
		
			$field_array="id,sys_number_prefix,sys_number_prefix_num,sys_number,delevery_date,company_id,location_id,buyer_id,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_mrr_number[1]."','".$new_mrr_number[2]."','".$new_mrr_number[0]."',".$txt_delevery_date.",".$cbo_company_id.",".$cbo_location_id.",".$cbo_buyer_id.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
			//$rID=sql_insert(" pro_grey_prod_delivery_mst",$field_array,$data_array,1); 
		}

			
		$field_array_dtls="id,mst_id,grey_sys_id,grey_sys_number,program_no,product_id,job_no,order_id,construction,composition,gsm,dia,current_delivery,roll,inserted_by,insert_date";
		$dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);
		$ref_dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);	
		$k=1;
		for($i=1;$i<=$total_row;$i++)
		{
			$sys_id="hidesysid_".$i;
			$hidesysnum="hidesysnum_".$i;
			$hideprogram="hideprogrum_".$i;
			$hideprodid="hideprodid_".$i;
			$hidejob="hidejob_".$i;
			$hideorder="hideorder_".$i;
			$hideconstruc="hideconstruction_".$i;
			$hidecomposit="hidecomposition_".$i;
			$hidegsm="hidegsm_".$i;
			$hidedia="hidedia_".$i;
			$txtcurrentdelivery="txtcurrentdelivery_".$i;
			$txt_roll="txtroll_".$i;
			//echo $$txtcurrentdelivery;die;
			if(str_replace("'","",$$txtcurrentdelivery)>0)
			{
				if($k!=1)$dtls_id=$dtls_id+1;
				if ($k!=1) $data_array_dtls .=",";
				$data_array_dtls.="(".$dtls_id.",".$id.",".$$sys_id.",".$$hidesysnum.",".$$hideprogram.",".$$hideprodid.",".$$hidejob.",".$$hideorder.",".$$hideconstruc.",".$$hidecomposit.",".$$hidegsm.",".$$hidedia.",".$$txtcurrentdelivery.",".$$txt_roll.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
				$k++;
			}
		}
		//oci_rollback($con); 
		//echo "10**".$rID."##".$rID2;die;
		//echo $field_array_dtls."*".$data_array_dtls;die;
		$rID=$rID2=true;
		if( str_replace("'","",$update_mst_id) == "" ) //new insert cbo_ready_to_approved
		{
			$rID=sql_insert("pro_grey_prod_delivery_mst",$field_array,$data_array,1); 
		}
		$rID2=sql_insert("pro_grey_prod_delivery_dtls",$field_array_dtls,$data_array_dtls,1);
		
		
		if($db_type==0)
		{
			if($rID && $rID2 )
			{
				mysql_query("COMMIT");
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$new_mrr_number[0])."**".str_replace("'",'',$ref_dtls_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID2 )
			{
				oci_commit($con); 
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$new_mrr_number[0])."**".str_replace("'",'',$ref_dtls_id);
			}
			else
			{
				oci_rollback($con); 
				echo "10**".str_replace("'",'',$id);
			}
		}
		disconnect($con);
		//die;
		
	}
	else if($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		if( str_replace("'","",$update_mst_id) != "") 
		{
			$field_array_mst="delevery_date*company_id*location_id*buyer_id*updated_by*update_date*status_active*is_deleted";
			$data_array_mst="".$txt_delevery_date."*".$cbo_company_id."*".$cbo_location_id."*".$cbo_buyer_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1*0";
			//$rID=sql_update("pro_grey_prod_delivery_mst",$field_array_mst,$data_array_mst,"id",$update_mst_id,0);
		}
		if( str_replace("'","",$update_mst_id) != "") 
		{
			$rID3=1;
			$id_arr=array();
			$data_array_dtls=array();
			$data_array_dtls_in="";
			$field_array_dtls="current_delivery*roll";
			$dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);
			$ref_dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);	
			$mst_id=str_replace("'",'',$update_mst_id);
			$field_array_dtls_in="id,mst_id,grey_sys_id,grey_sys_number,program_no,product_id,job_no,order_id,construction,composition,gsm,dia,current_delivery,roll,inserted_by,insert_date";
			$coma=0;
			for($i=1; $i<=$total_row; $i++)
			{
				$sys_id="hidesysid_".$i;
				$hidesysnum="hidesysnum_".$i;
				$hideprogram="hideprogrum_".$i;
				$hideprodid="hideprodid_".$i;
				$hidejob="hidejob_".$i;
				$hideorder="hideorder_".$i;
				$hideconstruc="hideconstruction_".$i;
				$hidecomposit="hidecomposition_".$i;
				$hidegsm="hidegsm_".$i;
				$hidedia="hidedia_".$i;
				$txtcurrentdelivery="txtcurrentdelivery_".$i;
				$update_id_dtls="hiddendtlsid_".$i;
				$txt_roll="txtroll_".$i;
				
				if(str_replace("'",'',$$update_id_dtls)!="")
				{
					$id_arr[]=str_replace("'",'',$$update_id_dtls);
					$data_array_dtls[str_replace("'",'',$$update_id_dtls)] =explode(",",("".$$txtcurrentdelivery.",".$$txt_roll.""));
				}
				else
				{
					if(str_replace("'","",$$txtcurrentdelivery)>0)
					{
						if ($coma!=0) $data_array_dtls_in .=",";
						if ($coma!=0) $dtls_id=$dtls_id+1;
						$data_array_dtls_in	.="(".$dtls_id.",".$mst_id.",".$$sys_id.",".$$hidesysnum.",".$$hideprogram.",".$$hideprodid.",".$$hidejob.",".$$hideorder.",".$$hideconstruc.",".$$hidecomposit.",".$$hidegsm.",".$$hidedia.",".$$txtcurrentdelivery.",".$$txt_roll.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
						$coma++;
					}
					
				}
			}
			$rID=$rID2=$rID3=true;
			if( str_replace("'","",$update_mst_id) != "") 
			{
				$rID=sql_update("pro_grey_prod_delivery_mst",$field_array_mst,$data_array_mst,"id",$update_mst_id,1);
			}
			$rID2=execute_query(bulk_update_sql_statement("pro_grey_prod_delivery_dtls","id", $field_array_dtls,$data_array_dtls,$id_arr),1);
			if($data_array_dtls_in!="")
			{
				$rID3=sql_insert("pro_grey_prod_delivery_dtls",$field_array_dtls_in,$data_array_dtls_in,1);
			}
		}
		
		
		
		if($db_type==0)
		{
			if($rID && $rID2 && $rID3)
			{
				mysql_query("COMMIT");
				echo "1**".str_replace("'",'',$mst_id)."**".str_replace("'",'',$ref_dtls_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$mst_id);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID2 && $rID3)
			{
				oci_commit($con); 
				echo "1**".str_replace("'",'',$mst_id)."**".str_replace("'",'',$ref_dtls_id);
			}
			else
			{
				oci_rollback($con); 
				echo "10**".str_replace("'",'',$mst_id);
			}
		}
		disconnect($con);
		die;
	
		}
	

	exit();	
}


if($action=="delevery_search")
{

	echo load_html_head_contents("Export Information Entry Form", "../../", 1, 1,'','1','');
	extract($_REQUEST);
	?>
     
	<script>
	
		function js_set_value(data)
		{
			$('#hidden_tbl_id').val(data);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:955px;">
	<form name="searchexportinformationfrm"  id="searchexportinformationfrm">
		<fieldset style="width:950px;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="950" class="rpt_table" border="1" rules="all" align="center">
                <thead>
                    <th width="100" class="must_entry_caption">Company</th>
                    <th  width="100">Location</th>
                    <th width="100">Buyer</th>
                    <th width="90">Prog. No</th>
                    <th width="90">Order No</th>
                    <th width="90">Deli. Date from</th>
                    <th width="90">Deli. Date To</th>
                    <th width="100">System Id</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:70px" class="formbutton" />
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <?php
                        	echo create_drop_down( "cbo_company_id", 100, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "--Select Company--", str_replace("'","",$company) , "load_drop_down( 'grey_feb_delivery_entry_controller', this.value, 'load_drop_down_location', 'location_td' );load_drop_down( 'grey_feb_delivery_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                        ?>                        
                    </td>
                    <td id="location_td">
                            <?php
								$blank_array="select id,location_name from lib_location where company_id='".str_replace("'","",$company)."' and status_active =1 and is_deleted=0 order by location_name"; 
                                echo create_drop_down( "cbo_location_id",100,$blank_array,"id,location_name", 1, "--Select Location--", $selected, "","","","","","",2);
                            ?>
                        </td>
                        <td id="buyer_td">
                            <?php 
								$blank_array="select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='".str_replace("'","",$company)."' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name";
                                echo create_drop_down( "cbo_buyer_id",100,$blank_array,"id,buyer_name", 1, "--Select Buyer--", $selected, "","","","","","",2);
                            ?>
                        </td>
                        <td>
                        	<input type="text" name="txt_prog_no" id="txt_prog_no" class="text_boxes" style="width:80px;"/>
                        </td>
                        <td>
                        	<input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:80px;"/>
                        </td>
                        <td>
                        	<input type="date" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px;" readonly/> 
                        </td>
                        <td>
                        	<input type="date" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px;" readonly/>
                        </td>
                        <td>
                        	<input type="text" name="txt_sys_id" id="txt_sys_id" class="text_boxes" style="width:95px;"/>
                        </td>                   
                     <td>
                        <input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_location_id').value+'**'+document.getElementById('cbo_buyer_id').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value+'**'+document.getElementById('txt_sys_id').value+'**'+document.getElementById('txt_prog_no').value+'**'+document.getElementById('txt_order_no').value, 'delivery_search_list_view', 'search_div', 'grey_feb_delivery_entry_controller', 'setFilterGrid(\'tbl_body\',-1)')" style="width:70px;" />
                     </td>
                </tr>
                <tr>
                	<td colspan="9" align="center" valign="bottom"><?php echo load_month_buttons(1);  ?></td>
                </tr>
           </table>
           <input type="hidden" id="hidden_tbl_id">
            <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
$("#cbo_location_id").val(0);
$("#cbo_buyer_id").val(0);
</script>
</html>
<?php
	exit(); 
 
}

if ($action=="delivery_search_list_view")
{
	$data=explode("**",$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_location_name=str_replace("'","",$data[1]);
	$cbo_buyer_name=str_replace("'","",$data[2]);
	$txt_date_from=str_replace("'","",$data[3]);
	$txt_date_to=str_replace("'","",$data[4]);
	$sys_id=str_replace("'","",$data[5]);
	$prog_no=str_replace("'","",$data[6]);
	$order_no=str_replace("'","",$data[7]);
	//echo $cbo_buyer_name."**".$txt_date_to;die;
	if($order_no!="")
	{
		if($db_type==0)
		{
			$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '%$order_no' and status_active=1","po_id");
		}
		else if($db_type==2)
		{
			$po_id=return_field_value("LISTAGG(CAST( id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as po_id","wo_po_break_down","po_number like '%$order_no' and status_active=1","po_id");
		}
	
	}
	if($prog_no!="") $program_cond="and b.program_no='$prog_no'"; else $program_cond="";
	if($po_id!="") $order_cond="and b.order_id in($po_id)"; else $order_cond="";
	if($cbo_company_name!=0) {$cbo_company_name="and a.company_id='$cbo_company_name'";} else {echo "Please select the company";die;}
	if($cbo_location_name!=0) $cbo_location_name="and a.location_id='$cbo_location_name'"; else $cbo_location_name="";
	if($cbo_buyer_name !=0) $cbo_buyer_cond="and a.buyer_id='$cbo_buyer_name'"; else $cbo_buyer_cond="";
	if($db_type==0)
	{
		if($txt_date_from!="" && $txt_date_to !="") $date_cond="and a.delevery_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."'"; else $date_cond="";
	}
	//if($txt_date_from!="" || $txt_date_to!="") $sql_cond .= " and wo_date  between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'";
	else if($db_type==2)
	{
		if($txt_date_from!="" && $txt_date_to !="") $date_condition="and a.delevery_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'"; else $date_condition="";
	}
	//echo $date_condition;die;

	//LISTAGG(CAST( b.grey_sys_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.grey_sys_id) as grey_sys_id
	if($sys_id!="") $sys_cond="and a.sys_number_prefix_num like '$sys_id'"; else $sys_cond="";
	if($db_type==2)
	{
		$sql="select a.id,a.sys_number_prefix_num,$mrr_date_check as sys_year,a.sys_number,a.delevery_date,a.company_id,a.location_id,LISTAGG(CAST( b.program_no  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.program_no) as prog_no,LISTAGG(CAST( b.order_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.order_id) as order_id,sum(b.current_delivery) as current_delivery 
			from  pro_grey_prod_delivery_mst a, pro_grey_prod_delivery_dtls b 
			where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0  $cbo_company_name $cbo_location_name $date_condition $sys_cond $program_cond $order_cond $cbo_buyer_cond
			group by  a.id,a.sys_number,a.delevery_date,a.company_id,a.location_id,a.sys_number_prefix_num,a.insert_date";
	}
	else if($db_type==0)
	{
		$sql="select a.id,a.sys_number_prefix_num,$mrr_date_check as sys_year,a.sys_number,a.delevery_date,a.company_id,a.location_id,group_concat(distinct b.program_no) as prog_no,group_concat(distinct b.order_id) as order_id,sum(b.current_delivery) as current_delivery 
			from  pro_grey_prod_delivery_mst a, pro_grey_prod_delivery_dtls b 
			where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0  $cbo_company_name $cbo_location_name $date_condition $sys_cond $program_cond $order_cond $cbo_buyer_cond
			group by  a.id,a.sys_number,a.delevery_date,a.company_id,a.location_id,a.sys_number_prefix_num,a.insert_date";
	}
	
	//echo $sql;die;
	$sql_result=sql_select($sql);
	/*$arr=array(2=>$company_arr,3=>$location_arr,5=>$order_no_arr);
	echo  create_list_view("list_view", "Year,Delivery Sys.Num,Company Name,Location Name,Program No, Order No,Delivery Date,Delivery Qty","100,80,150,140,100,100,110","950","260",0, $sql , "js_set_value", "id,company_id,location_id,sys_number", "", 1, "0,0,company_id,location_id,0,order_id,0,0", $arr, "sys_year,sys_number_prefix_num,company_id,location_id,prog_no,order_id,delevery_date,current_delivery", "",'','0,0,0,0,0,0,3,2') ;	*/
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="950" class="rpt_table">
    	<thead>
        	<tr>
            	<th width="30">SL</th>
                <th width="70">Year</th>
                <th width="70">Delivery Sys.Num</th>
                <th width="110">Company Name</th>
                <th width="110">Location Name</th>
                <th width="150">Program No</th>
                <th width="250">Order No</th>
                <th width="70">Delivery Date</th>
                <th>Delivery Qty</th>
            </tr>
        </thead>
    </table>
    <div id="scroll_body">
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="950" class="rpt_table" id="tbl_body">
        <tbody>
        <?php
		$i=1;
		foreach($sql_result as $row)
		{
			if ($i%2==0)  
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			$progr_no=implode(",",array_unique(explode(",",$row[csf("prog_no")])));
			$po_no_arr=array_unique(explode(",",$row[csf("order_id")]));
			?>
        	<tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value('<?php echo $row[csf("id")] ?>_<?php echo $row[csf("company_id")] ?>_<?php echo $row[csf("location_id")] ?>_<?php echo $row[csf("sys_number")] ?>')">
            	<td width="30"><?php echo $i; ?></td>
                <td align="center" width="70"><p><?php echo $row[csf("sys_year")]; ?></p></td>
                <td width="70"><p><?php echo $row[csf("sys_number_prefix_num")]; ?></p></td>
                <td width="110"><p><?php echo $company_arr[$row[csf("company_id")]]; ?></p></td>
                <td width="110"><p><?php echo $location_arr[$row[csf("location_id")]]; ?></p></td>
                <td width="150"><p><?php echo $progr_no; ?></p></td>
                <td width="250">
				<p><?php
				$po_group=""; 
				foreach($po_no_arr as $po)
				{
					if($po_group=="") $po_group=$order_no_arr[$po]; else $po_group=$po_group.",".$order_no_arr[$po];
				}
				echo $po_group; 
				?></p></td>
                <td width="70"><p><?php if($row[csf("delevery_date")]!='0000-00-00' || $row[csf("delevery_date")]!="") echo change_date_format($row[csf("delevery_date")]);//echo $i; ?></p></td>
                <td align="right"><p><?php echo number_format($row[csf("current_delivery")],2); ?></p></td>
            </tr>
            <?php
			$i++;
		}
		?>
        </tbody>
    </table>
    </div>
    <?php
}


if($action=="populate_master_from_data")
{
	if($db_type==2)
	{						
		$sql=sql_select("select a.id,a.delevery_date,a.company_id,a.location_id,a.buyer_id,LISTAGG(CAST( b.grey_sys_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.grey_sys_id) as grey_sys_id,LISTAGG(CAST( b.program_no  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.program_no) as program_no,LISTAGG(CAST( b.order_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.order_id) as order_id,LISTAGG(CAST( b.product_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.product_id) as product_id from  pro_grey_prod_delivery_mst a,  pro_grey_prod_delivery_dtls b where a.id=b.mst_id and a.id=$data group by a.id,a.delevery_date,a.company_id,a.location_id,a.buyer_id");
	}
	else if($db_type==0)
	{
		$sql=sql_select("select a.id,a.delevery_date,a.company_id,a.location_id,a.buyer_id,group_concat(distinct b.grey_sys_id  ) as grey_sys_id,group_concat(distinct b.program_no ) as program_no,group_concat(distinct b.order_id ) as order_id,group_concat(distinct b.product_id ) as product_id from  pro_grey_prod_delivery_mst a,  pro_grey_prod_delivery_dtls b where a.id=b.mst_id and a.id=$data group by a.id");
	}
	foreach($sql as $row)
	{
		$receive_id=implode(",",array_unique(explode(",",$row[csf("grey_sys_id")])));
		$progrum_id=implode(",",array_unique(explode(",",$row[csf("program_no")])));
		$order_id=implode(",",array_unique(explode(",",$row[csf("order_id")])));
		$product_detail_id=implode(",",array_unique(explode(",",$row[csf("product_id")])));
		
		echo "document.getElementById('txt_delevery_date').value 			= '".change_date_format($row[csf("delevery_date")])."';\n";
		echo "document.getElementById('cbo_company_id').value 				= ".$row[csf("company_id")].";\n";
		echo "document.getElementById('cbo_location_id').value 				= ".$row[csf("location_id")].";\n";
		echo "document.getElementById('cbo_buyer_id').value 				= ".$row[csf("buyer_id")].";\n";
		echo "document.getElementById('hidden_receive_id').value 			= '".$receive_id."';\n";
		echo "document.getElementById('hidden_product_id').value 			= '".$product_detail_id."';\n";
		echo "document.getElementById('hidden_order_id').value 				= '".$order_id."';\n";
		echo "document.getElementById('update_mst_id').value 				= '".$row[csf("id")]."';\n";

	}
}


if($action=="delivery_challan_print")
{
	extract($_REQUEST);	
	
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$datas=explode('_',$data);
	$program_ids = str_replace("'","",$datas[0]);
	$company = str_replace("'","",$datas[1]);
	$from_date = str_replace("'","",$datas[2]);
	$to_date = str_replace("'","",$datas[3]);
	$product_ids = str_replace("'","",$datas[4]);
	$order_ids = str_replace("'","",$datas[5]);
	$location= str_replace("'","",$datas[6]);
	$buyer = str_replace("'","",$datas[7]);
	$update_mst_id = str_replace("'","",$datas[8]);
	$delivery_date = str_replace("'","",$datas[9]);
	$Challan_no = str_replace("'","",$datas[10]);
	//echo $Challan_no;die;
	$company_details=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$country_arr=return_library_array( "select id,country_name from lib_country", "id", "country_name");
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	
	$machine_details=array();
	$machine_data=sql_select("select id, machine_no, dia_width from lib_machine_name");
	foreach($machine_data as $row)
	{
		$machine_details[$row[csf('id')]]['no']=$row[csf('machine_no')];
		$machine_details[$row[csf('id')]]['dia']=$row[csf('dia_width')];
	}
	
	$po_array=array();
	//echo "select a.job_no, a.job_no_prefix_num, a.style_ref_no, b.id, b.po_number as po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company group by b.po_number";
	$po_data=sql_select("select a.job_no, a.job_no_prefix_num, a.style_ref_no, b.id, b.po_number as po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company group by  b.id, b.po_number,a.job_no, a.job_no_prefix_num, a.style_ref_no");
	foreach($po_data as $row)
	{
		$job_year=explode("-",$row[csf('job_no')]);
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')];
		$po_array[$row[csf('id')]]['job_no_prifix']=$job_year[1]."-".$job_year[2];
		$po_array[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
	}
	
	$composition_arr=array();
	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}
	
	/*$knit_plan_arr=array();
	$plan_data=sql_select("select id, color_range, stitch_length from ppl_planning_info_entry_dtls");
	foreach($plan_data as $row)
	{
		$knit_plan_arr[$row[csf('id')]]['cr']=$row[csf('color_range')];
		$knit_plan_arr[$row[csf('id')]]['sl']=$row[csf('stitch_length')]; 
	}*/
	
	$update_row_check=array();
	if($update_mst_id!="")
	{
		$sql_update=sql_select("select id,grey_sys_id,product_id,job_no,order_id,current_delivery,roll from pro_grey_prod_delivery_dtls where mst_id=$update_mst_id");
		foreach($sql_update as $row)
		{
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["current_delivery"] =$row[csf("current_delivery")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["id"] =$row[csf("id")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["roll"] =$row[csf("roll")];
		}
	}	
	
	//var_dump($update_row_check);die;
	
	?>
	<div style="width:1500px;">
		<table width="1490" cellspacing="0" align="center" border="0">
			<tr>
				<td colspan="17" align="center" style="font-size:x-large"><strong><?php echo $company_details[$company]; ?></strong></td>
			</tr>
			<tr>
				<td colspan="17" align="center" style="font-size:18px"><strong><u>Delivery Challan</u></strong></td>
			</tr>
			<tr>
				<td colspan="17" align="center" style="font-size:16px"><strong><u>Knitting Section</u></strong></td>
			</tr>
        </table> 
        <br>
		<table width="1490" cellspacing="0" align="center" border="0">
			<tr>
				<td   style="font-size:16px; font-weight:bold;" width="110">Challan No</td>
                <td colspan="16"  >:&nbsp;<?php echo $Challan_no; ?></td>
			</tr>
            <tr>
				<td style="font-size:16px; font-weight:bold;" width="110">Delivery Date </td>
                <td colspan="16" >:&nbsp;<?php echo $delivery_date; ?></td>
			</tr>
        </table> 
    </div>
    <div style="width:1500px">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1500" class="rpt_table" >
            <thead>
                <tr>
                    <th width="30" >SL</th>
                    <th width="130" >Order No</th>
                    <th width="60" >Buyer <br> Job</th>
                    <th width="50" >System ID</th>
                    <th width="50" >Prog. /Booking ID</th>
                    <th width="70" >Knitting Source</th>
                    <th width="90" >Knitting Company</th>
                    <th width="50" >Yarn Issue Challan No</th>
                    <th width="80" >Yarn Count</th>
                    <th width="90" >Yarn Brand</th>
                    <th width="60" >Lot No</th>
                    <th width="70" >Color</th>
                    <th width="80" >Color Range</th>
                    <th width="240">Fabric Type</th>
                    <th width="50" >Stich</th>
                    <th width="60" >Fin GSM</th>
                    <th width="40" >Fab. Dia</th>
                    <th width="40" >M/C Dia</th>
                    <th width="70" >Total Roll</th>
                    <th width="80">Total Qty</th>
                </tr>
            </thead>
          <tbody>
	<?php
		if($db_type==0)
		{
			if($from_date!="" && $to_date!="") $date_con="and a.receive_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'"; else $date_con="";
		}
		else if($db_type==2)
		{
			//if($txt_date_from!="" || $txt_date_to!="") $sql_cond .= " and wo_date  between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'";
			if($from_date!="" && $to_date!="") $date_con="and a.receive_date between '".date("j-M-Y",strtotime($from_date))."' and '".date("j-M-Y",strtotime($to_date))."'"; else $date_con="";
		}
		
		if($location!=0) $location_con="and a.location_id=$location"; else $location_con="";
		if($buyer!=0) $buyer_con="and a.buyer_id=$buyer"; else $buyer_con="";
		if($db_type==0)
		{
			$sql="select a.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_id, a.booking_no, a.buyer_id, a.remarks,a.yarn_issue_challan_no, b.prod_id, b.febric_description_id, group_concat(b.gsm) as gsm, group_concat(b.width) as width,   group_concat(b.yarn_lot) as yarn_lot, group_concat(b.yarn_count) as yarn_count, max(b.brand_id) as brand_id, min(b.machine_no_id) as machine_no_id,b.color_id,b.color_range_id,b.stitch_length , c.po_breakdown_id, sum(c.quantity)  as outqntyshift 
			from 
				inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c 
			where 
				a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and a.status_active=1 and a.is_deleted=0 and c.trans_type=1 and a.company_id=$company  and a.id in ($program_ids) and b.prod_id in ($product_ids) and c.po_breakdown_id in($order_ids) $date_con $location_con $buyer_con  
				group by a.id,b.prod_id,c.po_breakdown_id order by a.id";
		}
		//LISTAGG(CAST( b.id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.id) as po_id
		else
		{
			$sql="select a.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_id, a.booking_no, a.buyer_id, a.remarks,a.yarn_issue_challan_no, b.prod_id, max(b.febric_description_id) as febric_description_id, max(b.gsm) as gsm, max(b.width) as width, LISTAGG(CAST(b.yarn_lot AS VARCHAR2(4000)), ',') WITHIN GROUP (ORDER BY b.yarn_lot )as yarn_lot, LISTAGG(CAST(b.yarn_count AS VARCHAR2(4000)), ',') WITHIN GROUP (ORDER BY b.yarn_count )as yarn_count, max(b.brand_id) as brand_id, max(b.machine_no_id) as machine_no_id,max(b.color_id) as color_id,max(b.color_range_id) as color_range_id,max(b.stitch_length) as stitch_length, c.po_breakdown_id, sum(c.quantity)  as outqntyshift
			from 
				inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c 
			where 
				a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and a.status_active=1 and a.is_deleted=0 and c.trans_type=1 and a.company_id=$company  and a.id in ($program_ids) and b.prod_id in ($product_ids) and c.po_breakdown_id in($order_ids) $date_con $location_con $buyer_con  
			group by 
					 a.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_id, a.booking_no, 
	 a.buyer_id, a.remarks, a.yarn_issue_challan_no, b.prod_id,c.po_breakdown_id 
			order by a.id";	
		}
	//echo $sql;
	

	$nameArray=sql_select( $sql); $i=1; $tot_roll=0; $tot_qty=0;
	foreach($nameArray as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
		$count='';
		$yarn_count=explode(",",$row[csf('yarn_count')]);
		foreach($yarn_count as $count_id)
		{
			if($count=='') $count=$yarn_count_details[$count_id]; else $count.=",".$yarn_count_details[$count_id];
		}
		
		$reqsn_no=""; /*$stich_length=""; $color="";
		if($row[csf('receive_basis')]==2)
		{
			$stich_length=$knit_plan_arr[$row[csf('booking_id')]]['sl']; 
			$color=$color_range[$knit_plan_arr[$row[csf('booking_id')]]['cr']];
		}*/
		if($update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["current_delivery"]>0)
		{
			?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td width="30"><div style="word-wrap:break-word; width:30px"><?php echo $i; ?></div></td>
                <td width="130"><div style="word-wrap:break-word; width:130px"><?php echo $po_array[$row[csf('po_breakdown_id')]]['no'];?></div></td>
                <td width="60"><div style="word-wrap:break-word; width:60px"><?php echo $buyer_array[$row[csf('buyer_id')]]; echo "<br>"."Job-".$po_array[$row[csf('po_breakdown_id')]]['job_no_prifix']?></div></td>
                <td width="50"><div style="word-wrap:break-word; width:50px"><?php echo $row[csf('recv_number_prefix_num')]; ?></div></td>
                <td width="50"><div style="word-wrap:break-word; width:50px"><?php echo $row[csf('booking_id')]; ?></div></td>
                <td width="70"><div style="word-wrap:break-word; width:70px"><?php if($row[csf("knitting_source")]==1)  echo "In-House"; else if($row[csf("knitting_source")]==3) echo "Sub-Contract";  ?></div></td>
                <td width="90"><div style="word-wrap:break-word; width:90px">
				<?php 
					if($row[csf("knitting_source")]==1)  echo $company_arr[$row[csf("knitting_company")]]; //$company_arr
					else if($row[csf("knitting_source")]==3) echo $supplier_arr[$row[csf("knitting_company")]];  
				?>
                </div></td>
                <td width="50" align="center"><div style="word-wrap:break-word; width:50px"><?php echo $row[csf("yarn_issue_challan_no")]; ?></div></td>
                <td width="80"><div style="word-wrap:break-word; width:80px"><?php echo $count; ?></div></td>
                <td width="90"><div style="word-wrap:break-word; width:90px"><?php echo $brand_details[$row[csf('brand_id')]];?></div></td>
                <td width="60"><div style="word-wrap:break-word; width:60px"><?php echo $row[csf('yarn_lot')]; ?></div></td>
                <td width="70"><div style="word-wrap:break-word; width:70px"><?php echo $color_arr[$row[csf('color_id')]]; ?></div></td>
                <td width="80"><div style="word-wrap:break-word; width:80px"><?php echo $color_range[$row[csf('color_range_id')]]; ?></div></td>
                <td width="240"><div style="word-wrap:break-word; width:240px"><?php echo $composition_arr[$row[csf('febric_description_id')]]; ?></div></td>
                <td width="50"><div style="word-wrap:break-word; width:50px"><?php echo $row[csf("stitch_length")]; ?></div></td>
                <td width="60"><div style="word-wrap:break-word; width:60px"><?php  echo $row[csf('gsm')]; ?></div></td>
                <td width="40"><div style="word-wrap:break-word; width:40px"><?php echo $row[csf('width')];?></div></td>
                <td width="40"><div style="word-wrap:break-word; width:40px"><?php echo $machine_details[$row[csf('machine_no_id')]]['dia']; ?></div></td>
                <td width="70" align="right"><div style="word-wrap:break-word; width:70px"><?php echo number_format($update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["roll"],2); $tot_roll+=$update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["roll"]; ?></div></td>
                <td  align="right" width="80"><div style="word-wrap:break-word; width:80px"><?php echo number_format($update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["current_delivery"],2); $tot_qty+=$update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["current_delivery"]; ?></div></td>
			</tr>
			<?php
			$i++;
		}
	}
	?>
        	<tr> 
                <td align="right" colspan="18" ><strong>Total:</strong></td>
                <td align="right"><?php echo number_format($tot_roll,2,'.',''); ?>&nbsp;</td>
                <td align="right" ><?php echo number_format($tot_qty,2,'.',''); ?>&nbsp;</td>
			</tr>
            <tr>
                <td colspan="2" align="left"><b>Remarks: <?php //echo number_to_words($format_total_amount,$uom_unit,$uom_gm); ?></b></td>
                <td colspan="18" >&nbsp;</td>
            </tr>
		</table>
        <br>
		 <?php
            echo signature_table(44, $company, "1490px");
         ?>

	</div>
	<?php
    exit();
}

?>
