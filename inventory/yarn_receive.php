<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Yarn Receive Entry
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	07-05-2013
Updated by 		: 	Kausar	
Update date		: 	28-10-2013	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Receive Info","../", 1, 1, $unicode,1,1); 

// for autocomplete brand
$brand_sql = sql_select("select brand_name from lib_brand order by brand_name");
$brand_name = "";
foreach($brand_sql as $row)
{  
	$brand_name.= "{value:'".$row[csf('brand_name')]."'},";
}

// for autocomplete color
$color_sql = sql_select("select id,color_name from lib_color order by id");
$color_name = "";
foreach($color_sql as $row)
{ 
	$color_name.= "{value:'".$row[csf('color_name')]."',id:".$row[csf('id')]."},";
}


// last yarn receive exchange rate, currency, store name---------------
$sql = sql_select("select store_id,exchange_rate,currency_id,max(id) from inv_receive_master where item_category=1");
$storeName=$exchangeRate=$currencyID=0;
foreach($sql as $row)
{
	$storeName=$row[csf("store_id")];
	$exchangeRate=$row[csf("exchange_rate")];
	$currencyID=$row[csf("currency_id")];
}
?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

// autocomplete brand script-------
	$(function() {
				var brand_name = [<?php echo substr($brand_name, 0, -1); ?>];
				$("#txt_brand").autocomplete({
					source: brand_name 
			});
	});


function rcv_basis_reset()
{
	document.getElementById('cbo_receive_basis').value=0;
} 
	
	
// popup for WO/PI----------------------	
function openmypage(page_link,title)
{
	if( form_validation('cbo_company_name*cbo_receive_basis','Company Name*Receive Basis')==false )
	{
		return;
	}
	
	var company = $("#cbo_company_name").val();
	var receive_basis = $("#cbo_receive_basis").val();
	var receive_purpose = $("#cbo_receive_purpose").val();
	 
	page_link='requires/yarn_receive_controller.php?action=wopi_popup&company='+company+'&receive_basis='+receive_basis+'&receive_purpose='+receive_purpose;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=400px, center=1, resize=0, scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var rowID=this.contentDoc.getElementById("hidden_tbl_id").value; // wo/pi table id
		var wopiNumber=this.contentDoc.getElementById("hidden_wopi_number").value; // wo/pi number
		if (rowID!="")
		{
			freeze_window(5);
			$("#txt_wo_pi").val(wopiNumber);
			$("#txt_wo_pi_id").val(rowID);
			get_php_form_data(receive_basis+"**"+rowID+"**"+receive_purpose, "populate_data_from_wopi_popup", "requires/yarn_receive_controller" );
			show_list_view(receive_basis+"**"+rowID+"**"+receive_purpose,'show_product_listview','list_product_container','requires/yarn_receive_controller','');
			
			if(receive_basis==2 && receive_purpose==2)
			{
				load_drop_down( 'requires/yarn_receive_controller', rowID, 'load_drop_down_color', 'color_td_id' );
			}
			else
			{
				load_drop_down( 'requires/yarn_receive_controller', '', 'load_drop_down_color', 'color_td_id' );
			}
			
			exchange_rate($("#cbo_currency").val());
			
			release_freezing();
			$("#tbl_child").find('input[type="text"],input[type="hidden"],select').val('');	 
		}
	}		
}

// enable disable field for independent
function fn_independent(val)
{
	reset_form('','list_product_container','txt_wo_pi*txt_wo_pi_id*cbo_supplier*cbo_source*txt_lc_no*hidden_lc_id','cbo_currency,1*txt_exchange_rate,1','','');
	if(val==4)
	{    
		$("#cbo_supplier").attr("disabled",false);
		$("#cbo_currency").attr("disabled",false);
		$("#cbo_source").attr("disabled",false);
		$("#txt_wo_pi").attr("disabled",true);
	}
	else
	{
		$("#cbo_supplier").attr("disabled",true);
		$("#cbo_currency").attr("disabled",true);
		$("#cbo_source").attr("disabled",true);
		$("#txt_wo_pi").attr("disabled",false);
	}
	
	/*if(val==1)
	{
		$("#txt_lc_no").attr("disabled",false);
	}
	else
	{
		$("#txt_lc_no").attr("disabled",true);
	}*/
}


// LC pop up script here-----------------------------------Not Used
function popuppage_lc()
{
	
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/yarn_receive_controller.php?action=lc_popup&company='+company; 
	var title="Search LC Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px,height=370px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];//("search_order_frm"); //Access the form inside the modal window
		var rowID=this.contentDoc.getElementById("hidden_tbl_id").value; // lc table id
		var wopiNumber=this.contentDoc.getElementById("hidden_wopi_number").value; // lc number
		$("#txt_lc_no").val(wopiNumber);
		$("#hidden_lc_id").val(rowID);		  
	}
 	
}


// calculate ILE ---------------------------
function fn_calile()
{
	if( form_validation('cbo_company_name*cbo_source*txt_rate','Company Name*Source*Rate')==false )
	{
		return;
	}
	
	var company=$('#cbo_company_name').val();	
	var source=$('#cbo_source').val();	
	var rate=$('#txt_rate').val();	 
	var responseHtml = return_ajax_request_value(company+'**'+source+'**'+rate, 'show_ile', 'requires/yarn_receive_controller');
	var splitResponse="";
	if(responseHtml!="")
	{
		splitResponse = responseHtml.split("**");
		$("#ile_td").html('ILE% '+splitResponse[0]);
		$("#txt_ile").val(splitResponse[1]);
	}
	else
	{
		$("#ile_td").html('ILE% 0');
		$("#txt_ile").val(0);
	}
	
	//amount and book currency calculate--------------//
	var quantity 		= $("#txt_receive_qty").val();
	var exchangeRate 	= $("#txt_exchange_rate").val();
	var ile_cost 		= $("#txt_ile").val();
	var amount = quantity*1*(rate*1+ile_cost*1); 
	var bookCurrency = (rate*1+ile_cost*1)*exchangeRate*1*quantity*1;
	$("#txt_amount").val(number_format_common(amount,"","",1));
	$("#txt_book_currency").val(number_format_common(bookCurrency,"","",1));
	
}


function fn_room_rack_self_box()
{ 
	if( $("#txt_room").val()*1 > 0 )  
		disable_enable_fields( 'txt_rack', 0, '', '' ); 
	else
	{
		reset_form('','','txt_rack*txt_self*txt_binbox','','','');
		disable_enable_fields( 'txt_rack*txt_self*txt_binbox', 1, '', '' ); 
	}
	if( $("#txt_rack").val()*1 > 0 )  
		disable_enable_fields( 'txt_self', 0, '', '' ); 
	else
	{
		reset_form('','','txt_self*txt_binbox','','','');
		disable_enable_fields( 'txt_self*txt_binbox', 1, '', '' ); 	
	}
	if( $("#txt_self").val()*1 > 0 )  
		disable_enable_fields( 'txt_binbox', 0, '', '' ); 
	else
	{
		reset_form('','','txt_binbox','','','');
		disable_enable_fields( 'txt_binbox', 1, '', '' ); 	
	}
}


function fn_comp_new(val)
{	
	
	if(document.getElementById(val).value=='N') // when new(N) button click
	{											
		load_drop_down( 'requires/yarn_receive_controller', 1, 'load_drop_down_composition', 'composition_td' );		 		
	}
	else // When F button click
	{			
		load_drop_down( 'requires/yarn_receive_controller', 2, 'load_drop_down_composition', 'composition_td' );
	}
		
}



function fn_color_new(val)
{
	var receive_basis = $("#cbo_receive_basis").val();
	var receive_purpose = $("#cbo_receive_purpose").val();
	var cbo_company_name= $("#cbo_company_name").val();
	if(receive_basis==2 && receive_purpose==2)
	{
		load_drop_down( 'requires/yarn_receive_controller', '', 'load_drop_down_color', 'color_td_id' );
		return;
	}
	
	if(document.getElementById(val).value=='N') // when new(N) button click
	{
		document.getElementById('color_td_id').innerHTML='<input type="text" name="cbo_color" id="cbo_color" class="text_boxes" style="width:100px" /><input type="button" class="formbuttonplasminus" name="btn_color" id="btn_color" width="15" onClick="fn_color_new(this.id)" value="F" />';
		$('#cbo_color').attr('readonly',false);
		$('#cbo_color').removeAttr('placeholder','Click');
	}
	else // When F button click
	{		
 		load_drop_down( 'requires/yarn_receive_controller', '', 'load_drop_down_color', 'color_td_id' );
 	}
}

function fnc_yarn_receive_entry(operation)
{
	if(operation==4)
	{
		 print_report( $('#cbo_company_name').val()+'*'+$('#txt_mrr_no').val(), "yarn_receive_print", "requires/yarn_receive_controller" ) 
		 return;
	}
	else if(operation==2)
	{
		show_msg('13');
		return;
	}
	else
	{
		if( form_validation('cbo_company_name*cbo_receive_basis*cbo_receive_purpose*txt_receive_date*txt_challan_no*cbo_store_name*cbo_supplier*cbo_currency*cbo_source*cbo_yarn_count*cbo_yarn_type*cbo_color*txt_yarn_lot*txt_receive_qty*txt_rate','Company Name*Receive Basis*Receive Purpose*Receive Date*Challan No*Store Name*Supplier*Currency*Source*Yarn Count*Yarn Type*Color*Yarn Lot*Receive Quantity*Rate')==false )
		{
			return;
		}	
		if( ($("#percentage1").val()!="" && $("#cbocomposition1").val()==0) || ($("#percentage1").val()=="" && $("#cbocomposition1").val()!=0) )
		{
			alert('First Composition');
			return;
		}
		else if( ($("#percentage2").val()!="" && $("#cbocomposition2").val()==0) || ($("#percentage2").val()=="" && $("#cbocomposition2").val()!=0) )
		{
			alert('2nd Composition');
			return;
		}
		else if($("#cbocomposition1").val()==$("#cbocomposition2").val())
		{
			alert('2nd Composition');
			return;
		}
		else if( $("#txt_rate").val()=="" || $("#txt_rate").val()==0 )
		{
			$("#txt_rate").val('');
			form_validation('txt_rate','Rate');
			return;
		}
		else if( $("#txt_exchange_rate").val()=="" || $("#txt_exchange_rate").val()==0 )
		{
			$("#txt_exchange_rate").val('');
			form_validation('txt_exchange_rate','Exchange Rate');
			return;
		}
		else
		{
			var dataString = "txt_mrr_no*cbo_company_name*cbo_receive_basis*cbo_receive_purpose*txt_receive_date*txt_challan_no*cbo_store_name*txt_lc_no*hidden_lc_id*cbo_supplier*cbo_currency*txt_exchange_rate*cbo_source*txt_wo_pi*txt_wo_pi_id*cbo_yarn_count*cbocomposition1*cbocomposition2*percentage1*percentage2*cbo_yarn_type*btn_color*cbo_color*txt_yarn_lot*txt_brand*txt_receive_qty*txt_rate*txt_ile*cbo_uom*txt_amount*txt_book_currency*txt_order_qty*txt_no_bag*txt_prod_code*txt_room*txt_rack*txt_self*txt_binbox*txt_prod_id*update_id*txt_cone_per_bag*txt_remarks*txt_weight_per_bag*txt_weight_per_cone*job_no*txt_issue_challan_no*txt_issue_id*allocation_maintained";
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
			freeze_window(operation);
			http.open("POST","requires/yarn_receive_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_yarn_receive_entry_reponse;
		}
	}
}

function fnc_yarn_receive_entry_reponse()
{	
	if(http.readyState == 4) 
	{	  	
		//release_freezing(); return;
		var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_yarn_receive_entry('+ reponse[2] +')',8000); 
			 return;
		}
 		else if(reponse[0]==20)
		{
			alert(reponse[1]);
			release_freezing();
			return;
		}
		else if(reponse[0]==10)
		{
			release_freezing();
			return;
		}		
		else if(reponse[0]==0)
		{
			show_msg(reponse[0]);
			$("#txt_mrr_no").val(reponse[1]);
 			//$("#tbl_master :input").attr("disabled", true);	
		}
		else if(reponse[0]==1)
		{
			show_msg(reponse[0]);			 
 			//$("#tbl_master :input").attr("disabled", true);	
			set_button_status(0, permission, 'fnc_yarn_receive_entry',1,1);
		}		
		
		show_list_view(reponse[1],'show_dtls_list_view','list_container_yarn','requires/yarn_receive_controller','');
		set_button_status(0, permission, 'fnc_yarn_receive_entry',1,1);			
		disable_enable_fields( 'txt_mrr_no*txt_wo_pi*cbo_yarn_count*cbo_yarn_type*cbocomposition1*percentage1*cbo_color', 0, "", "" );
		
		if($("#cbo_receive_basis").val()==1 || $("#cbo_receive_basis").val()==2)
		{
			change_color_tr(0,'');
		}
 		//child form reset here after save data-------------//
		var txt_wo_pi_id = $("#txt_wo_pi_id").val();
		var txt_wo_pi = $("#txt_wo_pi").val();
		var btn_color = $("#btn_color").val();
 		$("#tbl_child").find('input,select').val('');
		//$("#tbl_master").find('input,select').attr("disabled", true);
		$("#txt_wo_pi_id").val(txt_wo_pi_id);
		$("#txt_wo_pi").val(txt_wo_pi);
		$("#btn_color").val(btn_color);
		$("#cbo_uom").val(12);
		release_freezing();
	}
}


function control_composition(type)
{
	var cbocompone=(document.getElementById('cbocomposition1').value);
	var cbocomptwo=(document.getElementById('cbocomposition2').value);
	var percentone=(document.getElementById('percentage1').value)*1;
	var percenttwo=(document.getElementById('percentage2').value)*1;
	 
	
	if(type=='percent_one' && percentone>100)
	{
		alert("Greater Than 100 Not Allwed");
		document.getElementById('percentage1').value="";
	}
	
	if(type=='percent_one' && percentone<=0)
	{
		alert("0 Or Less Than 0 Not Allwed")
		document.getElementById('percentage1').value="";
		document.getElementById('percentage1').disabled=true;
		document.getElementById('cbocomposition1').value=0;
		document.getElementById('cbocomposition1').disabled=true;
		document.getElementById('percentage2').value=100;
	    document.getElementById('percentage2').disabled=false;
		document.getElementById('cbocomposition2').disabled=false;
 	}
	if(type=='percent_one' && percentone==100)
	{
		document.getElementById('percentage2').value="";
		document.getElementById('cbocomposition2').value=0;
		document.getElementById('percentage1').disabled=false;
		document.getElementById('cbocomposition1').disabled=false;
		document.getElementById('percentage2').disabled=true;
		document.getElementById('cbocomposition2').disabled=true;
 	}
	
	if(type=='percent_one' && percentone < 100 && percentone > 0 )
	{
		document.getElementById('percentage2').value=100-percentone;
	    document.getElementById('percentage2').disabled=false;
		document.getElementById('cbocomposition2').disabled=false;
 	}
	
	if(type=='comp_one' && cbocompone==cbocomptwo  )
	{
		alert("Same Composition Not Allowed");
		document.getElementById('cbocomposition1').value=0;
 	}
	
 	if(type=='percent_two' && percenttwo>100)
	{
		alert("Greater Than 100 Not Allwed")
		document.getElementById('percentage2').value="";
 	}
	if(type=='percent_two' && percenttwo<=0)
	{
		alert("0 Or Less Than 0 Not Allwed")
		document.getElementById('percentage2').value="";
		document.getElementById('percentage2').disabled=true;
		document.getElementById('cbocomposition2').value=0;
		document.getElementById('cbocomposition2').disabled=true;
		document.getElementById('percentage1').value=100;
		document.getElementById('percentage1').disabled=false;
		document.getElementById('cbocomposition1').disabled=false;
	}
	if(type=='percent_two' && percenttwo==100)
	{
		document.getElementById('percentage1').value="";
		document.getElementById('cbocomposition1').value=0;
		document.getElementById('percentage1').disabled=false;
		document.getElementById('cbocomposition1').disabled=false;
		document.getElementById('percentage2').disabled=true;
		document.getElementById('cbocomposition2').disabled=true;
	}
	
	if(type=='percent_two' && percenttwo<100 && percenttwo>0)
	{
		document.getElementById('percentage1').value=100-percenttwo;
		document.getElementById('percentage1').disabled=false;
		document.getElementById('cbocomposition1').disabled=false;
 	}
	
	if(type=='comp_two' && cbocomptwo==cbocompone)
	{
		alert("Same Composition Not Allowed");
		document.getElementById('cbocomposition2').value=0;
 	}
}


function open_mrrpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/yarn_receive_controller.php?action=mrr_popup_info&company='+company; 
	var title="Search MRR Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=400px,center=1,resize=0,scrolling=0',' ')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		
		var recv_id=this.contentDoc.getElementById("hidden_recv_id").value;
		var mrrNumber=this.contentDoc.getElementById("hidden_recv_number").value; // mrr number
 		$("#txt_mrr_no").val(mrrNumber);
		$("#tbl_child").find('input,select').val('');
		$("#cbo_uom").val(12);
		$("#btn_color").val('N');
		load_drop_down('requires/yarn_receive_controller', '', 'load_drop_down_color', 'color_td_id' );
		// master part call here
		get_php_form_data(mrrNumber+"_"+recv_id, "populate_data_from_data", "requires/yarn_receive_controller");
		$("#tbl_master").find('input,select').attr("disabled", true);	
		disable_enable_fields( 'txt_mrr_no', 0, "", "" );
		set_button_status(0, permission, 'fnc_yarn_receive_entry',1,1);	
		//right side list call here
		//show_list_view(receive_basis+"**"+rowID,'show_product_listview','list_product_container','requires/yarn_receive_controller','');
		//list view call here
		//show_list_view(mrrNumber,'show_dtls_list_view','list_container_yarn','requires/yarn_receive_controller','');
 	}
}

function change_color_tr(v_id,e_color)
{
	var tot_row=$("#tbl_product tbody tr").length;
	for(var i=1; i<=tot_row;i++)
	{
		if(v_id==i)
		{
			document.getElementById("tr_"+v_id).bgColor="#33CC00";
		}
		else
		{
			if (i%2==0) Bcolor="#E9F3FF";						
			else Bcolor="#FFFFFF";
			document.getElementById("tr_"+i).bgColor=Bcolor;
		}
	}
}

function openpage_challan()
{
	if( form_validation('cbo_company_name*cbo_supplier','Company Name*Supplier')==false )
	{
		return;
	}
	
	var receive_purpose = $("#cbo_receive_purpose").val();
	var company = $("#cbo_company_name").val();	
	var supplier = $("#cbo_supplier").val();	
	
	if(receive_purpose==15)
	{
		var page_link='requires/yarn_receive_controller.php?action=issue_challan_popup_info&company='+company+'&supplier='+supplier; 
		var title="Issue Challan No Popup";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=370px,center=1,resize=0,scrolling=0',' ')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]; 
			
			var issue_id=this.contentDoc.getElementById("hidden_issue_id").value;
			var challan_number=this.contentDoc.getElementById("hidden_challan_number").value; // mrr number
			$("#txt_issue_challan_no").val(issue_id);
			$("#txt_issue_id").val(challan_number);
		}
	}
}
function load_supplier()
{
	var receive_purpose = $("#cbo_receive_purpose").val();
	var company = $("#cbo_company_name").val();	
	
	if( form_validation('cbo_company_name','Company')==false )
	{
		$("#cbo_receive_purpose").val(0);
		return;
	}
	
	$("#txt_issue_challan_no").val('');
	$("#txt_issue_id").val('');
	
	if(receive_purpose==15)
	{
		$("#txt_issue_challan_no").attr("disabled",false);
		load_drop_down( 'requires/yarn_receive_controller', company, 'load_drop_down_supplier_from_issue', 'supplier' );
		
		if($('#cbo_supplier option').length==2)
		{
			$('#cbo_supplier').val($('#cbo_supplier option:last').val());
		}
	}
	else
	{
		$("#txt_issue_challan_no").attr("disabled",true);
		load_drop_down( 'requires/yarn_receive_controller', company, 'load_drop_down_supplier', 'supplier' );
	}
}

//form reset/refresh function here
function fnResetForm()
{
	$("#tbl_master").find('input').attr("disabled", false);	
	disable_enable_fields( 'cbo_company_name*cbo_receive_basis*cbo_receive_purpose*cbo_store_name*txt_wo_pi*cbo_yarn_count*cbo_yarn_type*cbocomposition1*percentage1*cbo_color', 0, "", "" );
	set_button_status(0, permission, 'fnc_yarn_receive_entry',1);
	reset_form('yarn_receive_1','list_container_yarn*list_product_container','','','','cbo_uom*cbo_currency*txt_exchange_rate*cbo_color');
	$("#txt_rate").val(0);
}

function exchange_rate(val)
{
	if(val==1)
	{
		$("#txt_exchange_rate").val(1);
	}
	else
	{
		$("#txt_exchange_rate").val('');
	}
}

</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../",$permission);  ?><br />    		 
    <form name="yarn_receive_1" id="yarn_receive_1" autocomplete="off" > 
    <div style="width:80%;">       
    <table width="80%" cellpadding="0" cellspacing="2" align="left">
     	<tr>
        	<td width="80%" align="center" valign="top">   
            	<fieldset style="width:1000px; float:left;">
                <legend>Yarn Receive</legend>
                <br />
                 	<fieldset style="width:950px;">                                       
                        <table  width="950" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                            <tr>
                           		<td colspan="6" align="center">&nbsp;<b>MRR Number</b>
                                	<input type="text" name="txt_mrr_no" id="txt_mrr_no" class="text_boxes" style="width:155px" placeholder="Double Click To Search" onDblClick="open_mrrpopup()" readonly />
                                </td>
                           </tr>
                           <tr>
                                <td width="130" align="right" class="must_entry_caption">Company Name </td>
                                <td width="170">
									<?php 
                                  		echo create_drop_down( "cbo_company_name", 170, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "rcv_basis_reset();load_drop_down( 'requires/yarn_receive_controller', this.value, 'load_drop_down_supplier', 'supplier' );load_drop_down( 'requires/yarn_receive_controller', this.value, 'load_drop_down_store', 'store_td' );get_php_form_data(this.value, 'is_allocation_maintained', 'requires/yarn_receive_controller');" );
                                    ?>
                                </td>
                                <td width="94" align="right" class="must_entry_caption"> Receive Basis </td>
                                <td width="160">
                                    <?php 
                                    //load_drop_down( 'requires/yarn_receive_controller', this.value, 'load_drop_down_supplier', 'supplier'); load_drop_down('requires/yarn_receive_controller', this.value, 'load_drop_down_currency', 'currency'); load_drop_down('requires/yarn_receive_controller', this.value, 'load_drop_down_source', 'sources'); load_drop_down('requires/yarn_receive_controller', this.value, 'load_drop_down_lc', 'lc_no');
                                    echo create_drop_down( "cbo_receive_basis", 170, $receive_basis_arr,"", 1, "- Select Receive Basis -", $selected, "fn_independent(this.value)","","1,2,4" );
                                    ?>
                               </td>
                               <td width="130" align="right" class="must_entry_caption">Receive Purpose</td>
                               <td width="170">
                                    <?php 
                                    	echo create_drop_down( "cbo_receive_purpose", 170, $yarn_issue_purpose,"", 1, "-- Select Purpose --", 16, "load_supplier();", "","2,5,6,15,16");
                                    ?>
                               </td>
                            </tr>
                            <tr>
                                <td  width="130" align="right" class="must_entry_caption">Receive Date </td>
                                <td width="170">
                                   <input type="text" name="txt_receive_date" id="txt_receive_date" class="datepicker" style="width:160px;" placeholder="Select Date" />
                                </td>
                                <td width="94" align="right" class="must_entry_caption"> Challan No </td>
                                <td width="160"><input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:160px" ></td>
                                <td width="130" align="right" class="must_entry_caption">Store Name</td>
                                <td width="170" id="store_td">
                                    <?php 
                                    echo create_drop_down( "cbo_store_name", 170, $blank_array,"", 1, "-- Select Store --", $storeName, "" );
                                    ?>
                                </td>
                            </tr>
                            <tr>
                          		<td width="130" align="right">WO / PI</td>
                                <td width="170"><input class="text_boxes"  type="text" name="txt_wo_pi" id="txt_wo_pi" onDblClick="openmypage('xx','Order Search')"  placeholder="Double Click" style="width:160px;" readonly disabled />
                                <input type="hidden" id="txt_wo_pi_id" name="txt_wo_pi_id" value="" /></td> 
                                <td width="94" align="right" class="must_entry_caption"> Supplier </td>
                                   <td id="supplier" width="160"> 
                                    <?php
                                      echo create_drop_down( "cbo_supplier", 170, $blank_array,"", 1, "--- Select Supplier ---", $selected, "",1);
                                    ?>
                                   </td>
                                   <td width="130" align="right" class="must_entry_caption">Currency</td>
                                   <td width="170" id="currency"> 
                                    <?php
                                       echo create_drop_down( "cbo_currency", 170, $currency,"", 1, "-- Select Currency --", $currencyID, "exchange_rate(this.value)",1 );
                                    ?>
                                   </td>
                            </tr>
                             <tr>
                                <td width="130" align="right" class="must_entry_caption">Exchange Rate</td>
                                <td width="170">
                                    <input type="text" name="txt_exchange_rate" id="txt_exchange_rate" class="text_boxes_numeric" style="width:160px" value="<?php echo $exchangeRate; ?>" onBlur="fn_calile()" />	
                                </td>
                                <td width="94" align="right">Issue Challan No.</td>
                                <td width="160" id="sources">  
									<input type="text" name="txt_issue_challan_no" id="txt_issue_challan_no" class="text_boxes" style="width:160px" onDblClick="openpage_challan();" placeholder="Double Click To Search" disabled readonly/>	
                                    <input type="hidden" id="txt_issue_id" name="txt_issue_id" value="" />
                                </td>
                                <td width="94" align="right" class="must_entry_caption">Source</td>
                                <td width="160" id="sources">  
									<?php
                                        echo create_drop_down( "cbo_source", 170, $source,"", 1, "-- Select --", $selected, "",1 );
                                    ?>
                                </td>                                  
                            </tr>
                            <tr>
                            	<td width="130" align="right"> L/C No </td>
                                <td id="lc_no" width="170">
                                <input class="text_boxes"  type="text" name="txt_lc_no" id="txt_lc_no" style="width:160px;" placeholder="Display" onDblClick="popuppage_lc()" readonly disabled  />  
                                <input type="hidden" name="hidden_lc_id" id="hidden_lc_id" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                    <br />
                    <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                    <tr>
                    <td width="49%" valign="top">
                   	  <fieldset style="width:950px;">  
                        <legend>New Receive Item</legend>                                     
                            <table width="220" cellspacing="2" cellpadding="0" border="0" style="float:left"> 
                                <tr>    
                                        <td align="right" class="must_entry_caption" width="100">Yarn Count</td>
                                        <td width="150">         
                                            <?php
                                                echo create_drop_down( "cbo_yarn_count", 130, "select id,yarn_count from lib_yarn_count where is_deleted = 0 AND status_active = 1 ORDER BY yarn_count ASC","id,yarn_count", 1, "--Select--", 0, "",0 );
                                            ?>
                                        </td>
                                </tr>
                                <tr>    
                                        <td align="right">Composition</td>
                                        <td id="composition_td">
                                        <?php
                                            echo create_drop_down( "cbocomposition1", 80, $composition,"", 1, "-- Select --", "", "", $disabled,"" );
                                            echo '<input type="text" id="percentage1" name="percentage1" class="text_boxes_numeric" style="width:40px" placeholder="%" onBlur="control_composition(\'percent_one\')" />';
                                            echo create_drop_down( "cbocomposition2", 80, $composition,"", 1, "-- Select --", "", "", $disabled,"" );
                                            echo '<input type="text" id="percentage2" name="percentage2" class="text_boxes_numeric" style="width:40px" placeholder="%" onBlur="control_composition(\'percent_two\')" />';
                                        ?>	
                                            <!--script>load_drop_down( 'requires/yarn_receive_controller', '', 'load_drop_down_composition', 'composition_td' );</script-->
                                        
                                        </td>
                                </tr>
                                <tr>    
                                        <td align="right" class="must_entry_caption">Yarn Type</td>
                                        <td>
                                            <?php
                                                echo create_drop_down( "cbo_yarn_type", 130, $yarn_type,"", 1, "--Select--", 0, "",0 );
                                            ?>
                                        </td>
                                </tr>
                                <tr>   
                                        <td align="right" class="must_entry_caption">Color</td>
                                        <td id="color_td_id">
                                            <?php
												if($db_type==0) $color_cond=" and color_name!=''"; else $color_cond=" and color_name IS NOT NULL";
                                                echo create_drop_down( "cbo_color", 110, "select id,color_name from lib_color where status_active=1 $color_cond order by color_name","id,color_name", 1, "--Select--", 0, "",0 );
                                            ?>
                                            <input type="button" name="btn_color" id="btn_color" class="formbuttonplasminus"  style="width:20px" onClick="fn_color_new(this.id)" value="N" />
                                        </td>
                                </tr>  
                                <tr>
                                		<td align="right" class="must_entry_caption"><!--Yarn -->Lot/ Batch</td>
                                        <td><input type="text" name="txt_yarn_lot" id="txt_yarn_lot" class="text_boxes" style="width:120px;" /></td>
                                </tr>  
                            </table>
                            
                            <table width="240" cellspacing="2" cellpadding="0" border="0" style="float:left">
                            	<tr>    
                                        <td align="right" width="88">Brand</td>
                                    	<td width="146"><input type="text" name="txt_brand" id="txt_brand" class="text_boxes" style="width:120px;" /></td>
                                </tr>
                                <tr>    
                                        <td align="right" class="must_entry_caption">Recv. Qnty.</td>   
                                        <td >
                                            <input name="txt_receive_qty" id="txt_receive_qty" class="text_boxes_numeric" type="text" style="width:120px;" onBlur="fn_calile()" />
                                        </td> <!--onKeyUp="fn_calile()" -->
                                </tr>
                                <tr>
                                        <td align="right">UOM</td>   
                                        <td><?php echo create_drop_down( "cbo_uom", 130, $unit_of_measurement,"", 0, "--Select--", 12, "",1 ); ?></td> 
                                </tr>
                                
                                <tr>    
                                        <td align="right" class="must_entry_caption">Rate</td>   
                                        <td >
                                        	<input name="txt_rate" id="txt_rate" class="text_boxes_numeric" type="text" style="width:120px;" onBlur="fn_calile()" value="0" />
                                        </td>
                                </tr>
                                <tr>   
                                        <td align="right" id="ile_td">ILE%</td>   
                                        <td >
                                        	<input name="txt_ile" id="txt_ile" class="text_boxes_numeric" type="text" style="width:120px;" placeholder="ILE COST" readonly />
                                        </td>
                                </tr>
                                 <tr>
                                 		<td align="right">Amount</td>
                                        <td><input type="text" name="txt_amount" id="txt_amount" class="text_boxes_numeric" style="width:120px;" readonly disabled /></td>
                                </tr>  
                            </table>
                            
                            <table width="244" cellspacing="2" cellpadding="0" border="0" style="float:left">
                                <tr> 
                                		<td align="right" width="111">Book Currency.</td>
                                        <td width="133">
                                            <input type="text" name="txt_book_currency" id="txt_book_currency" class="text_boxes_numeric" style="width:120px;" readonly disabled />
                                        </td>                
                                </tr>
                                <tr>
                                		<td align="right">No. Of Bag</td>
                                      	<td><input name="txt_no_bag" id="txt_no_bag" type="text" class="text_boxes_numeric"  style="width:120px;" /></td> 
                                </tr>
                                <tr> 
                                		<td align="right">No. Of Cone</td>   
                                        <td>
                                            <input name="txt_cone_per_bag" id="txt_cone_per_bag" class="text_boxes_numeric" type="text" style="width:120px;"/>
                                    	</td>
                                </tr>
                                <tr> 
                                		<td align="right">Weight per Bag</td>   
                                        <td>
                                            <input name="txt_weight_per_bag" id="txt_weight_per_bag" class="text_boxes_numeric" type="text" style="width:120px;"/>
                                        </td>
                                </tr>
                                <tr> 
                                		<td align="right">Wght @ Cone</td>
                                        <td><input class="text_boxes_numeric"  name="txt_weight_per_cone" id="txt_weight_per_cone" type="text" style="width:120px;"  /></td>
                                 </tr> 
                                 <tr>
                                 		<td align="right">Room</td>
                                    	<td><input class="text_boxes_numeric" name="txt_room" id="txt_room" type="text" style="width:120px;" onKeyUp="fn_room_rack_self_box()"/></td>
                                </tr>  
                            </table>
                            
                            
                            <table width="230" cellspacing="2" cellpadding="0" border="0">
                                
                                <tr> 
                                    <td width="118" align="right">Rack</td>
                                    <td width="106"><input class="text_boxes_numeric" name="txt_rack" id="txt_rack" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" disabled /></td>
                                </tr>
                                <tr> 
                                	<td align="right">Self</td>
                                	<td><input class="text_boxes_numeric"  name="txt_self" id="txt_self" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" disabled /></td>
                                </tr>
                                <tr> 
                                      <td align="right">Bin/Box</td>
                                      <td><input class="text_boxes_numeric"  name="txt_binbox" id="txt_binbox" type="text" style="width:100px;" disabled /></td>
                                 </tr> 
                                <tr> 
                                      <td align="right">Remarks</td>   
                                      <td><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:100px;" /></td> 
                                </tr>
                                <tr> 
                                    <td align="right">Bal. PI/ Ord. Qnty</td>
                                    <td><input class="text_boxes_numeric"  name="txt_order_qty" id="txt_order_qty" type="text" style="width:100px;" readonly /></td>
                                </tr>
                                <tr>                 
                                    <td align="right">Product Code</td>
                                    <td><input class="text_boxes"  name="txt_prod_code" id="txt_prod_code" type="text" style="width:100px;" readonly  /></td>
                                </tr> 
                            </table>
                    </fieldset>
                    </td>
                    </tr>
                </table>                
               	<table cellpadding="0" cellspacing="1" width="100%">
                	<tr> 
                       <td colspan="6" align="center"></td>				
                	</tr>
                	<tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <!-- details table id for update -->
                             <input type="hidden" id="txt_prod_id" name="txt_prod_id" value="" />
                             <input type="hidden" id="allocation_maintained" name="allocation_maintained" value="" />
                             <input type="hidden" id="update_id" name="update_id" value="" />
                             <input type="hidden" name="job_no" id="job_no" readonly /><!--For Basis Bokking and Yarn Dyeing Purpose-->
							 <?php echo load_submit_buttons( $permission, "fnc_yarn_receive_entry", 0,1,"fnResetForm()",1);?>
                        </td>
                   </tr> 
                </table>                 
              	</fieldset>
              	<fieldset>
    			<div style="width:990px;" id="list_container_yarn"></div>
    		  	</fieldset>
           </td>
         </tr>
    </table>
    </div>
    <div id="list_product_container" style="max-height:500px; width:20%; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>  
	</form>
</div>    
</body>  
<script src="../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
