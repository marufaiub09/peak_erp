﻿<?php
/*-------------------------------------------- Comments----------------
Version                  :   V2
Purpose			         : 	This form will create  Capacity & order Booking Status Report
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 		Maruf
Update date		         : 		 08-12-2015  
QC Performed BY	         :		
QC Date			         :	
Comments		         :  Oracle Compatible Version
-----------------------------------------------------------------------*/
error_reporting('0');
session_start();
include('../../../../includes/common.php');

extract($_REQUEST);
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$date=date('Y-m-d');
$buyer_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$company_short_name_arr=return_library_array( "select id,company_short_name from lib_company",'id','company_short_name');
$company_team_name_arr=return_library_array( "select id,team_name from lib_marketing_team",'id','team_name');
$company_team_member_name_arr=return_library_array( "select id,team_member_name from  lib_mkt_team_member_info",'id','team_member_name');
$imge_arr=return_library_array( "select master_tble_id,image_location from   common_photo_library",'master_tble_id','image_location');
$commission_for_shipment_schedule_arr=return_library_array( "select job_no,commission from  wo_pre_cost_dtls",'job_no','commission');

$costing_per_arr=return_library_array( "select job_no,costing_per from  wo_pre_cost_mst",'job_no','costing_per');
$company_arr=return_library_array( "select id, company_name from  lib_company",'id','company_name');
$country_name_arr=return_library_array( "select id, country_name from lib_country",'id','country_name');


if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 172, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );   	 
} 

if ($action=="load_drop_down_team_member")
{
	echo create_drop_down( "cbo_team_member", 172, "select id,team_member_name 	 from lib_mkt_team_member_info  where team_id='$data' and status_active=1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-Select Team Member-", $selected, "" );   	 
}


if($type=="report_generate")
{
	$data=explode("_",$data);
	if($data[0]==0) $company_name="%%"; else $company_name=$data[0];
	if($data[1]==0) $buyer_name="%%"; else $buyer_name=$data[1];
	if(trim($data[2])!="") $start_date=$data[2];
	if(trim($data[3])!="") $end_date=$data[3];
	if(trim($data[4])=="0") $team_leader="%%"; else $team_leader="$data[4]";
	if(trim($data[5])=="0") $dealing_marchant="%%"; else $dealing_marchant="$data[5]";
	if($db_type==0)
	{
	$start_date=change_date_format($start_date,'yyyy-mm-dd','-');
	$end_date=change_date_format($end_date,'yyyy-mm-dd','-');
    }
	if($db_type==2)
	{
	$start_date=change_date_format($start_date,'yyyy-mm-dd','-',1);
	$end_date=change_date_format($end_date,'yyyy-mm-dd','-',1);
    }
	$cbo_category_by=$data[6];
	$zero_value=$data[7];
	$cbo_capacity_source=$data[8];
	if($cbo_category_by==1)
	{
		if ($start_date!="" && $end_date!="")
		{
			$date_cond="and c.country_ship_date between '$start_date' and  '$end_date'";
			$date_cond_target_basic="and b.date between '$start_date' and  '$end_date'";
		}
		else	
		{
			$date_cond="";
			$date_cond_target_basic="";
		}
	}
	else
	{
		if ($start_date!="" && $end_date!="")
		{
			$date_cond=" and c.country_ship_date between '$start_date' and  '$end_date'";
			$date_cond_target_basic="and b.date between '$start_date' and  '$end_date'";
		}
		else	
		{
			$date_cond="";
			$date_cond_target_basic="";
		}
	}
	
	if($cbo_capacity_source==1)
	{
		$capacity_source_cond="and c.capacity_source=1";
	}
	else if($cbo_capacity_source==3)
	{
		$capacity_source_cond="and c.capacity_source=3";
	}
	else
	{
		$capacity_source_cond="";
	}
	
	if ($start_date!="" && $end_date!="")
	{
		$year="";
		$sy = date('Y',strtotime($start_date));
		$ey = date('Y',strtotime($end_date));
		$dif_y=$ey-$sy;
		for($i=1; $i<$dif_y; $i++)
		{
		 $year.= $sy+$i.","; 
		}
		$tot_year= $sy;
		if($year !="")
		{
			$tot_year.=",".$year;
		}
		if($ey!=$sy)
		{
			if($year=="")
			{
			$tot_year.=",".$ey;
			}
			else
			{
			$tot_year.=$ey;	
			}
		}
		$year_cond="and a.year_id in($tot_year)";
	}
    $target_basic_qnty=array();
	$total_target_basic_qnty=0;
    $sm = date('m',strtotime($start_date));
	$em = date('m',strtotime($end_date));
	
	if($db_type==0)
	{
    $sql_con="SELECT  a.company_id,b.buyer_id,b.allocation_percentage";
	}
	
	if($db_type==2)
	{
    $sql_con="SELECT  b.buyer_id";
	}
	
	for($i=1;$i<=12;$i++)
	{
		 $sql_con .= ",SUM(CASE WHEN d.month_id =".$i. " THEN (d.capacity_month_pcs)   END) AS capa$i ,SUM(CASE WHEN d.month_id =".$i. " THEN (d.capacity_month_pcs* b.allocation_percentage)/100   END) AS sum$i";
	}
	$sql_con .= " FROM lib_capacity_allocation_mst a,lib_capacity_allocation_dtls b, lib_capacity_calc_mst c,  lib_capacity_year_dtls d
	WHERE 
	a.id=b.mst_id AND 
	a.year_id=c.year AND 
	a.month_id=d.month_id  AND 
	c.id=d.mst_id AND 
	a.company_id=$company_name   AND 
	a.year_id=$sy and 
	d.month_id between $sm and $em 
	$capacity_source_cond  and 
	a.status_active=1 and 
	a.is_deleted=0 and 
	b.status_active=1 and 
	b.is_deleted=0 and 
	c.status_active=1 and 
	c.is_deleted=0  
	GROUP BY b.buyer_id";
	
	
	$sql_data=sql_select($sql_con);
	foreach( $sql_data as $row)
		{
			$total_sum1 = $total_sum1+$row[csf("sum1")];
			$total_sum2 = $total_sum2+$row[csf("sum2")];
			$total_sum3 = $total_sum3+$row[csf("sum3")];
			$total_sum4 = $total_sum4+$row[csf("sum4")];
			$total_sum5 = $total_sum5+$row[csf("sum5")];
			$total_sum6 = $total_sum6+$row[csf("sum6")];
			$total_sum7 = $total_sum7+$row[csf("sum7")];
			$total_sum8 = $total_sum8+$row[csf("sum8")];
			$total_sum9 = $total_sum9+$row[csf("sum9")];
			$total_sum10 = $total_sum10+$row[csf("sum10")];
			$total_sum11 = $total_sum11+$row[csf("sum11")];
			$total_sum12 = $total_sum12+$row[csf("sum12")];
			$total_cap=$row[csf("sum1")]+$row[csf("sum2")]+$row[csf("sum3")]+$row[csf("sum4")]+$row[csf("sum5")]+$row[csf("sum6")]+$row[csf("sum7")]+$row[csf("sum8")]+$row[csf("sum9")]+$row[csf("sum10")]+$row[csf("sum11")]+$row[csf("sum12")];
			$target_basic_qnty[$row[csf("buyer_id")]]=$total_cap;
			$total_target_basic_qnty+=$total_cap;
		}
		
		
		
  $asking_avg_rate_arr=return_library_array( "select company_id, asking_avg_rate from lib_standard_cm_entry",'company_id','asking_avg_rate');
  $basic_smv_arr=return_library_array( "select comapny_id, basic_smv from lib_capacity_calc_mst",'comapny_id','basic_smv');
  $job_smv_arr=return_library_array( "select job_no, set_smv from wo_po_details_master",'job_no','set_smv');
  $buy_sew_effi_percent_arr=return_library_array( "select id, sewing_effi_plaing_per from lib_buyer",'id','sewing_effi_plaing_per');
  
 
$sql_data=sql_select("select a.company_id,a.location_id,a.year_id,a.month_id, b.buyer_id,b.allocation_percentage from lib_capacity_allocation_mst a, lib_capacity_allocation_dtls b where a.id=b.mst_id and a.company_id like '$company_name' and b.buyer_id like '$buyer_name'  $year_cond and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
$company_buyer_array=array();
foreach ($sql_data as $row)
{
	$company_buyer_array[$row[csf("company_id")]][$row[csf("buyer_id")]]= array("order_qty_pcs"=>0,"order_value"=>0,"ex_factory"=>0,"smv"=>0);
	$company_buyer_array_confirm[$row[csf("company_id")]][$row[csf("buyer_id")]]= array("order_qty_pcs"=>0,"order_value"=>0,"ex_factory"=>0);
	$company_buyer_array_projected[$row[csf("company_id")]][$row[csf("buyer_id")]]= array("order_qty_pcs"=>0,"order_value"=>0,"ex_factory"=>0);
}
$po_total_price_tot=0;
$quantity_tot=0;
$exfactory_tot=0;


$po_total_price_tot_c=0;
$quantity_tot_c=0;

$po_total_price_tot_p=0;
$quantity_tot_p=0;

$booked_basic_qnty_tot_c=0;
$booked_basic_qnty_tot_p=0;
/*$data_array=sql_select("select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.style_description, a.job_quantity, a.product_category, a.job_no, a.location_name, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,a.product_code, b.id, b.is_confirmed, b.po_number, sum(c.order_quantity/a.total_set_qnty) as po_quantity, sum(c.order_quantity) as po_quantity_pcs, b.pub_shipment_date, b.po_received_date, DATEDIFF(c.country_ship_date, '$date') date_diff_1, DATEDIFF(c.country_ship_date, '$date') date_diff_2, b.unit_price, sum(c.order_total) as po_total_price, b.details_remarks, b.shiping_status,c.country_ship_date,c.country_id, sum(d.ex_factory_qnty) as ex_factory_qnty, MAX(d.ex_factory_date) as ex_factory_date, DATEDIFF(c.country_ship_date, MAX(d.ex_factory_date)) date_diff_3, DATEDIFF(c.country_ship_date,MAX(d.ex_factory_date)) date_diff_4, b.t_year, b.t_month  from wo_po_details_master a, wo_po_break_down b, wo_po_color_size_breakdown c LEFT JOIN pro_ex_factory_mst d on c.po_break_down_id = d.po_break_down_id and c.country_id=d.country_id and d.status_active=1 and d.is_deleted=0 where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id   and a.company_name like '$company_name' and a.buyer_name like '$buyer_name' and a.team_leader like '$team_leader'  and a.dealing_marchant like '$dealing_marchant' $date_cond  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by b.id,c.country_id order by c.country_ship_date,a.job_no_prefix_num,b.id");*/
$exfactory_data_array=array();
$exfactory_data=sql_select("select po_break_down_id,country_id,sum(ex_factory_qnty) as ex_factory_qnty, MAX(ex_factory_date) as ex_factory_date from pro_ex_factory_mst  where status_active=1 and is_deleted=0 group by po_break_down_id,country_id");
foreach($exfactory_data as $exfatory_row)
{
	$exfactory_data_array[$exfatory_row[csf('po_break_down_id')]][$exfatory_row[csf('country_id')]][ex_factory_qnty]=$exfatory_row[csf('ex_factory_qnty')];
	$exfactory_data_array[$exfatory_row[csf('po_break_down_id')]][$exfatory_row[csf('country_id')]][ex_factory_date]=$exfatory_row[csf('ex_factory_date')];
}

if($db_type==0)
{
$data_array=sql_select("select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.style_description, a.job_quantity, a.product_category, a.job_no, a.location_name, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,a.product_code, b.id, b.is_confirmed, b.po_number, sum(c.order_quantity/a.total_set_qnty) as po_quantity, sum(c.order_quantity) as po_quantity_pcs, b.pub_shipment_date, b.po_received_date, DATEDIFF(c.country_ship_date, '$date') date_diff_1, DATEDIFF(c.country_ship_date, '$date') date_diff_2, b.unit_price, sum(c.order_total) as po_total_price, b.details_remarks, c.shiping_status,c.country_ship_date,c.country_id,  b.t_year, b.t_month  from wo_po_details_master a, wo_po_break_down b, wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id   and a.company_name like '$company_name' and a.buyer_name like '$buyer_name' and a.team_leader like '$team_leader'  and a.dealing_marchant like '$dealing_marchant' $date_cond  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.style_description, a.job_quantity, a.product_category, a.job_no, a.location_name, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,a.product_code,b.id,b.is_confirmed,b.po_number,b.pub_shipment_date,b.po_received_date,b.unit_price,b.details_remarks,b.t_year, b.t_month,c.country_id,c.country_ship_date,c.shiping_status order by c.country_ship_date,a.job_no_prefix_num,b.id");


/*$data_array=sql_select("select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.style_description, a.job_quantity, a.product_category, a.job_no, a.location_name, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,a.product_code, b.id, b.is_confirmed, b.po_number, sum(c.order_quantity/a.total_set_qnty) as po_quantity, sum(c.order_quantity) as po_quantity_pcs, b.pub_shipment_date, b.po_received_date, DATEDIFF(c.country_ship_date, '$date') date_diff_1, DATEDIFF(c.country_ship_date, '$date') date_diff_2, b.unit_price, sum(c.order_total) as po_total_price, b.details_remarks, c.shiping_status,c.country_ship_date,c.country_id,  b.t_year, b.t_month  from wo_po_details_master a, wo_po_break_down b, wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id   and a.company_name like '$company_name' and a.buyer_name like '$buyer_name' and a.team_leader like '$team_leader'  and a.dealing_marchant like '$dealing_marchant' $date_cond  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.style_description, a.job_quantity, a.product_category, a.job_no, a.location_name, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,a.product_code,b.id,b.is_confirmed,b.po_number,b.pub_shipment_date,b.po_received_date,b.unit_price,b.details_remarks,b.t_year, b.t_month,c.country_id,c.country_ship_date,c.shiping_status order by c.country_ship_date,a.job_no_prefix_num,b.id");*/
}
if($db_type==2)
{
	$date=date('d-m-Y');
	$data_array=sql_select("select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.style_description, a.job_quantity, a.product_category, a.job_no, a.location_name, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,a.product_code, b.id, b.is_confirmed, b.po_number, sum(c.order_quantity/a.total_set_qnty) as po_quantity, sum(c.order_quantity) as po_quantity_pcs, b.pub_shipment_date, b.po_received_date, (c.country_ship_date - to_date('$date','dd-mm-yyyy')) date_diff_1, (c.country_ship_date - to_date('$date','dd-mm-yyyy')) date_diff_2, b.unit_price, sum(c.order_total) as po_total_price, b.details_remarks, c.shiping_status,c.country_ship_date,c.country_id,  b.t_year, b.t_month  from wo_po_details_master a, wo_po_break_down b, wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id   and a.company_name like '$company_name' and a.buyer_name like '$buyer_name' and a.team_leader like '$team_leader'  and a.dealing_marchant like '$dealing_marchant' $date_cond  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.style_description, a.job_quantity, a.product_category, a.job_no, a.location_name, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,a.product_code,b.id,b.is_confirmed,b.po_number,b.pub_shipment_date,b.po_received_date,b.unit_price,b.details_remarks,b.t_year, b.t_month,c.country_id,c.country_ship_date,c.shiping_status order by c.country_ship_date,a.job_no_prefix_num,b.id");
}
foreach ($data_array as $row)
{
 $y = date('Y',strtotime($row[csf("pub_shipment_date")]));
 $po_total_price_tot+=$row[csf("po_total_price")];
 $quantity_tot+=$row[csf("po_quantity_pcs")];
 //$exfactory_tot+=$row[csf("ex_factory_qnty")];
 $exfactory_tot+=$exfactory_data_array[$row[csf("id")]][$row[csf("country_id")]][ex_factory_qnty];

 $company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][order_qty_pcs]+=$row[csf("po_quantity_pcs")];
 $company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][order_value]+=$row[csf("po_total_price")];
 //$company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][ex_factory]+=$row[csf("ex_factory_qnty")];
 $company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][ex_factory]+=$exfactory_data_array[$row[csf("id")]][$row[csf("country_id")]][ex_factory_qnty];
 $company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][smv]+=($job_smv_arr[$row[csf("job_no")]])*$row[csf('po_quantity')];
 if($precost_smv_arr[$row[csf("job_no")]] !="" || $job_smv_arr[$row[csf("job_no")]] !=0)
 {
	$booked_basic_qnty=($row[csf("po_quantity")]*($job_smv_arr[$row[csf("job_no")]]))/$basic_smv_arr[$row[csf("company_name")]];
	$company_buyer_array[$row[csf("company_name")]][$row[csf("buyer_name")]][booked_basic_qnty]+=$booked_basic_qnty;
	$booked_basic_qnty_tot+=$booked_basic_qnty;
 }

if($row[csf("is_confirmed")]==1)
{
$po_total_price_tot_c+=$row[csf("po_total_price")];
$quantity_tot_c+=$row[csf("po_quantity_pcs")];
$company_buyer_array_confirm[$row[csf("company_name")]][$row[csf("buyer_name")]][order_qty_pcs]+=$row[csf("po_quantity_pcs")];
$company_buyer_array_confirm[$row[csf("company_name")]][$row[csf("buyer_name")]][order_value]+=$row[csf("po_total_price")];
$company_buyer_array_confirm[$row[csf("company_name")]][$row[csf("buyer_name")]][ex_factory]+=$exfactory_data_array[$row[csf("id")]][$row[csf("country_id")]][ex_factory_qnty];
if($precost_smv_arr[$row[csf("job_no")]] !="" || $job_smv_arr[$row[csf("job_no")]] !=0)
 {
	$booked_basic_qnty_c=($row[csf("po_quantity")]*($job_smv_arr[$row[csf("job_no")]]))/$basic_smv_arr[$row[csf("company_name")]];
	$company_buyer_array_confirm[$row[csf("company_name")]][$row[csf("buyer_name")]][booked_basic_qnty]+=$booked_basic_qnty_c;
	$booked_basic_qnty_tot_c+=$booked_basic_qnty_c;
 }
}

if($row[csf("is_confirmed")]==2)
{
$po_total_price_tot_p+=$row[csf("po_total_price")];
$quantity_tot_p+=$row[csf("po_quantity_pcs")];
$company_buyer_array_projected[$row[csf("company_name")]][$row[csf("buyer_name")]][order_qty_pcs]+=$row[csf("po_quantity_pcs")];
$company_buyer_array_projected[$row[csf("company_name")]][$row[csf("buyer_name")]][order_value]+=$row[csf("po_total_price")];
$company_buyer_array_projected[$row[csf("company_name")]][$row[csf("buyer_name")]][ex_factory]+=$exfactory_data_array[$row[csf("id")]][$row[csf("country_id")]][ex_factory_qnty];
if($precost_smv_arr[$row[csf("job_no")]] !="" || $job_smv_arr[$row[csf("job_no")]] !=0)
 {
	$booked_basic_qnty_p=($row[csf("po_quantity")]*($job_smv_arr[$row[csf("job_no")]]))/$basic_smv_arr[$row[csf("company_name")]];
	$company_buyer_array_projected[$row[csf("company_name")]][$row[csf("buyer_name")]][booked_basic_qnty]+=$booked_basic_qnty_p;
 }
$booked_basic_qnty_tot_p+=$booked_basic_qnty_p;
}
}
?>
<div>
<table width="100%">

<tr>
<td align="center" id="projected_and_confirmed_order">
<table width="1320" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all" >
<thead>
<tr>
<th>
<input type="button" id="projected_and_confirmed_order_print_button" value="Print" onclick="print_report_part_by_part('projected_and_confirmed_order','#projected_and_confirmed_order_print_button')"/>
</th>
<th colspan="18">Projected and Confirmed Order - <?php  echo date('M,Y',strtotime($start_date));?> </th>
</tr>

<tr>
<th width="50">SL</th>
<th width="70">Company Name</th>
<th width="70">Buyer Name</th>
<th width="50">Avg.SMV</th>
<th width="100">Ord. Qty. (Pcs)</th>
<th width="100"> Allocated Capacity (Pcs)</th>
<th width="50">%</th>
<th width="100">Booked Basic  Qty (Pcs)</th>
<th width="50">%</th>
<th width="80">Balance Basic (Pcs)</th>
<th width="50"> %</th>
<th width="100">Ex-factory Qty. (Pcs)</th>
<th width="100">Yet to Ex-factory (Pcs)</th>
<th width="50">Ex-fac Bal. %</th>
<th width="100">Ord. Value (USD)</th>
<th width="50">Avg.Rate (USD)</th>
<th width="50">Buyer Share % </th>
<th width="50">Avg Basic Rate (USD)</th>
<th width="">Stnd. Devn.(USD)</th>
</tr> 
</thead>
<tbody>
<?php
$total_avg_sam=0;
$avg_rate=0;
$percent_total=0;
$exfactory_tot_yet=0;
$exfactory_tot_over=0;
foreach ($company_buyer_array as $key=>$value)
{
	$i=0;
	foreach($value as $buyer=> $value1)
	{
		if($zero_value==0)
		{
		if($value1[order_qty_pcs]>0 || $target_basic_qnty[$buyer] > 0 || $value1[booked_basic_qnty] > 0 || $value1[ex_factory] > 0  || $value1[order_value] > 0)
		{
$i++;
if ($i%2==0)  
$bgcolor="#E9F3FF";
else
$bgcolor="#FFFFFF";	
	?>
<tr bgcolor="<?php echo $bgcolor; ?>">
<td width="50" align="center"><?php echo $i;?></td>
<td width="70" align="center"><?php echo $company_short_name_arr[$key];?></td>
<td width="70" align="center"><?php echo $buyer_short_name_arr[$buyer]?></td>
<td width="50" align="right"><?php echo number_format($value1[smv]/$value1[order_qty_pcs],2); $total_avg_sam+=$value1[smv];?></td>
<td width="100" align="right"><?php echo number_format($value1[order_qty_pcs],0);?></td>
<td width="100" align="right" bgcolor="#CCCCCC"><?php echo number_format($target_basic_qnty[$buyer],0); $target_basic_qnty_tot+=$target_basic_qnty[$buyer];?></td>
<td width="50" align="right" bgcolor="#CCCCCC">
<?php  echo number_format(($target_basic_qnty[$buyer]/$total_target_basic_qnty)*100,2); $percent_total+=($target_basic_qnty[$buyer]/$total_target_basic_qnty)*100; ?>
</td>
<td width="100" align="right" bgcolor="#CCCCCC"><?php echo number_format($value1[booked_basic_qnty],0);?></td>
<td width="50" align="right" bgcolor="#CCCCCC"><?php echo number_format(($value1[booked_basic_qnty]/$target_basic_qnty[$buyer])*100,2); ?></td>
<td width="80" align="right" bgcolor="#CCCCCC"><?php echo number_format($target_basic_qnty[$buyer]-$value1[booked_basic_qnty],0);?></td>
<td width="50" align="right" bgcolor="#CCCCCC"><?php echo number_format((($target_basic_qnty[$buyer]-$value1[booked_basic_qnty])/$target_basic_qnty[$buyer])*100,2);?></td> 
<td width="100" align="right"><?php echo number_format($value1[ex_factory],0);?></td> 
<td width="100" align="right">
<?php 
echo number_format($value1[order_qty_pcs]-$value1[ex_factory],0);
if($value1[order_qty_pcs]-$value1[ex_factory]>=0)
{
$exfactory_tot_yet+=($value1[order_qty_pcs]-$value1[ex_factory]);
}
else
{
$exfactory_tot_over+=($value1[order_qty_pcs]-$value1[ex_factory]);
}
?>
</td>
<td width="50" align="right"><?php echo number_format((($value1[order_qty_pcs]-$value1[ex_factory])/$quantity_tot)*100,2);?></td> 
<td width="100" align="right"><?php echo  number_format($value1[order_value],2);?></td>
<td width="50" align="right"><?php echo number_format($value1[order_value]/$value1[order_qty_pcs],2);?></td>
<td width="50" align="right"><?php echo number_format(($value1[order_value]/$po_total_price_tot)*100 ,2);  ?></td>
<td width="50" align="right" title="<?php echo "Asking Avg Rate: ".$asking_avg_rate_arr[$key]; $avg_rate=$asking_avg_rate_arr[$key];?>"><?php echo number_format(($value1[order_value]/$value1[booked_basic_qnty]),2); ?></td>
<td width="" align="right" title="<?php echo "Avg Basic Rate-Asking Avg Rate";?>"><?php echo number_format((($value1[order_value]/$value1[booked_basic_qnty])-$asking_avg_rate_arr[$key]),2); ?></td>
</tr>
<?php
		}
		}
		else
		{
            $i++;
			if ($i%2==0)  
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";	
				?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
			<td width="50" align="center"><?php echo $i;?></td>
			<td width="70" align="center"><?php echo $company_short_name_arr[$key];?></td>
			<td width="70" align="center"><?php echo $buyer_short_name_arr[$buyer]?></td>
			<td width="50" align="right"><?php echo number_format($value1[smv]/$value1[order_qty_pcs],2); $total_avg_sam+=$value1[smv];?></td>
			<td width="100" align="right"><?php echo number_format($value1[order_qty_pcs],0);?></td>
			<td width="100" align="right" bgcolor="#CCCCCC"><?php echo number_format($target_basic_qnty[$buyer],0); $target_basic_qnty_tot+=$target_basic_qnty[$buyer];?></td>
			<td width="50" align="right" bgcolor="#CCCCCC"><?php  echo number_format(($target_basic_qnty[$buyer]/$total_target_basic_qnty)*100,2); $percent_total+=($target_basic_qnty[$buyer]/$total_target_basic_qnty)*100; ?></td>
			<td width="100" align="right" bgcolor="#CCCCCC"><?php echo number_format($value1[booked_basic_qnty],0);?></td>
			<td width="50" align="right" bgcolor="#CCCCCC"><?php echo number_format(($value1[booked_basic_qnty]/$target_basic_qnty[$buyer])*100,2); ?></td>
			<td width="80" align="right" bgcolor="#CCCCCC"><?php echo number_format($target_basic_qnty[$buyer]-$value1[booked_basic_qnty],0);?></td>
			<td width="50" align="right" bgcolor="#CCCCCC"><?php echo number_format((($target_basic_qnty[$buyer]-$value1[booked_basic_qnty])/$target_basic_qnty[$buyer])*100,2);?></td> 
			<td width="100" align="right"><?php echo number_format($value1[ex_factory],0);?></td> 
			<td width="100" align="right">
			<?php 
			echo number_format($value1[order_qty_pcs]-$value1[ex_factory],0);
			if($value1[order_qty_pcs]-$value1[ex_factory]>=0)
			{
			$exfactory_tot_yet+=($value1[order_qty_pcs]-$value1[ex_factory]);
			}
			else
			{
			$exfactory_tot_over+=($value1[order_qty_pcs]-$value1[ex_factory]);
			}
			?>
            </td>
			<td width="50" align="right"><?php echo number_format((($value1[order_qty_pcs]-$value1[ex_factory])/$quantity_tot)*100,2);?></td> 
			<td width="100" align="right"><?php echo  number_format($value1[order_value],2);?></td>
			<td width="50" align="right"><?php echo number_format($value1[order_value]/$value1[order_qty_pcs],2);?></td>
			<td width="50" align="right"><?php echo number_format(($value1[order_value]/$po_total_price_tot)*100 ,2);  ?></td>
			<td width="50" align="right" title="<?php echo "Asking Avg Rate: ".$asking_avg_rate_arr[$key]; $avg_rate=$asking_avg_rate_arr[$key];?>"><?php echo number_format(($value1[order_value]/$value1[booked_basic_qnty]),2); ?></td>
			<td width="" align="right" title="<?php echo "Avg Basic Rate-Asking Avg Rate";?>"><?php echo number_format((($value1[order_value]/$value1[booked_basic_qnty])-$asking_avg_rate_arr[$key]),2); ?></td>
			</tr>
            
            <?php
		}
	}
}
?>
</tbody>
<tfoot>
<tr>
<th width="50"></th>
<th width="70"></th>
<th width="70"></th>
<th width="50"><?php echo number_format($total_avg_sam/$quantity_tot,2); ?></th>
<th width="100"><?php echo number_format($quantity_tot,0); ?></th>

<th width="100"><?php echo number_format($target_basic_qnty_tot,0); ?></th>
<th width="50"><?php echo number_format($percent_total,2); ?></th>
<th width="100"><?php echo number_format($booked_basic_qnty_tot,0); ?></th>
<th width="50"><?php echo number_format($booked_basic_qnty_tot/$target_basic_qnty_tot*100,2); ?></th>
<th width="80"><?php echo number_format($target_basic_qnty_tot-$booked_basic_qnty_tot,0); ?></th>
<th width="50"><?php echo number_format((($target_basic_qnty_tot-$booked_basic_qnty_tot)/$target_basic_qnty_tot)*100,2); ?></th>
<th width="100"><?php echo number_format($exfactory_tot,0); ?></th> 
<th width="100"><?php echo number_format($quantity_tot-$exfactory_tot,0); ?></th>
<th width="50"><?php echo number_format((($quantity_tot-$exfactory_tot)/$quantity_tot)*100,2); ?></th> 
<th width="100"><?php  echo number_format($po_total_price_tot,2); ?></th>
<th width="50"><?php  echo number_format($po_total_price_tot/$quantity_tot,2); ?> </th>
<th width="50"><?php  //echo number_format($total_target_basic_qnty,2); ?></th>

<th width="50" title="<?php echo "Asking Avg Rate: ".$avg_rate;?>"><?php echo number_format($po_total_price_tot/$booked_basic_qnty_tot,2); ?></th>
<th width="" title="<?php echo "Avg Basic Rate-Asking Avg Rate";?>"><?php echo number_format(($po_total_price_tot/$booked_basic_qnty_tot)-$avg_rate,2); ?></th>
</tr>
<tr>
<th width="50"></th>
<th width="70"></th>
<th width="70"></th>
<th width="50">&nbsp;</th>
<th width="100">&nbsp;</th>

<th width="100">&nbsp;</th>
<th width="50">&nbsp;</th>
<th width="100">&nbsp;</th>
<th width="50">&nbsp;</th>
<th width="80">&nbsp;</th>

<th width="100" colspan="2">Short Shipment</th> 
<th width="100"><?php echo number_format($exfactory_tot_yet,0); ?></th>
<th width="50">&nbsp;</th> 
<th width="100">&nbsp;</th>
<th width="50">&nbsp; </th>
<th width="50">&nbsp;</th>

<th width="50" >&nbsp;</th>
<th width="" >&nbsp;</th>
</tr>
<tr>
<th width="50"></th>
<th width="70"></th>
<th width="70"></th>
<th width="50">&nbsp;</th>
<th width="100">&nbsp;</th>

<th width="100">&nbsp;</th>
<th width="50">&nbsp;</th>
<th width="100">&nbsp;</th>
<th width="50">&nbsp;</th>
<th width="80">&nbsp;</th>

<th width="100" colspan="2">Over Shipment</th> 
<th width="100"><?php echo number_format(ltrim($exfactory_tot_over,'-'),0); ?></th>
<th width="50">&nbsp;</th> 
<th width="100">&nbsp;</th>
<th width="50">&nbsp; </th>
<th width="50">&nbsp;</th>

<th width="50" >&nbsp;</th>
<th width="" >&nbsp;</th>
</tr>


<tr>
<td colspan="19">&nbsp;</td>
</tr>
<tr>
<td width="" colspan="2" align="right"></td>

<td width="" colspan="2"  align="center"><strong>Ord. Qty. (Pcs)</strong></td>

<td width="" colspan="2" align="center"><strong> Eqv. Basic Qty.</strong></td>
<td width="" colspan="2" align="center"><strong>Ord. Value (USD)</strong></td>
<td width="" colspan="2" align="center"><strong>Ord. Avg.Rate (USD)</strong></td>
<td width="" colspan="2" align="center"><strong>Basic Avg.Rate (USD)</strong></td>



<td width="" colspan="6" align="right"> <strong>Company Avg. Rate (USD)</strong></td>

<td width="" align="right"><?php  echo number_format($po_total_price_tot/$booked_basic_qnty_tot,2); ?></td>
</tr>
<tr>
<td width="" colspan="2" align="right"> <strong>Confirmed Order</strong></td>

<td width="" colspan="2"  align="right"><?php echo number_format($quantity_tot_c,0); ?></td>

<td width="" colspan="2" align="right"><?php echo number_format($booked_basic_qnty_tot_c,0); ?></td>
<td width="" colspan="2" align="right"><?php  echo number_format($po_total_price_tot_c,2); ?></td>
<td width="" colspan="2" align="right"><?php  echo number_format($po_total_price_tot_c/$quantity_tot_c,2); ?> </td>
<td width="" colspan="2" align="right"><?php  echo number_format($po_total_price_tot_c/$booked_basic_qnty_tot_c,2); ?> </td>


<td width="50" colspan="6" align="right"> Company Asking Avg. Rate (USD)</td>
<td width="" align="right"><?php  echo number_format($avg_rate,2); ?></td>
</tr>
<tr>
<td width="" colspan="2" align="right"><strong>Projected Order</strong></td>

<td width="" colspan="2"  align="right"> <?php echo number_format($quantity_tot_p,0); ?></td>

<td width="" colspan="2" align="right"><?php echo number_format($booked_basic_qnty_tot_p,0); ?></td>
<td width="" colspan="2" align="right"><?php  echo number_format($po_total_price_tot_p,2); ?></td>
<td width="" colspan="2" align="right"><?php  echo number_format($po_total_price_tot_p/$quantity_tot_p,2); ?> </td>
<td width="" colspan="2" align="right"><?php  echo number_format($po_total_price_tot_p/$booked_basic_qnty_tot_p,2); ?> </td>

<td width="" colspan="6" align="right"> Company Stnd. Devn. (USD)</td>
<td width="" align="right"><?php echo number_format(($po_total_price_tot/$booked_basic_qnty_tot)-$avg_rate,2); ?></td>
</tr>
</tfoot>
</table>
</td>
</tr>
</table> 

<table width="100%">
<tr>
<td width="50%" id="confirmed_order">
<table width="650" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all">
<thead>
<tr>
<th><input type="button" id="confirmed_order_print_button" value="Print" onclick="print_report_part_by_part('confirmed_order','#confirmed_order_print_button')"/></th>
<th colspan="8">Confirmed Order - <?php  echo date('M,Y',strtotime($start_date));?></th>
</tr>
<tr>
<th width="50">SL</th>
<th width="70">Company Name</th>
<th width="70">Buyer Name</th>
<th width="100">Ord. Qty. (Pcs)</th>
<th width="100">Eqv. Basic Qty. (Pcs)</th>
<th width="100">Ord. Value (USD)</th>
<th width="50">Buyer Share %</th>
<th width="50">Ord. Avg.Rate (USD)</th>
<th width="">Basic. Avg.Rate (USD)</th>
</tr>
</thead>
<tbody>
<?php
$chart_cap="";
$chart_data="";
$i=0;
$pie_chart="[";
$chart="[
		 {
		key: 'Cumulative Return',
		   values: [
			 ";
foreach ($company_buyer_array_confirm as $key=>$value)
{
	foreach($value as $buyer=> $value1)
	{
		if($zero_value==0)
		{
		if($value1[order_qty_pcs] > 0 || $value1[booked_basic_qnty] > 0 || $value1[order_value] >0)
		{
		$i++;
		if ($i%2==0)  
		$bgcolor="#E9F3FF";
		else
		$bgcolor="#FFFFFF";	
		$chart.="{
			'label': '".$buyer_short_name_arr[$buyer]."',
            'value': ".$value1[order_qty_pcs]."},
			";
			$pie_chart.="{
      'key': '".$buyer_short_name_arr[$buyer]."',
      'y': ".$value1[order_qty_pcs]."},
    ";
		?>
		<tr bgcolor="<?php echo $bgcolor; ?>">
		<td width="50" align="center"><?php echo $i;?></td>
		<td width="70" align="center"><?php echo $company_short_name_arr[$key];?></td>
		<td width="70" align="center"><?php echo $buyer_short_name_arr[$buyer];?></td>
		<td width="100" align="right"><?php echo number_format($value1[order_qty_pcs],0);?></td>
		
		<td width="100" align="right"><?php echo number_format($value1[booked_basic_qnty],0);?></td>
		
		
		<td width="100" align="right"><?php echo  number_format($value1[order_value],2);?></td>
		<td width="50" align="right"><?php echo number_format(($value1[order_value]/$po_total_price_tot_c)*100 ,2);  ?></td>
        <td width="50" align="right"><?php echo number_format($value1[order_value]/$value1[order_qty_pcs],2);?></td>
        <td width="" align="right"><?php echo number_format($value1[order_value]/$value1[booked_basic_qnty],2);?></td>
		</tr>
		<?php
		}
		}
		else
		{
		$i++;
		if ($i%2==0)  
		$bgcolor="#E9F3FF";
		else
		$bgcolor="#FFFFFF";	
		$chart.="{
			'label': '".$buyer_short_name_arr[$buyer]."',
            'value': ".$value1[order_qty_pcs]."},
			";
			$pie_chart.="{
      'key': '".$buyer_short_name_arr[$buyer]."',
      'y': ".$value1[order_qty_pcs]."},";
    
		?>
		<tr bgcolor="<?php echo $bgcolor; ?>">
		<td width="50" align="center"><?php echo $i;?></td>
		<td width="70" align="center"><?php echo $company_short_name_arr[$key];?></td>
		<td width="70" align="center"><?php echo $buyer_short_name_arr[$buyer];?></td>
		<td width="100" align="right"><?php echo number_format($value1[order_qty_pcs],0);?></td>
		
		<td width="100" align="right"><?php echo number_format($value1[booked_basic_qnty],0);?></td>
		
		
		<td width="100" align="right"><?php echo  number_format($value1[order_value],2);?></td>
		<td width="50" align="right"><?php echo number_format(($value1[order_value]/$po_total_price_tot_c)*100 ,2);  ?></td>
        <td width="50" align="right"><?php echo number_format($value1[order_value]/$value1[order_qty_pcs],2);?></td>
        <td width="" align="right"><?php echo number_format($value1[order_value]/$value1[booked_basic_qnty],2);?></td>
		</tr>
            
            <?php
		}
	}
}
$chart.="]
		 }
		 ];
";
$pie_chart.="];";
//echo $pie_chart;
?>
</tbody>
<tfoot>
<th width="50"></th>
<th width="70"></th>
<th width="70"></th>
<th width="100"><?php echo number_format($quantity_tot_c,0); ?></th>
<th width="100"><?php echo number_format($booked_basic_qnty_tot_c,0); ?></th>
<th width="100"><?php  echo number_format($po_total_price_tot_c,2); ?></th>
<th width="50"><?php  //echo number_format(($po_total_price_tot/$po_total_price_tot)*100,2); ?></th>
<th width="50"><?php  echo number_format($po_total_price_tot_c/$quantity_tot_c,2); ?> </th>
<th width=""><?php  echo number_format($po_total_price_tot_c/$booked_basic_qnty_tot_c,2); ?> </th>
</tfoot>
</table>
</td>
<td width="50%" valign="top">
<table width="650" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all" style="margin-left:10px">
<thead>
<tr>
<th>Buyer wise Confirmed Order Chart</th>
</tr>
<tr>
<th></th>
</tr>
</thead>
<tbody>
<tr>
<td align="center">
<!--<iframe align="middle" src="chart/examples/pieChart.php?cardata=<?php //echo $pie_chart;  ?>" width="650" height="400"></iframe>-->
</td>
</tr>
</tbody>
<tfoot>

</tfoot>

</table>
</td>
</tr>
</table>


<table width="100%">
<tr>
<td width="50%" id="projected_order">
<table width="650" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all">
<thead>
<tr>
<th><input type="button" id="projected_order_print_button" value="Print" onclick="print_report_part_by_part('projected_order','#projected_order_print_button')"/></th>
<th colspan="8">Projected Order - <?php  echo date('M,Y',strtotime($start_date));?></th>
</tr>
<tr>
<th width="50">SL</th>
<th width="70">Company Name</th>
<th width="70">Buyer Name</th>
<th width="100">Ord. Qty. (Pcs)</th>
<th width="100">Eqv. Basic Qty. (Pcs)</th>

<th width="100">Ord. Value (USD)</th>
<th width="50">Buyer Share %</th>
<th width="50"> Ord. Avg.Rate (USD)</th>
<th width=""> Basic. Avg.Rate (USD)</th>
</tr>
</thead>
<tbody>
<?php
$i=0;
$pie_chart1="[";

$chart1="[
		 {
		key: 'Cumulative Return',
		   values: [
			 ";
foreach ($company_buyer_array_projected as $key=>$value)
{
	foreach($value as $buyer=> $value1)
	{
		if($zero_value==0)
		{
		if($value1[order_qty_pcs] > 0 || $value1[booked_basic_qnty] > 0 || $value1[order_value] >0)
		{
		$i++;
		if ($i%2==0)  
		$bgcolor="#E9F3FF";
		else
		$bgcolor="#FFFFFF";	
		$chart1.="{
			'label': '".$buyer_short_name_arr[$buyer]."',
            'value': ".$value1[order_qty_pcs]."},
			";
			$pie_chart1.="{
      'key': '".$buyer_short_name_arr[$buyer]."',
      'y': ".$value1[order_qty_pcs]."},";
		?>
		<tr bgcolor="<?php echo $bgcolor; ?>">
		<td width="50" align="center"><?php echo $i;?></td>
		<td width="70" align="center"><?php echo $company_short_name_arr[$key];?></td>
		<td width="70" align="center"><?php echo $buyer_short_name_arr[$buyer];?></td>
		<td width="100" align="right"><?php echo number_format($value1[order_qty_pcs],0);?></td>
		<td width="100" align="right"><?php echo number_format($value1[booked_basic_qnty],0);?></td>
		<td width="100" align="right"><?php echo number_format($value1[order_value],2);?></td>
		<td width="50" align="right"><?php echo number_format(($value1[order_value]/$po_total_price_tot_p)*100 ,2);  ?></td>
        <td width="50" align="right"><?php echo number_format($value1[order_value]/$value1[order_qty_pcs],2);?></td>
        <td width="" align="right"><?php echo number_format($value1[order_value]/$value1[booked_basic_qnty],2);?></td>
		</tr>
		<?php
		}
		}
		else
		{
			$i++;
		if ($i%2==0)  
		$bgcolor="#E9F3FF";
		else
		$bgcolor="#FFFFFF";	
		$chart1.="{
			'label': '".$buyer_short_name_arr[$buyer]."',
            'value': ".$value1[order_qty_pcs]."},
			"
		?>
		<tr bgcolor="<?php echo $bgcolor; ?>">
		<td width="50" align="center"><?php echo $i;?></td>
		<td width="70" align="center"><?php echo $company_short_name_arr[$key];?></td>
		<td width="70" align="center"><?php echo $buyer_short_name_arr[$buyer];?></td>
		<td width="100" align="right"><?php echo number_format($value1[order_qty_pcs],0);?></td>
		
		<td width="100" align="right"><?php echo number_format($value1[booked_basic_qnty],0);?></td>
		
		
		<td width="100" align="right"><?php echo number_format($value1[order_value],2);?></td>
		<td width="50" align="right"><?php echo number_format(($value1[order_value]/$po_total_price_tot_p)*100 ,2);  ?></td>
        <td width="50" align="right"><?php echo number_format($value1[order_value]/$value1[order_qty_pcs],2);?></td>
        <td width="" align="right"><?php echo number_format($value1[order_value]/$value1[booked_basic_qnty],2);?></td>
		</tr>
            <?php
		}
	}
}
$chart1.="]
		 }
		 ];
";
$pie_chart1.="];";

?>
</tbody>
<tfoot>
<th width="50"></th>
<th width="70"></th>
<th width="70"></th>
<th width="100"><?php echo number_format($quantity_tot_p,0); ?></th>
<th width="100"><?php echo number_format($booked_basic_qnty_tot_p,0); ?></th>

<th width="100"><?php  echo number_format($po_total_price_tot_p,2); ?></th>
<th width="50"><?php  //echo number_format(($po_total_price_tot/$po_total_price_tot)*100,2); ?></th>
<th width=""><?php  echo number_format($po_total_price_tot_p/$quantity_tot_p,2); ?> </th>
<th width=""><?php  echo number_format($po_total_price_tot_p/$booked_basic_qnty_tot_p,2); ?> </th>
</tfoot>

</table>
</td>
<td width="50%" valign="top">
<table width="650" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all" style=" margin-left:10px">
<thead>
<tr>
<th>Buyer wise Projected Order Chart</th>
</tr>

</thead>
<tbody>
<tr>
<td>
<!--<iframe src="chart/examples/pieChart.php?cardata=<?php //echo $pie_chart1;  ?>" width="650" height="450"></iframe>-->
</td>
</tr>
</tbody>
<tfoot>

</tfoot>

</table>
</td>
</tr>
</table>
<br/>
<table>
<tr>
<td bgcolor="orange" height="15" width="30"></td>
<td>Maximum 10 Days Remaing To Ship</td>
<td bgcolor="green" height="15" width="30">&nbsp;</td>
<td>On Time Shipment</td>
<td bgcolor="#2A9FFF" height="15" width="30"></td>
<td>Delay shipment</td>
<td bgcolor="red" height="15" width="30"></td>
<td>Shipment Date Over & Pending</td>


</tr>
</table>

<h3 align="left" id="accordion_h4" class="accordion_h" onClick="accordion_menu( this.id,'content_report_panel', '')"> -Report Panel </h3>
            <div id="content_report_panel"> 
                <table width="3330" id="table_header_1" border="1" class="rpt_table" rules="all">
                    <thead>
                        <tr>
                            <th width="50">SL</th>
                            <th width="65" >Company</th>
                            <th width="60">Job No</th>
                            <th  width="50">Buyer</th>
                            <th  width="150">Order No</th>
                            <th  width="100">Pord. Dept Code</th>
                            <th width="30">Img</th>
                            <th width="150">Item</th>
                            <th width="90">Style Ref</th>
                            <th width="150">Style Des</th>
                            <th width="100">Country</th>
                            <th width="80">Ship Date</th>
                            <th  width="100">SMV</th>
                            <th  width="100">Total SMV</th>
                            <th width="90">Order Qnty</th>
                            <th width="30">Uom</th>
                            <th width="90">Order Qnty(Pcs)</th>
                            <th width="100">Eqv. Basic Qty. (Pcs)</th>
                            <th  width="50">Per Unit Price</th>
                            <th width="100">Order Value</th>
                            <th width="100">Commission</th>
                            <th width="100">Net Order Value</th>
                            <th width="90">Ex-Fac Qnty (Pcs) </th>
                            <th  width="90">Ex-factory Bal. (Pcs)</th>
                            <th  width="90">Ex-factory Over (Pcs)</th>
                            <th width="120">Ex-factory Bal. Value</th>
                            <th width="120">Ex-factory Over. Value</th>
                            <th width="60">Order Status</th>
                            <th width="70">Prod. Catg</th>
                            <th width="80">PO Rec. Date</th>
                            <th  width="50">Days in Hand</th>
                            <th width="100" >Shipping Status</th>
                            <th width="150"> Team Member</th>
                            <th width="150">Team Name</th>
                            <th width="30">Id</th>
                            <th>Remarks</th>
                        </tr>
                    </thead>
                </table>
                <div style=" max-height:400px; overflow-y:scroll; width:3330px"  align="left" id="scroll_body">
                    <table width="3310" border="1" class="rpt_table" rules="all" id="table-body">
                    <?php
					
					$template_id_arr=return_library_array("select po_number_id, template_id from tna_process_mst group by po_number_id, template_id","po_number_id","template_id");
					
                    $i=1;
                    $order_qnty_pcs_tot=0;
                    $order_qntytot=0;
                    $oreder_value_tot=0;
                    $total_ex_factory_qnty=0;
                    $total_short_access_qnty=0;
                    $total_short_access_value=0;
                    $yarn_req_for_po_total=0;
                    foreach ($data_array as $row)
                    {
						$template_id=$template_id_arr[$row[csf('id')]];
						 
						if ($i%2==0)  
						$bgcolor="#E9F3FF";
						else
						$bgcolor="#FFFFFF";	
						
						if($row['is_confirmed']==2)
						{
							$color_font="#F00";
						}
						else
						{
							$color_font="#000";
						}
						$ex_factory_date=$exfactory_data_array[$row[csf('id')]][$row[csf('country_id')]][ex_factory_date];
						$date_diff_3=datediff( "d", $ex_factory_date , $row[csf('country_ship_date')]);
						$date_diff_4=datediff( "d", $ex_factory_date , $row[csf('country_ship_date')]);
						$cons=0;
						$costing_per_pcs=0;
						/*$data_array_costing_per=sql_select("select costing_per from  wo_pre_cost_mst where  job_no='".$row[csf('job_no')]."'");
						list($costing_per)=$data_array_costing_per;*/
                        $costing_per=$costing_per_arr[$row[csf('job_no')]];
						if($costing_per ==1)
						{
						  $costing_per_pcs=1*12;	
						}
						else if($costing_per==2)
						{
						 $costing_per_pcs=1*1;	
						}
						else if($costing_per==3)
						{
						 $costing_per_pcs=2*12;	
						}
						else if($costing_per==4)
						{
						 $costing_per_pcs=3*12;	
						}
						else if($costing_per==5)
						{
						 $costing_per_pcs=4*12;	
						}
						
						/*$data_array_yarn_cons=sql_select("select yarn_cons_qnty from  wo_pre_cost_sum_dtls where  job_no='".$row[csf('job_no')]."'");
						$yarn_req_for_po=0;
						foreach($data_array_yarn_cons as $row_yarn_cons)
						{
							$cons=$row_yarn_cons[csf('yarn_cons_qnty')];
							$yarn_req_for_po=($row_yarn_cons[csf('yarn_cons_qnty')]/ $costing_per_pcs)*$row[csf('po_quantity')];
						}*/
						
						//--Calculation Yarn Required-------
						//--Color Determination-------------
						//==================================
						$shipment_performance=0;
						if($row[csf('shiping_status')]==1 && $row[csf('date_diff_1')]>10 )
						{
						$color="";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row[csf('shiping_status')]==1 && ($row[csf('date_diff_1')]<=10 && $row[csf('date_diff_1')]>=0))
						{
						$color="orange";
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row[csf('shiping_status')]==1 &&  $row[csf('date_diff_1')]<0)
						{
						$color="red";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						//=====================================
						if($row[csf('shiping_status')]==2 && $row[csf('date_diff_1')]>10 )
						{
						$color="";	
						}
						if($row[csf('shiping_status')]==2 && ($row[csf('date_diff_1')]<=10 && $row[csf('date_diff_1')]>=0))
						{
						$color="orange";	
						}
						if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_1')]<0)
						{
						$color="red";	
						}
						if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_2')]>=0)
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;	
						}
						if($row[csf('shiping_status')]==2 &&  $row[csf('date_diff_2')]<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						//========================================
						if($row[csf('shiping_status')]==3 && $date_diff_3 >=0 )
						{
						$color="green";	
						}
						if($row[csf('shiping_status')]==3 &&  $date_diff_3<0)
						{
						$color="#2A9FFF";	
						}
						if($row[csf('shiping_status')]==3 && $date_diff_4>=0 )
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;
						}
						if($row[csf('shiping_status')]==3 && $date_diff_4<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="vertical-align:middle" height="25" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i; ?>" >
								<td width="50" align="center" bgcolor="<?php echo $color; ?>"> <?php echo $i; ?> </td>
								<td width="65" align="center"><?php echo $company_short_name_arr[$row[csf('company_name')]];?></td>
                                <td width="60" align="center"><?php echo $row[csf('job_no_prefix_num')];?></td>
								<td  width="50" align="center"><?php echo $buyer_short_name_arr[$row[csf('buyer_name')]];?></td>
                                <td  width="150" align="center">
                                <font style="color:<?php echo $color_font; ?>">
                                
                                	<a href='#report_details' onclick="progress_comment_popup('<?php echo $row[csf('job_no')]; ?>','<?php echo $row[csf('id')]; ?>','<?php echo $template_id; ?>','<?php echo $tna_process_type; ?>');"><?php echo $row[csf('po_number')];  ?></a>
                                    
                                </font>
                                </td>
                                 <td  width="100" align="center"><font style="color:<?php echo $color_font; ?>"><?php echo $row[csf('product_code')];?></font></td>
                                <td width="30" onclick="openmypage_image('requires/capacity_and_order_booking_status_controller.php?action=show_image&job_no=<?php echo $row[csf("job_no")] ?>','Image View')"><img  src='../../../<?php echo $imge_arr[$row[csf('job_no')]]; ?>' height='25' width='30' /></td>
                                <td width="150" align="center">
								<?php
								$gmts_item_name="";
								$gmts_item_id=explode(',',$row[csf('gmts_item_id')]);
								for($j=0; $j<count($gmts_item_id); $j++)
								{
								$gmts_item_name.= $garments_item[$gmts_item_id[$j]].",";
								}
								?>
                                <p> <?php echo rtrim($gmts_item_name,","); ?> </p>
								</td>
                                <td width="90" align="center"><p><?php echo $row[csf('style_ref_no')];?></p></td>
                                <td width="150" align="center"><p><?php echo $row[csf('style_description')];?></p></td>
                                <td width="100" align="center"><p><?php echo $country_name_arr[$row[csf('country_id')]];?></p></td>
                                <td width="80" align="center"><?php echo change_date_format($row[csf('country_ship_date')],'dd-mm-yyyy','-');?></td>
                                <td  width="100" align="right">
								<?php   
								//echo number_format($job_smv_arr[$row['job_no']],2); 
								echo number_format($job_smv_arr[$row[csf('job_no')]],2); 
								?>
                                </td>
                                
								<td  width="100" align="right"><?php  $smv= ($job_smv_arr[$row[csf('job_no')]])*$row[csf('po_quantity')]; $smv_tot+=$smv; echo number_format($smv,2); ?></td>
								<td width="90" align="right">
								<?php 
								echo number_format( $row[csf('po_quantity')],0);
								$order_qntytot=$order_qntytot+$row[csf('po_quantity')];
								$gorder_qntytot=$gorder_qntytot+$row[csf('po_quantity')];
								?>
								</td>
								<td width="30" align="center"><?php echo $unit_of_measurement[$row[csf('order_uom')]];?></td>
								<td width="90" align="right">
								<?php 
								echo number_format(($row[csf('po_quantity')]*$row[csf('total_set_qnty')]),0);  
								$order_qnty_pcs_tot=$order_qnty_pcs_tot+($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
								$gorder_qnty_pcs_tot=$gorder_qnty_pcs_tot+($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
								?>
                                </td>
								<td width="100" align="right" title="<?php echo "Basic SMV:".$basic_smv_arr[$row[csf("company_name")]];?>">
								<?php 
								//$basic_qnty_pcs= (($job_smv_arr[$row['job_no']]*100/$buy_sew_effi_percent_arr[$row[csf("buyer_name")]])*$row['po_quantity_pcs'])/$basic_smv_arr[$row[csf("company_name")]];
								$basic_qnty_pcs= (($job_smv_arr[$row[csf('job_no')]])*$row[csf('po_quantity')])/$basic_smv_arr[$row[csf("company_name")]];
								//$basic_qnty_pcs= ((($job_smv_arr[$row['job_no']]*$row['po_quantity_pcs'])/$basic_smv_arr[$row[csf("company_name")]])*$buy_sew_effi_percent_arr[$row[csf("buyer_name")]])/100;
								

								$basic_qnty_pcs_tot+=$basic_qnty_pcs;
								echo number_format($basic_qnty_pcs,0);
								
								?>
                                </td>
								<td  width="50" align="right"><?php echo number_format($row[csf('unit_price')],2);?></td>
								<td width="100" align="right">
								<?php 
								echo number_format($row[csf('po_total_price')],2);
								$oreder_value_tot=$oreder_value_tot+$row[csf('po_total_price')];
								$goreder_value_tot=$goreder_value_tot+$row[csf('po_total_price')];
								?>
                                </td>
								<td width="100"  align="right">
								<?php 
								$commission=($row[csf('po_quantity')]/$costing_per_pcs)*$commission_for_shipment_schedule_arr[$row[csf('job_no')]]; $commission_tot+=$commission; echo number_format($commission,2); 
								?>
                                </td>
                                <td width="100" align="right"><?php $net_order_value=$row[csf('po_total_price')]-$commission;$net_order_value_tot+=$net_order_value; echo number_format ($net_order_value,2); ?></td>
								<td width="90" align="right">
								<?php 
								$ex_factory_qnty=$exfactory_data_array[$row[csf("id")]][$row[csf("country_id")]][ex_factory_qnty]; 
								echo  number_format( $ex_factory_qnty,0); 
								$total_ex_factory_qnty=$total_ex_factory_qnty+$ex_factory_qnty ;
								$gtotal_ex_factory_qnty=$gtotal_ex_factory_qnty+$ex_factory_qnty ;;
								if ($shipment_performance==0)
								{
								$po_qnty['yet']+=($row[csf('po_quantity')]*$row[csf('total_set_qnty')]);
								$po_value['yet']+=100;
								}
								else if ($shipment_performance==1)
								{
								$po_qnty['ontime']+=$ex_factory_qnty;
								$po_value['ontime']+=((100*$ex_factory_qnty)/($row[csf('po_quantity')]*$row[csf('total_set_qnty')]));
								$po_qnty['yet']+=(($row[csf('po_quantity')]*$row[csf('total_set_qnty')])-$ex_factory_qnty);
								}
								else if ($shipment_performance==2)
								{
								$po_qnty['after']+=$ex_factory_qnty;
								$po_value['after']+=((100*$ex_factory_qnty)/($row[csf('po_quantity')]*$row[csf('total_set_qnty')]));
								$po_qnty['yet']+=(($row[csf('po_quantity')]*$row[csf('total_set_qnty')])-$ex_factory_qnty);
								}
								?> 
								</td>
								<td  width="90" align="right">
								<?php 
								$short_access_qnty=(($row[csf('po_quantity')]*$row[csf('total_set_qnty')])-$ex_factory_qnty); 
								if($short_access_qnty>=0){
								echo number_format($short_access_qnty,0);
								$total_short_access_qnty=$total_short_access_qnty+$short_access_qnty;
								$gtotal_short_access_qnty=$gtotal_short_access_qnty+$short_access_qnty;
								}
								?>
                                </td>
                                <td  width="90" align="right">
								<?php 
								//$short_access_qnty=(($row['po_quantity']*$row['total_set_qnty'])-$ex_factory_qnty); 
								if($short_access_qnty<0){
								echo number_format(ltrim($short_access_qnty,'-'),0);
								$total_over_access_qnty=$total_over_access_qnty+$short_access_qnty;
								//$gtotal_short_access_qnty=$gtotal_short_access_qnty+$short_access_qnty;
								}
								?>
                                </td>
								<td width="120" align="right">
								<?php 
								if($short_access_qnty>=0){
								$short_access_value=$short_access_qnty*$row[csf('unit_price')];
								echo  number_format($short_access_value,2);
								$total_short_access_value=$total_short_access_value+$short_access_value;
								$gtotal_short_access_value=$gtotal_short_access_value+$short_access_value;
								}
								?>
                                </td>
                                <td width="120" align="right">
								<?php 
								if($short_access_qnty<0){
								$short_over_value=$short_access_qnty*$row[csf('unit_price')];
								echo  number_format(ltrim($short_over_value,'-'),2);
								$total_over_access_value=$total_over_access_value+$short_over_value;
								//$gtotal_short_access_value=$gtotal_short_access_value+$short_access_value;
								}
								?>
                                </td>
                                <td width="60" align="center"><?php echo  $order_status[$row[csf('is_confirmed')]];?></td>
								<td width="70" align="center"><?php echo $product_category[$row[csf('product_category')]];?></td>
                                <td width="80" align="center"><?php echo change_date_format($row[csf('po_received_date')],'dd-mm-yyyy','-');?></td>
								<td  width="50" align="center" bgcolor="<?php echo $color; ?>"> 
								<?php
								if($row[csf('shiping_status')]==1 || $row[csf('shiping_status')]==2)
								{
								echo $row[csf('date_diff_1')];
								}
								if($row[csf('shiping_status')]==3)
								{
								echo $date_diff_3;
								}
								?>
								</td>
								<td width="100" align="center"><?php echo $shipment_status[$row[csf('shiping_status')]]; ?></td>
								<td width="150" align="center"><?php echo $company_team_member_name_arr[$row[csf('dealing_marchant')]];?></td>
								<td width="150" align="center"><?php echo $company_team_name_arr[$row[csf('team_leader')]];?></td>
								<td width="30"><?php echo $row['id']; ?></td>
								<td><?php echo $row[csf('details_remarks')]; ?></td>
							</tr>
                    <?php
                    $i++;
					}
                    ?>
                    </table>
                </div>
                <table width="3330" id="report_table_footer" border="1" class="rpt_table" rules="all">
                    <tfoot>
                        <tr>
                            <th width="50"></th>
                            <th width="65" ></th>
                            <th width="60"></th>
                            <th  width="50"></th>
                            <th  width="150"></th>
                            <th  width="100"></th>
                            <th width="30"></th>
                            <th width="150"></th>
                            <th width="90"></th>
                            <th width="150"></th>
                            <th width="100"></th>
                            <th width="80"></th>
                            <th  width="100" id=""></th>  
                            <th  width="100" id="value_smv_tot"><?php echo number_format($smv_tot,2); ?></th>
                           <th width="90" id="total_order_qnty"><?php echo number_format($order_qntytot,0); ?></th>
                            <th width="30"></th>
                            
                            <th width="90" id="total_order_qnty_pcs"><?php echo number_format($order_qnty_pcs_tot,0); ?></th>
                            <th width="100" id="value_yarn_req_tot"><?php echo number_format($basic_qnty_pcs_tot,2); ?></th>
                             <th  width="50"></th>
                             <th width="100" id="value_total_order_value"><?php echo number_format($oreder_value_tot,2); ?></th>
                            <th width="100" id="value_total_commission"><?php echo number_format($commission_tot,2); ?></th>
                            <th width="100" id="value_total_net_order_value"><?php echo number_format($net_order_value_tot,2); ?></th>
                            <th width="90" id="total_ex_factory_qnty"> <?php echo number_format($total_ex_factory_qnty,0); ?></th>
                            <th  width="90" id="total_short_access_qnty"><?php echo number_format($total_short_access_qnty,0); ?></th>
                             <th  width="90" id="total_over_access_qnty"><?php echo number_format(ltrim($total_over_access_qnty,'-'),0); ?></th>
                            <th width="120" id="value_total_short_access_value"><?php echo number_format($total_short_access_value,2); ?></th>
                            <th width="120" id="value_total_over_access_value"><?php echo number_format(ltrim($total_over_access_value,'-'),2); ?></th>
                            <th width="60"></th>
                            <th width="70"></th>
                            <th width="80"></th>
                            <th  width="50"></th>
                            <th width="100" ></th>
                            <th width="150"> </th>
                            <th width="150"></th>
                            <th width="30"></th>
                            <th></th>
                        </tr>
                       
                    </tfoot>
                </table>
                <div id="shipment_performance">
                    <fieldset>
                        <table width="600" border="1" cellpadding="0" cellspacing="1" class="rpt_table" rules="all" >
                        <thead>
                        <tr>
                        <th colspan="4"> <font size="4">Shipment Performance</font></th>
                        </tr>
                        <tr>
                        <th>Particulars</th><th>No of PO</th><th>PO Qnty</th><th> %</th>
                        </tr>
                        </thead>
                        <tr bgcolor="#E9F3FF">
                        <td>On Time Shipment</td><td><?php echo $number_of_order['ontime']; ?></td><td align="right"><?php echo number_format($po_qnty['ontime'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['ontime'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        <tr bgcolor="#FFFFFF">
                        <td> Delivery After Shipment Date</td><td><?php echo $number_of_order['after']; ?></td><td align="right"><?php echo number_format($po_qnty['after'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['after'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        <tr bgcolor="#E9F3FF">
                        <td>Yet To Shipment </td><td><?php echo $number_of_order['yet']; ?></td><td align="right"><?php echo number_format($po_qnty['yet'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['yet'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        
                        <tr bgcolor="#E9F3FF">
                        <td> </td><td></td><td align="right"><?php echo number_format($po_qnty['yet']+$po_qnty['ontime']+$po_qnty['after'],0); ?></td><td align="right"><?php echo number_format(((100*$po_qnty['yet'])/$order_qnty_pcs_tot)+((100*$po_qnty['after'])/$order_qnty_pcs_tot)+((100*$po_qnty['ontime'])/$order_qnty_pcs_tot),2); ?></td>
                        </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
     </div>
        
<?php
}

if($action=="show_image")
{
	echo load_html_head_contents("Set Entry","../../../../", 1, 1, $unicode);
    extract($_REQUEST);
	//echo "select image_location  from common_photo_library  where master_tble_id='$job_no' and form_name='knit_order_entry' and is_deleted=0 and file_type=1";
	$data_array=sql_select("select image_location  from common_photo_library  where master_tble_id='$job_no' and form_name='knit_order_entry' and is_deleted=0 and file_type=1");
	
	?>
    <table>
    <tr>
    <?php
    foreach ($data_array as $row)
	{ 
	?>
    <td><img src='../../../../<?php echo $row[csf('image_location')]; ?>' height='250' width='300' /></td>
    <?php
	}
	?>
    </tr>
    </table>
    
    <?php
}



if($action=="generate_report_summary")
{
	
	
	if(str_replace("'","",$cbo_company_name)==0) $company_name=""; else $company_name=" and a.company_name=".str_replace("'","",$cbo_company_name)."";
	if(str_replace("'","",$cbo_buyer_name)==0) $buyer_name=""; else $buyer_name=" and a.buyer_name=".str_replace("'","",$cbo_buyer_name)."";
	if(trim($txt_date_from)!="") $start_date=$txt_date_from;
	if(trim($txt_date_to)!="") $end_date=$txt_date_to;
	if(str_replace("'","",$cbo_team_name)=="0") $team_leader=""; else $team_leader=" and a.team_leader=".str_replace("'","",$cbo_team_name)."";
	if(str_replace("'","",$cbo_team_member)=="0") $dealing_marchant=""; else $dealing_marchant=" and a.dealing_marchant=".str_replace("'","",$cbo_team_member)."";
	//echo $txt_date_from;die;
	$strar_month=str_replace("0","",date('m', strtotime($txt_date_from)));
	$end_month=str_replace("0","",date('m', strtotime($txt_date_to)));
	//echo change_date_format(".$txt_date_from.");die;
	$end_year=date('Y', strtotime($txt_date_to));
	//echo $start_year."dd".$end_year;die;

	if ($start_date!="" && $end_date!="")
		{
			$date_cond="and b.pub_shipment_date between $start_date and  $end_date";
		}
		else	
		{
			$date_cond="";
		}
    if($db_type==2) $shipment_year="  extract(year from c.country_ship_date) as year";
	if($db_type==0) $shipment_year="  year(c.country_ship_date) as year";
	if($db_type==2) $shipment_month="  extract(month from c.country_ship_date) as month";
	if($db_type==0) $shipment_month="  month(c.country_ship_date) as month";
	if (str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
		{
			$date_cond="and c.country_ship_date between $txt_date_from and  $txt_date_to";
		}
	else	
		{
			$date_cond="";
		}

		$data_array=sql_select("select   a.company_name, a.buyer_name,  b.is_confirmed,  sum(c.order_quantity) as po_quantity_pcs, $shipment_year,$shipment_month,  sum(c.order_total) as po_total_price from wo_po_details_master a, wo_po_break_down b, wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id  $company_name $buyer_name $team_leader $dealing_marchant $date_cond  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by  a.company_name, a.buyer_name,  b.is_confirmed,c.country_ship_date order by c.country_ship_date ");
		$project_data_arr=array();
		$confrim_data_arr=array();
		$month_count_arr=array();
		$total_buyer_arr=array();
		$total_qty_arr=array();
		$total_confrim=array();
		$total_project=array();
		$grand_confirm_buyer=array();
		$grand_project_buyer=array();
		
		$buyer_total=array();
	    foreach($data_array as $row)
		  {
			 $total_buyer_arr[$row[csf('company_name')]][$row[csf('buyer_name')]]=$row[csf('buyer_name')];
			 $month_count_arr[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['year']=$row[csf('year')];
			 $month_count_arr[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['month']=$row[csf('month')];
			 $total_qty_arr[$row[csf('company_name')]][$row[csf('buyer_name')]][$row[csf('year')]][$row[csf('month')]]['poqty']+=$row[csf('po_quantity_pcs')];
			 $buyer_total[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['poqty']+=$row[csf('po_quantity_pcs')];
			 $buyer_total[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['amount']+=$row[csf('po_total_price')];
			 
		if($row[csf('is_confirmed')]==1)
		   {
			 $confrim_data_arr[$row[csf('company_name')]][$row[csf('buyer_name')]][$row[csf('year')]][$row[csf('month')]]['poqty']+=$row[csf('po_quantity_pcs')];
			 $confrim_data_arr[$row[csf('company_name')]][$row[csf('buyer_name')]][$row[csf('year')]][$row[csf('month')]]['amount']+=$row[csf('po_total_price')]; 
			  $total_confrim[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['poqty']+=$row[csf('po_quantity_pcs')];
			  $total_confrim[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['amount']+=$row[csf('po_total_price')]; 
			  $grand_confirm_buyer[$row[csf('company_name')]][$row[csf('buyer_name')]]['poqty']+=$row[csf('po_quantity_pcs')]; 
			  $grand_confirm_buyer[$row[csf('company_name')]][$row[csf('buyer_name')]]['amount']+=$row[csf('po_total_price')]; 
			  
			  $grand_total_confirm_po+=$row[csf('po_quantity_pcs')];
			  $grand_total_confirm_amount+=$row[csf('po_total_price')];      
		   }
		   if($row[csf('is_confirmed')]==2)
		   {
			 $project_data_arr[$row[csf('company_name')]][$row[csf('buyer_name')]][$row[csf('year')]][$row[csf('month')]]['poqty']+=$row[csf('po_quantity_pcs')];
			 $project_data_arr[$row[csf('company_name')]][$row[csf('buyer_name')]][$row[csf('year')]][$row[csf('month')]]['amount']+=$row[csf('po_total_price')];
			  $total_project[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['poqty']+=$row[csf('po_quantity_pcs')];
			  $total_project[$row[csf('company_name')]][$row[csf('year')]][$row[csf('month')]]['amount']+=$row[csf('po_total_price')];
			  $grand_project_buyer[$row[csf('company_name')]][$row[csf('buyer_name')]]['poqty']+=$row[csf('po_quantity_pcs')]; 
			  $grand_project_buyer[$row[csf('company_name')]][$row[csf('buyer_name')]]['amount']+=$row[csf('po_total_price')];
			  $grand_total_poject_po+=$row[csf('po_quantity_pcs')];
			  $grand_total_poject_amount+=$row[csf('po_total_price')];  
			
		   }
			
			//$month_count_arr[$row[csf('year')]][$row[csf('month')]]=$row[csf('month')];	
				
		  }
	
	            $total_month=0;
	            foreach($month_count_arr as $company_id=>$compnay_val)
                      {
                         foreach($compnay_val as $year_id=>$year_val)
                          {
                              foreach($year_val as $month_id=>$month_val)
                               {
                                $total_month=$total_month+1;
                               }
                           }
                         
                       }
	
	$table_width=120+($total_month*530)+590;
	$col_span=2+($total_month*7)+8;
	//echo $col_span;die;
	
	ob_start();
	
	?>
  <fieldset style="width:<?php echo $table_width+10;  ?>px;">  
  <table id="" class="" width="<?php echo $table_width;  ?>" cellspacing="0" >
     <tr class="" style="border:none; ">
       <td colspan="<?php echo $col_span; ?>" align="center" style="font-size:24px"> Order booking Summary</td>
     </tr>
      <tr class="" style="border:none;">
       <td colspan="<?php echo $col_span; ?>" align="center" style="font-size:20px">Company Name:  <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?></td>
     </tr>
      <tr class="" style="border:none;">
       <td colspan="<?php echo $col_span; ?>" align="center" style="font-size:20px">
       Date: 
	   <?php 
	    if($db_type==0){     echo change_date_format(str_replace("'","",$txt_date_from))." To  ".change_date_format(str_replace("'","",$txt_date_to)); }
		if($db_type==2){ echo str_replace("'","",$txt_date_from)."  To  ".str_replace("'","",$txt_date_to); }
	    ?> 
        
        </td>
     </tr>
  </table>
    <table id="table_header" class="rpt_table" width="<?php echo $table_width;  ?>" cellpadding="0" cellspacing="0" border="1" rules="all">
      <thead>
           <tr >
                <th width="40" rowspan="2">SL</th>
                <th width="80" rowspan="2">Buyer</th>
                  <?php
                    foreach($month_count_arr as $company_id=>$compnay_val)
                      {
                         foreach($compnay_val as $year_id=>$year_val)
                          {
                              foreach($year_val as $month_id=>$month_val)
                               {
                                 ?>  
                                   <th width="530" colspan="7"><?php   echo $months[$month_val['month']]."   ".$month_val['year']; ?></th>
                                
                                  <?php
                               }
                           }
                         
                       }
                  ?>
                 <th width="590" colspan="8">Total</th>
            </tr>
            <tr>
            <?php
                  foreach($month_count_arr as $company_id=>$compnay_val)
                     {
                        foreach($compnay_val as $year_id=>$year_val)
                        {
                            foreach($year_val as $month_id=>$month_val)
                            {
                               ?>  
                                    <th width="80" >Proj. Qty</th>
                                    <th width="80" >Proj. Amt </th>
                                    <th width="80" >Conf. Qty</th>
                                    <th width="80" >Conf. Amt</th>
                                    <th width="80" >Total Qty</th>
                                    <th width="80" >Total Amt</th>
                                    <th width="50" >%</th>
                                <?php
                            }
                        }
                         
                     }
            ?> 
            
                    <th width="80" >Proj. Qty</th>
                    <th width="80" >Proj. Amt </th>
                    <th width="80" >Conf. Qty</th>
                    <th width="80" >Conf. Amt</th>
                    <th width="80" >Total Qty</th>
                    <th width="80" >Total Amt</th>
                    <th width="50" >%</th>
                    <th width="60" >Avg Price</th>
            </tr>
        </thead>
        <tbody>
              <?php
			  $i=1;
			    foreach($total_buyer_arr as $com_id=>$com_val)
				{
				   foreach($com_val as $buy_id=>$buy_val)
						{	
					     if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						   ?>
					  
							 <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $i; ?>">
                                    <td> <?php echo $i; ?>  </td>
                                    <td align="center"> <?php echo $buyer_short_name_arr[$buy_val]; ?>  </td>
										<?php
                                           foreach($month_count_arr as $company_id=>$compnay_val)
                                             {
                                                foreach($compnay_val as $year_id=>$year_val)
                                                {
                                                    foreach($year_val as $month_id=>$month_val)
                                                    {
													  $buyer_pc_total=$project_data_arr[$com_id][$buy_id][$year_id][$month_id]['poqty']+$confrim_data_arr[$com_id][$buy_id][$year_id][$month_id]['poqty'];
													  	  $buyer_pc_total_amount=$project_data_arr[$com_id][$buy_id][$year_id][$month_id]['amount']+$confrim_data_arr[$com_id][$buy_id][$year_id][$month_id]['amount'];
                                                       ?>  
                                                            <td width="80"  align="right"><?php echo  number_format($project_data_arr[$com_id][$buy_id][$year_id][$month_id]['poqty'],0);  ?></td>
                                                            <td width="80"  align="right"><?php echo  number_format($project_data_arr[$com_id][$buy_id][$year_id][$month_id]['amount'],2);  ?> </td>
                                                            <td width="80"  align="right"><?php echo  number_format($confrim_data_arr[$com_id][$buy_id][$year_id][$month_id]['poqty'],0);  ?></td>
                                                            <td width="80"  align="right"><?php echo  number_format($confrim_data_arr[$com_id][$buy_id][$year_id][$month_id]['amount'],2);  ?></td>
                                                            <td width="80"  align="right"><?php echo  number_format($buyer_pc_total,0);  ?></td>
                                                            <td width="80"  align="right"><?php echo  number_format($buyer_pc_total_amount,2);  ?></td>
                                                            <td width="50"  align="center"><?php echo  number_format(($buyer_pc_total_amount/$buyer_total[$company_id][$year_id][$month_id]['amount'])*100,2);  ?></td>
                                                            
                                                       
                                                        
                                                        <?php
                                                    }
                                                }
                                                 
                                             }
                                        
                                        ?>
                                        <td width="80" align="right"><?php echo  number_format($grand_project_buyer[$company_id][$buy_id]['poqty'],0);  ?></td>
                                        <td width="80" align="right"><?php echo  number_format($grand_project_buyer[$company_id][$buy_id]['amount'],2);  ?> </td>
                                        <td width="80" align="right"><?php echo  number_format($grand_confirm_buyer[$company_id][$buy_id]['poqty'],0);  ?></td>
                                        <td width="80" align="right"><?php echo  number_format($grand_confirm_buyer[$company_id][$buy_id]['amount'],2);  ?></td>
                                        <td width="80" align="right"><?php echo  number_format($grand_project_buyer[$company_id][$buy_id]['poqty']+$grand_confirm_buyer[$company_id][$buy_id]['poqty'],0);  ?></td>
                                        <td width="80" align="right"><?php echo  number_format($grand_project_buyer[$company_id][$buy_id]['amount']+$grand_confirm_buyer[$company_id][$buy_id]['amount'],2);  ?></td>
                                        <td width="50" align="center">

										<?php
										
										$buyer_wise_grand_total=($grand_project_buyer[$company_id][$buy_id]['amount']+$grand_confirm_buyer[$company_id][$buy_id]['amount']);
										$grand_total_all_buyer=$grand_total_poject_amount+$grand_total_confirm_amount;
										 echo number_format(($buyer_wise_grand_total/$grand_total_all_buyer)*100,2);
										  ?>

                                        </td>
                                        <td width="60" align="right">
										<?php
									  	$grand_total_price_all_buyer=$grand_project_buyer[$company_id][$buy_id]['poqty']+$grand_confirm_buyer[$company_id][$buy_id]['poqty'];
										$buyer_wise_grand_total_price=$grand_project_buyer[$company_id][$buy_id]['amount']+$grand_confirm_buyer[$company_id][$buy_id]['amount'];
										
										
										  echo number_format(($buyer_wise_grand_total_price/$grand_total_price_all_buyer),2);
										 
										 
										 
										 
										   ?>
                                        
                                        
                                        
                                        </td>
							 </tr>
							
							<?php
							$i++;
						}
				}
				?>
        </tbody>
        <tfoot>
            <tr> 
                <th width="40" ></th>
                <th width="120" >Total</th>
                <?php
				  foreach($month_count_arr as $company_id=>$compnay_val)
						 {
							foreach($compnay_val as $year_id=>$year_val)
							{
								foreach($year_val as $month_id=>$month_val)
								{
								   ?>  
										<th width="80" align="right" ><?php echo  number_format($total_project[$company_id][$year_id][$month_id]['poqty'],0);  ?></th>
										<th width="80" align="right"> <?php echo  number_format($total_project[$company_id][$year_id][$month_id]['amount'],2);  ?></th>
										<th width="80" align="right"><?php echo  number_format($total_confrim[$company_id][$year_id][$month_id]['poqty'],0);  ?></th>
										<th width="80" align="right"><?php echo  number_format($total_confrim[$company_id][$year_id][$month_id]['amount'],2);  ?></th>
										<th width="80" align="right"><?php echo  number_format($buyer_total[$company_id][$year_id][$month_id]['poqty'],0);  ?></th>
										<th width="80" align="right"><?php echo  number_format($buyer_total[$company_id][$year_id][$month_id]['amount'],2);  ?></th>
										<th width="50" align="right" ></th>
									<?php
								}
							}
							 
						 }
				 ?> 
            
                    <th width="80" align="right"><?php echo  number_format($grand_total_poject_po,0); ?></th>
                    <th width="80" align="right"><?php echo  number_format($grand_total_poject_amount,2); ?></th>
                    <th width="80" align="right"><?php echo  number_format($grand_total_confirm_po,0); ?></th>
                    <th width="80" align="right"><?php echo  number_format($grand_total_confirm_amount,2); ?></th>
                    <th width="80" align="right"><?php echo  number_format($grand_total_poject_po+$grand_total_confirm_po,0); ?></th>
                    <th width="80" align="right"><?php echo  number_format($grand_total_poject_amount+$grand_total_confirm_amount,2); ?></th>
                    <th width="50" align="right"></th>
                    <th width="60" align="right"> <?php echo  number_format(($grand_total_poject_amount+$grand_total_confirm_amount)/($grand_total_poject_po+$grand_total_confirm_po),2); ?></th>
            </tr>
        </tfoot>
    </table>
    
    </fieldset>
    <?php
	foreach (glob("$user_id*.xls") as $filename) 
		{
			if( @filemtime($filename) < (time()-$seconds_old) )
			@unlink($filename);
		}
		//---------end------------//
		$name=time();
		$filename=$user_id."_".$name.".xls";
		$create_new_doc = fopen($filename, 'w');
		$is_created = fwrite($create_new_doc,ob_get_contents());
		//$filename=$user_id."_".$name.".xls";
		echo "$total_data####$filename";
		exit(); 
	
}























?>