<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Lc/Salse Contact Report.
Functionality	:	
JS Functions	:
Created by		:	Jahid
Creation date 	: 	21-12-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Export Lc/Sc Report", "../../", 1, 1,'','','');
?>	

<script>

 	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
	var permission = '<?php echo $permission; ?>';
	
function generate_report()
	{
		if(form_validation('cbo_company_name','Company Name')==false)
		{
			return;
		}
		
		var report_title=$( "div.form_caption" ).html();	
		var data="action=report_generate"+get_submitted_data_string("cbo_company_name*txt_year*cbo_lien_bank*cbo_currency_name*cbo_buyer_name*cbo_applicant*txt_date_from*txt_date_to","../../")+'&report_title='+report_title;
		freeze_window(3);
		http.open("POST","requires/export_lc_and_sales_contract_report_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fn_report_generated_reponse;
	}
	
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			release_freezing();
			var response=trim(http.responseText);
			$('#report_container2').html(response);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
			show_msg('3');
		}
	}
</script>
</head>

<body onLoad="set_hotkey();">
    <div style="width:100%;" align="center">
    <?php echo load_freeze_divs ("../../",''); ?>
    <form id="frm_lc_salse_contact" name="frm_lc_salse_contact">
    <div style="width:1070px;">
    <h3 align="left" id="accordion_h1" style="width:1070px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3> 
    <div id="content_search_panel"> 
    <fieldset style="width:1070px;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center">
                <table class="rpt_table" cellspacing="0" cellpadding="0" width="1020">
                <thead>
                    <th width="130" class="must_entry_caption">Company</th>
                    <th width="130">Year</th>
                    <th width="120">Lien Bank</th>
                    <th width="130">Currency</th>
                    <th width="110">Buyer</th>
                    <th width="120">Applicant</th>
                    <th width="90" >Date From</th>
                    <th width="90" >Date To</th>
                    <th width="80"><input type="reset" name="res" id="res" value="Reset" style="width:70px" class="formbutton" onClick="reset_form('frm_lc_salse_contact','report_container*report_container2','','','')" /></th>
                </thead>
                <tbody>
                    <tr >
                        <td>
						<?php
                        	echo create_drop_down( "cbo_company_name", 130, "select comp.id, comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "- Select Company -", $selected, "load_drop_down( 'requires/export_lc_and_sales_contract_report_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );load_drop_down( 'requires/export_lc_and_sales_contract_report_controller',this.value, 'load_drop_down_applicant', 'applicat_td' );" );
                        ?>
                        </td>
                        <td>
                        <input type="text" id="txt_year" name="txt_year" class="text_boxes" style="width:120px;">
                        </td>
                        <td>
						<?php
                        	echo create_drop_down( "cbo_lien_bank", 130, "select bank_name,id from lib_bank where is_deleted=0 and status_active=1 and lien_bank=1 order by bank_name","id,bank_name", 1, "- All Lien Bank -", 0, "" );
                        ?>
                        </td>
                        <td>	
                        <?php 
                        	echo create_drop_down( "cbo_currency_name", 130, $currency,"", 1, "- Select Currency --", $selected, "",0,"" );
                        ?>
                        </td>
                        <td id="buyer_td">
						<?php 
                        	echo create_drop_down( "cbo_buyer_name", 110, $blank_array,"", 1, "- All Buyer -", $selected, "",0,"" );
                        ?>
                        </td>
                        <td id="applicat_td">
                        <?php 
                        	echo create_drop_down( "cbo_applicant", 110, $blank_array,"", 1, "- All Applicant -", $selected, "",0,"" );
                        ?>
                        </td>
                        <td>
                        
                        <input name="txt_date_from" id="txt_date_from" class="datepicker"  style="width:70px">
                        </td>
                        <td>
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
                        </td>
                        <td>
                        <input type="button" name="search" id="search" value="Show" onClick="generate_report()" style="width:70px" class="formbutton" />
                        </td>
                    </tr>
                </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="15" align="center" valign="bottom"><?php echo load_month_buttons(1);  ?></td>
        </tr>
    </table>
   
    </fieldset>
    </div>
    </div>
    <div id="report_container" align="center"></div>
    <div id="report_container2"></div>
    </form>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>