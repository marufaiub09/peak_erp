<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=$_SESSION['page_permission'];

include('../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$menu_id=$_SESSION['menu_id'];

$company_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$user_arr=return_library_array( "select id, user_name from user_passwd", "id", "user_name"  );
if($db_type==0) $year_cond="SUBSTRING_INDEX(a.`insert_date`, '-', 1) as year";
if($db_type==2) $year_cond="to_char(a.insert_date,'YYYY') as year";
if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 152, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );  
	exit();
	
}

if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$sequence_no='';
	$company_name=str_replace("'","",$cbo_company_name);
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";
	}
	
	$date_cond='';
	if(str_replace("'","",$txt_date)!="")
	{
		if(str_replace("'","",$cbo_get_upto)==1) $date_cond=" and b.costing_date>$txt_date";
		else if(str_replace("'","",$cbo_get_upto)==2) $date_cond=" and b.costing_date<=$txt_date";
		else if(str_replace("'","",$cbo_get_upto)==3) $date_cond=" and b.costing_date=$txt_date";
		else $date_cond='';
	}
	$approval_type=str_replace("'","",$cbo_approval_type);
	$user_sequence_no=return_field_value("sequence_no","electronic_approval_setup","page_id=$menu_id and user_id=$user_id and is_deleted=0");
	$min_sequence_no=return_field_value("min(sequence_no)","electronic_approval_setup","page_id=$menu_id and is_deleted=0");
	if($user_sequence_no=="")
	{
		echo "<font style='color:#F00; font-size:14px; font-weight:bold'>You Have No Authority To Sign Pre-Costing.</font>";
		die;
	}
	if($approval_type==2)
	{
		$sequence_no=return_field_value("max(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no<$user_sequence_no and bypass=2 and is_deleted=0");
	
	if($user_sequence_no==$min_sequence_no)
		{
			$sql="select b.id,a.job_no_prefix_num,$year_cond,a.job_no, a.buyer_name, a.style_ref_no,b.costing_date,'0' as approval_id, b.approved,b.inserted_by  from wo_pre_cost_mst b,  wo_po_details_master a where a.job_no=b.job_no and a.company_name=$company_name  and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and b.approved=$approval_type $buyer_id_cond $date_cond";
		}
		else if($sequence_no=="")
		{
			
		 if($db_type==0)
			{
			$sequence_no_by=return_field_value("group_concat(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no<$user_sequence_no and bypass=1 and is_deleted=0");
		
			$pre_cost_id=return_field_value("group_concat(distinct(mst_id)) as pre_cost_id","wo_pre_cost_mst a, approval_history b,wo_po_details_master c","a.id=b.mst_id and a.job_no=c.job_no and c.company_name=$company_name and b.sequence_no in ($sequence_no_by) and b.entry_form=15    and b.current_approval_status=1");
				
			$pre_cost_id_app_byuser=return_field_value("group_concat(distinct(mst_id)) as pre_cost_id","wo_pre_cost_mst a, approval_history b,wo_po_details_master c","a.id=b.mst_id and a.job_no=c.job_no and c.company_name=$company_name and b.sequence_no=$user_sequence_no and b.entry_form=15 and b.current_approval_status=1","pre_cost_id");
			}
			if($db_type==2)
			{
			$sequence_no_by=return_field_value("listagg(sequence_no,',') within group (order by sequence_no) as sequence_no","electronic_approval_setup","page_id=$menu_id and sequence_no<$user_sequence_no and bypass=1 and is_deleted=0","sequence_no");
			$pre_cost_id=return_field_value("LISTAGG(mst_id, ',') WITHIN GROUP (ORDER BY mst_id) as pre_cost_id","wo_pre_cost_mst a, approval_history b,wo_po_details_master c","a.id=b.mst_id and a.job_no=c.job_no and c.company_name=$company_name and b.sequence_no in ($sequence_no_by) and b.entry_form=15    and b.current_approval_status=1","pre_cost_id");
			$pre_cost_id=implode(",",array_unique(explode(",",$pre_cost_id)));	
			
			$pre_cost_id_app_byuser=return_field_value("LISTAGG(mst_id, ',') WITHIN GROUP (ORDER BY mst_id) as pre_cost_id","wo_pre_cost_mst a, approval_history b,wo_po_details_master c","a.id=b.mst_id and a.job_no=c.job_no and c.company_name=$company_name and b.sequence_no=$user_sequence_no and b.entry_form=15 and b.current_approval_status=1","pre_cost_id");
			$pre_cost_id_app_byuser=implode(",",array_unique(explode(",",$pre_cost_id_app_byuser)));
			
			}
			$result=array_diff(explode(',',$pre_cost_id),explode(',',$pre_cost_id_app_byuser));
			$pre_cost_id=implode(",",$result);
	    	$pre_cost_id_cond="";
			if($pre_cost_id_app_byuser!="") $pre_cost_id_cond=" and b.id not in($pre_cost_id_app_byuser)";
			else $pre_cost_id_cond=" eeeeeeeeeeee";
			 if($pre_cost_id!="") $pre_cost_id_cond.=" or (b.id in($pre_cost_id))";
			$sql="select b.id,a.job_no_prefix_num,$year_cond,a.job_no, a.buyer_name, a.style_ref_no,b.costing_date,'0' as approval_id, b.approved,b.inserted_by 
			      from wo_pre_cost_mst b,wo_po_details_master a
				  where a.job_no=b.job_no and a.company_name=$company_name  and a.is_deleted=0 and a.status_active=1 and b.status_active=1
				  and b.is_deleted=0 and b.approved=$approval_type $buyer_id_cond $pre_cost_id_cond $date_cond";
		}
		else
		{
			$user_sequence_no=$user_sequence_no-1;
			if($sequence_no==$user_sequence_no) $sequence_no_by_pass='';
			else
			{
				if($db_type==0)
					{
						$sequence_no_by_pass=return_field_value("group_concat(sequence_no)","electronic_approval_setup","page_id=$menu_id and
						 sequence_no between $sequence_no and $user_sequence_no and bypass=1 and is_deleted=0");
					}
				else
					{
						$sequence_no_by_pass=return_field_value("LISTAGG(sequence_no, ',') WITHIN GROUP (ORDER BY sequence_no)
						 as sequence_no","electronic_approval_setup","page_id=$menu_id and sequence_no between $sequence_no and $user_sequence_no
						 and bypass=1 and is_deleted=0","sequence_no");	
					}
			}
			if($sequence_no_by_pass=="") $sequence_no_cond=" and c.sequence_no='$sequence_no'";
			else $sequence_no_cond=" and (c.sequence_no='$sequence_no' or c.sequence_no in ($sequence_no_by_pass))";
			$sql="select b.id,a.job_no_prefix_num,$year_cond,a.job_no, a.buyer_name, a.style_ref_no,b.costing_date, b.approved,b.inserted_by,
			      c.id as approval_id, c.sequence_no, c.approved_by,c.id as approval_id
			      from wo_pre_cost_mst b,  wo_po_details_master a,approval_history c
				  where b.id=c.mst_id and c.entry_form=15 and a.job_no=b.job_no and a.company_name=$company_name  and a.is_deleted=0 and
				  a.status_active=1 and b.status_active=1 and c.current_approval_status=1 and 
				  b.is_deleted=0 and b.approved=1 $buyer_id_cond $sequence_no_cond $date_cond";
		}
	}
	else
	{
		$sequence_no_cond=" and c.sequence_no='$user_sequence_no'";
		$sql="select b.id,a.job_no_prefix_num,$year_cond,a.job_no, a.buyer_name, a.style_ref_no,b.costing_date, b.approved,b.inserted_by,
			      c.id as approval_id, c.sequence_no, c.approved_by,c.id as approval_id 
			      from wo_pre_cost_mst b,  wo_po_details_master a,approval_history c
				  where b.id=c.mst_id and c.entry_form=15 and a.job_no=b.job_no and a.company_name=$company_name  and a.is_deleted=0 and
				  a.status_active=1 and b.status_active=1 and c.current_approval_status=1 and 
				  b.is_deleted=0 and b.approved=1 $buyer_id_cond $sequence_no_cond $date_cond";
	}
	?>
    <form name="requisitionApproval_2" id="requisitionApproval_2">
        <fieldset style="width:940px; margin-top:10px">
        <legend>Pre-Costing Approval</legend>	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="920" class="rpt_table" >
                <thead>
                	<th width="50"></th>
                    <th width="40">SL</th>
                    <th width="100">Job No</th>
                     <th width="50">Year</th>
                    <th width="125">Buyer</th>
                    <th width="160">Style Ref.</th>
                    <th width="100">Costing Date</th>
                    <th width="100">Est. Ship Date</th>
                    <th width="50">Image</th>
                    <th width="">Insert By</th>
                </thead>
            </table>
            <div style="width:920px; overflow-y:scroll; max-height:330px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="902" class="rpt_table" id="tbl_list_search">
                    <tbody>
                        <?php
						
                            $i=1;
                            $nameArray=sql_select( $sql );
                            foreach ($nameArray as $row)
                            {
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$value='';
								if($approval_type==2)
								{
									$value=$row[csf('id')];
								}
								else
								{
									$app_id=return_field_value("id","approval_history","mst_id ='".$row[csf('id')]."' and entry_form='15' order by id desc","id");
									$value=$row[csf('id')]."**".$app_id;
								}
								
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>" align="center"> 
                                	<td width="50" align="center" valign="middle">
                                        <input type="checkbox" id="tbl_<?php echo $i;?>" />
                                        <input id="booking_id_<?php echo $i;?>" name="booking_id[]" type="hidden" value="<?php echo $value; ?>" />
                                        <input id="booking_no_<?php echo $i;?>" name="booking_no[]" type="hidden" value="<?php echo $row[csf('job_no')]; ?>" />
                                        <input id="approval_id_<?php echo $i;?>" name="approval_id[]" type="hidden" value="<?php echo $row[csf('approval_id')]; ?>" />
                                    </td>   
									<td width="40" align="center"><?php echo $i; ?></td>
									<td width="100">
                                    	<p><a href='##'  onclick="generate_worder_report('<?php echo "preCostRpt2"; ?>',<?php echo $row[csf('id')]; ?>,<?php echo $row[csf('company_id')]; ?>,<?php echo $row[csf('buyer_name')]; ?>,'<?php echo $row[csf('style_ref_no')];?>','<?php echo $row[csf('costing_date')]; ?>')"><?php echo $row[csf('job_no_prefix_num')]; ?></a></p>
                                    </td>
                                    <td width="50"><p><?php echo $row[csf('year')]; ?>&nbsp;</p></td>
									
                                    <td width="125"><p><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?>&nbsp;</p></td>
									<td width="160" align="center"><p><?php echo $row[csf('style_ref_no')]; ?>&nbsp;</p></td>
                                    <td width="100" align="center"><?php if($row[csf('costing_date')]!="0000-00-00") echo change_date_format($row[csf('costing_date')]); ?>&nbsp;</td>
									<td align="center" width="100"><?php if($row[csf('est_ship_date')]!="0000-00-00") echo change_date_format($row[csf('est_ship_date')]); ?>&nbsp;</td>
                                    <td width="50" align="center"><a href="##" onClick="openImgFile('<?php echo $row[csf('job_no')];?>','img');">View</a></td>
                                    <td><p><?php echo ucfirst ($user_arr[$row[csf('inserted_by')]]); ?>&nbsp;</p></td>
								</tr>
								<?php
								$i++;
							}
                        ?>
                    </tbody>
                </table>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" rules="all" width="920" class="rpt_table">
				<tfoot>
                    <td width="50" align="center" ><input type="checkbox" id="all_check" onclick="check_all('all_check')" /></td>
                    <td colspan="2" align="left"><input type="button" value="<?php if($approval_type==1) echo "Un-Approve"; else echo "Approve"; ?>" class="formbutton" style="width:100px" onclick="submit_approved(<?php echo $i; ?>,<?php echo $approval_type; ?>)"/></td>
				</tfoot>
			</table>
        </fieldset>
    </form>         
<?php
	exit();	
}

if ($action=="approve")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	//$user_id=23;
	$con = connect();
	if($db_type==0)
	{
		mysql_query("BEGIN");
	}
	
	$msg=''; $flag=''; $response='';
	
	$user_sequence_no=return_field_value("sequence_no","electronic_approval_setup","page_id=$menu_id and user_id=$user_id and is_deleted=0");
	$min_sequence_no=return_field_value("min(sequence_no)","electronic_approval_setup","page_id=$menu_id and is_deleted=0");
	
	if($approval_type==2)
	{
		$response=$booking_ids;
		//echo $response;die;
		$field_array="id, entry_form, mst_id, approved_no, sequence_no, current_approval_status, approved_by, approved_date"; 
		$id=return_next_id( "id","approval_history", 1 ) ;
		
		$max_approved_no_arr = return_library_array("select mst_id, max(approved_no) as approved_no from approval_history where mst_id in($booking_ids) and entry_form=15 group by mst_id","mst_id","approved_no");
		
		$approved_status_arr = return_library_array("select id, approved from wo_pre_cost_mst where id in($booking_ids)","id","approved");
		
/*		
function sql_multirow_update2($strTable,$arrUpdateFields,$arrUpdateValues,$arrRefFields,$arrRefValues, $commit)
{ 

	$strQuery = "UPDATE ".$strTable." SET ";
	$arrUpdateFields=explode("*",$arrUpdateFields);
	$arrUpdateValues=explode("*",$arrUpdateValues);	
   
 
	if(is_array($arrUpdateFields))
	{
		$arrayUpdate = array_combine($arrUpdateFields,$arrUpdateValues);
		$Arraysize = count($arrayUpdate);
		$i = 1;
		foreach($arrayUpdate as $key=>$value):
			$strQuery .= ($i != $Arraysize)? $key."=".$value.", ":$key."=".$value." WHERE ";
			$i++;
		endforeach;
	}
	else
	{
		$strQuery .= $arrUpdateFields."=".$arrUpdateValues." WHERE ";
	}
	
	//$arrRefFields=explode("*",$arrRefFields);
	//$arrRefValues=explode("*",$arrRefValues);	
	$strQuery .= $arrRefFields." in (".$arrRefValues.")";
	 echo $strQuery;die;
    global $con;
	$stid =  oci_parse($con, $strQuery);
	$exestd=oci_execute($stid,OCI_NO_AUTO_COMMIT);
	if ($exestd) 
		return "1";
	else 
		return "0";
	
	die;
	$_SESSION['last_query']=$_SESSION['last_query'].";;".$strQuery;
	if ($commit==1)
	{
		if (!oci_error($stid))
		{
			
		$pc_time= add_time(date("H:i:s",time()),360); 
		$pc_date_time = date("d-M-Y h:i:s",strtotime(add_time(date("H:i:s",time()),360)));
	    $pc_date = date("d-M-Y",strtotime(add_time(date("H:i:s",time()),360)));
		
		$strQuery= "INSERT INTO activities_history ( session_id,user_id,ip_address,entry_time,entry_date,module_name,form_name,query_details,query_type) VALUES ('".$_SESSION['logic_erp']["history_id"]."','".$_SESSION['logic_erp']["user_id"]."','".$_SESSION['logic_erp']["pc_local_ip"]."','".$pc_time."','".$pc_date."','".$_SESSION["module_id"]."','".$_SESSION['menu_id']."','".encrypt($_SESSION['last_query'])."','1')"; 

		mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
		 
		$resultss=oci_parse($con, $strQuery);
		oci_execute($resultss);
		$_SESSION['last_query']="";
		oci_commit($con); 
		return "0";
		}
		else
		{
			oci_rollback($con);
			return "10";
		}
	}
	else return 1;
	die;
}

	*/	


		$rID=sql_multirow_update("wo_pre_cost_mst","approved",1,"id",$booking_ids,0);
		
		if($rID) $flag=1; else $flag=0;
		
		if($approval_ids!="")
		{
			$rIDapp=sql_multirow_update("approval_history","current_approval_status",0,"id",$approval_ids,0);
			if($flag==1) 
			{
				if($rIDapp) $flag=1; else $flag=0; 
			} 
		}
		$approved_no_array=array();
		$booking_ids_all=explode(",",$booking_ids);
		$booking_nos_all=explode(",",$booking_nos);
		$book_nos='';
		
		for($i=0;$i<count($booking_nos_all);$i++)
		{
			$val=$booking_nos_all[$i];
			$booking_id=$booking_ids_all[$i];

			$approved_no=$max_approved_no_arr[$booking_id];
			$approved_status=$approved_status_arr[$booking_id];
			
			if($approved_status==2)
			{
				$approved_no=$approved_no+1;
				$approved_no_array[$val]=$approved_no;
				if($book_nos=="") $book_nos=$val; else $book_nos.=",".$val;
			}
			
			if($data_array!="") $data_array.=",";
			
			$data_array.="(".$id.",15,".$booking_id.",".$approved_no.",'".$user_sequence_no."',1,".$user_id.",'".$pc_date_time."')"; 
				
			$id=$id+1;
			
		}
		
		//echo "insert into approval_history (".$field_array.") Values ".$data_array."**".$book_nos."**".$booking_nos;die;
		$rID2=sql_insert("approval_history",$field_array,$data_array,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		if(count($approved_no_array)>0)
		{
			$approved_string="";
			
				if($db_type==0)
				{
					foreach($approved_no_array as $key=>$value)
					{
						$approved_string.=" WHEN $key THEN $value";
					}
				}
				else
				{
					foreach($approved_no_array as $key=>$value)
					{
						$approved_string.=" WHEN TO_NCHAR($key) THEN '".$value."'";
					}
				}
			$approved_string_mst="CASE job_no ".$approved_string." END";
			$approved_string_dtls="CASE job_no ".$approved_string." END";
			$sql_insert="insert into wo_pre_cost_mst_histry(id, approved_no, pre_cost_mst_id, garments_nature, job_no, costing_date, incoterm, incoterm_place, machine_line, prod_line_hr, costing_per, remarks, copy_quatation, cm_cost_predefined_method_id, exchange_rate, sew_smv, cut_smv, sew_effi_percent, cut_effi_percent, efficiency_wastage_percent, approved, approved_by, approved_date, inserted_by, insert_date, updated_by, update_date, status_active, is_deleted) 
				select	
				'', $approved_string_mst, id, garments_nature, job_no, costing_date, incoterm, incoterm_place, machine_line, prod_line_hr, costing_per, remarks, copy_quatation, cm_cost_predefined_method_id, exchange_rate, sew_smv, cut_smv, sew_effi_percent, cut_effi_percent, efficiency_wastage_percent, approved, approved_by, approved_date, inserted_by, insert_date, updated_by, update_date, status_active, is_deleted from wo_pre_cost_mst where job_no in ($book_nos)";
			//echo $sql_insert;die;	
			$rID3=execute_query($sql_insert,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
	
			$sql_precost_dtls="insert into wo_pre_cost_dtls_histry(id,approved_no,pre_cost_dtls_id,job_no,costing_per_id,order_uom_id,fabric_cost,  fabric_cost_percent,trims_cost,trims_cost_percent,embel_cost,embel_cost_percent,wash_cost,wash_cost_percent,comm_cost,comm_cost_percent,commission,
commission_percent,	lab_test,lab_test_percent,inspection, inspection_percent,cm_cost,cm_cost_percent,freight,freight_percent,currier_pre_cost,
currier_percent, certificate_pre_cost,certificate_percent,common_oh,common_oh_percent,total_cost,total_cost_percent,price_dzn,price_dzn_percent,
margin_dzn,margin_dzn_percent,cost_pcs_set,cost_pcs_set_percent,price_pcs_or_set,price_pcs_or_set_percent,margin_pcs_set,margin_pcs_set_percent,
cm_for_sipment_sche,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,job_no,costing_per_id,order_uom_id,fabric_cost,  fabric_cost_percent,trims_cost,trims_cost_percent,embel_cost,embel_cost_percent,wash_cost,wash_cost_percent,comm_cost,comm_cost_percent,commission,
commission_percent,	lab_test,lab_test_percent,inspection, inspection_percent,cm_cost,cm_cost_percent,freight,freight_percent,currier_pre_cost,
currier_percent, certificate_pre_cost,certificate_percent,common_oh,common_oh_percent,total_cost,total_cost_percent,price_dzn,price_dzn_percent,
margin_dzn,margin_dzn_percent,cost_pcs_set,cost_pcs_set_percent,price_pcs_or_set,price_pcs_or_set_percent,margin_pcs_set,margin_pcs_set_percent,
cm_for_sipment_sche,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted from wo_pre_cost_dtls  where  job_no in ($book_nos)";
			//echo $sql_precost_dtls;die;
			$rID4=execute_query($sql_precost_dtls,1);
			if($flag==1) 
			{
				if($rID4) $flag=1; else $flag=0; 
			} 
			
		//--------------------------------------wo_pre_cost_fabric_cost_dtls_h---------------------------------------------------------------------------
			$sql_precost_fabric_cost_dtls="insert into wo_pre_cost_fabric_cost_dtls_h(id,approved_no,pre_cost_fabric_cost_dtls_id, job_no, item_number_id, body_part_id, fab_nature_id, color_type_id,lib_yarn_count_deter_id,construction,composition, fabric_description, gsm_weight,color_size_sensitive,	color, avg_cons, fabric_source, rate, amount,avg_finish_cons,avg_process_loss, inserted_by, insert_date,updated_by,update_date, status_active, is_deleted, company_id, costing_per,consumption_basis,process_loss_method,cons_breack_down,msmnt_break_down,color_break_down,yarn_breack_down,marker_break_down,width_dia_type)
				select	
				'', $approved_string_dtls, id,job_no, item_number_id, body_part_id, fab_nature_id, color_type_id,lib_yarn_count_deter_id,construction,composition, fabric_description, gsm_weight,color_size_sensitive,	color, avg_cons, fabric_source, rate, amount,avg_finish_cons,avg_process_loss, inserted_by, insert_date,updated_by,update_date, status_active, is_deleted, company_id, costing_per,consumption_basis,process_loss_method,cons_breack_down,msmnt_break_down,color_break_down,yarn_breack_down,marker_break_down,width_dia_type from wo_pre_cost_fabric_cost_dtls where  job_no in ($book_nos)";
			//echo $sql_precost_fabric_cost_dtls;die;
			$rID5=execute_query($sql_precost_fabric_cost_dtls,1);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			} 
			
			//--------------------------------------wo_pre_cost_fab_yarn_cst_dtl_h---------------------------------------------------------------------------
			$sql_precost_fab_yarn_cst="insert into wo_pre_cost_fab_yarn_cst_dtl_h(id,approved_no,pre_cost_fab_yarn_cost_dtls_id,fabric_cost_dtls_id,job_no,count_id,copm_one_id,percent_one,copm_two_id,percent_two,type_id,cons_ratio,cons_qnty,rate,amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,fabric_cost_dtls_id,job_no,count_id,copm_one_id,percent_one,copm_two_id,percent_two,type_id,cons_ratio,cons_qnty,rate,amount,
				inserted_by,insert_date,updated_by,update_date,status_active,is_deleted from wo_pre_cost_fab_yarn_cost_dtls  where  job_no in ($book_nos)";
				//echo $sql_precost_fab_yarn_cst;die;
			$rID6=execute_query($sql_precost_fab_yarn_cst,1);
			if($flag==1) 
			{
				if($rID6) $flag=1; else $flag=0; 
			} 
			
//--------------------------------------wo_pre_cost_comarc_cost_dtls_h---------------------------------------------------------------------------
			$sql_precost_fcomarc_cost_dtls="insert into wo_pre_cost_comarc_cost_dtls_h(id,approved_no,pre_cost_comarci_cost_dtls_id,job_no,item_id,rate,amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted)
				select	
				'', $approved_string_dtls,id,job_no,item_id,rate,amount,inserted_by,insert_date,updated_by,update_date,status_active,
				is_deleted from wo_pre_cost_comarci_cost_dtls where  job_no in ($book_nos)";
				//echo $sql_precost_fcomarc_cost_dtls;die;
			$rID7=execute_query($sql_precost_fcomarc_cost_dtls,1);
			if($flag==1) 
			{
				if($rID7) $flag=1; else $flag=0; 
			} 			
		
		
//--------------------------------------  wo_pre_cost_commis_cost_dtls_h---------------------------------------------------------------------------
			$sql_precost_commis_cost_dtls="insert into wo_pre_cost_commis_cost_dtls_h(id,approved_no,pre_cost_commiss_cost_dtls_id,job_no,particulars_id,
			commission_base_id,commision_rate,commission_amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,job_no,particulars_id,commission_base_id,commision_rate,commission_amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted from wo_pre_cost_commiss_cost_dtls where  job_no in ($book_nos)";
			//	echo $sql_precost_commis_cost_dtls;die;
			$rID8=execute_query($sql_precost_commis_cost_dtls,1);
			if($flag==1) 
			{
				if($rID8) $flag=1; else $flag=0; 
			}
			
		//--------------------------------------   wo_pre_cost_embe_cost_dtls_his---------------------------------------------------------------------------
			$sql_precost_embe_cost_dtls="insert into  wo_pre_cost_embe_cost_dtls_his(id,approved_no,pre_cost_embe_cost_dtls_id,job_no,emb_name,
			emb_type,cons_dzn_gmts,rate,amount,charge_lib_id,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,job_no,emb_name,
			emb_type,cons_dzn_gmts,rate,amount,charge_lib_id,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted from wo_pre_cost_embe_cost_dtls  where  job_no in ($book_nos)";
				//echo $sql_precost_commis_cost_dtls;die;
			$rID9=execute_query($sql_precost_embe_cost_dtls,1);
			if($flag==1) 
			{
				if($rID9) $flag=1; else $flag=0; 
			} 
//----------------------------------------------------wo_pre_cost_fab_yarnbkdown_his------------------------------------------------------------------------

			$sql_precost_fab_yarnbkdown_his="insert into  wo_pre_cost_fab_yarnbkdown_his(id,approved_no,pre_cost_fab_yarnbreakdown_id,fabric_cost_dtls_id,job_no,count_id,copm_one_id,percent_one,copm_two_id,percent_two,type_id,cons_ratio,cons_qnty,rate,amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,fabric_cost_dtls_id,job_no,count_id,copm_one_id,percent_one,copm_two_id,percent_two,type_id,cons_ratio,cons_qnty,rate,amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted from wo_pre_cost_fab_yarnbreakdown  where  job_no in ($book_nos)";
				//echo $sql_precost_fab_yarnbkdown_his;die;
			$rID10=execute_query($sql_precost_fab_yarnbkdown_his,1);
			if($flag==1) 
			{
				if($rID10) $flag=1; else $flag=0; 
			} 
//----------------------------------------------------wo_pre_cost_sum_dtls_histroy------------------------------------------------------------------------

			$sql_precost_fab_sum_dtls="insert into  wo_pre_cost_sum_dtls_histroy(id,approved_no,pre_cost_sum_dtls_id,job_no,fab_yarn_req_kg,fab_woven_req_yds,fab_knit_req_kg,fab_woven_fin_req_yds,fab_knit_fin_req_kg,fab_amount,avg,yarn_cons_qnty,yarn_amount,conv_req_qnty,conv_charge_unit,conv_amount,trim_cons,trim_rate,trim_amount,emb_amount,comar_rate,
			comar_amount,commis_rate,commis_amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,job_no,fab_yarn_req_kg,fab_woven_req_yds,fab_knit_req_kg,fab_woven_fin_req_yds,fab_knit_fin_req_kg,fab_amount,avg,yarn_cons_qnty,yarn_amount,conv_req_qnty,conv_charge_unit,conv_amount,trim_cons,trim_rate,trim_amount,emb_amount,comar_rate,
			comar_amount,commis_rate,commis_amount,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted from wo_pre_cost_sum_dtls  where  job_no in ($book_nos)";
				//echo $sql_precost_fab_sum_dtls;die;
			$rID11=execute_query($sql_precost_fab_sum_dtls,1);
			if($flag==1) 
			{
				if($rID11) $flag=1; else $flag=0; 
			} 
			
			//----------------------------------------------------wo_pre_cost_trim_cost_dtls_his------------------------------------------------------------------------

			$sql_precost_trim_cost_dtls="insert into  wo_pre_cost_trim_cost_dtls_his(id,approved_no,pre_cost_trim_cost_dtls_id,job_no, trim_group,description,brand_sup_ref, cons_uom, cons_dzn_gmts, rate, amount, apvl_req, nominated_supp,cons_breack_down,inserted_by, insert_date,updated_by,update_date, status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,job_no, trim_group,description,brand_sup_ref, cons_uom, cons_dzn_gmts, rate, amount, apvl_req, nominated_supp,cons_breack_down,inserted_by, insert_date,updated_by,update_date, status_active,is_deleted from wo_pre_cost_trim_cost_dtls  where  job_no in ($book_nos)";
				//echo $sql_precost_trim_cost_dtls;die;
			$rID12=execute_query($sql_precost_trim_cost_dtls,1);
			if($flag==1) 
			{
				if($rID12) $flag=1; else $flag=0; 
			} 
			
//----------------------------------------------------wo_pre_cost_trim_co_cons_dtl_h------------------------------------------------------------------------

			$sql_precost_trim_co_cons_dtl="insert into   wo_pre_cost_trim_co_cons_dtl_h(id,approved_no,pre_cost_trim_co_cons_dtls_id,wo_pre_cost_trim_cost_dtls_id,job_no, po_break_down_id,item_size, cons, place, pcs,country_id)
				select	
				'', $approved_string_dtls, id,wo_pre_cost_trim_cost_dtls_id,job_no,po_break_down_id,item_size, cons,place, pcs,country_id from wo_pre_cost_trim_co_cons_dtls  where  job_no in ($book_nos)";
				//echo $sql_precost_trim_co_cons_dtl_;die;
			$rID13=execute_query($sql_precost_trim_co_cons_dtl,1);
			if($flag==1) 
			{
				if($rID13) $flag=1; else $flag=0; 
			} 
	//----------------------------------------------------wo_pre_cost_fab_con_cst_dtls_h------------------------------------------------------------------------

			$sql_precost_fab_con_cst_dtls="insert into   wo_pre_cost_fab_con_cst_dtls_h(id,approved_no,pre_cost_fab_conv_cst_dtls_id,job_no, fabric_description,cons_process,req_qnty,charge_unit,amount,color_break_down,charge_lib_id,inserted_by,insert_date,updated_by,update_date, status_active,is_deleted)
				select	
				'', $approved_string_dtls, id,job_no, fabric_description,cons_process,req_qnty,charge_unit,amount,color_break_down,charge_lib_id,inserted_by,insert_date,updated_by,update_date, status_active,is_deleted from wo_pre_cost_fab_conv_cost_dtls  where  job_no in ($book_nos)";
				//echo $sql_precost_fab_con_cst_dtls;die;
			$rID13=execute_query($sql_precost_fab_con_cst_dtls,1);
			if($flag==1) 
			{
				if($rID13) $flag=1; else $flag=0; 
			}
		}
		
		if($flag==1) $msg='19'; else $msg='21';
	}
	else
	{
		$booking_ids_all=explode(",",$booking_ids);
		
		$booking_ids=''; $app_ids='';
		
		foreach($booking_ids_all as $value)
		{
			$data = explode('**',$value);
			$booking_id=$data[0];
			$app_id=$data[1];
			
			if($booking_ids=='') $booking_ids=$booking_id; else $booking_ids.=",".$booking_id;
			if($app_ids=='') $app_ids=$app_id; else $app_ids.=",".$app_id;
		}
		$rID=sql_multirow_update("wo_pre_cost_mst","approved",2,"id",$booking_ids,0);
		if($rID) $flag=1; else $flag=0;
		
		$rID2=sql_multirow_update("approval_history","current_approval_status",0,"id",$approval_ids,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 

		$data=$user_id."*'".$pc_date_time."'";
		$rID3=sql_multirow_update("approval_history","un_approved_by*un_approved_date",$data,"id",$app_ids,1);
		if($flag==1) 
		{
			if($rID3) $flag=1; else $flag=0; 
		} 
		
		$response=$booking_ids;
		
		if($flag==1) $msg='20'; else $msg='22';
	}
	if($db_type==0)
		{ 
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo $msg."**".$response;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo $msg."**".$response;
			}
		}
	
	if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo $msg."**".$response;
			}
			else
			{
				oci_rollback($con); 
				echo $msg."**".$response;
			}
		}
	disconnect($con);
	die;
	
}

if($action=="img")
{
	echo load_html_head_contents("Image View", "../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<fieldset style="width:600px; margin-left:5px">
		<div style="width:100%; word-wrap:break-word" id="scroll_body">
             <table border="0" rules="all" width="100%" cellpadding="2" cellspacing="2">
             	<tr>
					<?php
					$i=0;
                    $sql="select image_location from common_photo_library where master_tble_id='$id' and form_name='knit_order_entry' and file_type=1";
					
                    $result=sql_select($sql);
                    foreach($result as $row)
                    {
						$i++;
                    ?>
                    <!--<td align="center"><?php echo $row[csf('image_location')];?></td>-->
                    	<td align="center"><img width="300px" height="180px" src="../../<?php echo $row[csf('image_location')];?>" /></td>
                    <?php
						if($i%2==0) echo "</tr><tr>";
                    }
                    ?>
                </tr>
            </table>
        </div>	
	</fieldset>     
<?php
exit();
}
/*if($action=="file")
{
	echo load_html_head_contents("File View", "../../", 1, 1,'','','');
	extract($_REQUEST);
	
?>
	<fieldset style="width:600px; margin-left:5px">
		<div style="width:100%; word-wrap:break-word" id="scroll_body">
             <table border="0" rules="all" width="100%" cellpadding="2" cellspacing="2">
             	<tr>
					<?php
					
					$i=0;
                    $sql="select image_location from common_photo_library where master_tble_id='$id' and form_name='quotation_entry' and file_type=2";
                    $result=sql_select($sql);
                    foreach($result as $row)
                    {
						$i++;
                    ?>
                    	<td width="100" align="center"><a href="../../<?php echo $row[csf('image_location')]; ?>"><img width="89" height="97" src="../../file_upload/blank_file.png"><br>File-<?php echo $i; ?></a></td>
                    <?php
						if($i%6==0) echo "</tr><tr>";
                    }
                    ?>
                </tr>
            </table>
        </div>	
	</fieldset>     
<?php
exit();
}*/

?>