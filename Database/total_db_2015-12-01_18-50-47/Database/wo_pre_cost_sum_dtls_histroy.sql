-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pre_cost_sum_dtls_histroy`
--

DROP TABLE IF EXISTS `wo_pre_cost_sum_dtls_histroy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pre_cost_sum_dtls_histroy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) DEFAULT NULL,
  `pre_cost_sum_dtls_id` int(11) DEFAULT NULL,
  `job_no` varchar(25) NOT NULL,
  `fab_yarn_req_kg` double NOT NULL,
  `fab_woven_req_yds` double NOT NULL,
  `fab_knit_req_kg` double NOT NULL,
  `fab_woven_fin_req_yds` double NOT NULL,
  `fab_knit_fin_req_kg` double NOT NULL,
  `fab_amount` double NOT NULL,
  `avg` varchar(30) NOT NULL,
  `yarn_cons_qnty` double NOT NULL,
  `yarn_amount` double NOT NULL,
  `conv_req_qnty` double NOT NULL,
  `conv_charge_unit` double NOT NULL,
  `conv_amount` double NOT NULL,
  `trim_cons` double NOT NULL,
  `trim_rate` double NOT NULL,
  `trim_amount` double NOT NULL,
  `emb_amount` double NOT NULL,
  `comar_rate` double NOT NULL,
  `comar_amount` double NOT NULL,
  `commis_rate` double NOT NULL,
  `commis_amount` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_no` (`job_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pre_cost_sum_dtls_histroy`
--

LOCK TABLES `wo_pre_cost_sum_dtls_histroy` WRITE;
/*!40000 ALTER TABLE `wo_pre_cost_sum_dtls_histroy` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_pre_cost_sum_dtls_histroy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
