<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Prices Rate Work Order

Functionality	:	
JS Functions	:
Created by		:	Saidul Reza
Creation date 	: 	21.10.2014
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/ 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Finish Production Entry Info","../", 1, 1, "",'1','');
?>

<script>

	var permission='<?php echo $permission; ?>';
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

	
	function set_exchange_rate(currence)
	{	// 1 for TK.
		if(currence==1)
		{
			$('#txt_exchange_rate').val(1);
			$('#txt_exchange_rate').attr('readonly', 1);
		}
		else
		{
			$('#txt_exchange_rate').val('');
			$('#txt_exchange_rate').removeAttr("readonly");
		}
	}
	
	
	
	function openmypage_systemid()
	{ 
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{
			var cbo_company_id = $('#cbo_company_id').val();
			var title = 'System ID Info';
			var page_link = 'requires/piece_rate_work_order_controller.php?cbo_company_id='+cbo_company_id+'&action=systemId_popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe',  page_link, title, 'width=850px,height=390px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var mst_id=this.contentDoc.getElementById("hidden_mst_id").value;
				//alert(mst_id);
				get_php_form_data(mst_id, "populate_price_rat_mst_form_data", "requires/piece_rate_work_order_controller" );
				show_list_view(mst_id,'show_price_rate_wo_listview','list_container_price_rate_wo','requires/piece_rate_work_order_controller','');
				
				show_list_view(mst_id, 'load_details_entry_single', 'details_entry_list_view', 'requires/piece_rate_work_order_controller', '');
				set_button_status(1, permission, 'fnc_prices_rate_wo',1);
				
			}
		}
	}
	
	
	
	
	
	function openmypage_service_provider()
	{
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{
			var cbo_company_id = $('#cbo_company_id').val();
			var title = 'Service Provider Info';
			var page_link = 'requires/piece_rate_work_order_controller.php?cbo_company_id='+cbo_company_id+'&action=service_provider_popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe',  page_link, title, 'width=850px,height=390px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var supplier_id=this.contentDoc.getElementById("hidden_supplier_id").value;
				var sp=supplier_id.split("__");
				$('#txt_service_provider_id').val(sp[0]);
				$('#txt_service_provider_name').val(sp[1]);
			}
		}
	}
	
	
	
	
	
	function openmypage_job_no(str)
	{		
			
		if (form_validation('cbo_company_id*cbo_order_source_'+str,'Company*Order Source')==false)
		{
			return;
		}
		else
		{
			var order_source=$('#cbo_order_source_'+str).val();
			var txt_history=$('#txt_history').val();
			var mst_id=$('#update_id').val();
			var details_update_id=job_id=order_id=buyer_id=item_id=job_no=0;

			rowCount = $('#details_entry_list_view tr').length;
			for(i=1; i <= rowCount; i++)
			{ 
				if(i==1)
				{
					if($('#details_update_id_'+i).val())
					{
					details_update_id=$('#details_update_id_'+i).val();
					job_no=$('#txtjobno_'+i).val();
					job_id=$('#txtjobid_'+i).val();
					order_id=$('#txtorderid_'+i).val();
					buyer_id=$('#txtbuyerid_'+i).val();
					item_id=$('#txtitemid_'+i).val();
					}
					
						
				}
				else
				{
					if($('#details_update_id_'+i).val())
					{
					details_update_id+=','+$('#details_update_id_'+i).val();
					
					job_no+=','+$('#txtjobno_'+i).val();
					job_id+=','+$('#txtjobid_'+i).val();
					order_id+=','+$('#txtorderid_'+i).val();
					buyer_id+=','+$('#txtbuyerid_'+i).val();
					item_id+=','+$('#txtitemid_'+i).val();
					}
				}
				
			}
			var cbo_company_id = $('#cbo_company_id').val();
			var title = 'Job Number Info';
			var page_link = 'requires/piece_rate_work_order_controller.php?odr_source='+order_source+'&job_id='+job_no+'&order_id='+order_id+'&buyer_id='+buyer_id+'&item_id='+item_id+'&txt_history='+txt_history+'&cbo_company_id='+cbo_company_id+'&mst_id='+mst_id+'&action=job_no_popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe',  page_link, title, 'width=850px,height=390px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var job_data=this.contentDoc.getElementById("txt_selected_id").value;
				show_list_view( job_data+'_~_'+order_source+'_~_'+mst_id+'_~_'+details_update_id+'_~_'+job_id, 'load_details_entry', 'details_entry_list_view', 'requires/piece_rate_work_order_controller', '');	
				if(mst_id!=""){set_button_status(1, permission, 'fnc_prices_rate_wo',1);}
			}
		}
	}
	
	
	function openmypage_wo_qty(str)
	{
			
		if (form_validation('txtjobno_'+str,'Cob Number')==false)
		{
			return;
		}
		else
		{
			var order_source=$('#cbo_order_source_'+str).val();
			var txt_job_no 		= $('#txtjobno_'+str).val();
			var txt_order_no 	= $('#txtorderno_'+str).val();
			var txt_buyer 		= $('#txtbuyer_'+str).val();
			var txt_item 		= $('#txtitem_'+str).val();
			var txt_item_id 	= $('#txtitemid_'+str).val();
			var txt_style 		= $('#txtstyle_'+str).val();
			
			var search_history 		= $('#txt_order_qty_history_'+str).val();
			
			var data=txt_job_no+'__'+txt_order_no+'__'+txt_buyer+'__'+txt_item_id+'__'+txt_item+'__'+txt_style;

			var title = 'Work Order Qty';
			var page_link = 'requires/piece_rate_work_order_controller.php?o_source='+order_source+'&search_history='+search_history+'&data='+data+'&action=wo_qty_popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe',  page_link, title, 'width=880px,height=400px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				
				var hidden_qty=this.contentDoc.getElementById("hidden_qty").value;
				var hidden_rate=this.contentDoc.getElementById("hidden_rate").value;
				var hidden_uom=this.contentDoc.getElementById("hidden_uom").value;
				var hidden_color=this.contentDoc.getElementById("hidden_color").value;
				var hidden_size=this.contentDoc.getElementById("hidden_size").value;
				var hidden_oqty=this.contentDoc.getElementById("hidden_oqty").value;
				var hidden_wo_qty_uom=this.contentDoc.getElementById("hidden_wo_qty_uom").value;
				
				var hidden_up_ids=this.contentDoc.getElementById("hidden_up_ids").value;
				
				
				
				
				var search_history=this.contentDoc.getElementById("hidden_search_history").value;
				$("#txt_order_qty_history_"+str).val(search_history+'~~'+hidden_oqty+'~~'+hidden_qty+'~~'+hidden_rate+'~~'+hidden_uom+'~~'+hidden_wo_qty_uom+'~~'+hidden_color+'~~'+hidden_size+'~~'+hidden_up_ids);

				var spq=hidden_wo_qty_uom.split(",");
				var spr=hidden_rate.split(",");
				var suom=hidden_uom.split(",");
				
				var total_qty=amount=0;
				for(i=0;i<spq.length; i++ )
				{
				total_qty+=spq[i]*1;	
				amount+=(spq[i]*1)*(spr[i]*1);	
				}
				var avarage=(amount/total_qty);		
				avarage=avarage.toFixed(2);	
				$("#txtwoqty_"+str).val(Math.round(total_qty));
				$("#txtavgrate_"+str).val(avarage);
				$("#txtdtlamount_"+str).val(amount.toFixed(2));
				$("#cbodtlsuom_"+str).val(suom[0]);
			}
		}
	}
	
	
	
	function generate_report_file(data,action)
	{
		window.open("requires/piece_rate_work_order_controller.php?data=" + data+'&action='+action, true );
	}
	
	function fnc_prices_rate_wo( operation )
	{ 
		rowCount = $('#details_entry_list_view tr').length;
		$("#tot_rows").val(rowCount);
		
		
		if(operation==4)
		{
			// var report_title=$( "div.form_caption" ).html();
			 generate_report_file($('#update_id').val(),'price_rate_wo_print');
			 return;
		}
			
		
		
		if(operation==2)
		{
			show_msg('13');
			return;
		}
		
		var fill_txt='update_id*txt_system_id*cbo_company_id*txt_service_provider_id*txt_wo_date*cbo_rate_for*txt_attention*cbo_currency*txt_exchange_rate*txt_remarks_mst*tot_rows';
		var validation_fill_order_source='';
		var validation_fill_wo_order_qty='';
		var tot_rows=$("#tot_rows").val()*1;
		for(i=1; i<= tot_rows; i++)
		{ 
			fill_txt+="*txt_order_qty_history_"+i+"*cbo_order_source_"+i+"*cbo_ord_rceve_comp_id_"+i+"*txtjobid_"+i+"*txtorderid_"+i+"*txtbuyerid_"+i+"*txtitemid_"+i+"*txtstyle_"+i+"*colortype_"+i+"*txtwoqty_"+i+"*cbodtlsuom_"+i+"*txtavgrate_"+i+"*txtdtlamount_"+i+"*txtremarks_"+i+"*details_update_id_"+i;
			validation_fill_order_source+="*cbo_order_source_"+i+"*cbo_ord_rceve_comp_id_"+i+"*colortype_"+i;
			if(validation_fill_wo_order_qty==''){validation_fill_wo_order_qty="txtwoqty_"+i;} else {validation_fill_wo_order_qty+="*txtwoqty_"+i;}
		}
		
		// alert(validation_fill_order_source);
		if( form_validation('cbo_company_id*txt_service_provider_name*txt_wo_date*cbo_rate_for*cbo_currency*txt_exchange_rate'+validation_fill_order_source,'company*service provider name*production date*rate for*currency*exchange rate*Order Source*Order Receiving Company*Rate Variables')==false )
		{
			return;
		}	
		else if( form_validation(validation_fill_wo_order_qty,'WO Qty')==false )
		{
			if(confirm("System will not save zero or blank wo qty.")==0)return;	
		}
		
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(fill_txt,"../");
		
		//alert (data);return;
	  freeze_window(operation);
	  http.open("POST","requires/piece_rate_work_order_controller.php",true);
	  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  http.send(data);
	  http.onreadystatechange = fnc_prices_rate_wo_reponse;
	}
	
	
	function fnc_prices_rate_wo_reponse()
	{
		if(http.readyState == 4) 
		{
			    // release_freezing();alert(http.responseText);return;
			var reponse=trim(http.responseText).split('**');
			  //alert(reponse[0]); release_freezing();return;
			show_msg(reponse[0]);
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_system_id').value = reponse[2];
				show_list_view(reponse[1],'show_price_rate_wo_listview','list_container_price_rate_wo','requires/piece_rate_work_order_controller','');
				show_list_view(reponse[1], 'load_details_entry_single', 'details_entry_list_view', 'requires/piece_rate_work_order_controller', '');
				set_button_status(0, permission, 'fnc_prices_rate_wo',1);
			}
			
			if(reponse[0]==1)
			{
				
				 show_list_view(reponse[1],'show_price_rate_wo_listview','list_container_price_rate_wo','requires/piece_rate_work_order_controller','');
				set_button_status(0, permission, 'fnc_prices_rate_wo',1);
			}
			
			
			
			release_freezing();
		}
	}



function open_terms_condition_popup(title)
{
	var update_id=document.getElementById('update_id').value;
	if (update_id=="")
	{
		alert("Save Work Order First")
		return;
	}	
	else
	{
		var page_link="requires/piece_rate_work_order_controller.php?action=terms_condition_popup&data="+update_id;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=720px,height=300px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
		}
	}
}

	
/*-------------------------------------------javascript start------------------------------------------------*/	
		function fnc_cutting_wages()
		{
			if(operation==2)
			{
				show_msg('13');
				return;
			}
			
			
			if( form_validation('cbo_company_id*cbo_bill_for*cbo_final_bill*cbo_shift*txt_week_date_from*txt_week_date_to','Company*Bill For*Final Bill*Shift*Week Date From*Week Date To')==false )
				{
					return;
				}	
				
			var fill_txt='cbo_company_id*cbo_bill_for*cbo_final_bill*cbo_location*cbo_division*cbo_department*cbo_shift*txt_week_date_from*txt_week_date_to*txt_emp_code*';	
				
				
				var data="action=save_update_delete&operation="+operation+get_submitted_data_string(fill_txt,"../");
				
				 //alert (data);return;
			  freeze_window(operation);
			  http.open("POST","requires/weekly_wages_bill_controller.php",true);
			  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			  http.send(data);
			  http.onreadystatechange = fnc_prices_rate_wo_reponse;
		}
		
		
		
	
        function calculate_date()
		{		
			var thisDate=($('#txt_week_date_from').val()).split('-');
			var in_date=thisDate[2]+'-'+thisDate[1]+'-'+thisDate[0];
			//var days=($('#days_required').val())-1;
			var days=5;
			var date = add_days(in_date,days);	
			var split_date=date.split('-');			
			var res_date=split_date[0]+'-'+split_date[1]+'-'+split_date[2];
			$('#txt_week_date_to').val(res_date);
		}
	
	
	
	
</script>


	<?php
        $company_arr = return_library_array("select id, company_name from lib_company order by company_name","id","company_name");
        $location_details = return_library_array("select id,location_name from lib_location order by location_name","id","location_name");
        $division_details = return_library_array("select id,division_name from lib_division order by division_name","id","division_name");
        $department_details = return_library_array("select id,department_name from lib_department order by department_name","id","department_name");
    ?>	
    


</head>
<body onLoad="set_hotkey()">
<div style="width:850px;">

<?php echo load_freeze_divs ("../",$permission); ?>
    <form name="cutting_wages_1" id="cutting_wages_1" autocomplete="off" >
        <fieldset>
        <legend>Weekly Wages Bill</legend>
     <fieldset>
    <div style=" width:800px; margin:0 auto;">   
    <table cellpadding="0" cellspacing="2" width="100%">
           <tr>
                <td colspan="3" align="right">System ID</td>
                <td colspan="4" align="left">
                    <input name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:150px" placeholder="Double Click to Search" readonly value="" onDblClick="popuppage_cutting_wages_system_id('search_cutting_wages_system_id.php','Cutting Wages Info'); return false" tabindex="12" /></td>
           </tr>
           
           <tr>
                <td colspan="7">&nbsp;</td>
           </tr>
           
            <tr>
                <td width="130" class="must_entry_caption">Company Name</td>
                <td width="170">
					<?php
                        echo create_drop_down( "cbo_company_id", 160, $company_arr,"", 1, "--Select Company--", 0, "" );
                    ?>
                </td>
                <td width="130" class="must_entry_caption">Bill For</td>
                <td width="170">
					<?php
							echo create_drop_down("cbo_bill_for", 160, $rate_for,"", 1,"-- Select --", 0,"","","20,30,35,40");
                    ?>
                <td width="130" class="must_entry_caption">Final Bill</td>	
                <td width="170">
					<?php
                        echo create_drop_down( "cbo_final_bill", 160, $yes_no,"", 1, "--Select Final Bill--", 0, "" );
                    ?>
              </td>	
            </tr>
            <tr>
                <td width="130">Location</td>
                <td width="170">
					<?php
                        echo create_drop_down( "cbo_location", 160, $location_details,"", 1, "--Select Location--", 0, "" );
                    ?>
                </td>
                <td width="130">Division Name</td>
                <td width="170">
					<?php
                        echo create_drop_down( "cbo_division", 160, $division_details,"", 1, "--Select Division--", 0, "" );
                    ?>
                </td>
                <td width="130">Department Name</td>
                <td width="170">
					<?php
                        echo create_drop_down( "cbo_department", 160, $department_details,"", 1, "--Select Department--", 0, "" );
                    ?>
                </td>
            </tr>
            <tr>
                <td width="130" class="must_entry_caption">Shift</td>
                <td width="170">
					<?php
                        echo create_drop_down( "cbo_shift", 160, $shift_name,"", 1, "--Select Shift--", 0, "" );
                    ?>
                </td>
                <td width="130" class="must_entry_caption">Week From</td>
                <td width="170">
                    <input type="text" name="txt_week_date_from" id="txt_week_date_from" class="datepicker" style="text-align:center;width:150px" onChange="calculate_date()" readonly />
                </td>
                <td width="130" class="must_entry_caption">Week To</td>
                <td width="170">
                    <input type="text" name="txt_week_date_to" id="txt_week_date_to" class="datepicker" style="text-align:center;width:150px" readonly />
                </td>
            </tr>
        </table>
        </div>
        </fieldset>
      
      
        <fieldset>
        <legend>New Entry</legend>
        
      <table cellpadding="0" cellspacing="2" width="850" border="0">
						<tr>
							<td width="130">Emp. ID No</td>
							<td width="170">
                    			<input type="text" name="txt_emp_card_no" id="txt_emp_card_no" class="text_boxes" style="width:150px" value="" placeholder="Double Click to Search" readonly onDblClick="openpage_searchemp('search_employee.php','Search Employee')" />
                                <input type="hidden" name="txt_emp_code" id="txt_emp_code">
                    		</td>
                            <td width="130" >Order No</td>
							<td width="470" colspan="4">
                            	<input name="txt_order_no" placeholder="Double Click to Search" id="txt_order_no" onDblClick="search_order('search_cutting_wages_order.php','Cutting Wages_Order_Info'); return false"  class="text_boxes" style="width:473px ">
                            </td>
						</tr>
                        				
						<tr>
                        	<td width="130">Emp. Name</td>
                            <td width="170">
                               <input type="text" name="txt_emp_name" id="txt_emp_name" class="text_boxes" style="width:150px"  tabindex="13" disabled />  
                            </td>
                            <td width="130">Buyer</td>
                            <td width="170">
								<?php
                                    echo create_drop_down( "cbo_buyer_name", 160, "select buyer_name,id from lib_buyer where is_deleted=0  and status_active=1 order by buyer_name","id,buyer_name", 1, "--Select Buyer--", 0, "" );
                                ?>
                            </td>
                            <td width="130">Cutting/Bill Qty</td>
							<td width="100">
                            	<input type="text" name="txt_cutting_bill_qty" id="txt_cutting_bill_qty" class="text_boxes" style="width:107px; text-align:right" tabindex="22"  disabled />
                            </td>
                            <td width="60">
								<?php
                                    echo create_drop_down( "cbo_cutting_bill_uom", 60, $unit_of_measurement,"",1, "--Select--", "","",0,"1,2" );
                                ?>
                            </td>                  
						</tr>
						<tr>
                        	<td width="130">Designation</td>
							<td width="170">
								<?php
                                    echo create_drop_down( "cbo_designation", 160, "select id,custom_designation from lib_designation where status_active=1 and is_deleted=0 order by id","id,custom_designation", 1, "--Select Designation--", 0, "" );
                                ?>
							</td>
                            <td width="130">Style Ref. No</td>
							<td width="170">
                            	<input type="text" name="txt_style_ref_no" id="txt_style_ref_no" class="text_boxes" style="width:150px" disabled tabindex="18"  />
                            </td>
                            <td width="130">WO Rate</td>
							<td width="100"><input type="text" name="txt_wo_rate" id="txt_wo_rate" class="text_boxes" tabindex="23" style="width:107px;text-align:right" disabled /></td>
                            <td width="60">
								<?php
                                    echo create_drop_down( "cbo_wo_rate_uom", 60,$unit_of_measurement,"",1, "--Select--", "","",0,"1,2" );
                                ?>
                            </td> 
                    	</tr>
                        <tr>
                        	<td width="130">Grade</td>
							<td width="170"><input type="text" name="txt_salary_grade" id="txt_salary_grade" class="text_boxes" style="width:150px"  tabindex="13" disabled /></td>
                            <td width="130">Gmt. Item Name</td>
							<td width="170">
                            	<input type="text" name="txt_gmt_item_name" id="txt_gmt_item_name" class="text_boxes" style="width:150px" disabled tabindex="18"  />
                                <input type="hidden" name="txt_gmt_item_id" id="txt_gmt_item_id"   class="text_boxes" style="width:140px ">
                            </td>
                            <td width="130">Amount</td>
                            <td width="110">
                            	<input type="text" name="txt_amount" id="txt_amount" class="text_boxes" tabindex="24" style="width:107px;text-align:right " disabled />
                                <input type="hidden" name="txt_amount_hidden" id="txt_amount_hidden" class="text_boxes" tabindex="24" style="width:107px;text-align:right " disabled />
                            </td>
                            <td>&nbsp;</td>
                    	</tr>
                        <tr>
                        	<td width="130">Salary</td>
							<td width="170"><input type="text" name="txt_salary" id="txt_salary" class="text_boxes" style="width:150px"  tabindex="13" disabled /></td>
                           	<td width="130">Apv. Order Qty.</td>
							<td width="170">
                            	<input type="text" name="txt_appv_wo_order_qty" id="txt_appv_wo_order_qty" class="text_boxes" style="width:150px" disabled tabindex="18"  />
                                <input type="hidden" name="txt_appv_wo_order_qty_hidden" id="txt_appv_wo_order_qty_hidden" class="text_boxes" style="width:150px" disabled tabindex="18"  />
                            </td>
                            <td width="130">Previous Bill Qty.</td>
                            <td width="100">
                            	<input type="text" name="txt_previous_bill_qty" id="txt_previous_bill_qty" class="text_boxes" tabindex="24" style="width:107px;text-align:right " disabled />
                                <input type="hidden" name="txt_previous_bill_qty_hidden" id="txt_previous_bill_qty_hidden" class="text_boxes" tabindex="24" style="width:107px;text-align:right " disabled />
                            </td>
                            <td width="60">
								<?php
                                    echo create_drop_down( "cbo_previous_bill_qty_uom",60,$unit_of_measurement,"",1, "--Select--", "","",0,"1,2" );
                                ?>
                            </td>
                    	</tr>		
                        <tr>
                        	<td width="130">Status</td>
							<td width="170">
								<?php
                                    echo create_drop_down( "cbo_po_status", 160,$po_status,"", 1, "--Select Status--", 0, "" );
                                ?>
                            </td>                       		
                            <td width="130">Rate Variables</td>
                            <td width="170" id="item_group_td_id">
								<?php
                                    echo create_drop_down( "cbo_wages_rate_variables", 160,$color_type,"", 1, "--Rate Variable--", 0, "" );
                                ?>
                            </td>
                            <td width="130">Yet To Bill Qty.</td>
                            <td width="100">
                            	<input type="text" name="txt_yet_to_bill_qty" id="txt_yet_to_bill_qty" class="text_boxes" tabindex="24" style="width:107px;text-align:right " disabled />
                                <input type="hidden" name="txt_yet_to_bill_qty_hidden" id="txt_yet_to_bill_qty_hidden" class="text_boxes" tabindex="24" style="width:107px;text-align:right " disabled />
                            </td>
                            <td width="60">
								<?php
                                    echo create_drop_down( "cbo_yet_to_bill_qty_uom", 60,$unit_of_measurement,"",1, "--Select--", "","",0,"1,2" );
                                ?>
                            </td>
                        </tr>
                        <tr><td colspan="7" height="8"></td></tr>
                		<tr>
                    		<td colspan="7" align="center" class="button_container">
								<?php
                                    echo load_submit_buttons($permission, "fnc_cutting_wages", 0,1,"reset_form('cutting_wages_1','list_container_cutting_wages','','','')",1);
                                ?>
                    		</td>
                 		</tr>
					</table>        
        </fieldset>      
        <div id="list_container_cutting_wages"></div>
		</fieldset>
   
	</form>
    </div>
</div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>