﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
 
include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------- Start-------------------------------------//
if($db_type==0) $select_field="group"; 
else if($db_type==2) $select_field="wm";
else $select_field="";//defined Later

//------------------------------------------Load Drop Down on Change---------------------------------------------//
if ($action=="load_supplier_dropdown")
{
	//echo $data;
	$data = explode('_',$data);
	
	if ($data[1]==0) echo create_drop_down( "cbo_supplier_id", 165, $blank_array,'', 1, '----Select----',0,0,0);
	
	if($data[1]==1)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =2",'id,supplier_name', 1, '----Select----',0,0,0);		
	}
	if($data[1]==2 || $data[1]==3 || $data[1]==13 || $data[1]==14)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =9",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==4)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(4,5)",'id,supplier_name', 1, '----Select----',0,0,0);
		
	}
	if($data[1]==5 || $data[1]==6 || $data[1]==7)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type=3",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==8)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 7",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==9 || $data[1]==10)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 6",'id,supplier_name', 1, '----Select----',0,0,0);
	} 
	if($data[1]==11)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 8",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==12)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(20,21,22,23,24,30,31,32,35,36,37,38,39)",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	
	exit();
}

if($action=="btb_lc_search")
{
	echo load_html_head_contents("BTB L/C Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		function js_set_value(id)
		{
			$('#hidden_btb_id').val(id);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:1000px;">
	<form name="searchscfrm"  id="searchscfrm">
		<fieldset style="width:100%;">
            <legend>Enter search words</legend>           
            	<table cellpadding="0" cellspacing="0" width="950" class="rpt_table">
                	<thead>
                    	<th>Item Category</th>
                    	<th>Company</th>
                        <th>Supplier</th>
                        <th>L/C Date</th>
                        <th>Enter</th>
                        <th>
                        	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        	<input type="hidden" name="id_field" id="id_field" value="" />
                        </th>
                    </thead>
                    <tr class="general">
                    	
                        <td> 
                             <?php echo create_drop_down( "cbo_item_category_id", 165, $item_category,'', 1, '--Select--',0,"",0); ?>  
                        </td>
                        <td>
                           <?php 
								echo create_drop_down( "txt_company_id",165,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, 'Select',0,"load_drop_down( 'btb_margin_lc_amendment_controller',this.value+'_'+document.getElementById('cbo_item_category_id').value,'load_supplier_dropdown','supplier_td' );",0); 
							?>  
                         </td>
                         <td align="center" id="supplier_td">
                          <?php echo create_drop_down( "cbo_supplier_id", 165,$blank_array,'', 0, '',0,0,0); ?>       
                          
                         </td>            
						<td> 
                        	 <input type="text" name="btb_start_date" id="btb_start_date" class="datepicker" style="width:70px;" />To
                             <input type="text" name="btb_end_date" id="btb_end_date" class="datepicker" style="width:70px;" />
                        </td>						
						<td id="search_by_td">
							<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                            <input type="hidden" id="hidden_btb_id" />
            			</td>                       
                         <td>
                 		  	<input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_company_id').value+'**'+document.getElementById('cbo_item_category_id').value+'**'+document.getElementById('cbo_supplier_id').value+'**'+document.getElementById('btb_start_date').value+'**'+document.getElementById('btb_end_date').value+'**'+document.getElementById('txt_search_common').value, 'create_btb_search_list_view', 'search_div', 'btb_margin_lc_amendment_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                         </td>
					</tr>
               </table>
               <table width="100%" style="margin-top:5px" align="center">
					<tr>
                    	<td colspan="5" id="search_div" align="center"></td>
                    </tr>
                </table> 
            </fieldset>
		</form>
	</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
 
}

if($action=="create_btb_search_list_view")
{
	$data=explode('**',$data);
	$company_id = $data[0];
	$item_category_id = $data[1];
	$supplier_id = $data[2];
	$lc_start_date = $data[3];
	$lc_end_date = $data[4];
	$system_id = $data[5]; 
	
	if($company_id==0)
	{
		echo 'Select Importer';die;
	}
	if($item_category_id==0)
	{
		echo 'Select Item Category';die;
	}
	
	if($company_id!=0) $company=$company_id;
	if($item_category_id!=0) $item_category=$item_category_id;
	if($supplier_id!=0) $supplier=$supplier_id; else $supplier='%%';
	if($system_id!='') $system_number=$system_id; else $system_number='%%';
	
	if($lc_start_date!='' && $lc_end_date!='')
	{
		if($db_type==0)
		{
			$date = "and application_date between '".change_date_format($lc_start_date,'yyyy-mm-dd')."' and '".change_date_format($lc_end_date,'yyyy-mm-dd')."'";
		}
		else if($db_type==2)
		{
			$date = "and application_date between '".change_date_format($lc_start_date,'','',1)."' and '".change_date_format($lc_end_date,'','',1)."'";
		}
	}
	else
	{
		$date = "";
	}
	
	if($db_type==0) $year_field="YEAR(insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	 
	$sql = "SELECT id, $year_field btb_prefix_number, btb_system_id, lc_number, supplier_id, application_date, last_shipment_date, lc_date, lc_value, item_category_id, supplier_id, importer_id FROM com_btb_lc_master_details WHERE btb_system_id like '".$system_number."' and importer_id = '".$company."' and supplier_id like '".$supplier."' and item_category_id = '".$item_category."' $date and status_active = 1 and is_deleted = 0 order by id";
	
	$supplier_lib=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name'); 
	 
	$arr=array(2=>$supplier_lib);
			
	echo  create_list_view("list_view", "Year,System Id,Supplier,L/C Number,L/C Date,L/C Value,Application Date,Last Ship Date", "60,70,150,150,100,120,100,100","950","240",0, $sql , "js_set_value", "id", "", 1, "0,0,supplier_id,0,0,0,0,0", $arr , "year,btb_prefix_number,supplier_id,lc_number,lc_date,lc_value,application_date,last_shipment_date", "",'','0,0,0,0,3,2,3,3') ;
		 
	exit();
}

if($action=='populate_data_from_btb_lc')
{
	$data_array=sql_select("select id, lc_number, supplier_id, importer_id, lc_date, application_date, last_shipment_date, lc_expiry_date, item_basis_id, lc_value, currency_id, issuing_bank_id, item_category_id, tenor, tolerance, inco_term_id, inco_term_place, delivery_mode_id, port_of_loading, port_of_discharge, remarks, partial_shipment, pi_id, pi_value, payterm_id from com_btb_lc_master_details where id='$data'"); 
	foreach ($data_array as $row)
	{  
		$file_no='';
		
		$nameArray=sql_select("select a.internal_file_no from com_export_lc a, com_btb_export_lc_attachment b where a.id=b.lc_sc_id and b.is_lc_sc=0 and b.import_mst_id='$data' and b.status_active=1 and b.is_deleted=0",1);
		if(count($nameArray)>0)
		{
			$file_no=$nameArray[0][csf("internal_file_no")];
		}
		else
		{
			$nameArray=sql_select("select a.internal_file_no from com_sales_contract a, com_btb_export_lc_attachment b where a.id=b.lc_sc_id and b.is_lc_sc=1 and b.import_mst_id='$data' and b.status_active=1 and b.is_deleted=0",1);
			$file_no=$nameArray[0][csf("internal_file_no")];
		}
		
		if($row[csf("pi_id")]!="")
		{
			if($db_type==0)
			{
				$pi_no=return_field_value("group_concat(pi_number)","com_pi_master_details","id in(".$row[csf("pi_id")].") and status_active=1 and is_deleted=0");
			}
			else
			{
				$pi_no=return_field_value("LISTAGG(cast(pi_number as varchar2(4000)), ',') WITHIN GROUP (ORDER BY id) as pi_number","com_pi_master_details","id in(".$row[csf("pi_id")].") and status_active=1 and is_deleted=0","pi_number");
			}
		}
		else
		{
			$pi_no="";
		}
		
 		echo "document.getElementById('txt_system_id').value 			= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_internal_file_no').value		= '".$file_no."';\n";
		echo "document.getElementById('txt_btb_lc_no').value 			= '".$row[csf("lc_number")]."';\n";
		echo "document.getElementById('cbo_importer_id').value 			= '".$row[csf("importer_id")]."';\n";
		echo "document.getElementById('cbo_supplier_id').value			= '".$row[csf("supplier_id")]."';\n";
		echo "document.getElementById('txt_lc_value').value 			= '".$row[csf("lc_value")]."';\n";
		echo "document.getElementById('cbo_currency_name').value 		= '".$row[csf("currency_id")]."';\n";
		echo "document.getElementById('cbo_item_category_id').value 	= '".$row[csf("item_category_id")]."';\n";		
		echo "document.getElementById('cbo_issuing_bank').value 		= '".$row[csf("issuing_bank_id")]."';\n";
		echo "document.getElementById('txt_lc_date').value 				= '".change_date_format($row[csf("lc_date")])."';\n";
		echo "document.getElementById('txt_last_shipment_date').value 	= '".change_date_format($row[csf("last_shipment_date")])."';\n";
		echo "document.getElementById('txt_expiry_date').value 			= '".change_date_format($row[csf("lc_expiry_date")])."';\n";
		echo "document.getElementById('txt_tolerance').value 			= '".$row[csf("tolerance")]."';\n";
		echo "document.getElementById('cbo_delevery_mode').value 		= '".$row[csf("delivery_mode_id")]."';\n";
		echo "document.getElementById('cbo_pay_term').value 			= '".$row[csf("payterm_id")]."';\n";
		echo "document.getElementById('txt_tenor').value 				= '".$row[csf("tenor")]."';\n";
		echo "document.getElementById('cbo_lc_basis_id').value 			= '".$row[csf("item_basis_id")]."';\n";
		
		echo "active_inactive(".$row[csf("item_basis_id")].");\n";
		
		echo "document.getElementById('txt_application_date').value 	= '".change_date_format($row[csf("application_date")])."';\n";
		echo "document.getElementById('txt_port_of_loading').value 		= '".$row[csf("port_of_loading")]."';\n";
		echo "document.getElementById('txt_port_of_discharge').value 	= '".$row[csf("port_of_discharge")]."';\n";
		echo "document.getElementById('txt_remarks').value 				= '".$row[csf("remarks")]."';\n";
		
		echo "document.getElementById('txt_amendment_no').value 			= '';\n";
		echo "document.getElementById('update_id').value 					= '';\n";
		echo "document.getElementById('txt_amendment_date').value 			= '';\n";
		echo "document.getElementById('txt_amendment_value').value 			= '';\n";
		echo "document.getElementById('hide_amendment_value').value 		= '';\n";
		echo "document.getElementById('cbo_value_change_by').value 			= '0';\n";
		echo "document.getElementById('hide_value_change_by').value 		= '';\n";
		
		echo "document.getElementById('txt_pi').value 						= '".$pi_no."';\n";
		echo "document.getElementById('txt_hidden_pi_id').value 			= '".$row[csf("pi_id")]."';\n";
		echo "document.getElementById('txt_pi_value').value 				= '".$row[csf("pi_value")]."';\n";
		
		/*echo "document.getElementById('txt_last_shipment_date_amnd').value	= '';\n";
		echo "document.getElementById('txt_expiry_date_amend').value 		= '';\n";
		echo "document.getElementById('cbo_delevery_mode_amnd').value 		= '0';\n";
		echo "document.getElementById('cbo_inco_term').value 				= '0';\n";  
		echo "document.getElementById('txt_inco_term_place').value 			= '';\n";
		echo "document.getElementById('cbo_partial_ship_id').value 			= '';\n";  
		echo "document.getElementById('txt_port_of_loading_amnd').value 	= '';\n";
		echo "document.getElementById('txt_port_of_discharge_amnd').value 	= '';\n";
		echo "document.getElementById('cbo_pay_term_amnd').value 			= '0';\n";
		echo "document.getElementById('txt_tenor_amnd').value 				= '';\n";
		echo "document.getElementById('txt_remarks_amnd').value 			= '';\n";*/
		
		echo "document.getElementById('txt_last_shipment_date_amnd').value	= '".change_date_format($row[csf("last_shipment_date")])."';\n";
		echo "document.getElementById('txt_expiry_date_amend').value 		= '".change_date_format($row[csf("lc_expiry_date")])."';\n";
		echo "document.getElementById('cbo_delevery_mode_amnd').value 		= '".$row[csf("delivery_mode_id")]."';\n";
		echo "document.getElementById('cbo_inco_term').value 				= '".$row[csf("inco_term_id")]."';\n";  
		echo "document.getElementById('txt_inco_term_place').value 			= '".$row[csf("inco_term_place")]."';\n";
		echo "document.getElementById('cbo_partial_ship_id').value 			= '".$row[csf("partial_shipment")]."';\n";  
		echo "document.getElementById('txt_port_of_loading_amnd').value 	= '".$row[csf("port_of_loading")]."';\n";
		echo "document.getElementById('txt_port_of_discharge_amnd').value 	= '".$row[csf("port_of_discharge")]."';\n";
		echo "document.getElementById('cbo_pay_term_amnd').value 			= '".$row[csf("payterm_id")]."';\n";
		echo "document.getElementById('txt_tenor_amnd').value 				= '".$row[csf("tenor")]."';\n";
		echo "document.getElementById('txt_remarks_amnd').value 			= '".$row[csf("remarks")]."';\n";
		
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_amendment_save',1);\n";
 		
		exit();
	}
}
// PI Popup
if ($action=="pi_popup")
{
 
	echo load_html_head_contents("PI Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>

    <script>
	
	 var btb_id='<?php echo $btb_id; ?>';
	
	 var selected_id = new Array, selected_name = new Array();
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_pi_row_id').value;
			if(old!="")
			{   
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{  
					js_set_value( old[i] ) 
				}
			}
		}

		function js_set_value( str ) 
		{
			if(btb_id!="")
			{
				var data=$('#txt_individual_id' + str).val()+"**"+btb_id;
				if(document.getElementById('search' + str).style.backgroundColor=='yellow')
				{
					var pi_no=$('#search' + str).find("td:eq(1)").text();
					var response = return_global_ajax_value( data, 'check_used_or_not', '', 'btb_margin_lc_amendment_controller');
					response=response.split("**");
					if(response[0]==1)
					{
						alert("Bellow Invoice Found Against PI- "+pi_no+". So You can't Detach it.\n Invoice No: "+response[1]);
						return false;
					}
				}
			}
			
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			 
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
				
			}
			else
			{
				for( var i = 0; i < selected_id.length; i++ )
				{
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id =''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}
		
		function reset_hide_field(type)
		{
			$('#txt_selected_id').val( '' );
			$('#txt_selected').val( '' );
			if(type==1)
			{
				$('#search_div').html( '' );
			}
		}
    </script>

</head>

<body>
<div align="center" style="width:900px;" >
	<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
		<fieldset style="width:890px;margin-left:10px">
            <table style="margin-top:10px" width="780" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
                <thead>  
                    <th width="120">Importer</th> 
                    <th width="120">Supplier</th>              	 
                    <th width="100">PI Number</th>
                    <th width="200">Date Range</th>  
                    <th><input type="reset" name="button" class="formbutton" value="Reset" onClick="reset_hide_field(1)" style="width:100px;"></th>
                    <input type="hidden" name="txt_item_category" id="txt_item_category" class="text_boxes" style="width:70px" value="<?php echo $item_category_id; ?>">  
                    <input type="hidden" name="txt_selected_id" id="txt_selected_id" value="" />
                    <input type="hidden" name="txt_selected"  id="txt_selected" width="650px" value="" />  
                </thead>
                <tr>
                    <td align="center">
                        <?php 
                            echo create_drop_down( "cbo_company_id", 165,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, '----Select----',0,"load_drop_down( 'btb_margin_lc_amendment_controller',this.value+'_'+document.getElementById('txt_item_category').value, 'load_supplier_dropdown', 'supplier_td' );",0); 
                        ?>  
                     
                     </td>
                     <td align="center" id="supplier_td">
                      <?php echo create_drop_down( "cbo_supplier_id", 165,$blank_array,'', 0, '',0,0,0); ?>       
                      
                     </td>
                    <td align="center">
                     <input type="text" name="txt_pi_no" id="txt_pi_no" class="text_boxes" style="width:120px">
                     </td>
                    <td align="center">
                      <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">To
                      <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
                     </td> 
                     <td align="center">
                     <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_pi_no').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_item_category').value+'_'+document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_supplier_id').value+'_'+'<?php echo $txt_hidden_pi_id; ?>', 'create_pi_search_list_view', 'search_div', 'btb_margin_lc_amendment_controller', 'setFilterGrid(\'list_view\',-1)');reset_hide_field(0);set_all();" style="width:100px;" />
                     </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1);  ?></td>
                </tr>
            </table>
			<div style="margin-top:10px" id="search_div"></div>
		</fieldset>   
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_pi_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!="") $pi_number="%".$data[0]."%"; else $pi_number = '%%';
	if ($data[1]!="" &&  $data[2]!="") $pi_date = "and pi_date between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'"; else $pi_date ="";
	$item_category_id =$data[3];
	if($data[4]!=0) $importer_id =$data[4]; else $importer_id='%%';
	if($data[5]!=0) $supplier_id =$data[5]; else $supplier_id='%%';
	 
	/*if($supplier_id==0)
	{
		echo 'Select Supplier'; 
		echo '<input type="hidden" name="txt_pi_row_id" id="txt_pi_row_id" value=""/>';
		die;
	}*/
	
	$all_pi_id=$data[6];
	$hidden_pi_id=explode(",",$all_pi_id);
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$supplier=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name'); 
	
	if($all_pi_id=="")
	{
		$sql= "select id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id from com_pi_master_details a where supplier_id like '".$supplier_id."' and importer_id like '".$importer_id."' and item_category_id = $item_category_id and pi_number like '$pi_number' $pi_date and status_active = 1 and is_deleted =0 and id not in(select pi_id from com_btb_lc_pi where status_active=1 and is_deleted=0) group by id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id order by pi_number";  
	}
	else
	{
		$sql= "select id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id from com_pi_master_details a where supplier_id like '".$supplier_id."' and importer_id like '".$importer_id."' and item_category_id = $item_category_id and pi_number like '$pi_number' $pi_date and status_active = 1 and is_deleted =0 and id not in(select pi_id from com_btb_lc_pi where status_active=1 and is_deleted=0) or id in($all_pi_id) group by id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id order by pi_number";   
	}
	
	?>	
		<div style="width:880px;" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table" id="tbl_list_search">
                <thead class="table_header" style="width:880px;">
                    <th width="30">SL No</th>
                    <th width="120">PI No</th>
                    <th width="90">PI Date</th>
                    <th width="100">Item Category</th>
                    <th width="120">Importer</th>
                    <th width="120">Supplier</th>
                    <th width="90">Last Ship Date</th>
                    <th width="80">HS Code</th>
                    <th width="100">PI Basis</th>
                </thead>
                <tbody class="table_body" style="width:880px; max-height:250px;">
                <?php 
                 $i=1; $pi_row_id="";
                 $nameArray=sql_select( $sql );
                 foreach ($nameArray as $selectResult)
                 {
                    if ($i%2==0)  
                        $bgcolor="#E9F3FF";
                    else
                        $bgcolor="#FFFFFF";	

					if(in_array($selectResult[csf('id')],$hidden_pi_id)) 
					{
						if($pi_row_id=="") $pi_row_id=$i; else $pi_row_id.=",".$i;
					}
            ?>
                    <tr height="20" bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
                        <td width="30" align="center"><?php echo "$i"; ?>
                         <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i; ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>	
                         <input type="hidden" name="txt_individual" id="txt_individual<?php echo $i; ?>" value="<?php echo $selectResult[csf('pi_number')]; ?>"/>
                        </td>	
                        <td width="120"><p><?php echo $selectResult[csf('pi_number')];?></p></td>
                        <td width="90"><?php echo change_date_format($selectResult[csf('pi_date')]);?></td> 
                        <td width="100"><?php echo $item_category[$selectResult[csf('item_category_id')]];  ?></td>
                        <td width="120"><?php echo $comp[$selectResult[csf('importer_id')]];  ?></td>
                        <td width="120"><?php echo $supplier[$selectResult[csf('supplier_id')]];  ?></td>
                        <td width="90"><?php echo change_date_format($selectResult[csf('last_shipment_date')]);  ?></td>	
                        <td width="80"><?php echo $selectResult[csf('hs_code')];  ?></td>
                        <td width="100"><?php echo $pi_basis[$selectResult[csf('pi_basis_id')]];  ?></td>
                    </tr>
                <?php
                $i++;
                 }
                 ?>
                 <input type="hidden" name="txt_pi_row_id" id="txt_pi_row_id" value="<?php echo $pi_row_id; ?>"/>	
            </tbody> 
        </table>
        <table width="840" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                        	<input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>
<?php	 
}

if($action=="check_used_or_not")
{
	$data=explode("**",$data);
	$pi_id=$data[0];
	$btb_id=$data[1];
	
	$sql="select a.invoice_no from com_import_invoice_mst a, com_import_invoice_dtls b where a.id=b.import_invoice_id and b.btb_lc_id=$btb_id and b.pi_id=$pi_id and b.status_active=1 and b.is_deleted=0 group by a.id, a.invoice_no";
	$data_array=sql_select($sql);
	$invoice_no='';
	if(count($data_array)>0)
	{
		foreach($data_array as $row)
		{
			if($invoice_no=="") $invoice_no=$row[csf('invoice_no')]; else $invoice_no.=", ".$row[csf('invoice_no')];
		}
		echo "1**".$invoice_no;
	}
	else
	{
		echo "0**";	
	}
	exit();
}

if ($action=="set_value_pi_select")
{
	$data=explode("**",$data);
	$pi_id=$data[0];
	$lc_value=$data[1];
	$currency_id=$data[2];
	
	$inc_dec=0; $amnd_value=0;
	
	$pi_value = return_field_value("sum(net_pi_amount)","com_pi_item_details","pi_id in($pi_id) and status_active=1 and is_deleted=0");
	
	if($currency_id==1)
		$pi_value=number_format($pi_value,$dec_place[4],'.','');
	else
		$pi_value=number_format($pi_value,$dec_place[5],'.','');
	
	if($pi_value>$lc_value)
	{
		$inc_dec=1;
		$amnd_value=$pi_value-$lc_value;
	}
	else if($pi_value<$lc_value)
	{
		$inc_dec=2;
		$amnd_value=$lc_value-$pi_value;
	}
	else
	{
		$inc_dec=0;
		$amnd_value=0;
	}
	
	if($currency_id==1)
		$amnd_value=number_format($amnd_value,$dec_place[4],'.','');
	else
		$amnd_value=number_format($amnd_value,$dec_place[5],'.','');
	
	echo "document.getElementById('txt_pi_value').value					= '".$pi_value."';\n";
	echo "document.getElementById('txt_amendment_value').value 			= '".$amnd_value."';\n";
	echo "document.getElementById('hide_amendment_value').value 		= '".$amnd_value."';\n";
	echo "document.getElementById('cbo_value_change_by').value 			= '".$inc_dec."';\n";
	echo "document.getElementById('hide_value_change_by').value 		= '".$inc_dec."';\n";
	
	exit();
}


//amendment popup
if($action=="amendment_popup")
{
	echo load_html_head_contents("BTB LC Amendment Form", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		function js_set_value(id)
		{
			$('#hidden_amendment_no').val(id);
			parent.emailwindow.hide();
		}	
    </script>
    <div align="center" style="width:100%; margin-top:10px">
        <input type="hidden" id="hidden_amendment_no" value="" />
        <?php
        $sql = "SELECT id, amendment_no, amendment_date, btb_lc_no, btb_lc_value FROM com_btb_lc_amendment WHERE btb_id='$btb_lc_id' and amendment_no<>0 and status_active=1 and is_deleted=0 and is_original=0 order by id";
                    
        echo  create_list_view("list_view", "Amendment No,Amendment Date,BTB LC No, LC Value", "110,100,150,130","600","250",0, $sql , "js_set_value", "id", "", 1, 0, 0, "amendment_no,amendment_date,btb_lc_no,btb_lc_value", "",'setFilterGrid(\'list_view\',-1)','0,3,0,2');
        ?>
    </div>
    <?php
	exit();
}


if($action=="get_amendment_data")
{
	$data_array = sql_select("SELECT btb_id, amendment_no, amendment_date, amendment_value, value_change_by, last_shipment_date, expiry_date, delivery_mode, inco_term, inco_term_place, partial_shipment, port_of_loading, port_of_discharge, pay_term, tenor, pi_id, pi_value, remarks FROM com_btb_lc_amendment WHERE id='$data' and status_active=1 and is_deleted=0");
						
	foreach ($data_array as $row)
	{ 	
		 $sql=sql_select("SELECT last_shipment_date, lc_expiry_date, delivery_mode_id, inco_term_id, inco_term_place, partial_shipment, port_of_loading, port_of_discharge, payterm_id, remarks, tenor, pi_id, pi_value, currency_id FROM com_btb_lc_master_details WHERE id='".$row[csf("btb_id")]."'");
		 
		/* $pi_currency_id=return_field_value("concat_ws('**',pi_id,currency_id)","com_btb_lc_master_details","id=$row[btb_id]");
		 $pi_currency_id=explode("**",$pi_currency_id);*/
		 
		 $pi_id=$sql[0][csf('pi_id')]; 
		 $currency_id=$sql[0][csf('currency_id')];
		 
 		 if($pi_id!="")
		 {
			if($db_type==0)
			{
				$pi_no=return_field_value("group_concat(pi_number)","com_pi_master_details","id in($pi_id) and status_active=1 and is_deleted=0");
			}
			else
			{
				$pi_no=return_field_value("LISTAGG(cast(pi_number as varchar2(4000)), ',') WITHIN GROUP (ORDER BY id) as pi_number","com_pi_master_details","id in($pi_id) and status_active=1 and is_deleted=0","pi_number");
			}
			
			$pi_value = return_field_value("sum(net_pi_amount)","com_pi_item_details","pi_id in($pi_id) and status_active=1 and is_deleted=0");
			
			if($currency_id==1)
				$pi_value=number_format($pi_value,$dec_place[4],'.','');
			else
				$pi_value=number_format($pi_value,$dec_place[5],'.','');
		 }
		 else
		 {
			$pi_no="";
			$pi_value="";
		 }

 		 echo "document.getElementById('txt_amendment_no').value 			= '".$row[csf("amendment_no")]."';\n";
 		 echo "document.getElementById('txt_amendment_date').value 			= '".change_date_format($row[csf("amendment_date")])."';\n";
		 echo "document.getElementById('txt_amendment_value').value 		= '".$row[csf("amendment_value")]."';\n";
		 echo "document.getElementById('hide_amendment_value').value 		= '".$row[csf("amendment_value")]."';\n";
		 echo "document.getElementById('cbo_value_change_by').value 		= '".$row[csf("value_change_by")]."';\n";
		 echo "document.getElementById('hide_value_change_by').value 		= '".$row[csf("value_change_by")]."';\n";
		 echo "document.getElementById('txt_last_shipment_date_amnd').value	= '".change_date_format($sql[0][csf("last_shipment_date")])."';\n";
		 echo "document.getElementById('txt_expiry_date_amend').value 		= '".change_date_format($sql[0][csf("lc_expiry_date")])."';\n";
		 echo "document.getElementById('cbo_delevery_mode_amnd').value 		= '".$sql[0][csf("delivery_mode_id")]."';\n";
		 echo "document.getElementById('cbo_inco_term').value 				= '".$sql[0][csf("inco_term_id")]."';\n";  
		 echo "document.getElementById('txt_inco_term_place').value 		= '".$sql[0][csf("inco_term_place")]."';\n";
		 echo "document.getElementById('cbo_partial_ship_id').value 		= '".$sql[0][csf("partial_shipment")]."';\n";  
		 echo "document.getElementById('txt_port_of_loading_amnd').value 	= '".$sql[0][csf("port_of_loading")]."';\n";
		 echo "document.getElementById('txt_port_of_discharge_amnd').value 	= '".$sql[0][csf("port_of_discharge")]."';\n";
		 echo "document.getElementById('cbo_pay_term_amnd').value 			= '".$sql[0][csf("payterm_id")]."';\n";
		 echo "document.getElementById('txt_tenor_amnd').value 				= '".$sql[0][csf("tenor")]."';\n";
		 echo "document.getElementById('txt_pi').value 						= '".$pi_no."';\n";
		 echo "document.getElementById('txt_hidden_pi_id').value 			= '".$pi_id."';\n";
		 echo "document.getElementById('txt_pi_value').value 				= '".$pi_value."';\n";
		 echo "document.getElementById('txt_remarks_amnd').value 			= '".$sql[0][csf("remarks")]."';\n";
		 echo "document.getElementById('update_id').value 					= '".$data."';\n";
 		 echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_amendment_save',1);\n";
 	}
	exit();
}


if ($action=="save_update_delete_amendment")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
 	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if (is_duplicate_field( "amendment_no", "com_btb_lc_amendment", "amendment_no=$txt_amendment_no and btb_id=$txt_system_id")==1)
		{
			echo "11**0"; 
			die;			
		}
		
		$user_id=''; $entry_date='';
		$data_array=sql_select("select lc_number, lc_value, last_shipment_date, lc_expiry_date, delivery_mode_id, inco_term_id, inco_term_place, partial_shipment, port_of_loading, port_of_discharge, payterm_id, tolerance, remarks, tenor, pi_id, pi_value, currency_id, updated_by, inserted_by, update_date, insert_date from com_btb_lc_master_details where id=$txt_system_id");
		
		$lc_value = $data_array[0][csf('lc_value')];
		$btb_lc_no = $data_array[0][csf('lc_number')];
		$currency_id = $data_array[0][csf('currency_id')];
		
		if($data_array[0][csf('updated_by')]==0)
		{
			$user_id=$data_array[0][csf('inserted_by')];
			$entry_date=$data_array[0][csf('insert_date')];
		}
		else 
		{
			$user_id=$data_array[0][csf('updated_by')];
			$entry_date=$data_array[0][csf('update_date')];
		}
		
		if($currency_id==1)
		{
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[4],'.','');
			$txt_amendment_value=number_format(str_replace("'", '',$txt_amendment_value),$dec_place[4],'.','');
		}
		else
		{
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[5],'.','');
			$txt_amendment_value=number_format(str_replace("'", '',$txt_amendment_value),$dec_place[5],'.','');
		}
		
		if( str_replace("'", '', $cbo_value_change_by)==1 )
			$new_lc_value = $lc_value+str_replace("'", '', $txt_amendment_value);
		else if(str_replace("'", '', $cbo_value_change_by)==2 )
			$new_lc_value = $lc_value-str_replace("'", '', $txt_amendment_value);
		else
			$new_lc_value = $lc_value;	
		
		$maximum_tolarence = 0; $minimum_tolarence = 0;
 		$maximum_tolarence = $new_lc_value+($new_lc_value*$data_array[0][csf('tolerance')])/100;
		$minimum_tolarence = $new_lc_value-($new_lc_value*$data_array[0][csf('tolerance')])/100;
 		
		$field_array_update="lc_value*max_lc_value*min_lc_value*last_shipment_date*lc_expiry_date*delivery_mode_id*payterm_id*inco_term_id*inco_term_place*partial_shipment* port_of_loading*port_of_discharge*remarks*tenor*pi_id*pi_value*updated_by*update_date";
		
		$data_array_update=$new_lc_value."*".$maximum_tolarence."*".$minimum_tolarence."*".$txt_last_shipment_date_amnd."*".$txt_expiry_date_amend."*".$cbo_delevery_mode_amnd."*".$cbo_pay_term_amnd."*".$cbo_inco_term."*".$txt_inco_term_place."*".$cbo_partial_ship_id."*".$txt_port_of_loading_amnd."*".$txt_port_of_discharge_amnd."*".$txt_remarks_amnd."*".$txt_tenor_amnd."*".$txt_hidden_pi_id."*'".$txt_pi_value."'*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$rID=sql_update("com_btb_lc_master_details",$field_array_update,$data_array_update,"id","".$txt_system_id."",1);
		if($rID) $flag=1; else $flag=0; */
		
		if (is_duplicate_field( "amendment_no", "com_btb_lc_amendment", "amendment_no=0 and btb_id=$txt_system_id")==0)
		{
			$id=return_next_id( "id", "com_btb_lc_amendment", 1 );
			$field_array="id, amendment_no, amendment_date, btb_id, btb_lc_no, btb_lc_value, amendment_value, value_change_by, last_shipment_date, expiry_date, delivery_mode, pay_term, inco_term, inco_term_place, partial_shipment, port_of_loading, port_of_discharge, remarks, tenor, pi_id, pi_value, is_original, inserted_by, insert_date";
			
			$amnd_date="";
			$data_array_amnd="(".$id.",0,'".$amnd_date."',".$txt_system_id.",'".$btb_lc_no."',".$lc_value.",0,0,'".$data_array[0][csf(last_shipment_date)]."','".$data_array[0][csf(lc_expiry_date)]."','".$data_array[0][csf(delivery_mode_id)]."','".$data_array[0][csf(payterm_id)]."','".$data_array[0][csf(inco_term_id)]."','".$data_array[0][csf(inco_term_place)]."','".$data_array[0][csf(partial_shipment)]."','".$data_array[0][csf(port_of_loading)]."','".$data_array[0][csf(port_of_discharge)]."','".$data_array[0][csf(remarks)]."','".$data_array[0][csf(tenor)]."','".$data_array[0][csf(pi_id)]."','".$data_array[0][csf(pi_value)]."',1,".$user_id.",'".$entry_date."')";
			
			/*$rID2=sql_insert("com_btb_lc_amendment",$field_array,$data_array_amnd,0);
			if($flag==1)
			{
				if($rID2) $flag=1; else $flag=0; 
			}*/
			
			$id+=1;
		}
		else
		{
			$id=return_next_id( "id", "com_btb_lc_amendment", 1 );
		}
			/*$data_array_amnd.=",(".$id.",".$txt_amendment_no.",".$txt_amendment_date.",".$txt_system_id.",'".$btb_lc_no."',".$new_lc_value.",".$txt_amendment_value.",".$cbo_value_change_by.",".$txt_last_shipment_date_amnd.",".$txt_expiry_date_amend.",".$cbo_delevery_mode_amnd.",".$cbo_pay_term_amnd.",".$cbo_inco_term.",".$txt_inco_term_place.",".$cbo_partial_ship_id.",".$txt_port_of_loading_amnd.",".$txt_port_of_discharge_amnd.",".$txt_remarks_amnd.",".$txt_tenor_amnd.",".$txt_hidden_pi_id.",'".$txt_pi_value."',0,".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";    */
			
		$shipment_date=strtotime($data_array[0][csf('last_shipment_date')]);
		$shipment_date_amnd=strtotime(str_replace("'","",$txt_last_shipment_date_amnd));
		$expiry_date=strtotime($data_array[0][csf('lc_expiry_date')]);
		$expiry_date_amnd=strtotime(str_replace("'","",$txt_expiry_date_amend));

		$field_array_amnd=""; $data_array_amnd2="";
		
		$field_array_amnd="id, amendment_no, amendment_date, btb_id, btb_lc_no, btb_lc_value, amendment_value, value_change_by";
		$data_array_amnd2="(".$id.",".$txt_amendment_no.",".$txt_amendment_date.",".$txt_system_id.",'".$btb_lc_no."','".$new_lc_value."','".$txt_amendment_value."',".$cbo_value_change_by;
		
		if($data_array[0][csf('pi_id')]!=str_replace("'","",$txt_hidden_pi_id))
		{
			$field_array_amnd.=",pi_id,pi_value";
			$data_array_amnd2.=",".$txt_hidden_pi_id.",'".$txt_pi_value."'";
			
			//----------Insert Data in  com_btb_lc_pi Table----------------------------------------
			/*$delete=execute_query( "delete from com_btb_lc_pi where com_btb_lc_master_details_id = $txt_system_id",0);
			if($flag==1) 
			{
				if($delete) $flag=1; else $flag=0; 
			} */
			
			if(str_replace("'","",$txt_hidden_pi_id)!="")
			{
				$data_array2="";
				$tag_pi=explode(',',str_replace("'","",$txt_hidden_pi_id));
				$id_lbtb_lc_pi=return_next_id( "id","com_btb_lc_pi", 1 );
				$field_array2="id, com_btb_lc_master_details_id, pi_id, inserted_by, insert_date";
				for($i=0; $i<count($tag_pi); $i++)
				{  
					if($i==0) $add_comma=""; else $add_comma=",";
					$data_array2.="$add_comma(".$id_lbtb_lc_pi.",".$txt_system_id.",".$tag_pi[$i].",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
					$id_lbtb_lc_pi++;
				}
			
				/*$btb_pi=sql_insert("com_btb_lc_pi",$field_array2,$data_array2,0);
				if($flag==1) 
				{
					if($btb_pi) $flag=1; else $flag=0; 
				} */
			}
		}
		
		if($shipment_date!=$shipment_date_amnd)
		{
			$field_array_amnd.=",last_shipment_date";
			$data_array_amnd2.=",".$txt_last_shipment_date_amnd;
		}
		
		if($expiry_date!=$expiry_date_amnd)
		{
			$field_array_amnd.=",expiry_date";
			$data_array_amnd2.=",".$txt_expiry_date_amend;
		}
		
		if($data_array[0][csf('delivery_mode_id')]!=str_replace("'","",$cbo_delevery_mode_amnd))
		{
			$field_array_amnd.=",delivery_mode";
			$data_array_amnd2.=",".$cbo_delevery_mode_amnd;
		}
		
		if($data_array[0][csf('payterm_id')]!=str_replace("'","",$cbo_pay_term_amnd))
		{
			$field_array_amnd.=",pay_term";
			$data_array_amnd2.=",".$cbo_pay_term_amnd;
		}
		
		if($data_array[0][csf('inco_term_id')]!=str_replace("'","",$cbo_inco_term))
		{
			$field_array_amnd.=",inco_term";
			$data_array_amnd2.=",".$cbo_inco_term;
		}
		
		if($data_array[0][csf('inco_term_place')]!=str_replace("'","",$txt_inco_term_place))
		{
			$field_array_amnd.=",inco_term_place";
			$data_array_amnd2.=",".$txt_inco_term_place;
		}
		
		if($data_array[0][csf('partial_shipment')]!=str_replace("'","",$cbo_partial_ship_id))
		{
			$field_array_amnd.=",partial_shipment";
			$data_array_amnd2.=",".$cbo_partial_ship_id;
		}
		
		if($data_array[0][csf('port_of_loading')]!=str_replace("'","",$txt_port_of_loading_amnd))
		{
			$field_array_amnd.=",port_of_loading";
			$data_array_amnd2.=",".$txt_port_of_loading_amnd;
		}
		
		if($data_array[0][csf('port_of_discharge')]!=str_replace("'","",$txt_port_of_discharge_amnd))
		{
			$field_array_amnd.=",port_of_discharge";
			$data_array_amnd2.=",".$txt_port_of_discharge_amnd;
		}
		
		if($data_array[0][csf('remarks')]!=str_replace("'","",$txt_remarks_amnd))
		{
			$field_array_amnd.=",remarks";
			$data_array_amnd2.=",".$txt_remarks_amnd;
		}
		
		if($data_array[0][csf('tenor')]!=str_replace("'","",$txt_tenor_amnd))
		{
			$field_array_amnd.=",tenor";
			$data_array_amnd2.=",".$txt_tenor_amnd;
		}
		
		$field_array_amnd.=",inserted_by, insert_date";
		$data_array_amnd2.=",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";

		$rID=sql_update("com_btb_lc_master_details",$field_array_update,$data_array_update,"id","".$txt_system_id."",1);
		if($rID) $flag=1; else $flag=0; 
		
		if (is_duplicate_field( "amendment_no", "com_btb_lc_amendment", "amendment_no=0 and btb_id=$txt_system_id")==0)
		{
			$rID2=sql_insert("com_btb_lc_amendment",$field_array,$data_array_amnd,0);
			if($flag==1)
			{
				if($rID2) $flag=1; else $flag=0; 
			}
		}
		
		if($data_array[0][csf('pi_id')]!=str_replace("'","",$txt_hidden_pi_id))
		{
			$delete=execute_query( "delete from com_btb_lc_pi where com_btb_lc_master_details_id = $txt_system_id",0);
			if($flag==1) 
			{
				if($delete) $flag=1; else $flag=0; 
			} 
			if(str_replace("'","",$txt_hidden_pi_id)!="")
			{
				$btb_pi=sql_insert("com_btb_lc_pi",$field_array2,$data_array2,0);
				if($flag==1) 
				{
					if($btb_pi) $flag=1; else $flag=0; 
				} 
			}
		}
		//echo "insert into com_btb_lc_amendment (".$field_array_amnd.") values ".$data_array_amnd2;die;
		$rID3=sql_insert("com_btb_lc_amendment",$field_array_amnd,$data_array_amnd2,1);
		if($flag==1)
		{
			if($rID3) $flag=1; else $flag=0; 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**0**".str_replace("'", '', $txt_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**".str_replace("'", '', $txt_system_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "0**0**".str_replace("'", '', $txt_system_id);
			}
			else
			{
				oci_rollback($con); 
				echo "5**0**".str_replace("'", '', $txt_system_id);
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$last_amendment_id=return_field_value("max(id)","com_btb_lc_amendment","btb_id=$txt_system_id");
		
		if($last_amendment_id!=str_replace("'", '', $update_id))
		{
			echo "14**1"; 
			die;
		}
		
		if (is_duplicate_field( "id", "com_btb_lc_amendment", "amendment_no=$txt_amendment_no and btb_id=$txt_system_id and id<>$update_id")==1)
		{
			echo "11**1"; 
			die;			
		}
		
		$data_array=sql_select("select lc_number, lc_value, last_shipment_date, lc_expiry_date, delivery_mode_id, inco_term_id, inco_term_place, partial_shipment, port_of_loading, port_of_discharge, payterm_id, tolerance, remarks, tenor, pi_id, pi_value, currency_id from com_btb_lc_master_details where id=$txt_system_id");
		
		$lc_value = $data_array[0][csf('lc_value')];
		$btb_lc_no = $data_array[0][csf('lc_number')];
		$currency_id = $data_array[0][csf('currency_id')];
		
		if($currency_id==1)
		{
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[4],'.','');
			$txt_amendment_value=number_format(str_replace("'", '',$txt_amendment_value),$dec_place[4],'.','');
		}
		else
		{
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[5],'.','');
			$txt_amendment_value=number_format(str_replace("'", '',$txt_amendment_value),$dec_place[5],'.','');
		}

		if(str_replace("'", '', $hide_value_change_by)==1) $lc_value=$lc_value-str_replace("'", '', $hide_amendment_value); 
		else if(str_replace("'", '', $hide_value_change_by)==2) $lc_value=$lc_value+str_replace("'", '', $hide_amendment_value);
		else $lc_value = $lc_value;	

		if( str_replace("'", '', $cbo_value_change_by)==1 )
			$new_lc_value = $lc_value+str_replace("'", '', $txt_amendment_value);
		else if(str_replace("'", '', $cbo_value_change_by)==2 )
			$new_lc_value = $lc_value-str_replace("'", '', $txt_amendment_value);
		else
			$new_lc_value = $lc_value;
		
		$maximum_tolarence = 0; $minimum_tolarence = 0;
 		$maximum_tolarence = $new_contract_value+($new_contract_value*$data_array[0][csf('tolerance')])/100;
		$minimum_tolarence = $new_contract_value-($new_contract_value*$data_array[0][csf('tolerance')])/100;
 		
		//update BTB lc table
		$field_array_update="lc_value*max_lc_value*min_lc_value*last_shipment_date*lc_expiry_date*delivery_mode_id*payterm_id*inco_term_id*inco_term_place*partial_shipment* port_of_loading*port_of_discharge*remarks*tenor*pi_id*pi_value*updated_by*update_date";
		
		$data_array_update=$new_lc_value."*".$maximum_tolarence."*".$minimum_tolarence."*".$txt_last_shipment_date_amnd."*".$txt_expiry_date_amend."*".$cbo_delevery_mode_amnd."*".$cbo_pay_term_amnd."*".$cbo_inco_term."*".$txt_inco_term_place."*".$cbo_partial_ship_id."*".$txt_port_of_loading_amnd."*".$txt_port_of_discharge_amnd."*".$txt_remarks_amnd."*".$txt_tenor_amnd."*".$txt_hidden_pi_id."*'".$txt_pi_value."'*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$rID=sql_update("com_btb_lc_master_details",$field_array_update,$data_array_update,"id","".$txt_system_id."",0);
		if($rID) $flag=1; else $flag=0;*/ 
		
		$shipment_date=strtotime($data_array[0][csf('last_shipment_date')]);
		$shipment_date_amnd=strtotime(str_replace("'","",$txt_last_shipment_date_amnd));
		$expiry_date=strtotime($data_array[0][csf('lc_expiry_date')]);
		$expiry_date_amnd=strtotime(str_replace("'","",$txt_expiry_date_amend));

		$field_array_amnd=""; $data_array_amnd2="";
		/*
		$field_array="amendment_no*amendment_date*btb_id*btb_lc_value*amendment_value*value_change_by*last_shipment_date*expiry_date*delivery_mode*pay_term* inco_term*inco_term_place*partial_shipment*port_of_loading*port_of_discharge*remarks*tenor*pi_id*pi_value*updated_by*update_date";	
		
		$data_array=$txt_amendment_no."*".$txt_amendment_date."*".$txt_system_id."*".$new_lc_value."*".$txt_amendment_value."*".$cbo_value_change_by."*".$txt_last_shipment_date_amnd."*".$txt_expiry_date_amend."*".$cbo_delevery_mode_amnd."*".$cbo_pay_term_amnd."*".$cbo_inco_term."*".$txt_inco_term_place."*".$cbo_partial_ship_id."*".$txt_port_of_loading_amnd."*".$txt_port_of_discharge_amnd."*".$txt_remarks_amnd."*".$txt_tenor_amnd."*".$txt_hidden_pi_id."*'".$txt_pi_value."'*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";*/
		
		$field_array_amnd="amendment_date*btb_lc_no*btb_lc_value*amendment_value*value_change_by*updated_by*update_date";
		$data_array_amnd2=$txt_amendment_date."*'".$btb_lc_no."'*'".$new_lc_value."'*'".$txt_amendment_value."'*".$cbo_value_change_by."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		if($data_array[0][csf('pi_id')]!=str_replace("'","",$txt_hidden_pi_id))
		{
			$field_array_amnd.="*pi_id*pi_value";
			$data_array_amnd2.="*".$txt_hidden_pi_id."*'".$txt_pi_value."'";
			
			//----------Insert Data in  com_btb_lc_pi Table----------------------------------------
			/*$delete=execute_query( "delete from com_btb_lc_pi where com_btb_lc_master_details_id = $txt_system_id",0);
			if($flag==1) 
			{
				if($delete) $flag=1; else $flag=0; 
			} */
			
			if(str_replace("'","",$txt_hidden_pi_id)!="")
			{
				$data_array2="";
				$tag_pi=explode(',',str_replace("'","",$txt_hidden_pi_id));
				
				$field_array2="id, com_btb_lc_master_details_id, pi_id, inserted_by, insert_date";
				for($i=0; $i<count($tag_pi); $i++)
				{  
					if($id_lbtb_lc_pi=="") {$id_lbtb_lc_pi=return_next_id( "id","com_btb_lc_pi", 1 ); }
					if($i==0) $add_comma=""; else $add_comma=",";
					$data_array2.="$add_comma(".$id_lbtb_lc_pi.",".$txt_system_id.",".$tag_pi[$i].",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
					$id_lbtb_lc_pi++;
				}
			
			/*	$btb_pi=sql_insert("com_btb_lc_pi",$field_array2,$data_array2,0);
				if($flag==1) 
				{
					if($btb_pi) $flag=1; else $flag=0; 
				} */
			}
		}
		
		if($shipment_date!=$shipment_date_amnd)
		{
			$field_array_amnd.="*last_shipment_date";
			$data_array_amnd2.="*".$txt_last_shipment_date_amnd;
		}
		
		if($expiry_date!=$expiry_date_amnd)
		{
			$field_array_amnd.="*expiry_date";
			$data_array_amnd2.="*".$txt_expiry_date_amend;
		}
		
		if($data_array[0][csf('delivery_mode_id')]!=str_replace("'","",$cbo_delevery_mode_amnd))
		{
			$field_array_amnd.="*delivery_mode";
			$data_array_amnd2.="*".$cbo_delevery_mode_amnd;
		}
		
		if($data_array[0][csf('payterm_id')]!=str_replace("'","",$cbo_pay_term_amnd))
		{
			$field_array_amnd.="*pay_term";
			$data_array_amnd2.="*".$cbo_pay_term_amnd;
		}
		
		if($data_array[0][csf('inco_term_id')]!=str_replace("'","",$cbo_inco_term))
		{
			$field_array_amnd.="*inco_term";
			$data_array_amnd2.="*".$cbo_inco_term;
		}
		
		if($data_array[0][csf('inco_term_place')]!=str_replace("'","",$txt_inco_term_place))
		{
			$field_array_amnd.="*inco_term_place";
			$data_array_amnd2.="*".$txt_inco_term_place;
		}
		
		if($data_array[0][csf('partial_shipment')]!=str_replace("'","",$cbo_partial_ship_id))
		{
			$field_array_amnd.="*partial_shipment";
			$data_array_amnd2.="*".$cbo_partial_ship_id;
		}
		
		if($data_array[0][csf('port_of_loading')]!=str_replace("'","",$txt_port_of_loading_amnd))
		{
			$field_array_amnd.="*port_of_loading";
			$data_array_amnd2.="*".$txt_port_of_loading_amnd;
		}
		
		if($data_array[0][csf('port_of_discharge')]!=str_replace("'","",$txt_port_of_discharge_amnd))
		{
			$field_array_amnd.="*port_of_discharge";
			$data_array_amnd2.="*".$txt_port_of_discharge_amnd;
		}
		
		if($data_array[0][csf('remarks')]!=str_replace("'","",$txt_remarks_amnd))
		{
			$field_array_amnd.="*remarks";
			$data_array_amnd2.="*".$txt_remarks_amnd;
		}
		
		if($data_array[0][csf('tenor')]!=str_replace("'","",$txt_tenor_amnd))
		{
			$field_array_amnd.="*tenor";
			$data_array_amnd2.="*".$txt_tenor_amnd;
		}
		
		$rID=sql_update("com_btb_lc_master_details",$field_array_update,$data_array_update,"id","".$txt_system_id."",0);
		if($rID) $flag=1; else $flag=0;
		
		$rID2=sql_update("com_btb_lc_amendment",$field_array_amnd,$data_array_amnd2,"id","".$update_id."",1);
		if($flag==1)
		{
			if($rID2) $flag=1; else $flag=0; 
		}
		
		if($data_array[0][csf('pi_id')]!=str_replace("'","",$txt_hidden_pi_id))
		{
			//----------Insert Data in  com_btb_lc_pi Table----------------------------------------
			$delete=execute_query( "delete from com_btb_lc_pi where com_btb_lc_master_details_id = $txt_system_id",0);
			if($flag==1) 
			{
				if($delete) $flag=1; else $flag=0; 
			} 
			
			if(str_replace("'","",$txt_hidden_pi_id)!="")
			{
				$btb_pi=sql_insert("com_btb_lc_pi",$field_array2,$data_array2,0);
				if($flag==1) 
				{
					if($btb_pi) $flag=1; else $flag=0; 
				} 
			}
		}
	//	echo "5**1**".$flag;die;
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**0**".str_replace("'", '', $txt_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**1**".str_replace("'", '', $txt_system_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "1**0**".str_replace("'", '', $txt_system_id);
			}
			else
			{
				oci_rollback($con); 
				echo "6**1**".str_replace("'", '', $txt_system_id);
			}
		}
		disconnect($con);
		die;
	}
	
}

?>


 