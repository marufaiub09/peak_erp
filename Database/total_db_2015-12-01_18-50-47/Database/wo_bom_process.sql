-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_bom_process`
--

DROP TABLE IF EXISTS `wo_bom_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_bom_process` (
  `id` int(11) NOT NULL DEFAULT '0',
  `job_no` varchar(25) DEFAULT NULL,
  `po_break_down_id` int(11) DEFAULT NULL,
  `color_size_table_id` int(11) DEFAULT NULL,
  `item_number_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `color_number_id` int(11) DEFAULT NULL,
  `size_number_id` int(11) DEFAULT NULL,
  `order_quantity` int(11) DEFAULT NULL,
  `plan_cut_qnty` int(11) DEFAULT NULL,
  `kint_fin_fab_qnty` double DEFAULT NULL,
  `kint_fin_fab_qnty_prod` double NOT NULL,
  `kint_fin_fab_qnty_purc` double NOT NULL,
  `kint_fin_fab_qnty_buysu` double NOT NULL,
  `kint_grey_fab_qnty` double NOT NULL,
  `kint_grey_fab_qnty_prod` double NOT NULL,
  `kint_grey_fab_qnty_purc` double NOT NULL,
  `kint_grey_fab_qnty_buysu` double NOT NULL,
  `kint_rate` double NOT NULL,
  `kint_amount` double DEFAULT NULL,
  `woven_fin_fab_qnty` double DEFAULT NULL,
  `woven_fin_fab_qnty_prod` double NOT NULL,
  `woven_fin_fab_qnty_purc` double NOT NULL,
  `woven_fin_fab_qnty_buysu` double NOT NULL,
  `woven_grey_fab_qnty` double DEFAULT NULL,
  `woven_grey_fab_qnty_prod` double NOT NULL,
  `woven_grey_fab_qnty_purc` double NOT NULL,
  `woven_grey_fab_qnty_buysu` double NOT NULL,
  `woven_rate` double DEFAULT NULL,
  `woven_amount` double DEFAULT NULL,
  `yarn_qnty` double DEFAULT NULL,
  `yarn_rate` double DEFAULT NULL,
  `yarn_amount` double DEFAULT NULL,
  `conv_qnty` double DEFAULT NULL,
  `conv_rate` double DEFAULT NULL,
  `conv_amount` double DEFAULT NULL,
  `trim_qty` double DEFAULT NULL,
  `trim_rate` double DEFAULT NULL,
  `trim_amount` double DEFAULT NULL,
  `emb_cons` double DEFAULT NULL,
  `emb_rate` double DEFAULT NULL,
  `emb_amount` double DEFAULT NULL,
  `wash_cons` double DEFAULT NULL,
  `wash_rate` double DEFAULT NULL,
  `wash_amount` double DEFAULT NULL,
  `commercial_rate` double DEFAULT NULL,
  `commercial_amount` double DEFAULT NULL,
  `commision_rate` double DEFAULT NULL,
  `commision_amount` double DEFAULT NULL,
  `lab_test_rate` double DEFAULT NULL,
  `lab_test_amount` double DEFAULT NULL,
  `inspection_rate` double DEFAULT NULL,
  `inspection_amount` double DEFAULT NULL,
  `cm_cost_rate` double DEFAULT NULL,
  `cm_cost_amount` double DEFAULT NULL,
  `freight_rate` double DEFAULT NULL,
  `freight_amount` double DEFAULT NULL,
  `currier_rate` double DEFAULT NULL,
  `currier_amount` double DEFAULT NULL,
  `certificate_rate` double DEFAULT NULL,
  `certificate_amount` double DEFAULT NULL,
  `common_oh_rate` double DEFAULT NULL,
  `common_oh_amount` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_bom_process`
--

LOCK TABLES `wo_bom_process` WRITE;
/*!40000 ALTER TABLE `wo_bom_process` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_bom_process` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
