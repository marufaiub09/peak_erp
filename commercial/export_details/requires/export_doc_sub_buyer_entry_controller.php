<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
 
//--------------------------- Start-------------------------------------//

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  
	exit();	
}


if($action=="populate_acc_loan_no_data")
{
	$data=explode("**",$data);
	$acc_type=$data[0];
	$rowID=$data[1];
	
	$sql="select account_no from lib_bank_account where account_type=$acc_type";
	$nameArray=sql_select($sql);
	echo "$('#txt_ac_loan_no_".$rowID."').removeAttr('readonly');\n";
 	echo "$('#txt_ac_loan_no_".$rowID."').val('');\n";
	foreach($nameArray as $row)
	{
		echo "$('#txt_ac_loan_no_".$rowID."').attr('readonly','readonly');\n";
		echo "$('#txt_ac_loan_no_".$rowID."').val('".$row[csf("account_no")]."');\n";
	}
	exit();
}


/*if($action=="transaction_add_row")
{
	$rowNo = $data+1;
	echo '<tr id="tr'.$rowNo.'">
			<td>'.create_drop_down( "cbo_account_head_".$rowNo, 200, $commercial_head,"", 1, "-- Select --", $selected, "get_php_form_data(this.value+'**'+".$rowNo.", 'populate_acc_loan_no_data', 'requires/export_doc_sub_buyer_entry_controller' );",0,"" ).'</td>							
			<td><input type="text" id="txt_ac_loan_no_'.$rowNo.'" name="txt_ac_loan_no[]" class="text_boxes" style="width:100px" /></td>
			<td><input type="text" id="txt_domestic_curr_'.$rowNo.'" name="txt_domestic_curr[]" class="text_boxes_numeric" style="width:100px" onkeyup="fn_calculate(this.id,'.$rowNo.')" /></td>
			<td><input type="text" id="txt_conversion_rate_'.$rowNo.'" name="txt_conversion_rate[]" class="text_boxes_numeric" style="width:100px" onkeyup="fn_calculate(this.id,'.$rowNo.')" /></td>
			<td><input type="text" id="txt_lcsc_currency_'.$rowNo.'" name="txt_lcsc_currency[]" class="text_boxes_numeric" style="width:100px" onkeyup="fn_calculate(this.id,'.$rowNo.')" /></td>
			<td>
			<input type="button" id="increaserow_'.$rowNo.'" style="width:30px" class="formbutton" value="+" onClick="javascript:fn_inc_decr_row('.$rowNo.',\'increase\');" /><input type="button" id="decreaserow_'.$rowNo.'" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_inc_decr_row('.$rowNo.',\'decrease\');" />
			</td> 
		</tr>'; 
	 
	exit();
}*/




if($action=="lcSc_popup_search")
{
	echo load_html_head_contents("Export Information Entry Form", "../../../", 1, 1,'','1','');
	extract($_REQUEST);
	?>
	<script>
	
	function set_all()
	{
		var old=document.getElementById('old_data_row_color').value;
		if(old!="")
		{   
			old=old.split(",");
			for(var i=0; i<old.length; i++)
			{  
				js_set_value( old[i] ) 
			}
		}
	}
	
	
	var selected_id = new Array;
	var selected_name = new Array;
	var currencyArr = new Array; //for check currency mix
	var buyerArr = new Array; //for check buyer mix
	var lcScArr = new Array; //for check lc sc mix
	
	
	function check_all_data() {
		var tbl_row_count = document.getElementById( 'table_body' ).rows.length;
		tbl_row_count = tbl_row_count - 0;
		for( var i = 1; i <= tbl_row_count; i++ ) {
			js_set_value( i );
		}
	}
	
	function toggle( x, origColor ) {
		var newColor = 'yellow';
		if ( x.style ) {
			x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
		}
	}
	
	function js_set_value(str) 
	{ 
			
		var invoiceID = $('#hidden_invoice_id'+str).val();
		var currency = $('#hidden_currency'+str).val();
		var buyer = $('#hidden_buyer'+str).val();
		var lcsc = $('#hidden_lc_sc'+str).val();
		
		//ls sc mix check-------------------------------//
		if(lcScArr.length==0)
		{
			lcScArr.push( lcsc );
		}
		else if( jQuery.inArray( lcsc, lcScArr )==-1 &&  lcScArr.length>0)
		{
			alert("LC or SC Mixed is Not Allow");
			return;
		}
		 
		//currency mix check-------------------------------//
		 
		//currency mix check-------------------------------//
		if(currencyArr.length==0)
		{
			currencyArr.push( currency );
		}
		else if( jQuery.inArray( currency, currencyArr )==-1 &&  currencyArr.length>0)
		{
			alert("Currency Mixed is Not Allow");
			return;
		}
		
		//buyer mix check--------------------------------//
		if(buyerArr.length==0)
		{
			buyerArr.push( buyer );
		}
		else if( jQuery.inArray( buyer, buyerArr )==-1 &&  buyerArr.length>0)
		{
			alert("Buyer Mixed is Not Allow");
			return;
		}
					
		toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
		
		if( jQuery.inArray( invoiceID, selected_id ) == -1 ) {
			selected_id.push( invoiceID );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == invoiceID ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id =''; var name = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
		}
		id 		= id.substr( 0, id.length - 1 );
		 
		$('#all_invoice_id').val( id );			
 	}
	
    </script>

</head>

<body>
<div align="center" style="width:740px;">
	<form name="searchexportinformationfrm"  id="searchexportinformationfrm">
		<fieldset style="width:720px;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" border="1" rules="all" width="670" class="rpt_table">
                <thead>
                    <th>Company</th>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th>Enter No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="hidden_lcSc_id" id="hidden_lcSc_id" value="" />
                        <input type="hidden" name="is_lcSc" id="is_lcSc" value="" />
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <?php
                            echo create_drop_down( "cbo_company_name", 150, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--- Select Company ---", $companyID, "",1 );
                        ?>                        
                    </td>
                    <td>
                    	<?php
                    		echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$companyID' $buyer_cond group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
						?>
                    </td>
                    <td> 
                        <?php
                            $arr=array(1=>'Invoice No',2=>' Sales Contract No');
                            echo create_drop_down( "cbo_search_by", 130, $arr,"", 0, "", 0, "" );
                        ?> 
                    </td>						
                    <td>
                        <input type="text" style="width:110px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>                       
                     <td>
                        <input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+'<?php echo $invoice_id_string; ?>'+'**'+'<?php echo $mst_tbl_id; ?>', 'lcSc_search_list_view', 'search_div', 'export_doc_sub_buyer_entry_controller', 'setFilterGrid(\'list_view\',-1)'); set_all();" style="width:100px;" />
                     </td>
                </tr>
           </table>
            <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
 
}

if($action=="lcSc_search_list_view")
{
	
	$data=explode('**',$data); 	
	$company_id=$data[0];
	$buyer_id=$data[1];
	$search_by=$data[2];
	$search_string=trim($data[3]);
	$invoiceArr = explode(",",$data[4]);
	$mst_tbl_id = $data[5];
	//echo $mst_tbl_id ;die;
	if($mst_tbl_id=='') $mst_tbl_id=0;
	
	if($buyer_id!=0) $buyer_con="and m.buyer_id=$buyer_id"; else $buyer_con;
	if($company_id==0){ echo "Please Select Company first"; die; }
	
	if($search_by==1)
	{
		
 		if($search_string != "")
			$sql = "SELECT m.is_lc,m.id,invoice_no,lc_sc_id,net_invo_value,m.buyer_id,short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and invoice_no like '%$search_string%' and b.id=m.buyer_id and m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id !='$mst_tbl_id' and status_active=1 and is_deleted=0)";
		else 
			$sql = "SELECT m.is_lc,m.id,invoice_no,lc_sc_id,net_invo_value,m.buyer_id,short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE  m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id != '$mst_tbl_id' and status_active=1 and is_deleted=0)";
	}
	else
	{ 
		if($search_string != "")
			$sql = "SELECT m.is_lc,m.id,invoice_no,lc_sc_id,net_invo_value,short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and ( lc_sc_id in (select id from com_sales_contract where contract_no like '%$search_string%' and status_active=1 and is_deleted=0) or lc_sc_id in(select id from com_export_lc where export_lc_no like '%$search_string%' and status_active=1 and is_deleted=0) ) and b.id=m.buyer_id and  m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id !='$mst_tbl_id' and status_active=1 and is_deleted=0)";
		else
			$sql = "SELECT m.is_lc,m.id,invoice_no,lc_sc_id,net_invo_value,short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE   m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and  m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id != '$mst_tbl_id' and status_active=1 and is_deleted=0)";
	}
	
	//echo $sql;		
		 
	?>
		<div style=" width:680px;">
                <table  width="100%" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">                    
                    <thead>
                        <th width="30">SL</th>
                        <th width="150">Invoice No</th>
                        <th width="150">Sales Contract No</th>
                        <th width="100">Buyer</th>
                        <th width="100">Currency</th>
                        <th width="">Net Invoice Value</th>
                    </thead> 
               </table>
       </div>             
       <div style="width:680px; overflow-y:scroll; max-height:200px">                
       		<table width="660" cellpadding="0" cellspacing="0" class="rpt_table" id="list_view" rules="all">
			<?php
                $i=1; $oldDataRow=$pay_term_cond="";
              
				$exprt_lc_arr=array();
				$lcscRes=sql_select("select id,export_lc_no,currency_name, 0 as pay_term from com_export_lc where status_active=1 and is_deleted=0 "); 
				  foreach($lcscRes as $row_lc)
				  {
					$exprt_lc_arr[$row_lc[csf('id')]]['export_lc_no']= $row_lc[csf('export_lc_no')];
					$exprt_lc_arr[$row_lc[csf('id')]]['currency_name']= $row_lc[csf('currency_name')];
					$exprt_lc_arr[$row_lc[csf('id')]]['pay_term']= $row_lc[csf('pay_term')];
				  } //var_dump($exprt_lc_arr);
				   $lcsc_lc_arr=array();
				   $lcscRes_data=sql_select("select id, contract_no,currency_name,pay_term from com_sales_contract where status_active=1 and is_deleted=0"); 
				 foreach($lcscRes_data as $row_lcsc)
				  {
					 $lcsc_lc_arr[$row_lcsc[csf('id')]]['contract_no']=$row_lcsc[csf('contract_no')];
					 $lcsc_lc_arr[$row_lcsc[csf('id')]]['currency_name']=$row_lcsc[csf('currency_name')]; 
					 $lcsc_lc_arr[$row_lcsc[csf('id')]]['pay_term']=$row_lcsc[csf('pay_term')];   
				  } //var_dump($lcsc_lc_arr);
                  $nameArray=sql_select($sql);
				foreach($nameArray as $row)
                {  
                    if ($i%2==0)  $bgcolor="#E9F3FF";
                    else $bgcolor="#FFFFFF";   
                    
                    if($row[csf("is_lc")]==1)
					{
                      /* $lcscRes=sql_select("select export_lc_no,currency_name, 0 as pay_term from com_export_lc where id=".$row[csf("lc_sc_id")].""); 
						$lc_sc_no = $lcscRes[0][csf('export_lc_no')]; 
                    	$currency_name = $lcscRes[0][csf('currency_name')];
						$pay_term_cond =$lcscRes[0][csf('pay_term')];
						*/
						$currency_name =$exprt_lc_arr[$row[csf('lc_sc_id')]]['currency_name'];
						$lc_sc_no=$exprt_lc_arr[$row[csf('lc_sc_id')]]['export_lc_no'];
						$pay_term_cond = $exprt_lc_arr[$row[csf('lc_sc_id')]]['pay_term'];//$lcscRes[0][csf('pay_term')];
					}
					else 
					{
                      /* $lcscRes=sql_select("select contract_no,currency_name,pay_term from com_sales_contract where id=".$row[csf("lc_sc_id")].""); 
						$lc_sc_no = $lcscRes[0][csf('contract_no')];  
                    	$currency_name = $lcscRes[0][csf('currency_name')];		
						$pay_term_cond = $lcscRes[0][csf('pay_term')]; */
					
					
						$currency_name=$lcsc_lc_arr[$row[csf('lc_sc_id')]]['currency_name'];
						$lc_sc_no=$lcsc_lc_arr[$row[csf('lc_sc_id')]]['contract_no'];
						$pay_term_cond =$lcsc_lc_arr[$row[csf('lc_sc_id')]]['pay_term'] ;
 					}
					 
					//var_dump($lcscRes);
					if($pay_term_cond!=3)	// pay term 3 means cash in advanced sales contact not come in document submission form		
					{
						//old data row arrange here------
						if( in_array($row[csf("id")], $invoiceArr) )
						{

							if($oldDataRow=="") $oldDataRow = $i; else $oldDataRow .= ",".$i;
						}
						?>   	
								<tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $i;?>)" > 
									<td width="30"><?php echo $i;?> 
										<input type="hidden" id="hidden_invoice_id<?php echo $i; ?>" value="<?php echo $row[csf("id")];?>" />
										<input type="hidden" id="hidden_invoice_no<?php echo $i; ?>" value="<?php echo $row[csf("invoice_no")];?>" />                                     
									</td>		                   
									<td width="150"><?php echo $row[csf("invoice_no")];?></td>
									<td width="150"><p><?php echo $lc_sc_no;?></p></td><input type="hidden" id="hidden_lc_sc<?php echo $i; ?>" value="<?php echo $lc_sc_no; ?>" /> 
									<td width="100"><p><?php echo $row[csf("short_name")]; ?></p><input type="hidden" id="hidden_buyer<?php echo $i; ?>" value="<?php echo $row[csf("buyer_id")]; ?>" /></td>
									<td width="100">
										<p><?php echo $currency[$currency_name]; ?></p>
										<input type="hidden" id="hidden_currency<?php echo $i; ?>" value="<?php echo $currency_name; ?>" />
									</td>
									<td ><p><?php echo $row[csf("net_invo_value")];?></p></td>
								</tr>         
						<?php           
						$i++; 
					}//pay term if cond end
					
                } //foeach end
             ?>		
             		<input type="hidden" name="old_data_row_color" id="old_data_row_color" value="<?php echo $oldDataRow; ?>"/>
			</table>
                </div>
                <div style="width:50%; float:left" align="left">
                            <input type="hidden"  id="all_invoice_id" value="" />
                            <input type="hidden"  id="all_invoice_no" value="" /> 
                            <input type="checkbox" name="check_all_lc" id="check_all_lc" onClick="check_all_data()" value="0" />&nbsp;&nbsp;Check All
                </div>
                <div style="width:40%; float:left" align="left">
                            <input type="submit" class="formbutton" id="close" style="width:80px" onClick="parent.emailwindow.hide();" value="Close" />
                </div>       
            <?php        
       
	   exit();    
 	
}




//invoice list view create
if ($action=="show_invoice_list_view")
{
	 
	$sql="select id,is_lc,lc_sc_id from com_export_invoice_ship_mst where id in($data)";
	$nameArray=sql_select($sql);
	$lcscNoString="";$lcscNoID="";$lienBank="";$currencyID="";$invoice_tr="";$tot_invoice_val=0; 
	$i=1;
	foreach($nameArray as $row)
	{
		
		if($row[csf("is_lc")] == "1") // if LC
		{
			if($db_type==0)
			{
			$sqlDtls="SELECT invo.id,invo.invoice_no,invo.buyer_id,invo.invoice_date,invo.lc_sc_id,invo.is_lc,invo.bl_no,invo.bl_date,invo.net_invo_value,lc.lien_bank,lc.currency_name,lc.export_lc_no as lcsc_no, GROUP_CONCAT(dtl.po_breakdown_id SEPARATOR ',') as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_export_lc lc, com_export_invoice_ship_dtls dtl WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = lc.id and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by dtl.mst_id";
			}
			else if($db_type==2)
			{
			$sqlDtls="SELECT invo.id,invo.invoice_no,invo.buyer_id,invo.invoice_date,invo.lc_sc_id,invo.is_lc,invo.bl_no,invo.bl_date,invo.net_invo_value,lc.lien_bank,lc.currency_name,lc.export_lc_no as lcsc_no, LISTAGG(dtl.po_breakdown_id, ',') WITHIN GROUP (ORDER BY dtl.po_breakdown_id) as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_export_lc lc, com_export_invoice_ship_dtls dtl WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = lc.id and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by dtl.mst_id,invo.id,invo.invoice_no,invo.buyer_id,invo.invoice_date,invo.lc_sc_id,invo.is_lc,invo.bl_no,invo.bl_date,invo.net_invo_value,lc.lien_bank,lc.currency_name,lc.export_lc_no";	
			}
		
		}
		else
		{
			if($db_type==0)
			{
			$sqlDtls="SELECT invo.id,invo.invoice_no,invo.buyer_id,invo.invoice_date,invo.lc_sc_id,invo.is_lc,invo.bl_no,invo.bl_date,invo.net_invo_value, sc.lien_bank,sc.currency_name,sc.contract_no as lcsc_no, GROUP_CONCAT(dtl.po_breakdown_id SEPARATOR ',') as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_sales_contract sc, com_export_invoice_ship_dtls dtl  WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = sc.id  and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by dtl.mst_id";
			}
			else if($db_type==2)
			{
			$sqlDtls="SELECT invo.id,invo.invoice_no,invo.buyer_id,invo.invoice_date,invo.lc_sc_id,invo.is_lc,invo.bl_no,invo.bl_date,invo.net_invo_value, sc.lien_bank,sc.currency_name,sc.contract_no as lcsc_no, LISTAGG(dtl.po_breakdown_id, ',') WITHIN GROUP (ORDER BY dtl.po_breakdown_id) as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_sales_contract sc, com_export_invoice_ship_dtls dtl  WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = sc.id  and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by dtl.mst_id,invo.id,invo.invoice_no,invo.buyer_id,invo.invoice_date,invo.lc_sc_id,invo.is_lc,invo.bl_no,invo.bl_date,invo.net_invo_value, sc.lien_bank,sc.currency_name,sc.contract_no";	
			}
			
		}  
		//echo $sqlDtls;
		$resArray=sql_select($sqlDtls);
		foreach($resArray as $res)
		{
			if($lcscNoString=="") $lcscNoString .= $res[csf("lcsc_no")]; else $lcscNoString .=",".$res[csf("lcsc_no")];
			if($lcscNoID=="") $lcscNoID .= $res[csf("lc_sc_id")]; else $lcscNoID .=",".$res[csf("lc_sc_id")];
			$lienBank=$res[csf("lien_bank")];
			$currencyID=$res[csf("currency_name")];
			$buyerID=$res[csf("buyer_id")];
			
			//list view data arrange-----------------// <td align=\"center\">".$i."</td>
			$poSQL=sql_select("select po_number from wo_po_break_down where id in (".$res[csf("po_breakdown_id")].")");
			$po_numbers="";
			foreach($poSQL as $poR)
			{
				if($po_numbers=="") $po_numbers=$poR[csf("po_number")]; else $po_numbers.=",".$poR[csf("po_number")];
			}
			$invoice_tr .= "<tr>".
								"<td align=\"center\">".$i."</td>".
								"<td><input type=\"text\" id=\"txt_invoice_no$i\" name=\"txt_invoice_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$res[csf("invoice_no")]."\" /><input type=\"hidden\" id=\"hidden_invoice_id$i\" value=\"".$res[csf("id")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_lcsc_no$i\" name=\"txt_lcsc_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$res[csf("lcsc_no")]."\" /><input type=\"hidden\" id=\"txt_lcsc_id$i\" value=\"".$res[csf("lc_sc_id")]."\" /><input type=\"hidden\" id=\"hidden_is_lc$i\" value=\"".$res[csf("is_lc")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_bl_no$i\" name=\"txt_bl_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$res[csf("bl_no")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_invoice_date$i\" name=\"txt_invoice_date[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$res[csf("invoice_date")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_net_invo_value$i\" name=\"txt_net_invo_value[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$res[csf("net_invo_value")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_po_numbers$i\" name=\"txt_po_numbers[]\" class=\"text_boxes\" style=\"width:180px\" value=\"".$po_numbers."\" /><input type=\"hidden\" id=\"hidden_po_numbers_id$i\" value=\"".$res[csf("po_breakdown_id")]."\" /></td>".
							"</tr>"; 
			$tot_invoice_val+=$res[csf("net_invo_value")];				
			$i++; 
		}
		
	}
		$invoice_tr .= "<tr class=\"tbl_bottom\">".
							"<td colspan=\"5\" align=\"right\">Total</td>".
							"<td><input type=\"text\" name=\"txt_total\" id=\"txt_total\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$tot_invoice_val."\"/></td>".
							"<td>&nbsp;</td>".
						"</tr>";
		echo "$('#lcsc_no').val('".$lcscNoString."');\n";
		echo "$('#lc_sc_id').val('".$lcscNoID."');\n";	
		echo "$('#cbo_buyer_name').val('".$buyerID."');\n";	
		echo "$('#cbo_lien_bank').val('".$lienBank."');\n";
		echo "$('#cbo_currency').val('".$currencyID."');\n";
		
		//list view for invoice area----------------------------------------//
 		echo "$('#invo_table').find('tr:gt(0)').remove()".";\n";         
		echo "$('#invoice_container').html('".$invoice_tr."')".";\n";
		echo "$('#invoice_container').find('input').attr('Disabled','Disabled');\n";
		
	
	exit();
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
 		
 	 	//---------------Check Duplicate Bank Ref/ Bill No  ------------------------//
		/*$duplicate = is_duplicate_field("id"," com_export_doc_submission_mst","bank_ref_no=$txt_bank_ref and company_id=$cbo_company_name  and buyer_id=$cbo_buyer_name"); 
		if($duplicate==1) 
		{			 
			echo "20**Duplicate Bank Ref. Number";
			die;
		}*/
		//------------------------------Check Duplicate END---------------------------------------//
		
		
		//master table entry here START---------------------------------------//		
		$id=return_next_id("id", "com_export_doc_submission_mst", 1);		
 		$field_array="id,company_id,buyer_id,submit_date,entry_form,submit_to,days_to_realize,possible_reali_date,courier_receipt_no,courier_company,courier_date,lien_bank,lc_currency,submit_type,remarks,inserted_by,insert_date";
		$data_array="(".$id.",".$cbo_company_name.",".$cbo_buyer_name.",".$txt_submit_date.",39,2,".$txt_day_to_realize.",".$txt_possible_reali_date.",".$courier_receipt_no.",".$txt_courier_company.",".$txt_courier_date.",".$cbo_lien_bank.",".$cbo_currency.",1,".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
		//echo "20**".$field_array."<br>".$data_array;die;
		//master table entry here END---------------------------------------// 
 		 
 		//echo "insert into com_export_doc_submission_mst (".$field_array.") values ".$data_array;die;
 		//dtls table entry here START---------------------------------------//		
		$dtlsid=return_next_id("id", "com_export_doc_submission_invo", 1);		
		$field_array_dtls="id,doc_submission_mst_id,invoice_id,is_lc,lc_sc_id,bl_no,invoice_date,net_invo_value,all_order_no,inserted_by,insert_date";
		$data_array_dtls="";
		for($i=1;$i<=$invoiceRow;$i++)
		{
			if($i>1) $data_array_dtls .= ",";
			$hidden_invoice_id 		= 'hidden_invoice_id'.$i;
			$txt_lcsc_id 			= 'txt_lcsc_id'.$i;
			$hidden_is_lc 			= 'hidden_is_lc'.$i;
			$txt_lcsc_no 			= 'txt_lcsc_no'.$i;
			$txt_bl_no 				= 'txt_bl_no'.$i;
			$txt_invoice_date 		= 'txt_invoice_date'.$i;
			$txt_net_invo_value 	= 'txt_net_invo_value'.$i;
			$hidden_po_numbers_id 	= 'hidden_po_numbers_id'.$i;
			
  			$data_array_dtls .= "(".$dtlsid.",".$id.",'".$$hidden_invoice_id."','".$$hidden_is_lc."','".$$txt_lcsc_id."','".$$txt_bl_no."','".$$txt_invoice_date."','".$$txt_net_invo_value."','".$$hidden_po_numbers_id."','".$user_id."','".$pc_date_time."')";	
			$dtlsid=$dtlsid+1;
		}
 		//echo "20**".$field_array."<br>".$data_array;die;
		//echo "insert into com_export_doc_submission_mst (".$field_array.") values ".$data_array;die;
		$rID=sql_insert("com_export_doc_submission_mst",$field_array,$data_array,1);
		
		if($data_array_dtls!="")
		{
			
			$dtlsrID=sql_insert("com_export_doc_submission_invo",$field_array_dtls,$data_array_dtls,1);
			//echo $dtlsrID;die;
		}
		//dtls table entry here END---------------------------------------// 
		
		
		//transaction table entry here START---------------------------------------//		
		/*$trid=return_next_id("id", "com_export_doc_submission_trans", 1);		
		$field_array="id,doc_submission_mst_id,acc_head,acc_loan,dom_curr,conver_rate,lc_sc_curr,inserted_by,insert_date";
		$data_array="";$tsrID=true;
		for($i=1;$i<=$transRow;$i++)
		{
			if($i>1) $data_array .= ",";
			$cbo_account_head 		= 'cbo_account_head_'.$i;
			$txt_ac_loan_no 		= 'txt_ac_loan_no_'.$i;
			$txt_domestic_curr 		= 'txt_domestic_curr_'.$i;
			$txt_conversion_rate 	= 'txt_conversion_rate_'.$i;
			$txt_lcsc_currency 		= 'txt_lcsc_currency_'.$i;
			
  			$data_array .= "(".$trid.",".$id.",'".$$cbo_account_head."','".$$txt_ac_loan_no."','".$$txt_domestic_curr."','".$$txt_conversion_rate."','".$$txt_lcsc_currency."','".$user_id."','".$pc_date_time."')";	
			$trid=$trid+1;
		}
 		//echo "20**".$field_array."<br>".$data_array;die;
		if($data_array!="")
		{
			$tsrID=sql_insert("com_export_doc_submission_trans",$field_array,$data_array,1);
		}*/
		//transaction table entry here END---------------------------------------// 
		
		//echo "20**".$rID." && ".$dtlsrID." && ".$tsrID;mysql_query("ROLLBACK");die;
		
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "0**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10".$id;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID)
			{
				oci_commit($con);  
				echo "0**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if(str_replace("'","",$mst_tbl_id)=="" || str_replace("'","",$invoice_tbl_id)=="") { echo "10**";die; }
		
 		 
		//---------------Check Duplicate Bank Ref/ Bill No  ------------------------//
		/*$duplicate = is_duplicate_field("id"," com_export_doc_submission_mst"," id!=$mst_tbl_id and company_id=$cbo_company_name and buyer_id=$cbo_buyer_name"); 
		if($duplicate==1) 
		{			 
			echo "20**Duplicate Bank Ref. Number";
			die;
		}*/
		//------------------------------Check Duplicate END---------------------------------------//
		
		 
		//master table entry here START---------------------------------------//
		$cbo_submit_to=2;
		$entry_form=39;	
		$submission_type=1;	
 		$id=str_replace("'","",$mst_tbl_id);
		$field_array="company_id*buyer_id*submit_date*entry_form*submit_to*days_to_realize*possible_reali_date*courier_receipt_no*courier_company*courier_date*lien_bank*lc_currency*submit_type*remarks*updated_by*update_date";
		$data_array="".$cbo_company_name."*".$cbo_buyer_name."*".$txt_submit_date."*".$entry_form."*".$cbo_submit_to."*".$txt_day_to_realize."*".$txt_possible_reali_date."*".$courier_receipt_no."*".$txt_courier_company."*".$txt_courier_date."*".$cbo_lien_bank."*".$cbo_currency."*".$submission_type."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
		//echo "20**".$field_array."<br>".$data_array;die;
 		
		//master table entry here END---------------------------------------// 
 		 
 		 
 		//dtls table entry here START---------------------------------------//	
 		
		$dtlsid=return_next_id("id", "com_export_doc_submission_invo", 1);
		$field_array_dtls="id,doc_submission_mst_id,invoice_id,is_lc,lc_sc_id,bl_no,invoice_date,net_invo_value,all_order_no,inserted_by,insert_date";
		$data_array_dtls="";
		for($i=1;$i<=$invoiceRow;$i++)
		{
			if($i>1) $data_array_dtls .= ",";
			$hidden_invoice_id 		= 'hidden_invoice_id'.$i;
			$txt_lcsc_id 			= 'txt_lcsc_id'.$i;
			$hidden_is_lc 			= 'hidden_is_lc'.$i;
			$txt_lcsc_no 			= 'txt_lcsc_no'.$i;
			$txt_bl_no 				= 'txt_bl_no'.$i;
			$txt_invoice_date 		= 'txt_invoice_date'.$i;
			$txt_net_invo_value 	= 'txt_net_invo_value'.$i;
			$hidden_po_numbers_id 	= 'hidden_po_numbers_id'.$i;
			
  			$data_array_dtls .= "(".$dtlsid.",".$id.",'".$$hidden_invoice_id."','".$$hidden_is_lc."','".$$txt_lcsc_id."','".$$txt_bl_no."','".$$txt_invoice_date."','".$$txt_net_invo_value."','".$$hidden_po_numbers_id."','".$user_id."','".$pc_date_time."')";	
			$dtlsid=$dtlsid+1;
		}
 		//echo "20**".$invoiceRow."<br>".$data_array;die;
		$deleteDtls = execute_query("DELETE FROM com_export_doc_submission_invo WHERE doc_submission_mst_id=$mst_tbl_id");
		$dtlsrID=true;
		if($data_array_dtls!="")
		{
			$dtlsrID=sql_insert("com_export_doc_submission_invo",$field_array_dtls,$data_array_dtls,1);
		}
			$rID=sql_update("com_export_doc_submission_mst",$field_array,$data_array,"id",$mst_tbl_id,1);

		//dtls table entry here END---------------------------------------// 
		
		
		//transaction table entry here START---------------------------------------//
		/*$deleteDtls = execute_query("DELETE FROM com_export_doc_submission_trans WHERE doc_submission_mst_id=$mst_tbl_id");		
		$trid=return_next_id("id", "com_export_doc_submission_trans", 1);		
		$field_array="id,doc_submission_mst_id,acc_head,acc_loan,dom_curr,conver_rate,lc_sc_curr,inserted_by,insert_date";
		$data_array="";$tsrID=true;
		for($i=1;$i<=$transRow;$i++)
		{
			if($i>1) $data_array .= ",";
			$cbo_account_head 		= 'cbo_account_head_'.$i;
			$txt_ac_loan_no 		= 'txt_ac_loan_no_'.$i;
			$txt_domestic_curr 		= 'txt_domestic_curr_'.$i;
			$txt_conversion_rate 	= 'txt_conversion_rate_'.$i;
			$txt_lcsc_currency 		= 'txt_lcsc_currency_'.$i;
			
  			$data_array .= "(".$trid.",".$id.",'".$$cbo_account_head."','".$$txt_ac_loan_no."','".$$txt_domestic_curr."','".$$txt_conversion_rate."','".$$txt_lcsc_currency."','".$user_id."','".$pc_date_time."')";	
			$trid=$trid+1;
		}
 		//echo "20**".$field_array."<br>".$data_array;die;
		if($data_array!="")
		{
			$tsrID=sql_insert("com_export_doc_submission_trans",$field_array,$data_array,1);
		}*/
		//transaction table entry here END---------------------------------------// 
		
		//echo "20**".$rID." && ".$dtlsrID." && ".$tsrID;mysql_query("ROLLBACK");die;
		
		
		if($db_type==0)
		{
			if($rID && $dtlsrID && $deleteDtls)
			{
				mysql_query("COMMIT");  
				echo "1**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID && $deleteDtls)
			{
				oci_commit($con);    
				echo "1**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		disconnect($con);
		die;
	}
	
	else if ($operation==2)   // Delete Here
	{
		 //in future
	}
}




if($action=="doc_sub_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_system_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchdocfrm_1"  id="searchdocfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th width="150">Invoice No</th>
                    <th width="250" align="center" id="search_by_td_up">Sales Contract No</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr align="center">                    
                    <td>
                      <input type="text" style="width:230px" class="text_boxes"  name="txt_invoice_num" id="txt_invoice_num" />	
                        <?php  
                            
							 //echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company_name' $buyer_cond order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" ); 
							$search_by = array(1=>'Bank Ref/Bill No',2=>'Submitted To',3=>'Submission Type');
							$dd="change_search_event(this.value, '0*2*2', '0*submited_to*submission_type', '../../../')";
							//echo create_drop_down( "cbo_search_by", 140, $search_by, "", 0, "--Select--", 0,$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_sales_con" id="txt_sales_con" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_invoice_num').value+'_'+document.getElementById('txt_sales_con').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company_name; ?>+'_'+<?php echo $buyer_name; ?>, 'create_system_id_search_list_view', 'search_div', 'export_doc_sub_buyer_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_system_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div" style="margin-top:5px"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_system_id_search_list_view")
{
	 
	$ex_data = explode("_",$data);
	$invoice_num = $ex_data[0];
	$search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company_name = $ex_data[4];
	$buyer_name = $ex_data[5];
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );

	//echo $invoice_num;
	$sales_con_no="";
	if($search_common!="")
	{
		$sales_con_no .= " and c.contract_no like '%$search_common%'";
	}
	if($invoice_num!="")
	{
		$invoice_number .= " and d.invoice_no like '%$invoice_num%'";
	}
	
	 
 	if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and a.submit_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if(trim($company_name)!="") $sql_cond .= " and a.company_id ='$company_name'";
	if(trim($buyer_name)!=0) $sql_cond .= " and a.buyer_id ='$buyer_name'";
	
	$bank_arr = return_library_array( "select id, bank_name from lib_bank",'id','bank_name');
	if($db_type==0)
	{
	$sql = "select a.id,a.bank_ref_no,a.submit_date,a.buyer_id,sum(b.net_invo_value) as net_invo_value,a.submit_to,a.lien_bank,a.submit_type, GROUP_CONCAT(b.is_lc) as is_lc_string, GROUP_CONCAT(b.invoice_id) as invoice_id_string, GROUP_CONCAT(b.lc_sc_id) as lc_sc_id_string 
			from  com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id  and a.entry_form=39 and a.status_active=1 and b.status_active=1  $sql_cond  $sales_con_no $invoice_number group by a.id";	
	}
	else if($db_type==2)
	{
		$sql = "select a.id,a.submit_date,a.buyer_id,sum(b.net_invo_value) as net_invo_value,a.submit_to,a.lien_bank,a.submit_type,  LISTAGG(b.is_lc, ',') WITHIN GROUP (ORDER BY b.is_lc) as is_lc_string, LISTAGG(b.invoice_id, ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id_string, LISTAGG(b.lc_sc_id, ',') WITHIN GROUP (ORDER BY b.lc_sc_id) as lc_sc_id_string 
			from  com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id and a.entry_form=39 and a.status_active=1 and b.status_active=1  $sql_cond  $sales_con_no $invoice_number group by a.id,a.submit_date,a.buyer_id,a.submit_to,a.lien_bank,a.submit_type";
	}
	
	//echo $sql;
	$res = sql_select($sql);
		   
    ?>
    <div style="width:918px;">
        <table border="0" width="100%" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
            <thead>
                <th width="30">SL</th>
                <th width="80">System Id</th>
                <th width="100">Buyer</th>
                <th width="120">Sales Contract No</th>
                <th width="150">Invoice No</th>
                <th width="80">Submit Date</th>
               
                <th width="130">Lien Bank</th>
                <th width="">Invoice Value</th>
            </thead>
        </table>
    </div>            
    <div style="width:918px; overflow-y:scroll; max-height:230px">
        <table border="0" width="900" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="list_view">             
   		<?php                     
        	$i=1;
			foreach($res as $row)
			{  
				
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
				
				$expisLC = explode(",",$row[csf("is_lc_string")]);
				$expisINV = explode(",",$row[csf("lc_sc_id_string")]);
				$j=0;$lcID="";$scID="";
				foreach($expisLC as $key=>$val)
				{
					if($val==1 && $expisINV[$j]!=0)
					{
 						// export LC id
						if($lcID=="") $lcID .= $expisINV[$j]; else $lcID .=",".$expisINV[$j];
					}
					else if($expisINV[$j]!=0)
					{  
 						//Sales Contact id
						if($scID=="") $scID .= $expisINV[$j]; else $scID .=",".$expisINV[$j];
					}
					$j++;	
 				}
				
				$lc_sc_no="";
				if($lcID!="")
				{
 					// export LC number
					$poSQL=sql_select("select export_lc_no from com_export_lc where id in (".$lcID.")");
 					foreach($poSQL as $poR)
					{
						if($lc_sc_no=="") $lc_sc_no=$poR[csf("export_lc_no")]; else $lc_sc_no.=",".$poR[csf("export_lc_no")];
					} 
				}
				if($scID!="")
				{
					//Sales Contact Number
					$scSQL=sql_select("select contract_no from com_sales_contract where id in (".$scID.")");
 					foreach($scSQL as $poR)
					{
						if($lc_sc_no=="") $lc_sc_no=$poR[csf("contract_no")]; else $lc_sc_no.=",".$poR[csf("contract_no")];
					}  		 
				}
				
				//invoice list 
				$invoiceNo="";
				if($row[csf("invoice_id_string")]!="")
				{
					$invoiceSQL=sql_select("select invoice_no from com_export_invoice_ship_mst where  id in (".$row[csf("invoice_id_string")].") ");
					//echo "select invoice_no from com_export_invoice_shipping_mst where  id in (".$row[csf("invoice_id_string")].") and invoice_no='$invoice_no' ";
					foreach($invoiceSQL as $poR)
					{
						if($invoiceNo=="") $invoiceNo=$poR[csf("invoice_no")]; else $invoiceNo.=",".$poR[csf("invoice_no")];
					} 
				}
					//echo array_unique($row[csf("id")]);//$mst_id=array_unique($row[csf("id")]);
          		?>     
			   		<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf("id")];?>)" > 
                        <td width="30"><?php echo $i;?></td>
                        <td width="80"><?php echo $row[csf("id")];?></td>
                        <td width="100"><p><?php echo $buyer_library[$row[csf("buyer_id")]]; ?></p></td>                    
                        <td width="120"><p><?php echo $lc_sc_no; ?></p></td>
                        <td width="150"><p><?php echo $invoiceNo; ?></p></td>
                        <td width="80" align="center"><?php echo change_date_format($row[csf("submit_date")]); ?></td>
                        
                        <td width="130"><p><?php echo $bank_arr[$row[csf("lien_bank")]]; ?></p></td>
                        <td align="right"><p><?php echo $row[csf("net_invo_value")]; ?></p></td>
                    </tr>
          		<?php
			                 
            	$i++; 
			}      
		?>	
        </table>
	</div>
	<?php	
	exit();
}




if($action=="populate_master_from_data")
{  
	
	if($db_type==0)
	{
	$sql = "select a.id,company_id,buyer_id,submit_date,submit_to,bank_ref_no,bank_ref_date,days_to_realize,possible_reali_date,courier_receipt_no,courier_company,courier_date,bnk_to_bnk_cour_no,bnk_to_bnk_cour_dt,lien_bank,lc_currency,submit_type,negotiation_date,remarks,group_concat(b.id) as inv_id, GROUP_CONCAT(b.is_lc SEPARATOR ',') as is_lc_string, GROUP_CONCAT(b.invoice_id SEPARATOR ',') as invoice_id_string, GROUP_CONCAT(b.lc_sc_id SEPARATOR ',') as lc_sc_id_string 
			from  com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id and a.entry_form=39 and a.id=$data group by a.id";
	}
	else if($db_type==2)
	{
	$sql = "select a.id,a.company_id,a.buyer_id,a.submit_date,a.submit_to,a.days_to_realize,a.possible_reali_date,a.courier_receipt_no,a.courier_company,a.courier_date,a.lien_bank,a.lc_currency,a.submit_type,a.remarks,LISTAGG(b.id, ',') WITHIN GROUP (ORDER BY b.id) as inv_id, LISTAGG(b.is_lc, ',') WITHIN GROUP (ORDER BY b.is_lc) as is_lc_string, LISTAGG(b.invoice_id, ',')  WITHIN GROUP ( ORDER BY b.invoice_id) as invoice_id_string, LISTAGG(b.lc_sc_id, ',') WITHIN GROUP (ORDER BY b.lc_sc_id) as lc_sc_id_string 
			from  com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id and a.entry_form=39 and a.id=$data group by a.id,a.company_id,a.buyer_id,a.submit_date,a.submit_to,a.days_to_realize,a.possible_reali_date,a.courier_receipt_no,a.courier_company,a.courier_date,a.lien_bank,a.lc_currency,a.submit_type,a.remarks ";	
	}
	//echo $sql;
	$res = sql_select($sql);
	$i=1;
	foreach($res as $row)
	{
		
  		echo "$('#cbo_company_name').val('".$row[csf("company_id")]."');\n";
		echo "$('#cbo_buyer_name').val('".$row[csf("buyer_id")]."');\n";
		
 		
		$expisLC = explode(",",$row[csf("is_lc_string")]);
		$expisINV = explode(",",$row[csf("lc_sc_id_string")]);
		$j=0;$lcID="";$scID="";
		foreach($expisLC as $key=>$val)
		{
			if($val==1 && $expisINV[$j]!=0)
			{
				// export LC id
				if($lcID=="") $lcID .= $expisINV[$j]; else $lcID .=",".$expisINV[$j];
			}
			else if($expisINV[$j]!=0)
			{  
				//Sales Contact id
				if($scID=="") $scID .= $expisINV[$j]; else $scID .=",".$expisINV[$j];
			}
			$j++;	
		}
		 
		$lc_sc_no="";
		if($lcID!="")
		{
			// export LC number
			$poSQL=sql_select("select export_lc_no from com_export_lc where id in (".$lcID.")");
			foreach($poSQL as $poR)
			{
				if($lc_sc_no=="") $lc_sc_no=$poR[csf("export_lc_no")]; else $lc_sc_no.=",".$poR[csf("export_lc_no")];
			} 
		}
		if($scID!="")
		{
			//Sales Contact Number
			$scSQL=sql_select("select contract_no from com_sales_contract where id in (".$scID.")");
			foreach($scSQL as $poR)
			{
				if($lc_sc_no=="") $lc_sc_no=$poR[csf("contract_no")]; else $lc_sc_no.=",".$poR[csf("contract_no")];
			}  		 
		}
		echo "$('#lcsc_no').val('".$lc_sc_no."');\n";  
		echo "$('#lc_sc_id').val('".$row[csf("lc_sc_id_string")]."');\n"; 
		echo "$('#invoice_id_string').val('".$row[csf("invoice_id_string")]."');\n"; 
		if($row[csf("submit_date")]!='0000-00-00')
		{		 
 			echo "$('#txt_submit_date').val('".change_date_format($row[csf("submit_date")])."');\n";
		}
		else
		{
			echo "$('#txt_submit_date').val('');\n";
		}
		
		/*echo "$('#cbo_submission_type').val('".$row[csf("submit_type")]."');\n";
		if($row[csf("submit_type")]==1)
		{
		 echo "$('#txt_negotiation_date').attr('disabled',true);\n";
		}
		else
		{
			if($row[csf("negotiation_date")]!='0000-00-00')
			{
				echo "$('#txt_negotiation_date').attr('disabled',false);\n";
				echo "$('#txt_negotiation_date').val('".change_date_format($row[csf("negotiation_date")])."');\n";
			}
			else
			{
				echo "$('#txt_negotiation_date').val('');\n";
			}
		}*/
		echo "$('#txt_day_to_realize').val('".$row[csf("days_to_realize")]."');\n";
		if($row[csf("possible_reali_date")]!='0000-00-00')
		{
			echo "$('#txt_possible_reali_date').val('".change_date_format($row[csf("possible_reali_date")])."');\n";
		}
		else
		{
			echo "$('#txt_possible_reali_date').val('');\n";
		}
		echo "$('#courier_receipt_no').val('".$row[csf("courier_receipt_no")]."');\n";
		echo "$('#txt_courier_company').val('".$row[csf("courier_company")]."');\n";
		if($row[csf("courier_date")]!='0000-00-00')
		{
			echo "$('#txt_courier_date').val('".change_date_format($row[csf("courier_date")])."');\n";
		}
		else
		{
			echo "$('#txt_courier_date').val('');\n";
		}
		/*echo "$('#txt_bnk_to_bnk_cour_no').val('".$row[csf("bnk_to_bnk_cour_no")]."');\n";
		if($row[csf("bnk_to_bnk_cour_dt")]!='0000-00-00')
		{
		echo "$('#txt_bnk_to_bnk_cour_date').val('".change_date_format($row[csf("bnk_to_bnk_cour_dt")])."');\n";
		}
		else
		{
			echo "$('#txt_bnk_to_bnk_cour_date').val('');\n";
		}*/
		echo "$('#cbo_lien_bank').val('".$row[csf("lien_bank")]."');\n";
		echo "$('#cbo_currency').val('".$row[csf("lc_currency")]."');\n";
		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		
		echo "$('#mst_tbl_id').val('".$row[csf("id")]."');\n";
		echo "$('#invoice_tbl_id').val('".$row[csf("inv_id")]."');\n";
		
		//invoice list view -----------------------------------------------------------------------//
		//start -----------------------------------------------------------------------//
		$invSQL="select a.id,a.doc_submission_mst_id,a.invoice_id,a.is_lc,a.lc_sc_id,a.bl_no,a.invoice_date,a.net_invo_value,a.all_order_no,b.invoice_no from com_export_doc_submission_invo a, com_export_invoice_ship_mst b where a.invoice_id=b.id and a.doc_submission_mst_id=".$row[csf("id")]."";
 		//echo $invSQL;
		$resArray=sql_select($invSQL);
		$tot_invoice_val=0;
		
 		foreach($resArray as $invRow)
		{ 
 			//ls or sc number
			if($invRow[csf("is_lc")]==1)
				$lc_sc_nos = return_field_value("export_lc_no","com_export_lc","id=".$invRow[csf("lc_sc_id")].""); 
			else
				$lc_sc_nos = return_field_value("contract_no","com_sales_contract","id=".$invRow[csf("lc_sc_id")].""); 
				
			//list view data arrange-----------------// 
			$poSQL=sql_select("select po_number from wo_po_break_down where id in (".$invRow[csf("all_order_no")].")");
			$po_numbers="";
			foreach($poSQL as $poR)
			{
				if($po_numbers=="") $po_numbers=$poR[csf("po_number")]; else $po_numbers.=",".$poR[csf("po_number")];
			}
			$invoice_tr .= "<tr>".
								"<td align=\"center\">".$i."</td>".
								"<td><input type=\"text\" id=\"txt_invoice_no$i\" name=\"txt_invoice_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$invRow[csf("invoice_no")]."\" /><input type=\"hidden\" id=\"hidden_invoice_id$i\" value=\"".$invRow[csf("invoice_id")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_lcsc_no$i\" name=\"txt_lcsc_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$lc_sc_nos."\" /><input type=\"hidden\" id=\"txt_lcsc_id$i\" value=\"".$invRow[csf("lc_sc_id")]."\" /><input type=\"hidden\" id=\"hidden_is_lc$i\" value=\"".$invRow[csf("is_lc")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_bl_no$i\" name=\"txt_bl_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$invRow[csf("bl_no")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_invoice_date$i\" name=\"txt_invoice_date[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$invRow[csf("invoice_date")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_net_invo_value$i\" name=\"txt_net_invo_value[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("net_invo_value")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_po_numbers$i\" name=\"txt_po_numbers[]\" class=\"text_boxes\" style=\"width:200px\" value=\"".$po_numbers."\" /><input type=\"hidden\" id=\"hidden_po_numbers_id$i\" value=\"".$invRow[csf("all_order_no")]."\" /></td>".
							"</tr>"; 
			
			$tot_invoice_val+=$invRow[csf("net_invo_value")];
			$i++; 
		}
		
		$invoice_tr.= "<tr class=\"tbl_bottom\">".
							"<td colspan=\"5\" align=\"right\">Total</td>".
							"<td><input type=\"text\" name=\"txt_total\" id=\"txt_total\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$tot_invoice_val."\"/></td>".
							"<td>&nbsp;</td>".
						"</tr>";
		//list view for invoice area----------------------------------------//
 		echo "$('#invo_table').find('tr:gt(0)').remove()".";\n";         
		echo "$('#invoice_container').html( '".$invoice_tr."')".";\n";
		echo "$('#invoice_container').find('input').attr('Disabled','Disabled');\n";
 		//invoice list view -----------------------------------------------------------------------//
		//END -----------------------------------------------------------------------//
		
		 
		
		//transaction list generate here Start------------------------------------//
		/*$invSQL =  "select a.id,a.doc_submission_mst_id,a.acc_head,a.acc_loan,a.dom_curr,a.conver_rate,a.lc_sc_curr 
					from com_export_doc_submission_trans a, com_export_invoice_shipping_mst b 
					where a.doc_submission_mst_id=b.id and a.doc_submission_mst_id=".$row[csf("id")]."";
 		//echo $invSQL;
		$resArray=sql_select($invSQL);
		$rowNo=1; $transaction_tr="";
 		foreach($resArray as $invRow)
		{
			$transaction_tr .= "<tr id=\"tr".$rowNo."\">".
					"<td>".create_drop_down( "cbo_account_head_".$rowNo, 200, $commercial_head,"", 1, "-- Select --", $invRow[csf("acc_head")], "get_php_form_data(this.value+\'**\'+".$rowNo.", \'populate_acc_loan_no_data\', \'requires/export_doc_sub_buyer_entry_controller\' )",0, "" )."</td>".
					"<td><input type=\"text\" id=\"txt_ac_loan_no_".$rowNo."\" name=\"txt_ac_loan_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$invRow[csf("acc_loan")]."\" /></td>".
					"<td><input type=\"text\" id=\"txt_domestic_curr_".$rowNo."\" name=\"txt_domestic_curr[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("dom_curr")]."\" onkeyup=\"fn_calculate(this.id,".$rowNo.")\" /></td>".
					"<td><input type=\"text\" id=\"txt_conversion_rate_".$rowNo."\" name=\"txt_conversion_rate[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("conver_rate")]."\" onkeyup=\"fn_calculate(this.id,".$rowNo.")\" /></td>".
					"<td><input type=\"text\" id=\"txt_lcsc_currency_".$rowNo."\" name=\"txt_lcsc_currency[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("lc_sc_curr")]."\" onkeyup=\"fn_calculate(this.id,".$rowNo.")\" /></td>".
					"<td>".
					"<input type=\"button\" id=\"increaserow_".$rowNo."\" style=\"width:30px\" class=\"formbutton\" value=\"+\" onClick=\"javascript:fn_inc_decr_row(".$rowNo.",\'increase\');\" />".
					"<input type=\"button\" id=\"decreaserow_".$rowNo."\" style=\"width:30px\" class=\"formbutton\" value=\"-\" onClick=\"javascript:fn_inc_decr_row(".$rowNo.",\'decrease\');\" />".
					"</td>".
			"</tr>";	
			$rowNo = $rowNo+1;
		}
		
		//list view for transaction area----------------------------------------//
 		echo "$('#transaction_container').find('tr').remove();\n";         
		echo "$('#transaction_container').html( '".$transaction_tr."')".";\n";	*/
		//echo "sum_of_currency();\n";	
		//transaction list generate here End------------------------------------//
		
				 
   	}//main foreach end
		
	exit();	
}

if($action=="buyer_submit_letter")
{
	//echo load_html_head_contents("Buyer Submission Letter","../../", 1, 1, $unicode,'','');
	$order_inv_array=array();
	/*if($db_type==0)
	{
		$all_inv_id=return_field_value("group_concat(b.invoice_id) inv_id","com_export_doc_submission_mst a, com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and a.entry_form=39 and a.id=".$data,"inv_id");
		
		$sql_order_inv=sql_select("select a.mst_id, group_concat(b.po_number) as po_no, group_concat(c.style_ref_no) as style_ref_no, group_concat(date_format(c.insert_date,'%d-%m-%Y')) as style_date from com_export_invoice_ship_dtls a, wo_po_break_down b, wo_po_details_master c where a.po_breakdown_id=b.id and b.job_no_mst=c.job_no and a.mst_id in($all_inv_id) group by a.mst_id");
		
	}
	else
	{
		$all_inv_id=return_field_value("listagg(cast(b.invoice_id as varchar(4000)), ',') within group(order by b.invoice_id) as inv_id","com_export_doc_submission_mst a, com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and a.entry_form=39 and a.id=".$data,"inv_id");
		
		
		$sql_order_inv=sql_select("select a.mst_id, listagg(cast(b.po_number as varchar(4000)), ',') within group(order by b.po_number) as po_no, listagg(cast(c.style_ref_no as varchar(4000)), ',') within group(order by c.style_ref_no) as style_ref_no, listagg(cast(to_char(c.insert_date,'DD-MM-YYYY') as varchar(4000)), ',') within group(order by to_char(c.insert_date,'DD-MM-YYYY')) as style_date from com_export_invoice_ship_dtls a, wo_po_break_down b, wo_po_details_master c where a.po_breakdown_id=b.id and b.job_no_mst=c.job_no and a.mst_id in($all_inv_id) group by a.mst_id");
	}*/
	//var_dump($order_inv_array);die;
	
	if($db_type==0)
	{
		$sql_order_inv=sql_select("select b.id as po_id, b.po_number as po_no, c.style_ref_no as style_ref_no, date_format(c.insert_date,'%d-%m-%Y') as style_date from  wo_po_break_down b, wo_po_details_master c where  b.job_no_mst=c.job_no");
		
	}
	else
	{
		$sql_order_inv=sql_select("select b.id as po_id, b.po_number as po_no, c.style_ref_no as style_ref_no,to_char(c.insert_date,'DD-MM-YYYY') as style_date from  wo_po_break_down b, wo_po_details_master c where b.job_no_mst=c.job_no ");
	}
	foreach($sql_order_inv as $row)
	{
		$order_inv_array[$row[csf("po_id")]]["po_no"]=$row[csf("po_no")];
		$order_inv_array[$row[csf("po_id")]]["style_ref_no"]=$row[csf("style_ref_no")];
		$order_inv_array[$row[csf("po_id")]]["style_date"]=$row[csf("style_date")];
	}	
	
	$sql_sub_buyer="select a.id as sub_id, a.company_id, a.buyer_id, a.submit_date, b.is_lc, b.lc_sc_id, b.all_order_no, c.id as inv_id, c.invoice_no, c.invoice_date, c.invoice_quantity, c.invoice_value, c.bl_no, c.bl_date from com_export_doc_submission_mst a, com_export_doc_submission_invo b, com_export_invoice_ship_mst c where a.id=b.doc_submission_mst_id and b.invoice_id=c.id and a.entry_form=39 and a.id=$data";
	//echo $sql_sub_buyer;die;
	$result=sql_select($sql_sub_buyer);
	
	$sql_buyer_info=sql_select("select id, buyer_name, contact_person, address_1 from lib_buyer where id='".$result[0][csf("buyer_id")]."' ");
	
	$sql_lc=sql_select("SELECT id,export_lc_no,lc_date FROM com_export_lc ");
	foreach($sql_lc as $row)
	{
		$export_lc_no_arr[$row[csf("id")]]["export_lc_no"]=$row[csf("export_lc_no")];
		$export_lc_no_arr[$row[csf("id")]]["lc_date"]=$row[csf("lc_date")];
	}
	$sql_sc=sql_select("SELECT id,contract_no,contract_date FROM com_sales_contract ");
	foreach($sql_sc as $row)
	{
		$export_sc_no_arr[$row[csf("id")]]["contract_no"]=$row[csf("contract_no")];
		$export_sc_no_arr[$row[csf("id")]]["contract_date"]=$row[csf("contract_date")];
	}
	

	 
	/*//echo $data."jahid";die;
	if($db_type==2)
	{
		$sql_com="select a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.origin, a.issuing_bank_id, a.currency_id, a.supplier_id, a.lc_value, a.margin, listagg(cast(b.is_lc_sc || '__' || b.lc_sc_id as varchar(4000)),',') within group (order by b.is_lc_sc,b.lc_sc_id) as lc_sc  from com_btb_lc_master_details a, com_btb_export_lc_attachment b where a.id=b.import_mst_id and  a.id=$data and a.is_deleted = 0 AND a.status_active = 1 group by a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.origin, a.issuing_bank_id, a.currency_id, a.supplier_id, a.lc_value, a.margin";
	}
	elseif($db_type==0)
	{
		$sql_com="select a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.origin, a.issuing_bank_id, a.currency_id, a.supplier_id, a.lc_value, a.margin, group_concat(concat(b.is_lc_sc, '__', b.lc_sc_id)) as lc_sc  from com_btb_lc_master_details a, com_btb_export_lc_attachment b where a.id=b.import_mst_id and  a.id=$data and a.is_deleted = 0 AND a.status_active = 1 group by a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.supplier_id, a.origin, a.issuing_bank_id, a.currency_id, a.lc_value, a.margin";
	}
	
	$result=sql_select($sql_com);
	
	$company_name = return_field_value("company_name","lib_company","id=".$result[0][csf("importer_id")],"company_name");
	$supplier_name = return_field_value("supplier_name","lib_supplier","id=".$result[0][csf("supplier_id")],"supplier_name");
	$country_name = return_field_value("country_name"," lib_country","id=".$result[0][csf("origin")],"country_name");*/
	
	//echo $sql_com;
	
	?>
    
    <table width="700" cellpadding="0" align="left" cellspacing="0" border="0">
        <tr>
        	<td colspan="3" height="110"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">Dated : <?php echo change_date_format($result[0][csf("submit_date")]); ?> </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="20"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            <?php
				echo "The Manager, <br>";
				echo $sql_buyer_info[0][csf("buyer_name")];
				$adress_all=explode(",",$sql_buyer_info[0][csf("address_1")]);
				$i=0;
				$address_first="";
				foreach($adress_all as $adress)
				{
					if($i%3==0) $address_first.="<br>";
					$address_first.=$adress.", ";
					$i++;
				}
				$address_first=chop($address_first," , ");
				echo $address_first;
				//echo $bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["contact_person"]."<br>".$bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["bank_name"]."<br>".$bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["branch_name"]."<br>".$bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["address"];
			?>
           
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="30"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            Subject: Submission of shipping documents with original BL & CO after payment realization.
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="20"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left"> Dear Sir, </td>
            <td width="25" ></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            We like to inform you that we have submitted the following shipping documents to your office for payment. 
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            	<table cellpadding="0" align="left" cellspacing="0" border="1" width="650">
                	<thead>
                    	<tr>
                        	<th width="80">Lc/Sc. No <br> Date</th>
                            <th width="120">Order No</th>
                            <th width="100">Invoice No <br> Date</th>
                            <th width="120">Style No</th>
                            <th width="60">Quantity</th>
                            <th width="70">Value</th>
                            <th>Bl No <br> Date</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$i=1;
					foreach($result as $row)
					{
						$all_order_arr=explode(",",$row[csf("all_order_no")]);
						$all_po=$all_style=$all_style_date="";
						foreach($all_order_arr as $order_id)
						{
							$all_po.=$order_inv_array[$order_id]["po_no"].", ";
							$all_style.=$order_inv_array[$order_id]["style_ref_no"].", ";
							$all_style_date.=$order_inv_array[$order_id]["style_date"].", ";
						}
						$all_po=chop($all_po," , ");
						$all_style=chop($all_style," , ");
						$all_style_date=chop($all_style_date," , ");
						if($row[csf("bl_date")]!="" && $row[csf("bl_date")]!='0000-00-00') $bl_date=change_date_format($row[csf("bl_date")]); else $bl_date="&nbsp;";
						?>
                    	<tr>
                        	<td width="80" valign="top"><div style="width:80px;; word-wrap:break-word;">
							<?php 
							if($row[csf("is_lc")]==1)
							{
								echo $export_lc_no_arr[$row[csf("lc_sc_id")]]["export_lc_no"]."<br>";
								echo change_date_format($export_lc_no_arr[$row[csf("lc_sc_id")]]["lc_date"]);
							}
							else
							{
								echo $export_sc_no_arr[$row[csf("lc_sc_id")]]["contract_no"]."<br>";
								echo change_date_format($export_sc_no_arr[$row[csf("lc_sc_id")]]["contract_date"]);
							}
							?></div>
                            </td>
                            <td width="120" valign="top"><div style="width:120px;; word-wrap:break-word;"><?php echo $all_po; ?></div></td>
                            <td width="100" valign="top"><div style="width:100px;; word-wrap:break-word;"><?php echo $row[csf("invoice_no")]."<br>".$row[csf("invoice_date")];?> </div></td>
                            <td width="120" valign="top"><div style="width:120px;; word-wrap:break-word;"><?php echo $all_style."<br>".$all_style_date;?></div></td>
                            <td width="60" valign="top" align="right"><div style="width:60px;; word-wrap:break-word;"><?php echo number_format($row[csf("invoice_quantity")],2,'.','')?></div></td>
                            <td width="70" valign="top" align="right"><div style="width:70px;; word-wrap:break-word;"><?php echo number_format($row[csf("invoice_value")],2,'.','')?></div></td>
                            <td valign="top"><?php echo $row[csf("bl_no")]."<br>".$bl_date; ?></td>
                        </tr>
                        <?php
						$i++;
					}
					?>
                    </tbody>
                </table>
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
    
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            Please acknowledge receipt for above shipping documents with original  BL endorsed by our bank.
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
        
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            Thanking You,
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="80"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
			<?php 
			
			echo $company_name;
			 
			?></td>
            <td width="25" ></td>
        </tr>
       <tr>
        	<td colspan="3" height="50"></td>
       </tr>
       <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            Authorized signature
            </td>
            <td width="25" ></td>
        </tr>
    </table>
    <?php
	exit();

}



?>


 