-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_company`
--

DROP TABLE IF EXISTS `lib_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT '0',
  `company_name` varchar(64) DEFAULT NULL,
  `company_short_name` varchar(5) DEFAULT NULL,
  `service_cost_allocation` int(11) DEFAULT NULL,
  `posting_pre_year` int(11) DEFAULT '0',
  `statutory_account` int(11) DEFAULT NULL,
  `contract_person` varchar(64) DEFAULT NULL,
  `ceo` varchar(64) DEFAULT NULL,
  `cfo` varchar(64) DEFAULT NULL,
  `company_nature` int(11) DEFAULT '0',
  `core_business` int(11) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `website` varchar(64) DEFAULT NULL,
  `ac_code_length` int(3) DEFAULT '0',
  `profit_center_affected` tinyint(1) DEFAULT '0' COMMENT '0=no,1=yes',
  `plot_no` varchar(20) DEFAULT NULL,
  `level_no` varchar(20) DEFAULT NULL,
  `road_no` varchar(20) DEFAULT NULL,
  `block_no` varchar(20) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `province` varchar(30) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL COMMENT 'Employee Code',
  `zip_code` varchar(20) DEFAULT NULL,
  `trade_license_no` varchar(20) DEFAULT NULL COMMENT 'Employee Code',
  `incorporation_no` varchar(20) DEFAULT NULL,
  `erc_no` varchar(20) DEFAULT NULL,
  `irc_no` varchar(20) DEFAULT NULL,
  `tin_number` varchar(20) DEFAULT NULL,
  `contact_no` varchar(20) NOT NULL,
  `vat_number` varchar(20) DEFAULT NULL,
  `epb_reg_no` varchar(20) DEFAULT NULL,
  `trade_license_renewal` date DEFAULT NULL,
  `erc_expiry_date` date DEFAULT NULL,
  `irc_expiry_date` date DEFAULT NULL,
  `bang_bank_reg_no` varchar(50) DEFAULT NULL,
  `graph_color` varchar(15) NOT NULL DEFAULT '0',
  `logo_location` varchar(50) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `company_name` (`company_name`),
  KEY `company_nature` (`company_nature`),
  KEY `core_business` (`core_business`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_company`
--

LOCK TABLES `lib_company` WRITE;
/*!40000 ALTER TABLE `lib_company` DISABLE KEYS */;
INSERT INTO `lib_company` VALUES (1,1,'Peak Apparels Limited','PAL',2,2,2,'','','',1,1,'porna@pkapparels.net','www.peakapparels.com',0,2,'65/1','Vogra','National University','',21,'','Joydevpur,  Gazipur','','','','12345','','','','','','2015-04-30','2015-12-31','2015-04-30','','','',0,NULL,1,'2015-10-07 14:31:09',1,0,0);
/*!40000 ALTER TABLE `lib_company` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
