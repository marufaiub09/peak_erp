-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_btb_export_lc_attachment`
--

DROP TABLE IF EXISTS `com_btb_export_lc_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_btb_export_lc_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `import_mst_id` int(11) NOT NULL DEFAULT '0',
  `lc_sc_id` int(11) NOT NULL DEFAULT '0',
  `is_lc_sc` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=export lc; 1=sales contract',
  `current_distribution` double NOT NULL DEFAULT '0',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `import_mst_id` (`import_mst_id`),
  KEY `lc_sc_id` (`lc_sc_id`),
  KEY `is_lc_sc` (`is_lc_sc`),
  KEY `is_deleted` (`is_deleted`),
  KEY `status_active` (`status_active`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_btb_export_lc_attachment`
--

LOCK TABLES `com_btb_export_lc_attachment` WRITE;
/*!40000 ALTER TABLE `com_btb_export_lc_attachment` DISABLE KEYS */;
INSERT INTO `com_btb_export_lc_attachment` VALUES (1,1,5,0,6362.5,0,1),(2,2,14,0,13957.5,0,1),(3,3,14,0,12240,0,1),(4,4,14,0,23400,0,1),(5,5,14,0,6972,0,1),(6,7,15,0,33540,0,1),(7,8,24,0,2170,0,1),(8,9,24,0,6030,0,1),(9,10,2,1,25110.0427,0,1),(10,11,2,1,6120,0,1),(11,12,16,0,3537.2,0,1),(12,13,2,1,10155.2362,0,1),(13,14,16,0,4312.656,0,1),(14,15,12,0,2579.789,0,1),(15,16,26,0,5006.4638,0,1),(16,17,3,1,25300,0,1),(17,18,30,0,37660,0,1),(18,19,24,0,18090,0,1),(19,20,4,1,15625,0,1),(21,22,13,0,26319.7795,0,1),(22,22,26,0,19139.9007,0,1),(23,22,27,0,22807.1198,0,1),(24,23,9,0,10002,0,1),(25,24,27,0,1951.385,0,1),(26,25,28,0,15887,0,1),(27,26,26,0,3181.4862,0,1),(28,26,8,0,241.439,0,1),(29,26,27,0,3791.0613,0,1),(30,26,9,0,2786.0135,0,1),(31,27,26,0,10855,0,1),(33,21,15,0,0,0,1),(34,28,2,1,9936.8,0,1),(35,29,5,1,3780,0,1),(36,30,3,1,8710,0,1),(37,31,30,0,17550,0,1),(38,32,2,1,8666.4067,0,1),(39,33,16,0,19999.38,0,1),(40,34,16,0,6262.398,0,1),(41,35,36,0,24300,0,1),(42,36,4,1,1478.2518,0,1),(43,37,4,1,11545,0,1),(44,38,2,1,9899.8245,0,1),(45,39,2,1,15614.3283,0,1),(46,41,3,1,3712.9764,0,1),(47,42,37,0,8999.8774,0,1),(48,40,6,1,65318.4,0,1);
/*!40000 ALTER TABLE `com_btb_export_lc_attachment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
