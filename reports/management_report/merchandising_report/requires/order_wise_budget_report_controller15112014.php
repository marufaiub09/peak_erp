<?

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
//--------------------------------------------------------------------------------------------------------------------
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$buyer_short_name_library=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$item_library=return_library_array( "select id, item_name from lib_item_group", "id", "item_name"  );
$yarn_count_library=return_library_array( "select id, yarn_count from lib_yarn_count", "id", "yarn_count"  );
$costing_library=return_library_array( "select job_no, costing_date from wo_pre_cost_mst", "job_no", "costing_date"  );
$team_member_arr=return_library_array( "select id, team_member_name from lib_mkt_team_member_info",'id','team_member_name');
$order_arr=return_library_array( "select id, po_number from wo_po_break_down", "id", "po_number"  );



if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 160, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );     	 
	exit();
}
if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
	
		function js_set_value(str)
		{
			var splitData = str.split("_");
			//alert (splitData[1]);
			$("#hide_job_id").val(splitData[0]); 
			$("#hide_job_no").val(splitData[1]); 
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Job No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:100px;" onClick="reset_form('styleRef_form','search_div','','','','');"></th> 					<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                    <input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <? 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?
                       		$search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<? echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+'<? echo $cbo_year_id; ?>'+'**'+'<? echo $cbo_month_id; ?>', 'create_job_no_search_list_view', 'search_div', 'order_wise_budget_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
	exit(); 
}

if($action=="create_job_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	$year_id=$data[4];
	$month_id=$data[5];
	//echo $month_id;
	
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	if($data[1]==0)
	{
	if ($_SESSION['logic_erp']["data_level_secured"]==1)
	{
	if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
	}
	else
	{
	$buyer_id_cond="";
	}
	}
	else
	{
	$buyer_id_cond=" and buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";
	
	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	//$year="year(insert_date)";
	if($db_type==0) $year_field="YEAR(insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year";
	else $year_field="";
	
	
	if($db_type==0)
	{
	if($year_id!=0) $year_cond=" and year(insert_date)=$year_id"; else $year_cond="";	
	}
	else if($db_type==2)
	{
	$year_field_con=" and to_char(insert_date,'YYYY')";
	if($year_id!=0) $year_cond="$year_field_con=$year_id"; else $year_cond="";	
	
	}
	
	//if($month_id!=0) $month_cond=" and month(insert_date)=$month_id"; else $month_cond="";
	
	$arr=array (0=>$company_arr,1=>$buyer_arr);
	
	$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no, $year_field from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $year_cond  order by job_no";
	
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","600","240",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0,0','') ;
	exit(); 
} // Job Search end

if ($action=="order_no_popup")
	{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
					<script>
                    function js_set_value(str)
                    {
                        var splitData = str.split("_");
                        //alert (splitData[1]);
                        $("#order_no_id").val(splitData[0]); 
                        $("#order_no_val").val(splitData[1]); 
                        parent.emailwindow.hide();
                    }
                    </script>
                     <input type="hidden" id="order_no_id" />
                     <input type="hidden" id="order_no_val" />
                 <?
					if ($data[1]==0) $buyer_name=""; else $buyer_name=" and b.buyer_name=$data[1]";
					if ($data[2]=="") $order_no=""; else $order_no=" and a.po_number=$data[2]";
					$job_no=str_replace("'","",$txt_job_id);
					if($db_type==0)
					{
					if ($data[2]=="") $job_no_cond=""; else $job_no_cond="  and FIND_IN_SET(b.job_no_prefix_num,'$data[2]')";
					}
					else if($db_type==2)
					{
						
					if ($data[2]=="") $job_no_cond=""; else $job_no_cond="  and ',' || b.job_no_prefix_num || ',' LIKE '%$data[2]%' ";
					}
					
					$sql="select a.id, a.po_number, b.job_no_prefix_num, b.job_no, b.buyer_name, b.style_ref_no from wo_po_details_master b, wo_po_break_down a  where b.job_no=a.job_no_mst and b.company_name=$data[0] and b.is_deleted=0 $buyer_name $job_no_cond ORDER BY b.job_no";
					//echo $sql;
					$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
					$arr=array(1=>$buyer);
					
					echo  create_list_view("list_view", "Job No,Buyer,Style Ref.,Order No", "110,110,150,180","610","350",0, $sql, "js_set_value", "id,po_number", "", 1, "0,buyer_name,0,0,0", $arr , "job_no_prefix_num,buyer_name,style_ref_no,po_number", "order_wise_budget_report_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
					disconnect($con);
					exit(); 
	}					// Order Search End
					$tmplte=explode("**",$data);

					if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
					if ($template=="") $template=1;

				  if($action=="report_generate")
	  				{ 
					$process = array( &$_POST );
					extract(check_magic_quote_gpc( $process )); 
					$report_type=str_replace("'","",$reporttype);
					//echo $report_type;
					//echo $cbo_search_date;die;
					$company_name=str_replace("'","",$cbo_company_name);
					if(str_replace("'","",$cbo_buyer_name)==0)
					{
						if ($_SESSION['logic_erp']["data_level_secured"]==1)
						{
							if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
						}
						else
						{
							$buyer_id_cond="";
						}
					}
					else
					{
						$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
					}
					
					$cbo_year=str_replace("'","",$cbo_year);
					if($db_type==0)
						{
						if(trim($cbo_year)!=0) $year_cond=" and YEAR(a.insert_date)=$cbo_year"; else $year_cond="";
						}
					else if($db_type==2)
						{
						$year_field_con=" and to_char(a.insert_date,'YYYY')";
						if(trim($cbo_year)!=0) $year_cond=" $year_field_con=$cbo_year"; else $year_cond="";
						}
					$order_status_id=str_replace("'","",$cbo_order_status);
					$order_status_cond='';
					if($order_status_id==0)
					{
						$order_status_cond=" and b.is_confirmed in(1,2)";
					}
					else if($order_status_id!=0)
					{
					$order_status_cond=" and b.is_confirmed=$order_status_id";	
					}
					
					$date_cond='';
					if(str_replace("'","",$cbo_search_date)==1)
					{
						if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
						{
						 if($db_type==0)
							{
								$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd","");
								$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd","");
							}
							else if($db_type==2)
							{
								$start_date=change_date_format(str_replace("'","",$txt_date_from),"","",1);
								$end_date=change_date_format(str_replace("'","",$txt_date_to),"","",1);
							}
					 
						$date_cond=" and b.pub_shipment_date between '$start_date' and '$end_date'";
						}
					}
					else if(str_replace("'","",$cbo_search_date)==2)
					{
						if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
						{
						 if($db_type==0)
							{
								$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd","");
								$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd","");
							}
							else if($db_type==2)
							{
								$start_date=change_date_format(str_replace("'","",$txt_date_from),"","",1);
								$end_date=change_date_format(str_replace("'","",$txt_date_to),"","",1);
							}
					 
						$date_cond=" and b.po_received_date between '$start_date' and '$end_date'";
						}
					}
					$job_no=str_replace("'","",$txt_job_no);
					if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in ($job_no) ";
					$order_no=str_replace("'","",$txt_order_id);
					$order_num=str_replace("'","",$txt_order_no);
					if(str_replace("'","",$txt_order_id)!="" && str_replace("'","",$txt_order_id)!=0) $order_id_cond_trans=" and b.id in ($order_no)";
					else if ($order_num=="") $order_no_cond=""; else $order_no_cond=" and  b.po_number in ('$order_num') ";
			if($report_type==1)
			{
					if($template==1)
					{
					ob_start();
					$style1="#E9F3FF"; 
					$style="#FFFFFF";
 
 					$fab_precost_arr=array();$commission_array=array();$knit_arr=array(); $fabriccostArray=array(); $fab_emb=array();$fabric_data_Array=array();$asking_profit_arr=array(); $yarncostArray=array(); $yarn_desc_array=array();
					
					$yarncostDataArray=sql_select("select job_no, count_id, type_id, sum(cons_qnty) as cons_qnty, sum(amount) as amount from wo_pre_cost_fab_yarn_cost_dtls where status_active=1 and is_deleted=0 group by job_no, count_id, type_id");
					foreach($yarncostDataArray as $yarnRow)
					{
					   $yarncostArray[$yarnRow[csf('job_no')]].=$yarnRow[csf('count_id')]."**".$yarnRow[csf('type_id')]."**".$yarnRow[csf('cons_qnty')]."**".$yarnRow[csf('amount')].",";
					}
					$asking_profit=sql_select("select id,company_id,asking_profit,max_profit from lib_standard_cm_entry where status_active=1 and is_deleted=0");
					foreach($asking_profit as $ask_row )
					{
					$asking_profit_arr[$ask_row[csf('company_id')]]['asking_profit']=$ask_row[csf('asking_profit')];
					$asking_profit_arr[$ask_row[csf('company_id')]]['max_profit']=$ask_row[csf('max_profit')];
					} //var_dump($asking_profit_arr);
					$fab_arr=sql_select("select a.job_no,a.pre_cost_fabric_cost_dtls_id, a.po_break_down_id, sum(a.requirment) as requirment ,sum(a.pcs) as pcs from wo_pre_cos_fab_co_avg_con_dtls a,wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.job_no=b.job_no  and b.status_active=1 and b.is_deleted=0 group by a.po_break_down_id,a.pre_cost_fabric_cost_dtls_id,a.job_no");
					foreach($fab_arr as $row_pre)
					{
					$fab_precost_arr[$row_pre[csf('job_no')]][$row_pre[csf('po_break_down_id')]].=$row_pre[csf('requirment')]."**".$row_pre[csf('pcs')].",";	
					}
					$fabricDataArray=sql_select("select a.job_no, a.fab_nature_id, a.fabric_source, a.rate, b.yarn_cons_qnty, b.yarn_amount from wo_pre_cost_fabric_cost_dtls a, wo_pre_cost_sum_dtls b where a.job_no=b.job_no and a.fabric_source!=3 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
					foreach($fabricDataArray as $fabricRow)
					{
					$fabric_data_Array[$fabricRow[csf('job_no')]].=$fabricRow[csf('fab_nature_id')]."**".$fabricRow[csf('fabric_source')]."**".$fabricRow[csf('rate')]."**".$fabricRow[csf('yarn_cons_qnty')]."**".$fabricRow[csf('yarn_amount')].",";
					}//Pre cost end
					
					 $data_array_emb=("select  job_no,
					 sum(CASE WHEN emb_name=1 THEN amount END) AS print_amount,
					 sum(CASE WHEN emb_name=2 THEN amount END) AS embroidery_amount,
					 sum(CASE WHEN emb_name=4 THEN amount END) AS special_amount,
					 sum(CASE WHEN emb_name=5 THEN amount END) AS other_amount
					 from  wo_pre_cost_embe_cost_dtls where  status_active=1 and  is_deleted=0 and emb_name!=3 group by job_no");
					 $embl_array=sql_select($data_array_emb);
					foreach($embl_array as $row_emb)
					 {
					 $fab_emb[$row_emb[csf('job_no')]]['print']=$row_emb[csf('print_amount')];
					 $fab_emb[$row_emb[csf('job_no')]]['embroidery']=$row_emb[csf('embroidery_amount')];
					 $fab_emb[$row_emb[csf('job_no')]]['special']=$row_emb[csf('special_amount')];
					 $fab_emb[$row_emb[csf('job_no')]]['other']=$row_emb[csf('other_amount')];
					 }
					 $fabriccostDataArray=sql_select("select job_no, costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost,certificate_pre_cost,currier_pre_cost from wo_pre_cost_dtls where status_active=1 and is_deleted=0  ");
					foreach($fabriccostDataArray as $fabRow)
					{
					 $fabriccostArray[$fabRow[csf('job_no')]]['costing_per_id']=$fabRow[csf('costing_per_id')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['trims_cost']=$fabRow[csf('trims_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['embel_cost']=$fabRow[csf('embel_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['cm_cost']=$fabRow[csf('cm_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['commission']=$fabRow[csf('commission')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['common_oh']=$fabRow[csf('common_oh')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['lab_test']=$fabRow[csf('lab_test')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['inspection']=$fabRow[csf('inspection')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['freight']=$fabRow[csf('freight')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['comm_cost']=$fabRow[csf('comm_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['certificate_pre_cost']=$fabRow[csf('certificate_pre_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['currier_pre_cost']=$fabRow[csf('currier_pre_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['c_cost']=$fabRow[csf('cm_cost')];
					} 
					$knit_data=sql_select("select job_no,
					  sum(CASE WHEN cons_process=1 THEN amount END) AS knit_charge,
					  sum(CASE WHEN cons_process=2 THEN amount END) AS weaving_charge,
					  sum(CASE WHEN cons_process=3 THEN amount END) AS knit_charge_collar_cuff,
					  sum(CASE WHEN cons_process=4 THEN amount END) AS knit_charge_feeder_stripe,
					  sum(CASE WHEN cons_process in(64,82,89) THEN amount END) AS washing_cost,
					  sum(CASE WHEN cons_process in(35,36,37) THEN amount END) AS all_over_cost,
					  sum(CASE WHEN cons_process=30 THEN amount END) AS yarn_dyeing_cost,
					  sum(CASE WHEN cons_process=33 THEN amount END) AS heat_setting_cost,
					  sum(CASE WHEN cons_process in(25,31,32,60,61,62,63,72,80,81,84,85,86,87,38,74,78,79) THEN amount END) AS fabric_dyeing_cost,
					  sum(CASE WHEN cons_process in(34,65,66,67,68,69,70,71,73,75,76,77,88,90,91,92,93,100,125,127,128,129) THEN amount END) AS fabric_finish_cost
					  from wo_pre_cost_fab_conv_cost_dtls where  status_active=1 and is_deleted=0 group by job_no");
					foreach($knit_data as $row_knit)
					{
					$knit_arr[$row_knit[csf('job_no')]]['knit']=$row_knit[csf('knit_charge')];
					$knit_arr[$row_knit[csf('job_no')]]['weaving']=$row_knit[csf('weaving_charge')];
					$knit_arr[$row_knit[csf('job_no')]]['collar_cuff']=$row_knit[csf('knit_charge_collar_cuff')];
					$knit_arr[$row_knit[csf('job_no')]]['feeder_stripe']=$row_knit[csf('knit_charge_feeder_stripe')];
					$knit_arr[$row_knit[csf('job_no')]]['washing']=$row_knit[csf('washing_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['all_over']=$row_knit[csf('all_over_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['fabric_dyeing']=$row_knit[csf('fabric_dyeing_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['yarn_dyeing']=$row_knit[csf('yarn_dyeing_cost')];	
					$knit_arr[$row_knit[csf('job_no')]]['heat']=$row_knit[csf('heat_setting_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['fabric_finish']=$row_knit[csf('fabric_finish_cost')];	
					}
					$data_array=sql_select("select  job_no,
					 sum(CASE WHEN particulars_id=1 THEN commission_amount END) AS foreign_comm,
					 sum(CASE WHEN particulars_id=2 THEN commission_amount END) AS local_comm
					 from  wo_pre_cost_commiss_cost_dtls where status_active=1 and is_deleted=0 group by job_no");// quotation_id='$data'
					 foreach($data_array as $row_fl )
					{
						$commission_array[$row_fl[csf('job_no')]]['foreign']=$row_fl[csf('foreign_comm')];
						$commission_array[$row_fl[csf('job_no')]]['local']=$row_fl[csf('local_comm')];
					}
	?>
				<script>
                    var total_fab_cost=document.getElementById('total_fab_cost').value;
                    var total_fab_percent=document.getElementById('total_fab_percent').value;
                    document.getElementById('fab_cost').innerHTML=total_fab_cost;
                    document.getElementById('fab_percent').innerHTML=total_fab_percent;
                    
                    var total_trim_cost=document.getElementById('total_trim_cost').value;
                    var total_trim_percent=document.getElementById('total_trim_percent').value;
                    document.getElementById('trim_cost_id').innerHTML=total_trim_cost;
                    document.getElementById('trim_percent').innerHTML=total_trim_percent;
                    
                    var total_embelishment_cost=document.getElementById('total_embelishment_cost').value;
                    var total_embelishment_percent=document.getElementById('total_embelishment_percent').value;
                    document.getElementById('embelishment_id').innerHTML=total_embelishment_cost;
                    document.getElementById('embelishment_percent').innerHTML=total_embelishment_percent;
                    
                    var total_commercial_cost=document.getElementById('total_commercial_cost').value;
                    var total_commercial_percent=document.getElementById('total_commercial_percent').value;
                    document.getElementById('commercial_id').innerHTML=total_commercial_cost;
                    document.getElementById('commercial_percent').innerHTML=total_commercial_percent;
                    
                    var total_commssion_cost=document.getElementById('total_commssion_cost').value;
                    var total_commssion_percent=document.getElementById('total_commssion_percent').value;
                    document.getElementById('commission_id').innerHTML=total_commssion_cost;
                    document.getElementById('commission_percent').innerHTML=total_commssion_percent;
                    
                    var total_testing_cost=document.getElementById('total_testing_cost').value;
                    var total_testing_cost_percent=document.getElementById('total_testing_cost_percent').value;
                    document.getElementById('testing_id').innerHTML=total_testing_cost;
                    document.getElementById('testing_percent').innerHTML=total_testing_cost_percent;
                    
                    var total_freight_cost=document.getElementById('total_freight_cost').value;
                    var total_freight_cost_percent=document.getElementById('total_freight_cost_percent').value;
                    document.getElementById('freight_id').innerHTML=total_freight_cost;
                    document.getElementById('freight_percent').innerHTML=total_freight_cost_percent;
                    var total_cost_up=document.getElementById('total_cost_up2').value;
                 
                    document.getElementById('cost_id').innerHTML=total_cost_up;
                   
                    var total_cm_cost=document.getElementById('total_cm_cost').value;
                    var total_cm_percent=document.getElementById('total_cm_percent').value;
                    document.getElementById('cm_id').innerHTML=total_cm_cost;
                    document.getElementById('cm_percent').innerHTML=total_cm_percent;
                    var total_order_amount=document.getElementById('total_order_amount').value;
                    var total_order_amount_percent=document.getElementById('total_order_amount_percent').value;
                    document.getElementById('order_id').innerHTML=total_order_amount;
                    document.getElementById('order_percent').innerHTML=total_order_amount_percent;
                    var total_inspection=document.getElementById('total_inspection').value;
                    var total_inspection_percent=document.getElementById('total_inspection_percent').value;
                    document.getElementById('inspection_id').innerHTML=total_inspection;
                    document.getElementById('inspection_percent').innerHTML=total_inspection_percent;
                    var total_certificate_cost=document.getElementById('total_certificate_cost').value;
                    var total_certificate_percent=document.getElementById('total_certificate_percent').value;
                    document.getElementById('certificate_id').innerHTML=total_certificate_cost;
                    document.getElementById('certificate_percent').innerHTML=total_certificate_percent;
                    var total_common_oh=document.getElementById('total_common_oh').value;
                    var total_common_oh_percent=document.getElementById('total_common_oh_percent').value;
                    document.getElementById('commn_id').innerHTML=total_common_oh;
                    document.getElementById('commn_percent').innerHTML=total_common_oh_percent;
                    
                    var total_common_oh=document.getElementById('total_common_oh').value;
                    var total_common_oh_percent=document.getElementById('total_common_oh_percent').value;
                    document.getElementById('commn_id').innerHTML=total_common_oh;
                    document.getElementById('commn_percent').innerHTML=total_common_oh_percent;
                    var total_currier_cost=document.getElementById('total_currier_cost').value;
                    var total_currier_cost_percent=document.getElementById('total_currier_cost_percent').value;
                    document.getElementById('courier_id').innerHTML=total_currier_cost;
                    document.getElementById('courier_percent').innerHTML=total_currier_cost_percent;
					
					var total_fab_profit_id=document.getElementById('total_fab_profit_id').value;
                    var total_expected_profit_id=document.getElementById('total_expected_profit_id').value;
                    document.getElementById('fab_profit_id').innerHTML=total_fab_profit_id;
                    document.getElementById('expected_id').innerHTML=total_expected_profit_id;
					
					var total_expt_profit_variance=document.getElementById('total_expt_profit_variance_id').value;
                  
                    document.getElementById('expt_p_variance_id').innerHTML=total_expt_profit_variance;
                  
				    var total_cost_percent=document.getElementById('total_cost_percent').value;
				    document.getElementById('cost_percent').innerHTML=total_cost_percent;
					var total_profit_fab_percentage=document.getElementById('total_profit_fab_percentage_id').value;
				    document.getElementById('profit_fab_percentage').innerHTML=total_profit_fab_percentage;
					var total_expt_profit_percentage=document.getElementById('total_expt_profit_percentage_id').value;
				    document.getElementById('profit_expt_fab_percentage').innerHTML=total_expt_profit_percentage;
					var total_expt_profit_percentage=document.getElementById('total_expt_profit_variance_percentage_id').value;
				    document.getElementById('expt_p_percent').innerHTML=total_expt_profit_percentage;
					var expected_profit_percent=document.getElementById('expected_profit_percent').value;
				    document.getElementById('expt_percent').innerHTML=expected_profit_percent;
					
				function toggle() 
				{
					var ele = document.getElementById("yarn_summary");
					//alert(ele);
					var text = document.getElementById("displayText");
					if(ele.style.display!= "none") 
					{
						ele.style.display = "none";
						text.innerHTML = "Show Yarn Summary";
					}
					else 
					{
						ele.style.display = "block";
						text.innerHTML = "Hide Yarn Summary";
					}
				} 
				
				
            
				 </script>
        <div style="width:4570px;">
        <div style="width:900px;" align="left">
        	<table width="900" cellpadding="0" cellspacing="2" border="0">
            	
                <tr>
                	<td width="600" align="left">
                    	<table width="300" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="2">
                        <caption><strong>Order Wise Budget Cost Summary</strong></caption>
                        <thead align="center">
                        <th>SL</th><th>Particulars</th><th>Amount</th><th>Percentage</th>
                        </thead>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">1</td>
                        <td width="100">Fabric Cost</td><td width="120" align="right" id="fab_cost"></td>
                        <td width="80" align="right" id="fab_percent"></td>
                        </tr>
                        <tr bgcolor="<?  echo $style; ?>">
                        <td width="20">2</td>
                        <td width="100">Trims Cost</td><td align="right" id="trim_cost_id"></td>
                        <td align="right" id="trim_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">3</td>
                        <td width="100">Embellish Cost</td><td align="right" id="embelishment_id"></td>
                        <td align="right" id="embelishment_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">4</td>
                        <td width="100">Commercial Cost</td><td align="right" id="commercial_id"></td>
                        <td align="right" id="commercial_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">5</td>
                        <td width="100">Commision Cost</td><td align="right" id="commission_id"></td>
                        <td align="right" id="commission_percent"> </td>
                        </tr>
                         <tr bgcolor="<? echo $style; ?>">
                        <td width="20">6</td>
                        <td width="100">Testing Cost</td><td align="right" id="testing_id"></td>
                        <td align="right" id="testing_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">7</td>
                        <td width="100">Freight Cost</td><td align="right" id="freight_id"></td>
                        <td align="right" id="freight_percent"> </td>
                        </tr>
                        
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">8</td>
                        <td width="100">Inspection Cost</td><td align="right" id="inspection_id"></td>
                        <td align="right" id="inspection_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">9</td>
                        <td width="100">Certificate Cost</td><td align="right" id="certificate_id"></td>
                        <td align="right" id="certificate_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">10</td>
                        <td width="100">Commn OH</td><td align="right" id="commn_id"></td>
                        <td align="right" id="commn_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">11</td>
                        <td width="100">Courier Cost</td><td align="right" id="courier_id"></td>
                        <td align="right" id="courier_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">12</td>
                        <td width="100">CM Cost</td><td align="right" id="cm_id"></td>
                        <td align="right" id="cm_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">13</td>
                        <td width="100">Total Cost</td><td align="right" id="cost_id"></td>
                        <td align="right" id="cost_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">14</td>
                        <td width="100">Total Order Value</td><td align="right" id="order_id"></td>
                        <td align="right" id="order_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">15</td>
                        <td width="100">Profit/Loss </td><td align="right" id="fab_profit_id"></td>
                        <td align="right" id="profit_fab_percentage"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">16</td>
                        <td width="100">Expected Profit <div id="expt_percent"></div> </td><td align="right" id="expected_id"></td>
                        <td align="right" id="profit_expt_fab_percentage"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">17</td>
                        <td width="100">Expt.Profit Variance </td><td align="right" id="expt_p_variance_id"></td>
                        <td align="right" id="expt_p_percent"> </td>
                        </tr>
                       
                        </table>
                    </td>
                    <td colspan="5" style="min-height:800px; max-height:100%" align="center" valign="top">
                  <div id="chartdiv" style="width:700px; height:900px;" align="center"></div>
                   </td>
                  
                  </tr>
           </table>
           </div>
           <br/>   
         <h3 align="left" id="accordion_h2" style="width:4610px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel2', '')"> -Search Panel</h3>
        <fieldset style="width:100%;" id="content_search_panel2">	
            <table width="4610">
                    <tr class="form_caption">
                        <td colspan="46" align="center"><strong>Order Wise Budget Report</strong></td>
                    </tr>
                    <tr class="form_caption">
                        <td colspan="46" align="center"><strong><? echo $company_library[$company_name]; ?></strong></td>
                    </tr>
                    <tr class="form_caption">
                        <td align="left"><strong>Details Report </strong></td>
                    </tr>
            </table>
            <table id="table_header_1" class="rpt_table" width="4570" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                <tr>
                    <th width="40" rowspan="2">SL</th>
                   	<th width="70" rowspan="2">Buyer</th>
                    <th width="70" rowspan="2">Job No</th>
                    <th width="100" rowspan="2">Order No</th>
                    <th width="100" rowspan="2">Order Status</th>
                    <th width="110" rowspan="2">Style</th>
                    <th width="110" rowspan="2">Item Name</th>
                    <th width="110" rowspan="2">Dealing</th>
                    <?
                   if(str_replace("'","",$cbo_search_date)==1)
					{ ?>
						<th width="70" rowspan="2">Ship. Date</th>
					<? }
					else
					{ ?>
						<th width="70" rowspan="2">PO Recv. Date</th>
					<? }
					?>
                   
                    <th width="90" rowspan="2">Order Qty</th>
                    <th width="90" rowspan="2">Avg Unit Price</th>
                    <th width="100" rowspan="2">Order Value</th>
                    <th colspan="14">Fabric Cost</th>
                    <th width="100" rowspan="2">Trim Cost</th>
                    <th colspan="4">Embell. Cost</th>
                    <th width="120" rowspan="2">Commercial Cost</th>
                    <th colspan="2">Commission</th>
                    <th width="100" rowspan="2">Testing Cost</th>
                    <th width="100" rowspan="2">Freight Cost</th>
                    <th width="120" rowspan="2">Inspection Cost</th>
                    <th width="100" rowspan="2">Certificate Cost</th>
                    <th width="100" rowspan="2">Commn OH</th>
                    <th width="100" rowspan="2">Courier Cost</th>
                    <th width="120" rowspan="2">CM/DZN</th>
                    <th width="100" rowspan="2">CM Cost</th>
                    <th width="100" rowspan="2">Total Cost</th>
                    <th width="100" rowspan="2">Profit/Loss</th>
                    <th width="100" rowspan="2">Profit/Loss %</th>
                    <th width="100" rowspan="2">Expected Profit</th>
                    <th width="" rowspan="2">Expt.Profit Variance</th>
                    </tr>
                    <tr>
                    <th width="100">Avg Yarn Rate</th>
                    <th width="80">Yarn Cost</th>
                    <th width="80">Yarn Cost %</th>
                    <th width="100">Fabric Purchase</th>
                    <th width="80">Knit/ Weav Cost/Dzn</th>
                    <th width="80">Knitting/ Weav Cost</th>
                    <th width="100">Yarn Dye Cost/Dzn </th>
                    <th width="110">Yarn Dyeing Cost </th>
                    <th width="120">Fab.Dye Cost/Dzn</th>
                    <th width="100">Fabric Dyeing Cost</th>
                    <th width="90">Heat Setting</th>
                    <th width="100">Finishing Cost</th>
                    <th width="90">Washing Cost</th>
                    <th width="90">All Over Print</th>
                    <th width="80">Printing</th>
                    <th width="85">Embroidery</th>
                    <th width="80">Washing</th>
                    <th width="80">Other</th>
                    <th width="120">Foreign</th>
                    <th width="120">Local</th>
                   </tr>
                </thead>
            </table>
            <div style="width:4610px; max-height:400px; overflow-y:scroll" id="scroll_body">
             <table class="rpt_table" width="4570" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
                <? 
					
                $i=1; $total_order_qty=0;  $total_yarn_dyeing_cost=0; $total_yarn_cost=0; $total_order_value=0;$total_purchase_cost=0; $grand_tot_trims_cost=0; $total_fabric_dyeing_cost=0; $total_knitting_cost=0; $total_heat_setting_cost=0;$total_finishing_cost=0; $total_washing_cost=0; $fabric_dyeing_cost_dzn=0; $other_cost=0;
$all_over_print_cost=0;$total_trim_cost=0;$total_commercial_cost=0;
                
                $sql="select a.job_no_prefix_num, a.job_no,a.company_name,a.buyer_name,a.style_ref_no,b.is_confirmed,a.agent_name,a.avg_unit_price,a.dealing_marchant, a.gmts_item_id, a.total_set_qnty as ratio,b.plan_cut,b.id as po_id, b.po_number, b.pub_shipment_date,b.po_received_date, b.po_quantity, b.unit_price from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_name' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $date_cond $buyer_id_cond  $year_cond $job_no_cond $order_id_cond_trans $order_no_cond $order_status_cond group by b.id,a.job_no_prefix_num, a.job_no,a.company_name,a.buyer_name,b.is_confirmed,a.style_ref_no,a.agent_name,a.avg_unit_price,a.dealing_marchant,a.total_set_qnty,b.plan_cut, a.gmts_item_id, b.po_number, b.pub_shipment_date,b.po_received_date, b.po_quantity, b.unit_price order by  b.pub_shipment_date,b.id ";
				//echo $sql;
				$result=sql_select($sql);
				 $tot_rows=count($result);
				 foreach($result as $row )
                {
                    if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$total_order_value=$row[csf('po_quantity')]*$row[csf('avg_unit_price')];
					
					if(str_replace("'","",$cbo_search_date)==1)
					{
						$ship_po_recv_date=change_date_format($row[csf('pub_shipment_date')]);
					}
					else if(str_replace("'","",$cbo_search_date)==2)
					{
						$ship_po_recv_date=change_date_format($row[csf('po_received_date')]);
					}
					
				?>
				 <tr bgcolor="<? echo $bgcolor;?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
                
                     <td width="40"><? echo $i; ?></td>
                     <td  width="70" title="<? echo $buyer_library[$row[csf('buyer_name')]] ?>"><p><? echo $buyer_library[$row[csf('buyer_name')]] ?></p></td>
                     <td  width="70" title="<? echo $row[csf('job_no_prefix_num')];  ?>"><p><? echo $row[csf('job_no_prefix_num')];  ?></p></td>
                     <td  width="100" title="<? echo $row[csf('po_number')]; ?>"><p><a href="#" onClick="precost_bom_pop('<?  echo $row[csf('po_id')]; ?>','<?  echo $row[csf('job_no')]; ?>','<?  echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>');"><? echo $row[csf('po_number')]; ?></a></p></td>
                     <td  width="100" title="<? echo $order_status[$row[csf('is_confirmed')]]; ?>"><p><? echo  $order_status[$row[csf('is_confirmed')]]; ?></p></td>
                     <td  width="110" title="<? echo $row[csf('style_ref_no')]; ?>"><p><? echo $row[csf('style_ref_no')]; ?></p></td>
                     <td  width="110" title="<?   echo $gmts_item; ?>"><p><? echo $row[csf('gmts_item_id')]; ?>
					<? $gmts_item='';
                    $gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
                    foreach($gmts_item_id as $item_id)
                    {
                        if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
                    }
                    echo $gmts_item;
					
					?>
                     </p></td>
                     <td  width="110" title="<? echo $team_member_arr[$row[csf('dealing_marchant')]]; ?>"><p><? echo $team_member_arr[$row[csf('dealing_marchant')]]; ?></p></td>
                     <td  width="70" title="<? echo $ship_po_recv_date; ?>"><p><? echo $ship_po_recv_date; ?></p></td>
                     <td  width="90" align="right" title="<? echo number_format($row[csf('po_quantity')],2); ?>"><p><? echo number_format($row[csf('po_quantity')],2); ?></p></td>
                     <td  width="90" align="right" title="<? echo number_format($row[csf('avg_unit_price')],2); ?>"><p><? echo number_format($row[csf('avg_unit_price')],2); ?></p></td>
                     <td width="100" align="right" title="<? echo number_format($total_order_value,2); ?>" ><p><? 
					 $plan_cut_qnty=$row[csf('plan_cut')]*$row[csf('ratio')];
					 $total_order_amount+=$total_order_value;
					 echo number_format($total_order_value,2); ?></p></td>
                     <?
                        $dzn_qnty=0;
						$costing_per_id=$fabriccostArray[$row[csf('job_no')]]['costing_per_id'];
                        if($costing_per_id==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($costing_per_id==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($costing_per_id==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($costing_per_id==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$dzn_qnty_yarn=$dzn_qnty*$row[csf('ratio')];
						$commercial_cost=$fabriccostArray[$row[csf('job_no')]]['comm_cost'];
						$tot_commercial_cost=($commercial_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$fabricData=explode(",",substr($fabric_data_Array[$row[csf('job_no')]],0,-1));
						$fab_precost_Data=explode(",",substr($fab_precost_arr[$row[csf('job_no')]][$row[csf('po_id')]],0,-1));
						foreach($fabricData as $fabricRow)
						{
						$fabricRow=explode("**",$fabricRow);
						$fab_nature_id=$fabricRow[0];	
						$fab_source_id=$fabricRow[1];
						$fab_rate=$fabricRow[2];
						$yarn_qty=$fabricRow[3];
						$yarn_amount=$fabricRow[4];
						if($fab_source_id==2)
							{
							foreach($fab_precost_Data as $fab_row)
							{
								$fab_dataRow=explode("**",$fab_row);
								$fab_requirment=$fab_dataRow[0];
								$fab_pcs=$fab_dataRow[1];
								$fab_purchase_qty=$fab_requirment/$fab_pcs*$row[csf('po_quantity')]; 
							//echo $fab_purchase_qty;
							$fab_purchase=$fab_purchase_qty*$fab_rate; 
								
							}
							}
						else if($fab_source_id==1 || $fab_source_id==3)
							{
							$avg_rate=$yarn_amount/$yarn_qty;
							$yarn_costing=$yarn_amount/$dzn_qnty*$row[csf('po_quantity')];		
							}
						 
						}
						$kniting_cost=$knit_arr[$row[csf('job_no')]]['knit']+$knit_arr[$row[csf('job_no')]]['weaving']+$knit_arr[$row[csf('job_no')]]['collar_cuff']+$knit_arr[$row[csf('job_no')]]['feeder_stripe'];
						$tot_knit_cost=($kniting_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$knit_cost_dzn=$kniting_cost; 
						$washing_cost=($knit_arr[$row[csf('job_no')]]['washing']/$dzn_qnty)*$row[csf('po_quantity')];
						$all_over_cost=($knit_arr[$row[csf('job_no')]]['all_over']/$dzn_qnty)*$row[csf('po_quantity')];;
						$yarn_dyeing_cost=($knit_arr[$row[csf('job_no')]]['yarn_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$yarn_dyeing_cost_dzn=$knit_arr[$row[csf('job_no')]]['yarn_dyeing'];
						$fabric_dyeing_cost=($knit_arr[$row[csf('job_no')]]['fabric_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$fabric_dyeing_cost_dzn=$knit_arr[$row[csf('job_no')]]['fabric_dyeing'];
						$heat_setting_cost=($knit_arr[$row[csf('job_no')]]['heat']/$dzn_qnty)*$row[csf('po_quantity')];
						$fabric_finish=($knit_arr[$row[csf('job_no')]]['fabric_finish']/$dzn_qnty)*$row[csf('po_quantity')];
						
						if($fabric_dyeing_cost<=0)
						{
						$color_fab="red";
						}
						else
						{
						$color_fab="";	
						}
						if($yarn_costing<=0)
						{
						$color_yarn="red";
						}
						else
						{
						$color_yarn="";	
						}
						if($kniting_cost<=0)
						{
						$color_knit="red";
						}
						else
						{
						$color_knit="";	
						}
						if($fabric_finish<=0)
						{
						$color_finish="red";
						}
						else
						{
						$color_finish="";	
						}
						if($commercial_cost<=0)
						{
						$color_com="red";
						}
						else
						{
						$color_com="";	
						}
						
						$yarn_cost_percent=($yarn_costing/$total_order_value)*100;
						$total_yarn_cost_percent+=$yarn_cost_percent;
					 ?>
                     <td width="100" align="right"><a href="##" onClick="generate_pre_cost_report('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','precost_yarn_detail')"><? echo number_format($avg_rate,2); ?></a></td>
                     <td width="80" align="right" title="<? echo $yarn_costing; ?>" bgcolor="<? echo $color_yarn; ?>"><? echo number_format($yarn_costing,2); ?></td>
                     <td width="80" align="right" title="<? echo $yarn_cost_percent; ?>"><? echo number_format($yarn_cost_percent,2); ?></td>
                     <td width="100" align="right"><a href="##" onClick="generate_precost_fab_purchase_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $fab_source_id; ?>','fab_purchase_detail')"><? echo number_format($fab_purchase,2); ?></a></td>
                     <td width="80" title="<? echo $knit_cost_dzn; ?>" align="right"><? echo number_format($knit_cost_dzn,2); ?></td>
                     <td width="80" align="right" title="<? echo $tot_knit_cost; ?>"  bgcolor="<? echo $color_knit; ?>"><?
					 ?>
                     <a href="##" onClick="generate_pre_cost_knit_popup('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $cons_process; //$row[csf('style_ref_no')]; ?>','precost_knit_detail')"><? 
					 echo number_format($tot_knit_cost,2);
					  ?></a></td>
                     <td  width="100" align="right" title="<? echo number_format($yarn_dyeing_cost_dzn ,2); ?>" ><? echo number_format($yarn_dyeing_cost_dzn ,2); ?></td>
                     <td  width="110" align="right" title="<? echo number_format($yarn_dyeing_cost ,2); ?>" ><? echo number_format($yarn_dyeing_cost ,2); ?></td>
                     <td  width="120" align="right"  title="<? echo number_format($fabric_dyeing_cost_dzn ,2); ?>" ><? echo number_format($fabric_dyeing_cost_dzn,2); 
					 $total_fabrics_cost_summary+=($row[csf('po_quantity')]/$dzn_qnty)*$fabric_dyeing_cost_dzn;
						
					  ?></td>
                     
                     <td  width="100" align="right" title="<? echo number_format($fabric_dyeing_cost ,2); ?>" bgcolor="<? echo $color_fab; ?>"><a href="##" onClick="generate_precost_fab_dyeing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $fab_source_id; ?>','fab_dyeing_detail')"><? echo number_format($fabric_dyeing_cost,2); ?></a></td>
                     <td  width="90" align="right"><? echo number_format($heat_setting_cost,2); ?></td>
                     <td  width="100" align="right" ><a href="##" onClick="generate_precost_fab_finishing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','fab_finishing_detail')"><? echo number_format($fabric_finish,2); ?></a> </td>
                     <td  width="90" align="right"><a href="##" onClick="generate_precost_fab_finishing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','fab_washing_detail')"><? echo number_format($washing_cost,2); ?></a></td>
                     <td  width="90" align="right"><a href="##" onClick="generate_precost_fab_all_over_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','fab_all_over_detail')"><? echo number_format($all_over_cost,2); ?></a></td>
				<?
					$tot_trim_amount= $fabriccostArray[$row[csf('job_no')]]['trims_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    $tot_test_cost=$fabriccostArray[$row[csf('job_no')]]['lab_test']/$dzn_qnty*$row[csf('po_quantity')];
                    $print_amount=($fab_emb[$row[csf('job_no')]]['print']/$dzn_qnty)*$row[csf('po_quantity')];
                    $embroidery_amount=($fab_emb[$row[csf('job_no')]]['embroidery']/$dzn_qnty)*$row[csf('po_quantity')];
                    $special_amount=($fab_emb[$row[csf('job_no')]]['special']/$dzn_qnty)*$row[csf('po_quantity')];
                    $other_amount=($fab_emb[$row[csf('job_no')]]['other']/$dzn_qnty)*$row[csf('po_quantity')];
                    $foreign=$commission_array[$row[csf('job_no')]]['foreign']/$dzn_qnty*$row[csf('po_quantity')];
                    $local=$commission_array[$row[csf('job_no')]]['local']/$dzn_qnty*$row[csf('po_quantity')];
                    $freight_cost= $fabriccostArray[$row[csf('job_no')]]['freight']/$dzn_qnty*$row[csf('po_quantity')];
                    $inspection=$fabriccostArray[$row[csf('job_no')]]['inspection']/$dzn_qnty*$row[csf('po_quantity')];
                    $certificate_cost=$fabriccostArray[$row[csf('job_no')]]['certificate_pre_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    
                    $common_oh=$fabriccostArray[$row[csf('job_no')]]['common_oh']/$dzn_qnty*$row[csf('po_quantity')];
                    $currier_cost=$fabriccostArray[$row[csf('job_no')]]['currier_pre_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    //echo $currier_cost;
                    $cm_cost=$fabriccostArray[$row[csf('job_no')]]['c_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    $cm_cost_dzn=$fabriccostArray[$row[csf('job_no')]]['c_cost'];
                    $total_cost=$yarn_costing+$fab_purchase+$tot_knit_cost+$washing_cost+$all_over_cost+$yarn_dyeing_cost+$fabric_dyeing_cost+$heat_setting_cost+$fabric_finish+$tot_trim_amount+$tot_test_cost+$print_amount+$embroidery_amount+$special_amount+$other_amount+$tot_commercial_cost+$foreign+$local+$freight_cost+$inspection+$certificate_cost+$common_oh+$currier_cost+$cm_cost;
					//echo  $total_cost;
					$total_print_amount+=$print_amount;
					$total_embroidery_amount+=$embroidery_amount;
					$total_special_amount+=$special_amount;
					$total_other_amount+=$other_amount;
					
					$total_foreign_amount+=$foreign;
					$total_local_amount+=$local;
					$total_test_cost_amount+=$tot_test_cost;
					$total_freight_amount+=$freight_cost;
					$total_inspection_amount+=$inspection;
					$total_certificate_amount+=$certificate_cost;
					
					$total_common_oh_amount+=$common_oh;
					$total_currier_amount+=$currier_cost;
					$total_cm_amount+=$cm_cost;
					$max_profit=$asking_profit_arr[$row[csf('company_name')]]['max_profit'];
					//echo $max_profit;
					$company_asking=$asking_profit_arr[$row[csf('company_name')]]['asking_profit'];
					
					if($tot_trim_amount<=0)
						{
						$color_trim="red";
						}
						else
						{
						$color_trim="";	
						}
						
					if($cm_cost<=0)
						{
						$color="red";
						}
						else
						{
						$color="";	
						}
						$yarnData=explode(",",substr($yarncostArray[$row[csf('job_no')]],0,-1));
						//print_r($yarnData);
						foreach($yarnData as $yarnRow)
						{
							$yarnRow=explode("**",$yarnRow);
							$count_id=$yarnRow[0];
							$type_id=$yarnRow[1];
							$cons_qnty=$yarnRow[2];
							$amount=$yarnRow[3];
													
							$yarn_desc=$yarn_count_library[$count_id]."**".$yarn_type[$type_id];
							$req_qnty=($plan_cut_qnty/$dzn_qnty_yarn)*$cons_qnty;
							$req_amnt=($plan_cut_qnty/$dzn_qnty_yarn)*$amount;
							 
							$yarn_desc_array[$yarn_desc]['qnty']+=$req_qnty;
							$yarn_desc_array[$yarn_desc]['amnt']+=$req_amnt;
						}
				
					?>
                     <td width="100" align="right" bgcolor="<? echo $color_trim; ?>"><a href="##" onClick="generate_precost_trim_cost_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','trim_cost_detail')"><? echo number_format($tot_trim_amount,2); ?></a></td>
                     <td width="80" align="right"><? echo number_format($print_amount,2); ?></td>
                     <td width="85" align="right"><? echo number_format($embroidery_amount,2); ?></td>
                     <td width="80" align="right"><? echo number_format($special_amount,2); ?></td>
                     <td width="80" align="right"><? echo number_format($other_amount,2); ?></td>
                     <td width="120" align="right" bgcolor="<? echo $color_com; ?>"><? echo number_format($tot_commercial_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($foreign,2) ?></td>
                     <td width="120" align="right"><? echo number_format($local,2) ?></td>
                     <td width="100" align="right"><? echo number_format($tot_test_cost,2);?></td>
                     <td width="100" align="right"><? echo number_format($freight_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($inspection,2);?></td>
                     <td width="100" align="right"><? echo number_format($certificate_cost,2); ?></td>
                     <td width="100" align="right"><? echo number_format($common_oh,2); ?></td>
                     <td width="100" align="right"><? echo number_format($currier_cost,2);?></td>
                     <td width="120" align="right"><? echo number_format($cm_cost_dzn,2);?></td>
                     <td width="100" align="right" bgcolor="<? echo $color; ?>"><? echo number_format($cm_cost,2);?></td>
                     <td width="100" align="right"><? echo number_format($total_cost,2); ?></td>
                    <?
						$total_profit=$total_order_value-$total_cost;
						$total_profit_percentage2=$total_profit/$total_order_value*100; 
						if($total_profit_percentage2<=0 )
						{
							$color_pl="red";
						}
						else if($total_profit_percentage2>$max_profit)
						{
							$color_pl="yellow";	
						}
						else if($total_profit_percentage2<=$max_profit)
						{
							$color_pl="green";	
						}
						else
						{
							$color_pl="";	
						}
					?>
                     <td width="100" align="right" bgcolor="<? echo $color_pl; ?>"><? echo number_format($total_profit,2); ?></td>
                     <td width="100" align="right"><? echo number_format($total_profit_percentage2,2); ?></td>
                     <td width="100" align="right"><?  $expected_profit=$asking_profit_arr[$row[csf('company_name')]]['asking_profit']*$total_order_value/100; echo number_format($expected_profit,2) //$total_profit=$total_cost-$total_order_value; //echo number_format($total_profit,2); ?></td>
                     <td width="" align="right"><? $expect_variance=$total_profit-$expected_profit; echo number_format($expect_variance,2)?></td>
                  </tr> 
                <?
				$total_order_qty+=$row[csf('po_quantity')];
				$total_yarn_dyeing_cost+=$yarn_dyeing_cost;
				$total_yarn_cost+=$yarn_costing;
				$total_purchase_cost+=$fab_purchase;
				$total_knitting_cost+=$tot_knit_cost;
				$total_fabric_dyeing_cost+=$fabric_dyeing_cost;
				$total_heat_setting_cost+=$heat_setting_cost;
				$total_finishing_cost+=$fabric_finish;
				$total_washing_cost+=$washing_cost;
				$all_over_print_cost+=$all_over_cost;
				$total_trim_cost+=$tot_trim_amount;
				$total_commercial_cost+=$tot_commercial_cost;
				$total_fab_cost_amount=$total_yarn_cost+$total_purchase_cost+$total_knitting_cost+$total_yarn_dyeing_cost+$total_fabric_dyeing_cost+$total_heat_setting_cost+$total_finishing_cost+$total_washing_cost+$all_over_print_cost;
				
				
				//echo $total_purchase_cost;
				//$total_fab_cost_amount2+=$total_fab_cost_amount;
				$total_embelishment_cost+=$print_amount+$embroidery_amount+$special_amount+$other_amount;
				$total_commssion+=$foreign+$local;
				$total_testing_cost+=$tot_test_cost;
				$total_freight_cost+=$freight_cost;
				$total_cm_cost+=$cm_cost;
				$total_cost_up+=$total_cost;
				
				$total_inspection+=$inspection;
				$total_certificate_cost+=$certificate_cost;
				$total_common_oh+=$common_oh;
				$total_currier_cost+=$currier_cost;
				$total_fab_profit+=$total_profit;
				$total_expected_profit+=$expected_profit;
				$total_expt_profit_percentage+=$total_profit_percentage;
				$total_expect_variance+=$expect_variance;
				$total_profit_fab_percentage_up+=$total_profit_percentage2;
				
				//echo $total_fab_cost_amount;
				$i++;
				}
               ?>
                </table>
                <table class="rpt_table" width="4570" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tfoot>
                     <th width="40"></th>
                     <th width="70"></th>
                     <th width="70"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="70"></th>
                     <th width="90" align="right" id="total_order_qnty"><? echo number_format($total_order_qty,2); ?></th>
                     <th width="90"></th>
                     <th width="100" align="right" id="total_order_amount2"><? echo number_format($total_order_amount,2); ?></th>
                     <th width="100"></th>
                     <th width="80" align="right" id="total_yarn_cost2"><? echo number_format($total_yarn_cost,2); ?></th>
                     <th width="80" align="right" ><?  $total_yarn_cost_percentage=$total_yarn_cost/$total_order_amount*100;  echo number_format($total_yarn_cost_percentage,2); ?></th>
                     <th width="100" align="right"><? echo number_format($total_purchase_cost,2); ?></th>
                     <th width="80"></th>
                     <th width="80" align="right"><? echo number_format($total_knitting_cost,2); ?></th>
                     <th width="100"></th>
                     <th width="110" align="right"><? echo number_format($total_yarn_dyeing_cost,2); ?></th>
                     <th width="120"><? ?></th>
                     <th width="100" align="right" id="total_fabric_dyeing_cost4"><? echo number_format($total_fabric_dyeing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_heat_setting_cost,2); ?></th>		
                     <th width="100" align="right"><? echo number_format($total_finishing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_washing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($all_over_print_cost,2); ?></th>
                     <th width="100" align="right"><strong><? echo number_format($total_trim_cost,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_print_amount,2); ?></strong></th>
                     <th width="85" align="right"><strong><? echo number_format($total_embroidery_amount,2); ?></strong></th>
                     <th width="80" align="right"><strong> <? echo number_format($total_special_amount,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_other_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_commercial_cost,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_foreign_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_local_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_test_cost_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_freight_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_inspection_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_certificate_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_common_oh_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_currier_amount,2); ?></strong></th>
                     <th width="120"></th>
                     <th width="100" align="right"><strong><? echo number_format($total_cm_amount,2); ?></strong></th>
                     <th width="100" id="total_cost_up" align="right"><strong><? echo number_format($total_cost_up,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_fab_profit,2);?></strong></th>
                     <th width="100" align="right"><strong><? $total_profit_fab_percentage=$total_fab_profit/$total_order_amount*100; echo number_format($total_profit_fab_percentage,2);?></strong></th>
                     <th width="100"  align="right"><strong><? echo number_format($total_expected_profit,2);?></strong></th>
                     <th width=""  align="right"><strong><? echo number_format($total_expect_variance,2);?></strong></th>
                  </tfoot>
                </table>
                <?
                $fab_percent=($total_fab_cost_amount*100)/$total_order_amount;
				$fab_percent=$fab_percent;
				$trim_percent=($total_trim_cost*100)/$total_order_amount;
				$trim_percent=$trim_percent;
				
				$embelishment_percent=($total_embelishment_cost*100)/$total_order_amount;
				$embelishment_percent=$embelishment_percent;
				$total_commercial_percent=($total_commercial_cost*100)/$total_order_amount;
				$total_commercial_percent=$total_commercial_percent;
				
				$total_commssion_percent=(($total_commssion*100)/$total_order_amount);
				$total_testing_cost_percent=(($total_testing_cost*100)/$total_order_amount);
				$total_freight_cost_percent=(($total_freight_cost*100)/$total_order_amount);
				$total_cost_percent=(($total_cost_up*100)/$total_order_amount);
				$total_cm_percent=(($total_cm_cost*100)/$total_order_amount);
				$total_order_amount_percent=(($total_order_amount*100)/$total_order_amount);
				
				$total_inspection_percent=(($total_inspection*100)/$total_order_amount);
				$total_certificate_percent=(($total_certificate_cost*100)/$total_order_amount);
				$total_common_oh_percent=(($total_common_oh*100)/$total_order_amount);
				$total_currier_cost_percent=(($total_currier_cost*100)/$total_order_amount);
				$all_tot_cost_percentage=$fab_percent+$trim_percent+$embelishment_percent+$total_commercial_percent+$total_commssion_percent+$total_testing_cost_percent+$total_freight_cost_percent+$total_cm_percent+$total_inspection_percent+$total_common_oh_percent+$total_currier_cost_percent;
				
				$total_expected_profit_percent=(($total_expected_profit*100)/$total_order_amount);
				$total_expected_profit_variance_percent=(($total_expect_variance*100)/$total_order_amount);
				
				?>
                  <input type="hidden" id="total_fab_cost" value="<? echo number_format($total_fab_cost_amount,2); ?>">
                  <input type="hidden" id="total_fab_percent" value="<? echo number_format($fab_percent,2)."%"; ?>">
                  <input type="hidden" id="total_trim_cost" value="<? echo number_format($total_trim_cost,2); ?>">
                  <input type="hidden" id="total_trim_percent" value="<? echo number_format($trim_percent,2)."%"; ?>">
                  <input type="hidden" id="total_embelishment_cost" value="<? echo number_format($total_embelishment_cost,2); ?>">
                  <input type="hidden" id="total_embelishment_percent" value="<? echo number_format($embelishment_percent,2)."%"; ?>">
                  <input type="hidden" id="total_commercial_cost" value="<? echo number_format($total_commercial_cost,2); ?>">
                  <input type="hidden" id="total_commercial_percent" value="<? echo number_format($total_commercial_percent,2)."%"; ?>">
                  <input type="hidden" id="total_commssion_cost" value="<? echo number_format($total_commssion,2); ?>">
                  <input type="hidden" id="total_commssion_percent" value="<? echo number_format($total_commssion_percent,2)."%"; ?>">
                  <input type="hidden" id="total_testing_cost" value="<? echo number_format($total_testing_cost,2); ?>">
                  <input type="hidden" id="total_testing_cost_percent" value="<? echo number_format($total_testing_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_freight_cost" value="<? echo number_format($total_freight_cost,2); ?>">
                  <input type="hidden" id="total_freight_cost_percent" value="<? echo number_format($total_freight_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_cost_up2" value="<? echo number_format($total_cost_up,2); ?>">
                  <input type="hidden" id="total_cost_percent" value="<? echo number_format($all_tot_cost_percentage,2)."%"; ?>">
                  <input type="hidden" id="total_cm_cost" value="<? echo number_format($total_cm_cost,2); ?>">
                  <input type="hidden" id="total_cm_percent" value="<? echo number_format($total_cm_percent,2)."%"; ?>">
                  <input type="hidden" id="total_order_amount" value="<? echo number_format($total_order_amount,2); ?>">
                  <input type="hidden" id="total_order_amount_percent" value="<? echo number_format($total_order_amount_percent,2)."%"; ?>">
                  <input type="hidden" id="total_inspection" value="<? echo number_format($total_inspection,2); ?>">
                  <input type="hidden" id="total_inspection_percent" value="<? echo number_format($total_inspection_percent,2)."%"; ?>">
                  <input type="hidden" id="total_certificate_cost" value="<? echo number_format($total_certificate_cost,2); ?>">
                  <input type="hidden" id="total_certificate_percent" value="<? echo number_format($total_certificate_percent,2)."%"; ?>">
                  <input type="hidden" id="total_common_oh" value="<? echo number_format($total_common_oh,2); ?>">
                  <input type="hidden" id="total_common_oh_percent" value="<? echo number_format($total_common_oh_percent,2)."%"; ?>">
                 
                  <input type="hidden" id="total_currier_cost" value="<? echo number_format($total_currier_cost,2); ?>">
                  <input type="hidden" id="total_currier_cost_percent" value="<? echo number_format($total_currier_cost_percent,2)."%"; ?>">
                  
                  <input type="hidden" id="total_fab_profit_id" value="<? echo number_format($total_fab_profit,2); ?>">
                  <input type="hidden" id="total_expected_profit_id" value="<? echo number_format($total_expected_profit,2); ?>">
                  <input type="hidden" id="total_expt_profit_variance_id" value="<? echo number_format($total_expect_variance,2); ?>">
                  
                   <input type="hidden" id="total_profit_fab_percentage_id" value="<? echo number_format($total_profit_fab_percentage,2)."%"; ?>">
                   <input type="hidden" id="total_expt_profit_percentage_id" value="<? echo number_format($total_expected_profit_percent,2)."%"; ?>">
                   <input type="hidden" id="total_expt_profit_variance_percentage_id" value="<? echo number_format($total_expected_profit_variance_percent,2)."%"; ?>">
                   <input type="hidden" id="expected_profit_percent" value="<? echo '('.$company_asking.'%'.')'; ?>">
                  
            </div>
            <table>
                <tr>
                	<?
					$total_fab_cost=number_format($total_fab_cost_amount,2,'.','');
					$total_trim_cost=number_format($total_trim_cost,2,'.','');
					$total_embelishment_cost=number_format($total_embelishment_cost,2,'.','');
					$total_commercial_cost=number_format($total_commercial_cost,2,'.','');
					$total_commssion=number_format($total_commssion,2,'.','');
					$total_testing_cost=number_format($total_testing_cost,2,'.','');
					$total_freight_cost=number_format($total_freight_cost,2,'.','');
					$total_cost_up=number_format($total_cost_up,2,'.','');
					$total_cm_cost=number_format($total_cm_cost,2,'.','');
					$total_order_amount=number_format($total_order_amount,2,'.','');
					$total_inspection=number_format($total_inspection,2,'.','');
					$total_certificate_cost=number_format($total_certificate_cost,2,'.','');
					$total_common_oh=number_format($total_common_oh,2,'.','');
					$total_currier_cost=number_format($total_currier_cost,2,'.','');
					$total_fabric_profit_up=number_format($total_fab_profit,2,'.','');
					$total_expected_profit_up=number_format($total_expected_profit,2,'.','');
					//echo $total_fabric_profit_up;
					$chart_data_qnty="Fabric Cost;".$total_fab_cost."\nTrimCost;".$total_trim_cost."\nEmbelishment Cost;".$total_embelishment_cost."\nCommercial Cost;".$total_commercial_cost."\nCommission Cost;".$total_commssion."\nTesting Cost;".$total_testing_cost."\nFreightCost;".$total_freight_cost."\nCM Cost;".$total_cm_cost."\nInspection Cost;".$total_inspection."\nCertificate Cost;".$total_certificate_cost."\nCommn OH Cost;".$total_common_oh."\nCurrier Cost;".$total_currier_cost."\n Profit/Loss;".$total_fabric_profit_up."\n";
					 
					?>
                    <input type="hidden" id="graph_data" value="<? echo substr($chart_data_qnty,0,-1); ?>"/>
                    
                </tr>
            </table>
             <table>
                <tr><td height="15"></td></tr>
            </table>
           <a id="displayText" href="javascript:toggle();">Show Yarn Summary</a>
            <div style="width:600px; display:none" id="yarn_summary" >
            <div id="data_panel2" align="center" style="width:500px">
                 <input type="button" value="Print Preview" class="formbutton" style="width:100px" name="print" id="print" onClick="new_window(1)" />
            </div>

            <table width="500">
                    <tr class="form_caption">
                        <td colspan="6" align="center"><strong>Yarn Cost Summary </strong></td>
                    </tr>
            </table>
            <table class="rpt_table" width="500" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="30">SL</th>
                    <th width="80">Yarn Count</th>
                    <th width="120">Type</th>
                    <th width="120">Req. Qnty</th>
                    <th width="80">Avg. rate</th>
                    <th>Amount</th>
                </thead>
                <?
                $s=1; $tot_yarn_req_qnty=0; $tot_yarn_req_amnt=0;
                foreach($yarn_desc_array as $key=>$value)
                {
                    if($s%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                    $yarn_desc=explode("**",$key);
                    
                    $tot_yarn_req_qnty+=$yarn_desc_array[$key]['qnty']; 
                    $tot_yarn_req_amnt+=$yarn_desc_array[$key]['amnt'];
                ?>
                    <tr bgcolor="<? echo $bgcolor;?>" onClick="change_color('tr3_<? echo $s; ?>','<? echo $bgcolor; ?>')" id="tr3_<? echo $s;?>">
                        <td><? echo $s; ?></td>
                        <td align="center"><? echo $yarn_desc[0]; ?></td>
                        <td><? echo $yarn_desc[1]; ?></td>
                        <td align="right"><? echo number_format($yarn_desc_array[$key]['qnty'],2); ?></td>
                        <td align="right"><? echo number_format($yarn_desc_array[$key]['amnt']/$yarn_desc_array[$key]['qnty'],2); ?></td>
                        <td align="right"><? echo number_format($yarn_desc_array[$key]['amnt'],2); ?></td>
                    </tr>
                <?	
                $s++;
                }
                ?>
                <tfoot>
                    <th colspan="3" align="right">Total</th>
                    <th align="right"><? echo number_format($tot_yarn_req_qnty,2); ?></th>
                    <th align="right"><? echo number_format($tot_yarn_req_amnt/$tot_yarn_req_qnty,2); ?></th>
                    <th align="right"><? echo number_format($tot_yarn_req_amnt,2); ?></th>
                </tfoot>
        </table> 
        	</div>
		</fieldset>
	</div>
<?
			}
	} //Budget end
	else if($report_type==2)
	{
					if($template==1)
					{
					ob_start();
					$style1="#E9F3FF"; 
					$style="#FFFFFF";
 
 					$fab_precost_arr=array();$p_fab_precost_arr=array();$commission_array=array();$price_commission_array=array();$knit_arr=array();$pq_knit_arr=array(); $fabriccostArray=array(); $price_fabriccostArray=array(); $fab_emb=array();$price_fab_emb=array();$fabric_data_Array=array(); $price_fabric_data_Array=array();$price_costing_perArray=array();$asking_profit_arr=array(); $yarncostArray=array(); $yarn_desc_array=array();
						
					$yarncostDataArray=sql_select("select job_no, count_id, type_id, sum(cons_qnty) as cons_qnty, sum(amount) as amount from wo_pre_cost_fab_yarn_cost_dtls where status_active=1 and is_deleted=0 group by job_no, count_id, type_id");
					foreach($yarncostDataArray as $yarnRow)
					{
					   $yarncostArray[$yarnRow[csf('job_no')]].=$yarnRow[csf('count_id')]."**".$yarnRow[csf('type_id')]."**".$yarnRow[csf('cons_qnty')]."**".$yarnRow[csf('amount')].",";
					}
					$asking_profit=sql_select("select id,company_id,asking_profit,max_profit from lib_standard_cm_entry where status_active=1 and is_deleted=0");
					foreach($asking_profit as $ask_row )
					{
					$asking_profit_arr[$ask_row[csf('company_id')]]['asking_profit']=$ask_row[csf('asking_profit')];
					$asking_profit_arr[$ask_row[csf('company_id')]]['max_profit']=$ask_row[csf('max_profit')];
					} //var_dump($asking_profit_arr);
					$fab_arr=sql_select("select a.job_no,a.pre_cost_fabric_cost_dtls_id, a.po_break_down_id, sum(a.requirment) as requirment ,sum(a.pcs) as pcs from wo_pre_cos_fab_co_avg_con_dtls a,wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.job_no=b.job_no  and b.status_active=1 and b.is_deleted=0 group by a.po_break_down_id,a.pre_cost_fabric_cost_dtls_id,a.job_no");
					foreach($fab_arr as $row_pre)
					{
					$fab_precost_arr[$row_pre[csf('job_no')]][$row_pre[csf('po_break_down_id')]].=$row_pre[csf('requirment')]."**".$row_pre[csf('pcs')].",";	
					}//pre cost purchase cost end
					$pri_fab_arr=sql_select("select a.quotation_id,a.wo_pri_quo_fab_co_dtls_id, sum(a.requirment) as requirment ,sum(a.pcs) as pcs from wo_pri_quo_fab_co_avg_con_dtls a,wo_pri_quo_fabric_cost_dtls  b where a.wo_pri_quo_fab_co_dtls_id=b.id and a.quotation_id=b.quotation_id  and b.status_active=1 and b.is_deleted=0 group by a.quotation_id,a.wo_pri_quo_fab_co_dtls_id");
					foreach($pri_fab_arr as $p_row_pre)
					{
					$p_fab_precost_arr[$p_row_pre[csf('quotation_id')]].=$p_row_pre[csf('requirment')]."**".$p_row_pre[csf('pcs')].",";	
					}
					
					$fabricDataArray=sql_select("select a.job_no, a.fab_nature_id, a.fabric_source, a.rate, b.yarn_cons_qnty, b.yarn_amount from wo_pre_cost_fabric_cost_dtls a, wo_pre_cost_sum_dtls b where a.job_no=b.job_no and a.fabric_source!=3 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
					foreach($fabricDataArray as $fabricRow)
					{
					$fabric_data_Array[$fabricRow[csf('job_no')]].=$fabricRow[csf('fab_nature_id')]."**".$fabricRow[csf('fabric_source')]."**".$fabricRow[csf('rate')]."**".$fabricRow[csf('yarn_cons_qnty')]."**".$fabricRow[csf('yarn_amount')].",";
					} //Pre cost end
					 $price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
				
					$price_fabricDataArray=sql_select("select a.quotation_id, a.fab_nature_id, a.fabric_source, a.rate, b.yarn_cons_qnty, b.yarn_amount from wo_pri_quo_fabric_cost_dtls a, wo_pri_quo_sum_dtls b where a.quotation_id=b.quotation_id and a.fabric_source!=3 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
					foreach($price_fabricDataArray as $price_fabricRow)
					{
					$price_fabric_data_Array[$price_fabricRow[csf('quotation_id')]].=$price_fabricRow[csf('fab_nature_id')]."**".$price_fabricRow[csf('fabric_source')]."**".$price_fabricRow[csf('rate')]."**".$price_fabricRow[csf('yarn_cons_qnty')]."**".$price_fabricRow[csf('yarn_amount')].",";
					} //var_dump($price_fabric_data_Array);
					
					 $data_array_emb=("select  job_no,
					 sum(CASE WHEN emb_name=1 THEN amount END) AS print_amount,
					 sum(CASE WHEN emb_name=2 THEN amount END) AS embroidery_amount,
					 sum(CASE WHEN emb_name=4 THEN amount END) AS special_amount,
					 sum(CASE WHEN emb_name=5 THEN amount END) AS other_amount
					 from  wo_pre_cost_embe_cost_dtls where  status_active=1 and  is_deleted=0 and emb_name!=3 group by job_no");
					 $embl_array=sql_select($data_array_emb);
					foreach($embl_array as $row_emb)
					 {
					 $fab_emb[$row_emb[csf('job_no')]]['print']=$row_emb[csf('print_amount')];
					 $fab_emb[$row_emb[csf('job_no')]]['embroidery']=$row_emb[csf('embroidery_amount')];
					 $fab_emb[$row_emb[csf('job_no')]]['special']=$row_emb[csf('special_amount')];
					 $fab_emb[$row_emb[csf('job_no')]]['other']=$row_emb[csf('other_amount')];
					 }  //Pre Cost embel end
					  $price_data_array_emb=("select  quotation_id,
					 sum(CASE WHEN emb_name=1 THEN amount END) AS print_amount,
					 sum(CASE WHEN emb_name=2 THEN amount END) AS embroidery_amount,
					 sum(CASE WHEN emb_name=4 THEN amount END) AS special_amount,
					 sum(CASE WHEN emb_name=5 THEN amount END) AS other_amount
					 from  wo_pri_quo_embe_cost_dtls where  status_active=1  and is_deleted=0 and emb_name!=3  group by quotation_id");
					 $sql_embl_array=sql_select($price_data_array_emb);
					foreach($sql_embl_array as $p_row_emb)
					 {
					 $price_fab_emb[$p_row_emb[csf('quotation_id')]]['print']=$p_row_emb[csf('print_amount')];
					 $price_fab_emb[$p_row_emb[csf('quotation_id')]]['embroidery']=$p_row_emb[csf('embroidery_amount')];
					 $price_fab_emb[$p_row_emb[csf('quotation_id')]]['special']=$p_row_emb[csf('special_amount')];
					 $price_fab_emb[$p_row_emb[csf('quotation_id')]]['other']=$p_row_emb[csf('other_amount')];
					 } //var_dump($price_fab_emb);
					 $fabriccostDataArray=sql_select("select job_no, costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost,certificate_pre_cost,currier_pre_cost from wo_pre_cost_dtls where status_active=1 and is_deleted=0  ");
					foreach($fabriccostDataArray as $fabRow)
					{
					 $fabriccostArray[$fabRow[csf('job_no')]]['costing_per_id']=$fabRow[csf('costing_per_id')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['trims_cost']=$fabRow[csf('trims_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['embel_cost']=$fabRow[csf('embel_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['cm_cost']=$fabRow[csf('cm_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['commission']=$fabRow[csf('commission')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['common_oh']=$fabRow[csf('common_oh')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['lab_test']=$fabRow[csf('lab_test')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['inspection']=$fabRow[csf('inspection')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['freight']=$fabRow[csf('freight')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['comm_cost']=$fabRow[csf('comm_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['certificate_pre_cost']=$fabRow[csf('certificate_pre_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['currier_pre_cost']=$fabRow[csf('currier_pre_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['c_cost']=$fabRow[csf('cm_cost')];
					} //pre cost trim emb, commission and others end
					
					$p_fabriccostDataArray=sql_select("select quotation_id, costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost,certificate_pre_cost,currier_pre_cost from wo_price_quotation_costing_mst where status_active=1 and is_deleted=0  ");
					foreach($p_fabriccostDataArray as $p_fabRow)
					{
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['costing_per_id']=$p_fabRow[csf('costing_per_id')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['trims_cost']=$p_fabRow[csf('trims_cost')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['embel_cost']=$p_fabRow[csf('embel_cost')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['cm_cost']=$p_fabRow[csf('cm_cost')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['commission']=$p_fabRow[csf('commission')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['common_oh']=$p_fabRow[csf('common_oh')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['lab_test']=$p_fabRow[csf('lab_test')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['inspection']=$p_fabRow[csf('inspection')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['freight']=$p_fabRow[csf('freight')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['comm_cost']=$p_fabRow[csf('comm_cost')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['certificate_pre_cost']=$p_fabRow[csf('certificate_pre_cost')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['currier_pre_cost']=$p_fabRow[csf('currier_pre_cost')];
					 $price_fabriccostArray[$p_fabRow[csf('quotation_id')]]['c_cost']=$p_fabRow[csf('cm_cost')];
					}
					$knit_data=sql_select("select job_no,
					  sum(CASE WHEN cons_process=1 THEN amount END) AS knit_charge,
					  sum(CASE WHEN cons_process=2 THEN amount END) AS weaving_charge,
					  sum(CASE WHEN cons_process=3 THEN amount END) AS knit_charge_collar_cuff,
					  sum(CASE WHEN cons_process=4 THEN amount END) AS knit_charge_feeder_stripe,
					  sum(CASE WHEN cons_process in(64,82,89) THEN amount END) AS washing_cost,
					  sum(CASE WHEN cons_process in(35,36,37) THEN amount END) AS all_over_cost,
					  sum(CASE WHEN cons_process=30 THEN amount END) AS yarn_dyeing_cost,
					  sum(CASE WHEN cons_process=33 THEN amount END) AS heat_setting_cost,
					  sum(CASE WHEN cons_process in(25,31,32,60,61,62,63,72,80,81,84,85,86,87,38,74,78,79) THEN amount END) AS fabric_dyeing_cost,
					  sum(CASE WHEN cons_process in(34,65,66,67,68,69,70,71,73,75,76,77,88,90,91,92,93,100,125,127,128,129) THEN amount END) AS fabric_finish_cost
					  from wo_pre_cost_fab_conv_cost_dtls where  status_active=1 and is_deleted=0 group by job_no");
					foreach($knit_data as $row_knit)
					{
					$knit_arr[$row_knit[csf('job_no')]]['knit']=$row_knit[csf('knit_charge')];
					$knit_arr[$row_knit[csf('job_no')]]['weaving']=$row_knit[csf('weaving_charge')];
					$knit_arr[$row_knit[csf('job_no')]]['collar_cuff']=$row_knit[csf('knit_charge_collar_cuff')];
					$knit_arr[$row_knit[csf('job_no')]]['feeder_stripe']=$row_knit[csf('knit_charge_feeder_stripe')];
					$knit_arr[$row_knit[csf('job_no')]]['washing']=$row_knit[csf('washing_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['all_over']=$row_knit[csf('all_over_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['fabric_dyeing']=$row_knit[csf('fabric_dyeing_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['yarn_dyeing']=$row_knit[csf('yarn_dyeing_cost')];	
					$knit_arr[$row_knit[csf('job_no')]]['heat']=$row_knit[csf('heat_setting_cost')];
					$knit_arr[$row_knit[csf('job_no')]]['fabric_finish']=$row_knit[csf('fabric_finish_cost')];	
					} //Pre cost knit charge end
					$pq_knit_data=sql_select("select quotation_id,
					  sum(CASE WHEN cons_type=1 THEN amount END) AS knit_charge,
					  sum(CASE WHEN cons_type=2 THEN amount END) AS weaving_charge,
					  sum(CASE WHEN cons_type=3 THEN amount END) AS knit_charge_collar_cuff,
					  sum(CASE WHEN cons_type=4 THEN amount END) AS knit_charge_feeder_stripe,
					  sum(CASE WHEN cons_type in(64,82,89) THEN amount END) AS washing_cost,
					  sum(CASE WHEN cons_type in(35,36,37) THEN amount END) AS all_over_cost,
					  sum(CASE WHEN cons_type=30 THEN amount END) AS yarn_dyeing_cost,
					  sum(CASE WHEN cons_type=33 THEN amount END) AS heat_setting_cost,
					  sum(CASE WHEN cons_type in(25,31,32,60,61,62,63,72,80,81,84,85,86,87,38,74,78,79) THEN amount END) AS fabric_dyeing_cost,
					  sum(CASE WHEN cons_type in(34,65,66,67,68,69,70,71,73,75,76,77,88,90,91,92,93,100,125,127,128,129) THEN amount END) AS fabric_finish_cost
					  from wo_pri_quo_fab_conv_cost_dtls where  status_active=1  and is_deleted=0 group by quotation_id");
					foreach($pq_knit_data as $p_row_knit)
					{
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['knit']=$p_row_knit[csf('knit_charge')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['weaving']=$p_row_knit[csf('weaving_charge')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['collar_cuff']=$p_row_knit[csf('knit_charge_collar_cuff')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['feeder_stripe']=$p_row_knit[csf('knit_charge_feeder_stripe')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['washing']=$p_row_knit[csf('washing_cost')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['all_over']=$p_row_knit[csf('all_over_cost')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['fabric_dyeing']=$p_row_knit[csf('fabric_dyeing_cost')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['yarn_dyeing']=$p_row_knit[csf('yarn_dyeing_cost')];	
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['heat']=$p_row_knit[csf('heat_setting_cost')];
					$pq_knit_arr[$p_row_knit[csf('quotation_id')]]['fabric_finish']=$p_row_knit[csf('fabric_finish_cost')];	
					} 
					$data_array=sql_select("select  job_no,
					 sum(CASE WHEN particulars_id=1 THEN commission_amount END) AS foreign_comm,
					 sum(CASE WHEN particulars_id=2 THEN commission_amount END) AS local_comm
					 from  wo_pre_cost_commiss_cost_dtls where status_active=1 and is_deleted=0 group by job_no");// quotation_id='$data'
					 foreach($data_array as $row_fl )
					{
						$commission_array[$row_fl[csf('job_no')]]['foreign']=$row_fl[csf('foreign_comm')];
						$commission_array[$row_fl[csf('job_no')]]['local']=$row_fl[csf('local_comm')];
					} //Pre Cost Commission end
					$p_data_array=sql_select("select  quotation_id,
					 sum(CASE WHEN particulars_id=1 THEN commission_amount END) AS foreign_comm,
					 sum(CASE WHEN particulars_id=2 THEN commission_amount END) AS local_comm
					 from  wo_pri_quo_commiss_cost_dtls where status_active=1 and  is_deleted=0 group by quotation_id");// quotation_id='$data'
					 foreach($p_data_array as $p_row_fl )
					{
						$price_commission_array[$p_row_fl[csf('quotation_id')]]['foreign']=$p_row_fl[csf('foreign_comm')];
						$price_commission_array[$p_row_fl[csf('quotation_id')]]['local']=$p_row_fl[csf('local_comm')];
					} 
					?>
	<script>
		var total_fab_cost=document.getElementById('total_fab_cost').value;
		var total_fab_percent=document.getElementById('total_fab_percent').value;
		document.getElementById('fab_cost').innerHTML=total_fab_cost;
		document.getElementById('fab_percent').innerHTML=total_fab_percent;
		
		var total_trim_cost=document.getElementById('total_trim_cost').value;
		var total_trim_percent=document.getElementById('total_trim_percent').value;
		document.getElementById('trim_cost_id').innerHTML=total_trim_cost;
		document.getElementById('trim_percent').innerHTML=total_trim_percent;
		
		var total_embelishment_cost=document.getElementById('total_embelishment_cost').value;
		var total_embelishment_percent=document.getElementById('total_embelishment_percent').value;
		document.getElementById('embelishment_id').innerHTML=total_embelishment_cost;
		document.getElementById('embelishment_percent').innerHTML=total_embelishment_percent;
		
		var total_commercial_cost=document.getElementById('total_commercial_cost').value;
		var total_commercial_percent=document.getElementById('total_commercial_percent').value;
		document.getElementById('commercial_id').innerHTML=total_commercial_cost;
		document.getElementById('commercial_percent').innerHTML=total_commercial_percent;
		
		var total_commssion_cost=document.getElementById('total_commssion_cost').value;
		var total_commssion_percent=document.getElementById('total_commssion_percent').value;
		document.getElementById('commission_id').innerHTML=total_commssion_cost;
		document.getElementById('commission_percent').innerHTML=total_commssion_percent;
		
		var total_testing_cost=document.getElementById('total_testing_cost').value;
		var total_testing_cost_percent=document.getElementById('total_testing_cost_percent').value;
		document.getElementById('testing_id').innerHTML=total_testing_cost;
		document.getElementById('testing_percent').innerHTML=total_testing_cost_percent;
		var total_freight_cost=document.getElementById('total_freight_cost').value;
		var total_freight_cost_percent=document.getElementById('total_freight_cost_percent').value;
		document.getElementById('freight_id').innerHTML=total_freight_cost;
		document.getElementById('freight_percent').innerHTML=total_freight_cost_percent;
		var total_cost_up=document.getElementById('total_cost_up2').value;
		
		document.getElementById('cost_id').innerHTML=total_cost_up;
		
		var total_cm_cost=document.getElementById('total_cm_cost').value;
		var total_cm_percent=document.getElementById('total_cm_percent').value;
		document.getElementById('cm_id').innerHTML=total_cm_cost;
		document.getElementById('cm_percent').innerHTML=total_cm_percent;
		var total_order_amount=document.getElementById('total_order_amount').value;
		var total_order_amount_percent=document.getElementById('total_order_amount_percent').value;
		document.getElementById('order_id').innerHTML=total_order_amount;
		document.getElementById('order_percent').innerHTML=total_order_amount_percent;
		var total_inspection=document.getElementById('total_inspection').value;
		var total_inspection_percent=document.getElementById('total_inspection_percent').value;
		document.getElementById('inspection_id').innerHTML=total_inspection;
		document.getElementById('inspection_percent').innerHTML=total_inspection_percent;
		var total_certificate_cost=document.getElementById('total_certificate_cost').value;
		var total_certificate_percent=document.getElementById('total_certificate_percent').value;
		document.getElementById('certificate_id').innerHTML=total_certificate_cost;
		document.getElementById('certificate_percent').innerHTML=total_certificate_percent;
		var total_common_oh=document.getElementById('total_common_oh').value;
		var total_common_oh_percent=document.getElementById('total_common_oh_percent').value;
		document.getElementById('commn_id').innerHTML=total_common_oh;
		document.getElementById('commn_percent').innerHTML=total_common_oh_percent;
		
		var total_common_oh=document.getElementById('total_common_oh').value;
		var total_common_oh_percent=document.getElementById('total_common_oh_percent').value;
		document.getElementById('commn_id').innerHTML=total_common_oh;
		document.getElementById('commn_percent').innerHTML=total_common_oh_percent;
		var total_currier_cost=document.getElementById('total_currier_cost').value;
		var total_currier_cost_percent=document.getElementById('total_currier_cost_percent').value;
		document.getElementById('courier_id').innerHTML=total_currier_cost;
		document.getElementById('courier_percent').innerHTML=total_currier_cost_percent;
		
		var total_fab_profit_id=document.getElementById('total_fab_profit_id').value;
		var total_expected_profit_id=document.getElementById('total_expected_profit_id').value;
		document.getElementById('fab_profit_id').innerHTML=total_fab_profit_id;
		document.getElementById('expected_id').innerHTML=total_expected_profit_id;
		var total_expt_profit_variance=document.getElementById('total_expt_profit_variance_id').value;
		
		document.getElementById('expt_p_variance_id').innerHTML=total_expt_profit_variance;
		var total_cost_percent=document.getElementById('total_cost_percent').value;
		document.getElementById('cost_percent').innerHTML=total_cost_percent;
		var total_profit_fab_percentage=document.getElementById('total_profit_fab_percentage_id').value;
		document.getElementById('profit_fab_percentage').innerHTML=total_profit_fab_percentage;
		var total_expt_profit_percentage=document.getElementById('total_expt_profit_percentage_id').value;
		document.getElementById('profit_expt_fab_percentage').innerHTML=total_expt_profit_percentage;
		var total_expt_profit_percentage=document.getElementById('total_expt_profit_variance_percentage_id').value;
		document.getElementById('expt_p_percent').innerHTML=total_expt_profit_percentage;
		var expected_profit_percent=document.getElementById('expected_profit_percent').value;
		document.getElementById('expt_percent').innerHTML=expected_profit_percent;
		var total_p_fab_cost=document.getElementById('total_p_fab_cost').value;
		var total_p_fab_percent=document.getElementById('total_p_fab_percent').value;
		document.getElementById('p_fab_cost').innerHTML=total_p_fab_cost;
		document.getElementById('p_fab_percent').innerHTML=total_p_fab_percent;
		
		var total_p_trim_cost=document.getElementById('total_p_trim_cost').value;
		var total_p_trim_percent=document.getElementById('total_p_trim_percent').value;
		document.getElementById('p_trim_cost_id').innerHTML=total_p_trim_cost;
		document.getElementById('p_trim_percent').innerHTML=total_p_trim_percent;
		
		var total_p_embelishment_cost=document.getElementById('total_p_embelishment_cost').value;
		var total_p_embelishment_percent=document.getElementById('total_p_embelishment_percent').value;
		document.getElementById('p_embelishment_id').innerHTML=total_p_embelishment_cost;
		document.getElementById('p_embelishment_percent').innerHTML=total_p_embelishment_percent;
		
		var total_p_commercial_cost=document.getElementById('total_p_commercial_cost').value;
		var total_p_commercial_percent=document.getElementById('total_p_commercial_percent').value;
		document.getElementById('p_commercial_id').innerHTML=total_p_commercial_cost;
		document.getElementById('p_commercial_percent').innerHTML=total_p_commercial_percent;
		
		var total_p_commssion_cost=document.getElementById('total_p_commssion_cost').value;
		var total_p_commssion_percent=document.getElementById('total_p_commssion_percent').value;
		document.getElementById('p_commission_id').innerHTML=total_p_commssion_cost;
		document.getElementById('p_commission_percent').innerHTML=total_p_commssion_percent;
		
		var total_p_testing_cost=document.getElementById('total_p_testing_cost').value;
		var total_p_testing_cost_percent=document.getElementById('total_p_testing_cost_percent').value;
		document.getElementById('p_testing_id').innerHTML=total_p_testing_cost;
		document.getElementById('p_testing_percent').innerHTML=total_p_testing_cost_percent;
		
		var total_p_freight_cost=document.getElementById('total_p_freight_cost').value;
		var total_p_freight_cost_percent=document.getElementById('total_p_freight_cost_percent').value;
		document.getElementById('p_freight_id').innerHTML=total_p_freight_cost;
		document.getElementById('p_freight_percent').innerHTML=total_p_freight_cost_percent;
		var total_p_cost_up=document.getElementById('total_p_cost_up2').value;
		document.getElementById('p_cost_id').innerHTML=total_p_cost_up;
		
		var total_p_cm_cost=document.getElementById('total_p_cm_cost').value;
		var total_p_cm_percent=document.getElementById('total_p_cm_percent').value;
		document.getElementById('p_cm_id').innerHTML=total_p_cm_cost;
		document.getElementById('p_cm_percent').innerHTML=total_p_cm_percent;
		var total_p_order_amount=document.getElementById('total_p_order_amount').value;
		var total_p_order_amount_percent=document.getElementById('total_p_order_amount_percent').value;
		document.getElementById('p_order_id').innerHTML=total_p_order_amount;
		document.getElementById('p_order_percent').innerHTML=total_p_order_amount_percent;
		var total_p_inspection=document.getElementById('total_p_inspection').value;
		var total_p_inspection_percent=document.getElementById('total_p_inspection_percent').value;
		document.getElementById('p_inspection_id').innerHTML=total_p_inspection;
		document.getElementById('p_inspection_percent').innerHTML=total_p_inspection_percent;
		var total_p_certificate_cost=document.getElementById('total_p_certificate_cost').value;
		var total_p_certificate_percent=document.getElementById('total_p_certificate_percent').value;
		document.getElementById('p_certificate_id').innerHTML=total_p_certificate_cost;
		document.getElementById('p_certificate_percent').innerHTML=total_p_certificate_percent;
		var total_p_common_oh=document.getElementById('total_p_common_oh').value;
		var total_p_common_oh_percent=document.getElementById('total_p_common_oh_percent').value;
		document.getElementById('p_commn_id').innerHTML=total_p_common_oh;
		document.getElementById('p_commn_percent').innerHTML=total_p_common_oh_percent;
		
		var total_p_common_oh=document.getElementById('total_p_common_oh').value;
		var total_common_p_oh_percent=document.getElementById('total_p_common_oh_percent').value;
		document.getElementById('p_commn_id').innerHTML=total_p_common_oh;
		document.getElementById('p_commn_percent').innerHTML=total_p_common_oh_percent;
		var total_p_currier_cost=document.getElementById('total_p_currier_cost').value;
		var total_p_currier_cost_percent=document.getElementById('total_p_currier_cost_percent').value;
		document.getElementById('p_courier_id').innerHTML=total_p_currier_cost;
		document.getElementById('p_courier_percent').innerHTML=total_p_currier_cost_percent;
		
		var total_p_fab_profit_id=document.getElementById('total_p_fab_profit_id').value;
		var total_p_expected_profit_id=document.getElementById('total_p_expected_profit_id').value;
		document.getElementById('p_fab_profit_id').innerHTML=total_p_fab_profit_id;
		document.getElementById('p_expected_id').innerHTML=total_p_expected_profit_id;
		
		var total_p_expt_profit_variance=document.getElementById('total_p_expt_profit_variance_id').value;
		
		document.getElementById('p_expt_variance_id').innerHTML=total_p_expt_profit_variance;
		
		var total_p_cost_percent=document.getElementById('total_p_cost_percent').value;
		document.getElementById('p_cost_percent').innerHTML=total_p_cost_percent;
		var total_p_profit_fab_percentage=document.getElementById('total_p_profit_fab_percentage_id').value;
		document.getElementById('p_profit_fab_percentage').innerHTML=total_p_profit_fab_percentage;
		var total_p_expt_profit_percentage=document.getElementById('total_p_expt_profit_percentage_id').value;
		document.getElementById('p_profit_expt_fab_percentage').innerHTML=total_p_expt_profit_percentage;
		var total_p_expt_profit_percentage=document.getElementById('total_p_expt_profit_variance_percentage_id').value;
		document.getElementById('p_expt_percent').innerHTML=total_p_expt_profit_percentage;
		var p_expected_profit_percent=document.getElementById('p_expected_profit_percent').value;
		document.getElementById('p_expt_percent').innerHTML=p_expected_profit_percent;
		
		var total_v_fab_cost=document.getElementById('total_v_fab_cost').value;
		var total_v_fab_percent=document.getElementById('total_v_fab_percent').value;
		document.getElementById('v_fab_cost').innerHTML=total_v_fab_cost;
		document.getElementById('v_fab_percent').innerHTML=total_v_fab_percent;
		
		var total_v_trim_cost=document.getElementById('total_v_trim_cost').value;
		var total_v_trim_percent=document.getElementById('total_v_trim_percent').value;
		document.getElementById('v_trim_cost_id').innerHTML=total_v_trim_cost;
		document.getElementById('v_trim_percent').innerHTML=total_v_trim_percent;
		var total_v_embelishment_cost=document.getElementById('total_v_embelishment_cost').value;
		var total_v_embelishment_percent=document.getElementById('total_v_embelishment_percent').value;
		document.getElementById('v_embelishment_id').innerHTML=total_v_embelishment_cost;
		document.getElementById('v_embelishment_percent').innerHTML=total_v_embelishment_percent;
		
		var total_v_commercial_cost=document.getElementById('total_v_commercial_cost').value;
		var total_v_commercial_percent=document.getElementById('total_v_commercial_percent').value;
		document.getElementById('v_commercial_id').innerHTML=total_v_commercial_cost;
		document.getElementById('v_commercial_percent').innerHTML=total_v_commercial_percent;
		
		var total_v_commssion_cost=document.getElementById('total_v_commssion_cost').value;
		var total_v_commssion_percent=document.getElementById('total_v_commssion_percent').value;
		document.getElementById('v_commission_id').innerHTML=total_v_commssion_cost;
		document.getElementById('v_commission_percent').innerHTML=total_v_commssion_percent;
		
		var total_v_testing_cost=document.getElementById('total_v_testing_cost').value;
		var total_v_testing_cost_percent=document.getElementById('total_v_testing_cost_percent').value;
		document.getElementById('v_testing_id').innerHTML=total_v_testing_cost;
		document.getElementById('v_testing_percent').innerHTML=total_v_testing_cost_percent;
		
		var total_v_freight_cost=document.getElementById('total_v_freight_cost').value;
		var total_v_freight_cost_percent=document.getElementById('total_v_freight_cost_percent').value;
		document.getElementById('v_freight_id').innerHTML=total_v_freight_cost;
		document.getElementById('v_freight_percent').innerHTML=total_v_freight_cost_percent;
		var total_v_cost_up=document.getElementById('total_v_cost_up2').value;
		
		document.getElementById('v_cost_id').innerHTML=total_v_cost_up;
		
		
		var total_v_cm_cost=document.getElementById('total_v_cm_cost').value;
		var total_v_cm_percent=document.getElementById('total_v_cm_percent').value;
		document.getElementById('v_cm_id').innerHTML=total_v_cm_cost;
		document.getElementById('v_cm_percent').innerHTML=total_v_cm_percent;
		var total_v_order_amount=document.getElementById('total_v_order_amount').value;
		var total_v_order_amount_percent=document.getElementById('total_v_order_amount_percent').value;
		document.getElementById('v_order_id').innerHTML=total_v_order_amount;
		
		//error start
		
		
		
		
		document.getElementById('v_order_percent').innerHTML=total_v_order_amount_percent;
		var total_v_inspection=document.getElementById('total_v_inspection').value;
		var total_v_inspection_percent=document.getElementById('total_v_inspection_percent').value;
		document.getElementById('v_inspection_id').innerHTML=total_v_inspection;
		document.getElementById('v_inspection_percent').innerHTML=total_v_inspection_percent;
		var total_v_certificate_cost=document.getElementById('total_v_certificate_cost').value;
		var total_v_certificate_percent=document.getElementById('total_v_certificate_percent').value;
		document.getElementById('v_certificate_id').innerHTML=total_v_certificate_cost;
		document.getElementById('v_certificate_percent').innerHTML=total_v_certificate_percent;
		var total_v_common_oh=document.getElementById('total_v_common_oh').value;
		var total_v_common_oh_percent=document.getElementById('total_v_common_oh_percent').value;
		document.getElementById('v_commn_id').innerHTML=total_v_common_oh;
		document.getElementById('v_commn_percent').innerHTML=total_v_common_oh_percent;
		
		var total_v_common_oh=document.getElementById('total_v_common_oh').value;
		var total_common_v_oh_percent=document.getElementById('total_v_common_oh_percent').value;
		document.getElementById('v_commn_id').innerHTML=total_v_common_oh;
		document.getElementById('v_commn_percent').innerHTML=total_v_common_oh_percent;
		var total_v_currier_cost=document.getElementById('total_v_currier_cost').value;
		var total_v_currier_cost_percent=document.getElementById('total_v_currier_cost_percent').value;
		document.getElementById('v_courier_id').innerHTML=total_v_currier_cost;
		document.getElementById('v_courier_percent').innerHTML=total_v_currier_cost_percent;
		
		var total_v_fab_profit_id=document.getElementById('total_v_fab_profit_id').value;
		var total_v_expected_profit_id=document.getElementById('total_v_expected_profit_id').value;
		document.getElementById('v_fab_profit_id').innerHTML=total_v_fab_profit_id;
		document.getElementById('v_expected_id').innerHTML=total_v_expected_profit_id;
		
		var total_v_expt_profit_variance=document.getElementById('total_v_expt_profit_variance_id').value;
		
		document.getElementById('v_expt_variance_id').innerHTML=total_v_expt_profit_variance;
		
		var total_v_cost_percent=document.getElementById('total_v_cost_percent').value;
		document.getElementById('v_cost_percent').innerHTML=total_v_cost_percent;
		var total_v_profit_fab_percentage=document.getElementById('total_v_profit_fab_percentage_id').value;
		document.getElementById('v_profit_fab_percentage').innerHTML=total_v_profit_fab_percentage;
		var total_v_expt_profit_percentage=document.getElementById('total_v_expt_profit_percentage_id').value;
		document.getElementById('v_profit_expt_fab_percentage').innerHTML=total_v_expt_profit_percentage;
		var total_v_expt_profit_percentage=document.getElementById('total_v_expt_profit_variance_percentage_id').value;
		document.getElementById('v_expt_percent').innerHTML=total_v_expt_profit_percentage;
		var v_expected_profit_percent=document.getElementById('v_expected_profit_percent').value;
		document.getElementById('v_expt_percent').innerHTML=v_expected_profit_percent;
	
		
		function toggle() 
		{
			var ele = document.getElementById("yarn_summary");
			
			var text = document.getElementById("displayText");
			if(ele.style.display!= "none") 
			{
				ele.style.display = "none";
				text.innerHTML = "Show Yarn Summary";
			}
			else 
			{
				ele.style.display = "block";
				text.innerHTML = "Hide Yarn Summary";
			}
		} 
    </script>
        <div style="width:4570px;">
        <div style="width:900px;" align="left">
        	<table width="900" cellpadding="0" cellspacing="2" border="0">
            	
                <tr>
                	<td width="600" align="left">
                    	<table width="500" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="2">
                        <caption><strong>Price Quotation Vs Budget Variance</strong></caption>
                        <thead align="center">
                        <th>SL</th><th>Particulars</th><th>Quoted Amount</th><th>% On Order Value</th><th>Budget Amount</th><th>% On Order Value</th><th>Variance</th><th>% On Order Value</th>
                        </thead>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">1</td>
                        <td width="100">Fabric Cost</td>
                        <td width="120" align="right" id="p_fab_cost"></td>
                        <td width="80" align="right" id="p_fab_percent"></td>
                       
                        <td width="120" align="right" id="fab_cost"></td>
                        <td width="80" align="right" id="fab_percent"></td>
                        <td width="100" align="right" id="v_fab_cost"></td>
                        <td width="80" align="right" id="v_fab_percent"></td>
                        
                        </tr>
                        <tr bgcolor="<?  echo $style; ?>">
                        <td width="20">2</td>
                        <td width="100">Trims Cost</td>
                        <td align="right" id="p_trim_cost_id"></td>
                        <td align="right" id="p_trim_percent"> </td>
                      
                        <td align="right" id="trim_cost_id"></td>
                        <td align="right" id="trim_percent"> </td>
                      
                        <td align="right" id="v_trim_cost_id"></td>
                        <td align="right" id="v_trim_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">3</td>
                        <td width="100">Embellish Cost</td>
                        <td align="right" id="p_embelishment_id"></td>
                        <td align="right" id="p_embelishment_percent"> </td>
                       
                        <td align="right" id="embelishment_id"></td>
                        <td align="right" id="embelishment_percent"> </td>
                      
                        <td align="right" id="v_embelishment_id"></td>
                        <td align="right" id="v_embelishment_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">4</td>
                        <td width="100">Commercial Cost</td>
                        <td align="right" id="p_commercial_id"></td>
                        <td align="right" id="p_commercial_percent"> </td>
                        
                         <td align="right" id="commercial_id"></td>
                        <td align="right" id="commercial_percent"> </td>
                        
                         <td align="right" id="v_commercial_id"></td>
                        <td align="right" id="v_commercial_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">5</td>
                        <td width="100">Commision Cost</td>
                        <td align="right" id="p_commission_id"></td>
                        <td align="right" id="p_commission_percent"> </td>
                        
                        
                         <td align="right" id="commission_id"></td>
                        <td align="right" id="commission_percent"> </td>
                         
                         <td align="right" id="v_commission_id"></td>
                        <td align="right" id="v_commission_percent"> </td>
                        </tr>
                         <tr bgcolor="<? echo $style; ?>">
                        <td width="20">6</td>
                        <td width="100">Testing Cost</td>
                        <td align="right" id="p_testing_id"></td>
                        <td align="right" id="p_testing_percent"> </td>
                        
                         <td align="right" id="testing_id"></td>
                        <td align="right" id="testing_percent"> </td>
                   
                         <td align="right" id="v_testing_id"></td>
                        <td align="right" id="v_testing_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">7</td>
                        <td width="100">Freight Cost</td>
                        <td align="right" id="p_freight_id"></td>
                        <td align="right" id="p_freight_percent"> </td>
                        
                         <td align="right" id="freight_id"></td>
                        <td align="right" id="freight_percent"> </td>
                         
                        <td align="right" id="v_freight_id"></td>
                        <td align="right" id="v_freight_percent"> </td>
                        </tr>
                        
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">8</td>
                        <td width="100">Inspection Cost</td>
                        <td align="right" id="p_inspection_id"></td>
                        <td align="right" id="p_inspection_percent"> </td>
                        
                        <td align="right" id="inspection_id"></td>
                        <td align="right" id="inspection_percent"> </td>
                        
                        <td align="right" id="v_inspection_id"></td>
                        <td align="right" id="v_inspection_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">9</td>
                        <td width="100">Certificate Cost</td>
                        <td align="right" id="p_certificate_id"></td>
                        <td align="right" id="p_certificate_percent"> </td>
                        
                        <td align="right" id="certificate_id"></td>
                        <td align="right" id="certificate_percent"> </td>
                        
                       
                        <td align="right" id="v_certificate_id"></td>
                        <td align="right" id="v_certificate_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">10</td>
                        <td width="100">Commn OH</td>
                        <td align="right" id="p_commn_id"></td>
                        <td align="right" id="p_commn_percent"> </td>
                         
                        <td align="right" id="commn_id"></td>
                        <td align="right" id="commn_percent"> </td>
                       
                        <td align="right" id="v_commn_id"></td>
                        <td align="right" id="v_commn_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">11</td>
                        <td width="100">Courier Cost</td>
                        <td align="right" id="p_courier_id"></td>
                        <td align="right" id="p_courier_percent"> </td>
                        
                        <td align="right" id="courier_id"></td>
                        <td align="right" id="courier_percent"> </td>
                  
                  
                        <td align="right" id="v_courier_id"></td>
                        <td align="right" id="v_courier_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">12</td>
                        <td width="100">CM Cost</td>
                        <td align="right" id="p_cm_id"></td>
                        <td align="right" id="p_cm_percent"> </td>
                       
                        <td align="right" id="cm_id"></td>
                        <td align="right" id="cm_percent"> </td>
                     
                        <td align="right" id="v_cm_id"></td>
                        <td align="right" id="v_cm_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style; ?>">
                        <td width="20">13</td>
                        <td width="100">Total Cost</td><td align="right" id="p_cost_id"></td>
                        <td align="right" id="p_cost_percent"> </td>
                        
                        <td align="right" id="cost_id"></td>
                        <td align="right" id="cost_percent"> </td>
                        
                         <td align="right" id="v_cost_id"></td>
                        <td align="right" id="v_cost_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">14</td>
                        <td width="100">Total Order Value</td><td align="right" id="p_order_id"></td>
                        <td align="right" id="p_order_percent"> </td>
                         
                        <td align="right" id="order_id"></td>
                        <td align="right" id="order_percent"> </td>
                          
                        <td align="right" id="v_order_id"></td>
                        <td align="right" id="v_order_percent"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">15</td>
                        <td width="100">Profit/Loss </td>
                        <td align="right" id="p_fab_profit_id"></td>
                        <td align="right" id="p_profit_fab_percentage"> </td>
                      
                        <td align="right" id="fab_profit_id"></td>
                        <td align="right" id="profit_fab_percentage"> </td>
                        
                        <td align="right" id="v_fab_profit_id"></td>
                        <td align="right" id="v_profit_fab_percentage"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">16</td>
                        <td width="100">Expected Profit <div id="expt_percent"></div> </td>
                        <td align="right" id="p_expected_id"></td>
                        <td align="right" id="p_profit_expt_fab_percentage"> </td>
                       
                         <td align="right" id="expected_id"></td>
                        <td align="right" id="profit_expt_fab_percentage"> </td>
                         
                         <td align="right" id="v_expected_id"></td>
                        <td align="right" id="v_profit_expt_fab_percentage"> </td>
                        </tr>
                        <tr bgcolor="<? echo $style1; ?>">
                        <td width="20">17</td>
                        <td width="100">Expt.Profit Variance </td>
                        <td align="right" id="p_expt_variance_id"></td>
                        <td align="right" id="p_expt_percent"> </td>
                       
                         <td align="right" id="expt_p_variance_id"></td>
                        <td align="right" id="expt_p_percent"> </td>
                        
                         <td align="right" id="v_expt_variance_id"></td>
                        <td align="right" id="v_expt_percent"> </td>
                        </tr>
                       
                        </table>
                    </td>
                    <td colspan="5" style="min-height:800px; max-height:100%" align="center" valign="top">
                 
                   </td>
                  
                  </tr>
           </table>
           </div>
           <br/>   
         <h3 align="left" id="accordion_h2" style="width:4710px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel2', '')"> -Search Panel</h3>
        <fieldset style="width:100%;" id="content_search_panel2">	
            <table width="4710">
                    <tr class="form_caption">
                     <td  align="left">Decimal rounded to 2 digit.</td>
                      <td colspan="45" align="center"><strong>Order Wise Budget Report</strong></td>
                    </tr>
                    <tr class="form_caption">
                   
                    <td colspan="46" align="center"><strong><? echo $company_library[$company_name]; ?></strong></td>
                    </tr>
                    <tr class="form_caption">
                        <td align="left"><strong>Details Report </strong></td>
                    </tr>
            </table>
            <? $asking_profit_head=$asking_profit_arr[$company_name]['asking_profit']; ?>
            <table id="table_header_1" class="rpt_table" width="4670" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                <tr>
                    <th width="40" rowspan="2">SL</th>
                   	<th width="70" rowspan="2">Buyer</th>
                    <th width="70" rowspan="2">Job No</th>
                    <th width="100" rowspan="2">Order No</th>
                    <th width="100" rowspan="2">Order Status</th>
                    <th width="110" rowspan="2">Style</th>
                    <th width="110" rowspan="2">Item Name</th>
                    <th width="110" rowspan="2">Dealing</th>
                    <?
                     if(str_replace("'","",$cbo_search_date)==1)
					{ ?>
						<th width="70" rowspan="2">Ship. Date</th>
					<? }
					else
					{ ?>
						<th width="70" rowspan="2">PO Recv. Date</th>
					<? }
					?>
                    <th width="90" rowspan="2">Order Qty</th>
                    <th width="90" rowspan="2">Avg Unit Price</th>
                    <th width="100" rowspan="2">Order Value</th>
                     <th width="100" rowspan="2">Particulars</th>
                    <th colspan="14">Fabric Cost</th>
                    <th width="100" rowspan="2">Trim Cost</th>
                    <th colspan="4">Embell. Cost</th>
                    <th width="120" rowspan="2">Commercial Cost</th>
                    <th colspan="2">Commission</th>
                    <th width="100" rowspan="2">Testing Cost</th>
                    <th width="100" rowspan="2">Freight Cost</th>
                    <th width="120" rowspan="2">Inspection Cost</th>
                    <th width="100" rowspan="2">Certificate Cost</th>
                    <th width="100" rowspan="2">Commn OH</th>
                    <th width="100" rowspan="2">Courier Cost</th>
                    <th width="120" rowspan="2">CM/DZN</th>
                    <th width="100" rowspan="2">CM Cost</th>
                    <th width="100" rowspan="2">Total Cost</th>
                    <th width="100" rowspan="2">Profit/Loss</th>
                    <th width="100" rowspan="2">Profit/Loss %</th>
                    <th width="100" rowspan="2">Expected Profit(<? echo $asking_profit_head.'%';?>)</th>
                    <th width="" rowspan="2">Expt.Profit Variance</th>
                    </tr>
                    <tr>
                    <th width="100">Avg Yarn Rate</th>
                    <th width="80">Yarn Cost</th>
                    <th width="80">Yarn Cost %</th>
                    <th width="100">Fabric Purchase</th>
                    <th width="80">Knit/ Weav Cost/Dzn</th>
                    <th width="80">Knitting/ Weav Cost</th>
                    <th width="100">Yarn Dye Cost/Dzn </th>
                    <th width="110">Yarn Dyeing Cost </th>
                    <th width="120">Fab.Dye Cost/Dzn</th>
                    <th width="100">Fabric Dyeing Cost</th>
                    <th width="90">Heat Setting</th>
                    <th width="100">Finishing Cost</th>
                    <th width="90">Washing Cost</th>
                    <th width="90">All Over Print</th>
                    <th width="80">Printing</th>
                    <th width="85">Embroidery</th>
                    <th width="80">Washing</th>
                    <th width="80">Other</th>
                    <th width="120">Foreign</th>
                    <th width="120">Local</th>
                   </tr>
                </thead>
            </table>
            <div style="width:4710px; max-height:400px; overflow-y:scroll" id="scroll_body">
             <table class="rpt_table" width="4670" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
                <? 
					
                $i=1; $total_order_qty=0;  $total_yarn_dyeing_cost=0; $total_yarn_cost=0; $total_order_value=0;$total_purchase_cost=0; $grand_tot_trims_cost=0; $total_fabric_dyeing_cost=0; $total_knitting_cost=0; $total_heat_setting_cost=0;$total_finishing_cost=0; $total_washing_cost=0; $fabric_dyeing_cost_dzn=0; $other_cost=0;
$all_over_print_cost=0;$total_trim_cost=0;$total_commercial_cost=0;
                
                $sql="select a.job_no_prefix_num, a.job_no,a.company_name,a.buyer_name,a.style_ref_no,b.is_confirmed,a.quotation_id,a.agent_name,a.avg_unit_price,a.dealing_marchant, a.gmts_item_id,a.total_set_qnty as ratio,b.plan_cut,b.id as po_id, b.po_number, b.pub_shipment_date,b.po_received_date, b.po_quantity, b.unit_price from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_name' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $date_cond $buyer_id_cond  $year_cond $job_no_cond $order_id_cond_trans $order_no_cond $order_status_cond group by b.id,a.job_no_prefix_num, a.job_no,a.company_name,a.buyer_name,b.is_confirmed,a.style_ref_no,a.agent_name,a.avg_unit_price,a.dealing_marchant, a.gmts_item_id, b.po_number, b.pub_shipment_date,b.po_received_date, b.po_quantity, b.unit_price,a.quotation_id,a.total_set_qnty,b.plan_cut order by  b.pub_shipment_date,b.id ";
				//echo $sql;
				$result=sql_select($sql);
				 $tot_rows=count($result);
				 foreach($result as $row )
                {
                    if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$total_order_value=$row[csf('po_quantity')]*$row[csf('avg_unit_price')];
					
					if(str_replace("'","",$cbo_search_date)==1)
					{
						$ship_po_recv_date=change_date_format($row[csf('pub_shipment_date')]);
					}
					else if(str_replace("'","",$cbo_search_date)==2)
					{
						$ship_po_recv_date=change_date_format($row[csf('po_received_date')]);
					}
					
				?>
				 <tr bgcolor="<? echo $bgcolor;?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
                
                     <td width="40" rowspan="4"><? echo $i; ?></td>
                     <td  width="70" rowspan="4" title="<? echo $buyer_library[$row[csf('buyer_name')]] ?>"><p><? echo $buyer_library[$row[csf('buyer_name')]] ?></p></td>
                     <td  width="70" rowspan="4" title="<? echo $row[csf('job_no_prefix_num')];  ?>"><p><? echo $row[csf('job_no_prefix_num')];  ?></p></td>
                     <td  width="100" rowspan="4" title="<? echo $row[csf('po_number')]; ?>"><p><? echo $row[csf('po_number')]; ?></p></td>
                     <td  width="100" rowspan="4" title="<? echo $order_status[$row[csf('is_confirmed')]]; ?>"><p><? echo  $order_status[$row[csf('is_confirmed')]]; ?></p></td>
                     <td  width="110" rowspan="4" title="<? echo $row[csf('style_ref_no')]; ?>"><p><? echo $row[csf('style_ref_no')]; ?></p></td>
                     <td  width="110" rowspan="4" title="<?   echo $gmts_item; ?>"><p><? echo $row[csf('gmts_item_id')]; ?>
					<? $gmts_item='';
                    $gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
                    foreach($gmts_item_id as $item_id)
                    {
                        if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
                    }
                    echo $gmts_item;
					?>
                     </p></td>
                     <td rowspan="4"  width="110" title="<? echo $team_member_arr[$row[csf('dealing_marchant')]]; ?>"><p><? echo $team_member_arr[$row[csf('dealing_marchant')]]; ?></p></td>
                     <td rowspan="4" width="70" title="<? echo $ship_po_recv_date; ?>"><p><? echo $ship_po_recv_date; ?></p></td>
                     <td rowspan="4"  width="90" align="right" title="<? echo number_format($row[csf('po_quantity')],2); ?>"><p><? echo number_format($row[csf('po_quantity')],2); ?></p></td>
                     <td rowspan="4" width="90" align="right" title="<? echo number_format($row[csf('avg_unit_price')],2); ?>"><p><? echo number_format($row[csf('avg_unit_price')],2); ?></p></td>
                     <td rowspan="4" width="100" align="right" title="<? echo number_format($total_order_value,2); ?>" ><p><? 
					 $total_order_amount+=$total_order_value;
					 echo number_format($total_order_value,2); ?></p>
                     </td>
                     <td width="100">
                  	 Price Quotation
                     </td>
                     <?
                        $dzn_qnty=0;
						
						$price_costing_per_id=$price_costing_perArray[$row[csf('quotation_id')]]['costing_per'];
						
                        if($price_costing_per_id==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($price_costing_per_id==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($price_costing_per_id==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($price_costing_per_id==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						
						 
						$p_commercial_cost=$price_fabriccostArray[$row[csf('quotation_id')]]['comm_cost'];
						
						$tot_p_commercial_cost=($p_commercial_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$price_fabric_data_Array[$price_fabricRow[csf('quotation_id')]];
						$price_fabricData=explode(",",substr($price_fabric_data_Array[$row[csf('quotation_id')]],0,-1));
						$p_fab_precost_Data=explode(",",substr($p_fab_precost_arr[$row[csf('quotation_id')]],0,-1));
						foreach($price_fabricData as $p_fabricRow)
						{
						$p_fabricRow=explode("**",$p_fabricRow);
						$p_fab_nature_id=$p_fabricRow[0];	
						$p_fab_source_id=$p_fabricRow[1];
						$p_fab_rate=$p_fabricRow[2];
						$p_yarn_qty=$p_fabricRow[3];
						$p_yarn_amount=$p_fabricRow[4];
					
						if($p_fab_source_id==2)
							{
								foreach($p_fab_precost_Data as $p_fab_row)
								{
									$p_fab_dataRow=explode("**",$p_fab_row);
									$p_fab_requirment=$p_fab_dataRow[0];
									$p_fab_pcs=$p_fab_dataRow[1];
									$p_fab_purchase_qty=$p_fab_requirment/$p_fab_pcs*$row[csf('po_quantity')]; 
							
								$p_fab_purchase=$p_fab_purchase_qty*$p_fab_rate; 
									
								}
							}
						else if($p_fab_source_id==1 || $p_fab_source_id==3)
							{
							$p_avg_rate=$p_yarn_amount/$p_yarn_qty;
							$p_yarn_costing=$p_yarn_amount/$dzn_qnty*$row[csf('po_quantity')];	
							
							}
						 
						}
						$p_kniting_cost=$pq_knit_arr[$row[csf('quotation_id')]]['knit']+$pq_knit_arr[$row[csf('quotation_id')]]['weaving']+$pq_knit_arr[$row[csf('quotation_id')]]['collar_cuff']+$pq_knit_arr[$row[csf('quotation_id')]]['feeder_stripe'];
						$tot_p_knit_cost=($p_kniting_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$p_knit_cost_dzn=$p_kniting_cost; 
						$p_washing_cost=($pq_knit_arr[$row[csf('quotation_id')]]['washing']/$dzn_qnty)*$row[csf('po_quantity')];
						$p_all_over_cost=($pq_knit_arr[$row[csf('quotation_id')]]['all_over']/$dzn_qnty)*$row[csf('po_quantity')];;
						$p_yarn_dyeing_cost=($pq_knit_arr[$row[csf('quotation_id')]]['yarn_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$p_yarn_dyeing_cost_dzn=$pq_knit_arr[$row[csf('quotation_id')]]['yarn_dyeing'];
						$p_fabric_dyeing_cost=($pq_knit_arr[$row[csf('quotation_id')]]['fabric_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$p_fabric_dyeing_cost_dzn=$pq_knit_arr[$row[csf('quotation_id')]]['fabric_dyeing'];
						$p_heat_setting_cost=($pq_knit_arr[$row[csf('quotation_id')]]['heat']/$dzn_qnty)*$row[csf('po_quantity')];
						$p_fabric_finish=($pq_knit_arr[$row[csf('quotation_id')]]['fabric_finish']/$dzn_qnty)*$row[csf('po_quantity')];
						
						if($p_fabric_dyeing_cost<=0)
						{
						$color_fab="red";
						}
						else
						{
						$color_fab="";	
						}
						if($p_yarn_costing<=0)
						{
						$color_yarn="red";
						}
						else
						{
						$color_yarn="";	
						}
						if($tot_p_knit_cost<=0)
						{
						$color_knit="red";
						}
						else
						{
						$color_knit="";	
						}
						if($p_fabric_finish<=0)
						{
						$color_finish="red";
						}
						else
						{
						$color_finish="";	
						}
						if($tot_p_commercial_cost<=0)
						{
						$color_com="red";
						}
						else
						{
						$color_com="";	
						}
						
						$p_yarn_cost_percent=($p_yarn_costing/$total_order_value)*100;
						$total_yarn_cost_percent+=$p_yarn_cost_percent;
						$tot_p_yarn_cost_percent+=$p_yarn_cost_percent;
						$tot_yarn_cost_price+=$p_yarn_costing;
						
					 ?>
                     <td width="100" align="right"><a href="##" onClick="generate_pricecost_yarnavg_popup('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('quotation_id')]; ?>','pricost_yarnavg_detail')"><? echo number_format($p_avg_rate,2); ?></a></td>
                     <td width="80" align="right" title="<? echo $p_yarn_costing; ?>" bgcolor="<? echo $color_yarn; ?>"><? echo number_format($p_yarn_costing,2); ?></td>
                     <td width="80" align="right" title="<? echo $p_yarn_cost_percent.'%'; ?>"><? echo number_format($p_yarn_cost_percent,2).'%'; ?></td>
                     <td width="100" align="right"><a href="##" onClick="generate_pricecost_purchase_popup('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $p_fab_source_id; ?>','<? echo $row[csf('quotation_id')]; ?>','fab_price_purchase_detail')"><? echo number_format($p_fab_purchase,2); ?></a></td>
                     <td width="80" title="<? echo $p_knit_cost_dzn; ?>" align="right"><? echo number_format($p_knit_cost_dzn,2); ?></td>
                     <td width="80" align="right" title="<? echo $tot_knit_cost; ?>"  bgcolor="<? echo $color_knit; ?>"><?
					 ?>
                     <a href="##" onClick="generate_pri_cost_knit_popup('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $cons_process; ?>','<? echo $row[csf('quotation_id')]; ?>','pricost_knit_detail')"><? 
					 echo number_format($tot_p_knit_cost,2);
					  ?></a></td>
                     <td  width="100" align="right" title="<? echo number_format($p_yarn_dyeing_cost_dzn ,2); ?>" ><? echo number_format($p_yarn_dyeing_cost_dzn ,2); ?></td>
                     <td  width="110" align="right" title="<? echo number_format($p_yarn_dyeing_cost ,2); ?>" ><? echo number_format($p_yarn_dyeing_cost ,2); ?></td>
                     <td  width="120" align="right"  title="<? echo number_format($p_fabric_dyeing_cost_dzn ,2); ?>" ><? echo number_format($p_fabric_dyeing_cost_dzn,2); 
					 $total_fabrics_cost_summary+=($row[csf('po_quantity')]/$dzn_qnty)*$p_fabric_dyeing_cost_dzn;
						
					  ?></td>
                     
                     <td  width="100" align="right" title="<? echo number_format($p_fabric_dyeing_cost ,2); ?>" bgcolor="<? echo $color_fab; ?>"><a href="##" onClick="generate_pricost_fab_dyeing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $fab_source_id; ?>','<? echo $row[csf('quotation_id')];?>','fab_price_dyeing_detail')"><? echo number_format($p_fabric_dyeing_cost,2); ?></a></td>
                     <td  width="90" align="right"><? echo number_format($p_heat_setting_cost,2); ?></td>
                     <td  width="100" align="right"><a href="##" onClick="generate_pricost_fab_finishing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('quotation_id')]; ?>','fab_price_finishing_detail')"><? echo number_format($p_fabric_finish,2); ?></a> </td>
                     <td  width="90" align="right"><a href="##" onClick="generate_pricost_fab_finishing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('quotation_id')]; ?>','fab_price_washing_detail')"><? echo number_format($p_washing_cost,2); ?></a></td>
                     <td  width="90" align="right"><a href="##" onClick="generate_pricost_fab_all_over_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('quotation_id')]; ?>','fab_price_all_over_detail')"><? echo number_format($p_all_over_cost,2); ?></a></td>
				<?
					
					$tot_p_trim_amount= $price_fabriccostArray[$row[csf('quotation_id')]]['trims_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    $tot_p_test_cost=$price_fabriccostArray[$row[csf('quotation_id')]]['lab_test']/$dzn_qnty*$row[csf('po_quantity')];
                 
				    $p_print_amount=($price_fab_emb[$row[csf('quotation_id')]]['print']/$dzn_qnty)*$row[csf('po_quantity')];
                    $p_embroidery_amount=($price_fab_emb[$row[csf('quotation_id')]]['embroidery']/$dzn_qnty)*$row[csf('po_quantity')];
                    $p_special_amount=($price_fab_emb[$row[csf('quotation_id')]]['special']/$dzn_qnty)*$row[csf('po_quantity')];
                    $p_other_amount=($price_fab_emb[$row[csf('quotation_id')]]['other']/$dzn_qnty)*$row[csf('po_quantity')];
					
                    $p_foreign=$price_commission_array[$row[csf('quotation_id')]]['foreign']/$dzn_qnty*$row[csf('po_quantity')];
                    $p_local=$price_commission_array[$row[csf('quotation_id')]]['local']/$dzn_qnty*$row[csf('po_quantity')];
					
                    $p_freight_cost= $price_fabriccostArray[$row[csf('quotation_id')]]['freight']/$dzn_qnty*$row[csf('po_quantity')];
                    $p_inspection=$price_fabriccostArray[$row[csf('quotation_id')]]['inspection']/$dzn_qnty*$row[csf('po_quantity')];
                    $p_certificate_cost=$price_fabriccostArray[$row[csf('quotation_id')]]['certificate_pre_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    
                    $p_common_oh=$price_fabriccostArray[$row[csf('quotation_id')]]['common_oh']/$dzn_qnty*$row[csf('po_quantity')];
                    $p_currier_cost=$price_fabriccostArray[$row[csf('quotation_id')]]['currier_pre_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    //echo $currier_cost;
                    $p_cm_cost=$price_fabriccostArray[$row[csf('quotation_id')]]['c_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    $p_cm_cost_dzn=$price_fabriccostArray[$row[csf('quotation_id')]]['c_cost'];
                    $total_p_cost=$p_yarn_costing+$p_fab_purchase+$tot_p_knit_cost+$p_washing_cost+$p_all_over_cost+$p_yarn_dyeing_cost+$p_fabric_dyeing_cost+$p_heat_setting_cost+$p_fabric_finish+$tot_p_trim_amount+$tot_p_test_cost+$p_print_amount+$p_embroidery_amount+$p_special_amount+$p_other_amount+$tot_p_commercial_cost+$p_foreign+$p_local+$p_freight_cost+$p_inspection+$p_certificate_cost+$p_common_oh+$p_currier_cost+$p_cm_cost;
					//echo  $total_cost;
					$total_p_print_amount+=$p_print_amount;
					$total_p_embroidery_amount+=$p_embroidery_amount;
					$total_p_special_amount+=$p_special_amount;
					$total_p_other_amount+=$p_other_amount;
					
					$total_p_foreign_amount+=$p_foreign;
					$total_p_local_amount+=$p_local;
					$total_p_test_cost_amount+=$tot_p_test_cost;
					$total_p_freight_amount+=$p_freight_cost;
					$total_p_inspection_amount+=$p_inspection;
					$total_p_certificate_amount+=$p_certificate_cost;
					
					$total_p_common_oh_amount+=$p_common_oh;
					$total_p_currier_amount+=$p_currier_cost;
					$total_p_cm_amount+=$p_cm_cost;
					$max_profit=$asking_profit_arr[$row[csf('company_name')]]['max_profit'];
					//echo $max_profit;
					$company_asking=$asking_profit_arr[$row[csf('company_name')]]['asking_profit'];
					
					if($tot_p_trim_amount<=0)
						{
						$color_trim="red";
						}
						else
						{
						$color_trim="";	
						}
						
					if($p_cm_cost<=0)
						{
						$color="red";
						}
						else
						{
						$color="";	
						}
						
					?>
                     <td width="100" align="right" bgcolor="<? echo $color_trim; ?>"><a href="##" onClick="generate_pricost_trim_cost_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('quotation_id')]; ?>','trim_cost_price_detail')"><? echo number_format($tot_p_trim_amount,2); ?></a></td>
                     <td width="80" align="right"><? echo number_format($p_print_amount,2); ?></td>
                     <td width="85" align="right"><? echo number_format($p_embroidery_amount,2); ?></td>
                     <td width="80" align="right"><? echo number_format($p_special_amount,2); ?></td>
                     <td width="80" align="right"><? echo number_format($p_other_amount,2); ?></td>
                     <td width="120" align="right" bgcolor="<? echo $color_com; ?>"><? echo number_format($tot_p_commercial_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($p_foreign,2) ?></td>
                     <td width="120" align="right"><? echo number_format($p_local,2) ?></td>
                     <td width="100" align="right"><? echo number_format($tot_p_test_cost,2);?></td>
                     <td width="100" align="right"><? echo number_format($p_freight_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($p_inspection,2);?></td>
                     <td width="100" align="right"><? echo number_format($p_certificate_cost,2); ?></td>
                     <td width="100" align="right"><? echo number_format($p_common_oh,2); ?></td>
                     <td width="100" align="right"><? echo number_format($p_currier_cost,2);?></td>
                     <td width="120" align="right"><? echo number_format($p_cm_cost_dzn,2);?></td>
                     <td width="100" align="right" bgcolor="<? echo $color; ?>"><? echo number_format($p_cm_cost,2);?></td>
                     <td width="100" align="right"><? echo number_format($total_p_cost,2); ?></td>
                    <?
						
						$plan_cut_qnty=$row[csf('plan_cut')]*$row[csf('ratio')];
						$total_p_profit=$total_order_value-$total_p_cost;
						$total_p_profit_percentage2=$total_p_profit/$total_order_value*100; 
						if($total_p_profit_percentage2<=0 )
						{
							$color_pl="red";
						}
						else if($total_p_profit_percentage2>$max_profit)
						{
							$color_pl="yellow";	
						}
						else if($total_p_profit_percentage2<=$max_profit)
						{
							$color_pl="green";	
						}
						else
						{
							$color_pl="";	
						}
						$p_expected_profit=$asking_profit_arr[$row[csf('company_name')]]['asking_profit']*$total_order_value/100;
						
					?>
                     <td width="100" align="right" bgcolor="<? echo $color_pl; ?>"><? echo number_format($total_p_profit,2); ?></td>
                     <td width="100" align="right"><? echo number_format($total_p_profit_percentage2,2).'%'; ?></td>
                     <td width="100" align="right"><?   echo number_format($p_expected_profit,2); ?></td>
                     <td width="" align="right"><? $tot_expect_variance=$total_p_profit-$p_expected_profit; echo number_format($tot_expect_variance,2)?></td>
                  </tr> 
                   <tr bgcolor="<? echo $bgcolor;?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
                     <td width="100">
                     Pre Cost
                     </td>
                     <?
                        $dzn_qnty=0;
						$costing_per_id=$fabriccostArray[$row[csf('job_no')]]['costing_per_id'];
                        if($costing_per_id==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($costing_per_id==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($costing_per_id==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($costing_per_id==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$dzn_qnty_yarn=$dzn_qnty*$row[csf('ratio')];
						$commercial_cost=$fabriccostArray[$row[csf('job_no')]]['comm_cost'];
						$tot_commercial_cost=($commercial_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$fabricData=explode(",",substr($fabric_data_Array[$row[csf('job_no')]],0,-1));
						$fab_precost_Data=explode(",",substr($fab_precost_arr[$row[csf('job_no')]][$row[csf('po_id')]],0,-1));
						foreach($fabricData as $fabricRow)
						{
						$fabricRow=explode("**",$fabricRow);
						$fab_nature_id=$fabricRow[0];	
						$fab_source_id=$fabricRow[1];
						$fab_rate=$fabricRow[2];
						$yarn_qty=$fabricRow[3];
						$yarn_amount=$fabricRow[4];
						
						if($fab_source_id==2)
							{
							foreach($fab_precost_Data as $fab_row)
							{
								$fab_dataRow=explode("**",$fab_row);
								$fab_requirment=$fab_dataRow[0];
								$fab_pcs=$fab_dataRow[1];
								$fab_requirment=number_format($fab_requirment,4);
								
								$fab_purchase_qty=$fab_requirment/$fab_pcs*$row[csf('po_quantity')]; 
							
							$fab_purchase=$fab_purchase_qty*$fab_rate; 
								
							}
							}
						else if($fab_source_id==1 || $fab_source_id==3)
							{
							$avg_rate=$yarn_amount/$yarn_qty;
							$yarn_costing=$yarn_amount/$dzn_qnty*$row[csf('po_quantity')];		
							}
						 
						}
						$kniting_cost=$knit_arr[$row[csf('job_no')]]['knit']+$knit_arr[$row[csf('job_no')]]['weaving']+$knit_arr[$row[csf('job_no')]]['collar_cuff']+$knit_arr[$row[csf('job_no')]]['feeder_stripe'];
						$tot_knit_cost=($kniting_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$knit_cost_dzn=$kniting_cost; 
						$washing_cost=($knit_arr[$row[csf('job_no')]]['washing']/$dzn_qnty)*$row[csf('po_quantity')];
						$all_over_cost=($knit_arr[$row[csf('job_no')]]['all_over']/$dzn_qnty)*$row[csf('po_quantity')];
						$yarn_dyeing_cost=($knit_arr[$row[csf('job_no')]]['yarn_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$yarn_dyeing_cost_dzn=$knit_arr[$row[csf('job_no')]]['yarn_dyeing'];
						$fabric_dyeing_cost=($knit_arr[$row[csf('job_no')]]['fabric_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$fabric_dyeing_cost_dzn=$knit_arr[$row[csf('job_no')]]['fabric_dyeing'];
						$heat_setting_cost=($knit_arr[$row[csf('job_no')]]['heat']/$dzn_qnty)*$row[csf('po_quantity')];
						$fabric_finish=($knit_arr[$row[csf('job_no')]]['fabric_finish']/$dzn_qnty)*$row[csf('po_quantity')];
						
						if($fabric_dyeing_cost<=0)
						{
						$color_fab="red";
						}
						else
						{
						$color_fab="";	
						}
						if($yarn_costing<=0)
						{
						$color_yarn="red";
						}
						else
						{
						$color_yarn="";	
						}
						if($kniting_cost<=0)
						{
						$color_knit="red";
						}
						else
						{
						$color_knit="";	
						}
						if($fabric_finish<=0)
						{
						$color_finish="red";
						}
						else
						{
						$color_finish="";	
						}
						if($commercial_cost<=0)
						{
						$color_com="red";
						}
						else
						{
						$color_com="";	
						}
						
						$yarn_cost_percent=($yarn_costing/$total_order_value)*100;
						$total_yarn_cost_percent+=$yarn_cost_percent;
					 ?>
                     <td width="100" align="right"><a href="##" onClick="generate_pre_cost_report('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','precost_yarn_detail')"><? echo  number_format($avg_rate,2); ?></a></td>
                     <td width="80" align="right" title="<? echo $yarn_costing; ?>" bgcolor="<? echo $color_yarn; ?>"><? echo number_format($yarn_costing,2); ?></td>
                     <td width="80" align="right" title="<? echo $yarn_cost_percent; ?>"><? echo number_format($yarn_cost_percent,2).'%'; ?></td>
                     <td width="100" align="right"><a href="##" onClick="generate_precost_fab_purchase_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $fab_source_id; ?>','fab_purchase_detail')"><? echo number_format($fab_purchase,2); ?></a></td>
                     <td width="80" title="<? echo $knit_cost_dzn; ?>" align="right"><? echo number_format($knit_cost_dzn,2); ?></td>
                     <td width="80" align="right" title="<? echo $tot_knit_cost; ?>"  bgcolor="<? echo $color_knit; ?>"><?
					 ?>
                     <a href="##" onClick="generate_pre_cost_knit_popup('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $cons_process; //$row[csf('style_ref_no')]; ?>','precost_knit_detail')"><? 
					 echo number_format($tot_knit_cost,2);
					  ?></a></td>
                     <td  width="100" align="right" title="<? echo number_format($yarn_dyeing_cost_dzn ,2); ?>" ><? echo number_format($yarn_dyeing_cost_dzn ,2); ?></td>
                     <td  width="110" align="right" title="<? echo number_format($yarn_dyeing_cost ,2); ?>" ><? echo number_format($yarn_dyeing_cost ,2); ?></td>
                     <td  width="120" align="right"  title="<? echo number_format($fabric_dyeing_cost_dzn ,2); ?>" ><? echo number_format($fabric_dyeing_cost_dzn,2); 
					 $total_fabrics_cost_summary+=($row[csf('po_quantity')]/$dzn_qnty)*$fabric_dyeing_cost_dzn;
						
					  ?></td>
                     
                     <td  width="100" align="right" title="<? echo number_format($fabric_dyeing_cost ,2); ?>" bgcolor="<? echo $color_fab; ?>"><a href="##" onClick="generate_precost_fab_dyeing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $fab_source_id; ?>','fab_dyeing_detail')"><? echo number_format($fabric_dyeing_cost,2); ?></a></td>
                     <td  width="90" align="right"><? echo number_format($heat_setting_cost,2); ?></td>
                     <td  width="100" align="right" ><a href="##" onClick="generate_precost_fab_finishing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','fab_finishing_detail')"><? echo number_format($fabric_finish,2); ?></a> </td>
                     <td  width="90" align="right"><a href="##" onClick="generate_precost_fab_finishing_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','fab_washing_detail')"><? echo number_format($washing_cost,2); ?></a></td>
                     <td  width="90" align="right"><a href="##" onClick="generate_precost_fab_all_over_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','fab_all_over_detail')"><? echo number_format($all_over_cost,2); ?></a></td>
				<?
					$tot_trim_amount= $fabriccostArray[$row[csf('job_no')]]['trims_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    $tot_test_cost=$fabriccostArray[$row[csf('job_no')]]['lab_test']/$dzn_qnty*$row[csf('po_quantity')];
                    $print_amount=($fab_emb[$row[csf('job_no')]]['print']/$dzn_qnty)*$row[csf('po_quantity')];
                    $embroidery_amount=($fab_emb[$row[csf('job_no')]]['embroidery']/$dzn_qnty)*$row[csf('po_quantity')];
					
                    $special_amount=($fab_emb[$row[csf('job_no')]]['special']/$dzn_qnty)*$row[csf('po_quantity')];
                    $other_amount=($fab_emb[$row[csf('job_no')]]['other']/$dzn_qnty)*$row[csf('po_quantity')];
                    $foreign=$commission_array[$row[csf('job_no')]]['foreign']/$dzn_qnty*$row[csf('po_quantity')];
                    $local=$commission_array[$row[csf('job_no')]]['local']/$dzn_qnty*$row[csf('po_quantity')];
                    $freight_cost= $fabriccostArray[$row[csf('job_no')]]['freight']/$dzn_qnty*$row[csf('po_quantity')];
                    $inspection=$fabriccostArray[$row[csf('job_no')]]['inspection']/$dzn_qnty*$row[csf('po_quantity')];
                    $certificate_cost=$fabriccostArray[$row[csf('job_no')]]['certificate_pre_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    
                    $common_oh=$fabriccostArray[$row[csf('job_no')]]['common_oh']/$dzn_qnty*$row[csf('po_quantity')];
                    $currier_cost=$fabriccostArray[$row[csf('job_no')]]['currier_pre_cost']/$dzn_qnty*$row[csf('po_quantity')];
                  
                    $cm_cost=$fabriccostArray[$row[csf('job_no')]]['c_cost']/$dzn_qnty*$row[csf('po_quantity')];
                    $cm_cost_dzn=$fabriccostArray[$row[csf('job_no')]]['c_cost'];
                    $total_cost=$yarn_costing+$fab_purchase+$tot_knit_cost+$washing_cost+$all_over_cost+$yarn_dyeing_cost+$fabric_dyeing_cost+$heat_setting_cost+$fabric_finish+$tot_trim_amount+$tot_test_cost+$print_amount+$embroidery_amount+$special_amount+$other_amount+$tot_commercial_cost+$foreign+$local+$freight_cost+$inspection+$certificate_cost+$common_oh+$currier_cost+$cm_cost;
					
					$total_print_amount+=$print_amount;
					
					$total_embroidery_amount+=$embroidery_amount;
					
					$total_special_amount+=$special_amount;
					$total_other_amount+=$other_amount;
					
					$total_foreign_amount+=$foreign;
					$total_local_amount+=$local;
					$total_test_cost_amount+=$tot_test_cost;
					$total_freight_amount+=$freight_cost;
					$total_inspection_amount+=$inspection;
					$total_certificate_amount+=$certificate_cost;
					
					$total_common_oh_amount+=$common_oh;
					$total_currier_amount+=$currier_cost;
					$total_cm_amount+=$cm_cost;
					$max_profit=$asking_profit_arr[$row[csf('company_name')]]['max_profit'];
					
					$company_asking=$asking_profit_arr[$row[csf('company_name')]]['asking_profit'];
					
					if($tot_trim_amount<=0)
						{
						$color_trim="red";
						}
						else
						{
						$color_trim="";	
						}
						
					if($cm_cost<=0)
						{
						$color="red";
						}
						else
						{
						$color="";	
						}
						$yarnData=explode(",",substr($yarncostArray[$row[csf('job_no')]],0,-1));
						
						foreach($yarnData as $yarnRow)
						{
							$yarnRow=explode("**",$yarnRow);
							$count_id=$yarnRow[0];
							$type_id=$yarnRow[1];
							$cons_qnty=$yarnRow[2];
							$amount=$yarnRow[3];
													
							$yarn_desc=$yarn_count_library[$count_id]."**".$yarn_type[$type_id];
							$req_qnty=($plan_cut_qnty/$dzn_qnty_yarn)*$cons_qnty;
							$req_amnt=($plan_cut_qnty/$dzn_qnty_yarn)*$amount;
							 
							$yarn_desc_array[$yarn_desc]['qnty']+=$req_qnty;
							$yarn_desc_array[$yarn_desc]['amnt']+=$req_amnt;
						}
				
					?>
                     <td width="100" align="right" bgcolor="<? echo $color_trim; ?>"><a href="##" onClick="generate_precost_trim_cost_detail('<? echo $row[csf('po_id')]; ?>','<? echo $row[csf('job_no')];?>','<? echo $row[csf('company_name')]; ?>','<? echo $row[csf('buyer_name')]; ?>','<? echo $row[csf('style_ref_no')]; ?>','trim_cost_detail')"><? echo number_format($tot_trim_amount,2); ?></a></td>
                     <td width="80" align="right"><? echo number_format($print_amount,2); ?></td>
                     <td width="85" align="right"><? echo number_format($embroidery_amount,2); ?></td>
                     <td width="80" align="right"><? echo number_format($special_amount,2); ?></td>
                     <td width="80" align="right"><? echo number_format($other_amount,2); ?></td>
                     <td width="120" align="right" bgcolor="<? echo $color_com; ?>"><? echo number_format($tot_commercial_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($foreign,2) ?></td>
                     <td width="120" align="right"><? echo number_format($local,2) ?></td>
                     <td width="100" align="right"><? echo number_format($tot_test_cost,2);?></td>
                     <td width="100" align="right"><? echo number_format($freight_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($inspection,2);?></td>
                     <td width="100" align="right"><? echo number_format($certificate_cost,2); ?></td>
                     <td width="100" align="right"><? echo number_format($common_oh,2); ?></td>
                     <td width="100" align="right"><? echo number_format($currier_cost,2);?></td>
                     <td width="120" align="right"><? echo number_format($cm_cost_dzn,2);?></td>
                     <td width="100" align="right" bgcolor="<? echo $color; ?>"><? echo number_format($cm_cost,2);?></td>
                     <td width="100" align="right"><? echo number_format($total_cost,2); ?></td>
                    <?
						$total_profit=$total_order_value-$total_cost;
						$total_profit_percentage2=$total_profit/$total_order_value*100; 
						if($total_profit_percentage2<=0 )
						{
							$color_pl="red";
						}
						else if($total_profit_percentage2>$max_profit)
						{
							$color_pl="yellow";	
						}
						else if($total_profit_percentage2<=$max_profit)
						{
							$color_pl="green";	
						}
						else
						{
							$color_pl="";	
						}
					?>
                     <td width="100" align="right" bgcolor="<? echo $color_pl; ?>"><? echo number_format($total_profit,2); ?></td>
                     <td width="100" align="right"><? echo number_format($total_profit_percentage2,2).'%'; ?></td>
                     <td width="100" align="right"><?  $expected_profit=$asking_profit_arr[$row[csf('company_name')]]['asking_profit']*$total_order_value/100; echo number_format($expected_profit,2) //$total_profit=$total_cost-$total_order_value; //echo number_format($total_profit,2); ?></td>
                     <td width="" align="right"><? $expect_variance=$total_profit-$expected_profit; echo number_format($expect_variance,2)?></td>
                  </tr> 
                   <tr bgcolor="<? echo $bgcolor;?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
                     <td width="100">
                     Variance
                     </td>
                     <?
						$total_avg_rate_percent_variance=$p_avg_rate-$avg_rate;
						$tot_fab_purchase_variance=$p_fab_purchase-$fab_purchase;
						$tot_knit_cost_dzn_variance=$p_knit_cost_dzn-$knit_cost_dzn;
						$tot_knit_cost_variance=$tot_p_knit_cost-$tot_knit_cost;
						$tot_yarn_dyeing_cost_dzn_vairance=$p_yarn_dyeing_cost_dzn-$yarn_dyeing_cost_dzn ;
						$tot_yarn_dyeing_cost_variance=$p_yarn_dyeing_cost-$yarn_dyeing_cost;
						$tot_fabric_dyeing_variance=$p_fabric_dyeing_cost_dzn-$fabric_dyeing_cost_dzn;
						$tot_fabric_dyeing_cost_variance=$p_fabric_dyeing_cost-$fabric_dyeing_cost;
						$tot_heat_variance_cost=$p_heat_setting_cost-$heat_setting_cost;
						$tot_fabric_finish_variance_cost=$p_fabric_finish-$fabric_finish;
						$tot_wash_cost_variance=$p_washing_cost-$washing_cost;
						$tot_all_over_variance=$p_all_over_cost-$all_over_cost;
					 ?>
                     <td width="100" align="right"><? echo number_format($total_avg_rate_percent_variance,2); ?></td>
                     <td width="80" align="right" title="<? echo $tot_yarn_variance; ?>" bgcolor="<? echo $color_yarn; ?>"><? $tot_yarn_variance=$p_yarn_costing-$yarn_costing; echo number_format($tot_yarn_variance,2); ?></td>
                     <td width="80" align="right" title="<? echo $tot_yarn_cost_percent; ?>"><? $tot_yarn_cost_percent=$p_yarn_cost_percent-$yarn_cost_percent;echo number_format($tot_yarn_cost_percent,2).'%'; ?></td>
                     <td width="100" align="right"><? echo number_format($tot_fab_purchase_variance,2); ?></td>
                     <td width="80" title="<? echo $knit_cost_dzn; ?>" align="right"><? echo number_format($tot_knit_cost_dzn_variance,2); ?></td>
                     <td width="80" align="right" title="<? echo $tot_knit_cost; ?>"  bgcolor="<? echo $color_knit; ?>"><?
					 ?>
                     <? 
					 echo number_format($tot_p_knit_cost-$tot_knit_cost,2);
					  ?></td>
                     <td  width="100" align="right" title="<? echo number_format($tot_yarn_dyeing_cost_dzn_vairance ,2); ?>" ><? echo number_format($tot_yarn_dyeing_cost_dzn_vairance ,2); ?></td>
                     <td  width="110" align="right" title="<? echo number_format($tot_yarn_dyeing_cost_variance ,2); ?>" ><? echo number_format($tot_yarn_dyeing_cost_variance ,2); ?></td>
                     <td  width="120" align="right"  title="<? echo number_format($tot_fabric_dyeing_variance ,2); ?>" ><? echo number_format($tot_fabric_dyeing_variance,2); 
					 $total_fabrics_cost_summary+=($row[csf('po_quantity')]/$dzn_qnty)*$fabric_dyeing_cost_dzn;
						
					  ?></td>
                     
                     <td  width="100" align="right" title="<? //echo number_format($fabric_dyeing_cost ,2); ?>" bgcolor="<? echo $color_fab; ?>"><? echo number_format($tot_fabric_dyeing_cost_variance,2); ?></td>
                     <td  width="90" align="right"><? echo number_format($tot_heat_variance_cost,2); ?></td>
                     <td  width="100" align="right"><? echo number_format($tot_fabric_finish_variance_cost,2); ?> </td>
                     <td  width="90" align="right"><? echo number_format($p_washing_cost-$washing_cost,2); ?></td>
                     <td  width="90" align="right"><? echo number_format($p_all_over_cost-$all_over_cost,2); ?></td>
				<?
					
					$max_profit=$asking_profit_arr[$row[csf('company_name')]]['max_profit'];
					//echo $max_profit;
					$company_asking=$asking_profit_arr[$row[csf('company_name')]]['asking_profit'];
					
					if($tot_trim_amount<=0)
						{
						$color_trim="red";
						}
						else
						{
						$color_trim="";	
						}
						
					if($cm_cost<=0)
						{
						$color="red";
						}
						else
						{
						$color="";	
						}
						
						$tot_trim_cost_variance=$tot_p_trim_amount-$tot_trim_amount;
						$tot_print_varinace=$p_print_amount-$print_amount;
						$tot_embrodery_variance=$p_embroidery_amount-$embroidery_amount;
						$tot_special_variance=$p_special_amount-$special_amount;
						$tot_other_amount_variance=$p_other_amount-$other_amount;
						$tot_commercial_variance=$tot_p_commercial_cost-$tot_commercial_cost;
						$tot_local_variance=$p_local-$local;
						$tot_test_variance=$tot_p_test_cost-$tot_test_cost;
						$tot_feight_variance=$p_freight_cost-$freight_cost;
						$tot_inspection_variance=$p_inspection-$inspection;
						$tot_certificate_variance=$p_certificate_cost-$certificate_cost;
						$tot_common_variance=$p_common_oh-$common_oh;
						$tot_currier_variance=$p_currier_cost-$currier_cost;
						$tot_cm_dzn_variance=$p_cm_cost_dzn-$cm_cost_dzn;
						$tot_cm_variance=$p_cm_cost-$cm_cost;
						$tot_total_cost_varaince=$total_p_cost-$total_cost;
				
					?>
                     <td width="100" align="right"><? echo number_format($tot_p_trim_amount-$tot_trim_amount,2); ?></td>
                     <td width="80" align="right"><? echo number_format($tot_print_varinace,2); ?></td>
                     <td width="85" align="right"><? echo number_format($tot_embrodery_variance,2); ?></td>
                     <td width="80" align="right"><? echo number_format($tot_special_variance,2); ?></td>
                     <td width="80" align="right"><? echo number_format($p_other_amount-$other_amount,2); ?></td>
                     <td width="120" align="right"><? echo number_format($tot_p_commercial_cost-$tot_commercial_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($p_foreign-$foreign,2) ?></td>
                     <td width="120" align="right"><? echo number_format($tot_local_variance,2) ?></td>
                     <td width="100" align="right"><? echo number_format($tot_test_variance,2);?></td>
                     <td width="100" align="right"><? echo number_format($p_freight_cost-$freight_cost,2); ?></td>
                     <td width="120" align="right"><? echo number_format($p_inspection-$inspection,2);?></td>
                     <td width="100" align="right"><? echo number_format($tot_certificate_variance,2); ?></td>
                     <td width="100" align="right"><? echo number_format($tot_common_variance,2); ?></td>
                     <td width="100" align="right"><? echo number_format($p_currier_cost-$currier_cost,2);?></td>
                     <td width="120" align="right"><? echo number_format($tot_cm_dzn_variance,2);?></td>
                     <td width="100" align="right"><? echo number_format($p_cm_cost-$cm_cost,2);?></td>
                     <td width="100" align="right"><? echo number_format($total_p_cost-$total_cost,2); ?></td>
                    <?
						$total_profit=$total_order_value-$total_cost;
						$total_profit_percentage2=$total_profit/$total_order_value*100; 
						if($total_profit_percentage2<=0 )
						{
							$color_pl="red";
						}
						else if($total_profit_percentage2>$max_profit)
						{
							$color_pl="yellow";	
						}
						else if($total_profit_percentage2<=$max_profit)
						{
							$color_pl="green";	
						}
						else
						{
							$color_pl="";	
						}
						$tot_profit_variance=$total_profit-$total_p_profit;
						$tot_profit_percient_varaince=$total_profit_percentage2-$total_p_profit_percentage2;
						
						$tot_expected_profit=$expected_profit-$p_expected_profit;
						
						$tot_expected_varaince_data_vairance=$expect_variance-$tot_expect_variance;
					
						
					?>
                     <td width="100" align="right" bgcolor="<? echo $color_pl; ?>"><? echo number_format($total_profit-$total_p_profit,2); ?></td>
                     <td width="100" align="right"><? echo number_format($total_profit_percentage2-$total_p_profit_percentage2,2).'%'; ?></td>
                     <td width="100" align="right"><?   echo '-'; //$tot_expected_profit ?></td>
                     <td width="" align="right"><? echo '-';// echo number_format($tot_expected_varaince_data_vairance,2)?></td>
                  </tr>
                  <tr bgcolor="<? echo $bgcolor;?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
                
                     
                     <td width="100">
                     Variance %
                     </td>
                     <?
                        $dzn_qnty=0;
						$costing_per_id=$fabriccostArray[$row[csf('job_no')]]['costing_per_id'];
                        if($costing_per_id==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($costing_per_id==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($costing_per_id==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($costing_per_id==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$commercial_cost=$fabriccostArray[$row[csf('job_no')]]['comm_cost'];
						$tot_commercial_cost=($commercial_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$fabricData=explode(",",substr($fabric_data_Array[$row[csf('job_no')]],0,-1));
						$fab_precost_Data=explode(",",substr($fab_precost_arr[$row[csf('job_no')]][$row[csf('po_id')]],0,-1));
						foreach($fabricData as $fabricRow)
						{
						$fabricRow=explode("**",$fabricRow);
						$fab_nature_id=$fabricRow[0];	
						$fab_source_id=$fabricRow[1];
						$fab_rate=$fabricRow[2];
						$yarn_qty=$fabricRow[3];
						$yarn_amount=$fabricRow[4];
						if($fab_source_id==2)
							{
							foreach($fab_precost_Data as $fab_row)
							{
								$fab_dataRow=explode("**",$fab_row);
								$fab_requirment=$fab_dataRow[0];
								$fab_pcs=$fab_dataRow[1];
								$fab_purchase_qty=$fab_requirment/$fab_pcs*$row[csf('po_quantity')]; 
							
							$fab_purchase=$fab_purchase_qty*$fab_rate; 
								
							}
							}
						else if($fab_source_id==1 || $fab_source_id==3)
							{
							$avg_rate=$yarn_amount/$yarn_qty;
							$yarn_costing=$yarn_amount/$dzn_qnty*$row[csf('po_quantity')];		
							}
						 
						}
						$kniting_cost=$knit_arr[$row[csf('job_no')]]['knit']+$knit_arr[$row[csf('job_no')]]['weaving']+$knit_arr[$row[csf('job_no')]]['collar_cuff']+$knit_arr[$row[csf('job_no')]]['feeder_stripe'];
						$tot_knit_cost=($kniting_cost/$dzn_qnty)*$row[csf('po_quantity')];
						$knit_cost_dzn=$kniting_cost; 
						$washing_cost=($knit_arr[$row[csf('job_no')]]['washing']/$dzn_qnty)*$row[csf('po_quantity')];
						$all_over_cost=($knit_arr[$row[csf('job_no')]]['all_over']/$dzn_qnty)*$row[csf('po_quantity')];
						$yarn_dyeing_cost=($knit_arr[$row[csf('job_no')]]['yarn_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$yarn_dyeing_cost_dzn=$knit_arr[$row[csf('job_no')]]['yarn_dyeing'];
						$fabric_dyeing_cost=($knit_arr[$row[csf('job_no')]]['fabric_dyeing']/$dzn_qnty)*$row[csf('po_quantity')];
						$fabric_dyeing_cost_dzn=$knit_arr[$row[csf('job_no')]]['fabric_dyeing'];
						$heat_setting_cost=($knit_arr[$row[csf('job_no')]]['heat']/$dzn_qnty)*$row[csf('po_quantity')];
						$fabric_finish=($knit_arr[$row[csf('job_no')]]['fabric_finish']/$dzn_qnty)*$row[csf('po_quantity')];
						
						if($fabric_dyeing_cost<=0)
						{
						$color_fab="red";
						}
						else
						{
						$color_fab="";	
						}
						if($yarn_costing<=0)
						{
						$color_yarn="red";
						}
						else
						{
						$color_yarn="";	
						}
						if($kniting_cost<=0)
						{
						$color_knit="red";
						}
						else
						{
						$color_knit="";	
						}
						if($fabric_finish<=0)
						{
						$color_finish="red";
						}
						else
						{
						$color_finish="";	
						}
						if($commercial_cost<=0)
						{
						$color_com="red";
						}
						else
						{
						$color_com="";	
						}
						
						$tot_yarn_cost_variance=$p_yarn_costing/$yarn_costing*100;
						$yarn_cost_variance_percent=($tot_yarn_cost_variance/$total_order_value)*100;
						
						$total_yarn_cost_variance_percent+=($tot_yarn_variance/$p_yarn_costing)*100;
						
					 ?>
                     <td width="100" align="right"><? echo number_format($total_avg_rate_percent_variance/$p_avg_rate*100,2).'%'; ?></td>
                     <td width="80" align="right" title="<? echo number_format($tot_yarn_variance/$p_yarn_costing*100,2).'%'; ?>" bgcolor="<? echo $color_yarn; ?>"><? echo number_format($tot_yarn_variance/$p_yarn_costing*100,2); ?></td>
                     <td width="80" align="right" title="<? echo $yarn_cost_variance_percent/$p_yarn_cost_percent*100; ?>"><? echo number_format($tot_yarn_cost_percent/$p_yarn_cost_percent*100,2).'%'; ?></td>
                     <td width="100" align="right"><? echo number_format($tot_fab_purchase_variance/$p_fab_purchase*100,2).'%'; ?></td>
                     <td width="80" title="<? echo $tot_knit_cost_dzn_variance/$p_knit_cost_dzn*100; ?>" align="right"><? echo number_format($tot_knit_cost_dzn_variance/$p_knit_cost_dzn*100,2).'%'; ?></td>
                     <td width="80" align="right" title="<? echo $tot_knit_cost; ?>"  bgcolor="<? echo $color_knit; ?>"><?
					 ?>
                    <? 
					 echo number_format($tot_knit_cost_variance/$tot_p_knit_cost*100,2).'%';
					  ?></td>
                     <td  width="100" align="right" title="<? echo number_format($p_yarn_dyeing_cost_dzn /$yarn_dyeing_cost_dzn*100 ,2); ?>" ><? echo number_format($tot_yarn_dyeing_cost_dzn_vairance/$p_yarn_dyeing_cost_dzn*100 ,2).'%'; ?></td>
                     <td  width="110" align="right" title="<? echo number_format($p_yarn_dyeing_cost/$yarn_dyeing_cost*100 ,2); ?>" ><? echo number_format($tot_yarn_dyeing_cost_variance/$p_yarn_dyeing_cost*100 ,2).'%'; ?></td>
                     <td  width="120" align="right"  title="<? echo number_format($tot_fabric_dyeing_variance/$p_fabric_dyeing_cost_dzn*100 ,2); ?>" ><? echo number_format($tot_fabric_dyeing_variance/$p_fabric_dyeing_cost_dzn*100,2).'%'; 
					 $total_fabrics_cost_summary+=($row[csf('po_quantity')]/$dzn_qnty)*$fabric_dyeing_cost_dzn;
						
					  ?></td>
                     
                     <td  width="100" align="right" title="<? echo number_format($tot_fabric_dyeing_cost_variance/$p_fabric_dyeing_cost*100,2); ?>" bgcolor="<? echo $color_fab; ?>"><? echo number_format($tot_fabric_dyeing_cost_variance/$p_fabric_dyeing_cost*100,2).'%'; ?></td>
                     <td  width="90" align="right"><? echo number_format($tot_heat_variance_cost/$p_heat_setting_cost*100,2).'%'; ?></td>
                     <td  width="100" align="right" ><? echo number_format($tot_fabric_finish_variance_cost/$p_fabric_finish*100,2).'%'; ?> </td>
                     <td  width="90" align="right"><? echo number_format($tot_wash_cost_variance/$p_washing_cost*100,2).'%'; ?></td>
                     <td  width="90" align="right"><? echo number_format($tot_all_over_variance/$p_all_over_cost*100,2).'%'; ?></td>
				<?
					
					$total_cm_amount+=$cm_cost;
					$max_profit=$asking_profit_arr[$row[csf('company_name')]]['max_profit'];
					//echo $max_profit;
					$company_asking=$asking_profit_arr[$row[csf('company_name')]]['asking_profit'];
					
					if($tot_trim_cost_variance<=0)
						{
						$color_trim="red";
						}
						else
						{
						$color_trim="";	
						}
						
					if($cm_cost<=0)
						{
						$color="red";
						}
						else
						{
						$color="";	
						}
						
					$tot_foreign_variance=$p_foreign/$foreign*100;
					?>
                     <td width="100" align="right"><? echo number_format($tot_trim_cost_variance/$tot_p_trim_amount*100,2).'%'; ?></td>
                     <td width="80" align="right"><? echo number_format($tot_print_varinace/$p_print_amount*100,2).'%'; ?></td>
                     <td width="85" align="right"><? echo number_format($tot_embrodery_variance/$p_embroidery_amount*100,2).'%'; ?></td>
                     <td width="80" align="right"><? echo number_format($tot_special_variance/$p_special_amount*100,2).'%'; ?></td>
                     <td width="80" align="right"><? echo number_format($tot_other_amount_variance/$p_other_amount*100,2).'%'; ?></td>
                     <td width="120" align="right"><? echo number_format($tot_commercial_variance/$tot_p_commercial_cost*100,2).'%'; ?></td>
                     <td width="120" align="right"><? echo number_format($tot_foreign_variance/$p_foreign*100,2).'%'; ?></td>
                     <td width="120" align="right"><? echo number_format($tot_local_variance/$p_local*100,2).'%'; ?></td>
                     <td width="100" align="right"><? echo number_format($tot_test_variance/$tot_p_test_cost*100,2).'%';?></td>
                     <td width="100" align="right"><? echo number_format($tot_feight_variance/$p_freight_cost*100,2).'%'; ?></td>
                     <td width="120" align="right"><? echo number_format($tot_inspection_variance/$p_inspection*100,2).'%';?></td>
                     <td width="100" align="right"><? echo number_format($tot_certificate_variance/$p_certificate_cost*100,2).'%'; ?></td>
                     <td width="100" align="right"><? echo number_format($tot_common_variance/$p_common_oh*100,2).'%'; ?></td>
                     <td width="100" align="right"><? echo number_format($tot_currier_variance/$p_currier_cost*100,2).'%';?></td>
                     <td width="120" align="right"><? echo number_format($tot_cm_dzn_variance/$p_cm_cost_dzn*100,2).'%';?></td>
                     <td width="100" align="right"><? echo number_format($tot_cm_variance/$p_cm_cost*100,2).'%';?></td>
                     <td width="100" align="right"><? echo number_format($tot_total_cost_varaince/$total_p_cost*100,2).'%'; ?></td>
                    <?
						$total_cost_vari_percent_amount=$total_p_cost/$total_cost*100;
						$total_profit_vari_percnt_amount=$total_order_value-$total_cost_vari_percent_amount;
						$total_profit_percentage2=$total_profit/$total_order_value*100; 
						if($total_profit_percentage2<=0 )
						{
							$color_pl="red";
						}
						else if($total_profit_percentage2>$max_profit)
						{
							$color_pl="yellow";	
						}
						else if($total_profit_percentage2<=$max_profit)
						{
							$color_pl="green";	
						}
						else
						{
							$color_pl="";	
						}
						
					?>
                     <td width="100" align="right"><? echo number_format($tot_profit_variance/$total_p_profit*100,2).'%'; ?></td>
                     <td width="100" align="right"><? echo number_format($tot_profit_percient_varaince/$total_p_profit_percentage2*100,2).'%'; ?></td>
                     <td width="100" align="right">
					 <?  
					 $expected_profit=$asking_profit_arr[$row[csf('company_name')]]['asking_profit']*$total_order_value/100; 
					 echo number_format($tot_expected_profit_varaince/$expected_profit*100,2).'%'; 
					
					 ?>
                     </td>
                     <td width="" align="right"><? $expect_variance=$total_profit-$expected_profit; echo '-'; //number_format($tot_expected_varaince_data_vairance /$tot_expect_variance*100,2).'%'; ?></td>
                  </tr>
                <?
				$total_order_qty+=$row[csf('po_quantity')];
				$total_yarn_dyeing_cost+=$yarn_dyeing_cost;
				$total_yarn_cost+=$yarn_costing;
				$total_purchase_cost+=$fab_purchase;
				$total_knitting_cost+=$tot_knit_cost;
				$total_fabric_dyeing_cost+=$fabric_dyeing_cost;
				$total_heat_setting_cost+=$heat_setting_cost;
				$total_finishing_cost+=$fabric_finish;
				$total_washing_cost+=$washing_cost;
				$all_over_print_cost+=$all_over_cost;
				$total_trim_cost+=$tot_trim_amount;
				$total_commercial_cost+=$tot_commercial_cost;
				$total_fab_cost_amount=$total_yarn_cost+$total_purchase_cost+$total_knitting_cost+$total_yarn_dyeing_cost+$total_fabric_dyeing_cost+$total_heat_setting_cost+$total_finishing_cost+$total_washing_cost+$all_over_print_cost;
				$total_fab_cost_amount2+=$total_fab_cost_amount;
				$total_embelishment_cost+=$print_amount+$embroidery_amount+$special_amount+$other_amount;
				$total_commssion+=$foreign+$local;
				$total_testing_cost+=$tot_test_cost;
				$total_freight_cost+=$freight_cost;
				$total_cm_cost+=$cm_cost;
				$total_cost_up+=$total_cost;
				
				$total_inspection+=$inspection;
				$total_certificate_cost+=$certificate_cost;
				$total_common_oh+=$common_oh;
				$total_currier_cost+=$currier_cost;
				$total_fab_profit+=$total_profit;
				$total_expected_profit+=$expected_profit;
				$total_expt_profit_percentage+=$total_profit_percentage;
				$total_expect_variance+=$expect_variance;
				$total_profit_fab_percentage_up+=$total_profit_percentage2;
				//Pre Cost end;
				
				$total_p_yarn_dyeing_cost+=$p_yarn_dyeing_cost;
				$total_p_yarn_cost+=$p_yarn_costing;
				$total_p_purchase_cost+=$p_fab_purchase;
				$total_p_knitting_cost+=$tot_p_knit_cost;
				$total_p_fabric_dyeing_cost+=$p_fabric_dyeing_cost;
				$total_p_heat_setting_cost+=$p_heat_setting_cost;
				$total_p_finishing_cost+=$p_fabric_finish;
				$total_p_washing_cost+=$p_washing_cost;
				$p_all_over_print_cost+=$p_all_over_cost;
				$total_p_trim_cost+=$tot_p_trim_amount;
				$total_p_commercial_cost+=$tot_p_commercial_cost;
				
				$total_p_fab_cost_amount=$total_p_yarn_cost+$total_p_purchase_cost+$total_p_knitting_cost+$total_p_yarn_dyeing_cost+$total_p_fabric_dyeing_cost+$total_p_heat_setting_cost+$total_p_finishing_cost+$total_p_washing_cost+$p_all_over_print_cost;
				
				$total_fab_cost_amount2+=$total_fab_cost_amount;
				$total_p_embelishment_cost+=$p_print_amount+$p_embroidery_amount+$p_special_amount+$p_other_amount;
				$total_p_commssion+=$p_foreign+$p_local;
				$total_p_testing_cost+=$tot_p_test_cost;
				$total_p_freight_cost+=$p_freight_cost;
				$total_p_cm_cost+=$p_cm_cost;
				$total_p_cost_up+=$total_p_cost;
				
				$total_p_inspection+=$p_inspection;
				$total_p_certificate_cost+=$p_certificate_cost;
				$total_p_common_oh+=$p_common_oh;
				$total_p_currier_cost+=$p_currier_cost;
				$total_p_fab_profit+=$total_p_profit;
				$total_p_expected_profit+=$p_expected_profit;
				$total_p_expt_profit_percentage+=$total_p_profit_percentage;
				$total_p_expect_variance+=$p_expected_profit;
				
				$total_p_profit_fab_percentage_up+=$total_p_profit_percentage2;
				//Varaince Start here;
				$total_yarn_pp_variance+=$tot_yarn_variance;
				$total_yarncost_percent+=$tot_yarn_cost_percent;
				$total_fab_purchase_variance+=$p_fab_purchase-$fab_purchase;
				$total_knit_variance+=$tot_p_knit_cost-$tot_knit_cost;
				$total_yarn_dyeing_variance+=$p_yarn_dyeing_cost-$yarn_dyeing_cost ;
				$total_fab_dyeing_cost_variance+=$p_fabric_dyeing_cost-$fabric_dyeing_cost;
				$total_heatsetting_variance+=$p_heat_setting_cost-$heat_setting_cost;
				$total_fab_finish_varince+=$p_fabric_finish-$fabric_finish;
				$total_wash_cost_variance+=$p_washing_cost-$washing_cost;
				$total_all_over_variance+=$p_all_over_cost-$all_over_cost;
				$total_trim_variance+=$tot_p_trim_amount-$tot_trim_amount;
				$total_print_variance+=$p_print_amount-$print_amount;
				$total_embrod_variance+=$p_embroidery_amount-$embroidery_amount;
				$total_special_variance+=$p_special_amount-$special_amount;
				$total_other_variance+=$p_other_amount-$other_amount;
				$total_local_variance+=$p_local-$local;
				
				$total_commcercial_variance+=$tot_p_commercial_cost-$tot_commercial_cost;
				
				$total_foreign_variance+=$p_foreign-$foreign;
				$total_test_variance+=$tot_p_test_cost-$tot_test_cost;
				$total_freigt_variance+=$p_freight_cost-$freight_cost;
				$total_inspection_variance+=$p_inspection-$inspection;
				$total_certificate_variance+=$p_certificate_cost-$certificate_cost;
				$total_common_oh_variance+=$p_common_oh-$common_oh;
				$total_currier_variance+=$p_currier_cost-$currier_cost;
				$total_cm_variance_dzn+=$p_cm_cost_dzn-$cm_cost_dzn;
				$total_cm_cost_variance+=$p_cm_cost-$cm_cost;
				$total_cost_variance+=$total_order_amount-($total_p_cost-$total_cost);
				$total_profit_variance+=$total_profit-$total_p_profit;
				//echo $total_cost_variance;
				
				$total_p_profit_fab_percentage=$total_profit_variance/$total_p_fab_profit*100;
				$total_profit_variance_percent_variance=$total_profit_variance/$total_p_fab_profit*100;
				$total_expected_profit_variance+=$total_expected_profit-$total_p_expect_variance;
				$total_expected_profit_vari_varianace+=$tot_expected_varaince_data_vairance;
				

				
				 //Variance % start
				$total_v_fab_cost_varaince=$total_yarn_pp_variance+$total_fab_purchase_variance+$total_knit_variance+$total_yarn_dyeing_variance+
				$total_fab_dyeing_cost_variance+$total_heatsetting_variance+$total_fab_finish_varince+$total_wash_cost_variance+$total_all_over_variance;
				$total_v_embellish_cost_variance=$total_print_variance+$total_embrod_variance+$total_special_variance+$total_other_variance;
				$total_v_commission_variance=$total_foreign_variance+$total_local_variance;
				
								
				//$total_yarn_cost_variance_percent+=$p_yarn_costing/$yarn_costing*100;
				$total_purchase_variance_percent+=$tot_fab_purchase_variance/$p_fab_purchase*100;
				$total_knit_cost_variance_percent+=$tot_knit_cost_dzn_variance/$p_knit_cost_dzn*100;
				$total_yarn_dyeing_cost_variance_percent+=$tot_yarn_dyeing_cost_dzn_vairance/$p_yarn_dyeing_cost_dzn*100;
				
				$total_fab_variance_percent+=$tot_fabric_dyeing_variance/$p_fabric_dyeing_cost_dzn*100;
				$total_heat_variance_percent+=$tot_heat_variance_cost/$p_heat_setting_cost*100;
				$total_fab_finish_variance_percent+=$tot_fabric_finish_variance_cost/$p_fabric_finish*100;
				$total_wash_cost_variance_percent+=$tot_wash_cost_variance/$p_washing_cost*100;
				$total_all_over_variance_percent+=$tot_all_over_variance/$p_all_over_cost*100;
				$total_trim_variance_percent+=$tot_trim_cost_variance/$tot_p_trim_amount*100;
				$total_print_amount_variance_percent+=$tot_print_varinace/$p_print_amount*100;
				$total_embrodery_variance_percent+=$tot_embrodery_variance/$p_embroidery_amount*100;
				$total_special_variance_percent+=$tot_special_variance/$p_special_amount*100;
				$total_other_variance_percent+=$tot_other_amount_variance/$p_other_amount*100;
				$total_commcercial_variance_percent+=$tot_commercial_variance/$tot_p_commercial_cost*100;
				
				$total_foreign_variance_percent+=$tot_foreign_variance/$p_foreign*100;
//echo $total_foreign_variance_percent;
				$total_local_variance_percent+=$tot_local_variance/$p_local*100;
				
				$total_test_variance_percent+=$tot_test_variance/$tot_p_test_cost*100;
				$total_freight_variance_percent+=$tot_feight_variance/$p_freight_cost*100;
				$total_inspection_variance_percent+=$tot_inspection_variance/$p_inspection*100;
				$total_certificate_variance_percent+=$tot_certificate_variance/$p_certificate_cost*100;
				$total_common_variance_percent+=$tot_common_variance/$p_common_oh*100;
				$total_currier_variance_percent+=$tot_currier_variance/$p_currier_cost*100;
				
				$total_cm_cost_variance_percent+=$tot_cm_variance/$p_cm_cost*100;
				$total_cost_variance_percent+=$tot_total_cost_varaince/$total_p_cost*100;
				
				$total_profit_variance_percent+=$tot_profit_variance/$total_p_profit*100;
				$total_profit_fab_percentage_variance_percent+=$tot_profit_percient_varaince/$total_p_profit_percentage2*100;
				$tot_yarn_cost_vari_pernct+=$tot_yarn_cost_percent/$p_yarn_cost_percent*100;//$yarn_cost_variance_percent;
				$total_expected_profit_variance_percent_amount+=$tot_expected_profit_varaince/$p_expected_profit*100;
				$tot_expected_profit_variance+=$tot_expected_varaince_data_vairance /$tot_expect_variance*100;
				//echo $tot_expected_varaince/$p_expect_variance*100;

				$i++;
				}
               ?>
                </table>
                <table class="rpt_table" width="4670" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tfoot>
                     <th width="40"></th>
                     <th width="70"></th>
                     <th width="70"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="70">Price Quote</th>
                     <th width="90" align="right" id="total_order_qnty"><? echo number_format($total_order_qty,2); ?></th>
                     <th width="90"></th>
                     <th width="100" align="right" id="total_order_amount2"><? echo number_format($total_order_amount,2); ?></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="80" align="right" id="total_yarn_cost2"><? echo number_format($tot_yarn_cost_price,2); ?></th>
                     <th width="80" align="right" ><?  $total_p_yarn_cost_percentage=$tot_yarn_cost_price/$total_order_amount*100;  echo number_format($total_p_yarn_cost_percentage,2); ?></th>
                     <th width="100" align="right"><? echo number_format($total_p_purchase_cost,2); ?></th>
                     <th width="80"></th>
                     <th width="80" align="right"><? echo number_format($total_p_knitting_cost,2); ?></th>
                     <th width="100"></th>
                     <th width="110" align="right"><? echo number_format($total_p_yarn_dyeing_cost,2); ?></th>
                     <th width="120"><? ?></th>
                     <th width="100" align="right" id="total_fabric_dyeing_cost4"><? echo number_format($total_p_fabric_dyeing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_p_heat_setting_cost,2); ?></th>		
                     <th width="100" align="right"><? echo number_format($total_p_finishing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_p_washing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($p_all_over_print_cost,2); ?></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_trim_cost,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_p_print_amount,2); ?></strong></th>
                     <th width="85" align="right"><strong><? echo number_format($total_p_embroidery_amount,2); ?></strong></th>
                     <th width="80" align="right"><strong> <? echo number_format($total_p_special_amount,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_p_other_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_p_commercial_cost,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_p_foreign_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_p_local_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_test_cost_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_freight_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_p_inspection_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_certificate_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_common_oh_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_currier_amount,2); ?></strong></th>
                     <th width="120"></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_cm_amount,2); ?></strong></th>
                     <th width="100" id="total_cost_up" align="right"><strong><? echo number_format($total_p_cost_up,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_p_fab_profit,2);?></strong></th>
                     <th width="100" align="right"><strong><?  echo number_format($total_p_profit_fab_percentage,2);?></strong></th>
                     <th width="100"  align="right"><strong><? echo number_format($total_p_expected_profit,2);?></strong></th>
                     <th width=""  align="right"><strong><? echo number_format($total_p_expect_variance,2);?></strong></th>
                  </tfoot>
                </table>
                 <table class="rpt_table" width="4670" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tfoot>
                     <th width="40"></th>
                     <th width="70"></th>
                     <th width="70"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="70">Pre Cost</th>
                     <th width="90" align="right" id="total_order_qnty"><? echo number_format($total_order_qty,2); ?></th>
                     <th width="90"></th>
                     <th width="100" align="right" id="total_order_amount2"><? echo number_format($total_order_amount,2); ?></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="80" align="right" id="total_yarn_cost2"><? echo number_format($total_yarn_cost,2); ?></th>
                     <th width="80" align="right" ><?  $total_yarn_cost_percentage=$total_yarn_cost/$total_order_amount*100;  echo number_format($total_yarn_cost_percentage,2); ?></th>
                     <th width="100" align="right"><? echo number_format($total_purchase_cost,2); ?></th>
                     <th width="80"></th>
                     <th width="80" align="right"><? echo number_format($total_knitting_cost,2); ?></th>
                     <th width="100"></th>
                     <th width="110" align="right"><? echo number_format($total_yarn_dyeing_cost,2); ?></th>
                     <th width="120"><? ?></th>
                     <th width="100" align="right" id="total_fabric_dyeing_cost4"><? echo number_format($total_fabric_dyeing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_heat_setting_cost,2); ?></th>		
                     <th width="100" align="right"><? echo number_format($total_finishing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_washing_cost,2); ?></th>
                     <th width="90" align="right"><? echo number_format($all_over_print_cost,2); ?></th>
                     <th width="100" align="right"><strong><? echo number_format($total_trim_cost,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_print_amount,2); ?></strong></th>
                     <th width="85" align="right"><strong><? echo number_format($total_embroidery_amount,2); ?></strong></th>
                     <th width="80" align="right"><strong> <? echo number_format($total_special_amount,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_other_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_commercial_cost,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_foreign_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_local_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_test_cost_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_freight_amount,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_inspection_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_certificate_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_common_oh_amount,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_currier_amount,2); ?></strong></th>
                     <th width="120"></th>
                     <th width="100" align="right"><strong><? echo number_format($total_cm_cost,2); ?></strong></th>
                     <th width="100" id="total_cost_up" align="right"><strong><? echo number_format($total_cost_up,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_fab_profit,2);?></strong></th>
                     <th width="100" align="right"><strong><? $total_profit_fab_percentage=$total_fab_profit/$total_order_amount*100; echo number_format($total_profit_fab_percentage,2);?></strong></th>
                     <th width="100"  align="right"><strong><? echo number_format($total_expected_profit,2);?></strong></th>
                     <th width=""  align="right"><strong><? echo number_format($total_expect_variance,2);?></strong></th>
                  </tfoot>
                </table>
                 <table class="rpt_table" width="4670" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tfoot>
                     <th width="40"></th>
                     <th width="70"></th>
                     <th width="70"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="70"> Variance</th>
                     <th width="90" align="right" id="total_order_qnty"><? echo number_format($total_order_qty,2); ?></th>
                     <th width="90"></th>
                     <th width="100" align="right" id="total_order_amount2"><? echo number_format($total_order_amount,2); ?></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="80" align="right" id="total_yarn_cost2"><? echo number_format($total_yarn_pp_variance,2); ?></th>
                     <th width="80" align="right" ><?  $total_p_yarn_cost_percentage=$total_p_yarn_cost/$total_order_amount*100;  echo number_format($total_yarn_pp_variance/$total_order_amount*100,2); ?></th>
                     <th width="100" align="right"><? echo number_format($total_fab_purchase_variance,2); ?></th>
                     <th width="80"></th>
                     <th width="80" align="right"><? echo number_format($total_knit_variance,2); ?></th>
                     <th width="100"></th>
                     <th width="110" align="right"><? echo number_format($total_yarn_dyeing_variance,2); ?></th>
                     <th width="120"><? ?></th>
                     <th width="100" align="right" id="total_fabric_dyeing_cost4"><? echo number_format($total_fab_dyeing_cost_variance,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_heatsetting_variance,2); ?></th>		
                     <th width="100" align="right"><? echo number_format($total_fab_finish_varince,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_wash_cost_variance,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_all_over_variance,2); ?></th>
                     <th width="100" align="right"><strong><? echo number_format($total_trim_variance,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_print_variance,2); ?></strong></th>
                     <th width="85" align="right"><strong><? echo number_format($total_embrod_variance,2); ?></strong></th>
                     <th width="80" align="right"><strong> <? echo number_format($total_special_variance,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_other_variance,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_commcercial_variance,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_foreign_variance,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_local_variance,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_test_variance,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_freigt_variance,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_inspection_variance,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_certificate_variance,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_common_oh_variance,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_currier_variance,2); ?></strong></th>
                     <th width="120"></th>
                     <th width="100" align="right"><strong><? echo number_format($total_cm_cost_variance,2); ?></strong></th>
                     <th width="100" id="total_cost_up" align="right"><strong><? echo number_format($total_cost_variance,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_profit_variance,2);?></strong></th>
                     <th width="100" align="right"><strong><? $total_profit_fab_percentage_variance=$total_profit_variance/$total_order_amount*100; echo number_format($total_profit_fab_percentage_variance,2);?></strong></th>
                     <th width="100"  align="right"><strong><? echo number_format($total_expected_profit_variance,2);?></strong></th>
                     <th width=""  align="right"><strong><? echo number_format($total_expected_profit_vari_varianace ,2);?></strong></th>
                  </tfoot>
                </table>
                 <table class="rpt_table" width="4670" id="report_table_footer" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tfoot>
                     <th width="40"></th>
                     <th width="70"></th>
                     <th width="70"></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="110"></th>
                     <th width="70"> Variance %</th>
                     <th width="90" align="right" id="total_order_qnty"><? echo number_format($total_order_qty,2); ?></th>
                     <th width="90"></th>
                     <th width="100" align="right" id="total_order_amount2"><? echo number_format($total_order_amount,2); ?></th>
                     <th width="100"></th>
                     <th width="100"></th>
                     <th width="80" align="right" id="total_yarn_cost2"><? echo number_format($total_yarn_cost_variance_percent,2); ?></th>
                     <th width="80" align="right" ><?  echo number_format($tot_yarn_cost_vari_pernct,2); ?></th>
                     <th width="100" align="right"><? echo number_format($total_purchase_variance_percent,2); ?></th>
                     <th width="80"></th>
                    
                     <th width="80" align="right"><? echo number_format($total_knit_cost_variance_percent,2); ?></th>
                     <th width="100"></th>
                     <th width="110" align="right"><? echo number_format($total_yarn_dyeing_cost_variance_percent,2); ?></th>
                     <th width="120"><? ?></th>
                     <th width="100" align="right" id="total_fabric_dyeing_cost4"><? echo number_format($total_fab_variance_percent,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_heat_variance_percent,2); ?></th>		
                     <th width="100" align="right"><? echo number_format($total_fab_finish_variance_percent,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_wash_cost_variance_percent,2); ?></th>
                     <th width="90" align="right"><? echo number_format($total_all_over_variance_percent,2); ?></th>
                     <th width="100" align="right"><strong><? echo number_format($total_trim_variance_percent,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_print_amount_variance_percent,2); ?></strong></th>
                     <th width="85" align="right"><strong><? echo number_format($total_embrodery_variance_percent,2); ?></strong></th>
                     <th width="80" align="right"><strong> <? echo number_format($total_special_variance_percent,2); ?></strong></th>
                     <th width="80" align="right"><strong><? echo number_format($total_other_variance_percent,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_commcercial_variance_percent,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_foreign_variance_percent,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_local_variance_percent,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_test_variance_percent,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_freight_variance_percent,2); ?></strong></th>
                     <th width="120" align="right"><strong><? echo number_format($total_inspection_variance_percent,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_certificate_variance_percent,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_common_variance_percent,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_currier_variance_percent,2); ?></strong></th>
                     <th width="120"></th>
                     <th width="100" align="right"><strong><? echo number_format($total_cm_cost_variance_percent,2); ?></strong></th>
                     <th width="100" id="total_cost_up" align="right"><strong><? echo number_format($total_cost_variance_percent,2); ?></strong></th>
                     <th width="100" align="right"><strong><? echo number_format($total_profit_variance_percent,2);?></strong></th>
                     <th width="100" align="right"><strong><? $total_profit_fab_percentage_variance_percent=$total_profit_variance/$total_order_amount*100; echo number_format($total_profit_variance_percent,2);?></strong></th>
                     <th width="100"  align="right"><strong><? echo number_format($total_expected_profit_variance_percent_amount,2);?></strong></th>
                     <th width=""  align="right"><strong><? echo number_format($tot_expected_profit_variance,2);?></strong></th>
                  </tfoot>
                </table>
                <?
                $fab_percent=($total_fab_cost_amount*100)/$total_order_amount;
				$fab_percent=$fab_percent;
				$trim_percent=($total_trim_cost*100)/$total_order_amount;
				$trim_percent=$trim_percent;
				
				$embelishment_percent=($total_embelishment_cost*100)/$total_order_amount;
				$embelishment_percent=$embelishment_percent;
				$total_commercial_percent=($total_commercial_cost*100)/$total_order_amount;
				$total_commercial_percent=$total_commercial_percent;
				
				$total_commssion_percent=(($total_commssion*100)/$total_order_amount);
				$total_testing_cost_percent=(($total_testing_cost*100)/$total_order_amount);
				$total_freight_cost_percent=(($total_freight_cost*100)/$total_order_amount);
				$total_cost_percent=(($total_cost_up*100)/$total_order_amount);
				$total_cm_percent=(($total_cm_cost*100)/$total_order_amount);
				$total_order_amount_percent=(($total_order_amount*100)/$total_order_amount);
				
				$total_inspection_percent=(($total_inspection*100)/$total_order_amount);
				$total_certificate_percent=(($total_certificate_cost*100)/$total_order_amount);
				$total_common_oh_percent=(($total_common_oh*100)/$total_order_amount);
				$total_currier_cost_percent=(($total_currier_cost*100)/$total_order_amount);
				$all_tot_cost_percentage=$total_cost_up/$total_order_amount*100;
								
				
				$total_expected_profit_percent=(($total_expected_profit*100)/$total_order_amount);
				$total_expected_profit_variance_percent=(($total_expect_variance*100)/$total_order_amount);
				//Pre cost End
				
				$p_fab_percent=($total_p_fab_cost_amount*100)/$total_order_amount;
				$p_fab_percent=$p_fab_percent;
				$trim_p_percent=($total_p_trim_cost*100)/$total_order_amount;
				$trim_p_percent=$trim_p_percent;
				
				$p_embelishment_percent=($total_p_embelishment_cost*100)/$total_order_amount;
				$p_embelishment_percent=$p_embelishment_percent;
				$total_p_commercial_percent=($total_p_commercial_cost*100)/$total_order_amount;
				$total_p_commercial_percent=$total_p_commercial_percent;
				
				$total_p_commssion_percent=(($total_p_commssion*100)/$total_order_amount);
				$total_p_testing_cost_percent=(($total_p_testing_cost*100)/$total_order_amount);
				$total_p_freight_cost_percent=(($total_p_freight_cost*100)/$total_order_amount);
				$total_p_cost_percent=(($total_p_cost_up*100)/$total_order_amount);
				$total_p_cm_percent=(($total_p_cm_cost*100)/$total_order_amount);
				$total_p_order_amount_percent=(($total_order_amount*100)/$total_order_amount);
				
				$total_p_inspection_percent=(($total_p_inspection*100)/$total_order_amount);
				$total_p_certificate_percent=(($total_p_certificate_cost*100)/$total_order_amount);
				$total_p_common_oh_percent=(($total_p_common_oh*100)/$total_order_amount);
				$total_p_currier_cost_percent=(($total_p_currier_cost*100)/$total_order_amount);
				$p_all_tot_cost_percentage=$total_p_cost_up/$total_order_amount*100;
			
				$total_p_expected_profit_percent=(($total_p_expected_profit*100)/$total_order_amount);
				$total_p_expected_profit_variance_percent=(($total_p_expect_variance*100)/$total_order_amount);
				//Price Quotation End;
				//echo $total_v_fab_cost_varaince;
				$v_fab_percent=($total_v_fab_cost_varaince*100)/$total_order_amount;
				
				$v_trim_percent=($total_trim_variance*100)/$total_order_amount;
				
				
				$v_embelishment_percent=$total_v_embellish_cost_variance/$total_order_amount*100;
				
				$total_v_commercial_percent=$total_commcercial_variance/$total_order_amount*100;
				
				$total_v_commssion_percent=(($total_v_commission_variance*100)/$total_order_amount);
				$total_v_testing_cost_percent=(($total_test_variance*100)/$total_order_amount);
				$total_v_freight_cost_percent=(($total_freigt_variance*100)/$total_order_amount);
				$total_v_cost_percent=(($total_cost_variance*100)/$total_order_amount);
				$total_v_cm_percent=(($total_cm_cost_variance*100)/$total_order_amount);
				$total_v_order_amount_percent=(($total_order_amount*100)/$total_order_amount);
				
				$total_v_inspection_percent=(($total_inspection_variance*100)/$total_order_amount);
				$total_v_certificate_percent=(($total_certificate_variance*100)/$total_order_amount);
				$total_v_common_oh_percent=(($total_common_oh_variance*100)/$total_order_amount);
				$total_v_currier_cost_percent=(($total_currier_variance*100)/$total_order_amount);
				$v_all_tot_cost_percentage=$v_fab_percent+$v_trim_percent+$v_embelishment_percent+$total_v_commercial_percent+$total_v_commssion_percent+$total_v_testing_cost_percent+$total_v_freight_cost_percent+$total_v_cm_percent+$total_v_inspection_percent+$total_v_common_oh_percent+$total_v_currier_cost_percent;
			
				
				$total_v_expected_profit_percent=(($total_expected_profit_variance*100)/$total_order_amount);
				$total_v_expected_profit_variance_percent=(($tot_expected_profit_variance*100)/$total_p_expected_profit_variance_percent);
				//echo $total_expected_profit_variance;$tot_expected_profit_variance;
				
				?>
                  <input type="hidden" id="total_fab_cost" value="<? echo number_format($total_fab_cost_amount,2); ?>">
                  <input type="hidden" id="total_fab_percent" value="<? echo number_format($fab_percent,2)."%"; ?>">
                  <input type="hidden" id="total_trim_cost" value="<? echo number_format($total_trim_cost,2); ?>">
                  <input type="hidden" id="total_trim_percent" value="<? echo number_format($trim_percent,2)."%"; ?>">
                  <input type="hidden" id="total_embelishment_cost" value="<? echo number_format($total_embelishment_cost,2); ?>">
                  <input type="hidden" id="total_embelishment_percent" value="<? echo number_format($embelishment_percent,2)."%"; ?>">
                  <input type="hidden" id="total_commercial_cost" value="<? echo number_format($total_commercial_cost,2); ?>">
                  <input type="hidden" id="total_commercial_percent" value="<? echo number_format($total_commercial_percent,2)."%"; ?>">
                  <input type="hidden" id="total_commssion_cost" value="<? echo number_format($total_commssion,2); ?>">
                  <input type="hidden" id="total_commssion_percent" value="<? echo number_format($total_commssion_percent,2)."%"; ?>">
                  <input type="hidden" id="total_testing_cost" value="<? echo number_format($total_testing_cost,2); ?>">
                  <input type="hidden" id="total_testing_cost_percent" value="<? echo number_format($total_testing_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_freight_cost" value="<? echo number_format($total_freight_cost,2); ?>">
                  <input type="hidden" id="total_freight_cost_percent" value="<? echo number_format($total_freight_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_cost_up2" value="<? echo number_format($total_cost_up,2); ?>">
                  <input type="hidden" id="total_cost_percent" value="<? echo number_format($all_tot_cost_percentage,2)."%"; ?>">
                  <input type="hidden" id="total_cm_cost" value="<? echo number_format($total_cm_cost,2); ?>">
                  <input type="hidden" id="total_cm_percent" value="<? echo number_format($total_cm_percent,2)."%"; ?>">
                  <input type="hidden" id="total_order_amount" value="<? echo number_format($total_order_amount,2); ?>">
                  <input type="hidden" id="total_order_amount_percent" value="<? echo number_format($total_order_amount_percent,2)."%"; ?>">
                  <input type="hidden" id="total_inspection" value="<? echo number_format($total_inspection,2); ?>">
                  <input type="hidden" id="total_inspection_percent" value="<? echo number_format($total_inspection_percent,2)."%"; ?>">
                  <input type="hidden" id="total_certificate_cost" value="<? echo number_format($total_certificate_cost,2); ?>">
                  <input type="hidden" id="total_certificate_percent" value="<? echo number_format($total_certificate_percent,2)."%"; ?>">
                  <input type="hidden" id="total_common_oh" value="<? echo number_format($total_common_oh,2); ?>">
                  <input type="hidden" id="total_common_oh_percent" value="<? echo number_format($total_common_oh_percent,2)."%"; ?>">
                 
                  <input type="hidden" id="total_currier_cost" value="<? echo number_format($total_currier_cost,2); ?>">
                  <input type="hidden" id="total_currier_cost_percent" value="<? echo number_format($total_currier_cost_percent,2)."%"; ?>">
                  
                  <input type="hidden" id="total_fab_profit_id" value="<? echo number_format($total_fab_profit,2); ?>">
                  <input type="hidden" id="total_expected_profit_id" value="<? echo number_format($total_expected_profit,2); ?>">
                  <input type="hidden" id="total_expt_profit_variance_id" value="<? echo number_format($total_expect_variance,2); ?>">
                  
                   <input type="hidden" id="total_profit_fab_percentage_id" value="<? echo number_format($total_profit_fab_percentage,2)."%"; ?>">
                   <input type="hidden" id="total_expt_profit_percentage_id" value="<? echo number_format($total_expected_profit_percent,2)."%"; ?>">
                   <input type="hidden" id="total_expt_profit_variance_percentage_id" value="<? echo number_format($total_expected_profit_variance_percent,2)."%"; ?>">
                   <input type="hidden" id="expected_profit_percent" value="<? echo '('.$company_asking.'%'.')'; ?>">
                   <? //Pre cost end here ?>

                  <input type="hidden" id="total_p_fab_cost" value="<? echo number_format($total_p_fab_cost_amount,2); ?>">
                  <input type="hidden" id="total_p_fab_percent" value="<? echo number_format($p_fab_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_trim_cost" value="<? echo number_format($total_p_trim_cost,2); ?>">
                  <input type="hidden" id="total_p_trim_percent" value="<? echo number_format($trim_p_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_embelishment_cost" value="<? echo number_format($total_p_embelishment_cost,2); ?>">
                  <input type="hidden" id="total_p_embelishment_percent" value="<? echo number_format($p_embelishment_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_commercial_cost" value="<? echo number_format($total_p_commercial_cost,2); ?>">
                  <input type="hidden" id="total_p_commercial_percent" value="<? echo number_format($total_p_commercial_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_commssion_cost" value="<? echo number_format($total_p_commssion,2); ?>">
                  <input type="hidden" id="total_p_commssion_percent" value="<? echo number_format($total_p_commssion_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_testing_cost" value="<? echo number_format($total_p_testing_cost,2); ?>">
                  <input type="hidden" id="total_p_testing_cost_percent" value="<? echo number_format($total_p_testing_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_freight_cost" value="<? echo number_format($total_p_freight_cost,2); ?>">
                  <input type="hidden" id="total_p_freight_cost_percent" value="<? echo number_format($total_p_freight_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_cost_up2" value="<? echo number_format($total_p_cost_up,2); ?>">
                  <input type="hidden" id="total_p_cost_percent" value="<? echo number_format($p_all_tot_cost_percentage,2)."%"; ?>">
                  <input type="hidden" id="total_p_cm_cost" value="<? echo number_format($total_p_cm_cost,2); ?>">
                  <input type="hidden" id="total_p_cm_percent" value="<? echo number_format($total_p_cm_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_order_amount" value="<? echo number_format($total_order_amount,2); ?>">
                  <input type="hidden" id="total_p_order_amount_percent" value="<? echo number_format($total_p_order_amount_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_inspection" value="<? echo number_format($total_p_inspection,2); ?>">
                  <input type="hidden" id="total_p_inspection_percent" value="<? echo number_format($total_p_inspection_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_certificate_cost" value="<? echo number_format($total_p_certificate_cost,2); ?>">
                  <input type="hidden" id="total_p_certificate_percent" value="<? echo number_format($total_p_certificate_percent,2)."%"; ?>">
                  <input type="hidden" id="total_p_common_oh" value="<? echo number_format($total_p_common_oh,2); ?>">
                  <input type="hidden" id="total_p_common_oh_percent" value="<? echo number_format($total_p_common_oh_percent,2)."%"; ?>">
                 
                  <input type="hidden" id="total_p_currier_cost" value="<? echo number_format($total_p_currier_cost,2); ?>">
                  <input type="hidden" id="total_p_currier_cost_percent" value="<? echo number_format($total_p_currier_cost_percent,2)."%"; ?>">
                  
                  <input type="hidden" id="total_p_fab_profit_id" value="<? echo number_format($total_p_fab_profit,2); ?>">
                  <input type="hidden" id="total_p_expected_profit_id" value="<? echo number_format($total_p_expected_profit,2); ?>">
                  <input type="hidden" id="total_p_expt_profit_variance_id" value="<? echo number_format($total_p_expect_variance,2); ?>">
                  
                   <input type="hidden" id="total_p_profit_fab_percentage_id" value="<? echo number_format($total_p_profit_fab_percentage,2)."%"; ?>">
                   <input type="hidden" id="total_p_expt_profit_percentage_id" value="<? echo number_format($total_p_expected_profit_percent,2)."%"; ?>">
                   <input type="hidden" id="total_p_expt_profit_variance_percentage_id" value="<? echo number_format($total_p_expected_profit_variance_percent,2)."%"; ?>">
                   <input type="hidden" id="p_expected_profit_percent" value="<? echo '('.$company_asking.'%'.')'; ?>">
                   <? 
				  // Price Quotation End
				   ?>
                   <input type="hidden" id="total_v_fab_cost" value="<? echo number_format($total_v_fab_cost_varaince,2); ?>">
                  <input type="hidden" id="total_v_fab_percent" value="<? echo number_format($v_fab_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_trim_cost" value="<? echo number_format($total_trim_variance,2); ?>">
                  <input type="hidden" id="total_v_trim_percent" value="<? echo number_format($v_trim_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_embelishment_cost" value="<? echo number_format($total_v_embellish_cost_variance,2); ?>">
                  <input type="hidden" id="total_v_embelishment_percent" value="<? echo number_format($v_embelishment_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_commercial_cost" value="<? echo number_format($total_commcercial_variance,2); ?>">
                  <input type="hidden" id="total_v_commercial_percent" value="<? echo number_format($total_v_commercial_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_commssion_cost" value="<? echo number_format($total_v_commission_variance,2); ?>">
                  <input type="hidden" id="total_v_commssion_percent" value="<? echo number_format($total_v_commssion_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_testing_cost" value="<? echo number_format($total_test_variance,2); ?>">
                  <input type="hidden" id="total_v_testing_cost_percent" value="<? echo number_format($total_v_testing_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_freight_cost" value="<? echo number_format($total_freigt_variance,2); ?>">
                  <input type="hidden" id="total_v_freight_cost_percent" value="<? echo number_format($total_v_freight_cost_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_cost_up2" value="<? echo number_format($total_cost_variance,2); ?>">
                  <input type="hidden" id="total_v_cost_percent" value="<? echo number_format($v_all_tot_cost_percentage,2)."%"; ?>">
                  <input type="hidden" id="total_v_cm_cost" value="<? echo number_format($total_cm_cost_variance,2); ?>">
                  <input type="hidden" id="total_v_cm_percent" value="<? echo number_format($total_v_cm_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_order_amount" value="<? echo number_format($total_order_amount,2); ?>">
                  <input type="hidden" id="total_v_order_amount_percent" value="<? echo number_format($total_p_order_amount_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_inspection" value="<? echo number_format($total_inspection_variance,2); ?>">
                  <input type="hidden" id="total_v_inspection_percent" value="<? echo number_format($total_v_inspection_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_certificate_cost" value="<? echo number_format($total_certificate_variance,2); ?>">
                  <input type="hidden" id="total_v_certificate_percent" value="<? echo number_format($total_v_certificate_percent,2)."%"; ?>">
                  <input type="hidden" id="total_v_common_oh" value="<? echo number_format($total_common_oh_variance,2); ?>">
                  <input type="hidden" id="total_v_common_oh_percent" value="<? echo number_format($total_v_common_oh_percent,2)."%"; ?>">
                 
                  <input type="hidden" id="total_v_currier_cost" value="<? echo number_format($total_currier_variance,2); ?>">
                  <input type="hidden" id="total_v_currier_cost_percent" value="<? echo number_format($total_v_currier_cost_percent,2)."%"; ?>">
                  
                  <input type="hidden" id="total_v_fab_profit_id" value="<? echo number_format($total_profit_variance,2); ?>">
                  <input type="hidden" id="total_v_expected_profit_id" value="<? echo number_format($total_expected_profit_variance,2); ?>">
                  <input type="hidden" id="total_v_expt_profit_variance_id" value="<? echo number_format($total_expected_profit_vari_varianace,2); ?>">
                  
                   <input type="hidden" id="total_v_profit_fab_percentage_id" value="<? echo number_format($tot_expected_profit_variance,2)."%"; ?>">
                   <input type="hidden" id="total_v_expt_profit_percentage_id" value="<? echo number_format($total_v_expected_profit_percent,2)."%"; ?>">
                   <input type="hidden" id="total_v_expt_profit_variance_percentage_id" value="<? echo number_format($total_v_expected_profit_variance_percent,2)."%"; ?>">
                   <input type="hidden" id="v_expected_profit_percent" value="<? echo '('.$company_asking.'%'.')'; ?>">
                  
                  
            </div>
            <table>
                <tr>
                	<?
					$total_fab_cost=number_format($total_fab_cost_amount,2,'.','');
					$total_trim_cost=number_format($total_trim_cost,2,'.','');
					$total_embelishment_cost=number_format($total_embelishment_cost,2,'.','');
					$total_commercial_cost=number_format($total_commercial_cost,2,'.','');
					$total_commssion=number_format($total_commssion,2,'.','');
					$total_testing_cost=number_format($total_testing_cost,2,'.','');
					$total_freight_cost=number_format($total_freight_cost,2,'.','');
					$total_cost_up=number_format($total_cost_up,2,'.','');
					$total_cm_cost=number_format($total_cm_cost,2,'.','');
					$total_order_amount=number_format($total_order_amount,2,'.','');
					$total_inspection=number_format($total_inspection,2,'.','');
					$total_certificate_cost=number_format($total_certificate_cost,2,'.','');
					$total_common_oh=number_format($total_common_oh,2,'.','');
					$total_currier_cost=number_format($total_currier_cost,2,'.','');
					$total_fabric_profit_up=number_format($total_fab_profit,2,'.','');
					$total_expected_profit_up=number_format($total_expected_profit,2,'.','');
					//echo $total_fabric_profit_up;
					$chart_data_qnty="Fabric Cost;".$total_fab_cost."\nTrimCost;".$total_trim_cost."\nEmbelishment Cost;".$total_embelishment_cost."\nCommercial Cost;".$total_commercial_cost."\nCommission Cost;".$total_commssion."\nTesting Cost;".$total_testing_cost."\nFreightCost;".$total_freight_cost."\nCM Cost;".$total_cm_cost."\nOrder Value;".$total_order_amount."\nInspection Cost;".$total_inspection."\nCertificate Cost;".$total_certificate_cost."\nCommn OH Cost;".$total_common_oh."\nCurrier Cost;".$total_currier_cost."\n Profit/Loss;".$total_fabric_profit_up."\n";
					 
					?>
                    <input type="hidden" id="graph_data" value="<? //echo substr($chart_data_qnty,0,-1); ?>"/>
                    
                </tr>
            </table>
             <table>
                <tr><td height="15"></td></tr>
            </table>
           <a id="displayText" href="javascript:toggle();">Show Yarn Summary</a>
            <div style="width:600px; display:none" id="yarn_summary" >
            <div id="data_panel2" align="center" style="width:500px">
                 <input type="button" value="Print Preview" class="formbutton" style="width:100px" name="print" id="print" onClick="new_window(1)" />
            </div>

            <table width="500">
                    <tr class="form_caption">
                        <td colspan="6" align="center"><strong>Yarn Cost Summary As Per Pre-cost </strong></td>
                    </tr>
            </table>
            <table class="rpt_table" width="500" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <th width="30">SL</th>
                    <th width="80">Yarn Count</th>
                    <th width="120">Type</th>
                    <th width="120">Req. Qnty</th>
                    <th width="80">Avg. rate</th>
                    <th>Amount</th>
                </thead>
                <?
                $s=1; $tot_yarn_req_qnty=0; $tot_yarn_req_amnt=0;
                foreach($yarn_desc_array as $key=>$value)
                {
                    if($s%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                    $yarn_desc=explode("**",$key);
                    
                    $tot_yarn_req_qnty+=$yarn_desc_array[$key]['qnty']; 
                    $tot_yarn_req_amnt+=$yarn_desc_array[$key]['amnt'];
                ?>
                    <tr bgcolor="<? echo $bgcolor;?>" onClick="change_color('tr3_<? echo $s; ?>','<? echo $bgcolor; ?>')" id="tr3_<? echo $s;?>">
                        <td><? echo $s; ?></td>
                        <td align="center"><? echo $yarn_desc[0]; ?></td>
                        <td><? echo $yarn_desc[1]; ?></td>
                        <td align="right"><? echo number_format($yarn_desc_array[$key]['qnty'],2); ?></td>
                        <td align="right"><? echo number_format($yarn_desc_array[$key]['amnt']/$yarn_desc_array[$key]['qnty'],2); ?></td>
                        <td align="right"><? echo number_format($yarn_desc_array[$key]['amnt'],2); ?></td>
                    </tr>
                <?	
                $s++;
                }
                ?>
                <tfoot>
                    <th colspan="3" align="right">Total</th>
                    <th align="right"><? echo number_format($tot_yarn_req_qnty,2); ?></th>
                    <th align="right"><? echo number_format($tot_yarn_req_amnt/$tot_yarn_req_qnty,2); ?></th>
                    <th align="right"><? echo number_format($tot_yarn_req_amnt,2); ?></th>
                </tfoot>
        </table> 
        	</div>
		</fieldset>
	</div>
<?
			}
	}

echo "$total_data****$filename";
	exit();
}
if($action=="precost_yarn_detail")
{
	echo load_html_head_contents("Yarn Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:830px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
                             <tr> 
                                <td colspan="3" align="center"><strong>Yarn Cost Details</strong></td>
                            </tr>
                            <tr> 
                                <td width="150"><strong>Job No.:</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                            </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
                        <thead>
                            <th width="30">Sl</th>
                            <th width="70">Count</th>
                            <th width="80">Comp 1</th>
                            <th width="50">%</th>
                            <th width="80">Comp 2</th>
                            <th width="50">%</th>
                            <th width="80">Type</th>
                            <th width="80">GMTS Qty</th>
                            <th width="80">Cons Qnty/&nbsp; <? echo $costing_per_dzn; ?></th>
                            <th width="80">Yarn Req. Qty</th>
                            <th width="70">Yarn Rate</th>
                            <th width="80">Amount</th>
                        </thead>
                   
                <tbody>
                <?
					$i=1;
					$fabricArray=("select id, count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id,  cons_qnty, rate, amount,status_active from wo_pre_cost_fab_yarn_cost_dtls where job_no='$job_no'");
						$sql_result=sql_select($fabricArray);
					
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$req_qty=($row[csf('cons_qnty')]/$dzn_qnty)*$order_qty;
							//$total_amount=$req_qty*$row[csf('rate')];
							//$req_qty=$cost_per_qty*$order_qty;
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $yarn_count_library[$row[csf('count_id')]]; ?></p></td>
                            <td width="80" align="center"><p><? echo $composition[$row[csf('copm_one_id')]]; ?></p></td>
                            <td width="50" align="center"><p><? echo number_format($row[csf('percent_one')],2); ?></p></td>
                            <td width="80" align="center"><p><? echo $composition[$row[csf('copm_two_id')]]; ?></p></td>
                            <td width="50" align="center"><p><? echo $row[csf('percent_two')]; ?></p></td>
                            <td width="80" align="center"><p><? echo $yarn_type[$row[csf('type_id')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('cons_qnty')],4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($req_qty,2); ?></p></td>
                            <td width="70"  align="right"><p><? echo number_format($row[csf('rate')],2); ?></p></td>
                            <td width="80"  align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
                        
						<?
						$tot_qty+=$req_qty;
						$tot_amount_yarn+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                    <tfoot>
                        <tr class="tbl_bottom">
                            <td colspan="9" align="right">Total</td>
                            <td align="right"><? echo number_format($tot_qty,2); ?>&nbsp;</td>
                            <td>&nbsp; </td>
                            <td align="right"><? echo number_format($tot_amount_yarn,2); ?>&nbsp;</td>
                        </tr>
                        <tr class="tbl_bottom">
                            <td colspan="9" align="right">Avg.Yarn Rate</td>
                            <td colspan="1" align="right"> <? echo number_format($tot_amount_yarn/$tot_qty,2); ?></td>
                        </tr>
                    </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}// pre cost Yarn AVG end
if($action=="pricost_yarnavg_detail")
{
	echo load_html_head_contents("Yarn Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
   // $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
	
				$price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:830px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
                             <tr> 
                                <td colspan="3" align="center"><strong>Yarn Cost Details</strong></td>
                            </tr>
                            <tr> 
                                <td width="150"><strong>Job No.:</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                            </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
                        <thead>
                            <th width="30">Sl</th>
                            <th width="70">Count</th>
                            <th width="80">Comp 1</th>
                            <th width="50">%</th>
                            <th width="80">Comp 2</th>
                            <th width="50">%</th>
                            <th width="80">Type</th>
                            <th width="80">GMTS Qty</th>
                            <th width="80">Cons Qnty/&nbsp; <? echo $costing_per_dzn; ?></th>
                            <th width="80">Yarn Req. Qty</th>
                            <th width="70">Yarn Rate</th>
                            <th width="80">Amount</th>
                        </thead>
                   
                <tbody>
                <?
					$i=1;
					$fabricArray=("select id, count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id,  cons_qnty, rate, amount from wo_pri_quo_fab_yarn_cost_dtls where quotation_id='$quotation_id'");
						$sql_result=sql_select($fabricArray);
					
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$req_qty=($row[csf('cons_qnty')]/$dzn_qnty)*$order_qty;
							//$total_amount=$req_qty*$row[csf('rate')];
							//$req_qty=$cost_per_qty*$order_qty;
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $yarn_count_library[$row[csf('count_id')]]; ?></p></td>
                            <td width="80" align="center"><p><? echo $composition[$row[csf('copm_one_id')]]; ?></p></td>
                            <td width="50" align="center"><p><? echo number_format($row[csf('percent_one')],2); ?></p></td>
                            <td width="80" align="center"><p><? echo $composition[$row[csf('copm_two_id')]]; ?></p></td>
                            <td width="50" align="center"><p><? echo $row[csf('percent_two')]; ?></p></td>
                            <td width="80" align="center"><p><? echo $yarn_type[$row[csf('type_id')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('cons_qnty')],4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($req_qty,2); ?></p></td>
                            <td width="70"  align="right"><p><? echo number_format($row[csf('rate')],2); ?></p></td>
                            <td width="80"  align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
                        
						<?
						$tot_qty+=$req_qty;
						$tot_amount_yarn+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                    <tfoot>
                        <tr class="tbl_bottom">
                            <td colspan="9" align="right">Total</td>
                            <td align="right"><? echo number_format($tot_qty,2); ?>&nbsp;</td>
                            <td>&nbsp; </td>
                            <td align="right"><? echo number_format($tot_amount_yarn,2); ?>&nbsp;</td>
                        </tr>
                        <tr class="tbl_bottom">
                            <td colspan="10" align="right">Avg.Yarn Rate</td>
                            <td colspan="1" align="right"> <? echo number_format($tot_amount_yarn/$tot_qty,2); ?></td>
                             <td align="right"></td>
                        </tr>
                    </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
if($action=="fab_purchase_detail")
{
	echo load_html_head_contents("Purchase Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:830px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
                     <tr> 
                        <td colspan="3" align="center"><strong>Fabric Purchase Cost Details</strong></td>
                    </tr>
                    <tr> 
                        <td width="150"><strong>Job No.:</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                    </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="70">Body Part</th>
                    <th width="80">Fab. Nature</th>
                    <th width="100">Fab. Descrp.</th>
                    <th width="80">GMTS Qty.</th>
                    <th width="50">Source</th>
                    <th width="80">Cons Qty./<? echo $costing_per_dzn; ?></th>
                    <th width="80">Req. Qty.</th>
                    <th width="70">Rate</th>
                    <th width="80">Amount</th>
				</thead>
                <tbody>
                <?
					$i=1;
					$data_array=("select  a.body_part_id, a.fab_nature_id,a.fabric_source, a.fabric_description,  a.fabric_source, a.rate, a.amount,a.avg_finish_cons,b.cons,avg(b.requirment) as avg_cons   from wo_pre_cost_fabric_cost_dtls a,wo_pre_cos_fab_co_avg_con_dtls b where b.pre_cost_fabric_cost_dtls_id=a.id and a.job_no='$job_no' and a.job_no=b.job_no and b.po_break_down_id='$po_id'  and a.fabric_source=2 and a.status_active=1 and  a.is_deleted=0 group by a.id, a.job_no, a.item_number_id, a.body_part_id, a.fab_nature_id,a.consumption_basis,a.fabric_source, a.fabric_description, a.fabric_source, a.rate, a.amount,a.avg_finish_cons,b.cons");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							 $tot_avg=number_format($row[csf('avg_cons')],4);
							 //echo $tot_avg;
							$req_qty_avg=($tot_avg/$dzn_qnty)*$order_qty;
							//echo $req_qty_avg;
							$req_qty_p=$req_qty_avg/$dzn_qnty*$order_qty;
							$total_amount=$req_qty_avg*$row[csf('rate')];
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $body_part[$row[csf('body_part_id')]]; ?></p></td>
                            <td width="80" align="center"><p><? echo $item_category[$row[csf('fab_nature_id')]]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('fabric_description')]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="50" align="center"><p><?  
							if($row[csf('fabric_source')]==2) 
							{ 
							echo "Purchase";
							}
							else
							{
								echo "";
							}
							?>
                            </p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('avg_cons')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($req_qty_avg,2); ?></p></td>
                            <td width="70" align="right"><p><? echo number_format($row[csf('rate')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_qty+=$req_qty_avg;
						$tot_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="7" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
} //Pre Cost Purchase End
if($action=="fab_price_purchase_detail")
{
	echo load_html_head_contents("Purchase Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
  				$price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:830px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
                     <tr> 
                        <td colspan="3" align="center"><strong>Fabric Purchase Cost Details</strong></td>
                    </tr>
                    <tr> 
                        <td width="150"><strong>Job No.:</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                    </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="830" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="70">Body Part</th>
                    <th width="80">Fab. Nature</th>
                    <th width="100">Fab. Descrp.</th>
                    <th width="80">GMTS Qty.</th>
                    <th width="50">Source</th>
                    <th width="80">Cons Qty./<? echo $costing_per_dzn; ?></th>
                    <th width="80">Req. Qty.</th>
                    <th width="70">Rate</th>
                    <th width="80">Amount</th>
				</thead>
                <tbody>
                <?
					$i=1;
					
					$data_array=("select  a.body_part_id, a.fab_nature_id,a.fabric_source, a.fabric_description,  a.fabric_source, a.rate, a.amount,a.avg_finish_cons,b.cons,avg(b.requirment) as avg_cons   from wo_pri_quo_fabric_cost_dtls a,wo_pri_quo_fab_co_avg_con_dtls b where b.wo_pri_quo_fab_co_dtls_id=a.id and a.quotation_id='$quotation_id' and a.quotation_id=b.quotation_id  and a.fabric_source=2 and a.status_active=1 and  a.is_deleted=0 group by a.id,a.quotation_id, a.item_number_id, a.body_part_id, a.fab_nature_id,a.fabric_source, a.fabric_description, a.fabric_source, a.rate, a.amount,a.avg_finish_cons,b.cons");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							 $tot_avg=number_format($row[csf('avg_cons')],4);
							 //echo $tot_avg;
							$req_qty_avg=($tot_avg/$dzn_qnty)*$order_qty;
							//echo $req_qty_avg;
							$req_qty_p=$req_qty_avg/$dzn_qnty*$order_qty;
							$total_amount=$req_qty_avg*$row[csf('rate')];
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $body_part[$row[csf('body_part_id')]]; ?></p></td>
                            <td width="80" align="center"><p><? echo $item_category[$row[csf('fab_nature_id')]]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('fabric_description')]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="50" align="center"><p><?  
							if($row[csf('fabric_source')]==2) 
							{ 
							echo "Purchase";
							}
							else
							{
								echo "";
							}
							?>
                            </p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('avg_cons')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($req_qty_avg,2); ?></p></td>
                            <td width="70" align="right"><p><? echo number_format($row[csf('rate')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_qty+=$req_qty_avg;
						$tot_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="7" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}

if($action=="precost_knit_detail")
{
	echo load_html_head_contents("Knitting Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:630px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="600" cellpadding="0" cellspacing="0" align="center">
                    <tr> 
                    <td colspan="3" align="center"><strong> Knitting Cost Details</strong></td>
                    </tr>
                    <tr> 
                    <td width="150"><strong>Job No.:</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                    </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="600" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="70">Process</th>
                    <th width="80">GMTS Qty.</th>
                    <th width="100">Cons Qty./<? echo $costing_per_dzn; ?></th>
                    <th width="80">Req. Qty.</th>
                    <th width="50">Cost/Per Unit</th>
                    <th width="80">Amount</th>
				</thead>
                <tbody>
                <?
			  $i=1;
			  $data_array=("select cons_process,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pre_cost_fab_conv_cost_dtls where job_no='$job_no' and cons_process in(1,2,3,4) and status_active=1 and is_deleted=0 group by id,cons_process,req_qnty,charge_unit,amount");
			  $sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_cons_amount=$cons_qty*$order_qty;
							
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
							
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="center"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="100" align="center"><p><? echo number_format($cons_qty,4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_amount_knit+=$total_amount; 
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount_knit,2); ?>&nbsp;</td>
                    </tr>
                   
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}//Pre Cost Knit cost End
if($action=="pricost_knit_detail")
{
	echo load_html_head_contents("Knitting Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
   				 $price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
	?>
<fieldset style="width:630px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="600" cellpadding="0" cellspacing="0" align="center">
                    <tr> 
                    <td colspan="3" align="center"><strong> Knitting Cost Details</strong></td>
                    </tr>
                    <tr> 
                    <td width="150"><strong>Job No.:</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                    </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="600" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="70">Process</th>
                    <th width="80">GMTS Qty.</th>
                    <th width="100">Cons Qty./<? echo $costing_per_dzn; ?></th>
                    <th width="80">Req. Qty.</th>
                    <th width="50">Cost/Per Unit</th>
                    <th width="80">Amount</th>
				</thead>
                <tbody>
                <?
			  $i=1;
			  $data_array=("select cons_type,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pri_quo_fab_conv_cost_dtls where quotation_id='$quotation_id' and cons_type in(1,2,3,4) and status_active=1 and is_deleted=0 group by id,cons_type,req_qnty,charge_unit,amount");
			  $sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_cons_amount=$cons_qty*$order_qty;
							
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
							
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="center"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="100" align="center"><p><? echo number_format($cons_qty,4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_amount_knit+=$total_amount; 
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount_knit,2); ?>&nbsp;</td>
                    </tr>
                   
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}

if($action=="fab_dyeing_detail")
{
	echo load_html_head_contents("Fabrics Dyeing Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                        <tr> 
                        <td colspan="3" align="center"><strong> Fabric Dyeing Cost Details</strong></td>
                        </tr>
                        <tr> 
                        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                    <thead>
                        <th width="30">Sl</th>
                        <th width="70">Process</th>
                        <th width="80">GMTS Qty.</th>
                        <th width="100">Cons Qty./<? echo $costing_per_dzn; ?></th>
                        <th width="80">Req.Qty.</th>
                        <th width="50">Cost/Per Unit</th>
                        <th width="80">Amount</th>
                    </thead>
                <tbody>
                <?
					
			 $i=1;
			  $data_array=("select cons_process,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pre_cost_fab_conv_cost_dtls where job_no='$job_no' and cons_process in(25,31,32,60,61,62,63,72,80,81,84,85,86,87,38,74,78,79) and status_active=1 and is_deleted=0 group by id,cons_process,req_qnty,charge_unit,amount
");
			$sql_result=sql_select($data_array);
					
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_cons_amount=$cons_qty*$order_qty;
							
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							//$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($cons_qty,4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_amount_fab_dyeing+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount_fab_dyeing,2); ?>&nbsp;</td>
                    </tr>
                   
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}//Pre cost Fab dyeing cost details End

if($action=="fab_price_dyeing_detail")
{
	echo load_html_head_contents("Fabrics Dyeing Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
  				$price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                        <tr> 
                        <td colspan="3" align="center"><strong> Fabric Dyeing Cost Details</strong></td>
                        </tr>
                        <tr> 
                        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                    <thead>
                        <th width="30">Sl</th>
                        <th width="70">Process</th>
                        <th width="80">GMTS Qty.</th>
                        <th width="100">Cons Qty./<? echo $costing_per_dzn; ?></th>
                        <th width="80">Req.Qty.</th>
                        <th width="50">Cost/Per Unit</th>
                        <th width="80">Amount</th>
                    </thead>
                <tbody>
                <?
					
			 $i=1;
			  $data_array=("select cons_type,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from  wo_pri_quo_fab_conv_cost_dtls where quotation_id='$quotation_id' and cons_type in(25,31,32,60,61,62,63,72,80,81,84,85,86,87,38,74,78,79) and status_active=1 and is_deleted=0 group by id,cons_type,req_qnty,charge_unit,amount
");
			$sql_result=sql_select($data_array);
					
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_cons_amount=$cons_qty*$order_qty;
							
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							//$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($cons_qty,4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_amount_fab_dyeing+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount_fab_dyeing,2); ?>&nbsp;</td>
                    </tr>
                   
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}


if($action=="fab_finishing_detail")
{
	echo load_html_head_contents("Fabrics Finishing Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
+                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
         <tr> 
        <td colspan="3" align="center"><strong> Fabric Finishing Cost Details</strong></td>
        </tr>
        <tr> 
        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                    <thead>
                        <th width="30">Sl</th>
                        <th width="70">Process</th>
                        <th width="80">GMTS Qty.</th>
                        <th width="100">Cons Qty./<? echo $costing_per_dzn; ?></th>
                        <th width="80">Req.Qty.</th>
                        <th width="50">Cost/Per Unit</th>
                        <th width="80">Amount</th>
                    </thead>
                <tbody>
                <?
					
			 $i=1;
			 $data_array=("select cons_process,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pre_cost_fab_conv_cost_dtls where job_no='$job_no' and cons_process in(34,65,66,67,68,69,70,71,73,75,76,77,88,90,91,92,93,100,125,127,128,129) and status_active=1 and is_deleted=0 group by id,cons_process,req_qnty,charge_unit,amount ");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($cons_qty,4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($tot_cons_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_amount+=$tot_cons_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
} //Pre Cost Finish cost end

if($action=="fab_price_finishing_detail")
{
	echo load_html_head_contents("Fabrics Finishing Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
				$price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
         <tr> 
        <td colspan="3" align="center"><strong> Fabric Finishing Cost Details</strong></td>
        </tr>
        <tr> 
        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                    <thead>
                        <th width="30">Sl</th>
                        <th width="70">Process</th>
                        <th width="80">GMTS Qty.</th>
                        <th width="100">Cons Qty./<? echo $costing_per_dzn; ?></th>
                        <th width="80">Req.Qty.</th>
                        <th width="50">Cost/Per Unit</th>
                        <th width="80">Amount</th>
                    </thead>
                <tbody>
                <?
					
			 $i=1;
			$data_array=("select cons_type,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pri_quo_fab_conv_cost_dtls where quotation_id='$quotation_id' and cons_type in(34,65,66,67,68,69,70,71,73,75,76,77,88,90,91,92,93,100,125,127,128,129) and status_active=1 and is_deleted=0 group by id,cons_type,req_qnty,charge_unit,amount ");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($cons_qty,4); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($tot_cons_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_amount+=$tot_cons_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}

if($action=="fab_washing_detail")
{
	echo load_html_head_contents("Fabrics Finishing Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                    <tr> 
                    <td colspan="3" align="center"><strong> Fabric Washing Cost Details</strong></td>
                    </tr>
                    <tr> 
                    <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                    </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="70">Process</th>
                    <th width="80">GMTS Qty.</th>
                   
                    <th width="80">Req.Qty./<? echo $costing_per_dzn; ?></th>
                    <th width="50">Cost/Per Unit</th>
                    <th width="80">Amount</th>
				</thead>
                <tbody>
                <?
					
			 $i=1;
			  $data_array=("select cons_process,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pre_cost_fab_conv_cost_dtls where job_no='$job_no' and cons_process in(64,82,89) and status_active=1 and is_deleted=0 group by id,cons_process,req_qnty ,charge_unit,amount");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							//echo $dzn_qnty;
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                          
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_wash_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_wash_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
} //Pre Cost Wash details end
if($action=="fab_price_washing_detail")
{
	echo load_html_head_contents("Fabrics Finishing Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
				$price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                    <tr> 
                    <td colspan="3" align="center"><strong> Fabric Washing Cost Details</strong></td>
                    </tr>
                    <tr> 
                    <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
                    </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="70">Process</th>
                    <th width="80">GMTS Qty.</th>
                   
                    <th width="80">Req.Qty./<? echo $costing_per_dzn; ?></th>
                    <th width="50">Cost/Per Unit</th>
                    <th width="80">Amount</th>
				</thead>
                <tbody>
                <?
					
			 $i=1;
			  $data_array=("select cons_type ,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pri_quo_fab_conv_cost_dtls where quotation_id='$quotation_id' and cons_type in(64,82,89) and status_active=1 and is_deleted=0 group by id,cons_type ,req_qnty ,charge_unit,amount");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							//echo $dzn_qnty;
							$cons_qty=($row[csf('req_qnty')]/$dzn_qnty)*$row[csf('charge_unit')];
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                          
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$tot_wash_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($tot_wash_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
if($action=="fab_all_over_detail")
{
	echo load_html_head_contents("Fabrics All Over Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
         <tr> 
        <td colspan="3" align="center"><strong> Fabric All Over Print Cost Details</strong></td>
        </tr>
        <tr> 
        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                        <thead>
                            <th width="30">Sl</th>
                            <th width="70">Process</th>
                            <th width="80">GMTS Qty.</th>
                           
                            <th width="80">Req.Qty./<? echo $costing_per_dzn; ?></th>
                            <th width="50">Cost/Per Unit</th>
                            <th width="80">Amount</th>
                        </thead>
                <tbody>
                <?
			 $i=1;
			  $data_array=("select cons_process,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from wo_pre_cost_fab_conv_cost_dtls where job_no='$job_no' and cons_process in(35,36,37) and status_active=1 and is_deleted=0 group by id,cons_process,req_qnty,charge_unit,amount");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$total_all_over_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><? echo  number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($total_all_over_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
// Pre cost all over end
if($action=="fab_price_all_over_detail")
{
	echo load_html_head_contents("Fabrics All Over Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
				$price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
	?>
<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
         <tr> 
        <td colspan="3" align="center"><strong> Fabric All Over Print Cost Details</strong></td>
        </tr>
        <tr> 
        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
                        <thead>
                            <th width="30">Sl</th>
                            <th width="70">Process</th>
                            <th width="80">GMTS Qty.</th>
                           
                            <th width="80">Req.Qty./<? echo $costing_per_dzn; ?></th>
                            <th width="50">Cost/Per Unit</th>
                            <th width="80">Amount</th>
                        </thead>
                <tbody>
                <?
			 $i=1;
			  $data_array=("select cons_type ,req_qnty as req_qnty,charge_unit as charge_unit,amount as amount from  wo_pri_quo_fab_conv_cost_dtls where quotation_id='$quotation_id' and cons_type  in(35,36,37) and status_active=1 and is_deleted=0 group by id,cons_type ,req_qnty,charge_unit,amount");
					$sql_result=sql_select($data_array);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$tot_amount=$row[csf('amount')];
							$total_amount=($tot_amount/$dzn_qnty)*$order_qty;
							$tot_cons_amount=$cons_qty*$order_qty;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $conversion_cost_head_array[$row[csf('cons_process')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($order_qty,2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($row[csf('req_qnty')],2); ?></p></td>
                            <td width="50" align="right"><p><? echo number_format($row[csf('charge_unit')],2); ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_amount,2); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$row[csf('req_qnty')];
						$total_all_over_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><? echo  number_format($tot_req_qty,2); ?>&nbsp;</td>
                        <td>&nbsp; </td>
                        <td align="right"><? echo number_format($total_all_over_amount,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
?>
<?
if($action=="trim_cost_detail")
{
	echo load_html_head_contents("Trim Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	//print($order_qty);die;
    $costing_per=return_field_value("costing_per as costing_per", "wo_pre_cost_mst", "job_no='$job_no'","costing_per");
						if($costing_per==1)
						{
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per==3)
						{
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per==4)
						{
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per==5)
						{
							$costing_per_dzn="4 Dzn";
						}
						
	$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
	?>
<fieldset style="width:770px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="750" cellpadding="0" cellspacing="0" align="center">
         <tr> 
        <td colspan="6" align="center"><strong> Trim Cost Details</strong></td>
        </tr>
        <tr> 
        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="750" cellpadding="0" cellspacing="0" align="center">
                        <thead>
                            <th width="30">Sl</th>
                            <th width="70">Item Name</th>
                            <th width="80">Description.</th>
                           
                            <th width="80">Brand/Supplier Ref</th>
                            <th width="80">UOM</th>
                            <th width="70">Cons Rer Uni/<? echo $costing_per_dzn;?></th>
                            <th width="80">Req. Qty</th>
                            <th width="70">Rate Per Unit</th>
                            <th width="80">Amount</th>
                          
                        </thead>
                <tbody>
                <?
			 $i=1;
			$trimsArray=("select  b.po_break_down_id,a.trim_group,a.description, a.cons_dzn_gmts,a.cons_uom, a.brand_sup_ref,a.amount, a.rate 
	from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b 
	where a.id=b.wo_pre_cost_trim_cost_dtls_id and a.job_no=b.job_no  and a.job_no='$job_no' and b.po_break_down_id=$po_id and a.status_active=1 and  a.is_deleted=0 group by   b.po_break_down_id,a.trim_group,a.description, a.cons_dzn_gmts,a.cons_uom, a.brand_sup_ref,a.amount, a.rate  ");
					$sql_result=sql_select($trimsArray);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$tot_amount=$row[csf('cons_dzn_gmts')];
							$total_reg=($order_qty/$dzn_qnty)*$tot_amount;
							$tot_cons_amount=$row[csf('rate')]*$total_reg;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $item_library[$row[csf('trim_group')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo $row[csf('description')]; ?></p></td>
                            <td width="80" align="right"><p><? echo $row[csf('brand_sup_ref')]; ?></p></td>
                            <td width="80" align="right"><p><? echo $unit_of_measurement[$row[csf('cons_uom')]]; ?></p></td>
                            <td width="70" align="right"><p><? echo $row[csf('cons_dzn_gmts')]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_reg,4); ?></p></td>
                            <td width="70" align="right"><p><? echo $row[csf('rate')]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($tot_cons_amount,4); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$tot_cons_amount;
						$total_all_over_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="8" align="right">Total</td>
                        <td align="right"><? echo  number_format($tot_req_qty,2); ?>&nbsp;</td>
                        
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}// Pre cost Trim End

if($action=="trim_cost_price_detail")
{
	echo load_html_head_contents("Trim Cost Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $job_no;die;
	$order_qty=return_field_value("sum(b.po_quantity) as po_quantity", "wo_po_break_down b,wo_po_details_master a ", "a.job_no=b.job_no_mst and b.job_no_mst='$job_no' and  a.company_name='$company_id' and b.id=$po_id","po_quantity");
	$price_costDataArray=sql_select("select  id,costing_per  from wo_price_quotation where status_active=1 and is_deleted=0  ");
				$price_costing_perArray=array();
					foreach($price_costDataArray as $pri_fabRow)
					{
					 $price_costing_perArray[$pri_fabRow[csf('id')]]['costing_per']=$pri_fabRow[csf('costing_per')];
					
					}
					$costing_per_price=$price_costing_perArray[$quotation_id]['costing_per'];
					//echo $costing_per_price;
						if($costing_per_price==1)
						{
							 $dzn_qnty=12;
							$costing_per_dzn="1 Dzn";
						}
						else if($costing_per_price==2)
						{
							$costing_per_dzn="1 Pcs";
						}
						else if($costing_per_price==3)
						{
							$dzn_qnty=12*2;
							$costing_per_dzn="2 Dzn";
						}
						else if($costing_per_price==4)
						{
							$dzn_qnty=12*3;
							$costing_per_dzn="3 Dzn";
						}
						else if($costing_per_price==5)
						{
							$dzn_qnty=12*4;
							$costing_per_dzn="4 Dzn";
						}
						                        
                       else
                        {
                            $dzn_qnty=1;
                        }
	?>
<fieldset style="width:770px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table  border="1" class="rpt_table" rules="all" width="750" cellpadding="0" cellspacing="0" align="center">
         <tr> 
        <td colspan="6" align="center"><strong> Price Trim Cost Details</strong></td>
        </tr>
        <tr> 
        <td width="150"><strong>Job No.</strong>&nbsp; <? echo $job_no; ?> </td><td  width="150"><strong>Order: </strong>&nbsp; <? echo $order_arr[$po_id];  ?></td><td  width="150"><strong>Buyer:</strong> &nbsp; <? echo $buyer_library[$buyer_id]; ?></td>
        </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="750" cellpadding="0" cellspacing="0" align="center">
                        <thead>
                            <th width="30">Sl</th>
                            <th width="70">Item Name</th>
                            
                            <th width="80">UOM</th>
                            <th width="70">Cons Rer Uni/<? echo $costing_per_dzn;?></th>
                            <th width="80">Req. Qty</th>
                            <th width="70">Rate Per Unit</th>
                            <th width="80">Amount</th>
                          
                        </thead>
                <tbody>
                <?
			 $i=1;
		$trimsArray=("select  a.trim_group, a.cons_dzn_gmts,a.cons_uom, a.amount, a.rate 
	from wo_pri_quo_trim_cost_dtls a
	where   a.quotation_id='$quotation_id' and a.status_active=1 and  a.is_deleted=0 group by  a.trim_group,a.cons_dzn_gmts,a.cons_uom,a.amount, a.rate  ");
					$sql_result=sql_select($trimsArray);
					foreach($sql_result as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
							$tot_amount=$row[csf('cons_dzn_gmts')];
							$total_reg=($order_qty/$dzn_qnty)*$tot_amount;
							$tot_cons_amount=$row[csf('rate')]*$total_reg;
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="70" align="center"><p><? echo $item_library[$row[csf('trim_group')]]; ?></p></td>
                         
                            <td width="80" align="right"><p><? echo $unit_of_measurement[$row[csf('cons_uom')]]; ?></p></td>
                            <td width="70" align="right"><p><? echo $row[csf('cons_dzn_gmts')]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($total_reg,4); ?></p></td>
                            <td width="70" align="right"><p><? echo $row[csf('rate')]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($tot_cons_amount,4); ?></p></td>
                        </tr>
						<?
						$tot_req_qty+=$tot_cons_amount;
						$total_all_over_amount+=$total_amount;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="6" align="right">Total</td>
                        <td align="right"><? echo  number_format($tot_req_qty,2); ?>&nbsp;</td>
                        
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
if($action=="budget_precostbomRpt")
{
	echo load_html_head_contents("Budget Pre Cost BOM Report", "../../../../", 1, 1,'','','');
	//extract($_REQUEST);
	//echo $job_no;die;
	
	///extract($_REQUEST);
	
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$txt_costing_date=change_date_format(str_replace("'","",$txt_costing_date),'yyyy-mm-dd','-');
	//if($job_no=="") $job_no=''; else $job_no=" and a.job_no=".$job_no."";
	//$job_num=str_replace("'","",$job_no);
	//$gmt_item_id=str_replace("'","",$gmt_item_id);
	if($po_id=="") $order_no=''; else $order_no=" and d.id=".$po_id."";
	if($company_id=="") $company_name=''; else $company_name=" and a.company_name=".$company_id."";
	if($buyer_id=="") $cbo_buyer_name=''; else $cbo_buyer_name=" and a.buyer_name=".$buyer_id."";
	if($txt_style_ref=="") $txt_style_ref=''; else $txt_style_ref=" and a.style_ref_no=".$txt_style_ref."";
	if($txt_costing_date=="") $txt_costing_date=''; else $txt_costing_date=" and b.costing_date='".$txt_costing_date."'";
 	
 	
	//array for display name
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	//echo $gmt_item_id;die;
	
	if($db_type==0)
	{	
	  $sql = "select a.job_no,a.company_name, a.buyer_name,a.style_ref_no, a.gmts_item_id,a.order_uom, a.avg_unit_price, b.costing_per, c.fab_knit_req_kg, c.fab_woven_req_yds, c.fab_yarn_req_kg,sum(d.plan_cut) as job_quantity,sum(d.po_quantity) as ord_qty
			from wo_po_details_master a, wo_pre_cost_mst b,wo_pre_cost_sum_dtls c,wo_po_break_down d
			where a.job_no=b.job_no and b.job_no=c.job_no and c.job_no=d.job_no_mst and a.status_active=1 and a.is_deleted =0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $order_no $company_name $cbo_buyer_name $txt_style_ref  group by a.job_no,a.company_name, a.buyer_name,a.style_ref_no, a.gmts_item_id,a.order_uom, a.avg_unit_price, b.costing_per, c.fab_knit_req_kg, c.fab_woven_req_yds, c.fab_yarn_req_kg order by a.job_no"; 
	}
	if($db_type==2)
	{	
	  $sql = "select a.job_no,a.company_name, a.buyer_name,a.style_ref_no, a.gmts_item_id,a.order_uom, a.avg_unit_price, b.costing_per, c.fab_knit_req_kg, c.fab_woven_req_yds, c.fab_yarn_req_kg,sum(d.plan_cut) as job_quantity,sum(d.po_quantity) as ord_qty
			from wo_po_details_master a, wo_pre_cost_mst b,wo_pre_cost_sum_dtls c,wo_po_break_down d
			where a.job_no=b.job_no and b.job_no=c.job_no and c.job_no=d.job_no_mst and a.status_active=1 and a.is_deleted =0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $order_no $company_name $cbo_buyer_name $txt_style_ref group by a.job_no,a.company_name, a.buyer_name,a.style_ref_no, a.gmts_item_id,a.order_uom, a.avg_unit_price, b.costing_per, c.fab_knit_req_kg, c.fab_woven_req_yds, c.fab_yarn_req_kg  order by a.job_no"; //a.job_quantity as job_quantity,
	}
	//echo $sql;die;
	$data_array=sql_select($sql);
	
	
	?>
    <div style="width:850px; font-size:20px; font-weight:bold" align="center"><? echo $comp[str_replace("'","",$cbo_company_name)]; ?></div>
    <div style="width:850px; font-size:14px; font-weight:bold" align="center">Budget BOM Report</div>
	<?
	foreach ($data_array as $row)
	{	
		$order_price_per_dzn=0;
		$order_job_qnty=0;
		$ord_qty=0;
		$avg_unit_price=0;
		$order_values = $row[csf("ord_qty")]*$row[csf("avg_unit_price")];
		$result =sql_select("select po_number,pub_shipment_date from wo_po_break_down where id=$po_id and status_active=1 and is_deleted=0 order by pub_shipment_date DESC");
		$job_in_orders = '';$pulich_ship_date='';
		foreach ($result as $val)
		{
			$job_in_orders .= $val[csf('po_number')].", ";
			$pulich_ship_date = $val[csf('pub_shipment_date')];
		}
		$job_in_orders = substr(trim($job_in_orders),0,-1);
		?>
            	<table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px" rules="all">
                	<tr>
                    	<td width="80">Job Number</td>
                        <td width="80"><b><? echo $row[csf("job_no")]; ?></b></td>
                        <td width="90">Buyer</td>
                        <td width="100"><b><? echo $buyer_arr[$row[csf("buyer_name")]]; ?></b></td>
                        <td width="80">Plan Cut Qnty</td>
                        <? 
							$grmnt_items = "";
							if($garments_item[$row[csf("gmts_item_id")]]=="")
							{
								
								$grmts_sql = sql_select("select job_no,gmts_item_id,set_item_ratio from wo_po_details_mas_set_details where job_no=$job_no");
								foreach($grmts_sql as $key=>$val){
									$grmnt_items .=$garments_item[$val[csf("gmts_item_id")]].", ";
								}
								$grmnt_items = substr_replace($grmnt_items,"",-1,1);
							}else{
								$grmnt_items = $garments_item[$row[csf("gmts_item_id")]];
							}
							
						?>
                        <td width="100"><b><? echo $row[csf("job_quantity")]." ".$unit_of_measurement[$row[csf("order_uom")]]; ?></b></td>
                    </tr>
                    <tr>
                        <td>Order Qty </td>
                        <td><b><? echo $row[csf("ord_qty")]." ".$unit_of_measurement[$row[csf("order_uom")]]; ?></b></td>
                    	<td>Style Ref. No </td>
                        <td><b><? echo $row[csf("style_ref_no")]; ?></b></td>
                       
                        <td>Price Per Unit</td>
                        <td><b><? echo $row[csf("avg_unit_price")]; ?></b></td>
                    </tr>
                    <tr>
                    	<td>Order Numbers</td>
                        <td colspan="5"><? echo $job_in_orders; ?></td>
                    </tr>
                    <tr>
                    	<td>Garments Item</td>
                        <td colspan="3"><b><? echo $grmnt_items; ?></b></td>
                        <td>Shipment Date </td>
                        <td><b><? echo change_date_format($pulich_ship_date); ?></b></td>
                    </tr>
                    
                </table>

            <?	
			
			if($row[csf("costing_per")]==1){$order_price_per_dzn=12;$costing_for=" DZN";}
			else if($row[csf("costing_per")]==2){$order_price_per_dzn=1;$costing_for=" PCS";}
			else if($row[csf("costing_per")]==3){$order_price_per_dzn=24;$costing_for=" 2 DZN";}
			else if($row[csf("costing_per")]==4){$order_price_per_dzn=36;$costing_for=" 3 DZN";}
			else if($row[csf("costing_per")]==5){$order_price_per_dzn=48;$costing_for=" 4 DZN";}
			$order_job_qnty=$row[csf("job_quantity")];
			$avg_unit_price=$row[csf("avg_unit_price")];
			$ord_qty=$row[csf("ord_qty")];
			
	}//end first foearch
	
	
	
	
	//2 Fabric Cost part here------------------------------------------- 	   	
	$sql = "select id, job_no,item_number_id, body_part_id, fab_nature_id, color_type_id, fabric_description, avg_cons, fabric_source, rate, amount,avg_finish_cons,status_active   from wo_pre_cost_fabric_cost_dtls 
			where job_no=".$job_no."";
	$data_array=sql_select($sql);
		
		$knit_fab="";$woven_fab=""; 
		$knit_subtotal_amount=0;
		$woven_subtotal_amount=0;
        $i=2;$j=2;
		foreach( $data_array as $row )
        {
			    $set_item_ratio=return_field_value("set_item_ratio"," wo_po_details_mas_set_details", "job_no='".$row[csf('job_no')]."' and gmts_item_id='".$row[csf('item_number_id')]."'");
			// echo  $set_item_ratio.'<br>';
			   $fincons=0;
			   $greycons=0;
			   $order_qty_fab=0;
			  // echo "select po_break_down_id,cons,requirment from wo_pre_cos_fab_co_avg_con_dtls where pre_cost_fabric_cost_dtls_id=".$row[csf("id")]." and cons !=0";
			  	  
					 $fab_dtls_data=sql_select("select po_break_down_id,color_number_id,gmts_sizes,cons,requirment from wo_pre_cos_fab_co_avg_con_dtls where pre_cost_fabric_cost_dtls_id=".$row[csf("id")]." and cons !=0");
				   foreach($fab_dtls_data as $fab_dtls_data_row )
				   {
						 $sql_po_qty_fab=sql_select("select sum(c.plan_cut_qnty) as order_quantity,(sum(c.order_quantity)/a.total_set_qnty) as order_quantity_set  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id and  b.id=".$fab_dtls_data_row[csf('po_break_down_id')]." and item_number_id='".$row[csf('item_number_id')]."' and size_number_id='".$fab_dtls_data_row[csf('gmts_sizes')]."' and  color_number_id= '".$fab_dtls_data_row[csf('color_number_id')]."'  and a.status_active=1 and b.status_active=1 and c.status_active=1");
						 
						 list($sql_po_qty_row_fab)=$sql_po_qty_fab;
						 $po_qty_fab=$sql_po_qty_row_fab[csf('order_quantity')];
						 $fincons+=($po_qty_fab/($order_price_per_dzn*$set_item_ratio))*$fab_dtls_data_row[csf("cons")];
						 $greycons+=($po_qty_fab/($order_price_per_dzn*$set_item_ratio))*$fab_dtls_data_row[csf("requirment")];
						 $order_qty_fab+=$po_qty_fab;
				   }  
			 
			//$row[csf("avg_cons")] = $greycons;
			//$row[csf("avg_finish_cons")] = $fincons;
 			//$row[csf("amount")] = ($row[csf("amount")]/($order_price_per_dzn*$set_item_ratio))*($order_qty_fab*$set_item_ratio);
			
			//$row[csf("avg_cons")] = ($row[csf("avg_cons")]/($order_price_per_dzn*$set_item_ratio))*($order_job_qnty*$set_item_ratio);
			//$row[csf("avg_finish_cons")] = ($row[csf("avg_finish_cons")]/($order_price_per_dzn*$set_item_ratio))*($order_job_qnty*$set_item_ratio);
 			//$row[csf("amount")] = ($row[csf("amount")]/($order_price_per_dzn*$set_item_ratio))*($order_job_qnty*$set_item_ratio);
				
			if($row[csf("fab_nature_id")]==2)//knit fabrics
			{
				$item_descrition = $body_part[$row[csf("body_part_id")]].", ".$color_type[$row[csf("color_type_id")]].", ".$row[csf("fabric_description")];
				
 				$i++;	
                $knit_fab .= '<tr>
                    <td align="left">'.$item_descrition.'</td>
                    <td align="left">'.$fabric_source[$row[csf("fabric_source")]].'</td>
                    <td align="right">'.number_format( $greycons,4).'</td>
 					<td align="right">'.number_format( $fincons,4).'</td>
                    <td align="right">'.number_format($row[csf("rate")],4).'</td>
                    <td align="right">'.number_format($row[csf("amount")],4).'</td>  
                </tr>';	
				$knit_subtotal_avg_cons+=$row[csf("avg_cons")];
				$knit_subtotal_avg_finish_cons+=$row[csf("avg_finish_cons")];
            	$knit_subtotal_amount+=$row[csf("amount")];
			}			
			if($row[csf("fab_nature_id")]==3)//woven fabrics
			{
				$item_descrition = $body_part[$row[csf("body_part_id")]].", ".$color_type[$row[csf("color_type_id")]].", ".$row[csf("fabric_description")];
				$j++;
                 $woven_fab .= '<tr>
                    <td align="left">'.$item_descrition.'</td>
                    <td align="left">'.$fabric_source[$row[csf("fabric_source")]].'</td>
                    <td align="right">'.number_format($row[csf("avg_cons")],4).'</td>
 					<td align="right">'.number_format($row[csf("avg_finish_cons")],4).'</td>
                    <td align="right">'.number_format($row[csf("rate")],4).'</td>
                    <td align="right">'.number_format($row[csf("amount")],4).'</td>  
                </tr>';	
				$woven_subtotal_avg_cons+=$row[csf("avg_cons")];
				$woven_subtotal_avg_finish_cons+=$row[csf("avg_finish_cons")];
				$woven_subtotal_amount+=$row[csf("amount")];
			}
        }	
	 
		$knit_fab= '<div style="margin-top:15px">
				<table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px;text-align:center;" rules="all">					
						<tr style="font-weight:bold"  align="center">
							<td width="80" rowspan="'.$i.'" ><div class="verticalText"><b>Knit Fabric</b></div></td>
							<td width="300">Description</td>
							<td width="100">Source</td>
							<td width="100">Gray Fabric Qnty</td>	
							<td width="100">Finish Fab Qnty</td>
 							<td width="50">Rate</td>
							<td width="50">Amount</td>
						</tr>'.$knit_fab;
		$woven_fab = '<tr><td colspan="7">&nbsp;</td></tr><tr>
						<td width="80" rowspan="'.$j.'"><div class="verticalText"><b>Woven Fabric</b></div></td></tr>'.$woven_fab;	
						
		//knit fabrics table here 
		$knit_fab .='<tr class="rpt_bottom" style="font-weight:bold">
						<td colspan="2">Total</td>
						<td align="right">'.number_format($knit_subtotal_avg_cons,4).'</td>
						<td align="right">'.number_format($knit_subtotal_avg_finish_cons,4).'</td>
						<td align="right"></td>
						<td align="right">'.number_format($knit_subtotal_amount,4).'</td>
					</tr>';
  		echo $knit_fab;
		
		//woven fabrics table here 
		$woven_fab .='<tr class="rpt_bottom" style="font-weight:bold">
						<td colspan="2">Total</td>
						<td align="right">'.number_format($woven_subtotal_avg_cons,4).'</td>
						<td align="right">'.number_format($woven_subtotal_avg_finish_cons,4).'</td>
						<td align="right"></td>
						<td align="right">'.number_format($woven_subtotal_amount,4).'</td>
					</tr>
   					</table></div>';
        echo $woven_fab;           		
  		
		//end 	All Fabric Cost part report-------------------------------------------
  	
	
		//Start	Yarn Cost part report here -------------------------------------------
		$lib_yarn_count=return_library_array( "select yarn_count,id from lib_yarn_count", "id", "yarn_count"  );
		//Mysql
		/*$sql = "select id, count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id, cons_ratio, sum(cons_qnty) as cons_qnty, rate, sum(amount) as amount from wo_pre_cost_fab_yarn_cost_dtls where job_no=".$txt_job_no." group by count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id, rate";*/
				//oracle 
				$sql = "select min(id) as id, count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id, min(cons_ratio) as cons_ratio, sum(cons_qnty) as cons_qnty, rate, sum(amount) as amount from wo_pre_cost_fab_yarn_cost_dtls where job_no=".$job_no." group by count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id, rate";
		$data_array=sql_select($sql); 
		?>
        <div style="margin-top:15px">
        	<table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px;text-align:center;" rules="all">
                <tr style="font-weight:bold">
                    <td width="70" rowspan="<? echo count($data_array)+2; ?>"><div class="verticalText"><b>Yarn Cost</b></div></td>
                    <td width="350">Yarn Desc</td>
                    <td width="100">Yarn Qnty</td>
                    <td width="100">Rate</td>
                    <td width="100">Amount</td>
                </tr>
			<?
            $total_yarn_amount = 0;
			foreach( $data_array as $row )
            { 
				if($row[csf("percent_one")]==100)
					$item_descrition = $lib_yarn_count[$row[csf("count_id")]]." ".$composition[$row[csf("copm_one_id")]]." ".$row[csf("percent_one")]."% ".$yarn_type[$row[csf("type_id")]];
            	else
					$item_descrition = $lib_yarn_count[$row[csf("count_id")]]." ".$composition[$row[csf("copm_one_id")]]." ".$row[csf("percent_one")]."% ".$composition[$row[csf("copm_two_id")]]." ".$row[csf("percent_two")]."% ".$yarn_type[$row[csf("type_id")]];
					
 				$row[csf("cons_qnty")] = $row[csf("cons_qnty")]/$order_price_per_dzn*$order_job_qnty;
				$row[csf("amount")] = $row[csf("amount")]/$order_price_per_dzn*$order_job_qnty;
			?>	 
                <tr>
                    <td align="left"><? echo $item_descrition; ?></td>
                    <td align="right"><? echo number_format($row[csf("cons_qnty")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("rate")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("amount")],4); ?></td>
                </tr>
            <?  
				 $total_yarn_amount +=$row[csf("amount")];
            }
            ?>
            	<tr class="rpt_bottom" style="font-weight:bold">
                    <td colspan="3">Total</td>
                    <td align="right"><? echo number_format($total_yarn_amount,4); ?></td>
                </tr>
        	</table>
      </div>
      <?
	 
	//End Yarn Cost part report here -------------------------------------------
	
	
	
	//start	Conversion Cost to Fabric report here -------------------------------------------
   	$sql = "select a.id, a.job_no, a.cons_process, a.req_qnty, a.charge_unit, a.amount, a.status_active,b.body_part_id,b.fab_nature_id,b.color_type_id,b.fabric_description 
			from wo_pre_cost_fab_conv_cost_dtls a left join wo_pre_cost_fabric_cost_dtls b on a.job_no=b.job_no and a.fabric_description=b.id
			where a.job_no=".$job_no." ";
	$data_array=sql_select($sql);
		
 	?>

        <div style="margin-top:15px">
            <table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px;text-align:center;" rules="all">
                <tr style="font-weight:bold">
                    <td width="80" rowspan="<? echo count($data_array)+2; ?>"><div class="verticalText"><b>Conversion Cost to Fabric</b></div></td>
                    <td width="350">Particulars</td>
                    <td width="100">Process</td>
                    <td width="100">Required</td>
                    <td width="100">Rate</td>
                    <td width="100">Amount</td>
                </tr>
            <?
            $total_conversion_cost=0;
            foreach( $data_array as $row )
            { 
 				$row[csf("req_qnty")] = $row[csf("req_qnty")]/$order_price_per_dzn*$order_job_qnty;
				$row[csf("amount")] = $row[csf("amount")]/$order_price_per_dzn*$order_job_qnty;
				$item_descrition = $body_part[$row[csf("body_part_id")]].", ".$color_type[$row[csf("color_type_id")]].", ".$row[csf("fabric_description")];
			?>	 
                <tr>
                    <td align="left"><? echo $item_descrition; ?></td>
                    <td align="left"><? echo $conversion_cost_head_array[$row[csf("cons_process")]]; ?></td>
                    <td align="right"><? echo number_format($row[csf("req_qnty")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("charge_unit")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("amount")],4); ?></td>
                </tr>
            <?
                 $total_conversion_cost += $row[csf("amount")];
            }
            ?>
                <tr class="rpt_bottom" style="font-weight:bold">
                    <td colspan="4">Total</td>                    
                    <td align="right"><? echo $total_conversion_cost; ?></td>
                </tr>                
            </table>
      </div>
      <?
	//End Conversion Cost to Fabric report here -------------------------------------------
	
	
	
	//start	Trims Cost part report here -------------------------------------------
   	$sql = "select id, job_no, trim_group,description,brand_sup_ref, cons_uom, cons_dzn_gmts, rate, amount, apvl_req, nominated_supp,status_active
			from wo_pre_cost_trim_cost_dtls  
			where job_no=".$job_no."";
	$data_array=sql_select($sql);
 	?>
 

        <div style="margin-top:15px">
            <table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px;text-align:center;" rules="all">
            <label><b>Trims Cost</b></label>
                <tr style="font-weight:bold">
                    <td width="150">Item Group</td>
                    <td width="150">Description</td>
                    <td width="150">Brand/Supp Ref</td>
                    <td width="100">UOM</td>
                    <td width="100">Consumption</td>
                    <td width="100">Rate</td>
                    <td width="100">Amount</td>
                </tr>
            <?
            $total_trims_cost=0; 
            foreach( $data_array as $row )
            { 
			   $order_qty_tr=0;
			   $dtls_data=sql_select("select po_break_down_id,cons,country_id from wo_pre_cost_trim_co_cons_dtls where wo_pre_cost_trim_cost_dtls_id=".$row[csf("id")]." and cons !=0");
			   foreach($dtls_data as $dtls_data_row )
			   {
				   if($dtls_data_row[csf('country_id')]==0)
					 {
						 $txt_country_cond="";
					 }
					 else
					 {
						 $txt_country_cond ="and c.country_id in (".$dtls_data_row[csf('country_id')].")";
					 }
					 
					 $sql_po_qty=sql_select("select sum(c.order_quantity) as order_quantity,(sum(c.order_quantity)/a.total_set_qnty) as order_quantity_set  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id and  b.id=".$dtls_data_row[csf('po_break_down_id')]."  $txt_country_cond  and a.status_active=1 and b.status_active=1 and c.status_active=1 group by b.id,a.total_set_qnty");
	                 list($sql_po_qty_row)=$sql_po_qty;
	                 $po_qty=$sql_po_qty_row[csf('order_quantity_set')];
					 $order_qty_tr+=$po_qty;
			   }
				$row[csf("cons_dzn_gmts")] = $row[csf("cons_dzn_gmts")]/$order_price_per_dzn*$order_qty_tr;
				$row[csf("amount")] = $row[csf("amount")]/$order_price_per_dzn*$order_qty_tr;
 				$trim_group=return_library_array( "select item_name,id from  lib_item_group where id=".$row[csf("trim_group")], "id", "item_name" );            	 
			?>	 
                <tr>
                    <td align="left"><? echo $trim_group[$row[csf("trim_group")]]; ?></td>
                    <td align="left"><? echo $row[csf("description")]; ?></td>
                    <td align="left"><? echo $row[csf("brand_sup_ref")]; ?></td>
                    <td align="left"><? echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></td>
                    <td align="right"><? echo number_format($row[csf("cons_dzn_gmts")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("rate")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("amount")],4); ?></td>
                </tr>
            <?
                 $total_trims_cost += $row[csf("amount")];
            }
            ?>
                <tr class="rpt_bottom" style="font-weight:bold">
                    <td colspan="6">Total</td>                    
                    <td align="right"><? echo number_format($total_trims_cost,4); ?></td>
                </tr>                
            </table>
      </div>
      <?
	 //End Trims Cost Part report here -------------------------------------------	
	 
	 
	 
	 //start	Embellishment Details part report here -------------------------------------------
    	$sql = "select id, job_no, emb_name, emb_type, cons_dzn_gmts, rate, amount,status_active
			from wo_pre_cost_embe_cost_dtls  
			where job_no=".$job_no."";
	$data_array=sql_select($sql);
	?> 
 

        <div style="margin-top:15px">
            <table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px;text-align:center;" rules="all">
            <label><b>Embellishment Details</b></label>
                <tr style="font-weight:bold">
                    <td width="150">Particulars</td>
                    <td width="150">Type</td>
                    <td width="150">Gmts. Qnty (Dzn)</td>
                    <td width="100">Rate</td>
                    <td width="100">Amount</td>
                 </tr>
            <?
            $total_embellishment_amt=0;  
            foreach( $data_array as $row )
            { 
 				//$emblishment_name_array=array(1=>"Printing",2=>"Embroidery",3=>"Wash",4=>"Special Works",5=>"Others");
 				if($row[csf("emb_name")]==1)$em_type = $emblishment_print_type[$row[csf("emb_type")]];
				else if($row[csf("emb_name")]==2)$em_type = $emblishment_embroy_type[$row[csf("emb_type")]];
				else if($row[csf("emb_name")]==3)$em_type = $emblishment_wash_type[$row[csf("emb_type")]];
				else if($row[csf("emb_name")]==4)$em_type = $emblishment_spwork_type[$row[csf("emb_type")]];
				
				$row[csf("cons_dzn_gmts")] = $row[csf("cons_dzn_gmts")]/$order_price_per_dzn*$order_job_qnty;
				$row[csf("amount")] = $row[csf("amount")]/$order_price_per_dzn*$order_job_qnty;
			?>	 
                <tr>
                    <td align="left"><? echo $emblishment_name_array[$row[csf("emb_name")]]; ?></td>
                    <td align="left"><? echo $em_type; ?></td>
                    <td align="right"><? echo number_format($row[csf("cons_dzn_gmts")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("rate")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("amount")],4); ?></td>
                </tr>
            <?
                 $total_embellishment_amt += $row[csf("amount")];
            }
            ?>
                <tr class="rpt_bottom" style="font-weight:bold">
                    <td colspan="4">Total</td>                    
                    <td align="right"><? echo number_format($total_embellishment_amt,4); ?></td>
                </tr>                
            </table>
      </div>
      <?
	 //End Embellishment Details Part report here -------------------------------------------	
	 
	 
	 
	 //start	Commercial Cost part report here -------------------------------------------
   	$sql = "select id, job_no, item_id, rate, amount, status_active
			from  wo_pre_cost_comarci_cost_dtls  
			where job_no=".$job_no."";
	$data_array=sql_select($sql);
	?> 
 
        <div style="margin-top:15px">
            <table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px;text-align:center;" rules="all">
            <label><b>Commercial Cost</b></label>
                <tr style="font-weight:bold">
                    <td width="150">Particulars</td>
                    <td width="100">Rate</td>
                    <td width="100">Amount</td>
                 </tr>
            <?
            $total_commercial_cost=0;
            foreach( $data_array as $row )
            { 
				$row[csf("amount")] = $row[csf("amount")]/$order_price_per_dzn*$order_job_qnty;
  			?>	 
                <tr>
                    <td align="left"><? echo $camarcial_items[$row[csf("item_id")]]; ?></td>
                    <td align="right"><? echo number_format($row[csf("rate")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("amount")],4); ?></td>
                </tr>
            <?
                 $total_commercial_cost += $row[csf("amount")];
            }
            ?>
                <tr class="rpt_bottom" style="font-weight:bold">
                    <td colspan="2">Total</td>                    
                    <td align="right"><? echo number_format($total_commercial_cost,4); ?></td>
                </tr>                
            </table>
      </div>
      <?
	 //End Commercial Cost Part report here -------------------------------------------	
  
   
  	//start	Commission Cost part report here -------------------------------------------
   	$sql = "select id,job_no,particulars_id,commission_base_id,commision_rate,commission_amount, status_active
			from  wo_pre_cost_commiss_cost_dtls  
			where job_no=".$job_no."";
	$data_array=sql_select($sql);
	?> 
  

        <div style="margin-top:15px">
            <table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:850px;text-align:center;" rules="all">
            <label><b>Commission Cost</b></label>
                <tr style="font-weight:bold">
                    <td width="150">Particulars</td>
                    <td width="150">Commission Basis</td>
                    <td width="100">Rate</td>
                    <td width="100">Amount</td>
                 </tr>
            <?
            $total_commission_cost=0;
            foreach( $data_array as $row )
            { 
				$row[csf("commission_amount")] = $row[csf("commission_amount")]/$order_price_per_dzn*$order_job_qnty;
  			?>	 
                <tr>
                    <td align="left"><? echo $commission_particulars[$row[csf("particulars_id")]]; ?></td>
                    <td align="left"><? echo $commission_base_array[$row[csf("commission_base_id")]]; ?></td>
                    <td align="right"><? echo number_format($row[csf("commision_rate")],4); ?></td>
                    <td align="right"><? echo number_format($row[csf("commission_amount")],4); ?></td>
                </tr>
            <?
                 $total_commission_cost += $row[csf("commission_amount")];
            }
            ?>
                <tr class="rpt_bottom" style="font-weight:bold">
                    <td colspan="3">Total</td>                    
                    <td align="right"><? echo number_format($total_commission_cost,4); ?></td>
                </tr>                
            </table>
      </div>
      <?
	//End Commission Cost Part report here -------------------------------------------	
  
  
	//start	Other Components part report here -------------------------------------------
   	$sql = "select id,job_no,costing_per_id,order_uom_id,fabric_cost,fabric_cost_percent,trims_cost,trims_cost_percent,embel_cost,embel_cost_percent,comm_cost,comm_cost_percent,commission,commission_percent,lab_test,lab_test_percent,inspection,inspection_percent,cm_cost,cm_cost_percent,freight,freight_percent,common_oh,common_oh_percent,total_cost,total_cost_percent,price_dzn,price_dzn_percent,margin_dzn,margin_dzn_percent,price_pcs_or_set,price_pcs_or_set_percent,margin_pcs_set,margin_pcs_set_percent,cm_for_sipment_sche  
			from wo_pre_cost_dtls  
			where job_no=".$job_no."";
	$data_array=sql_select($sql);
	?> 
 
        <div style="margin-top:15px">
         <table>
         <tr>
         <td>
            <table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:450px;text-align:center;" rules="all">
            <label><b>Others Components</b></label>
                <tr style="font-weight:bold">
                    <td width="150">Particulars</td>
                    <td width="100">Amount</td>
                 </tr>
            <?
            $total_other_components=0;
			$lab_test = 0;
			$inspection = 0;
			$cm_cost = 0;
			$freight = 0;
			$common_oh = 0;
            foreach( $data_array as $row )
            { 
				$lab_test = $row[csf("lab_test")]/$order_price_per_dzn*$order_job_qnty;
				$inspection = $row[csf("inspection")]/$order_price_per_dzn*$order_job_qnty;
				$cm_cost = $row[csf("cm_cost")]/$order_price_per_dzn*$order_job_qnty;
				$freight = $row[csf("freight")]/$order_price_per_dzn*$order_job_qnty;
				$common_oh = $row[csf("common_oh")]/$order_price_per_dzn*$order_job_qnty;
   			?>	 

                <tr>
                    <td align="left"s>Lab Test </td>
                    <td align="right"><? echo number_format($lab_test,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Inspection Cost</td>
                    <td align="right"><? echo number_format($inspection,4); ?></td>
                </tr>
                <tr>
                    <td align="left">CM Cost - IE</td>
                    <td align="right"><? echo number_format($cm_cost,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Freight Cost</td>
                    <td align="right"><? echo number_format($freight,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Office OH</td>
                    <td align="right"><? echo number_format($common_oh,4); ?></td>
                </tr>
            <?
                 $total_other_components += $lab_test+$inspection+$cm_cost+$freight+$common_oh;
            }
            ?>
                <tr class="rpt_bottom" style="font-weight:bold">
                    <td>Total</td>                    
                    <td align="right"><? echo number_format($total_other_components,4); ?></td>
                </tr>                
            </table>
            </td>
            <td valign="top" rowspan="2">
             
            <?
     	 // image show here  -------------------------------------------
		 $sql = "select id,master_tble_id,image_location
				from common_photo_library  
				where master_tble_id=$job_no limit 1";
		$data_array=sql_select($sql);
 	  ?> 
          <div style="margin:15px 5px;float:right;width:500px" >
          	<? foreach($data_array AS $inf){ ?>
                <img  src='../../<? echo $inf[csf("image_location")]; ?>' height='400' width='300' />
            <?  } ?>			
          </div>
          </td>
          </tr>
          <tr>
          <td>
           <?
	
	 $total_summary_amount = 0;	
	 $total_summary_amount = $total_commission_cost+$total_commercial_cost+$total_embellishment_amt+$total_trims_cost+$total_conversion_cost+$total_yarn_amount +$woven_subtotal_amount+$knit_subtotal_amount+$lab_test+$inspection+$cm_cost+$freight+$common_oh;
	
	 ?>
	 <div style="margin-top:15px">
            <table class="rpt_table" border="1" cellpadding="1" cellspacing="1" style="width:450px;text-align:center;" rules="all">
            <label><b>Summary</b></label>
                <tr style="font-weight:bold">
                    <td width="150">Cost Summary</td>
                    <td width="100">Total</td>
                 </tr>
                 <tr> 
                    <td align="left">Knit Fabric (Purchase) </td>
                    <td align="right"><? echo number_format($knit_subtotal_amount,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Woven Fabric (Purchase)</td>
                    <td align="right"><? echo number_format($woven_subtotal_amount,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Yarn</td>
                    <td align="right"><? echo number_format($total_yarn_amount,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Conversion to Fabric</td>
                    <td align="right"><? echo number_format($total_conversion_cost,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Trims</td>
                    <td align="right"><? echo number_format($total_trims_cost,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Embellishment</td>
                    <td align="right"><? echo number_format($total_embellishment_amt,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Commercial</td>
                    <td align="right"><? echo number_format($total_commercial_cost,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Commission</td>
                    <td align="right"><? echo number_format($total_commission_cost,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Lab Test</td>
                    <td align="right"><? echo number_format($lab_test,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Inspection Cost</td>
                    <td align="right"><? echo number_format($inspection,4); ?></td>
                </tr>
                <tr>
                    <td align="left">CM Cost - IE</td>
                    <td align="right"><? echo number_format($cm_cost,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Freight Cost</td>
                    <td align="right"><? echo number_format($freight,4); ?></td>
                </tr>
                <tr>
                    <td align="left">Office OH</td>
                    <td align="right"><? echo number_format($common_oh,4); ?></td>
                </tr>
                <tr class="rpt_bottom" style="font-weight:bold">
                    <td>Total</td>                    
                    <td align="right"><? echo number_format($total_summary_amount,4); ?></td>
                </tr>                
            </table> 
          </td>
          </tr>
          </table>
      </div>
      
     
      
      <!--End CM on Net Order Value Part report here ------------------------------------------->
 	<table class="rpt_table" border="0" cellpadding="1" cellspacing="1" style="width:800px;text-align:center;" rules="all">	
		<tr style="alignment-baseline:baseline;">
        	<td height="130" width="33%" style="text-decoration:overline; border:none">Prepared By</td>
            <td width="33%" style="text-decoration:overline; border:none">Checked By</td>
            <td width="33%" style="text-decoration:overline; border:none">Approved By</td>
        </tr>
    </table>
	 
	 <?
	

	exit();
}
?>