<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$batch_arr = return_library_array("select id, batch_no from pro_batch_create_mst","id","batch_no");
$machine_name=return_library_array( "select machine_no,id from  lib_machine_name where is_deleted=0", "id", "machine_no"  );


if ($action=="load_drop_floor")
{
	$data=explode('_',$data);
	$loca=$data[0];
	$com=$data[1];
	//echo "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]'  order by floor_name";die;
	echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]'  order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected,"load_drop_down( 'requires/dyeing_production_controller', document.getElementById('cbo_company_id').value+'**'+this.value, 'load_drop_machine', 'machine_td' );" );     	 
	exit();
}

if ($action=="load_drop_machine")
{
$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
$data=explode('**',$data);

	$com=$data[0];
	$floor=$data[1];
	

	echo create_drop_down( "cbo_machine_name", 135, "select id,concat(machine_no,'-',brand) as machine_name from lib_machine_name where category_id=2 and company_id=$com and floor_id=$floor and status_active=1 and is_deleted=0 and is_locked=0 order by machine_name","id,machine_name", 1, "-- Select Machine --", $selected, "get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+this.value, 'populate_data_from_machine', 'requires/dyeing_production_controller' );","" );
	exit();
	
}
if ($action=="populate_data_from_machine")
{ 
	$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
	$ex_data=explode('**',$data);
	
	//print "select id, floor_id, machine_group from lib_machine_name where id=$ex_data[2] and category_id=2 and company_id=$ex_data[0] and floor_id=$ex_data[1] and status_active=1 and is_deleted=0 group by id";
	 $sql_res="select id, floor_id, machine_group from lib_machine_name where id=$ex_data[2] and category_id=2 and company_id=$ex_data[0] and  floor_id=$ex_data[1] and status_active=1 and is_deleted=0 group by id";
	$nameArray=sql_select($sql_res);
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_machine_no').value 			= '".$floor_arr[$row[csf("floor_id")]]."';\n";
		echo "document.getElementById('txt_mc_group').value 			= '".$row[csf("machine_group")]."';\n";
	}
	exit();
}

if ($action=="batch_number_popup")
{
	echo load_html_head_contents("Batch Number Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id)
		{ 
			$('#hidden_batch_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
<div align="center" style="width:800px;">
    <form name="searchbatchnofrm"  id="searchbatchnofrm">
        <fieldset style="width:790px;">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" width="770" border="1" rules="all" class="rpt_table">
                <thead>
                    <th width="200px">Batch Date Range</th>
                    <th width="160px">Search By</th>
                    <th id="search_by_td_up" width="180">Enter Batch No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" value="">
                    </th>
                </thead>
                <tr>
                    <td align="center">
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px;">To<input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px;">
                    </td>
                    <td align="center" width="160px">
						<?php
							$search_by_arr=array(0=>"Batch No",1=>"Fabric Booking no.",2=>"Color");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td align="center" id="search_by_td" width="140px">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value, 'create_batch_search_list_view', 'search_div', 'dyeing_production_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </table>
            <table width="100%" style="margin-top:5px;">
                <tr>
                    <td colspan="5">
                        <div style="width:100%; margin-left:3px;" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_batch_search_list_view")
{
	$data = explode("_",$data);
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$start_date =$data[2];
	$end_date =$data[3];
	$company_id =$data[4];

	if(trim($data[0])!="")
	{
		if($search_by==0)
			$search_field_cond="and batch_no like '$search_string'";
		else if($search_by==1)
			$search_field_cond="and booking_no like '$search_string'";
		else
			$search_field_cond="and color_id in(select id from lib_color where color_name like '$search_string')";
	}
	else
	{
		$search_field_cond="";
	}
	
	$sql = "select id, batch_no, batch_date, batch_weight, booking_no, extention_no, color_id, batch_against, re_dyeing_from from pro_batch_create_mst where entry_form=0 and company_id=$company_id and batch_for in(0,1) and batch_against<>4 and status_active=1 and is_deleted=0 $search_field_cond $date_cond"; 
	//echo $sql;//die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Batch No</th>
                <th width="80">Extention No</th>
                <th width="80">Batch Date</th>
                <th width="90">Batch Qnty</th>
                <th width="115">Job No</th>
                <th width="80">Color</th>
                <th>Po No</th>
            </thead>
        </table>
        <div style="width:770px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					
					$po_no='';
					
					if($selectResult['re_dyeing_from']==0)
					{	
						$sql_po="select a.po_number as po_no,a.job_no_mst from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and b.mst_id=$selectResult[id]  and b.status_active=1 and b.is_deleted=0 group by a.id";
						//echo $sql_po;die;
						$poArray=sql_select( $sql_po );
						foreach ($poArray as $row1)
						{
							if($po_no=='') $po_no=$row1[csf('po_no')]; else $po_no.=",".$row1[csf('po_no')];
						}
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
							<td width="40" align="center"><?php echo $i; ?></td>	
							<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
                            <td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?></p></td>
							<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
							<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
                            <td width="115"><p><?php echo $selectResult[csf('job_no_mst')]; ?></p></td>
							<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
							<td><?php echo $po_no; ?></td>	
						</tr>
						<?php
						$i++;
					}
					else
					{
						$sql_re= "select id, batch_no, batch_date, batch_weight, booking_no, extention_no, color_id, batch_against, re_dyeing_from from pro_batch_create_mst where  batch_for in(0,1) and entry_form=0 and batch_against<>4 and status_active=1 and is_deleted=0 and id=$selectResult[re_dyeing_from]";
						$dataArray=sql_select( $sql_re );
						foreach($dataArray as $row)
						{
							if($row['re_dyeing_from']==0)
							{
								$sql_po="select a.po_number as po_no,a.job_no_mst from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and b.mst_id=$row[id] and b.status_active=1 and b.is_deleted=0 group by a.id";
								$poArray=sql_select( $sql_po );
								foreach ($poArray as $row2)
								{
									if($po_no=='') $po_no=$row2[csf('po_no')]; else $po_no.=",".$row2[csf('po_no')];
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
									<td width="40" align="center"><?php echo $i; ?></td>	
									<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
									<td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?></p></td>
									<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
									<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
								
                                    <td width="115"><p><?php echo $selectResult[csf('job_no_mst')]; ?></p></td>
									<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
									<td><?php echo $po_no; ?></td>	
								</tr>
								<?php
								$i++;
							}
						}
					}
				}
			?>
            </table>
        </div>
	</div>           
<?php

exit();
}

if($action=='populate_data_from_batch')
{ 
	$ex_data=explode('_',$data);
	$load_unload=$ex_data[0];
		$batch_id=$ex_data[1]; 
		//echo $batch_id;die;
	$data_array=sql_select("select a.id,a.batch_no, a.batch_weight,a.extention_no, a.color_id, a.booking_without_order, sum(b.batch_qnty) as batch_qnty, group_concat(distinct(b.po_id)) as po_id from pro_batch_create_mst a,pro_batch_create_dtls b where a.id='$batch_id' and a.id=b.mst_id");
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('txt_batch_no').value 				= '".$row[csf("batch_no")]."';\n";
		echo "document.getElementById('hidden_batch_id').value 				= ".$batch_id.";\n";
		echo "document.getElementById('txt_batch_ID').value 				= ".$batch_id.";\n";
		echo "document.getElementById('txt_color').value 					= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('txt_ext_id').value 					= ".$row[csf("extention_no")].";\n";
		
		$result_job=sql_select("select group_concat(distinct(a.po_number)) as po_no, a.job_no_mst, b.buyer_name from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and a.id in($row[po_id]) and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0");
				
		echo "document.getElementById('txt_buyer').value 				= '".$buyer_arr[$result_job[0][csf("buyer_name")]]."';\n";
		echo "document.getElementById('txt_job_no').value 				= '".$result_job[0][csf("job_no_mst")]."';\n";
		echo "document.getElementById('txt_order_no').value 			= '".$result_job[0][csf("po_no")]."';\n";
		$sql_batch_d=sql_select("select id,batch_no,process_end_date,end_hours,end_minutes,machine_id,floor_id,process_id,remarks from pro_fab_subprocess where batch_no='".$row[csf("batch_no")]."' and entry_form=35 and load_unload_id=1");
			foreach($sql_batch_d as $dyeing_d)
			{
			echo "document.getElementById('txt_dying_started').value = '".$dyeing_d[csf("process_end_date")]."';\n";
			echo "document.getElementById('txt_dying_end').value = '".$dyeing_d[csf("end_hours")].':'.$dyeing_d[csf("end_minutes")]."';\n";
			}
		
		if($load_unload==1)
		{
				$sql_batch=sql_select("select id,batch_no,process_start_date,process_end_date,load_unload_id,end_hours,end_minutes,start_hours,start_minutes,machine_id,floor_id,process_id,result,remarks from pro_fab_subprocess where batch_no='".$row[csf("batch_no")]."' and entry_form=35 and load_unload_id in(1) ");
		}
		else if($load_unload==2)
		{
			
				$sql_batch=sql_select("select id,batch_no,process_start_date,process_end_date,load_unload_id,end_hours,end_minutes,machine_id,floor_id,process_id,result,remarks from pro_fab_subprocess where batch_no='".$row[csf("batch_no")]."' and entry_form=35 and load_unload_id in(2,1) order by id desc limit 1  ");
		}	
		 
		foreach($sql_batch as $r_batch)
		{
			
			if($r_batch['load_unload_id']==1)
				{
				echo "document.getElementById('txt_update_id').value 				= '".$r_batch[csf("id")]."';\n";
				echo "document.getElementById('txt_process_start_date').value 				= '".change_date_format($r_batch[csf("process_end_date")])."';\n";
				echo "document.getElementById('cbo_sub_process').value 				= '".$r_batch[csf("process_id")]."';\n";
				echo "set_multiselect('cbo_sub_process','0','1','".$r_batch[csf('process_id')]."','0');\n";
				echo "document.getElementById('txt_start_minutes').value	= '".$r_batch[csf("end_minutes")]."';\n";
				echo "document.getElementById('txt_start_hours').value	= '".$r_batch[csf("end_hours")]."';\n";
				echo "document.getElementById('cbo_floor').value = '".$r_batch[csf("floor_id")]."';\n";
			
			echo "load_drop_down( 'requires/dyeing_production_controller', document.getElementById('cbo_company_id').value+'**'+".$r_batch[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
				echo "document.getElementById('cbo_machine_name').value = '".$r_batch[csf("machine_id")]."';\n";
				echo "document.getElementById('txt_remarks').value	= '".$r_batch[csf("remarks")]."';\n";
				if(count($sql_batch)>0)
						{
							echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',1);\n"; 	
						}
					else
						{
							echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',0);\n"; 	
						}
				
				}
				if($r_batch['load_unload_id']==2)
				{ 
				echo "document.getElementById('txt_update_id').value 				= '".$r_batch[csf("id")]."';\n";
				echo "document.getElementById('txt_process_end_date').value 				= '".change_date_format($r_batch[csf("process_end_date")])."';\n";
				echo "document.getElementById('cbo_sub_process').value 				= '".$r_batch[csf("process_id")]."';\n";
				echo "set_multiselect('cbo_sub_process','0','1','".$r_batch[csf('process_id')]."','0');\n";
				echo "document.getElementById('txt_end_minutes').value	= '".$r_batch[csf("end_minutes")]."';\n";
				echo "document.getElementById('txt_end_hours').value	= '".$r_batch[csf("end_hours")]."';\n";
				echo "document.getElementById('txt_dying_started').value = '".$r_batch[csf("process_end_date")]."';\n";
				echo "document.getElementById('txt_dying_end').value = '".$r_batch[csf("end_hours")].':'.$r_batch[csf("end_minutes")]."';\n";
				echo "document.getElementById('cbo_floor').value = '".$r_batch[csf("floor_id")]."';\n";
				
				echo "load_drop_down( 'requires/dyeing_production_controller', document.getElementById('cbo_company_id').value+'**'+".$r_batch[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
				echo "document.getElementById('cbo_machine_name').value = '".$r_batch[csf("machine_id")]."';\n";
				echo "document.getElementById('cbo_result_name').value	= '".$r_batch[csf("result")]."';\n";
				echo "document.getElementById('txt_remarks').value	= '".$r_batch[csf("remarks")]."';\n";
				
				if(count($sql_batch)>0)
					{
						echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',1);\n"; 	
					}
				else
					{
						echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',0);\n"; 	
					}
				}
		}
		exit();
	}
}

if($action=='show_fabric_desc_listview')
{
	$result=sql_select("select item_description, sum(batch_qnty) as batch_qnty from pro_batch_create_dtls where mst_id=$data and status_active=1 and is_deleted=0 group by item_description");
	$i=1;
	 $b_qty=0;
	foreach($result as $row)
	{
		$desc=explode(",",$row[csf('item_description')]);
	?>
    	<tr class="general" id="row_<?php echo $i; ?>">
        	<td><input type="text" name="txt_body_part_<?php echo $i; ?>" id="txt_body_part_<?php echo $i; ?>" class="text_boxes" style="width:110px;" value="<?php echo $desc[0]; ?>" disabled/></td> 
            <td title="<?php echo $desc[2]; ?>"><input type="text" name="txt_cons_comp_<?php echo $i; ?>" id="txt_cons_comp_<?php echo $i; ?>" class="text_boxes" style="width:170px;" value="<?php echo $desc[2]; ?>" disabled/></td>
            <td><input type="text" name="txt_gsm_<?php echo $i; ?>" id="txt_gsm_<?php echo $i; ?>" class="text_boxes" style="width:60px;" value="<?php echo $desc[3]; ?>" disabled/></td>
            <td><input type="text" name="txt_dia_width_<?php echo $i; ?>" id="txt_dia_width_<?php echo $i; ?>" class="text_boxes" style="width:70px;" disabled/></td>
            <td><input type="text" name="txt_batch_qnty_<?php echo $i; ?>" id="txt_batch_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px;" value="<?php echo $row[csf('batch_qnty')]; ?>" disabled/></td>
        </tr>
    <?php
	$b_qty+= $row[csf('batch_qnty')];
		$i++;
		?>
		
	<?php }
	?>
	 <tr>
        <td colspan="4" align="right"><b>Sum:</b> <?php //echo $b_qty; ?> </td>
         <td align="right"><?php echo $b_qty; ?> </td>
        </tr>
        <?php
	exit();
}

if($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="";
		$data_array="";
		$id=return_next_id( "id", "pro_fab_subprocess", 1 ) ;
		//echo $cbo_load_unload;die;
		if($cbo_load_unload=="'1'")
		{
			$field_array="id,company_id,batch_no,batch_id,batch_ext_no,process_id,process_end_date,end_hours,end_minutes,machine_id,floor_id,load_unload_id,entry_form,remarks,inserted_by,insert_date";			$data_array="(".$id.",".$cbo_company_id.",".$txt_batch_no.",".$hidden_batch_id.",".$txt_ext_id.",".$cbo_sub_process.",".$txt_process_start_date.",".$txt_start_hours.",".$txt_start_minutes.",".$cbo_machine_name.",".$cbo_floor.",".$cbo_load_unload.",35,".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		}
		if($cbo_load_unload=="'2'")
		{
			$field_array="id,company_id,batch_no,batch_id,batch_ext_no,process_id,process_end_date,end_hours,end_minutes,machine_id,floor_id,load_unload_id,result,entry_form,remarks,inserted_by,insert_date";			$data_array="(".$id.",".$cbo_company_id.",".$txt_batch_no.",".$hidden_batch_id.",".$txt_ext_id.",".$cbo_sub_process.",".$txt_process_end_date.",".$txt_end_hours.",".$txt_end_minutes.",".$cbo_machine_name.",".$cbo_floor.",".$cbo_load_unload.",".$cbo_result_name.",35,".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		//print_r($data_array);
		}
		
		$rID=sql_insert("pro_fab_subprocess",$field_array,$data_array,0);
		check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "0**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo '0**0';
		}
		disconnect($con);
		die;
			
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$update_id=str_replace("'","",$txt_update_id);
		
			$field_array="";
		$data_array="";
		$id=return_next_id( "id", "pro_fab_subprocess", 1 ) ;
		//echo $cbo_load_unload;die;
		if($cbo_load_unload=="'1'")
		{
			$field_array_update="company_id*batch_no*batch_id*batch_ext_no*process_id*process_end_date*end_hours*end_minutes*machine_id*floor_id*load_unload_id*entry_form*remarks*updated_by*update_date";
			$data_array_update="".$cbo_company_id."*".$txt_batch_no."*".$hidden_batch_id."*".$txt_ext_id."*".$cbo_sub_process."*".$txt_process_start_date."*".$txt_start_hours."*".$txt_start_minutes."*".$cbo_machine_name."*".$cbo_floor."*".$cbo_load_unload."*35*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		}
		if($cbo_load_unload=="'2'")
		{
			$field_array_update="company_id*batch_no*batch_id*batch_ext_no*process_id*process_end_date*end_hours*end_minutes*machine_id*floor_id*load_unload_id*result*entry_form*remarks*updated_by*update_date";
			$data_array_update="".$cbo_company_id."*".$txt_batch_no."*".$hidden_batch_id."*".$txt_ext_id."*".$cbo_sub_process."*".$txt_process_end_date."*".$txt_end_hours."*".$txt_end_minutes."*".$cbo_machine_name."*".$cbo_floor."*".$cbo_load_unload."*".$cbo_result_name."*35*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
		}
		$rID=sql_update("pro_fab_subprocess",$field_array_update,$data_array_update,"id",$update_id,0);
		//echo "insert into pro_fab_subprocess values $field_array_update,$data_array_update,'id',$update_id,0)";die;
		check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "1**".$update_id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$update_id;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo '0**0';
		}
		disconnect($con);
		die;
	}
}

if($action=="check_batch_no")
{
	$data=explode("**",$data);
	$sql="select id, batch_no from pro_batch_create_mst where batch_no='".trim($data[1])."' and company_id='".trim($data[0])."' and is_deleted=0 and status_active=1";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('id')];
	}
	else
	{
		echo "0_";
	}
	
	exit();	
}
if ($action=="on_change_data")
{
	//echo $data;
	$explode_data = explode("_",$data);
	$type = $explode_data[0];
	$company_id = $explode_data[1];
	if($data=="1") // Loading
	{
			?>
              <div>
			<fieldset>
					<table cellpadding="0" cellspacing="2" width="100%" id="main_tbl">
                    	<tr>
                         <td class="must_entry_caption" width="130">Company</td>
                            <td>
                                <?php
                                    echo create_drop_down( "cbo_company_id", 135, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down('requires/dyeing_production_controller', this.value, 'load_drop_floor', 'floor_td' );","","","","","" );
                                ?>
                                <input type="hidden" name="txt_update_id" id="txt_update_id" style="width:100px;" class="text_boxes"  readonly />
                            </td>
                        </tr>
                        <tr> 
                            <td width="" class="must_entry_caption">Batch No.</td>
                            <td>
                                <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:122px;" placeholder="Write/Browse" maxlength="20" title="Maximum 20 Character" onDblClick="openmypage_batchnum();" onBlur="check_batch();"  />
                                <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" readonly />
                                <input type="hidden" name="txt_ext_id" id="txt_ext_id" style="width:100px;" class="text_boxes" readonly />
                            </td>
                        </tr>
                        <tr>
                            <td class="">Process </td>
                            <td>
                               <?php
								   echo create_drop_down( "cbo_sub_process", 135, $conversion_cost_head_array,"", 1, "-- Select --", 0, "","","34,60,61,62,82,83,84,85,86,87,89,100","","","");
                               ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="">Process St. Date</td>
                            <td>
                                <input type="text" name="txt_process_start_date" id="txt_process_start_date" class="datepicker" style="width:122px;"  readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td>Process St. Time</td>
                            <td>
                                 <input type="text" name="txt_start_hours" id="txt_start_hours" class="text_boxes_numeric" placeholder="Hours" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_hours','txt_end_date',2,23)"   />
                                <input type="text" name="txt_start_minutes" id="txt_start_minutes" class="text_boxes_numeric" placeholder="Minutes" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_minutes','txt_end_date',2,59)"  />
                            </td>
                        </tr>
                         <tr>
                            <td>Floor</td>
                            <td id="floor_td">
								<?php
									 echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 order by floor_name limit 0 ,0","id,floor_name", 1, "-- Select Floor --", $selected, "",0,"","","","",4 );
                                ?>
                            </td>
                        <tr>
                        <tr>
                            <td>Machine Name</td>
                            <td id="machine_td">
								<?php
									echo create_drop_down("cbo_machine_name", 135, $blank_array,"", 1, "-- Select Machine --", 0, "",0,"","","",""); 
                                ?>
                            </td>
                        </tr>
                    </table>
			</fieldset>
  </div>
<?php    				
	}
	if($data=="2") // Un-loading
	{
			?>
			<fieldset>
					<table cellpadding="0" cellspacing="2" width="100%" id="main_tbl">
                    	<tr>
                         <td class="must_entry_caption" width="130">Company</td>
                            <td>
                                <?php
                                    echo create_drop_down( "cbo_company_id", 135, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down('requires/dyeing_production_controller', this.value, 'load_drop_floor', 'floor_td' );" );
                                ?>
                                <input type="hidden" name="txt_update_id" id="txt_update_id" style="width:100px;" class="text_boxes" readonly />
                            </td>
                        </tr>
                        <tr> 
                            <td width="" class="must_entry_caption">Batch No.</td>
                            <td>
                                <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:122px;" placeholder="Write/Browse" maxlength="20" title="Maximum 20 Character" onDblClick="openmypage_batchnum();" onBlur="check_batch();" />
                                <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" readonly />
                                <input type="hidden" name="txt_ext_id" id="txt_ext_id" style="width:100px;" class="text_boxes" readonly />
                            </td>
                        </tr>
                        <tr>
                            <td class="">Process </td>
                            <td>
                               <?php
								   echo create_drop_down( "cbo_sub_process", 135, $conversion_cost_head_array,"", 1, "-- Select --", 0, "","","32,63,83,84" );
                               ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="">Process End Date</td>
                            <td>
                                <input type="text" name="txt_process_end_date" id="txt_process_end_date" class="datepicker" style="width:122px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td>Process End Time</td>
                            <td>
                                 <input type="text" name="txt_end_hours" id="txt_end_hours" class="text_boxes_numeric" placeholder="Hours" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_hours','txt_end_date',2,23)" />
                                <input type="text" name="txt_end_minutes" id="txt_end_minutes" class="text_boxes_numeric" placeholder="Minutes" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_minutes','txt_end_date',2,59)" />
                            </td>
                        </tr>
                        <tr>
                            <td>Machine Name</td>
                            <td id="machine_td">
								<?php
									echo create_drop_down("cbo_machine_name", 135, $blank_array,"", 1, "-- Select Machine --", 0, "",1 ); 
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Result</td>
                            <td>
								<?php
								$cbo_result_name=array(1=>'Shade Matched',2=>'Re-Dyeing Needed',3=>'Fabric Damaged');
									echo create_drop_down("cbo_result_name", 135, $cbo_result_name,"", 1, "-- Select Result --", 0, "",0 ,"","","","","txt_remarks"); 
                                ?>
                            </td>
                        </tr>
                    </table>
				<input type="hidden" name="txt_process_start_date" id="txt_process_start_date" />
                <input type="hidden" name="txt_start_minutes" id="txt_start_minutes" class="text_boxes_numeric"  />
               
			</fieldset>
<?php    				
	}
	exit();
}
?>