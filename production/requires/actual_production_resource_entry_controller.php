<?php
session_start();
include('../../includes/common.php');
 
$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//------------------------------------------------------------------------------------------------------


if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location", 220, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select Location --", $selected, "load_drop_down( 'requires/actual_production_resource_entry_controller', this.value+'_'+document.getElementById('cbo_company_name').value, 'load_drop_down_floor', 'floor_td' );","","","","","",3 );     	 
	exit();
}

if ($action=="load_drop_down_floor")
{
	$data=explode('_',$data);
	$loca=$data[0];
	$com=$data[1];
	echo create_drop_down( "cbo_floor", 220, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 and company_id='$com' and location_id='$loca' order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected, "","","","","","",4 );     	 
exit();
}

if($action=='StyleRef_popup'){
echo load_html_head_contents("Popup Info","../../",1, 1,'',1,'');
?>	

<script>

// flowing script for multy select data------------------------------------------------------------------------------start;
/*
 var selected_id = new Array();
 var selected_sf = new Array();
 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}

		
		
		function js_set_value( str ) {
			var str=str.split("_");
			var styleref=trim($('#email_id'+str).html());
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_sf.push(styleref);
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_sf.splice( i, 1 );
				
			}

			var id =''; var srf='';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				srf += selected_sf[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			srf = srf.substr( 0, srf.length - 1 );
			$('#selecteds').val( id );
			$('#styles').val( srf);
		
		
		}
		

*/
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }

// avobe script for multy select data------------------------------------------------------------------------------end;

</script>

<form>


<div style="width:640px; height:400px; overflow:hidden;">
<div style="width:630px;">

        <input type="hidden" id="selected_id" name="selected_id" /> 
        <table cellspacing="0" width="100%"  border="1" rules="all" class="rpt_table">
                           
            <thead>
                <tr>
                    <th width="50"><strong>SL</strong></th>
                    <th width="60"><strong>Style Ref</strong></th>
                    <th><strong>Buyer</strong></th>
                    <th width="80"><strong>PO No</strong></th>
                    <th width="90"><strong>Ship Date</strong></th>
                    <th width="86"><strong>Ord. Qty</strong></th>
                </tr>
            </thead>
          </table>  
</div>         
<div style="width:630px; height:380px; overflow:auto;">
       
        <table cellspacing="0" width="100%"  border="1" rules="all" class="rpt_table" id="tbl_style_ref" >
         <tbody>  
        <?php
        $sql_con="SELECT 
          	b.id,
		    b.po_number,
            a.buyer_name,
            a.style_ref_no,
            b.po_quantity,
            b.unit_price,
            b.pub_shipment_date
        FROM wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.status_active=1";
//echo $sql_con;			
        $sql_data=sql_select($sql_con);
        $buyer_nameArr = return_library_array("select id,buyer_name from lib_buyer","id","buyer_name");
		$lineArr = return_library_array("select id,line_name from lib_sewing_line where  is_deleted=0 and status_active=1","id","line_name");       
        $sl=1;
        foreach($sql_data as $row){
        $bgcolor=($sl%2==0)?"#E9F3FF":"#FFFFFF";
        
        ?>    
                <tr bgcolor="<?php echo $bgcolor ; ?>" id="tr_<?php echo $sl; ?>" onClick="js_set_value('<?php echo $row[csf('id')].'_'.$row[csf('style_ref_no')]; ?>')" style="cursor:pointer;">
                    <td width="50"><?php echo $sl; ?></td>
                    <td width="60" align="center"><?php echo $row[csf('style_ref_no')]; ?></td>
                    <td><?php echo $buyer_nameArr[$row[csf('buyer_name')]]; ?></td>
                    <td width="80" align="center"><?php echo $row[csf('po_number')]; ?></td>
                    <td width="90" align="center"><?php echo $row[csf('pub_shipment_date')]; ?></td>
                    <td width="70" align="right"><?php echo $row[csf('po_quantity')]; ?></td>
                </tr>
         <?php $sl++; } ?>
            <tbody>
        </table>
        
</div> </div>     
        
</form>


<script language="javascript" type="text/javascript">
  setFilterGrid("tbl_style_ref");
</script>


<?php
}


if($action=='SystemIdPopup'){
echo load_html_head_contents("Popup Info","../../",1, 1,'',1,'');
?>	

<script>

// flowing script for multy select data------------------------------------------------------------------------------start;
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }

// avobe script for multy select data------------------------------------------------------------------------------end;

</script>

<form>
        <input type="hidden" id="selected_id" name="selected_id" /> 
       
        <table cellspacing="0" width="600"  border="1" rules="all" class="rpt_table" >
            <thead>
                <tr>
                    <th width="100"><strong>Resource Number</strong></th>
                    <th width="100"><strong>Company</strong></th>
                    <th width="100"><strong>Location</strong></th>
                    <th width="100"><strong>Floor</strong></th>
                    <th width="100"><strong>Line Marge</strong></th>
                    <th width="100"><strong>Line Number</strong></th>
                </tr>
            </thead>
        </table>
        
  <div style=" height:380px; overflow:auto;">          
        <table cellspacing="0" width="600"  border="1" rules="all" class="rpt_table" id="tbl_style_ref" >
        <?php
        $sql_con="SELECT id,prefix, prod_resource_num, resource_num, company_id,location_id, floor_id, line_marge,line_number FROM prod_resource_mst where is_deleted=0";
        $sql_data=sql_select($sql_con);
	    $company_nameArr = return_library_array("select id,company_name from lib_company","id","company_name");
	    $locatinArr = return_library_array("select id,location_name from lib_location","id","location_name");
	    $floorArr = return_library_array("select id,floor_name from lib_prod_floor","id","floor_name");
	    $lineArr = return_library_array("select id,line_name from lib_sewing_line","id","floor_name");

	    $line_name = return_library_array("select id,line_name from lib_sewing_line","id","line_name");
	    $sl=1;
        foreach($sql_data as $row){
        $bgcolor=($sl%2==0)?"#E9F3FF":"#FFFFFF";
        
		$lname='';
		$line_id=explode(",",$row[csf('line_number')]);
			for($i=0; $i<count($line_id); $i++)
			{
			$lname.=($i==(count($line_id)-1))?$line_name[$line_id[$i]]:$line_name[$line_id[$i]].',';
			}

        ?>    
                <tr bgcolor="<?php echo $bgcolor ; ?>" id="tr_<?php echo $sl; ?>" onClick="js_set_value('<?php echo $row[csf('id')].'_'.$row[csf('resource_num')].'_'.$row[csf('company_id')].'_'.$row[csf('location_id')].'_'.$row[csf('floor_id')].'_'.$row[csf('line_marge')].'_'.$row[csf('line_number')].'_'.$lname; ?>')" style="cursor:pointer;">
                    <td width="100" align="center"><?php echo $row[csf('prod_resource_num')]; ?></td>
                    <td width="100" align="center"><?php echo $company_nameArr[$row[csf('company_id')]]; ?></td>
                    <td width="100" align="center"><?php echo $locatinArr[$row[csf('location_id')]]; ?></td>
                    <td width="100" align="center"><?php echo $floorArr[$row[csf('floor_id')]]; ?></td>
                    <td width="100" align="center"><?php echo $yes_no[$row[csf('line_marge')]]; ?></td>
                    <td width="100" align="center"><?php echo $lname; ?></td>
                </tr>
         <?php $sl++; } ?>
        </table>
      </div>
        
</form>
<script language="javascript" type="text/javascript">
  setFilterGrid("tbl_style_ref");
</script>


<?php
}// action SystemIdPopup end;

$linedata_acttion=explode('**',$action);
if($linedata_acttion[0]=="Line_popup")
{
	echo load_html_head_contents("Sewing Out Info","../../", 1, 1, $unicode,1,'');
?>

<script>
var lineid="<?php echo $linedata_acttion[1];?>";
var line_merge="<?php echo $linedata_acttion[2];?>";

// flowing script for multy select data------------------------------------------------------------------------------start;
 var selected_id = new Array();
 var selected_line = new Array();
 var selected_line_serial = new Array();
 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}

		
		
		function js_set_value(str) {
			
			var linetexts=trim($("#lnnetext_"+str).html());
			var lineserial=trim($("#lineserial"+str).html());



			toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( str, selected_id ) == -1 ) {
				selected_id.push( str );
				selected_line.push(linetexts);
				selected_line_serial.push(parseInt(lineserial));
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str ) break;
				}
				selected_id.splice( i, 1 );
				selected_line.splice( i, 1 );
				selected_line_serial.splice( i, 1 );
				
			}

			var id =''; var ln='';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				ln += selected_line[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			ln = ln.substr( 0, ln.length - 1 );
			$('#selecteds').val( id );
			$('#linename').val( ln);

		}
		
	
	
	
	function fn_onClosed()
	{
	
		var x=Math.min.apply(Math,selected_line_serial);	
		if(line_merge==1 && (selected_line_serial.length!=0))
		{
			for( var i = 0; i < selected_line_serial.length; i++ )
			{	
				if(( jQuery.inArray( x, selected_line_serial ))== -1)
				{
				//alert(selected_line_serial.min());
				if(selected_line_serial.length!=1){ alert("Line sequence must be sequentially."); return;}
				}
				x++;
				
			}
		}
	
		if(line_merge==2 && (selected_line_serial.length!=0))
		{
			if(selected_line_serial.length!=1){ alert("Please select single line"); return;}	
		}

		var txt_string = $('#selecteds').val();
		$('#linename').val();
		if(txt_string==""){ alert("Please Select The Serial"); return;}
		parent.emailwindow.hide();
		
	}

</script>
<form>
 <div style="height:395px; overflow:auto;">       
        <input type="hidden" id="selecteds" name="selecteds" /> 
        <input type="hidden" id="linename" name="linename" />
        <table cellspacing="0" width="100%"  border="1" rules="all" class="rpt_table" id="tbl_style_ref" >
                           
            <thead>
                <tr>
                    <th width="50"><strong>SL</strong></th>
                    <th width="60"><strong>Sequence</strong></th>
                    <th><strong>Line Name</strong></th>
                </tr>
            </thead>
            <tbody>
           
        <?php
       
	$com=$linedata_acttion[3];
	$loca=$linedata_acttion[4];
	$floor=$linedata_acttion[5];
	   
	    //$sql_con="select id,line_name from lib_sewing_line where is_deleted=0 and status_active=1 and floor_name!=0 order by line_name";
		
	   $sql_con="select id,line_name,sewing_line_serial from lib_sewing_line where  is_deleted=0 and status_active=1 and company_name='$com' and location_name='$loca' and floor_name=$floor and floor_name!=0 order by line_name";
		
		
        $sql_data=sql_select($sql_con);
	    $sl=1;
        foreach($sql_data as $row){
        $bgcolor=($sl%2==0)?"#E9F3FF":"#FFFFFF";
        ?>    
                <tr bgcolor="<?php echo $bgcolor ; ?>" id="tr_<?php echo $row[csf('id')]; ?>" onClick="js_set_value(<?php echo $row[csf('id')]; ?>)" style="cursor:pointer;">
                    <td align="center"><?php echo $sl; ?></td>
                    <td align="center" id="lineserial<?php echo $row[csf('id')]; ?>"><?php echo $row[csf('sewing_line_serial')]; ?></td>
                    <td id="lnnetext_<?php echo $row[csf('id')]; ?>"><?php echo $row[csf('line_name')]; ?></td>
                </tr>
         <?php $sl++; } ?>
            </tbody>
        </table>
</div>     
        <table width="100%">
            <tr>
                <td colspan="2" align="right"><input type="button" name="btn_close" class="formbutton" style="width:100px" value="Close" onClick="fn_onClosed()" /></td>
            </tr>
        </table>
        
</form>

<script>
var lineid=lineid.split(',');
	for(i=0; i<lineid.length; i++)
	{
	js_set_value(parseInt(trim(lineid[i])));	
	}


</script>

<?php



}// action Line_popup end;



//pro_ex_factory_mst
if ($action=="save_update_delete")
{  $flag=0;
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 

	if ($operation==0) // Insert Here----------------------------------------------------------
	{
		$prod_reso_allo=return_field_value("auto_update","variable_settings_production","company_name=$cbo_company_name and variable_list=23 and is_deleted=0 and status_active=1");

 // echo $prod_reso_allo;die;
		if($prod_reso_allo!=1) { echo 45;die;}
		
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
 		//table lock here 
 	if(str_replace("'",'',$system_id)=="")
	{	
		$resource_mst= return_field_value("id","prod_resource_mst" ,"company_id=$cbo_company_name and location_id=$cbo_location and floor_id=$cbo_floor and line_marge=$cbo_line_merge and line_number=$cbo_line_no","id");
		
		if(str_replace("'","",$resource_mst)=='')
			{
			if($db_type==2)
			  {
				$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'APR',date('Y',time()), 5, "select prefix,prod_resource_num  from prod_resource_mst where company_id=$cbo_company_name and  TO_CHAR(insert_date,'YYYY')=".date('Y',time())." order by id DESC ", "prefix", "prod_resource_num" ));
				
				}
			 if($db_type==0)
				{
					$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'APR', date("Y"), 5, "select prefix,prod_resource_num  from prod_resource_mst where company_id=$cbo_company_name order by id DESC ", "prefix", "prod_resource_num" ));
		
				}
				$id=return_next_id("id", "prod_resource_mst", 1);
				$field_array1="id, prefix,prod_resource_num,resource_num, company_id,location_id,floor_id,line_marge,line_number,inserted_by,insert_date,is_deleted";
				$data_array1="(".$id.",'".$new_sys_number[1]."',".$new_sys_number[2].",'".$new_sys_number[0]."',".$cbo_company_name.",".$cbo_location.",".$cbo_floor.",".$cbo_line_merge.",".$cbo_line_no.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',0)";
				//echo "insert into prod_resource_mst ($field_array1) values $data_array1";die;
	
			
		  }
		  else
		  {
			$id= $resource_mst;   
			$new_sys_number[0]=return_field_value("resource_num","prod_resource_mst" ," id=$id","resource_num"); 
		  }
	}
	else 
	{
		
		$id=$system_id;
	}
		if(str_replace("'",'',$h_dtl_mst_id)=="")
		{	
			$resource=return_field_value("a.resource_num","prod_resource_mst a,prod_resource_dtls b","a.id=b.mst_id and a.company_id=$cbo_company_name and a.location_id=$cbo_location and a.floor_id=$cbo_floor and a.line_number=$cbo_line_no and b.is_deleted=0 and b.pr_date between ".$txt_date_from." and ".$txt_date_to." ","resource_num");
		
			if($resource!='')
				{
					echo "6**".$resource;
					die;			
				} 
			$id3=return_next_id("id", "prod_resource_dtls_mast", 1);
				$field_array3="id,mst_id,from_date,to_date, man_power,operator,helper,line_chief,active_machine,target_per_hour,working_hour,style_ref_id,capacity,smv_adjust,smv_adjust_type,total_smv,inserted_by,insert_date,is_deleted";
				
			    $line_smv_adjust=0;
			    $txt_smv_adjustment=str_replace("'","",$txt_smv_adjustment);
				if(str_replace("'","",$cbo_smv_adjust_by)==1)     $line_smv_adjust=$txt_smv_adjustment;
			    if(str_replace("'","",$cbo_smv_adjust_by)==2)   $line_smv_adjust=(($txt_smv_adjustment)*(-1));
				$total_smv=((str_replace("'","",$txt_man_power)*str_replace("'","",$txt_working_hour))*60)+$line_smv_adjust;
			$data_array3="(".$id3.",".$id.",".$txt_date_from.",".$txt_date_to.",".$txt_man_power.",".$txt_operator.",".$txt_helper.",".$txt_Line_chief.",".$txt_active_machine.",".$txt_target_hour.",".$txt_working_hour.",".$txt_style_ref.",".$txt_capacity.",'".$txt_smv_adjustment."',".$cbo_smv_adjust_by.",".$total_smv.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',0)";
			$id2=return_next_id("id", "prod_resource_dtls", 1);
			for($i=0; $i < datediff('d',str_replace("'","",$txt_date_from),str_replace("'","",$txt_date_to)); $i++)
			{
				$date = add_date(str_replace("'","",$txt_date_from),$i); 
				if($db_type==2) $dd=change_date_format($date,'mm-dd-yyyy','-',1);
				if($db_type==0) $dd=change_date_format($date,'yyyy-mm-dd');
				
				$field_array="id, mast_dtl_id,mst_id, pr_date, man_power,operator,helper,line_chief,active_machine,target_per_hour,working_hour,style_ref_id,smv_adjust,smv_adjust_type,total_smv,inserted_by,insert_date,is_deleted";     
				if($i!=0)$data_array.=",";
				$data_array.="(".$id2.",".$id3.",".$id.",'".$dd."',".$txt_man_power.",".$txt_operator.",".$txt_helper.",".$txt_Line_chief.",".$txt_active_machine.",".$txt_target_hour.",".$txt_working_hour.",".$txt_style_ref.",'".$txt_smv_adjustment."',".$cbo_smv_adjust_by.",".$total_smv.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',0)";
				$id2++;
			}
		}
	//echo "10**".$data_array3;die;
		$field_array_time="id,mst_id,mast_dtl_id,shift_id,prod_start_time,lunch_start_time,remarks";
		$id_time= return_next_id( "id", "prod_resource_dtls_time", 1 );
		$save_string=explode(",",str_replace("'","",$save_string));
		foreach($save_string as $prevData)
		{
			$time_dtls=explode("_",$prevData);
			$shift_id=$time_dtls[0];
			$prod_start_time=$time_dtls[1];
			$lunch_start_time=$time_dtls[2];
			$remarks=$time_dtls[3];
			
			if($db_type==2)
			{
				$prod_start_time="to_date('".$prod_start_time."','HH24:MI:SS')";
				$lunch_start_time="to_date('".$lunch_start_time."','HH24:MI:SS')";
				$data_array_time.=" INTO prod_resource_dtls_time (".$field_array_time.") VALUES(".$id_time.",".$id.",".$id3.",".$shift_id.",".$prod_start_time.",".$lunch_start_time.",'".$remarks."')";
			}
			else
			{
				if($data_array_time!="") $data_array_time.=",";
				$data_array_time.="(".$id_time.",".$id.",".$id3.",".$shift_id.",'".$prod_start_time."','".$lunch_start_time."','".$remarks."')";
			}
			
			$id_time = $id_time+1;
		}
		
		$rID=1;	$rID2=1; $rID3=1; $rID_time=true;
	
		if($data_array1!="") 
		{ 
			 $rID=sql_insert("prod_resource_mst",$field_array1,$data_array1,0);
		}
	
		if(str_replace("'",'',$h_dtl_mst_id)=="")
		{	
			$rID2=sql_insert("prod_resource_dtls_mast",$field_array3,$data_array3,0);
			$rID3=sql_insert("prod_resource_dtls",$field_array,$data_array,0);
		}
	   
		if($data_array_time!="")
	  	{	
		  	if($db_type==2)
			{
				$query="INSERT ALL".$data_array_time." SELECT * FROM dual";
				$rID_time=execute_query($query);
			}
			else
			{
				$rID_time=sql_insert("prod_resource_dtls_time",$field_array_time,$data_array_time,1);
			} 
	   	}
	   
		if($db_type==0 )
		{	
			if($rID && $rID2 && $rID3 && $rID_time)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'","",$id)."**".$new_sys_number[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$rID);
			}
		}
		if($db_type==2 || $db_type==1 )
		{	if($rID && $rID2 && $rID3 && $rID_time)
			{
				oci_commit($con);
				echo "0**".str_replace("'","",$id)."**".$new_sys_number[0];
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$rID);
			}
		}
		disconnect($con);
		die;
	}
  	else if ($operation==1) // Update Here End------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$prod_reso_allo=return_field_value("auto_update","variable_settings_production","company_name=$cbo_company_name and variable_list=23 and is_deleted=0 and status_active=1");
 
		if($prod_reso_allo!=1) { echo 45; die;}
		if(str_replace("'",'',$system_id)!="")
		{
			 $resource=is_duplicate_field("a.resource_num","prod_resource_mst a,prod_resource_dtls b","a.id=b.mst_id and  b.mast_dtl_id!=".str_replace("'","",$h_dtl_mst_id)." and a.company_id=$cbo_company_name and a.location_id=$cbo_location and a.floor_id=$cbo_floor and a.line_number=$cbo_line_no and b.is_deleted=0 and b.pr_date between ".$txt_date_from." and ".$txt_date_to." and a.is_deleted=0 and b.is_deleted=0");
			if($resource!='')
				{
					echo "6**".$resource;
					die;			
				}
					 
			$mst_id=str_replace("'",'',$system_id);
			$field_array_up="location_id*floor_id*line_marge*line_number*updated_by*update_date";
			$data_array_up="".$cbo_location."*".$cbo_floor."*".$cbo_line_merge."*".$cbo_line_no."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			$field_array_up3="from_date*to_date*man_power*operator*helper*line_chief*active_machine*target_per_hour*working_hour*style_ref_id*capacity*smv_adjust*smv_adjust_type*total_smv*updated_by*update_date*is_deleted";
			
		
			$line_smv_adjust=0;
			$txt_smv_adjustment=str_replace("'","",$txt_smv_adjustment);
			if(str_replace("'","",$cbo_smv_adjust_by)==1)     $line_smv_adjust=$txt_smv_adjustment;
			if(str_replace("'","",$cbo_smv_adjust_by)==2)   $line_smv_adjust=(($txt_smv_adjustment)*(-1));
			$total_smv=((str_replace("'","",$txt_man_power)*str_replace("'","",$txt_working_hour))*60)+$line_smv_adjust;
			$data_array_up3="".$txt_date_from."*".$txt_date_to."*".$txt_man_power."*".$txt_operator."*".$txt_helper."*".$txt_Line_chief."*".$txt_active_machine."*".$txt_target_hour."*".$txt_working_hour."*".$txt_style_ref."*".$txt_capacity."*'".$txt_smv_adjustment."'*".$cbo_smv_adjust_by."*".$total_smv."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*0";
			$field_array="id,mast_dtl_id,mst_id,pr_date,man_power,operator,helper,line_chief,active_machine,target_per_hour,working_hour,style_ref_id,smv_adjust,smv_adjust_type,total_smv,inserted_by,insert_date, is_deleted";
			$id=return_next_id("id", "prod_resource_dtls", 1);
			for($i=0; $i < datediff('d',str_replace("'","",$txt_date_from),str_replace("'","",$txt_date_to)); $i++)
			{
				    $date = add_date(str_replace("'","",$txt_date_from),$i);
					//$dd=change_date_format($date,'mm-dd-yyyy','-',1);
					if($db_type==2) $dd=change_date_format($date,'mm-dd-yyyy','-',1);
				    if($db_type==0) $dd=change_date_format($date,'yyyy-mm-dd');
					 $line_smv_adjust=0;
			        $txt_smv_adjustment=str_replace("'","",$txt_smv_adjustment);
				    if(str_replace("'","",$cbo_smv_adjust_by)==1)     $line_smv_adjust=$txt_smv_adjustment;
			        if(str_replace("'","",$cbo_smv_adjust_by)==2)   $line_smv_adjust=(($txt_smv_adjustment)*(-1));
					if($i!=0)$data_array.=",";
					$data_array.="(".$id.",".$h_dtl_mst_id.",".$mst_id.",'".$dd."',".$txt_man_power.",".$txt_operator.",".$txt_helper.",".$txt_Line_chief.",".$txt_active_machine.",".$txt_target_hour.",".$txt_working_hour.",".$txt_style_ref.",'".$txt_smv_adjustment."',".$cbo_smv_adjust_by.",".$total_smv.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',0)";
					$id++;
			}
		}
		
		$field_array_time="id,mst_id,mast_dtl_id,shift_id,prod_start_time,lunch_start_time,remarks";
		$id_time= return_next_id( "id", "prod_resource_dtls_time", 1 );
		$save_string=explode(",",str_replace("'","",$save_string));
		foreach($save_string as $prevData)
		{
			$time_dtls=explode("_",$prevData);
			$shift_id=$time_dtls[0];
			$prod_start_time=$time_dtls[1];
			$lunch_start_time=$time_dtls[2];
			$remarks=$time_dtls[3];
			
			if($db_type==2)
			{
				$prod_start_time="to_date('".$prod_start_time."','HH24:MI:SS')";
				$lunch_start_time="to_date('".$lunch_start_time."','HH24:MI:SS')";
				$data_array_time.=" INTO prod_resource_dtls_time (".$field_array_time.") VALUES(".$id_time.",".$mst_id.",".$h_dtl_mst_id.",".$shift_id.",".$prod_start_time.",".$lunch_start_time.",'".$remarks."')";
			}
			else
			{
				if($data_array_time!="") $data_array_time.=",";
				$data_array_time.="(".$id_time.",".$mst_id.",".$h_dtl_mst_id.",".$shift_id.",'".$prod_start_time."','".$lunch_start_time."','".$remarks."')";
			}
			
			$id_time = $id_time+1;
		}


		$rID_time=true;
		if(str_replace("'",'',$system_id)!="")
			{
				$rID=sql_update("prod_resource_mst",$field_array_up,$data_array_up,"id",$mst_id,1);
				
				$rID1=sql_update("prod_resource_dtls_mast",$field_array_up3,$data_array_up3,"id",$h_dtl_mst_id,1);
				$delete=execute_query("delete from prod_resource_dtls where mst_id=$mst_id and mast_dtl_id=$h_dtl_mst_id");
				$delete2=execute_query("delete from prod_resource_dtls_time where mst_id=$mst_id and mast_dtl_id=$h_dtl_mst_id");
				$rID2=sql_insert("prod_resource_dtls",$field_array,$data_array,1);
				
				if($data_array_time!="")
				{	
					if($db_type==2)
					{
						$query="INSERT ALL".$data_array_time." SELECT * FROM dual";
						$rID_time=execute_query($query);
					}
					else
					{
						$rID_time=sql_insert("prod_resource_dtls_time",$field_array_time,$data_array_time,1);
					} 
				}
			}
			
			
		if($db_type==0)
		{
		 if($rID && $rID1 && $delete && $rID2 && $delete2 && $rID_time)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$mst_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$rID);
			}
		}
		if($db_type==2 || $db_type==1 )
			{			
			if($rID && $rID1 && $delete && $rID2 && $delete2 && $rID_time)
				{
					oci_commit($con);
					echo "1**".str_replace("'",'',$mst_id);
				}
				else
				{
					oci_rollback($con);
					echo "10**".str_replace("'",'',$rID);
				}
			}
						disconnect($con);
						die;
	}
 
	else if ($operation==2)  // Delete Here---------------------------------------------------------- 
	{
 		
	}
 
}// action save_update_delete end;



if($action=='create_acl_pdc_rec_entry_list_view'){
	   
	$sql ="SELECT  id,mst_id,from_date,to_date,man_power,operator,helper,line_chief,active_machine ,target_per_hour ,working_hour ,style_ref_id FROM prod_resource_dtls_mast where mst_id=$data and is_deleted=0"; 
	   
	    $styleArr = return_library_array("SELECT b.id, a.style_ref_no FROM wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst","id","style_ref_no");
		$arr=array (9=>$styleArr);
		
		
		echo  create_list_view ( "list_view", "Form Date,To Date,Man Power,Operator,Helper,Line Chief,Active Machine,Target/ Hour,Working Hour,Style Ref.", "80,80,50,60,170,100,100,100,100,100","1000","220",0, $sql , "get_php_form_data", "id,mst_id,from_date,to_date", "'load_php_data_to_form'", 1, "0,0,3,0,0,0,0,0,0,style_ref_id", $arr , "from_date,to_date,man_power,operator,helper,line_chief,active_machine,target_per_hour,working_hour,style_ref_id", "requires/actual_production_resource_entry_controller", 'setFilterGrid("list_view",-1);','3,3,0,0,0,0,0,0,0,0' ) ; 
	
}// action create_acl_pdc_rec_entry_list_view end;



if($action=="load_php_data_to_form")
{
	list($id,$mast_id,$form_date,$form_to)=explode("_",$data);
/*$fdate=explode("-",$form_date);
$tdate=explode("-",$form_to);
$form_date=$fdate[2].'-'.$fdate[1].'-'.$fdate[0];
$form_to=$tdate[2].'-'.$tdate[1].'-'.$tdate[0];*/
//echo $frm_d=change_date_format($form_date);die;
	$sql_con = return_library_array("SELECT id, resource_num FROM prod_resource_mst","id","resource_num"); 
	

	$res = sql_select("SELECT  id,mst_id,man_power,operator,helper,line_chief,active_machine,target_per_hour,working_hour,style_ref_id,smv_adjust,smv_adjust_type,capacity FROM prod_resource_dtls_mast where mst_id=$mast_id  and id=$id and is_deleted=0");

	$styleRefArr = return_library_array("SELECT b.id,a.style_ref_no FROM wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.status_active=1","id","style_ref_no");
 	
	foreach($res as $result)
	{
		
		echo "$('#h_dtl_mst_id').val('".$id."');\n";
		echo "$('#system_id_show').val('".$sql_con[$mast_id]."');\n";
		echo "$('#system_id').val('".$mast_id."');\n";
		echo "$('#txt_date_from').val('".change_date_format($form_date)."');\n";
		echo "$('#txt_date_to').val('".change_date_format($form_to)."');\n";
		echo "$('#txt_man_power').val('".$result[csf('man_power')]."');\n";
		
		echo "$('#txt_operator').val('".$result[csf('operator')]."');\n";
		echo "$('#txt_helper').val('".$result[csf('helper')]."');\n";
		echo "$('#txt_Line_chief').val('".$result[csf('line_chief')]."');\n";

        echo "$('#txt_smv_adjustment').val('".$result[csf('smv_adjust')]."');\n";
		echo "$('#cbo_smv_adjust_by').val('".$result[csf('smv_adjust_type')]."');\n";
		echo "$('#txt_active_machine').val('".$result[csf('active_machine')]."');\n";
		echo "$('#txt_target_hour').val('".$result[csf('target_per_hour')]."');\n"; 		
		echo "$('#txt_working_hour').val('".$result[csf('working_hour')]."');\n";
		echo "$('#txt_style_ref_show').val('".$styleRefArr[$result[csf('style_ref_id')]]."');\n";
		echo "$('#txt_style_ref').val('".$result[csf('style_ref_id')]."');\n";
		echo "$('#txt_capacity').val('".$result[csf('capacity')]."');\n";
		echo "$('#cbo_company_name').attr('disabled',true);\n";
		echo "$('#cbo_location').attr('disabled',true);\n";
		echo "$('#cbo_floor').attr('disabled',true);\n";
		echo "$('#cbo_line_merge').attr('disabled',true);\n";
		echo "$('#cbo_line_no_sow').attr('disabled',true);\n";
		
		if($db_type==0)
		{
			$nameArray=sql_select("select shift_id, TIME_FORMAT( prod_start_time, '%H:%i' ) as prod_start_time,TIME_FORMAT( lunch_start_time, '%H:%i' ) as lunch_start_time, remarks from prod_resource_dtls_time where mast_dtl_id='$id' and status_active=1 and is_deleted=0");
		}
		else
		{
			$nameArray=sql_select("select shift_id, TO_CHAR(prod_start_time,'HH24:MI') as prod_start_time, TO_CHAR(lunch_start_time,'HH24:MI') as lunch_start_time, remarks from prod_resource_dtls_time where mast_dtl_id='$id' and status_active=1 and is_deleted=0");
		}
		
		$save_string='';
		foreach($nameArray as $row)
		{
			if($save_string=="") $save_string=$row[csf('shift_id')]."_".$row[csf('prod_start_time')]."_".$row[csf('lunch_start_time')]."_".$row[csf('remarks')];
			else $save_string.=",".$row[csf('shift_id')]."_".$row[csf('prod_start_time')]."_".$row[csf('lunch_start_time')]."_".$row[csf('remarks')];
		}
		
		echo "document.getElementById('save_string').value 					= '".$save_string."';\n";
	
		echo "set_button_status(1, permission, 'fnc_Ac_Production_Resource_Entry',1);";
  	}
 	exit();	
}

if ($action=="time_popup")
{
	echo load_html_head_contents("Production Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?> 
	<script>
		
		function fnc_valid_time(val,field_id)
		{
			var val_length=val.length;
			if(val_length==2)
			{
				document.getElementById(field_id).value=val+":";
			}
			
			var colon_contains=val.contains(":");
			if(colon_contains==false)
			{
				if(val>23)
				{
					document.getElementById(field_id).value='23:';
				}
			}
			else
			{
				var data=val.split(":");
				var minutes=data[1];
				var str_length=minutes.length;
				var hour=data[0]*1;
				
				if(hour>23)
				{
					hour=23;
				}
				
				if(str_length>=2)
				{
					minutes= minutes.substr(0, 2);
					if(minutes*1>59)
					{
						minutes=59;
					}
				}
				
				var valid_time=hour+":"+minutes;
				document.getElementById(field_id).value=valid_time;
			}
		}
		
		function numOnly(myfield, e, field_id)
		{
			var key;
			var keychar;
			if (window.event)
				key = window.event.keyCode;
			else if (e)
				key = e.which;
			else
				return true;
			keychar = String.fromCharCode(key);

			// control keys
			if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
			return true;
			// numbers
			else if ((("0123456789:").indexOf(keychar) > -1))
			{
				var dotposl=document.getElementById(field_id).value.lastIndexOf(":");
				if(keychar==":" && dotposl!=-1)
				{
					return false;
				}
				return true;
			}
			else
				return false;
		}  
	
		function fnc_close()
		{
			var save_string='';
			
			$("#tbl_list_search").find('tbody tr').each(function()
			{
				var shiftId=$(this).find('input[name="shiftId[]"]').val();
				var txt_prod_start_time=$(this).find('input[name="txt_prod_start_time[]"]').val();
				var txt_lunch_start_time=$(this).find('input[name="txt_lunch_start_time[]"]').val();
				var txtRemarks=$(this).find('input[name="txtRemarks[]"]').val();

				if(save_string=="")
				{
					save_string=shiftId+"_"+txt_prod_start_time+"_"+txt_lunch_start_time+"_"+txtRemarks;
				}
				else
				{
					save_string+=","+shiftId+"_"+txt_prod_start_time+"_"+txt_lunch_start_time+"_"+txtRemarks;
				}
			});
			
			$('#hide_save_string').val( save_string );
			
			parent.emailwindow.hide();
		}
    </script>

</head>

<body>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:10px">
            <input type="hidden" name="hide_save_string" id="hide_save_string" class="text_boxes" value="">
            <div style="width:620px;max-height:250px;" align="center">
                <table cellspacing="0" width="100%" cellpadding="0" class="rpt_table" rules="all" border="1" id="tbl_list_search">
                	<thead>
                    	<th width="100">Shift Name</th>
                        <th width="160">Production Start Time</th>
                        <th width="160">Lunch Start Time</th>
                        <th>Remarks</th>
                    </thead>
                    <tbody>
						<?php
                            $i=1; $dataArray=array();
							if($save_string!="")
							{
								$save_string=explode(",",$save_string);
								foreach($save_string as $prevData)
								{
									$prevData=explode("_",$prevData);
									$shift_id=$prevData[0];
									$prod_start_time=$prevData[1];
									$lunch_start_time=$prevData[2];
									$remarks=$prevData[3];
									
									$dataArray[$shift_id]['pst']=$prod_start_time;
									$dataArray[$shift_id]['lst']=$lunch_start_time;
									$dataArray[$shift_id]['rk']=$remarks;
								}
							}
							else
							{
								if($db_type==0)
								{
									$nameArray=sql_select("select shift_id,TIME_FORMAT( prod_start_time, '%H:%i' ) as prod_start_time,TIME_FORMAT( lunch_start_time, '%H:%i' ) as lunch_start_time from variable_settings_production where company_name='$cbo_company_name' and variable_list=26 and status_active=1 and is_deleted=0");
								}
								else
								{
									$nameArray=sql_select("select shift_id, TO_CHAR(prod_start_time,'HH24:MI') as prod_start_time, TO_CHAR(lunch_start_time,'HH24:MI') as lunch_start_time from variable_settings_production where company_name='$cbo_company_name' and variable_list=26 and status_active=1 and is_deleted=0");	
								}
								foreach($nameArray as $row)
								{
									$dataArray[$row[csf('shift_id')]]['pst']=$row[csf('prod_start_time')];
									$dataArray[$row[csf('shift_id')]]['lst']=$row[csf('lunch_start_time')];
								}
							}
							//print_r($dataArray);
                            
                            foreach($shift_name as $id=>$name)
                            {
                                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                                
                                $prod_start_time=$dataArray[$id]['pst'];
                                $lunch_start_time=$dataArray[$id]['lst'];
                                $remarks=$dataArray[$id]['rk'];
                                
                                ?>
                                <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" align="center" valign="middle"> 
                                    <td><b><?php echo $name; ?></b></td>
                                    <td>
                                    	<input class="text_boxes" type="text" style="width:140px" name="txt_prod_start_time[]" id="txt_prod_start_time<?php echo $i; ?>" value="<?php echo $prod_start_time; ?>" onBlur="fnc_valid_time(this.value,'txt_prod_start_time<?php echo $i; ?>');" onKeyUp="fnc_valid_time(this.value,'txt_prod_start_time<?php echo $i; ?>');" onKeyPress="return numOnly(this,event,this.id);" maxlength="8"/>
                                    </td>
                                    <td>
                                    	<input class="text_boxes" type="text" style="width:140px" name="txt_lunch_start_time[]" id="txt_lunch_start_time<?php echo $i; ?>" value="<?php echo $lunch_start_time; ?>" onBlur="fnc_valid_time(this.value,'txt_lunch_start_time<?php echo $i; ?>');" onKeyUp="fnc_valid_time(this.value,'txt_lunch_start_time<?php echo $i; ?>');" onKeyPress="return numOnly(this,event,this.id);" maxlength="8"/>
                                      	<input type="hidden"name="shiftId[]" id="shiftId<?php echo $i; ?>" value="<?php echo $id; ?>">
                                    </td>
                                    <td><input class="text_boxes" type="text" style="width:220px" name="txtRemarks[]" id="txtRemarks<?php echo $i; ?>" value="<?php echo $remarks; ?>"/></td>
                                </tr>
                            <?php 
                                $i++;
                            }
                        ?>
                    </tbody>
                </table>
                <table width="620">
                     <tr>
                        <td align="center" >
                            <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                        </td>
                    </tr>
                </table>
            </div>
		</fieldset>
	</form>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="sweing_production_start")
{
	if($db_type==0)
	{
		$nameArray=sql_select("select shift_id,TIME_FORMAT( prod_start_time, '%H:%i' ) as prod_start_time,TIME_FORMAT( lunch_start_time, '%H:%i' ) as lunch_start_time from variable_settings_production where company_name='$data' and variable_list=26 and status_active=1 and is_deleted=0");
	}
	else
	{
		$nameArray=sql_select("select shift_id, TO_CHAR(prod_start_time,'HH24:MI') as prod_start_time, TO_CHAR(lunch_start_time,'HH24:MI') as lunch_start_time from variable_settings_production where company_name='$data' and variable_list=26 and status_active=1 and is_deleted=0");	
	}
	$save_string='';
	foreach($nameArray as $row)
	{
		if($save_string=="") $save_string=$row[csf('shift_id')]."_".$row[csf('prod_start_time')]."_".$row[csf('lunch_start_time')];
		else $save_string.=",".$row[csf('shift_id')]."_".$row[csf('prod_start_time')]."_".$row[csf('lunch_start_time')];
	}
	
	echo "document.getElementById('save_string').value 					= '".$save_string."';\n";
	exit();	
}


?>