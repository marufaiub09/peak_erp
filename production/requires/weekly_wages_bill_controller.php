<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');
extract($_REQUEST);

$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier order by supplier_name",'id','supplier_name');
$buyer_arr=return_library_array( "select id,short_name from lib_buyer order by short_name",'id','short_name');

$subcon_buyer_arr=return_library_array( "select id,cust_buyer from subcon_ord_dtls order by cust_buyer",'id','cust_buyer');




$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$size_arr = return_library_array("select id, size_name from lib_size","id","size_name");

$job_arr = return_library_array("select id, job_no from wo_po_details_master","id","job_no");
$subcon_job_arr = return_library_array("select id,subcon_job from subcon_ord_mst","id","subcon_job");


$po_number_arr = return_library_array("select id, po_number from wo_po_break_down","id","po_number");
$subcon_po_number_arr = return_library_array("select id, order_no from subcon_ord_dtls","id","order_no");

$company_arr = return_library_array("select id, company_name from lib_company order by company_name","id","company_name");


//====================Location ACTION========

//====================SYSTEM ID POPUP========


if ($action=="systemId_popup")
{
	echo load_html_head_contents("System ID Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id)
		{ 
			$('#hidden_mst_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
    
    
</head>

<body>
<div align="center" style="width:840px;">
    <form name="searchsystemidfrm"  id="searchsystemidfrm">
        <fieldset style="width:830px;">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" width="700" border="1" rules="all" class="rpt_table">
                <thead>
                    <th>Buyer Name</th>
                    <th>Job Number</th>
                    <th>Style</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" id="hidden_mst_id">
					</th>
                </thead>
                <tr>
                    <td>
						<?php
							echo create_drop_down( "cbo_buyer_id", 150, $buyer_arr,"", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td>
						<input type="text" style="width:180px;" class="text_boxes"  name="txt_job_no" id="txt_job_no" />
                        </td>
                    <td>
						<input type="text" style="width:180px;" class="text_boxes"  name="txt_style_no" id="txt_style_no" />
                        </td>
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_job_no').value+'_'+document.getElementById('txt_style_no').value+'_'+document.getElementById('cbo_buyer_id').value+'_'+document.getElementById('txt_company_id').value, 'price_rate_list_view', 'search_div', 'piece_rate_work_order_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
            <table width="100%" style="margin-top:5px;">
                <tr>
                    <td colspan="5">
                        <div style="margin-top:10px; margin-left:3px;" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table>
    	</fieldset>
    </form>
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}




if($action=="check_unique")
{
	
	$operation_arr=explode("__",$operation);
	$flag=0;
	foreach($operation_arr as $operation_values)
	{ 
	list($id,$job_no,$buyer_id,$buyer_name,$style_ref_no,$gmts_item_id,$gmts_item,$po_id,$po_number)=explode("**",$operation_values);
	
	$is_duplicate = is_duplicate_field( "id", "piece_rate_wo_dtls", "mst_id='$mst_id' and job_id='$id' and item_id='$gmts_item_id'" );
	
	if($is_duplicate==1){
		if($items=='')$items=$gmts_item; else $items.=' and '.$gmts_item;
		$flag=1;
		}
		else
		{
		$flag=0;
		}
	}
	
	if($flag==1){echo $items;}else{echo 0;}

exit();
}

if($action=="create_job_no_list_view")
{
	
	
	
list($job_no,$style_no,$buyer_id,$company_id,$txt_history,$order_source,$cob_year,$job_ids,$order_ids,$buyer_ids,$item_ids)=explode("_",$data);	


if($order_source==1){

	if($buyer_id==0)$buyer_id="a.buyer_name like('%%')"; else $buyer_id="a.buyer_name =$buyer_id";	
	
	if($job_no=='')$job_no="a.job_no_prefix_num like('%%')"; else $job_no="a.job_no_prefix_num ='$job_no'";	
	if($style_no=='')$style_no="a.style_ref_no like('%%')"; else $style_no="a.style_ref_no='$style_no'";	
	
		if($db_type==0)
		{
		
			if($cob_year=='')$cob_year=""; else $cob_year="and year(a.insert_date)='$cob_year'";	
		
			$sql = "select a.id,a.job_no,a.job_no_prefix_num,a.buyer_name,a.style_ref_no,year(a.insert_date) as year,b.gmts_item_id,c.id as po_id,c.po_number from wo_po_details_master a,wo_po_details_mas_set_details b,wo_po_break_down c where a.job_no=b.job_no and a.job_no=c.job_no_mst and a.company_name=$company_id and $buyer_id and $job_no and $style_no $cob_year and a.status_active=1 and a.is_deleted=0";
		}
		else
		{
			if($cob_year=='')$cob_year=""; else $cob_year="and to_char(a.insert_date,'YYYY')='$cob_year'";	
			
			$sql = "select a.id,a.job_no,a.job_no_prefix_num,a.buyer_name,a.style_ref_no,to_char(a.insert_date,'YYYY') as year,b.gmts_item_id,c.id as po_id,c.po_number from wo_po_details_master a,wo_po_details_mas_set_details b,wo_po_break_down c where a.job_no=b.job_no and a.job_no=c.job_no_mst and a.company_name=$company_id and $buyer_id and $job_no and $style_no $cob_year and a.status_active=1 and a.is_deleted=0"; 
		}

	
	
	
}// echo $sql; 
else
{ 

	if($buyer_id==0)$buyer_id="b.cust_buyer like('%%')"; else $buyer_id="b.cust_buyer =$buyer_id";	
	
	if($job_no=='')$job_no="a.job_no_prefix_num like('%%')"; else $job_no="a.job_no_prefix_num ='$job_no'";	
	if($style_no=='')$style_no="b.cust_style_ref like('%%')"; else $style_no="b.cust_style_ref='$style_no'";	
	
		if($db_type==0)
		{
			if($cob_year=='')$cob_year=""; else $cob_year="and YEAR(a.insert_date)='$cob_year'";	
			$sql = "select
			a.id,
			a.subcon_job as job_no,
			a.job_no_prefix_num,
			b.cust_buyer as buyer_name,
			b.cust_style_ref as style_ref_no,
			year(a.insert_date) as year,
			c.item_id as gmts_item_id,
			b.id as po_id,
			b.order_no as po_number
			 
			 from subcon_ord_mst a,subcon_ord_dtls b,subcon_ord_breakdown c 
			 where 
			a.subcon_job=b.job_no_mst 
			and a.id=c.mst_id 
			and a.company_id=$company_id 
			and $buyer_id 
			and $job_no 
			and $style_no
			$cob_year
			and a.status_active=1 
			and a.is_deleted=0";
		}
		else
		{
			if($cob_year=='')$cob_year=""; else $cob_year="and to_char(a.insert_date,'YYYY')='$cob_year'";	
			$sql = "select
			a.id,
			a.subcon_job as job_no,
			a.job_no_prefix_num,
			b.cust_buyer as buyer_name,
			b.cust_style_ref as style_ref_no,
			to_char(a.insert_date,'YYYY') as year,
			c.item_id as gmts_item_id,
			b.id as po_id,
			b.order_no as po_number
			 
			 from subcon_ord_mst a,subcon_ord_dtls b,subcon_ord_breakdown c 
			 where 
			a.subcon_job=b.job_no_mst 
			and a.id=c.mst_id 
			and a.company_id=$company_id 
			and $buyer_id 
			and $job_no 
			and $style_no
			$cob_year
			and a.status_active=1 
			and a.is_deleted=0"; 
		}
		
	
}


	$result = sql_select($sql);

	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table">
        <thead>
            <th width="50">SL</th>
            <th width="60">Year</th>
            <th width="120">Job No</th>
            <th width="100">Order</th>
            <th width="140">Buyer</th>
            <th width="150">Item</th>
            <th>Style</th>
        </thead>
	</table>
	<div style="width:820px; max-height:230px; overflow-y:scroll" id="list_container_batch" align="left">	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table" id="tbl_list_search">  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                $bgcolor=($i%2==0)?"#E9F3FF":"#FFFFFF";	 
				
				if($order_source==1){
				$send_data=$row[csf('id')]."**".$row[csf('job_no')]."**".$row[csf('buyer_name')]."**".$buyer_arr[$row[csf('buyer_name')]]."**".$row[csf('style_ref_no')]."**".$row[csf('gmts_item_id')]."**".$garments_item[$row[csf('gmts_item_id')]]."**".$row[csf('po_id')]."**".$row[csf('po_number')];	
				}
				else
				{
				$send_data=$row[csf('id')]."**".$row[csf('job_no')]."**".$row[csf('id')]."**".$row[csf('buyer_name')]."**".$row[csf('style_ref_no')]."**".$row[csf('gmts_item_id')]."**".$garments_item[$row[csf('gmts_item_id')]]."**".$row[csf('po_id')]."**".$row[csf('po_number')];	
				}
				
				$check_data=$job_ids.'**'.$order_ids.'**'.$buyer_ids.'**'.$item_ids;	
        	?>
                <tr id="tr_<?php echo $row[csf('id')].$i; ?>" bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $send_data; ?>','<?php echo $row[csf('id')].$i; ?>','<?php echo $check_data; ?>');"> 
                    <td width="50" align="center"><?php echo $i; ?></td>
                    <td width="60" align="center"><p><?php echo $row[csf('year')]; ?></p></td>
                    <td width="120" align="center"><p><?php echo $row[csf('job_no_prefix_num')]; ?></p></td>
                    <td width="100"><p><?php echo $row[csf('po_number')]; ?></p></td>
                    <td width="140"><p><?php if($order_source==1)echo $buyer_arr[$row[csf('buyer_name')]]; else echo $row[csf('buyer_name')]; ?></p></td>
                    <td width="150"><p><?php echo $garments_item[$row[csf('gmts_item_id')]]; ?></p></td>
                    <td><p><?php echo $row[csf('style_ref_no')]; ?></p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
    </div>
        <table>
            <tr>
                <td>
                    <input type="hidden" name="txt_selected_id" id="txt_selected_id" value="">
                    <input type="button" value="Close" class="formbutton" onClick="close_popup();" />
                </td>
            </tr>
        </table>
<?php


exit();
}



if($action=='populate_price_rat_mst_form_data')
{
	
	
	$sql = "select id,sys_number, company_id, service_provider_id, wo_date, rate_for, attension, currence, exchange_rate, remarks from piece_rate_wo_mst where id=$data and status_active=1 and is_deleted=0"; 
	
	$data_array=sql_select($sql);
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('update_id').value					= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_system_id').value				= '".$row[csf("sys_number")]."';\n";
		echo "document.getElementById('cbo_company_id').value				= '".$row[csf("company_id")]."';\n";
		echo "document.getElementById('txt_service_provider_name').value	= '".$supplier_arr[$row[csf("service_provider_id")]]."';\n";
		echo "document.getElementById('txt_service_provider_id').value		= '".$row[csf("service_provider_id")]."';\n";
		echo "document.getElementById('txt_wo_date').value					= '".change_date_format($row[csf("wo_date")])."';\n";
		echo "document.getElementById('cbo_rate_for').value					= '".$row[csf("rate_for")]."';\n";
		echo "document.getElementById('txt_attention').value				= '".$row[csf("attension")]."';\n";
		echo "document.getElementById('cbo_currency').value					= '".$row[csf("currence")]."';\n";
		echo "document.getElementById('txt_exchange_rate').value			= '".$row[csf("exchange_rate")]."';\n";
		echo "document.getElementById('txt_remarks_mst').value				= '".$row[csf("remarks")]."';\n";

		exit();
	}
	
	
	
}






if ($action=="save_update_delete")
{

$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id=return_next_id( "id", "piece_rate_terms_condition ", 1 ) ;
		 $field_array="id,mst_id,terms";
		 for ($i=1;$i<=$total_row;$i++)
		 {
			 $termscondition="termscondition_".$i;
			if ($i!=1) $data_array .=",";
			$data_array .="(".$id.",".$txt_mst_id.",".$$termscondition.")";
			$id=$id+1;
		 }
		// echo  $data_array;
		$rID_de3=execute_query( "delete from piece_rate_terms_condition where  mst_id =".$txt_mst_id."",0);
		$rID=sql_insert("piece_rate_terms_condition",$field_array,$data_array,1);
		
		 check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$new_booking_no[0];
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_booking_no[0];
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);
				echo "0**".$new_booking_no[0];
			}
			else{
				oci_rollback($con);
				echo "10**".$new_booking_no[0];
			}
		}
		disconnect($con);
		die;
	}	

	
	
exit();
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		
		$flag=1;
		if(str_replace("'","",$update_id)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			
			
// master part--------------------------------------------------------------;
			$price_rate_wo_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'PRWO', date("Y",time()), 5, "select sys_number_prefix, sys_number_prefix_num from piece_rate_wo_mst where company_id=$cbo_company_id and $year_cond=".date('Y',time())." order by id desc", "sys_number_prefix", "sys_number_prefix_num" ));
		 	
			$id=return_next_id( "id", "piece_rate_wo_mst", 1 ) ;
			$field_array_mst="id,sys_number_prefix,sys_number_prefix_num,sys_number,company_id,service_provider_id,wo_date,rate_for,attension,currence,exchange_rate,remarks,inserted_by,insert_date,status_active,is_deleted";
			
			$data_array_mst="(".$id.",'".$price_rate_wo_system_id[1]."',".$price_rate_wo_system_id[2].",'".$price_rate_wo_system_id[0]."',".$cbo_company_id.",".$txt_service_provider_id.",".$txt_wo_date.",".$cbo_rate_for.",".$txt_attention.",".$cbo_currency.",".$txt_exchange_rate.",".$txt_remarks_mst.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
			
// details part--------------------------------------------------------------;

			$field_array_dtls="id, mst_id, order_source,ord_recev_company, job_id, order_id, buyer_id, item_id, style_ref,color_type, wo_qty,uom, avg_rate,amount, remarks, inserted_by, insert_date,status_active,is_deleted";
			$field_array_wo_dtls="id, dtls_id, color_id, size_id, order_qty, wo_qty, rate, uom,final_wo_qty, inserted_by, insert_date,status_active,is_deleted";
			$id_dtls=return_next_id( "id", "piece_rate_wo_dtls", 1 ) ;
			$id_wo_dtls=return_next_id( "id", "piece_rate_wo_qty_dtls", 1 ) ;
			
			$tot_rows= str_replace("'","",$tot_rows);
			
			for($i=1; $i<=$tot_rows; $i++)
			{
			$cbo_order_source='cbo_order_source_'.$i;
			$cbo_ord_rceve_comp_id='cbo_ord_rceve_comp_id_'.$i;
			$txtjobid='txtjobid_'.$i;
			$txtorderid='txtorderid_'.$i;
			
			$txtbuyerid='txtbuyerid_'.$i;
			$txtitemid='txtitemid_'.$i;
			$txtstyle='txtstyle_'.$i;
			$colortype='colortype_'.$i;
			
			$txtwoqty='txtwoqty_'.$i;
			$txtavgrate='txtavgrate_'.$i;
			$txtremarks='txtremarks_'.$i;
			
			$cbodtlsuom='cbodtlsuom_'.$i;
			$txtdtlamount='txtdtlamount_'.$i;
			
				if($i==1)
				{
					if(str_replace("'",'',$$txtwoqty)!=""){
					$data_array_dtls="(".$id_dtls.",".$id.",".$$cbo_order_source.",".$$cbo_ord_rceve_comp_id.",".$$txtjobid.",".$$txtorderid.",".$$txtbuyerid.",".$$txtitemid.",".$$txtstyle.",".$$colortype.",".$$txtwoqty.",".$$cbodtlsuom.",".$$txtavgrate.",".$$txtdtlamount.",".$$txtremarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
					}
				}
				else
				{
					if(str_replace("'",'',$$txtwoqty)!=""){
					$data_array_dtls.=", (".$id_dtls.",".$id.",".$$cbo_order_source.",".$$txtjobid.",".$$txtorderid.",".$$txtbuyerid.",".$$txtitemid.",".$$txtstyle.",".$$colortype.",".$$txtwoqty.",".$$cbodtlsuom.",".$$txtavgrate.",".$$txtdtlamount.",".$$txtremarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
					
					}
				}
			
			// order dtls start --------------------------
				$order_qty_history='txt_order_qty_history_'.$i;
				list($dtls_history,$order_qty,$wo_qty,$order_rate,$order_uom,$final_wo_qty,$order_color,$order_size)=explode("~~",$$order_qty_history);
				
				 //echo "insert into piece_rate_wo_dtls ($field_array_dtls) values($data_array_dtls)"; die;
				
				$dtls_history=explode("__",$dtls_history);
				$wo_qty=explode(",",str_replace("'","",$wo_qty));
				$order_rate=explode(",",str_replace("'","",$order_rate));
				$order_uom=explode(",",str_replace("'","",$order_uom));
				$final_wo_qty=explode(",",str_replace("'","",$final_wo_qty));
				$order_color=explode(",",str_replace("'","",$order_color));
				$order_size=explode(",",str_replace("'","",$order_size));
				$order_qty=explode(",",str_replace("'","",$order_qty));


 //echo $txt_order_qty_history_3; die;



					for($di=0; $di<$dtls_history[6]; $di++)
					{
						
						if($di==0 && $i==1)
						{
						$data_array_wo_dtls="(".$id_wo_dtls.",".$id_dtls.",".$order_color[$di].",".$order_size[$di].",".$order_qty[$di].",".$wo_qty[$di].",'".$order_rate[$di]."',".$order_uom[$di].",".$final_wo_qty[$di].",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
						}
						else
						{
						$data_array_wo_dtls.=",(".$id_wo_dtls.",".$id_dtls.",".$order_color[$di].",".$order_size[$di].",".$order_qty[$di].",".$wo_qty[$di].",'".$order_rate[$di]."',".$order_uom[$di].",".$final_wo_qty[$di].",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
						}
						
						
						$id_wo_dtls++;
					}
				
			//order dtls end -----------------------------------
				$id_dtls++;

			}


		}
		
		

		$rID1=sql_insert("piece_rate_wo_mst",$field_array_mst,$data_array_mst,0);
		if($flag==1) 
		{
			if($rID1) $flag=1; else $flag=0; 
		} 

		$rID2=sql_insert("piece_rate_wo_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		
		$rID3=sql_insert("piece_rate_wo_qty_dtls",$field_array_wo_dtls,$data_array_wo_dtls,0);
		if($flag==1) 
		{
			if($rID3) $flag=1; else $flag=0; 
		} 
		
	
		
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$id."**".$price_rate_wo_system_id[0]."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**0**"."&nbsp;"."**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "0**".$id."**".$price_rate_wo_system_id[0]."**0";
			}
			else
			{
				oci_rollback($con);
				echo "10**0**"."&nbsp;"."**0";
			}
		}
		
				
		disconnect($con);
		die;
	}
	
	else if ($operation==1)   // Update Here
	{ 
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			$flag=1;
	//echo "1**".$update_id;die;
	
			
			$field_array_mst="company_id*service_provider_id*wo_date*rate_for*attension*currence*exchange_rate*remarks*updated_by*update_date";
			$data_array_mst="".$cbo_company_id."*".$txt_service_provider_id."*".$txt_wo_date."*".$cbo_rate_for."*".$txt_attention."*".$cbo_currency."*".$txt_exchange_rate."*".$txt_remarks_mst."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
//-----------------------------------------------------			
			$field_array_dtls_up="order_source*ord_recev_company*job_id*order_id*buyer_id*item_id*style_ref*color_type*wo_qty*avg_rate*amount*remarks*updated_by*update_date";
			$field_array_dtls="id, mst_id, order_source,ord_recev_company,job_id, order_id, buyer_id, item_id, style_ref,color_type,wo_qty, uom, avg_rate, amount, remarks, inserted_by, insert_date, status_active, is_deleted";
			$id_dtls=return_next_id( "id", "piece_rate_wo_dtls", 1 ) ;
			$wo_next_id=return_next_id( "id", "piece_rate_wo_qty_dtls", 1 ) ; 
			
			$tot_rows=str_replace("'","",$tot_rows);
			$f=1;$df=1;

			for($i=1; $i<=$tot_rows; $i++)
			{

			$cbo_order_source='cbo_order_source_'.$i;
			$cbo_ord_rceve_comp_id='cbo_ord_rceve_comp_id_'.$i;
			$txtjobid='txtjobid_'.$i;
			$txtorderid='txtorderid_'.$i;
			
			$txtbuyerid='txtbuyerid_'.$i;
			$txtitemid='txtitemid_'.$i;
			$txtstyle='txtstyle_'.$i;
			$colortype='colortype_'.$i;
			$txtwoqty='txtwoqty_'.$i;
			$txtavgrate='txtavgrate_'.$i;
			$txtremarks='txtremarks_'.$i;
			
			$cbodtlsuom='cbodtlsuom_'.$i;
			$txtdtlamount='txtdtlamount_'.$i;
			$details_update_id='details_update_id_'.$i;
			
			$cbo_order_source=str_replace("'",'',$$cbo_order_source);
			$cbo_ord_rceve_comp_id=str_replace("'",'',$$cbo_ord_rceve_comp_id);
			$txtjobid=str_replace("'","",$$txtjobid);
			$txtorderid=str_replace("'","",$$txtorderid);
			$txtbuyerid=str_replace("'","",$$txtbuyerid);
			$txtitemid=str_replace("'","",$$txtitemid);
			$txtstyle=str_replace("'","",$$txtstyle);
			$colortype=str_replace("'","",$$colortype);
			$txtwoqty=str_replace("'","",$$txtwoqty);
			$txtavgrate=str_replace("'","",$$txtavgrate);
			$txtremarks=str_replace("'","",$$txtremarks);
			$cbodtlsuom=str_replace("'","",$$cbodtlsuom);
			$txtdtlamount=str_replace("'","",$$txtdtlamount);
			
			
					if(str_replace("'","",$$details_update_id)!="")
					{
					//this is for update dels
					$update_dtls_id[]=str_replace("'","",$$details_update_id);
					$data_array_dtls_up[str_replace("'","",$$details_update_id)] =explode("*",("'".$cbo_order_source."'*'".$cbo_ord_rceve_comp_id."'*'".$txtjobid."'*'".$txtorderid."'*'".$txtbuyerid."'*'".$txtitemid."'*'".$txtstyle."'*'".$colortype."'*'".$txtwoqty."'*'".$txtavgrate."'*'".$txtdtlamount."'*'".$txtremarks."'*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
					}
					else
					{
					//this is for news insert dels	
						if($txtwoqty!="")
						{ 
							if($data_array_dtls=='')
							{
								$data_array_dtls="('".$id_dtls."',".$update_id.",'".$cbo_order_source."','".$cbo_ord_rceve_comp_id."','".$txtjobid."','".$txtorderid."','".$txtbuyerid."','".$txtitemid."','".$txtstyle."','".$colortype."','".$txtwoqty."','".$cbodtlsuom."','".$txtavgrate."','".$txtdtlamount."','".$txtremarks."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
							}
							else
							{
								$data_array_dtls.=",('".$id_dtls."',".$update_id.",'".$cbo_order_source."','".$txtjobid."','".$txtorderid."','".$txtbuyerid."','".$txtitemid."','".$txtstyle."','".$colortype."','".$txtwoqty."','".$cbodtlsuom."','".$txtavgrate."','".$txtdtlamount."','".$txtremarks."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
							}
								
							$f++;
						}
					}
	
			
//-----------------------------------------------------			
			
			//bulk_update_sql_statement
	
			$field_array_wo_dtls_up="color_id*size_id*wo_qty*rate*uom*final_wo_qty*updated_by*update_date";
			$field_array_wo_dtls="id, dtls_id, color_id, size_id, order_qty, wo_qty, rate, uom,final_wo_qty, inserted_by, insert_date,status_active,is_deleted";
			
			$txt_order_qty_history="txt_order_qty_history_".$i;  
			list($dtls_history,$order_qty,$wo_qty,$order_rate,$order_uom,$final_wo_qty,$order_color,$order_size,$up_ids)=explode("~~",$$txt_order_qty_history);
			
			
			$dtls_history=explode("__",$dtls_history);
			$up_id_arr=explode(",",str_replace("'","",$up_ids));
			$wo_qty=explode(",",str_replace("'","",$wo_qty));
			$order_rate=explode(",",str_replace("'","",$order_rate));
			$order_uom=explode(",",str_replace("'","",$order_uom));
			$final_wo_qty=explode(",",str_replace("'","",$final_wo_qty));
			$order_color=explode(",",str_replace("'","",$order_color));
			$order_size=explode(",",str_replace("'","",$order_size));
		 	$order_qty=explode(",",str_replace("'","",$order_qty));
		
		 
		
		   for($s=0; $s < $dtls_history[6]; $s++)
			{   
				if($up_id_arr[$s])
				{
				$id_arr[]=str_replace("'","",$up_id_arr[$s]);
				$data_array_wo_dtls_up[str_replace("'","",$up_id_arr[$s])] = explode("*",("'".$order_color[$s]."'*'".$order_size[$s]."'*'".$wo_qty[$s]."'*'".$order_rate[$s]."'*'".$order_uom[$s]."'*'".$final_wo_qty[$s]."'*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				}
				else
				{ 
				$wo_next_id+=1;
				// news inser wo rate data;	
				
					if($data_array_wo_dtls=='')
					{ 
					$data_array_wo_dtls="(".$wo_next_id.",".$id_dtls.",'".$order_color[$s]."','".$order_size[$s]."','".$order_qty[$s]."','".$wo_qty[$s]."','".$order_rate[$s]."','".$order_uom[$s]."','".$final_wo_qty[$s]."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
					}
					else
					{  
					$data_array_wo_dtls.=",(".$wo_next_id.",".$id_dtls.",'".$order_color[$s]."','".$order_size[$s]."','".$order_qty[$s]."','".$wo_qty[$s]."','".$order_rate[$s]."','".$order_uom[$s]."','".$final_wo_qty[$s]."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
					}
					
				}
			
			}
			if(str_replace("'","",$$details_update_id)==""){$id_dtls++;}
		}

//echo $data_array_wo_dtls; die;
	 	  //echo $txt_order_qty_history_3; die;
		 
		   //print_r($data_array_wo_dtls);  die; 
			 
			
			$rID1=sql_update("piece_rate_wo_mst",$field_array_mst,$data_array_mst,"id","".$update_id."",1);
			if($flag==1) 
			{
				if($rID1) $flag=1; else $flag=0; 
			} 
			
			$rID2=execute_query(bulk_update_sql_statement("piece_rate_wo_dtls", "id",$field_array_dtls_up,$data_array_dtls_up,$update_dtls_id ));
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
			//news insert;
			if($data_array_dtls!=''){$rID2_insert=sql_insert("piece_rate_wo_dtls",$field_array_dtls,$data_array_dtls,0);}
			if($data_array_dtls!='') 
			{
				if($rID2_insert) $flag=1; else $flag=0; 
			} 
			
			
		
			
			$rID3=execute_query(bulk_update_sql_statement("piece_rate_wo_qty_dtls", "id",$field_array_wo_dtls_up,$data_array_wo_dtls_up,$id_arr )); 
	
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
		 	
			// new insert;
			if($data_array_wo_dtls!=''){$rID3_insert=sql_insert("piece_rate_wo_qty_dtls",$field_array_wo_dtls,$data_array_wo_dtls,0);}
			if($data_array_wo_dtls!='') 
			{
				if($rID3_insert) $flag=1; else $flag=0; 
			} 
			
			
			
			
			if($db_type==0)
			{
				if($flag==1)
				{
					mysql_query("COMMIT");  
					echo "1**".str_replace("'", '', $update_id)."**".str_replace("'", '', $txt_system_id)."**0";
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**0**0**1";
				}
			}
			else if($db_type==2 || $db_type==1 )
			{
				if($flag==1)
				{
					oci_commit($con);  
					echo "1**".str_replace("'", '', $update_id)."**".str_replace("'", '', $txt_system_id)."**0";
				}
				else
				{
					oci_rollback($con);
					echo "10**0**0**1";
				}
			}
			disconnect($con);
			die;
			
		
	
	}
}









?>