<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$costing_per_id_library=return_library_array( "select job_no, costing_per from wo_pre_cost_mst", "job_no", "costing_per");
$consumtion_library=return_library_array( "select job_no, avg_finish_cons from wo_pre_cost_fabric_cost_dtls", "job_no", "avg_finish_cons");
if($db_type==0)  $select_groupcon="group";	
if($db_type==2) $select_groupcon="wm"; 
else $select_groupcon=""; 

if($action=="load_drop_down_buyer")
{
	$data=explode("_",$data);
	if($data[1]==1) $party="1,3,21,90"; else $party="80";
	echo create_drop_down( "cbo_buyer_id", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[0]' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in ($party)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $selected, "","" );
	exit();
}

if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#hide_job_id').val( id );
		$('#hide_job_no').val( ddd );
	}		
	
/*		function js_set_value(str)
		{
			var splitData = str.split("_");
			//alert (splitData[1]);
			$("#hide_job_id").val(splitData[0]); 
			$("#hide_job_no").val(splitData[1]); 
			parent.emailwindow.hide();
		}
*/    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search Job</th>
                    <th>Search Style</th>
                    <!--<th>Search Order</th>-->
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:70px;" onClick="reset_form('styleRef_form','search_div','','','','');"></th> 					<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                    <input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_job" id="txt_search_job" placeholder="Job No" />	
                        </td>
                        <td align="center">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_style" id="txt_search_style" placeholder="Style Ref." />	
                        </td>     
                        <!--<td align="center">				
                            <input type="text" style="width:80px" class="text_boxes" name="txt_search_order" id="txt_search_order" placeholder="Order No" />	
                        </td> +'**'+document.getElementById('txt_search_order').value-->	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('txt_search_job').value+'**'+document.getElementById('txt_search_style').value+'**'+'<?php echo $cbo_year_id; ?>', 'create_job_no_search_list_view', 'search_div', 'order_wise_finish_fabric_stock_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:70px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_job_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	$year_id=$data[4];
	//$month_id=$data[5];
	//echo $month_id;
	
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and buyer_name=$data[1]";
	}
	
	if($data[2]!='') $job_cond=" and job_no_prefix_num=$data[2]"; else $job_cond="";
	if($data[3]!='') $style_cond=" and style_ref_no like '$data[3]'"; else $style_cond="";
	//if($data[4]!='') $order_cond=" and po_number like '$data[4]'"; else $order_cond="";
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	//$year="year(insert_date)";
	if($db_type==0) $year_field_by="year(insert_date)"; 
	else if($db_type==2) $year_field_by="to_char(insert_date,'YYYY')";
	else $year_field_by="";

	if($year_id!=0) $year_cond=" and $year_field_by='$year_id'"; else $year_cond="";
	//if($month_id!=0) $month_cond="$month_field_by=$month_id"; else $month_cond="";
	$arr=array (0=>$buyer_arr);
	$sql= "select id, job_no, job_no_prefix_num, buyer_name, style_ref_no, $year_field_by as year from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id $buyer_id_cond $job_cond $style_cond $year_cond order by id DESC";
		
	echo create_list_view("tbl_list_search", "Buyer Name,Job No,Year,Style Ref. No", "170,130,80,60","610","270",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "buyer_name,0,0,0", $arr , "buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0','',1) ;
   exit(); 
} 

if ($action=="order_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
    <script>
	function js_set_value(str)
	{
		var splitData = str.split("_");
		//alert (splitData[1]);
		$("#order_no_id").val(splitData[0]); 
		$("#order_no_val").val(splitData[1]); 
		parent.emailwindow.hide();
	}
	</script>
     <input type="hidden" id="order_no_id" />
     <input type="hidden" id="order_no_val" />
 <?php
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and b.buyer_name=$data[1]";
	if ($data[2]=="") $order_no=""; else $order_no=" and a.po_number=$data[2]";
	$job_no=str_replace("'","",$txt_job_id);
	if($db_type==0)  $find_inset=" and FIND_IN_SET(b.job_no_prefix_num,'$data[2]')";	
	if($db_type==2 || $db_type==1)$find_inset=" and b.job_no_prefix_num in($data[2]) "; 
	if ($data[2]=="") $job_no_cond=""; else $job_no_cond="  $find_inset";
	$sql="select a.id, a.po_number, b.job_no_prefix_num, b.job_no, b.buyer_name, b.style_ref_no from wo_po_details_master b, wo_po_break_down a  where b.job_no=a.job_no_mst and b.company_name=$data[0] and b.is_deleted=0 $buyer_name $job_no_cond ORDER BY b.job_no";
	//echo $sql;
	$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$arr=array(1=>$buyer);
	
	echo  create_list_view("list_view", "Job No,Buyer,Style Ref.,Order No", "110,110,150,180","610","350",0, $sql, "js_set_value", "id,po_number", "", 1, "0,buyer_name,0,0,0", $arr , "job_no_prefix_num,buyer_name,style_ref_no,po_number", "order_wise_finish_fabric_stock_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
	disconnect($con);
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_report_type=str_replace("'","",$cbo_report_type);
	//echo $cbo_report_type;die;
	if(str_replace("'","",$cbo_buyer_id)!="" && str_replace("'","",$cbo_buyer_id)!=0) $buyer_id_cond=" and a.buyer_name=$cbo_buyer_id";
	$job_no=str_replace("'","",$txt_job_no);
	if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in ($job_no) ";
	//$year_id=str_replace("'","",$cbo_year);
	//if($year_id!=0) $year_cond=" and year(a.insert_date)=$year_id"; else $year_cond="";
	$order_no=str_replace("'","",$txt_order_id);
	if(str_replace("'","",$txt_order_id)!="" && str_replace("'","",$txt_order_id)!=0) $order_id_cond=" and b.id in ($order_no)";
	
	$date_from=str_replace("'","",$txt_date_from);
	if( $date_from=="") $receive_date=""; else $receive_date= " and d.receive_date <=".$txt_date_from."";
	//==================================================
	if(str_replace("'","",$cbo_buyer_id)!="" && str_replace("'","",$cbo_buyer_id)!=0) $buyer_id_cond_trans=" and d.buyer_name=$cbo_buyer_id";
	$job_no=str_replace("'","",$txt_job_no);
	if ($job_no=="") $job_no_cond_trans=""; else $job_no_cond_trans=" and d.job_no_prefix_num in ($job_no) ";

	//if($year_id!=0) $year_cond_trans=" and year(d.insert_date)=$year_id"; else $year_cond_trans="";
	$order_no=str_replace("'","",$txt_order_id);
	if(str_replace("'","",$txt_order_id)!="" && str_replace("'","",$txt_order_id)!=0) $order_id_cond_trans=" and c.id in ($order_no)";
	$date_from=str_replace("'","",$txt_date_from);
	if( $date_from=="") $receive_date_trans=""; else $receive_date_trans= " and a.transfer_date <=".$txt_date_from."";
	if($cbo_report_type==1) // Knit Finish Start
	{
		$product_array=array();
		$sql_product="select id, color from product_details_master where item_category_id=2 and status_active=1 and is_deleted=0";
		$sql_product_result=sql_select($sql_product);
		foreach( $sql_product_result as $row )
		{
			$product_array[$row[csf('id')]]=$row[csf('color')];
		}
	
		$transfer_in_arr=array();
		$sql_transfer_in="select a.id, a.to_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_in_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=2 group by a.to_order_id,b.from_prod_id,a.id";
		$data_transfer_in_array=sql_select($sql_transfer_in);
		if(count($data_transfer_in_array)>0)
		{
			foreach( $data_transfer_in_array as $row )
			{
				$transfer_in_arr[$row[csf('to_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_in_qnty')];
			}
		}
		
		$transfer_out_arr=array(); 
		$sql_transfer_out="select a.id, a.from_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_out_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=2 group by a.from_order_id,b.from_prod_id,a.id";
		$data_transfer_out_array=sql_select($sql_transfer_out);
		if(count($data_transfer_out_array)>0)
		{
			foreach( $data_transfer_out_array as $row )
			{
				$transfer_out_arr[$row[csf('from_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_out_qnty')];
			}
		}//var_dump ($transfer_in_arr);
		
		
		$issue_qnty=array();
		$sql_issue=sql_select(" select b.po_breakdown_id,b.color_id, sum(b.quantity) as issue_qnty  from inv_finish_fabric_issue_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and b.entry_form=18 group by b.po_breakdown_id,b.color_id");
		foreach( $sql_issue as $row_iss )
		{
			$issue_qnty[$row_iss[csf('po_breakdown_id')]][$row_iss[csf('color_id')]]['issue_qnty']=$row_iss[csf('issue_qnty')];
		}
		$prod_color_arr=return_library_array( "select id, color from product_details_master", "id", "color"  );
		$iss_return_qnty=array();
		$sql_issue_ret=sql_select("select po_breakdown_id, prod_id, sum(quantity) as issue_ret_qnty  from order_wise_pro_details where trans_id!=0 and status_active=1 and is_deleted=0 and entry_form=52 group by po_breakdown_id, prod_id");
		foreach( $sql_issue_ret as $row )
		{
			$iss_return_qnty[$row[csf('po_breakdown_id')]][$prod_color_arr[$row[csf('prod_id')]]]['issue_ret_qnty']=$row[csf('issue_ret_qnty')];
		}
		//print_r($iss_return_qnty);
		$rec_return_qnty=array();
		$sql_rec_ret=sql_select("select po_breakdown_id, prod_id, sum(quantity) as rec_ret_qnty  from order_wise_pro_details where trans_id!=0 and status_active=1 and is_deleted=0 and entry_form=46 group by po_breakdown_id, prod_id");
		foreach( $sql_rec_ret as $row )
		{
			$rec_return_qnty[$row[csf('po_breakdown_id')]][$prod_color_arr[$row[csf('prod_id')]]]['rec_ret_qnty']=$row[csf('rec_ret_qnty')];
		}
		
		$booking_qnty=array();
		$sql_booking=sql_select(" select b.po_break_down_id,b.fabric_color_id, sum(b.fin_fab_qnty ) as fin_fab_qnty  from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2  and a.booking_type=1 and a.is_deleted=0 and a.status_active=1 and b.booking_type=1 and b.status_active=1 and b.is_deleted=0 group by b.po_break_down_id,b.fabric_color_id");
		foreach( $sql_booking as $row_book)
		{
			$booking_qnty[$row_book[csf('po_break_down_id')]][$row_book[csf('fabric_color_id')]]['fin_fab_qnty']=$row_book[csf('fin_fab_qnty')];
		}
		
		$actual_cut_qnty=array();
		$sql_actual_cut_qty=sql_select(" select a.po_break_down_id,c.color_number_id, sum(b.production_qnty) as actual_cut_qty  from pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c where a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.production_type=1 and a.is_deleted=0 and a.status_active=1  group by  a.po_break_down_id,c.color_number_id");
		foreach( $sql_actual_cut_qty as $row_actual)
		{
			$actual_cut_qnty[$row_actual[csf('po_break_down_id')]][$row_actual[csf('color_number_id')]]['actual_cut_qty']=$row_actual[csf('actual_cut_qty')];
		} 
		
		$result_consumtion=array();
		//$sql_consumtiont_qty=sql_select(" select b.job_no, b.po_break_down_id,b.color_number_id, sum( b.cons ), count( b.color_number_id ) AS conjunction, pcs   from wo_pre_cos_fab_co_avg_con_dtls b, wo_pre_cost_fabric_cost_dtls a WHERE a.id=b.pre_cost_fabric_cost_dtls_id and b.po_break_down_id=4164 GROUP BY b.po_break_down_id,b.color_number_id, pcs,b.job_no ");
		//echo " select b.job_no, b.po_break_down_id,b.color_number_id, sum( b.cons ) / count( b.color_number_id ) AS conjunction, pcs   from wo_pre_cos_fab_co_avg_con_dtls b, wo_pre_cost_fabric_cost_dtls a WHERE a.id=b.pre_cost_fabric_cost_dtls_id and b.po_break_down_id=2880 GROUP BY b.po_break_down_id,b.color_number_id, pcs,b.job_no ";
		$sql_consumtiont_qty=sql_select("select b.po_break_down_id, a.color_number_id, b.cons/b.pcs as requirment, a.plan_cut_qnty
							  from wo_po_color_size_breakdown a, wo_pre_cos_fab_co_avg_con_dtls b, wo_pre_cost_fabric_cost_dtls c
							  where a.po_break_down_id=b.po_break_down_id and a.color_number_id=b.color_number_id  and b.pre_cost_fabric_cost_dtls_id=c.id and a.job_no_mst=c.job_no and a.item_number_id=c.item_number_id and a.is_deleted=0 and a.status_active=1 and b.pcs>0 group by b.po_break_down_id, a.color_number_id, b.cons, b.pcs, a.plan_cut_qnty");
		foreach($sql_consumtiont_qty as $row_consum)
		{
			$result_consumtion[$row_consum[csf('po_break_down_id')]][$row_consum[csf('color_number_id')]]+=$row_consum[csf('requirment')]*$row_consum[csf('plan_cut_qnty')];
		} 
							  
		$sql_plan_cut_qty=sql_select(" select po_break_down_id, color_number_id, sum(plan_cut_qnty) as plan_cut_qnty, sum(order_quantity) as  order_quantity  from wo_po_color_size_breakdown where status_active=1 and is_deleted =0 group by po_break_down_id, color_number_id, item_number_id");
		$plan_cut_array=array();
		foreach($sql_plan_cut_qty as $row)
		{
			$plan_cut_array[$row[csf('po_break_down_id')]][$row[csf('color_number_id')]]=$row[csf('plan_cut_qnty')];
		} 
	
	ob_start();
	?>
    <fieldset style="width:1235px;">
        <table cellpadding="0" cellspacing="0" width="1210">
            <tr class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:18px"><strong><?php echo $report_title; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:14px"><strong> <?php if($date_from!="") echo "Upto : ".change_date_format(str_replace("'","",$txt_date_from)) ;?></strong></td>
            </tr>
        </table>
		<table width="1210" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
            <thead>
                <th width="30">SL</th>
                <th width="100">Buyer</th>
                <th width="90">Job</th>
                <th width="80">Order</th>
                <th width="100">Style</th> 
                <th width="80">Order Qty (Pcs)</th>
                <th width="90">Color</th>
                
                <th width="80">Req. Qty</th>
                <th width="80">Total Received</th>
                <th width="80">Received Balance</th>
                <th width="80">Total Issued</th>
                
                <th width="80">Stock</th>
                <th width="80">Consumption Pcs.</th>
                <th width="80">Possible Cut Pcs.</th>
                <th width="">Actual Cut</th>
            </thead>
        </table>
        <div style="width:1230px; max-height:350px; overflow-y:scroll;" id="scroll_body">
			<table width="1210" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="table_body" > 
            <?php
			if($cbo_report_type==1)// Knit Finish Start
			{
				if($db_type==0)
				{
					$sql_query="Select a.id, a.company_name, a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, a.total_set_qnty as ratio, b.id as po_id, b.po_number, b.plan_cut as po_quantity, 
					sum( e.quantity ) as receive_qnty,  e.color_id, group_concat(c.prod_id) as prod_id
					from  wo_po_details_master a, wo_po_break_down b, pro_finish_fabric_rcv_dtls c, inv_receive_master d, order_wise_pro_details e
					where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1
					and d.entry_form in (7,37) and d.item_category=2 and c.status_active=1 and c.is_deleted=0 and c.mst_id=d.id and d.status_active=1 and d.is_deleted=0 and e.po_breakdown_id=b.id and c.id=e.dtls_id and e.entry_form in (7,37) and e.trans_id!=0 and
					a.company_name=$cbo_company_id $receive_date $buyer_id_cond $job_no_cond $order_id_cond  group by a.job_no, b.po_number, e.color_id order by a.buyer_name, a.job_no, b.po_number, e.color_id";
				}
				else if($db_type==2)
				{
					$sql_query="Select a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, sum(a.total_set_qnty) as ratio, b.id as po_id, b.po_number, 
					b.plan_cut as po_quantity, 
					sum(e.quantity) as receive_qnty, e.color_id, 
					listagg(cast(c.prod_id as varchar2(4000)),',') within group (order by c.prod_id) as prod_id
					from  wo_po_details_master a, wo_po_break_down b, pro_finish_fabric_rcv_dtls c, inv_receive_master d, order_wise_pro_details e
					where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1
					and d.entry_form in (7,37) and d.item_category=2 and c.status_active=1 and c.is_deleted=0 and c.mst_id=d.id and d.status_active=1 and d.is_deleted=0 and e.po_breakdown_id=b.id and c.id=e.dtls_id and e.entry_form in (7,37) and e.trans_id!=0 and
					a.company_name=$cbo_company_id $receive_date $buyer_id_cond $job_no_cond $order_id_cond group by a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, b.id, b.po_number, e.color_id, b.plan_cut order by a.buyer_name, a.job_no, b.po_number, e.color_id";	
				}
			}
			
			//echo $sql_query;
			$i=1; 
			$nameArray=sql_select( $sql_query );
			foreach ($nameArray as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$dzn_qnty=0;
				if($costing_per_id_library[$row[csf('job_no')]]==1)
				{
					$dzn_qnty=12;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==3)
				{
					$dzn_qnty=12*2;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==4)
				{
					$dzn_qnty=12*3;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==5)
				{
					$dzn_qnty=12*4;
				}
				else
				{
					$dzn_qnty=1;
				}
				
				$order_id=$row[csf('po_id')];
				$color_id=$row[csf("color_id")];
				
				$transfer_in_qty=$transfer_in_arr[$row[csf('po_id')]][$row[csf('color_id')]];
				$transfer_out_qty=$transfer_out_arr[$row[csf('po_id')]][$row[csf('color_id')]];
				?>
                <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                    <td width="30"><?php echo $i; ?></td>
                    <td width="100" align="center"><p><?php echo $buyer_arr[$row[csf("buyer_name")]]; ?></p></td>
                    <td width="90" align="center"><p><?php echo $row[csf("job_no_prefix_num")]; ?></p></td>
                    <td width="80" align="center"><p><?php echo $row[csf("po_number")]; ?></p></td>
                    <td width="100" align="center"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($row[csf("po_quantity")],2,'.',''); ?>&nbsp;</p></td>
                    <td width="90" align="center"><p><?php echo $color_arr[$row[csf("color_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php $book_qty=$booking_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['fin_fab_qnty']; echo number_format($book_qty,2,'.','');//$booking_qty,2,'.',''); ?>&nbsp;</p></td>
                    <?php
						$issue_ret_qnty=$iss_return_qnty[$row[csf('po_id')]][$row[csf("color_id")]]['issue_ret_qnty'];
						$rec_qty=($row[csf("receive_qnty")]+$transfer_in_qty+$issue_ret_qnty)-$transfer_out_qty; //$consumtion_library
					?>
                    <td width="80" align="right"><p><a href='#report_details' onClick="openmypage('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prod_id')]; ?>','<?php echo $row[csf('color_id')]; ?>','receive_popup');"><?php echo number_format($rec_qty,2,'.',''); ?></a>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php $rec_bal=$book_qty-$rec_qty; echo number_format($rec_bal,2,'.',''); ?>&nbsp;</p></td> 
                    <td width="80" align="right"><p><a href='#report_details' onClick="openmypage('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prod_id')]; ?>','<?php echo $row[csf('color_id')]; ?>','issue_popup');"><?php $iss_qty=$issue_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['issue_qnty']+$transfer_out_qty+$rec_return_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['rec_ret_qnty']; echo number_format($iss_qty,2,'.',''); ?></a></p></td>
                    <td width="80" align="right"><p><?php $stock=$rec_qty-$iss_qty; echo number_format($stock,2,'.',''); ?>&nbsp;</p></td>
                    <td width="80" align="center"><p><?php $cons_per=($result_consumtion[$row[csf('po_id')]][$row[csf('color_id')]]/$plan_cut_array[$row[csf('po_id')]][$row[csf('color_id')]]); echo number_format($cons_per,5,'.',''); ?></p></td>
                    <td width="80" align="right"><p><?php $possible_cut_pcs=$iss_qty/($cons_per); echo number_format($possible_cut_pcs); ?></p></td>
                    <td width="" align="right"><p><a href='#report_details' onClick="openmypage('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prod_id')]; ?>','<?php echo $row[csf('color_id')]; ?>','actual_cut_popup');"><?php $actual_qty=$actual_cut_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['actual_cut_qty']; echo number_format($actual_qty,2,'.',''); ?></a>&nbsp;</p></td>
                </tr>
            <?php	
            $i++;	
			$total_order_qnty+=$row[csf("po_quantity")];
			$total_req_qty+=$book_qty;
			$total_rec_qty+=$rec_qty;
			$total_rec_bal+=$rec_bal;
			$total_issue_qty+=$iss_qty;
			$total_stock+=$stock;
			$total_possible_cut_pcs+=$possible_cut_pcs;
			$total_actual_cut_qty+=$actual_qty;
			}
			?>
            </table>
            <table width="1210" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" > 
                <tfoot>
                    <th width="30"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="80" align="right" id=""><?php echo number_format($total_order_qnty,2,'.',''); ?></th>
                    <th width="90">&nbsp;</th>
                    <th width="80" align="right" id="total_req_qty"><?php echo number_format($total_req_qty,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_rec_qty"><?php echo number_format($total_rec_qty,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_rec_bal"><?php echo number_format($total_rec_bal,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_issue_qty"><?php echo number_format($total_issue_qty,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_stock"><?php echo number_format($total_stock,2,'.',''); ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="80" align="right" id="total_possible_cut_pcs"><?php echo number_format($total_possible_cut_pcs,2,'.',''); ?></th>
                    <th width="" align="right" id="total_actual_cut_qty"><?php echo number_format($total_actual_cut_qty,2,'.',''); ?></th>
                </tfoot>
            </table> 
        </div>  
    </fieldset>         
	<?php
	exit();
	}//Knit end
	else if($cbo_report_type==2) // Woven Finish Start
	{
		
		$product_array=array();
		$sql_product="select id, color from product_details_master where item_category_id=3 and status_active=1 and is_deleted=0";
		$sql_product_result=sql_select($sql_product);
		foreach( $sql_product_result as $row )
		{
			$product_array[$row[csf('id')]]=$row[csf('color')];
		}
	
		/*$transfer_in_arr=array();
		$sql_transfer_in="select a.id, a.to_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_in_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=2 group by a.to_order_id,b.from_prod_id,a.id";
		$data_transfer_in_array=sql_select($sql_transfer_in);
		if(count($data_transfer_in_array)>0)
		{
			foreach( $data_transfer_in_array as $row )
			{
				$transfer_in_arr[$row[csf('to_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_in_qnty')];
			}
		}
		
		$transfer_out_arr=array(); 
		$sql_transfer_out="select a.id, a.from_order_id, b.from_prod_id, sum(b.transfer_qnty) as transfer_out_qnty from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=2 group by a.from_order_id,b.from_prod_id,a.id";
		$data_transfer_out_array=sql_select($sql_transfer_out);
		if(count($data_transfer_out_array)>0)
		{
			foreach( $data_transfer_out_array as $row )
			{
				$transfer_out_arr[$row[csf('from_order_id')]][$product_array[$row[csf('from_prod_id')]]]+=$row[csf('transfer_out_qnty')];
			}
		}*///var_dump ($transfer_in_arr);
		
		$issue_qnty=array();
		$sql_issue=sql_select(" select b.po_breakdown_id,b.color_id, sum(b.quantity) as issue_qnty  from inv_issue_master a,inv_transaction c, order_wise_pro_details b where a.id=c.mst_id and c.prod_id=b.prod_id  and b.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and a.issue_purpose in(4,9) and b.entry_form=19 group by b.po_breakdown_id,b.color_id");
		foreach( $sql_issue as $row_iss )
		{
			$issue_qnty[$row_iss[csf('po_breakdown_id')]][$row_iss[csf('color_id')]]['issue_qnty']=$row_iss[csf('issue_qnty')];
		} //var_dump($issue_qnty);
		
		$booking_qnty=array();
		$sql_booking=sql_select("select b.po_break_down_id,b.fabric_color_id, sum(b.fin_fab_qnty ) as fin_fab_qnty  from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=3  and a.booking_type=1 and a.is_deleted=0 and a.status_active=1 and b.booking_type=1 and b.status_active=1 and b.is_deleted=0 group by b.po_break_down_id,b.fabric_color_id");
		foreach( $sql_booking as $row_book)
		{
			$booking_qnty[$row_book[csf('po_break_down_id')]][$row_book[csf('fabric_color_id')]]['fin_fab_qnty']=$row_book[csf('fin_fab_qnty')];
		}
		
		$actual_cut_qnty=array();
		$sql_actual_cut_qty=sql_select(" select a.po_break_down_id,c.color_number_id, sum(b.production_qnty) as actual_cut_qty  from pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c where a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.production_type=1 and a.is_deleted=0 and a.status_active=1  group by  a.po_break_down_id,c.color_number_id");
		foreach( $sql_actual_cut_qty as $row_actual)
		{
			$actual_cut_qnty[$row_actual[csf('po_break_down_id')]][$row_actual[csf('color_number_id')]]['actual_cut_qty']=$row_actual[csf('actual_cut_qty')];
		} 
		
		$result_consumtion=array();
		$sql_consumtiont_qty=sql_select(" select b.job_no, b.po_break_down_id, b.color_number_id, sum( b.cons ) / count( b.color_number_id ) AS conjunction, pcs   from wo_pre_cos_fab_co_avg_con_dtls b, wo_pre_cost_fabric_cost_dtls a WHERE a.id=b.pre_cost_fabric_cost_dtls_id GROUP BY b.po_break_down_id, b.color_number_id, pcs, b.job_no ");
		$con_avg=0;
		foreach($sql_consumtiont_qty as $row_consum)
		{
			$con_avg= $row_consum[csf("conjunction")];///str_replace("'","",$row_sew[csf("pcs")]);
			$con_per_pcs=$con_avg/$row_consum[csf("pcs")];	
			$result_consumtion[$row_consum[csf('job_no')]][$row_consum[csf('po_break_down_id')]][$row_consum[csf('color_number_id')]]['consum']=$con_per_pcs;
		} 
		
	ob_start();
	?>
    <fieldset style="width:1235px;">
        <table cellpadding="0" cellspacing="0" width="1210">
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:18px"><strong><?php echo $report_title; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:14px"><strong> <?php if($date_from!="") echo "Upto : ".change_date_format(str_replace("'","",$txt_date_from)) ;?></strong></td>
            </tr>
        </table>
		<table width="1210" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
            <thead>
                <th width="30">SL</th>
                <th width="100">Buyer</th>
                <th width="90">Job</th>
                <th width="80">Order</th>
                <th width="100">Style</th> 
                <th width="80">Order Qty (Pcs)</th>
                <th width="90">Color</th>
                
                <th width="80">Req. Qty</th>
                <th width="80">Total Received</th>
                <th width="80">Received Balance</th>
                <th width="80">Total Issued</th>
                
                <th width="80">Stock</th>
                <th width="80">Consumption</th>
                <th width="80">Possible Cut Pcs.</th>
                <th width="">Actual Cut</th>
            </thead>
        </table>
        <div style="width:1230px; max-height:350px; overflow-y:scroll;" id="scroll_body">
			<table width="1210" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="table_body" > 
            <?php
			if($db_type==0) $prod_cond="group_concat(e.prod_id) as prod_id"; 
			else if($db_type==2) $prod_cond="listagg(cast(e.prod_id as varchar2(4000)),',') within group (order by e.prod_id) as prod_id";
	
			
				 $sql_query="Select a.id, a.company_name, a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, sum(a.total_set_qnty) as ratio, b.id as po_id, b.po_number, b.plan_cut as po_quantity, 
				sum( c.cons_quantity ) as receive_qnty,  e.color_id, $prod_cond
				from  wo_po_details_master a, wo_po_break_down b,inv_receive_master d,inv_transaction c,  order_wise_pro_details e
				where a.job_no=b.job_no_mst and e.po_breakdown_id=b.id and  d.id=c.mst_id and c.id=e.trans_id and d.entry_form=e.entry_form and  e.entry_form in (17) and e.trans_id!=0  
				and d.entry_form in (17)  and d.item_category=3 and d.status_active=1 and d.is_deleted=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and
				a.company_name=$cbo_company_id $receive_date $buyer_id_cond $job_no_cond $order_id_cond  group by a.job_no, b.po_number, e.color_id order by a.buyer_name, a.job_no, b.po_number, e.color_id";
			
			/*else if($db_type==2)
			{
				$sql_query="
				Select a.id, a.company_name, a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, sum(a.total_set_qnty) as ratio, b.id as po_id, b.po_number, b.plan_cut as po_quantity, 
				sum( e.quantity ) as receive_qnty,  e.color_id, listagg(cast(e.prod_id as varchar2(4000)),',') within group (order by e.prod_id) as prod_id
				from  wo_po_details_master a, wo_po_break_down b, pro_finish_fabric_rcv_dtls c, inv_receive_master d, order_wise_pro_details e
				where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1
				and d.entry_form in (17) and d.item_category=3 and c.status_active=1 and c.is_deleted=0 and c.mst_id=d.id and d.status_active=1 and d.is_deleted=0 and e.po_breakdown_id=b.id and c.id=e.dtls_id and e.entry_form in (17) and e.trans_id!=0 and
				a.company_name=$cbo_company_id $receive_date $buyer_id_cond $job_no_cond $order_id_cond group by  a.job_no_prefix_num, b.id,a.job_no,a.id, a.company_name, a.buyer_name, a.style_ref_no, b.id, b.po_number, e.color_id, b.plan_cut order by a.buyer_name, a.job_no, b.po_number, e.color_id
				";	
			}	*/
			
			
			//echo $sql_query;
			$i=1; 
			$nameArray=sql_select( $sql_query );
			foreach ($nameArray as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$dzn_qnty=0;
				if($costing_per_id_library[$row[csf('job_no')]]==1)
				{
					$dzn_qnty=12;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==3)
				{
					$dzn_qnty=12*2;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==4)
				{
					$dzn_qnty=12*3;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==5)
				{
					$dzn_qnty=12*4;
				}
				else
				{
					$dzn_qnty=1;
				}
				$order_id=$row[csf('po_id')];
				$color_id=$row[csf("color_id")];
				//echo $issue_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['issue_qnty'];
				$transfer_in_qty=$transfer_in_arr[$row[csf('po_id')]][$row[csf('color_id')]];
				$transfer_out_qty=$transfer_out_arr[$row[csf('po_id')]][$row[csf('color_id')]];
				?>
                <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                    <td width="30"><?php echo $i; ?></td>
                    <td width="100" align="center"><p><?php echo $buyer_arr[$row[csf("buyer_name")]]; ?></p></td>
                    <td width="90" align="center"><p><?php echo $row[csf("job_no_prefix_num")]; ?></p></td>
                    <td width="80" align="center"><p><?php echo $row[csf("po_number")]; ?></p></td>
                    <td width="100" align="center"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($row[csf("po_quantity")],2,'.',''); ?>&nbsp;</p></td>
                    <td width="90" align="center"><p><?php echo $color_arr[$row[csf("color_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php $book_qty=$booking_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['fin_fab_qnty']; echo number_format($book_qty,2,'.','');//$booking_qty,2,'.',''); ?>&nbsp;</p></td>
                    <?php
						$rec_qty=($row[csf("receive_qnty")]+$transfer_in_qty)-$transfer_out_qty; //$consumtion_library
					?>
                    <td width="80" align="right"><p><a href='#report_details' onClick="openmypage('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prod_id')]; ?>','<?php echo $row[csf('color_id')]; ?>','woven_receive_popup');"><?php echo number_format($rec_qty,2,'.',''); ?></a>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php $rec_bal=$book_qty-$rec_qty; echo number_format($rec_bal,2,'.',''); ?>&nbsp;</p></td> 
                    <td width="80" align="right"><p><a href='#report_details' onClick="openmypage('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prod_id')]; ?>','<?php echo $row[csf('color_id')]; ?>','woven_issue_popup');"><?php $iss_qty=$issue_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['issue_qnty']+$transfer_out_qty; echo number_format($iss_qty,2,'.',''); ?></a>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php $stock=$rec_qty-$iss_qty; echo number_format($stock,2,'.',''); ?>&nbsp;</p></td>
                    <td width="80" align="center"><p><?php $cons_per=$result_consumtion[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('color_id')]]['consum']; echo number_format($cons_per,4,'.',''); ?></p></td>
                    <td width="80" align="right"><p><?php $possible_cut_pcs=$iss_qty/($cons_per/$dzn_qnty); echo number_format($possible_cut_pcs,2,'.',''); ?></p></td>
                    <td width="" align="right"><p><a href='#report_details' onClick="openmypage('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prod_id')]; ?>','<?php echo $row[csf('color_id')]; ?>','woven_actual_cut_popup');"><?php $actual_qty=$actual_cut_qnty[$row[csf('po_id')]][$row[csf('color_id')]]['actual_cut_qty']; echo number_format($actual_qty,2,'.',''); ?></a>&nbsp;</p></td>
                </tr>
            <?php	
            $i++;	
			$total_order_qnty+=$row[csf("po_quantity")];
			$total_req_qty+=$book_qty;
			$total_rec_qty+=$rec_qty;
			$total_rec_bal+=$rec_bal;
			$total_issue_qty+=$iss_qty;
			$total_stock+=$stock;
			$total_possible_cut_pcs+=$possible_cut_pcs;
			$total_actual_cut_qty+=$actual_qty;
			}
			?>
            </table>
            <table width="1210" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" > 
                <tfoot>
                    <th width="30"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="80" align="right" id=""><?php echo number_format($total_order_qnty,2,'.',''); ?></th>
                    <th width="90">&nbsp;</th>
                    <th width="80" align="right" id="total_req_qty"><?php echo number_format($total_req_qty,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_rec_qty"><?php echo number_format($total_rec_qty,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_rec_bal"><?php echo number_format($total_rec_bal,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_issue_qty"><?php echo number_format($total_issue_qty,2,'.',''); ?></th>
                    <th width="80" align="right" id="total_stock"><?php echo number_format($total_stock,2,'.',''); ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="80" align="right" id="total_possible_cut_pcs"><?php echo number_format($total_possible_cut_pcs,2,'.',''); ?></th>
                    <th width="" align="right" id="total_actual_cut_qty"><?php echo number_format($total_actual_cut_qty,2,'.',''); ?></th>
                </tfoot>
            </table> 
        </div>  
    </fieldset>         
	<?php
	}
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html####$filename"; 
    exit();
}

if($action=="receive_popup")
{
	echo load_html_head_contents("Receive Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:580px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:570px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="570" cellpadding="0" cellspacing="0" align="center">
				<thead>
                	<tr>
                    	<th colspan="7">Receive Details</th>
                    </tr>
                	<tr>
                        <th width="30">Sl</th>
                        <th width="100">Receive ID</th>
                        <th width="80">Prod. ID</th>
                        <th width="75">Receive Date</th>
                        <th width="200">Fabric Des.</th>
                        <th width="80">Rack</th>
                        <th>Qty</th>
                    </tr>
				</thead>
                <tbody>
                <?php
					$product_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details"  );
					$i=1;
					if($db_type==0)
					{
						$mrr_sql="select a.recv_number, a.receive_date, group_concat(b.rack_no) as rack_no, b.prod_id, sum(c.quantity) as quantity
						from  inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c 
						where a.id=b.mst_id and b.id=c.dtls_id and a.entry_form in (7,37) and c.entry_form in (7,37)  and a.item_category=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in($po_id) and a.company_id='$companyID' and b.color_id='$color' and c.trans_id!=0 group by a.recv_number, a.receive_date, b.prod_id";
					}
					else
					{
						$mrr_sql="select a.recv_number, a.receive_date, listagg(cast(b.rack_no as varchar(4000)),',')  within group (order by b.rack_no) as rack_no,  b.prod_id, sum(c.quantity) as quantity from  inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c
						 where a.id=b.mst_id and b.id=c.dtls_id and a.entry_form in (7,37) and c.entry_form in (7,37)  and a.item_category=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in($po_id) and a.company_id='$companyID' and b.color_id='$color' and c.trans_id!=0 group by a.recv_number, a.receive_date, b.prod_id";
					}
					//echo $mrr_sql;
					
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('recv_number')]; ?></p></td> 
                            <td width="80"><p><?php echo $row[csf('prod_id')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('receive_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $product_arr[$row[csf('prod_id')]]; ?></p></td>
                            <td width="80" ><p><?php echo $rack=implode(",",array_unique(explode(",",$row[csf('rack_no')]))); ?>&nbsp;</p></td>
                            <td align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="6" align="right">Total</td>
                        <td align="right">&nbsp;<?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
            <table border="1" class="rpt_table" rules="all" width="570" cellpadding="0" cellspacing="0" align="center">
				<thead>
                	<tr>
                    	<th colspan="7">Issue Return Details</th>
                    </tr>
                	<tr>
                        <th width="30">Sl</th>
                        <th width="100">Return ID</th> 
                        <th width="80">Prod. ID</th>
                        <th width="75">Return Date</th>
                        <th width="200">Fabric Des.</th>
                        <th width="80">Rack</th>
                        <th>Qty</th>
                    </tr>
				</thead>
                <tbody>
                <?php
					$product_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details");
					$i=1;
					if($db_type==0)
					{
						$ret_sql="select a.recv_number, a.receive_date, group_concat(b.rack) as rack_no, b.prod_id, sum(c.quantity) as quantity
						from inv_receive_master a, inv_transaction b, order_wise_pro_details c 
						where a.id=b.mst_id and b.id=c.trans_id and a.entry_form in (52) and c.entry_form in (52)  and a.item_category=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in($po_id) and a.company_id='$companyID' and c.color_id='$color' and c.trans_id!=0 group by a.recv_number, a.receive_date, b.prod_id";
					}
					else
					{
						$ret_sql="select a.recv_number, a.receive_date, listagg(cast(b.rack as varchar(4000)),',')  within group (order by b.rack) as rack_no,  b.prod_id, sum(c.quantity) as quantity from  inv_receive_master a, inv_transaction b, order_wise_pro_details c 
						where a.id=b.mst_id and b.id=c.trans_id and a.entry_form in (52) and c.entry_form in (52)  and a.item_category=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in($po_id) and a.company_id='$companyID' and c.color_id='$color' and c.trans_id!=0 group by a.recv_number, a.receive_date, b.prod_id";
					}
					//echo $ret_sql;
					
					$retDataArray=sql_select($ret_sql);
					
					foreach($retDataArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('recv_number')]; ?></p></td> 
                            <td width="80"><p><?php echo $row[csf('prod_id')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('receive_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $product_arr[$row[csf('prod_id')]]; ?></p></td>
                            <td width="80" ><p><?php echo $rack=implode(",",array_unique(explode(",",$row[csf('rack_no')]))); ?>&nbsp;</p></td>
                            <td align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_return_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="6" align="right">Total</td>
                        <td align="right">&nbsp;<?php echo number_format($tot_return_qty,2); ?>&nbsp;</td>
                    </tr>
                    <tr class="tbl_bottom">
                    	<td colspan="6" align="right">Total Receive Balance</td>
                        <td align="right">&nbsp;<?php $tot_balance=$tot_qty+$tot_return_qty; echo number_format($tot_balance,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}//Knit Finish end
if($action=="woven_receive_popup")
{
	echo load_html_head_contents("Receive Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:580px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:570px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="550" cellpadding="0" cellspacing="0" align="center">
				<thead>
                	<tr>
                        <th width="30">Sl</th>
                        <th width="100">Receive ID</th>
                        <th width="75">Receive Date</th>
                        <th width="200">Fabric Des.</th>
                        <th width="80">Qty</th>
                    </tr>
				</thead>
                <tbody>
                <?php
					$product_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details"  );
					$i=1;
					
					$mrr_sql="select a.recv_number, a.receive_date, b.prod_id, sum(c.quantity) as quantity
					from  inv_receive_master a, inv_transaction b, order_wise_pro_details c 
					where a.id=b.mst_id and b.id=c.trans_id  and a.entry_form in (17) and c.entry_form in (17)  and a.item_category=3 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in($po_id) and a.company_id='$companyID' and c.color_id='$color' group by a.recv_number, a.receive_date, b.prod_id";
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('recv_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('receive_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $product_arr[$row[csf('prod_id')]]; ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}

if($action=="issue_popup")
{
	echo load_html_head_contents("Issue Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	$cutting_floor_library=return_library_array( "select id, floor_name from lib_prod_floor where production_process=1 ", "id", "floor_name"  );
	?>
<!--	<div style="width:580px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:570px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="570" cellpadding="0" cellspacing="0" align="center">
				<thead>
                	<tr>
                    	<th colspan="6">Issue Details</th>
                    </tr>
                	<tr>
                        <th width="30">Sl</th>
                        <th width="100">Issue ID</th>
                        <th width="70">Issue Date</th>
                        <th width="200">Fabric Des.</th>
                        <th width="80">Cut Unit No</th>
                        <th>Qty</th>
                    </tr>
				</thead>
                <tbody>
                <?php
					$product_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details"  );
					$i=1;
					$mrr_sql="select a.id, a.issue_number, a.issue_date, b.prod_id, b.cutting_unit, c.quantity
					from  inv_issue_master a, inv_finish_fabric_issue_dtls b, order_wise_pro_details c 
					where a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=18 and c.entry_form=18 and a.item_category=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in ($po_id) and a.company_id='$companyID' and c.color_id='$color'";
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('issue_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $product_arr[$row[csf('prod_id')]]; ?></p></td>
                            <td width="80" ><p><?php echo $cutting_floor_library[$row[csf('cutting_unit')]]; ?> &nbsp;</p></td>
                            <td  align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
            <table border="1" class="rpt_table" rules="all" width="570" cellpadding="0" cellspacing="0" align="center">
				<thead>
                	<tr>
                    	<th colspan="5">Receive Return Details</th>
                    </tr>
                	<tr>
                        <th width="30">Sl</th>
                        <th width="120">Return ID</th>
                        <th width="70">Return Date</th>
                        <th width="200">Fabric Des.</th>
                        <th>Return Qty</th>
                    </tr>
				</thead>
                <tbody>
                <?php
					$product_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details"  );
					$i=1;
					$ret_sql="select a.issue_number, a.issue_date, b.prod_id, sum(c.quantity) as quantity
						from inv_issue_master a, inv_transaction b, order_wise_pro_details c 
						where a.id=b.mst_id and b.id=c.trans_id and a.entry_form in (46) and c.entry_form in (46)  and a.item_category=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in($po_id) and a.company_id='$companyID' and c.color_id='$color' and c.trans_id!=0 group by a.issue_number, a.issue_date, b.prod_id";
					$retDataArray=sql_select($ret_sql);
					
					foreach($retDataArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="120"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('issue_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $product_arr[$row[csf('prod_id')]]; ?></p></td>
                            <td  align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_ret_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_ret_qty,2); ?>&nbsp;</td>
                    </tr>
                    <tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total Issue Balance</td>
                        <td align="right"><?php $tot_iss_bal=$tot_qty+$tot_ret_qty; echo number_format($tot_iss_bal,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
} // Issue End
if($action=="woven_issue_popup")
{
	echo load_html_head_contents("Issue Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:580px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:570px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="550" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="100">Issue ID</th>
                    <th width="75">Issue Date</th>
                    <th width="200">Fabric Des.</th>
                    <th width="80">Qty</th>
				</thead>
                <tbody>
                <?php
					$product_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details"  );
					$i=1;
										
					$mrr_sql="select a.id, a.issue_number, a.issue_date, b.prod_id, c.quantity
					from  inv_issue_master a, inv_transaction b, order_wise_pro_details c 
					where a.id=b.mst_id and b.id=c.trans_id and a.entry_form=19 and c.entry_form=19 and a.item_category=3 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and c.po_breakdown_id in ($po_id) and a.company_id='$companyID' and c.color_id='$color'";
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('issue_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $product_arr[$row[csf('prod_id')]]; ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}

if($action=="actual_cut_popup")
{
	echo load_html_head_contents("Actual Cut Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:580px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:570px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="550" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <!--<th width="100">Issue ID</th>--> 
                    <th width="75">Production Date</th>
                    <th width="200">Item Name</th>
                    <th width="80">Qty</th>
				</thead>
                <tbody>
                <?php
					if($db_type==0) $select_grpby_actual="group by a.id";
					if($db_type==2) $select_grpby_actual=" group by a.id,a.production_date, a.item_number_id,b.color_size_break_down_id, c.color_mst_id";
					else $select_grpby_actual="";
					//$color_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details"  );
					$i=1;
					$mrr_sql="select a.id, a.production_date, a.item_number_id, sum(b.production_qnty) as production_qnty, b.color_size_break_down_id, c.color_mst_id
					from  pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c
					where a.id=b.mst_id and b.color_size_break_down_id=c.id and c.color_number_id='$color' and a.production_type=1 and a.is_deleted=0 and a.status_active=1 and a.po_break_down_id in ($po_id) $select_grpby_actual";
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
<!--                            <td width="100"><p><?php //echo $row[csf('issue_number')]; ?></p></td>
-->                            <td width="75"><p><?php echo change_date_format($row[csf('production_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $garments_item[$row[csf('item_number_id')]]; ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($row[csf('production_qnty')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('production_qnty')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
} //actual end

if($action=="woven_actual_cut_popup")
{
	echo load_html_head_contents("Actual Cut Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:580px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:570px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="550" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <!--<th width="100">Issue ID</th>--> 
                    <th width="75">Production Date</th>
                    <th width="200">Item Name</th>
                    <th width="80">Qty</th>
				</thead>
                <tbody>
                <?php
					if($db_type==0) $select_grpby_actual="group by a.id";
					if($db_type==2) $select_grpby_actual=" group by a.id,a.production_date, a.item_number_id,b.color_size_break_down_id, c.color_mst_id";
					else $select_grpby_actual="";
					//$color_arr=return_library_array( "select id, product_name_details from product_details_master", "id", "product_name_details"  );
					$i=1;
					$mrr_sql="select a.id, a.production_date, a.item_number_id, sum(b.production_qnty) as production_qnty, b.color_size_break_down_id, c.color_mst_id
					from  pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c
					where a.id=b.mst_id and b.color_size_break_down_id=c.id and c.color_number_id='$color' and a.production_type=1 and a.is_deleted=0 and a.status_active=1 and a.po_break_down_id in ($po_id) $select_grpby_actual";
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
<!--                            <td width="100"><p><?php //echo $row[csf('issue_number')]; ?></p></td>
-->                            <td width="75"><p><?php echo change_date_format($row[csf('production_date')]); ?></p></td>
                            <td width="200" ><p><?php echo $garments_item[$row[csf('item_number_id')]]; ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($row[csf('production_qnty')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('production_qnty')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}
?>