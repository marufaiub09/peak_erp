<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');

if($db_type==0) $select_field="group"; 
else if($db_type==2) $select_field="wm";

/*if($action=="load_drop_down_item_desc")
{
	$data=explode("**",$data);
	$po_id=$data[0];
	$row_no=$data[1];
	$item_id=$data[2];
	$process_id=$data[3];
	$fabric_from=$data[4];
	$job_no=$data[5];
	
	if($fabric_from==1)
	{
		if ($db_type==0)
		{
			$sql="select concat(b.material_description,',',b.gsm,',',b.grey_dia,',',b.fin_dia) as material_description from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and b.order_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.material_description, b.gsm, b.grey_dia, b.fin_dia";
		}
		elseif($db_type==2)
		{
			$sql="select b.material_description || ',' || b.gsm || ',' || b.grey_dia || ',' || b.fin_dia as material_description from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and b.order_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.material_description, b.gsm, b.grey_dia, b.fin_dia"; 
		}
		echo create_drop_down( "cboItemDesc_".$row_no, 150, $sql,'material_description,material_description', 1, "--Select Item Desc--",'0',"gsm_dia_load(".$row_no.")",'',$item_id);
		echo "<input type='hidden' name='txtItemDesc_".$row_no."' id='txtItemDesc_".$row_no."' class='text_boxes' style='width:60px' />";
	}
	else
	{
		if ($db_type==0)
		{
			$sql="select concat(fabric_description,',', gsm,',', dia_width) as fabric_description from subcon_production_dtls where job_no='$job_no' and status_active=1 group by fabric_description, gsm, dia_width";
		}
		else if ($db_type==2)
		{
			$sql="select fabric_description || ',' || gsm || ',' || dia_width as fabric_description from subcon_production_dtls where job_no='$job_no' and status_active=1 group by fabric_description, gsm, dia_width";
		}
			echo create_drop_down( "cboItemDesc_".$row_no, 150, $sql,'fabric_description, fabric_description', 1, "--Select Item Desc--",'0',"gsm_dia_load(".$row_no.")",'',$item_id);
		echo "<input type='hidden' name='txtItemDesc_".$row_no."' id='txtItemDesc_".$row_no."' class='text_boxes' style='width:60px' />";
	}
	exit();
}*/

/*if($action=="load_receive_production_data")
{
	$ex_data=explode('_',$data);
	$row=$ex_data[2];
	if($ex_data[0]==1)
	{
		$sql="select id, gsm, grey_dia as dia, fin_dia from sub_material_dtls where id=$ex_data[1] and status_active=1 and is_deleted=0";
	}
	else
	{
		$sql="select id, gsm, dia_width as dia, '' as fin_dia from subcon_production_dtls where id=$ex_data[1] and status_active=1 and is_deleted=0";
	}
	$sql_result = sql_select($sql);
 	foreach($sql_result as $result)
	{
		echo "$('#txtGsm_".$row."').val(".$result[csf("gsm")].");\n";
		echo "$('#txtDia_".$row."').val(".$result[csf("dia")].");\n";
		echo "$('#txtFinDia_".$row."').val(".$result[csf("fin_dia")].");\n";
	}
 	exit();
}*/

if ($action=="load_variable_settings")
{
	echo "$('#variable_check').val(0);\n";
	$sql_result = sql_select("select batch_no_creation from variable_settings_production where company_name=$data and variable_list=24 and status_active=1 and is_deleted=0");
 	foreach($sql_result as $result)
	{
		echo "$('#variable_check').val(".$result[csf("batch_no_creation")].");\n";
	}
 	exit();
}

if($action=="po_popup")
{
  	echo load_html_head_contents("Order Info","../../", 1, 1, '','1','');
	extract($_REQUEST);
?>
	<script>
		var party_id='';
		var hide_party_id='<? echo $hide_party_id; ?>';
		var no_of_row=<? echo $no_of_row; ?>;
		
		function js_set_value( po_id,po_no,job_no,party_id,item_id,process_id)
		{
			//alert (po_no)
			if(no_of_row>1 && hide_party_id!="")
			{
				if(party_id!=hide_party_id)
				{
					alert("Party Mixing Not Allowed");
					return;
				}
			}
			document.getElementById('po_id').value=po_id;
			document.getElementById('po_no').value=po_no;
			document.getElementById('job_no').value=job_no;
			document.getElementById('party_id').value=party_id;
			document.getElementById('item_id').value=item_id;
			document.getElementById('process_id').value=process_id;
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
	<fieldset style="width:620px;margin-left:10px">
        <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
            <table cellpadding="0" cellspacing="0" border="1" rules="all" width="620" class="rpt_table">
                <thead>
                    <tr>
                        <th colspan="5"><? echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" ); ?></th>
                    </tr>
                	<tr>
                        <th>Year</th>
                        <th>Buyer</th>
                        <th>Job Search</th>
                        <th>Order Search</th>
                        <th>
                            <input type="reset" name="reset" id="reset" value="Reset" style="width:70px" class="formbutton" />
                            <input type="hidden" name="po_id" id="po_id" value="">
                            <input type="hidden" name="po_no" id="po_no" value="">
                            <input type="hidden" name="job_no" id="job_no" value="">
                            <input type="hidden" name="party_id" id="party_id" value="">
                            <input type="hidden" name="item_id" id="item_id" value="">
                            <input type="hidden" name="process_id" id="process_id" value="">
                        </th>
                    </tr>
                </thead>
                <tr class="general">
                    <td> 
                        <?
                            echo create_drop_down( "cbo_year", 80, create_year_array(),"", 1,"-- All --", date("Y",time()), "",0,"" );
                        ?>
                    </td>
                    <td align="center">
                        <?
							echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (2,3)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", 0 ); 
                        ?>       
                    </td>
                    <td align="center">				
                        <input type="text" style="width:100px" class="text_boxes"  name="txt_job" id="txt_job" placeholder="Job" />	
                    </td> 						
                    <td align="center">				
                        <input type="text" style="width:100px" class="text_boxes"  name="txt_order" id="txt_order" placeholder="Order" />	
                    </td> 						
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_order').value+'_'+document.getElementById('txt_job').value+'_'+<? echo $cbo_company_id; ?>+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('cbo_year').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_po_search_list_view', 'search_div', 'subcon_batch_creation_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:70px;" />
                    </td>
                </tr>
            </table>
            <div id="search_div" style="margin-top:10px"></div>   
        </form>
    </fieldset>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}

if($action=="create_po_search_list_view")
{
	$data=explode('_',$data);
	$order_val=$data[0];
	$job_val=$data[1];
	$company_id =$data[2];
	$buyer_id =$data[3];
	$year_id =$data[4];
	$search_type =$data[5];
	
	if($buyer_id==0) $buyer_con=''; else  $buyer_con=" and a.party_id=$buyer_id";
	
	if($search_type==1)
	{
		if($order_val=='') $order_con=''; else  $order_con=" and b.order_no='$order_val'";
		if($job_val=='') $job_con=''; else  $job_con=" and a.job_no_prefix_num='$job_val'";
	}
	else if($search_type==4 || $search_type==0)
	{
		if($order_val=='') $order_con=''; else  $order_con=" and b.order_no like '%$order_val%'";
		if($job_val=='') $job_con=''; else  $job_con=" and a.job_no_prefix_num like '%$job_val%'";
	}
	else if($search_type==2)
	{
		if($order_val=='') $order_con=''; else  $order_con=" and b.order_no like '$order_val%'";
		if($job_val=='') $job_con=''; else  $job_con=" and a.job_no_prefix_num like '$job_val%'";
	}
	else if($search_type==3)
	{
		if($order_val=='') $order_con=''; else  $order_con=" and b.order_no like '%$order_val'";
		if($job_val=='') $job_con=''; else  $job_con=" and a.job_no_prefix_num like '%$job_val'";
	}	
	
	if($db_type==0)
	{
		if($year_id==0) $year_con=''; else  $year_con=" and YEAR(a.insert_date)=$year_id";
		$sql="select a.subcon_job as job_no, a.job_no_prefix_num, YEAR(a.insert_date) as year, a.party_id, b.id as po_id, b.main_process_id, b.process_id, b.cust_style_ref as style_ref_no, b.order_uom, b.order_no as po_number, b.order_quantity as po_qnty_in_pcs, b.delivery_date as pub_shipment_date, group_concat(distinct(c.item_id)) as item_id from subcon_ord_mst a, subcon_ord_dtls b, subcon_ord_breakdown c where a.subcon_job=b.job_no_mst and b.id=c.order_id and a.company_id=$company_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $buyer_con $order_con $job_con $year_con group by b.id order by b.id DESC";// die;
	}
	else if($db_type==2)
	{
		if($year_id==0) $year_con=''; else  $year_con=" and TO_CHAR(a.insert_date,'YYYY')=$year_id";
		$sql="select LOG_CONCAT(distinct(cast(a.subcon_job as varchar2(4000)))) as job_no, a.party_id, LOG_CONCAT(distinct(a.job_no_prefix_num)) as job_no_prefix_num, TO_CHAR(max(a.insert_date),'YYYY') as year, b.id as po_id, b.main_process_id, b.process_id, b.cust_style_ref as style_ref_no, b.order_uom, b.order_no as po_number, b.order_quantity as po_qnty_in_pcs, b.delivery_date as pub_shipment_date, Log_concat(distinct(c.item_id)) as item_id from subcon_ord_mst a, subcon_ord_dtls b, subcon_ord_breakdown c where a.subcon_job=b.job_no_mst and b.id=c.order_id and a.company_id=$company_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $buyer_con $order_con $job_con $year_con group by a.party_id, b.id, b.main_process_id, b.process_id, b.cust_style_ref, b.order_uom, b.order_no, b.order_quantity, b.delivery_date order by b.id DESC";// die;
	}
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="30">SL</th>
                <th width="40">Job </th>
                <th width="60">Year</th>
                <th width="100">Style No</th>
                <th width="90">PO No</th>
                <th width="90">Process</th>
                <th width="80">PO Quantity</th>
                <th width="40">UOM</th>
                <th>Delivery Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:240px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					$processid=explode(",",$selectResult[csf('process_id')]);
					$dye_fin_array=array(25,26,31,39,32,33,34,38,60,61,62,63,65,66,67,68,69,70,71,73,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,125,127,128,129,132,135);
					//$query_arr=array_intersect($dye_fin_array,$processid);
					//print_r ($query_arr); 
					if(array_intersect($dye_fin_array,$processid))
					{
				?>
                    <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<? echo $i;?>" onClick="js_set_value(<? echo $selectResult[csf('po_id')]; ?>,'<? echo $selectResult[csf('po_number')]; ?>','<? echo $selectResult[csf('job_no')]; ?>','<? echo $selectResult[csf('party_id')]; ?>','<? echo $selectResult[csf('item_id')]; ?>','<? echo $selectResult[csf('main_process_id')]; ?>')"> 
                        <td width="30" align="center"><? echo $i; ?></td>	
                        <td width="40" align="center"><p><? echo $selectResult[csf('job_no_prefix_num')]; ?></p></td>
                        <td width="60" align="center"><? echo $selectResult[csf('year')]; ?></td>
                        <td width="100"><p><? echo $selectResult[csf('style_ref_no')]; ?></p></td>
                        <td width="90"><p><? echo $selectResult[csf('po_number')]; ?></p></td>
                        <td width="90"><p><? echo $production_process[$selectResult[csf('main_process_id')]]; ?></p></td>
                        <td width="80" align="right"><? echo number_format( $selectResult[csf('po_qnty_in_pcs')],2); ?>&nbsp;</td> 
                        <td width="40" align="center"><p><? echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                        <td align="center"><? echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                    </tr>
                <?
                	$i++;
					}
				}
			?>
            </table>
        </div>
	</div>           
<?
exit();	
}

if($action=="itemdes_popup")
{
  	echo load_html_head_contents("Item Description Info","../../", 1, 1, '','1','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value( prod_id,challan,description,gsm,grey_dia,fin_dia)
		{
			document.getElementById('prod_id').value=prod_id;
			document.getElementById('challan').value=challan;
			document.getElementById('description').value=description;
			document.getElementById('gsm').value=gsm;
			document.getElementById('grey_dia').value=grey_dia;
			document.getElementById('fin_dia').value=fin_dia;
			parent.emailwindow.hide();
		}
    </script>
</head>
<?
	$batch_array=array();
	$batch_sql="select po_id, prod_id, sum(batch_qnty) as batch_qnty from pro_batch_create_dtls where po_id='$po_id' and status_active=1 and is_deleted=0 group by po_id, prod_id";
	$result_batch_sql=sql_select( $batch_sql );
	foreach($result_batch_sql as $row)
	{
		$batch_array[$row[csf('po_id')]][$row[csf('prod_id')]]=$row[csf('batch_qnty')];
	}
	
	if($fabricfrom==1)
	{
		if ($db_type==0)
		{
			$sql="select a.chalan_no, b.material_description, b.id, b.gsm, b.grey_dia, b.fin_dia, b.color_id, sum(b.quantity) as quantity, concat(b.material_description,',',b.gsm,',',b.grey_dia,',',b.fin_dia) as description from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and a.company_id=$cbo_company_id and b.order_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=2 and b.is_deleted=0 group by a.chalan_no, b.id, b.material_description, b.gsm, b.grey_dia, b.fin_dia, b.color_id order by a.chalan_no";
		}
		elseif($db_type==2)
		{
			$sql="select  a.chalan_no, b.material_description, b.id, b.gsm, b.grey_dia, b.fin_dia, b.color_id, sum(b.quantity) as quantity, b.material_description || ',' || b.gsm || ',' || b.grey_dia || ',' || b.fin_dia as description from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and a.company_id=$cbo_company_id and b.order_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=2 and b.is_deleted=0 group by a.chalan_no, b.id, b.material_description, b.gsm, b.grey_dia, b.fin_dia, b.color_id order by a.chalan_no"; 
		}
	}
	else
	{
		if ($db_type==0)
		{
			$sql="select '' as chalan_no, fabric_description as material_description, id, gsm, dia_width as fin_dia, color_id, sum(product_qnty) as quantity, concat(fabric_description,',', gsm,',', dia_width) as description from subcon_production_dtls where order_id='$po_id' and product_type=2 and status_active=1 group by id, fabric_description, gsm, dia_width, color_id";
		}
		else if ($db_type==2)
		{
			$sql="select '' as chalan_no, fabric_description as material_description, id, gsm, dia_width as fin_dia, color_id, sum(product_qnty) as quantity, fabric_description || ',' || gsm || ',' || dia_width as description from subcon_production_dtls where order_id='$po_id' and product_type=2 and status_active=1 group by id, fabric_description, gsm, dia_width, color_id";
		}
	}
	//echo $sql;
	?>
        <input type="hidden" name="prod_id" id="prod_id" value="">
        <input type="hidden" name="challan" id="challan" value="">
        <input type="hidden" name="description" id="description" value="">
        <input type="hidden" name="gsm" id="gsm" value="">
        <input type="hidden" name="grey_dia" id="grey_dia" value="">
        <input type="hidden" name="fin_dia" id="fin_dia" value="">

    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="30">SL</th>
                <th width="60">Challan</th>
                <th width="130">Fab. Desc.</th>
                <th width="40">GSM</th>
                <th width="50">G.Dia</th>
                <th width="50">F.Dia</th>
                <th width="80">Color</th>
                <th width="80">Rec. Qty</th>
                <th>Balance</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:240px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					//echo $po_id;
					$balance=$selectResult[csf('quantity')]-$batch_array[$po_id][$selectResult[csf('id')]];
					if($balance!=0)
					{
					?>
						<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<? echo $i;?>" onClick="js_set_value(<? echo $selectResult[csf('id')]; ?>,'<? echo $selectResult[csf('chalan_no')]; ?>','<? echo $selectResult[csf('description')]; ?>','<? echo $selectResult[csf('gsm')]; ?>','<? echo $selectResult[csf('grey_dia')]; ?>','<? echo $selectResult[csf('fin_dia')]; ?>')"> 
							<td width="30" align="center"><? echo $i; ?></td>	
							<td width="60" align="center"><p><? echo $selectResult[csf('chalan_no')]; ?></p></td>
							<td width="130" align="center"><? echo $selectResult[csf('material_description')]; ?></td>
							<td width="40"  align="center"><p><? echo $selectResult[csf('gsm')]; ?></p></td>
							<td width="50"  align="center"><p><? echo $selectResult[csf('grey_dia')]; ?></p></td>
							<td width="50"  align="center"><p><? echo $selectResult[csf('fin_dia')]; ?></p></td>
							<td width="80"  align="center"><p><? echo  $color_arr[$selectResult[csf('color_id')]]; ?></p></td> 
							<td width="80" align="right"><p><? echo number_format($selectResult[csf('quantity')],2); ?></p></td>
							<td align="right"><?  echo number_format($balance,2); ?></td>	
						</tr>
					<? 
						$i++;
					}
				}
			?>
            </table>
        </div>
	</div>           
<?
exit();	
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
/*	$po_batch_no_arr=array();
	$po_batch_data=sql_select("select max(a.po_batch_no) as po_batch_no, a.po_id, b.color_id from  pro_batch_create_dtls a, pro_batch_create_mst b where a.mst_id=b.id group by b.color_id, a.po_id");
	foreach($po_batch_data as $row)
	{
		$po_batch_no_arr[$row[csf('color_id')]][$row[csf('po_id')]]=$row[csf('po_batch_no')];
	}
*/	
	if($operation==0)// Insert Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		$batch_update_id=''; $batch_no_creation=str_replace("'","",$batch_no_creation);
		
		$color_id=return_id( $txt_batch_color, $color_arr, "lib_color", "id,color_name");
		
		if(str_replace("'","",$update_id)=="")
		{
			$id=return_next_id( "id", "pro_batch_create_mst", 1 ) ;
			
			$batch_update_id=$id;
			$serial_no=date("y",strtotime($pc_date_time))."-".$id;
			
		 	if($batch_no_creation==1)
			{
				//$txt_batch_number="'".$serial_no."'";
				$txt_batch_number="'".$id."'";
			}
			else
			{
				if(is_duplicate_field( "batch_no", "pro_batch_create_mst", "company_id=$cbo_company_id and batch_no=$txt_batch_number and extention_no=$txt_ext_no and entry_form=36" )==1)
				{
					check_table_status( $_SESSION['menu_id'],0);
					echo "11**0"; 
					die;			
				}
				
				$txt_batch_number=$txt_batch_number;
			}
			
			$field_array="id, batch_against, batch_no, batch_date, entry_form, company_id, extention_no, color_id, batch_weight, total_trims_weight, color_range_id, process_id, inserted_by, insert_date";
			
			$data_array="(".$id.",".$cbo_batch_against.",".$txt_batch_number.",".$txt_batch_date.",36,".$cbo_company_id.",".$txt_ext_no.",".$color_id.",".$txt_batch_weight.",".$txt_tot_trims_weight.",".$cbo_color_range.",".$txt_process_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into pro_batch_create_mst (".$field_array.") values ".$data_array;die;
			$rID=sql_insert("pro_batch_create_mst",$field_array,$data_array,0);
			
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$batch_update_id=str_replace("'","",$update_id);
			$serial_no=str_replace("'","",$txt_batch_sl_no);
			
			if($batch_no_creation!=1)
			{
				if(is_duplicate_field( "batch_no", "pro_batch_create_mst", "company_id=$cbo_company_id and batch_no=$txt_batch_number and color_id=$color_id and entry_form=36" )==1)
				{
					check_table_status( $_SESSION['menu_id'],0);
					echo "11**0"; 
					die;			
				}
			}
			
			$field_array_update="batch_against*batch_no*batch_date*company_id*extention_no*color_id*batch_weight*total_trims_weight*color_range_id*process_id*updated_by*update_date";
			
			$data_array_update=$cbo_batch_against."*".$txt_batch_number."*".$txt_batch_date."*".$cbo_company_id."*".$txt_ext_no."*".$color_id."*".$txt_batch_weight."*".$txt_tot_trims_weight."*".$cbo_color_range."*".$txt_process_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$rID=sql_update("pro_batch_create_mst",$field_array_update,$data_array_update,"id",$update_id,0);
			if($rID) $flag=1; else $flag=0; 
		}
		
		$id_dtls=return_next_id( "id", "pro_batch_create_dtls", 1 ) ;
		//$id_roll = return_next_id( "id", "pro_roll_details", 1 );
		
		$field_array_dtls="id, mst_id, fabric_from, po_id, item_description, prod_id, fin_dia, width_dia_type, roll_no, batch_qnty, rec_challan, inserted_by, insert_date";
		//$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, roll_id, inserted_by, insert_date";
		//$roll_table_id='';
		
		for($i=1;$i<=$total_row;$i++)
		{
			$po_id="poId_".$i;
			$cbofabricfrom="cbofabricfrom_".$i; 
			$prod_id="txtItemDescid_".$i;
			$prod_desc="txtItemDesc_".$i;
			$gsm="txtGsm_".$i;
			$dia="txtDia_".$i;
			$findia="txtFinDia_".$i;
			$txtRollNo="txtRollNo_".$i;
			$hideRollNo="hideRollNo_".$i;
			$txtBatchQnty="txtBatchQnty_".$i;
			$cboDiaWidthType="cboDiaWidthType_".$i;
			$txtrecChallan="txtrecChallan_".$i;
			//$itemDesc=str_replace("'","",$$prod_desc).', '.str_replace("'","",$$gsm).', '.str_replace("'","",$$dia);
			//echo $ItemDesc;die;
			//$po_batch_no=$po_batch_no_arr[$color_id][str_replace("'","",$$po_id)]+1;
		
			if($data_array_dtls!="") $data_array_dtls.=","; 	
			
			$data_array_dtls.="(".$id_dtls.",".$batch_update_id.",".$$cbofabricfrom.",".$$po_id.",".$$prod_desc.",".$$prod_id.",".$$findia.",".$$cboDiaWidthType.",".$$txtRollNo.",".$$txtBatchQnty.",".$$txtrecChallan.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
			
			$id_dtls=$id_dtls+1;
		}
		
		//echo "insert into pro_batch_create_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;
		$rID2=sql_insert("pro_batch_create_dtls",$field_array_dtls,$data_array_dtls,1);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		
		/*if($data_array_roll!="")
		{
			$rID3=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
		}
		//echo $flag;die;
		if($roll_table_id!="")
		{
			$rID4=sql_multirow_update("pro_roll_details","roll_used",1,"id",$roll_table_id,1);
			if($flag==1) 
			{
				if($rID4) $flag=1; else $flag=0; 
			} 
		}*/
		
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$batch_update_id."**".$serial_no."**".str_replace("'","",$txt_batch_number);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**0";
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "0**".$batch_update_id."**".$serial_no."**".str_replace("'","",$txt_batch_number);
			}
			else
			{
				oci_rollback($con);
				echo "5**0**0";
			}
		}
				
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$prev_batch_data_arr=array();
		$prev_batch_data=sql_select("select a.id as dtls_id, a.po_id, b.color_id from pro_batch_create_dtls a, pro_batch_create_mst b where a.mst_id=b.id and b.id=$update_id");
		foreach($prev_batch_data as $row)
		{
			$prev_batch_data_arr[$row[csf('dtls_id')]]['po_id']=$row[csf('po_id')];
			$prev_batch_data_arr[$row[csf('dtls_id')]]['color']=$row[csf('color_id')];
		}

		$color_id=return_id( $txt_batch_color, $color_arr, "lib_color", "id,color_name");
		$flag=1; $batch_no_creation=str_replace("'","",$batch_no_creation);
		
		if(str_replace("'","",$cbo_batch_against)==2 && str_replace("'","",$hide_update_id)=="")
		{
			if(is_duplicate_field( "batch_no", "pro_batch_create_mst", "company_id=$cbo_company_id and batch_no=$txt_batch_number and extention_no=$txt_ext_no and entry_form=36" )==1)
			{
				check_table_status( $_SESSION['menu_id'],0);
				echo "11**0"; 
				die;			
			}
			
			$id=return_next_id( "id", "pro_batch_create_mst", 1 ) ;
			
			$batch_update_id=$id;
			$serial_no=date("y",strtotime($pc_date_time))."-".$id;
			$field_array="id, batch_against, batch_no, batch_date, entry_form, company_id, extention_no, color_id, batch_weight, total_trims_weight, color_range_id, process_id, inserted_by, insert_date";
			
			$data_array="(".$id.",".$cbo_batch_against.",".$txt_batch_number.",".$txt_batch_date.",36,".$cbo_company_id.",".$txt_ext_no.",".$color_id.",".$txt_batch_weight.",".$txt_tot_trims_weight.",".$cbo_color_range.",".$txt_process_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
/*			$field_array="id, batch_against, batch_no, batch_date, company_id, extention_no, color_id, batch_weight, total_trims_weight, color_range_id, process_id, re_dyeing_from, inserted_by, insert_date";
			
			$data_array="(".$id.",".$cbo_batch_against.",".$txt_batch_number.",".$txt_batch_date.",".$cbo_company_id.",".$txt_ext_no.",".$color_id.",".$txt_batch_weight.",".$txt_tot_trims_weight.",".$cbo_color_range.",".$txt_process_id.",".$update_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
*/					 
			//echo "insert into pro_batch_create_mst (".$field_array.") values ".$data_array;die;
			$rID=sql_insert("pro_batch_create_mst",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;
			
			$id_dtls=return_next_id( "id", "pro_batch_create_dtls", 1 ) ;
			
			$field_array_dtls="id, mst_id, fabric_from, po_id, item_description, prod_id, fin_dia, width_dia_type, roll_no, batch_qnty, rec_challan, inserted_by, insert_date";
			
			
			for($i=1;$i<=$total_row;$i++)
			{
				$po_id="poId_".$i;
				$cbofabricfrom="cbofabricfrom_".$i;  
				$prod_id="txtItemDescid_".$i;
				$prod_desc="txtItemDesc_".$i;
				$gsm="txtGsm_".$i;
				$dia="txtDia_".$i;
				$findia="txtFinDia_".$i;
				$txtRollNo="txtRollNo_".$i;
				$txtBatchQnty="txtBatchQnty_".$i;
				$updateIdDtls="updateIdDtls_".$i;
				$cboDiaWidthType="cboDiaWidthType_".$i;
				$txtrecChallan="txtrecChallan_".$i;
				//$itemDesc=str_replace("'","",$$prod_desc).', '.str_replace("'","",$$gsm).', '.str_replace("'","",$$dia);
			
				if($data_array_dtls!="") $data_array_dtls.=",";
				
				$data_array_dtls.="(".$id_dtls.",".$batch_update_id.",".$$cbofabricfrom.",".$$po_id.",".$$prod_desc.",".$$prod_id.",".$$findia.",".$$cboDiaWidthType.",".$$txtRollNo.",".$$txtBatchQnty.",".$$txtrecChallan.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
				
				$id_dtls=$id_dtls+1;
			}
			//echo "insert into pro_batch_create_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;
			$rID2=sql_insert("pro_batch_create_dtls",$field_array_dtls,$data_array_dtls,1);//die;
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		else
		{
			//$poBatchNoArr=array();
			$batch_update_id=str_replace("'","",$update_id);
			$serial_no=str_replace("'","",$txt_batch_sl_no);
			
			if($batch_no_creation!=1)
			{
				if(is_duplicate_field( "batch_no", "pro_batch_create_mst", "company_id=$cbo_company_id and batch_no=$txt_batch_number and extention_no=$txt_ext_no and id<>$update_id and entry_form=36" )==1)
				{
					check_table_status( $_SESSION['menu_id'],0);
					echo "11**0"; 
					die;			
				}
			}
		
			$field_array_update="batch_against*batch_no*batch_date*company_id*extention_no*color_id*batch_weight*total_trims_weight*color_range_id*process_id*updated_by*update_date";
			
			$data_array_update=$cbo_batch_against."*".$txt_batch_number."*".$txt_batch_date."*".$cbo_company_id."*".$txt_ext_no."*".$color_id."*".$txt_batch_weight."*".$txt_tot_trims_weight."*".$cbo_color_range."*".$txt_process_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$rID=sql_update("pro_batch_create_mst",$field_array_update,$data_array_update,"id",$update_id,1);
			if($rID) $flag=1; else $flag=0;
			
			$id_dtls_batch=return_next_id( "id", "pro_batch_create_dtls", 1 ) ;
			
			$field_array_dtls="id, mst_id, fabric_from, po_id, item_description, prod_id, fin_dia, width_dia_type, roll_no, batch_qnty, rec_challan, inserted_by, insert_date";
			$field_array_dtls_update="fabric_from*po_id*item_description*prod_id*fin_dia*width_dia_type*roll_no*batch_qnty*rec_challan*updated_by*update_date";
			
			for($i=1;$i<=$total_row;$i++)
			{
				$po_id="poId_".$i;
				$cbofabricfrom="cbofabricfrom_".$i;   
				$prod_id="txtItemDescid_".$i;
				$prod_desc="txtItemDesc_".$i;
				$gsm="txtGsm_".$i;
				$dia="txtDia_".$i;
				$findia="txtFinDia_".$i;
				$txtRollNo="txtRollNo_".$i;
				$txtBatchQnty="txtBatchQnty_".$i;
				$updateIdDtls="updateIdDtls_".$i;
				$cboDiaWidthType="cboDiaWidthType_".$i;
				$txtrecChallan="txtrecChallan_".$i;
				//$itemDesc=str_replace("'","",$$prod_desc).', '.str_replace("'","",$$gsm).', '.str_replace("'","",$$dia);
				
				if(str_replace("'","",$$updateIdDtls)!="")
				{
					$id_arr[]=str_replace("'",'',$$updateIdDtls);
					$data_array_dtls_update[str_replace("'",'',$$updateIdDtls)] = explode("*",($$cbofabricfrom."*".$$po_id."*".$$prod_desc."*".$$prod_id."*".$$findia."*".$$cboDiaWidthType."*".$$txtRollNo."*".$$txtBatchQnty."*".$$txtrecChallan."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));
					
					$id_dtls=str_replace("'",'',$$updateIdDtls);
				}
				else
				{
					if($data_array_dtls!="") $data_array_dtls.=","; 	
					$data_array_dtls.="(".$id_dtls_batch.",".$batch_update_id.",".$$cbofabricfrom.",".$$po_id.",".$$prod_desc.",".$$prod_id.",".$$findia.",".$$cboDiaWidthType.",".$$txtRollNo.",".$$txtBatchQnty.",".$$txtrecChallan.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
					
					$id_dtls_batch=$id_dtls_batch+1;
				}
				
			}
			
			//echo bulk_update_sql_statement( "pro_batch_create_dtls", "id", $field_array_dtls_update, $data_array_dtls_update, $id_arr );die;
			if($data_array_dtls_update!="")
			{
				$rID2=execute_query(bulk_update_sql_statement( "pro_batch_create_dtls", "id", $field_array_dtls_update, $data_array_dtls_update, $id_arr ));
				if($flag==1) 
				{
					if($rID2) $flag=1; else $flag=0; 
				} 
			}
			
			//echo "6**0**insert into pro_batch_create_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;
			if($data_array_dtls!="")
			{
				$rID3=sql_insert("pro_batch_create_dtls",$field_array_dtls,$data_array_dtls,0);
				if($flag==1) 
				{
					if($rID3) $flag=1; else $flag=0; 
				} 
			}
		
			if($txt_deleted_id!="")
			{
				$field_array_status="updated_by*update_date*status_active*is_deleted";
				$data_array_status=$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*0*1";
		
				$rID4=sql_multirow_update("pro_batch_create_dtls",$field_array_status,$data_array_status,"id",$txt_deleted_id,1);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				} 
			}
		}

		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".$batch_update_id."**".$serial_no."**".str_replace("'","",$txt_batch_number);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**1";
			}
		}

		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "1**".$batch_update_id."**".$serial_no."**".str_replace("'","",$txt_batch_number);
			}
			else
			{
				oci_rollback($con);
				echo "6**0**1";
			}
		}
		disconnect($con);
		die;
	}
}

if($action=="batch_popup")
{
  	echo load_html_head_contents("Batch Info","../../", 1, 1, '','1','');
	extract($_REQUEST);
?>
	<script>
	function js_set_value( batch_id)
	{
		//alert (batch_id);
		document.getElementById('hidden_batch_id').value=batch_id;
		parent.emailwindow.hide();
	}
    </script>
</head>
<body>
<div align="center">
	<fieldset style="width:600px;margin-left:4px;">
        <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
            <table cellpadding="0" cellspacing="0" border="1" rules="all" width="500" class="rpt_table">
                <thead>
                    <tr>
                        <th colspan="3"><? echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" ); ?></th>
                    </tr>
                	<tr>
                        <th>Search By</th>
                        <th>Search</th>
                        <th>
                            <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                            <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" value="">
                        </th>
                    </tr> 
                </thead>
                <tr class="general">
                    <td align="center">	
                        <?
                            $search_by_arr=array(1=>"Batch No");
                            echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>                 
                    <td align="center">				
                        <input type="text" style="width:140px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 						
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<? echo $cbo_company_id; ?>+'_'+document.getElementById('cbo_string_search_type').value, 'create_batch_search_list_view', 'search_div', 'subcon_batch_creation_controller', 'setFilterGrid(\'list_view\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
            <div id="search_div" style="margin-top:10px"></div>   
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}

if($action=="create_batch_search_list_view")
{
	//print_r ($data);
	$data=explode('_',$data);
	$search_common=$data[1];
	$batch_number_search =$data[0];
	$company_id =$data[2];
	$search_type =$data[3];
	
	$po_arr=return_library_array( "select id,order_no from subcon_ord_dtls",'id','order_no');
	
	if($search_type==1)
	{
		if($batch_number_search!='') $batch_number_cond=" and a.batch_no='$batch_number_search'"; else $batch_number_cond="";
	}
	else if($search_type==4 || $search_type==0)
	{
		if($batch_number_search!='') $batch_number_cond=" and a.batch_no like '%$batch_number_search%'"; else $batch_number_cond="";
	}
	else if($search_type==2)
	{
		if($batch_number_search!='') $batch_number_cond=" and a.batch_no like '$batch_number_search%'"; else $batch_number_cond="";
	}
	else if($search_type==3)
	{
		if($batch_number_search!='') $batch_number_cond=" and a.batch_no like '%$batch_number_search'"; else $batch_number_cond="";
	}	
	
	if($db_type==0)
	{
		$sql = "select a.id, a.batch_no, a.extention_no, a.batch_weight, a.total_trims_weight, a.batch_date, a.color_id, group_concat(b.po_id) as po_id from pro_batch_create_mst a,  pro_batch_create_dtls b where a.id=b.mst_id and a.company_id=$company_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.entry_form=36 $batch_number_cond group by a.id, a.batch_no, a.extention_no order by a.id DESC"; 
	}
	elseif($db_type==2)
	{
		$sql = "select a.id, a.batch_no, a.extention_no, a.batch_weight, a.total_trims_weight, a.batch_date, a.color_id, listagg(b.po_id,',') within group (order by b.po_id) as po_id from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.company_id=$company_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.entry_form=36 $batch_number_cond group by a.id, a.batch_no, a.extention_no, a.batch_weight, a.total_trims_weight, a.batch_date, a.color_id order by a.id DESC"; 
	}
	//echo $sql;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="618" class="rpt_table" >
            <thead>
                <th width="30">SL</th>
                <th width="70">Batch No</th>
                <th width="40">Ex.</th>
                <th width="90">Color</th>
                <th width="80">Batch Weight</th>
                <th width="80">Total Trims Weight</th>
                <th width="70">Batch Date</th>
                <th>PO No.</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:280px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="list_view" >
            <?
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$order_no='';
					$order_id=array_unique(explode(",",$selectResult[csf("po_id")]));
					foreach($order_id as $val)
					{
						if($order_no=="") $order_no=$po_arr[$val]; else $order_no.=", ".$po_arr[$val];
					}
					
				?>
                    <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<? echo $i;?>" onClick="js_set_value(<? echo $selectResult[csf('id')]; ?>)"> 
                        <td width="30" align="center"><? echo $i; ?></td>	
                        <td width="70" align="center"><p><? echo $selectResult[csf('batch_no')]; ?></p></td>
                        <td width="40" align="center"><? echo $selectResult[csf('extention_no')]; ?></td>
                        <td width="90"  align="center"><p><? echo  $color_arr[$selectResult[csf('color_id')]]; ?></p></td> 
                        <td width="80"  align="center"><p><? echo $selectResult[csf('batch_weight')]; ?></p></td>
                        <td width="80"  align="center"><p><? echo $selectResult[csf('total_trims_weight')]; ?></p></td>
                        <td width="70"  align="center"><p><? echo $selectResult[csf('batch_date')]; ?></p></td>
                        <td><p><? echo $order_no; ?></p></td>
                    </tr>
                <? 
                	$i++;
				}
			?>
            </table>
        </div> 
    </div>   
    <?
	//echo  create_list_view("list_view", "Batch No,Ext. No,Batch Weight,Total Trims Weight, Batch Date, Color", "100,70,80,80,80,80","600","250",0, $sql, "js_set_value", "id", "", 1, "0,0,0,0,0,color_id", $arr, "batch_no,extention_no,batch_weight,total_trims_weight,batch_date,color_id", "",'','0,0,2,2,3,0');
	
exit();	
}

if ($action=="populate_data_from_search_popup")
{
	$data=explode("**",$data);
	$batch_id=$data[1];
	$batch_against=$data[0];
	//echo "select id, company_id, batch_no, extention_no, batch_weight, total_trims_weight, batch_date, color_id, color_range_id, process_id, DATE_FORMAT(insert_date,'%y') as year from pro_batch_create_mst where id='$batch_id'";
	if($db_type==0)
	{
		$year_cond=" DATE_FORMAT(insert_date,'%y') as year";
	}
	else if($db_type==2)
	{
		$year_cond=" TO_CHAR(insert_date,'RR') as year";
	}
	
	$dyeing_batch="Select batch_id from pro_fab_subprocess where batch_id='$batch_id' and entry_form=38 and status_active=1 and is_deleted=0";
	$dyeing_batch_result=sql_select($dyeing_batch);
	foreach ($dyeing_batch_result as $row)
	{
		echo "document.getElementById('dyeing_batch_id').value 	= '".$row[csf("batch_id")]."';\n";  
	}
	//echo "select id, company_id, batch_against, batch_no, extention_no, batch_weight, total_trims_weight, batch_date, re_dyeing_from, color_id, color_range_id, process_id, $year_cond from pro_batch_create_mst where id='$batch_id' and entry_form=36";
	$data_array=sql_select("select id, company_id, batch_against, batch_no, extention_no, batch_weight, total_trims_weight, batch_date, re_dyeing_from, color_id, color_range_id, process_id, $year_cond from pro_batch_create_mst where id='$batch_id' and entry_form=36");
	foreach ($data_array as $row)
	{
		if($row[csf("extention_no")]==0) $ext_no=''; else $ext_no=$row[csf("extention_no")];
		
		$serial_no=$row[csf("id")]."-".$row[csf("year")];
		
		$process_name='';
		$process_id_array=explode(",",$row[csf("process_id")]);
		foreach($process_id_array as $val)
		{
			if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=",".$conversion_cost_head_array[$val];
		}
		
		echo "document.getElementById('txt_batch_sl_no').value 		= '".$serial_no."';\n"; 
		echo "document.getElementById('cbo_batch_against').value 	= '".$row[csf("batch_against")]."';\n";    
		echo "active_inactive();\n";
		echo "document.getElementById('txt_batch_date').value 		= '".change_date_format($row[csf("batch_date")])."';\n";  
		echo "document.getElementById('txt_batch_weight').value 	= '".$row[csf("batch_weight")]."';\n";  
		echo "document.getElementById('cbo_company_id').value 		= '".$row[csf("company_id")]."';\n";  
		echo "document.getElementById('txt_tot_trims_weight').value = '".$row[csf("total_trims_weight")]."';\n";  
		echo "document.getElementById('txt_batch_number').value 	= '".$row[csf("batch_no")]."';\n";  
		echo "document.getElementById('txt_ext_no').value 			= '".$ext_no."';\n";  
		echo "document.getElementById('txt_batch_color').value 		= '".$color_arr[$row[csf("color_id")]]."';\n";  
		echo "document.getElementById('cbo_color_range').value 		= '".$row[csf("color_range_id")]."';\n";
		echo "document.getElementById('txt_process_id').value 		= '".$row[csf("process_id")]."';\n";
		echo "document.getElementById('txt_process_name').value 	= '".$process_name."';\n";
		echo "document.getElementById('update_id').value 			= '".$row[csf("id")]."';\n";
		echo "document.getElementById('hide_update_id').value 		= '';\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_batch_creation',1);\n";	
		
		if($batch_against==2)
		{
			echo "document.getElementById('cbo_batch_against').value = '".$batch_against."';\n";
			echo "$('#txt_ext_no').removeAttr('disabled','disabled');\n";
			echo "$('#txt_batch_color').attr('disabled','disabled');\n";
			echo "$('#txt_batch_number').attr('readOnly','readOnly');\n";
			echo "$('#cbo_color_range').attr('disabled','disabled');\n";
			echo "$('#txt_process_name').attr('disabled','disabled');\n";
		}
		
		if($row[csf("batch_against")]==2)
		{
			$prv_batch_against=return_field_value("batch_against","pro_batch_create_mst","id=$row[re_dyeing_from]");
			echo "document.getElementById('hide_batch_against').value = '".$prv_batch_against."';\n"; 
			echo "document.getElementById('hide_update_id').value = '".$row[csf("id")]."';\n";
		}
		else
		{
			echo "document.getElementById('hide_batch_against').value = '".$row[csf("batch_against")]."';\n"; 
			echo "document.getElementById('hide_update_id').value = '';\n";
		}
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_batch_creation',1);\n";	 		
		 
	}
	exit();
}

if( $action == 'batch_details' ) 
{
	$data=explode('**',$data);
	$batch_id=$data[1];
	$batch_against=$data[0];
	$dyeing_batch_id=$data[2];
	$tblRow=0;
 
	if($batch_against==2)
	{
		$disbled="disabled='disabled'";
		$disbled_drop_down=1; 
	}
	elseif ($batch_against==1)
	{
		if ($dyeing_batch_id=='')
		{
			$disbled="";
			$disbled_drop_down=0; 
		}
		else
		{
			$disbled="disabled='disabled'";
			$disbled_drop_down=1; 
		}
	}
 
	$process_arr=return_library_array( "select id, main_process_id from subcon_ord_dtls",'id','main_process_id');
	$data_array=sql_select("select b.id, b.fabric_from, b.po_id, b.item_description, b.prod_id, b.fin_dia, b.width_dia_type, b.roll_no, b.batch_qnty, b.rec_challan from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and b.mst_id='$batch_id' and b.status_active=1 and b.is_deleted=0 and a.entry_form=36 order by b.id"); 
	foreach($data_array as $row)
	{
		$tblRow++;
		?>
		<tr class="general" id="tr_<? echo $tblRow; ?>">
			<?
				$po_no=$row[csf('po_id')];
				$item_id=$row[csf('prod_id')];
				$item_desc=$row[csf('item_description')];
				$ex_item_desc=explode(',',$item_desc);
				$po_number=return_field_value("order_no","subcon_ord_dtls","id='$po_no' and status_active=1 and is_deleted=0 group by order_no",'order_no');
				?>
                <td>
                    <?
                        $fabricfrom=array(1=>"Receive",2=>"Production");
                        echo create_drop_down( "cbofabricfrom_".$tblRow, 70, $fabricfrom,"", 0, "", $row[csf('fabric_from')], "",$disbled_drop_down );
                    ?>
                </td>
				<td align='center' >
					<input type="text" name="txtPoNo_<? echo $tblRow; ?>" id="txtPoNo_<? echo $tblRow; ?>" class="text_boxes" style="width:70px;" placeholder="Browse or Write" onDblClick="openmypage_po(<? echo $tblRow; ?>)" value="<? echo $po_number; ?>" <? echo $disbled; ?> readonly /><!--onChange="check_po_no(this.value);"-->
				</td>
				<td align='center' id='itemDescTd_<? echo $tblRow; ?>'>
				<?
					/*if($row[csf('fabric_from')]==1)
					{
						if ($process_arr[$row[csf('po_id')]]==2 || $process_arr[$row[csf('po_id')]]==3 || $process_arr[$row[csf('po_id')]]==4 || $process_arr[$row[csf('po_id')]]==6 || $process_arr[$row[csf('po_id')]]==7)
						{
							if ($db_type==0)
							{
								$sql="select concat(b.material_description,',',b.gsm,',',b.grey_dia,',',b.fin_dia) as material_description from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and b.order_id in (".$row[csf('po_id')].") and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.material_description, b.gsm, b.grey_dia, b.fin_dia"; 
							}
							elseif($db_type==2)
							{
								$sql="select b.material_description || ',' || b.gsm || ',' || b.grey_dia || ',' || b.fin_dia as material_description from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and b.order_id in (".$row[csf('po_id')].") and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.material_description, b.gsm, b.grey_dia, b.fin_dia"; 
							}							
							//$sql="select b.id, b.material_description from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and b.order_id in (".$row[csf('po_id')].") and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.id, b.material_description "; 

							echo create_drop_down( "cboItemDesc_".$tblRow, 150, $sql,'material_description,material_description', 1, "--Select Item Desc--",$row[csf('item_description')],"gsm_dia_load(".$tblRow.")",$disbled_drop_down);
						}*/						
/*						else if ($process_arr[$row[csf('po_id')]]==1 || $process_arr[$row[csf('po_id')]]==5 || $process_arr[$row[csf('po_id')]]==8 || $process_arr[$row[csf('po_id')]]==9)
						{
							create_drop_down( "cboItemDesc_".$tblRow, 150, $garments_item,'', 1, "-- Select Item Desc --",'0',"get_php_form_data(document.getElementById('cbofabricfrom_".$tblRow."').value+'_'+this.value+'_'+".$tblRow.",'load_receive_production_data','requires/subcon_batch_creation_controller');",$disbled_drop_down);
						}
*/					/*}
					else if($row[csf('fabric_from')]==2)
					{
						if ($db_type==0)
						{
							$sql="select concat(fabric_description,',',gsm,',',dia_width) as fabric_description from subcon_production_dtls where order_id in (".$row[csf('po_id')].") and status_active=1 group by fabric_description, gsm, dia_width"; 
						}
						elseif($db_type==2)
						{
							$sql="select fabric_description || ',' || gsm || ',' || dia_width as fabric_description from subcon_production_dtls where order_id in (".$row[csf('po_id')].") and status_active=1 group by fabric_description, gsm, dia_width"; 
						}
						echo create_drop_down( "cboItemDesc_".$tblRow, 150, $sql,'fabric_description,fabric_description', 1, "--Select Item Desc--",$row[csf('item_description')],"gsm_dia_load(".$tblRow.")",$disbled_drop_down);*/
						/*}
						else if ($process_arr[$row[csf('po_id')]]==3 || $process_arr[$row[csf('po_id')]]==4 || $process_arr[$row[csf('po_id')]]==6 || $process_arr[$row[csf('po_id')]]==7)
						{
							//$sql="select id,const_comp from lib_subcon_charge where id in ($item_id) and status_active=1 and rate_type_id in (2,3)";
							$sql="select id,const_comp from lib_subcon_charge where id in ($item_id) and status_active=1 and rate_type_id in (2,3,4,6,7)"; 
							echo create_drop_down( "cboItemDesc_".$tblRow, 150, $sql,'id,const_comp', 1, "-- Select Item Desc --",$row[csf('prod_id')],'',$disbled_drop_down,$item_id);  
						}						
						else if ($process_arr[$row[csf('po_id')]]==1 || $process_arr[$row[csf('po_id')]]==5 || $process_arr[$row[csf('po_id')]]==8 || $process_arr[$row[csf('po_id')]]==9)
						{
							create_drop_down( "cboItemDesc_".$tblRow, 150, $garments_item,'', 1, "-- Select Item Desc --",'0',"get_php_form_data(document.getElementById('cbofabricfrom_".$tblRow."').value+'_'+this.value+'_'+".$tblRow.",'load_receive_production_data','requires/subcon_batch_creation_controller');",$disbled_drop_down);
						}
					}	*/			
				?>
                <input type="text" name="txtItemDesc_<? echo $tblRow; ?>" id="txtItemDesc_<? echo $tblRow; ?>" value="<? echo $row[csf('item_description')]; ?>" class="text_boxes" style="width:130px" placeholder="Browse" onDblClick="openmypage_itemdes(<? echo $tblRow; ?>)" readonly />
                <input type="hidden" name="txtItemDescid_<? echo $tblRow; ?>" id="txtItemDescid_<? echo $tblRow; ?>" value="<? echo $row[csf('prod_id')]; ?>" class="text_boxes" style="width:60px" />
				</td>
				<td id="gsmTd_<? echo $tblRow; ?>">
					<input type="text" name="txtGsm_<? echo $tblRow; ?>" id="txtGsm_<? echo $tblRow; ?>" value="<? echo $ex_item_desc[1]; ?>" class="text_boxes_numeric" style="width:50px" <? echo $disbled; ?> readonly />
				</td>
				<td id="diaTd_<? echo $tblRow; ?>">
					<input type="text" name="txtDia_<? echo $tblRow; ?>" id="txtDia_<? echo $tblRow; ?>" value="<? echo $ex_item_desc[2]; ?>" class="text_boxes_numeric" style="width:50px" <? echo $disbled; ?> readonly />
				</td>
				<td id="finDiaTd_<? echo $tblRow; ?>">
					<input type="text" name="txtFinDia_<? echo $tblRow; ?>" id="txtFinDia_<? echo $tblRow; ?>" value="<? echo $row[csf('fin_dia')]; ?>" class="text_boxes_numeric" style="width:50px" <? echo $disbled; ?> readonly />
				</td>
				<td id='DiaWidthType_<? echo $tblRow; ?>'>
					<?
						echo create_drop_down( "cboDiaWidthType_".$tblRow, 100, $fabric_typee,"",1, "-- Select --", $row[csf('width_dia_type')], "",$disbled_drop_down );
					?>
				</td>
			<td>
				<input type="text" name="txtRollNo_<? echo $tblRow; ?>" id="txtRollNo_<? echo $tblRow; ?>" class="text_boxes" style="width:50px" value="<? if($row[csf('roll_no')]!=0) echo $row[csf('roll_no')]; ?>" <? echo $disbled; ?> />
				<input type="hidden" name="poId_<? echo $tblRow; ?>" id="poId_<? echo $tblRow; ?>"  value="<? echo $po_no; ?>" class="text_boxes" readonly />
				<input type="hidden" name="updateIdDtls_<? echo $tblRow;?>" id="updateIdDtls_<? echo $tblRow; ?>" class="text_boxes" value="<? echo $row[csf('id')]; ?>" readonly />
				<input type="hidden" name="processId_<? echo $tblRow;?>" id="processId_<? echo $tblRow;?>" style="width:50px" class="text_boxes" readonly />
			</td>
			<td>
				<input type="text" name="txtBatchQnty_<? echo $tblRow; ?>"  id="txtBatchQnty_<? echo $tblRow; ?>" class="text_boxes_numeric" onKeyUp="calculate_batch_qnty();" style="width:80px" value="<? echo $row[csf('batch_qnty')]; ?>" <? echo $disbled; ?> />
			</td>
            <td>
                <input type="text" name="txtrecChallan_<? echo $tblRow; ?>" id="txtrecChallan_<? echo $tblRow; ?>" value="<? echo $row[csf('rec_challan')]; ?>" class="text_boxes" style="width:80px" />
            </td>
			<td width="70">
				<input type="button" id="increase_<? echo $tblRow; ?>" name="increase_<? echo $tblRow; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr(<? echo $tblRow; ?>)" />
				<input type="button" id="decrease_<? echo $tblRow; ?>" name="decrease_<? echo $tblRow; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<? echo $tblRow; ?>);" />
			</td>
		</tr>
	<?
	}
	exit();
}

if($action=="process_name_popup")
{
  	echo load_html_head_contents("Process Name Info","../../", 1, 1, '','1','');
	extract($_REQUEST);
?>
	<script>
	
		$(document).ready(function(e) {
			setFilterGrid('tbl_list_search',-1);
		});
		
		var selected_id = new Array(); var selected_name = new Array(); var buyer_id=''; var style_ref_array= new Array();
		
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_process_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		
		function js_set_value( str ) 
		{
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hidden_process_id').val(id);
			$('#hidden_process_name').val(name);
		}
    </script>
</head>
<body>
<div align="center">
	<fieldset style="width:370px;margin-left:10px">
    	<input type="hidden" name="hidden_process_id" id="hidden_process_id" class="text_boxes" value="">
        <input type="hidden" name="hidden_process_name" id="hidden_process_name" class="text_boxes" value="">
        <form name="searchprocessfrm_1"  id="searchprocessfrm_1" autocomplete="off">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="350" class="rpt_table" >
                <thead>
                    <th width="50">SL</th>
                    <th>Process Name</th>
                </thead>
            </table>
            <div style="width:350px; overflow-y:scroll; max-height:280px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="332" class="rpt_table" id="tbl_list_search" >
                <?
                    $i=1; $process_row_id='';// $not_process_id_print_array=array(1,2,3,4,101,120,121,122,124); 
					$process_id_print_array=array(25,31,32,33,34,39,60,63,64,65,66,68,69,70,71,82,83,84,89,90,91,93,125,129,132,133,136);
					$hidden_process_id=explode(",",$txt_process_id);
                    foreach($conversion_cost_head_array as $id=>$name)
                    {
						if(in_array($id,$process_id_print_array))
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							 
							if(in_array($id,$hidden_process_id)) 
							{ 
								if($process_row_id=="") $process_row_id=$i; else $process_row_id.=",".$i;
							}
							?>
							<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<? echo $i;?>" onClick="js_set_value(<? echo $i;?>)"> 
								<td width="50" align="center"><?php echo "$i"; ?>
									<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<? echo $id; ?>"/>	
									<input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<? echo $name; ?>"/>
								</td>	
								<td><p><? echo $name; ?></p></td>
							</tr>
							<?
							$i++;
						}
                    }
                ?>
                    <input type="hidden" name="txt_process_row_id" id="txt_process_row_id" value="<?php echo $process_row_id; ?>"/>
                </table>
            </div>
             <table width="350" cellspacing="0" cellpadding="0" style="border:none" align="center">
                <tr>
                    <td align="center" height="30" valign="bottom">
                        <div style="width:100%"> 
                            <div style="width:50%; float:left" align="left">
                                <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                            </div>
                            <div style="width:50%; float:left" align="left">
                                <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	set_all();
</script>
</html>
<?
exit();
}

if($action=="batch_no_creation")
{
	$batch_no_creation=return_field_value("batch_no_creation","variable_settings_production","company_name ='$data' and variable_list=24 and is_deleted=0 and status_active=1");

	if($batch_no_creation!=1) $batch_no_creation=0;
	
	echo "document.getElementById('batch_no_creation').value 				= '".$batch_no_creation."';\n";
	echo "$('#txt_batch_number').val('');\n";
	echo "$('#update_id').val('');\n";
	if($batch_no_creation==1)
	{
		echo "$('#txt_batch_number').attr('readonly','readonly');\n";
	}
	else
	{
		echo "$('#txt_batch_number').removeAttr('readonly','readonly');\n";
	}
	exit();	
}

if($action=="roll_maintained")
{
	
	$roll_maintained=return_field_value("fabric_roll_level","variable_settings_production","company_name ='$data' and variable_list=3 and is_deleted=0 and status_active=1");
	if($roll_maintained=="" || $roll_maintained==2) $roll_maintained=0; else $roll_maintained=$roll_maintained;
	echo "document.getElementById('roll_maintained').value 				= '".$roll_maintained."';\n";
	exit();	
}

if($action=="batch_card_print")
{
	extract($_REQUEST);
	$data=explode('*',$data);
	$company=$data[0];
	$update_id=$data[1];
	$batch_sl_no=$data[2];
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name");
	$job_buyer=return_library_array( "select subcon_job, party_id from subcon_ord_mst", "subcon_job", "party_id");
	if($db_type==0)
	{
		$sql=" select a.id, a.batch_no, a.color_id, a.color_range_id, a.extention_no, a.batch_weight, a.process_id, group_concat(distinct(c.order_no)) AS po_number, a.total_trims_weight, c.job_no_mst, c.delivery_date from pro_batch_create_mst a, pro_batch_create_dtls b, subcon_ord_dtls c where a.entry_form=36 and a.id=b.mst_id and b.po_id=c.id and a.id=$update_id  and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.batch_no, a.extention_no";
	}
	else if ($db_type==2)
	{
		$sql=" select a.batch_no, a.color_id, a.color_range_id, a.extention_no, a.batch_weight, a.process_id, listagg(cast(c.order_no as varchar2(4000)),',') within group (order by c.order_no) as po_number, a.total_trims_weight, listagg(cast(c.job_no_mst as varchar2(4000)),',') within group (order by c.job_no_mst) as job_no_mst, listagg(cast(c.delivery_date as varchar2(4000)),',') within group (order by c.delivery_date) as delivery_date from pro_batch_create_mst a, pro_batch_create_dtls b, subcon_ord_dtls c where a.entry_form=36 and a.id=b.mst_id and b.po_id=c.id and a.id=$update_id  and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.batch_no, a.color_id, a.color_range_id, a.extention_no, a.batch_weight, a.process_id, a.total_trims_weight ";
	}

	$dataArray=sql_select($sql);
?>
    <div style="width:930px">
    <div align="right"><strong>Printing Time: &nbsp;</strong> <? echo $date=date("F j, Y, g:i a"); ?> </div>
	<table width="930" cellspacing="0" align="center" border="0">
        <tr>
            <td colspan="6" align="center" style="font-size:22px"><strong><? echo $company_library[$company]; ?></strong></td>
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:18"><strong><u> Batch Card</u></strong></td>
        </tr>
         <tr>
            <td colspan="6" align="left" style="font-size:16px;"><strong><u>Reference Details</u></strong></td>
        </tr>
        <tr>
            <td width="120"><strong>Batch No : </strong></td><td width="110px"><? echo $dataArray[0][csf('batch_no')]; ?></td>
            <td width="120"><strong>Batch Ext. : </strong></td><td width="110px"><? echo $dataArray[0][csf('extention_no')]; ?></td>
            <td width="120"><strong>Batch Serial : </strong></td><td width="110px"><? echo $batch_sl_no; ?></td>
        </tr>
        <tr>
            <td><strong>Batch Color : </strong></td><td width="110px"><? echo   $color_arr[$dataArray[0][csf('color_id')]]; ?></td>
            <td><strong>Color Range : </strong></td><td><? echo $color_range[$dataArray[0][csf('color_range_id')]];?></td>
            <td><strong>Batch Weight : </strong></td><td><? echo $dataArray[0][csf('batch_weight')];?></td>
        </tr>
        <tr>
            <td><strong>Party : </strong></td><td><? $job_no_party=implode(",",array_unique(explode(",",$dataArray[0][csf('job_no_mst')]))); echo $buyer_arr[$job_buyer[$job_no_party]] ; ?></td>
            <td><strong>Job No : </strong></td><td><? 
			if($db_type==0)
			{
				$job_no=$dataArray[0][csf('job_no_mst')];
			}
			else if ($db_type==2)
			{
				$job_no=implode(",",array_unique(explode(",",$dataArray[0][csf('job_no_mst')])));
			}
			
			echo $job_no; ?></td>
            <td><strong>Delivery Date : </strong></td><td><? echo  change_date_format($dataArray[0][csf('delivery_date')]); ?></td>
        </tr>
        <tr>
            <td align="left"><strong>Order No : </strong></td><td colspan="5" align="left"><? 
			if($db_type==0)
			{
				$po_no=$dataArray[0][csf('po_number')];
			}
			else if ($db_type==2)
			{
				$po_no=implode(",",array_unique(explode(",",$dataArray[0][csf('po_number')])));
			}
			echo $po_no; ?></td>
        </tr>
    </table>
    <div style="float:left; margin-left:10px;"><strong><u>Fabrication Details</u></strong> </div>
    <table align="center" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
       <thead bgcolor="#dddddd">
        <tr>
            <th width="30">SL</th>
            <th width="150">Const. & Comp.</th>
            <th width="50">GSM</th>
            <th width="60">Grey Dia/Width</th>
            <th width="60">Fin. Dia/Width</th>
            <th width="80">M/Dia X Gauge</th>
            <th width="80">D/W Type</th>
          
            <th width="80">Grey Qty.</th>
            <th width="50">Roll No.</th>
            <th width="70">Yarn Lot</th>
            <th width="70">Yarn Count</th>
            <th width="">ID Code</th>
         </tr>
        </thead>
        <tbody>
		<?
			$i=1;
			$yarncount=return_library_array( "select id, yarn_count from  lib_yarn_count",'id','yarn_count');
			$yarn_lot_supp=return_library_array( "select lot, supplier_id from  product_details_master",'lot','supplier_id');
			$machine_lib_dia=return_library_array( "select id,dia_width from  lib_machine_name", "id", "dia_width"  );
			$machine_lib_gauge=return_library_array( "select id,gauge from  lib_machine_name", "id", "gauge"  );
			
			$yarn_dtls_arr=array();
			$yarn_lot_data=sql_select("select order_id, cons_comp_id, yarn_lot, yrn_count_id, machine_id from  subcon_production_dtls where product_type=2 and status_active=1 and is_deleted=0 and yarn_lot!='' group by cons_comp_id, order_id");
			foreach($yarn_lot_data as $rows)
			{
				$yarn_dtls_arr[$rows[csf('cons_comp_id')]][$rows['order_id']]['yarn_lot']=$rows[csf('yarn_lot')];//implode(",",array_unique($rows[csf('yarn_lot')]));
				$yarn_dtls_arr[$rows[csf('cons_comp_id')]][$rows['order_id']]['yarn_count']=$rows[csf('yarn_count')];
				$yarn_dtls_arr[$rows[csf('cons_comp_id')]][$rows['order_id']]['machine_no_id']=$rows[csf('machine_no_id')];
			}
			//var_dump($yarn_dtls_arr);
			$sql_dtls="select id, SUM(batch_qnty) AS batch_qnty, roll_no, item_description, fin_dia, po_id, prod_id, width_dia_type from pro_batch_create_dtls where mst_id=$update_id and  status_active=1 and is_deleted=0 GROUP BY id, roll_no, item_description, fin_dia, po_id, prod_id, width_dia_type ";
	//echo $sql_dtls;
			$sql_result=sql_select($sql_dtls);
	
			foreach($sql_result as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$desc=explode(",",$row[csf('item_description')]); 
				$y_count=$yarn_dtls_arr[$rows[csf('prod_id')]][$rows['po_id']]['yarn_count'];
				$y_count_id=explode(',',$y_count);
				$yarn_count_value='';
				foreach ($y_count_id as $val)
				{
					if($yarn_count_value=='') $yarn_count_value=$yarncount[$val]; else $yarn_count_value.=", ".$yarncount[$val];
				
				}
				$yarn_lot_d=$yarn_dtls_arr[$rows[csf('prod_id')]][$rows['po_id']]['yarn_lot'];
				$exp_lot=explode(',',$yarn_lot_d);
					
				$machine_dia_up=$machine_lib_dia[$yarn_dtls_arr[$row[csf('prod_id')]][$row['po_id']]['machine_no_id']];
				$machine_gauge_up=$machine_lib_gauge[$yarn_dtls_arr[$row[csf('prod_id')]][$row['po_id']]['machine_no_id']];
					
					?>
					<tr bgcolor="<? echo $bgcolor; ?>" >
						<td width="30"><? echo $i; ?></td>
						<td width="150" ><? echo $desc[0]; ?></td>
						<td width="50" align="center"><? echo $desc[1]; ?></td>
						<td width="60" align="center"><? echo $desc[2]; ?></td>
						<td width="60"><? echo $row[csf('fin_dia')]; ?></td>
						<td width="80" align="center"><? if($machine_gauge_up!='') echo $machine_dia_up." X ".$machine_gauge_up; else echo $machine_dia_up; ?></td>
					  
						<td width="80" align="center"><? echo $fabric_typee[$row[csf('width_dia_type')]]; ?></td>
						<td width="80" align="right"><? echo number_format($row[csf('batch_qnty')],2); ?></td>
						<td width="50" align="center"><? echo $row[csf('roll_no')];  ?></td>
						<td width="70" align="left"><p><? echo $yarn_dtls_arr[$rows[csf('prod_id')]][$rows['po_id']]['yarn_lot'];  ?></p></td>
						<td width="70"><? echo $yarn_count_value; ?></td>
						<td width="">&nbsp;</td>
					</tr>
					
				<?php
			   $b_qty+= $row[csf('batch_qnty')];
				
			$i++;
			}
			?>
        </tbody>
        <tr>
            <td colspan="7" align="right"><b>Sum:</b></td>
            <td align="right" ><b><? echo number_format($b_qty,2); ?> </b></td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7" align="right"><b>Trims Weight:</b></td>
            <td align="right"><b><? echo number_format($dataArray[0][csf('total_trims_weight')],2); ?> </b></td>
            <td colspan="4">&nbsp;</td>
        </tr>
         <tr>
            <td colspan="7" align="right"><b>Total:</b></td>
            <td align="right"><b><? echo number_format($b_qty+$dataArray[0][csf('total_trims_weight')],2); ?></b></td>
            <td colspan="4">&nbsp;</td>
        </tr>
         <tr>
            <td colspan="12"  align="right">&nbsp; </td>
        </tr>
			<? 
                $process=$dataArray[0][csf('process_id')];
                $process_id=explode(',',$process);
                $process_value='';
                $i=1;
                foreach ($process_id as $val)
                {
                    if($process_value=='') $process_value=$i.'. '. $conversion_cost_head_array[$val]; else $process_value.=", ".$i.'. '.$conversion_cost_head_array[$val];
                    $i++;
                }
             ?>
            <tr>
                <th colspan="12" align="left" ><strong>Process Required</strong></th>
            </tr>
            <tr>
                <td colspan="12" title="<? echo $process_value; ?>"> <strong><? echo $process_value; ?> </strong></td>
            </tr>
            <tr>
                <td colspan="4" align="left">Heat Setting:</td>
                <td colspan="4" align="left">Loading Date:</td>
                <td colspan="4" align="left">UnLoading Date:</td>
            </tr>
        </table>
     <div style="float:left; margin-left:10px;"><strong> Quality Instruction(<i>Hand Written</i>)</strong> </div>
    <table width="930" cellspacing="0" align="center" >
        <tr>
            <td valign="top" align="left" width="440">
                <table cellspacing="0" width="430"  align="left" border="1" rules="all" class="rpt_table">
                    <tr>
                    	<th>SL</th><th>Roll No</th><th>Actual Dia</th><th>Roll Wgt.</th><th>Remarks</th>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                </table>
            </td>
            <td width="15" align="justify" valign="top">&nbsp;</td>
            <td width="440" valign="top" align="right">
                <table width="430"  cellspacing="0"  border="1" rules="all" class="rpt_table">
                    <tr>
                        <th>SL</th><th>Roll No</th><th>Actual Dia</th><th>Roll Wgt.</th><th>Remarks</th>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="440" valign="top">
                <table width="430" cellspacing="0" border="1" rules="all" class="rpt_table">
                    <tr>
                        <th align="left"><strong>Shade Result(<i>Hand Written</i>)</strong></th>
                    </tr>
                    <tr>
                        <td colspan="1" style="width:451px; height:80px" >&nbsp;</td>
                    </tr>
                </table>
        	</td>
            <td width="15" align="justify" valign="top">&nbsp;</td>
            <td width="440" valign="top" align="right">
                <table cellspacing="0" border="1" rules="all" class="rpt_table" width="428" >
                    <tr>
                        <th align="left" colspan="3"><strong>Shrinkage(<i>Hand Written</i>)</strong></th>
                    </tr>
                    <tr>
                        <th><b>Length % </b></th><th><b>Width % </b></th><th><b> Twist % </b></th>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td><td>&nbsp; </td><td>&nbsp; </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="930" colspan="3">
                <table cellspacing="0" border="1" rules="all" class="rpt_table" width="930" >
                    <tr>
                        <th align="center"><strong>Other Information(<i>Hand Written</i>)</strong></th>
                    </tr>
                    <tr>
                        <td style="width:930px; height:120px" >&nbsp;</td>
                    </tr>
                </table> 
            </td>
        </tr>
    </table>
    <br>
		<?
            echo signature_table(56, $company, "930px");
        ?>
    </div>
	<? 
}
?>