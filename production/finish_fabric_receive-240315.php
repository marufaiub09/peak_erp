<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Finish Fabric Production Entry

Functionality	:	
JS Functions	:
Created by		:	Kausar
Creation date 	: 	19-05-2013
Updated by 		: 	Fuad Shahriar	
Update date		: 	25-05-2013	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/ 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Finish Production Entry Info","../", 1, 1, "",'1','');
?>

<script>

	var permission='<?php echo $permission; ?>';
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];

	$(document).ready(function(e)
	{
          $("#txt_color").autocomplete({
			 source: str_color
		  });
     });

	function set_production_basis()
	{
		var production_basis = $('#cbo_production_basis').val();
		$('#list_fabric_desc_container').html('');
		document.getElementById('body_part_td').innerHTML='<?php echo create_drop_down( "cbo_body_part", 182, $body_part,"", 1, "-- Select Body Part --", 0, "",0 ); ?>';
		$('#buyer_id').val('');
		$('#buyer_name').val('');
		$('#txt_production_qty').val('');
		$('#all_po_id').val('');
		$('#save_data').val('');
		$('#batch_booking_without_order').val('');
		
		$('#txt_production_qty').attr('readonly','readonly');
		$('#txt_production_qty').attr('onClick','openmypage_po();');	
		$('#txt_production_qty').attr('placeholder','Single Click to Search');
		
        if(production_basis == 4)
        {
			$('#cbo_body_part').val(0);
			//$('#cbo_body_part').removeAttr('disabled','disabled');
			$('#txt_fabric_desc').val('');
			$('#txt_fabric_desc').removeAttr('disabled','disabled');
			$('#txt_color').val('');
			$('#txt_color').removeAttr('disabled','disabled');
			$("#txt_batch_no").val('');
			$("#txt_batch_id").val('');
			$('#txt_batch_no').removeAttr("onDblClick");
			$('#txt_batch_no').attr("placeholder","Write");
			$('#fabric_desc_id').val('');
			$('#txt_fabric_desc').attr("onDblClick","openmypage_fabricDescription();");
			$('#txt_fabric_desc').attr("placeholder","Double Click For Search");
			
        }
        else if(production_basis == 5)
        {
			$('#cbo_body_part').val(0);
			//$('#cbo_body_part').attr('disabled','disabled');
			$('#txt_fabric_desc').val('');
			$('#txt_fabric_desc').attr('disabled','disabled');
			$('#txt_color').val('');
			$('#txt_color').attr('disabled','disabled');
			$("#txt_batch_no").val('');
			$("#txt_batch_id").val('');
			$('#txt_batch_no').attr("onDblClick","openmypage_batchnum();");
			$('#txt_batch_no').attr("placeholder","Write / Browse");
			$('#fabric_desc_id').val('');
			$('#txt_fabric_desc').removeAttr("onDblClick");
			$('#txt_fabric_desc').removeAttr("placeholder");
        }
	 }

	function openmypage_systemid()
	{
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{
			var cbo_company_id = $('#cbo_company_id').val();
			var title = 'System ID Info';
			var page_link = 'requires/finish_fabric_receive_controller.php?cbo_company_id='+cbo_company_id+'&action=systemId_popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe',  page_link, title, 'width=850px,height=390px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var finish_fabric_id=this.contentDoc.getElementById("hidden_sys_id").value;
				
				reset_form('finishFabricEntry_1','list_container_finishing*list_fabric_desc_container','','','','roll_maintained*fabric_store_auto_update');
				get_php_form_data(finish_fabric_id, "populate_data_from_finish_fabric", "requires/finish_fabric_receive_controller" );
				show_list_view(finish_fabric_id,'show_finish_fabric_listview','list_container_finishing','requires/finish_fabric_receive_controller','');
			}
		}
	}

	function openmypage_batchnum()
	{
		var cbo_company_id = $('#cbo_company_id').val();

		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{
			var page_link='requires/finish_fabric_receive_controller.php?cbo_company_id='+cbo_company_id+'&action=batch_number_popup';
			var title='Batch Number Popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=820px,height=390px,center=1,resize=1,scrolling=0','');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var batch_id=this.contentDoc.getElementById("hidden_batch_id").value;
				var job_no=this.contentDoc.getElementById("job_no").value; //Access form field with id="emailfield"
				var booking_no=this.contentDoc.getElementById("booking_no").value; //Access form field with id="emailfield"
				
				if(batch_id!="")
				{
					freeze_window(5);
					set_production_basis();
					
					//Batch Based Means Inhouse. Inhouse Id=1
					$('#cbo_dyeing_source').val(1);
					load_drop_down( 'requires/finish_fabric_receive_controller','1_'+document.getElementById('cbo_company_id').value, 'load_drop_down_dyeing_com','dyeingcom_td');
					
					get_php_form_data(batch_id, "populate_data_from_batch", "requires/finish_fabric_receive_controller" );
					//show_list_view(job_no+"**"+booking_no,'show_fabric_desc_listview','list_fabric_desc_container','requires/finish_fabric_receive_controller','');
					show_list_view(batch_id,'show_fabric_desc_listview','list_fabric_desc_container','requires/finish_fabric_receive_controller','');
					release_freezing();
				}
			}
		}
	}
	
	function openmypage_fabricDescription()
	{
		var title = 'Fabric Description Info';	
		var page_link = 'requires/finish_fabric_receive_controller.php?action=fabricDescription_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=550px,height=370px,center=1,resize=1,scrolling=0','');
		
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theename=this.contentDoc.getElementById("hidden_desc_no").value; //Access form field with id="emailfield"
			var theegsm=this.contentDoc.getElementById("hidden_gsm").value; //Access form field with id="emailfield"
			//var theeDiaWith=this.contentDoc.getElementById("hidden_dia_width").value; //Access form field with id="emailfield"
			var fabric_desc_id=this.contentDoc.getElementById("fabric_desc_id").value; //Access form field with id="emailfield"
			
			$('#txt_fabric_desc').val(theename);
			$('#fabric_desc_id').val(fabric_desc_id);
			$('#txt_gsm').val(theegsm);
			//$('#txt_dia_width').val(theeDiaWith);
		}
	}
	
	function openmypage_po()
	{
		var production_basis=$('#cbo_production_basis').val();
		var cbo_company_id = $('#cbo_company_id').val();
		var txt_batch_no = $('#txt_batch_no').val();
		var txt_batch_id = $('#txt_batch_id').val();
		var dtls_id = $('#update_dtls_id').val();
		var roll_maintained = $('#roll_maintained').val();
		var save_data = $('#save_data').val();
		var all_po_id = $('#all_po_id').val();
		var txt_production_qty = $('#txt_production_qty').val(); 
		var distribution_method = $('#distribution_method_id').val();
		
		if(production_basis==4 && cbo_company_id==0)
		{
			alert("Please Select Company.");
			$('#cbo_company_id').focus();
			return false;
		}
		else if(production_basis==5 && txt_batch_no=="")
		{
			alert("Please Select Batch No.");
			$('#txt_batch_no').focus();
			return false;
		}

		var title = 'PO Info';
		var page_link = 'requires/finish_fabric_receive_controller.php?production_basis='+production_basis+'&cbo_company_id='+cbo_company_id+'&txt_batch_id='+txt_batch_id+'&dtls_id='+dtls_id+'&all_po_id='+all_po_id+'&roll_maintained='+roll_maintained+'&save_data='+save_data+'&txt_production_qty='+txt_production_qty+'&prev_distribution_method='+distribution_method+'&action=po_popup';
		 
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=370px,center=1,resize=1,scrolling=0','');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var save_string=this.contentDoc.getElementById("save_string").value;	 //Access form field with id="emailfield"
			var tot_finish_qnty=this.contentDoc.getElementById("tot_finish_qnty").value; //Access form field with id="emailfield"
			var all_po_id=this.contentDoc.getElementById("all_po_id").value; //Access form field with id="emailfield"
			var buyer_name=this.contentDoc.getElementById("buyer_name").value; //Access form field with id="emailfield"
			var buyer_id=this.contentDoc.getElementById("buyer_id").value; //Access form field with id="emailfield"
			var distribution_method=this.contentDoc.getElementById("distribution_method").value;
			
			$('#save_data').val(save_string);
			$('#txt_production_qty').val(tot_finish_qnty);
			$('#all_po_id').val(all_po_id);
			$('#buyer_name').val(buyer_name);
			$('#buyer_id').val(buyer_id);
			$('#distribution_method_id').val(distribution_method);
			
			if(production_basis==4)
			{
				get_php_form_data(all_po_id, 'load_color', 'requires/finish_fabric_receive_controller');
			}
		}
	}
		
	function fnc_finish_production( operation )
	{
		if(operation==2)
		{
			show_msg('13');
			return;
		}
		
		if( form_validation('cbo_company_id*txt_production_date*cbo_dyeing_source*cbo_dyeing_company*cbo_store_name*txt_batch_no*cbo_body_part*txt_fabric_desc*txt_production_qty*txt_color','Company*Production Date*Dyeing Source*Dyeing Company*Store Name*Body Part*Fabric Description*Production Qnty*Color')==false )
		{
			return;
		}	
		
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_production_basis*cbo_company_id*txt_system_id*txt_production_date*cbo_dyeing_source*cbo_dyeing_company*txt_challan_no*cbo_location*cbo_store_name*txt_batch_no*txt_batch_id*cbo_body_part*txt_fabric_desc*fabric_desc_id*txt_color*txt_gsm*txt_dia_width*txt_production_qty*txt_reject_qty*txt_no_of_roll*buyer_id*cbo_machine_name*txt_rack*txt_shelf*update_id*update_dtls_id*save_data*all_po_id*update_trans_id*previous_prod_id*hidden_receive_qnty*roll_maintained*fabric_store_auto_update*batch_booking_without_order',"../");
		//alert (data);return;
	  freeze_window(operation);
	  http.open("POST","requires/finish_fabric_receive_controller.php",true);
	  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  http.send(data);
	  http.onreadystatechange = fnc_finish_production_reponse;
	}
	
	function fnc_finish_production_reponse()
	{
		if(http.readyState == 4) 
		{
			//release_freezing();alert(http.responseText);return;
			var reponse=trim(http.responseText).split('**');
			
			if(reponse[0]==20)
			{
				alert("Multiple Order Not Allowed.");
				release_freezing();
				return;
			}
			
			show_msg(reponse[0]);
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_system_id').value = reponse[2];
				$('#cbo_company_id').attr('disabled','disabled');
				show_list_view(reponse[1],'show_finish_fabric_listview','list_container_finishing','requires/finish_fabric_receive_controller','');
			
				var cbo_production_basis=$('#cbo_production_basis').val();
				if(cbo_production_basis==4)
				{
					reset_form('finishFabricEntry_1','','','','','update_id*txt_system_id*cbo_production_basis*cbo_company_id*txt_production_date*cbo_dyeing_source*cbo_dyeing_company*txt_challan_no*cbo_location*cbo_store_name*roll_maintained*fabric_store_auto_update');
				}
				else
				{
					reset_form('finishFabricEntry_1','','','','','update_id*txt_system_id*cbo_production_basis*cbo_company_id*txt_production_date*cbo_dyeing_source*cbo_dyeing_company*txt_challan_no*cbo_location*cbo_store_name*roll_maintained*fabric_store_auto_update*txt_batch_no*txt_batch_id*batch_booking_without_order*txt_color');
					document.getElementById('body_part_td').innerHTML='<?php echo create_drop_down( "cbo_body_part", 182, $body_part,"", 1, "-- Select Body Part --", 0, "",0 ); ?>';
				}
				
				set_button_status(0, permission, 'fnc_finish_production',1);
			}
			release_freezing();
		}
	}

	
	function set_form_data(data)
	{
		var data=data.split("**");
		$('#txt_fabric_desc').val(data[0]);
		$('#fabric_desc_id').val(data[1]);
		$('#txt_gsm').val(data[2]);
		$('#txt_dia_width').val(data[3]);
		
		var prod_id=data[4];
		var txt_batch_id = $('#txt_batch_id').val();
		load_drop_down( 'requires/finish_fabric_receive_controller', prod_id+"**"+txt_batch_id, 'load_drop_down_body_part', 'body_part_td' );
		var body_part_length=$("#cbo_body_part option").length;
		if(body_part_length==2)
		{
			$('#cbo_body_part').val($('#cbo_body_part option:last').val());
		}
	}
	
	function put_data_dtls_part(id,type,page_path)
	{
		get_php_form_data(id+"**"+$('#roll_maintained').val(), type, page_path );
	}
	
	function set_auto_complete()
	{
		$("#txt_color").autocomplete({
			 source: str_color
		});
	}
	
	function check_batch(data)
	{
		var production_basis=$('#cbo_production_basis').val();
		var cbo_company_id=$('#cbo_company_id').val();
		if(production_basis==5)
		{
			if (form_validation('cbo_company_id','Company')==false)
			{
				$('#txt_batch_no').val('');
				$('#txt_batch_id').val('');
				return;
			}
			
			var batch_id=return_global_ajax_value( data+"**"+cbo_company_id, 'check_batch_no', '', 'requires/finish_fabric_receive_controller');
			if(batch_id==0)
			{
				alert("Batch No Found");
				reset_form('','list_fabric_desc_container','txt_batch_no*txt_batch_id*txt_batch_qty*batch_booking_without_order*txt_fabric_desc*fabric_desc_id*txt_color*txt_production_qty*buyer_name*buyer_id*txt_dia_width*txt_gsm*all_po_id*save_data*distribution_method_id*txt_batch_qnty*txt_total_received*txt_yet_receive','','','');
				return;
			}
			else
			{
				freeze_window(5);
				get_php_form_data(batch_id, "populate_data_from_batch", "requires/finish_fabric_receive_controller" );
				show_list_view(batch_id,'show_fabric_desc_listview','list_fabric_desc_container','requires/finish_fabric_receive_controller','');
				release_freezing();
			}
		}
	}
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">

<?php echo load_freeze_divs ("../",$permission); ?>
    <form name="finishFabricEntry_1" id="finishFabricEntry_1" autocomplete="off" >
    <div style="width:830px; float:left;" align="center">   
        <fieldset style="width:820px;">
        <legend>Finish Fabric Entry</legend>
        <fieldset>
            <table cellpadding="0" cellspacing="2" width="810" border="0">
                <tr>
                    <td colspan="3" align="right"><strong>System ID</strong><input type="hidden" name="update_id" id="update_id" /></td>
                    <td colspan="3" align="left">
                        <input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:150px;" placeholder="Double click to search" onDblClick="openmypage_systemid();" readonly />
                    </td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td>Production Basis</td>
                    <td>
						<?php
							echo create_drop_down("cbo_production_basis", 150, $receive_basis_arr,"", 0,"", 5,"set_production_basis();","","4,5","","","");
                        ?>
                    </td>
                    <td class="must_entry_caption">Company Name</td>
                    <td>
						<?php
							echo create_drop_down( "cbo_company_id", 160, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", 0, "load_drop_down('requires/finish_fabric_receive_controller', this.value, 'load_drop_down_location', 'location_td' );load_drop_down( 'requires/finish_fabric_receive_controller', this.value, 'load_drop_down_store', 'store_td' );get_php_form_data(this.value,'roll_maintained','requires/finish_fabric_receive_controller' );" );
                        ?>
                    </td>
                    <td class="must_entry_caption">Production Date</td>
                    <td>
                        <input type="date" name="txt_production_date" id="txt_production_date" class="datepicker" style="width:150px;" readonly>
                    </td>
                </tr>
                <tr>
                    <td class="must_entry_caption">Dyeing Source</td>
                    <td>
						<?php
							echo create_drop_down("cbo_dyeing_source", 150, $knitting_source,"", 1,"-- Select Source --", 0,"load_drop_down( 'requires/finish_fabric_receive_controller', this.value+'_'+document.getElementById('cbo_company_id').value, 'load_drop_down_dyeing_com','dyeingcom_td');","","1,3");
                        ?>
                    </td>
                    <td class="must_entry_caption">Dyeing Company</td>
                    <td id="dyeingcom_td">
						<?php
							echo create_drop_down("cbo_dyeing_company", 160, $blank_array,"", 1,"-- Select Dyeing Company --", 0,"");
                        ?>
                    </td>
                    <td>Challan No.</td>
                    <td>
                        <input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:150px;" maxlength="20" title="Maximum 20 Character" />
                    </td>
                </tr>
                <tr>
                    <td>Location</td>
                    <td id="location_td">
						<?php
							echo create_drop_down("cbo_location", 150, $blank_array,"", 1,"-- Select Location --", 0,"");
                        ?>
                    </td>
                    <td class="must_entry_caption"> Store Name </td>
                    <td id="store_td">
						<?php
                            echo create_drop_down( "cbo_store_name", 160, $blank_array,"",1, "--Select store--", 1, "" );
                        ?>
                    </td>
                </tr>
            </table>
        </fieldset>
        <table cellpadding="0" cellspacing="1" width="810" border="0">
            <tr>
                <td width="70%" valign="top">
                    <fieldset>
                    <legend>New Entry</legend>
                        <table cellpadding="0" cellspacing="2" width="100%">
                            <tr>
                                <td width="110" class="must_entry_caption">Batch No.</td>
                                <td>
                                    <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:170px;" placeholder="Write / Browse" onDblClick="openmypage_batchnum();" onChange="check_batch(this.value);" />
                                    <input type="hidden" name="txt_batch_id" id="txt_batch_id" readonly />
                                    <input type="hidden" name="txt_batch_qty" id="txt_batch_qty" readonly />
                                    <input type="hidden" name="batch_booking_without_order" id="batch_booking_without_order"/>
                                </td>
                                <td class="must_entry_caption" width="90">QC Pass Qty</td>
                                <td>
                                    <input type="text" name="txt_production_qty" id="txt_production_qty" class="text_boxes_numeric" placeholder="Single Click to Search" style="width:130px;" onClick="openmypage_po()" readonly />
                                </td>
                            </tr>
                            <tr>
                                <td class="must_entry_caption">Body Part</td>
                                <td id="body_part_td">
                                     <?php
                                     	echo create_drop_down( "cbo_body_part", 182, $body_part,"", 1, "-- Select Body Part --", 0, "",0 );
                                     ?>
                                </td>
                                <td>Reject Qty</td>
                                <td>
                                    <input type="text" name="txt_reject_qty" id="txt_reject_qty" class="text_boxes_numeric" style="width:130px;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="must_entry_caption">Fabric Description</td>
                                <td>
                                    <input type="text" name="txt_fabric_desc" id="txt_fabric_desc" class="text_boxes" style="width:170px;" readonly disabled/>
                                    <input type="hidden" name="fabric_desc_id" id="fabric_desc_id" readonly/>
                                </td>
                                <td class="must_entry_caption">Color</td>
                                <td>
                                    <input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:130px;" disabled />
                                </td>
                            </tr>
                            <tr>
                                <td>GSM</td>
                                <td>
                                    <input type="text" name="txt_gsm" id="txt_gsm" class="text_boxes_numeric" style="width:170px;" maxlength="10"/>
                                </td>
                                <td>No Of Roll</td>
                                <td>
                                    <input type="text" name="txt_no_of_roll" id="txt_no_of_roll" class="text_boxes_numeric" style="width:130px;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Dia/Width</td>
                                <td>
                                    <input type="text" name="txt_dia_width" id="txt_dia_width" class="text_boxes_numeric" style="width:170px;" maxlength="10" />
                                </td>
                                <td>Machine Name</td>
                                <td>
                                    <?php
                                        echo create_drop_down( "cbo_machine_name", 142, "select id,company_id,concat(machine_no,'-',brand) as machine_name from lib_machine_name where category_id=4 and status_active=1 and is_deleted=0 and is_locked=0","id,machine_name", 1, "-- Select Machine --", 0, "","" );
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Rack</td>
                                <td>
                                    <input type="text" name="txt_rack" id="txt_rack" class="text_boxes" style="width:170px">
                                </td>
                                <td>Shelf</td>
                                <td>
                                    <input type="text" name="txt_shelf" id="txt_shelf" class="text_boxes_numeric" style="width:130px">
                                </td>
                            </tr>
                            <tr>
                                <td>Buyer</td>
                                <td>
                                    <input type="text" name="buyer_name" id="buyer_name" class="text_boxes" placeholder="Display" style="width:170px;" disabled="disabled" />
                                    <input type="hidden" name="buyer_id" id="buyer_id" class="text_boxes" disabled="disabled" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td width="2%" valign="top">&nbsp;</td>
                <td width="28%" valign="top">
                    <fieldset>
                    <legend>Display</legend>
                        <table>
                            <tr>
                                <td width="100">Batch Quantity</td>
                                <td>
                                    <input type="text" name="txt_batch_qnty" id="txt_batch_qnty" class="text_boxes" style="width:100px;" disabled />
                                </td>
                            </tr>
                            <tr>
                                <td>Total Received</td>
                                <td>
                                    <input type="text" name="txt_total_received" id="txt_total_received" class="text_boxes" style="width:100px;" disabled />
                                </td>
                            </tr>
                            <tr>
                                <td>Yet to Received</td>
                                <td>
                                    <input type="text" name="txt_yet_receive" id="txt_yet_receive" class="text_boxes" style="width:100px;" disabled />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" class="button_container">
                    <?php
                        echo load_submit_buttons($permission, "fnc_finish_production", 0,0,"reset_form('finishFabricEntry_1','list_container_finishing*list_fabric_desc_container','','cbo_production_basis,5','disable_enable_fields(\'cbo_company_id\');set_production_basis();set_auto_complete();')",1);
                    ?>
                    <input type="hidden" name="update_dtls_id" id="update_dtls_id" readonly>
                    <input type="hidden" name="update_trans_id" id="update_trans_id" readonly>
                    <input type="hidden" name="previous_prod_id" id="previous_prod_id" readonly>
                    <input type="hidden" name="hidden_receive_qnty" id="hidden_receive_qnty" readonly>
                    <input type="hidden" name="all_po_id" id="all_po_id" readonly>
                    <input type="hidden" name="save_data" id="save_data" readonly>
                    <input type="hidden" name="roll_maintained" id="roll_maintained" readonly>
                    <input type="hidden" name="fabric_store_auto_update" id="fabric_store_auto_update" readonly>
                    <input type="hidden" name="distribution_method_id" id="distribution_method_id" readonly />
                </td>
            </tr>
        </table>
        <div style="width:820px;" id="list_container_finishing"></div>
		</fieldset>
    </div>
    <div style="width:10px; overflow:auto; float:left; position:relative;"></div>
    <div id="list_fabric_desc_container" style="width:360px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>
	</form>
</div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>