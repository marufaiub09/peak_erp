﻿<? 
/*-------------------------------------------- Comments
Version                  :  V1
Purpose			         : 	This form will create Sample Fabric Booking
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 	27-12-2012
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 		
Update date		         : 		   
QC Performed BY	         :		
QC Date			         :	
Comments		         : From this version oracle conversion is start
*/

header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$permission=$_SESSION['page_permission'];

//---------------------------------------------------- Start---------------------------------------------------------------------------
$po_number=return_library_array( "select id,po_number from wo_po_break_down", "id", "po_number"  );
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$size_library=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 172, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$cbo_company_name and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
} 
if ($action=="load_drop_down_buyer_order")
{
	echo create_drop_down( "cbo_buyer_name", 172, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$data and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
}

if ($action=="load_drop_down_po_number")
{
	echo create_drop_down( "cbo_order_id",172, $po_number,"", 1, "--Select--", "", "","",$data,"","","","" );
} 

if ($action=="load_drop_down_fabric_description")
{
	$data=explode("_",$data);
	$txt_order_no_id=$data[0];
	$cbo_fabric_natu=$data[1];
	$cbo_fabric_source=$data[2];
	if ($cbo_fabric_natu!=0) $cbo_fabric_natu="and a.fab_nature_id='$cbo_fabric_natu'"; 
	if ($cbo_fabric_source!=0) $cbo_fabric_source_cond="and a.fabric_source='$cbo_fabric_source'"; 
	
	/*$nameArray=sql_select( "
	select
	a.id as pre_cost_fabric_cost_dtls_id,
	a.body_part_id,
	a.color_type_id,
	a.gsm_weight,
	a.construction,
	a.composition,
	b.dia_width
	
	FROM
	 
	wo_pre_cost_fabric_cost_dtls a,
	wo_po_color_size_breakdown c,
	wo_po_details_mas_set_details f,
	wo_pre_cos_fab_co_avg_con_dtls b
	
	LEFT JOIN
	
	wo_booking_dtls d 
	on 
	b.po_break_down_id=d.po_break_down_id and
	b.color_size_table_id=d.color_size_table_id and 
	b.pre_cost_fabric_cost_dtls_id=d.pre_cost_fabric_cost_dtls_id and
	d.status_active=1 and	
	d.is_deleted=0
	
	WHERE
	a.job_no=b.job_no and
	a.job_no=c.job_no_mst and 
	a.job_no=f.job_no and
	a.id=b.pre_cost_fabric_cost_dtls_id and
	c.id=b.color_size_table_id and
	c.item_number_id=f.gmts_item_id and
	c.po_break_down_id in (".$txt_order_no_id.")  $cbo_fabric_natu $cbo_fabric_source_cond
	group by a.id order by a.id"); */
	$nameArray=sql_select( "
	select
	a.id as pre_cost_fabric_cost_dtls_id,
	a.body_part_id,
	a.color_type_id,
	a.gsm_weight,
	a.construction,
	a.composition
	FROM
	wo_pre_cost_fabric_cost_dtls a,
	wo_po_break_down b
	WHERE
	a.job_no=b.job_no_mst and 
	b.id in (".$txt_order_no_id.")  $cbo_fabric_natu $cbo_fabric_source_cond
	group by a.id,a.body_part_id,a.color_type_id,a.gsm_weight,a.construction,a.composition order by a.id"); 
	$fabric_description_array= array();
	foreach ($nameArray as $result)
	{
		if (count($nameArray)>0 )
		{
			$fabric_description_array[$result[csf("pre_cost_fabric_cost_dtls_id")]]=$body_part[$result[csf("body_part_id")]].', '.$color_type[$result[csf("color_type_id")]].', '.$result[csf("construction")].', '.$result[csf("composition")].', '.$result[csf("gsm_weight")];
		}
	}
	echo create_drop_down( "cbo_fabricdescription_id", 420, $fabric_description_array,"", 1, "--Select--", "", "","","","","","","" );
}
if ($action=="load_drop_down_gmts_color")
{
	$data=explode("_",$data);
	$txt_order_no_id=$data[0];
	$cbo_fabric_natu=$data[1];
	$cbo_fabric_source=$data[2];
	$color_library_order=return_library_array( "select b.color_number_id,a.color_name from lib_color a , wo_po_color_size_breakdown b where a.id=b.color_number_id and b.po_break_down_id in (".$txt_order_no_id.") ", "color_number_id", "color_name"  );
	echo create_drop_down( "cbo_garmentscolor_id", 172, $color_library_order,"", 1, "-- Select Color --", $selected, "" );
}


if ($action=="load_drop_down_fabric_color")
{
	$data=explode("_",$data);
	$txt_order_no_id=$data[0];
	$cbo_fabric_natu=$data[1];
	$cbo_fabric_source=$data[2];
	if ($cbo_fabric_natu!=0) $cbo_fabric_natu="and fab_nature_id='$cbo_fabric_natu'"; 
	if ($cbo_fabric_source!=0) $cbo_fabric_source_cond="and fabric_source='$cbo_fabric_source'"; 
	
/*$nameArray=sql_select( "
select
a.id as pre_cost_fabric_cost_dtls_id,
a.job_no,
a.color_size_sensitive,
a.color,
a.color_break_down,
c.color_number_id
FROM
 
wo_pre_cost_fabric_cost_dtls a,
wo_po_color_size_breakdown c,
wo_po_details_mas_set_details f,
wo_pre_cos_fab_co_avg_con_dtls b

LEFT JOIN

wo_booking_dtls d 
on 
b.po_break_down_id=d.po_break_down_id and
b.color_size_table_id=d.color_size_table_id and 
b.pre_cost_fabric_cost_dtls_id=d.pre_cost_fabric_cost_dtls_id and
d.status_active=1 and	
d.is_deleted=0

WHERE
a.job_no=b.job_no and
a.job_no=c.job_no_mst and 
a.job_no=f.job_no and
a.id=b.pre_cost_fabric_cost_dtls_id and
c.id=b.color_size_table_id and
c.item_number_id=f.gmts_item_id and
c.po_break_down_id in (".$txt_order_no_id.")  $cbo_fabric_natu $cbo_fabric_source_cond
order by a.id");*/
$nameArray=sql_select( "
	select
	a.id as pre_cost_fabric_cost_dtls_id,
	a.job_no,
	a.color_size_sensitive,
	a.color,
	a.color_break_down,
	c.color_number_id
	FROM
	wo_pre_cost_fabric_cost_dtls a,
	wo_po_color_size_breakdown c
	WHERE
	a.job_no=c.job_no_mst and 
	c.po_break_down_id in (".$txt_order_no_id.")  $cbo_fabric_natu $cbo_fabric_source_cond
	order by a.id"); 
	$fabric_color_array= array();
	foreach ($nameArray as $result)
	{
		if (count($nameArray)>0 )
		{
			$constrast_color_arr=array();
			if($result[csf("color_size_sensitive")]==3)
			{
				$constrast_color=explode('__',$result[csf("color_break_down")]);
				for($i=0;$i<count($constrast_color);$i++)
				{
					$constrast_color2=explode('_',$constrast_color[$i]);
					$constrast_color_arr[$constrast_color2[0]]=$constrast_color2[2];
				}
			}
			$color_id="";
			if($result[csf("color_size_sensitive")]==3)
			{
				$color_id=return_field_value("contrast_color_id","wo_pre_cos_fab_co_color_dtls","job_no='".$result[csf('job_no')]."' and pre_cost_fabric_cost_dtls_id=".$result[csf('pre_cost_fabric_cost_dtls_id')]." and gmts_color_id=".$result[csf('color_number_id')]."");
				$fabric_color_array[$color_id]=$constrast_color_arr[$result[csf("color_number_id")]];
			}
			else if($result[csf("color_size_sensitive")]==0)
			{
				$fabric_color_array[$result[csf("color")]]=$color_library[$result[csf("color")]];
			}
			else
			{
				$fabric_color_array[$result[csf("color_number_id")]]=$color_library[$result[csf("color_number_id")]];
			}
		}
	}
	echo create_drop_down( "cbo_fabriccolor_id",172, $fabric_color_array,"", 0, "", "", "","","","","","","" );
}
 
if ($action=="load_drop_down_gmts_size")
{
	$data=explode("_",$data);
	$txt_order_no_id=$data[0];
	$cbo_fabric_natu=$data[1];
	$cbo_fabric_source=$data[2];
	$size_library_order=return_library_array( "select b.size_number_id,a.size_name from lib_size a, wo_po_color_size_breakdown b where a.id=b.size_number_id and b.po_break_down_id in (".$txt_order_no_id.") ", "size_number_id", "size_name");
	
	echo create_drop_down( "cbo_garmentssize_id", 172, $size_library_order,"", 1, "-- Select Size --", $selected, "" );
}

 if ($action=="load_drop_down_item_size")
{
	$data=explode("_",$data);
	$txt_order_no_id=$data[0];
	$cbo_fabric_natu=$data[1];
	$cbo_fabric_source=$data[2];
	if ($cbo_fabric_natu!=0) $cbo_fabric_natu="and fab_nature_id='$cbo_fabric_natu'"; 
	if ($cbo_fabric_source!=0) $cbo_fabric_source_cond="and fabric_source='$cbo_fabric_source'"; 
	
	/*$nameArray=sql_select( "
	select
	b.item_size
	FROM
	 
	wo_pre_cost_fabric_cost_dtls a,
	wo_po_color_size_breakdown c,
	wo_po_details_mas_set_details f,
	wo_pre_cos_fab_co_avg_con_dtls b
	
	LEFT JOIN
	
	wo_booking_dtls d 
	on 
	b.po_break_down_id=d.po_break_down_id and
	b.color_size_table_id=d.color_size_table_id and 
	b.pre_cost_fabric_cost_dtls_id=d.pre_cost_fabric_cost_dtls_id and
	d.status_active=1 and	
	d.is_deleted=0
	
	WHERE
	a.job_no=b.job_no and
	a.job_no=c.job_no_mst and 
	a.job_no=f.job_no and
	a.id=b.pre_cost_fabric_cost_dtls_id and
	c.id=b.color_size_table_id and
	c.item_number_id=f.gmts_item_id and
	c.po_break_down_id in (".$txt_order_no_id.")  $cbo_fabric_natu $cbo_fabric_source_cond
	order by a.id"); */
	$nameArray=sql_select( "
	select
	b.item_size
	FROM
	wo_pre_cost_fabric_cost_dtls a,
	wo_pre_cos_fab_co_avg_con_dtls b
	WHERE
	a.job_no=b.job_no and
	a.id=b.pre_cost_fabric_cost_dtls_id and
	b.po_break_down_id in (".$txt_order_no_id.")  $cbo_fabric_natu $cbo_fabric_source_cond
	order by b.item_size"); 
	$item_size_array= array();
	foreach ($nameArray as $result)
	{
		if (count($nameArray)>0 )
		{
		    $item_size_array[$result[csf("item_size")]]=$result[csf("item_size")];
		}
	}
	
	echo create_drop_down( "cbo_itemsize_id",172, $item_size_array,"", 0, "", "", "","","","","","","" );
}

if($action=="process_loss_method_id")
{
	$data=explode("_",$data);
	$process_loss_method=return_field_value("process_loss_method", "variable_order_tracking", "company_name=$data[0]  and variable_list=18 and item_category_id=$data[1] and status_active=1 and is_deleted=0");
	echo $process_loss_method;
	
}

if($action=="show_fabric_booking")
{
	extract($_REQUEST);
	$arr=array (0=>$po_number,1=>$body_part,2=>$color_type,6=>$color_library);
 	//$sql= "select is_confirmed,po_number,po_received_date,pub_shipment_date,shipment_date,po_quantity,unit_price,po_total_price,excess_cut,plan_cut,DATEDIFF(shipment_date,po_received_date) date_diff,status_active,id from  wo_po_break_down  where   status_active=1 and is_deleted=0 and job_no_mst='$data'"; 
	$txt_booking_no=str_replace("'","",$txt_booking_no);
	  $sql= "select a.po_break_down_id,b.body_part_id,b.color_type_id,b.construction,b.composition,b.gsm_weight,a.fabric_color_id,a.item_size,a.dia_width,a.fin_fab_qnty,a.process_loss_percent,a.grey_fab_qnty,a.rate,a.amount,a.id,a.pre_cost_fabric_cost_dtls_id FROM wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b WHERE a.pre_cost_fabric_cost_dtls_id=b.id and  a.booking_no ='".$data."' and a.is_short=2 and a.status_active=1 and	a.is_deleted=0"; 
	 
	echo  create_list_view("list_view", "PO Number,Body Part,Color Type,Construction,Composition,GSM,Fab.Color,Item Size,Dia/ Width,Fin Fab Qnty,Process Loss,Gray Qnty,Rate,Amount", "100,130,100,100,150,50,80,100,50,60,60,60,60","1280","220",0, $sql , "get_php_form_data", "id", "'populate_details_data_from_for_update'", 1, "po_break_down_id,body_part_id,color_type_id,0,0,0,fabric_color_id,0,0,0,0,0,0,0", $arr , "po_break_down_id,body_part_id,color_type_id,construction,composition,gsm_weight,fabric_color_id,item_size,dia_width,fin_fab_qnty,process_loss_percent,grey_fab_qnty,rate,amount", "requires/sample_booking_controller",'','0,0,0,0,0,0,0,0,0,2,2,2,2,2') ;
}
/*if ($action=="show_fabric_booking1")
{
	extract($_REQUEST);
	?>
    <table width="1280" class="rpt_table" border="0" rules="all">
        	<thead>
        	<tr>
                <th width="50">Sl</th>
            	<th width="120">PO Number</th>
                <th width="120">Body Part</th>
                <th width="120">Color Type</th>   
                <th width="120">Construction</th>
                <th width="120">Composition</th>
                <th width="50">GSM</th>
                <th width="100">Dia/Width</th>  
                <th width="100">Fab.Color</th>
                <th width="50">Item Size</th>
                <th width="150">Fin Fab Qnty</th>
                <th width="120">Process Loss</th>
                <th width="100">Gray Qnty</th>
                <th width="100">Rate</th> 
                <th width="100">Amount</th>  
                <th></th>          
           </tr>
       </thead>
       
    <?
	$txt_booking_no=str_replace("'","",$txt_booking_no);
	$cbo_fabric_source=str_replace("'","",$cbo_fabric_source);
	$tot_finish_fab_qnty=0;
    $tot_grey_fab_qnty=0;
	if($type==1)
	{
	$nameArray=sql_select("select id,pre_cost_fabric_cost_dtls_id,po_break_down_id,fabric_color_id,item_size,dia_width,fin_fab_qnty,process_loss_percent,grey_fab_qnty,rate,amount FROM wo_booking_dtls WHERE booking_no ='".$txt_booking_no."' and is_short=1 and status_active=1 and	is_deleted=0"); 
	}
	$count=0;
	foreach ($nameArray as $result)
	{
		if (count($nameArray)>0 )
		{
			 if ($count%2==0)  
                	$bgcolor="#E9F3FF";
                else
                	$bgcolor="#FFFFFF";	
					
						 $count++;
						 $fabric_description=sql_select("select body_part_id,fab_nature_id,color_type_id,construction,composition,gsm_weight FROM  wo_pre_cost_fabric_cost_dtls WHERE id ='".$result[csf("pre_cost_fabric_cost_dtls_id")]."'  and status_active=1 and	is_deleted=0"); 
						 list($fabric_description_row)=$fabric_description
						 
			?>
                	<tr bgcolor="<? echo $bgcolor; ?>" onClick="change_color('tr_<? echo $count; ?>','<? echo $bgcolor;?>');get_php_form_data( <? echo $result[csf("id")]  ?>, 'populate_details_data_from_for_update', 'requires/sample_booking_controller' )" id="tr_<? echo $count; ?>">
                        <td width="50"> 
						<? echo $count; ?>
                        </td>
                        <td width="120" align="center">
						<? echo $po_number[$result[csf("po_break_down_id")]];?> 
                        </td>
                        <td width="120" align="center">
						<? echo $body_part[$fabric_description_row[csf("body_part_id")]];?>
                        </td>
                        <td width="120" align="center">
						<? echo $color_type[$fabric_description_row[csf("color_type_id")]];?> 
                        </td>   
                        <td width="120" align="center">
						<? echo $fabric_description_row[csf("construction")]; ?> 
                        </td>
                        <td width="120" align="center">
						<? echo $fabric_description_row[csf("composition")]; ?>
                        </td>
                        
                        <td width="50" align="center"><?  echo $fabric_description_row[csf("gsm_weight")]; ?> </td>
                        <td width="100" align="center"><?  echo $result[csf("dia_width")]; ?></td> 
                        
                         <td width="100" align="center"><?  echo $color_library[$result[csf("fabric_color_id")]]; ?></td> 
                        <td width="50" align="center"><?  echo $result[csf("item_size")];?></td> 
                        <td width="150" align="right"><? echo number_format( $result[csf("fin_fab_qnty")],2); ?></td>  
                        <td width="120" align="right">
						<?
						echo number_format($result[csf("process_loss_percent")],2);
						?>
                        </td>
                        <td width="100" align="right">
						<? 
						echo number_format($result[csf("grey_fab_qnty")],2);
						?>
                        </td>
                        <td width="100" align="right"><? echo number_format($result[csf("rate")],2); ?></td>
                        <td align="right" width="100" align="right"> <? echo number_format($result[csf("amount")],2); ?> </td>
                        <td align="right"></td> 
                    </tr>
                <?
		} // if count namearray end
	} // for each name arra
	?>
	</tbody>
    </table>
    
    
   
<?
}*/

if($action=="check_is_booking_used")
{
	$work_order_no=return_field_value("work_order_no","com_pi_item_details","work_order_no='$data' and status_active =1 and is_deleted=0");
	echo $work_order_no;
	die;
}

if($action=="delete_booking_item")
{
   execute_query( "update wo_booking_dtls set status_active=0,is_deleted =1 where  booking_no ='$data'",0);	
}

if($action=="show_fabric_booking_report")
{
	extract($_REQUEST);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$imge_arr=return_library_array( "select master_tble_id,image_location from   common_photo_library",'master_tble_id','image_location');
   $marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$country_arr=return_library_array( "select id,country_name from   lib_country",'id','country_name');
	$supplier_name_arr=return_library_array( "select id,supplier_name from   lib_supplier",'id','supplier_name');
	$supplier_address_arr=return_library_array( "select id,address_1 from   lib_supplier",'id','address_1');
	$buyer_name_arr=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
	$po_qnty_tot=return_field_value( "sum(plan_cut)", "wo_po_break_down","id in(".str_replace("'","",$txt_order_no_id).")");
	?>
	<div style="width:1330px" align="center">       
    										<!--    Header Company Information         --> 
       <table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid black">
           <tr>
               <td width="100"> 
               <img  src='../../<? echo $imge_arr[$cbo_company_name]; ?>' height='100%' width='100%' />
               </td>
               <td width="1250">                                     
                    <table width="100%" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td align="center" style="font-size:20px;">
                              <?php      
                                    echo $company_library[$cbo_company_name];
                              ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:14px">  
                            <?
                            $nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$cbo_company_name"); 
                            foreach ($nameArray as $result)
                            { 
                            ?>
                                            Plot No: <? echo $result[csf('plot_no')]; ?> 
                                            Level No: <? echo $result[csf('level_no')]?>
                                            Road No: <? echo $result[csf('road_no')]; ?> 
                                            Block No: <? echo $result[csf('block_no')];?> 
                                            City No: <? echo $result[csf('city')];?> 
                                            Zip Code: <? echo $result[csf('zip_code')]; ?> 
                                            Province No: <?php echo $result[csf('province')];?> 
                                            Country: <? echo $country_arr[$result[csf('country_id')]]; ?><br> 
                                            Email Address: <? echo $result[csf('email')];?> 
                                            Website No: <? echo $result[csf('website')];
                            }
                                            ?>   
                                         
                               </td> 
                            </tr>
                            <tr>
                            <td align="center" style="font-size:20px">  
                            <strong><? echo $report_title;?> &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font style="color:#F00"><? if(str_replace("'","",$id_approved_id) ==1){ echo "(Approved)";}else{echo "";}; ?> </font></strong>
                             </td> 
                            </tr>
                      </table>
                </td>       
            </tr>
       </table>
       
                <?
				$job_no='';
				$total_set_qnty=0;
				$colar_excess_percent=0;
				$cuff_excess_percent=0;
                $nameArray=sql_select( "select a.booking_no,a.booking_date,a.supplier_id,a.currency_id,a.exchange_rate,a.attention,a.delivery_date,a.po_break_down_id,a.colar_excess_percent,a.cuff_excess_percent,a.delivery_date,a.is_apply_last_update,a.fabric_source,b.job_no,b.buyer_name, b.style_ref_no ,b.gmts_item_id,b.order_uom,b.total_set_qnty,b.style_description,b.season,b.product_dept,b.product_code,b.pro_sub_dep,b.dealing_marchant from wo_booking_mst a, wo_po_details_master b where  a.job_no=b.job_no and a.booking_no=$txt_booking_no"); 
				foreach ($nameArray as $result)
				{
					$total_set_qnty=$result[csf('total_set_qnty')];
					$colar_excess_percent=$result[csf('colar_excess_percent')];
				    $cuff_excess_percent=$result[csf('cuff_excess_percent')];
					$po_no="";
					$shipment_date="";
					$sql_po= "select po_number,MIN(pub_shipment_date) pub_shipment_date from  wo_po_break_down  where id in(".$result[csf('po_break_down_id')].") group by po_number"; 
					$data_array_po=sql_select($sql_po);
					foreach ($data_array_po as $row_po)
					{
						$po_no.=$row_po[csf('po_number')].", ";
						$shipment_date.=change_date_format($row_po[csf('pub_shipment_date')],'dd-mm-yyyy','-').", ";
					}
					
					$lead_time="";
					if($db_type==0)
					{
					$sql_lead_time= "select DATEDIFF(pub_shipment_date,po_received_date) date_diff from  wo_po_break_down  where id in(".$result[csf('po_break_down_id')].")"; 
					}
					
					if($db_type==2)
					{
					$sql_lead_time= "select (pub_shipment_date-po_received_date) date_diff from  wo_po_break_down  where id in(".$result[csf('po_break_down_id')].")"; 
					}
					$data_array_lead_time=sql_select($sql_lead_time);
					foreach ($data_array_lead_time as $row_lead_time)
					{
						$lead_time.=$row_lead_time[csf('date_diff')].",";
						//$shipment_date.=change_date_format($row_po['pub_shipment_date'],'dd-mm-yyyy','-').",";
					}
					
					$po_received_date="";
					$sql_po_received_date= "select MIN(po_received_date) as po_received_date from  wo_po_break_down  where id in(".$result[csf('po_break_down_id')].")"; 
					$data_array_po_received_date=sql_select($sql_po_received_date);
					foreach ($data_array_po_received_date as $row_po_received_date)
					{
						$po_received_date=change_date_format($row_po_received_date[csf('po_received_date')],'dd-mm-yyyy','-');
						//$shipment_date.=change_date_format($row_po['pub_shipment_date'],'dd-mm-yyyy','-').",";
					}
				?>
       <table width="100%" style="border:1px solid black" >                    	
            <tr>
                <td colspan="6" valign="top" style="font-size:18px; color:#F00"><? if($result[csf('is_apply_last_update')]==2){echo "Booking Info not synchronized with order entry and pre-costing. order entry or pre-costing has updated after booking entry.  Contact to ".$marchentrArr[$result[csf('dealing_marchant')]]; } else{ echo "";} ?></td>                             
            </tr>                                                
            <tr>
                <td width="100"><span style="font-size:18px"><b>Buyer/Agent Name</b></span></td>
                <td width="110">:&nbsp;<span style="font-size:18px"><b><? echo $buyer_name_arr[$result[csf('buyer_name')]]; ?></b></span></td>
                <td width="100"><span style="font-size:12px"><b>Dept.</b></span></td>
                <td width="110">:&nbsp;<? echo $product_dept[$result[csf('product_dept')]] ; if($result[csf('product_code')] !=""){ echo " (".$result[csf('product_code')].")";} if($result[csf('pro_sub_dep')] !=0){ echo " (".$pro_sub_dept_array[$result[csf('pro_sub_dep')]].")";}?></td>	
                <td width="100"><span style="font-size:12px"><b>Order Qnty</b></span></td>
                <td width="110">:&nbsp;
				<?  echo $po_qnty_tot1." ".$unit_of_measurement[$result[csf('order_uom')]] ; ?>
                </td>
            </tr>
            <tr>
                
                <td width="100" style="font-size:12px"><b>Garments Item</b></td>
                <td width="110">:&nbsp;
				<? 
				$gmts_item_name="";
				$gmts_item=explode(',',$result[csf('gmts_item_id')]);
				for($g=0;$g<=count($gmts_item); $g++)
				{
					$gmts_item_name.= $garments_item[$gmts_item[$g]].",";
				}
				echo rtrim($gmts_item_name,',');
				?>
                </td>
                <td width="100" style="font-size:12px"><b>Booking Date</b></td>
                <td width="110">:&nbsp;<? echo change_date_format($result[csf('booking_date')],'dd-mm-yyyy','-');?>&nbsp;&nbsp;&nbsp;</td>
                <td width="100" style="font-size:18px"><b>Style Ref.</b>   </td>
                <td width="110" style="font-size:18px">:&nbsp;<b><? echo $result[csf('style_ref_no')];?> </b>   </td>
                
            </tr>
             <tr>
                
                	
                
                <td  width="100" style="font-size:12px"><b>Style Des.</b></td>
                <td  width="110" >:&nbsp;<? echo $result[csf('style_description')]; $job_no= $result[csf('job_no')];?></td>
                <td width="100" style="font-size:12px"><b>Lead Time </b>   </td>
                <td width="110">:&nbsp;<?  echo rtrim($lead_time,",");;?> </td>
                <td width="100" style="font-size:12px"><b>Dealing Merchant</b></td>
                <td width="110">:&nbsp;<? echo $marchentrArr[$result[csf('dealing_marchant')]]; ?></td>
                
                
                
            </tr>
             
            <tr>
                <td width="100" style="font-size:12px"><b>Supplier Name</b>   </td>
                <td width="110">:&nbsp;<? echo $supplier_name_arr[$result[csf('supplier_id')]];?>    </td>
                <td width="100" style="font-size:12px"><b>Delivery Date</b></td>
               	<td width="110">:&nbsp;<? echo change_date_format( $result[csf('delivery_date')],'dd-mm-yyyy','-');?></td> 
                <td width="100" style="font-size:18px"><b>Booking No </b>   </td>
                <td width="110" style="font-size:18px">:&nbsp;<b><? echo $result[csf('booking_no')];?></b><? echo "(".$fabric_source[$result[csf('fabric_source')]].")"?></td>
                
                
                
            </tr> 
            <tr>
                <td width="100" style="font-size:12px"><b>Season</b></td>
                <td width="110">:&nbsp;<? echo $result[csf('season')]; ?></td>
                <td  width="100" style="font-size:12px"><b>Attention</b></td>
                <td  width="110" >:&nbsp;<? echo $result[csf('attention')]; ?></td>
                <td  width="100" style="font-size:12px"><b>Po Received Date</b></td>
                <td  width="110" >:&nbsp;<? echo $po_received_date; ?></td>
                
                
                
            </tr>  
           <tr>
               <td width="100" style="font-size:18px"><b>Order No</b></td>
                <td width="110" style="font-size:18px" colspan="5">:&nbsp;<b><? echo rtrim($po_no,", "); ?></b></td>
                
            </tr> 
            <tr>
               <td width="100" style="font-size:12px"><b>Shipment Date</b></td>
                <td width="110" colspan="5"> :&nbsp;<? echo rtrim($shipment_date,", "); ?></td>
                
            </tr> 
            
        </table> 
        <?
			}
		?>
            
      <br/>   									 <!--  Here will be the main portion  -->
     <style>
	 .main_table tr th{
		 border:1px solid black;
		 font-size:13px;
		 outline: 0;
	 }
	  .main_table tr td{
		 border:1px solid black;
		 font-size:13px;
		 outline: 0;
	 }
	 </style>
     <?
	 $costing_per="";
	 $costing_per_qnty=0;
	 $costing_per_id=return_field_value( "costing_per", "wo_pre_cost_mst","job_no ='$job_no'");
	 if($costing_per_id==1)
			{
				$costing_per="1 Dzn";
				$costing_per_qnty=12;
				
			}
			if($costing_per_id==2)
			{
				$costing_per="1 Pcs";
				$costing_per_qnty=1;
				
			}
			if($costing_per_id==3)
			{
				$costing_per="2 Dzn";
				$costing_per_qnty=24;
				
			}
			if($costing_per_id==4)
			{
				$costing_per="3 Dzn";
				$costing_per_qnty=36;
				
			}
			if($costing_per_id==5)
			{
				$costing_per="4 Dzn";
				$costing_per_qnty=48;
				
			}
			//$process_loss_method=return_field_value( "process_loss_method", "wo_pre_cost_fabric_cost_dtls","job_no='$job_no'");
			
			$process_loss_method=return_field_value("process_loss_method", "variable_order_tracking", "company_name=$cbo_company_name  and variable_list=18 and item_category_id=$cbo_fabric_natu and status_active=1 and is_deleted=0");

	 ?>
     <? 
	$nameArray_fabric_description= sql_select("SELECT a.body_part_id,a.color_type_id,a.construction,a.composition,a.gsm_weight,b.dia_width,b.process_loss_percent FROM wo_pre_cost_fabric_cost_dtls a,wo_booking_dtls b where b.booking_no =$txt_booking_no and a.id=b.pre_cost_fabric_cost_dtls_id  and b.status_active=1 and	
b.is_deleted=0  group by a.body_part_id,a.color_type_id,a.construction,a.composition,a.gsm_weight,b.dia_width,b.process_loss_percent order by a.body_part_id");
	 ?>
     <table class="rpt_table" width="100%"  border="1" cellpadding="0" cellspacing="0" rules="all" >
     <tr align="center"><th colspan="2" align="left">Body Part</th>
        <? 
		foreach($nameArray_fabric_description  as $result_fabric_description)
		{
			if( $result_fabric_description[csf('body_part_id')] == "")  echo "<td  colspan='2'>&nbsp</td>";	
			else         		               echo "<td  colspan='2'>". $body_part[$result_fabric_description[csf('body_part_id')]]."</td>";			
		}
		?>
        <td  rowspan="8" width="50"><p>Total  Finish Fabric (KG)</p></td> <td  rowspan="8" width="50"><p>Total Grey Fabric (KG)</p></td>
             <td  rowspan="8" width="50"><p>Process Loss %</p></td>
       </tr> 
     <tr align="center"><th colspan="2" align="left">Color Type</th>
        <? 
		foreach($nameArray_fabric_description  as $result_fabric_description)
		{
			if( $result_fabric_description[csf('color_type_id')] == "")  echo "<td  colspan='2'>&nbsp</td>";	
			else         		               echo "<td  colspan='2'>". $color_type[$result_fabric_description[csf('color_type_id')]]."</td>";			
		}
		?>
        
       </tr>  
        <tr align="center"><th colspan="2" align="left">Construction</th>
        <? 
		foreach($nameArray_fabric_description  as $result_fabric_description)
		{
			if( $result_fabric_description[csf('construction')] == "")  echo "<td  colspan='2'>&nbsp</td>";	
			else         		               echo "<td  colspan='2'>". $result_fabric_description[csf('construction')]."</td>";			
		}
		?>
        	
           
       </tr>       
        <tr align="center"><th   colspan="2" align="left">Composition</th>
        <? 
		foreach($nameArray_fabric_description as $result_fabric_description)
		{
			if( $result_fabric_description[csf('composition')] == "")   echo "<td colspan='2' >&nbsp</td>";
			else         		               echo "<td colspan='2' >".$result_fabric_description[csf('composition')]."</td>";			
		}
		?>
       
       </tr>
       <tr align="center"><th  colspan="2" align="left">GSM</th>
        <? 
		foreach($nameArray_fabric_description  as $result_fabric_description)
		{
			if( $result_fabric_description[csf('gsm_weight')] == "")   echo "<td colspan='2'>&nbsp</td>";
			else         		       echo "<td colspan='2' align='center'>". $result_fabric_description[csf('gsm_weight')]."</td>";			
		}
		?>
       
       </tr>
       <tr align="center"><th   colspan="2" align="left">Dia/Width</th>
        <? 
		foreach($nameArray_fabric_description as $result_fabric_description)
		{
			if( $result_fabric_description[csf('dia_width')] == "")   echo "<td colspan='2'>&nbsp</td>";
			else         		              echo "<td colspan='2' align='center'>". $result_fabric_description[csf('dia_width')]."</td>";			
		}
		?>
        
       </tr>
       <tr align="center"><th  colspan="2" align="left">Process Loss%</th>
        <? 
		foreach($nameArray_fabric_description as $result_fabric_description)
		{
			if( $result_fabric_description[csf('process_loss_percent')] == "")   echo "<td colspan='2'>&nbsp</td>";
			else         		                      echo "<td align='center' colspan='2'>". $result_fabric_description[csf('process_loss_percent')]."</td>";			
		}
		?>
        
       </tr>
       <tr>
            <!--<th  width="120" align="left">Gmts. Color</th>-->
            <th  width="120" align="left">Fabric Color</th>
            <th  width="120" align="left">Lapdip No</th>
        <? 
		foreach($nameArray_fabric_description as $result_fabric_description)
		{
			  echo "<th width='50'>Finish</th><th width='50' >Gray</th>";			
		}
		?>
       
       </tr>
       <?
	        
	        $grand_total_fin_fab_qnty=0;
			$grand_total_grey_fab_qnty=0;
			$grand_totalcons_per_finish=0;
			$grand_totalcons_per_grey=0;
			$color_wise_wo_sql=sql_select("select fabric_color_id 
										  FROM 
										  wo_booking_dtls
										  WHERE 
										  booking_no =$txt_booking_no and
										  status_active=1 and
                                          is_deleted=0
										  group by fabric_color_id");
		foreach($color_wise_wo_sql as $color_wise_wo_result)
	    {
		?> 
			<tr>
           <!-- <td  width="120" align="left">
			<?
			
			//echo $color_library[$color_wise_wo_result['fabric_color_id']]; 
			
			?></td>-->
            <td  width="120" align="left">
			<?
			echo $color_library[$color_wise_wo_result[csf('fabric_color_id')]];

			?>
            </td>
            <td  width="120" align="left">
			<? 
			$lapdip_no="";
			$lapdip_no=return_field_value("lapdip_no","wo_po_lapdip_approval_info","job_no_mst='".$color_wise_wo_result[csf('job_no')]."' and approval_status=3 and color_name_id=".$color_wise_wo_result[csf('fabric_color_id')]."");
			if($lapdip_no=="") echo "&nbsp;"; echo $lapdip_no; 
			?>
            </td>
            <?
			$total_fin_fab_qnty=0;
			$total_grey_fab_qnty=0;
			
			foreach($nameArray_fabric_description as $result_fabric_description)
		    {
				
				
												  
				if($db_type==0)
				{
				$color_wise_wo_sql_qnty=sql_select("select sum(b.fin_fab_qnty) as fin_fab_qnty,sum(b.grey_fab_qnty) as grey_fab_qnty
												  FROM 
												  wo_pre_cost_fabric_cost_dtls a,
												  wo_booking_dtls b 
												  WHERE 
												  b.booking_no =$txt_booking_no  and
												  a.id=b.pre_cost_fabric_cost_dtls_id and
												  a.body_part_id='".$result_fabric_description[csf('body_part_id')]."' and
												  a.color_type_id='".$result_fabric_description[csf('color_type_id')]."' and 
												  a.construction='".$result_fabric_description[csf('construction')]."' and 
												  a.composition='".$result_fabric_description[csf('composition')]."' and 
												  a.gsm_weight='".$result_fabric_description[csf('gsm_weight')]."' and 
												  b.dia_width='".$result_fabric_description[csf('dia_width')]."' and 
												  b.process_loss_percent='".$result_fabric_description[csf('process_loss_percent')]."' and 
												  b.fabric_color_id=".$color_wise_wo_result[csf('fabric_color_id')]." and
												  b.status_active=1 and
												  b.is_deleted=0");
				}
				
				if($db_type==2)
				{
				$color_wise_wo_sql_qnty=sql_select("select sum(b.fin_fab_qnty) as fin_fab_qnty,sum(b.grey_fab_qnty) as grey_fab_qnty
												  FROM 
												  wo_pre_cost_fabric_cost_dtls a,
												  wo_booking_dtls b 
												  WHERE 
												  b.booking_no =$txt_booking_no  and
												  a.id=b.pre_cost_fabric_cost_dtls_id and
												  nvl(a.body_part_id,0)=nvl('".$result_fabric_description[csf('body_part_id')]."',0) and
												  nvl(a.color_type_id,0)=nvl('".$result_fabric_description[csf('color_type_id')]."',0) and 
												  nvl(a.construction,0)=nvl('".$result_fabric_description[csf('construction')]."',0) and 
												  nvl(a.composition,0)=nvl('".$result_fabric_description[csf('composition')]."',0) and 
												  nvl(a.gsm_weight,0)=nvl('".$result_fabric_description[csf('gsm_weight')]."',0) and 
												  nvl(b.dia_width,0)=nvl('".$result_fabric_description[csf('dia_width')]."',0) and 
												  nvl(b.process_loss_percent,0)=nvl('".$result_fabric_description[csf('process_loss_percent')]."',0) and 
												  nvl(b.fabric_color_id,0)=nvl(".$color_wise_wo_result[csf('fabric_color_id')].",0) and
												  b.status_active=1 and
												  b.is_deleted=0");
				}
												 
				
				list($color_wise_wo_result_qnty)=$color_wise_wo_sql_qnty

			?>
			<td width='50' align='right'>
			<? 
			if($color_wise_wo_result_qnty[csf('fin_fab_qnty')]!="")
			{
			echo number_format($color_wise_wo_result_qnty[csf('fin_fab_qnty')],2) ;
			$total_fin_fab_qnty+=$color_wise_wo_result_qnty[csf('fin_fab_qnty')];
			}
			?>
            </td>
            <td width='50' align='right' > 
			<? 
			if($color_wise_wo_result_qnty[csf('grey_fab_qnty')]!="")
			{
			echo number_format($color_wise_wo_result_qnty[csf('grey_fab_qnty')],2); 
			$total_grey_fab_qnty+=$color_wise_wo_result_qnty[csf('grey_fab_qnty')];
			}
			?>
            </td>
            <?
			}
			?>
            <td align="right"><? echo number_format($total_fin_fab_qnty,2); $grand_total_fin_fab_qnty+=$total_fin_fab_qnty;?></td>
            <td align="right"><? echo number_format($total_grey_fab_qnty,2); $grand_total_grey_fab_qnty+=$total_grey_fab_qnty;?></td>
            
            <td align="right">
            <?
			
			if($process_loss_method==1)
			{
				$process_percent=(($total_grey_fab_qnty-$total_fin_fab_qnty)/$total_fin_fab_qnty)*100;
			}
			
			if($process_loss_method==2)
			{
				//$devided_val = 1-(($total_grey_fab_qnty-$total_fin_fab_qnty)/100);
				//$process_percent=$total_grey_fab_qnty/$devided_val;
				$process_percent=(($total_grey_fab_qnty-$total_fin_fab_qnty)/$total_grey_fab_qnty)*100;
			}
			echo number_format($process_percent,2);
			
			?>
            </td>
            </tr>
         <?
		}
		?>
        <tr>
        <!--<td  width="120" align="left">&nbsp;</td>-->
        <td  width="120" align="left">&nbsp;</td>
        <td  width="120" align="left"><strong>Total</strong></td>
        <?
			foreach($nameArray_fabric_description as $result_fabric_description)
		    {
				if($db_type==0)
				{
				$color_wise_wo_sql_qnty=sql_select("select sum(b.fin_fab_qnty) as fin_fab_qnty,sum(b.grey_fab_qnty) as grey_fab_qnty
												  FROM 
												  wo_pre_cost_fabric_cost_dtls a,
												  wo_booking_dtls b 
												  WHERE 
												  b.booking_no =$txt_booking_no  and
												  a.id=b.pre_cost_fabric_cost_dtls_id and
												  a.body_part_id='".$result_fabric_description[csf('body_part_id')]."' and
												  a.color_type_id='".$result_fabric_description[csf('color_type_id')]."' and 
												  a.construction='".$result_fabric_description[csf('construction')]."' and 
												  a.composition='".$result_fabric_description[csf('composition')]."' and 
												  a.gsm_weight='".$result_fabric_description[csf('gsm_weight')]."' and 
												  b.dia_width='".$result_fabric_description[csf('dia_width')]."' and 
												  b.process_loss_percent='".$result_fabric_description[csf('process_loss_percent')]."' and
												  b.status_active=1 and
												  b.is_deleted=0
												  ");
				}
				if($db_type==2)
				{
				$color_wise_wo_sql_qnty=sql_select("select sum(b.fin_fab_qnty) as fin_fab_qnty,sum(b.grey_fab_qnty) as grey_fab_qnty
												  FROM 
												  wo_pre_cost_fabric_cost_dtls a,
												  wo_booking_dtls b 
												  WHERE 
												  b.booking_no =$txt_booking_no  and
												  a.id=b.pre_cost_fabric_cost_dtls_id and
												  nvl(a.body_part_id,0)=nvl('".$result_fabric_description[csf('body_part_id')]."',0) and
												  nvl(a.color_type_id,0)=nvl('".$result_fabric_description[csf('color_type_id')]."',0) and 
												  nvl(a.construction,0)=nvl('".$result_fabric_description[csf('construction')]."',0) and 
												  nvl(a.composition,0)=nvl('".$result_fabric_description[csf('composition')]."',0) and 
												  nvl(a.gsm_weight,0)=nvl('".$result_fabric_description[csf('gsm_weight')]."',0) and 
												  nvl(b.dia_width,0)=nvl('".$result_fabric_description[csf('dia_width')]."',0) and 
												  nvl(b.process_loss_percent,0)=nvl('".$result_fabric_description[csf('process_loss_percent')]."',0) and
												  b.status_active=1 and
												  b.is_deleted=0
												  ");
				}
				list($color_wise_wo_result_qnty)=$color_wise_wo_sql_qnty
			?>
			<td width='50' align='right'><?  echo number_format($color_wise_wo_result_qnty[csf('fin_fab_qnty')],2) ;?></td><td width='50' align='right' > <? echo number_format($color_wise_wo_result_qnty[csf('grey_fab_qnty')],2);?></td>
            <?
			}
			?>
            <td align="right"><? echo number_format($grand_total_fin_fab_qnty,2);?></td>
            <td align="right"><? echo number_format($grand_total_grey_fab_qnty,2);?></td>
            <td align="right">
            <?
            if($process_loss_method==1)
			{
				$totalprocess_percent=(($grand_total_grey_fab_qnty-$grand_total_fin_fab_qnty)/$grand_total_fin_fab_qnty)*100;
			}
			
			if($process_loss_method==2)
			{
				$totalprocess_percent=(($grand_total_grey_fab_qnty-$total_fin_fab_qnty)/$grand_total_grey_fab_qnty)*100;
			}
			echo number_format($totalprocess_percent,2);
			?>
            </td>
            </tr> 
    </table>

        <br/>
        
        
        
        
         <?
		$yarn_count_arr=return_library_array( "select id,yarn_count from lib_yarn_count",'id','yarn_count');			
		$yarn_sql_array=sql_select("SELECT min(id) as id ,count_id, copm_one_id, percent_one,copm_two_id, percent_two, type_id, sum(cons_qnty) as yarn_required, AVG(rate) as rate from wo_pre_cost_fab_yarn_cost_dtls where job_no='$job_no' and  status_active=1 and is_deleted=0 group by count_id,copm_one_id,percent_one, copm_two_id,percent_two,type_id order by id");
		?>
        <table  width="100%"  border="0" cellpadding="0" cellspacing="0" >
            <tr>
                <td width="49%" valign="top">
                    <table  width="100%"  border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
                    <tr align="center">
                    <td colspan="7"><b>Yarn Required Summary (Pre Cost)</b></td>
                    
                    </tr>
                    <tr align="center">
                    <td>Sl</td>
                    <td>Yarn Description</td>
                    <td>Brand</td>
                    <td>Lot</td>
                    <?
					if($show_yarn_rate==1)
					{
					?>
                    <td>Rate</td>
                    <?
					}
					?>
                    <td>Cons for <? echo $costing_per; ?> Gmts</td>
                    <td>Total (KG)</td>
                    </tr>
                    <?
					$i=0;
					$total_yarn=0;
					foreach($yarn_sql_array  as $row)
                    {

						$i++;
					?>
                    <tr align="center">
                    <td><? echo $i; ?></td>
                    <td>
					<?
					$yarn_des=$yarn_count_arr[$row[csf('count_id')]]." ".$composition[$row[csf('copm_one_id')]]." ".$row[csf('percent_one')]."%  ";
					if($row['copm_two_id'] !=0)
					{
						$yarn_des.=$composition[$row[csf('copm_two_id')]]." ".$row[csf('percent_two')]."%";
					}
					$yarn_des.=$yarn_type[$row[csf('type_id')]];
					//echo $yarn_count_arr[$row['count_id']]." ".$composition[$row['copm_one_id']]." ".$row['percent_one']."%  ".$composition[$row['copm_two_id']]." ".$row['percent_two']."%  ".$yarn_type[$row['type_id']]; 
					echo $yarn_des;
					?>
                    </td>
                    <td></td>
                    <td></td>
                    <?
					if($show_yarn_rate==1)
					{
					?>
                     <td><? echo number_format($row[csf('rate')],4); ?></td>
                     <?
					}
					 ?>
                    <td><? echo number_format($row[csf('yarn_required')],4); ?></td>
                   
                    <!--<td><? //echo number_format(($row['yarn_required']/$po_qnty_tot)*$costing_per_qnty,2); ?></td>-->
                    <td align="right"><? //echo number_format($row['yarn_required'],2); $total_yarn+=$row['yarn_required']; ?></td>
                    </tr>
                    <?
					}
					?>
                    <tr align="center">
                    <td>Total</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <?
					if($show_yarn_rate==1)
					{
					?>
                    <td></td>
                    <?
                    }
					?>
                    <td></td>
                    <td align="right"><? //echo number_format($total_yarn,2); ?></td>
                    </tr>
                    </table>
                </td>
                <td width="2%">
                </td>
                <td width="49%" valign="top" align="center">
                <?
				$yarn_sql_array=sql_select("SELECT min(a.id) as id, a.item_id, sum(a.qnty) as qnty ,min(b.supplier_id) as supplier_id,min(b.lot) as lot from inv_material_allocation_dtls a,product_details_master b where a.item_id=b.id and a.booking_no=$txt_booking_no and  a.status_active=1 and a.is_deleted=0 group by a.item_id order by a.id");
				if(count($yarn_sql_array)>0)
				{
				?>
                   <table  width="100%"  border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
                    <tr align="center">
                    <td colspan="7"><b>Allocated Yarn</b></td>
                    
                    </tr>
                    <tr align="center">
                    <td>Sl</td>
                    <td>Yarn Description</td>
                    <td>Brand</td>
                    <td>Lot</td>
                   
                   
                    <td>Allocated Qty (Kg)</td>
                    </tr>
                    <?
					$total_allo=0;
					$item=return_library_array( "select id, product_name_details from   product_details_master",'id','product_name_details');
					$supplier=return_library_array( "select id, short_name from   lib_supplier",'id','short_name');
					//$yarn_sql_array=sql_select("SELECT a.item_id, a.qnty,b.supplier_id,b.lot from inv_material_allocation_dtls a,product_details_master b where a.item_id=b.id and a.booking_no=$txt_booking_no and  a.status_active=1 and a.is_deleted=0");
					$i=0;
					$total_yarn=0;
					foreach($yarn_sql_array  as $row)
                    {

						$i++;
					?>
                    <tr align="center">
                    <td><? echo $i; ?></td>
                    <td>
					<?
					
					echo $item[$row[csf('item_id')]];
					?>
                    </td>
                    <td>
                    <?
					
					echo $supplier[$row[csf('supplier_id')]];
					?>
                    </td>
                    <td>
					<?
					
					echo $row[csf('lot')];
					?>
                    </td>
                    <td align="right"><? echo number_format($row[csf('qnty')],4); $total_allo+= $row[csf('qnty')];?></td>
                    </tr>
                    <?
					}
					?>
                    <tr align="center">
                    <td>Total</td>
                    <td></td>
                    
                    
                    <td></td>
                    <td></td>
                    <td align="right"><? echo number_format($total_allo,4); ?></td>
                    </tr>
                    </table>
                    <?
				}
				else
				{
					$is_yarn_allocated=return_field_value("allocation", "variable_settings_inventory", "company_name=$cbo_company_name and variable_list=18 and item_category_id=1"); 
					if($is_yarn_allocated==1)
					{
					?>
					<font style=" font-size:30px"><b> Draft</b></font>
                    <?
					}
					else
					{
						echo "";
					}
				}
					?>
                </td>
            </tr>
        </table>
        <br/>
        
        <table  width="100%"  border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="49%" style="border:solid; border-color:#000; border-width:thin" valign="top">
                    <table  width="100%"  border="0" cellpadding="0" cellspacing="0">
                	<thead>
                    	<tr>
                        	<th width="3%"></th><th width="97%" align="left"><u>Special Instruction</u></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?
					$data_array=sql_select("select id, terms from  wo_booking_terms_condition where booking_no=$txt_booking_no");// quotation_id='$data'
					if ( count($data_array)>0)
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							?>
                            	<tr id="settr_1" valign="top">
                                    <td style="vertical-align:top">
                                    <? echo $i;?>
                                    </td>
                                    <td>
                                   <strong style="font-size:20px"> <? echo $row[csf('terms')]; ?></strong>
                                    </td>
                                </tr>
                            <?
						}
					}
					/*else
					{
				    $i=0;
					$data_array=sql_select("select id, terms from  lib_terms_condition");// quotation_id='$data'
					foreach( $data_array as $row )
						{
							$i++;
					?>
                    <tr id="settr_1" align="">
                                    <td valign="top">
                                    <? echo $i;?>
                                    </td>
                                    <td>
                                    <? echo $row['terms']; ?>
                                    </td>
                                    
                                </tr>
                    <? 
						}
					} */
					?>
                </tbody>
                </table>
                </td>
                <td width="2%">
                </td>
                <td width="49%" valign="top">
                   <table width="100%"  border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
                   <tr align="center">
                    <td colspan="10"><b>Comments</b></td>
                    
                    </tr>
                    <tr align="center">
                    <td>Sl</td>
                    <td>Po NO</td>
                    <td>Ship Date</td>
                    <td>Pre-Cost Qty</td>
                    <td>Mn.Book Qty</td>
                    <td>Sht.Book Qty</td>
                    <td>Smp.Book Qty</td>
                    <td>Tot.Book Qty</td>
                    <td>Balance</td>
                    <td>Comments</td>
                    </tr>
                    <?
	$cbo_fabric_natu=str_replace("'","",$cbo_fabric_natu);
	$cbo_fabric_source=str_replace("'","",$cbo_fabric_source);
	if ($cbo_fabric_natu!=0) $cbo_fabric_natu="and a.fab_nature_id='$cbo_fabric_natu'"; 
	if ($cbo_fabric_source!=0) $cbo_fabric_source_cond="and a.fabric_source='$cbo_fabric_source'"; 
	$paln_cut_qnty_array=return_library_array( "select min(id) as id,sum(plan_cut_qnty) as plan_cut_qnty from wo_po_color_size_breakdown  where po_break_down_id in(".str_replace("'","",$txt_order_no_id).") and is_deleted=0 and status_active=1 group by color_mst_id,size_mst_id,item_mst_id", "id", "plan_cut_qnty");
	
	$item_ratio_array=return_library_array( "select gmts_item_id,set_item_ratio from wo_po_details_mas_set_details  where job_no =$txt_job_no", "gmts_item_id", "set_item_ratio");
	$nameArray=sql_select("
	select
	a.id,
	a.item_number_id,
	a.costing_per,
	b.po_break_down_id,
	b.color_size_table_id,
	b.requirment,
	c.po_number
FROM
	wo_pre_cost_fabric_cost_dtls a,
	wo_pre_cos_fab_co_avg_con_dtls b,
	wo_po_break_down c
WHERE
	a.job_no=b.job_no and
	a.job_no=c.job_no_mst and
    a.id=b.pre_cost_fabric_cost_dtls_id and
	b.po_break_down_id=c.id and
	b.po_break_down_id in (".str_replace("'","",$txt_order_no_id).")  $cbo_fabric_natu $cbo_fabric_source_cond and a.status_active=1 and a.is_deleted=0
	order by a.id");
	$count=0;
	$tot_grey_req_as_pre_cost_arr=array();
	foreach ($nameArray as $result)
	{
		if (count($nameArray)>0 )
		{
            if($result[csf("costing_per")]==1)
			{
				$tot_grey_req_as_pre_cost=def_number_format((($paln_cut_qnty_array[$result[csf("color_size_table_id")]]/(12*$item_ratio_array[$result[csf("item_number_id")]]))*$result[csf("requirment")]),5,"");
			}
			if($result[csf("costing_per")]==2)
			{
				$tot_grey_req_as_pre_cost=def_number_format((($paln_cut_qnty_array[$result[csf("color_size_table_id")]]/(1*$item_ratio_array[$result[csf("item_number_id")]]))*$result[csf("requirment")]),5,"");
			}
			if($result[csf("costing_per")]==3)
			{
				$tot_grey_req_as_pre_cost=def_number_format((($paln_cut_qnty_array[$result[csf("color_size_table_id")]]/(24*$item_ratio_array[$result[csf("item_number_id")]]))*$result[csf("requirment")]),5,"");
			}
			if($result[csf("costing_per")]==4)
			{
				$tot_grey_req_as_pre_cost=def_number_format((($paln_cut_qnty_array[$result[csf("color_size_table_id")]]/(36*$item_ratio_array[$result[csf("item_number_id")]]))*$result[csf("requirment")]),5,"");
			}
			if($result[csf("costing_per")]==5)
			{
				$tot_grey_req_as_pre_cost=def_number_format((($paln_cut_qnty_array[$result[csf("color_size_table_id")]]/(48*$item_ratio_array[$result[csf("item_number_id")]]))*$result[csf("requirment")]),5,"");
			}
			$tot_grey_req_as_pre_cost_arr[$result[csf("po_number")]]+=$tot_grey_req_as_pre_cost;
        }
    }
	                $total_pre_cost=0;
					$total_booking_qnty_main=0;
					$total_booking_qnty_short=0;
					$total_booking_qnty_sample=0;
					$total_tot_bok_qty=0;
					$tot_balance=0;
					/*$booking_qnty=return_library_array( "select max(a.po_break_down_id) as po_break_down_id ,sum(a.grey_fab_qnty) as grey_fab_qnty  from wo_booking_dtls a, wo_po_break_down b where a.po_break_down_id =b.id and a.po_break_down_id in(".str_replace("'","",$txt_order_no_id).") and booking_type in(1,4)  and a.status_active=1 and a.is_deleted=0 group by b.po_number order by a.po_break_down_id", "po_break_down_id", "grey_fab_qnty");*/
					$booking_qnty_main=return_library_array( "select max(a.po_break_down_id) as po_break_down_id ,sum(a.grey_fab_qnty) as grey_fab_qnty  from wo_booking_dtls a, wo_po_break_down b where a.po_break_down_id =b.id and a.po_break_down_id in(".str_replace("'","",$txt_order_no_id).") and booking_type =1 and is_short=2 and a.status_active=1 and a.is_deleted=0 group by b.po_number order by a.po_break_down_id", "po_break_down_id", "grey_fab_qnty");
					$booking_qnty_short=return_library_array( "select max(a.po_break_down_id) as po_break_down_id ,sum(a.grey_fab_qnty) as grey_fab_qnty  from wo_booking_dtls a, wo_po_break_down b where a.po_break_down_id =b.id and a.po_break_down_id in(".str_replace("'","",$txt_order_no_id).") and booking_type =1 and is_short=1 and a.status_active=1 and a.is_deleted=0 group by b.po_number order by a.po_break_down_id", "po_break_down_id", "grey_fab_qnty");
					$booking_qnty_sample=return_library_array( "select max(a.po_break_down_id) as po_break_down_id ,sum(a.grey_fab_qnty) as grey_fab_qnty  from wo_booking_dtls a, wo_po_break_down b where a.po_break_down_id =b.id and a.po_break_down_id in(".str_replace("'","",$txt_order_no_id).") and booking_type =4  and a.status_active=1 and a.is_deleted=0 group by b.po_number order by a.po_break_down_id", "po_break_down_id", "grey_fab_qnty");
					
					$sql_data=sql_select( "select max(a.id) as id,  a.po_number,max(a.pub_shipment_date) as pub_shipment_date,sum(a.plan_cut) as plan_cut  from wo_po_break_down a,wo_pre_cost_sum_dtls b,wo_pre_cost_mst c where a.job_no_mst=b.job_no and a.job_no_mst=c.job_no and a.id in(".str_replace("'","",$txt_order_no_id).") group by a.po_number order by a.id");
					foreach($sql_data  as $row)
                    {
					$col++;
					?>
                    <tr align="center">
                    <td><? echo $col; ?></td>
                    <td><? echo $row[csf("po_number")]; ?></td>
                     <td><? echo change_date_format($row[csf("pub_shipment_date")],"dd-mm-yyyy",'-'); ?></td>
                    <td align="right"><? echo number_format($tot_grey_req_as_pre_cost_arr[$row[csf("po_number")]],2); $total_pre_cost+=$tot_grey_req_as_pre_cost_arr[$row[csf("po_number")]]; ?></td>
                    <td align="right"><? echo number_format($booking_qnty_main[$row[csf("id")]],2); $total_booking_qnty_main+=$booking_qnty_main[$row[csf("id")]];?></td>
                    <td align="right"><? echo number_format($booking_qnty_short[$row[csf("id")]],2); $total_booking_qnty_short+=$booking_qnty_short[$row[csf("id")]];?></td>
                    <td align="right"><? echo number_format($booking_qnty_sample[$row[csf("id")]],2); $total_booking_qnty_sample+=$booking_qnty_sample[$row[csf("id")]];?></td>
                    <td align="right"><? $tot_bok_qty=$booking_qnty_main[$row[csf("id")]]+$booking_qnty_short[$row[csf("id")]]+$booking_qnty_sample[$row[csf("id")]]; echo number_format($tot_bok_qty,2); $total_tot_bok_qty+=$tot_bok_qty;?></td>
                    <td align="right">
					<? $balance= def_number_format($tot_grey_req_as_pre_cost_arr[$row[csf("po_number")]]-$tot_bok_qty,2,""); echo number_format($balance,2); $tot_balance+= $balance?>
                    </td>
                    <td>
					<? 
					if( $balance>0)
					{
						echo "Less Booking";
					}
					else if ($balance<0) 
					{
						echo "Over Booking";
					} 
					else
					{
						echo "";
					}
					?>
                    </td>
                    </tr>
                    <?
					}
					?>
                    <tfoot>
                    
                    <tr>
                    <td colspan="3">Total:</td>
                    
                    <td align="right"><? echo number_format($total_pre_cost,2); ?></td>
                    <td align="right"><? echo number_format($total_booking_qnty_main,2); ?></td>
                    <td align="right"><? echo number_format($total_booking_qnty_short,2); ?></td>
                    <td align="right"><? echo number_format($total_booking_qnty_sample,2); ?></td>
                     <td align="right"><? echo number_format($total_tot_bok_qty,2); ?></td>
                    <td align="right"><? echo number_format($tot_balance,2); ?></td>
                    <td></td>
                    </tr>
                    </tfoot>
                    </table>
                </td>
                
            </tr>
        </table>
        
          <?
		 	echo signature_table(5, $cbo_company_name, "1330px");
		 ?>
       </div>
       <?
      
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if($db_type==0)
		{
		$new_booking_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'SM', date("Y",time()), 5, "select booking_no_prefix, booking_no_prefix_num from wo_booking_mst where company_id=$cbo_company_name and booking_type=4 and YEAR(insert_date)=".date('Y',time())." order by booking_no_prefix_num desc ", "booking_no_prefix", "booking_no_prefix_num" ));
		}
		if($db_type==2)
		{
		$new_booking_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'SM', date("Y",time()), 5, "select booking_no_prefix, booking_no_prefix_num from wo_booking_mst where company_id=$cbo_company_name and booking_type=4 and to_char(insert_date,'YYYY')=".date('Y',time())." order by booking_no_prefix_num desc ", "booking_no_prefix", "booking_no_prefix_num" ));
		}
		$id=return_next_id( "id", "wo_booking_mst", 1 ) ;
		$field_array="id,booking_type,is_short,booking_no_prefix,booking_no_prefix_num,booking_no,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,currency_id,exchange_rate,pay_mode,source,booking_date,delivery_date,booking_month,booking_year,supplier_id,attention,ready_to_approved,inserted_by,insert_date"; 
		 $data_array ="(".$id.",4,2,'".$new_booking_no[1]."',".$new_booking_no[2].",'".$new_booking_no[0]."',".$cbo_company_name.",".$cbo_buyer_name.",".$txt_job_no.",".$txt_order_no_id.",".$cbo_fabric_natu.",".$cbo_fabric_source.",".$cbo_currency.",".$txt_exchange_rate.",".$cbo_pay_mode.",".$cbo_source.",".$txt_booking_date.",".$txt_delivery_date.",".$cbo_booking_month.",".$cbo_booking_year.",".$cbo_supplier_name.",".$txt_attention.",".$cbo_ready_to_approved.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		 $rID=sql_insert("wo_booking_mst",$field_array,$data_array,0);
		if($db_type==0)
		{
			if($rID){
				mysql_query("COMMIT");  
				echo "0**".$new_booking_no[0];
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_booking_no[0];
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID){
				oci_commit($con);  
				echo "0**".$new_booking_no[0];
			}
			else{
				oci_rollback($con);  
				echo "10**".$new_booking_no[0];
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
	
		/*if (is_duplicate_field( "sample_type_id", "wo_po_sample_approval_info", "job_no_mst=$txt_job_no and sample_type_id=$cbo_sample_type and id!=$update_id and is_deleted=0" ) == 1)
		{
			echo "11**0"; 
			die;
		}*/
			 
		
		
		$field_array="company_id*buyer_id*job_no*po_break_down_id*item_category*fabric_source*currency_id*exchange_rate*pay_mode*source*booking_date*delivery_date*booking_month*booking_year*supplier_id*attention*ready_to_approved*updated_by*update_date"; 
		 $data_array ="".$cbo_company_name."*".$cbo_buyer_name."*".$txt_job_no."*".$txt_order_no_id."*".$cbo_fabric_natu."*".$cbo_fabric_source."*".$cbo_currency."*".$txt_exchange_rate."*".$cbo_pay_mode."*".$cbo_source."*".$txt_booking_date."*".$txt_delivery_date."*".$cbo_booking_month."*".$cbo_booking_year."*".$cbo_supplier_name."*".$txt_attention."*".$cbo_ready_to_approved."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		$rID=sql_update("wo_booking_mst",$field_array,$data_array,"booking_no","".$txt_booking_no."",0);
		
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con);  
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array="status_active*is_deleted";
		$data_array="'2'*'1'";
		$rID=sql_delete("wo_booking_mst",$field_array,$data_array,"id","".$update_id."",1);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);   
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con);   
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
		 
		
	}
}

if($action=="save_update_delete_dtls")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		 if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id=return_next_id( "id", "wo_booking_dtls", 1 ) ;
		 $field_array="id,job_no,po_break_down_id,pre_cost_fabric_cost_dtls_id,booking_no,booking_type, is_short,fabric_color_id,gmts_color_id,item_size,gmts_size,dia_width,fin_fab_qnty,process_loss_percent,grey_fab_qnty,rate,amount";
			$data_array="(".$id.",".$txt_job_no.",".$cbo_order_id.",".$cbo_fabricdescription_id.",".$txt_booking_no.",4,2,".$cbo_fabriccolor_id.",".$cbo_garmentscolor_id.",".$cbo_itemsize_id.",".$cbo_garmentssize_id.",".$txt_dia_width.",".$txt_finish_qnty.",".$txt_process_loss.",".$txt_grey_qnty.",".$txt_rate.",".$txt_amount.")";
			//$id=$id+1;
		 
		 $rID=sql_insert("wo_booking_dtls",$field_array,$data_array,1);
		 check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "0**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
	if ($operation==1)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
	    if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**1"; die;}		
	    $field_array_up="job_no*po_break_down_id*pre_cost_fabric_cost_dtls_id*booking_no*booking_type*is_short*fabric_color_id*gmts_color_id*item_size*gmts_size*dia_width*fin_fab_qnty*process_loss_percent*grey_fab_qnty*rate*amount";
	    $data_array_up ="".$txt_job_no."*".$cbo_order_id."*".$cbo_fabricdescription_id."*".$txt_booking_no."*4*2*".$cbo_fabriccolor_id."*".$cbo_garmentscolor_id."*".$cbo_itemsize_id."*".$cbo_garmentssize_id."*".$txt_dia_width."*".$txt_finish_qnty."*".$txt_process_loss."*".$txt_grey_qnty."*".$txt_rate."*".$txt_amount."";
	    $rID=sql_update("wo_booking_dtls",$field_array_up,$data_array_up,"id","".$update_id_details."",0);
	    check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con);   
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
	if ($operation==2)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$is_yarn_issued=sql_select("select id,issue_number from inv_issue_master where issue_basis=1 and issue_purpose=4 and item_category=1 and entry_form=3 and booking_no=$txt_booking_no and status_active=1	and is_deleted=0");
		
		if(count($is_yarn_issued)>0)
		{
		     echo "13**".str_replace("'","",$txt_booking_no); die;
		}
		$rID=execute_query( "update wo_booking_dtls set status_active=0,is_deleted =1 where  id =$update_id_details",0);	
			
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				rollback($con);  
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
}


if ($action=="fabric_booking_popup")
{
  	echo load_html_head_contents("Booking Search","../../../", 1, 1, $unicode);
?>
     
	<script>
	 
	function js_set_value(booking_no)
	{
		document.getElementById('selected_booking').value=booking_no;
		parent.emailwindow.hide();
	}
	
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="900" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
    	<tr>
        	<td align="center" width="100%">
            	<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                   <thead>
                           <th colspan="2"> </th>
                        	<th  >
                              <?
                               echo create_drop_down( "cbo_search_category", 130, $string_search_type,'', 1, "-- Search Catagory --" );
                              ?>
                            </th>
                            <th colspan="3"></th>
                     </thead>
                    <thead>                	 
                        <th width="150">Company Name</th>
                        <th width="150">Buyer Name</th>
                         <th width="100">Booking No</th>
                        <th width="100">Job No</th>
                        <th width="200">Date Range</th><th></th>           
                    </thead>
        			<tr>
                    	<td> <input type="hidden" id="selected_booking">
							<? 
								echo create_drop_down( "cbo_company_mst", 150, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", '', "load_drop_down( 'size_color_breakdown_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );");
							?>
                        </td>
                   	<td id="buyer_td">
                     <? 
						echo create_drop_down( "cbo_buyer_name", 172, $blank_array,"", 1, "-- Select Buyer --" );
					?>	</td>
                     <td><input name="txt_booking_prifix" id="txt_booking_prifix" class="text_boxes" style="width:100px"></td>
                    <td><input name="txt_job_prifix" id="txt_job_prifix" class="text_boxes" style="width:100px"></td>
                    <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">
					  <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_mst').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_job_prifix').value+'_'+document.getElementById('cbo_year_selection').value+'_'+document.getElementById('txt_booking_prifix').value+'_'+document.getElementById('cbo_search_category').value, 'create_booking_search_list_view', 'search_div', 'sample_booking_controller','setFilterGrid(\'list_view\',-1)')" style="width:100px;" /></td>
        		</tr>
             </table>
          </td>
        </tr>
        <tr>
            <td  align="center" height="40" valign="middle">
            <? 
			echo create_drop_down( "cbo_year_selection", 70, $year,"", 1, "-- Select --", date('Y'), "",0 );		
			?>
			<? echo load_month_buttons();  ?>
            </td>
            </tr>
        <tr>
            <td align="center"valign="top" id="search_div"> 
            </td>
        </tr>
    </table>    
    
    </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}

if ($action=="create_booking_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company="  company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $buyer=" and buyer_id='$data[1]'"; else $buyer="";//{ echo "Please Select Buyer First."; die; }
	
	if($db_type==0)
	 {
		  $booking_year_cond=" and SUBSTRING_INDEX(a.insert_date, '-', 1)=$data[5]";
		  $year_cond=" and SUBSTRING_INDEX(b.insert_date, '-', 1)=$data[5]"; 
		  if ($data[2]!="" &&  $data[3]!="") $booking_date  = "and a.booking_date  between '".change_date_format($data[2], "yyyy-mm-dd", "-")."' 
		  and '".change_date_format($data[3], "yyyy-mm-dd", "-")."'"; else $booking_date =""; 
     }
	if($db_type==2)
	 {
		  $booking_year_cond=" and to_char(a.insert_date,'YYYY')=$data[5]";
		  $year_cond=" and to_char(b.insert_date,'YYYY')=$data[5]";
		  if ($data[2]!="" &&  $data[3]!="") $booking_date  = "and a.booking_date  between '".change_date_format($data[2], "yyyy-mm-dd", "-",1)."'
		  and '".change_date_format($data[3], "yyyy-mm-dd", "-",1)."'"; else $booking_date ="";
	 }
	if($data[7]==4 || $data[7]==0)
		{
			if (str_replace("'","",$data[4])!="") $job_cond=" and b.job_no_prefix_num like '%$data[4]%' $year_cond "; else  $job_cond=""; 
			if (str_replace("'","",$data[6])!="") $booking_cond=" and a.booking_no_prefix_num like '%$data[6]%'  $booking_year_cond  "; else $booking_cond="";
		}
    if($data[7]==1)
		{
			if (str_replace("'","",$data[4])!="") $job_cond=" and b.job_no_prefix_num ='$data[4]' "; else  $job_cond=""; 
			if (str_replace("'","",$data[6])!="") $booking_cond=" and a.booking_no_prefix_num ='$data[6]'   "; else $booking_cond="";
		}
   if($data[7]==2)
		{
			if (str_replace("'","",$data[4])!="") $job_cond=" and b.job_no_prefix_num like '$data[4]%'  $year_cond"; else  $job_cond=""; 
			if (str_replace("'","",$data[6])!="") $booking_cond=" and a.booking_no_prefix_num like '$data[6]%'  $booking_year_cond  "; else $booking_cond="";
		}
	if($data[7]==3)
		{
			if (str_replace("'","",$data[4])!="") $job_cond=" and b.job_no_prefix_num like '%$data[4]'  $year_cond"; else  $job_cond=""; 
			if (str_replace("'","",$data[6])!="") $booking_cond=" and a.booking_no_prefix_num like '%$data[6]'  $booking_year_cond  "; else $booking_cond="";
		}

	$po_array=array();
	$sql_po= sql_select("select a.booking_no_prefix_num, a.booking_no,a.po_break_down_id from  wo_booking_mst a where $company $buyer $booking_date and a.booking_type=4 and a.is_short=2 and a.status_active=1  and 	a.is_deleted=0 order by a.booking_no");
	foreach($sql_po as $row)
	{
		$po_id=explode(",",$row[csf("po_break_down_id")]);
		$po_number_string="";
		foreach($po_id as $key=> $value )
		{
			$po_number_string.=$po_number[$value].",";
		}
		$po_array[$row[csf("po_break_down_id")]]=rtrim($po_number_string,",");
	}
	 $approved=array(0=>"No",1=>"Yes");
	 $is_ready=array(0=>"No",1=>"Yes",2=>"No"); 
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$suplier=return_library_array( "select id, short_name from lib_supplier",'id','short_name');
	//$po_no=return_library_array( "select job_no, job_no_prefix_num from  wo_po_details_master",'job_no','job_no_prefix_num');
	//print_r($po_no);die;
	$arr=array (2=>$comp,3=>$buyer_arr,5=>$po_array,6=>$item_category,7=>$fabric_source,8=>$suplier,9=>$approved,10=>$is_ready);
	
	$sql= "select a.booking_no_prefix_num, b.job_no_prefix_num, a.booking_no,a.booking_date,a.company_id,buyer_id,a.po_break_down_id,a.item_category,a.fabric_source,a.supplier_id,a.is_approved,a.ready_to_approved from wo_booking_mst a,wo_po_details_master b  where $company $buyer $booking_date $job_cond $booking_cond and a.job_no=b.job_no and a.booking_type=4 and a.is_short=2 and  a.status_active=1  and 	a.is_deleted=0 order by a.booking_no"; 
	//echo $sql;
	echo  create_list_view("list_view", "Booking No,Booking Date,Company,Buyer,Job No.,PO number,Fabric Nature,Fabric Source,Supplier,Approved,Is-Ready", "80,80,80,100,90,200,80,80,50,50","1020","300",0, $sql , "js_set_value", "booking_no", "", 1, "0,0,company_id,buyer_id,0,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", $arr , "booking_no_prefix_num,booking_date,company_id,buyer_id,job_no_prefix_num,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", '','','0,0,0,0,0,0,0,0,0,0,0','','');
}


if ($action=="order_search_popup")
{
  	echo load_html_head_contents("Order Search","../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
	<script>
	 var selected_id = new Array, selected_name = new Array();	
	 function check_all_data() {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 
			tbl_row_count = tbl_row_count;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str_data ) {
			//toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			var str_all=str_data.split("_");
			var str_po=str_all[1];
			var str=str_all[0];
			//alert(str_all[2]);
			if ( document.getElementById('job_no').value!="" && document.getElementById('job_no').value!=str_all[2] )
			{
				alert('No Job Mix Allowed')
				return;	
			}
				document.getElementById('job_no').value=str_all[2];
				
				if( jQuery.inArray( str , selected_id ) == -1 ) {
					selected_id.push( str );
					selected_name.push( str_po );
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == str ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 );
				}
				var id = '' ; var name = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ',';
				}
				id = id.substr( 0, id.length - 1 );
				name = name.substr( 0, name.length - 1 );
				$('#po_number_id').val( id );
				$('#po_number').val( name );
		}
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
<?
$booking_month=0;
 if(str_replace("'","",$cbo_booking_month)<10)
 {
	 $booking_month.=str_replace("'","",$cbo_booking_month);
 }
 else
 {
	$booking_month=str_replace("'","",$cbo_booking_month); 
 }
$start_date="01"."-".$booking_month."-".str_replace("'","",$cbo_booking_year);
$end_date=cal_days_in_month(CAL_GREGORIAN, $booking_month, str_replace("'","",$cbo_booking_year))."-".$booking_month."-".str_replace("'","",$cbo_booking_year);
?>
	<form name="searchpofrm_1" id="searchpofrm_1">
         
				<table width="900"  align="center" rules="all">
                    <tr>
                        <td align="center" width="100%">
                            <table  width="900" class="rpt_table" align="center" rules="all">
                                <thead>                	 
                                    <th width="150">Company Name</th>
                                    <th width="150">Buyer Name</th>
                                    <th width="100">Job No</th>
                                    <th width="130">Order No</th>
                                    <th width="200">Date Range</th><th><input type="checkbox" value="0" onClick="set_checkvalue()" id="chk_job_wo_po">Job Without PO</th>           
                                </thead>
                                <tr>
                                    <td> 
                                        <? 
                                            echo create_drop_down( "cbo_company_mst", 150, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", str_replace("'","",$cbo_company_name), "load_drop_down( 'sample_booking_controller', this.value, 'load_drop_down_buyer_order', 'buyer_td' );");
                                        ?>
                                    </td>
                                <td id="buyer_td">
									<? 
                                    	echo create_drop_down( "cbo_buyer_name", 172, $blank_array,"", 1, "-- Select Buyer --" );
                                    ?>	
                                </td>
                                <td><input name="txt_job_prifix" id="txt_job_prifix" class="text_boxes" style="width:100px"></td>
                                <td><input name="txt_order_search" id="txt_order_search" class="text_boxes" style="width:130px"></td>
                                <td>
                                  <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:85px" value="<? echo $start_date; ?>"/>
                                  <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:85px" value="<? echo $end_date; ?>"/>
                                 </td> 
                                 <td align="center">
                                 <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_mst').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('chk_job_wo_po').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_job_prifix').value+'_'+document.getElementById('txt_order_search').value, 'create_po_search_list_view', 'search_div', 'sample_booking_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100%;" /></td>
                            </tr>
                            <tr>
                                <td  align="center"  valign="top" colspan="6">
                                    <? //echo load_month_buttons();  ?>
                                    <input type="hidden" id="po_number_id">
                                    <input type="hidden" id="job_no">
                                </td>
                            </tr>
                            <tr>
                            	<td colspan="6" align="center"><strong>Selected PO Number:</strong> &nbsp;<input type="text" class="text_boxes"  readonly style="width:550px" id="po_number"></td>
                            </tr>
                         </table>
                        
    				</td>
           		</tr>
                
          	
            <tr>
                <td align="center" >
                <input type="button" name="close" onClick="parent.emailwindow.hide();"  class="formbutton" value="Close" style="width:100px" /> 
                </td>
            </tr>
            <tr>
                <td id="search_div" align="center">
                            
                </td>
            </tr>
       </table>
	</form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
  exit();
}
if($action=="create_po_search_list_view")
{
	
	$data=explode('_',$data);
	if ($data[0]!=0) $company=" and a.company_name='$data[0]'"; else { echo "Please Select Company First."; die; }
	
	if ($data[1]!=0) $buyer=" and a.buyer_name='$data[1]'"; else $buyer="";//{ echo "Please Select Buyer First."; die; }
	
	
	if (str_replace("'","",$data[5])!="") $job_cond=" and a.job_no_prefix_num='$data[5]'"; else  $job_cond=""; 
	if (str_replace("'","",$data[6])!="") $order_cond=" and b.po_number like '%$data[6]%'  "; else  $order_cond=""; 
	if($db_type==0)
	{
	if ($data[3]!="" &&  $data[4]!="") $shipment_date = "and b.pub_shipment_date between '".change_date_format($data[3], "yyyy-mm-dd", "-")."' and '".change_date_format($data[4], "yyyy-mm-dd", "-")."'"; else $shipment_date ="";
	}
	if($db_type==2)
	{
	if ($data[3]!="" &&  $data[4]!="") $shipment_date = "and b.pub_shipment_date between '".change_date_format($data[3], "yyyy-mm-dd", "-",1)."' and '".change_date_format($data[4], "yyyy-mm-dd", "-",1)."'"; else $shipment_date ="";
	}
	 
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');

	$arr=array (1=>$comp,2=>$buyer_arr);
	
	if ($data[2]==0)
	{
		 $sql= "select a.job_no_prefix_num, a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.job_quantity,b.id, b.po_number,b.po_quantity,b.shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and b.status_active=1  $shipment_date $company $buyer $job_cond $order_cond order by a.job_no";  
		 
		echo  create_list_view("list_view", "Job No,Company,Buyer,Style Ref. No,Job Qty.,PO number,PO Qty,Shipment Date", "90,60,50,100,70,90,70,80","710","320",0, $sql , "js_set_value", "id,po_number,job_no", 0, 1, "0,company_name,buyer_name,0,0,0,0", $arr , "job_no_prefix_num,company_name,buyer_name,style_ref_no,job_quantity,po_number,po_quantity,shipment_date", '','','0,0,0,0,1,0,1,3','','');
	}
	else
	{
		$sql= "select a.job_no,a.company_name,a.buyer_name,a.style_ref_no from wo_po_details_master a where a.status_active=1  and a.is_deleted=0 $company $buyer $job_cond order by a.job_no";
		
		echo  create_list_view("list_view", "Job No,Company,Buyer,Style Ref. No", "90,60,50,100,90","710","320",0, $sql , "js_set_value", "id", "", 1, "0,company_name,buyer_name,0,0,0,0", $arr , "job_no,company_name,buyer_name,style_ref_no", '','','0,0,0,0,1,0,2,3','','') ;
	}
	
} 

if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Order Search","../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
	<script>
function add_break_down_tr(i) 
 {
	var row_num=$('#tbl_termcondi_details tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
	 
		 $("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});  
		  }).end().appendTo("#tbl_termcondi_details");
		 $('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
		  $('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
		  $('#termscondition_'+i).val("");
	}
		  
}

function fn_deletebreak_down_tr(rowNo) 
{   
	
	
		var numRow = $('table#tbl_termcondi_details tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_termcondi_details tbody tr:last').remove();
		}
	
}

function fnc_fabric_booking_terms_condition( operation )
{
	    var row_num=$('#tbl_termcondi_details tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('termscondition_'+i,'Term Condition')==false)
			{
				return;
			}
			
			data_all=data_all+get_submitted_data_string('txt_booking_no*termscondition_'+i,"");
		}
		var data="action=save_update_delete_fabric_booking_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
		//freeze_window(operation);
		http.open("POST","sample_booking_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_booking_terms_condition_reponse;
}

function fnc_fabric_booking_terms_condition_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
			if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				parent.emailwindow.hide();
			}
	}
}
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
<? echo load_freeze_divs ("../../../",$permission);  ?>
<fieldset>
        	<form id="termscondi_1" autocomplete="off">
           <input type="text" id="txt_booking_no" name="txt_booking_no" value="<? echo str_replace("'","",$txt_booking_no) ?>"/>
            
            
            <table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
                	<thead>
                    	<tr>
                        	<th width="50">Sl</th><th width="530">Terms</th><th ></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?
					$data_array=sql_select("select id, terms from  wo_booking_terms_condition where booking_no=$txt_booking_no");// quotation_id='$data'
					if ( count($data_array)>0)
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							?>
                            	<tr id="settr_1" align="center">
                                    <td>
                                    <? echo $i;?>
                                    </td>
                                    <td>
                                    <input type="text" id="termscondition_<? echo $i;?>"   name="termscondition_<? echo $i;?>" style="width:95%"  class="text_boxes"  value="<? echo $row[csf('terms')]; ?>"  /> 
                                    </td>
                                    <td> 
                                    <input type="button" id="increase_<? echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<? echo $i; ?> )" />
                                    <input type="button" id="decrease_<? echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<? echo $i; ?>);" />
                                    </td>
                                </tr>
                            <?
						}
					}
					else
					{
					$data_array=sql_select("select id, terms from  lib_terms_condition  where is_default=1");// quotation_id='$data'
					foreach( $data_array as $row )
						{
							$i++;
					?>
                    <tr id="settr_1" align="center">
                                    <td>
                                    <? echo $i;?>
                                    </td>
                                    <td>
                                    <input type="text" id="termscondition_<? echo $i;?>"   name="termscondition_<? echo $i;?>" style="width:95%"  class="text_boxes"  value="<? echo $row[csf('terms')]; ?>"  /> 
                                    </td>
                                    <td>
                                    <input type="button" id="increase_<? echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<? echo $i; ?> )" />
                                    <input type="button" id="decrease_<? echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<? echo $i; ?> );" />
                                    </td>
                                </tr>
                    <? 
						}
					} 
					?>
                </tbody>
                </table>
                
                <table width="650" cellspacing="0" class="" border="0">
                	<tr>
                        <td align="center" height="15" width="100%"> </td>
                    </tr>
                	<tr>
                        <td align="center" width="100%" class="button_container">
						        <?
									echo load_submit_buttons( $permission, "fnc_fabric_booking_terms_condition", 0,0 ,"reset_form('termscondi_1','','','','')",1) ; 
									?>
                        </td> 
                    </tr>
                </table>
            </form>
        </fieldset>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}

if($action=="save_update_delete_fabric_booking_terms_condition")
{
$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id=return_next_id( "id", "wo_booking_terms_condition", 1 ) ;
		 $field_array="id,booking_no,terms";
		 for ($i=1;$i<=$total_row;$i++)
		 {
			 $termscondition="termscondition_".$i;
			if ($i!=1) $data_array .=",";
			$data_array .="(".$id.",".$txt_booking_no.",".$$termscondition.")";
			$id=$id+1;
		 }
		// echo  $data_array;
		$rID_de3=execute_query( "delete from wo_booking_terms_condition where  booking_no =".$txt_booking_no."",0);

		 $rID=sql_insert("wo_booking_terms_condition",$field_array,$data_array,1);
		 check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$new_booking_no[0];
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_booking_no[0];
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);
				echo "0**".$new_booking_no[0];
			}
			else{
				oci_rollback($con);
				echo "10**".$new_booking_no[0];
			}
		}
		disconnect($con);
		die;
	}	
}


 





if ($action=="populate_order_data_from_search_popup")
{
	 
	$data_array=sql_select("select a.job_no,a.company_name,a.buyer_name from wo_po_details_master a, wo_po_break_down b where b.id in (".$data.") and a.job_no=b.job_no_mst");
	foreach ($data_array as $row)
	{
		echo "document.getElementById('cbo_company_name').value = '".$row[csf("company_name")]."';\n";  
		echo "document.getElementById('cbo_buyer_name').value = '".$row[csf("buyer_name")]."';\n";  
		echo "document.getElementById('txt_job_no').value = '".$row[csf("job_no")]."';\n";
	}
}

if ($action=="populate_data_from_search_popup")
{
	 $sql= "select booking_no,booking_date,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,currency_id,exchange_rate,pay_mode,booking_month,supplier_id,attention,delivery_date,source,booking_year,is_approved,ready_to_approved from wo_booking_mst  where booking_no='$data'"; 
	
	 $data_array=sql_select($sql);
	 foreach ($data_array as $row)
	 {
		echo "document.getElementById('txt_order_no_id').value = '".$row[csf("po_break_down_id")]."';\n";  
		echo "document.getElementById('cbo_company_name').value = '".$row[csf("company_id")]."';\n";  
		echo "document.getElementById('cbo_buyer_name').value = '".$row[csf("buyer_id")]."';\n";  
		echo "document.getElementById('txt_job_no').value = '".$row[csf("job_no")]."';\n";
		echo "document.getElementById('txt_booking_no').value = '".$row[csf("booking_no")]."';\n";
		echo "document.getElementById('cbo_fabric_natu').value = '".$row[csf("item_category")]."';\n";
		echo "document.getElementById('cbo_fabric_source').value = '".$row[csf("fabric_source")]."';\n";
		echo "document.getElementById('cbo_currency').value = '".$row[csf("currency_id")]."';\n";
		echo "document.getElementById('txt_exchange_rate').value = '".$row[csf("exchange_rate")]."';\n";
		echo "document.getElementById('cbo_pay_mode').value = '".$row[csf("pay_mode")]."';\n";
		echo "document.getElementById('txt_booking_date').value = '".change_date_format($row[csf("booking_date")],'dd-mm-yyyy','-')."';\n";
		echo "document.getElementById('cbo_booking_month').value = '".$row[csf("booking_month")]."';\n";
		echo "document.getElementById('cbo_supplier_name').value = '".$row[csf("supplier_id")]."';\n";
		echo "document.getElementById('txt_attention').value = '".$row[csf("attention")]."';\n";
		echo "document.getElementById('txt_delivery_date').value = '".change_date_format($row[csf("delivery_date")],'dd-mm-yyyy','-')."';\n";
	    echo "document.getElementById('cbo_source').value = '".$row[csf("source")]."';\n";
		echo "document.getElementById('cbo_booking_year').value = '".$row[csf("booking_year")]."';\n";
		echo "document.getElementById('id_approved_id').value = '".$row[csf("is_approved")]."';\n";
		echo "document.getElementById('cbo_ready_to_approved').value = '".$row[csf("ready_to_approved")]."';\n";
		if($row[csf("is_approved")]==1)
		{
			//echo "document.getElementById('app_sms').innerHTML = 'This booking is approved';\n";
			echo "document.getElementById('app_sms2').innerHTML = 'This booking is approved';\n";
		}
		else
		{
			//echo "document.getElementById('app_sms').innerHTML = '';\n";
			echo "document.getElementById('app_sms2').innerHTML = '';\n";
		}
		$po_no="";
		$sql_po= "select po_number from  wo_po_break_down  where id in(".$row[csf('po_break_down_id')].")"; 
		$data_array_po=sql_select($sql_po);
		foreach ($data_array_po as $row_po)
		{
			$po_no.=$row_po[csf('po_number')].",";
		}
		echo "document.getElementById('txt_order_no').value = '".substr($po_no, 0, -1)."';\n";
		echo "enable_disable('".$row[csf("fabric_source")]."');\n";


	 }
}

if($action=="populate_details_data_from_for_update")
{
	
		$data_array=sql_select("select id,pre_cost_fabric_cost_dtls_id,po_break_down_id,fabric_color_id,gmts_color_id,item_size,gmts_size,dia_width,fin_fab_qnty,process_loss_percent,grey_fab_qnty,rate,amount FROM wo_booking_dtls WHERE id ='".$data."' and is_short=2 and status_active=1 and	is_deleted=0");
		foreach ($data_array as $row)
		{
		echo "document.getElementById('cbo_order_id').value = '".$row[csf("po_break_down_id")]."';\n";  
		echo "document.getElementById('cbo_fabricdescription_id').value = '".$row[csf("pre_cost_fabric_cost_dtls_id")]."';\n";  
		echo "document.getElementById('cbo_fabriccolor_id').value = '".$row[csf("fabric_color_id")]."';\n";  
		
		echo "document.getElementById('cbo_garmentscolor_id').value = '".$row[csf("gmts_color_id")]."';\n";  
		echo "document.getElementById('cbo_itemsize_id').value = '".$row[csf("item_size")]."';\n";
		
		echo "document.getElementById('cbo_garmentssize_id').value = '".$row[csf("gmts_size")]."';\n";
		echo "document.getElementById('txt_dia_width').value = '".$row[csf("dia_width")]."';\n";
		echo "document.getElementById('txt_finish_qnty').value = '".$row[csf("fin_fab_qnty")]."';\n";
		echo "document.getElementById('txt_process_loss').value = '".$row[csf("process_loss_percent")]."';\n";
		echo "document.getElementById('txt_grey_qnty').value = '".$row[csf("grey_fab_qnty")]."';\n";
		echo "document.getElementById('txt_rate').value = '".$row[csf("rate")]."';\n";
		echo "document.getElementById('txt_amount').value = '".$row[csf("amount")]."';\n";
		echo "document.getElementById('update_id_details').value = '".$row[csf("id")]."';\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_fabric_booking_dtls',2);\n";  
		//set_button_status(1, permission, 'fnc_fabric_booking_dtls',2)
		}
}


?>