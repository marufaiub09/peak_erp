﻿<?
include('../../includes/common.php');
session_start();

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$trans_Type="1";

if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }

$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 172, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Lacation --", $selected, "" );	
	exit();	 
}

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_party_name", 172, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "" );
	exit();
} 

if ($action=="load_drop_down_buyer_pop")
{
	echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "" );
	exit();
} 

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$trans_Type="1";
	// Insert Start Here ----------------------------------------------------------
	if ($operation==0)   
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		
		if(str_replace("'",'',$update_id)=="")
		{
			if($db_type==0)
			{
				$new_receive_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name),'', 'RECV' , date("Y",time()), 5, "select id,prefix_no,prefix_no_num from sub_material_mst where company_id=$cbo_company_name and trans_Type='$trans_Type'  and YEAR(insert_date)=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
			}
			else if($db_type==2)
			{
				$new_receive_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name),'', 'RECV' , date("Y",time()), 5, "select id,prefix_no,prefix_no_num from sub_material_mst where company_id=$cbo_company_name and trans_Type='$trans_Type' and TO_CHAR(insert_date,'YYYY')=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
			}
			
	
			if(is_duplicate_field( "a.chalan_no", "sub_material_mst a, sub_material_dtls b", "a.sys_no='$new_receive_no[0]' and a.chalan_no=$txt_receive_challan and b.order_id=$order_no_id and b.item_category_id=$cbo_item_category and b.material_description=$txt_material_description and b.color_id=$color_id and b.gsm=$txt_gsm and b.grey_dia=$txt_grey_dia and b.fin_dia=$txt_fin_dia" )==1)
			{
				check_table_status( $_SESSION['menu_id'],0);
				echo "11**0"; 
				die;			
			}			
			/*echo "10**"."select id,prefix_no,prefix_no_num from sub_material_mst where company_id=$cbo_company_name and party_id=$cbo_party_name and trans_Type='$trans_Type'  and YEAR(insert_date)=".date('Y',time())." order by id desc ";
			print_r($new_receive_no);die;*/
			$id=return_next_id("id","sub_material_mst",1) ;
			$field_array="id,prefix_no,prefix_no_num,sys_no,trans_type,company_id,location_id,party_id,chalan_no,subcon_date,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_receive_no[1]."','".$new_receive_no[2]."','".$new_receive_no[0]."','".$trans_Type."',".$cbo_company_name.",".$cbo_location_name.",".$cbo_party_name.",".$txt_receive_challan.",".$txt_receive_date.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";  
			//echo "INSERT INTO sub_material_mst (".$field_array.") VALUES ".$data_array; die;
			$rID=sql_insert("sub_material_mst",$field_array,$data_array,0);
			
			$txt_receive_no=$new_receive_no[0];//change_date_format($data[2], "dd-mm-yyyy", "-",1)
		}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="company_id*location_id*party_id*chalan_no*subcon_date*updated_by*update_date";

			$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_party_name."*".$txt_receive_challan."*".$txt_receive_date."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 

			$rID=sql_update("sub_material_mst",$field_array,$data_array,"id",$update_id,0); 
			
			$txt_receive_no=str_replace("'",'',$txt_receive_no); 
		}
		$id1=return_next_id("id","sub_material_dtls",1) ; 
		$field_array2="id, mst_id, item_category_id, material_description, color_id, quantity, subcon_uom, subcon_roll, rec_cone, gsm, grey_dia, fin_dia, dia_uom, order_id, status_active, inserted_by, insert_date";
		$data_array2="(".$id1.",'".$id."',".$cbo_item_category.",".$txt_material_description.",'".$color_id."',".$txt_receive_quantity.",".$cbo_uom.",".$txt_roll.",".$txt_cone.",".$txt_gsm.",".$txt_grey_dia.",".$txt_fin_dia.",".$cbo_dia_uom.",".$order_no_id.",".$cbo_status.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
		//echo "INSERT INTO sub_material_dtls (".$field_array2.") VALUES ".$data_array2; die;
		$rID2=sql_insert("sub_material_dtls",$field_array2,$data_array2,1);				
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$id);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2) 
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$id);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$id);
			}
		}
		disconnect($con);
		die;		
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");

/*		if(is_duplicate_field( "a.chalan_no", "sub_material_mst a, sub_material_dtls b", "a.chalan_no=$txt_receive_challan and b.order_id=$order_no_id and b.item_category_id=$cbo_item_category and b.material_description=$txt_material_description and b.color_id=$color_id and b.gsm=$txt_gsm and b.grey_dia=$txt_grey_dia and b.fin_dia=$txt_fin_dia" )==1)
		{
			check_table_status( $_SESSION['menu_id'],0);
			echo "11**0"; 
			die;			
		}			
*/					
		$field_array="company_id*location_id*party_id*chalan_no*subcon_date*updated_by*update_date";
		$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_party_name."*".$txt_receive_challan."*".$txt_receive_date."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		
		$rID=sql_update("sub_material_mst",$field_array,$data_array,"id",$update_id,0); 
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		
		$field_array2="item_category_id*material_description*color_id*quantity*subcon_uom*subcon_roll*rec_cone*gsm*grey_dia*fin_dia*dia_uom*order_id*status_active*updated_by*update_date";
		$data_array2="".$cbo_item_category."*".$txt_material_description."*".$color_id."*".$txt_receive_quantity."*".$cbo_uom."*".$txt_roll."*".$txt_cone."*".$txt_gsm."*".$txt_grey_dia."*".$txt_fin_dia."*".$cbo_dia_uom."*".$order_no_id."*".$cbo_status."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";  
					
		$rID2=sql_update("sub_material_dtls",$field_array2,$data_array2,"id",$update_id2,1); //die;
		 
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$update_id);	
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$update_id);	
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$update_id);
			}
		}
		disconnect($con);
	}
	else if ($operation==2)   // delete
	{
		$con = connect();
		
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		 //echo $zero_val;
		if ( $zero_val==1 )
		{
			$field_array="status_active*is_deleted*updated_by*update_date";
			$data_array="0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			$data_array_dtls="1*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			
			if (str_replace("'",'',$cbo_status)==1)
			{
				$rID=sql_update("sub_material_dtls",$field_array,$data_array_dtls,"id",$update_id2,1); //die;
			}
			else
			{
				$rID=sql_update("sub_material_mst",$field_array,$data_array,"id",$update_id,0);  
				//echo "INSERT INTO sub_material_dtls (".$field_array.") VALUES ".$data_array_dtls; die;

				$rID=sql_update("sub_material_dtls",$field_array,$data_array_dtls,"mst_id",$update_id,1);  
			}
		}
		else
		{
			$rID=0;
		}
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id2);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id2);
			}
		}
		if($db_type==2)
		{
			if($rID)
			{
				oci_commit($con);
				echo "2**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id2);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$txt_receive_no)."**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id2);
			}
		}
		disconnect($con); 
	}
}

if ($action=="receive_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	?>
	<script>
		function js_set_value(id)
		{ 
			$("#hidden_mst_id").val(id);
			document.getElementById('selected_job').value=id;
			parent.emailwindow.hide();
		}		
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="searchreceivefrm_1"  id="searchreceivefrm_1" autocomplete="off">
                <table width="750" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>
                        <tr>
                            <th colspan="6"><? echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" ); ?></th>
                        </tr>
                    	<tr>                	 
                            <th width="150">Company Name</th>
                            <th width="150">Party Name</th>
                            <th width="80">Receive ID</th>
                            <th width="80">Challan No</th>
                            <th width="170">Date Range</th>
                            <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>
                        </tr>         
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="selected_job"><? $data=explode("_",$data); ?>  <!--  echo $data;-->
								<? 
									echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $data[0], "load_drop_down( 'sub_contract_material_receive_controller', this.value, 'load_drop_down_buyer_pop', 'buyer_td' );"); ?>
                            </td>
                            <td id="buyer_td">
								<? 
									echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $data[2], "" ); 
                                ?>
                            </td>
                            <td>
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes_numeric" style="width:75px" placeholder="Receive ID" />
                            </td>
                            <td>
                                <input type="text" name="txt_search_challan" id="txt_search_challan" class="text_boxes" style="width:75px" placeholder="Challan" />
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:65px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:65px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_search_challan').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_receive_search_list_view', 'search_div', 'sub_contract_material_receive_controller', 'setFilterGrid(\'tbl_po_list\',-1)')" style="width:70px;" /></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" height="40" valign="middle">
								<? echo load_month_buttons(1);  ?>
                                <input type="hidden" name="hidden_mst_id" id="hidden_mst_id" class="datepicker" style="width:70px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                    </tbody>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?
	exit();
}

if($action=="create_receive_search_list_view")
{
	$data=explode('_',$data);
	$search_type =$data[6];
	if ($data[0]!=0) $company=" and a.company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $buyer_cond=" and a.party_id='$data[1]'"; else $buyer_cond="";
	
	if($db_type==0)
	{ 
		if ($data[2]!="" &&  $data[3]!="") $recieve_date = "and a.subcon_date between '".change_date_format($data[2],'yyyy-mm-dd')."' and '".change_date_format($data[3],'yyyy-mm-dd')."'"; else $recieve_date ="";
	}
	else
	{
		if ($data[2]!="" &&  $data[3]!="") $recieve_date = "and a.subcon_date between '".change_date_format($data[2], "", "",1)."' and '".change_date_format($data[3], "", "",1)."'"; else $recieve_date ="";
	}
	
	if($search_type==1)
	{
		if ($data[4]!='') $rec_id_cond=" and a.prefix_no_num='$data[4]'"; else $rec_id_cond="";
		if ($data[5]!='') $challan_no_cond=" and a.chalan_no='$data[5]'"; else $challan_no_cond="";
	}
	else if($search_type==4 || $search_type==0)
	{
		if ($data[4]!='') $rec_id_cond=" and a.prefix_no_num like '%$data[4]%'"; else $rec_id_cond="";
		if ($data[5]!='') $challan_no_cond=" and a.chalan_no like '%$data[5]%'"; else $challan_no_cond="";
	}
	else if($search_type==2)
	{
		if ($data[4]!='') $rec_id_cond=" and a.prefix_no_num like '$data[4]%'"; else $rec_id_cond="";
		if ($data[5]!='') $challan_no_cond=" and a.chalan_no like '$data[5]%'"; else $challan_no_cond="";
	}
	else if($search_type==3)
	{
		if ($data[4]!='') $rec_id_cond=" and a.prefix_no_num like '%$data[4]'"; else $rec_id_cond="";
		if ($data[5]!='') $challan_no_cond=" and a.chalan_no like '%$data[5]'"; else $challan_no_cond="";
	}	
	
	$party_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$po_arr=return_library_array( "select id,order_no from subcon_ord_dtls",'id','order_no');
	$arr=array (2=>$party_arr,5=>$item_category);
	
	if($db_type==0)
	{
		$year_cond= "year(a.insert_date)as year";
	}
	else if($db_type==2)
	{
		$year_cond= "TO_CHAR(a.insert_date,'YYYY') as year";
	}
	 if($db_type==0)
	 {
		$sql= "select a.id, a.sys_no, a.prefix_no_num, $year_cond, a.location_id, a.party_id, a.subcon_date, a.chalan_no, a.remarks, a.status_active, group_concat(distinct(b.order_id)) as order_id from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=2 and b.is_deleted=0 $recieve_date $company $buyer_cond $rec_id_cond $challan_no_cond group by a.id order by a.id DESC ";
	 }
	 else if ($db_type==2)
	 {
		$sql= "select a.id, a.sys_no, a.prefix_no_num, $year_cond, a.location_id, a.party_id, a.subcon_date, a.chalan_no, a.remarks, a.status_active, listagg(b.order_id,',') within group (order by b.order_id) as order_id from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=2 and b.is_deleted=0 $recieve_date $company $buyer_cond $rec_id_cond $challan_no_cond group by a.id, a.sys_no, a.prefix_no_num, a.insert_date, a.location_id, a.party_id, a.subcon_date, a.chalan_no, a.remarks, a.status_active order by a.id DESC ";
	 }
		//echo $sql; 
	//echo  create_list_view("list_view", "Receive No,Year,Party Name,Challan No,Receive Date,Order No", "70,70,120,110,100,200","720","250",0, $sql , "js_set_value", "id", "", 1, "0,company_id,party_id,party_id,0", $arr , "prefix_no_num,year,party_id,chalan_no,subcon_date", "sub_contract_material_receive_controller","",'0,0,0,0,3') ; 
	$result = sql_select($sql);
	?>
    <div>
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="717" class="rpt_table">
            <thead>
                <th width="40" >SL</th>
                <th width="70" >Receive No</th>
                <th width="70" >Year</th>
                <th width="120" >Party Name</th>
                <th width="100" >Challan No</th>
                <th width="80" >Receive Date</th>
                <th>Order No</th>
            </thead>
     	</table>
     <div style="width:720px; max-height:270px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="700" class="rpt_table" id="tbl_po_list">
			<?
			$i=1;
            foreach( $result as $row )
            {
                if ($i%2==0)  $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$order_no='';
				$order_id=array_unique(explode(",",$row[csf("order_id")]));
				foreach($order_id as $val)
				{
					if($order_no=="") $order_no=$po_arr[$val]; else $order_no.=",".$po_arr[$val];
				}
				?>
					<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value(<? echo $row[csf("id")];?>);" > 
						<td width="40" align="center"><? echo $i; ?></td>
						<td width="70" align="center"><? echo $row[csf("prefix_no_num")]; ?></td>
                        <td width="70" align="center"><? echo $row[csf("year")]; ?></td>
                        <td width="120" align="center"><? echo $party_arr[$row[csf("party_id")]]; ?></td>		
						<td width="100" align="center"><? echo $row[csf("chalan_no")]; ?></td>
						<td width="80"><? echo change_date_format($row[csf("subcon_date")]);  ?></td>	
						<td><p><? echo $order_no; ?></p></td>
					</tr>
				<? 
				$i++;
            }
   		?>
			</table>
		</div>
     </div>
     <?	
	exit();
}

if ($action=="load_php_data_to_form")
{
	$nameArray=sql_select( "select id, sys_no, company_id, location_id, party_id, subcon_date, chalan_no, status_active from  sub_material_mst where id='$data'" ); 
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_receive_no').value 		= '".$row[csf("sys_no")]."';\n";
		echo "document.getElementById('cbo_company_name').value 	= '".$row[csf("company_id")]."';\n";
		echo "$('#cbo_company_name').attr('disabled','true')".";\n"; 
		echo "load_drop_down( 'requires/sub_contract_material_receive_controller', document.getElementById('cbo_company_name').value, 'load_drop_down_location', 'location_td' );\n"; 		echo "document.getElementById('cbo_location_name').value	= '".$row[csf("location_id")]."';\n";  
		echo "load_drop_down( 'requires/sub_contract_material_receive_controller', document.getElementById('cbo_company_name').value, 'load_drop_down_buyer', 'buyer_td' );\n";
		echo "document.getElementById('cbo_party_name').value		= '".$row[csf("party_id")]."';\n"; 
		echo "document.getElementById('txt_receive_challan').value	= '".$row[csf("chalan_no")]."';\n"; 
		echo "document.getElementById('txt_receive_date').value 	= '".change_date_format($row[csf("subcon_date")])."';\n";  
	    echo "document.getElementById('update_id').value            = '".$row[csf("id")]."';\n";
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_material_receive',1);\n";
	}
	exit();
}


if($action=="subcontract_receive_dtls_list_view")
{	
	$color_arrey=return_library_array( "select id,color_name from lib_color",'id','color_name');
	$order_arr=return_library_array( "select id, order_no from subcon_ord_dtls", "id", "order_no");
	$sql = "select id, order_id, item_category_id, material_description, color_id, gsm, quantity, subcon_uom, subcon_roll, grey_dia, fin_dia, rec_cone from sub_material_dtls where status_active=2 and mst_id='$data'"; 
		
	$arr=array(0=>$order_arr,1=>$item_category,3=>$color_arrey,8=>$unit_of_measurement);
	echo  create_list_view("list_view", "Order No,Item Categ,Material Description,Color,GSM,Grey Dia/Width,Fin. Dia/Width,Receive Qty,UOM,Roll,Cone", "80,100,150,70,60,60,60,70,60,60,60","900","250",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form_dtls'", 1, "order_id,item_category_id,0,color_id,0,0,0,0,subcon_uom,0,0", $arr,"order_id,item_category_id,material_description,color_id,gsm,grey_dia,fin_dia,quantity,subcon_uom,subcon_roll,rec_cone", "requires/sub_contract_material_receive_controller", "", "0,0,0,0,0,0,0,2,0,2,0",'0,0,0,0,0,0,0,0,quantity,0,subcon_roll,0','');
	/*$sql = "select a.id,a.mst_id,a.item_category_id,a.material_description,a.quantity,a.subcon_uom,a.status_active,b.order_no from sub_material_dtls a,subcon_ord_dtls b where a.order_id=b.id and a.status_active=2 and a.mst_id='$data'"; */
	
	//$result = sql_select($sql);
	
	exit();
}

if ($action=="load_php_data_to_form_dtls")
{
	$order_arr=return_library_array( "select id, order_no from subcon_ord_dtls", "id", "order_no");
	$nameArray=sql_select( "select id, item_category_id, material_description, color_id, quantity, subcon_uom, subcon_roll, rec_cone, gsm, grey_dia, fin_dia, dia_uom, order_id, status_active from sub_material_dtls where id='$data'" );
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('cbo_item_category').value		= '".$row[csf("item_category_id")]."';\n"; 
		echo "document.getElementById('txt_material_description').value	= '".$row[csf("material_description")]."';\n";
		echo "document.getElementById('txt_color').value				= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('txt_receive_quantity').value		= '".$row[csf("quantity")]."';\n";  
		echo "document.getElementById('cbo_uom').value		 			= '".$row[csf("subcon_uom")]."';\n";
		echo "document.getElementById('txt_roll').value		 			= '".$row[csf("subcon_roll")]."';\n";
		echo "document.getElementById('txt_cone').value		 			= '".$row[csf("rec_cone")]."';\n";
		echo "document.getElementById('txt_gsm').value		 			= '".$row[csf("gsm")]."';\n";
		echo "document.getElementById('txt_grey_dia').value		 		= '".$row[csf("grey_dia")]."';\n";
		echo "document.getElementById('txt_fin_dia').value		 		= '".$row[csf("fin_dia")]."';\n";
		echo "document.getElementById('cbo_dia_uom').value		 		= '".$row[csf("dia_uom")]."';\n";
		echo "document.getElementById('txt_order_no').value		 		= '".$order_arr[$row[csf("order_id")]]."';\n";  
		echo "document.getElementById('cbo_status').value				= '".$row[csf("status_active")]."';\n"; 
		
		//echo "document.getElementById('cbo_uom').value            	= '".$row[csf("mst_id")]."';\n";
		echo "document.getElementById('update_id2').value            	= '".$row[csf("id")]."';\n";
		echo "document.getElementById('order_no_id').value				= '".$row[csf("order_id")]."';\n"; 
		//echo "get_php_form_data( document.getElementById('update_id2').value+'_'+document.getElementById('order_no_id').value, 'load_php_data_allow_delete', 'requires/sub_contract_material_receive_controller' );\n";
		
		//$response= echo "return_global_ajax_value( document.getElementById('update_id2').value+'_'+document.getElementById('order_no_id').value, 'load_php_data_allow_delete', '', 'requires/sub_contract_material_receive_controller');\n";
		//print_r($response);
		
		$del_sql="select a.batch_no, b.prod_id from  pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and b.po_id='".$row[csf("order_id")]."' and b.prod_id='".$row[csf("id")]."' group by a.batch_no, b.prod_id";
		$result_del_sql=sql_select($del_sql,1);
		if(count($result_del_sql)>0)
		{
			echo "document.getElementById('delete_allowed').value            	= '".'1'."';\n";
			echo "document.getElementById('batch_no').value            	= '".$result_del_sql[0][csf('batch_no')]."';\n";
		}
		else
		{
			echo "document.getElementById('delete_allowed').value            	= '".'0'."';\n";
			echo "document.getElementById('batch_no').value            	= '".''."';\n";
		}

		echo "change_uom('".$row[csf("item_category_id")]."');\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_material_receive',1);\n";	
	}
	exit();	
}

if ($action=="load_php_data_allow_delete")
{
	//echo $data;
	$ex_data=explode('_',$data);
	$del_sql="select prod_id from pro_batch_create_dtls where po_id='$ex_data[1]' and prod_id='$ex_data[0]'";
	$result_del_sql=sql_select($del_sql,1);
	if(count($result_del_sql)>0)
	{
		echo "1"."_".$result_del_sql[0][csf('prod_id')];
	}
	else
	{
		echo "0"."_";
	}
	exit();
}

if ($action=="job_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('selected_order').value=id;
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="searchjobfrm_1"  id="searchjobfrm_1" autocomplete="off">
                <table width="750" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>
                        <tr>
                            <th colspan="6"><? echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" ); ?></th>
                        </tr>
                    	<tr>               	 
                            <th width="140">Company Name</th>
                            <th width="140">Party Name</th>
                            <th width="170">Date Range</th>
                            <th width="100">Search Job</th>
                            <th width="100">Search Order</th>
                            <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>
                        </tr>         
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="selected_order">  
								<?   
									$data=explode("_",$data);
									echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $data[1],"",1 );
                                ?>
                            </td>
                            <td id="buyer_td">
								<? echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[1]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --",$data[3], "",1 );   	 
								?>
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                            </td> 
                            <td >
							<?
								//$sarch_by_arr=array(1=>"Order No",2=>"Job No"); 
								//echo create_drop_down( "cbo_search_by", 100,$sarch_by_arr,"", 1, "-Select Search-", 0,"set_caption(this.value)");
                            ?>
                                 <input type="text" name="txt_search_job" id="txt_search_job" class="text_boxes" style="width:100px" placeholder="Job" />
                            </td>
                            <td align="center" id="search_by_td">
                                <input type="text" name="txt_search_order" id="txt_search_order" class="text_boxes" style="width:100px" placeholder="Order" />
                            </td>
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_job').value+'_'+document.getElementById('txt_search_order').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_job_search_list_view', 'search_div', 'sub_contract_material_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" height="40" valign="middle">
								<? echo load_month_buttons(1);  ?>
                                <input type="hidden" name="hidden_mst_id" id="hidden_mst_id" class="datepicker" style="width:70px">
                            </td>
                        </tr>
                    </tbody>
                </table> 
                <br>
                <div id="search_div"></div>   
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?
	exit();
}

if($action=="create_job_search_list_view")
{	
	$data=explode('_',$data);
	$search_job=str_replace("'","",$data[4]);
	$search_order=trim(str_replace("'","",$data[5]));
	$search_type =$data[6];
	
	if ($data[0]!=0) $company=" and company_id='$data[0]'"; else  $company="";
	if ($data[1]!=0) $buyer=" and party_id='$data[1]'"; else $buyer="";
	
	if($search_type==1)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num='$search_job'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no='$search_order'";
	}
	else if($search_type==4 || $search_type==0)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '%$search_job%'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '%$search_order%'";
	}
	else if($search_type==2)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '$search_job%'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '$search_order%'";
	}
	else if($search_type==3)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '%$search_job'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '%$search_order'";
	}	
	
	if($db_type==0)
	{ 
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2],'yyyy-mm-dd')."' and '".change_date_format($data[3],'yyyy-mm-dd')."'"; else $order_rcv_date ="";
	}
	else
	{
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2], "", "",1)."' and '".change_date_format($data[3], "", "",1)."'"; else $order_rcv_date ="";
	}
	
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$arr=array (3=>$comp);
	if($db_type==0)
	{
		$sql= "select a.id, b.id as ord_id, a.subcon_job, a.job_no_prefix_num, year(a.insert_date)as year, a.company_id, a.location_id, a.party_id, a.status_active, b.id, b.order_no, b.order_rcv_date, b.delivery_date, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $order_rcv_date $company $buyer $search_job_cond $search_order_cond order by a.id DESC";
	}
	else if($db_type==2)
	{
		$sql= "select a.id, b.id as ord_id, a.subcon_job, a.job_no_prefix_num, TO_CHAR(a.insert_date,'YYYY') as year, a.company_id, a.location_id, a.party_id, a.status_active, b.id, b.order_no, b.order_rcv_date, b.delivery_date, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $order_rcv_date $company $buyer $search_job_cond $search_order_cond order by a.id DESC";
	}
	//echo $sql;die;
	echo  create_list_view("list_view", "Job No,Year,Order No,Company,Ord Receive Date,Delivery Date","70,80,150,150,100","750","250",0,$sql, "js_set_value","ord_id","",1,"0,0,0,company_id,0,0",$arr,"job_no_prefix_num,year,order_no,company_id,order_rcv_date,delivery_date", "",'','0,0,0,0,3,3') ;
	exit();
} 

if($action=="load_php_data_to_form_dtls_order")
{
	$nameArray=sql_select( "select id,order_no from subcon_ord_dtls where id='$data'" );
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_order_no').value	= '".$row[csf("order_no")]."';\n"; 
		echo "document.getElementById('order_no_id').value	= '".$row[csf("id")]."';\n"; 
	}
	exit();
}


?>