-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_purchase_rqsn_mst_htry`
--

DROP TABLE IF EXISTS `inv_purchase_rqsn_mst_htry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_purchase_rqsn_mst_htry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) NOT NULL DEFAULT '0',
  `reqsn_mst_id` int(11) NOT NULL DEFAULT '0',
  `requ_no` varchar(20) NOT NULL,
  `requ_no_prefix` varchar(20) NOT NULL,
  `requ_prefix_num` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL,
  `item_category_id` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL DEFAULT '0',
  `division_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `section_id` int(11) NOT NULL DEFAULT '0',
  `requisition_date` date NOT NULL,
  `store_name` int(11) NOT NULL DEFAULT '0',
  `pay_mode` int(11) NOT NULL DEFAULT '0',
  `source` int(11) NOT NULL DEFAULT '0',
  `cbo_currency` int(11) NOT NULL DEFAULT '0',
  `delivery_date` date NOT NULL,
  `remarks` varchar(50) NOT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:pending,1:approved',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `requ_no` (`requ_no`),
  KEY `requ_no_prefix` (`requ_no_prefix`),
  KEY `requ_prefix_num` (`requ_prefix_num`),
  KEY `company_id` (`company_id`),
  KEY `item_category_id` (`item_category_id`),
  KEY `location_id` (`location_id`),
  KEY `division_id` (`division_id`),
  KEY `department_id` (`department_id`),
  KEY `section_id` (`section_id`),
  KEY `requisition_date` (`requisition_date`),
  KEY `store_name` (`store_name`),
  KEY `pay_mode` (`pay_mode`),
  KEY `source` (`source`),
  KEY `remarks` (`remarks`),
  KEY `inserted_by` (`inserted_by`),
  KEY `insert_date` (`insert_date`),
  KEY `updated_by` (`updated_by`),
  KEY `update_date` (`update_date`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_purchase_rqsn_mst_htry`
--

LOCK TABLES `inv_purchase_rqsn_mst_htry` WRITE;
/*!40000 ALTER TABLE `inv_purchase_rqsn_mst_htry` DISABLE KEYS */;
/*!40000 ALTER TABLE `inv_purchase_rqsn_mst_htry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
