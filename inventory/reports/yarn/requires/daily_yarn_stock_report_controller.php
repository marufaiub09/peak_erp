﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------

if ($action=="load_drop_down_store")
{	
	$data=explode("**",$data);
	if($data[1]==2) $disable=1; else $disable=0; 
	echo create_drop_down( "cbo_store_name", 100, "select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id and  a.status_active=1 and a.is_deleted=0 and a.company_id=$data[0] and b.category_type in(1)  order by a.store_name","id,store_name", 1, "-- All Store--", 0, "",$disable );  	 
	exit();
}

//load drop down supplier
if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier", 120, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data' and b.party_type =2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name",0, "-- Select --", 0, "",0 );  	 
	exit();
}

if ($action=="eval_multi_select")
{
 	echo "set_multiselect('cbo_supplier','0','0','','0');\n";
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,short_name from lib_supplier where status_active=1 and is_deleted=0","id","short_name"); 
	$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$buy_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	
	if($db_type==0) 
	{
		$from_date=change_date_format($from_date,'yyyy-mm-dd');
		$to_date=change_date_format($to_date,'yyyy-mm-dd');
	}
	else if($db_type==2) 
	{
		$from_date=change_date_format($from_date,'','',1);
		$to_date=change_date_format($to_date,'','',1);
	}
	else  
	{
		$from_date=""; $to_date="";
	}

	$search_cond="";

	if($cbo_dyed_type==0) $search_cond .= ""; 
	else if($cbo_dyed_type==1) $search_cond .= " and c.receive_purpose=2"; 
	else if($cbo_dyed_type==2) $search_cond .= " and c.receive_purpose!=2";
	
	if($cbo_yarn_type==0) $search_cond .= ""; else $search_cond .= " and a.yarn_type=$cbo_yarn_type";
	if($txt_count=="") $search_cond .= ""; else $search_cond .= " and a.yarn_count_id in($txt_count)";
	if($txt_lot_no=="") $search_cond .= ""; else $search_cond .= " and a.lot='$txt_lot_no'";
	if($value_with==0) $search_cond .=""; else $search_cond .= "  and a.current_stock>0";
	if($cbo_supplier==0) $search_cond .=""; else $search_cond .= "  and a.supplier_id in($cbo_supplier)";
	
	if($show_val_column==1) 
	{
		$value_width=200;
		$span=2;
		$column='<th rowspan="2" width="90">Avg. Rate (Tk)</th><th rowspan="2" width="110">Stock Value</th>';
	}
	else 
	{
		$value_width=0;
		$span=0;
		$column='';
	}
	
	if($store_wise==1) 
	{
		if($store_name==0) $store_cond .=""; else $store_cond .= "  and a.store_id=$store_name";
		$table_width='2100'+$value_width;
		$colspan='29'+$span;
		
		$store_arr=return_library_array("select id, store_name from lib_store_location",'id','store_name');
	}
	else
	{
		$table_width='2200'+$value_width;
		$colspan='30'+$span;
	}
	
	$nameArray=sql_select( "select allocation from variable_settings_inventory where company_name=$cbo_company_name and item_category_id=1 and variable_list=18" );
	$allocated_qty_variable_settings=$nameArray[0][csf('allocation')];
	//$allocated_qty_variable_settings=0;
	
	if($db_type==0) $select_field="group_concat(distinct(a.store_id))";
	else if($db_type==2) $select_field="listagg(a.store_id,',') within group (order by a.store_id)";
	
	$receive_array=array();
	$sql_receive="Select a.prod_id, $select_field as store_id,
		sum(case when a.transaction_type in (1,4) and a.transaction_date<'".$from_date."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type in (1) and c.receive_purpose<>5 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as purchase,
		sum(case when a.transaction_type in (1) and c.receive_purpose=5 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as rcv_loan,
		sum(case when a.transaction_type=4 and c.knitting_source=1 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as rcv_inside_return,
		sum(case when a.transaction_type=4 and c.knitting_source!=1 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as rcv_outside_return 
		from inv_transaction a, inv_receive_master c where a.mst_id=c.id and a.transaction_type in (1,4) and a.company_id=$cbo_company_name and a.item_category=1 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $store_cond group by a.prod_id";
	$result_sql_receive = sql_select($sql_receive);
	foreach($result_sql_receive as $row)
	{
		$receive_array[$row[csf("prod_id")]]['store_id']=$row[csf("store_id")];
		$receive_array[$row[csf("prod_id")]]['rcv_total_opening']=$row[csf("rcv_total_opening")];
		$receive_array[$row[csf("prod_id")]]['purchase']=$row[csf("purchase")];
		$receive_array[$row[csf("prod_id")]]['rcv_loan']=$row[csf("rcv_loan")];
		$receive_array[$row[csf("prod_id")]]['rcv_inside_return']=$row[csf("rcv_inside_return")];
		$receive_array[$row[csf("prod_id")]]['rcv_outside_return']=$row[csf("rcv_outside_return")];
	}
			
	$issue_array=array();
	$sql_issue="select a.prod_id, $select_field as store_id,
		sum(case when a.transaction_type in (2,3) and a.transaction_date<'".$from_date."' then a.cons_quantity else 0 end) as issue_total_opening,
		sum(case when a.transaction_type=2 and c.knit_dye_source=1 and c.issue_purpose<>5 and a.transaction_date  between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as issue_inside,
		sum(case when a.transaction_type=2 and c.knit_dye_source!=1 and c.issue_purpose<>5 and a.transaction_date  between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as issue_outside,
		sum(case when a.transaction_type=3 and c.entry_form=8 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as rcv_return,
		sum(case when a.transaction_type=2 and c.issue_purpose=5 and a.transaction_date between '".$from_date."' and '".$to_date."' then a.cons_quantity else 0 end) as issue_loan		
		from inv_transaction a, inv_issue_master c
		where a.mst_id=c.id and a.item_category=1 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $store_cond group by a.prod_id";
	$result_sql_issue=sql_select($sql_issue);
	foreach($result_sql_issue as $row)
	{
		$issue_array[$row[csf("prod_id")]]['store_id']=$row[csf("store_id")];
		$issue_array[$row[csf("prod_id")]]['issue_total_opening']=$row[csf("issue_total_opening")];
		$issue_array[$row[csf("prod_id")]]['issue_inside']=$row[csf("issue_inside")];
		$issue_array[$row[csf("prod_id")]]['issue_outside']=$row[csf("issue_outside")];
		$issue_array[$row[csf("prod_id")]]['rcv_return']=$row[csf("rcv_return")];
		$issue_array[$row[csf("prod_id")]]['issue_loan']=$row[csf("issue_loan")];
	}
	if($db_type==0)
	{
		$yarn_allo_sql=sql_select("select product_id, group_concat(buyer_id) as buyer_id, group_concat(allocate_qnty) as allocate_qnty from com_balk_yarn_allocate where status_active=1 group by product_id");
		//LISTAGG(CAST( a.lc_sc_id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.lc_sc_id) as lc_id
	}
	else if($db_type==2)
	{
		$yarn_allo_sql=sql_select("select product_id, LISTAGG(CAST(buyer_id as VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY buyer_id) as buyer_id, LISTAGG(CAST(allocate_qnty AS VARCHAR(4000)),',') WITHIN GROUP(ORDER BY allocate_qnty) as allocate_qnty from com_balk_yarn_allocate where status_active=1 group by product_id");
	}
	$yarn_allo_arr=array();
	foreach($yarn_allo_sql as $row)
	{
		$yarn_allo_arr[$row[csf("product_id")]]['product_id']=$row[csf("product_id")];
		$yarn_allo_arr[$row[csf("product_id")]]['buyer_id']=implode(",",array_unique(explode(",",$row[csf("buyer_id")])));
		$yarn_allo_arr[$row[csf("product_id")]]['allocate_qnty']=implode(",",array_unique(explode(",",$row[csf("allocate_qnty")])));
	}
	
	
	if($type==1)
	{
		$date_array=array();
		$returnRes_date="select prod_id, min(transaction_date) as min_date, max(transaction_date) as max_date from inv_transaction where is_deleted=0 and status_active=1 and item_category=1 group by prod_id";
		$result_returnRes_date = sql_select($returnRes_date);
		foreach($result_returnRes_date as $row)	
		{
			$date_array[$row[csf("prod_id")]]['min_date']=$row[csf("min_date")];
			$date_array[$row[csf("prod_id")]]['max_date']=$row[csf("max_date")];
		}
		
		//var_dump($date_array);
		if($cbo_dyed_type==0)
		{	
			$sql="select a.id, a.supplier_id, a.yarn_count_id, a.yarn_comp_type1st, a.yarn_comp_percent1st, a.yarn_comp_type2nd, a.yarn_comp_percent2nd, a.yarn_type, a.color, a.lot, a.allocated_qnty, a.available_qnty, a.avg_rate_per_unit from product_details_master a
		where a.item_category_id=1 and a.status_active=1 and a.is_deleted=0 $search_cond group by a.id, a.supplier_id, a.yarn_count_id, a.yarn_comp_type1st, a.yarn_comp_percent1st, a.yarn_comp_type2nd, a.yarn_comp_percent2nd, a.yarn_type, a.color, a.lot, a.allocated_qnty, a.available_qnty, a.avg_rate_per_unit order by a.yarn_count_id, a.yarn_comp_type1st, a.yarn_comp_percent1st, a.yarn_comp_type2nd, a.yarn_comp_percent2nd, a.yarn_type, a.id"; 
		}
		else
		{
			$sql="select a.id, a.supplier_id, a.yarn_count_id, a.yarn_comp_type1st, a.yarn_comp_percent1st, a.yarn_comp_type2nd, a.yarn_comp_percent2nd, a.yarn_type, a.color, a.lot, a.allocated_qnty, a.available_qnty, a.avg_rate_per_unit from product_details_master a, inv_transaction b, inv_receive_master c
		where a.id=b.prod_id and b.mst_id=c.id and a.item_category_id=1 and a.status_active=1 and a.is_deleted=0 and a.status_active=1 and a.is_deleted=0 and c.entry_form=1 and b.transaction_type in(1,4) $search_cond group by a.id, a.supplier_id, a.yarn_count_id, a.yarn_comp_type1st, a.yarn_comp_percent1st, a.yarn_comp_type2nd, a.yarn_comp_percent2nd, a.yarn_type, a.color, a.lot, a.allocated_qnty, a.available_qnty, a.avg_rate_per_unit order by a.yarn_count_id, a.yarn_comp_type1st, a.yarn_comp_percent1st, a.yarn_comp_type2nd, a.yarn_comp_percent2nd,a.yarn_type,a.id"; 	
		}
		//echo $sql;//die;//echo count($result);
		$result = sql_select($sql);	
		$i=1;
		ob_start();	
		?>
		<div> 
            <table width="<?php echo $table_width; ?>" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
                <thead>
                    <tr class="form_caption" style="border:none;">
                        <td colspan="<?php echo $colspan; ?>" align="center" style="border:none;font-size:16px; font-weight:bold" >Daily Yarn Stock </td> 
                    </tr>
                    <tr style="border:none;">
                        <td colspan="<?php echo $colspan; ?>" align="center" style="border:none; font-size:14px;">
                            Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?>                                
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td colspan="<?php echo $colspan; ?>" align="center" style="border:none;font-size:12px; font-weight:bold">
                            <?php if($from_date!="" && $to_date!="") echo "From ".change_date_format($from_date,'dd-mm-yyyy')." To ".change_date_format($to_date,'dd-mm-yyyy')."" ;?>
                        </td>
                    </tr>
                    <tr>
                        <th rowspan="2" width="30">SL</th>
                        <th colspan="7">Description</th>
                        <th rowspan="2" width="110">Opening Stock</th>
                        <th colspan="5">Receive</th>
                        <th colspan="5">Delivery</th>
                        <th rowspan="2" width="100">Stock In Hand</th>
                        <?php 
							echo $column;
							if($store_wise==1)
							{
								echo '<th rowspan="2" width="100">Store Name</th>';
							}
							else
							{
								echo '<th rowspan="2" width="100">Allocated to Order</th>';
								echo '<th rowspan="2" width="100">Un Allocated Qty.</th>';
							}
						?>
                        <th rowspan="2" width="50">Age (Days)</th>
                        <th rowspan="2" width="50">DOH</th>
                        <th rowspan="2">Remarks</th>
                    </tr> 
                    <tr>                         
                        <th width="60">Prod.ID</th>
                        <th width="60">Count</th>
                        <th width="100">Composition</th>
                        <th width="100">Yarn Type</th>
                        <th width="80">Color</th>
                        <th width="100">Lot</th>
                        <th width="80">Supplier</th>
                        <th width="90">Purchase</th>
                        <th width="90">Inside Return</th> 
                        <th width="90">Outside Return</th>
                        <th width="90">Loan</th>
                        <th width="100">Total Receved</th>
                        <th width="90">Inside</th>
                        <th width="90">Outside</th>
                        <th width="90">Receive Return</th>
                        <th width="90">Loan</th>
                        <th width="100">Total Delivery</th> 
                    </tr> 
                </thead>
			</table>  
			<div style="width:<?php echo $table_width+20; ?>px; overflow-y:scroll; max-height:350px" id="scroll_body" > 
				<table width="<?php echo $table_width; ?>" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body">   
					<?php
                    $tot_stock_value=0;
                    foreach($result as $row)
                    {
                        $ageOfDays = datediff("d",$date_array[$row[csf("id")]]['min_date'],date("Y-m-d"));
                        $daysOnHand = datediff("d",$date_array[$row[csf("id")]]['max_date'],date("Y-m-d"));
                        
                        $compositionDetails = $composition[$row[csf("yarn_comp_type1st")]]." ".$row[csf("yarn_comp_percent1st")]."%\n";
                        if($row[csf("yarn_comp_type2nd")]!=0) $compositionDetails.=$composition[$row[csf("yarn_comp_type2nd")]]." ".$row[csf("yarn_comp_percent2nd")]."%";
                        
                        $openingBalance = $receive_array[$row[csf("id")]]['rcv_total_opening']-$issue_array[$row[csf("id")]]['issue_total_opening'];
                        
                        $totalRcv = $receive_array[$row[csf("id")]]['purchase']+$receive_array[$row[csf("id")]]['rcv_inside_return']+$receive_array[$row[csf("id")]]['rcv_outside_return']+$receive_array[$row[csf("id")]]['rcv_loan'];
                        $totalIssue = $issue_array[$row[csf("id")]]['issue_inside']+$issue_array[$row[csf("id")]]['issue_outside']+$issue_array[$row[csf("id")]]['rcv_return']+$issue_array[$row[csf("id")]]['issue_loan'];
                        $stockInHand=$openingBalance+$totalRcv-$totalIssue;
                         
                        //subtotal and group-----------------------
                        $check_string=$row[csf("yarn_count_id")].$compositionDetails.$row[csf("yarn_type")];
                        
                        if((($get_upto==1 && $ageOfDays>$txt_days) || ($get_upto==2 && $ageOfDays<$txt_days) || ($get_upto==3 && $ageOfDays>=$txt_days) || ($get_upto==4 && $ageOfDays<=$txt_days) || ($get_upto==5 && $ageOfDays==$txt_days) || $get_upto==0) && (($get_upto_qnty==1 && $stockInHand>$txt_qnty) || ($get_upto_qnty==2 && $stockInHand<$txt_qnty) || ($get_upto_qnty==3 && $stockInHand>=$txt_qnty) || ($get_upto_qnty==4 && $stockInHand<=$txt_qnty) || ($get_upto_qnty==5 && $stockInHand==$txt_qnty) || $get_upto_qnty==0))
                        {
                            if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
                                                
                            if( !in_array($check_string,$checkArr) )
                            {
                                $checkArr[$i]=$check_string;
                                if($i>1)
                                {
                                ?>
                                    <tr bgcolor="#CCCCCC" style="font-weight:bold">
                                        <td colspan="8" align="right">Sub Total</td>
                                        <td width="110" align="right"><?php echo number_format($total_opening_balance,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_purchase,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_inside_return,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_outside_return,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_rcv_loan,2); ?></td>
                                        <td width="100" align="right"><?php echo number_format($total_total_rcv,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_issue_inside,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_issue_outside,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_receive_return,2); ?></td>
                                        <td width="90" align="right"><?php echo number_format($total_issue_loan,2); ?></td>
                                        <td width="100" align="right"><?php echo number_format($total_total_delivery,2); ?></td> 
                                        <td width="100" align="right"><?php echo number_format($total_stock_in_hand,2); ?></td>
                                        <?php
                                            if($show_val_column==1) 
                                            {
                                                echo '<td width="90" align="right">&nbsp;</td>';
                                                echo '<td width="110" align="right">'.number_format($sub_stock_value,2).'</td>';
                                            }
                                            if($store_wise==1) 
                                            {
                                                echo '<td width="100">&nbsp;</td>';
                                            }
                                            else
                                            {
                                                if($allocated_qty_variable_settings==1) 
                                                {
                                                    echo '<td width="100" align="right">'.number_format($total_alocatted,2).'</td>';
                                                    echo '<td width="100" align="right">'.number_format($total_free_stock,2).'</td>';
                                                }
                                                else 
                                                {
                                                    echo '<td width="100" align="right">&nbsp;</td>';
                                                    echo '<td width="100" align="right">&nbsp;</td>';
                                                }
                                            } 
                                        ?> 
                                        <td width="50" align="right">&nbsp;</td> 
                                        <td width="50" align="right">&nbsp;</td> 
                                        <td width="" align="right"></td> 
                                    </tr>
                                <?php
                                    $total_opening_balance=0;
                                    $total_purchase=0;
                                    $total_inside_return=0;
                                    $total_outside_return=0;
                                    $total_rcv_loan=0;
                                    $total_total_rcv=0;
                                    $total_issue_inside=0;
                                    $total_issue_outside=0;
                                    $total_receive_return=0;
                                    $total_issue_loan=0;
                                    $total_total_delivery=0;
                                    $total_stock_in_hand=0;
                                    $total_alocatted=0;
                                    $total_free_stock=0;
                                    $sub_stock_value=0;
                                }
                            }
            
                            ?>                                 
                            <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                                <td width="30"><?php echo $i; ?></td>								
                                <td width="60"><?php echo $row[csf("id")]; ?></td>                                 
                                <td width="60"><p><?php echo $yarn_count_arr[$row[csf("yarn_count_id")]]; ?></p></td>
                                <td width="100"><p><?php echo $compositionDetails; ?></p></td>
                                <td width="100"><p><?php echo $yarn_type[$row[csf("yarn_type")]]; ?></p></td> 
                                <td width="80"><p><?php echo $color_name_arr[$row[csf("color")]]; ?></p></td> 
                                <td width="100"><p><?php echo $row[csf("lot")]; ?></p></td> 
                                <td width="80"><p><?php echo $supplierArr[$row[csf("supplier_id")]]; ?></p></td>  
                                <td width="110" align="right"><p><?php echo number_format($openingBalance,2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($receive_array[$row[csf("id")]]['purchase'],2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($receive_array[$row[csf("id")]]['rcv_inside_return'],2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($receive_array[$row[csf("id")]]['rcv_outside_return'],2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($receive_array[$row[csf("id")]]['rcv_loan'],2); ?></p></td>
                                <td width="100" align="right"><p><?php echo number_format($totalRcv,2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($issue_array[$row[csf("id")]]['issue_inside'],2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($issue_array[$row[csf("id")]]['issue_outside'],2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($issue_array[$row[csf("id")]]['rcv_return'],2); ?></p></td>
                                <td width="90" align="right"><p><?php echo number_format($issue_array[$row[csf("id")]]['issue_loan'],2); ?></p></td>                                        
                                <td width="100" align="right"><p><?php echo number_format($totalIssue,2); ?></p></td>
                                <td width="100" align="right"><?php echo number_format($stockInHand,2); ?></td> 
                                <?php
                                    $stock_value=0;
                                    if($show_val_column==1) 
                                    {
                                        $stock_value=$stockInHand*$row[csf("avg_rate_per_unit")];
                                        echo '<td width="90" align="right">'.number_format($row[csf("avg_rate_per_unit")],2).'</td>';
                                        echo '<td width="110" align="right">'.number_format($stock_value,2).'</td>';
                                    }
                                    
                                    if($store_wise==1) 
                                    {
                                        $store_name='';
                                        $receive_store_id=explode(",",$receive_array[$row[csf("id")]]['store_id']);
                                        $issue_store_id=explode(",",$issue_array[$row[csf("id")]]['store_id']);
                                        $store_id=array_unique(array_merge($receive_store_id,$issue_store_id));
                                        foreach($store_id as $val)
                                        {
                                            if($store_name=="") $store_name=$store_arr[$val]; else $store_name.=", ".$store_arr[$val];
                                        }
                                        echo '<td width="100"><p>'.$store_name.'</p></td>';
                                    }
                                    else
                                    {
                                        if($allocated_qty_variable_settings==1) 
                                        {
                                            echo "<td width='100' align='right'><a href='##' onclick=\"openmypage('".$row[csf('id')]."','allocation_popup')\">".number_format($row[csf("allocated_qnty")],2)."</a></td>";
                                            echo '<td width="100" align="right">'.number_format($row[csf("available_qnty")],2).'</td>';
                                        }
                                        else 
                                        {
                                            echo '<td width="100" align="right">&nbsp;</td>';
                                            echo '<td width="100" align="right">&nbsp;</td>';
                                        }
                                    }
                                ?>  
                                <td width="50" align="right"><?php echo $ageOfDays; //$ageOfDays; ?></td> 
                                <td width="50" align="right"><?php echo $daysOnHand; //$daysOnHand; ?></td>
                                <td width="" align="right"><p>
								<?php 
								$buyer_id_arr=explode(",",$yarn_allo_arr[$row[csf("id")]]['buyer_id']);
								$allocate_qnty_arr=explode(",",$yarn_allo_arr[$row[csf("id")]]['allocate_qnty']);
								$buyer_all="";$m=0;
								foreach($buyer_id_arr as $buy_id)
								{
									if($buyer_all!="") $buyer_all.="<br>";
									$buyer_all .= $buy_short_name_arr[$buy_id];
									if($buyer_all!="") $buyer_all.="&nbsp;Qnty: ".$allocate_qnty_arr[$m];
									$m++;
								}
								echo $buyer_all;
								?></p></td>
                            </tr>
                            <?php 												
                            $i++; 
                            
                            $total_opening_balance+=$openingBalance;
                            $total_purchase+=$receive_array[$row[csf("id")]]['purchase'];
                            $total_inside_return+=$receive_array[$row[csf("id")]]['rcv_inside_return'];
                            $total_outside_return+=$receive_array[$row[csf("id")]]['rcv_outside_return'];
                            $total_rcv_loan+=$receive_array[$row[csf("id")]]['rcv_loan'];
                            $total_total_rcv+=$totalRcv;
                            $total_issue_inside+=$issue_array[$row[csf("id")]]['issue_inside'];
                            $total_issue_outside+=$issue_array[$row[csf("id")]]['issue_outside'];
                            $total_receive_return+=$issue_array[$row[csf("id")]]['rcv_return'];
                            $total_issue_loan+=$issue_array[$row[csf("id")]]['issue_loan'];
                            $total_total_delivery+=$totalIssue;
                            $total_stock_in_hand+=$stockInHand;
                            $total_alocatted+=$row[csf("allocated_qnty")];
                            $total_free_stock+=$row[csf("available_qnty")];
                            $sub_stock_value+=$stock_value;
                            
                            //grand total===========================
                            $grand_total_opening_balance+=$openingBalance;
                            $grand_total_purchase+=$receive_array[$row[csf("id")]]['purchase'];
                            $grand_total_inside_return+=$receive_array[$row[csf("id")]]['rcv_inside_return'];
                            $grand_total_outside_return+=$receive_array[$row[csf("id")]]['rcv_outside_return'];
                            $grand_total_rcv_loan+=$receive_array[$row[csf("id")]]['rcv_loan'];
                            $grand_total_total_rcv+=$totalRcv;
                            $grand_total_issue_inside+=$issue_array[$row[csf("id")]]['issue_inside'];
                            $grand_total_issue_outside+=$issue_array[$row[csf("id")]]['issue_outside'];
                            $grand_total_receive_return+=$issue_array[$row[csf("id")]]['rcv_return'];
                            $grand_total_issue_loan+=$issue_array[$row[csf("id")]]['issue_loan'];
                            $grand_total_total_delivery+=$totalIssue;
                            $grand_total_stock_in_hand+=$stockInHand;
                            $grand_total_alocatted+=$row[csf("allocated_qnty")];
                            $grand_total_free_stock+=$row[csf("available_qnty")];
                            $tot_stock_value+=$stock_value;
                        }
                    }
                    ?> 
                    <tr bgcolor="#CCCCCC" style="font-weight:bold">
                        <td colspan="8" align="right">Sub Total</td>
                        <td width="110" align="right"><?php echo number_format($total_opening_balance,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_purchase,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_inside_return,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_outside_return,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_rcv_loan,2); ?></td>
                        <td width="100" align="right"><?php echo number_format($total_total_rcv,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_issue_inside,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_issue_outside,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_receive_return,2); ?></td>
                        <td width="90" align="right"><?php echo number_format($total_issue_loan,2); ?></td>
                        <td width="100" align="right"><?php echo number_format($total_total_delivery,2); ?></td>
                        <td width="100" align="right"><?php echo number_format($total_stock_in_hand,2); ?></td>
                        <?php 
                            if($show_val_column==1) 
                            {
                                echo '<td width="90" align="right">&nbsp;</td>';
                                echo '<td width="110" align="right">'.number_format($sub_stock_value,2).'</td>';
                            }
                            
                            if($store_wise==1) 
                            {
                                echo '<td width="100">&nbsp;</td>';
                            }
                            else
                            {
                                if($allocated_qty_variable_settings==1) 
                                {
                                    echo '<td width="100" align="right">'.number_format($total_alocatted,2).'</td>';
                                    echo '<td width="100" align="right">'.number_format($total_free_stock,2).'</td>';
                                }
                                else 
                                {
                                    echo '<td width="100" align="right">&nbsp;</td>';
                                    echo '<td width="100" align="right">&nbsp;</td>';
                                }
                            } 
                        ?> 
                        <td width="50" align="right">&nbsp;</td> 
                        <td width="50" align="right">&nbsp;</td>
                        <td width="" align="right">&nbsp;</td>  
                    </tr>
                </table>  
            </div>  
            <table width="<?php echo $table_width; ?>" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_footer">  
                <tr class="tbl_bottom">
                    <td width="30"></td>
                    <td width="60"></td>
                    <td width="60"></td>
                    <td width="100"></td>
                    <td width="100"></td>
                    <td width="80"></td>
                    <td width="100"></td>
                    <td width="80" align="right">Grand Total</td>
                    <td width="110" align="right" id="value_total_opening_balance"><?php echo number_format($grand_total_opening_balance,2); ?></td>
                    <td width="90" align="right" id="value_total_purchase"><?php echo number_format($grand_total_purchase,2); ?></td>
                    <td width="90" align="right" id="value_total_inside_return"><?php echo number_format($grand_total_inside_return,2); ?></td>
                    <td width="90" align="right" id="value_total_outside_return"><?php echo number_format($grand_total_outside_return,2); ?></td>
                    <td width="90" align="right" id="value_total_rcv_loan"><?php echo number_format($grand_total_rcv_loan,2); ?></td>
                    <td width="100" align="right" id="value_total_total_rcv"><?php echo number_format($grand_total_total_rcv,2); ?></td>
                    <td width="90" align="right" id="value_total_issue_inside"><?php echo number_format($grand_total_issue_inside,2); ?></td>
                    <td width="90" align="right" id="value_total_issue_outside"><?php echo number_format($grand_total_issue_outside,2); ?></td>
                    <td width="90" align="right" id="value_total_receive_return"><?php echo number_format($grand_total_receive_return,2); ?></td>
                    <td width="90" align="right" id="value_total_issue_loan"><?php echo number_format($grand_total_issue_loan,2); ?></td>
                    <td width="100" align="right" id="value_total_total_delivery"><?php echo number_format($grand_total_total_delivery,2); ?></td>
                    <td width="100" align="right" id="value_total_stock_in_hand"><?php echo number_format($grand_total_stock_in_hand,2); ?></td>
                    <?php 
                        if($show_val_column==1) 
                        {
                            echo '<td width="90" align="right">&nbsp;</td>';
                            echo '<td width="110" align="right">'.number_format($tot_stock_value,2).'</td>';
                        }
                        
                        if($store_wise==1) 
                        {
                            echo '<td width="100">&nbsp;</td>';
                        }
                        else
                        {
                            if($allocated_qty_variable_settings==1) 
                            {
                                echo '<td width="100" align="right" id="value_total_alocatted">'.number_format($grand_total_alocatted,2).'</td>';
                                echo '<td width="100" align="right" id="value_total_free_stock">'.number_format($grand_total_free_stock,2).'</td>';
                            }
                            else 
                            {
                                echo '<td width="100" align="right" id="value_total_alocatted">&nbsp;</td>';
                                echo '<td width="100" align="right" id="value_total_free_stock">&nbsp;</td>';
                                
                            }
                        } 
                    ?>  
                    <td width="50" align="right">&nbsp;</td> 
                    <td width="50" align="right">&nbsp;</td>
                    <td width="" align="right">&nbsp;</td>  
                </tr>
            </table>
		</div>    
	<?php	 
	}
	else if($type==2)
	{
		$count_arr=array();
		if($cbo_dyed_type==0)
		{	
			$sql="select a.id, a.yarn_count_id, a.yarn_type from product_details_master a where a.item_category_id=1 and a.status_active=1 and a.is_deleted=0 $search_cond group by a.id, a.yarn_count_id, a.yarn_type order by a.yarn_count_id, a.yarn_type"; 
		}
		else
		{
			$sql="select a.id, a.yarn_count_id, a.yarn_type from product_details_master a, inv_transaction b, inv_receive_master c where a.id=b.prod_id and b.mst_id=c.id and a.item_category_id=1 and a.status_active=1 and a.is_deleted=0 and a.status_active=1 and a.is_deleted=0 and c.entry_form=1 and b.transaction_type in(1,4) $search_cond group by a.id, a.yarn_count_id, a.yarn_type order by a.yarn_count_id, a.yarn_type"; 	
		}	
		//echo $sql;die;
		$result = sql_select($sql);	
		foreach($result as $row)
		{
			$openingBalance = $receive_array[$row[csf("id")]]['rcv_total_opening']-$issue_array[$row[csf("id")]]['issue_total_opening'];
			$totalRcv = $receive_array[$row[csf("id")]]['purchase']+$receive_array[$row[csf("id")]]['rcv_inside_return']+$receive_array[$row[csf("id")]]['rcv_outside_return']+$receive_array[$row[csf("id")]]['rcv_loan'];
			$totalIssue = $issue_array[$row[csf("id")]]['issue_inside']+$issue_array[$row[csf("id")]]['issue_outside']+$issue_array[$row[csf("id")]]['rcv_return']+$issue_array[$row[csf("id")]]['issue_loan'];
			$stockInHand=$openingBalance+$totalRcv-$totalIssue;
			$count_arr[$row[csf("yarn_count_id")]][$row[csf("yarn_type")]]+=$stockInHand;
		}
		$i=1;
		ob_start();
		?>
		<div style="margin-top:5px"> 
            <table width="702" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
                <thead>
                    <tr class="form_caption" style="border:none;">
                        <td colspan="4" align="center" style="border:none;font-size:16px; font-weight:bold">Count Wise Yarn Stock Summary Report</td> 
                    </tr>
                    <tr style="border:none;">
                        <td colspan="4" align="center" style="border:none; font-size:14px;">
                            Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?>                                
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td colspan="4" align="center" style="border:none;font-size:12px; font-weight:bold">
                            <?php if($from_date!="" && $to_date!="") echo "From ".change_date_format($from_date,'dd-mm-yyyy')." To ".change_date_format($to_date,'dd-mm-yyyy')."" ;?>
                        </td>
                    </tr>
                    <tr>
                        <th width="70">SL</th>
                        <th width="150">Count</th>
                        <th width="200">Type</th>
                        <th>Stock In Hand</th>
                    </tr> 
                </thead>
			</table> 
			<div style="width:720px; overflow-y:scroll; max-height:350px" id="scroll_body" > 
				<table width="702" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_body">   
				<?php
                    $tot_stock_qty=0;
                    foreach($count_arr as $count=>$value)
                    {
						foreach($value as $type=>$stock_qnty)
						{
							if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
							if( !in_array($count,$checkArr))
							{
								$checkArr[$i]=$count;
								if($i>1)
								{
								?>
									<tr bgcolor="#CCCCCC" style="font-weight:bold">
										<td colspan="3" align="right">Sub Total</td>
										<td align="right"><?php echo number_format($count_tot_qnty,2); ?></td>
									</tr>
								<?php
									$count_tot_qnty=0;
								}
							}
			
							?>                                 
							<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="70"><?php echo $i; ?></td>								
								<td width="150"><?php echo $yarn_count_arr[$count]; ?></td>                                 
								<td width="200"><?php echo $yarn_type[$type]; ?></td>
								<td align="right"><?php echo number_format($stock_qnty,2); ?></td> 
							</tr>
							<?php 												
							$i++; 
							
							$count_tot_qnty+=$stock_qnty;
							$tot_stock_qty+=$stock_qnty;
						}
                    }
                    ?> 
                    <tr bgcolor="#CCCCCC" style="font-weight:bold">
                        <td colspan="3" align="right">Sub Total</td>
                        <td align="right"><?php echo number_format($count_tot_qnty,2); ?></td>
                    </tr>
                </table> 
			</div>
            <table width="720" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_footer">   
               <tr class="tbl_bottom">
                    <td width="70">&nbsp;</td>
                    <td width="150">&nbsp;</td>
                    <td width="200">Total</td>
                    <td align="right" style="padding-right:18px"><?php echo number_format($tot_stock_qty,2); ?></td>
                </tr>
            </table>
		</div>     
    <?php        
	}
	else
	{
		$type_arr=array();
		if($cbo_dyed_type==0)
		{	
			$sql="select a.id, a.yarn_type, a.yarn_count_id from product_details_master a where a.item_category_id=1 and a.status_active=1 and a.is_deleted=0 $search_cond group by a.id, a.yarn_type, a.yarn_count_id order by a.yarn_type, a.yarn_count_id"; 
		}
		else
		{
			$sql="select a.id, a.yarn_type, a.yarn_count_id from product_details_master a, inv_transaction b, inv_receive_master c where a.id=b.prod_id and b.mst_id=c.id and a.item_category_id=1 and a.status_active=1 and a.is_deleted=0 and a.status_active=1 and a.is_deleted=0 and c.entry_form=1 and b.transaction_type in(1,4) $search_cond group by a.id, a.yarn_type, a.yarn_count_id order by a.yarn_type, a.yarn_count_id"; 	
		}	
		//echo $sql;die;
		$result = sql_select($sql);	
		foreach($result as $row)
		{
			$openingBalance = $receive_array[$row[csf("id")]]['rcv_total_opening']-$issue_array[$row[csf("id")]]['issue_total_opening'];
			$totalRcv = $receive_array[$row[csf("id")]]['purchase']+$receive_array[$row[csf("id")]]['rcv_inside_return']+$receive_array[$row[csf("id")]]['rcv_outside_return']+$receive_array[$row[csf("id")]]['rcv_loan'];
			$totalIssue = $issue_array[$row[csf("id")]]['issue_inside']+$issue_array[$row[csf("id")]]['issue_outside']+$issue_array[$row[csf("id")]]['rcv_return']+$issue_array[$row[csf("id")]]['issue_loan'];
			$stockInHand=$openingBalance+$totalRcv-$totalIssue;
			$type_arr[$row[csf("yarn_type")]][$row[csf("yarn_count_id")]]+=$stockInHand;
		}
		$i=1;
		ob_start();
		?>
		<div style="margin-top:5px"> 
            <table width="702" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
                <thead>
                    <tr class="form_caption" style="border:none;">
                        <td colspan="4" align="center" style="border:none;font-size:16px; font-weight:bold">Count Wise Yarn Stock Summary Report</td> 
                    </tr>
                    <tr style="border:none;">
                        <td colspan="4" align="center" style="border:none; font-size:14px;">
                            Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?>                                
                        </td>
                    </tr>
                    <tr style="border:none;">
                        <td colspan="4" align="center" style="border:none;font-size:12px; font-weight:bold">
                            <?php if($from_date!="" && $to_date!="") echo "From ".change_date_format($from_date,'dd-mm-yyyy')." To ".change_date_format($to_date,'dd-mm-yyyy')."" ;?>
                        </td>
                    </tr>
                    <tr>
                        <th width="70">SL</th>
                        <th width="200">Type</th>
                        <th width="150">Count</th>
                        <th>Stock In Hand</th>
                    </tr> 
                </thead>
			</table> 
			<div style="width:720px; overflow-y:scroll; max-height:350px" id="scroll_body" > 
				<table width="702" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_body">   
				<?php
                    $tot_stock_qty=0;
                    foreach($type_arr as $type=>$value)
                    {
						foreach($value as $count=>$stock_qnty)
						{
							if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
							if( !in_array($type,$checkArr))
							{
								$checkArr[$i]=$type;
								if($i>1)
								{
								?>
									<tr bgcolor="#CCCCCC" style="font-weight:bold">
										<td colspan="3" align="right">Sub Total</td>
										<td align="right"><?php echo number_format($count_tot_qnty,2); ?></td>
									</tr>
								<?php
									$count_tot_qnty=0;
								}
							}
			
							?>                                 
							<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="70"><?php echo $i; ?></td>	
                                <td width="200"><?php echo $yarn_type[$type]; ?></td>							
								<td width="150"><?php echo $yarn_count_arr[$count]; ?></td>                                 
								<td align="right"><?php echo number_format($stock_qnty,2); ?></td> 
							</tr>
							<?php 												
							$i++; 
							
							$count_tot_qnty+=$stock_qnty;
							$tot_stock_qty+=$stock_qnty;
						}
                    }
                    ?> 
                    <tr bgcolor="#CCCCCC" style="font-weight:bold">
                        <td colspan="3" align="right">Sub Total</td>
                        <td align="right"><?php echo number_format($count_tot_qnty,2); ?></td>
                    </tr>
                </table> 
			</div>
            <table width="720" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_footer">   
               <tr class="tbl_bottom">
                    <td width="70">&nbsp;</td>
                    <td width="150">&nbsp;</td>
                    <td width="200">Total</td>
                    <td align="right" style="padding-right:18px"><?php echo number_format($tot_stock_qty,2); ?></td>
                </tr>
            </table>
		</div>     
    <?php        
	}
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}

if($action=="allocation_popup")
{
	echo load_html_head_contents("Allocation Statement", "../../../../", 1, 1,$unicode,'','');
 	extract($_REQUEST);
	//echo $prod_id;
	$issue_array=array();
	$sql_issue="select c.po_breakdown_id, sum(c.quantity) as issue_qty
	from inv_issue_master a, inv_transaction b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.trans_id and a.issue_purpose in(1,2) and c.trans_type=2 and a.entry_form=3 and b.item_category=1 and c.prod_id=$prod_id group by c.po_breakdown_id";
	$result_issue = sql_select($sql_issue);	
	foreach($result_issue as $row)
	{
		$issue_array[$row[csf("po_breakdown_id")]]=$row[csf("issue_qty")];
	}
	
	$issue_return_array=array();
	$sql_return="Select c.po_breakdown_id, sum(c.quantity) as issue_return_qty from inv_receive_master a, inv_transaction b, order_wise_pro_details c where a.id=b.mst_id and c.trans_type=4 and a.entry_form=9 and b.id=c.trans_id and b.item_category=1 and c.issue_purpose in(1,2) and c.prod_id=$prod_id group by c.po_breakdown_id";
	$result_return = sql_select($sql_return);	
	foreach($result_return as $row)
	{
		$issue_return_array[$row[csf("po_breakdown_id")]]=$row[csf("issue_return_qty")];
	}
	//var_dump($issue_array);
	$sql_allocation="select item_id, allocation_date, job_no, po_break_down_id, booking_no, qnty as allocate_qty from inv_material_allocation_dtls where status_active=1 and is_deleted=0 and item_id='$prod_id'";
	?>
	<div align="center">
        <table width="870" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all">
            <thead>
                <th width="30">SL</th>
                <th width="75">Date</th>
                <th width="60">Ref. No.</th>
                <th width="110">Job No.</th>
                <th width="110">Order No.</th>
                <th width="110">Booking No.</th>
                <th width="80">Allocated Qty</th>
                <th width="80">Issue Qty</th>
                <th width="80">Issue Rtn Qty</th>
                <th width="">Cumul. Balance</th>
            </thead>
        </table>
        <div style="width:870px; overflow-y:scroll; max-height:300px" id="scroll_body">	
            <table width="852" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body">
            <?php
            $po_number_arr=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
            $i=1;
            $result_allocation = sql_select($sql_allocation);
            $balance='';
            foreach($result_allocation as $row)
            {
                if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
                $issue_qty=$issue_array[$row[csf("po_break_down_id")]];
                $return_qty=$issue_return_array[$row[csf("po_break_down_id")]];
                ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                    <td width="30"><?php echo $i; ?></td>
                    <td width="75" align="center"><?php echo change_date_format($row[csf("allocation_date")]); ?>&nbsp;</td>
                    <td width="60"><?php //echo $row[csf("job_no")]; ?>&nbsp;</td>
                    <td width="110"><p><?php echo $row[csf("job_no")]; ?></p></td>
                    <td width="110"><p><?php echo $po_number_arr[$row[csf("po_break_down_id")]]; ?></p></td>
                    <td width="110"><p><?php echo $row[csf("booking_no")]; ?></p></td>
                    <td width="80" align="right"><?php echo number_format($row[csf("allocate_qty")],2); ?>&nbsp;</td>
                    <td width="80" align="right"><?php echo number_format($issue_qty,2); ?>&nbsp;</td>
                    <td width="80" align="right"><?php echo number_format($return_qty,2); ?>&nbsp;</td>
                    <td align="right"><?php $balance=$balance+$row[csf("allocate_qty")]-$issue_qty+$return_qty;  echo $balance; ?>&nbsp;</td>
                </tr>
                <?php
                $i++;
            }
            ?>
            </table>
        </div>
    </div>
<?php
	exit();
}
?>
