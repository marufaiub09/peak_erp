<? 
include('../../../../includes/common.php');
session_start();
extract($_REQUEST);
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$date=date('Y-m-d');

$buyer_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$company_short_name_arr=return_library_array( "select id,company_short_name from lib_company",'id','company_short_name');
//$company_team_name_arr=return_library_array( "select id,team_name from lib_marketing_team",'id','team_name');
//$company_team_member_name_arr=return_library_array( "select id,team_member_name from  lib_mkt_team_member_info",'id','team_member_name');
$imge_arr=return_library_array( "select master_tble_id,image_location from common_photo_library",'master_tble_id','image_location');
//$cm_for_shipment_schedule_arr=return_library_array( "select job_no,cm_for_sipment_sche from  wo_pre_cost_dtls",'job_no','cm_for_sipment_sche');

if ($action=="load_drop_down_buyer")
{ 
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
	exit();   	 
} 

if($action=="report_generate")
{ 
	$txt_search_string=str_replace("'","",$txt_search_string);
	
	if(trim($txt_search_string)!="") $search_string="%".trim($txt_search_string)."%"; else $search_string="%%";
	
	if(str_replace("'","",trim($txt_date_from))=="" && str_replace("'","",trim($txt_date_to))=="")$date_cond="";
	else $date_cond=" and b.pub_shipment_date between $txt_date_from and $txt_date_to";

/* 	if ($txt_date_from!="" && $txt_date_to!="")
	{
		$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd");
		$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd");
 		$date_cond=" and b.pub_shipment_date between '$start_date' and  '$end_date'";
	}
 	else	
 		$date_cond="";
*/	
	$searchCond="";	
	if(str_replace("'","",$cbo_company_name)!=0) $searchCond.=" and a.company_name=$cbo_company_name";
	
	//if(str_replace("'","",$cbo_buyer_name)!=0) $searchCond.=" and a.buyer_name=$cbo_buyer_name";	
 	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $searchCond.=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")";
		}
	}
	else
	{
		$searchCond.=" and a.buyer_name=$cbo_buyer_name";
	}
	
	ob_start();
	
	if( str_replace("'","",$cbo_search_by)==1 )
	{
	?>
         <div align="center">
                <table width="2880" id="table_header_1" border="1" class="rpt_table" rules="all">
                    <thead>
                        <tr>
                            <th width="50">SL</th>
                            <th width="65">Company</th>
                            <th width="70">Job No</th>
                            <th width="50">Buyer</th>
                            <th width="100">Order no</th>
                            <th width="50">Agent</th>
                            <th width="50">Image</th>                            
                            <th width="100">Style Name</th>
                            <th width="100">Order Quantity</th>
                            <th width="90">Order Value</th>
                            <th width="60">UOM</th>
                            <th width="90">Shipment Date</th>
                            <th width="60">Days in Hand</th>
                            <th width="80">Sample Approved</th>
                            <th width="80">Lapdip Approved</th>
                            <th width="80">Accessories Approved</th>
                            <th width="80">Embel. Approved</th>
                            <th width="80">Fabric Booking</th>
                            <th width="80">Knitting Finished</th>
                            <th width="80">LC/SC Received</th>
                            <th width="80">Finished Fab Recv</th>
                            <th width="80">Trims Received</th>
                            <th width="80">Cutting Finished</th>
                            <th width="80">Print & Emb. Completed</th>
                            <th width="80">Sewing Finished</th>
                            <th width="80">Finishing Input</th>
                            <th width="80">Iron Output</th>
                            <th width="80">Finishing Completed</th>
                            <th width="80">Buyer Inspection</th>
                            <th width="100">Ship Qnty(Pcs) As Per Ex-Fact.</th>
                            <th width="80">Actual Shipment Date</th>
                            <th width="80">Ship Qnty. (Pcs) As Per Invoice</th>
                            <th width="80">Ship Value (Gross)</th>
                            <th width="80">Submitted Date</th>
                            <th width="80">Proceed Realized</th>
                            <th width="">Balance Ship Qnty</th>
                         </tr>
                    </thead>
                </table>
                <div style="max-height:400px; overflow-y:scroll; width:2920px"  align="left" id="scroll_body">
                    <table width="2880" border="1" class="rpt_table" rules="all" id="table_body">
                    <?
                    $i=1; 
                    
					//for get Sample Approved percentage--------------------------------//
					$sampleSQL ="select po_break_down_id,SUM(CASE WHEN approval_status=3 THEN 1 ELSE 0 END) as apprv_status
 								from wo_po_sample_approval_info where approval_status<>4 and current_status=1 and is_deleted=0 and status_active=1 group by po_break_down_id,color_number_id,sample_type_id";					
					$sampleSQLresult = sql_select($sampleSQL);    
					$sampleAPParr = array();
					foreach($sampleSQLresult as $key=>$val){
						$sampleAPParr[$val[csf('po_break_down_id')]]['apprv_status']+=$val[csf('apprv_status')];
						$sampleAPParr[$val[csf('po_break_down_id')]]['total_po']+=1;
					}
					//print_r($sampleSQLresult);  
					
					//for lapdip approval percentage----------------------------------//
					$lapdipSQL = "select po_break_down_id,SUM(CASE WHEN approval_status=3 THEN 1 ELSE 0 END) as apprv_status,
								SUM(CASE WHEN approval_status<>4 THEN 1 ELSE 0 END) as total_po,
								SUM(CASE WHEN approval_status=2 and current_status=1 THEN 1 ELSE 0 END) as total_rejected									
								from wo_po_lapdip_approval_info where current_status=1 and is_deleted=0 and status_active=1 group by po_break_down_id";					
					$lapdipSQLresult = sql_select($lapdipSQL);    
					$lapdipAPParr = array();
					foreach($lapdipSQLresult as $key=>$val){
						$lapdipAPParr[$val[csf('po_break_down_id')]]['apprv_status']=$val[csf('apprv_status')];
						$lapdipAPParr[$val[csf('po_break_down_id')]]['total_po']=$val[csf('total_po')];//-$val[csf('total_rejected')]
					}
					
					//for trims/accessories approval percentage----------------------------------//
					$trimsSQL = "select po_break_down_id,SUM(CASE WHEN approval_status=3 THEN 1 ELSE 0 END) as apprv_status,
								SUM(CASE WHEN approval_status<>4 THEN 1 ELSE 0 END) as total_po,
								SUM(CASE WHEN approval_status=2 THEN 1 ELSE 0 END) as total_rejected
								from wo_po_trims_approval_info where current_status=1 and is_deleted=0 and status_active=1 group by po_break_down_id";					
					$trimsSQLresult = sql_select($trimsSQL);    
					$trimsAPParr = array();
					foreach($trimsSQLresult as $key=>$val){
						$trimsAPParr[$val[csf('po_break_down_id')]]['apprv_status']=$val[csf('apprv_status')];
						$trimsAPParr[$val[csf('po_break_down_id')]]['total_po']=$val[csf('total_po')];//-$val[csf('total_rejected')]
					}
					
					
					//for Embellishment Approval approval percentage----------------------------------//
					$embelSQL = "select po_break_down_id,SUM(CASE WHEN approval_status=3 THEN 1 ELSE 0 END) as apprv_status,
								SUM(CASE WHEN approval_status<>4 THEN 1 ELSE 0 END) as total_po,
								SUM(CASE WHEN approval_status=2 THEN 1 ELSE 0 END) as total_rejected	
								from wo_po_embell_approval where current_status=1 and is_deleted=0 and status_active=1 group by po_break_down_id";					
					$embelSQLresult = sql_select($embelSQL);    
					$embelAPParr = array();
					foreach($embelSQLresult as $key=>$val){
						$embelAPParr[$val[csf('po_break_down_id')]]['apprv_status']=$val[csf('apprv_status')];
						$embelAPParr[$val[csf('po_break_down_id')]]['total_po']=$val[csf('total_po')];//-$val[csf('total_rejected')]
					}
										
					
					//for Fabric Booking percentage----------------------------------//
					$fabricSQL = "select b.po_break_down_id, SUM(b.grey_fab_qnty) as grey_fab_qnty, SUM(b.fin_fab_qnty) as fin_fab_qnty
								 from wo_booking_mst a, wo_booking_dtls b 
								 where a.booking_no=b.booking_no and a.booking_type!=3 and item_category in (2,13) and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.po_break_down_id";
					$fabricSQLresult = sql_select($fabricSQL);
					$fabricArr = array();
					$finfabricArr = array();
					foreach($fabricSQLresult as $key=>$val){
						$fabricArr[$val[csf('po_break_down_id')]]=$val[csf('grey_fab_qnty')];
						$finfabricArr[$val[csf('po_break_down_id')]]=$val[csf('fin_fab_qnty')];
 					}
					
					
					$reqSQL = "select b.po_break_down_id, sum((b.requirment/b.pcs)*a.plan_cut_qnty) as requirment
							  from wo_po_color_size_breakdown a, wo_pre_cos_fab_co_avg_con_dtls b 
							  where a.po_break_down_id=b.po_break_down_id and a.color_number_id=b.color_number_id and a.size_number_id=b.gmts_sizes and a.is_deleted=0 and a.status_active=1 group by b.po_break_down_id";
					
					/*$reqSQL = "select b.po_break_down_id, (SUM(b.requirment)/b.pcs)*a.plan_cut as requirment
							  	 from wo_po_break_down a, wo_pre_cos_fab_co_avg_con_dtls b 
							  	 where a.id=b.po_break_down_id and a.is_deleted=0 and a.status_active=1 group by b.po_break_down_id";*/
					$reqSQLresult = sql_select($reqSQL);
					$reqArr = array();
					foreach($reqSQLresult as $key=>$val){
						$reqArr[$val[csf('po_break_down_id')]]=$val[csf('requirment')];
 					}
					
					
					
					//knitting finsih percentage-----------------------------//
					$knitSQL = "select po_breakdown_id as po_break_down_id, sum(quantity) as quantity
							  	 from order_wise_pro_details
							  	 where entry_form=2 and is_deleted=0 and status_active=1 group by po_breakdown_id";
					$knitSQLresult = sql_select($knitSQL);
					$knitArr = array();
					foreach($knitSQLresult as $key=>$val){
						$knitArr[$val[csf('po_break_down_id')]]=$val[csf('quantity')];
 					}
					 
					  
					//LC/SC Received percentage-----------------------------//
					$lcscSQL = "select wo_po_break_down_id as po_break_down_id,sum(attached_value) as attached_value
							  	 from com_sales_contract_order_info
							  	 where is_deleted=0 and status_active=1 group by wo_po_break_down_id
								 UNION ALL
								 select wo_po_break_down_id as po_break_down_id,sum(attached_value) as attached_value
							  	 from com_export_lc_order_info
							  	 where is_deleted=0 and status_active=1 group by wo_po_break_down_id";
					$lcscSQLresult = sql_select($lcscSQL);
					$lcscArr = array();
					foreach($lcscSQLresult as $key=>$val){
						$lcscArr[$val[csf('po_break_down_id')]]+=$val[csf('attached_value')];
 					}			 
					 
										                    
					//Finished Fab Recv percentage-----------------------------//
					$finishSQL = "select po_breakdown_id as po_break_down_id, sum(quantity) as quantity
							  	 from order_wise_pro_details
							  	 where entry_form=7 and is_deleted=0 and status_active=1 group by po_breakdown_id";
					$finishSQLresult = sql_select($finishSQL);
					$finishArr = array();
					foreach($finishSQLresult as $key=>$val){
						$finishArr[$val[csf('po_break_down_id')]]=$val[csf('quantity')];
 					}
					
					
					//Cutting Finished 	Print & Emb. Completed 	Sewing Finished Finishing Input Finishing Completed query -----------------------------//
					$sqlOrder = "SELECT  a.job_no_mst,a.id,
							SUM(CASE WHEN b.emb_name=1 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS print,  
							SUM(CASE WHEN b.emb_name=2 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS emb,
							SUM(CASE WHEN b.emb_name=3 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS wash,
							SUM(CASE WHEN b.emb_name=4 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS special
						FROM
							wo_po_break_down a, wo_pre_cost_embe_cost_dtls b 
						WHERE
							a.job_no_mst=b.job_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id";
					//echo $sqlOrder;die;		
					$sql_order=sql_select($sqlOrder);
					$poReqArr=array();
					foreach($sql_order as $resultRow)
					{
						$poReqArr[$resultRow[csf("id")]][1] = $resultRow[csf("print")];
						$poReqArr[$resultRow[csf("id")]][2] = $resultRow[csf("emb")];
						$poReqArr[$resultRow[csf("id")]][3] = $resultRow[csf("wash")];
						$poReqArr[$resultRow[csf("id")]][4] = $resultRow[csf("special")]; 
					}
					
					$prod_sql= "SELECT a.po_break_down_id,
						sum(CASE WHEN b.production_type ='1' THEN  b.production_qnty END) AS 'cutting',
						sum(CASE WHEN b.production_type ='3' THEN  b.production_qnty END) AS 'printing',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=1 THEN  b.production_qnty END) AS 'prnt',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=2 THEN  b.production_qnty END) AS 'embel',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=3 THEN  b.production_qnty END) AS 'wash',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=4 THEN  b.production_qnty END) AS 'special',
						sum(CASE WHEN b.production_type ='5' THEN  b.production_qnty END) AS 'sewingout',
						sum(CASE WHEN b.production_type ='6' THEN  b.production_qnty END) AS 'finishinput',
						sum(CASE WHEN b.production_type ='7' THEN  b.production_qnty END) AS 'ironoutput',
						sum(CASE WHEN b.production_type ='8' THEN  b.production_qnty END) AS 'finishcompleted'
						from pro_garments_production_mst a, pro_garments_production_dtls b
						where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 group by a.po_break_down_id";
					
					$prodSQLresult = sql_select($prod_sql);	
					$prodArr = array();
					foreach($prodSQLresult as $key=>$val){
						$prodArr[$val[csf('po_break_down_id')]]['cutting']=$val[csf('cutting')];
						$prodArr[$val[csf('po_break_down_id')]]['printing']=$val[csf('printing')];
						$prodArr[$val[csf('po_break_down_id')]]['prnt']=$val[csf('prnt')];
						$prodArr[$val[csf('po_break_down_id')]]['embel']=$val[csf('embel')];
						$prodArr[$val[csf('po_break_down_id')]]['wash']=$val[csf('wash')];
						$prodArr[$val[csf('po_break_down_id')]]['special']=$val[csf('special')];
						$prodArr[$val[csf('po_break_down_id')]]['sewingout']=$val[csf('sewingout')];
						$prodArr[$val[csf('po_break_down_id')]]['finishinput']=$val[csf('finishinput')];
						$prodArr[$val[csf('po_break_down_id')]]['ironoutput']=$val[csf('ironoutput')];
						$prodArr[$val[csf('po_break_down_id')]]['finishcompleted']=$val[csf('finishcompleted')];
 					}	
					
					
					//buyer inspection query----------------------------------//					
					$insp_qnty="select po_break_down_id, sum(inspection_qnty) as inspection_qnty from pro_buyer_inspection where inspection_status=1 and is_deleted=0 and status_active=1 group by po_break_down_id";					
					$inspSQLresult = sql_select($insp_qnty);
					$inspArr = array();
					foreach($inspSQLresult as $key=>$val){
						$inspArr[$val[csf('po_break_down_id')]]=$val[csf('inspection_qnty')];
 					}
					
					
					//Ship Qnty(Pcs)As Per Ex-Fact query----------------------------------//					
					$ex_qnty="select po_break_down_id, sum(ex_factory_qnty) as ex_factory_qnty from pro_ex_factory_mst where is_deleted=0 and status_active=1 group by po_break_down_id";					
					$exSQLresult = sql_select($ex_qnty);
					$exArr = array();
					foreach($exSQLresult as $key=>$val){
						$exArr[$val[csf('po_break_down_id')]]=$val[csf('ex_factory_qnty')];
 					}
					 	
					//Ship Qnty. (Pcs) As Per Invoice Ship Value (Gross) query----------------------------------//					
					$invoice_qnty="select po_breakdown_id, sum(current_invoice_qnty) as invoice_qnty, sum(current_invoice_value) as invoice_value from com_export_invoice_ship_dtls where is_deleted=0 and status_active=1 group by po_breakdown_id";					
					$invoiceSQLresult = sql_select($invoice_qnty);
					$invoiceArr = array();
					foreach($invoiceSQLresult as $key=>$val){
						$invoiceArr[$val[csf('po_breakdown_id')]]['invoice_qnty']=$val[csf('invoice_qnty')];
						$invoiceArr[$val[csf('po_breakdown_id')]]['invoice_value']=$val[csf('invoice_value')];
 					}
					
					 	
 					//Submitted Date query----------------------------------//					
					$submit_qnty="select a.po_breakdown_id, MAX(submit_date) as submit_date
						from com_export_invoice_ship_dtls a, com_export_doc_submission_invo b, com_export_doc_submission_mst c 
						where a.mst_id=b.invoice_id and b.doc_submission_mst_id=c.id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 group by a.po_breakdown_id";					
					$submitSQLresult = sql_select($submit_qnty);
					$submitArr = array();
					foreach($submitSQLresult as $key=>$val){
						$submitArr[$val[csf('po_breakdown_id')]]=$val[csf('submit_date')];
  					}
					
					
 					//Proceed Realized query----------------------------------//					
					$sql_realized = "select a.wo_po_break_down_id from com_sales_contract_order_info a, com_sales_contract b, com_export_doc_submission_invo c, com_export_proceed_realization e, com_export_proceed_rlzn_dtls f where a.com_sales_contract_id=b.id and b.id=c.lc_sc_id and c.is_lc=2 and c.doc_submission_mst_id=e.invoice_bill_id and f.mst_id=e.id AND a.is_deleted=0 AND a.status_active =1 AND b.is_deleted=0 AND b.status_active =1 AND c.is_deleted=0 AND c.status_active =1 AND e.status_active =1 AND e.is_deleted=0 AND f.status_active =1 AND f.is_deleted=0
					UNION ALL
					select a.wo_po_break_down_id from com_export_lc_order_info a, com_export_lc b, com_export_doc_submission_invo c, com_export_proceed_realization e, com_export_proceed_rlzn_dtls f where a.com_export_lc_id=b.id and b.id=c.lc_sc_id and c.is_lc=1 and c.doc_submission_mst_id=e.invoice_bill_id and f.mst_id=e.id AND a.is_deleted=0 AND a.status_active =1 AND b.is_deleted=0 AND b.status_active =1 AND c.is_deleted=0 AND c.status_active =1 AND e.status_active =1 AND e.is_deleted=0 AND f.status_active =1 AND f.is_deleted=0";			
					$realizedSQLresult = sql_select($sql_realized);
					$realizeArr = array();
					foreach($realizedSQLresult as $key=>$val){
						$realizeArr[$val[csf('wo_po_break_down_id')]]=$val[csf('wo_po_break_down_id')];
  					}
					
					//=======================================================================//
					// master query of this report==========================================//
					$jobSql = "select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant, b.id, b.is_confirmed, b.po_number, b.po_quantity, b.plan_cut, b.pub_shipment_date, b.po_received_date, DATEDIFF(b.pub_shipment_date, '$date') date_diff_1, DATEDIFF(b.shipment_date,'$date') date_diff_2, b.unit_price, b.po_total_price, b.details_remarks, b.shiping_status, sum(c.ex_factory_qnty) as ex_factory_qnty, MAX(c.ex_factory_date) as ex_factory_date, DATEDIFF(b.pub_shipment_date, MAX(c.ex_factory_date)) date_diff_3 ,DATEDIFF(b.shipment_date, MAX(c.ex_factory_date)) date_diff_4   
					from 
						wo_po_details_master a, wo_po_break_down b LEFT JOIN pro_ex_factory_mst c on b.id = c.po_break_down_id 
					where  
						a.job_no=b.job_no_mst and a.status_active=1 and b.status_active=1 and b.po_number like '$search_string' $date_cond $searchCond
						group by b.id order by b.pub_shipment_date,a.job_no_prefix_num,b.id";
					//echo $jobSql;
					$jobSqlResult = sql_select($jobSql);                   
				    foreach ($jobSqlResult as $row)
                    { 
						
						if ($i%2==0)  $bgcolor="#E9F3FF";else $bgcolor="#FFFFFF";
						
						//==================================
						$shipment_performance=0;
						if($row['shiping_status']==1 && $row['date_diff_1']>10 )
						{
						$color="";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row['shiping_status']==1 && ($row['date_diff_1']<=10 && $row['date_diff_1']>=0))
						{
						$color="orange";
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row['shiping_status']==1 &&  $row['date_diff_1']<0)
						{
						$color="red";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						//=====================================
						if($row['shiping_status']==2 && $row['date_diff_1']>10 )
						{
						$color="";	
						}
						if($row['shiping_status']==2 && ($row['date_diff_1']<=10 && $row['date_diff_1']>=0))
						{
						$color="orange";	
						}
						if($row['shiping_status']==2 &&  $row['date_diff_1']<0)
						{
						$color="red";	
						}
						if($row['shiping_status']==2 &&  $row['date_diff_2']>=0)
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;	
						}
						if($row['shiping_status']==2 &&  $row['date_diff_2']<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						//========================================
						if($row['shiping_status']==3 && $row['date_diff_3']>=0 )
						{
						$color="green";	
						}
						if($row['shiping_status']==3 &&  $row['date_diff_3']<0)
						{
						$color="#2A9FFF";	
						}
						if($row['shiping_status']==3 && $row['date_diff_4']>=0 )
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;
						}
						if($row['shiping_status']==3 &&  $row['date_diff_4']<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						
						?>
							<tr bgcolor="<? echo $bgcolor; ?>" style="vertical-align:middle" height="25" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
								<td width="50" bgcolor="<? echo $color; ?>"> <? echo $i; ?> </td>
								<td width="65"><p><? echo $company_short_name_arr[$row['company_name']];?></p></td>
								<td width="70" align="center"><p><? echo $row['job_no_prefix_num'];?></td>
								<td width="50"><p><? echo $buyer_short_name_arr[$row['buyer_name']];?></p></td>
								<td width="100"><p><? echo $row['po_number'];?></p></td>
								<td width="50"><p><? echo $buyer_short_name_arr[$row['agent_name']];?></p></td>
                                <td width="50"><img  src='../../../<? echo $imge_arr[$row['job_no']]; ?>' height='25' width='30' /></td>
								<td width="100"><p><? echo $row['style_ref_no'];?></p></td>
								<td width="100" align="right">
								<? 
								echo number_format(($row['po_quantity']*$row['total_set_qnty']),0);  
								?>
                                </td> 
								<td width="90" align="right">
								<?
									echo number_format($row['po_total_price'],2)
								?>
								</td>
                                <td width="60" align="center"><? echo $unit_of_measurement[$row['order_uom']];?></td>
								<td width="90" align="center"><? echo change_date_format($row['pub_shipment_date'],'dd-mm-yyyy','-');?></td>
								
                                <td  width="60" align="center" bgcolor="<? echo $color; ?>" > 
									<?
                                        if($row['shiping_status']==1 || $row['shiping_status']==2) echo $row['date_diff_1'];									 
                                        else if($row['shiping_status']==3) echo $row['date_diff_3'];									 
                                    ?>
								</td> 
                                
								<td width="80" align="right">
                                    <a href="##" onclick="show_progress_report_details('sample_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf("id")]; ?>','850px')">
                                    <?  
 										$sample_perc =  $sampleAPParr[$row[csf('id')]]['apprv_status']*100/$sampleAPParr[$row[csf('id')]]['total_po'];
                                        echo number_format(($sample_perc),2)." %"; 
                                    ?>
                                    </a>
								</td>
                                
								<td width="80" align="right">
                                	<a href="##" onclick="show_progress_report_details('lapdip_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')];?>','850px')">
									<?  
                                        $lapdip_perc =  $lapdipAPParr[$row[csf('id')]]['apprv_status']*100/$lapdipAPParr[$row[csf('id')]]['total_po'];
                                        echo number_format(($lapdip_perc),2)." %"; 
                                    ?>
                                    </a>
								</td>
                                
                                <td width="80" align="right">
                                    <a href="##" onclick="show_progress_report_details('accessories_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf("id")]; ?>','850px')">
                                    <?  
                                        $trims_perc =  $trimsAPParr[$row[csf('id')]]['apprv_status']*100/$trimsAPParr[$row[csf('id')]]['total_po'];
                                        echo number_format(($trims_perc),2)." %"; 
                                    ?>
                                    </a>
								</td>
                                
                                 <td width="80" align="right">
                                 	<a href="##" onclick="show_progress_report_details('embelishment_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf("id")]; ?>','850px')">
									<?  
                                        $embel_perc =  $embelAPParr[$row[csf('id')]]['apprv_status']*100/$embelAPParr[$row[csf('id')]]['total_po'];
                                        echo number_format(($embel_perc),2)." %"; 
                                    ?>
                                    </a>
								</td> 
								<td  width="80" align="right"> 
                                	<a href="##" onclick="show_progress_report_details('fabric_booking_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
									<?    
                                        $fabric_booking_perc =  $fabricArr[$row[csf('id')]]*100/$reqArr[$row[csf('id')]];
                                        echo number_format(($fabric_booking_perc),2)." %"; 
                                    ?>
                                    </a>
                                </td> 
                                <td  width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('knitting_finish_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
                                    <?   
										//echo $row[csf('id')]."=".$knitArr[$row[csf('id')]]."=".$fabricArr[$row[csf('id')]]."==";
                                        $knit_perc =  $knitArr[$row[csf('id')]]*100/$fabricArr[$row[csf('id')]];
                                        echo number_format(($knit_perc),2)." %"; 
                                    ?>
                                    </a>
                                </td>
                                
                                <td width="80" align="right"> 
                                	<a href="##" onclick="show_progress_report_details('lcsc_rcv_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
                                	<?
 									 echo number_format($lcscArr[$row[csf('id')]],2);
									?>
                                    </a>
                                </td>
                                
                                <td width="80" align="right"> 
                                	<a href="##" onclick="show_progress_report_details('finish_fabric_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
									<?   
                                        $finish_perc =  $finishArr[$row[csf('id')]]*100/$finfabricArr[$row[csf('id')]];
                                        echo number_format(($finish_perc),2)." %"; 
                                    ?>
                                    </a>
                                </td>
                                
                                <td width="80" align="center"> <!--trrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr-->
                                	<a href="##" onclick="show_trims_rec('trims_rec_popup','<? echo $row[csf("po_number")]; ?>','<? echo $row[csf('id')]; ?>','750px')">View</a>
                                </td>
                                
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('cutting_finish_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
                                        <?   
                                            $cutting_qty =  $prodArr[$row[csf('id')]]['cutting'];
 											if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/($row['plan_cut']*$row['total_set_qnty']);
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('print_emb_completed_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            
											$prnt_qty =  $prodArr[$row[csf('id')]]['prnt']/$poReqArr[$row[csf('id')]][1]*100;
											$embel_qty =  $prodArr[$row[csf('id')]]['embel']/$poReqArr[$row[csf('id')]][2]*100;
											$wash_qty =  $prodArr[$row[csf('id')]]['wash']/$poReqArr[$row[csf('id')]][3]*100;
											$special_qty =  $prodArr[$row[csf('id')]]['special']/$poReqArr[$row[csf('id')]][4]*100;
											$totalP=0;$n=0; 
											if($prnt_qty!==0){ $totalP+=$prnt_qty; $n++;}
											if($embel_qty!==0){ $totalP+=$embel_qty; $n++;}
											if($wash_qty!==0){ $totalP+=$wash_qty; $n++;}
											if($special_qty!==0){ $totalP+=$special_qty; $n++;}
											
											$embPercentage = $totalP/$n; 
                                            if($embPercentage!=''){ 
                                                echo number_format($embPercentage,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('sewing_finish_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('id')]]['sewingout'];
                                            if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/($row['po_quantity']*$row['total_set_qnty']);
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('finish_input_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('id')]]['finishinput'];
                                            if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/($row['po_quantity']*$row['total_set_qnty']);
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('iron_output_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('id')]]['ironoutput'];
                                            if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/($row['po_quantity']*$row['total_set_qnty']);
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('finish_completed_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('id')]]['finishcompleted'];
                                            if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/($row['po_quantity']*$row['total_set_qnty']);
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right">
                                    <a href="##" onclick="show_progress_report_details('buyer_inspection_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')"> 
										<?   
                                            $insp_qty_perc = ($inspArr[$row[csf('id')]]*100)/($row['po_quantity']*$row['total_set_qnty']);
                                            echo number_format($insp_qty_perc,2).' '.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="100" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('ex_factory_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')"> 
										<?   
                                            //$ex_qty_perc = ($exArr[$row[csf('id')]]*100)/($row['po_quantity']*$row['total_set_qnty']);
                                            //echo number_format($exArr[$row[csf('id')]],2).' '.'%';
											echo number_format($exArr[$row[csf('id')]],2);
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="center"> 
                                	<a href="##" onclick="show_progress_report_details('actual_shipment_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">View</a>
                                </td>
                                <td width="80" align="right"> 
                                <?   
 									echo number_format($invoiceArr[$row[csf('id')]]['invoice_qnty']);
 								?>
                                </td>
                                <td width="80" align="right"> 
                                <?   
 									echo number_format($invoiceArr[$row[csf('id')]]['invoice_value'],2);
 								?>
                                </td>
                                <td width="80"> 
                                    <a href="##" onclick="show_progress_report_details('submit_date_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','700px')">                         
										<?   
                                            if($submitArr[$row[csf('id')]]!="") echo change_date_format($submitArr[$row[csf('id')]]);
                                        ?>
                                    </a>
                                </td>
                                <td width="80"> 
                                <? if($realizeArr[$row[csf('id')]]!=""){ ?>
                                	 <a href="##" onclick="show_progress_report_details('proceed_realize_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','700px')">Relized</a>
                                <? } ?>
                                </td>
                                <td width="" align="right"> 
                                <?   
 									$balance_ship_qnty = $row['po_quantity']*$row['total_set_qnty']-$invoiceArr[$row[csf('id')]]['invoice_qnty'];
									echo number_format( $balance_ship_qnty,2 );
 								?>
                                </td>
                                 
							</tr>
                    <?
                    $i++;
                    }
					?>
                    
                    </table>
                    <table border="1" class="rpt_table"  width="2880" rules="all" id="report_table_footer_1" >
                            <tfoot>
                                <th width="50">&nbsp;</th>
                                <th width="65"></th>
                                <th width="70"></th>
                                <th width="50"></th>
                                <th width="100"></th>
                                <th width="50"></th>
                                <th width="50"></th>                            
                                <th width="100"></th>
                                <th width="100" id="value_total_order_qnty"></th>
                                <th width="90" id="value_total_order_value"></th>
                                <th width="60"></th>
                                <th width="90"></th>
                                <th width="60"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="100"></th>
                                <th width="80"></th>
                                <th width="80" id="value_total_ship_qnty"></th> 
                                <th width="80" id="value_total_ship_value"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="" id="value_total_balance_ship_qnty"></th>
                             </tfoot>
                 	</table>
               </div> 
       	</div>

 <?	
 
	}// end order wise report generate====================================================
	else if( str_replace("'","",$cbo_search_by)==2 ) //style wise report==================
	{
	?>
     
         <div align="center">
          <!--<h3 align="left" id="accordion_h4" class="accordion_h" onClick="accordion_menu( this.id,'content_report_panel', '')"> -Report Panel</h3>
            <div id="content_report_panel"> -->
                <table width="2880" id="table_header_1" border="1" class="rpt_table" rules="all">
                    <thead>
                        <tr>
                            <th width="50">SL</th>
                            <th width="65">Company</th>
                            <th width="70">Job No</th>
                            <th width="50">Buyer</th>
                            <th width="60">Order No</th>
                            <th width="50">Agent</th>
                            <th width="50">Image</th>                            
                            <th width="100">Style Name</th>
                            <th width="100">Order Quantity</th>
                            <th width="90">Order Value</th>
                            <th width="60">UOM</th>
                            <th width="90">Shipment Date</th>
                            <th width="60">Days in Hand</th>
                            <th width="80">Sample Approved</th>
                            <th width="80">Lapdip Approved</th>
                            <th width="80">Accessories Approved</th>
                            <th width="80">Embel. Approved</th>
                            <th width="80">Fabric Booking</th>
                            <th width="80">Knitting Finished</th>
                            <th width="80">LC/SC Received</th>
                            <th width="80">Finished Fab Recv</th>
                            <th width="80">Trims Received</th>
                            <th width="80">Cutting Finished</th>
                            <th width="80">Print & Emb. Completed</th>
                            <th width="80">Sewing Finished</th>
                            <th width="80">Finishing Input</th>
                            <th width="80">Iron Input</th>
                            <th width="80">Finishing Completed</th>
                            <th width="80">Buyer Inspection</th>
                            <th width="100">Ship Qnty(Pcs) As Per Ex-Fact.</th>
                            <th width="80">Actual Shipment Date</th>
                            <th width="80">Ship Qnty. (Pcs) As Per Invoice</th>
                            <th width="80">Ship Value (Gross)</th>
                            <th width="80">Submitted Date</th>
                            <th width="80">Proceed Realized</th>
                            <th width="">Balance Ship Qnty</th>
                         </tr>
                    </thead>
                </table>
                <div style="max-height:400px; overflow-y:scroll; width:2920px"  align="left" id="scroll_body">
                    <table width="2880" border="1" class="rpt_table" rules="all" id="table_body">
                    <?
                    $i=1;  
					
					
					//for get Sample Approved percentage--------------------------------//
					$sampleSQL ="select a.job_no_mst,SUM(CASE WHEN a.approval_status=3 THEN 1 ELSE 0 END) as apprv_status
 								from wo_po_sample_approval_info a, wo_po_break_down b where a.po_break_down_id=b.id and b.pub_shipment_date between '$start_date' and '$end_date' and a.approval_status<>4 and a.current_status=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.job_no_mst,a.color_number_id,a.sample_type_id";					
					//echo $sampleSQL;
					$sampleSQLresult = sql_select($sampleSQL);    
					$sampleAPParr = array();
					foreach($sampleSQLresult as $key=>$val){
						$sampleAPParr[$val[csf('job_no_mst')]]['apprv_status']+=$val[csf('apprv_status')];
						$sampleAPParr[$val[csf('job_no_mst')]]['total_po']+=1;
					}
										           
					
					//for lapdip approval percentage----------------------------------//
					$lapdipSQL = "select a.job_no_mst,SUM(CASE WHEN a.approval_status=3 THEN 1 ELSE 0 END) as apprv_status,
								SUM(CASE WHEN a.approval_status<>4 THEN 1 ELSE 0 END) as total_po,
								SUM(CASE WHEN a.approval_status=2 THEN 1 ELSE 0 END) as total_rejected									
								from wo_po_lapdip_approval_info a, wo_po_break_down b where a.po_break_down_id=b.id and b.pub_shipment_date between '$start_date' and '$end_date' and a.current_status=1 and a.is_deleted=0 and a.status_active=1 group by a.job_no_mst";					
					//echo $lapdipSQL;
					$lapdipSQLresult = sql_select($lapdipSQL);    
					$lapdipAPParr = array();
					foreach($lapdipSQLresult as $key=>$val){
						$lapdipAPParr[$val[csf('job_no_mst')]]['apprv_status']=$val[csf('apprv_status')];
						$lapdipAPParr[$val[csf('job_no_mst')]]['total_po']=$val[csf('total_po')];
					}
					
					//for trims/accessories approval percentage----------------------------------//
					$trimsSQL = "select a.job_no_mst,SUM(CASE WHEN a.approval_status=3 THEN 1 ELSE 0 END) as apprv_status,
								SUM(CASE WHEN a.approval_status<>4 THEN 1 ELSE 0 END) as total_po,
								SUM(CASE WHEN a.approval_status=2 THEN 1 ELSE 0 END) as total_rejected
								from wo_po_trims_approval_info a, wo_po_break_down b where a.po_break_down_id=b.id and b.pub_shipment_date between '$start_date' and '$end_date' and a.current_status=1 and a.is_deleted=0 and a.status_active=1 group by a.job_no_mst";					
					$trimsSQLresult = sql_select($trimsSQL);    
					$trimsAPParr = array();
					foreach($trimsSQLresult as $key=>$val){
						$trimsAPParr[$val[csf('job_no_mst')]]['apprv_status']=$val[csf('apprv_status')];
						$trimsAPParr[$val[csf('job_no_mst')]]['total_po']=$val[csf('total_po')];
					}
					
					
					//for Embellishment Approval approval percentage----------------------------------//
					$embelSQL = "select a.job_no_mst,SUM(CASE WHEN a.approval_status=3 THEN 1 ELSE 0 END) as apprv_status,
								SUM(CASE WHEN a.approval_status<>4 THEN 1 ELSE 0 END) as total_po,
								SUM(CASE WHEN a.approval_status=2 THEN 1 ELSE 0 END) as total_rejected
								from wo_po_embell_approval a, wo_po_break_down b where a.po_break_down_id=b.id and b.pub_shipment_date between '$start_date' and '$end_date' and a.current_status=1 and a.is_deleted=0 and a.status_active=1 group by a.job_no_mst";			
					/*$embelSQL = "select job_no_mst,SUM(CASE WHEN approval_status=3 THEN 1 ELSE 0 END) as apprv_status,
								SUM(CASE WHEN approval_status<>4 THEN 1 ELSE 0 END) as total_po,
								SUM(CASE WHEN approval_status=2 THEN 1 ELSE 0 END) as total_rejected	
								from wo_po_embell_approval where current_status=1 and is_deleted=0 and status_active=1 group by job_no_mst";*/					
					$embelSQLresult = sql_select($embelSQL);    
					$embelAPParr = array();
					foreach($embelSQLresult as $key=>$val){
						$embelAPParr[$val[csf('job_no_mst')]]['apprv_status']=$val[csf('apprv_status')];
						$embelAPParr[$val[csf('job_no_mst')]]['total_po']=$val[csf('total_po')];
					}
								
					
					//for Fabric Booking percentage----------------------------------//
					$fabricSQL = "select b.job_no as job_no_mst, SUM(b.grey_fab_qnty) as grey_fab_qnty, SUM(b.fin_fab_qnty) as fin_fab_qnty
								 from wo_booking_mst a, wo_booking_dtls b, wo_po_break_down c 
								 where a.booking_no=b.booking_no and b.po_break_down_id=c.id and c.pub_shipment_date between '$start_date' and '$end_date' and a.booking_type!=3 and item_category in (2,13) and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.job_no";
					$fabricSQLresult = sql_select($fabricSQL);
					$fabricArr = array();
					$finfabricArr = array();
					foreach($fabricSQLresult as $key=>$val){
						$fabricArr[$val[csf('job_no_mst')]]=$val[csf('grey_fab_qnty')];
						$finfabricArr[$val[csf('job_no_mst')]]=$val[csf('fin_fab_qnty')];
 					}
					
					
					$reqSQL = "select b.job_no as job_no_mst, sum((b.requirment/b.pcs)*a.plan_cut_qnty) as requirment
							  from wo_po_color_size_breakdown a, wo_pre_cos_fab_co_avg_con_dtls b, wo_po_break_down c 
							  where a.po_break_down_id=b.po_break_down_id and b.po_break_down_id=c.id and c.pub_shipment_date between '$start_date' and '$end_date' and a.color_number_id=b.color_number_id and a.size_number_id=b.gmts_sizes and a.is_deleted=0 and a.status_active=1 group by b.job_no";
					
					/*$reqSQL = "select b.po_break_down_id, (SUM(b.requirment)/b.pcs)*a.plan_cut as requirment
							  	 from wo_po_break_down a, wo_pre_cos_fab_co_avg_con_dtls b 
							  	 where a.id=b.po_break_down_id and a.is_deleted=0 and a.status_active=1 group by b.po_break_down_id";*/
					$reqSQLresult = sql_select($reqSQL);
					$reqArr = array();
					foreach($reqSQLresult as $key=>$val){
						$reqArr[$val[csf('job_no_mst')]]=$val[csf('requirment')];
 					}
					
							
					
					//knitting finsih percentage-----------------------------//
					$knitSQL = "select b.job_no_mst, 
								sum(case when a.entry_form=2 then a.quantity end) as knitting_quantity,
								sum(case when a.entry_form=7 then a.quantity end) as finish_quantity
							  	from order_wise_pro_details a, wo_po_break_down b
							  	where a.po_breakdown_id=b.id and b.pub_shipment_date between '$start_date' and '$end_date' and a.is_deleted=0 and a.status_active=1 group by b.job_no_mst";
					$knitSQLresult = sql_select($knitSQL);
					$knitArr = array();$finishArr = array();
					foreach($knitSQLresult as $key=>$val){
						$knitArr[$val[csf('job_no_mst')]]=$val[csf('knitting_quantity')]; //this for knitting quantity
						$finishArr[$val[csf('job_no_mst')]]=$val[csf('finish_quantity')]; //this is for finish quantity
 					}
					
					  
					//LC/SC Received percentage-----------------------------//
					$lcscSQL = "select b.job_no_mst,sum(a.attached_value) as attached_value
							  	 from com_sales_contract_order_info a,wo_po_break_down b
							  	 where a.wo_po_break_down_id=b.id and  a.is_deleted=0 and a.status_active=1 and b.pub_shipment_date between '$start_date' and '$end_date' group by job_no_mst
								 UNION ALL
								 select b.job_no_mst,sum(a.attached_value) as attached_value
							  	 from com_export_lc_order_info a, wo_po_break_down b
							  	 where a.wo_po_break_down_id=b.id and a.is_deleted=0 and a.status_active=1 and b.pub_shipment_date between '$start_date' and '$end_date' group by b.job_no_mst";
					$lcscSQLresult = sql_select($lcscSQL);
					$lcscArr = array();
					foreach($lcscSQLresult as $key=>$val){
						$lcscArr[$val[csf('job_no_mst')]]+=$val[csf('attached_value')];
 					}			 
					
					/*					                    
					//Finished Fab Recv percentage-----------------------------//
					$finishSQL = "select po_breakdown_id as po_break_down_id, sum(quantity) as quantity
							  	 from order_wise_pro_details
							  	 where entry_form=7 and is_deleted=0 and status_active=1 group by po_breakdown_id";
					$finishSQLresult = sql_select($finishSQL);
					$finishArr = array();
					foreach($finishSQLresult as $key=>$val){
						$finishArr[$val[csf('po_break_down_id')]]=$val[csf('quantity')];
 					}*/
					
					
					//Cutting Finished 	Print & Emb. Completed 	Sewing Finished Finishing Input Finishing Completed query -----------------------------//
					$sqlOrder = "SELECT  a.job_no_mst,
							SUM(CASE WHEN b.emb_name=1 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS print,  
							SUM(CASE WHEN b.emb_name=2 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS emb,
							SUM(CASE WHEN b.emb_name=3 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS wash,
							SUM(CASE WHEN b.emb_name=4 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS special
						FROM
							wo_po_break_down a, wo_pre_cost_embe_cost_dtls b 
						WHERE
							a.job_no_mst=b.job_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.pub_shipment_date between '$start_date' and '$end_date' group by a.job_no_mst";
					//echo $sqlOrder;die;		
					$sql_order=sql_select($sqlOrder);
					$poReqArr=array();
					foreach($sql_order as $resultRow)
					{
						$poReqArr[$resultRow[csf("job_no_mst")]][1] = $resultRow[csf("print")];
						$poReqArr[$resultRow[csf("job_no_mst")]][2] = $resultRow[csf("emb")];
						$poReqArr[$resultRow[csf("job_no_mst")]][3] = $resultRow[csf("wash")];
						$poReqArr[$resultRow[csf("job_no_mst")]][4] = $resultRow[csf("special")]; 
					}
					 
					
					$prod_sql= "SELECT c.job_no_mst,
						sum(CASE WHEN b.production_type ='1' THEN  b.production_qnty END) AS 'cutting',
						sum(CASE WHEN b.production_type ='3' THEN  b.production_qnty END) AS 'printing',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=1 THEN  b.production_qnty END) AS 'prnt',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=2 THEN  b.production_qnty END) AS 'embel',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=3 THEN  b.production_qnty END) AS 'wash',
						sum(CASE WHEN b.production_type ='3' and a.embel_name=4 THEN  b.production_qnty END) AS 'special',
						sum(CASE WHEN b.production_type ='5' THEN  b.production_qnty END) AS 'sewingout',
						sum(CASE WHEN b.production_type ='6' THEN  b.production_qnty END) AS 'finishinput',
						sum(CASE WHEN b.production_type ='7' THEN  b.production_qnty END) AS 'ironoutput',
						sum(CASE WHEN b.production_type ='8' THEN  b.production_qnty END) AS 'finishcompleted'
						from pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_break_down c
						where a.id=b.mst_id and a.po_break_down_id=c.id and a.status_active=1 and a.is_deleted=0 and c.pub_shipment_date between '$start_date' and '$end_date' group by c.job_no_mst";
					
					$prodSQLresult = sql_select($prod_sql);	
					$prodArr = array();
					foreach($prodSQLresult as $key=>$val){ 
						$prodArr[$val[csf('job_no_mst')]]['cutting']=$val[csf('cutting')];
						$prodArr[$val[csf('job_no_mst')]]['printing']=$val[csf('printing')];
						$prodArr[$val[csf('job_no_mst')]]['prnt']=$val[csf('prnt')];
						$prodArr[$val[csf('job_no_mst')]]['embel']=$val[csf('embel')];
						$prodArr[$val[csf('job_no_mst')]]['wash']=$val[csf('wash')];
						$prodArr[$val[csf('job_no_mst')]]['special']=$val[csf('special')];
						$prodArr[$val[csf('job_no_mst')]]['sewingout']=$val[csf('sewingout')];
						$prodArr[$val[csf('job_no_mst')]]['finishinput']=$val[csf('finishinput')];
						$prodArr[$val[csf('job_no_mst')]]['ironoutput']=$val[csf('ironoutput')];
						$prodArr[$val[csf('job_no_mst')]]['finishcompleted']=$val[csf('finishcompleted')];
 					}	
					
					
					//buyer inspection query----------------------------------//					
					$insp_qnty="select a.job_no as job_no_mst, sum(a.inspection_qnty) as inspection_qnty from pro_buyer_inspection a, wo_po_break_down b where a.inspection_status=1 and a.is_deleted=0 and a.status_active=1 and b.pub_shipment_date between '$start_date' and '$end_date' group by a.job_no";
					$inspSQLresult = sql_select($insp_qnty);
					$inspArr = array();
					foreach($inspSQLresult as $key=>$val){
						$inspArr[$val[csf('job_no_mst')]]=$val[csf('inspection_qnty')];
 					}
					
					
					//Ship Qnty(Pcs)As Per Ex-Fact query----------------------------------//					
					$ex_qnty="select b.job_no_mst, sum(a.ex_factory_qnty) as ex_factory_qnty 
							from pro_ex_factory_mst a, wo_po_break_down b where a.po_break_down_id=b.id and a.is_deleted=0 and a.status_active=1 and b.pub_shipment_date between '$start_date' and '$end_date' group by b.job_no_mst";					
					$exSQLresult = sql_select($ex_qnty);
					$exArr = array();
					foreach($exSQLresult as $key=>$val){
						$exArr[$val[csf('job_no_mst')]]=$val[csf('ex_factory_qnty')];
 					}
					 	
					//Ship Qnty. (Pcs) As Per Invoice Ship Value (Gross) query----------------------------------//					
					$invoice_qnty="select b.job_no_mst, sum(a.current_invoice_qnty) as invoice_qnty, sum(a.current_invoice_value) as invoice_value 
					from com_export_invoice_ship_dtls a, wo_po_break_down b
					where a.po_breakdown_id=b.id and a.is_deleted=0 and a.status_active=1 and b.pub_shipment_date between '$start_date' and '$end_date' group by b.job_no_mst";					
					$invoiceSQLresult = sql_select($invoice_qnty);
					$invoiceArr = array();
					foreach($invoiceSQLresult as $key=>$val){
						$invoiceArr[$val[csf('job_no_mst')]]['invoice_qnty']=$val[csf('invoice_qnty')];
						$invoiceArr[$val[csf('job_no_mst')]]['invoice_value']=$val[csf('invoice_value')];
 					}
					
					 	
 					//Submitted Date query----------------------------------//					
					$submit_qnty="select d.job_no_mst, MAX(submit_date) as submit_date
						from com_export_invoice_ship_dtls a, com_export_doc_submission_invo b, com_export_doc_submission_mst c, wo_po_break_down d 
						where a.mst_id=b.invoice_id and b.doc_submission_mst_id=c.id and a.po_breakdown_id=d.id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 group by d.job_no_mst";					
					$submitSQLresult = sql_select($submit_qnty);
					$submitArr = array();
					foreach($submitSQLresult as $key=>$val){
						$submitArr[$val[csf('job_no_mst')]]=$val[csf('submit_date')];
  					}
					
					
 					//Proceed Realized query----------------------------------//					
					$sql_realized = "select d.job_no_mst from com_sales_contract_order_info a, com_sales_contract b, com_export_doc_submission_invo c, com_export_proceed_realization e, com_export_proceed_rlzn_dtls f, wo_po_break_down d where a.com_sales_contract_id=b.id and b.id=c.lc_sc_id and c.is_lc=2 and c.doc_submission_mst_id=e.invoice_bill_id and f.mst_id=e.id AND a.wo_po_break_down_id=d.id AND a.is_deleted=0 AND a.status_active =1 AND b.is_deleted=0 AND b.status_active =1 AND c.is_deleted=0 AND c.status_active =1 AND e.status_active =1 AND e.is_deleted=0 AND f.status_active =1 AND f.is_deleted=0 and d.pub_shipment_date between '$start_date' and '$end_date'
					UNION ALL
					select d.job_no_mst from com_export_lc_order_info a, com_export_lc b, com_export_doc_submission_invo c, com_export_proceed_realization e, com_export_proceed_rlzn_dtls f, wo_po_break_down d where a.com_export_lc_id=b.id and b.id=c.lc_sc_id and c.is_lc=1 and c.doc_submission_mst_id=e.invoice_bill_id and f.mst_id=e.id AND a.wo_po_break_down_id=d.id AND a.is_deleted=0 AND a.status_active =1 AND b.is_deleted=0 AND b.status_active =1 AND c.is_deleted=0 AND c.status_active =1 AND e.status_active =1 AND e.is_deleted=0 AND f.status_active =1 AND f.is_deleted=0 and d.pub_shipment_date between '$start_date' and '$end_date'";			
					$realizedSQLresult = sql_select($sql_realized);
					$realizeArr = array();
					foreach($realizedSQLresult as $key=>$val){
						$realizeArr[$val[csf('job_no_mst')]]=$val[csf('job_no_mst')];
  					}
					
					
					 	
 					//Exfactory Date query----------------------------------//					
					$ex_sql="select b.job_no_mst, sum(a.ex_factory_qnty) as ex_factory_qnty,MAX(a.ex_factory_date) as ex_factory_date, DATEDIFF(b.pub_shipment_date, MAX(a.ex_factory_date)) date_diff_3,DATEDIFF(b.shipment_date,MAX(a.ex_factory_date)) date_diff_4   
						from pro_ex_factory_mst a, wo_po_break_down b
						where a.po_break_down_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.pub_shipment_date between '$start_date' and '$end_date'  group by b.job_no_mst";					
					$exSQLresult = sql_select($ex_sql);
					$exFactArr = array();
					foreach($exSQLresult as $key=>$val){
						$exFactArr[$val[csf('job_no_mst')]]['ex_factory_qnty']=$val[csf('ex_factory_qnty')];
						$exFactArr[$val[csf('job_no_mst')]]['ex_factory_date']=$val[csf('ex_factory_date')];
						$exFactArr[$val[csf('job_no_mst')]]['date_diff_3']=$val[csf('date_diff_3')];
						$exFactArr[$val[csf('job_no_mst')]]['date_diff_4']=$val[csf('date_diff_4')]; 
  					}
					
					
					
					
					//=======================================================================//
					// master query of this report==========================================//
					$jobSql = "select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.agent_name, a.style_ref_no, a.job_quantity, a.product_category, a.job_no, a.gmts_item_id, a.total_set_qnty, a.order_uom, a.team_leader, a.dealing_marchant,group_concat(distinct b.id) as id, b.is_confirmed, b.po_number, sum(b.po_quantity*a.total_set_qnty) as po_quantity, sum(b.plan_cut*a.total_set_qnty) as plan_cut, MIN(b.pub_shipment_date) as pub_shipment_date, b.po_received_date, DATEDIFF(b.pub_shipment_date,'$date') date_diff_1, DATEDIFF(b.shipment_date,'$date') date_diff_2, b.unit_price, sum(b.po_total_price) as po_total_price, b.details_remarks, b.shiping_status
					from 
						wo_po_details_master a, wo_po_break_down b
					where  
						a.job_no=b.job_no_mst and a.status_active=1 and b.status_active=1 and a.style_ref_no like '$search_string' $date_cond $searchCond
						group by b.job_no_mst order by b.pub_shipment_date,a.job_no_prefix_num,b.job_no_mst";
					//echo $jobSql;die;  
					$jobSqlResult = sql_select($jobSql);                   
				    foreach ($jobSqlResult as $row)
                    { 
						
						if ($i%2==0)  $bgcolor="#E9F3FF";else $bgcolor="#FFFFFF";
						
						//==================================
						$shipment_performance=0;
						if($row['shiping_status']==1 && $row['date_diff_1']>10 )
						{
						$color="";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row['shiping_status']==1 && ($row['date_diff_1']<=10 && $row['date_diff_1']>=0))
						{
						$color="orange";
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						if($row['shiping_status']==1 &&  $row['date_diff_1']<0)
						{
						$color="red";	
						$number_of_order['yet']+=1;
						$shipment_performance=0;
						}
						//=====================================
						if($row['shiping_status']==2 && $row['date_diff_1']>10 )
						{
						$color="";	
						}
						if($row['shiping_status']==2 && ($row['date_diff_1']<=10 && $row['date_diff_1']>=0))
						{
						$color="orange";	
						}
						if($row['shiping_status']==2 &&  $row['date_diff_1']<0)
						{
						$color="red";	
						}
						if($row['shiping_status']==2 &&  $row['date_diff_2']>=0)
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;	
						}
						if($row['shiping_status']==2 &&  $row['date_diff_2']<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						
						//======================================== with exfactory=========
						$row['ex_factory_qnty']=$exFactArr[$row[csf('job_no')]]['ex_factory_qnty'];
						$row['ex_factory_date']=$exFactArr[$row[csf('job_no')]]['ex_factory_date'];
						$row['date_diff_3']=$exFactArr[$row[csf('job_no')]]['date_diff_3'];
						$row['date_diff_4']=$exFactArr[$row[csf('job_no')]]['date_diff_4'];
						if($row['shiping_status']==3 && $row['date_diff_3']>=0 )
						{
						$color="green";	
						}
						if($row['shiping_status']==3 &&  $row['date_diff_3']<0)
						{
						$color="#2A9FFF";	
						}
						if($row['shiping_status']==3 && $row['date_diff_4']>=0 )
						{
						$number_of_order['ontime']+=1;
						$shipment_performance=1;
						}
						if($row['shiping_status']==3 &&  $row['date_diff_4']<0)
						{
						$number_of_order['after']+=1;
						$shipment_performance=2;	
						}
						
						?>
							<tr bgcolor="<? echo $bgcolor; ?>" style="vertical-align:middle" height="25" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">
								<td width="50" bgcolor="<? echo $color; ?>"> <? echo $i; ?> </td>
								<td width="65"><p><? echo $company_short_name_arr[$row['company_name']];?></p></td>
								<td width="70" align="center"><p><? echo $row['job_no_prefix_num'];?></td>
								<td width="50"><p><? echo $buyer_short_name_arr[$row['buyer_name']];?></p></td>
                                <td width="60" align="center"><a href="##" onclick="show_progress_report_details('order_number_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf("id")]; ?>','850px')"> View</a></td>
								<td width="50"><p><? echo $buyer_short_name_arr[$row['agent_name']];?></p></td>
                                <td width="50"><img  src='../../../<? echo $imge_arr[$row['job_no']]; ?>' height='25' width='30' /></td>
								<td width="100"><p><? echo $row['style_ref_no'];?></p></td>
								
								<td width="100" align="right">
                                <?
									echo number_format($row['po_quantity'],2)
								?>
                                
                                </td> 
								<td width="90" align="right">
								<?
									echo number_format($row['po_total_price'],2)
								?>
								</td>
                                <td width="60" align="center"><? echo $unit_of_measurement[$row['order_uom']];?></td>
								<td width="90" align="center"><? echo change_date_format($row['pub_shipment_date'],'dd-mm-yyyy','-');?></td>
								
                                <td  width="60" align="center" bgcolor="<? echo $color; ?>" > 
									<?
                                        if($row['shiping_status']==1 || $row['shiping_status']==2) echo $row['date_diff_1'];									 
                                        else if($row['shiping_status']==3) echo $row['date_diff_3'];									 
                                    ?>
								</td> 
                                
								<td width="80" align="right">
                                    <a href="##" onclick="show_progress_report_details('sample_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf("id")]; ?>','850px')">
                                    <?  
 										$sample_perc =  $sampleAPParr[$row[csf('job_no')]]['apprv_status']*100/$sampleAPParr[$row[csf('job_no')]]['total_po'];
                                        echo number_format(($sample_perc),2)." %"; 
                                    ?>
                                    </a>
								</td>
                                
								<td width="80" align="right">
                                	<a href="##" onclick="show_progress_report_details('lapdip_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')];?>','850px')">
									<?  
                                        $lapdip_perc =  $lapdipAPParr[$row[csf('job_no')]]['apprv_status']*100/$lapdipAPParr[$row[csf('job_no')]]['total_po'];
                                        echo number_format(($lapdip_perc),2)." %"; 
                                    ?>
                                    </a>
								</td>
                                
                                <td width="80" align="right">
                                    <a href="##" onclick="show_progress_report_details('accessories_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf("id")]; ?>','850px')">
                                    <?  
                                        $trims_perc =  $trimsAPParr[$row[csf('job_no')]]['apprv_status']*100/$trimsAPParr[$row[csf('job_no')]]['total_po'];
                                        echo number_format(($trims_perc),2)." %"; 
                                    ?>
                                    </a>
								</td>
                                
                                 <td width="80" align="right">
                                 	<a href="##" onclick="show_progress_report_details('embelishment_status','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf("id")]; ?>','850px')">
									<?  
                                        $embel_perc =  $embelAPParr[$row[csf('job_no')]]['apprv_status']*100/$embelAPParr[$row[csf('job_no')]]['total_po'];
                                        echo number_format(($embel_perc),2)." %"; 
                                    ?>
                                    </a>
								</td> 
								<td  width="80" align="right"> 
                                	<a href="##" onclick="show_progress_report_details('fabric_booking_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
									<?    
                                        $fabric_booking_perc =  $fabricArr[$row[csf('job_no')]]*100/$reqArr[$row[csf('job_no')]];
                                        echo number_format(($fabric_booking_perc),2)." %"; 
                                    ?>
                                    </a>
                                </td> 
                                <td  width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('knitting_finish_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
                                    <?   
										//echo $row[csf('id')]."=".$knitArr[$row[csf('id')]]."=".$fabricArr[$row[csf('id')]]."==";
                                        $knit_perc =  $knitArr[$row[csf('job_no')]]*100/$fabricArr[$row[csf('job_no')]];
                                        echo number_format(($knit_perc),2)." %"; 
                                    ?>
                                    </a>
                                </td>
                                
                                <td width="80" align="right"> 
                                	<a href="##" onclick="show_progress_report_details('lcsc_rcv_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
                                	<?
 									 echo number_format($lcscArr[$row[csf('job_no')]],2);
									?>
                                    </a>
                                </td>
                                
                                <td width="80" align="right"> 
                                	<a href="##" onclick="show_progress_report_details('finish_fabric_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
									<?   
                                        $finish_perc =  $finishArr[$row[csf('job_no')]]*100/$finfabricArr[$row[csf('job_no')]];
                                        echo number_format(($finish_perc),2)." %"; 
                                    ?>
                                    </a>
                                </td>
                                
                                <td width="80" align="center"> 
                                	<a href="##" onclick="##">View</a>
                                </td>
                                
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('cutting_finish_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
                                        <?   
                                            $cutting_qty =  $prodArr[$row[csf('job_no')]]['cutting'];
 											if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/$row['plan_cut'];
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('print_emb_completed_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $prnt_qty =  $prodArr[$row[csf('job_no')]]['prnt']/$poReqArr[$row[csf('job_no')]][1]*100;
											$embel_qty =  $prodArr[$row[csf('job_no')]]['embel']/$poReqArr[$row[csf('job_no')]][2]*100;
											$wash_qty =  $prodArr[$row[csf('job_no')]]['wash']/$poReqArr[$row[csf('job_no')]][3]*100;
											$special_qty =  $prodArr[$row[csf('job_no')]]['special']/$poReqArr[$row[csf('job_no')]][4]*100;
											$totalP=0;$n=0; 
											if($prnt_qty!==0){ $totalP+=$prnt_qty; $n++;}
											if($embel_qty!==0){ $totalP+=$embel_qty; $n++;}
											if($wash_qty!==0){ $totalP+=$wash_qty; $n++;}
											if($special_qty!==0){ $totalP+=$special_qty; $n++;}
											
											$embPercentage = $totalP/$n; 
                                            if($embPercentage!=''){ 
                                                echo number_format($embPercentage,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('sewing_finish_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('job_no')]]['sewingout'];
                                            if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/$row['po_quantity'];
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('finish_input_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('job_no')]]['finishinput'];
                                            if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/$row['po_quantity'];
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('iron_output_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('job_no')]]['ironoutput'];
                                            if($cutting_qty!=''){
                                                 $cutting_qty_perc = ($cutting_qty*100)/$row['po_quantity'];
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('finish_completed_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">
										<?   
                                            $cutting_qty =  $prodArr[$row[csf('job_no')]]['finishcompleted'];
                                            if($cutting_qty!=''){
                                                $cutting_qty_perc = ($cutting_qty*100)/$row['po_quantity'];
                                                echo number_format($cutting_qty_perc,2).' '.'%';
                                            }else echo '0.00'.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="right">
                                    <a href="##" onclick="show_progress_report_details('buyer_inspection_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')"> 
										<?   
                                            $insp_qty_perc = ($inspArr[$row[csf('job_no')]]*100)/$row['po_quantity'];
                                            echo number_format($insp_qty_perc,2).' '.'%';
                                        ?>
                                    </a>
                                </td>
                                <td width="100" align="right"> 
                                    <a href="##" onclick="show_progress_report_details('ex_factory_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')"> 
										<?   
                                            //$ex_qty_perc = ($exArr[$row[csf('id')]]*100)/($row['po_quantity']*$row['total_set_qnty']);
                                            //echo number_format($exArr[$row[csf('id')]],2).' '.'%';
											echo number_format($exArr[$row[csf('job_no')]],2);
                                        ?>
                                    </a>
                                </td>
                                <td width="80" align="center"> 
                                	<a href="##" onclick="show_progress_report_details('actual_shipment_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','850px')">View</a>
                                </td>
                                <td width="80" align="right"> 
                                <?   
 									echo number_format($invoiceArr[$row[csf('job_no')]]['invoice_qnty']);
 								?>
                                </td>
                                <td width="80" align="right"> 
                                <?   
 									echo number_format($invoiceArr[$row[csf('job_no')]]['invoice_value'],2);
 								?>
                                </td>
                                <td width="80"> 
                                    <a href="##" onclick="show_progress_report_details('submit_date_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','700px')">                         
										<?   
                                            if($submitArr[$row[csf('job_no')]]!="") echo change_date_format($submitArr[$row[csf('job_no')]]);
                                        ?>
                                    </a>
                                </td>
                                <td width="80"> 
                                <? if($realizeArr[$row[csf('job_no')]]!=""){ ?>
                                	 <a href="##" onclick="show_progress_report_details('proceed_realize_popup','<? echo $row[csf("job_no")]; ?>_<? echo $row[csf('id')]; ?>','700px')">Relized</a>
                                <? } ?>
                                </td>
                                <td width="" align="right"> 
                                <?   
 									$balance_ship_qnty = $row['po_quantity']-$invoiceArr[$row[csf('job_no')]]['invoice_qnty'];
									echo number_format( $balance_ship_qnty,2 );
 								?>
                                </td>
                                 
							</tr>
                    <?
                    $i++;
                    }
					?>
                    
                    </table>
                    <table border="1" class="rpt_table"  width="2880" rules="all" id="report_table_footer_1" >
                            <tfoot>
                                <th width="50">&nbsp;</th>
                                <th width="65"></th>
                                <th width="70"></th>
                                <th width="50"></th>
                                <th width="60"></th>
                                <th width="50"></th>
                                <th width="50"></th>                            
                                <th width="100"></th>
                                <th width="100" id="value_total_order_qnty"></th>
                                <th width="90" id="value_total_order_value"></th>
                                <th width="60"></th>
                                <th width="90"></th>
                                <th width="60"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="100"></th>
                                <th width="80"></th>
                                <th width="80" id="value_total_ship_qnty"></th> 
                                <th width="80" id="value_total_ship_value"></th>
                                <th width="80"></th>
                                <th width="80"></th>
                                <th width="" id="value_total_balance_ship_qnty"></th>
                             </tfoot>
                 	</table>
               </div> 
       	</div>

 <?	
 
	}// end style wise report generate==================================================== 
	
  
 
	$html = ob_get_contents();
	ob_clean();
	$new_link=create_delete_report_file( $html, 1, 1, "../../../" );
	
	echo "$html";
 	exit();
}






//######################################## ALL POP UP Here START ##############################
//#############################################################################################

if($action=="sample_status")
{
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sampleArr = return_library_array( "select id, sample_name from lib_sample",'id','sample_name');	
	echo load_html_head_contents("Sample Approve Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$po_arr=return_library_array( "select id, po_number from wo_po_break_down where id in ($po_id)",'id','po_number');
?>
<div style="width:100%" align="center">
	<fieldset style="width:820px">
        <table width="600">
            <?
			$job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
			foreach( $job_sql as $row_job);  // Master Job  table queery ends here
             ?>
                <tr class="form_caption">
                    <td align="center" colspan="4"><strong>Sample Approval Details</strong></td>	
                </tr>
                <tr>
                    <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                    <td align="left"  width="200"><? echo $job_number; ?></td> 
                    <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                    <td align="left"><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                </tr>
                <tr>
                    <td align="right"><strong>Company Name</strong> :</td> 
                    <td align="left"><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                    <td align="right"><strong>Style Ref No</strong> : </td> 
                    <td align="left"><? echo $row_job[csf("style_ref_no")]; ?> </td>
                </tr> 
                <tr>
                	<td colspan="4" height="15"></td>	                
                </tr>            
        </table>
        
            <div style="width:100%;" align="left">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                    	<tr>
                            <th width="35">SL</th>
                            <th width="80">PO Number</th>
                            <th width="100">Sample Type</th>
                            <th width="80">Color Name</th>
                            <th width="80">Target Date</th>
                            <th width="80">To Factory</th>
                            <th width="80">To Buyer</th>
                            <th width="80">Status</th>
                            <th width="80">Approval Date</th>
                            <th>Remarks</th>
                    	</tr>
                    </thead>
                </table>
            </div>  
            <div style="width:100%; max-height:230px; overflow-y:scroll" align="left">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1" >
                    <?
                    $i=0;
                    $sql= "select a.sample_type_id,b.color_number_id,b.po_break_down_id, target_approval_date, send_to_factory_date, submitted_to_buyer, approval_status, approval_status_date, sample_comments from wo_po_sample_approval_info a, wo_po_color_size_breakdown b where a.color_number_id=b.id and a.job_no_mst='$job_number' and a.approval_status<>0 and a.current_status=1 and a.is_deleted=0 and a.status_active=1 and b.po_break_down_id in ($po_id) order by a.sample_type_id,b.color_number_id";
                    //echo $sql;
					$apprv_sql= sql_select($sql);
					foreach( $apprv_sql as $row )
                    {
                        $i++;
                        if ($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
                   ?>
                    <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="80"><? echo $po_arr[$row[csf("po_break_down_id")]];?> </td>
                        <td width="100"><? echo $sampleArr[$row[csf("sample_type_id")]];?> </td> 
                        <td width="80"><p><? echo $color_arr[$row[csf("color_number_id")]]; ?></p></td>
                        <td align="center" width="80"><? echo change_date_format($row[csf("target_approval_date")]); ?></td>
                        <td align="center" width="80"><? echo change_date_format($row[csf("send_to_factory_date")]); ?></td>
                        <td align="center" width="80"><? echo change_date_format($row[csf("submitted_to_buyer")]); ?></td>
                        <td align="center" width="80"><? echo $approval_status[$row[approval_status]]; ?></td>
                        <td align="center" width="80"><? echo change_date_format($row[csf("approval_status_date")]); ?></td>
                        <td><p><? echo $row[csf("sample_comments")]; ?></p></td>
                    </tr>
                    
                    <? } ?>
                </table>
            </div> 
	</fieldset>
 </div> 
<?
}



if($action=="lapdip_status")
{
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$po_arr=return_library_array( "select id, po_number from wo_po_break_down where id in ($po_id)",'id','po_number');
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	echo load_html_head_contents("Lapdip Approve Details", "../../../../", 1, 1,$unicode,'','');
?>

<div style="width:100%" align="center">
	<fieldset style="width:800px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Lapdip Approval Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="80">PO Number</th>
                            <th width="80">Color Name</th>
                            <th width="80">Target Date</th>
                            <th width="80">To Factory</th>
                            <th width="80">To Buyer</th>
                            <th width="80">Status</th>
                            <th width="80">Approval Date</th>
                            <th width="80">Lapdip No</th>
                            <th>Remarks</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0;
                    /*$sql= "select a.color_name_id,a.lapdip_target_approval_date,a.send_to_factory_date,a.submitted_to_buyer,a.approval_status,a.approval_status_date,a.lapdip_no,a.lapdip_comments,b.po_break_down_id from wo_po_lapdip_approval_info a, wo_po_color_size_breakdown b where a.job_no_mst='$job_number'  and b.color_number_id=a.color_name_id and a.approval_status<>4 and a.current_status=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and a.po_break_down_id in ($po_id)"; */
					$sql= "select a.color_name_id,a.lapdip_target_approval_date,a.send_to_factory_date,a.submitted_to_buyer,a.approval_status,a.approval_status_date,a.lapdip_no,a.lapdip_comments,a.po_break_down_id from wo_po_lapdip_approval_info a where a.job_no_mst='$job_number' and a.approval_status<>4 and a.current_status=1 and a.is_deleted=0 and a.status_active=1 and a.po_break_down_id in ($po_id)";
					//( or b.po_break_down_id='0') 
					//echo $sql;
                    $lapdip_sql= sql_select($sql);
					foreach($lapdip_sql as $row)
                    {
                        $i++;
                        if ($i%2==0)  
                            $bgcolor="#EFEFEF";
                        else
                            $bgcolor="#FFFFFF";
                    ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="80"><? echo $po_arr[$row[csf("po_break_down_id")]];?> </td>
                        <td width="80"><p><? echo $color_arr[$row[csf("color_name_id")]]; ?></p></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("lapdip_target_approval_date")]); ?></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("send_to_factory_date")]); ?></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("submitted_to_buyer")]); ?></td>
                        <td width="80" align="center"><p><? echo $approval_status[$row[csf("approval_status")]]; ?></p></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("approval_status_date")]); ?></td>
                        <td width="80"><p><? echo $row[csf("lapdip_no")]; ?></p></td>
                        <td><p><? echo $row[csf("lapdip_comments")]; ?></p></td>
                    </tr>
                    <? } ?>
                </table>
            </div> 
    </fieldset>
</div>    
<?
}



if($action=="accessories_status")
{
 	echo load_html_head_contents("Lapdip Approve Details", "../../../../", 1, 1,$unicode,'','');
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$po_arr=return_library_array( "select id, po_number from wo_po_break_down where id in ($po_id)",'id','po_number');
?>

<div style="width:100%" align="center">
	<fieldset style="width:800px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Accessories Approval Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="80">PO Number</th>
                            <th width="100">Accessories Type</th>
                            <th width="80">Target Date</th>
                            <th width="80">To Supplier</th>
                            <th width="80">To Buyer</th>
                            <th width="80">Status</th>
                            <th width="80">Approval Date</th>
                            <th width="80">Supplier</th>
                            <th>Remarks</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0;
                    $sql= "select a.po_break_down_id,b.item_name, accessories_type_id, target_approval_date, sent_to_supplier, submitted_to_buyer, approval_status, approval_status_date, c.supplier_name, accessories_comments 
					from wo_po_trims_approval_info a left join lib_supplier c on a.supplier_name=c.id, lib_item_group b
					where a.job_no_mst='$job_number' and a.accessories_type_id=b.id and a.approval_status<>0 and a.current_status=1 and a.status_active=1 and a.is_deleted=0 and a.po_break_down_id in ($po_id) and b.status_active=1 and b.is_deleted=0"; 
                    //echo $sql;
					$acces_sql= sql_select($sql);
					foreach($acces_sql as $row)
                    {
                        $i++;
                        if ($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="80"><? echo $po_arr[$row[csf("po_break_down_id")]];?> </td>
                        <td width="100"><p><? echo $row[csf("item_name")]; ?></p></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("target_approval_date")]); ?></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("sent_to_supplier")]); ?></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("submitted_to_buyer")]); ?></td>
                        <td width="80" align="center"><p><? echo $approval_status[$row[csf("approval_status")]]; ?></p></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("approval_status_date")]); ?></td>
                        <td width="80"><p><? echo $row[csf("supplier_name")]; ?></p></td>
                        <td><p><? echo $row[csf("accessories_comments")]; ?></p></td>
                    </tr>
                    <? } ?>
                </table>
            </div> 
    </fieldset>
</div>    
<?
}



if($action=="embelishment_status")
{
 	echo load_html_head_contents("Lapdip Approve Details", "../../../../", 1, 1,$unicode,'','');

	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$po_arr=return_library_array( "select id, po_number from wo_po_break_down where id in ($po_id)",'id','po_number');
?>

<div style="width:100%" align="center">
	<fieldset style="width:800px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Embellishment Approval Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="80">PO Number</th> 
                            <th width="100">Embellishment Type</th>
                            <th width="80">Target Date</th>
                            <th width="80">To Supplier</th>
                            <th width="80">To Buyer</th>
                            <th width="80">Status</th>
                            <th width="80">Approval Date</th>
                            <th width="80">Supplier</th>
                            <th>Remarks</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0;
                    $emb_sql= sql_select("select a.po_break_down_id, embellishment_type_id, target_approval_date, sent_to_supplier, submitted_to_buyer, approval_status, approval_status_date, b.supplier_name, embellishment_comments 
					from  wo_po_embell_approval a left join lib_supplier b on a.supplier_name=b.id
					where a.job_no_mst='$job_number' and a.approval_status<>0 and a.current_status=1 and a.status_active=1 and a.is_deleted=0 and a.po_break_down_id in ($po_id)"); 
                    foreach($emb_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="80"><? echo $po_arr[$row[csf("po_break_down_id")]];?> </td>
                        <td width="100"><p><? echo $emblishment_print_type[$row[csf("embellishment_type_id")]]; ?></p></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("target_approval_date")]); ?></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("sent_to_supplier")]); ?></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("submitted_to_buyer")]); ?></td>
                        <td width="80" align="center"><p><? echo $approval_status[$row[csf("approval_status")]]; ?></p></td>
                        <td width="80" align="center"><? echo change_date_format($row[csf("approval_status_date")]); ?></td>
                        <td width="80"><p><? echo $row[csf("supplier_name")]; ?></p></td>
                        <td><p><? echo $row[csf("embellishment_comments")]; ?></p></td>
                    </tr>
                    <? } ?>
                </table>
            </div> 
    </fieldset>
</div>    
<?
}




if($action=="fabric_booking_popup")
{
 	echo load_html_head_contents("Fabric Booking Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:800px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Fabric Booking Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="120">PO Number</th>
                            <th width="100">PO Qnty</th>
                            <th width="100">Gray Required</th>
                            <th width="100">Booking Qnty</th>
                            <th width="100">Yet to booking</th>
                            <th>Booking No</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0;  
					$reqSQL = sql_select("select b.po_break_down_id, b.requirment/b.pcs as requirment, a.plan_cut_qnty
							  from wo_po_color_size_breakdown a, wo_pre_cos_fab_co_avg_con_dtls b 
							  where a.po_break_down_id in ($po_id) and a.po_break_down_id=b.po_break_down_id and a.color_number_id=b.color_number_id and a.size_number_id=b.gmts_sizes and a.is_deleted=0 and a.status_active=1 and a.po_break_down_id in ($po_id)");
					$requirment=0;
 					foreach($reqSQL as $key=>$val)
					{
						$requirment += $val[csf('requirment')]*$val[csf('plan_cut_qnty')]; // grey required					
					}
					
                    $sql= "select a.id, a.po_number, a.po_quantity, a.plan_cut, a.shipment_date, sum(b.grey_fab_qnty) as grey_fab_qnty,group_concat(distinct c.booking_no) as booking_no 
					from wo_po_break_down a, wo_booking_dtls b, wo_booking_mst c
					where a.job_no_mst='$job_number' and a.id=b.po_break_down_id and c.booking_no=b.booking_no and c.item_category in (2,13) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id in ($po_id) group by b.po_break_down_id"; 
					//echo $sql;
					$fabric_sql= sql_select($sql);
                    foreach($fabric_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="120"><p><? echo $row[csf("po_number")]; ?></p></td>
                        <td width="100" align="right"><? echo $row[csf("po_quantity")]; ?></td>
                        <td width="100" align="right"><? echo number_format($requirment,2); ?></td>
                        <td width="100" align="right"><? echo number_format($row[csf("grey_fab_qnty")],2); ?></td>
                        <? 
							$yet_to_booking = $requirment-$row[csf("grey_fab_qnty")];
						?>
                        <td width="100" align="right"><p><? echo number_format($yet_to_booking,2); ?></p></td> 
                        <td><p><? echo $row[csf("booking_no")]; ?></p></td>
                    </tr>
                    <? } ?>
                </table>
            </div> 
    </fieldset>
</div>    
<?
}


if($action=="knitting_finish_popup")
{
 	echo load_html_head_contents("Knitting Finish Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:600px">
            <table width="500">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Knitting Finish Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="150">Booking Number</th>
                            <th width="170">Booking Qnty</th>
                            <th>Production Qnty</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0;  
                    $sql= "select a.booking_no, b.po_break_down_id as po_break_down_id, sum(b.grey_fab_qnty) as grey_fab_qnty
					from wo_booking_mst a, wo_booking_dtls b
					where a.booking_no=b.booking_no and b.po_break_down_id in ($po_id) and a.item_category in (2,13) and b.status_active=1 and b.is_deleted=0 group by b.booking_no"; 
					//echo $sql;
					$fabric_sql= sql_select($sql);
					$total_booking_qnty=$totalreceive_qnty=0;
                    foreach($fabric_sql as $row)
                    {
                       // echo "select sum(c.quantity) as quantity from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and a.booking_no='".$row[csf("booking_no")]."' and a.status_active=1 and a.is_deleted=0 and b.id=c.dtls_id and c.po_breakdown_id='$po_id' and c.entry_form=2";
						$i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="150"><p><? echo $row[csf("booking_no")]; ?></p></td>
                        <td width="170" align="right"><? echo number_format($row[csf("grey_fab_qnty")],2); ?></td> 
                        <?
							$rcvQnty = return_field_value("sum(c.quantity) as quantity","inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c","a.id=b.mst_id and a.booking_no='".$row[csf("booking_no")]."' and a.status_active=1 and a.is_deleted=0 and b.id=c.dtls_id and c.po_breakdown_id='$po_id' and c.entry_form=2","quantity");
							$total_booking_qnty += $row[csf("grey_fab_qnty")];
							$totalreceive_qnty += $rcvQnty;
                        ?> 
                        <td align="right"><p><? echo number_format($rcvQnty,2); ?></p></td>
                   </tr>
                   <? } 
                   
                   //this is for independent--------------
				   $indrcvQnty = return_field_value("sum(c.quantity) as quantity","inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c","a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.id=c.dtls_id and c.po_breakdown_id='$po_id' and c.entry_form=2 and a.receive_basis=0","quantity");
                   if($indrcvQnty!=""){	
				   		$i++;
						if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
						?>
                        <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                   			<td width="35"><? echo $i; ?></td>
                            <td>Independent</td>
                            <td></td>
					   		<td align="right"><p><? echo number_format($indrcvQnty,2); ?></p></td>
                        </tr>
				   <?
                   		$totalreceive_qnty += $indrcvQnty;
						}                   
                   ?>
                   <tfoot>
                         <th colspan="2">Total</th>
                        <th><? echo number_format($total_booking_qnty,2); ?></th>
                        <th><? echo number_format($totalreceive_qnty,2); ?></th>
                    </tfoot>
                </table>
            </div> 
    </fieldset>
</div>    
<?

}


if($action=="lcsc_rcv_popup")
{
 	echo load_html_head_contents("LC/SC Receive Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:700px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>LC/SC Receive Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="150">LC/SC Number</th>
                            <th width="120">LC/SC Value</th>
                            <th width="120">Attached Value</th>
                            <th width="100">Expiry Date</th>
                            <th>Is LC</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0;  
                    $sql= "select a.contract_no, a.contract_value, a.expiry_date, sum(b.attached_value) as attached_value,'sc'
					from com_sales_contract a, com_sales_contract_order_info b
					where a.id=b.com_sales_contract_id and b.wo_po_break_down_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.contract_no,b.wo_po_break_down_id
					UNION ALL
					select a.export_lc_no, a.lc_value, a.expiry_date, sum(b.attached_value) as attached_value,'lc'
					from  com_export_lc a, com_export_lc_order_info b
					where a.id=b.com_export_lc_id and b.wo_po_break_down_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.export_lc_no,b.wo_po_break_down_id"; 
					//echo $sql;
					$fabric_sql= sql_select($sql);
					$total_booking_qnty=$totalreceive_qnty=0;
                    foreach($fabric_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="150"><p><? echo $row[csf("contract_no")]; ?></p></td>
                        <td width="120" align="right"><? echo number_format($row[csf("contract_value")],2); ?></td>
                        <td width="120" align="right"><? echo number_format($row[csf("attached_value")],2); ?></td>
                        <td width="100"><? echo $row[csf("expiry_date")]; ?></td>
                        <? if($row[csf("sc")]=="sc") $isLCSC="Sales Contact"; else $isLCSC="Export LC"; ?>
                        <td><p><? echo $isLCSC; ?></p></td>
                   </tr>
                   <? } ?>
                    
                </table>
            </div> 
    </fieldset>
</div>    
<?

}



if($action=="finish_fabric_popup")
{
 	echo load_html_head_contents("Finish Fabric Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$colorArr = return_library_array( "select id, color_name from lib_color",'id','color_name');
?>

<div style="width:100%" align="center">
	<fieldset style="width:700px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Finish Fabric Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="150">Fabric Color</th>
                            <th width="120">Booking Qnty</th>
                            <th width="120">Fin Qnty</th>
                            <th width="120">Balance</th>
                            <th> Received by Cutting</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
					$bookingCheck=array();
                    $i=0;  
                    $sql = "select group_concat(distinct a.booking_no) as booking_no,b.po_break_down_id,b.fabric_color_id, SUM(b.fin_fab_qnty) as booking_qnty
							from wo_booking_mst a, wo_booking_dtls b 
							where a.booking_no=b.booking_no and b.po_break_down_id in ($po_id) and a.booking_type!=3 and a.item_category in (2,13) and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by b.fabric_color_id";					
 					//echo $sql;
					$fabric_sql = sql_select($sql);
 					$total_booking_qnty=$total_finish_qnty=$total_balance_qnty=$total_receive_qnty=0;
                    foreach($fabric_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td> 
                        <td width="150"><? echo $colorArr[$row[csf("fabric_color_id")]]; ?></td>
                        <td width="120" align="right" title="<? echo $row[csf("booking_no")]; ?>"><? echo number_format($row[csf("booking_qnty")],2); ?></td>
                        <?   
                        	$finishRCVqnty = return_field_value("sum(c.quantity) as quantity","inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c","b.color_id='".$row[csf("fabric_color_id")]."' and c.po_breakdown_id='$po_id' and a.id=b.mst_id and b.id=c.dtls_id and a.status_active=1 and c.entry_form=7","quantity");
						?>
                        <td width="120" align="right"><? echo number_format($finishRCVqnty,2); ?></td>
                        <? $balance = $row[csf("booking_qnty")]-$finishRCVqnty; ?>
                        <td width="120" align="right"><? echo number_format($balance,2); ?></td>
                        <?
                        	$finishISSUEqnty = return_field_value("sum(quantity) as quantity","order_wise_pro_details","po_breakdown_id='$po_id' and status_active=1 and entry_form=18 and color_id='".$row[csf("fabric_color_id")]."'","quantity");
						?>
                         <td align="right"><p><? echo number_format($finishISSUEqnty,2); ?></p></td>
                   </tr>
                   <?
				   		$total_booking_qnty+=$row[csf("booking_qnty")];
						$total_finish_qnty+=$finishRCVqnty;
						$total_balance_qnty+=$balance;
						$total_receive_qnty+=$finishISSUEqnty;
				    } 
					?>
                    <tfoot>
                    	<tr>
                            <th colspan="2">Total</th>
                            <th><? echo number_format($total_booking_qnty,2); ?></th>
                            <th><? echo number_format($total_finish_qnty,2); ?></th>
                            <th><? echo number_format($total_balance_qnty,2); ?></th>
                            <th><? echo number_format($total_receive_qnty,2); ?></th>
						</tr>
                    </tfoot>
                </table>
            </div> 
    </fieldset>
</div>    
<?
}



if($action=="cutting_finish_popup")
{
 	echo load_html_head_contents("Cutting Finish Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$colorArr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sizeArr = return_library_array( "select id, size_name from  lib_size",'id','size_name');
 	
 				
	$sql = "select a.id,c.size_number_id,c.color_number_id,sum(b.production_qnty) as production_qnty
			from pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c 
			where a.id=b.mst_id and b.color_size_break_down_id=c.id and a.po_break_down_id in ($po_id) and a.production_type=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by c.size_number_id,c.color_number_id";	
	//echo $sql;die;
	$sqlResult = sql_select($sql);
	$poColorArr=$poSizeArr=$ColorSizeArr=array();
	//$ColorvalueArr=array();
	foreach($sqlResult as $row){
		$index = $row[csf("color_number_id")].$row[csf("size_number_id")];
		if( !in_array($row[csf("size_number_id")],$poSizeArr) )$poSizeArr[]=$row[csf("size_number_id")];
		if( !in_array($row[csf("color_number_id")],$poColorArr) )$poColorArr[]=$row[csf("color_number_id")]; 
		$ColorSizeArr[$index]=$row[csf("production_qnty")];
		//$ColorvalueArr[]+=$row[csf("plan_cut_qnty")];
		$mst_id=$row[csf("id")]; 
        $totalcolorqnty = return_field_value("sum(c.plan_cut_qnty) as plan_cut_qnty"," pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c"," a.id=$mst_id and a.id=b.mst_id and b.color_size_break_down_id=c.id and a.po_break_down_id in ($po_id) and a.production_type=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by c.color_number_id","plan_cut_qnty");
	}
	 
	$noSize = count($poSizeArr);
	$width = 450+($noSize*80);
	$row_total_color_qnty=0;	
	$col_total_size_qnty=array();
 			 	
?>

<div style="width:100%" align="center">
	<fieldset style="width:<? echo $width; ?>px">
            <table width="600">
                <?
                $job_sql= sql_select("select a.job_no,a.buyer_name,a.company_name,a.style_ref_no,b.po_quantity,b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.job_no='$job_number' and b.id=$po_id and a.is_deleted=0 and a.status_active=1 ");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Cutting Finish Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr>
                    <tr>
                    	<td align="right"><strong>Plan Cut</strong> :</td>
                        <td><? echo $row_job[csf("plan_cut")]; ?> </td>
                        <td></td>
                        <td></td>
                    </tr>  
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>
			
                        
            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="150">Color/Size</th>
                            <th width="100">Color Qnty</th>
                            <? foreach($poSizeArr as $val){ ?>                            
                            <th width="80"><? echo $sizeArr[$val]; ?></th>
                            <? } ?> 
                            <th width="80">Total</th>
                            <th width="">Balance</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?					
                    $i=0; $grandTotal=0;
					$noColor=count($poColorArr); 					 
                    for($k=0;$k<$noColor;$k++)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   		?>
                   		<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                       		<td width="150"><? echo $colorArr[$poColorArr[$k]]; ?></td> 
                            <td width="100" align="right"><? echo $totalcolorqnty;//$ColorvalueArr[$k];//[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
					   		<? for($j=0;$j<$noSize;$j++){ ?>
                        	<td width="80" align="right"><? echo $ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
                        	<? 
								$row_total_color_qnty+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
								$col_total_size_qnty[$poSizeArr[$j]]+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
							} 
							?> 
                        	<td width="80" align="right"><? echo $row_total_color_qnty; ?></td> 
                            <td width="" align="right"><? echo $balance= $totalcolorqnty-$row_total_color_qnty; ?></td> 
                    	</tr>
                    	<? 
							$row_total_color_qnty=0;
						} ?>   
                       <tfoot>
                       		<tr>
                            	<th colspan="2"></th>
                                <? foreach($poSizeArr as $val){ $grandTotal +=$col_total_size_qnty[$val]; ?>                            
                            	<th width="80" align="right"><? echo $col_total_size_qnty[$val]; ?></th>
                            	<? } ?> 
                                <th><? echo $grandTotal; ?></th>
                                <th><? //echo $grandTotal; ?></th>
                            </tr>
                       </tfoot>
                                       
                </table>
            </div> 
    </fieldset>
</div>    
<?
}

if($action=="print_emb_completed_popup")
{
 	echo load_html_head_contents("Print Emb Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	//$colorArr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	//$sizeArr = return_library_array( "select id, size_name from  lib_size",'id','size_name');
 	
 	
	$sqlOrder = "SELECT  a.job_no_mst,
			SUM(CASE WHEN b.emb_name=1 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS print,  
			SUM(CASE WHEN b.emb_name=2 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS emb,
			SUM(CASE WHEN b.emb_name=3 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS wash,
			SUM(CASE WHEN b.emb_name=4 and b.emb_type!=0 THEN a.po_quantity*b.cons_dzn_gmts ELSE 0 END) AS special
		FROM
			wo_po_break_down a, wo_pre_cost_embe_cost_dtls b 
		WHERE
			a.id in ($po_id) and a.job_no_mst=b.job_no";
	//echo $sqlOrder;die;		
	$sql_order=sql_select($sqlOrder);
	$reqArr=array();
	foreach($sql_order as $resultRow)
	{
		$reqArr[1] = $resultRow[csf("print")];
		$reqArr[2] = $resultRow[csf("emb")];
		$reqArr[3] = $resultRow[csf("wash")];
		$reqArr[4] = $resultRow[csf("special")]; 
	}
 	 	 	
?>

<div style="width:100%" align="center">
	<fieldset style="width:600px">
            <table width="600">
                <?
                $job_sql= sql_select("select a.job_no,a.buyer_name,a.company_name,a.style_ref_no,b.po_quantity,b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.job_no='$job_number'");
                foreach( $job_sql as $row_job);  
                ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Print & Emb. Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr>
                    <tr>
                    	<td align="right"><strong>Order Qnty</strong> :</td>
                        <td><? echo $row_job[csf("po_quantity")]; ?> </td>
                        <td></td>
                        <td></td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>
			
                        
            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="50">SL</th>
                            <th width="100">Embel. Name</th>
                            <th width="100">Required</th>
                            <th width="100">Receive</th>
                            <th width="100">Balance</th>
                            <th width="">Receive(%)</th> 
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?					
                    $i=0; 
					$sql = "select a.embel_name,c.size_number_id,c.color_number_id,sum(b.production_qnty) as production_qnty
							from pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c 
							where a.id=b.mst_id and b.color_size_break_down_id=c.id and a.po_break_down_id in ($po_id) and a.production_type=3 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.embel_name";					
					//echo $sql;die;
					$sqlResult = sql_select($sql); 
					$totalPer=0;				 
                    foreach($sqlResult as $resultRow)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   		?>
                   		<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>" >
                       		<td width="50"><? echo $i; ?></td>
                            <td width="100"><? echo $emblishment_name_array[$resultRow[csf("embel_name")]]; ?></td>
                            <td width="100" align="right"><? echo $reqArr[$resultRow[csf("embel_name")]]; ?></td>
                        	<td width="100" align="right"><? echo $resultRow[csf("production_qnty")]; ?></td> 
                            <? $balance = $reqArr[$resultRow[csf("embel_name")]]-$resultRow[csf("production_qnty")]; ?>
                            <td width="100" align="right"><? echo number_format($balance,2); ?></td>
                            <? 
								$percentage = $resultRow[csf("production_qnty")]/$reqArr[$resultRow[csf("embel_name")]]*100; 
								$totalPer += $percentage;
							?> 
                            <td width="" align="right"><? echo number_format($percentage,2)."%"; ?></td>
                    	</tr>
                    <? } ?>  
                      <tfoot>
                      		<tr>
                            	<th colspan="5"></th>
                                <th align="right"><? echo number_format($totalPer/$i,2)."%"; ?></th>
                            </tr>
                      </tfoot>                 
                </table>
            </div> 
    </fieldset>
</div>    
<?

}


if($action=="sewing_finish_popup")
{
 	echo load_html_head_contents("Sewing Finish Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$colorArr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sizeArr = return_library_array( "select id, size_name from  lib_size",'id','size_name');
 	
 				
	$sql = "select  b.id, c.size_number_id,c.color_number_id,sum(b.production_qnty) as production_qnty, c.order_quantity as color_qnty 
			from pro_garments_production_mst a, wo_po_color_size_breakdown c left join pro_garments_production_dtls b on b.color_size_break_down_id=c.id
			where a.id=b.mst_id  and a.po_break_down_id in ($po_id) and a.production_type=5 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by c.color_number_id,c.size_number_id";	
						
	//echo $sql;
	$sqlResult = sql_select($sql);
	$poColorArr=$poSizeArr=$ColorSizeArr=array();
	$ColorvalueArr=array();
	foreach($sqlResult as $row){
		$index = $row[csf("color_number_id")].$row[csf("size_number_id")];
		if( !in_array($row[csf("size_number_id")],$poSizeArr) )$poSizeArr[]=$row[csf("size_number_id")];
		if( !in_array($row[csf("color_number_id")],$poColorArr) )$poColorArr[]=$row[csf("color_number_id")];
		$ColorvalueArr[$row[csf("color_number_id")]]+=$row[csf("color_qnty")]; 
		$ColorSizeArr[$index]=$row[csf("production_qnty")];
	}
	//var_dump($ColorSizeArr);
	 //var_dump($ColorvalueArr);
	$noSize = count($poSizeArr);
	$width = 450+($noSize*80);
	$row_total_color_qnty=0;	
	$col_total_size_qnty=array();
 			 	
?>

<div style="width:100%" align="center">
	<fieldset style="width:<? echo $width; ?>px">
            <table width="600">
                <?
                //$job_sql= sql_select("select a.job_no,a.buyer_name,a.company_name,a.style_ref_no,sum(a.job_quantity) as job_quantity,b.po_quantity,b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.job_no='$job_number'");
				$job_sql= sql_select("select a.id, a.job_no,a.buyer_name,a.company_name,a.style_ref_no,sum(b.po_quantity) as po_quantity,b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id) ");//a.job_no='$job_number' group by a.job_no
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Sewing Finish Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr>
                    <tr>
                    	<td align="right"><strong>Order Qnty</strong> :</td>
                        <td><? $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];echo $row_job[csf("po_quantity")]; ?> </td>
                        <td></td>
                        <td></td>
                    </tr>  
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>
            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="150">Color/Size</th>
                            <th width="100">Color Qnty</th>
                            <? foreach($poSizeArr as $val){ ?>                            
                            <th width="80"><? echo $sizeArr[$val]; ?></th>
                            <? } ?> 
                            <th width="80">Total</th>
                            <th width="">Balance</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:470px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?					
                    $i=0; $grandTotal=0;
					$noColor=count($poColorArr); 					 
                    for($k=0;$k<$noColor;$k++)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
						
                   		?>
                   		<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                       		<td width="150"><? echo $colorArr[$poColorArr[$k]]; ?></td> 
                            <td width="100" align="right"><? echo $ColorvalueArr[$poColorArr[$k]];  $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];//[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
					   		<? for($j=0;$j<$noSize;$j++){ ?>
                        	<td width="80" align="right"><? echo $ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
                        	<? 
								$row_total_color_qnty+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
								$col_total_size_qnty[$poSizeArr[$j]]+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
							} 
							?> 
                        	<td width="80" align="right"><? echo $row_total_color_qnty; ?></td> 
                            <td width="" align="right"><? echo $balance=$ColorvalueArr[$poColorArr[$k]]-$row_total_color_qnty; ?></td> 
                    	</tr>
                    	<? 
							$row_total_color_qnty=0;
						} ?>   
                       <tfoot>
                       		<tr>
                            	<th></th>
                                <th width="80" align="right"><? echo $tot_color_qnty; ?></th>
                                <? foreach($poSizeArr as $val){ $grandTotal +=$col_total_size_qnty[$val]; ?>                            
                            	<th width="80" align="right"><? echo $col_total_size_qnty[$val]; ?></th>
                            	<? } ?> 
                                <th><? echo $grandTotal; ?></th>
                                <th><? //echo $grandTotal; ?></th>
                            </tr>
                       </tfoot>
                                       
                </table>
            </div> 
    </fieldset>
</div>    
<?

}


if($action=="finish_input_popup")
{
 	echo load_html_head_contents("Finish Input Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$colorArr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sizeArr = return_library_array( "select id, size_name from  lib_size",'id','size_name');
 	
 				
	$sql = "select  b.id, c.size_number_id,c.color_number_id,sum(b.production_qnty) as production_qnty, c.order_quantity as color_qnty 
			from pro_garments_production_mst a, wo_po_color_size_breakdown c left join pro_garments_production_dtls b on b.color_size_break_down_id=c.id
			where a.id=b.mst_id  and a.po_break_down_id in ($po_id) and a.production_type=6 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by c.color_number_id,c.size_number_id";	
						
	//echo $sql;
	$sqlResult = sql_select($sql);
	$poColorArr=$poSizeArr=$ColorSizeArr=array();
	$ColorvalueArr=array();
	foreach($sqlResult as $row){
		$index = $row[csf("color_number_id")].$row[csf("size_number_id")];
		if( !in_array($row[csf("size_number_id")],$poSizeArr) )$poSizeArr[]=$row[csf("size_number_id")];
		if( !in_array($row[csf("color_number_id")],$poColorArr) )$poColorArr[]=$row[csf("color_number_id")];
		$ColorvalueArr[$row[csf("color_number_id")]]+=$row[csf("color_qnty")]; 
		$ColorSizeArr[$index]=$row[csf("production_qnty")];
	}
	//var_dump($ColorSizeArr);
	 //var_dump($ColorvalueArr);
	$noSize = count($poSizeArr);
	$width = 450+($noSize*80);
	$row_total_color_qnty=0;	
	$col_total_size_qnty=array();
?>
    <div style="width:100%" align="center">
        <fieldset style="width:<? echo $width; ?>px">
            <table width="600">
                <?
				$job_sql= sql_select("select a.id, a.job_no,a.buyer_name,a.company_name,a.style_ref_no,sum(b.po_quantity) as po_quantity,b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id) ");//a.job_no='$job_number' group by a.job_no
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Finish Input Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr>
                    <tr>
                    	<td align="right"><strong>Order Qnty</strong> :</td>
                        <td><? $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];echo $row_job[csf("po_quantity")]; ?> </td>
                        <td></td>
                        <td></td>
                    </tr>  
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>
            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="150">Color/Size</th>
                            <th width="100">Color Qnty</th>
                            <? foreach($poSizeArr as $val){ ?>                            
                            <th width="80"><? echo $sizeArr[$val]; ?></th>
                            <? } ?> 
                            <th width="80">Total</th>
                            <th width="">Balance</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:470px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?					
                    $i=0; $grandTotal=0;
					$noColor=count($poColorArr); 					 
                    for($k=0;$k<$noColor;$k++)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
						
                   		?>
                   		<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                       		<td width="150"><? echo $colorArr[$poColorArr[$k]]; ?></td> 
                            <td width="100" align="right"><? echo $ColorvalueArr[$poColorArr[$k]];  $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];//[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
					   		<? for($j=0;$j<$noSize;$j++){ ?>
                        	<td width="80" align="right"><? echo $ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
                        	<? 
								$row_total_color_qnty+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
								$col_total_size_qnty[$poSizeArr[$j]]+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
							} 
							?> 
                        	<td width="80" align="right"><? echo $row_total_color_qnty; ?></td> 
                            <td width="" align="right"><? echo $balance=$ColorvalueArr[$poColorArr[$k]]-$row_total_color_qnty; ?></td> 
                    	</tr>
                    	<? 
							$row_total_color_qnty=0;
						} ?>   
                       <tfoot>
                       		<tr>
                            	<th></th>
                                <th width="80" align="right"><? echo $tot_color_qnty; ?></th>
                                <? foreach($poSizeArr as $val){ $grandTotal +=$col_total_size_qnty[$val]; ?>                            
                            	<th width="80" align="right"><? echo $col_total_size_qnty[$val]; ?></th>
                            	<? } ?> 
                                <th><? echo $grandTotal; ?></th>
                                <th><? //echo $grandTotal; ?></th>
                            </tr>
                       </tfoot>
                                       
                </table>
            </div> 
    </fieldset>
</div>    
<?

}

if($action=="iron_output_popup")
{
	echo load_html_head_contents("Finish Input Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$colorArr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sizeArr = return_library_array( "select id, size_name from  lib_size",'id','size_name');
 	
 				
	$sql = "select  b.id, c.size_number_id,c.color_number_id,sum(b.production_qnty) as production_qnty, c.order_quantity as color_qnty 
			from pro_garments_production_mst a, wo_po_color_size_breakdown c left join pro_garments_production_dtls b on b.color_size_break_down_id=c.id
			where a.id=b.mst_id  and a.po_break_down_id in ($po_id) and a.production_type=7 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by c.color_number_id,c.size_number_id";	
						
	//echo $sql;
	$sqlResult = sql_select($sql);
	$poColorArr=$poSizeArr=$ColorSizeArr=array();
	$ColorvalueArr=array();
	foreach($sqlResult as $row){
		$index = $row[csf("color_number_id")].$row[csf("size_number_id")];
		if( !in_array($row[csf("size_number_id")],$poSizeArr) )$poSizeArr[]=$row[csf("size_number_id")];
		if( !in_array($row[csf("color_number_id")],$poColorArr) )$poColorArr[]=$row[csf("color_number_id")];
		$ColorvalueArr[$row[csf("color_number_id")]]+=$row[csf("color_qnty")]; 
		$ColorSizeArr[$index]=$row[csf("production_qnty")];
	}
	$noSize = count($poSizeArr);
	$width = 450+($noSize*80);
	$row_total_color_qnty=0;	
	$col_total_size_qnty=array();
	?>

    <div style="width:100%" align="center">
        <fieldset style="width:<? echo $width; ?>px">
            <table width="600">
                <?
				$job_sql= sql_select("select a.id, a.job_no,a.buyer_name,a.company_name,a.style_ref_no,sum(b.po_quantity) as po_quantity,b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id) ");//a.job_no='$job_number' group by a.job_no
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Finish Input Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr>
                    <tr>
                    	<td align="right"><strong>Order Qnty</strong> :</td>
                        <td><? $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];echo $row_job[csf("po_quantity")]; ?> </td>
                        <td></td>
                        <td></td>
                    </tr>  
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>
            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="150">Color/Size</th>
                            <th width="100">Color Qnty</th>
                            <? foreach($poSizeArr as $val){ ?>                            
                            <th width="80"><? echo $sizeArr[$val]; ?></th>
                            <? } ?> 
                            <th width="80">Total</th>
                            <th width="">Balance</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:470px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?					
                    $i=0; $grandTotal=0;
					$noColor=count($poColorArr); 					 
                    for($k=0;$k<$noColor;$k++)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
						
                   		?>
                   		<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                       		<td width="150"><? echo $colorArr[$poColorArr[$k]]; ?></td> 
                            <td width="100" align="right"><? echo $ColorvalueArr[$poColorArr[$k]];  $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];//[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
					   		<? for($j=0;$j<$noSize;$j++){ ?>
                        	<td width="80" align="right"><? echo $ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
                        	<? 
								$row_total_color_qnty+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
								$col_total_size_qnty[$poSizeArr[$j]]+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
							} 
							?> 
                        	<td width="80" align="right"><? echo $row_total_color_qnty; ?></td> 
                            <td width="" align="right"><? echo $balance=$ColorvalueArr[$poColorArr[$k]]-$row_total_color_qnty; ?></td> 
                    	</tr>
                    	<? 
							$row_total_color_qnty=0;
						} ?>   
                       <tfoot>
                       		<tr>
                            	<th></th>
                                <th width="80" align="right"><? echo $tot_color_qnty; ?></th>
                                <? foreach($poSizeArr as $val){ $grandTotal +=$col_total_size_qnty[$val]; ?>                            
                            	<th width="80" align="right"><? echo $col_total_size_qnty[$val]; ?></th>
                            	<? } ?> 
                                <th><? echo $grandTotal; ?></th>
                                <th><? //echo $grandTotal; ?></th>
                            </tr>
                       </tfoot>
                                       
                </table>
            </div> 
    </fieldset>
</div>    
<?
}

if($action=="finish_completed_popup")
{
	echo load_html_head_contents("Finish Input Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
	$colorArr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sizeArr = return_library_array( "select id, size_name from  lib_size",'id','size_name');
 	
 				
	$sql = "select  b.id, c.size_number_id,c.color_number_id,sum(b.production_qnty) as production_qnty, c.order_quantity as color_qnty 
			from pro_garments_production_mst a, wo_po_color_size_breakdown c left join pro_garments_production_dtls b on b.color_size_break_down_id=c.id
			where a.id=b.mst_id  and a.po_break_down_id in ($po_id) and a.production_type=8 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by c.color_number_id,c.size_number_id";	
						
	//echo $sql;
	$sqlResult = sql_select($sql);
	$poColorArr=$poSizeArr=$ColorSizeArr=array();
	$ColorvalueArr=array();
	foreach($sqlResult as $row){
		$index = $row[csf("color_number_id")].$row[csf("size_number_id")];
		if( !in_array($row[csf("size_number_id")],$poSizeArr) )$poSizeArr[]=$row[csf("size_number_id")];
		if( !in_array($row[csf("color_number_id")],$poColorArr) )$poColorArr[]=$row[csf("color_number_id")];
		$ColorvalueArr[$row[csf("color_number_id")]]+=$row[csf("color_qnty")]; 
		$ColorSizeArr[$index]=$row[csf("production_qnty")];
	}
	$noSize = count($poSizeArr);
	$width = 450+($noSize*80);
	$row_total_color_qnty=0;	
	$col_total_size_qnty=array();
	?>

    <div style="width:100%" align="center">
        <fieldset style="width:<? echo $width; ?>px">
            <table width="600">
                <?
				$job_sql= sql_select("select a.id, a.job_no,a.buyer_name,a.company_name,a.style_ref_no,sum(b.po_quantity) as po_quantity,b.plan_cut from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id) ");//a.job_no='$job_number' group by a.job_no
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Finish Input Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr>
                    <tr>
                    	<td align="right"><strong>Order Qnty</strong> :</td>
                        <td><? $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];echo $row_job[csf("po_quantity")]; ?> </td>
                        <td></td>
                        <td></td>
                    </tr>  
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>
            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="150">Color/Size</th>
                            <th width="100">Color Qnty</th>
                            <? foreach($poSizeArr as $val){ ?>                            
                            <th width="80"><? echo $sizeArr[$val]; ?></th>
                            <? } ?> 
                            <th width="80">Total</th>
                            <th width="">Balance</th>
                         </tr>   
                    </thead>
                </table>
            </div>
            <div style="width:100%; max-height:470px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?					
                    $i=0; $grandTotal=0;
					$noColor=count($poColorArr); 					 
                    for($k=0;$k<$noColor;$k++)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
						
                   		?>
                   		<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                       		<td width="150"><? echo $colorArr[$poColorArr[$k]]; ?></td> 
                            <td width="100" align="right"><? echo $ColorvalueArr[$poColorArr[$k]];  $tot_color_qnty+=$ColorvalueArr[$poColorArr[$k]];//[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
					   		<? for($j=0;$j<$noSize;$j++){ ?>
                        	<td width="80" align="right"><? echo $ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]]; ?></td> 
                        	<? 
								$row_total_color_qnty+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
								$col_total_size_qnty[$poSizeArr[$j]]+=$ColorSizeArr[$poColorArr[$k].$poSizeArr[$j]];
							} 
							?> 
                        	<td width="80" align="right"><? echo $row_total_color_qnty; ?></td> 
                            <td width="" align="right"><? echo $balance=$ColorvalueArr[$poColorArr[$k]]-$row_total_color_qnty; ?></td> 
                    	</tr>
                    	<? 
							$row_total_color_qnty=0;
						} ?>   
                       <tfoot>
                       		<tr>
                            	<th></th>
                                <th width="80" align="right"><? echo $tot_color_qnty; ?></th>
                                <? foreach($poSizeArr as $val){ $grandTotal +=$col_total_size_qnty[$val]; ?>                            
                            	<th width="80" align="right"><? echo $col_total_size_qnty[$val]; ?></th>
                            	<? } ?> 
                                <th><? echo $grandTotal; ?></th>
                                <th><? //echo $grandTotal; ?></th>
                            </tr>
                       </tfoot>
                                       
                </table>
            </div> 
    </fieldset>
</div>    
<?

}


if($action=="buyer_inspection_popup")
{
 	echo load_html_head_contents("Buyer Inspection Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:700px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Buyer Inspection Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>
            <div style="width:100%">
            	<p><b>QC Passed</b></p>
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="35">SL</th>
                            <th width="120">Inspection Date</th>
                            <th width="120">Inspection Qnty</th>
                            <th width="120">Inspection Status</th>
                            <th width="">Remarks</th> 
                         </tr>   
                    </thead>
                </table>
            </div>
            
            <div style="width:100%; max-height:150px; overflow-y:scroll">            	
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1"> 
                    <?
                    $i=0;  
                    $sql= "select inspection_date,sum(inspection_qnty) as inspection_qnty,inspection_status,inspection_cause,comments 	
					from pro_buyer_inspection
					where po_break_down_id in ($po_id) and status_active=1 and is_deleted=0"; 
					//echo $sql;
					$fabric_sql= sql_select($sql);
					$total_qnty=$total_pass_qnty=0;
                    foreach($fabric_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
						if($row[csf("inspection_status")]==1)
						{
						   ?>
						   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
								<td width="35"><? echo $i; ?></td>
								<td width="120"><? echo change_date_format($row[csf("inspection_date")]); ?></td>
								<td width="120" align="right"><? echo $row[csf("inspection_qnty")]; ?></td>
								<td width="120"><p><? echo $inspection_status[$row[csf("inspection_status")]]; ?></p></td>  
								<td><p><? echo $row[csf("comments")]; ?></p></td>
						   </tr>
						   <? 
							$total_pass_qnty+=$row[csf("inspection_qnty")];
						}
						
				   } ?>
                   <tfoot>
                   		<tr>
                        	<th></th><th>Total</th>
                            <th><? echo $total_pass_qnty; ?></th>
                            <th></th><th></th>
                        </tr> 
                   </tfoot> 
                   
                </table>
             </div>
             
             <br /> 
             
             <div style="width:100%">
             	<p><b>QC Failed Or Re-Checked</b></p>
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="35">SL</th>
                            <th width="120">Inspection Date</th>
                            <th width="120">Inspection Qnty</th>
                            <th width="120">Inspection Status</th>
                            <th width="">Remarks</th> 
                         </tr>   
                    </thead>
                </table>
            </div>
            
            <div style="width:100%; max-height:150px; overflow-y:scroll"> 
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">                	
                    <?
                    $i=0;  
                    foreach($fabric_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
						if($row[csf("inspection_status")]!=1)
						{
						   ?>
						   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
								<td width="35"><? echo $i; ?></td>
								<td width="120"><? echo change_date_format($row[csf("inspection_date")]); ?></td>
								<td width="120" align="right"><? echo $row[csf("inspection_qnty")]; ?></td>
								<td width="120"><p><? echo $inspection_status[$row[csf("inspection_status")]]; ?></p></td>  
								<td><p><? echo $row[csf("comments")]; ?></p></td>
						   </tr>
						   <? 
							$total_qnty+=$row[csf("inspection_qnty")];
						}
										   		
 				   	} 
				   ?>
                   <tfoot>
                   		<tr>
                        	<th></th><th>Total</th>
                            <th><? echo $total_qnty; ?></th>
                            <th></th><th></th>
                        </tr> 
                   </tfoot>  
                </table>
            </div> 
    </fieldset>
</div>    
<?

}

if($action=="ex_factory_popup")
{
 	echo load_html_head_contents("Ship Quantity Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:700px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  // Master Job  table queery ends here
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>LC/SC Receive Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="35">SL</th>
                            <th width="120">Ex-Fac. Date</th>
                            <th width="120">Ex-Fac. Qnty</th>
                            <th width="120">Challan No</th>
                            <th width="">Remarks</th> 
                         </tr>   
                    </thead> 	 	
                </table>
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0;  
                    $sql= "select challan_no,ex_factory_date,sum(ex_factory_qnty) as ex_factory_qnty,remarks 	
					from pro_ex_factory_mst
					where po_break_down_id in ($po_id) and status_active=1 and is_deleted=0"; 
					//echo $sql;
					$fabric_sql= sql_select($sql);
					$total_qnty=0;
                    foreach($fabric_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <td width="120"><? echo change_date_format($row[csf("ex_factory_date")]); ?></td>
                        <td width="120" align="right"><? echo $row[csf("ex_factory_qnty")]; ?></td>
                        <td width="120"><p><? echo $row[csf("challan_no")]; ?></p></td>  
                        <td><p><? echo $row[csf("remarks")]; ?></p></td>
                   </tr>
                   <? 
				   		$total_qnty+=$row[csf("ex_factory_qnty")];
 				   } ?>
                   <tfoot>
                   		<tr>
                        	<th></th><th>Total</th>
                            <th><? echo $total_qnty; ?></th>
                            <th></th><th></th>
                        </tr> 
                   </tfoot> 
                   
                </table>
            </div> 
    </fieldset>
</div>    
<?

}



if($action=="actual_shipment_popup")
{
 	echo load_html_head_contents("Actual Shipment Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:800px">
            <table width="600">
                <?
                $job_sql= sql_select("select job_no,buyer_name,company_name,style_ref_no from wo_po_details_master where job_no='$job_number'");
                foreach( $job_sql as $row_job);  
                 ?>
                    <tr class="form_caption">
                        <td align="center" colspan="4"><strong>Actual Shipment Details</strong></td>	
                    </tr>
                    <tr>
                        <td align="right" width="130"> <strong>Job Number</strong> :</td> 
                        <td width="200"><? echo $job_number; ?></td> 
                        <td align="right"  width="130"><strong>Buyer Name</strong> :</td>  
                        <td><? echo $buyer_short_name_arr[$row_job[csf("buyer_name")]]; ?></td> 
                    </tr>
                    <tr>
                        <td align="right"><strong>Company Name</strong> :</td> 
                        <td><? echo $company_short_name_arr[$row_job[csf("company_name")]]; ?></td> 
                        <td align="right"><strong>Style Ref No</strong> : </td> 
                        <td><? echo $row_job[csf("style_ref_no")]; ?> </td>
                    </tr> 
                    <tr>
                        <td colspan="4" height="15"></td>	                
                    </tr>            
            </table>

            <div style="width:100%">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr> 
                            <th width="35">SL</th>
                            <th width="100">Order Number</th>
                            <th width="100">Order Qty</th>
                            <th width="100">Shipment date</th>
                            <th width="100">Invoice No</th>
                            <th width="100">Actual Ship date</th>
                            <th width="100">Invoice Qty</th>
                            <th width="">Invoice Value</th> 
                         </tr>   
                    </thead> 	 	
                </table> 
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0; 
					
                    $sql= "select a.po_breakdown_id,b.pub_shipment_date,c.invoice_no,a.current_invoice_qnty,a.current_invoice_value, c.actual_shipment_date 
					from com_export_invoice_ship_dtls a, wo_po_break_down b,com_export_invoice_ship_mst c 
					where a.po_breakdown_id in ($po_id) and a.po_breakdown_id=b.id and a.mst_id=c.id and c.is_deleted=0 and c.status_active=1"; 
					//echo $sql;die;
					$fabric_sql= sql_select($sql);
					$total_qnty=$total_value=0;
                    foreach($fabric_sql as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td>
                        <?
                        	$poSQL=sql_select("select b.po_number,b.po_quantity as order_qnty from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=".$row[csf("po_breakdown_id")]);
							foreach($poSQL as $res);
						?>
                        <td width="100"><? echo $res[csf("po_number")]; ?></td>  
                        <td width="100" align="right"><? echo $res[csf("order_qnty")]; ?></td>  
                        <td width="100"><? echo change_date_format($row[csf("pub_shipment_date")]); ?></td>
                        <td width="100"><? echo $row[csf("invoice_no")]; ?></td>  
                        <td width="100"><? echo change_date_format($row[csf("actual_shipment_date")]); ?></td>
                        <td width="100" align="right"><? echo $row[csf("current_invoice_qnty")]; ?></td>  
                        <td align="right"><? echo $row[csf("current_invoice_value")]; ?></td>
                   </tr>
                   <? 
				   		$total_qnty+=$row[csf("current_invoice_qnty")];
						$total_value+=$row[csf("current_invoice_value")];
 				   } ?>
                   <tfoot>
                   		<tr>
                        	<th colspan="5"></th><th>Total</th>
                            <th><? echo $total_qnty; ?></th>
                            <th><? echo $total_value; ?></th> 
                        </tr> 
                   </tfoot> 
                   
                </table>
            </div> 
    </fieldset>
</div>    
<?

}


if($action=="submit_date_popup")
{
 	echo load_html_head_contents("Submit Date Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:650px"> 
    		<div class="form_caption" align="center">
                  <strong>Document Submission Details</strong>
            </div><br />
            <div style="width:100%"> 
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="150">Invoice No</th>
                            <th width="150">Bill No</th>
                            <th width="150">Submission Date</th>
                            <th width="">Submission Type</th> 
                         </tr>   
                    </thead> 	 	
                </table> 
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <?
                    $i=0; 
					
                    $sql="select d.submit_date,d.submit_type,a.invoice_no,d.bank_ref_no
						from com_export_invoice_ship_mst a, com_export_invoice_ship_dtls b, com_export_doc_submission_invo c, com_export_doc_submission_mst d
						where b.po_breakdown_id in ($po_id) and a.id=b.mst_id and a.id=c.invoice_id and c.doc_submission_mst_id=d.id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1";														
					//echo $sql;die ;
					$sqlRes = sql_select($sql);	
					$total_qnty=$total_value=0;
                    foreach($sqlRes as $row)
                    {
                        $i++;
                        if($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF"; 
                   ?>
                   <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="35"><? echo $i; ?></td> 
                        <td width="150"><? echo $row[csf("invoice_no")]; ?></td>  
                        <td width="150"><? echo $row[csf("bank_ref_no")]; ?></td>  
                        <td width="150"><? echo change_date_format($row[csf("submit_date")]); ?></td> 
                        <td><? echo $submission_type[$row[csf("submit_type")]]; ?></td>
                   </tr> 
                   <? } ?>
                </table>
            </div> 
    </fieldset>
</div>    
<?

}


if($action=="proceed_realize_popup")
{
 	echo load_html_head_contents("Proceed Realize Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:680px"> 
    		<div class="form_caption" align="center">
                  <strong>Proceed Realization Details</strong>
            </div><br />
            <div style="width:100%"> 
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                            <th width="35">SL</th>
                            <th width="100">Bill No</th>
                            <th width="250">Invoice No</th>
                            <th width="100">Realized Amnt</th>
                            <th width="100">Short Realized Amnt</th>
                            <th>Realized Date</th>
                         </tr>   
                    </thead> 	 	
                </table>  
            </div>
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" cellspacing="0" border="1" rules="all" class="rpt_table">
					<?
                    $i=1; 
					$tot_realized_amnt=0;
					$tot_short_realized_amnt=0;
                    $sql="select a.bank_ref_no, a.id from com_export_doc_submission_mst a, com_export_doc_submission_invo b, com_sales_contract_order_info c where a.id=b.doc_submission_mst_id and b.lc_sc_id=c.com_sales_contract_id and b.is_lc=2 and c.wo_po_break_down_id in ($po_id) AND a.status_active =1 AND b.is_deleted=0 AND b.status_active =1 AND c.is_deleted=0 AND c.status_active =1 group by a.id order by a.bank_ref_no";
                    //echo $sql;
					$result=sql_select($sql);
                    foreach($result as $row)
                    {
                        $invoice_no="";
                        $sql_inv="select a.invoice_no from com_export_invoice_ship_mst a, com_export_doc_submission_invo b where a.id=b.invoice_id and b.doc_submission_mst_id='".$row[csf("id")]."' AND a.is_deleted=0  AND a.status_active =1 AND b.is_deleted=0 AND b.status_active =1 group by a.id order by a.invoice_no";
 						$res_inv=sql_select($sql_inv);
                        foreach($res_inv as $row_inv)
                        {
                            if($invoice_no=="") $invoice_no=$row_inv[csf("invoice_no")]; else $invoice_no.=", ".$row_inv[csf("invoice_no")];
                        }
                        $sql_real="select a.received_date, 
							sum(case when b.type=1 then b.document_currency end) as realized_value,
							sum(case when b.type=0 then b.document_currency end) as short_realized_value 
							from com_export_proceed_realization a, com_export_proceed_rlzn_dtls b where a.id=b.mst_id and a.invoice_bill_id='".$row[csf("id")]."' and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.id order by a.received_date";
 						$res_real=sql_select($sql_real);
                        foreach($res_real as $row_real)
                        {
                             if ($i%2==0)  
                                $bgcolor="#EFEFEF";
                            else
                                $bgcolor="#FFFFFF";
                        ?>
                            <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_s<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_s<? echo $i; ?>">
                                <td width="35"><? echo $i; ?></td> 
                                <td width="100"><? echo $row[csf("bank_ref_no")]; ?></td>
                                <td width="250"><p><? echo $invoice_no; ?></p></td>
                                <td width="100" align="right"><? echo number_format($row_real[csf("realized_value")],2); $tot_realized_amnt+=$row_real[csf("realized_value")]; ?></td>
                                <td width="100" align="right"><? echo number_format($row_real[csf("short_realized_value")],2); $tot_short_realized_amnt+=$row_real[csf("short_realized_value")]; ?></td>
                                <td><? echo change_date_format($row_real[csf("received_date")]); ?></td>
                            </tr>
                        <?	
                     $i++; 
                     }	
                    }
                    $i=1; 
                    $sql2="select a.bank_ref_no, a.id from com_export_doc_submission_mst a, com_export_doc_submission_invo b, com_export_lc_order_info c where a.id=b.doc_submission_mst_id and b.lc_sc_id=c.com_export_lc_id and b.is_lc=1 and c.wo_po_break_down_id in ($po_id) AND a.is_deleted=0  AND a.status_active=1 AND b.is_deleted=0 AND b.status_active=1 AND c.is_deleted=0 AND c.status_active=1 group by a.id order by a.bank_ref_no";
 					$result2=sql_select($sql2);
                    foreach($result2 as $row2)
                    {
                        if ($i%2==0)  $bgcolor="#EFEFEF"; 
                        else $bgcolor="#FFFFFF";
                           
                        $invoice_no="";
                        $sql_inv="select a.invoice_no from com_export_invoice_ship_mst a, com_export_doc_submission_invo b where a.id=b.invoice_id and b.doc_submission_mst_id='".$row2[csf("id")]."' AND a.is_deleted=0 AND a.status_active=1 AND b.is_deleted=0 AND b.status_active =1 group by a.id order by a.invoice_no";
 						$res_inv=sql_select($sql_inv);
                    	foreach($res_inv as $row_inv)
                        {
                            if($invoice_no=="") $invoice_no=$row_inv[invoice_no]; else $invoice_no.=", ".$row_inv[invoice_no];
                        }
                        $sql_real="select a.received_date, 
						sum(case when b.type=1 then b.document_currency end) as realized_value,
						sum(case when b.type=0 then b.document_currency end) as short_realized_value 
						from com_export_proceed_realization a, com_export_proceed_rlzn_dtls b where a.id=b.mst_id and a.invoice_bill_id='".$row2[csf("id")]."' and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.id order by a.received_date";
 						$res_real=sql_select($sql_real);
						foreach($res_real as $row_real)
                        {
                            if ($i%2==0) $bgcolor="#EFEFEF";
                            else $bgcolor="#FFFFFF";
                               
                         ?>
                            <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_l<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_l<? echo $i; ?>">
                                <td width="35"><? echo $i; ?></td> 
                                <td width="100"><? echo $row2[csf("bank_ref_no")]; ?></td>
                                <td width="250"><p><? echo $invoice_no; ?></p></td>
                                <td width="100" align="right"><? echo number_format($row_real[csf("realized_value")],2); $tot_realized_amnt+=$row_real[csf("realized_value")]; ?></td>
                                <td width="100" align="right"><? echo number_format($row_real[csf("short_realized_value")],2); $tot_short_realized_amnt+=$row_real[csf("short_realized_value")]; ?></td>
                                <td><? echo change_date_format($row_real[csf("received_date")]); ?></td> 
                            </tr>
                        <?	
                        $i++;
                        }
                    }
                    ?>
                        <tfoot>
                            <th colspan="3">Total</th>
                            <th><? echo number_format($tot_realized_amnt,2); ?></th>
                            <th><? echo number_format($tot_short_realized_amnt,2); ?></th>
                            <th>&nbsp;</th>
                        </tfoot>
                    </table>
            </div> 
    </fieldset>
</div>    
<?

}

if($action=="order_number_popup")
{
 	echo load_html_head_contents("Order Number Details", "../../../../", 1, 1,$unicode,'','');
	
	$expData=explode('_',$job_number);
	$job_number = $expData[0];
	$po_id = $expData[1];
?>

<div style="width:100%" align="center">
	<fieldset style="width:680px"> 
        <div class="form_caption" align="center">
        <strong>Order Details</strong>
        </div><br />
        <div style="width:100%"> 
            <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                <thead>
                    <tr>
                        <th width="35">SL</th>
                        <th width="100">Order Number</th>
                        <th width="100">Pub Ship. Date</th>
                        <th width="100">PO Quantity</th>
                        <th width="100">Plan Cut</th> 	
                        <th width="100">Unit Prince</th>                            
                        <th width="">Total Price</th>
                     </tr>   
                </thead> 	 	
            </table>  
        </div>
        <div style="width:100%; max-height:270px; overflow-y:scroll">
            <table cellpadding="0" width="100%" cellspacing="0" border="1" rules="all" class="rpt_table">
                <?
                $i=1; 
                $job_po_qnty=0;$job_plan_qnty=0;$job_total_price=0;  
                $jobs_sql= execute_query("select id,job_no_mst,pub_shipment_date,po_number,po_quantity,unit_price,plan_cut,po_total_price from wo_po_break_down where job_no_mst='$job_number' and id in ($po_id)");
                //print_r($jobs_sql);					 
                //foreach( $jobs_sql as $row2); 
                while($row2=mysql_fetch_assoc($jobs_sql))
                { 
                        if ($i%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";                               
                     ?>
                        <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_l<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_l<? echo $i; ?>">
                            <td width="35"><? echo $i; ?></td> 
                            <td width="100"><? echo $row2[csf("po_number")]; ?></td>
                            <td width="100"><? echo change_date_format($row2[csf("pub_shipment_date")]); ?></td> 
                            <td width="100" align="right"><? echo $row2[csf("po_quantity")]; ?></td>
                            <td width="100" align="right"><? echo $row2[csf("plan_cut")]; ?></td>
                            <td width="100" align="right"><? echo number_format($row2[csf("unit_price")],2); ?></td>
                            <td width="" align="right"><? echo number_format($row2[csf("po_total_price")],2); ?></td>
                        </tr>
                    <? 
                    $job_po_qnty+=$row2[csf("po_quantity")];
                    $job_plan_qnty+=$row2[csf("plan_cut")];
                    $job_total_price+=$row2[csf("po_total_price")];
                    $i++;
                }
                ?>
                    <tfoot>
                        <th colspan="3">Total</th>
                        <th><? echo number_format($job_po_qnty,2); ?></th>
                        <th><? echo number_format($job_plan_qnty,2); ?></th> 
                        <th>&nbsp;</th>
                        <th><? echo number_format($job_total_price,2); ?></th> 
                    </tfoot>
                </table>
            </div> 
    </fieldset>
</div>    
<?
}

if($action=="trims_rec_popup")
{
 	echo load_html_head_contents("Trims Receive Details", "../../../../", 1, 1,$unicode,'','');
	extract($_REQUEST);
	//echo $po_id;
?>

<div style="width:100%" align="center">
	<fieldset style="width:600px"> 
    		<div class="form_caption" align="center"><strong>Trims Receive Details</strong></div><br />
            <?
				$supplier_library=return_library_array( "select id,supplier_name from lib_supplier", "id", "supplier_name"  ); 
			?>
			
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                        <tr>
                        	<th width="30">SL</th>
                        	<th width="160">Trims Name</th>
                            <th width="60">UoM</th>
                            <th width="80">Req. Qnty.</th>
                            <th width="80">Rec. Qnty.</th>
                            <th width="80">Rec. %</th>
                            <th width="80">Bal. Qnty.</th>
                         </tr>   
                    </thead> 	 	
                </table>  
           
            <div style="width:100%; max-height:270px; overflow-y:scroll">
                <table cellpadding="0" width="100%" cellspacing="0" border="1" rules="all" class="rpt_table">
					<?
                    $i=1; 
					$trim_group= return_library_array( "select id, item_name from lib_item_group",'id','item_name');
					$qnty_sql="select a.id, a.po_id, a.po_number, a.trim_group, a.req_qnty, a.cons_uom, sum(b.receive_qnty) as receive_qnty  from wo_trim_booking_data_park a,inv_trims_entry_dtls b, order_wise_pro_details c where a.po_id='$po_id' and a.trim_group=b.item_group_id and b.id=c.dtls_id and c.po_breakdown_id=a.po_id group by a.trim_group order by trim_group";
					//echo $qnty_sql;
					$sql_dtls=sql_select($qnty_sql);
					foreach($sql_dtls as $row_real)
                    { 
					
                         if ($i%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";                               
                         ?>
                            <tr bgcolor="<? echo $bgcolor; ?>" id="tr_<? echo $i; ?>">
                                <td width="30"><? echo $i; ?></td> 
                                <td width="160"><? echo $trim_group[$row_real[csf("trim_group")]]; ?></td>
                                <td width="60" align="center"><? echo $unit_of_measurement[$row_real[csf("cons_uom")]]; ?></td>
                                <td width="80" align="right"><? echo number_format($row_real[csf("req_qnty")],2); ?></td>
                                <?
									
									$rec_per=($row_real[csf("receive_qnty")]/$row_real[csf("req_qnty")])*100;
									$bal_qnty=$row_real[csf("req_qnty")]-$row_real[csf("receive_qnty")];
								?>
                                <td width="80"><? echo $row_real[csf("receive_qnty")]; ?></td>
                                <td width="80" align="right"><? echo number_format($rec_per,2)." %"; ?></td>
                                <td width="80" align="right"><? echo number_format($bal_qnty,2); ?></td>
                            </tr>
                        <? 
                        $i++;
                    }
                    ?>
                    </table>
            </div> 
    </fieldset>
</div>    
<?
}
//######################################## ALL POP UP Here END ################################
//#############################################################################################


?>	
