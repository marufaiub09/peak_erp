<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Recipe Entry
Functionality	:	
JS Functions	:
Created by		:	Fuad 
Creation date 	: 	21.07.2013
Updated by 		: 		
Update date		:    
QC Performed BY	:		
QC Date			:	
Comments		:



*/
session_start(); 
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Recipe Entry Info", "../", 1, 1,$unicode,1);

?>	
<script>

	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
	var permission='<?php echo $permission; ?>';
	
	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];

	$(document).ready(function(e)
	{
          $("#txt_color").autocomplete({
			 source: str_color
		  });
    });

	

	function show_details()
	{
		var val=$('#copy_sub_process').val();
		//alert(val);return;
		
		var cbo_company_id = $('#cbo_company_id').val();
		var cbo_sub_process = $('#cbo_sub_process').val();
		
		if(form_validation('cbo_company_id*cbo_sub_process','Company*Sub Process')==false)
		{
			return;
		}
		if(val==2)
		{
			var list_view_orders = return_global_ajax_value( cbo_company_id+'**'+cbo_sub_process, 'item_details', '', 'requires/recipe_entry_controller');
			if(list_view_orders!='')
			{
				//$("#tbl_list tr").remove();
				$("#list_container_recipe_items").html(list_view_orders);
				setFilterGrid('tbl_list_search',-1);
			}
		}
		else
		{
			alert("Execute Not Allowed.");
			return;
		}
		//show_list_view(cbo_company_id+'**'+cbo_sub_process, 'item_details', 'list_container_recipe_items', 'requires/recipe_entry_controller','setFilterGrid("tbl_list_search",-1)');
		//set_button_status(0, permission, 'fnc_recipe_entry',1);	
	}

	function color_row(tr_id)
	{
		if(form_validation('cbo_dose_base_'+tr_id,'Dose Base')==false)
		{
			$('#txt_ratio_'+tr_id).val('');
			return;
		}
		else
		{
			var txt_ratio=$('#txt_ratio_'+tr_id).val()*1;
			if(txt_ratio>0)
			{
				$('#search' + tr_id).css('background-color','yellow');
			}
			else
			{
				$('#search' + tr_id).css('background-color','#FFFFCC');
			}
		}
	}
			
	function fnc_recipe_entry(operation)
	{
		if(operation==4)
		{
			var report_title=$( "div.form_caption" ).html();
			print_report( $('#cbo_company_id').val()+'*'+$('#update_id').val()+'*'+$('#txt_labdip_no').val()+'*'+report_title, "recipe_entry_print", "requires/recipe_entry_controller") 
			//return;
			show_msg("3");
		}
		else if(operation==0 || operation==1 || operation==2)
		{
			if(operation==2)
			{
				show_msg('13');
				return;
			}
			
			if( form_validation('txt_labdip_no*txt_batch_no*cbo_company_id*txt_recipe_date*cbo_order_source*txt_color*txt_liquor','Labdip No*Batch*Company*Recipe Date*Order Source*Color*Total Liquor')==false )
			{
				return;
			}
			var copy_val=$('#copy_id').val();
			if(copy_val==2)
			{
				if( $('#list_container_recipe_items').html() == "")
				{
					alert("Please Select Item");
					return;
				}
				
				var row_num=$('#tbl_list_search tbody tr').length-1;
				//alert (row_num);return;
				var data_all=""; var i=0;
		
				for(var j=1; j<=row_num; j++)
				{
					var txt_ratio=$('#txt_ratio_'+j).val();
					var updateIdDtls=$('#updateIdDtls_'+j).val();
					var dose=$('#cbo_dose_base_'+j).val();
					/*if(updateIdDtls!="")
					{
						if (form_validation('txt_ratio_'+j,'Ratio')==false)
						{
							return;
						}
					}*/
					
					if(updateIdDtls!="" || txt_ratio*1>0)
					{
						if (form_validation('cbo_dose_base_'+j,'Dose Base')==false)
						{
							return;
						}
						
						i++;
						data_all+="&txt_seqno_" + i + "='" + $('#txt_seqno_'+j).val()+"'"+"&product_id_" + i + "='" + $('#product_id_'+j).text()+"'"+"&txt_item_lot_" + i + "='" + $('#txt_item_lot_'+j).val()+"'"+"&cbo_dose_base_" + i + "='" + $('#cbo_dose_base_'+j).val()+"'"+"&txt_ratio_" + i + "='" + $('#txt_ratio_'+j).val()+"'"+"&updateIdDtls_" + i + "='" + $('#updateIdDtls_'+j).val()+"'";
		
					}
				}
				//alert (data_all);return;
				if(i<1)
				{
					alert("Please Insert Ratio At Least One Item");
					return;
				}
				
				var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_labdip_no*cbo_company_id*cbo_location*txt_recipe_date*cbo_order_source*txt_booking_order*txt_booking_id*txt_recipe_des*txt_batch_id*cbo_buyer_name*cbo_method*txt_color*cbo_color_range*txt_liquor*txt_batch_ratio*txt_liquor_ratio*txt_booking_type*cbo_sub_process*txt_remarks*copy_id*update_id',"../")+data_all+'&total_row='+i;
			}
			else if(copy_val==1)
			{
				var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_labdip_no*cbo_company_id*cbo_location*txt_recipe_date*cbo_order_source*txt_booking_order*txt_booking_id*txt_recipe_des*txt_batch_id*cbo_buyer_name*cbo_method*txt_color*cbo_color_range*txt_liquor*txt_batch_ratio*txt_liquor_ratio*txt_booking_type*cbo_sub_process*txt_remarks*copy_id*update_id_check',"../");
			}
			//alert (data);return;
			freeze_window(operation);
			http.open("POST","requires/recipe_entry_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange=fnc_recipe_entry_Reply_info;
		}
	}
	
	function fnc_recipe_entry_Reply_info()
	{
		if(http.readyState == 4) 
		{
			//alert(http.responseText);
			var reponse=trim(http.responseText).split('**');	

			show_msg(trim(reponse[0]));
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_sys_id').value = reponse[1];
				document.getElementById('update_id_check').value = reponse[1];
				show_list_view(reponse[1], 'recipe_item_details', 'recipe_items_list_view', 'requires/recipe_entry_controller', '' ) ;
				setFilterGrid('tbl_list_search',-1);
				$('#list_container_recipe_items').html('');
				$('#copy_id').val(2);
				$('#copy_id').removeAttr('disabled','disabled');
				$('#copy_sub_process').attr('disabled','disabled');
				reset_form('','','copy_id*copy_sub_process','','');
				set_button_status(0, permission, 'fnc_recipe_entry',1,1);	
			}
			else if(reponse[0]==14)
			{
				alert('Bellow Dyes And Chemical Issue Requisition Found. Change Not Allowed.\n Requisition No: '+reponse[1]+"\n");
			}
			
			release_freezing();	
		}
	}
	
	function openmypage_sysNo()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var title = 'System ID Selection Form';	
			var page_link = 'requires/recipe_entry_controller.php?cbo_company_id='+cbo_company_id+'&action=systemid_popup';
			  
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=880px,height=390px,center=1,resize=1,scrolling=0','');
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var update_id=this.contentDoc.getElementById("hidden_update_id").value;	 //Access form field with id="emailfield"
				
				if(update_id!="")
				{
					freeze_window(5);
					$('#list_container_recipe_items').html('');
					get_php_form_data(update_id, "populate_data_from_search_popup", "requires/recipe_entry_controller" );
					show_list_view(update_id, 'recipe_item_details', 'recipe_items_list_view', 'requires/recipe_entry_controller', '' ) ;
					setFilterGrid('tbl_list_search',-1);
					$('#copy_id').removeAttr('disabled','disabled');
					reset_form('','','copy_id*copy_sub_process','','');

					set_button_status(0, permission, 'fnc_recipe_entry',1,1);
					release_freezing();
				} 
			}
		}
	}
	
	/*function openmypage_booking()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var cbo_buyer_name = $('#cbo_buyer_name').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var title = 'Booking No Selection Form';	
			var page_link = 'requires/recipe_entry_controller.php?cbo_company_id='+cbo_company_id+'&cbo_buyer_name='+cbo_buyer_name+'&action=booking_popup';
			  
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=850px,height=390px,center=1,resize=1,scrolling=0','');
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var booking_id=this.contentDoc.getElementById("hidden_booking_id").value;
				var booking_no=this.contentDoc.getElementById("hidden_booking_no").value;
				var color=this.contentDoc.getElementById("hidden_color").value;
				var color_id=this.contentDoc.getElementById("hidden_color_id").value;
				var job_no=this.contentDoc.getElementById("hidden_job_no").value;
				var booking_without_order=this.contentDoc.getElementById("booking_without_order").value;
				if(booking_id!="")
				{
					freeze_window(5);
					document.getElementById("txt_booking_id").value=booking_id;
					document.getElementById("txt_booking_order").value=booking_no;
					document.getElementById("txt_color").value=color;
					document.getElementById("txt_color_id").value=color_id;
					document.getElementById("txt_order").value=job_no;
					document.getElementById("txt_booking_type").value=booking_without_order;
					release_freezing();
				} 
			}
		}
	}*/	
	
	function reset_div()
	{
		var val=$('#copy_sub_process').val();
		if(val==2)
		{
			$('#list_container_recipe_items').html('');
			$(".accordion_h").each(function() {
			
				 var tid=$(this).attr('id'); 
				 tid=tid+"span";
				 $('#'+tid).html("+");
			});
				
			set_button_status(0, permission, 'fnc_recipe_entry',1,0);
		}
		else if(val==1)
		{
			set_button_status(1, permission, 'fnc_recipe_entry',1,0);
		}
	}
	
	function fnc_item_details(sub_process_id)
	{
		$(".accordion_h").each(function() {
		
			 var tid=$(this).attr('id'); 
			 tid=tid+"span";
			 $('#'+tid).html("+");
		});
		
		$('#accordion_h'+sub_process_id+'span').html("-");
		var val=$('#copy_sub_process').val();
		var copy_id=$('#copy_id').val();
		var update_id= $('#update_id_check').val();
		var cbo_company_id= $('#cbo_company_id').val();
		
		$('#cbo_sub_process').val(sub_process_id);
		show_list_view(cbo_company_id+'**'+sub_process_id+"**"+update_id, 'item_details', 'list_container_recipe_items', 'requires/recipe_entry_controller', '');
		setFilterGrid('tbl_list_search',-1);
		$('#copy_sub_process').removeAttr('disabled','disabled');

		if(copy_id==2)
		{
			set_button_status(1, permission, 'fnc_recipe_entry',1,1);
		}
		else
		{
			set_button_status(0, permission, 'fnc_recipe_entry',1,0);
		}
	}
	
	function openmypage_itemLot(id)
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var txt_prod_id = $('#txt_prod_id_'+id).val();
		var txt_item_lot = $('#txt_item_lot_'+id).val();
		//alert  (txt_prod_id);
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var title = 'Item Lot Selection Form';	
			var page_link = 'requires/recipe_entry_controller.php?cbo_company_id='+cbo_company_id+'&txt_prod_id='+txt_prod_id+'&txt_item_lot='+txt_item_lot+'&action=itemLot_popup';
			  
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=350px,height=390px,center=1,resize=1,scrolling=0','');
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var item_lot=this.contentDoc.getElementById("item_lot").value;
				if(item_lot!="")
				{
					freeze_window(5);
					document.getElementById("txt_item_lot_"+id).value=item_lot;
					release_freezing();
				} 
			}			
		}
	}
	
	function caculate_tot_liquor()
	{
		var batch_weight=$('#txt_batch_weight').val();
		var tot_liquor=($('#txt_batch_ratio').val()*1)*($('#txt_liquor_ratio').val()*1)*(batch_weight*1);
		$('#txt_liquor').val(tot_liquor);
	}
	
	function openmypage_batchNo()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var title = 'Batch No Selection Form';	
			var page_link = 'requires/recipe_entry_controller.php?cbo_company_id='+cbo_company_id+'&action=batch_popup';
			  
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=370px,center=1,resize=1,scrolling=0','');
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var batch_id=this.contentDoc.getElementById("hidden_batch_id").value;	 //Access form field with id="emailfield"
				//alert(theemail);return;
				if(batch_id!="")
				{
					freeze_window(5);
					var batch_val=batch_id.split('_');
					$('#txt_batch_id').val(batch_val[0]);
					get_php_form_data(cbo_company_id+'**'+batch_val[0], "load_data_from_batch", "requires/recipe_entry_controller" );
					caculate_tot_liquor();
					release_freezing();
				} 
			}
		}
	}	
	
	function copy_check(type)
	{  
		if(type==1)
		{
			$('#update_id').val('');
			$('#txt_sys_id').val('');
			$('#txt_labdip_no').val('');
			$('#txt_batch_no').val('');
			$('#txt_batch_id').val('');
			$('#txt_batch_weight').val('');
			$('#txt_booking_order').val('');
			$('#txt_booking_id').val('');
			$('#txt_color').val('');
			$('#txt_color_id').val('');
			$('#txt_trims_weight').val('');
			$('#txt_yarn_lot').val('');
			$('#txt_brand').val('');
			$('#txt_count').val('');
			$('#txt_order').val('');
		}
		
		if ( document.getElementById('copy_id').checked==true)
		{
			document.getElementById('copy_id').value=1;
			set_button_status(0, permission, 'fnc_recipe_entry',1,1);
			//alert(chk );
		}
		else if(document.getElementById('copy_id').checked==false)
		{
			document.getElementById('copy_id').value=2;
		}
		//alert(type );
	}	
	
	function copy_check_sub(type)
	{  
		if ( document.getElementById('copy_sub_process').checked==true)
		{
			var chk=document.getElementById('copy_sub_process').value=1;
			//set_button_status(0, permission, 'fnc_recipe_entry',1,1);
			//alert(chk );
		}
		else if(document.getElementById('copy_sub_process').checked==false)
		{
			var chk=document.getElementById('copy_sub_process').value=2;
		}
		//alert(chk );
	}
	 
	function seq_no_val(id)
	{
		var row_num=$('#tbl_list_search tbody tr').length-1;
		var seq_no =new Array(); 
		var k=0;
		for(var j=1; j<=row_num; j++)
		{
			if(j!=id)
			{
				 
				if( $('#txt_seqno_'+j).val()*1>0) 
				{ 
					seq_no[k]=$('#txt_seqno_'+j).val()*1;
					k++;
				}
			}
		}
		var largest=0;
		if(seq_no!='')
		{
		var largest = Math.max.apply(Math, seq_no);
		}
		if(largest=='')
		{
			largest=0;
		}//alert (largest)
		/*alert (seq_no+"=="+largest)
		if ($('#txt_seqno_'+id).val()!='')
		{
			$('#txt_max_seq').val(largest*1);
		}*/
		
		largest=largest+1;
		/*var max_seq=$('#txt_max_seq').val()*1;
		if ($('#txt_ratio_'+id).val()!='')
		{
			max_seq=max_seq+1;
		}*/
		for(var i=1;i<=largest;i++)
		{
			if ($('#txt_ratio_'+id).val()!='')
			{
				if ($('#txt_seqno_'+id).val()=='')
				{
					$('#txt_seqno_'+id).val(largest);
				}
			}
			else
			{
				$('#txt_seqno_'+id).val('');
			}
		}
		//$('#txt_max_seq').val(max_seq);
		//row_sequence(id)
	}
	
	function row_sequence(row_id)
	{
		var row_num=$('#tbl_list_search tbody tr').length-1;
		var txt_seq=$('#txt_seqno_'+row_id).val();
		//var seq_no=1;
		for(var j=1; j<=row_num; j++)
		{
			if(j==row_id)
			{
				continue;
			}
			else
			{
				var txt_seq_check=$('#txt_seqno_'+j).val();
				
				if(txt_seq==txt_seq_check)
				{
					alert("Duplicate Seq No. "+txt_seq);
					$('#txt_seqno_'+row_id).val('');
					return;
				}
			}
		}
	}	
</script>
</head>

<body onLoad="set_hotkey();">
<div style="width:100%;" align="center">
	<?php echo load_freeze_divs ("../",$permission); ?>
	<fieldset style="width:950px;">
	<legend>Recipe Entry</legend> 
		<form name="recipeEntry_1" id="recipeEntry_1"> 
			<fieldset style="width:920px;">
				<table width="910" align="center" border="0">
                    <tr>
                        <td colspan="3" align="right"><strong>System ID</strong></td>
                        <td align="left">
                        <input type="text" name="txt_sys_id" id="txt_sys_id" class="text_boxes" style="width:140px;" placeholder="Double click to search" onDblClick="openmypage_sysNo();" readonly />
                        <input type="hidden" name="txt_max_seq" id="txt_max_seq" class="text_boxes" value="0" style="width:40px;" />
                        </td>
                        <td>
                       		<strong>Copy</strong>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="copy_id" id="copy_id" onClick="copy_check(1)" value="2" disabled >
                        </td>
                        <td>
                        	<div id="batch_type" style="color:#F00; font-size:18px"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                	<tr>
                        <td class="must_entry_caption">Labdip No</td>
                        <td>
                            <input type="text" name="txt_labdip_no" id="txt_labdip_no" class="text_boxes" style="width:140px;" />
                        </td>
                         <td>Recipe Description</td>
                        <td>
                            <input type="text" name="txt_recipe_des" id="txt_recipe_des" class="text_boxes" style="width:140px;" />
                        </td>
                        <td class="must_entry_caption">Batch No</td>
                        <td>
                            <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:70px;" placeholder="Double click to search" onDblClick="openmypage_batchNo();" /><input type="text" name="txt_batch_weight" id="txt_batch_weight" class="text_boxes_numeric" style="width:58px;" disabled /><input type="hidden" name="txt_batch_id" id="txt_batch_id" style="width:40px;" />
                        </td>
                    </tr>
                    <tr>
                        <td class="must_entry_caption">Company Name</td>
                        <td>
                            <?php
                                echo create_drop_down( "cbo_company_id", 152, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", 0, "load_drop_down('requires/recipe_entry_controller', this.value, 'load_drop_down_location', 'location_td' );load_drop_down('requires/recipe_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td_id' );" );
                            ?>
                        </td>
                        <td>Location</td>
                        <td id="location_td">
                            <?php
                                echo create_drop_down("cbo_location", 152, $blank_array,"", 1,"-- Select Location --", 0,"");
                            ?>
                        </td>
                        <td class="must_entry_caption">Recipe Date</td>
                        <td>
                        	<input type="date" name="txt_recipe_date" id="txt_recipe_date" class="datepicker" style="width:140px;" tabindex="6" />
                        </td>
                    </tr>
                    <tr>
                        <td class="must_entry_caption">Order Source</td>
                        <td>
                            <?php
                                echo create_drop_down("cbo_order_source", 152, $order_source,"", 1,"-- Select Source --", $selected,"",1,"","","","");
                            ?>
                        </td>
                        <td>Buyer Name</td>
                        <td id="buyer_td_id">
                            <?php
							   echo create_drop_down( "cbo_buyer_name", 152, $blank_array,"", 1, "-- Select Buyer --", $selected, "",0 );  
 							?>
                        </td>
                        <td>Booking/Order</td>
                        <td>
                        	<input type="text" name="txt_booking_order" id="txt_booking_order" class="text_boxes" style="width:140px;" disabled/>
                            <input type="hidden" name="txt_booking_id" id="txt_booking_id" class="text_boxes" style="width:120px;" />
                        </td>
                    </tr>
                    <tr>
                        <td class="must_entry_caption">Color</td>
                        <td>
                            <input type="text" name="txt_color" id="txt_color" class="text_boxes" value="" style="width:140px;" tabindex="10" disabled />
                            <input type="hidden" name="txt_color_id" id="txt_color_id" class="text_boxes" value="" style="width:120px;"/>
                        </td>
                        <td>Color Range</td>
                        <td>
                            <?php
                                echo create_drop_down("cbo_color_range", 152, $color_range,"", 1,"-- Select --", '','',1);
                            ?>
                        </td>
                        <td>Method</td>
                        <td>
                            <?php
							   echo create_drop_down( "cbo_method", 152, $dyeing_method,"", 1, "--Select Method--", $selected, "",0 );  
 							?>
                        </td>
                    </tr>
                    <tr>
                        <td class="">Batch:Liquor Ratio</td>
                        <td colspan="0">
                            <input type="text" name="txt_batch_ratio" id="txt_batch_ratio" class="text_boxes_numeric" value="" style="width:62px;" placeholder="Batch" onBlur="caculate_tot_liquor()" /><strong>:</strong><input type="text" name="txt_liquor_ratio" id="txt_liquor_ratio" class="text_boxes_numeric" value="" style="width:62px;" placeholder="Liquor" onBlur="caculate_tot_liquor()" />
                        </td>
                        <td class="must_entry_caption">Total Liquor(ltr)</td>
                        <td>
                            <input type="text" name="txt_liquor" id="txt_liquor" class="text_boxes_numeric" value="" style="width:140px;" readonly />
                        </td>
                        <td>Trims Weight</td>
                        <td>
                            <input type="text" name="txt_trims_weight" id="txt_trims_weight" class="text_boxes_numeric" value="" style="width:140px;" disabled />
                        </td>
                    </tr>
                    <tr>
                        <td>Yarn Lot</td>
                        <td colspan="0">
                            <input type="text" name="txt_yarn_lot" id="txt_yarn_lot" class="text_boxes" value="" style="width:140px;" disabled />
                        </td>
                        <td>Brand</td>
                        <td>
                            <input type="text" name="txt_brand" id="txt_brand" class="text_boxes" value="" style="width:140px;" disabled />
                        </td>
                        <td>Count</td>
                        <td>
                            <input type="text" name="txt_count" id="txt_count" class="text_boxes" value="" style="width:140px;" disabled />
                        </td>
                    </tr>
                    <tr>
                        <td>Order No.</td>
                        <td>
                            <input type="text" name="txt_order" id="txt_order" class="text_boxes" value="" style="width:140px;" disabled />
                            <input type="hidden" name="txt_booking_type" id="txt_booking_type" class="text_boxes" value="" style="width:60px;" />
                        </td>
                        <td>Remarks</td>
                        <td colspan="3">
                            <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" value="" style="width:460px;" />
                        </td>
                   </tr>
             </table>
			</fieldset>                 
            <fieldset style="width:920px; margin-top:10px">
            <legend>Item Details</legend>
 				<table cellpadding="0" cellspacing="0" border="0" width="100%">
                	<tr>
                        <td>
                       		<strong>Replace Sub-Process</strong>&nbsp;&nbsp;&nbsp;<input type="checkbox" name="copy_sub_process" id="copy_sub_process" onClick="copy_check_sub(1)" value="2" disabled >
                        </td>
                    	<td width="110" class="must_entry_caption"><b>Sub Process</b></td>
                    	<td width="200" align="left">						 
							<?php
                                echo create_drop_down( "cbo_sub_process", 180, $dyeing_sub_process,"", 1, "-- Select Sub Process --", 0, "reset_div();" );
                            ?>
                        </td> 
                        <td colspan="3"><input type="button" value="Show Items" name="show" id="show" class="formbuttonplasminus" style="width:100px" onClick="show_details()"/></td>                	</tr>
                </table>
                <div id="list_container_recipe_items" style="margin-top:10px"></div>
            </fieldset> 
            <table width="910">
            	<tr>
                    <td colspan="4" align="center" class="button_container">
						<?php 
                        	echo load_submit_buttons($permission, "fnc_recipe_entry", 0,1,"reset_form('recipeEntry_1','list_container_recipe_items*recipe_items_list_view','','copy_id*2','disable_enable_fields(\'cbo_company_id\',0)');",1);
                        ?> 
                        <input type="hidden" name="update_id" id="update_id"/>
                        <input type="hidden" name="update_id_check" id="update_id_check"/>
                    </td>	  
                </tr>
            </table>
		</form>
        <div id="recipe_items_list_view" style="margin-top:10px"></div>
	</fieldset>
</div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>