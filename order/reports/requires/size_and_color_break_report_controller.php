<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_id", 130, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $selected, "" );   	 
	exit();
}

if ($action=="job_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);
	
?>	
    <script>
	/*var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_job_id').val( id );
		$('#txt_job_val').val( ddd );
	} */
	
	function js_set_value( job_id )
	{
		//alert(po_id)
		document.getElementById('txt_job_id').value=job_id;
		parent.emailwindow.hide();
	}
		  
	</script>
     <input type="text" id="txt_job_id" />
 <?php
	if ($data[0]==0) $company_id=""; else $company_id=" and company_name=$data[0]";
	if ($data[1]==0) $buyer_id=""; else $buyer_id=" and buyer_name=$data[1]";
	
	$marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$teamMemberArr = return_library_array("select id,team_leader_name from lib_marketing_team ","id","team_leader_name");
	
	$sql= "select id, job_no,job_no_prefix_num, style_ref_no, product_dept, dealing_marchant, team_leader from wo_po_details_master where status_active=1 and is_deleted=0 $company_id $buyer_id order by job_no";
	//echo $sql;
	
	$arr=array(2=>$product_dept,3=>$marchentrArr,4=>$teamMemberArr);
	echo  create_list_view("list_view", "Job No,Style Ref.,Prod. Dept.,Marchant,Team Name", "100,110,110,150,150","680","360",0, $sql , "js_set_value", "id,job_no", "", 1, "0,0,product_dept,dealing_marchant,team_leader", $arr , "job_no_prefix_num,style_ref_no,product_dept,dealing_marchant,team_leader", "",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
	exit();
}

if ($action=="po_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);
	
?>	
    <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function check_all_data()
		{
			var row_num=$('#list_view tr').length-1;
			for(var i=1;  i<=row_num;  i++)
			{
				$("#tr_"+i).click();
			}
			
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
		  
	</script>
     <input type="text" id="txt_po_id" />
     <input type="text" id="txt_po_val" />
     <?php
	 if ($data[0]==0) $company_name=""; else $job_num=" and a.company_name='$data[0]'";
	 if ($data[1]==0) $buyer_name=""; else $buyer_name=" and a.buyer_name='$data[1]'";
	 if ($data[2]=="") $job_num=""; else $job_num=" and a.job_no='$data[2]'";
	
	 $sql= "select b.id, b.po_number, b.job_no_mst, b.pub_shipment_date,c.gmts_item_id from wo_po_details_master a, wo_po_break_down  b, wo_po_details_mas_set_details c where a.job_no=b.job_no_mst and a.job_no=c.job_no and a.status_active=1 and a.is_deleted=0 $job_num $company_name $buyer_name group by b.id order by po_number";
	
	$arr=array(3=>$garments_item);
	echo  create_list_view("list_view", "PO No.,Job No.,Pub Shipment Date,Item Name", "100,100,80,150","450","360",0, $sql , "js_set_value", "id,po_number", "", 1, "0,0,0,gmts_item_id", $arr , "po_number,job_no_mst,pub_shipment_date,gmts_item_id", "",'setFilterGrid("list_view",-1);','0,0,3,0','',1) ;
	exit();	 
}

if ($action=="report_generate")
{
	extract($_REQUEST);
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_buyer=str_replace("'","",$cbo_buyer_id);
	$job_no=str_replace("'","",$txt_job_no);
	$hidd_job=str_replace("'","",$hidd_job_id);
	$hidd_po=str_replace("'","",$hidd_po_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	if ($cbo_company==0) $company_id=""; else $company_id=" and a.company_name=$cbo_company";
	if ($cbo_buyer==0) $buyer_id=""; else $buyer_id=" and a.buyer_name=$cbo_buyer";
	if ($job_no=="") $job_num=""; else $job_num=" and a.job_no='$job_no'";
	if ($job_no=="") $job_num_mst=""; else $job_num_mst=" and b.job_no_mst=$job_no";
	if ($hidd_job==0) $job_id=""; else $job_id=" and a.id in ($hidd_job)";
	if ($hidd_po=="") $po_id=""; else $po_id=" and c.id in ( $hidd_po )";
	
	if($db_type==0)
	{
	if( $date_from=="" && $date_to=="" ) $pub_shipment_date=""; else $pub_shipment_date= " and d.country_ship_date between '".$date_from."' and '".$date_to."'";
	}
	if($db_type==2)
	{
	if( $date_from=="" && $date_to=="" ) $pub_shipment_date=""; else $pub_shipment_date= " and d.country_ship_date between '".change_date_format($date_from,'dd-mm-yyyy','-',1)."' and '".change_date_format($date_to,'dd-mm-yyyy','-',1)."'";
	}
	
	$companyArr = return_library_array("select id,company_name from lib_company ","id","company_name");
	$buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
	$marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$itemSizeArr = return_library_array("select id,size_name from  lib_size ","id","size_name");
	$countryArr = return_library_array("select id,country_name from lib_country ","id","country_name");
	$colorArr = return_library_array("select id,color_name from lib_color","id","color_name");
	
	$job_no_array=array();
	$po_details_array=array();
	$job_size_array=array();
	$po_item_array=array();
	$po_country_array=array();
	$po_country_ship_date_array=array();
	$po_color_array=array();
	$job_qnty_color_size_table_array=array();
	$job_size_tot_qnty_array=array();
	
	$po_color_size_qnty_array=array();
	$po_color_qnty_array=array();
	$po_qnty_array=array();
	$po_qnty_color_size_table_array=array();
	$po_size_tot_qnty_array=array();
	$po_item_qnty_array=array();
	$po_item_size_tot_qnty_array=array();
	$po_country_qnty_array=array();
	$po_country_size_tot_qnty_array=array();
	$po_ship_date_array=array();
	
	 /*$sql="SELECT a.id as job_id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.location_name, a.dealing_marchant, (a.job_quantity*a.total_set_qnty) as job_quantity , a.order_uom, a.total_set_qnty, b.id as po_id, b.po_number, b.pub_shipment_date, b.shipment_date, b.po_received_date, (b.po_quantity*a.total_set_qnty) as po_quantity, b.excess_cut, b.plan_cut, b.unit_price, b.po_total_price, c.item_number_id, c.country_id, c.size_number_id, c.color_number_id, c.order_quantity, c.order_rate, c.order_total, c.excess_cut_perc, c.plan_cut_qnty
	FROM wo_po_details_master a
	LEFT JOIN wo_po_break_down b ON a.job_no = b.job_no_mst
	AND b.is_deleted =0
	AND b.status_active =1
	LEFT JOIN wo_po_color_size_breakdown c ON b.job_no_mst = c.job_no_mst
	AND b.id = c.po_break_down_id
	AND c.is_deleted =0
	AND c.status_active =1
	WHERE 
	a.is_deleted =0
	AND a.status_active =1
	$company_id $buyer_id $job_id $po_id $pub_shipment_date
	";*/
	
	  $sql="SELECT a.id as job_id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.location_name, a.dealing_marchant, (a.job_quantity*a.total_set_qnty) as job_quantity , a.order_uom, a.total_set_qnty,b.gmts_item_id as item_number_id, c.id as po_id, c.po_number, c.pub_shipment_date, c.shipment_date, c.po_received_date, (c.po_quantity*a.total_set_qnty) as po_quantity, c.excess_cut, c.plan_cut, c.unit_price, c.po_total_price,d.id,d.country_id,d.country_ship_date,d.cutup_date, d.size_number_id, d.color_number_id, d.order_quantity, d.order_rate, d.order_total, d.excess_cut_perc, d.plan_cut_qnty
	FROM wo_po_details_master a
	LEFT JOIN wo_po_details_mas_set_details b ON a.job_no = b.job_no
	LEFT JOIN wo_po_break_down c ON a.job_no = c.job_no_mst
	AND c.is_deleted =0
	AND c.status_active =1
	LEFT JOIN wo_po_color_size_breakdown d ON c.job_no_mst = d.job_no_mst
	AND c.id = d.po_break_down_id
	AND d.is_deleted =0
	AND d.status_active =1
	WHERE 
	a.is_deleted =0
	AND a.status_active =1
	$company_id $buyer_id $job_id $po_id $pub_shipment_date order by a.job_no, c.id,d.country_ship_date,d.id
	";
	
	
	$sql_data = sql_select($sql);
	foreach( $sql_data as $row)
	{
	$job_no_array[$row[csf('job_no')]]=array("job_no"=>$row[csf('job_no')],"job_quantity"=>$row[csf('job_quantity')],"company_name"=>$row[csf('company_name')],"buyer_name"=>$row[csf('buyer_name')],"style_ref_no"=>$row[csf('style_ref_no')],"product_dept"=>$row[csf('product_dept')],"dealing_marchant"=>$row[csf('dealing_marchant')],"job_id"=>$row[csf('job_id')],"order_uom"=>$row[csf('order_uom')]);
	$po_details_array[$row[csf('job_no')]][$row[csf('po_id')]]=$row[csf('po_number')];
	$job_size_array[$row[csf('job_no')]][$row[csf('size_number_id')]]=$row[csf('size_number_id')];
	$po_item_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]]=$row[csf('item_number_id')];
	$po_country_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]=$row[csf('country_id')];
	$po_ship_date_array[$row[csf('job_no')]][$row[csf('po_id')]]=$row[csf('shipment_date')];
	$po_country_ship_date_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]=$row[csf('country_ship_date')];
	$po_color_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]] [$row[csf('color_number_id')]]=$row[csf('color_number_id')];
	
	$job_qnty_color_size_table_array[$row[csf('job_no')]]+=$row[csf('order_quantity')];
	$job_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	$po_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]]=$row[csf('po_quantity')];
	$po_qnty_color_size_table_array[$row[csf('job_no')]][$row[csf('po_id')]]+=$row[csf('order_quantity')];
	$po_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	
	$po_item_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]]+=$row[csf('order_quantity')];
	$po_item_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	
	$po_country_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]+=$row[csf('order_quantity')];
	$po_country_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	
	$po_color_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]] [$row[csf('color_number_id')]]+=$row[csf('order_quantity')];
	$po_color_size_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]] [$row[csf('color_number_id')]][$row[csf('size_number_id')]]=$row[csf('order_quantity')];
	}
	
	//print_r($po_item_array);
	
	$po_rowspan_arr=array();
	$item_rowspan_arr=array();
	$country_rowspan_arr=array();
	foreach($po_color_array as $job)
	{
		foreach($job as $po_id=>$po_value)
		{ 
			$po_rowspan=0;
			foreach($po_value as $item_id =>$item_value)
			{
				$item_rowspan=0;
				foreach($item_value as $country_id =>$country_value)
				{
					$country_rowspan=1;
					foreach($country_value as $color_id =>$color_value)
					{
						
						$po_rowspan++;
						
						$item_rowspan++;
						$country_rowspan_arr[$po_id][$item_id][$country_id]=$country_rowspan;
						$country_rowspan++;
					}
					$po_rowspan++;
					$po_rowspan_arr[$po_id]=$po_rowspan;
					$item_rowspan++;
				
					$item_rowspan_arr[$po_id][$item_id]=$item_rowspan;
					
				}
				$po_rowspan++;
				$po_rowspan_arr[$po_id]=$po_rowspan;
					
				
			}
			//$po_rowspan++;
		}
		//$po_rowspan++;
	}
	
	?>
    
	
    <table id="scroll_body" align="center" style="height:auto; width:1100px; margin:0 auto; padding:0;">
    <tr>
    <td width="1120">
    
	<?php
	foreach($job_no_array as $rdata=>$det)
	{
	?>
        <br/>
        <table width="1050px" align="center" border="1" rules="all">
            <tr style="background-color:#FFF">
                <td width="60" align="right">Job No: </td><td width="90" onclick="openmypage_job_color_size('requires/size_and_color_break_report_controller.php?action=job_color_size&job_no=<?php echo $det["job_no"] ?>','Job Color Size')"><a href="##"><?php echo $det['job_no']; ?></a></td>
                <td width="60" align="right">Job Qnty: </td><td width="90"><?php echo $det['job_quantity']."(Pcs)"; ?></td>
                <td width="60" align="right">Company: </td><td width="90"><?php echo $companyArr[$det['company_name']]; ?></td>
                <td width="60" align="right">Buyer: </td><td width="85"><?php echo $buyerArr[$det['buyer_name']]; ?></td>
                <td width="65" align="right">Style Ref.: </td><td width="85"><?php echo $det['style_ref_no']; ?></td>
                <td width="70" align="right">Prod. Dept.: </td><td width="80"><?php echo $product_dept[$det['product_dept']]; ?></td>
                <td width="60" align="right">Merchant: </td><td width="90"><?php echo $marchentrArr[$det['dealing_marchant']]; ?></td>
            </tr>
        </table>
        <br/>
        <table width="1050px" align="center" border="1" rules="all" class="rpt_table" >
            <thead>
                <tr>
                    <!--<th width="60">Sl</th>-->
                    <th width="60">PO Number</th>
                    <th width="60">PO Qnty</th>
                    <th width="70">Item </th>
                    <th width="60">Country</th>
                    <th width="60">Color</th>
                    <th width="60">Color Total</th>
                    <?php
					foreach($job_size_array[$det['job_no']] as $key=>$value)
                    {
						if($value !="")
						{
					?>
                    <th width="60"><?php echo $itemSizeArr[$value];?></th>
                    <?php
						}
					}
					?>
                </tr>
            </thead>
            <?php
            
            foreach($po_details_array[$det['job_no']] as $key=>$value)
            {
                $posl=1;
                foreach($po_item_array [$det['job_no']][$key] as $item_key=>$item_value)
                {
					
                    $itemsl=1;
                    foreach($po_country_array [$det['job_no']][$key][$item_value] as $country_key=>$country_value)
                    {
						//echo count($po_country_array [$det[csf('job_no')]][$key][$item_key]);
                        $countrysl=1;
                        foreach($po_color_array [$det['job_no']][$key][$item_value][$country_value] as $color_key=>$color_value)
                        {
							if($countrysl%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                        ?>
                            <tr bgcolor="<?php echo $bgcolor;?>">
                                <!--<td valign="middle" rowspan="" ><?php  //echo $itemsl;?></td>-->
                                <?php
                                if($posl==1)
                                {
									

                                ?>
                                <td  align="center" valign="middle" rowspan="<?php echo $po_rowspan_arr[$key]; ?>" ><?php  echo $value; echo "<br/> Ship Date:".change_date_format($po_ship_date_array[$det['job_no']][$key],"dd-mm-yyyy","-"); echo "<br/>".date('l', strtotime($po_ship_date_array[$det['job_no']][$key])); ?></td>
                                <?php
                                }
                                if($posl==1)
                                {
                                ?>
                                <td align="center" valign="middle" rowspan="<?php echo $po_rowspan_arr[$key]; ?>"><?php echo  $po_qnty_array[$det['job_no']][$key]; ?></td>
                                <?php
                                }
                                if($itemsl==1)
                                {
                                ?>
                                <td align="center" valign="middle" rowspan="<?php  echo $item_rowspan_arr[$key][$item_key]; ?>" ><?php  echo $garments_item[$item_value] ;?></td> 
                                <?php
                                }
                                if($countrysl==1)
                                {
                                ?>
                                <td align="center" valign="middle" rowspan="<?php  echo $country_rowspan_arr[$key][$item_key][$country_key]; ?>"><?php  echo $countryArr[$country_value]."<br/>".change_date_format($po_country_ship_date_array [$det['job_no']][$key][$item_value][$country_value],"dd-mm-yyyy","-")."<br/>".date('l', strtotime($po_country_ship_date_array [$det['job_no']][$key][$item_value][$country_value]));  ?></td>
                                <?php
                                }
                                ?>
                                <td><?php  echo $colorArr[$color_value] ;?></td>
                                <td align="right"><?php echo $po_color_qnty_array [$det['job_no']][$key][$item_value][$country_value][$color_value]; ?></td>
                                <?php
								foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
								{
									if($value_s !="")
						            {
								?>
								<td align="right"><?php echo $po_color_size_qnty_array [$det['job_no']][$key][$item_value][$country_value][$color_value][$value_s]?></td>
								<?php
									}
								}
								?>
                            </tr>
                            <?php
                            $posl++;
                            $itemsl++;
                            $countrysl++;
                        }
						?>
                 <tr style="font-weight:bold; font-size:12px">
                 <td colspan="2">Country Total:</td>
                 <td colspan="" align="right"><?php echo $po_country_qnty_array[$det['job_no']][$key][$item_key][$country_key] ?></td>
                 <?php
				foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
				{
				if($value_s !="")
				{
					//$po_country_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('size_number_id')]]
				?>
				<td align="right"><?php echo $po_country_size_tot_qnty_array [$det['job_no']][$key][$item_value][$country_value][$value_s]?></td>
				<?php
				}
				}
				?>
                </tr>
                    <?php
                    }
					?>
                <tr style="font-weight:bold; font-size:12px">
                <td colspan="3">Item Total:</td>
                <td colspan="" align="right"><?php echo $po_item_qnty_array[$det['job_no']][$key][$item_key] ?></td>
                <?php
				foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
				{
				if($value_s !="")
				{
					//$po_item_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('size_number_id')]]
				?>
				<td align="right"><?php echo $po_item_size_tot_qnty_array [$det['job_no']][$key][$item_value][$value_s]?></td>
				<?php
				}
				}
				?>
                </tr>
                <?php
					
                }
				?>
                <tr style="font-weight:bold; font-size:12px">
                <td colspan="5">Po Total:</td>
                 
                   <td  align="right"><?php echo $po_qnty_color_size_table_array [$det['job_no']][$key]?></td>
                   
                 <?php
				foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
				{
				if($value_s !="")
				{
					//$po_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('size_number_id')]]
				?>
				<td align="right"><?php echo $po_size_tot_qnty_array [$det['job_no']][$key][$value_s]?></td>
				<?php
				}
				}
				?>
                </tr>
                
                <?php
            }
            ?>
            <tr style="font-weight:bold; font-size:12px">
                <td colspan="5">Grand Total:</td>
                 
                   <td align="right"><?php echo $job_qnty_color_size_table_array [$det['job_no']];?></td>
                   
                 <?php
				foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
				{ //$job_size_array[$row[csf('job_no')]][$row[csf('size_number_id')]]
				if($value_s !="")
			    {
				?>
				<td align="right"><?php echo $job_size_tot_qnty_array [$det['job_no']][$value_s]?></td>
				<?php
				}
				}
				?>
                </tr>
        </table>
        
	<?php
	}
	?>
    
       </td>
       </tr>
       </table>
       
      
    <?php
}


//report for cut off date
if($action=="report_generate_cutoff")
{

	extract($_REQUEST);
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_buyer=str_replace("'","",$cbo_buyer_id);
	$job_no=str_replace("'","",$txt_job_no);
	$hidd_job=str_replace("'","",$hidd_job_id);
	$hidd_po=str_replace("'","",$hidd_po_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	if ($cbo_company==0) $company_id=""; else $company_id=" and a.company_name=$cbo_company";
	if ($cbo_buyer==0) $buyer_id=""; else $buyer_id=" and a.buyer_name=$cbo_buyer";
	if ($job_no=="") $job_num=""; else $job_num=" and a.job_no='$job_no'";
	if ($job_no=="") $job_num_mst=""; else $job_num_mst=" and b.job_no_mst=$job_no";
	if ($hidd_job==0) $job_id=""; else $job_id=" and a.id in ($hidd_job)";
	if ($hidd_po=="") $po_id=""; else $po_id=" and c.id in ( $hidd_po )";
	if($db_type==0)
	{
	if( $date_from=="" && $date_to=="" ) $pub_shipment_date=""; else $pub_shipment_date= " and d.cutup_date between '".$date_from."' and '".$date_to."'";
	}
	if($db_type==2)
	{
	if( $date_from=="" && $date_to=="" ) $pub_shipment_date=""; else $pub_shipment_date= " and d.cutup_date between '".change_date_format($date_from,'dd-mm-yyyy','-',1)."' and '".change_date_format($date_to,'dd-mm-yyyy','-',1)."'";
	}
	
	$companyArr = return_library_array("select id,company_name from lib_company ","id","company_name");
	$buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
	$marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$itemSizeArr = return_library_array("select id,size_name from  lib_size ","id","size_name");
	$countryArr = return_library_array("select id,country_name from lib_country ","id","country_name");
	$colorArr = return_library_array("select id,color_name from lib_color","id","color_name");
	
	$job_no_array=array();
	$po_details_array=array();
	$job_size_array=array();
	$po_item_array=array();
	$po_country_array=array();
	$po_country_ship_date_array=array();
	$po_color_array=array();
	$job_qnty_color_size_table_array=array();
	$job_size_tot_qnty_array=array();
	
	$po_cut_off_array=array();
	$po_color_size_qnty_array=array();
	$po_color_qnty_array=array();
	$po_qnty_array=array();
	$po_qnty_color_size_table_array=array();
	$po_size_tot_qnty_array=array();
	$po_item_qnty_array=array();
	$po_item_size_tot_qnty_array=array();
	$po_country_qnty_array=array();
	$po_country_size_tot_qnty_array=array();
	$po_ship_date_array=array();
	
	 /*$sql="SELECT a.id as job_id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.location_name, a.dealing_marchant, (a.job_quantity*a.total_set_qnty) as job_quantity , a.order_uom, a.total_set_qnty, b.id as po_id, b.po_number, b.pub_shipment_date, b.shipment_date, b.po_received_date, (b.po_quantity*a.total_set_qnty) as po_quantity, b.excess_cut, b.plan_cut, b.unit_price, b.po_total_price, c.item_number_id, c.country_id, c.size_number_id, c.color_number_id, c.order_quantity, c.order_rate, c.order_total, c.excess_cut_perc, c.plan_cut_qnty
	FROM wo_po_details_master a
	LEFT JOIN wo_po_break_down b ON a.job_no = b.job_no_mst
	AND b.is_deleted =0
	AND b.status_active =1
	LEFT JOIN wo_po_color_size_breakdown c ON b.job_no_mst = c.job_no_mst
	AND b.id = c.po_break_down_id
	AND c.is_deleted =0
	AND c.status_active =1
	WHERE 
	a.is_deleted =0
	AND a.status_active =1
	$company_id $buyer_id $job_id $po_id $pub_shipment_date
	";*/
	
	$sql="SELECT distinct a.id as job_id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.location_name, a.dealing_marchant, (a.job_quantity*a.total_set_qnty) as job_quantity , a.order_uom, a.total_set_qnty,d.item_number_id as item_number_id, c.id as po_id, c.po_number, c.pub_shipment_date, c.shipment_date, c.po_received_date, (c.po_quantity*a.total_set_qnty) as po_quantity, c.excess_cut, c.plan_cut, c.unit_price, c.po_total_price,d.id, d.cutup, d.country_id,d.country_ship_date,d.cutup_date, d.size_number_id, d.color_number_id, d.order_quantity, d.order_rate, d.order_total, d.excess_cut_perc, d.plan_cut_qnty
	FROM wo_po_details_master a

	LEFT JOIN wo_po_break_down c ON a.job_no = c.job_no_mst
	AND c.is_deleted =0
	AND c.status_active =1
	LEFT JOIN wo_po_color_size_breakdown d ON c.job_no_mst = d.job_no_mst
	AND c.id = d.po_break_down_id
	AND d.is_deleted =0
	AND d.status_active =1
	WHERE 
	d.id!=0 and
	a.is_deleted =0
	AND a.status_active =1
	$company_id $buyer_id $job_id $po_id $pub_shipment_date order by a.job_no,c.id,d.cutup_date,d.cutup,d.id
	";
	//echo $sql;die;
	
	$sql_data = sql_select($sql);
	foreach( $sql_data as $row)
	{
	$job_no_array[$row[csf('job_no')]]=array("job_no"=>$row[csf('job_no')],"job_quantity"=>$row[csf('job_quantity')],"company_name"=>$row[csf('company_name')],"buyer_name"=>$row[csf('buyer_name')],"style_ref_no"=>$row[csf('style_ref_no')],"product_dept"=>$row[csf('product_dept')],"dealing_marchant"=>$row[csf('dealing_marchant')],"job_id"=>$row[csf('job_id')],"order_uom"=>$row[csf('order_uom')]);
	$po_details_array[$row[csf('job_no')]][$row[csf('po_id')]]=$row[csf('po_number')];
	$po_cutup_date_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]]=$row[csf('cutup_date')];
	$job_size_array[$row[csf('job_no')]][$row[csf('size_number_id')]]=$row[csf('size_number_id')];
	$po_item_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]]=$row[csf('item_number_id')];
	$po_country_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]]=$row[csf('country_id')];
	//$po_ship_date_array[$row[csf('job_no')]][$row[csf('po_id')]]=$row[csf('cutup_date')];
	$po_country_ship_date_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]]=$row[csf('country_ship_date')];
	$po_color_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]] [$row[csf('color_number_id')]]=$row[csf('color_number_id')];
	
	$job_qnty_color_size_table_array[$row[csf('job_no')]]+=$row[csf('order_quantity')];
	$job_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	$po_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]]+=$row[csf('order_quantity')];
	$po_cut_off_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]]=$row[csf('cutup')];
	$po_qnty_color_size_table_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]]+=$row[csf('order_quantity')];
	$po_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	
	$po_item_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]]+=$row[csf('order_quantity')];
	$po_cut_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	
	$po_item_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	$po_cutoff_tot_qty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]]+=$row[csf('order_quantity')];
	$po_country_id_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]]=$row[csf('country_id')];
	$po_country_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]]+=$row[csf('order_quantity')];
	$po_country_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	
	$po_color_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]] [$row[csf('color_number_id')]]+=$row[csf('order_quantity')];
	$po_color_size_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('cutup_date')]][$row[csf('item_number_id')]][$row[csf('cutup')]][$row[csf('country_id')]] [$row[csf('color_number_id')]][$row[csf('size_number_id')]]=$row[csf('order_quantity')];
	}
	
	//print_r($po_cutup_date_array);
	
	$po_rowspan_arr=array();
	$item_rowspan_arr=array();
	$country_rowspan_arr=array();
	$cutoff_rowspan_arr=array();
	foreach($po_color_array as $job)
	{
		foreach($job as $po_id=>$po_value)
		{
		 foreach($po_value as $cut_up_id=>$cut_up_value)
	     {  
			$po_rowspan=0;
			foreach($cut_up_value as $item_id =>$item_value)
			{
				$item_rowspan=0;
				foreach($item_value as $cutoff_id =>$cutoff_value)
					{
					$cutoff_rowspan=0;
					foreach($cutoff_value as $country_id =>$country_value)
					{
						$country_rowspan=1;
						foreach($country_value as $color_id =>$color_value)
						{
							
							$po_rowspan++;
							
							$item_rowspan++;
							$cutoff_rowspan++;
							$country_rowspan_arr[$po_id][$cut_up_id][$item_id][$cutoff_id][$country_id]=$country_rowspan;
							$country_rowspan++;
						}
						$po_rowspan++;
						$po_rowspan_arr[$po_id][$cut_up_id]=$po_rowspan;
						$item_rowspan++;
						$cutoff_rowspan++;
						$cutoff_rowspan_arr[$po_id][$cut_up_id][$item_id][$cutoff_id]=$cutoff_rowspan;
						
					}
					$po_rowspan++;
					$po_rowspan_arr[$po_id][$cut_up_id]=$po_rowspan;
					$item_rowspan++;
				
					$item_rowspan_arr[$po_id][$cut_up_id][$item_id]=$item_rowspan;
						
				}
				$po_rowspan++;
				$po_rowspan_arr[$po_id][$cut_up_id]=$po_rowspan;
			}
		  }//$po_rowspan++;
		}
		//$po_rowspan++;
	}
	//print_r($item_rowspan_arr)
	?>
    
	
    <table id="scroll_body" align="center" style="height:auto; width:1100px; margin:0 auto; padding:0;">
    <tr>
    <td width="1120">
    
	<?php
	foreach($job_no_array as $rdata=>$det)
	{
	?>
        <br/>
        <table width="1050px" align="center" border="1" rules="all">
            <tr style="background-color:#FFF">
                <td width="60" align="right">Job No: </td><td width="90" onclick="openmypage_job_color_size('requires/size_and_color_break_report_controller.php?action=job_color_size_cut&job_no=<?php echo $det["job_no"] ?>','Job Color Size')"><a href="##"><?php echo $det['job_no']; ?></a></td>
                <td width="60" align="right">Job Qnty: </td><td width="90"><?php echo $det['job_quantity']."(Pcs)"; ?></td>
                <td width="60" align="right">Company: </td><td width="90"><?php echo $companyArr[$det['company_name']]; ?></td>
                <td width="60" align="right">Buyer: </td><td width="85"><?php echo $buyerArr[$det['buyer_name']]; ?></td>
                <td width="65" align="right">Style Ref.: </td><td width="85"><?php echo $det['style_ref_no']; ?></td>
                <td width="70" align="right">Prod. Dept.: </td><td width="80"><?php echo $product_dept[$det['product_dept']]; ?></td>
                <td width="60" align="right">Merchant: </td><td width="90"><?php echo $marchentrArr[$det['dealing_marchant']]; ?></td>
            </tr>
        </table>
        <br/>
        <table width="1050px" align="center" border="1" rules="all" class="rpt_table" >
            <thead>
                <tr>
                    <!--<th width="60">Sl</th>-->
                    <th width="60">PO Number</th>
                    <th width="60">PO Qnty</th>
                    <th width="70">Item </th>
                     <th width="70">Cut-off </th>
                    <th width="60">Country</th>
                    <th width="60">Color</th>
                    <th width="60">Color Total</th>
                    <?php
					foreach($job_size_array[$det['job_no']] as $key=>$value)
                    {
						if($value !="")
						{
					?>
                    <th width="60"><?php echo $itemSizeArr[$value];?></th>
                    <?php
						}
					}
					?>
                </tr>
            </thead>
            <?php
            
            foreach($po_details_array[$det['job_no']] as $key=>$value)
            { 
			   foreach($po_cutup_date_array[$det['job_no']][$key] as $cut_up_key=>$cut_up_value)
               {
                $posl=1;
                foreach($po_item_array [$det['job_no']][$key][$cut_up_value] as $item_key=>$item_value)
                {
					
                    $itemsl=1;
					   foreach($po_cut_off_array [$det['job_no']][$key][$cut_up_value][$item_value] as $cutoff_key=>$cutoff_value)
						{
							
							$cutoffsl=1;
							foreach($po_country_array [$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value] as $country_key=>$country_value)
							{
								//echo count($po_country_array [$det[csf('job_no')]][$key][$item_key]);
								$countrysl=1;
								foreach($po_color_array [$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$country_value] as $color_key=>$color_value)
								{
									if($countrysl%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
								?>
									<tr bgcolor="<?php echo $bgcolor;?>" >
										<!--<td valign="middle" rowspan="" ><?php  //echo $itemsl;?></td>-->
										<?php
										if($posl==1)
										{
											
		
										?>
										<td  align="center" valign="middle" rowspan="<?php echo $po_rowspan_arr[$key][$cut_up_key]; ?>" ><?php  echo $value; echo "<br/> Cut-off Date:<br/>".change_date_format($po_cutup_date_array[$det['job_no']][$key][$cut_up_value],"dd-mm-yyyy","-"); if($po_cutup_date_array[$det['job_no']][$key][$cut_up_value]!='0000-00-00') echo "<br/>".date('l', strtotime($po_cutup_date_array[$det['job_no']][$key][$cut_up_value])); ?></td>
										<?php
										}
										if($posl==1)
										{
										?>
										<td align="center" valign="middle" rowspan="<?php echo $po_rowspan_arr[$key][$cut_up_key]; ?>"><?php echo  $po_qnty_array[$det['job_no']][$key][$cut_up_value]; ?></td>
										<?php
										}
										if($itemsl==1)
										{
										?>
										<td align="center" valign="middle" rowspan="<?php  echo $item_rowspan_arr[$key][$cut_up_key][$item_key]; ?>" ><?php  echo $garments_item[$item_value] ;?></td> 
										<?php
										}
										if($cutoffsl==1)
										{
										?>
										<td align="center" valign="middle" rowspan="<?php  echo $cutoff_rowspan_arr[$key][$cut_up_key][$item_key][$cutoff_key]; ?>" ><?php  echo $cut_up_array[$po_cut_off_array[$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value]] ;?></td> 
										<?php
										}
										if($countrysl==1)
										{
										?>
										<td align="center" valign="middle" rowspan="<?php  echo $country_rowspan_arr[$key][$cut_up_key][$item_key][$cutoff_key][$country_key]; ?>"><?php  echo $countryArr[$po_country_id_array[$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$country_value]]."<br/>".change_date_format($po_country_ship_date_array [$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$country_value],"dd-mm-yyyy","-")."<br/>".date('l', strtotime($po_country_ship_date_array [$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$country_value]));  ?></td>
										<?php
										}
										?>
										<td><?php  echo $colorArr[$color_value] ;?></td>
										<td align="right"><?php echo $po_color_qnty_array [$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$country_value][$color_value]; ?></td>
										<?php
										foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
										{
											if($value_s !="")
											{
										?>
										<td align="right"><?php echo $po_color_size_qnty_array [$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$country_value][$color_value][$value_s]?></td>
										<?php
											}
										}
										?>
									</tr>
									<?php
									$posl++;
									$itemsl++;
									$cutoffsl++;
									$countrysl++;
								}
								?>
						 <tr style="font-weight:bold; font-size:12px; background-color:#DFDFDF">
						 <td colspan="2">Country Total:</td>
						 <td colspan="" align="right"><?php echo $po_country_qnty_array[$det['job_no']][$key][$cut_up_value][$item_key][$cutoff_value][$country_key] ?></td>
						 <?php
						foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
						{
						if($value_s !="")
						{
							//$po_country_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]][$row[csf('size_number_id')]]
						?>
						<td align="right"><?php echo $po_country_size_tot_qnty_array [$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$country_value][$value_s]; ?></td>
						<?php
						}
						}
						?>
						</tr>
							<?php
							}
							?>
						<tr style="font-weight:bold; font-size:12px">
						<td colspan="3"><?php echo $cut_up_array[$po_cut_off_array[$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value]]; ?> Total:</td>
						<td colspan="" align="right"><?php echo $po_cutoff_tot_qty_array[$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value] ?></td>
						<?php
						foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
						{
						if($value_s !="")
						{
							//$po_item_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('size_number_id')]]
						?>
						<td align="right"><?php echo $po_cut_size_tot_qnty_array[$det['job_no']][$key][$cut_up_value][$item_value][$cutoff_value][$value_s]?></td>
						<?php
						}
						}
						?>
						</tr>
						<?php
						}
						?>
                        
                        <tr style="font-weight:bold; font-size:12px;  background-color:#DFDFDF">
						<td colspan="4">Item Total:</td>
						<td colspan="" align="right"><?php echo $po_item_qnty_array[$det['job_no']][$key][$cut_up_value][$item_key] ?></td>
						<?php
						foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
						{
						if($value_s !="")
						{
							//$po_item_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('item_number_id')]][$row[csf('size_number_id')]]
						?>
						<td align="right"><?php echo $po_item_size_tot_qnty_array [$det['job_no']][$key][$cut_up_value][$item_value][$value_s]?></td>
						<?php
						}
						}
						?>
						</tr>
						<?php
                }
				?>
                <tr style="font-weight:bold; font-size:12px">
                <td colspan="6">Date wise Po Total:</td>
                 
                   <td  align="right"><?php echo $po_qnty_color_size_table_array [$det['job_no']][$key][$cut_up_value]?></td>
                   
                 <?php
				foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
				{
				if($value_s !="")
				{
					//$po_size_tot_qnty_array[$row[csf('job_no')]][$row[csf('po_id')]][$row[csf('size_number_id')]]
				?>
				<td align="right"><?php echo $po_size_tot_qnty_array [$det['job_no']][$key][$cut_up_value][$value_s]?></td>
				<?php
				}
				
				
				}
				?>
                </tr>
                
                <?php
			  }
            }
            ?>
            <tfoot>
            <tr style="font-weight:bold; font-size:12px">
                <th colspan="6" align="left">Grand Total:</th>
                 
                   <th align="right"><?php echo $job_qnty_color_size_table_array [$det['job_no']];?></th>
                   
                 <?php
				foreach($job_size_array[$det['job_no']] as $key_s=>$value_s)
				{ //$job_size_array[$row[csf('job_no')]][$row[csf('size_number_id')]]
				if($value_s !="")
			    {
				?>
				<th align="right"><?php echo $job_size_tot_qnty_array [$det['job_no']][$value_s]?></th>
				<?php
				}
				}
				?>
                </tr>
            </tfoot>
        </table>
         <br/>
	<?php
	}
	?>
    
       </td>
       </tr>
       </table>
       
      
    <?php
	
	
}



if($action=="job_color_size_cut")
{
	echo load_html_head_contents("Job Color Size","../../../", 1, 1, $unicode);
    extract($_REQUEST);
	 $sql="SELECT a.id as job_id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.location_name, a.dealing_marchant, (a.job_quantity*a.total_set_qnty) as job_quantity , a.order_uom, a.total_set_qnty,d.item_number_id as item_number_id, c.id as po_id, c.po_number, c.pub_shipment_date, c.shipment_date, c.po_received_date, (c.po_quantity*a.total_set_qnty) as po_quantity, c.excess_cut, c.plan_cut, c.unit_price, c.po_total_price,  d.country_id,d.country_ship_date, d.size_number_id, d.color_number_id, d.order_quantity, d.order_rate, d.order_total, d.excess_cut_perc, d.plan_cut_qnty
	FROM wo_po_details_master a
	LEFT JOIN wo_po_break_down c ON a.job_no = c.job_no_mst
	AND c.is_deleted =0
	AND c.status_active =1
	LEFT JOIN wo_po_color_size_breakdown d ON c.job_no_mst = d.job_no_mst
	AND c.id = d.po_break_down_id
	AND d.is_deleted =0
	AND d.status_active =1
	WHERE
	a.job_no='$job_no' and
	a.is_deleted =0 and
	a.status_active =1
	";
	$job_color_tot=0;
	$companyArr = return_library_array("select id,company_name from lib_company ","id","company_name");
	$buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
	$marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$colorArr = return_library_array("select id,color_name from lib_color","id","color_name");
    $itemSizeArr = return_library_array("select id,size_name from  lib_size ","id","size_name");
	$job_no_array=array();
	$job_size_array=array();
	$job_size_qnty_array=array();
	$job_color_array=array();
	$job_color_qnty_array=array();
	$job_color_size_qnty_array=array();
	$sql_data = sql_select($sql);
	foreach( $sql_data as $row)
	{
	$job_no_array[$row[csf('job_no')]]=array("job_no"=>$row[csf('job_no')],"job_quantity"=>$row[csf('job_quantity')],"company_name"=>$row[csf('company_name')],"buyer_name"=>$row[csf('buyer_name')],"style_ref_no"=>$row[csf('style_ref_no')],"product_dept"=>$row[csf('product_dept')],"dealing_marchant"=>$row[csf('dealing_marchant')],"job_id"=>$row[csf('job_id')],"order_uom"=>$row[csf('order_uom')]);
	$job_size_array[$row[csf('job_no')]][$row[csf('size_number_id')]]=$row[csf('size_number_id')];
	$job_size_qnty_array[$row[csf('job_no')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	$job_color_array[$row[csf('job_no')]][$row[csf('color_number_id')]]=$row[csf('color_number_id')];
	$job_color_qnty_array[$row[csf('job_no')]][$row[csf('color_number_id')]]+=$row[csf('order_quantity')];
	$job_color_size_qnty_array[$row[csf('job_no')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	}
	?>
    <table width="1030px" align="center" border="1" rules="all">
            <tr style="background-color:#FFF">
                <td width="60" align="right">Job No: </td><td width="90" ><?php echo $job_no; ?></td>
                <td width="60" align="right">Job Qnty: </td><td width="90"><?php echo $job_no_array[$job_no]['job_quantity']."(Pcs)"; ?></td>
                <td width="60" align="right">Company: </td><td width="90"><?php echo $companyArr[$job_no_array[$job_no][csf('company_name')]]; ?></td>
                <td width="60" align="right">Buyer: </td><td width="85"><?php echo $buyerArr[$job_no_array[$job_no][csf('buyer_name')]]; ?></td>
                <td width="65" align="right">Style Ref.: </td><td width="85"><?php echo $job_no_array[$job_no][csf('style_ref_no')]; ?></td>
                <td width="70" align="right">Prod. Dept.: </td><td width="80"><?php echo $product_dept[$job_no_array[$job_no][csf('product_dept')]]; ?></td>
                <td width="60" align="right">Merchant: </td><td width="90"><?php echo $marchentrArr[$job_no_array[$job_no][csf('dealing_marchant')]]; ?></td>
            </tr>
        </table>
    <table width="1030px" align="center" border="1" rules="all" class="rpt_table" >
            <thead>
                <tr>
                    <th width="60">Color</th>
                    <th width="60">Color Total</th>
                    <?php
					foreach($job_size_array[$job_no] as $key=>$value)
                    {
						if($value !="")
						{
					?>
                    <th width="60"><?php echo $itemSizeArr[$value];?></th>
                    <?php
						}
					}
					?>
                </tr>
            </thead>
            <?php
			$i=1;
			foreach($job_color_array[$job_no] as $key_c=>$value_c)
            {
			if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			if($value_c != "")
            {
			?>
           
             <tr bgcolor="<?php echo $bgcolor;?>">
             <td><?php echo  $colorArr[$value_c]; ?></td>
             <td align="right"><?php echo  $job_color_qnty_array[$job_no][$value_c]; $job_color_tot+=$job_color_qnty_array[$job_no][$value_c]; ?></td>
             <?php
					foreach($job_size_array[$job_no] as $key_s=>$value_s)
                    {
						if($value_s !="")
						{
					?>
                    <td width="60" align="right"><?php echo $job_color_size_qnty_array[$job_no][$value_c][$value_s];?></td>
                    <?php
						}
					}
					?>
             </tr>
            <?php
			$i++;
			}
			}
			?>
            <tfoot>
             <tr bgcolor="<?php // echo $bgcolor;?>">
             <th>Total</th>
             <th align="right"><?php echo  $job_color_tot; ?></th>
             <?php
					foreach($job_size_array[$job_no] as $key_s=>$value_s)
                    {
						if($value_s !="")
						{
					?>
                    <th width="60" align="right"><?php echo $job_size_qnty_array[$job_no][$value_s];?></th>
                    <?php
						}
					}
					?>
             </tr>
             </tfoot>
            </table>
    <?php
}













if($action=="job_color_size")
{
	echo load_html_head_contents("Job Color Size","../../../", 1, 1, $unicode);
    extract($_REQUEST);
	 $sql="SELECT a.id as job_id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.location_name, a.dealing_marchant, (a.job_quantity*a.total_set_qnty) as job_quantity , a.order_uom, a.total_set_qnty,b.gmts_item_id as item_number_id, c.id as po_id, c.po_number, c.pub_shipment_date, c.shipment_date, c.po_received_date, (c.po_quantity*a.total_set_qnty) as po_quantity, c.excess_cut, c.plan_cut, c.unit_price, c.po_total_price,  d.country_id,d.country_ship_date, d.size_number_id, d.color_number_id, d.order_quantity, d.order_rate, d.order_total, d.excess_cut_perc, d.plan_cut_qnty
	FROM wo_po_details_master a
	LEFT JOIN wo_po_details_mas_set_details b ON a.job_no = b.job_no
	LEFT JOIN wo_po_break_down c ON a.job_no = c.job_no_mst
	AND c.is_deleted =0
	AND c.status_active =1
	LEFT JOIN wo_po_color_size_breakdown d ON c.job_no_mst = d.job_no_mst
	AND c.id = d.po_break_down_id
	AND d.is_deleted =0
	AND d.status_active =1
	WHERE
	a.job_no='$job_no' and
	a.is_deleted =0 and
	a.status_active =1
	";
	$job_color_tot=0;
	$companyArr = return_library_array("select id,company_name from lib_company ","id","company_name");
	$buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
	$marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$colorArr = return_library_array("select id,color_name from lib_color","id","color_name");
    $itemSizeArr = return_library_array("select id,size_name from  lib_size ","id","size_name");
	$job_no_array=array();
	$job_size_array=array();
	$job_size_qnty_array=array();
	$job_color_array=array();
	$job_color_qnty_array=array();
	$job_color_size_qnty_array=array();
	$sql_data = sql_select($sql);
	foreach( $sql_data as $row)
	{
	$job_no_array[$row[csf('job_no')]]=array("job_no"=>$row[csf('job_no')],"job_quantity"=>$row[csf('job_quantity')],"company_name"=>$row[csf('company_name')],"buyer_name"=>$row[csf('buyer_name')],"style_ref_no"=>$row[csf('style_ref_no')],"product_dept"=>$row[csf('product_dept')],"dealing_marchant"=>$row[csf('dealing_marchant')],"job_id"=>$row[csf('job_id')],"order_uom"=>$row[csf('order_uom')]);
	$job_size_array[$row[csf('job_no')]][$row[csf('size_number_id')]]=$row[csf('size_number_id')];
	$job_size_qnty_array[$row[csf('job_no')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	$job_color_array[$row[csf('job_no')]][$row[csf('color_number_id')]]=$row[csf('color_number_id')];
	$job_color_qnty_array[$row[csf('job_no')]][$row[csf('color_number_id')]]+=$row[csf('order_quantity')];
	$job_color_size_qnty_array[$row[csf('job_no')]][$row[csf('color_number_id')]][$row[csf('size_number_id')]]+=$row[csf('order_quantity')];
	}
	?>
    <table width="1030px" align="center" border="1" rules="all">
            <tr style="background-color:#FFF">
                <td width="60" align="right">Job No: </td><td width="90" ><?php echo $job_no; ?></td>
                <td width="60" align="right">Job Qnty: </td><td width="90"><?php echo $job_no_array[$job_no]['job_quantity']."(Pcs)"; ?></td>
                <td width="60" align="right">Company: </td><td width="90"><?php echo $companyArr[$job_no_array[$job_no][csf('company_name')]]; ?></td>
                <td width="60" align="right">Buyer: </td><td width="85"><?php echo $buyerArr[$job_no_array[$job_no][csf('buyer_name')]]; ?></td>
                <td width="65" align="right">Style Ref.: </td><td width="85"><?php echo $job_no_array[$job_no][csf('style_ref_no')]; ?></td>
                <td width="70" align="right">Prod. Dept.: </td><td width="80"><?php echo $product_dept[$job_no_array[$job_no][csf('product_dept')]]; ?></td>
                <td width="60" align="right">Merchant: </td><td width="90"><?php echo $marchentrArr[$job_no_array[$job_no][csf('dealing_marchant')]]; ?></td>
            </tr>
        </table>
    <table width="1030px" align="center" border="1" rules="all" class="rpt_table" >
            <thead>
                <tr>
                    <th width="60">Color</th>
                    <th width="60">Color Total</th>
                    <?php
					foreach($job_size_array[$job_no] as $key=>$value)
                    {
						if($value !="")
						{
					?>
                    <th width="60"><?php echo $itemSizeArr[$value];?></th>
                    <?php
						}
					}
					?>
                </tr>
            </thead>
            <?php
			$i=1;
			foreach($job_color_array[$job_no] as $key_c=>$value_c)
            {
			if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			if($value_c != "")
            {
			?>
           
             <tr bgcolor="<?php echo $bgcolor;?>">
             <td><?php echo  $colorArr[$value_c]; ?></td>
             <td align="right"><?php echo  $job_color_qnty_array[$job_no][$value_c]; $job_color_tot+=$job_color_qnty_array[$job_no][$value_c]; ?></td>
             <?php
					foreach($job_size_array[$job_no] as $key_s=>$value_s)
                    {
						if($value_s !="")
						{
					?>
                    <td width="60" align="right"><?php echo $job_color_size_qnty_array[$job_no][$value_c][$value_s];?></td>
                    <?php
						}
					}
					?>
             </tr>
            <?php
			$i++;
			}
			}
			?>
            <tfoot>
             <tr bgcolor="<?php // echo $bgcolor;?>">
             <th>Total</th>
             <th align="right"><?php echo  $job_color_tot; ?></th>
             <?php
					foreach($job_size_array[$job_no] as $key_s=>$value_s)
                    {
						if($value_s !="")
						{
					?>
                    <th width="60" align="right"><?php echo $job_size_qnty_array[$job_no][$value_s];?></th>
                    <?php
						}
					}
					?>
             </tr>
             </tfoot>
            </table>
    <?php
}
?>
