-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pre_cost_dtls_histry`
--

DROP TABLE IF EXISTS `wo_pre_cost_dtls_histry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pre_cost_dtls_histry` (
  `id` int(11) NOT NULL DEFAULT '0',
  `approved_no` int(11) DEFAULT NULL,
  `pre_cost_dtls_id` int(11) DEFAULT NULL,
  `job_no` varchar(25) NOT NULL,
  `costing_per_id` int(11) NOT NULL DEFAULT '0',
  `order_uom_id` int(11) NOT NULL DEFAULT '0',
  `fabric_cost` double NOT NULL,
  `fabric_cost_percent` double NOT NULL,
  `trims_cost` double NOT NULL,
  `trims_cost_percent` double NOT NULL,
  `embel_cost` double NOT NULL,
  `embel_cost_percent` double NOT NULL,
  `wash_cost` double NOT NULL,
  `wash_cost_percent` double NOT NULL,
  `comm_cost` double NOT NULL,
  `comm_cost_percent` double NOT NULL,
  `commission` double NOT NULL DEFAULT '0',
  `commission_percent` double NOT NULL DEFAULT '0',
  `lab_test` double NOT NULL,
  `lab_test_percent` double NOT NULL,
  `inspection` double NOT NULL,
  `inspection_percent` double NOT NULL,
  `cm_cost` double NOT NULL,
  `cm_cost_percent` double NOT NULL,
  `freight` double NOT NULL,
  `freight_percent` double NOT NULL,
  `currier_pre_cost` double NOT NULL,
  `currier_percent` double NOT NULL,
  `certificate_pre_cost` double NOT NULL,
  `certificate_percent` double NOT NULL,
  `common_oh` double NOT NULL,
  `common_oh_percent` double NOT NULL,
  `total_cost` double NOT NULL,
  `total_cost_percent` double NOT NULL,
  `price_dzn` double NOT NULL,
  `price_dzn_percent` double NOT NULL,
  `margin_dzn` double NOT NULL,
  `margin_dzn_percent` double NOT NULL,
  `cost_pcs_set` double NOT NULL,
  `cost_pcs_set_percent` double NOT NULL,
  `price_pcs_or_set` double NOT NULL,
  `price_pcs_or_set_percent` double NOT NULL,
  `margin_pcs_set` double NOT NULL,
  `margin_pcs_set_percent` double NOT NULL,
  `cm_for_sipment_sche` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  KEY `job_no` (`job_no`),
  KEY `costing_per_id` (`costing_per_id`),
  KEY `order_uom_id` (`order_uom_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pre_cost_dtls_histry`
--

LOCK TABLES `wo_pre_cost_dtls_histry` WRITE;
/*!40000 ALTER TABLE `wo_pre_cost_dtls_histry` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_pre_cost_dtls_histry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
