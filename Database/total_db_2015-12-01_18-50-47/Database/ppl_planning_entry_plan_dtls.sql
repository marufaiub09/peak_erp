-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppl_planning_entry_plan_dtls`
--

DROP TABLE IF EXISTS `ppl_planning_entry_plan_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppl_planning_entry_plan_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `dtls_id` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `booking_no` varchar(25) NOT NULL,
  `po_id` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `finish_date` date NOT NULL,
  `body_part_id` int(11) NOT NULL DEFAULT '0',
  `color_type_id` int(11) NOT NULL DEFAULT '0',
  `determination_id` int(11) NOT NULL DEFAULT '0',
  `fabric_desc` varchar(255) NOT NULL,
  `gsm_weight` int(11) NOT NULL DEFAULT '0',
  `dia` varchar(20) NOT NULL,
  `width_dia_type` tinyint(2) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `yarn_desc` int(11) NOT NULL DEFAULT '0' COMMENT 'Pre Cost ID',
  `program_qnty` double NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Yes 0;No',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Yes 0:No ',
  PRIMARY KEY (`id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`),
  KEY `company_id` (`company_id`),
  KEY `buyer_id` (`buyer_id`),
  KEY `booking_no` (`booking_no`),
  KEY `po_id` (`po_id`),
  KEY `start_date` (`start_date`),
  KEY `finish_date` (`finish_date`),
  KEY `body_part_id` (`body_part_id`),
  KEY `fabric_desc` (`fabric_desc`),
  KEY `gsm_weight` (`gsm_weight`),
  KEY `dia` (`dia`),
  KEY `width_dia_type` (`width_dia_type`),
  KEY `yarn_desc` (`yarn_desc`),
  KEY `color_id` (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppl_planning_entry_plan_dtls`
--

LOCK TABLES `ppl_planning_entry_plan_dtls` WRITE;
/*!40000 ALTER TABLE `ppl_planning_entry_plan_dtls` DISABLE KEYS */;
INSERT INTO `ppl_planning_entry_plan_dtls` VALUES (1,1,1,1,2,'PAL-Fb-15-00001',24,'0000-00-00','0000-00-00',1,1,98,'S/Lacost, Cotton 95% Elastane 5%',190,'68',1,0,42,211.48360486698,1,'2015-04-27 10:55:28',0,'0000-00-00 00:00:00',1,0),(2,1,1,1,2,'PAL-Fb-15-00001',24,'0000-00-00','0000-00-00',40,1,98,'S/Lacost, Cotton 95% Elastane 5%',190,'68',1,0,133,79.516395133017,1,'2015-04-27 10:55:28',0,'0000-00-00 00:00:00',1,0),(3,2,2,1,1,'PAL-Fb-15-00004',13,'0000-00-00','0000-00-00',2,2,81,'Flat Knit Collar, Cotton 100%',350,'Any',1,0,12,186,1,'2015-04-27 11:13:01',0,'0000-00-00 00:00:00',1,0),(7,4,4,1,2,'PAL-Fb-15-00001',24,'0000-00-00','0000-00-00',60,1,3,'Single Jersey, Cotton 100%',140,'68',1,0,134,5,2,'2015-05-04 15:39:26',0,'0000-00-00 00:00:00',1,0),(9,3,3,1,2,'PAL-Fb-15-00042',104,'0000-00-00','0000-00-00',1,1,3,'Single Jersey, Cotton 100%',140,'70',1,0,160,2,2,'2015-05-04 16:40:26',0,'0000-00-00 00:00:00',1,0),(10,3,5,1,2,'PAL-Fb-15-00042',104,'0000-00-00','0000-00-00',1,1,3,'Single Jersey, Cotton 100%',140,'70',1,0,160,7,2,'2015-05-04 16:51:14',0,'0000-00-00 00:00:00',1,0),(11,5,6,1,2,'PAL-Fb-15-00020',25,'0000-00-00','0000-00-00',13,1,98,'S/Lacost, Cotton 95% Elastane 5%',190,'0',1,0,151,3,2,'2015-05-09 12:03:10',0,'0000-00-00 00:00:00',1,0),(12,6,7,1,14,'PAL-Fb-15-00100',235,'0000-00-00','0000-00-00',1,1,97,'Ly/Single Jersey, Cotton 95% Elastane 5%',140,'72 OPEN',1,0,303,652,17,'2015-05-12 17:56:38',0,'0000-00-00 00:00:00',1,0),(13,7,8,1,30,'PAL-Fb-15-00006',92,'0000-00-00','0000-00-00',4,1,62,'1X1 RIB, Cotton 95% Spandex 5%',220,'43 INCH',1,0,38,10,2,'2015-05-26 09:49:06',0,'0000-00-00 00:00:00',1,0),(14,8,9,1,14,'PAL-Fb-15-00099',239,'0000-00-00','0000-00-00',1,1,18,'S/Lacoste, Cotton 100%',180,'36 Inch',2,0,312,147.48,17,'2015-05-27 16:47:23',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `ppl_planning_entry_plan_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
