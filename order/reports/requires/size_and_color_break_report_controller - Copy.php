<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_id", 130, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $selected, "" );   	 
	exit();
}

if ($action=="job_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);
	
?>	
    <script>
	/*var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_job_id').val( id );
		$('#txt_job_val').val( ddd );
	} */
	
	function js_set_value( job_id )
	{
		//alert(po_id)
		document.getElementById('txt_job_id').value=job_id;
		parent.emailwindow.hide();
	}
		  
	</script>
     <input type="text" id="txt_job_id" />
 <?php
	if ($data[0]==0) $company_id=""; else $company_id=" and company_name=$data[0]";
	if ($data[1]==0) $buyer_id=""; else $buyer_id=" and buyer_name=$data[1]";
	
	$marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$teamMemberArr = return_library_array("select id,team_leader_name from lib_marketing_team ","id","team_leader_name");
	
	$sql= "select id, job_no, style_ref_no, product_dept, dealing_marchant, team_leader from wo_po_details_master where status_active=1 and is_deleted=0 $company_id $buyer_id order by job_no";
	//echo $sql;
	
	$arr=array(2=>$product_dept,3=>$marchentrArr,4=>$teamMemberArr);
	echo  create_list_view("list_view", "Job No,Style Ref.,Prod. Dept.,Marchant,Team Name", "100,110,110,150,150","680","360",0, $sql , "js_set_value", "id,job_no", "", 1, "0,0,product_dept,dealing_marchant,team_leader", $arr , "job_no,style_ref_no,product_dept,dealing_marchant,team_leader", "",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
	exit();
}

if ($action=="po_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);
	
?>	
    <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
		  
	</script>
     <input type="text" id="txt_po_id" />
     <input type="text" id="txt_po_val" />
     <?php
	if ($data[2]=="") $job_num=""; else $job_num=" and a.job_no_mst='$data[2]'";
	
	$sql= "select a.id, a.po_number, a.job_no_mst, a.shipment_date, b.item_number_id from wo_po_break_down a, wo_po_color_size_breakdown b where a.id=b.po_break_down_id and a.status_active=1 and a.is_deleted=0 $job_num group by a.id order by po_number";
	//echo $sql;die;
	
	$arr=array(3=>$garments_item);
	echo  create_list_view("list_view", "PO No.,Job No.,Shipment Date,Item Name", "100,100,80,150","450","360",0, $sql , "js_set_value", "id,po_number", "", 1, "0,0,0,item_number_id", $arr , "po_number,job_no_mst,shipment_date,item_number_id", "",'setFilterGrid("list_view",-1);','0,0,3,0','',1) ;
	exit();	 
}

if ($action=="report_generate")
{
	extract($_REQUEST);
	//echo $txt_job_no;
	
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_buyer=str_replace("'","",$cbo_buyer_id);
	$job_no=str_replace("'","",$txt_job_no);
	$hidd_job=str_replace("'","",$hidd_job_id);
	$hidd_po=str_replace("'","",$hidd_po_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	//echo $date_from;
	
	if ($cbo_company==0) $company_id=""; else $company_id=" and a.company_name=$cbo_company";
	if ($cbo_buyer==0) $buyer_id=""; else $buyer_id=" and a.buyer_name=$cbo_buyer";
	if ($job_no=="") $job_num=""; else $job_num=" and a.job_no='$job_no'";
	if ($job_no=="") $job_num_mst=""; else $job_num_mst=" and b.job_no_mst=$job_no";
	if ($hidd_job==0) $job_id=""; else $job_id=" and a.id in ($hidd_job)";
	if ($hidd_po==0) $po_id=""; else $po_id=" and c.id in ( $hidd_po )";
 	//if( $date_from==0 && $date_to==0 ) $maturity_date=""; else $maturity_date= " and a.maturity_date between '".$date_from."' and '".$date_to."'";
	
	$companyArr = return_library_array("select id,company_name from lib_company ","id","company_name");
	$buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
	$marchentrArr = return_library_array("select id,team_member_name from lib_mkt_team_member_info ","id","team_member_name");
	$itemSizeArr = return_library_array("select id,size_name from  lib_size ","id","size_name");
	$countryArr = return_library_array("select id,country_name from lib_country ","id","country_name");
	$colorArr = return_library_array("select id,color_name from lib_color","id","color_name");
	
	$job_no_array=array();
	$job_no_tmp=array();
	$sql="select a.id, a.job_no, a.job_quantity, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.dealing_marchant from wo_po_details_master a,wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 $company_id $buyer_id $job_id group by a.job_no order by job_no ";
	//$sql="SELECT a.id, a.job_no, a.job_quantity, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.dealing_marchant, b.id AS po_id, b.po_number, b.po_quantity, c.item_number_id, c.color_number_id, c.order_quantity, c.size_number_id, c.country_id FROM wo_po_details_master a, wo_po_break_down b, wo_po_color_size_breakdown c WHERE b.id = c.po_break_down_id AND a.job_no = b.job_no_mst AND a.status_active =1 AND a.is_deleted =0 $company_id $buyer_id $job_id GROUP BY a.job_no, c.item_number_id, b.po_number, c.country_id, c.color_number_id ORDER BY a.job_no";
	$sql_data = sql_select($sql);
	foreach( $sql_data as $row)
	{
		$job_no_array[$row[csf('job_no')]]['job_no']=$row[csf('job_no')];
		$job_no_array[$row[csf('job_no')]]['job_quantity']=$row[csf('job_quantity')];
		$job_no_array[$row[csf('job_no')]]['company_name']=$row[csf('company_name')];
		$job_no_array[$row[csf('job_no')]]['buyer_name']=$row[csf('buyer_name')];
		$job_no_array[$row[csf('job_no')]]['style_ref_no']=$row[csf('style_ref_no')];
		$job_no_array[$row[csf('job_no')]]['product_dept']=$row[csf('product_dept')];
		$job_no_array[$row[csf('job_no')]]['dealing_marchant']=$row[csf('dealing_marchant')];
		$job_no_array[$row[csf('job_no')]]['id']=$row[csf('id')];
		$job_no_tmp[]=$row[csf('job_no')];
	}
	$new_job="'".implode("','",$job_no_tmp)."'";
	
	$sql_dtls="select c.id  as po_id, c.po_number, c.po_quantity, d.item_number_id, d.color_number_id, d.order_quantity, d.size_number_id, d.country_id, d.po_break_down_id, d.job_no_mst from wo_po_break_down c, wo_po_color_size_breakdown d where c.job_no_mst in ( $new_job ) and c.id=d.po_break_down_id and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $po_id group by d.item_number_id, c.po_number, d.country_id, d.color_number_id";//, d.order_quantity
	
	$sql_result = sql_select($sql_dtls);
	$rpt_details_array=array(); 
	$item_details_array=array(); 
	$po_details_array=array(); 
	$country_details_array=array(); 
	
	foreach ($sql_result as $rows)
	{
		$rpt_details_array[$rows[csf('job_no_mst')]][$rows[csf('item_number_id')]][$rows[csf('po_number')]][$rows[csf('country_id')]][$rows[csf('color_number_id')]]['po_quantity']=$rows[csf('po_quantity')];
		$item_details_array[$rows[csf('job_no_mst')]][$rows[csf('item_number_id')]]['country_id']=$rows[csf('country_id')];
		$item_details_array[$rows[csf('job_no_mst')]][$rows[csf('item_number_id')]]['color_number_id']=$rows[csf('color_number_id')];
		$item_details_array[$rows[csf('job_no_mst')]][$rows[csf('item_number_id')]]['po_quantity']=$rows[csf('po_quantity')];
		$item_details_array[$rows[csf('job_no_mst')]][$rows[csf('item_number_id')]]['size_number_id']=$rows[csf('size_number_id')];
		
		//$rpt_details_array[$rows[job_no_mst]][$rows[item_number_id]][$rows[po_number]][$rows[country_id]][$rows[color_number_id]][$rows[po_number]]['po_quantity']=$rows[po_quantity];
		//c.id  as po_id, c.po_number, c.po_quantity, d.item_number_id, d.color_number_id,sum(d.order_quantity) as order_quantity, d.size_number_id, d.country_id, d.job_no_mst
		
	}
	
	?>
    <div id="scroll_body" align="center" style="height:auto; width:1100px; margin:0 auto; padding:0;">
    <?php
	//var_dump($job_no_array);
	//var_dump($rpt_details_array);
	foreach($job_no_array as $rdata=>$det)
	{
		 ?>
            <table width="1050px" align="center" border="1" rules="all" >
                <tr>
                   	<td width="60" align="right">Job No: </td><td width="90"><?php echo $det['job_no']; ?></td>
                    <td width="60" align="right">Job Qnty: </td><td width="90"><?php echo $det['job_quantity'].(" (Pcs)"); ?></td>
                    <td width="60" align="right">Company: </td><td width="90"><?php echo $companyArr[$row[csf('company_name')]]; ?></td>
                    <td width="60" align="right">Buyer: </td><td width="85"><?php echo $buyerArr[$row[csf('buyer_name')]]; ?></td>
                    <td width="65" align="right">Style Ref.: </td><td width="85"><?php echo $row[csf('style_ref_no')]; ?></td>
                    <td width="70" align="right">Prod. Dept.: </td><td width="80"><?php echo $product_dept[$row[csf('product_dept')]]; ?></td>
                    <td width="60" align="right">Merchent: </td><td width="90"><?php echo $marchentrArr[$row[csf('dealing_marchant')]]; ?></td>
                </tr>
            </table>
            <?php
		$tmp_job=array();
		//foreach($rpt_details_array as $job=>$jobdets)
		//{
			//if ($det['job_no']==$job)
			//{
				if(!in_array($job,$tmp_job))
				{
					$tmp_job[]=$job;
					?>
					<table width="1050px" align="center" border="1" rules="all" class="rpt_table" >
					<thead>
					<tr>
						<th width="70">Item </th>
						<th width="60">PO Number</th>
						<th width="60">Country</th>
						<th width="60">PO Qnty</th>
						<th width="60">Color</th>
						<th width="60">Color Total</th>
                        <?php
                            $job_mst_id=$rows[csf('job_no_mst')];
							$po_break_down_id=$rows[csf('po_break_down_id')];
                            $sql_size="Select a.id, a.size_name, b.id as poid from  lib_size a,  wo_po_color_size_breakdown b where a.id=b.size_number_id and b.status_active=1 and b.is_deleted=0 and b.job_no_mst='$job_mst_id' group by b.size_number_id order by b.id";
                            //echo $sql_size;
                            $sql_result_size = sql_select($sql_size);
                            foreach($sql_result_size as $size)
                            {
                                ?>
                                    <th width="60"><?php echo $size[csf('size_name')]; ?></th>
                                <?php
                            }
                            ?>
					</tr>
					</thead>
					<?php
				}
			//print_r($jobdets); 
			
				//foreach($jobdets as $itemdata=>$itemdets)
				foreach($rpt_details_array[$det['job_no']] as $itemdata=>$itemdets)
				{
					//$item_details_array[$rows[job_no_mst]][$rows[item_number_id]]['country_id']=$rows[country_id];
			//$item_details_array[$rows[job_no_mst]][$rows[item_number_id]]['color_number_id']=$rows[color_number_id];
			//$item_details_array[$rows[job_no_mst]][$rows[item_number_id]]['po_quantity']=$rows[po_quantity];
					
					$f=0;
					foreach($itemdets as $key=>$val)
					{
						foreach($val as $key2=>$val2)
						{
							foreach($val2 as $key3=>$val3)
							{
								$y++;
								//echo $y."<br>";
							}
						}
					}
					//echo $f."<br>";
					foreach($itemdets as $podata=>$podets)
					{ 
						//var_dump($podets);
						$z=0;
						foreach($podets as $key=>$val)
						{
							foreach($val as $key2=>$val2)
							{
								$z++;
							}
						}
						//echo $z."<br>";
						$x=0;
						
						foreach($podets as $countrydata=>$countrydets)
						{
							//print_r($countrydata);
							
							//print_r (count($item_details_array[$rows[job_no_mst]][$rows[item_number_id]]['size_number_id']));
							
							$item_rowspan=count($countrydets);
							$s=0;
							foreach($countrydets as $colordata=>$colordets)
							{
								//print_r($item_details_array[$rows[job_no_mst]][$rows[item_number_id]]['po_quantity']);
								?>
								<tr>
									<?php 
									if($f==0)
									{
									?>
										<td valign="middle" rowspan="<?php echo $y; ?>" ><?php echo $garments_item[$itemdata]; ?></td> 
									<?php	
									}
									if($x==0)
									{
									?>
										<td valign="middle" rowspan="<?php echo $z; ?>" ><?php echo $podata; ?></td> 
									<?php	
									}
									if($s==0)
									{
									?>
										 <td valign="middle" rowspan="<?php echo $item_rowspan; ?>"><?php echo $countryArr[$countrydata]; ?></td>
										 <td valign="middle" rowspan="<?php echo $item_rowspan; ?>"><?php echo $item_details_array[$rows[csf('job_no_mst')]][$rows[csf('item_number_id')]]['po_quantity']; ?></td
									><?php
									}
									?>
									<td><?php echo $colorArr[$colordata]; ?></td>
									<td><?php //echo $colordets[po_quantity]; ?></td>
									<td><?php //echo $colorArr[$colordata]; ?></td>
								</tr>
								<?php
								$s++;
								$x++;
								$f++;
							}
						}
					}
				//}
			//}
			?>
            </table>
            <?php
		}
	}
	die;
	?>
    <?php
	die;
	
	$sql="select a.id, a.job_no, a.job_quantity, a.company_name, a.buyer_name, a.style_ref_no, a.product_dept, a.dealing_marchant from wo_po_details_master a,wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 $company_id $buyer_id $job_id group by a.job_no order by job_no ";
	echo $sql;
	$sql_data = sql_select($sql);
	
	$color=1;  
	foreach( $sql_data as $row)
	{
		$color++;
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			//echo $colorArr[2];
			$mst_id=$row[csf('id')];
		?>
		<div id="scroll_body" align="center" style="height:auto; width:1100px; margin:0 auto; padding:0;">
			<table width="1050px" align="center" border="1" rules="all" class="rpt_table" >
            	<thead>
                    <tr>
                        <th width="70">Job No:</th>
                        <th width="60">Job Qnty</th>
                        <th width="60">Company</th>
                        <th width="60">Buyer</th>
                        <th width="60">Style Ref.</th>
                        <th width="60">Prod. Dept.</th>
                        <th width="60">Merchent</th>
                    </tr>
                    <tr>
                        <th width="70"><?php echo $row[csf('job_no')]; ?></th>
                        <th width="60"><?php echo $row[csf('job_quantity')].(" (Pcs)"); ?></th>
                        <th width="60"><?php echo $companyArr[$row[csf('company_name')]]; ?></th>
                        <th width="60"><?php echo $buyerArr[$row[csf('buyer_name')]]; ?></th>
                        <th width="60"><?php echo $row[csf('style_ref_no')]; ?></th>
                        <th width="60"><?php echo $product_dept[$row[csf('product_dept')]]; ?></th>
                        <th width="60"><?php echo $marchentrArr[$row[csf('dealing_marchant')]]; ?></th>
                    </tr>
                </thead>
            </table>
       
            <div align="center" style="height:auto; width:1100px; margin:0 auto; padding:0;">
			<table width="1050px" align="center" border="1" rules="all" class="rpt_table" >
            	<thead>
        
	<?php
	$job=$row[csf('job_no')];
	$sql_dtls="select c.id  as po_id, c.po_number, c.po_quantity, d.item_number_id, d.color_number_id,sum(d.order_quantity) as order_quantity, d.size_number_id, d.country_id, d.job_no_mst from wo_po_break_down c, wo_po_color_size_breakdown d where c.job_no_mst='$job' and c.id=d.po_break_down_id and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $po_id group by d.item_number_id, c.po_number, d.country_id";
	//$sql_dtls="Select  from wo_po_break_down where";
	
	 echo $sql_dtls;
	$sql_result = sql_select($sql_dtls);
	$row_count=count($sql_result);
	//echo $row_count;
	$item_size_array=array();
	$item_array=array();
	$td_int=0;
	
	foreach ($sql_result as $rows)
	{
		$po_break_down_id=$rows[csf('po_id')];
		$sql_size="Select a.id, a.size_name, b.id as poid from  lib_size a,  wo_po_color_size_breakdown b where a.id=b.size_number_id and b.status_active=1 and b.is_deleted=0  and b.po_break_down_id='$po_break_down_id' ";
		//echo $sql_size;
		$sql_result_size = sql_select($sql_size);
		
		if($sql_result_size!=0)
		{
			?>
            <tr>
                <?php
                if($td_int==0)
                {
                ?>
                    <th width="70">Item </th>
                    <th width="60">PO Number</th>
                    <th width="60">Country</th>
                    <th width="60">PO Qnty</th>
                    <th width="60">Color</th>
                    <th width="60">Color Total</th>
                    <?php 
                }
                else
                {
                ?>
                    <th colspan="6" height="0"  bgcolor="#E5E5E5"></th>	
                        <?php
                }//if($td_int==0)
			}//if($row_num_td!=0)
		$job_mst_id=$rows[csf('job_no_mst')];
		$sql_size="Select a.id, a.size_name, b.id as poid from  lib_size a,  wo_po_color_size_breakdown b where a.id=b.size_number_id and b.status_active=1 and b.is_deleted=0 and b.po_break_down_id='$po_break_down_id' group by a.size_name order by b.id ";
		//echo $sql_size;
		$sql_result_size = sql_select($sql_size);
		foreach($sql_result_size as $size)
		{
			?>
                <th width="60"><?php echo $size[csf('size_name')]; ?></th>
            <?php
        }
        ?>
            </tr>
        </thead>
        <?php
		$sql_color="Select a.id, a.color_name from lib_color a, wo_po_color_size_breakdown b where  a.id=b.color_number_id and b.status_active=1 and b.is_deleted=0 and b.po_break_down_id='$po_break_down_id' order by b.id ";
		$sql_result_color = sql_select($sql_color);
		$rowspna_item=count($sql_result_color);
		$tr=1;
		
		foreach($sql_result as $rows)
		{
			if ($tr%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				
			?>
				<tr id="tr_<?php echo $tr; ?>" bgcolor="<?php echo $bgcolor; ?>">
			<?php
			if($tr==1)
			{
			?>
				<td width="100" rowspan="<?php if($tr==1){echo $rowspna_item; } ?>" align="center"><?php echo $garments_item[$rows[csf('item_number_id')]]; ?></td>
				<?php
				//}
				?>
					<td width="60" rowspan="<?php if($tr==1){echo $rowspna_item; } ?>" align="center"><?php echo $rows[csf('po_number')]; ?></td>
				<?php
			}
				?>
					<td width="60"  align="center"><?php echo $countryArr[$rows[csf('country_id')]]; ?></td>
					<td width="60" rowspan="<?php //if($tr==1){echo $rowspna_item; } ?>" align="right" ><?php echo number_format($rows[csf('order_quantity')],0); ?></td>
					<td width="60" rowspan="<?php echo $sql_result_color; ?>" align="center"><?php echo $colorArr[$rows[csf('color_number_id')]]; ?></td>
					<td width="60" rowspan="<?php echo $sql_result_color; ?>" align="right"><?php echo number_format($rows[csf('order_quantity')],0); ?></td>
				<?php
				$tr++;
			}
			?>
			</tr>
			<?php
			if($sql_result_size!=0)
			{
                ?>
                <tr > 
                    <td colspan="6" align="right" bgcolor="#A8C7C2"><strong>Size Total</strong></td>
                </tr>      
            <?php
			}
			$td_int++;
			}// while ($row_po=mysql_fetch_array($company_sql_po))
			?>
            </table>
           </div>
        </div>
        <br />
	<?php
	}
}
?>
