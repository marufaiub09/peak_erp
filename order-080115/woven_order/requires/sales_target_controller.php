﻿<? 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if ($_SESSION['logic_erp']["data_level_secured"]==1)
{
	if($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_cond=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_cond="";
	if($_SESSION['logic_erp']["company_id"]!=0) $company_cond=" and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_cond="";
}


{
	$buyer_cond="";	$company_cond="";
}


 //cbo_buyer_name,cbo_team_leader,text_designation_value,cbo_starting_month,cbo_starting_year

//---------------------------------------------------- Start
//Md. Rabiul Islam Created the page  here


if ($action=="save_update_delete_mst")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$id=return_next_id( "id", "wo_sales_target_mst", 1 ) ;		 
		$field_array="id,cbo_buyer_name,cbo_team_leader,text_designation_value,cbo_starting_month,cbo_starting_year,total_target_qty, total_target_value, insert_date, inserted_by, status_active,is_deleted";
		$data_array="(".$id.",".$cbo_buyer_name.",".$cbo_team_leader.",".$text_designation_value.",".$cbo_starting_month.",".$cbo_starting_year.",1,1,".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1,0)";
		
 		$rID=sql_insert("wo_sales_target_mst",$field_array,$data_array,1);
		//echo  $rID; die;
		
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$cbo_buyer_name[0]."**".$rID;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$cbo_buyer_name[0]."**".$rID;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$cbo_buyer_name[0]."**".$rID;
		}
		disconnect($con);
		die;
	}


	
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if (is_duplicate_field( "cbo_buyer_name", "wo_sales_target_mst", "cbo_buyer_name=$cbo_buyer_name" ) ==1)
		{
			echo "11**0"; 
			die;			
		}
		 
		 
		 
		 //update code here
		$field_array="id,cbo_buyer_name,cbo_team_leader,text_designation_value,cbo_starting_month,cbo_starting_year,total_target_qty, total_target_value, insert_date, inserted_by, status_active,is_deleted";
		 
		$data_array="".$cbo_buyer_name."*".$cbo_team_leader."*".$text_designation_value."*".$cbo_starting_month."*".$cbo_starting_year."*1*1".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"*1*0;
 	
		$rID=sql_update("wo_sales_target_mst",$field_array,$data_array,"","".$cbo_buyer_name."",1);
		$txt_job_no=str_replace("'","",$cbo_buyer_name);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$cbo_buyer_name."**".$rID;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$cbo_buyer_name."**".$rID;
			}
		}
		
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$cbo_buyer_name."**".$rID;
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
			
		$field_array="status_active*is_deleted";
		$data_array="'0'*'1'";
		$rID=sql_delete("wo_sales_target_mst",$field_array,$data_array,"","".$cbo_buyer_name."",1);
		disconnect($con);
		echo "2****".$rID;
	}
}


	//end the page of 21-10-2012


if ($action=="show_po_active_listview")
{
	 
	$arr=array (0=>$order_status,11=>$row_status);
 	$sql= "select is_confirmed,po_number,po_received_date,shipment_date,pub_shipment_date,po_quantity,unit_price,po_total_price,excess_cut,plan_cut,DATEDIFF(shipment_date,po_received_date) date_diff,status_active,id from  wo_po_break_down  where   status_active=1 and is_deleted=0 and job_no_mst='$data'"; 
	 
	echo  create_list_view("list_view", "Order Status,PO No,PO Recv. Date,Ship Date,Orgn. Ship Date,PO Qnty,Avg. Rate,Amount, Excess Cut %,Plan Cut Qnty,Lead Time,Status", "90,130,80,80,80,80,80,80,80,80,50,70","1050","220",0, $sql , "get_php_form_data", "id", 1, 1, "is_confirmed,0,0,0,0,0,0,0,0,0,0,status_active", $arr , "is_confirmed,po_number,po_received_date,shipment_date,pub_shipment_date,po_quantity,unit_price,po_total_price,excess_cut,plan_cut,date_diff,status_active", "../woven_order/requires/woven_order_entry_controller/",'','0,0,3,3,3,1,2,2,2,2,1') ;
	
}

if ($action=="show_deleted_po_active_listview")
{
	 
	$arr=array (0=>$order_status,11=>$row_status);
 	$sql= "select is_confirmed,po_number,po_received_date,shipment_date,pub_shipment_date,po_quantity,unit_price,po_total_price,excess_cut,plan_cut,DATEDIFF(shipment_date,po_received_date) date_diff,status_active,id from  wo_po_break_down  where   status_active<>1 and is_deleted<>0 and job_no_mst='$data'"; 
  
	echo  create_list_view("list_view", "Order Status,PO No,PO Recv. Date,Ship Date,Orgn. Ship Date,PO Qnty,Avg. Rate,Amount, Excess Cut %,Plan Cut Qnty,Lead Time,Status", "90,130,80,80,80,80,80,80,80,80,50,70","1050","220",0, $sql , "get_php_form_data", "id", 1, 1, "is_confirmed,0,0,0,0,0,0,0,0,0,0,status_active", $arr , "is_confirmed,po_number,po_received_date,shipment_date,pub_shipment_date,po_quantity,unit_price,po_total_price,excess_cut,plan_cut,date_diff,status_active", "../woven_order/requires/woven_order_entry_controller/",'','0,0,3,3,3,1,2,2,2,2,1') ;
	
}


if ($action=="cbo_dealing_merchant")
{
	echo create_drop_down( "cbo_dealing_merchant", 172, "select id,team_member_name from lib_mkt_team_member_info where status_active =1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-- Select Team Member --", $selected, "" );
	
}

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 172, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select --", $selected, "" );		 
}

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 172, "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id and b.tag_company='$data' and a.party_type='' order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );   	 
} 

if ($action=="load_drop_down_agent")
{
	echo create_drop_down( "cbo_agent", 172, "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id and b.tag_company='$data' and a.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3))  order by buyer_name","id,buyer_name", 1, "-- Select Agent --", $selected, "" );  
	 	 
} 

if ($action=="save_update_delete_mst")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		 
		$id=return_next_id( "id", "wo_po_details_master", 1 ) ;
		$new_job_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', '', date("Y",time()), 5, "select job_no_prefix,job_no_prefix_num from wo_po_details_master where company_name=$cbo_company_name order by job_no_prefix_num desc ", "job_no_prefix", "job_no_prefix_num" ));
		 
		$field_array="id,garments_nature,job_no,job_no_prefix,job_no_prefix_num,company_name,buyer_name,location_name,style_ref_no,style_description,product_dept,currency_id,agent_name,order_repeat_no,region,exporting_item_catg,team_leader,dealing_marchant,packing,remarks,ship_mode,delay_clause,is_deleted,status_active,inserted_by,insert_date";
		 
		$data_array="(".$id.",".$garments_nature.",'".$new_job_no[0]."','".$new_job_no[1]."','".$new_job_no[2]."',".$cbo_company_name.",".$cbo_location_name.",".$cbo_buyer_name.",".$txt_style_ref.",".$txt_style_description.",".$cbo_product_department.",".$cbo_currercy.",".$cbo_agent.",".$txt_repeat_no.",".$cbo_region.",".$txt_item_catgory.",".$cbo_team_leader.",".$cbo_dealing_merchant.",".$cbo_packing.",".$txt_remarks.",".$cbo_ship_mode.",".$txt_delay_clause.",0,1,".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
 	
 		$rID=sql_insert("wo_po_details_master",$field_array,$data_array,1);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$new_job_no[0]."**".$rID;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_job_no[0]."**".$rID;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$new_job_no[0]."**".$rID;
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array="company_name*buyer_name*location_name*style_ref_no*style_description*product_dept*currency_id*agent_name*order_repeat_no*region*exporting_item_catg*team_leader*dealing_marchant*packing*remarks*ship_mode*delay_clause*is_deleted*status_active*updated_by*update_date";
		 
		$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_buyer_name."*".$txt_style_ref."*".$txt_style_description."*".$cbo_product_department."*".$cbo_currercy."*".$cbo_agent."*".$txt_repeat_no."*".$cbo_region."*".$txt_item_catgory."*".$cbo_team_leader."*".$cbo_dealing_merchant."*".$cbo_packing."*".$txt_remarks."*".$cbo_ship_mode."*".$txt_delay_clause."*0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
 	
		$rID=sql_update("wo_po_details_master",$field_array,$data_array,"job_no","".$txt_job_no."",1);
		
		$txt_job_no=str_replace("'","",$txt_job_no);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$txt_job_no."**".$rID;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$txt_job_no."**".$rID;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$txt_job_no."**".$rID;
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		
		$field_array="status_active*is_deleted";
		$data_array="'2'*'1'";
		$rID=sql_delete("wo_po_details_master",$field_array,$data_array,"job_no","".$txt_job_no."",1);
		disconnect($con);
		echo "2****".$rID;
	}
}

 


if ($action=="get_excess_cut_percent")
{
	$data=explode("_",$data);
	 $qry_result=sql_select( "select slab_rang_start,slab_rang_end,excess_percent from  var_prod_excess_cutting_slab where company_name='$data[1]' and variable_list=2 and status_active=1 and is_deleted=0");
	 foreach ($qry_result as $row)
	 {
		 if ( $data[0]>=$row[csf("slab_rang_start")] && $data[0]<=$row[csf("slab_rang_end")] )
		 {
			 echo $row[csf("excess_percent")]; die;
		 }
	 }
	 echo "0"; die;
}
 
 
if($action=="create_po_search_list_view")
{
	
	$data=explode('_',$data);
	if ($data[0]!=0) $company=" and a.company_name='$data[0]'"; else { echo "Please Select Company First."; die; }
	
	if ($data[1]!=0) $buyer=" and a.buyer_name='$data[1]'"; else { echo "Please Select Buyer First."; die; }
	
	//if ($data[2]!=0) $buyer=" and a.buyer_name='$data[2]'"; else { echo "Please Select CHEK First."; die; } die;
	
	if ($data[3]!="" &&  $data[4]!="") $shipment_date = "and b.shipment_date between '".change_date_format($data[3], "yyyy-mm-dd", "-")."' and '".change_date_format($data[4], "yyyy-mm-dd", "-")."'"; else $shipment_date ="";
	 
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');

	$arr=array (1=>$comp,2=>$buyer_arr);
	
	if ($data[2]==0)
	{
		$sql= "select a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.job_quantity, b.po_number,b.po_quantity,b.shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1  and b.status_active=1 $shipment_date $company $buyer order by a.job_no";  
		
		echo  create_list_view("list_view", "Job No,Company,Buyer Name,Style Ref. No,Job Qty.,PO number,PO Quantity,Shipment Date", "90,120,100,100,90,90,90,80","1000","320",0, $sql , "get_php_form_data", "id", 1, 1, "0,company_name,buyer_name,0,0,0,0", $arr , "job_no,company_name,buyer_name,style_ref_no,job_quantity,po_number,po_quantity,shipment_date", "../contact_details/requires/",'','0,0,0,0,1,0,1,3') ;
	}
	else
	{
		$sql= "select a.job_no,a.company_name,a.buyer_name,a.style_ref_no from wo_po_details_master a where a.status_active=1  and a.is_deleted=0 $company $buyer order by a.job_no";  
		// $sql= "select a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.job_quantity from wo_po_details_master a,wo_po_break_down b where a.job_no!=b.job_no_mst and  a.status_active=1  and a.is_deleted=0 $company $buyer $extra_cond"; 
		echo  create_list_view("list_view", "Job No,Company,Buyer Name,Style Ref. No,", "90,120,100,100,90","1000","320",0, $sql , "get_php_form_data", "id", 1, 1, "0,company_name,buyer_name,0,0,0,0", $arr , "job_no,company_name,buyer_name,style_ref_no", "../contact_details/requires/",'','0,0,0,0,1,0,2,3') ;
	}
	
} 






if ($action=="generate_list_view")
{
	//echo $data;
	
	$data=explode( "_", $data );
	
	//$data[0]
	?> 
 <form id="list_view" >
				
      <div style="border:1px solid #666; border-radius:5px; width:692px; margin-left:26px; float:left;  background-color:#D9EEF9; font-size:12px; font-family:Arial, Helvetica, sans-serif;" ><!--main div start-->
             
                <div style="width:688px;border:1px solid #D9EEF9; border-radius:1px;" ><!--first div start-->
                    <table width="692" border="1" cellpadding="1" cellspacing="1">
                     	<thead class="form_table_header">
                          <tr>                      
                             <th  width="95" height="37" rowspan="2" >Month</th>
                             <th  width="165" height="21" colspan="2">Current Year </th>                             
                             <th  width="220" colspan="2"><p>Previous Year's Target </p></th>
                          </tr> 
                           <tr>
                                <th  width="94" height="40">Qnty Target </th>
                                <th width="109">Value Target</th>  
                                <th width="152">Qnty. Target </th>                              
                                <th width="155">Value Target</th>
                           </tr>                                                 	  
                       </thead> 
                  </table>
              </div>
                    
                 <div style="width:600px;"><!--Second div start-->
                     <table width="600" border="1"  cellpadding="1" cellspacing="4"> 
                      
                  
                   <?
					 $k=1;
					 
						for ($i=0; $i<12; $i++)
						{
							if ($k<13)
							{
								$month=$data[1]+$i;
								$yy=substr($data[0],2,2);
								echo 
										'<tr>                       	 
										 <td width="100"><input type="text" name="'.$months[$month].'_'.$yy.'" id="'.$months[$month].'_'.$yy.'" readonly="readonly" placeholder="'.$months[$month].'-'.$yy.'" style="width:100px;border:1px solid #666; border-radius:5px;background-color:#B0C9AD;height:17px;"/></td>
										 <td width="90"><input type="text"  name="'.$months[$month].'_'.$yy.'" onblur="sum_qnty()" id="qnty_'.$i.'" style="width:99px; border:1px solid #666; border-radius:5px;height:17px; "   /></td>
										 <td width="87"><input type="text" name="'.$months[$month].'_'.$yy.'" onblur="sum_val()" id="val_'.$i.'" style="width:99px;border:1px solid #666; border-radius:5px;height:17px;" /></td>
										 
									 </tr>';
									 							
								if ($month==12)
									{ $data[1]=0; $i=0; $data[0]=$data[0]+1; }
								
							}
							$k++;
						}										
					 	echo '<tr>                       	 
										 <td width="100">Total</td>
										 <td width="90"><input type="text" style="width:100px;border:1px solid #666; border-radius:5px;background-color:#B0C9AD;height:17px;" id="total_qnty" ></td>
										 <td width="87"><input type="text" style="width:100px;border:1px solid #666; border-radius:5px;background-color:#B0C9AD;height:17px;" id="total_val" > </td>
										 <td width="116"><input type="text" style="width:100px;border:1px solid #666; border-radius:5px;background-color:#B0C9AD;height:17px;" id="total_qnty_02"  ></td>
										 <td width=""><input type="text" style="width:100px;border:1px solid #666; border-radius:5px;background-color:#B0C9AD;height:17px;" id="total_val_02"  ></td>
									 </tr>';
					 ?>	
                      
                        
                        
                                                                   
                </table>
			</div>
            
     </div> <!--main div close-->                        
  </form> 
 
<?
}
?>


 