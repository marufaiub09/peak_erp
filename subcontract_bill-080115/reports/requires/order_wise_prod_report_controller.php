<? 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------------------------------
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_id", 130, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );   	 
	exit();
}

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_id", 120, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select Location--", $selected, "load_drop_down( 'requires/order_wise_prod_report_controller', this.value, 'load_drop_down_floor', 'floor_td' );",0 );
	exit();     	 
}

if ($action=="load_drop_down_floor")
{
	echo create_drop_down( "cbo_floor_id", 120, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 and location_id='$data' order by floor_name","id,floor_name", 1, "-- Select Floor--", $selected, "",0 );  
	exit();   	 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$company_short_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name"  );
 	$buyer_short_library=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
	$line_library=return_library_array( "select id,line_name from lib_sewing_line", "id", "line_name"  ); 
 	$country_library=return_library_array( "select id,country_name from lib_country", "id", "country_name"  );


	//$garments_nature=str_replace("'","",$cbo_garments_nature);
	//if($garments_nature==1)$garments_nature="";
	$type = str_replace("'","",$cbo_type);
	if(str_replace("'","",$cbo_company_id)==0)$company_name=""; else $company_name=" and a.company_id=$cbo_company_id";
	if(str_replace("'","",$cbo_buyer_id)==0)$buyer_name="";else $buyer_name=" and a.party_id=$cbo_buyer_id";
	
	if(str_replace("'","",$cbo_location_id)==0)$location="";else $location=" and a.location_id 	=$cbo_location_id";
	if(str_replace("'","",$cbo_floor_id)==0)$floor="";else $floor=" and b.floor_id=$cbo_floor_id";
	
	if(str_replace("'","",trim($txt_date_from))=="" || str_replace("'","",trim($txt_date_to))=="")$txt_date="";
	else $txt_date=" and b.production_date between $txt_date_from and $txt_date_to";
	
	$fromDate = change_date_format( str_replace("'","",trim($txt_date_from)) );
	$toDate = change_date_format( str_replace("'","",trim($txt_date_to)) );
		
		
	if($type==2 || $type==4) //------------------------------------Show Date Location Floor & Line Wise $type==2
	{
		$groupByCond = "group by a.id,c.location,c.floor_id order by a.id,c.location,c.floor_id";
	}
	else //--------------------------------------------Show Order Wise  $type==1
	{
		$groupByCond = "group by a.id order by a.pub_shipment_date,a.id";
	}
		ob_start();
	?>
    <div style="width:1850px"> 
        <table width="1000"  cellspacing="0" >
            <tr class="form_caption" style="border:none;">
                <td align="center" style="border:none;font-size:16px; font-weight:bold" >
                    <? 
                        if($type==1) echo "Order Wise Production Report";
                        else if($type==2) echo "Order Location & Floor Wise Production Report";
                        else if($type==3) echo "Order Country Wise Production Report";
                        else echo "Order Country Location & Floor Wise Production Report";
                    ?>    
                </td>
            </tr>
            <tr style="border:none;">
                <td align="center" style="border:none; font-size:14px;">
                    Company Name : <? echo $company_library[str_replace("'","",$cbo_company_id)]; ?>                                
                </td>
            </tr>
            <tr style="border:none;">
                <td align="center" style="border:none;font-size:12px; font-weight:bold">
                <? echo "From $fromDate To $toDate" ;?>
                </td>
            </tr>
        </table>
        <div style="float:left; width:750px">
            <table width="730" cellspacing="0" border="1" class="rpt_table" rules="all" id="" >
                <thead>
                    <tr>
                        <th width="40">Sl.</th>    
                        <th width="110">Buyer Name</th>
                        <th width="80">Order Qty.(Pcs)</th>
                        <th width="80">PO Value</th>
                        <th width="80">Total Cut Qty</th>
                        <th width="80">Total Sew Qty</th>
                        <th width="80">Ex-Fac</th>
                        <th width="">Ex-Fac%</th>
                    </tr>
                </thead>
            </table>
            <div style="max-height:425px; width:730px" >
                <table cellspacing="0" border="1" class="rpt_table"  width="730" rules="all" id="" >
					<?  $total_po_quantity=0;$total_po_value=0;$total_cut=0;$total_sew_out=0;$total_ex_factory=0;
						$i=1;
						$exfactory_sql="select a.party_id, sum(c.order_quantity) as order_quantity 
									from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
									where a.subcon_job=c.job_no_mst and c.id=b.order_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 $company_name $buyer_name $location $floor $txt_date group by a.party_id";
						//echo  $exfactory_sql;
						$exfactory_sql_result=sql_select($exfactory_sql);
						$exfactory_arr=array(); 
						foreach($exfactory_sql_result as $resRow)
						{
							$exfactory_arr[$resRow[csf("party_id")]] = $resRow[csf("order_quantity")];
						}
						//var_dump($exfactory_arr);die;
						$pro_date_sql="SELECT a.party_id, sum(c.order_quantity) as po_quantity, sum(c.amount) as po_total_price         
									from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c, lib_buyer d 
									where c.job_no_mst=a.subcon_job and a.party_id=d.id and a.status_active=1 and b.status_active=1 $company_name $buyer_name $location $floor $txt_date group by a.party_id order by a.party_id ASC";
						//echo $pro_date_sql;//die;
						$pro_date_sql_result=sql_select($pro_date_sql);	
						foreach($pro_date_sql_result as $pro_date_sql_row)
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							$production_mst_sql= "SELECT
										sum(CASE WHEN production_type ='1' THEN production_qnty ELSE 0 END) AS cutting_qnty,
										sum(CASE WHEN production_type ='2' THEN production_qnty ELSE 0 END) AS sewingout_qnty 
									from subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c
									where c.job_no_mst=a.subcon_job and c.id=b.order_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1  and a.party_id=".$pro_date_sql_row[csf("party_id")]." $company_name $location $floor $txt_date ";
                            //echo $production_mst_sql;
							$production_mst_sql_result=sql_select($production_mst_sql);
                            foreach($production_mst_sql_result as $row)
                            {
								?>
                                <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_1nd<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_1nd<? echo $i; ?>">
                                    <td width="40"><? echo $i;?></td>
                                    <td width="110"><? echo $buyer_short_library[$pro_date_sql_row[csf("party_id")]]; ?></td>
                                    <td width="80" align="right"><? echo number_format($pro_date_sql_row[csf("po_quantity")]);?>&nbsp;</td>
                                    <td width="80" align="right"><? echo number_format($pro_date_sql_row[csf("po_total_price")],2);?>&nbsp;</td>
                                    <td width="80" align="right"><? echo number_format($row[csf("cutting_qnty")]); ?>&nbsp;</td>
                                    <td width="80" align="right"><? echo number_format($row[csf("sewingout_qnty")]); ?>&nbsp;</td>
                                    <td width="80" align="right"><? echo number_format($exfactory_arr[$pro_date_sql_row[csf("party_id")]]); ?>&nbsp;</td>
                                    <? $ex_gd_status = ($exfactory_arr[$pro_date_sql_row[csf("party_id")]]/$pro_date_sql_row[csf("po_quantity")])*100; ?>
                                    <td width="" align="right"><? echo  number_format($ex_gd_status,2)." %"; ?>&nbsp;</td>
                                </tr>	
                                <?		
                                    $total_po_quantity+=$pro_date_sql_row[csf("po_quantity")];
                                    $total_po_value+=$pro_date_sql_row[csf("po_total_price")];
                                    $total_cut+=$row[csf("cutting_qnty")];
                                    $total_sew_out+=$row[csf("sewingout_qnty")];
                                    $total_ex_factory+=$exfactory_arr[$pro_date_sql_row[csf("party_id")]];
						   } //end foreach 2nd
                            $i++;
                        }//end foreach 1st
						//$chart_data_qnty="Order Qty;".$total_po_quantity."\n"."Cutting;".$total_cut."\n"."Sew Out ;".$total_sew_out."\n"."Ex-Fact;".$total_ex_factory."\n";
                        ?>
                    </table>
                    <table border="1" class="tbl_bottom"  width="730" rules="all" id="" >
                        <tr> 
                            <td width="40">&nbsp;</td> 
                            <td width="110" align="right">Total</td> 
                            <td width="80" id="tot_po_quantity"><? echo number_format($total_po_quantity); ?>&nbsp;</td> 
                            <td width="80" id="tot_po_value"><? echo number_format($total_po_value); ?>&nbsp;</td> 
                            <td width="80" id="tot_cutting"><? echo number_format($total_cut); ?>&nbsp;</td>
                            <td width="80" id="tot_sew_out"><? echo number_format($total_sew_out); ?>&nbsp;</td>   
                            <td width="80"><? echo number_format($total_ex_factory); ?>&nbsp;</td >
                            <? $total_ex_status = ($total_ex_factory/$total_po_quantity)*100; ?>
                            <td width=""><? echo number_format($total_ex_status,2); ?>&nbsp;</td>
                        </tr>
                    </table>
                 </div>
                 </div>
                <div style="clear:both"></div>
                <br />
                <div>
                    <table width="1500" cellspacing="0" border="1" class="rpt_table" rules="all" id="table_header_1">
                        <thead>
                            <tr>
                                <th width="40">SL</th>    
                                <th width="100">Order Number</th>
                                <th width="100">Buyer Name</th>
                                <th width="100">Job Number</th>
                                <th width="100">Style Name</th>
                                <th width="150">Item Name</th>
                                <th width="80">Order Qty.</th>
                                <th width="80">Delivery Date</th>
                                <th width="80">Ex-Factory Date</th>
                                <th width="80">Delay</th>
                                <th width="80">Total Cut Qty</th>
                                <th width="80">Actual Exc. Cut %</th>
                                <th width="80">Total Sew Qty</th>
                                <th width="80">Total Out</th>
                                <th width="80">Shortage/ Excess</th>
                                <th width="80">Status</th>
                                <th width="">Remarks</th>
                            </tr>
                        </thead>
                    </table>
                    <div style="max-height:425px; width:1520px" id="scroll_body">
                        <table border="1" class="rpt_table"  width="1500" rules="all" id="table_body" >
							  <? 
                                //sql_select
                               // if($type==1) //--------------------------------------------Show Order Wise  $type==1 
                               // {
								$item_id_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');
								if ($db_type==0)
								{
									$order_sql= "select c.id, a.job_no_prefix_num, c.order_no, c.order_quantity, c.job_no_mst, 
	 b.production_date as production_date, a.party_id, a.location_id,c.cust_style_ref, b.gmts_item_id, 
										sum(CASE WHEN d.order_id=c.id THEN d.delivery_qty ELSE 0 END) AS ex_factory_qnty
									from 
										subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c left join subcon_delivery_dtls d on c.id=d.order_id 
									where 
										c.id=b.order_id and b.status_active=1 and a.subcon_job=c.job_no_mst and a.status_active=1 and b.status_active=1 $company_name $buyer_name $location $floor $txt_date group by b.production_date, a.party_id, c.id order by b.production_date";
								}
								elseif($db_type==2)
								{
									$order_sql= " select log_concat(c.id) as id, log_concat(a.job_no_prefix_num) as job_no_prefix_num, log_concat(c.order_no) as order_no, log_concat(c.order_quantity) as order_quantity, log_concat(c.job_no_mst) as job_no_mst, 
	 b.production_date as production_date, a.party_id, log_concat(a.location_id) as location_id,log_concat(c.cust_style_ref) as cust_style_ref, log_concat(b.gmts_item_id) as gmts_item_id, 
										sum(CASE WHEN d.order_id=c.id THEN d.delivery_qty ELSE 0 END) AS ex_factory_qnty
									from 
										subcon_ord_mst a, subcon_gmts_prod_dtls b, subcon_ord_dtls c left join subcon_delivery_dtls d on c.id=d.order_id  
									where 
										c.id=b.order_id and b.status_active=1 and a.subcon_job=c.job_no_mst and a.status_active=1 and b.status_active=1 $company_name $buyer_name $location $floor $txt_date group by b.production_date, a.party_id, c.id order by b.production_date";
								}
							  //echo $order_sql;//die; 
								$order_sql_result=sql_select($order_sql);
								   $i=0; $k=0;
								   foreach($order_sql_result as $orderRes)
								   {
									   $i++;
									   if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
									   
										?>
										<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_2nd<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_2nd<? echo $i; ?>" style="height:20px">
											<td width="40" ><? echo $i; ?></td>    
											<td width="100"><p><? echo $orderRes[csf("order_no")]; ?></p></td>
											<td width="100"><? echo $buyer_short_library[$orderRes[csf("party_id")]]; ?></td>
											<td width="100" align="center"><p><? echo $orderRes[csf("job_no_prefix_num")]; ?></p></td>
											<td width="100"><p><? echo $orderRes[csf("cust_style_ref")]; ?></p></td>
											<td width="150"><p><? echo $garments_item[$orderRes[csf("gmts_item_id")]];?></p></td>
											<td width="80" align="right"><? echo $orderRes[csf("order_quantity")]; $total_ord_quantity+=$orderRes[csf("order_quantity")]; ?>&nbsp;</td>
											<?
											   $sqlEx = sql_select("select MAX(a.delivery_date) AS ex_fac_date,sum(b.delivery_qty) AS ex_fac_qnty from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.order_id=".$orderRes[csf("id")]." ");
											   $orderRes[ex_fac_date]=$sqlEx[0][csf('ex_fac_date')];
											   $orderRes[ex_fac_qnty]=$sqlEx[0][csf('ex_fac_qnty')];
												$ex_factory_date = $orderRes[csf("ex_fac_date")];
													$date=date("Y-m-d");$color="";$days_remian="";
														$days_remian=datediff("d",$date,$orderRes[csf("production_date")]);
														$days_remians=datediff("d",$ex_factory_date,$orderRes[csf("production_date")]);
														if($orderRes[csf("production_date")] > $date) 
														{
															$color="green";
														}
														else if($orderRes[csf("production_date")] >= $ex_factory_date) 
														{ 
															$color="green";
														}
														else if($orderRes[csf("production_date")] < $date || $ex_factory_date=="") 
														{
															$color="red";
														}														
														else if($orderRes[csf("production_date")] >= $date && $days_remian<=5 ) 
														{
															$color="orange";
														}
														else if($orderRes[csf("production_date")] < $ex_factory_date) 
														{ 
															$color="#2A9FFF";
														}
													 
/*													else if($orderRes[csf("bill_status")]==0)
													{
														$days_remian=datediff("d",$ex_factory_date,$orderRes[csf("shipment_date")]);
														if($orderRes[csf("shipment_date")] >= $ex_factory_date) 
														{ 
															$color="green";
														}
														else if($orderRes[csf("shipment_date")] < $ex_factory_date) 
														{ 
															$color="#2A9FFF";
														}
														
													}//end if condition
*/												?>
												<td width="80" bgcolor="<? echo $color; ?>"><? echo change_date_format($orderRes[csf("production_date")]);  ?></td>
                                                <td width="80"><? if(!($ex_factory_date=="" || $ex_factory_date=="0000-00-00")) echo change_date_format($ex_factory_date); ?></td>
                                                <td width="80" align="center" title="<? echo $days_remian; ?>"><? echo $days_remian;  ?></td>
                                                <?
												$prod_sql= sql_select("SELECT b.location_id, b.floor_id,	 
													IFNULL(sum(CASE WHEN b.production_type ='1' THEN  b.production_qnty  ELSE 0 END),0) AS cutting_qnty,
													IFNULL(sum(CASE WHEN b.production_type ='2' THEN  b.production_qnty  ELSE 0 END),0) AS sewingout_qnty
												from 
													subcon_gmts_prod_dtls b
												where  
													b.order_id =".$orderRes[csf("id")]." and b.status_active=1 $floor $txt_date group by b.order_id");
												//echo $prod_sql."-------------------------";die;
												foreach($prod_sql as $proRes);
												$actual_exces_cut = $proRes[csf("cutting_qnty")];
												if($actual_exces_cut < $orderRes[csf("order_quantity")]) $actual_exces_cut=""; else $actual_exces_cut=number_format( (($actual_exces_cut-$orderRes[csf("order_quantity")])/$orderRes[csf("order_quantity")])*100,2)."%";
												?>
												 
												<td width="80" align="right"><? echo $proRes[csf("cutting_qnty")]; $total_cutt+=$proRes[csf("cutting_qnty")]; ?>&nbsp;</td>
												<td width="80" align="right" <? if(round($actual_exces_cut) > round($orderRes[csf("excess_cut")])) echo "bgcolor='#FF0000'"; ?>><? echo $actual_exces_cut; ?></td>
												<td width="80" align="right"><? echo $proRes[csf("sewingout_qnty")]; $total_sew+=$proRes[csf("sewingout_qnty")]; ?>&nbsp;</td>
												<td width="80" align="right"><? echo $orderRes[csf("ex_factory_qnty")]; $total_out_out+=$orderRes[csf("ex_factory_qnty")]; ?>&nbsp;</td>
												<? $shortage = $orderRes[csf("order_quantity")]-$orderRes[csf("ex_factory_qnty")]; ?>
												<td width="80" align="right"><? echo $shortage; $total_shortage+=$shortage; ?>&nbsp;</td>
												<td width="80"><? echo $shipment_status[$orderRes[csf("shiping_status")]]; ?></td>
												<td width="">&nbsp;</td>
                                             </tr>
										<?
								   }
								  ?>  
                                </table>	
                                <table border="1" class="tbl_bottom"  width="1500" rules="all" id="report_table_footer_1" >
                                    <tr>
                                        <td width="40"></td>
                                        <td width="100"></td>
                                        <td width="100"></td>
                                        <td width="100"></td>
                                        <td width="100"></td>
                                        <td width="150">Total</td>
                                        <td width="80" id="total_ord_quantity"><? echo $total_ord_quantity; ?>&nbsp;</td>
                                        <td width="80"></td>
                                        <td width="80"></td>
                                        <td width="80"></td>
                                        <td width="80" id="total_cutt"><? echo $total_cutt; ?>&nbsp;</td>
                                        <td width="80"></td>
                                        <td width="80" id="total_sew"><? echo $total_sew; ?>&nbsp;</td>
                                        <td width="80" id="total_out_out"><? echo $total_out_out; ?>&nbsp;</td>
                                        <td width="80" id="total_shortage"><? echo $total_shortage; ?>&nbsp;</td>
                                        <td width="80"></td>
                                        <td width=""></td>
                                     </tr>
                             </table>
                            </div>
			  </div>
        <br /><br />		
        </div><!-- end main div -->
		<?
	$html = ob_get_contents();
	ob_clean();
	$new_link=create_delete_report_file( $html, 1, 1, "../../../" );
	
	echo "$html";
	exit();	
}
?>