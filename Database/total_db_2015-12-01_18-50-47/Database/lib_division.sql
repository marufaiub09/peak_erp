-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_division`
--

DROP TABLE IF EXISTS `lib_division`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_division` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `division_name` varchar(64) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `contact_person` varchar(64) NOT NULL,
  `contact_no` varchar(64) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `website` varchar(64) NOT NULL,
  `email` varchar(32) NOT NULL,
  `short_name` varchar(5) NOT NULL,
  `address` varchar(500) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_division`
--

LOCK TABLES `lib_division` WRITE;
/*!40000 ALTER TABLE `lib_division` DISABLE KEYS */;
INSERT INTO `lib_division` VALUES (1,'Admin',1,'','',0,'','','','','',2,'2015-02-03 05:01:40',0,'0000-00-00 00:00:00',1,0,0),(2,'Merchandising',1,'','',0,'','','','','',2,'2015-02-03 05:02:22',0,'0000-00-00 00:00:00',1,0,0),(3,'Cutting',1,'','',0,'','','','','',2,'2015-02-03 05:02:37',0,'0000-00-00 00:00:00',1,0,0),(4,'Sewing',1,'','',0,'','','','','',2,'2015-02-03 05:02:47',0,'0000-00-00 00:00:00',1,0,0),(5,'Finishing',1,'','',0,'','','','','',2,'2015-02-03 05:02:56',0,'0000-00-00 00:00:00',1,0,0),(6,'Knitting',1,'','',0,'','','','','',2,'2015-02-03 05:03:04',0,'0000-00-00 00:00:00',1,0,0),(7,'Accounts',1,'','',0,'','','','','',2,'2015-02-03 05:11:33',0,'0000-00-00 00:00:00',1,0,0),(8,'IT',1,'','',0,'','','','','',2,'2015-02-03 05:11:42',0,'0000-00-00 00:00:00',1,0,0),(9,'Dying',1,'','',0,'','','','','',2,'2015-02-03 05:11:53',0,'0000-00-00 00:00:00',1,0,0),(10,'Sample',1,'','',0,'','','','','',2,'2015-02-04 21:24:29',0,'0000-00-00 00:00:00',1,0,0),(11,'Store',1,'','',0,'','','','','',2,'2015-02-04 21:26:19',0,'0000-00-00 00:00:00',1,0,0),(12,'Maintanence',1,'','',0,'','','','','',2,'2015-08-29 09:44:22',0,'0000-00-00 00:00:00',1,0,0);
/*!40000 ALTER TABLE `lib_division` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
