<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Knit Garments Order Entry
				
Functionality	:	
JS Functions	:
Created by		:	Ashraful
Creation date 	: 	23-04-2014
Purpose			: 	Country Wise Color and Size Cutting Entry  /Bundle Creation
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Cutting Delivery Info","../", 1, 1, $unicode,'','');

?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
 
function openmypage(page_link,title)
{
	var company = $("#cbo_company_name").val();
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1040px,height=370px,center=1,resize=0,scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var po_id=this.contentDoc.getElementById("hidden_mst_id").value;//po id
		
		var item_id=this.contentDoc.getElementById("hidden_grmtItem_id").value;
		var po_qnty=this.contentDoc.getElementById("hidden_po_qnty").value;
		var plan_qnty=this.contentDoc.getElementById("hidden_plancut_qnty").value; 
		var country_id=this.contentDoc.getElementById("hidden_country_id").value; 
		var job_num=this.contentDoc.getElementById("hid_job_num").value; 
		
		if (po_id!="")
		{
			freeze_window(5);
			$("#cbo_item_name").val(item_id);
			$("#txt_order_qty").val(number_format(po_qnty,'','',','));
			$("#txt_plancut_qty").val(number_format(plan_qnty,'','',','));
			$("#cbo_country_name").val(country_id);
			$("#hid_job_num").val(job_num);
			
			childFormReset();//child from reset
			get_php_form_data(po_id+'**'+item_id+'**'+country_id, "populate_data_from_search_popup", "requires/cutting_delevar_to_input_controller" );
			var variableSettings=$('#sewing_production_variable').val();
			var styleOrOrderWisw=$('#styleOrOrderWisw').val();
			
			if(variableSettings!=1)
			{ 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id+'**'+job_num, "color_and_size_level", "requires/cutting_delevar_to_input_controller" ); 
			}
			else
			{
				$("#txt_cutting_qty").removeAttr("readonly");
			}
			
			show_list_view(po_id+'**'+item_id+'**'+country_id,'show_dtls_listview','cutting_production_list_view','requires/cutting_delevar_to_input_controller','');	
			show_list_view(po_id,'show_country_listview','list_view_country','requires/cutting_delevar_to_input_controller','');		
 			set_button_status(0, permission, 'fnc_cutting_update_entry',1);
 			release_freezing();
		}
	}
}

 
function fnc_cutting_update_entry(operation)
{
	  if(operation==4)
		{
			 var report_title=$( "div.form_caption" ).html();
			
			 print_report( $('#cbo_company_name').val()+'*'+$('#txt_mst_id').val()+'*'+report_title, "cutting_delevery_print", "requires/cutting_delevar_to_input_controller" ) ;
			 return;
		}
	if(operation==2)
	{
		show_msg('13');
		return;
	}
		
	if ( form_validation('cbo_company_name*txt_order_no*cbo_cutting_company*txt_cutting_date*txt_cutting_qty','Company Name*Order No*Cutting Comapny*Cutting Date*Cutting Quantity')==false )
	{
		return;
	}		
	else
		{
			var sewing_production_variable = $("#sewing_production_variable").val();
			var colorList = ($('#hidden_colorSizeID').val()).split(",");
			var i=0;var colorIDvalue='';
			if(sewing_production_variable==2)//color level
			{
				$("input[name=txt_color]").each(function(index, element) {
 					if( $(this).val()!='' )
					{
						if(i==0)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val();
						}
						else
						{
							colorIDvalue += "**"+colorList[i]+"*"+$(this).val();
						}
					}
					i++;
				});
			}
			else if(sewing_production_variable==3)//color and size level
			{
				$("input[name=colorSize]").each(function(index, element) {
					if( $(this).val()!='' )
					{
						if(i==0)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val();
						}
						else
						{
							colorIDvalue += "***"+colorList[i]+"*"+$(this).val();
						}
					}
 					i++;
				});
			}
			
			var data="action=save_update_delete&operation="+operation+"&colorIDvalue="+colorIDvalue+get_submitted_data_string('garments_nature*cbo_company_name*cbo_country_name*sewing_production_variable*hidden_po_break_down_id*hidden_colorSizeID*cbo_buyer_name*txt_style_no*cbo_item_name*txt_order_qty*txt_plancut_qty*cbo_source*cbo_cutting_company*cbo_location*cbo_floor*txt_cutting_date*txt_reporting_hour*txt_cutting_qty*txt_challan_no*txt_remark*txt_cumul_cutting*txt_yet_cut*hidden_break_down_html*txt_mst_id*hid_job_num*txt_cut_no*txt_batch_no',"../");
 			//freeze_window(operation);
 			http.open("POST","requires/cutting_delevar_to_input_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = cutting_update_entry_Reply_info;
		}
}
  
function cutting_update_entry_Reply_info()
{
 	if(http.readyState == 4) 
	{
		
		var variableSettings=$('#sewing_production_variable').val();
		var styleOrOrderWisw=$('#styleOrOrderWisw').val();
		var item_id = $("#cbo_item_name").val();
		var country_id = $("#cbo_country_name").val();
		var reponse=trim(http.responseText).split('**');	
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_cutting_update_entry('+ reponse[1]+')',8000); 
		}	 
		if(reponse[0]==0)//insert
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id,'show_dtls_listview','cutting_production_list_view','requires/cutting_delevar_to_input_controller','');		
			reset_form('','','txt_cutting_date*txt_reporting_hour*txt_cutting_qty*hidden_break_down_html*hidden_colorSizeID*txt_remark*txt_mst_id','','');
 			get_php_form_data(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id, "populate_data_from_search_popup", "requires/cutting_delevar_to_input_controller" );
			
			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/cutting_delevar_to_input_controller" ); 
			}
			else
			{
				$("#txt_cutting_qty").removeAttr("readonly");
			}
			
			$('#txt_cutting_qty').attr('placeholder','');
			release_freezing();
		}
		if(reponse[0]==1)//update
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id,'show_dtls_listview','cutting_production_list_view','requires/cutting_delevar_to_input_controller','');		
			reset_form('','','txt_cutting_date*txt_reporting_hour*txt_challan_no*txt_cutting_qty*hidden_break_down_html*hidden_colorSizeID*txt_remark*txt_mst_id','','');
			get_php_form_data(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id, "populate_data_from_search_popup", "requires/cutting_delevar_to_input_controller" );
			
			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/cutting_delevar_to_input_controller" ); 
			}
			else
			{
				$("#txt_cutting_qty").removeAttr("readonly");
			}

			$('#txt_cutting_qty').attr('placeholder','');
			set_button_status(0, permission, 'fnc_cutting_update_entry',1);
			release_freezing();
		}
		if(reponse[0]==2)//delete
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id,'show_dtls_listview','cutting_production_list_view','requires/cutting_delevar_to_input_controller','');		
			reset_form('','','txt_cutting_date*txt_reporting_hour*txt_cutting_qty*hidden_break_down_html*hidden_colorSizeID*txt_remark*txt_mst_id','','');
			get_php_form_data(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id, "populate_data_from_search_popup", "requires/cutting_delevar_to_input_controller" );
			
			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/cutting_delevar_to_input_controller" ); 
			}
			else
			{
				$("#txt_cutting_qty").removeAttr("readonly");
			}

 			set_button_status(0, permission, 'fnc_cutting_update_entry',1);
			release_freezing();
		}
 	}
} 

 
function childFormReset()
{
	reset_form('','cutting_production_list_view','txt_cutting_date*txt_reporting_hour*txt_challan_no*txt_cutting_qty*hidden_break_down_html*hidden_colorSizeID*txt_remark*txt_cumul_cutting*txt_yet_cut*txt_mst_id','','');
	$('#txt_cutting_qty').attr('placeholder','');//placeholder value initilize
	$('#txt_cumul_cutting').attr('placeholder','');//placeholder value initilize
	$('#txt_yet_cut').attr('placeholder','');//placeholder value initilize
	$("#cutting_production_list_view").html('');
	$("#breakdown_td_id").html('');
}


function fn_hour_check(val)
{	
  	
	var hours = $("#txt_reporting_hour").val();
	var hoursArr = hours.split(".");
  	if( hoursArr[1] ) {
 		$("#txt_reporting_hour").val(hoursArr[0]);
		return;
	}
	
	if(val*1>12)
	{
		alert("You Cross 12!!This is 12 Hours.");
		$("#txt_reporting_hour").val('');
	}
		
}
 
 
function fn_total(tableName,index) // for color and size level
{
    var filed_value = $("#colSize_"+tableName+index).val();
	var placeholder_value = $("#colSize_"+tableName+index).attr('placeholder');
	if(filed_value*1 > placeholder_value*1)
	{
		if( confirm("Qnty Excceded by"+(placeholder_value-filed_value)) )	
			void(0);
		else
		{
			$("#colSize_"+tableName+index).val('');
 		}
	}
	
	var totalRow = $("#table_"+tableName+" tr").length;
	//alert(tableName);
	math_operation( "total_"+tableName, "colSize_"+tableName, "+", totalRow);
	if($("#total_"+tableName).val()*1!=0)
	{
		$("#total_"+tableName).html($("#total_"+tableName).val());
	}
	var totalVal = 0;
	$("input[name=colorSize]").each(function(index, element) {
        totalVal += ( $(this).val() )*1;
    });
	$("#txt_cutting_qty").val(totalVal);
}

function fn_colorlevel_total(index) //for color level
{
	
	var filed_value = $("#colSize_"+index).val();
	var placeholder_value = $("#colSize_"+index).attr('placeholder');
	if(filed_value*1 > placeholder_value*1)
	{
		if( confirm("Qnty Excceded by"+(placeholder_value-filed_value)) )	
			void(0);
		else
		{
			$("#colSize_"+index).val('');
 		}
	}
	
    var totalRow = $("#table_color tbody tr").length;
	//alert(totalRow);
	math_operation( "total_color", "colSize_", "+", totalRow);
	$("#txt_cutting_qty").val( $("#total_color").val() );
}

function put_country_data(po_id, item_id, country_id, po_qnty, plan_qnty)
{
	freeze_window(5);
	
	childFormReset();//child from reset
	$("#cbo_item_name").val(item_id);
	$("#txt_order_qty").val(po_qnty);
	$("#txt_plancut_qty").val(plan_qnty);
	$("#cbo_country_name").val(country_id);
	
	get_php_form_data(po_id+'**'+item_id+'**'+country_id, "populate_data_from_search_popup", "requires/cutting_delevar_to_input_controller" );
	var variableSettings=$('#sewing_production_variable').val();
	var styleOrOrderWisw=$('#styleOrOrderWisw').val();
	var job_num=$("#hid_job_num").val();
	
	if(variableSettings!=1)
	{ 
		get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id+'**'+job_num, "color_and_size_level","requires/cutting_delevar_to_input_controller"); 
	}
	else
	{
		$("#txt_cutting_qty").removeAttr("readonly");
	}
	
	show_list_view(po_id+'**'+item_id+'**'+country_id,'show_dtls_listview','cutting_production_list_view','requires/cutting_delevar_to_input_controller','');	
	set_button_status(0, permission, 'fnc_cutting_update_entry',1);
	release_freezing();
}

 function fnc_valid_time(val,field_id)
{
	var val_length=val.length;
	if(val_length==2)
	{
		document.getElementById(field_id).value=val+":";
	}
	
	var colon_contains=val.contains(":");
	if(colon_contains==false)
	{
		if(val>23)
		{
			document.getElementById(field_id).value='23:';
		}
	}
	else
	{
		var data=val.split(":");
		var minutes=data[1];
		var str_length=minutes.length;
		var hour=data[0]*1;
		
		if(hour>23)
		{
			hour=23;
		}
		
		if(str_length>=2)
		{
			minutes= minutes.substr(0, 2);
			if(minutes*1>59)
			{
				minutes=59;
			}
		}
		
		var valid_time=hour+":"+minutes;
		document.getElementById(field_id).value=valid_time;
	}
}

function numOnly(myfield, e, field_id)
{
	var key;
	var keychar;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
	return true;
	// numbers
	else if ((("0123456789:").indexOf(keychar) > -1))
	{
		var dotposl=document.getElementById(field_id).value.lastIndexOf(":");
		if(keychar==":" && dotposl!=-1)
		{
			return false;
		}
		return true;
	}
	else
		return false;
}
 

</script>
</head>
<body onLoad="set_hotkey()">
    <div style="width:100%;">
        <div style="width:850px;" align="center">
             <?php echo load_freeze_divs ("../",$permission); ?>
        </div>
       <div style="width:930px; float:left" align="center">  
            <fieldset style="width:750px"> 
            <legend>Production Module</legend>
            	<form name="cuttingupdate_1" id="cuttingupdate_1" action=""  autocomplete="off">
                    <fieldset>
                        <table width="100%">
                             <tr> 
                                   <td width="130" onClick="show_report(1)" class="must_entry_caption">Company </td>
                                    <td width="180">
                                        <?php
                                            echo create_drop_down( "cbo_company_name", 180, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/cutting_delevar_to_input_controller', this.value, 'load_drop_down_location', 'location_td' );get_php_form_data(this.value,'load_variable_settings','requires/cutting_delevar_to_input_controller');" );
                                        ?> 
                                        <input type="hidden" id="sewing_production_variable" />
                                        <input type="hidden" id="styleOrOrderWisw" /> 
                                        
                                    </td>
                                    <td width="100"  class="must_entry_caption">Order No</td>
                                    <td width="150">
                                        <input name="txt_order_no" placeholder="Double Click to Search" onDblClick="openmypage('requires/cutting_delevar_to_input_controller.php?action=order_popup&company='+document.getElementById('cbo_company_name').value+'&garments_nature='+document.getElementById('garments_nature').value,'Order Search')" id="txt_order_no" class="text_boxes" style="width:160px " readonly />
                                        <input type="hidden" id="hidden_po_break_down_id" value="" />
                                         <input type="hidden" id="hid_job_num" value="" />
                                    </td>
                                    <td width="70" >Country</td>
                                    <td width="150">
                                        <?php
                                            echo create_drop_down( "cbo_country_name", 170, "select id,country_name from lib_country","id,country_name", 1, "-- Select Country --", $selected, "",1 );
                                        ?> 
                                    </td>
                              </tr>
                              <tr>    
                                    <td width="130">Buyer Name</td>
                                    <td width="180" id="buyer_td">
                                        <?php
                                            echo create_drop_down( "cbo_buyer_name", 180, "select id,buyer_name from lib_buyer where is_deleted=0 and status_active=1","id,buyer_name", 1, "-- Select Buyer --", $selected, "",1,0 );	
                                        ?> 
                                     </td>
                                     <td width="100">Style</td>
                                     <td width="150">
                                        <input type="text" name="txt_style_no" id="txt_style_no" class="text_boxes" style="width:160px" disabled readonly />
                                     </td>
                                     <td width="100"> Item </td>
                                     <td width="150">
                                          <?php
                                            echo create_drop_down( "cbo_item_name", 170, $garments_item,"", 1, "-- Select Item --", $selected, "",1,0 );	
                                          ?> 						
                                     </td>
                              </tr>
                              <tr>    
                                     <td width="130">Order Qnty</td>
                                     <td width="180">
                                        <input name="txt_order_qty" id="txt_order_qty" class="text_boxes_numeric"  style="width:170px" disabled readonly  />
                                     </td>
                                     <td width="100">Plan Cut Qnty</td>
                                     <td width="150">
                                        <input name="txt_plancut_qty" id="txt_plancut_qty" class="text_boxes_numeric"  style="width:160px" disabled readonly />
                                     </td>
                                     <td width="70">Source</td>
                                     <td width="150">
                                          <?php
                                            echo create_drop_down( "cbo_source", 170, $knitting_source,"", 1, "-- Select Source --", $selected, "load_drop_down( 'requires/cutting_delevar_to_input_controller', this.value+'**'+$('#cbo_company_name').val(), 'load_drop_down_cutt_company', 'cutt_company_td' );",0,'1,3' );	
                                         ?> 
                                     </td>
                              </tr>
                              <tr>
                                     <td width="130" ><span class="must_entry_caption">Delivery. Company</span></td>
                                     <td width="180" id="cutt_company_td">
                                         <?php
                                            echo create_drop_down( "cbo_cutting_company", 180, $blank_array,"", 1, "--- Select Cutting Company ---", $selected, "",0 );	
                                         ?> 
                                     </td>
                                     <td width="100">Location</td>
                                     <td width="150" id="location_td">
                                           <?php
                                         echo create_drop_down( "cbo_location", 175, $blank_array,"", 1, "-- Select Location --", $selected, "",0 );	
                                         ?> 
                                </td>
                                     <td width="70">Floor</td>
                                     <td width="150" id="floor_td">
                                        <?php
                                         echo create_drop_down( "cbo_floor", 170, $blank_array,"", 1, "-- Select Floor--", $selected, "",0 );	
                                         ?>
                                     </td>
                              </tr>
                            
                        </table>
                        </fieldset>
                        <table><tr><td colspan="6" height="5"></td></tr></table><!----------------------this is blank----------------->
                        <table cellpadding="0" cellspacing="1" width="100%">
                        	<tr>
                           		<td width="30%" valign="top">
                                    <fieldset>
                                    <legend>New Entry</legend>
                                        <table  cellpadding="0" cellspacing="2" width="100%">
                                            <tr>
                                                <td width="120" class="must_entry_caption">Delivery Date</td>
                                                <td>
                                                <input class="datepicker" type="text" style="width:110px;" name="txt_cutting_date" id="txt_cutting_date" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="" class="must_entry_caption">Delivery Quantity</td> 
                                                <td width=""> 
                                                <input type="text" name="txt_cutting_qty" id="txt_cutting_qty" class="text_boxes_numeric" style="width:110px" readonly />
                                                <input type="hidden" id="hidden_break_down_html"  value="" readonly disabled />
                                                <input type="hidden" id="hidden_colorSizeID"  value="" readonly disabled />
                                                 </td>
                                             </tr>
                                            <tr>
                                                <td width="">Reporting Hour</td> 
                                                <td width=""> <input name="txt_reporting_hour" id="txt_reporting_hour" class="text_boxes" style="width:100px" placeholder="24 Hour Format" onBlur="fnc_valid_time(this.value,'txt_reporting_hour');" onKeyUp="fnc_valid_time(this.value,'txt_reporting_hour');" onKeyPress="return numOnly(this,event,this.id);" maxlength="8" />
                                               
                                                 </td>
                                            </tr>
                                               <tr>
                                                 <td width="">Cutting No</td>
                                                 <td width=""><input type="text" name="txt_cut_no" id="txt_cut_no" class="text_boxes" style="width:110px" /></td>
                                            </tr>
                                            <tr>
                                                 <td width="">Batch No</td>
                                                 <td width=""><input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:110px" /></td>
                                            </tr>
                                            <tr>
                                                 <td width="">Challan No</td>
                                                 <td width=""><input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:110px" /></td>
                                            </tr>
                                            <tr>
                                                 <td width="">Remarks</td> 
                                                 <td colspan="4"> 
                                                 <input type="text" name="txt_remark" id="txt_remark" class="text_boxes" style="width:110px"  />
                                                 </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>
                                <td width="1%" valign="top"></td>
                                <td width="25%" valign="top">
                                      <fieldset>
                                          <legend>Display</legend>
                                              <table  cellpadding="0" cellspacing="2" width="100%" >
                                                <tr>
                                                    <td width="120">Cuml. Delivery.</td>
                                                    <td>
                                                    <input type="text" name="txt_cumul_cutting" id="txt_cumul_cutting" class="text_boxes_numeric" style="width:80px" disabled readonly />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="120">Yet to Delivery</td>
                                                    <td>
                                                    <input type="text" name="txt_yet_cut" id="txt_yet_cut" class="text_boxes_numeric" style="width:80px" disabled  readonly />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>	
                                </td>
                                <td width="1%" valign="top"></td>                            
                                <td width="40%" valign="top">
                                    <div style="max-height:300px; overflow-y:scroll" id="breakdown_td_id" align="center"></div>
                                </td>  	                          
                         </tr>
                         <tr>
                            <td align="center" colspan="9" valign="middle" class="button_container">
                                <?php
                                echo load_submit_buttons( $permission, "fnc_cutting_update_entry", 0,1,"reset_form('cuttingupdate_1','list_view_country','','','childFormReset()');$('#txt_cutting_qty').attr('placeholder','');",1); 
                                ?>
                                <input type="hidden" name="txt_mst_id" id="txt_mst_id" readonly >	
                            </td>
                            <td>&nbsp;</td>					
                        </tr>
                    </table>
                    <div style="width:910px; margin-top:5px;" id="cutting_production_list_view" align="center"></div>
              	</form>
            </fieldset>
        </div>
        <div id="list_view_country" style="width:400px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative; margin-left:10px"></div>
    </div>
</body> 
<script src="../includes/functions_bottom.js" type="text/javascript"></script> 
<script>

$("#cbo_location").val(0);

</script> 
</html>