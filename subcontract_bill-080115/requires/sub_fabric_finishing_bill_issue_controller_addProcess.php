﻿<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 150, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "--Select Location--", $selected, "","","","","","",3 );
	exit();	 
}

if ($action=="load_drop_down_party_name")
{
	$data=explode('_',$data);
	if($data[1]==2)
	{
		echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "show_list_view(document.getElementById('cbo_party_source').value+'_'+this.value,'dyingfinishing_delivery_list_view','dyeingfinishing_info_list','requires/sub_fabric_finishing_bill_issue_controller','');","","","","","",5 );
	}
	else
	{
		echo create_drop_down( "cbo_party_name", 150, $blank_array,"", 1, "-- Select Party --", $selected, "",0,"","","","",5);
	}
	exit();
}

if ($action=="load_drop_down_party_name_popup")
{ 
	$data=explode('_',$data);
	echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "","","","","","",5 );
	exit(); 
}

if ($action=="load_variable_settings")
{
	echo "$('#variable_check').val(0);\n";
	$sql_result = sql_select("select dyeing_fin_bill from  variable_settings_subcon where company_id='$data' and variable_list=1 order by id");
 	foreach($sql_result as $result)
	{
		echo "$('#variable_check').val(".$result[csf("dyeing_fin_bill")].");\n";
		if ($result[csf("dyeing_fin_bill")]==1)
		{
			echo "$('#bill_on').text('Bill On Grey Qty');\n"; 
		}
		else if ($result[csf("dyeing_fin_bill")]==2)
		{
			echo "$('#bill_on').text('Bill On Delivery Qty');\n"; 
		}
	}
 	exit();
}

if ($action=="bill_no_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	$exdata=explode('_',$data);
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('issue_id').value=id;
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="dyingfinishingbill_1"  id="dyingfinishingbill_1" autocomplete="off">
                <table width="630" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="150">Company Name</th>
                        <th width="150">Party Name</th>
                        <th width="80">Issue ID</th>
                        <th width="170">Date Range</th>
                        <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>           
                    </thead>
                    <tbody>
                        <tr>
                            <td> 
                                <input type="hidden" id="issue_id">  
                                <?   
                                    echo create_drop_down( "cbo_company_id", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $exdata[0],"load_drop_down( 'sub_fabric_finishing_bill_issue_controller', this.value, 'load_drop_down_party_name_popup', 'party_td' );",0 );
                                ?>
                            </td>
                            <td width="140" id="party_td">
                                <?
                                    echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $exdata[1], "",0,"","","","");
                                ?> 
                            </td>
                            <td>
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes_numeric" style="width:75px" />
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value, 'dyeingfinishing_bill_list_view', 'search_div', 'sub_fabric_finishing_bill_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" height="40" valign="middle">
                                <? echo load_month_buttons(1);  ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                    </tbody>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?
	exit();
}

if ($action=="dyeingfinishing_bill_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company_name=" and company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $party_name=" and party_id='$data[1]'"; else { echo "Please Select Party First."; die; }
	if ($data[2]!="" &&  $data[3]!="") $return_date = "and bill_date between '".change_date_format($data[2], "mm-dd-yyyy", "/",1)."' and '".change_date_format($data[3], "mm-dd-yyyy", "/",1)."'"; else $return_date="";
	if ($data[4]!='') $bill_id_cond=" and prefix_no_num='$data[4]'"; else $bill_id_cond="";
	
	$company_id=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$location=return_library_array( "select id,location_name from lib_location",'id','location_name');
	$party_arr=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
	
	$arr=array (2=>$location,4=>$party_arr,5=>$knitting_source,6=>$bill_for);
	
	if($db_type==0)
	{
		$year_cond= "year(insert_date)as year";
	}
	else if($db_type==2)
	{
		$year_cond= "TO_CHAR(insert_date,'YYYY') as year";
	}
	
	$sql= "select id, bill_no, prefix_no_num, $year_cond, location_id, bill_date, party_id, party_source, bill_for from subcon_inbound_bill_mst where process_id in (3,4) and status_active=1 $company_name $party_name $return_date $bill_id_cond order by id DESC";
	
	echo  create_list_view("list_view", "Bill No,Year,Location,Bill Date,Party,Source,Bill For", "50,40,80,70,100,120,140","630","250",0, $sql , "js_set_value", "id", "", 1, "0,0,location_id,0,party_id,party_source,bill_for", $arr , "prefix_no_num,year,location_id,bill_date,party_id,party_source,bill_for", "sub_fabric_finishing_bill_issue_controller","",'0,0,0,3,0,0,0');
	exit();
}

if ($action=="load_php_data_to_form_issue")
{
	$nameArray= sql_select("select id, bill_no, company_id, location_id, bill_date, party_id, party_source, bill_for from subcon_inbound_bill_mst where id='$data'");
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_bill_no').value 					= '".$row[csf("bill_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "load_drop_down( 'requires/sub_fabric_finishing_bill_issue_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_location', 'location_td' );\n";
		echo "document.getElementById('cbo_location_name').value			= '".$row[csf("location_id")]."';\n"; 
		echo "document.getElementById('txt_bill_date').value 				= '".change_date_format($row[csf("bill_date")])."';\n";   
		echo "document.getElementById('cbo_party_source').value				= '".$row[csf("party_source")]."';\n"; 
		echo "load_drop_down( 'requires/sub_fabric_finishing_bill_issue_controller', document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_party_source').value, 'load_drop_down_party_name', 'party_td' );\n";
		echo "document.getElementById('cbo_party_name').value				= '".$row[csf("party_id")]."';\n"; 
		echo "document.getElementById('cbo_bill_for').value					= '".$row[csf("bill_for")]."';\n"; 
	    echo "document.getElementById('update_id').value            		= '".$row[csf("id")]."';\n";
	}
	exit();
}

if ($action=="dyingfinishing_delivery_list_view")
{
	echo load_html_head_contents("Popup Info","../", 1, 1, $unicode,1,'');
	//echo $data;
	$data=explode('_',$data);
	if($data[0]==2)
	{
		?>
		<script>
		
		</script>
        </head>
        <body>
            <div style="width:100%;">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="817px" class="rpt_table">
                    <thead>
						<th width="30">SL</th>
						<th width="80">Challan No</th>
						<th width="80">Delivery Date</th>
						<th width="150">Order No</th>                    
						<th width="200">Fabric Description</th>
                        <th width="100">Grey Qty</th>
						<th width="100">Delivery Qty</th>
						<th width="100">Process</th>
						<th width="" >Currency</th>
                    </thead>
                 </table>
            </div>
            <div style="width:820px;max-height:180px; overflow-y:scroll" id="sewing_production_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800px" class="rpt_table" id="list_view_issue">
				<? 
				$grey_qty_array=array();
				$grey_sql="Select b.id, b.fabric_from, b.po_id, b.prod_id, b.item_description, b.fin_dia, b.batch_qnty from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and b.status_active=1 and b.is_deleted=0";
				$grey_sql_result =sql_select($grey_sql);
				foreach($grey_sql_result as $row)
				{
					$item_name=explode(',',$row[csf('item_description')]);
					$grey_qty_array[$row[csf('po_id')]]=$row[csf('batch_qnty')];
				}
				// var_dump($grey_qty_array);
				$order_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
				$currency_arr=return_library_array( "select b.id, a.currency_id from  subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst",'id','currency_id');
                $i=1;
				if(!$data[3])
				{
					$sql="select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where b.bill_status=0 and a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and a.is_deleted=0"; 
				}
				else
				{
					$sql="(select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and b.bill_status=0)
					 union 
					 	(select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and b.id in ( $data[3] ) and a.status_active=1)";
				}
				$sql_result =sql_select($sql);
					
				foreach($sql_result as $row)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
					<tr id="tr_<? echo $row[csf('id')]; ?>" bgcolor="<? echo $bgcolor; ?>"  style="text-decoration:none; cursor:pointer" onClick="js_set_value('<? echo $row[csf('id')]."_".$currency_arr[$row[csf('order_id')]]; ?>');" >
						<td width="30" align="center"><? echo $i; ?></td>
						<td width="80" align="center"><p><? echo $row[csf('challan_no')]; ?></p></td>
						<td width="80" align="center"><? echo change_date_format($row[csf('delivery_date')]); ?></td>
						<td width="150" align="center"><? echo $order_arr[$row[csf('order_id')]]; ?></td>
						 <?
							$process_id_val=$row[csf('process_id')];
							if($process_id_val==1 || $process_id_val==5)
							{
								$item_id_arr=$garments_item;
							}
							else
							{
								$item_id_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');	
							}
						?>
						<td width="200" align="center"><? echo $item_id_arr[$row[csf('item_id')]]; ?></td>
                        <td width="100" align="right"><? echo $grey_qty_array[$row[csf('order_id')]]; ?>&nbsp;</td>
						<td width="100" align="right"><? echo $row[csf('delivery_qty')]; ?>&nbsp;</td>
						<td width="100" align="center"><? echo $production_process[$row[csf('process_id')]]; ?></td>
						<td align="center"><? echo $currency[$currency_arr[$row[csf('order_id')]]]; ?>
						<input type="hidden" id="currid<? echo $row[csf('id')]; ?>" value="<? echo $currency_arr[$row[csf('order_id')]]; ?>"></td>
					</tr>
					<?
					$i++;
				}
				?>
		   </table>
		</div>
		   <table align="center">
				<tr align="center">
					<td align="center">
						<input type="button" id="show_button" align="middle" class="formbutton" style="width:100px" value="Close" onClick="window_close()" />
					</td>
				</tr>
		   </table>
	</body>           
	<script src="../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
    <?
	}
	exit();
}

if ($action=="load_php_dtls_form")  //new issue 
{
	//echo $data;
	$data = explode("_",$data);
	$del_id=array_diff(explode(",",$data[0]), explode(",",$data[1]));
	$bill_id=array_intersect(explode(",",$data[0]), explode(",",$data[1]));
	$delete_id=array_diff(explode(",",$data[1]), explode(",",$data[0]));
	$del_id=implode(",",$del_id); $bill_id=implode(",",$bill_id);   $delete_id=implode(",",$delete_id);
	
	$color_arr=return_library_array( "select id,color_name from lib_color",'id','color_name');
	$febricdesc_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');
	
	$order_array=array();
	$order_sql="Select b.id, b.order_no, b.order_uom, b.cust_buyer, b.cust_style_ref, b.main_process_id, b.process_id, b.rate, b.amount, a.currency_id from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0";
	$order_sql_result =sql_select($order_sql);
	foreach ($order_sql_result as $row)
	{
		$order_array[$row[csf("id")]]['order_no']=$row[csf("order_no")];
		$order_array[$row[csf("id")]]['order_uom']=$row[csf("order_uom")];
		$order_array[$row[csf("id")]]['cust_buyer']=$row[csf("cust_buyer")];
		$order_array[$row[csf("id")]]['cust_style_ref']=$row[csf("cust_style_ref")];
		$order_array[$row[csf("id")]]['rate']=$row[csf("rate")];
		$order_array[$row[csf("id")]]['amount']=$row[csf("amount")];
		$order_array[$row[csf("id")]]['currency_id']=$row[csf("currency_id")];
		$order_array[$row[csf("id")]]['main_process_id']=$row[csf("main_process_id")];
		$order_array[$row[csf("id")]]['process_id']=$row[csf("process_id")];
	}
	
	$grey_qty_array=array();
	$grey_sql="Select b.id, b.fabric_from, b.po_id, b.prod_id, b.item_description, b.fin_dia, b.batch_qnty from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and b.status_active=1 and b.is_deleted=0";
	$grey_sql_result =sql_select($grey_sql);
	foreach($grey_sql_result as $row)
	{
		$item_name=explode(',',$row[csf('item_description')]);
		$grey_qty_array[$row[csf('po_id')]]=$row[csf('batch_qnty')];
	}
	
	if( $data[2]!="" )
	{
		$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, add_process, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where mst_id in ($data[2]) and status_active=1 and process_id  in (3,4)"; 
	}
	else
	{
		if($bill_id!="" && $del_id!="")
			$sql="(select id as upd_id, delivery_id, delivery_date, challan_no, item_id, '' as add_process, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id in (3,4))
			 union
			 (select 0, a.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, '' as add_process, b.carton_roll, b.delivery_qty, 0 as rate, 0 as amount, null as remarks, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and b.process_id in (3,4))";
		else if($bill_id!="" && $del_id=="")
			$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, '' add_process, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id in (3,4)";
		else if($bill_id=="" && $del_id!="")
			$sql="select 0, b.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, '' as add_process, b.carton_roll, b.delivery_qty,  0 as rate, 0 as amount, null as remarks, b.order_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and a.is_deleted=0 and b.process_id in (3,4)";
	}
	//echo $sql;
	$sql_result =sql_select($sql);
	$k=0;
	$num_rowss=count($sql_result);
	foreach ($sql_result as $row)
	{
		$k++;
		if( $data[2]!="" )
		{
			if($data[1]=="") $data[1]=$row[csf("delivery_id")]; else $data[1].=",".$row[csf("delivery_id")];
		}
	?>
        <tr align="center" id="tr_<? echo $k; ?>">				
           <td>
				<? if ($k==$num_rowss) { ?>
                    <input type="hidden" name="issue_id_all" id="issue_id_all"  style="width:50px" value="<? echo $data[1]; ?>" />
                    <input type="hidden" name="delete_id" id="delete_id"  style="width:50px" value="<? echo $delete_id; ?>" />
                <? } ?>
                <input type="hidden" name="curanci_<? echo $k; ?>" id="curanci_<? echo $k; ?>"  style="width:50px" value="<? echo $order_array[$row[csf("order_id")]]['currency_id']; ?>" />
                <input type="hidden" name="updateiddtls_<? echo $k; ?>" id="updateiddtls_<? echo $k; ?>" value="<? echo ($row[csf("upd_id")] != 0 ? $row[csf("upd_id")] : "") ?>">
                <input type="hidden" name="deliveryid_<? echo $k; ?>" id="deliveryid_<? echo $k; ?>" value="<? echo $row[csf("delivery_id")]; ?>">
                <input type="text" name="txt_deliverydate_<? echo $k; ?>" id="txt_deliverydate_<? echo $k; ?>"  class="datepicker" style="width:60px" value="<? echo change_date_format($row[csf("delivery_date")]); ?>" readonly />									
            </td>
            <td>
                <input type="text" name="txt_challenno_<? echo $k; ?>" id="txt_challenno_<? echo $k; ?>"  class="text_boxes" style="width:45px" value="<? echo $row[csf("challan_no")]; ?>" readonly />							 
            </td>
            <td>
                <input type="hidden" name="ordernoid_<? echo $k; ?>" id="ordernoid_<? echo $k; ?>" value="<? echo $row[csf("order_id")]; ?>" style="width:40px" > 
                <input type="text" name="txt_orderno_<? echo $k; ?>" id="txt_orderno_<? echo $k; ?>"  class="text_boxes" style="width:65px" value="<? echo $order_array[$row[csf("order_id")]]['order_no']; ?>" readonly />										
            </td>
            <td>
                <input type="text" name="txt_stylename_<? echo $k; ?>" id="txt_stylename_<? echo $k; ?>"  class="text_boxes" style="width:75px;" value="<? echo $order_array[$row[csf("order_id")]]['cust_style_ref']; ?>"  />
            </td>
            <td>
                <input type="text" name="txt_buyername_<? echo $k; ?>" id="txt_buyername_<? echo $k; ?>"  class="text_boxes" style="width:65px" value="<? echo $order_array[$row[csf("order_id")]]['cust_buyer']; ?>"  />								
            </td>
            <td>			
                <input name="txt_numberroll_<? echo $k; ?>" id="txt_numberroll_<? echo $k; ?>" type="text" class="text_boxes" style="width:40px" value="<? echo $row[csf("carton_roll")]; ?>" readonly />							
            </td> 
            <td>
                <input type="hidden" name="itemid_<? echo $k; ?>" id="itemid_<? echo $k; ?>" value="<? echo $row[csf("item_id")]; ?>">
                <input type="text" name="text_febricdesc_<? echo $k; ?>" id="text_febricdesc_<? echo $k; ?>"  class="text_boxes" style="width:100px" value="<? echo $febricdesc_arr[$row[csf("item_id")]]; ?>" readonly/>
            </td>
            <td>
                 <input type="hidden" name="color_process_<? echo $k; ?>" id="color_process_<? echo $k; ?>" value="<? echo $order_array[$row[csf("order_id")]]['main_process_id']; ?>">
                <input type="text" name="txt_color_process_<? echo $k; ?>" id="txt_color_process_<? echo $k; ?>"  class="text_boxes" style="width:80px" value="<? echo $production_process[$order_array[$row[csf("order_id")]]['main_process_id']]; ?>" readonly/>
            </td>
            <td>
				<?
					$additional_process='';
					if($row[csf("upd_id")]!=0 || $row[csf("upd_id")]!='')
					{
						$additional_process=$row[csf("add_process")];
					}
					else
					{
						$additional_process=$order_array[$row[csf("order_id")]]['process_id'];
					}
					echo create_drop_down("cbo_process_id_".$k, 100, $conversion_cost_head_array,"", 1, "","",$additional_process, "","","","");
					//create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index, $new_conn, $field_name )
                ?>
                <script>
					set_multiselect('cbo_process_id_<? echo $k; ?>','0','0','','0');
					set_multiselect('cbo_process_id_<? echo $k; ?>','0','1','<? echo $additional_process; ?>','0');
				</script>
	        </td>
            <td>
            	<?
					$bill_qty='';
					if($data[3]==1)
					{
						$bill_qty=$grey_qty_array[$row[csf('order_id')]];
					}
					else if($data[3]==2)
					{
						$bill_qty=$row[csf("delivery_qty")];
					}
				?>
                <input type="text" name="txt_deliveryqnty_<? echo $k; ?>" id="txt_deliveryqnty_<? echo $k; ?>"  class="text_boxes_numeric" style="width:50px" value="<? echo $bill_qty; ?>" readonly />
            </td>
            <td>
                <input type="text" name="txt_rate_<? echo $k; ?>" id="txt_rate_<? echo $k; ?>"  class="text_boxes_numeric" style="width:40px" value="<? echo $order_array[$row[csf("order_id")]]['rate']; ?>" />
            </td>
            <td>
				<?
					$total_amount=$bill_qty*$order_array[$row[csf("order_id")]]['rate'];
                ?>
                <input type="text" name="txt_amount_<? echo $k; ?>" id="txt_amount_<? echo $k; ?>" style="width:55px"  class="text_boxes_numeric"  value="<? echo  $total_amount; ?>" readonly />
            </td>
            <td>
                <input type="text" name="txt_remarks_<? echo $k; ?>" id="txt_remarks_<? echo $k; ?>"  class="text_boxes" style="width:80px" value="<? echo $row[csf("remarks")]; ?>" />
           </td>
        </tr>
	<?	
	}
	echo "**".$k;
	exit();
}

if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Trems & Condition Search","../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
	<script>
	function add_break_down_tr(i) 
	{
		var row_num=$('#tbl_termcondi_details tr').length-1;
		if (row_num!=i)
		{
			return false;
		}
		else
		{
			i++;
		 
			 $("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
				$(this).attr({
				  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
				  'name': function(_, name) { return name + i },
				  'value': function(_, value) { return value }              
				});  
			  }).end().appendTo("#tbl_termcondi_details");
			 $('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
			 $('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
			 $('#termscondition_'+i).val("");
		}
	}

	function fn_deletebreak_down_tr(rowNo) 
	{   
		var numRow = $('table#tbl_termcondi_details tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_termcondi_details tbody tr:last').remove();
		}
	}

	function fnc_knitting_bill_terms_condition( operation )
	{
	    var row_num=$('#tbl_termcondi_details tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('termscondition_'+i,'Term Condition')==false)
			{
				return;
			}
			
			data_all=data_all+get_submitted_data_string('txt_bill_no*termscondition_'+i,"../../");
		}
		var data="action=save_update_delete_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
		//freeze_window(operation);
		http.open("POST","sub_fabric_finishing_bill_issue_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_knitting_bill_terms_condition_reponse;
	}

	function fnc_knitting_bill_terms_condition_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split('**');
			if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				parent.emailwindow.hide();
			}
		}
	}
	
</script>
</head>
<body>
<div align="center" style="width:100%;" >
	<? echo load_freeze_divs ("../../",$permission);  ?>
    <fieldset>
    <form id="termscondi_1" autocomplete="off">
        <input type="hidden" id="txt_bill_no" name="txt_bill_no" value="<? echo str_replace("'","",$txt_bill_no) ?>"/>
        <table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
            <thead>
                <tr><th width="50">SL</th><th width="530">Terms</th><th ></th> </tr>
            </thead>
            <tbody>
				<?
					$data_array=sql_select("select id, terms from  subcon_terms_condition where bill_no=$txt_bill_no and entry_form=2");// quotation_id='$data'
					if(count($data_array)>0)
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							?>
							<tr id="settr_1" align="center">
                                <td><? echo $i;?></td>
                                <td><input type="text" id="termscondition_<? echo $i;?>"   name="termscondition_<? echo $i;?>" style="width:95%"  class="text_boxes"  value="<? echo $row[csf('terms')]; ?>"  /></td>
                                <td> 
                                    <input type="button" id="increase_<? echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<? echo $i; ?> )" />
                                    <input type="button" id="decrease_<? echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<? echo $i; ?>);" />
                                </td>
							</tr>
						<?
                        }
					}
					else
					{
						$data_array=sql_select("select id, terms from  lib_terms_condition");
						foreach( $data_array as $row )
						{
							$i++;
							?>
                            <tr id="settr_1" align="center">
                                <td><? echo $i;?></td>
                                <td><input type="text" id="termscondition_<? echo $i;?>"   name="termscondition_<? echo $i;?>" style="width:95%"  class="text_boxes"  value="<? echo $row[csf('terms')]; ?>"  /></td>
                                <td>
                                <input type="button" id="increase_<? echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<? echo $i; ?> )" />
                                <input type="button" id="decrease_<? echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<? echo $i; ?> );" />
                                </td>
                            </tr>
						<? 
                        }
					} 
					?>
                </tbody>
            </table>
            <table width="650" cellspacing="0" class="" border="0">
                <tr><td align="center" height="15" width="100%"></td></tr>
                <tr>
                    <td align="center" width="100%" class="button_container">
                        <?
                            echo load_submit_buttons( $permission, "fnc_knitting_bill_terms_condition", 0,0 ,"reset_form('termscondi_1','','','','')",1) ; 
                        ?>
                    </td> 
                </tr>
            </table>
        </form>
    </fieldset>
    </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?
	exit();
}

if($action=="save_update_delete_terms_condition")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		//if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		$id=return_next_id( "id", "subcon_terms_condition", 1 ) ;
		$field_array="id,bill_no,terms,entry_form";
		for ($i=1;$i<=$total_row;$i++)
		{
			 $termscondition="termscondition_".$i;
			if ($i!=1) $data_array .=",";
			$data_array .="(".$id.",".$txt_bill_no.",".$$termscondition.",2)";
			$id=$id+1;
		}
		// echo  $data_array;
		$rID_de3=execute_query( "delete from subcon_terms_condition where  bill_no =".$txt_bill_no." and entry_form=2",0);

		$rID=sql_insert("subcon_terms_condition",$field_array,$data_array,1);
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$txt_bill_no;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$txt_bill_no;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "0**".$txt_bill_no;
			}
			else{
				oci_rollback($con);  
				echo "10**".$txt_bill_no;
			}
		}
		disconnect($con);
		die;
	}	
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)   // Insert Here========================================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if (is_duplicate_field( "delivery_id", "subcon_inbound_bill_dtls", "mst_id=$update_id" )==1)
		{
		echo "11**0"; 
		die;			
		}
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)";	
		}
		else if($db_type==2)
		{
			$year_cond=" and TO_CHAR(insert_date,'YYYY')";	
		}
		
		for($i=1; $i<=$tot_row; $i++)
		{
			$color_process="color_process_".$i;  
		}
		if (str_replace("'",'',$$color_process)==4)
		{
			$new_bill_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'DYE', date("Y",time()), 5, "select prefix_no,prefix_no_num from  subcon_inbound_bill_mst where company_id=$cbo_company_id and process_id=".$$color_process." $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
		}
		else if (str_replace("'",'',$$color_process)==3)
		{
			$new_bill_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'FIN', date("Y",time()), 5, "select prefix_no,prefix_no_num from  subcon_inbound_bill_mst where company_id=$cbo_company_id and process_id=".$$color_process." $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
		}
		if(str_replace("'",'',$update_id)=="")
		{
			$id=return_next_id( "id", "subcon_inbound_bill_mst", 1 ) ; 	
			$field_array="id,prefix_no,prefix_no_num,bill_no,company_id,location_id,bill_date,party_id,party_source,bill_for,process_id,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_bill_no[1]."','".$new_bill_no[2]."','".$new_bill_no[0]."',".$cbo_company_id.",".$cbo_location_name.",".$txt_bill_date.",".$cbo_party_name.",".$cbo_party_source.",".$cbo_bill_for.",".$$color_process.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
			//echo "INSERT INTO subcon_inbound_bill_mst (".$field_array.") VALUES ".$data_array;
			$rID=sql_insert("subcon_inbound_bill_mst",$field_array,$data_array,0);
			$return_no=$new_bill_no[0];
		}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="bill_no*company_id*location_id*bill_date*party_id*party_source*bill_for*updated_by*update_date";
			$data_array="".$txt_bill_no."*".$cbo_company_id."*".$cbo_location_name."*".$txt_bill_date."*".$cbo_party_name."*".$cbo_party_source."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			$rID=sql_update("subcon_inbound_bill_mst",$field_array,$data_array,"id",$update_id,0);
			$return_no=str_replace("'",'',$txt_bill_no);
		}
		$id1=return_next_id( "id", "subcon_inbound_bill_dtls",1);
		$field_array1 ="id, mst_id, delivery_id, delivery_date, challan_no, order_id, item_id, add_process, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id, inserted_by, insert_date";
		$field_array_up ="delivery_id*delivery_date*challan_no*order_id*item_id*add_process*packing_qnty*delivery_qty*rate*amount*remarks*currency_id*updated_by*update_date";
		$field_array_delivery="bill_status";
		$field_array_order="cust_buyer*cust_style_ref";
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
		{
			$delivery_id="deliveryid_".$i;
			$delevery_date="txt_deliverydate_".$i;
			$challen_no="txt_challenno_".$i;
			$orderid="ordernoid_".$i;
			$item_id="itemid_".$i;
			$style_name="txt_stylename_".$i;
			$buyer_name="txt_buyername_".$i;
			$number_roll="txt_numberroll_".$i;
			$quantity="txt_deliveryqnty_".$i;
			$rate="txt_rate_".$i;
			$amount="txt_amount_".$i;
			$curanci="curanci_".$i;
			$remarks="txt_remarks_".$i;
			$updateid_dtls="updateiddtls_".$i;
			$color_process="color_process_".$i;
			$add_process="cbo_process_id_".$i;
			  
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{ 
				if ($add_comma!=0) $data_array1.=",";
				$data_array1.="(".$id1.",".$id.",".$$delivery_id.",".$$delevery_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$add_process.",".$$number_roll.",".$$quantity.",".$$rate.",".$$amount.",".$$remarks.",'".str_replace("'",'',$$curanci)."','".str_replace("'",'',$$color_process)."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id1=$id1+1;
				$add_comma++;
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1")); 
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$delivery_id."*".$$delevery_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$add_process."*".$$number_roll."*".$$quantity."*".$$rate."*".$$amount."*".$$remarks."*".$$curanci."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1"));
			}
			 
			//order table insert====================================================================================================
			if(str_replace("'",'',$$style_name)=="" || str_replace("'",'',$$buyer_name)=="")  
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));
			}
			else
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));	
			}
		}
		$rID1=execute_query(bulk_update_sql_statement("subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
			//echo bulk_update_sql_statement( "subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr );
		$rID2=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery ));
			//echo $data_array1;die;
		$rID4=execute_query(bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr ));
		if($data_array1!="")
		{
			//echo "insert into subcon_inbound_bill_dtls (".$field_array1.") values ".$data_array1; die;
			$rID1=sql_insert("subcon_inbound_bill_dtls",$field_array1,$data_array1,1);
		}
		if($db_type==0)
		{
			if($rID && $rID1 && $rID2 && $rID4)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else if($rID)
			{
				mysql_query("ROLLBACK");  
				echo "5**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID1 && $rID2 && $rID4)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else if($rID)
			{
				oci_rollback($con);
				echo "5**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here=============================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$id=str_replace("'",'',$update_id);
		$field_array="bill_no*company_id*location_id*bill_date*party_id*party_source*bill_for*updated_by*update_date";
		$data_array="".$txt_bill_no."*".$cbo_company_id."*".$cbo_location_name."*".$txt_bill_date."*".$cbo_party_name."*".$cbo_party_source."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		$rID=sql_update("subcon_inbound_bill_mst",$field_array,$data_array,"id",$id,0);
		$return_no=str_replace("'",'',$txt_bill_no);
		$id1=return_next_id( "id", "subcon_inbound_bill_dtls",1);
		$field_array1 ="id, mst_id, delivery_id, delivery_date, challan_no, order_id, item_id, add_process, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id, inserted_by, insert_date";
		$field_array_up ="delivery_id*delivery_date*challan_no*order_id*item_id*add_process*packing_qnty*delivery_qty*rate*amount*remarks*currency_id*updated_by*update_date";
		$field_array_delivery="bill_status";
		$field_array_order="cust_buyer*cust_style_ref";
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
		{
			$delivery_id="deliveryid_".$i;
			$delevery_date="txt_deliverydate_".$i;
			$challen_no="txt_challenno_".$i;
			$orderid="ordernoid_".$i;
			$item_id="itemid_".$i;
			$style_name="txt_stylename_".$i;
			$buyer_name="txt_buyername_".$i;
			$number_roll="txt_numberroll_".$i;
			$quantity="txt_deliveryqnty_".$i;
			$rate="txt_rate_".$i;
			$amount="txt_amount_".$i;
			$curanci="curanci_".$i;
			$remarks="txt_remarks_".$i;
			$updateid_dtls="updateiddtls_".$i;
			$color_process="color_process_".$i;
			$add_process="cbo_process_id_".$i;
				
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{ 
				  if ($add_comma!=0) $data_array1 .=",";
				  $data_array1 .="(".$id1.",".$id.",".$$delivery_id.",".$$delevery_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$add_process.",".$$number_roll.",".$$quantity.",".$$rate.",".$$amount.",".$$remarks.",'".str_replace("'",'',$$curanci)."',".$$color_process.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				  $id1=$id1+1;
				  $add_comma++;
				
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1")); 
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$delivery_id."*".$$delevery_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$add_process."*".$$number_roll."*".$$quantity."*".$$rate."*".$$amount."*".$$remarks."*".$$curanci."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1"));
			}
		//order table insert====================================================================================================
			if(str_replace("'",'',$$style_name)=="" || str_replace("'",'',$$buyer_name)=="")  
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));
			}
			else
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));	
			}
		//order table insert====================================================================================================
		}
	 
		//echo bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery );die;
		$rID2=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery ));
	
		//echo bulk_update_sql_statement( "subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr );
		$rID1=execute_query(bulk_update_sql_statement("subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
		
		//echo bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr );die;
		$rID4=execute_query(bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr ));
			
		if($data_array1!="")
		{
			//echo "insert into subcon_inbound_bill_dtls (".$field_array1.") values ".$data_array1;die;
			$rID1=sql_insert("subcon_inbound_bill_dtls",$field_array1,$data_array1,0);
		}
		if(str_replace("'",'',$delete_id)!="")
		{
			$delete_id=str_replace("'",'',$delete_id);
			$rID3=execute_query( "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id)",0);
			$delete_id=explode(",",str_replace("'",'',$delete_id));
			for ($i=0;$i<count($delete_id);$i++)
			{
				$id_delivery[]=$delete_id[$i];
				$data_delivery[str_replace("'",'',$delete_id[$i])] =explode(",",("0"));
			}
		}
		//echo bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery );
		$rID3=execute_query(bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery ));
				
		if($db_type==0)
		{
			if($rID && $rID1 && $rID2 && $rID3 && $rID4)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else if($rID && $rID1 && $rID2 && $rID4)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID1 && $rID2 && $rID3 && $rID4)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else if($rID && $rID1 && $rID2 && $rID4)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		disconnect($con);
		die;
	}
	else if ($operation==2)  //Delete here======================================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$id=str_replace("'",'',$update_id);
		$return_no=str_replace("'",'',$txt_bill_no);
		$field_array_delivery="bill_status";
		if(str_replace("'",'',$delete_id)!="")
		{
			$delete_id=str_replace("'",'',$delete_id);
			$rID3=execute_query( "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id)",0);
			$delete_id=explode(",",str_replace("'",'',$delete_id));
			for ($i=0;$i<count($delete_id);$i++)
			{
				$id_delivery[]=$delete_id[$i];
				$data_delivery[str_replace("'",'',$delete_id[$i])] =explode(",",("0"));
			}
		}
		//echo bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery );
		$rID4=execute_query(bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery ));
		
		if($db_type==0)
		{
			if($rID3 && $rID4)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		disconnect($con);
		die;
	}
}
//======================================================================Bill Print============================================================================================
if($action=="fabric_finishing_print") 
 {
    extract($_REQUEST);
	//echo $data;
	$data=explode('*',$data);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$party_library=return_library_array( "select id,buyer_name from lib_buyer", "id","buyer_name"  );
	$yarn_desc_arr=return_library_array( "select id,yarn_description from lib_subcon_charge",'id','yarn_description');
	$const_comp_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');
	$color_arr=return_library_array( "select id,color_name from  lib_color",'id','color_name');
	
	$sql_mst="Select id, bill_no, bill_date, party_id, party_source, bill_for from subcon_inbound_bill_mst where company_id=$data[0] and id='$data[1]' and status_active=1 and is_deleted=0";
	$dataArray=sql_select($sql_mst);
	?>
    <div style="width:930px;">
         <table width="930" cellspacing="0" align="right" border="0">
            <tr>
                <td colspan="6" align="center" style="font-size:22px"><strong><? echo $company_library[$data[0]]; ?></strong></td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <?
                        /*$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
                        foreach ($nameArray as $result)
                        { 
                        ?>
                            Plot No: <? echo $result['plot_no']; ?> 
                            Level No: <? echo $result['level_no']?>
                            Road No: <? echo $result['road_no']; ?> 
                            Block No: <? echo $result['block_no'];?> 
                            City No: <? echo $result['city'];?> 
                            Zip Code: <? echo $result['zip_code']; ?> 
                            Province No: <?php echo $result['province'];?> 
                            Country: <? echo $country_arr[$result['country_id']]; ?><br> 
                            Email Address: <? echo $result['email'];?> 
                            Website No: <? echo $result['website'];
                        }*/
                    ?> 
                </td>
            </tr>           
        	<tr>
                <td colspan="6" align="center" style="font-size:20px"><u><strong><? echo $data[3]; ?></strong></u></td>
            </tr>
            <tr>
                <td width="130"><strong>Bill No :</strong></td> <td width="175"><? echo $bill_no=$dataArray[0][csf('bill_no')]; ?></td>
                <td width="130"><strong>Bill Date: </strong></td><td width="175px"> <? echo change_date_format($dataArray[0][csf('bill_date')]); ?></td>
                <td width="130"><strong>Source :</strong></td> <td width="175"><? echo $knitting_source[$dataArray[0][csf('party_source')]]; ?></td>
            </tr>
             <tr>
             <?
			 	if($dataArray[0][csf('party_source')]==3)
				{
					$party_add=$dataArray[0][csf('party_id')];
					$nameArray=sql_select( "select address_1, web_site, buyer_email, country_id from  lib_buyer where id=$party_add"); 
					foreach ($nameArray as $result)
					{ 
                    	$address="";
						if($result!="") $address=$result['address_1'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
					}
				}
			 ?>
             
                <td><strong>Party Name : </strong></td><td colspan="5"> <? echo $party_library[$dataArray[0][csf('party_id')]].' : Address :- '.$address; ?></td>
            </tr>
        </table>
         <br>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="30">SL</th>
                <th width="60" align="center">Challan No</th>
                <th width="65"  align="center">D. Date</th>
                <th width="70"align="center">Order</th> 
                <th width="70"  align="center">Buyer</th>
                <th width="70"  align="center">Style</th>
                <th width="120"  align="center">F. Des.</th>
                <th width="70"  align="center">Color</th>
                <th width="70"  align="center">Process</th>
                <th width="30"  align="center">UOM</th>
                <th width="30"  align="center">Roll</th>
                <th width="60"  align="center">D. Qty</th>
                <th width="30"  align="center">Rate</th>
                <th width="70"  align="center">Amount</th>
                <th width=""  align="center">Remarks</th>
            </thead>
		 <?
		 	$order_array=array();
			$order_sql="select id, order_no, order_uom, cust_buyer, process_id, cust_style_ref from subcon_ord_dtls where status_active=1 and is_deleted=0";
			$order_sql_result =sql_select($order_sql);
			foreach($order_sql_result as $row)
			{
				$order_array[$row[csf('id')]]['order_no']=$row[csf('order_no')];
				$order_array[$row[csf('id')]]['order_uom']=$row[csf('order_uom')];
				$order_array[$row[csf('id')]]['cust_buyer']=$row[csf('cust_buyer')];
				$order_array[$row[csf('id')]]['cust_style_ref']=$row[csf('cust_style_ref')];
				//$order_array[$row[csf('id')]]['process_id']=$row[csf('process_id')];
			}
			
			$delivery_array=array(); $batch_array=array();
			$sql_delivery="Select id, batch_no, batch_ext, color_id from  subcon_delivery_dtls ";
			$sql_delivery_result =sql_select($sql_delivery);
			foreach($sql_delivery_result as $row)
			{
				$delivery_array[$row[csf('id')]]['color_id']=$row[csf('color_id')];
				$batch_array[$row[csf('batch_no')]][$row[csf('batch_ext')]]['batch_ext']=$row[csf('color_id')];
			}
			
			//var_dump($order_array);
     		$i=1;
			$mst_id=$dataArray[0][csf('id')];
			//echo "select id, delivery_id, delivery_date, challan_no, order_id, item_id, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id from subcon_inbound_bill_dtls  where id='$mst_id' and process_id in (3,4) and status_active=1 and is_deleted=0";
			$sql_result =sql_select("select id, delivery_id, delivery_date, challan_no, order_id, item_id, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, add_process, process_id from subcon_inbound_bill_dtls  where mst_id='$mst_id' and process_id in (3,4) and status_active=1 and is_deleted=0"); 
			foreach($sql_result as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$sub_process='';
				$sub_process_id=explode(',',$row[csf('add_process')]);
				foreach($sub_process_id as $val)
				{
					if ($sub_process=='') $sub_process=$conversion_cost_head_array[$val]; else  $sub_process.=', '.$conversion_cost_head_array[$val];
				}
				
			?>
				<tr bgcolor="<? echo $bgcolor; ?>"> 
                    <td><? echo $i; ?></td>
                    <td><p><? echo $row[csf('challan_no')]; ?></p></td>
                    <td><p><? echo change_date_format($row[csf('delivery_date')]); ?></p></td>
                    <td><p><? echo $order_array[$row[csf('order_id')]]['order_no']; ?></p></td>
                    <td><p><? echo $order_array[$row[csf('order_id')]]['cust_buyer']; ?></p></td>
                    <td><p><? echo $order_array[$row[csf('order_id')]]['cust_style_ref']; ?></p></td>
                    <td><p><? echo $const_comp_arr[$row[csf('item_id')]]; ?></p></td>
                    <td><p><? echo $color_arr[$delivery_array[$row[csf('delivery_id')]]['color_id']]; ?></p></td>
                    <td><p><? echo $sub_process; ?></p></td>
                    <td><p><? echo $unit_of_measurement[$order_array[$row[csf('order_id')]]['order_uom']]; ?></p></td>
                    <td align="right"><p><? echo $row[csf('packing_qnty')]; $tot_packing_qty+=$row[csf('packing_qnty')]; ?>&nbsp;</p></td>
                    <td align="right"><p><? echo number_format($row[csf('delivery_qty')],2,'.',''); $tot_delivery_qty+=$row[csf('delivery_qty')]; ?>&nbsp;</p></td>
                    <td align="right"><p><? echo number_format($row[csf('rate')],2,'.',''); ?>&nbsp;</p></td>
                    <td align="right"><p><? echo number_format($row[csf('amount')],2,'.','');  $total_amount += $row[csf('amount')]; ?>&nbsp;</p></td>
                    <td><p><? echo $row[csf('remarks')]; ?></p></td>
                    <? 
					$carrency_id=$row['currency_id'];
					if($carrency_id==1){$paysa_sent="Paisa";} else if($carrency_id==2){$paysa_sent="CENTS";}
				   ?>
                </tr>
                <?php
                $i++;
			}
			?>
        	<tr> 
                <td align="right" colspan="10"><strong>Total</strong></td>
                <td align="right"><? echo $tot_packing_qty; ?>&nbsp;</td>
                <td align="right"><? echo number_format($tot_delivery_qty,2,'.',''); ?>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><? echo $format_total_amount=number_format($total_amount,2,'.',''); ?>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
           <tr>
               <td colspan="14" align="left"><b>In Word: <? echo number_to_words($format_total_amount,$currency[$carrency_id],$paysa_sent); ?></b></td>
           </tr>
        </table>
        <table width="930" align="left" > 
        	<tr><td colspan="2" align="center"><strong><u>Terms & Condition</u></strong></td> </tr>
        <?
			$sql_terms="Select id, terms from subcon_terms_condition where bill_no='$bill_no' and entry_form=2";
			$result_sql_terms =sql_select($sql_terms);
			$i=1;
			foreach($result_sql_terms as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			?>
				<tr bgcolor="<? echo $bgcolor; ?>"> 
                    <td width="30"><? echo $i; ?></td>
                    <td><p><? echo $row[csf('terms')]; ?></p></td>
                </tr>
			<?
			$i++;
			}
			?>
        </table>
        <br>
		 <?
            echo signature_table(48, $data[0], "930px");
         ?>
   </div>
   </div>
<?
}

?>
