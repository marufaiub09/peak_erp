﻿<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Finish Fabric Issue Return Entry
				
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	29-11-2014
Updated by 		: 		
Update date		: 	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Finish Fabric Issue Return Info","../../", 1, 1, '','',''); 

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";

</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;">
	<?php echo load_freeze_divs ("../../",$permission);  ?><br />    		 
    <form name="finishFabricEntry_1" id="finishFabricEntry_1" autocomplete="off" >
    <div style="width:740px; float:left;" align="center"> 
        <fieldset style="width:730px;">
        <legend>Finish Fabric Issue Entry</legend>
        	<fieldset style="width:730px;">
                <table width="730" cellspacing="2" cellpadding="2" border="0" id="tbl_master">
                    <tr>
                        <td colspan="3" align="right"><strong>Issue No</strong></td>
                        <td colspan="3" align="left">
                            <input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:150px;" placeholder="Double click to search" onDblClick="openmypage_systemId();" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="must_entry_caption">Company</td>
                        <td>
                            <?php 
								echo create_drop_down( "cbo_company_id", 150, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", 0, "load_drop_down( 'requires/finish_fabric_issue_controller', this.value, 'load_drop_down_store', 'store_td' );" );
							?>
                        </td>
                        <td class="must_entry_caption">Issue Date</td>
                        <td>
                            <input type="text" name="txt_issue_date" id="txt_issue_date" class="datepicker" style="width:138px;" readonly placeholder="Select Date" />
                        </td>
                        <td>Challan No.</td>
                        <td>
                            <input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:138px;" maxlength="20" title="Maximum 20 Character" />
                        </td>
                    </tr>
                </table>
            </fieldset>
            <table width="730" cellspacing="2" cellpadding="2" border="0" id="tbl_dtls">
                <tr>
                    <td width="68%" valign="top">
                        <fieldset>
                        <legend>New Entry</legend>
                            <table id="tbl_item_info"  cellpadding="0" cellspacing="1" width="100%">										
                                <tr>
                                	<td class="must_entry_caption" width="30%">Store Name</td>
                                    <td id="store_td">
                                        <?php
                                            echo create_drop_down( "cbo_store_name", 170, "select id, store_name from lib_store_location where find_in_set(2,item_category_id) and status_active=1 and is_deleted=0 order by store_name","id,store_name", 1, "-- Select store --", 0, "" );
                                        ?>	
                                    </td>
                                </tr>
                                <tr>	
                                	<td class="must_entry_caption">Batch No.</td>
                                    <td>
                                        <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:158px;" placeholder="Write / Browse" onDblClick="openmypage_batchnum();" onChange="check_batch(this.value);"/>
                                        <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" readonly />
                                    </td>
                                </tr>						
                                <tr>
                                    <td class="must_entry_caption">Fabric Description</td>
                                    <td id="fabricDesc_td">
                                    	<input type="text" name="txt_fabric_desc" id="txt_fabric_desc" class="text_boxes" style="width:300px;" placeholder="Display" disabled />
                                        <input type="hidden" name="hidden_prod_id" id="hidden_prod_id" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Color</td>						
                                    <td>
                                    	<input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:158px" placeholder="Display" disabled />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="must_entry_caption">Issue Qnty</td>
                                    <td>
                                    	<input type="text" name="txt_issue_qnty" id="txt_issue_qnty" class="text_boxes_numeric" style="width:158px;" readonly placeholder="Double Click To Search" onDblClick="openmypage_po();" /></td>
                                </tr>
                                <tr>
                                    <td>No. of Roll</td>						
                                    <td>
                                    	<input type="text" name="txt_no_of_roll" id="txt_no_of_roll" class="text_boxes_numeric" style="width:158px" disabled="disabled" placeholder="Display" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Rack</td>						
                                    <td>
                                    	<input type="text" name="txt_rack" id="txt_rack" class="text_boxes" style="width:158px" placeholder="Display" disabled />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Shelf</td>						
                                    <td>
                                    	<input type="text" name="txt_shelf" id="txt_shelf" class="text_boxes" style="width:158px" placeholder="Display" disabled/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Cutting Unit No</td>						
                                    <td>
                                    	<?php
                                        echo create_drop_down( "cbo_cutting_floor", 170, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 and production_process=1 order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected, "",0 );
										?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>						
                                    <td>
                                    	<input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:300px" />
                                    </td>
                                </tr>
							</table>
						</fieldset>
					</td>
					<td width="2%" valign="top"></td>
					<td width="30%" valign="top">
						<fieldset>
                        <legend>Display</legend>					
                            <table id="tbl_display_info"  cellpadding="0" cellspacing="1" width="100%" >				
                                <tr>
                                    <td>Order Numbers</td>						
                                	<td>
                                    	<input type="text" name="txt_order_numbers" id="txt_order_numbers" class="text_boxes" style="width:100px" disabled />
                                    </td>
								</tr>
                                <tr>
                                    <td>Fabric Received</td>						
                                    <td><input type="text" name="txt_fabric_received" id="txt_fabric_received" class="text_boxes_numeric" style="width:100px" disabled /></td>
                                </tr>
                                <tr>
                                    <td>Cumulative Issued</td>
                                    <td><input type="text" name="txt_cumulative_issued" id="txt_cumulative_issued" class="text_boxes_numeric" style="width:100px" disabled /></td>
                                </tr>					
                                <tr>
                                    <td>Yet to Issue</td>
                                    <td><input type="text" name="txt_yet_to_issue" id="txt_yet_to_issue" class="text_boxes_numeric" style="width:100px" disabled /></td>
                                </tr>
                                <tr>
                                    <td>Global Stock</td>
                                    <td><input type="text" name="txt_global_stock" id="txt_global_stock" placeholder="Display" class="text_boxes" style="width:100px" readonly disabled /></td>
                                </tr>											
                            </table>                  
                       </fieldset>	
              		</td>
				</tr>	 	
                <tr>
                    <td align="center" colspan="3" class="button_container" width="100%">
                        <?php
                            echo load_submit_buttons($permission, "fnc_fabric_issue_entry", 0,1,"reset_form('finishFabricEntry_1','div_details_list_view*list_fabric_desc_container','','9','disable_enable_fields(\'cbo_company_id\');')",1);
                        ?>
                        <input type="hidden" id="update_id" name="update_id" value="" >
                        <input type="hidden" name="save_data" id="save_data" readonly>
                        <input type="hidden" name="save_string" id="save_string" readonly>
                        <input type="hidden" name="update_dtls_id" id="update_dtls_id" readonly>
                        <input type="hidden" name="update_trans_id" id="update_trans_id" readonly>
                        <input type="hidden" name="previous_prod_id" id="previous_prod_id" readonly>
                        <input type="hidden" name="hidden_issue_qnty" id="hidden_issue_qnty" readonly>
                        <input type="hidden" name="txt_issue_req_qnty" id="txt_issue_req_qnty" readonly>
                        <input type="hidden" name="all_po_id" id="all_po_id" readonly>
                        <input type="hidden" name="roll_maintained" id="roll_maintained" readonly>
                        <input type="hidden" name="distribution_method_id" id="distribution_method_id" readonly />
                    </td>
                </tr>
            </table>
            <div style="width:730px;" id="div_details_list_view"></div>
		</fieldset>
	</div>
    <div id="list_fabric_desc_container" style="width:490px; margin-left:10px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>
	</form>
</div>    
</body>  
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
