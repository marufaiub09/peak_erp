﻿<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Buyer Inquery VS Quatation Report
				
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	01/09/2014
Updated by 		: 	Maruf	
Update date		: 	08-12-2015	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Info","../../../", 1, 1, $unicode);
?>	

<script>
	var permission='<?php echo $permission; ?>';
		
	function fn_report_generated()
	{
		if(form_validation('cbo_company_name','Company Name')==false)
		{
			return;
		}
		else
		{	
			var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*txt_session',"../../../");
			freeze_window(3);
			http.open("POST","requires/buyer_inquery_qote_submit_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
		
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			//alert(http.responseText);
			var reponse=trim(http.responseText).split("****");
			$('#report_container2').html("");
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Html Preview" name="Print" class="formbutton" style="width:100px"/>';
			setFilterGrid('tbl_ship_pending',-1);
			show_msg('3');
			release_freezing();
		}
	}
	
	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		$('#tbl_ship_pending tbody').find('tr:first').hide();
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css"/><style>.verticalText2{writing-mode: tb-rl; filter: flipv fliph; -webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);-o-transform: rotate(270deg);-ms-transform: rotate(270deg);transform: rotate(270deg);width: 1em;line-height: 1em;};@media print {thead {display: table-header-group;}}</style></head><body>'+document.getElementById('report_container2').innerHTML+'</body></html>');
		d.close(); 
		$('#tbl_ship_pending tbody').find('tr:first').show();
		document.getElementById('scroll_body').style.overflowY="scroll";
		document.getElementById('scroll_body').style.maxHeight="200px";
	}
	
	
	function openSession()
	{
		if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}
		var company = $("#cbo_company_name").val();	
		var buyer = $("#cbo_buyer_name").val();	
		var page_link='requires/buyer_inquery_qote_submit_controller.php?action=season_surch&company='+company+'&buyer='+buyer;  
		var title="Search Season Popup";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=430px,height=370px,center=1,resize=0,scrolling=0','../../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]; 
			var session_ref=this.contentDoc.getElementById("hidden_session").value.split('_');
			$("#txt_session").val(session_ref[0]);
			$("#cbo_buyer_name").val(session_ref[1]);
		}
	}
	
</script>
</head>
<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
    <?php echo load_freeze_divs ("../../../");  ?>
    <h3 align="left" id="accordion_h1" style="width:700px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
        <div id="content_search_panel"> 
            <form id="frm_inquery_vs_quatation" name="frm_inquery_vs_quatation" action="" autocomplete="off" method="post">
                <fieldset style="width:700px;">
                    <table width="650" cellpadding="0" cellspacing="2" border="1" rules="all" class="rpt_table" >
                        <thead>
                            <th width="200">Company Name</th>
                            <th width="200">Buyer Name</th>
                            <th width="150">Season</th>
                            <th ><input type="reset" name="res" id="res" value="Reset" style="width:100px" class="formbutton" onClick="reset_form('frm_inquery_vs_quatation','report_container*report_container2','','','')" /></th>
                        </thead>
                        <tr>
                            <td align="center">
                            <?php
                                echo create_drop_down( "cbo_company_name", 180, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/buyer_inquery_qote_submit_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                            ?>                                     
                            </td>
                            <td id="buyer_td" align="center">
							<?php 
                            echo create_drop_down( "cbo_buyer_name", 180, $blank_array,"", 1, "-- All Buyer --", $selected, "",0,"" );
                            ?>
                            </td>
                             <td align="center">
                            <input type="text" id="txt_session" name="txt_session" class="text_boxes" style="width:140px;" onDblClick="openSession();" placeholder="Browse" readonly >  
                            </td>
                            <td>
                                <input type="button" name="show" id="Pending" onClick="fn_report_generated();" class="formbutton" style="width:100px" value="Show" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </form>
        </div>
       <div id="report_container" align="center"></div>
       <div id="report_container2"></div>
 </div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>