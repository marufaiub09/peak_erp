-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pri_quo_sum_dtls`
--

DROP TABLE IF EXISTS `wo_pri_quo_sum_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pri_quo_sum_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_id` int(11) NOT NULL,
  `fab_yarn_req_kg` double NOT NULL,
  `fab_woven_req_yds` double NOT NULL,
  `fab_knit_req_kg` double NOT NULL,
  `fab_amount` double NOT NULL DEFAULT '0',
  `yarn_cons_qnty` double NOT NULL,
  `yarn_amount` double NOT NULL DEFAULT '0',
  `conv_req_qnty` double NOT NULL,
  `conv_charge_unit` double NOT NULL,
  `conv_amount` double NOT NULL DEFAULT '0',
  `trim_cons` double NOT NULL,
  `trim_rate` double NOT NULL,
  `trim_amount` double NOT NULL DEFAULT '0',
  `emb_amount` double NOT NULL,
  `wash_amount` double NOT NULL,
  `comar_rate` double NOT NULL,
  `comar_amount` double NOT NULL,
  `commis_rate` double NOT NULL,
  `commis_amount` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_no` (`quotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pri_quo_sum_dtls`
--

LOCK TABLES `wo_pri_quo_sum_dtls` WRITE;
/*!40000 ALTER TABLE `wo_pri_quo_sum_dtls` DISABLE KEYS */;
INSERT INTO `wo_pri_quo_sum_dtls` VALUES (1,1,2.98,0,2.98,0,2.98,8.0758,5.96,3.96,5.9004,11,4.15,4.565,0.9,0,1.5,0.1896,0,0,0,NULL,0,NULL,1,0);
/*!40000 ALTER TABLE `wo_pri_quo_sum_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
