<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if($action=="pi_details")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	//print_r ($pi_id);
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
?>	
<script>

	function print_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	
		d.close();
		document.getElementById('scroll_body').style.overflowY="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
	}
	
	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<div style="width:800px" align="center" id="scroll_body" >
<fieldset style="width:100%; margin-left:10px" >
<input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" value="Close" onClick="window_close()" style="width:100px"  class="formbutton"/>
         <div id="report_container" align="center" style="width:100%" > 
             <div style="width:780px">
                <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="500" align="center">
                    <thead>
                        <th colspan="4" align="center"><?php echo $companyArr[$company_name]; ?></th>
                    </thead>
                    	<tr>
                            <td width="150"><strong>LC Number : </strong></td> <td width="150"><strong><?php  echo $lc_number; ?></strong></td>
                            <td><strong>Last Ship Date :</strong></td><td><strong><?php  echo change_date_format($ship_date); ?></strong></td>
                        </tr>
                    	<tr>
                            <td width="150"><strong>Supplier : </strong></td> <td width="150"><strong><?php  echo $supplierArr[$supplier_id]; ?></strong></td>
                            <td><strong>Expiry Date :</strong></td><td><strong><?php  echo change_date_format($exp_date); ?></strong></td>
                        </tr>
                    	<tr>
                            <td width="150"><strong>LC Date : </strong></td> <td width="150"><strong><?php  echo change_date_format($lc_date); ?></strong></td>
                            <td><strong>Pay Term :</strong></td><td><strong><?php  echo $pay_term[$payterm]; ?></strong></td>
                        </tr>
                </table>
                <br />
                <table class="rpt_table" border="1" rules="all" width="100%" cellpadding="0" cellspacing="0">
                	 <thead bgcolor="#dddddd">
                     	<tr>
                            <th width="30">SL</th>
                            <th width="80">PI NO.</th>
                            <th width="100">Item Group</th>
                            <th width="130">Item Description</th>
                            <th width="80">Qnty</th>
                            <th width="70">Rate</th>
                            <th width="90">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
		$i=1;
		//if ($pi_id==0) $piId =""; else $piId =" and a.id in ($pi_id)";
		$sql="Select a.id, a.pi_number, b.item_prod_id, b.determination_id, b.item_group, b.item_description, b.size_id, b.quantity, b.rate, b.amount from com_pi_master_details a, com_pi_item_details b where a.id=b.pi_id and a.id in ($pi_id) order by a.pi_number";
		$result=sql_select($sql);
		
		$pi_arr=array();
		foreach( $result as $row)
		{
/*			if (!in_array($row[csf("pi_number")],$pi_arr) )
			{
				$pi_arr[]=$row[csf('pi_number')];
*/			?>
                   
<!--                        <tr>
                            <td colspan="6" align="left">PI No : <?php //echo $row[csf('pi_number')]; ?></td>
                        </tr>
-->                    	
					<?php
					$total_qnt+=$row[csf("quantity")];
					$total_amount+=$row[csf("amount")];
					
             // }
				if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
			?>
            <tr bgcolor="<?php echo $bgcolor ; ?>">
            	<td><?php echo $i; ?></td>
                 <td><?php echo $row[csf("pi_number")]; ?></td>
                <td><?php echo $itemgroupArr[$row[csf("item_group")]]; ?></td>
                <td><?php echo $row[csf("item_description")]; ?></td>
                <td align="right"><?php echo $row[csf("quantity")]; ?></td>
                <td align="right"><?php echo $row[csf("rate")]; ?></td>
                <td align="right"><?php echo number_format($row[csf("amount")],2); ?></td>
            </tr>
          </tbody>
		<?php	
        $i++;
        } 
		   ?>
             <tfoot>
                <th colspan="4" align="right">Total : </th>
                <th align="right"><?php echo number_format($total_qnt,0); ?></th>
                <th>&nbsp;</th>
                <th align="right"><?php echo number_format($total_amount,2); ?></th>
            </tfoot>
        </table>
		</div>
        </div>
	</fieldset>
</div>
<?php
exit();
}

if ($action=="report_generate")
{
	extract($_REQUEST);
	//echo $cbo_company_id;
	
	$companyArr = return_library_array("select id,company_short_name from lib_company where status_active=1 and is_deleted=0","id","company_short_name"); 
	$supplierArr = return_library_array("select id,short_name from lib_supplier where status_active=1 and is_deleted=0","id","short_name");
	$issueBankrArr = return_library_array("select id,bank_name from lib_bank ","id","bank_name"); 
	$hscodeArr = return_library_array("select id,hs_code from com_pi_master_details ","id","hs_code"); 
	$lc_num_arr = return_library_array("select id,export_lc_no from com_export_lc ","id","export_lc_no"); 
	$sc_num_arr = return_library_array("select id,contract_no from com_sales_contract ","id","contract_no"); 
	
	$tot_lc_qty_arr = return_library_array("select pi_id,sum(quantity) as quantity from com_pi_item_details group by  pi_id","pi_id","quantity");
	$tot_rec_qty_arr = return_library_array("select pi_wo_batch_no,sum(cons_quantity) as cons_quantity from inv_transaction where receive_basis=1 and transaction_type=1 and item_category=1 and status_active=1 group by  pi_wo_batch_no","pi_wo_batch_no","cons_quantity");
	
	
	
	?>
	<div id="scroll_body" align="center" style="height:auto; width:2370px; margin:0 auto; padding:0;">
     <table width="2200px" >
	<?php
	$company_library=sql_select("select id, company_name, plot_no, level_no,road_no,city from lib_company where id=".$cbo_company_id."");
	foreach( $company_library as $row)
	{
	?>
        <tr>
            <td colspan="29" align="center" style="font-size:22px"><center><strong><?php echo $row[csf('company_name')];?></strong></center></td>
        </tr>
<!--        <span style="font-size:20px"><center><b><?php// echo $row[csf('company_name')];?></b></center></span>
-->	<?php
	}
	?>
   
        <tr>
            <td colspan="29" align="center" style="font-size:18px"><center><strong><u><?php echo $report_title; ?></u></strong></center></td>
        </tr>
    </table>
        <table cellspacing="0" width="2350px"  border="1" rules="all" class="rpt_table" >
           <thead>
                    <th width="30">SL</th>
                    <th width="60" align="center">Company</th>
                    <th width="100" align="center">LC Number</th>
                    <th width="70" align="center">Internal File No</th>
                    <th width="80" align="center">Supply Source</th>
                    <th width="100" align="center">LC/SC</th>
                    <th width="70" align="center">LC Date</th>
                    <th width="60" align="center">Supplier</th>
                    <th width="50" align="center">Curr.</th>
                    <th width="90" align="center">LC Value</th>
                    <th width="80" align="center">Total LC Qty</th>
                    <th width="80" align="center">Total Rec. Qty</th>
                    <th width="80" align="center">Balance</th>
                    <th width="40" align="center">PI Dtls</th>
                    <th width="50" align="center">Inco Term</th>
                    <th width="80" align="center">Inco T. Place</th>
                    <th width="120" align="center">Issuing Bank</th>
                    <th width="80" align="center">Item Category</th>
                    <th width="70" align="center">LC Type</th>
                    <th width="40" align="center">Tenor</th>
                    <th width="70" align="center">Ship Date</th>
                    <th width="70" align="center">LC Expiry Date</th>
                    <th width="60" align="center">HS Code</th>
                    <th width="40" align="center">Pres. Days</th>
                    <th width="60" align="center">Delivery Mode</th>
                    <th width="110" align="center">PSI Company</th>
                    <th width="110" align="center">Insurance Company</th>
                    <th width="80" align="center">Cover Note No</th>
                    <th width="100" align="center">Maturity From</th>
                    <th width="50" align="center">Margin %</th>
                    <th align="center">Bonded</th>
            </thead>
        </table>
        <div style="width:2350px; overflow-y: scroll; max-height:300px;" id="scroll_body2">
        <table cellspacing="0" width="2332px"  border="1" rules="all" class="rpt_table" id="tbl_marginlc_list" >
        <?php
		//var_dump($assocArray);die;
		$cbo_company=str_replace("'","",$cbo_company_id);
		$cbo_issue=str_replace("'","",$cbo_issue_banking);
		$item_category_id=str_replace("'","",$cbo_item_category_id);
		$lc_type_id=str_replace("'","",$cbo_lc_type_id);
		
		$supply_so=str_pad(str_replace("'","",$cbo_supply_source), 2, "0", STR_PAD_LEFT);
		if (str_replace("'","",$cbo_supply_source)==0) $supply_source_cond =""; else $supply_source_cond ="and a.lc_category='".$supply_so."'";
		
		$cbo_bonded=str_replace("'","",$cbo_bonded_warehouse);
		$from_date=str_replace("'","",$txt_date_from);
		$to_date=str_replace("'","",$txt_date_to);
		//echo $from_date."**".$to_date."jahid";die;
		
		if ($cbo_company==0) $company_id =""; else $company_id =" and a.importer_id=$cbo_company ";
		if ($cbo_issue==0) $issue_banking =""; else $issue_banking =" and a.issuing_bank_id=$cbo_issue ";
		if ($item_category_id==0) $category_id =""; else $category_id =" and a.item_category_id=$item_category_id ";
		if ($lc_type_id==0) $lc_tpe_id =""; else $lc_tpe_id =" and a.lc_type_id=$lc_type_id ";
		
		if ($cbo_bonded==0) $bonded_warehouse =""; else $bonded_warehouse =" and a.bonded_warehouse=$cbo_bonded ";
		//print $company; $txt_pay_date=date("j-M-Y",strtotime($txt_pay_date));
		if($db_type==2)
		{
 			if( $from_date=="" && $to_date=="" ) $lc_date=""; else $lc_date= " and a.lc_date between '".date("j-M-Y",strtotime($from_date))."' and '".date("j-M-Y",strtotime($to_date))."'";
		}
		else if($db_type==0)
		{
			if( $from_date=='' && $to_date=='' ) $lc_date=""; else $lc_date= " and a.lc_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'";
		}
		
		$sql="Select a.id, a.importer_id, a.lc_number, a.lc_date, a.lc_category, a.supplier_id, a.currency_id, a.lc_value, a.pi_id, a.inco_term_id, a.inco_term_place, a.issuing_bank_id, a.item_category_id, a.lc_type_id, a.tenor, a.last_shipment_date, a.lc_expiry_date, a.hs_code, a.doc_presentation_days, a.delivery_mode_id, a.psi_company, a.insurance_company_name, a.cover_note_no, a.maturity_from_id, a.margin, a.bonded_warehouse
		from com_btb_lc_master_details a
		where a.status_active=1 and a.is_deleted=0 $company_id  $issue_banking $category_id $lc_tpe_id $bonded_warehouse $lc_date  $supply_source_cond";
		
		//echo $sql;die;
		
		
		if($db_type==2)
		{
			$lc_sc_sql=sql_select("Select a.id,b.is_lc_sc ,LISTAGG(CAST(b.lc_sc_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.lc_sc_id) as lc_sc_id
					from com_btb_lc_master_details a, com_btb_export_lc_attachment b
					where a.id=b.import_mst_id and a.status_active=1 and a.is_deleted=0 $company_id 
					group by a.id,b.is_lc_sc");
		}
		else if($db_type==0)
		{
			$lc_sc_sql=sql_select("Select a.id,b.is_lc_sc ,group_concat(b.lc_sc_id) as lc_sc_id
					from com_btb_lc_master_details a, com_btb_export_lc_attachment b
					where a.id=b.import_mst_id and a.status_active=1 and a.is_deleted=0 $company_id 
					group by a.id,b.is_lc_sc");
		}
		
		foreach($lc_sc_sql as $row)
		{
			$ls_sc_data[$row[csf("id")]]['is_lc_sc']=$row[csf("is_lc_sc")];
			$ls_sc_data[$row[csf("id")]]['lc_sc_id']=$row[csf("lc_sc_id")];
		}
		
		
		$file_sql_all=sql_select("select b.id, a.internal_file_no, 1 as type from com_export_lc a, com_btb_lc_master_details b, com_btb_export_lc_attachment c where a.id=c.lc_sc_id and c.import_mst_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.is_lc_sc=0
	union all
	select b.id, a.internal_file_no, 2 as type from com_sales_contract a, com_btb_lc_master_details b, com_btb_export_lc_attachment c where a.id=c.lc_sc_id and c.import_mst_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.is_lc_sc=1");
		$file_no_arr=array();
		foreach($file_sql_all as $row)
		{
			$file_no_arr[$row[csf("id")]]=$row[csf("internal_file_no")];
		}
	
		
		
		//echo $sql;die;
		$sql_data = sql_select($sql);
		$i=1;
		foreach( $sql_data as $row)
		{
			if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
				
				$tot_lc_value+=$row[csf("lc_value")];
				
				$company_name=$row[csf("importer_id")];
				$lc_number=$row[csf("lc_number")];
				$ship_date=$row[csf("last_shipment_date")];
				$supplier_id=$row[csf("supplier_id")];
				$lc_date=$row[csf("lc_date")];
				$exp_date=$row[csf("lc_expiry_date")];
				$payterm=$row[csf("payterm_id")];
				$pi_id_arr=array_unique(explode(",",$row[csf("pi_id")]));
				
				
				foreach($pi_id_arr as $pi_id)
				{
					$tot_lc_qty +=$tot_lc_qty_arr[$pi_id];
					$tot_rec_qty +=$tot_rec_qty_arr[$pi_id];
				}
				
			?>
            <tr bgcolor="<?php echo $bgcolor ; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30" ><?php echo $i; ?></td>
                <td align="center"  width="60"><p><?php echo $companyArr[$row[csf("importer_id")]]; ?></p></td>
                <td width="100"><p><a href="#report_details" onclick="lc_details_popup('<?php echo $row[csf('id')];?>','lc_details','LC Details');"><?php echo $row[csf("lc_number")]; ?></a></p></td>
                <td width="70" align="center"><p><?php echo $file_no_arr[$row[csf("id")]]; ?></p></td>
                <td width="80"><p><?php echo $supply_source[(int)$row[csf("lc_category")]]; ?></p></td>
                <td width="100"><p>
                <?php
				$lc_sc_num="";
				$p=1;
				$lc_sc_id_arr=array_unique(explode(",",$ls_sc_data[$row[csf("id")]]['lc_sc_id']));
				foreach($lc_sc_id_arr as $lc_sc_id)
				{
					if($p!=1) $lc_sc_num .=", ";
					if($ls_sc_data[$row[csf("id")]]['is_lc_sc']==0)
					{
						$lc_sc_num .=$lc_num_arr[$lc_sc_id];
					}
					else
					{
						$lc_sc_num .=$sc_num_arr[$lc_sc_id];
					}
					$p++;
				}
				echo $lc_sc_num;
				?>
                </p></td>
                <td width="70"><p><?php echo change_date_format($row[csf("lc_date")]); ?></p></td>
                <td width="60"><p><?php echo $supplierArr[$row[csf("supplier_id")]]; ?></p></td>
                <td width="50" align="center"><p><?php echo $currency[$row[csf("currency_id")]]; ?></p></td>
                <td width="90" align="right"><p><?php echo number_format($row[csf("lc_value")],2); ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($tot_lc_qty,2); ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($tot_rec_qty,2); ?></p></td>
                <td width="80" align="right"><p><?php $balance=$tot_lc_qty-$tot_rec_qty; echo number_format($balance,2); ?></p></td>
                <td width="40" align="center"><p>
				<?php
				$pi_id=$row[csf("pi_id")]; 
				echo "<a href='#report_details' style='color:#000' onclick= \"openmypage('$company_name','$lc_number','$ship_date','$supplier_id','$lc_date','$exp_date','$payterm','$pi_id','pi_details','PI Details');\">"."View"."</a>"; //$row[csf("pi_id")]; 
				?>
                </p></td>
                <td width="50" align="center"><p><?php echo $incoterm[$row[csf("inco_term_id")]]; ?></p></td>
                <td width="80" align="center"><p><?php echo $row[csf("inco_term_place")]; ?></p></td>
                <td width="120"><p><?php echo $issueBankrArr[$row[csf("issuing_bank_id")]]; ?></p></td>
                <td width="80"><p><?php echo $item_category[$row[csf("item_category_id")]]; ?></p></td>
                <td width="70"><p><?php echo $lc_type[$row[csf("lc_type_id")]]; ?></p></td>
                <td  width="40" align="center"><p><?php echo $row[csf("tenor")]; ?></p></td>
                <td width="70"><p><?php echo change_date_format($row[csf("last_shipment_date")]); ?></p></td>
                <td width="70" align="center"><p><?php echo change_date_format($row[csf("lc_expiry_date")]); ?></p></td>
                <td width="60" align="center"><p><?php echo $hscodeArr[$row[csf("pi_id")]]; ?></p></td>
                <td width="40" align="center"><p><?php echo $row[csf("doc_presentation_days")]; ?></p></td>
                <td width="60" align="center"><p><?php echo $shipment_mode[$row[csf("delivery_mode_id")]]; ?></p></td>
                <td width="110" align="center"><p><?php echo $row[csf("psi_company")]; ?></p></td>
                <td width="110"><p><?php echo $row[csf("insurance_company_name")]; ?></p></td>
                <td width="80"><p><?php echo $row[csf("cover_note_no")]; ?></p></td>
                <td width="100" align="center"><p><?php echo $maturity_from[$row[csf("maturity_from_id")]]; ?></p></td>
                <td width="50" align="center"><p><?php echo $row[csf("margin")]; ?></p></td>
                <td align="center"><p><?php echo $yes_no[$row[csf("bonded_warehouse")]]; ?></p></td>
            </tr>
		<?php	
        $i++;
        }
	?>
         </table>
          <table cellspacing="0" width="2350px"  border="1" rules="all" class="rpt_table" >
            <tfoot>
            	<th width="30" >&nbsp;</th>
                <th align="center"  width="60">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="60">&nbsp;</th>
                <th width="50" align="center">Total : </th>
                <th align="right" width="90" id="value_tot_lc_value"><?php echo number_format($tot_lc_value,2); ?></th>
                <th width="80" align="center">&nbsp;</th>
                <th width="80" align="center">&nbsp;</th>
                <th width="80" align="center">&nbsp;</th>
                <th width="40" align="center">&nbsp;</th>
                <th width="50" align="center">&nbsp;</th>
                <th width="80" align="center">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th  width="40" align="center">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70" align="center">&nbsp;</th>
                <th width="60" align="center">&nbsp;</th>
                <th width="40" align="center">&nbsp;</th>
                <th width="60" align="center">&nbsp;</p></th>
                <th width="110" align="center">&nbsp;</th>
                <th width="110">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="100" align="center">&nbsp;</th>
                <th width="50" align="center"></th>
                <th align="center">&nbsp;</th>
            </tfoot>         
    </table>
	</div>
	</div>
<?php	
}

if($action=="lc_details")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	$file_sql="select a.internal_file_no, 1 as type from com_export_lc a, com_btb_lc_master_details b, com_btb_export_lc_attachment c where a.id=c.lc_sc_id and c.import_mst_id=b.id and  b.id=$lc_no and a.status_active=1 and a.is_deleted=0 and c.is_lc_sc=0
	union all
	select a.internal_file_no, 2 as type from com_sales_contract a, com_btb_lc_master_details b, com_btb_export_lc_attachment c where a.id=c.lc_sc_id and c.import_mst_id=b.id and  b.id=$lc_no and a.status_active=1 and a.is_deleted=0 and c.is_lc_sc=1";
	//echo $file_sql;
	
	$dataArray=sql_select($file_sql);
?>	
<script>
	
	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<div style="width:570px" align="center" id="scroll_body" >
<fieldset style="width:100%; margin-left:10px" >
<!--<input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>&nbsp;&nbsp;&nbsp;&nbsp;-->
	<input type="button" value="Close" onClick="window_close()" style="width:100px"  class="formbutton"/>
     <div style="width:550px" id="report_container" align="center">
        <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="500" align="center">
            <thead>
                <th width="250">File Number : </th>&nbsp;<th><?php echo $dataArray[0][csf('internal_file_no')]; ?></th>
            </thead>
        </table>
        <?php
			$ls_sql="select a.export_lc_no, a.expiry_date from com_export_lc a, com_btb_lc_master_details b, com_btb_export_lc_attachment c where a.id=c.lc_sc_id and c.import_mst_id=b.id and  b.id=$lc_no and a.status_active=1 and a.is_deleted=0 and c.is_lc_sc=0";
			 
			$result_ls_sql=sql_select($ls_sql);
			if(count($result_ls_sql)>0)
			{
		?>
        <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="500" align="center">
            <thead>
                <th width="40">SL</th>
                <th width="200">LC Number</th>
                <th width="200">Expiry Date</th>                    
            </thead>
            <tbody>
            <?php
				$i=1;
				foreach( $result_ls_sql as $row)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
                    <tr bgcolor="<?php echo $bgcolor ; ?>">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row[csf("export_lc_no")]; ?></td>
                        <td><?php echo change_date_format($row[csf("expiry_date")]); ?></td>
                    </tr>
                <?php
				}
            ?>
            </tbody>
        </table>
        <?php
			}
		?>
        <br />
		<?php
			$sc_sql="select a.contract_no, a.expiry_date from com_sales_contract a, com_btb_lc_master_details b, com_btb_export_lc_attachment c where a.id=c.lc_sc_id and c.import_mst_id=b.id and  b.id=$lc_no and a.status_active=1 and a.is_deleted=0 and c.is_lc_sc =1";
			$result_sc_sql=sql_select($sc_sql);
			if(count($result_sc_sql)>0)
			{
		?>
        <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="500" align="center">
            <thead>
                <th width="40">SL</th>
                <th width="200">LC Number</th>
                <th width="200">Expiry Date</th>                    
            </thead>
            <tbody>
            <?php
				$i=1;
				foreach( $result_sc_sql as $row)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
                    <tr bgcolor="<?php echo $bgcolor ; ?>">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row[csf("contract_no")]; ?></td>
                        <td><?php echo change_date_format($row[csf("expiry_date")]); ?></td>
                    </tr>
                <?php
				}
            ?>
            </tbody>
        </table>
        <?php
			}
		?>
       </div>
</fieldset>

</div>
<?php	
	
}
?>