<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_name=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$buyer_list=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
//--------------------------------------------------------------------------------------------------------------------

if($action=="BatchNumberShow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);

?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }

</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
$sql="select id,batch_no,batch_for,booking_no,color_id,batch_weight from pro_batch_create_mst where company_id=$company_id and is_deleted = 0 GROUP BY batch_no; ";	

$arr=array(1=>$color_library);
	
	echo  create_list_view("list_view", "Batch no,Color,Booking no, Batch for,Batch weight ", "100,100,100,100,170","620","350",0, $sql, "js_set_value", "batch_no,batch_no", "", 1, "0,color_id,0,0,0", $arr , "batch_no,color_id,booking_no,batch_for,batch_weight", "employee_info_controller",'setFilterGrid("list_view",-1);','0') ;


	disconnect($con);
	exit();
}//BatchNumberShow;


if($action=="load_drop_down_location")
{ 
	
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
    echo create_drop_down( "cbo_buyer_name", 150, "select id,buyer_name from lib_buyer where  FIND_IN_SET ($data,tag_company) and status_active=1 and is_deleted=0 order by short_name","id,buyer_name", 1, "-- Select Buyer --", 0, "" );

	disconnect($con);
	exit();
}//cbo_buyer_name_td





if($action=="JobNumberShow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);

?>
<script type="text/javascript">
  function js_set_value(id)
	  {
		document.getElementById('selected_id').value=id;
		  parent.emailwindow.hide();
	  }

</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php


$cbo_buyer_name=($cbo_buyer_name==0)?"%%" : "%$cbo_buyer_name%";
//$sql="select id,job_no,buyer_name,style_ref_no,gmts_item_id from wo_po_details_master where company_name=$company_id and buyer_name like '%$cbo_buyer_name%' and is_deleted = 0 ORDER BY job_no; ";	


$sql="select a.id,a.job_no,a.buyer_name,a.style_ref_no,a.gmts_item_id,b.po_number from wo_po_details_master a,wo_po_break_down b where b.job_no_mst=a.job_no and a.company_name=$company_id and a.buyer_name like '%$cbo_buyer_name%' and a.is_deleted = 0 GROUP BY a.job_no; ";	



$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
//$arr=array(0=>$buyer,3=>$garments_item);
	
	//echo  create_list_view("list_view", "Job no,Buyer,Style,Item Name", "100,100,200,170","620","350",0, $sql, "js_set_value", "job_no,job_no", "", 1, "buyer_name,0,0,0", $arr , "job_no,buyer_name,style_ref_no,gmts_item_id", "employee_info_controller",'setFilterGrid("list_view",-1);','0') ;
?>
<table width="690" border="1" rules="all" class="rpt_table">
	<thead>
        <tr>
            <th width="35">SL</th>
            <th width="100">Po number</th>
            <th width="100">Job no</th>
            <th width="100">Buyer</th>
            <th width="180">Style</th>
            <th>Item Name</th>
        </tr>
   </thead>
</table>
<div style="max-height:380px; overflow:auto;">
<table id="table_body2" width="690" border="1" rules="all" class="rpt_table">
 <?php $rows=sql_select($sql);
	 $i=1;
 foreach($rows as $data){
  ?>
	<tr onclick="js_set_value('<?php echo $data[job_no]; ?>')" style="cursor:pointer;">
		<td width="35"><?php echo $i; ?></td>
		<td width="100"><?php echo $data[po_number]; ?></td>
		<td width="100"><?php echo $data[job_no]; ?></td>
		<td width="100"><?php echo $buyer[$data[buyer_name]]; ?></td>
		<td width="180"><?php echo $data[style_ref_no]; ?></td>
		<td><p><?php 
		$itemid=explode(",",$data[gmts_item_id]);
		foreach($itemid as $index=>$id){
			echo ($itemid[$index]==end($itemid))? $garments_item[$id] : $garments_item[$id].', ';
		}
		?></p></td>
	</tr>
    <?php $i++; } ?>
</table>
</div>
<script> setFilterGrid("table_body2",-1); </script>
<?php

	disconnect($con);
	exit();
}//JobNumberShow






if($action=="batch_report")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 

	$company = str_replace("'","",$cbo_company_name);
	$buyer = str_replace("'","",$cbo_buyer_name);
	$job_number = str_replace("'","",$job_number);
	$batch_number = str_replace("'","",$batch_number);
	$txt_date_from = str_replace("'","",$txt_date_from);
	$txt_date_to = str_replace("'","",$txt_date_to);


	$jobdata=($job_number)? " and c.job_no_mst='".$job_number."'" : '';
	$buyerdata=($buyer)?' and d.buyer_name='.$buyer : '';
	$batch_num=($batch_number)?" and a.batch_no='".$batch_number."'" : '';
	
if($txt_date_from && $txt_date_to){
	$date_from=change_date_format($txt_date_from,'yyyy-mm-dd');
	$date_to=change_date_format($txt_date_to,'yyyy-mm-dd');
	$dates_com="and a.batch_date BETWEEN '$date_from' AND '$date_to'";
	}
	
	$wo_po_buyer=return_library_array( "select job_no,buyer_name from wo_po_details_master", "job_no", "buyer_name"  );
	
	$composition=return_library_array( "select job_no,composition from wo_pre_cost_fabric_cost_dtls", "job_no", "composition"  );
	$construction=return_library_array( "select job_no,construction from wo_pre_cost_fabric_cost_dtls", "job_no", "construction"  );
	 
 	
if($buyer){
	$sql="
		select
			a.batch_no,
			a.batch_date,
			a.batch_weight,
			a.color_id,
			a.booking_no_id,
			SUM(b.batch_qnty) AS batch_qnty,
			b.item_description,
			group_concat( distinct c.po_number ) AS po_number,
			c.job_no_mst,
			d.job_no_prefix_num
		from 
			pro_batch_create_mst a,
			pro_batch_create_dtls b,
			wo_po_break_down c,
			wo_po_details_master d
		where 
			a.company_id=$company 
			$dates_com
			$jobdata
			$batch_num
			$buyerdata
			and a.id=b.mst_id
			and b.po_id=c.id
			and d.job_no=c.job_no_mst
			
			GROUP BY a.batch_no, b.item_description
			order by a.batch_no
		";
}else{
	
	$sql="
		select
			a.batch_no,
			a.batch_date,
			a.batch_weight,
			a.color_id,
			a.booking_no_id,
			SUM(b.batch_qnty) AS batch_qnty,
			b.item_description,
			group_concat( distinct c.po_number ) AS po_number,
			c.job_no_mst,
			d.job_no_prefix_num
		from 
			pro_batch_create_mst a,
			pro_batch_create_dtls b,
			wo_po_break_down c,
			wo_po_details_master d
		where 
			a.company_id=$company 
			$dates_com
			$jobdata
			$batch_num
			$buyerdata
			and a.id=b.mst_id
			and b.po_id=c.id
			and d.job_no=c.job_no_mst
			
			GROUP BY a.batch_no, b.item_description
			order by a.batch_no
		";
}

$batchdata=sql_select($sql);





//echo $sql;
?>
<div>
<fieldset>
 <table class="rpt_table" width="1070" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
        <thead>
            <tr>
                <th width="30">SL</th>
                <th width="40">Batch No</th>
                <th width="80">Batch Color</th>
                <th width="80">Batch Date</th>
                <th width="50">Buyer</th>
                <th width="120">PO No</th>
                <th width="90">Job Number</th>
                <th width="100">Body Part</th>
                <th width="100">Construction</th>
                <th width="100">Composition</th>
                <th width="40">Dia/ Width</th>
                <th width="30">GSM</th>
                <th width="70">Batch Item Qty.</th>
                <th width="50">Batch Weight</th>
                <th>Grey Issued</th>
            </tr>
        </thead>
</table>
<div style=" max-height:350px; overflow:auto;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1070" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 

$storbatch=0;
$sl=1;
$i=1;
$f=0;
$btq=0;
foreach($batchdata as $batch){ 

if($storbatch==0){$storbatch=$batch[csf('batch_no')]; }
if($storbatch!=$batch[csf('batch_no')]){$i++; $f=0; $storbatch=$batch[csf('batch_no')];}
$bgcolor=($i%2==0)?"#E9F3FF":"#FFF";

list($body,$color,$const_com,$gsm,$dia)=explode(",",$batch[csf('item_description')]);  
?>
            <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $sl; ?>" onclick="js_set_value(<?php echo $sl; ?>)" style="cursor:pointer;">
                <td width="30"><?php echo $sl; ?></td>
                <td style=" <?php if($f==1) echo 'color:'.$bgcolor; ?>" align="center" width="40"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
                <td style=" <?php if($f==1) echo 'color:'.$bgcolor; ?>" width="80"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
                <td style=" <?php if($f==1) echo 'color:'.$bgcolor; ?>" align="center" width="80"><p><?php echo $batch[csf('batch_date')]; ?></p></td>
                <td style=" <?php if($f==1) echo 'color:'.$bgcolor; ?>" width="50"><p><?php echo $buyer_list[$wo_po_buyer[$batch[csf('job_no_mst')]]]; ?></p></td>
                <td width="120"><p><?php echo $batch[csf('po_number')]; ?></p></td>
                <td width="90"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
                <td width="100"><p><?php echo $body; ?></p></td>
                <td width="100"><p><?php echo $construction[$batch[csf('job_no_mst')]]; ?></p></td>
                <td width="100"><p><?php echo $composition[$batch[csf('job_no_mst')]]; ?></p></td>
                <td align="right" width="40"><?php echo $dia; ?></td>
                <td align="right" width="30"><?php echo $gsm; ?></td>
                <td align="right" width="70"><?php echo $batch[csf('batch_qnty')]; $btq+=$batch[csf('batch_qnty')]; ?></td>
                <td style=" <?php if($f==1) echo 'color:'.$bgcolor; ?>" align="right" width="50"><?php echo $batch[csf('batch_weight')]; ?></td>
                <td><?php echo $batch[xxx]; ?></td>
            </tr>

<?php 


$storbatch=$batch[csf('batch_no')];
$f=1;


$sl++;

} //batchdata froeach

 ?>
       </tbody>
       
       
       
</table>
 <table class="rpt_table" width="1070" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
        <tfoot>
            <tr>
                <th width="30">&nbsp;</th>
                <th width="40">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="50">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="90">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="40">&nbsp;</th>
                <th width="30">&nbsp;</th>
                <th width="70" id="btg"><?php echo $btq; ?></th>
                <th width="50">&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </tfoot>
</table>







</div>
</fieldset>
</div>
<?php
	disconnect($con);
	exit();

}//BatchReport






?>