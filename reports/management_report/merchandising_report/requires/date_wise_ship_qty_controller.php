<?php
error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id=$_SESSION['logic_erp']['user_id'];
//--------------------------------------------------------------------------------------------------------------------
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_library=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$color_library=return_library_array( "select id,color_name from lib_color",'id','color_name');
$ord_qty_arr=return_library_array( "select id,po_quantity from wo_po_break_down",'id','po_quantity');


if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 120, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}



//report_generate_order
if($action=="report_generate_order")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	//echo $cbo_year_from;die;
	$company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	//echo $cbo_buyer_name;die;
	if($cbo_buyer_name!=0) $buyer_cond=" and a.buyer_name='$cbo_buyer_name'"; else $buyer_cond="";
	$date_cond='';
	if(str_replace("'","",$cbo_year_from)!=0 && str_replace("'","",$cbo_month_from)!=0)
	{
		$start_year=str_replace("'","",$cbo_year_from);
		$start_month=str_replace("'","",$cbo_month_from);
		$start_date=$start_year."-".$start_month."-01";
		
		$end_year=str_replace("'","",$cbo_year_to);
		$end_month=str_replace("'","",$cbo_month_to);
		$num_days = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);
		$end_date=$end_year."-".$end_month."-$num_days";
		if($db_type==0) 
		{
			$date_cond=" and b.country_ship_date between '$start_date' and '$end_date'";
			$order_by_cond="DATE_FORMAT(b.country_ship_date, '%Y%m')";
			
		}
		if($db_type==2) 
		{
			$date_cond=" and b.country_ship_date between '".date("j-M-Y",strtotime(str_replace("'","",$start_date)))."' and '".date("j-M-Y",strtotime(str_replace("'","",$end_date)))."'";
			$order_by_cond="to_char(b.country_ship_date,'YYYY-MM')";
		}
	}
	$sql_color_no_arr=return_library_array("select po_break_down_id, count(distinct color_number_id) as color_id from wo_po_color_size_breakdown group by po_break_down_id","po_break_down_id","color_id");
	$body_part_arr=return_library_array("select c.id,a.construction from wo_pre_cost_fabric_cost_dtls a ,wo_po_details_master b,  wo_po_break_down c where a.job_no=b.job_no and b.job_no=c.job_no_mst and a.body_part_id in(1,20)","id","construction");
	//var_dump($body_part_arr);die;
	$weak_of_year=return_library_array( "select week_date,week from  week_of_year",'week_date','week');
	
	$data_array="select
					 a.id as job_id,
					 a.job_no,
					 a.style_ref_no,
					 a.company_name, 
					 a.buyer_name,
					 a.ship_mode,
					 a.total_set_qnty, 
					 b.id as color_break_id, 
					 b.po_break_down_id, 
					 b.order_quantity as po_quantity_pcs, 
					 b.country_ship_date, 
					 b.order_total,
					 c.is_confirmed,
					 c.po_number,
					 c.po_quantity,
					 c.po_received_date
			from 
					wo_po_details_master a, 
					wo_po_color_size_breakdown b,
					wo_po_break_down c 
			where  
					a.job_no=b.job_no_mst
					and b.po_break_down_id=c.id   
					and a.company_name like '$company_name' 
					$date_cond 
					$buyer_cond 
					and a.status_active=1 
					and b.status_active=1
					and c.status_active=1
					
			order by 
					 $order_by_cond,c.is_confirmed ASC";
		
	//echo $data_array;die;
	$result_po=sql_select($data_array);
	$row_data_dtls=array();
	$tmp_arr=array();
	//$num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		foreach($result_po as $row)
		{
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["po_qty"]+=$row[csf("po_quantity_pcs")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["buyer"] =$row[csf("buyer_name")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["is_confirmed"] =$row[csf("is_confirmed")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["job_no"] =$row[csf("job_no")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["po_break_down_id"] =$row[csf("po_break_down_id")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["po_number"] =$row[csf("po_number")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["style_ref_no"] =$row[csf("style_ref_no")];
			//$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["po_quantity"] =$row[csf("po_quantity")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["ship_mode"] =$row[csf("ship_mode")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]][$row[csf("po_break_down_id")]]["po_received_date"] =$row[csf("po_received_date")];
			
			if($row[csf("is_confirmed")]==1)
			{
				$row_data_dtls[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("po_break_down_id")]][date("Y-m-d",strtotime($row[csf("country_ship_date")]))]+=$row[csf("po_quantity_pcs")];
			}
			else
			{
				$row_data_dtls_project[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("po_break_down_id")]][date("Y-m-d",strtotime($row[csf("country_ship_date")]))]+=$row[csf("po_quantity_pcs")];
			}
			
			/*$po_total_price+= $row[csf("po_total_price")];
			$quantity_tot+=$row[csf("po_quantity_pcs")];
			
			if( $job_smv_arr[$row[csf("job_no")]] !=0)
			{
				$booked_basic_qnty=($row[csf("po_quantity")]*($job_smv_arr[$row[csf("job_no")]]))/$basic_smv_arr[$row[csf("company_name")]];
				$row_data_dtls[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]]['boking_basic_qty']+=$booked_basic_qnty;
				$booked_basic_qnty_tot+=$booked_basic_qnty;
			}*/
		}
		
		//var_dump($tmp_arr);die;
		ob_start();
?>
	
    
        <table width="1570">
            <tr class="form_caption">
                <td colspan="37" align="center" style="font-size:16px;">Date Wise Ship Quantity Report</td>
            </tr>
            <tr class="form_caption">
                <td colspan="37" align="center" style="font-size:16px;"><? echo $company_library[$company_name]; ?></td>
            </tr>
        </table>
        
        <?php
		foreach($tmp_arr as $date_key=>$buyer_id_arr)
		{
            $month_value=explode("-",$date_key);
			$num_days = cal_days_in_month(CAL_GREGORIAN, $month_value[1], $month_value[0]);
			//echo $num_days;die;
			//arsort($buyer_id_arr);
			//var_dump($date_key);die;
				?>
                Month: <?php echo $months[$month_value[1]*1]."-".$month_value[0]; 
				?><br />
                <?php
				foreach($buyer_id_arr as $buyer_id=>$is_confirm_arr)
				{
					$confirm_project_buyer_total=array();
					?>
                    Buyer : <?php echo $buyer_library[$buyer_id]; ?><br />
                    <?php
					foreach($is_confirm_arr as $is_confirm_id=>$order_id_arr)
					{
						if($is_confirm_id==1)
						{
							echo "Confirm Order :";
							?>
							<br />
							<table  width="1570" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                                <thead> 
                                    <tr >
                                        <th width="80">Job No</th>
                                        <th width="80">Order No</th>
                                        <th width="80">Style Ref.</th>
                                        <th width="80">Febric (body)</th>
                                        <th width="50">No of Color</th>
                                        <th width="70">PO Receive Date</th>
                                        <th width="50">Week</th>
                                        <th width="70">Order Qty</th>
                                        <th width="70">C.Month Total</th>
                                        <th width="60">Ship mode</th>
                                        <?php
                                        for($m=1;$m<=$num_days;$m++)
                                        {
											if($m==$num_days)
											{
												?>
												<th ><?php echo  ($m<=9)? '0'.$m:$m; ?></th>
												<?php
											}
											else
											{
												?>
												<th width="27"><?php echo  ($m<=9)? '0'.$m:$m; ?></th>
												<?php
											}
                                        }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                //($m<=9)? '0'.$m:$m.$month_value[1].$month_value[0];
                                $k=1;$total_val="";$total_buyer_qty="";$confirm_total_qty=array();
                                foreach($order_id_arr as $order_id=>$row)
                                {
									if ($k%2==0)
									$bgcolor="#E9F3FF";
									else
									$bgcolor="#FFFFFF";
									//$confirm_total_qty =array();
                                    ?>
                                    <tr bgcolor="<? echo $bgcolor; ?>">
                                        <td><p><? echo $row["job_no"];?></p></td>
                                        <td><p><? echo $row["po_number"];?></p></td>
                                        <td><p><? echo $row["style_ref_no"];?></p></td>
                                        <td><p>
										<?php
										//$job="'".$row["job_no"]."'";
										 echo $body_part_arr[$row["po_break_down_id"]]; 
										 ?></p>
                                        </td>
                                        <td align="center"><p><?php echo $sql_color_no_arr[$row["po_break_down_id"]]; ?></p></td>
                                        <td align="center"><p><?php if($row["po_received_date"]!="" && $row["po_received_date"]!='0000-00-00') echo change_date_format($row["po_received_date"]); ?></p></td>
                                        <td align="center"><p><?php if($row["po_received_date"]!="" && $row["po_received_date"]!='0000-00-00') echo $weak_of_year[$row["po_received_date"]]; ?></p></td>
                                        <td align="right"><p><?php echo number_format($ord_qty_arr[$order_id],0); $total_ord_qty +=$ord_qty_arr[$order_id]; ?></p></td>
                                        <td align="right"><p><?php echo number_format($row["po_qty"],0); $total_month_qty +=$row["po_qty"]; ?></p></td>
                                        <td><p><? echo $shipment_mode[$row["ship_mode"]]; ?></p></td>
                                       	<?php
                                            for($p=1;$p<=$num_days;$p++)
                                            {
												$day=($p<=9)? '0'.$p:$p;
												?>
												<td style="height:60px;"  align="center"  valign="bottom"><div class="verticalText2"><?php echo  $row_data_dtls[$date_key][$buyer_id][$order_id][$date_key."-".$day]; ?></div></td>
												<?php
												$confirm_total_qty[$p] +=$row_data_dtls[$date_key][$buyer_id][$order_id][$date_key."-".$day];
												$confirm_project_buyer_total[$buyer_id][$p] +=$row_data_dtls[$date_key][$buyer_id][$order_id][$date_key."-".$day];
                                            }
                                         ?>
                                    </tr>
                                    <?php
									$k++;
									?>
                                    
                                    
                                    <?php
									
                                }
                                ?>
                                	<tr bgcolor="#CCCCCC">
                                        <td colspan="7"  align="right" style="font-weight:bold;">Confirm Total:</td>
                                        <td  align="right" style="font-weight:bold;"><p><?php echo number_format($total_ord_qty,0); $buyer_total_order [$buyer_id] +=$total_ord_qty;  $total_ord_qty=''; ?></p></td>
                                        <td align="right" style="font-weight:bold;"><p><?php echo number_format($total_month_qty,0);  $buyer_total_month [$buyer_id] +=$total_month_qty;  $total_month_qty=''; ?></p></td>
                                        <td align="right" style="font-weight:bold;">&nbsp;</td>
                                        <?php
                                        for($p=1;$p<=$num_days;$p++)
                                        {
                                            $day=($p<=9)? '0'.$p:$p;
                                            //echo $day;
                                            ?>
                                            <td  style="height:60px;"  align="center"  valign="bottom"><div class="verticalText2"><?php  echo number_format($confirm_total_qty[$p],0); ?></div></td>
                                           
                                            <?php
                                            //$total_val[]
                                        }
                                        ?>
                                	</tr>
                                </tbody>
							</table>
							<?php
						}
						//project start here
						else if($is_confirm_id==2)
						{
							//
							echo "Projected :";
							?>
							<br />
							<table  width="1570" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                                <thead> 
                                    <tr>
                                        <th width="80">Job No</th>
                                        <th width="80">Order No</th>
                                        <th width="80">Style Ref.</th>
                                        <th width="80">Febric (body)</th>
                                        <th width="50">No of Color</th>
                                        <th width="70">PO Receive Date</th>
                                        <th width="50">Week</th>
                                        <th width="70">Order Qty</th>
                                        <th width="70">C.Month Total</th>
                                        <th width="60">Ship mode</th>
                                        <?php
                                        for($m=1;$m<=$num_days;$m++)
                                        {
											if($m==$num_days)
											{
												?>
												<th ><?php echo  ($m<=9)? '0'.$m:$m; 
												?></th>
												<?php
											}
											else
											{
												?>
												<th width="27"><?php echo  ($m<=9)? '0'.$m:$m; ?></th>
												<?php
											}
                                        }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                //($m<=9)? '0'.$m:$m.$month_value[1].$month_value[0];
                                $k=1;$total_val="";$total_buyer_qty="";$confirm_total_qty_project=array();
                                foreach($order_id_arr as $order_id=>$row)
                                {
									if ($k%2==0)
									$bgcolor="#E9F3FF";
									else
									$bgcolor="#FFFFFF";
									//$confirm_total_qty =array();
                                    ?>
                                    <tr bgcolor="<?php echo $bgcolor; ?>">
                                        <td><p><?php echo $row["job_no"];?></p></td>
                                        <td><p><?php echo $row["po_number"];?></p></td>
                                        <td><p><?php echo $row["style_ref_no"];?></p></td>
                                        <td>
										<p><?php
										echo $body_part_arr[$row["po_break_down_id"]]; 
										 ?></p>
                                        </td>
                                        <td align="center"><p><?php echo $sql_color_no_arr[$row["po_break_down_id"]]; ?></p></td>
                                        <td align="center"><p><?php if($row["po_received_date"]!="" && $row["po_received_date"]!='0000-00-00') echo change_date_format($row["po_received_date"]); ?></p></td>
                                        <td align="center"><p><?php if($row["po_received_date"]!="" && $row["po_received_date"]!='0000-00-00') echo $weak_of_year[$row["po_received_date"]]; ?></p></td>
                                        <td align="right"><p><?php echo number_format($ord_qty_arr[$order_id],2); $total_ord_proj_qty +=$ord_qty_arr[$order_id]; ?></p></td>
                                        <td align="right"><p><?php echo number_format($row["po_qty"],0); $total_month_proj_qty +=$row["po_qty"]; ?></p></td>
                                        <td><p><? echo $shipment_mode[$row["ship_mode"]]; ?></p></td>
                                        <?php
                                            for($p=1;$p<=$num_days;$p++)
                                            {
												$day=($p<=9)? '0'.$p:$p;
												?>
												<td style="height:60px;"  align="center"  valign="bottom"><div class="verticalText2"><?php echo  $row_data_dtls_project[$date_key][$buyer_id][$order_id][$date_key."-".$day]; ?></div></td>
												<?php
												$confirm_total_qty_project[$p] +=$row_data_dtls_project[$date_key][$buyer_id][$order_id][$date_key."-".$day];
												$confirm_project_buyer_total[$buyer_id][$p] +=$row_data_dtls_project[$date_key][$buyer_id][$order_id][$date_key."-".$day];
                                            }
                                         ?>
                                    </tr>
                                    <?php
									$k++;
									
                                }
                                ?>
                                <tr bgcolor="#CCCCCC">
                                    <td colspan="7"  align="right" style="font-weight:bold;">Projected Total:</td>
                                    <td  align="right" style="font-weight:bold;"><p><?php echo number_format($total_ord_proj_qty,0); $total_ord_qty=''; $buyer_total_order [$buyer_id] +=$total_ord_proj_qty;  $total_ord_proj_qty=''; ?></p></td>
                                    <td align="right" style="font-weight:bold;"><p><?php echo number_format($total_month_proj_qty,0);  $buyer_total_month [$buyer_id] +=$total_month_proj_qty; $total_month_proj_qty=''; ?></p></td>
                                    <td align="right" style="font-weight:bold;">&nbsp;</td>
                                    <?php
                                    for($p=1;$p<=$num_days;$p++)
                                    {
                                        $day=($p<=9)? '0'.$p:$p;
                                        //echo $day;
                                        ?>
                                        <td  style="height:60px;"  align="center"  valign="bottom"><div class="verticalText2"><?php echo number_format($confirm_total_qty_project[$p],0); ?></div></td>
                                       
                                        <?php
                                        //$total_val[]
                                    }
                                    ?>
                                </tr>
                                </tbody>
							</table>
							<?php
						
						}
						
					}
					?>
					<table  width="1570" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                        <tr bgcolor="#FFFFCC">
                        	<td width="80">&nbsp;</td>
                            <td width="80">&nbsp;</td>
                            <td width="80">&nbsp;</td>
                            <td width="80">&nbsp;</td>
                            <td width="50">&nbsp;</td>
                            <td width="70">&nbsp;</td>
                            <td  align="right" style="font-weight:bold;" width="50"><p>Buyer</p><p>Total:</p></td>
                            <td  align="right" style="font-weight:bold;" width="70"><p><?php echo number_format($buyer_total_order [$buyer_id],0); ?></p></td>
                            <td align="right" style="font-weight:bold;" width="70"><p><?php echo number_format($buyer_total_month [$buyer_id],0); ?></p></td>
                            <td align="right" style="font-weight:bold;" width="60">&nbsp;</td>
							<?php
                            for($m=1;$m<=$num_days;$m++)
                            {
								if($m==$num_days)
								{
									?>
									<td style="height:60px;"  align="center"  valign="bottom"><div class="verticalText2"><?php echo number_format($confirm_project_buyer_total[$buyer_id][$m],0);  ?></div></td>
									<?php
								}
								else
								{
									?>
									<td  style="height:60px;"  align="center"  valign="bottom"  width="27"><div class="verticalText2"><? echo number_format($confirm_project_buyer_total[$buyer_id][$m],0);  ?></div></td>
									<?php
								}
                            }
                            ?>
                            
                        </tr>
                    </table>
                    <?php
					
					//buyer loop end
				}

		}
		?>
    
<?php
	 
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data****$filename";
	exit();
	disconnect($con);
}
if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	//echo $cbo_year_from;die;

	$company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	//echo $cbo_buyer_name;die;
	if($cbo_buyer_name!=0) $buyer_cond=" and a.buyer_name='$cbo_buyer_name'"; else $buyer_cond="";
	$date_cond='';
	if(str_replace("'","",$cbo_year_from)!=0 && str_replace("'","",$cbo_month_from)!=0)
	{
		$start_year=str_replace("'","",$cbo_year_from);
		$start_month=str_replace("'","",$cbo_month_from);
		$start_date=$start_year."-".$start_month."-01";
		
		$end_year=str_replace("'","",$cbo_year_to);
		$end_month=str_replace("'","",$cbo_month_to);
		$num_days = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);
		$end_date=$end_year."-".$end_month."-$num_days";
		if($db_type==0) 
		{
			$date_cond=" and b.country_ship_date between '$start_date' and '$end_date'";
			$order_by_cond="DATE_FORMAT(b.country_ship_date, '%Y%m')";
			
		}
		if($db_type==2) 
		{
			$date_cond=" and b.country_ship_date between '".date("j-M-Y",strtotime(str_replace("'","",$start_date)))."' and '".date("j-M-Y",strtotime(str_replace("'","",$end_date)))."'";
			$order_by_cond="to_char(b.country_ship_date,'YYYY-MM')";
		}
	}
	$data_array="select
					 a.id as job_id,
					 a.job_no, 
					 a.company_name, 
					 a.buyer_name,
					 a.total_set_qnty, 
					 b.id as color_break_id, 
					 b.po_break_down_id, 
					 b.order_quantity as po_quantity, 
					 b.order_quantity as po_quantity_pcs, 
					 b.country_ship_date, 
					 b.order_total,
					 c.is_confirmed
			from 
					wo_po_details_master a, 
					wo_po_color_size_breakdown b,
					wo_po_break_down c 
			where  
					a.job_no=b.job_no_mst
					and b.po_break_down_id=c.id   
					and a.company_name like '$company_name' 
					$date_cond
					$buyer_cond  
					and a.status_active=1 
					and b.status_active=1 
					and c.status_active=1 
			order by 
					 $order_by_cond ASC";
		
	//echo $data_array;die;
	$result_po=sql_select($data_array);
	$row_data_dtls=array();
	$tmp_arr=array();
	//$num_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		foreach($result_po as $row)
		{
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]]["po_qty"]+=$row[csf("po_quantity_pcs")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]]["buyer"] =$row[csf("buyer_name")];
			$tmp_arr[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][$row[csf("is_confirmed")]]["is_confirmed"] =$row[csf("is_confirmed")];
			$month_wise_total[date("Y-m",strtotime($row[csf("country_ship_date")]))] +=$row[csf("po_quantity_pcs")];
			if($row[csf("is_confirmed")]==1)
			{
				$row_data_dtls[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][date("Y-m-d",strtotime($row[csf("country_ship_date")]))]+=$row[csf("po_quantity_pcs")];
			}
			else
			{
				$row_data_dtls_project[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]][date("Y-m-d",strtotime($row[csf("country_ship_date")]))]+=$row[csf("po_quantity_pcs")];
			}
			
			/*$po_total_price+= $row[csf("po_total_price")];
			$quantity_tot+=$row[csf("po_quantity_pcs")];
			
			if( $job_smv_arr[$row[csf("job_no")]] !=0)
			{
				$booked_basic_qnty=($row[csf("po_quantity")]*($job_smv_arr[$row[csf("job_no")]]))/$basic_smv_arr[$row[csf("company_name")]];
				$row_data_dtls[date("Y-m",strtotime($row[csf("country_ship_date")]))][$row[csf("buyer_name")]]['boking_basic_qty']+=$booked_basic_qnty;
				$booked_basic_qnty_tot+=$booked_basic_qnty;
			}*/
		}
		//var_dump($tmp_arr);die;
		ob_start();
?>
	
        <table width="1150">
            <tr class="form_caption">
                <td colspan="32" align="center" style="font-size:16px;">Date Wise Ship Quantity Report</td>
            </tr>
            <tr class="form_caption">
                <td colspan="32" align="center" style="font-size:16px;"><? echo $company_library[$company_name]; ?></td>
            </tr>
        </table>
        
        <?php
		foreach($tmp_arr as $date_key=>$buyer_id_arr)
		{
            $month_value=explode("-",$date_key);
			$num_days = cal_days_in_month(CAL_GREGORIAN, $month_value[1], $month_value[0]);
			//echo $num_days;die;
			//arsort($buyer_id_arr);
			//var_dump($date_key);die;
				?>
                <br />
                <table class="rpt_table" width="1150" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <tr bgcolor="#FFFFFF">
                        <td align="right" style="font-weight:bold;" width="84" >Month:</td>
                        <td style="font-weight:bold;" width="85">
                        <?php
                        echo $months[$month_value[1]*1]."-".$month_value[0]; 
                        ?>
                        </td>
                        <td  style="font-weight:bold;" width="50" align="right">Total &nbsp; =</td>
                        <td  style="font-weight:bold;" colspan="29">&nbsp;
                        <?php 
                            echo number_format($month_wise_total[$date_key],0)." "."Pcs"; 
							
                        ?>
                        </td>
                    </tr>
                </table>
               <table  width="1150" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
                   <thead> 
                        <tr >
                            <th width="80">Buyer</th>
                            <th width="80">Total</th>
                            <?php
                            for($m=1;$m<=$num_days;$m++)
                            {
                                ?>
                                <th width="30"><? echo  ($m<=9)? '0'.$m:$m;
								
								?></th>
                                <?php
                            }
							
                            ?>
                        </tr>
                    </thead>
                    <tbody>
					<?php
                    //($m<=9)? '0'.$m:$m.$month_value[1].$month_value[0];
					$k=1;$total_val="";$total_buyer_qty="";
                    foreach($buyer_id_arr as $buyer_id=>$is_confirm)
                    {
						foreach($is_confirm as $row)
						{
							if ($k%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							if($row['is_confirmed']==1)
							{
							?>
							<tr bgcolor="<?php echo $bgcolor ; ?>">
								<td ><?php echo $buyer_library[$row['buyer']]; ?></td>
								<td align="right"><?php echo number_format($row['po_qty'],0); $total_buyer_qty+=$row['po_qty']; ?>&nbsp;</td>
								<?php
								for($p=1;$p<=$num_days;$p++)
								{
									$day=($p<=9)? '0'.$p:$p;
									//echo $day;
									?>
									<td  style="height:60px;" align="center" valign="bottom"><div class="verticalText2" ><?php if($row_data_dtls[$date_key][$buyer_id][$date_key."-".$day]>0)echo number_format($row_data_dtls[$date_key][$buyer_id][$date_key."-".$day],0); else echo "0"; ?></div></td>
									<?php
									$total_val[$day] +=$row_data_dtls[$date_key][$buyer_id][$date_key."-".$day];
								}
								
								?>
							</tr>
							<?php
							}
							else
							{
								?>
								<tr bgcolor="<?php echo $bgcolor ; ?>">
									<td ><?php echo $buyer_library[$row['buyer']]."(bk)"; ?></td>
									<td  align="right"><? echo number_format($row['po_qty'],0); $total_buyer_qty+=$row['po_qty']; ?>&nbsp;</td>
									<?php
									for($p=1;$p<=$num_days;$p++)
									{
										$day=($p<=9)? '0'.$p:$p;
										//echo $day;
										?>
										<td  style="height:60px;" align="center"  valign="bottom"><div class="verticalText2"><?php  if($row_data_dtls_project[$date_key][$buyer_id][$date_key."-".$day] >0) echo number_format($row_data_dtls_project[$date_key][$buyer_id][$date_key."-".$day],0); else echo "0";  ?></div></td>
										<?php
										$total_val[$day] +=$row_data_dtls_project[$date_key][$buyer_id][$date_key."-".$day];
									}
									
									?>
								</tr>
								<?php	
							}
							$k++;//font-weight:bold; 
						}
                    }
					?>
                    	<tr bgcolor="#CCCCCC">
                            <td  align="right" style="font-weight:bold;">Total:</td>
                            <td  align="right" style="font-weight:bold;"><? echo number_format($total_buyer_qty,0); ?>&nbsp;</td>
                            <?php
                            for($p=1;$p<=$num_days;$p++)
                            {
								$day=($p<=9)? '0'.$p:$p;
								//echo $day;
                                ?>
                                <td  style="height:60px;"  align="center"  valign="bottom"><div class="verticalText2"><?php  echo number_format($total_val[$day],0); ?></div></td>
                                <?php
								//$total_val[]
                            }
                            ?>
                        </tr>
                    </tbody>
             </table>
             <?php
		}
	
	
	
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data****$filename";
	exit();
	disconnect($con);
}
?>