<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------

if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th>Supplier</th>
                    <th>Search By</th>
                    <th align="center" id="search_by_td_up">Enter MRR Number</th>
                    <th>Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr class="general">
                    <td>
                        <?php  
 							 echo create_drop_down( "cbo_supplier", 150, "select c.supplier_name,c.id from lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and b.party_type=2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td>
                        <?php  
                            $search_by = array(1=>'MRR No',2=>'Challan No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 120, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'yarn_receive_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" style="margin-top:10px" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for mrr
		{
			$sql_cond .= " and a.recv_number LIKE '%$txt_search_common'";	
		}
		else if(trim($txt_search_by)==2) // for chllan no
		{
			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";				
 		}		 
 	} 
	
	if( $txt_date_from!="" && $txt_date_to!="" ) 
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	if(trim($supplier)!=0) $sql_cond .= " and a.supplier_id='$supplier'";
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql = "select a.recv_number_prefix_num,a.recv_number,$year_field a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis,sum(b.cons_quantity) as receive_qnty from inv_transaction b, inv_receive_master a left join com_btb_lc_master_details c on a.lc_no=c.id where a.id=b.mst_id and a.entry_form in(1,9) and a.status_active=1 $sql_cond group by b.mst_id, a.recv_number_prefix_num ,a.recv_number,a.supplier_id,a.challan_no,a.receive_date,a.receive_basis,a.insert_date,c.lc_number order by b.mst_id";
	//echo $sql;
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(2=>$supplier_arr,6=>$receive_basis_arr);
	echo create_list_view("list_view", "MRR No, Year, Supplier Name, Challan No, LC No, Receive Date, Receive Basis, Receive Qnty","70,60,140,110,120,100,100,100","900","220",0, $sql , "js_set_value", "recv_number", "", 1, "0,0,supplier_id,0,0,0,receive_basis,0", $arr, "recv_number_prefix_num,year,supplier_id,challan_no,lc_number,receive_date,receive_basis,receive_qnty", "",'','0,0,0,0,0,3,0,1') ;	
	exit();
	
}

if($action=="populate_data_from_data")
{
	$sql = "select id,recv_number,entry_form,company_id,receive_basis,receive_purpose,receive_date,challan_no,store_id,lc_no,supplier_id,exchange_rate,currency_id,lc_no,source 
			from inv_receive_master 
			where recv_number='$data' and entry_form in(1,9)";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#txt_received_id').val('".$row[csf("id")]."');\n";
		echo "$('#txt_mrr_no').val('".$row[csf("recv_number")]."');\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
 		echo "$('#cbo_return_to').val('".$row[csf("supplier_id")]."');\n";
		
		if($row[csf("entry_form")]==9)
		{
			echo "$('#txt_pi_no').removeAttr('disabled','disabled');\n";
		}
		else
		{
			echo "$('#txt_pi_no').attr('disabled','disabled');\n";
		}
		
		//right side list view
		echo"show_list_view('".$row[csf("recv_number")]."','show_product_listview','list_product_container','requires/yarn_receive_return_controller','');\n";
   	}	
	exit();	
}



//right side product list create here--------------------//
if($action=="show_product_listview")
{ 
 	$mrr_no = $data;
	$sql = "select c.product_name_details,c.current_stock,a.id as mrr_id,b.id as tr_id, c.id as prod_id,b.cons_quantity,b.balance_qnty
	from inv_receive_master a, inv_transaction b, product_details_master c 
	where a.id=b.mst_id and b.prod_id=c.id and a.recv_number='$mrr_no' and b.transaction_type in(1,4) and b.item_category=1";
  	$result = sql_select($sql);
	$i=1; 
 	?>
    	<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all">
        	<thead><tr><th>SL</th><th>Product Name</th><th>Curr.Stock</th></tr></thead>
            <tbody>
            	<?php foreach($result as $row){ 
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";  
 				?>
                 	 
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("tr_id")];?>","item_details_form_input","requires/yarn_receive_return_controller")' style="cursor:pointer" >
                		<td><?php echo $i; ?></td>
                    	<td><?php echo $row[csf("product_name_details")]; ?></td>
                        <td align="right"><?php echo $row[csf("balance_qnty")]; ?></td>
                    </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
     </fieldset>   
	<?php	 
	exit();
}



//child form data input here-----------------------------//
if($action=="item_details_form_input")
{
	$sql = "select b.id as prod_id, b.product_name_details, a.store_id, a.order_rate, a.order_ile_cost, a.cons_uom, a.cons_rate, a.cons_quantity, a.cons_amount, a.balance_qnty, a.balance_amount
			from inv_transaction a, product_details_master b
 			where a.id=$data and a.status_active=1 and a.item_category=1 and transaction_type in(1,4) and a.prod_id=b.id and b.status_active=1";
 	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		
 		echo "$('#txt_item_description').val('".$row[csf("product_name_details")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#cbo_store_name').val('".$row[csf("store_id")]."');\n";
		echo "$('#txt_return_qnty').val('');\n";
		echo "$('#txt_receive_qnty').val('".$row[csf("balance_qnty")]."');\n";
		echo "$('#cbo_uom').val('".$row[csf("cons_uom")]."');\n";
		echo "$('#txt_rate').val('".$row[csf("cons_rate")]."');\n";
		echo "$('#txt_return_value').val('');\n"; 
		
		echo "$('#order_rate').val('".$row[csf("order_rate")]."');\n";
		echo "$('#order_ile_cost').val('".$row[csf("order_ile_cost")]."');\n";
		
	}
	exit();		
}




//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
 		
		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_issue_master a, inv_transaction b","a.id=b.mst_id and a.issue_number=$txt_return_no and b.prod_id=$txt_prod_id and b.transaction_type=3"); 
		if($duplicate==1) 
		{
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			check_table_status( $_SESSION['menu_id'], 0 );
			die;
		}
		//------------------------------Check Brand END---------------------------------------//
		
		
 		if(str_replace("'","",$txt_return_no)!="")
		{
			$new_return_number[0] = str_replace("'","",$txt_return_no);
			$id=return_field_value("id","inv_issue_master","issue_number=$txt_return_no");
			//yarn master table UPDATE here START----------------------//		
 			$field_array_mst="entry_form*company_id*supplier_id*issue_date*received_id*received_id*pi_id*updated_by*update_date";
			$data_array_mst="8*".$cbo_company_name."*".$cbo_return_to."*".$txt_return_date."*".$txt_received_id."*".$pi_id."*".$txt_mrr_no."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"id",$id,1);	
			//yarn master table UPDATE here END---------------------------------------// 
		}
		else  	
		{	 
			//yarn master table entry here START---------------------------------------//		
			$id=return_next_id("id", "inv_issue_master", 1);
			
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
					
			$new_return_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'YRR', date("Y",time()), 5, "select issue_number_prefix,issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_name and entry_form=8 and $year_cond=".date('Y',time())." order by id DESC ", "issue_number_prefix", "issue_number_prefix_num" ));
			
 			$field_array_mst="id, issue_number_prefix, issue_number_prefix_num, issue_number, entry_form, item_category, company_id, supplier_id, issue_date, received_id, pi_id, received_mrr_no, inserted_by, insert_date";
			$data_array_mst="(".$id.",'".$new_return_number[1]."','".$new_return_number[2]."','".$new_return_number[0]."',8,1,".$cbo_company_name.",".$cbo_return_to.",".$txt_return_date.",".$txt_received_id.",".$pi_id.",".$txt_mrr_no.",'".$user_id."','".$pc_date_time."')";
			//echo "20**".$field_array."<br>".$data_array;die;
			//$rID=sql_insert("inv_issue_master",$field_array_mst,$data_array_mst,1);
			//yarn master table entry here END---------------------------------------// 
		}
		
	  
		
		//transaction table insert here START--------------------------------//
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$txt_issue_qnty = str_replace("'","",$txt_return_qnty);
		$txt_rate = str_replace("'","",$txt_rate);
 		$issue_stock_value = str_replace("'","",$txt_return_value);
		$transactionID = return_next_id("id", "inv_transaction", 1); 				
		$field_array_trans = "id,mst_id,company_id,supplier_id,prod_id,item_category,transaction_type,transaction_date,store_id,order_rate,order_ile_cost,cons_uom, cons_quantity, cons_rate, cons_amount, inserted_by, insert_date";
 		$data_array_trans = "(".$transactionID.",".$id.",".$cbo_company_name.",".$cbo_return_to.",".$txt_prod_id.",1,3,".$txt_return_date.",".$cbo_store_name.",".$order_rate.",".$order_ile_cost.",".$cbo_uom.",".$txt_issue_qnty.",".$txt_rate.",".$issue_stock_value.",'".$user_id."','".$pc_date_time."')"; 
		//echo $field_array."<br>".$data_array;die;
		//$transID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,1);
		//transaction table insert here END ---------------------------------//
		 
		//adjust product master table START-------------------------------------//
 		$txt_return_qnty = str_replace("'","",$txt_return_qnty);
		$txt_return_value = str_replace("'","",$txt_return_value);
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value,available_qnty,allocated_qnty from product_details_master where id=$txt_prod_id");
		$presentStock=$presentStockValue=$presentAvgRate=$allocated_qnty=$available_qnty=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$product_name_details 	=$result[csf("product_name_details")];
			$available_qnty			=$result[csf("available_qnty")];	
			$allocated_qnty 		=$result[csf("allocated_qnty")];
		}
		
		$receive_purpose=return_field_value("receive_purpose","inv_receive_master","id=$txt_received_id");
 		//echo $receive_purpose;die;
		if($receive_purpose==2)
		{
			$allocated_qnty=$allocated_qnty-$txt_return_qnty;
			$available_qnty = $available_qnty;
		}
		else
		{
			$allocated_qnty=$allocated_qnty;
			$available_qnty = $available_qnty-$txt_return_qnty;
		}
		
		$nowStock 		= $presentStock-$txt_return_qnty;
		$nowStockValue 	= $presentStockValue-$txt_return_value;
		$nowAvgRate		= number_format($nowStockValue/$nowStock,$dec_place[3],".","");
			
		$field_array_prod="last_issued_qnty*current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";
		$data_array_prod=$txt_return_qnty."*".$nowStock."*".$nowStockValue."*".$allocated_qnty."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'";
		//echo "20**".$field_array."<br>".$data_array."--";
		//$prodUpdate = sql_update("product_details_master",$field_array_prod,$data_array_prod,"id",$txt_prod_id,1);	 
		//adjust product master table END  -------------------------------------//
		//if($prodUpdate) echo "20**OK"; else "20**ERROR";die;
		
		
		
		//if LIFO/FIFO then START -----------------------------------------//
		$field_array = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
		$data_array="";
		$updateID_array=array();
		$update_data=array();
		$issueQnty = $txt_return_qnty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);  
		
		$isLIFOfifo=return_field_value("store_method","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17");
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC"; 
		
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in (1,4,5) and item_category=1 order by transaction_date $cond_lifofifo");			
		foreach($sql as $result)
		{				
			$issue_trans_id = $result[csf("id")]; // this row will be updated
			$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];
			$cons_rate = $result[csf("cons_rate")]; 
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$transactionID.",8,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$transactionID.",8,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//echo "20**".$data_array;die;
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
			
		}//end foreach 
 		  
		$mrrWiseIssueID=true; $upTrID=true;
		if(str_replace("'","",$txt_return_no)!="")
		{
			$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"id",$id,1);
		}
		else
		{
			$rID=sql_insert("inv_issue_master",$field_array_mst,$data_array_mst,1);
		}
		
		$transID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,1);
		$prodUpdate = sql_update("product_details_master",$field_array_prod,$data_array_prod,"id",$txt_prod_id,1);
		//mrr wise issue data insert here----------------------------//
		
		if($data_array!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array,$data_array,1);
		}
		
		//transaction table stock update here------------------------//
		if(count($updateID_array)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array));
		}	
 		//if LIFO/FIFO then END -----------------------------------------//
		
		//echo "20**".$rID." && ".$transID." && ".$prodUpdate." && ".$mrrWiseIssueID." && ".$upTrID;mysql_query("ROLLBACK"); die;
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if( $rID && $transID && $prodUpdate && $mrrWiseIssueID && $upTrID)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_return_number[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_return_number[0];
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if( $rID && $transID && $prodUpdate && $mrrWiseIssueID && $upTrID)
			{
				oci_commit($con);  
				echo "0**".$new_return_number[0];
			}
			else
			{
				oci_rollback($con);
				echo "10**".$new_return_number[0];
			}
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		if( str_replace("'","",$update_id) == "" )
		{
			check_table_status( $_SESSION['menu_id'],0);
			echo "15";exit(); 
		}
		 
		
		
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
		//product master table information
		//before stock update
		$sql = sql_select( "select a.id,a.avg_rate_per_unit,a.current_stock,a.stock_value, a.allocated_qnty, a.available_qnty, b.cons_quantity, b.cons_amount from product_details_master a, inv_transaction b where a.id=b.prod_id and b.id=$update_id and a.item_category_id=1 and b.item_category=1 and b.transaction_type=3" );
		$before_prod_id=$before_issue_qnty=$before_stock_qnty=$before_stock_value=$before_available_qnty=$before_allocated_qnty=0;
		foreach($sql as $result)
		{
			$before_prod_id 	= $result[csf("id")];
 			$before_stock_qnty 	= $result[csf("current_stock")];
			$before_stock_value = $result[csf("stock_value")];
			$before_available_qnty	=$result[csf("available_qnty")];	
			$before_allocated_qnty	=$result[csf("allocated_qnty")];	
			//before quantity and stock value
			$before_issue_qnty	= $result[csf("cons_quantity")];
			$before_issue_value	= $result[csf("cons_amount")];
		}
		
		//current product ID
		$txt_prod_id = str_replace("'","",$txt_prod_id);
		$txt_return_qnty = str_replace("'","",$txt_return_qnty);
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value,available_qnty,allocated_qnty from product_details_master where id=$txt_prod_id and item_category_id=1");
		$curr_avg_rate=$curr_stock_qnty=$curr_stock_value=0;
		foreach($sql as $result)
		{
			$curr_avg_rate 		= $result[csf("avg_rate_per_unit")];
			$curr_stock_qnty 	= $result[csf("current_stock")];
			$curr_stock_value 	= $result[csf("stock_value")];
			$available_qnty		=$result[csf("available_qnty")];	
			$allocated_qnty 	=$result[csf("allocated_qnty")];
		}
		
		$receive_purpose=return_field_value("receive_purpose","inv_receive_master","id=$txt_received_id");
 		//echo $receive_purpose;die;
		//weighted and average rate START here------------------------//
		//product master table data UPDATE START----------------------//		
		$update_array_prod= "last_issued_qnty*current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";
		if($before_prod_id==$txt_prod_id)
		{
			$adj_stock_qnty = $curr_stock_qnty+$before_issue_qnty-$txt_return_qnty; // CurrentStock + Before Issue Qnty - Current Issue Qnty
			$adj_stock_val  = $curr_stock_value+$before_issue_value-($txt_return_qnty*$curr_avg_rate); // CurrentStockValue + Before Issue Value - Current Issue Value
			$adj_avgrate	= number_format($adj_stock_val/$adj_stock_qnty,$dec_place[3],'.','');
			 
			if($receive_purpose==2)
			{
				$adj_allocated_qnty=$allocated_qnty+$before_issue_qnty-$txt_return_qnty;
				$adj_beforeAvailableQnty=$available_qnty;
			}
			else
			{
				$adj_allocated_qnty=$allocated_qnty;
				$adj_beforeAvailableQnty=$available_qnty+$before_issue_qnty-$txt_return_qnty;
			} 
			 
			$data_array_prod= $txt_return_qnty."*".$adj_stock_qnty."*".number_format($adj_stock_val,$dec_place[4],'.','')."*".$adj_allocated_qnty."*".$adj_beforeAvailableQnty."*'".$user_id."'*'".$pc_date_time."'";
 			//$query1 		= sql_update("product_details_master",$update_array,$data_array,"id",$before_prod_id,1);
			//echo "20**ERROR".$txt_return_qnty;die;
			//if($query1) echo "20**OK"; else echo "20**ERROR";die;
			//now current stock
			$curr_avg_rate 		= $adj_avgrate;
			$curr_stock_qnty 	= $adj_stock_qnty;
			$curr_stock_value 	= $adj_stock_val;
		}
		else
		{
			if($receive_purpose==2)
			{
				$adj_allocated_qnty=$before_allocated_qnty+$before_issue_qnty;
				$adj_beforeAvailableQnty=$before_available_qnty;
				
				$allocated_qnty=$allocated_qnty-$txt_return_qnty;
				$available_qnty = $available_qnty;
			}
			else
			{
				$adj_allocated_qnty=$before_allocated_qnty;
				$adj_beforeAvailableQnty=$before_available_qnty+$before_issue_qnty;
				
				$allocated_qnty=$allocated_qnty;
				$available_qnty = $available_qnty-$txt_return_qnty;
			} 
			
			$updateIdprod_array = $update_dataProd = array();
			//before product adjust
			$adj_before_stock_qnty 	= $before_stock_qnty+$before_issue_qnty; // CurrentStock + Before Issue Qnty
			$adj_before_stock_val  	= $before_stock_value+$before_issue_value; // CurrentStockValue + Before Issue Value
			$adj_before_avgrate		= number_format($adj_before_stock_val/$adj_before_stock_qnty,$dec_place[3],'.','');
			 
			$updateIdprod_array[]=$before_prod_id;
			$update_dataProd[$before_prod_id]=explode("*",("".$txt_return_qnty."*".$adj_before_stock_qnty."*".number_format($adj_before_stock_val,$dec_place[4],'.','')."*".$adj_allocated_qnty."*".$adj_beforeAvailableQnty."*'".$user_id."'*'".$pc_date_time."'"));
  			//$query1 = sql_update("product_details_master",$field_array,$data_array,"id",$before_prod_id,1);Not used
			
			//current product adjust
			$adj_curr_stock_qnty = 	$curr_stock_qnty-$txt_return_qnty; // CurrentStock + Before Issue Qnty
			$adj_curr_stock_val  = 	$curr_stock_value-($txt_return_qnty*$curr_avg_rate); // CurrentStockValue + Before Issue Value
			$adj_curr_avgrate	 =	number_format($adj_curr_stock_val/$adj_curr_stock_qnty,$dec_place[3],'.','');
			
			$updateIdprod_array[]=$txt_prod_id;
			$update_dataProd[$txt_prod_id]=explode("*",("".$txt_return_qnty."*".$adj_curr_stock_qnty."*".number_format($adj_curr_stock_val,$dec_place[4],'.','')."*".$allocated_qnty."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'"));
			//$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array_prod,$update_dataProd,$updateIdprod_array));
			
			//now current stock
			$curr_avg_rate 		= $adj_curr_avgrate;
			$curr_stock_qnty 	= $adj_curr_stock_qnty;
			$curr_stock_value 	= $adj_curr_stock_val;
		}
  		//------------------ product_details_master END--------------//
		//weighted and average rate END here-------------------------//
		 
		 $trans_data_array=array();		
 		//transaction table START--------------------------//
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$sql = sql_select("select a.id,a.balance_qnty,a.balance_amount,b.issue_qnty,b.rate,b.amount from inv_transaction a, inv_mrr_wise_issue_details b where a.id=b.recv_trans_id and b.issue_trans_id=$update_id and b.entry_form=8 and a.item_category=1"); 
		$updateID_array = array();
		$update_data = array();
		foreach($sql as $result)
		{
			$adjBalance = $result[csf("balance_qnty")]+$result[csf("issue_qnty")];
			$adjAmount = $result[csf("balance_amount")]+$result[csf("amount")];
			$updateID_array[]=$result[csf("id")]; 
			$update_data[$result[csf("id")]]=explode("*",("".$adjBalance."*".$adjAmount."*'".$user_id."'*'".$pc_date_time."'"));
			$trans_data_array[$result[csf("id")]]['qnty']=$adjBalance;
			$trans_data_array[$result[csf("id")]]['amnt']=$adjAmount;
		}
		
		$query2=true; $query3=true;
		/*if(count($updateID_array)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array));
		}
		//transaction table END----------------------------//
		
		 
		//LIFO/FIFO  START here------------------------//
		if(count($updateID_array)>0)
		{
			 $updateIDArray = implode(",",$updateID_array);
			 $query3 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=8");
		} */
		//****************************************** BEFORE ENTRY ADJUST END *****************************************//
		
		
		//############## SAVE POINT START  ###################
		if($db_type==0)
		{
			$savepoint="updatesql";
			mysql_query("SAVEPOINT $savepoint");
		}
		//############## SAVE POINT END    ###################
		
		 
				
  		$id=return_field_value("id","inv_issue_master","issue_number=$txt_return_no");
		//yarn master table UPDATE here START----------------------//		
		$field_array_mst="entry_form*company_id*supplier_id*issue_date*received_id*pi_id*received_mrr_no*updated_by*update_date";
		$data_array_mst="8*".$cbo_company_name."*".$cbo_return_to."*".$txt_return_date."*".$txt_received_id."*".$pi_id."*".$txt_mrr_no."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
		//$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"id",$id,1);	
		//yarn master table UPDATE here END---------------------------------------//	 
  		
		//transaction table insert here START--------------------------------//
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		
		$txt_rate = str_replace("'","",$txt_rate);
 		$issue_stock_value = str_replace("'","",$txt_return_value);
 		$field_array_trans= "company_id*supplier_id*prod_id*item_category*transaction_type*transaction_date*store_id 	*order_rate*order_ile_cost*cons_uom*cons_quantity*cons_rate*cons_amount*updated_by*update_date";
 		$data_array_trans= "".$cbo_company_name."*".$cbo_return_to."*".$txt_prod_id."*1*3*".$txt_return_date."*".$cbo_store_name."*".$order_rate."*".$order_ile_cost."*".$cbo_uom."*".$txt_return_qnty."*".$txt_rate."*".$txt_return_value."*'".$user_id."'*'".$pc_date_time."'"; 
		//echo $field_array."<br>".$data_array;die;
 		//$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1); 
		//transaction table insert here END ---------------------------------//
		
		//if LIFO/FIFO then START -----------------------------------------//
		$field_array = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$updateTrans_array = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
		$data_array="";
		$updateIDtrans_array=array();
		$update_dataTrans=array();
		$issueQnty = $txt_return_qnty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);  
		
		$isLIFOfifo=return_field_value("store_method","variable_settings_inventory","company_name=$cbo_company_name and variable_list=17");
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC"; 
		
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in (1,4,5) and item_category=1 order by transaction_date $cond_lifofifo");			
		foreach($sql as $result)
		{				
			$issue_trans_id = $result[csf("id")]; // this row will be updated
			if($trans_data_array[$issue_trans_id]['qnty']=="")
			{
				$balance_qnty = $result[csf("balance_qnty")];
				$balance_amount = $result[csf("balance_amount")];
			}
			else
			{
				$balance_qnty = $trans_data_array[$issue_trans_id]['qnty'];
				$balance_amount = $trans_data_array[$issue_trans_id]['amnt'];
			}
			
			$cons_rate = $result[csf("cons_rate")]; 
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",8,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateIDtrans_array[]=$issue_trans_id; 
				$update_dataTrans[$issue_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",8,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//echo "20**".$data_array;die;
				//for update
				$updateIDtrans_array[]=$issue_trans_id; 
				$update_dataTrans[$issue_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
			
		}//end foreach 
		
 		$mrrWiseIssueID=true; $upTrID=true;
		
		if($before_prod_id==$txt_prod_id)
		{
			$query1= sql_update("product_details_master",$update_array_prod,$data_array_prod,"id",$before_prod_id,1);
		}
		else
		{
			$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array_prod,$update_dataProd,$updateIdprod_array));
		}
		
		if(count($updateID_array)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array));
			$updateIDArray = implode(",",$updateID_array);
			$query3 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=8");
		}
		$rID=sql_update("inv_issue_master",$field_array_mst,$data_array_mst,"id",$id,1);
		$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1); 
		//mrr wise issue data insert here----------------------------//
		if($data_array!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array,$data_array,1);
		}

		//transaction table stock update here------------------------//
		if(count($updateIDtrans_array)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$updateTrans_array,$update_dataTrans,$updateIDtrans_array));
		}	
 		//if LIFO/FIFO then END -----------------------------------------//
		
		

 		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
 		
		//echo "20**".$query1." && ".$query2." && ".$query3." && ".$upTrID." && ".$rID." && ".$transID." && ".$data_array." && ".$upTrID;mysql_query("ROLLBACK");mysql_query("ROLLBACK TO $savepoint"); die; 
		
		if($db_type==0)
		{
			if($query1 && $query2 && $query3 && $rID && $transID && $mrrWiseIssueID && $upTrID)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				mysql_query("ROLLBACK TO $savepoint");
				echo "10**".str_replace("'","",$txt_return_no);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($query1 && $query2 && $query3 && $rID && $transID && $mrrWiseIssueID && $upTrID)
			{
				oci_commit($con);   
				echo "1**".str_replace("'","",$txt_return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_return_no);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		 //no operation
	}		
}



if($action=="return_number_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_return_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="850" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="180">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter Return Number</th>
                    <th width="220">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>                    
                    <td align="center">
                        <?php  
                            $search_by = array(1=>'Return Number');
							//$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 140, $search_by,"",0, "--Select--", "",1,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date" />&nbsp;&nbsp;&nbsp;
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_return_search_list_view', 'search_div', 'yarn_receive_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_return_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" style="margin-top:10px" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_return_search_list_view")
{
	
	$ex_data = explode("_",$data);
	$search_by = $ex_data[0];
	$search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
	
	$sql_cond="";
	if($search_by==1)
	{
		if($search_common!="") $sql_cond .= " and a.issue_number like '%$search_common'";
	}
		 
	if( $txt_date_from!="" && $txt_date_to!="" ) 
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.issue_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.issue_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
	
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql = "select a.id, $year_field a.issue_number_prefix_num, a.issue_number, a.company_id, a.supplier_id,a.issue_date, a.item_category, a.received_id,a.received_mrr_no, sum(b.cons_quantity)as cons_quantity
			from inv_issue_master a, inv_transaction b
			where a.id=b.mst_id and b.transaction_type=3 and a.status_active=1 and a.item_category=1 and b.item_category=1 and a.entry_form=8 $sql_cond group by a.id, a.issue_number_prefix_num, a.issue_number, a.company_id, a.supplier_id, a.issue_date, a.item_category, a.received_id, a.received_mrr_no, a.insert_date order by a.id";
	//echo $sql;
	$company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(2=>$company_arr,3=>$supplier_arr);
 	echo create_list_view("list_view", "Return No, Year, Company Name, Returned To, Return Date,Return Qty,Receive MRR","70,60,150,170,80,100,150","850","230",0, $sql , "js_set_value", "issue_number", "", 1, "0,0,company_id,supplier_id,0,0,0", $arr, "issue_number_prefix_num,year,company_id,supplier_id,issue_date,cons_quantity,received_mrr_no","","",'0,0,0,0,3,1,0') ;	
 	exit();
}

 

if($action=="populate_master_from_data")
{  
	$sql = "select id,issue_number,company_id,supplier_id,issue_date,item_category,received_id,received_mrr_no,pi_id   
			from inv_issue_master 
			where issue_number='$data' and item_category=1 and entry_form=8";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#txt_return_no').val('".$row[csf("issue_number")]."');\n";
 		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
 		echo "$('#cbo_return_to').val('".$row[csf("supplier_id")]."');\n";
		echo "$('#txt_return_date').val('".change_date_format($row[csf("issue_date")])."');\n";
		echo "$('#txt_mrr_no').val('".$row[csf("received_mrr_no")]."');\n";
		echo "$('#txt_received_id').val('".$row[csf("received_id")]."');\n";
		
		echo "$('#cbo_company_name').attr('disabled','disabled');\n";
		echo "$('#txt_mrr_no').attr('disabled','disabled');\n";
		
		$entry_form=return_field_value("entry_form","inv_receive_master","id=".$row[csf("received_id")]);
		if($entry_form==9)
		{
			$pi_no=return_field_value("pi_number","com_pi_master_details","id='".$row[csf("pi_id")]."'");
			echo "$('#txt_pi_no').removeAttr('disabled','disabled');\n";
			echo "$('#txt_pi_no').val('".$pi_no."');\n";
			echo "$('#pi_id').val('".$row[csf("pi_id")]."');\n";
		}
		else
		{
			echo "$('#txt_pi_no').attr('disabled','disabled');\n";
			echo "$('#txt_pi_no').val('');\n";
			echo "$('#pi_id').val('');\n";
		}
		//right side list view
		echo "show_list_view('".$row[csf("received_mrr_no")]."','show_product_listview','list_product_container','requires/yarn_receive_return_controller','');\n";
   	}	
	exit();	
}



if($action=="show_dtls_list_view")
{
	$ex_data = explode("**",$data);
	$return_number = $ex_data[0];
	$ret_mst_id = $ex_data[1];
	
	$cond="";
	if($return_number!="") $cond .= " and a.issue_number='$return_number'";
	if($ret_mst_id!="") $cond .= " and a.id='$ret_mst_id'";
	
	$sql = "select a.issue_number, a.company_id, a.supplier_id, a.issue_date, a.item_category, a.received_id, a.received_mrr_no, b.id, b.cons_quantity, b.cons_uom, b.cons_rate, b.cons_amount, c.product_name_details, c.id as prod_id   
			from inv_issue_master a, inv_transaction b left join product_details_master c on b.prod_id=c.id
			where a.id=b.mst_id and b.item_category=1 and b.transaction_type=3 $cond";
	//echo $sql;
	$result = sql_select($sql);
	$i=1;
	$rettotalQnty=0;
	$rcvtotalQnty=0;
	$totalAmount=0;
	?> 
     	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:1000px" >
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Return No</th>
                    <th>Item Description</th>
                    <th>Product ID</th>
                    <th>Received No</th>
                    <th>Return Qnty</th>
                    <th>UOM</th>                    
                    <th>Rate</th>
                    <th>Return Value</th> 
                </tr>
            </thead>
            <tbody>
            	<?php 
				foreach($result as $row){					
					if($i%2==0)
						$bgcolor="#E9F3FF";
					else 
						$bgcolor="#FFFFFF";
					
					/*echo "select b.balance_qnty from inv_receive_master a, inv_transaction b where a.id=b.mst_id and b.prod_id=".$row[csf("prod_id")]." and b.item_category=1 and b.transaction_type=1 and a.recv_number='".$row[csf("received_mrr_no")]."'";*/
					if($row[csf("prod_id")]!="")
					{	
						$sqlTr = sql_select("select b.balance_qnty from inv_receive_master a, inv_transaction b where a.id=b.mst_id and b.prod_id=".$row[csf("prod_id")]." and b.item_category=1 and b.transaction_type in (1,4) and a.id='".$row[csf("received_id")]."'");
					}
					$rcvQnty = $sqlTr[0][csf('balance_qnty')];
					
					$rettotalQnty +=$row[csf("cons_quantity")];
					//$rcvtotalQnty +=$rcvQnty;
					$totalAmount +=$row[csf("cons_amount")];
					
					
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>,<?php echo $rcvQnty;?>","child_form_input_data","requires/yarn_receive_return_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="110"><p><?php echo $row[csf("issue_number")]; ?></p></td>
                        <td width="200"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td width="70"><p><?php echo $row[csf("prod_id")]; ?></p></td>
                        <td width="100"><p><?php echo $row[csf("received_mrr_no")]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("cons_quantity")]; ?></p></td>
                        <!--<td width="70" align="right"><p><!?php echo $rcvQnty; ?></p></td>-->
                        <td width="70"><p><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("cons_rate")]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("cons_amount")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                        <th colspan="5">Total</th>                         
                        <th><?php echo $rettotalQnty; ?></th> 
                        <th colspan="2"></th>
                        <th><?php echo $totalAmount; ?></th>
                   </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}


if($action=="child_form_input_data")
{
	$ex_data = explode(",",$data);
	$data = $ex_data[0]; 	// transaction id
	$rcvQnty = $ex_data[1]; 
	
 	$sql = "select b.id as prod_id, b.product_name_details, a.id as tr_id, a.store_id, a.order_rate, a.order_ile_cost, a.cons_uom, a.cons_rate, a.cons_quantity, a.cons_amount
			from inv_transaction a, product_details_master b
 			where a.id=$data and a.status_active=1 and a.item_category=1 and transaction_type=3 and a.prod_id=b.id and b.status_active=1";
 	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $row)
	{
 		echo "$('#txt_item_description').val('".$row[csf("product_name_details")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#before_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#cbo_store_name').val('".$row[csf("store_id")]."');\n";
		echo "$('#txt_return_qnty').val('".$row[csf("cons_quantity")]."');\n";	
		$rcvQnty = $rcvQnty+$row[csf("cons_quantity")];
		echo "$('#txt_receive_qnty').val('".$rcvQnty."');\n";
		echo "$('#cbo_uom').val('".$row[csf("cons_uom")]."');\n";
		echo "$('#txt_rate').val('".$row[csf("cons_rate")]."');\n";		
		echo "$('#txt_return_value').val(".$row[csf("cons_amount")].");\n";
		echo "$('#update_id').val(".$row[csf("tr_id")].");\n";
		
		echo "$('#order_rate').val('".$row[csf("order_rate")]."');\n";
		echo "$('#order_ile_cost').val('".$row[csf("order_ile_cost")]."');\n";
	}
	
 	echo "set_button_status(1, permission, 'fnc_yarn_receive_return_entry',1,1);\n";
	//echo "$('#tbl_master').find('input,select').attr('disabled', false);\n";
	//echo "disable_enable_fields( 'cbo_company_name*txt_mrr_no', 1, '', '');\n";
  	exit();
}

// pi popup here----------------------// 
if ($action=="pi_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // pi id
		$("#hidden_pi_number").val(splitData[1]); // pi number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th>Supplier</th>
                    <th align="center" id="search_by_th_up">Enter PI Number</th>
                    <th>Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr class="general">
                    <td>
                        <?php  
                           echo create_drop_down( "cbo_supplier", 170, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$company' and b.party_type =2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 ); 
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_wopi_search_list_view', 'search_div', 'yarn_receive_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_tbl_id" value="" />
                    <input type="hidden" id="hidden_pi_number" value="hidden_pi_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" style="margin-top:5px" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_wopi_search_list_view")
{
 	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	
	$sql_cond="";
	$sql_cond .= " and a.pi_number LIKE '%$txt_search_common%'";
	if(trim($company)!=0) $sql_cond .= " and a.importer_id='$company'";
	if(trim($supplier)!=0) $sql_cond .= " and a.supplier_id='$supplier'";
	
	if($txt_date_from!="" && $txt_date_to!="" )
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.pi_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.pi_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
	
	$sql = "select a.id, a.pi_number, a.pi_date, a.supplier_id, a.currency_id, a.source, c.lc_number as lc_number
			from com_pi_master_details a 
			left join com_btb_lc_pi b on a.id=b.pi_id 
			left join com_btb_lc_master_details c on b.com_btb_lc_master_details_id=c.id
			where 
			a.item_category_id = 1 and
			a.status_active=1 and a.is_deleted=0
			$sql_cond order by a.id";
	//echo $sql;
	$result = sql_select($sql);
 	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(3=>$supplier_arr,4=>$currency,5=>$source);
	
	echo  create_list_view("list_view", "PI No, LC ,Date, Supplier, Currency, Source","120,130,100,200,100","830","230",0, $sql , "js_set_value", "id,pi_number", "", 1, "0,0,0,supplier_id,currency_id,source", $arr, "pi_number,lc_number,pi_date,supplier_id,currency_id,source", "",'','0,0,3,0,0,0') ;
	exit();	
}


if ($action=="yarn_receive_return_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql=" select id, issue_number, received_id, issue_date, supplier_id from  inv_issue_master where issue_number='$data[1]' and entry_form=8 and item_category=1 and status_active=1 and is_deleted=0";
	$dataArray=sql_select($sql);

	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr = return_library_array("select id,country_name from lib_country","id","country_name");
	$receive_arr = return_library_array("select id,recv_number from inv_receive_master","id","recv_number");
	?>
	<div style="width:900px;">
    <table width="880" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u>Purchase Return/Delivery Challan</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Return Number:</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="110"><strong>Receive ID:</strong></td><td width="175px"><?php echo $receive_arr[$dataArray[0][csf('received_id')]]; ?></td>
            <td width="100"><strong>Return To :</strong></td> <td width="175px"><?php echo $supplier_library[$dataArray[0][csf('supplier_id')]]; ?></td>
        </tr>
        <tr>
            <td width="110"><strong>Return Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
            <td colspan="4">&nbsp;</td>
        </tr>
    </table>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="880"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="50">SL</th>
                <th width="250" align="center">Item Description</th>
                <th width="70" align="center">UOM</th> 
                <th width="80" align="center">Return Qnty.</th>
                <th width="100" align="center">Store</th>
            </thead>
<?php
	$mrr_no =$dataArray[0][csf('issue_number')];;
	//$up_id =$data[1];
	$cond="";
	if($mrr_no!="") $cond .= " and c.issue_number='$mrr_no'";
	//if($up_id!="") $cond .= " and a.id='$up_id'";
	 $i=1;
 	$sql_dtls = "select b.id as prod_id, b.product_name_details, a.id as tr_id, a.store_id, a.cons_uom, a.cons_quantity
			from inv_transaction a, product_details_master b, inv_issue_master c
 			where c.id=a.mst_id and a.status_active=1 and a.company_id='$data[0]' and c.issue_number='$data[1]' and a.item_category=1 and transaction_type=3 and a.prod_id=b.id and b.status_active=1 ";
			$sql_result= sql_select($sql_dtls);
			
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
		$qnty+=$row[csf('cons_quantity')];
		?>

			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $row[csf('product_name_details')]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf('cons_uom')]]; ?></td>
                <td align="right"><?php echo $row[csf('cons_quantity')]; ?></td>
                <td><?php echo $store_library[$row[csf('store_id')]]; ?></td>
			</tr>
	<?php
    $i++;
    }
    ?>
        	<tr> 
                <td align="right" colspan="3" >Total</td>
                <td align="right"><?php echo number_format($qnty,0,'',','); ?></td>
                <td align="right">&nbsp;</td>
			</tr>
		</table>
        <br>
		 <?php
            echo signature_table(7, $data[0], "880px");
         ?>
      </div>
	</div> 
	<?php
    exit();
}
?>
