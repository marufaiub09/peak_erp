-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_sales_contract_order_info`
--

DROP TABLE IF EXISTS `com_sales_contract_order_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_sales_contract_order_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_sales_contract_id` int(11) NOT NULL DEFAULT '0',
  `wo_po_break_down_id` int(11) NOT NULL DEFAULT '0',
  `attached_qnty` int(11) NOT NULL DEFAULT '0',
  `attached_rate` double NOT NULL DEFAULT '0',
  `attached_value` double NOT NULL DEFAULT '0',
  `fabric_description` varchar(50) NOT NULL,
  `category_no` float NOT NULL DEFAULT '0',
  `hs_code` varchar(50) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `com_sales_contract_id` (`com_sales_contract_id`),
  KEY `wo_po_break_down_id` (`wo_po_break_down_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_sales_contract_order_info`
--

LOCK TABLES `com_sales_contract_order_info` WRITE;
/*!40000 ALTER TABLE `com_sales_contract_order_info` DISABLE KEYS */;
INSERT INTO `com_sales_contract_order_info` VALUES (1,1,163,117000,0.25,29250,'',0,'',13,'2015-05-13 10:25:40',0,'0000-00-00 00:00:00',1,0),(2,1,164,195000,0.25,48750,'',0,'',13,'2015-05-13 10:25:40',0,'0000-00-00 00:00:00',1,0),(3,1,165,156000,0.25,39000,'',0,'',13,'2015-05-13 10:25:40',0,'0000-00-00 00:00:00',1,0),(4,4,637,1200,4.2,5040,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(5,4,638,1200,4.2,5040,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(6,4,639,1200,4.2,5040,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(7,4,640,1200,5.25,6300,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(8,4,641,1200,6.1,7320,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(9,4,642,1200,6.1,7320,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(10,4,643,1200,4.4,5280,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(11,4,644,2400,4.4,10560,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(12,4,645,2400,4.45,10680,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(13,4,646,2508,4.8,12038.4,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(14,4,647,3000,2,6000,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(15,4,648,3000,2,6000,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(16,4,649,3000,2.1,6300,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(17,4,650,2004,2.3,4609.2,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(18,4,651,1008,2.3,2318.4,'',0,'',13,'2015-11-10 10:34:34',0,'0000-00-00 00:00:00',1,0),(19,5,653,3538,2.5,8845,'',0,'',13,'2015-11-16 10:50:26',0,'0000-00-00 00:00:00',1,0),(20,5,654,3538,3.2,11321.6,'',0,'',13,'2015-11-16 10:50:26',0,'0000-00-00 00:00:00',1,0),(21,3,629,58380,1.55,90489,'',0,'',13,'2015-11-16 11:24:27',0,'0000-00-00 00:00:00',1,0),(22,2,527,8563,4.3,36820.9,'',0,'',13,'2015-11-16 11:28:53',0,'0000-00-00 00:00:00',1,0),(23,2,528,9964,3.65,36368.6,'',0,'',13,'2015-11-16 11:28:53',0,'0000-00-00 00:00:00',1,0),(24,2,529,3500,6.4,22400,'',0,'',13,'2015-11-16 11:28:53',0,'0000-00-00 00:00:00',1,0),(25,2,530,3000,6,18000,'',0,'',13,'2015-11-16 11:28:53',0,'0000-00-00 00:00:00',1,0),(26,2,531,8000,3.3,26400,'',0,'',13,'2015-11-16 11:28:53',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `com_sales_contract_order_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
