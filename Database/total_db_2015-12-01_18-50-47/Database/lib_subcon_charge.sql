-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_subcon_charge`
--

DROP TABLE IF EXISTS `lib_subcon_charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_subcon_charge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comapny_id` int(11) DEFAULT '0',
  `body_part` int(11) NOT NULL DEFAULT '0',
  `cons_comp_id` int(11) NOT NULL DEFAULT '0',
  `const_comp` varchar(200) NOT NULL,
  `gsm` int(11) NOT NULL DEFAULT '0',
  `yarn_description` varchar(150) NOT NULL,
  `process_type_id` tinyint(2) NOT NULL DEFAULT '0',
  `process_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `width_dia_id` tinyint(1) NOT NULL DEFAULT '0',
  `in_house_rate` double NOT NULL DEFAULT '0',
  `uom_id` tinyint(4) NOT NULL DEFAULT '0',
  `rate_type_id` int(11) NOT NULL DEFAULT '0' COMMENT '2=knt,3=dy,4=fin',
  `customer_rate` double NOT NULL DEFAULT '0',
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_subcon_charge`
--

LOCK TABLES `lib_subcon_charge` WRITE;
/*!40000 ALTER TABLE `lib_subcon_charge` DISABLE KEYS */;
INSERT INTO `lib_subcon_charge` VALUES (1,1,1,0,'S/J EMELL',130,'28/s 99% Cotton 1% Emell',0,0,0,0,0,12,2,10,0,5,'2015-01-28 22:14:20',2,'2015-02-02 04:32:30',1,0),(2,1,1,0,'S/J CVC 60/40',160,'28/s CVC COTTON 60% POLYESTER 40%',0,0,0,0,0,12,2,10,0,5,'2015-01-29 06:08:22',2,'2015-02-02 04:31:53',1,0),(3,1,1,0,'S/J CVC 60/40',180,'24/s 60%COTTON 40% POLISTER',0,0,0,0,0,12,2,10,0,5,'2015-01-29 06:10:20',2,'2015-02-02 04:34:54',1,0),(4,1,1,0,'S/J 100% COTTON',160,'26/S CD.100% COTTON',0,0,0,0,0,12,2,10,0,5,'2015-02-02 04:15:22',2,'2015-02-02 04:30:09',1,0),(5,1,1,0,'L/S/LACOST',190,'30/S CD 20/D LYCRA',0,0,0,0,0,12,2,28,0,5,'2015-02-02 04:16:59',2,'2015-02-02 04:29:57',1,0),(6,1,1,0,'S/J CVC 60/40',160,'28/S CVC COTTON 60% POLYESTER40%',0,0,0,0,0,12,2,10,0,5,'2015-02-02 04:18:24',2,'2015-02-02 04:33:31',0,1),(7,1,1,0,'S/J cotton 60% Polyester 40%',180,'24/S CVC cotton 60% Polyester 40%',0,0,0,0,0,12,2,10,0,5,'2015-02-02 04:19:44',2,'2015-02-02 04:27:59',1,0),(8,1,1,0,'S/J 100% COTTON',160,'30/S CD',0,0,0,0,0,12,2,0,0,2,'2015-02-02 04:26:07',NULL,NULL,1,0),(9,1,1,0,'S/J 100% COTTON',130,'30/S CD',0,0,0,0,0,12,2,0,0,2,'2015-02-02 04:48:57',NULL,NULL,1,0),(10,1,1,0,'S/J 100% COTTON',130,'26/S CD.100% COTTON',0,0,0,0,0,1,2,0,0,2,'2015-02-02 04:50:54',NULL,NULL,1,0),(11,1,1,0,'S/J 95% COTTON 5% LYCRA',190,'30/s CD 20D LYCRA',0,0,0,0,0,12,2,0,0,2,'2015-02-02 04:55:27',NULL,NULL,1,0),(12,1,1,0,'S/J Peach Finish 100% cotton',180,'S/J Peach Finish 100% cotton',0,0,0,0,0,12,2,0,0,2,'2015-02-06 22:30:26',2,'2015-02-06 22:39:22',1,0),(13,1,1,0,'1x1 full feeder lycra rib.',190,'34/s cd 95% cotton 5% lycra',0,0,0,0,0,12,2,0,0,1,'2015-02-07 00:49:29',NULL,NULL,1,0),(14,1,1,0,'1x1 full feeder lycra rib.',190,'20/D LYCRA',0,0,0,0,0,12,2,0,0,1,'2015-02-07 02:24:11',NULL,NULL,1,0),(15,1,85,0,'S/J 95% COTTON 5% LYCRA',450,'2x1 Rib',0,0,0,0,0,12,2,0,0,2,'2015-02-07 04:22:00',NULL,NULL,1,0),(16,1,1,0,'Waffel',180,'100% cotton',0,0,0,0,0,12,2,0,0,1,'2015-02-11 14:49:06',NULL,NULL,1,0),(17,1,1,0,'90% Cotton 10% Giscos',160,'26/S CD',0,0,0,0,0,12,2,0,0,1,'2015-02-14 15:10:58',1,'2015-02-14 15:12:06',1,0),(18,1,1,0,'90% Cotton 10% Giscos',140,'30/S CD',0,0,0,0,0,12,2,0,0,1,'2015-02-14 15:11:33',NULL,NULL,1,0),(19,1,1,0,'S/J',130,'95% COTTON 5% Viscos',0,0,0,0,0,12,2,0,0,1,'2015-02-14 17:05:13',NULL,NULL,1,0),(20,1,1,0,'99% COTTON 1% Viscos',130,'S/J',0,0,0,0,0,12,2,0,0,1,'2015-02-14 17:06:12',NULL,NULL,1,0),(21,1,1,0,'1x1 RIB 85% cotton 15%viscos',180,'85% cotton 15%viscos',0,0,0,0,0,12,2,0,0,2,'2015-02-18 12:31:09',2,'2015-02-18 12:33:58',1,0),(22,1,1,0,'85% Cotton 15%Viscos',180,'1x1 RIB',0,0,0,0,0,12,2,0,0,2,'2015-02-18 12:35:00',NULL,NULL,1,0),(23,1,1,0,'S/S/J',180,'100% cotton S/S/J',0,0,0,0,0,12,2,0,0,20,'2015-03-03 15:16:41',NULL,NULL,1,0),(24,1,1,0,'1x1 RIB',190,'30/1',0,0,0,0,0,12,2,0,0,2,'2015-04-02 09:32:50',NULL,NULL,1,0);
/*!40000 ALTER TABLE `lib_subcon_charge` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
