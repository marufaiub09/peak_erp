<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Party Statement Report.
Functionality	:	
JS Functions	:
Created by		:	Kausar 
Creation date 	: 	21-10-2014
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Party Statement Report", "../../", 1, 1,$unicode,1,1);

/*$sql_trans=sql_select("SELECT  id, batch_no from pro_batch_create_mst where entry_form=36");


								$update_field="batch_idn";
								foreach($sql_trans as $row)
								{
									
									$update_id_arr[]="'".$row[csf("batch_no")]."'";
									$update_data_arr["'".$row[csf('batch_no')]."'"]=explode("*",($row[csf('id')]));
								}
								print_r ($update_data_arr); die;
								$upsubDtlsID=bulk_update_sql_statement("subcon_production_dtls","batch_id",$update_field,$update_data_arr,$update_id_arr);
								echo $upsubDtlsID."<br/><br/>"; die;
								
function bulk_update_sql_statement( $table, $id_column, $update_column, $data_values, $id_count )
{
	$field_array=explode("*",$update_column);
	//$id_count=explode("*",$id_count);
	//$data_values=explode("*",$data_values);
	//print_r($data_values);die;
	$sql_up.= "UPDATE $table SET ";
	
	 for ($len=0; $len<count($field_array); $len++)
	 {
		 $sql_up.=" ".$field_array[$len]." = CASE $id_column ";
		 for ($id=0; $id<count($id_count); $id++)
		 {
			 if (trim($data_values[$id_count[$id]][$len])=="") $sql_up.=" when ".$id_count[$id]." then  '".$data_values[$id_count[$id]][$len]."'" ;
			 else $sql_up.=" when ".$id_count[$id]." then  ".$data_values[$id_count[$id]][$len]."" ;
		 }
		 if ($len!=(count($field_array)-1)) $sql_up.=" END, "; else $sql_up.=" END ";
	 }
	 $sql_up.=" where id in (".implode(",",$id_count).")";
	 return $sql_up;     
}	*/							

?>	
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
	var permission = '<?php echo $permission; ?>';
			
	function fn_report_generated(type)
	{
		//alert(type);
		if(type==1 || type==4)
		{
			if (form_validation('cbo_company_id*txt_date_from*txt_date_to','Comapny Name*Date From*Date To')==false)
			{
				return;
			}
			else if (type==1)
			{
				if ($('#cbo_party_id').val()==0)
				{
					alert ("Please Select Party.");
					return;
				}
			}
		}
		else if(type==2 || type==3)
		{
			if (form_validation('cbo_company_id*txt_date_to','Comapny Name*Date To')==false)
			{
				return;
			}
		}
		
			var report_title=$( "div.form_caption" ).html();
			var data="action=report_generate"+get_submitted_data_string('cbo_company_id*cbo_party_id*cbo_bill_type*txt_date_from*txt_date_to',"../../")+'&report_title='+report_title+'&type='+type;
			//alert (data);
			freeze_window(3);
			http.open("POST","requires/subcon_party_statement_report_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
	}

	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("**"); 
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
			//append_report_checkbox('table_header_1',1);
			//setFilterGrid("table_body",-1,tableFilters);
			show_msg('3');
			release_freezing();
		}
	}
	
	function show_progress_report_details(action,order_id,width)
	{ 
		 emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/subcon_party_statement_report_controller.php?action='+action+'&order_id='+order_id, 'Work Progress Report Details', 'width='+width+',height=400px,center=1,resize=0,scrolling=0','../');
		
	} 

	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}

	function openImageWindow(id)
	{
		var title = 'Image View';	
		var page_link = 'requires/subcon_party_statement_report_controller.php?&action=image_view_popup&id='+id;
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=1,scrolling=0','../');
		
		emailwindow.onclose=function()
		{
		}
	}

</script>
</head>
<body onLoad="set_hotkey();">
<form id="partyStatementReport_1">
    <div style="width:100%;" align="center">    
        <?php echo load_freeze_divs ("../../",'');  ?>
         <h3 style="width:940px; margin-top:20px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3> 
         <div id="content_search_panel" >      
         <fieldset style="width:940px;">
            <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
                <thead>                    
                    <th width="150" class="must_entry_caption">Company</th>
                    <th width="140">Party </th>
                    <th width="120">Bill Type</th>
                    <th width="170" class="must_entry_caption">Transaction Date</th>
                    <th width=""><input type="reset" id="reset_btn" class="formbutton" style="width:70px" value="Reset" /></th>
                </thead>
                <tbody>
                    <tr class="general" >
                        <td  align="center"> 
                            <?php
                                echo create_drop_down( "cbo_company_id", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/subcon_party_statement_report_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                            ?>
                        </td>
                        <td id="buyer_td">
                            <?php 
                                echo create_drop_down( "cbo_party_id", 140, $blank_array,"", 1, "--Select Party--", $selected, "",1,"" );
                            ?>
                        </td>
                        <td>
                            <?php 
                                echo create_drop_down( "cbo_bill_type", 120, $production_process,"", 1, "-Select Type-", $selected, "","","" );
                            ?>
                        </td>
                        <td>
                            <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px" placeholder="From Date" >&nbsp; To
                            <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px"  placeholder="To Date"  >
                        </td>
                        <td align="center">
                            <input type="button" id="show_button" class="formbutton" style="width:60px" value="Ledger" onClick="fn_report_generated(1);" />
                            <input type="button" id="show_button" class="formbutton" style="width:70px" value="Receivable" onClick="fn_report_generated(2);" />
                            <input type="button" id="show_button" class="formbutton" style="width:80px" value="Invoice(AR)" onClick="fn_report_generated(3);" />
                            <input type="button" id="show_button" class="formbutton" style="width:100px" value="Periodic Bill" onClick="fn_report_generated(4);" />
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5" align="center">
							<?php echo load_month_buttons(1); ?>
                        </td>
                    </tr>
                </tfoot>
           </table> 
           <br />
        </fieldset>
    </div>
    </div>
        
    <div id="report_container" align="center"></div>
    <div id="report_container2" align="left"></div>
 </form>    
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
