-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_bank`
--

DROP TABLE IF EXISTS `lib_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(100) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `bank_code` varchar(50) DEFAULT NULL,
  `contact_person` varchar(100) DEFAULT NULL,
  `contact_no` varchar(100) DEFAULT NULL,
  `swift_code` varchar(100) NOT NULL,
  `web_site` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `country_id` int(1) DEFAULT NULL COMMENT 'comes from lib_list_country',
  `lien_bank` tinyint(1) DEFAULT NULL COMMENT '0:no 1:yes',
  `issusing_bank` tinyint(1) DEFAULT NULL COMMENT '0:no 1:yes',
  `advising_bank` tinyint(1) DEFAULT NULL COMMENT '0:no 1:yes',
  `salary_bank` tinyint(1) DEFAULT NULL COMMENT '0:no 1:yes',
  `remark` varchar(500) DEFAULT NULL,
  `total_account` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'employee code will be add here',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0' COMMENT 'employee code will be add here',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `bank_name` (`bank_name`),
  KEY `bank_code` (`bank_code`),
  KEY `country_id` (`country_id`),
  KEY `lien_bank` (`lien_bank`),
  KEY `issusing_bank` (`issusing_bank`),
  KEY `advising_bank` (`advising_bank`),
  KEY `salary_bank` (`salary_bank`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_bank`
--

LOCK TABLES `lib_bank` WRITE;
/*!40000 ALTER TABLE `lib_bank` DISABLE KEYS */;
INSERT INTO `lib_bank` VALUES (1,'DHAKA BANK LIMITED','UTTARA BRANCH','1417','','88-02-8951788','DHBLBDDH204','','','HOUSE~01, ROAD~13, SECTOR~1, UTTARA MODEL TOWN, DHAKA-1230',0,1,1,1,1,'',1,1,'2015-02-26 11:16:14',13,'2015-10-17 10:23:36',1,0),(2,'MERCANTILE BANK LIMITED','UTTARA BARANCH','1757','','88-02-8931725','MBLBBDDHA017','','','HOUSE : 10/A, ROAD: 7D, SECTOR : 9,  UTTARA MODEAL TOWN, DHAKA-1230',0,1,1,0,0,'',1,1,'2015-02-26 11:18:33',13,'2015-10-08 17:57:57',1,0),(3,'PAL','0','0','','','','','','',0,0,1,0,0,'',0,1,'2015-03-05 13:17:13',2,'2015-11-19 11:12:41',1,0),(4,'SOUTHEAST BANK LTD.','UTTARA BRANCH','1345','','88-02-8923680','SEBDBDDHUTT','','','POLT~01, ROAD#11, SECTOR#01, UTTARA MODEAL TOWN, DHAKA-1230.',0,1,1,0,0,'',1,13,'2015-08-26 18:13:29',13,'2015-10-08 17:58:09',1,0),(5,'DHAKA BANK LIMITED(TFL)','GULSHAN CIRCLE-2 BRANCH','1416','','880-28836224-6','DHBLBDDHGL2','','','BILQUIS TOWER (1ST FLOOR),PLOT -06, ROAD -46, GULSHAN AVENUE, GULSHAN CIRCLE-2,DHAKA-1230,BANGLADESH',0,1,1,0,0,'',1,13,'2015-10-08 13:25:43',13,'2015-11-19 11:11:45',1,0);
/*!40000 ALTER TABLE `lib_bank` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
