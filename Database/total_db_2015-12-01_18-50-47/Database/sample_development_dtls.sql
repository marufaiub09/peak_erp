-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sample_development_dtls`
--

DROP TABLE IF EXISTS `sample_development_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sample_development_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_mst_id` int(11) NOT NULL DEFAULT '0',
  `sample_name` int(11) NOT NULL DEFAULT '0',
  `sample_color` varchar(100) DEFAULT NULL,
  `working_factory` varchar(150) DEFAULT NULL,
  `sent_to_factory_date` date NOT NULL,
  `factory_dead_line` date NOT NULL,
  `recieve_date_from_buyer` date NOT NULL,
  `receive_date_from_factory` date NOT NULL,
  `fabrication` varchar(200) DEFAULT NULL,
  `fabric_sorce` int(2) NOT NULL,
  `sent_to_buyer_date` date NOT NULL,
  `key_point` varchar(100) DEFAULT NULL,
  `approval_status` int(5) NOT NULL DEFAULT '0',
  `department` varchar(100) DEFAULT NULL,
  `status_date` date NOT NULL,
  `tf_receive_date` date NOT NULL,
  `buyer_meeting_date` date NOT NULL,
  `sample_charge` int(11) NOT NULL,
  `sample_curency` tinyint(3) NOT NULL,
  `buyer_dead_line` date NOT NULL,
  `buyer_req_no` varchar(100) NOT NULL,
  `comments` text NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sample_development_dtls`
--

LOCK TABLES `sample_development_dtls` WRITE;
/*!40000 ALTER TABLE `sample_development_dtls` DISABLE KEYS */;
INSERT INTO `sample_development_dtls` VALUES (1,1,2,'605','','0000-00-00','0000-00-00','0000-00-00','0000-00-00','1X1 COTTON RIB 220 GSM',0,'0000-00-00','',0,'','0000-00-00','0000-00-00','0000-00-00',0,0,'0000-00-00','','',28,'2015-06-08 18:12:04',2,'2015-11-16 13:38:25',1,0),(2,2,3,'1302','','2015-10-24','0000-00-00','0000-00-00','0000-00-00','100% organic cotton, 1x1 rib, 220 gsm',0,'0000-00-00','',0,'','0000-00-00','0000-00-00','0000-00-00',0,0,'0000-00-00','','',28,'2015-10-24 12:36:36',2,'2015-11-16 13:40:07',1,0),(3,3,2,'681','','2015-11-05','2015-11-08','2015-11-01','0000-00-00','',0,'2015-11-11','',1,'','0000-00-00','2015-11-01','0000-00-00',0,0,'2015-11-03','','',2,'2015-11-18 11:30:52',2,'2015-11-18 11:30:59',1,0);
/*!40000 ALTER TABLE `sample_development_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
