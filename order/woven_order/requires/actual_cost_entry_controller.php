<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id = $_SESSION['logic_erp']["user_id"];
$based_on=array(1=>"Ex-factory Qty",2=>"Production Qty",3=>"SMV Produced");

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array="id, company_id, cost_head, exchange_rate, incurred_date, incurred_date_to, applying_period_date, applying_period_to_date, based_on, po_id, job_no, amount, amount_usd, inserted_by,insert_date";
		$id = return_next_id( "id", "wo_actual_cost_entry", 1 );
		
		if(str_replace("'", '', $cbo_cost_head)==5 || str_replace("'", '', $cbo_cost_head)==6)
		{
			if(is_duplicate_field( "id", "wo_actual_cost_entry", "company_id=$cbo_company_id and cost_head=$cbo_cost_head and incurred_date=$txt_incurred_date and incurred_date_to=$txt_incurred_to_date" )==1)
			{
				echo "11**0"; 
				die;			
			}
			
			$qnty_array=array(); $job_array=array(); $tot_qnty=0;
			if(str_replace("'", '', $cbo_based_on)==1)
			{
				$sql="select a.id, a.job_no_mst, sum(b.ex_factory_qnty) as qnty from wo_po_details_master w, wo_po_break_down a, pro_ex_factory_mst b where w.job_no=a.job_no_mst and a.id=b.po_break_down_id and w.company_name=$cbo_company_id and b.ex_factory_date between $txt_incurred_date and $txt_incurred_to_date and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.job_no_mst";
			}
			else if(str_replace("'", '', $cbo_based_on)==2)
			{
				$sql="select a.id, a.job_no_mst, sum(b.production_quantity) as qnty from wo_po_details_master w, wo_po_break_down a, pro_garments_production_mst b where w.job_no=a.job_no_mst and a.id=b.po_break_down_id and w.company_name=$cbo_company_id and b.production_date between $txt_incurred_date and $txt_incurred_to_date and b.production_type=5 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.job_no_mst";
			}
			else
			{
				$sql="select w.set_break_down, a.id, a.job_no_mst, b.item_number_id, sum(b.production_quantity) as qnty from wo_po_details_master w, wo_po_break_down a, pro_garments_production_mst b where w.job_no=a.job_no_mst and a.id=b.po_break_down_id and w.company_name=$cbo_company_id and b.production_date between $txt_incurred_date and $txt_incurred_to_date and b.production_type=5 and a.status_active=1 and a.is_deleted=0 and a.status_active=1 and a.is_deleted=0 group by a.id, b.item_number_id, a.job_no_mst, w.set_break_down";
			}
			//echo "10**".$sql;die;
			$result=sql_select($sql);
			foreach($result as $row)
			{
				if(str_replace("'", '', $cbo_based_on)==3)
				{
					$item_smv='';
					$exp_grmts_item = explode("__",$row[csf("set_break_down")]);
					foreach($exp_grmts_item as $value)
					{
						$grmts_item_qty = explode("_",$value);
						if($row[csf('item_number_id')]==$grmts_item_qty[0])
						{
							$item_smv=$grmts_item_qty[2];
							break;
						}
					}
					
					$smv_produced=$row[csf('qnty')]*$item_smv;
					$qnty_array[$row[csf('id')]]+=$smv_produced;
					$tot_qnty+=$smv_produced;
				}
				else
				{
					$qnty_array[$row[csf('id')]]=$row[csf('qnty')];
					$tot_qnty+=$row[csf('qnty')];
				}
				
				$job_array[$row[csf('id')]]=$row[csf('job_no_mst')];
			}
			
			$per_pcs_amnt=str_replace("'", '', $txt_amount)/$tot_qnty;
			foreach($qnty_array as $po_id=>$qnty)
			{
				$amount=$qnty*$per_pcs_amnt;
				$amntUsd=$amount/str_replace("'", '', $txt_exchange_rate_order);
				
				if($data_array!="") $data_array.=",";
				$data_array.="(".$id.",".$cbo_company_id.",".$cbo_cost_head.",".$txt_exchange_rate_order.",".$txt_incurred_date.",".$txt_incurred_to_date.",".$txt_applying_period_date.",".$txt_applying_period_to_date.",".$cbo_based_on.",'".$po_id."','".$job_array[$po_id]."','".$amount."','".$amntUsd."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id = $id+1;
			}
		}
		else
		{
			for($j=1;$j<=$tot_row;$j++)
			{ 	
				$txt_amount="txt_amount".$j;
				$po_id="po_id".$j;
				$jobNo="jobNo".$j;
				$amntUsd=$$txt_amount/str_replace("'", '', $txt_exchange_rate_order);
				
				if($data_array!="") $data_array.=",";
				$data_array.="(".$id.",".$cbo_company_id.",".$cbo_cost_head.",".$txt_exchange_rate_order.",".$txt_incurred_date.",".$txt_incurred_to_date.",".$txt_applying_period_date.",".$txt_applying_period_to_date.",".$cbo_based_on.",'".$$po_id."','".$$jobNo."','".$$txt_amount."','".$amntUsd."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id = $id+1;
			}
		}
		//echo "10**insert into wo_actual_cost_entry (".$field_array.") values ".$data_array;die;
		$rID=sql_insert("wo_actual_cost_entry",$field_array,$data_array,0);

		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "0**";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID)
			{
				oci_commit($con);  
				echo "0**";
			}
			else
			{
				oci_rollback($con);
				echo "5**";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array="id, company_id, cost_head, exchange_rate, incurred_date, incurred_date_to, applying_period_date, applying_period_to_date, based_on, po_id, job_no, amount, amount_usd, inserted_by,insert_date";
		$id = return_next_id( "id", "wo_actual_cost_entry", 1 );
		
		if(str_replace("'", '', $cbo_cost_head)==5 || str_replace("'", '', $cbo_cost_head)==6)
		{
			$qnty_array=array(); $job_array=array(); $tot_qnty=0;
			if(str_replace("'", '', $cbo_based_on)==1)
			{
				$sql="select a.id, a.job_no_mst, sum(b.ex_factory_qnty) as qnty from wo_po_details_master w, wo_po_break_down a, pro_ex_factory_mst b where w.job_no=a.job_no_mst and a.id=b.po_break_down_id and w.company_name=$cbo_company_id and b.ex_factory_date between $txt_incurred_date and $txt_incurred_to_date and a.status_active=1 and a.is_deleted=0 and a.status_active=1 and a.is_deleted=0 group by a.id, a.job_no_mst";
			}
			else if(str_replace("'", '', $cbo_based_on)==2)
			{
				$sql="select a.id, a.job_no_mst, sum(b.production_quantity) as qnty from wo_po_details_master w, wo_po_break_down a, pro_garments_production_mst b where w.job_no=a.job_no_mst and a.id=b.po_break_down_id and w.company_name=$cbo_company_id and b.production_date between $txt_incurred_date and $txt_incurred_to_date and b.production_type=5 and a.status_active=1 and a.is_deleted=0 and a.status_active=1 and a.is_deleted=0 group by a.id, a.job_no_mst";
			}
			else
			{
				$sql="select w.set_break_down, a.id, a.job_no_mst, b.item_number_id, sum(b.production_quantity) as qnty from wo_po_details_master w, wo_po_break_down a, pro_garments_production_mst b where w.job_no=a.job_no_mst and a.id=b.po_break_down_id and w.company_name=$cbo_company_id and b.production_date between $txt_incurred_date and $txt_incurred_to_date and b.production_type=5 and a.status_active=1 and a.is_deleted=0 and a.status_active=1 and a.is_deleted=0 group by a.id, b.item_number_id, a.job_no_mst, w.set_break_down";
			}
			//echo "10**".$sql;die;
			$result=sql_select($sql);
			foreach($result as $row)
			{
				if(str_replace("'", '', $cbo_based_on)==3)
				{
					$item_smv='';
					$exp_grmts_item = explode("__",$row[csf("set_break_down")]);
					foreach($exp_grmts_item as $value)
					{
						$grmts_item_qty = explode("_",$value);
						if($row[csf('item_number_id')]==$grmts_item_qty[0])
						{
							$item_smv=$grmts_item_qty[2];
							break;
						}
					}
					
					$smv_produced=$row[csf('qnty')]*$item_smv;
					$qnty_array[$row[csf('id')]]+=$smv_produced;
					$tot_qnty+=$smv_produced;
				}
				else
				{
					$qnty_array[$row[csf('id')]]=$row[csf('qnty')];
					$tot_qnty+=$row[csf('qnty')];
				}
				
				$job_array[$row[csf('id')]]=$row[csf('job_no_mst')];
			}
			
			$per_pcs_amnt=str_replace("'", '', $txt_amount)/$tot_qnty;
			foreach($qnty_array as $po_id=>$qnty)
			{
				$amount=$qnty*$per_pcs_amnt;
				$amntUsd=$amount/str_replace("'", '', $txt_exchange_rate_order);
				
				if($data_array!="") $data_array.=",";
				$data_array.="(".$id.",".$cbo_company_id.",".$cbo_cost_head.",".$txt_exchange_rate_order.",".$txt_incurred_date.",".$txt_incurred_to_date.",".$txt_applying_period_date.",".$txt_applying_period_to_date.",".$cbo_based_on.",'".$po_id."','".$job_array[$po_id]."','".$amount."','".$amntUsd."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id = $id+1;
			}
		}
		else
		{
			for($j=1;$j<=$tot_row;$j++)
			{ 	
				$txt_amount="txt_amount".$j;
				$po_id="po_id".$j;
				$jobNo="jobNo".$j;
				$amntUsd=$$txt_amount/str_replace("'", '', $txt_exchange_rate_order);
				
				if($data_array!="") $data_array.=",";
				$data_array.="(".$id.",".$cbo_company_id.",".$cbo_cost_head.",".$txt_exchange_rate_order.",".$txt_incurred_date.",".$txt_incurred_to_date.",".$txt_applying_period_date.",".$txt_applying_period_to_date.",".$cbo_based_on.",'".$$po_id."','".$$jobNo."','".$$txt_amount."','".$amntUsd."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id = $id+1;
			}
		}
		
		$delete=execute_query("delete from wo_actual_cost_entry where company_id=$cbo_company_id and cost_head=$cbo_cost_head and incurred_date=$txt_incurred_date and incurred_date_to=$txt_incurred_to_date",0);
		
		//echo "10**insert into wo_actual_cost_entry (".$field_array.") values ".$data_array;die;
		$rID=sql_insert("wo_actual_cost_entry",$field_array,$data_array,0);
		if($db_type==0)
		{
			if($rID && $delete)
			{
				mysql_query("COMMIT");  
				echo "1**";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $delete)
			{
				oci_commit($con);  
				echo "1**";
			}
			else
			{
				oci_rollback($con);
				echo "6**";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$delete=execute_query("delete from wo_actual_cost_entry where company_id=$cbo_company_id and cost_head=$cbo_cost_head and incurred_date=$txt_incurred_date and incurred_date_to=$txt_incurred_to_date",0);

		if($db_type==0)
		{
			if($delete)
			{
				mysql_query("COMMIT");  
				echo "2**";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "7**";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($delete)
			{
				oci_commit($con);  
				echo "2**";
			}
			else
			{
				oci_rollback($con);
				echo "7**";
			}
		}
		disconnect($con);
		die;
	}
}

 
if($action=="on_change_load_page")
{
	if($data==5 || $data==6)
	{
		$disbled="disabled='disabled'";
		$display="";
	}
	else 
	{
		$disbled='';
		$display='style="display:none"';
	}
?>
	<div style="width:650px; float:left; margin:auto" align="center">
		<table width="550" cellspacing="2" cellpadding="0" border="0">
        	<tr>
				<td align="center" class="must_entry_caption">Incurred Date</td>
				<td>
					<input type="text" name="txt_incurred_date" id="txt_incurred_date" class="datepicker" onChange="calculate_date()" style="width:140px" readonly/>	
				</td>
				<td align="center" class="must_entry_caption">Incurred Date To</td>
				<td>
					<input type="text" name="txt_incurred_to_date" id="txt_incurred_to_date" style="width:140px" class="datepicker" disabled/>	
				</td>
			</tr>
			<tr>
				<td align="center" class="must_entry_caption">Applying Period</td>
				<td>
					<input type="text" name="txt_applying_period_date" id="txt_applying_period_date" class="datepicker" style="width:140px" onChange="show_po_list()" readonly="readonly" <?php echo $disbled; ?>/>	
				</td>
				<td align="center" class="must_entry_caption">Applying Period To</td>
				<td>
					<input type="text" name="txt_applying_period_to_date" id="txt_applying_period_to_date" style="width:140px" class="datepicker" onChange="show_po_list()" readonly="readonly" <?php echo $disbled; ?>/>	
				</td>
			</tr>
            <tr>
				<td align="center" class="must_entry_caption">Exchange Rate</td>
				<td><input type="text" name="txt_exchange_rate_order" id="txt_exchange_rate_order" class="text_boxes_numeric" style="width:140px" disabled="disabled" /></td>
				<td align="center" class="must_entry_caption">Amount (TK.)</td>
				<td><input type="text" name="txt_amount" id="txt_amount" class="text_boxes_numeric"style="width:140px" onkeyup="calculate_balance(1)"/></td>
			</tr>
            <tr <?php echo $display; ?>>
                <td align="center" class="must_entry_caption">Based On</td>
                <td>
                    <?php 
						if($data==5)
						{
                        	echo create_drop_down( "cbo_based_on", 152, $based_on,'', '0', '---- Select ----', 1,"",''); 
						}
						else
						{
							echo create_drop_down( "cbo_based_on", 152, $based_on,'', '0', '---- Select ----', 1,"",'','1,2'); 	
						}
                    ?>	
                </td>
            </tr>
		</table>
	</div>
<?php
	exit();
}

if($action=="check_conversion_rate")
{ 
	$data=explode("**",$data);
	if($db_type==0)
	{
		$conversion_date=change_date_format($data[1], "Y-m-d", "-",1);
	}
	else
	{
		$conversion_date=change_date_format($data[1], "d-M-y", "-",1);
	}
	$exchange_rate=set_conversion_rate( $data[0], $conversion_date );
	echo $exchange_rate;
	exit();	
}

if($action=='populate_data_from_actual_cost')
{
	$data=explode("**",$data);
	
	if($db_type==0)
	{
		$incurred_date=change_date_format(trim($data[2]), "yyyy-mm-dd", "-");
		$incurred_date_to=change_date_format(trim($data[3]), "yyyy-mm-dd", "-");
	}
	else
	{
		$incurred_date=change_date_format(trim($data[2]),'','',1);
		$incurred_date_to=change_date_format(trim($data[3]),'','',1);
	}

	$data_array=sql_select("select sum(amount) as amount, exchange_rate, applying_period_date, applying_period_to_date from wo_actual_cost_entry where company_id='$data[0]' and cost_head='$data[1]' and incurred_date='$incurred_date' and incurred_date_to='$incurred_date_to' and status_active=1 and is_deleted=0 group by exchange_rate, applying_period_date, applying_period_to_date");
	
	if(count($data_array)>0) 
	{
		$button_status=1;
		$exchange_rate=$data_array[0][csf("exchange_rate")];
	}
	else 
	{
		$exchange_rate=set_conversion_rate( 2, $incurred_date_to );
		$button_status=0;
	}
	
	echo "document.getElementById('txt_exchange_rate_order').value 			= '".$exchange_rate."';\n";
	echo "document.getElementById('txt_amount').value 						= '".$data_array[0][csf("amount")]."';\n";
	echo "document.getElementById('txt_applying_period_date').value 		= '".change_date_format($data_array[0][csf("applying_period_date")])."';\n";
	echo "document.getElementById('txt_applying_period_to_date').value 		= '".change_date_format($data_array[0][csf("applying_period_to_date")])."';\n";
	echo "set_button_status($button_status, '".$_SESSION['page_permission']."', 'fnc_actual_cost_entry',1,1);\n";  
	
	exit();
}

if($action=="show_po_listview")
{
	$data=explode("**",$data);
	$company_id=$data[0];
	$cost_head=$data[1];
	
	if($db_type==0)
	{
		$applying_period_date=change_date_format(trim($data[2]), "yyyy-mm-dd", "-");
		$applying_period_to_date=change_date_format(trim($data[3]), "yyyy-mm-dd", "-");
		$incurred_date=change_date_format(trim($data[4]), "yyyy-mm-dd", "-");
		$incurred_date_to=change_date_format(trim($data[5]), "yyyy-mm-dd", "-");
	}
	else
	{
		$applying_period_date=change_date_format(trim($data[2]),'','',1);
		$applying_period_to_date=change_date_format(trim($data[3]),'','',1);
		$incurred_date=change_date_format(trim($data[4]),'','',1);
		$incurred_date_to=change_date_format(trim($data[5]),'','',1);
	}
	
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$amnt_arr=return_library_array( "select po_id, amount from wo_actual_cost_entry where company_id='$company_id' and cost_head='$cost_head' and incurred_date='$incurred_date' and incurred_date_to='$incurred_date_to' and status_active=1 and is_deleted=0",'po_id','amount');
	?>
    <table cellspacing="0" width="820" class="rpt_table" border="1" rules="all">
        <thead>
            <th colspan="8" width="">&nbsp;</th>
            <th align="right" width="160" style="color:#F00">Remaining Amount :</th>
            <th id="tot_remain" width="110" style="color:#F00">0</th>
        </thead>
    </table>            
    <table cellspacing="0" width="820" class="rpt_table" border="1" rules="all">
        <thead>
            <th width="30">SL</th>
            <th width="60">Buyer Name</th>
            <th width="60">Order Status</th>
            <th width="80">PO Number</th>
            <th width="90">Job Number</th>
            <th width="100">Style Name</th>
            <th width="120">Item Name</th>
            <th width="80">Shipment Date</th>
            <th width="80">Order Quantity</th>
            <th>Amount(TK.)</th>
        </thead>
    </table>
    <div style="width:820px; overflow-y:scroll; max-height:250px;" id="search_div">
    	<table cellspacing="0" width="800" class="rpt_table" border="1" rules="all" id="table_body"> 
		<?php
		$select_field='';
		if($cost_head==1) $select_field='lab_test';
		else if($cost_head==2) $select_field='freight';
		else if($cost_head==3) $select_field='inspection';
		else $select_field='currier_pre_cost';
		$fabriccostDataArray=sql_select("select job_no, $select_field from wo_pre_cost_dtls where status_active=1 and is_deleted=0");
		foreach($fabriccostDataArray as $fabRow)
		{
			 $fabriccostArray[$fabRow[csf('job_no')]]=$fabRow[csf($select_field)];
		}
		
        $sql="select a.job_no, a.buyer_name, a.style_ref_no, a.gmts_item_id, a.total_set_qnty, b.id as po_id, b.po_number, b.po_quantity, b.is_confirmed, b.pub_shipment_date as shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_id' and b.pub_shipment_date between '$applying_period_date' and '$applying_period_to_date' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 order by b.pub_shipment_date, a.id";
		$result=sql_select($sql);
        $i=1; $tot_po_qty=0; $tot_amount=0;
        foreach($result as $row)
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			$po_qty=$row[csf("total_set_qnty")]*$row[csf("po_quantity")];
			$tot_po_qty+=$po_qty; 
			$amount=$amnt_arr[$row[csf("po_id")]];
			$tot_amount+=$amount;
			
			if($fabriccostArray[$row[csf('job_no')]]>0) {$bgcolor="yellow";}
		?>
            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30"><?php echo $i; ?></td>
                <td width="60"><p><?php echo $buyer_arr[$row[csf("buyer_name")]]; ?></p></td>
                <td width="60"><?php echo $order_status[$row[csf("is_confirmed")]]; ?></td>
              	<td width="80"><p><?php echo $row[csf("po_number")]; ?></p>
                    <input type="hidden" name="po_id_<?php echo $i; ?>" id="po_id_<?php echo $i; ?>" value="<?php echo $row[csf("po_id")]; ?>">
                </td>
                <td width="90" id="job_no_<?php echo $i; ?>"><p><?php echo $row[csf("job_no")] ?></p></td>
                <td width="100"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
                <td width="120">
                    <p>
						<?php 
							$gmts_item='';
							$gmts_item_id=explode(",",$row[csf("gmts_item_id")]);
							foreach($gmts_item_id as $item_id)
							{
								$gmts_item.=$garments_item[$item_id].",";
							}
							$gmts_item=substr($gmts_item,0,-1); 
                        	echo $gmts_item; 
                        ?>
                    </p>
                </td>
                <td width="80" align="center"><p><?php echo change_date_format($row[csf("shipment_date")]); ?></p></td>
                <td width="80" align="right"><p><?php echo $po_qty; ?></p></td>
                <td align="center">
                    <input type="text" name="txt_amount_<?php echo $i; ?>" id="txt_amount_<?php echo $i; ?>" style="width:70px;" class="text_boxes_numeric" value="<?php echo $amount; ?>" onkeyup="calculate_balance(<?php echo $i; ?>);">
                </td>
            </tr>
        <?php	
            $i++;
		}
		?>
		</table>
    </div>
    <table cellspacing="0" width="820" class="rpt_table" border="1" rules="all">
        <tfoot>	 
           <th colspan="8">Total</th>
           <th align="right" width="80"><?php echo $tot_po_qty; ?></th>
           <th align="center" width="108"><input type="text" name="tot_amount" id="tot_amount" style="width:70px;" class="text_boxes_numeric" value="<?php echo $tot_amount; ?>" readonly="readonly">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>	
         </tfoot>	
    </table>
<?php
	exit();
}

if($action=="show_details_listview")
{
	if($data=='0') $data='';
?>
    <table cellspacing="0" width="750" class="rpt_table" border="1" rules="all">
        <thead>
            <th width="50">SL</th>
            <th width="70">Company</th>
            <th width="90">Cost Head</th>
            <th width="90">Based On</th>
            <th width="90">Incurred Date</th>
            <th width="90">Incurred Date To</th>
            <th width="90">Period From</th>
            <th width="90">Period To</th>
            <th>Amount(TK.)</th>
        </thead>
    </table>
    <div style="width:770px; overflow-y:scroll; max-height:250px;" id="search_div">
    	<table cellspacing="0" width="750" class="rpt_table" border="1" rules="all" id="table_body"> 
		<?php
        $sql="select a.id, a.company_short_name, b.cost_head, b.incurred_date, b.incurred_date_to, b.applying_period_date, b.applying_period_to_date, b.based_on, sum(b.amount) as amount from lib_company a, wo_actual_cost_entry b where a.id=b.company_id and a.company_name like '%".$data."%' and b.cost_head in(5,6) and a.status_active=1 and a.is_deleted=0 group by a.id, a.company_short_name, b.cost_head, b.incurred_date, b.incurred_date_to, b.applying_period_date, b.applying_period_to_date, b.based_on";
		$result=sql_select($sql);
        $i=1;
        foreach($result as $row)
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
		?>
            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="get_php_form_data('<?php echo $row[csf("id")]."_".$row[csf("cost_head")]."_".$row[csf("incurred_date")]."_".$row[csf("incurred_date_to")]; ?>', 'populate_data_from_cm_cost','requires/actual_cost_entry_controller');" id="tr_<?php echo $i; ?>" style="cursor:pointer" >
            	<td width="50"><?php echo $i; ?></td>
                <td width="70"><p><?php echo $row[csf("company_short_name")]; ?></p></td>
                <td width="90"><?php echo $actual_cost_heads[$row[csf("cost_head")]]; ?></td>
                <td width="90" align="center"><?php echo $based_on[$row[csf("based_on")]]; ?></td>
                <td width="90" align="center"><?php echo change_date_format($row[csf("incurred_date")]); ?></td>
                <td width="90" align="center"><?php echo change_date_format($row[csf("incurred_date_to")]); ?></td>
                <td width="90" align="center"><?php echo change_date_format($row[csf("applying_period_date")]); ?></td>
                <td width="90" align="center"><?php echo change_date_format($row[csf("applying_period_to_date")]); ?></td>
                <td align="right"><?php echo number_format($row[csf("amount")],2,'.',''); ?></td>
            </tr>
        <?php	
            $i++;
		}
		?>
		</table>
    </div>
<?php
	exit();
}

if($action=='populate_data_from_cm_cost')
{
	$data=explode("_",$data);
	$data_array=sql_select("select max(based_on) as based_on, max(exchange_rate) as exchange_rate, max(applying_period_date) as applying_period_date, max(applying_period_to_date) as applying_period_to_date, sum(amount) as amount from wo_actual_cost_entry where company_id='$data[0]' and cost_head='$data[1]' and incurred_date='$data[2]' and incurred_date_to='$data[3]' and status_active=1 and is_deleted=0");
	
	echo "document.getElementById('cbo_company_id').value 					= '".$data[0]."';\n";
	echo "$('#cbo_company_id').attr('disabled','true')".";\n";
	echo "document.getElementById('cbo_cost_head').value 					= '".$data[1]."';\n";
	echo "document.getElementById('cbo_based_on').value 					= '".$data_array[0][csf("based_on")]."';\n";
	echo "document.getElementById('txt_exchange_rate_order').value 			= '".$data_array[0][csf("exchange_rate")]."';\n";
	echo "document.getElementById('txt_amount').value 						= '".number_format($data_array[0][csf("amount")],2,'.','')."';\n";
	echo "document.getElementById('txt_incurred_date').value 				= '".change_date_format($data[2])."';\n";
	echo "document.getElementById('txt_incurred_to_date').value 			= '".change_date_format($data[3])."';\n";
	echo "document.getElementById('txt_applying_period_date').value 		= '".change_date_format($data_array[0][csf("applying_period_date")])."';\n";
	echo "document.getElementById('txt_applying_period_to_date').value 		= '".change_date_format($data_array[0][csf("applying_period_to_date")])."';\n";
	
	echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_actual_cost_entry',1,1);\n";  
	
	exit();
}
?>