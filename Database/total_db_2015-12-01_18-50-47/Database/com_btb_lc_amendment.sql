-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_btb_lc_amendment`
--

DROP TABLE IF EXISTS `com_btb_lc_amendment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_btb_lc_amendment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amendment_no` int(11) NOT NULL DEFAULT '0',
  `amendment_date` date NOT NULL,
  `btb_id` int(11) NOT NULL DEFAULT '0',
  `btb_lc_no` varchar(50) NOT NULL,
  `btb_lc_value` double NOT NULL DEFAULT '0',
  `amendment_value` double NOT NULL DEFAULT '0',
  `value_change_by` tinyint(2) NOT NULL DEFAULT '0',
  `last_shipment_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `delivery_mode` int(11) NOT NULL DEFAULT '0',
  `pay_term` int(11) NOT NULL DEFAULT '0',
  `inco_term` int(11) NOT NULL DEFAULT '0',
  `inco_term_place` varchar(100) NOT NULL,
  `partial_shipment` tinyint(1) NOT NULL DEFAULT '0',
  `port_of_loading` varchar(50) NOT NULL,
  `port_of_discharge` varchar(50) NOT NULL,
  `remarks` varchar(500) NOT NULL,
  `tenor` int(11) NOT NULL DEFAULT '0',
  `pi_id` varchar(1000) NOT NULL,
  `pi_value` double NOT NULL DEFAULT '0',
  `is_original` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No;1=Yes',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `amendment_no` (`amendment_no`),
  KEY `amendment_date` (`amendment_date`),
  KEY `contact_system_id` (`btb_id`),
  KEY `contract_no` (`btb_lc_no`),
  KEY `last_shipment_date` (`last_shipment_date`),
  KEY `expiry_date` (`expiry_date`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_btb_lc_amendment`
--

LOCK TABLES `com_btb_lc_amendment` WRITE;
/*!40000 ALTER TABLE `com_btb_lc_amendment` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_btb_lc_amendment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
