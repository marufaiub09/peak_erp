<?php
error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );


//return_field_value("sum(a.ex_factory_qnty) as po_quantity"," pro_ex_factory_mst a, wo_po_break_down b","a.po_break_down_id=b.id and b.id='".$row[csf("po_id")]."' and a.is_deleted=0 and a.status_active=1","po_quantity");
//$lc_sc=return_field_value("b.contract_no as export_lc_no"," com_sales_contract b"," b.id in($sc_lc_id)' ","export_lc_no");
//$lc_sc=return_field_value("b.export_lc_no as export_lc_no","com_export_lc b"," b.id in($sc_lc_id) ","export_lc_no");
//$lc_type=return_field_value("is_lc","com_export_invoice_ship_mst","id in(".$row[csf('invoice_no')].")","is_lc");
//$last_ex_factory_date=return_field_value(" max(ex_factory_date) as ex_factory_date","pro_ex_factory_mst","po_break_down_id in(".$row[csf('po_id')].")","ex_factory_date");

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	$reportType=str_replace("'","",$reportType);
	if($txt_date_from!="" && $txt_date_to!="")
	{
		$str_cond="and a.ex_factory_date between '$txt_date_from' and  '$txt_date_to ' ";
	}
	else
	{
		$str_cond="";
	}
	
	
	
	$details_report="";
	$master_data=array();
	$current_date=date("Y-m-d");
	$date=date("Y-m-d");$break_id=0;$sc_lc_id=0;
	if($reportType==1)
	{
		$exfact_qty_arr=return_library_array( "select po_break_down_id,sum(ex_factory_qnty) as ex_factory_qnty from pro_ex_factory_mst where status_active=1 and is_deleted=0 group by po_break_down_id", "po_break_down_id", "ex_factory_qnty");
		$invoice_array=return_library_array( "select id,invoice_no from com_export_invoice_ship_mst", "id", "invoice_no"  );
		$lc_sc_type_arr=return_library_array( "select id,is_lc from com_export_invoice_ship_mst", "id", "is_lc"  );
		$lc_sc_id_array=return_library_array( "select id,lc_sc_id from com_export_invoice_ship_mst", "id", "lc_sc_id"  );
		$lc_num_arr=return_library_array( "select id,export_lc_no from com_export_lc", "id", "export_lc_no"  );
		$sc_num_arr=return_library_array( "select id,contract_no from com_sales_contract", "id", "contract_no"  );

		
		$details_report .='<table width="2352" cellspacing="0" cellpadding="0"  border="1"  class="rpt_table" rules="all" id="table_body">';
		$i=1;
		
		//var_dump($break_down_id_lc);
		/*$sql= "select b.id as po_id, a.po_break_down_id, a.invoice_no, c.company_name, c.id,a.po_break_down_id, a.item_number_id, max(a.ex_factory_date) as ex_factory_date, sum(a.ex_factory_qnty) as ex_factory_qnty, a.lc_sc_no, b.shipment_date, c.buyer_name, c.style_ref_no, c.style_description, b.po_number, b.po_quantity, b.unit_price, a.challan_no, a.invoice_no
		from pro_ex_factory_mst a, wo_po_break_down b, wo_po_details_master c 
		where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' $str_cond  and a.is_deleted=0 and a.status_active=1 
		group by 
				b.id 
		order by a.ex_factory_date ASC";*/
				// LISTAGG(CAST(b.id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY b.id) as tr_id
				
		if($db_type==0)
		{
			$sql= "select b.id as po_id, max(a.lc_sc_no) as lc_sc_arr_no, group_concat(distinct a.invoice_no) as invoice_no, group_concat(distinct a.item_number_id) as itm_num_id, sum(a.ex_factory_qnty) as ex_factory_qnty, max(a.ex_factory_date)  as ex_factory_date, group_concat(distinct  a.lc_sc_no) as lc_sc_no, group_concat(distinct a.challan_no) as challan_no,  b.shipment_date, b.po_number, (b.po_quantity*c.total_set_qnty) as po_quantity, (b.unit_price/c.total_set_qnty) as unit_price, c.id, c.company_name, c.buyer_name, c.style_ref_no, c.style_description
			
			from pro_ex_factory_mst a, wo_po_break_down b, wo_po_details_master c 
			where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' $str_cond  and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1
			group by 
					b.id , b.shipment_date, b.po_number, b.unit_price, c.id, c.company_name, c.buyer_name, c.style_ref_no, c.style_description
			order by c.buyer_name, b.shipment_date ASC";
		}
		else if($db_type==2)
		{
			$sql= "select b.id as po_id,max(a.lc_sc_no) as lc_sc_arr_no, LISTAGG(CAST( a.invoice_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.invoice_no) as invoice_no,LISTAGG(CAST( a.item_number_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.item_number_id) as itm_num_id, sum(a.ex_factory_qnty) as ex_factory_qnty, max(a.ex_factory_date) as ex_factory_date,  LISTAGG(CAST( a.lc_sc_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.lc_sc_no) as lc_sc_no, LISTAGG(CAST( a.challan_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.challan_no) as challan_no,  b.shipment_date, b.po_number, (b.po_quantity*c.total_set_qnty) as po_quantity,(b.unit_price/c.total_set_qnty) as unit_price, c.id, c.company_name, c.buyer_name, c.style_ref_no, c.style_description
			
			from pro_ex_factory_mst a, wo_po_break_down b, wo_po_details_master c 
			where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' $str_cond  and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 
			group by 
					b.id , b.shipment_date, b.po_number, b.unit_price,b.po_quantity,c.total_set_qnty,c.id,c.company_name, c.buyer_name, c.style_ref_no, c.style_description,c.total_set_qnty
			order by c.buyer_name, b.shipment_date ASC";
		}
		
		//echo $sql;die;
		$sql_result=sql_select($sql);
		//print_r($sql_result);die;
		foreach($sql_result as $row)
		{
			//$last_ex_factory_date=return_field_value(" max(ex_factory_date) as ex_factory_date","pro_ex_factory_mst","po_break_down_id in(".$row[csf('po_id')].")","ex_factory_date");
			$ex_fact_date_range=$txt_date_from."*".$txt_date_to."_". 1;
			$total_exface_qnty=$txt_date_from."*".$txt_date_to."_". 2;
			if($break_id==0) $break_id=$row[csf('po_id')]; else $break_id=$break_id.",".$row[csf('po_id')];
			if($sc_lc_id==0) $sc_lc_id=$row[csf('lc_sc_no')]; else $sc_lc_id=$sc_lc_id.",".$row[csf('lc_sc_no')];
			//$lc_type=return_field_value("is_lc","com_export_invoice_ship_mst","id in(".$row[csf('invoice_no')].")","is_lc");
			$invoce_id_arr=array_unique(explode(",",$row[csf('invoice_no')]));
			
			if ($i%2==0)  
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";	
			$comapny_id=$row[csf("company_name")];
			
			$onclick=" change_color('tr_".$i."','".$bgcolor."')";
			$details_report .='<tr bgcolor="'.$bgcolor.'" onclick="'.$onclick.'" id="tr_'.$i.'">';
			$details_report .='<td width="40" align="center">'.$i.'</td>
								<td width="100" align="center" ><p>'.$buyer_arr[$row[csf("buyer_name")]].'</p></td>
								<td width="110" align="center"><p>'.$row[csf("po_number")].'</p></td>
								<td width="120" align="center"><p>'.$row[csf("challan_no")].'</p></td>
								<td width="100" align="center"><p>';
								$inv_id=""; $lc_sc_no="";
								foreach($invoce_id_arr as $invoice_id)
								{
									if($inv_id=="") $inv_id=$invoice_array[$invoice_id]; else $inv_id=$inv_id.",".$invoice_array[$invoice_id];
									if($lc_sc_type_arr[$invoice_id]==1)
									{
										if($lc_sc_no=="") $lc_sc_no=$lc_num_arr[$lc_sc_id_array[$invoice_id]]; else $lc_sc_no=$lc_sc_no.",".$lc_num_arr[$lc_sc_id_array[$invoice_id]];
									}
									else
									{
										if($lc_sc_no=="") $lc_sc_no=$sc_num_arr[$lc_sc_id_array[$invoice_id]]; else $lc_sc_no=$lc_sc_no.",".$sc_num_arr[$lc_sc_id_array[$invoice_id]];
									}
								}
								
			$details_report .=$inv_id.'</p></td>
								<td width="100" align="center"><p>'.$lc_sc_no.'</p></td>
								<td width="100"><p>'.$row[csf("style_ref_no")].'</p></td>
								<td width="100"><p>'.$row[csf("style_description")].'</p></td>
								<td width="110" align="center"><p>';//$garments_item
								$item_name_arr=explode(",",$row[csf("itm_num_id")]);
								$item_name_arr=array_unique($item_name_arr);
								if(!empty($item_name_arr))
								{
									$p=1;
									foreach($item_name_arr as $item_id)
									{
										if($p!=1) $item_name_all .=",";
										$item_name_all .=$garments_item[$item_id];
										$p++;
									}
								}
								
			$details_report .=$item_name_all.'</p></td>
								<td width="120" align="center"><p>'.change_date_format($row[csf("shipment_date")]).'</p></td>
								<td width="120" align="center"><a href="##" onclick="openmypage_ex_date('.$comapny_id.",'".$row[csf('po_id')]."','".$ex_fact_date_range."','ex_date_popup'".')">'.change_date_format($row[csf('ex_factory_date')]).'</a></td>
								
								<td width="100" align="center"><p>'. $diff=datediff("d",$current_date, $row[csf("shipment_date")]).'</p></td>
								<td width="100" align="right"><p>'. number_format($po_quantity=$row[csf("po_quantity")],0,'.', '').'</p></td>
								<td width="100" align="right"><p>'. number_format($unit_price=$row[csf("unit_price")],2).'</p></td>
								<td width="130" align="right"><p>'. number_format($value=($po_quantity*$unit_price),2).'</p></td>
								<td width="100" align="right"><p><a href="##" onclick="openmypage_ex_date('.$comapny_id.",'".$row[csf('po_id')]."','".$ex_fact_date_range."','ex_date_popup'".')">'. number_format($current_ex_Fact_Qty=$row[csf("ex_factory_qnty")],0,'.', '').'</a></p></td>
								<td width="130" align="right"><p>'. number_format( $current_ex_fact_value=$current_ex_Fact_Qty*$unit_price,2).'</p></td>
								<td width="100" align="right"><p><a href="##" onclick="openmypage_ex_date('.$comapny_id.",'".$row[csf('po_id')]."','".$total_exface_qnty."','ex_date_popup'".')">'.number_format($total_ex_fact_qty=$exfact_qty_arr[$row[csf("po_id")]],0,'.', '').'</a></p></td>
								<td width="130" align="right"><p>'. number_format($total_ex_fact_value=$total_ex_fact_qty*$unit_price,2).'</p></td>
								<td width="100" align="right"><p>'. number_format($excess_shortage_qty=$po_quantity-$total_ex_fact_qty,0,'.', '').'</p></td>
								<td width="130" align="right"><p>'. number_format($excess_shortage_value=$excess_shortage_qty*$unit_price,2).'</p></td>
								<td align="center" width="90"><p>'. number_format($total_ex_fact_qty_parcent=($total_ex_fact_qty/$po_quantity)*100,0).'</p></td>
							</tr>';
							
			$master_data[$row[csf("buyer_name")]]['b_id']=$row[csf("buyer_name")];	
			$master_data[$row[csf("buyer_name")]]['po_qnty'] +=$row[csf("po_quantity")];
			$master_data[$row[csf("buyer_name")]]['po_value'] +=$row[csf("po_quantity")]*$row[csf("unit_price")];
			$master_data[$row[csf("buyer_name")]]['ex_factory_qnty'] +=$row[csf("ex_factory_qnty")];
			$master_data[$row[csf("buyer_name")]]['ex_factory_value'] +=$row[csf("ex_factory_qnty")]*$row[csf("unit_price")];
			$master_data[$row[csf("buyer_name")]]['total_ex_fact_qty'] +=$total_ex_fact_qty;
			$master_data[$row[csf("buyer_name")]]['total_ex_fact_value'] +=$total_ex_fact_qty*$row[csf("unit_price")];
			
			$total_po_qty+=$row[csf("po_quantity")];
			$total_po_valu+=$row[csf("po_quantity")]*$row[csf("unit_price")];
			$total_ex_qty+=$row[csf("ex_factory_qnty")];
			$total_ex_valu=$row[csf("ex_factory_qnty")]*$row[csf("unit_price")];
			$g_total_ex_qty+=$total_ex_fact_qty;
			$g_total_ex_val+=$total_ex_fact_qty*$row[csf("unit_price")];
			$total_eecess_storage_qty+=$excess_shortage_qty;
			$total_eecess_storage_val+=$excess_shortage_value;
			
			$i++;$item_name_all="";
		} 
		/*echo "SELECT b.export_lc_no as export_lc_no from com_export_lc_order_info a, com_export_lc b where a.com_export_lc_id=b.id and a.wo_po_break_down_id in($break_id) and b.id in($sc_lc_id)"."<br>";
		echo "SELECT b.contract_no as export_lc_no from com_export_lc_order_info a, com_sales_contract b where a.com_export_lc_id=b.id and a.wo_po_break_down_id in($break_id) and b.id in($sc_lc_id)"."<br>";*/
		//print_r($master_data);
		
		$details_report .='
						</table>';
							
		foreach($master_data as $rows)
		{
			$total_po_val+=$rows[po_value];
		}
							
		?>
        <div style="width:2270px;">
            <div style="width:1120px" >
                <table width="1090"  cellspacing="0"  align="center">
                    <tr>
                        <td align="center" colspan="10" class="form_caption">
                            <strong style="font-size:16px;">Company Name:<?php echo  $company_library[$cbo_company_name] ;?></strong>
                        </td>
                    </tr>
                    <tr class="form_caption">
                        <td colspan="10" align="center" class="form_caption"> <strong style="font-size:15px;">Ex-Factory Report</strong></td>
                    </tr>
                     <tr align="center">
                        <td colspan="10" align="center" class="form_caption"> <strong style="font-size:15px;">Total Summary</strong></td>
                    </tr>
                    </table>
                    <table width="1090" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" id="">
                    <thead>
                        <th width="40" height="34">SL</th>
                        <th width="130">Buyer Name</th>
                        <th width="100">PO Qty.</th>
                        <th width="130">PO Value</th>
                        <th width="100">PO Value(%)</th>
                        <th width="100">Current Ex-Fact. Qty.</th>
                        <th width="130">Current Ex-Fact. Value</th>
                        <th width="100">Total Ex-Fact. Qty.</th>
                        <th width="130">Total Ex-Fact. Value </th>
                        <th >Total Ex-Fact. Value %</th>
                    </thead>
                 </table>
                 <table width="1090" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" id="table_header_1">
                 <?php
                 $m=1;
                foreach($master_data as $rows)
                {
                    if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                    else
                    $bgcolor="#FFFFFF";
                     ?>
                  <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>" >
                        <td width="40" align="center"><?php echo $m; ?></td>
                        <td width="130">
                        <p><?php
                        echo $buyer_arr[$rows[b_id]];
                        ?></p>
                        </td>
                        <td width="100" align="right"><p><?php  $po_quantity=$rows[po_qnty];echo number_format($po_quantity,0); $total_buyer_po_quantity+=$po_quantity; ?></p></td>
                        <td width="130" align="right" ><p  id="value_<?php echo $i ; ?>"><?php $buyer_po_value=$rows[po_value]; echo number_format($buyer_po_value,2 ,'.', '');  $total_buyer_po_value+=$buyer_po_value; ?></p></td>
                        <td width="100" align="right">
                         <?php echo number_format(($buyer_po_value/$total_po_val)*100,2,'.','');$parcentages+=($buyer_po_value/$total_po_val)*100; ?>
                        </td>
                        <td width="100" align="right">
                        <p><?php
                         $current_ex_Fact_Qty=$rows[ex_factory_qnty];  echo number_format($current_ex_Fact_Qty,0,'',''); $total_current_ex_Fact_Qty +=$current_ex_Fact_Qty;
                        ?></p>
                        </td>
                        <td width="130" align="right">
                        <p><?php
                        $current_ex_fact_value=$rows[ex_factory_value]; echo number_format($current_ex_fact_value,2,'.',''); $total_current_ex_fact_value+=$current_ex_fact_value;
                        ?></p>
                        </td>
                        <td align="right" width="100">
                        <p><?php
                         $total_ex_fact_qty=$rows[total_ex_fact_qty]; echo number_format($total_ex_fact_qty,0,'',''); $mt_total_ex_fact_qty+=$total_ex_fact_qty;
                        ?></p>
                        </td>
                        <td align="right" width="130">
                        <p><?php
                         $total_ex_fact_value=$rows[total_ex_fact_value];  echo  number_format($total_ex_fact_value,2,'.',''); $mt_total_ex_fact_value+=$total_ex_fact_value;
                        ?></p>
                        </td>
                        <td align="right">
                        <p><?php
                        $total_ex_fact_value_parcentage=($total_ex_fact_value/$buyer_po_value)*100;
                        echo number_format($total_ex_fact_value_parcentage,0)
                        ?> %</p>
                        </td>
                    </tr>
                    <?php
                    $i++;$m++;
                    $buyer_po_quantity=0;
                    $buyer_po_value=0;
                    $current_ex_Fact_Qty=0;
                    $current_ex_fact_value=0;
                    $total_ex_fact_qty=0;
                    $total_ex_fact_value=0;
                    
                }
                    ?>
                    <input type="hidden" name="total_i" id="total_i" value="<?php echo $i; ?>" />
                    <tfoot>
                        <th align="right" colspan="2"><b>Total:</b></th>
                        <th  align="right" id="total_buyer_po_quantity"><?php echo number_format($total_buyer_po_quantity,0);  ?></th>
                        <th  align="right" id="value_total_buyer_po_value"><?php echo number_format($total_buyer_po_value,2 ,'.', ''); ?> </th>
                        <th align="right" id="parcentages"><?php echo ceil($parcentages); ?></th>
                        <th  align="right" id="total_current_ex_Fact_Qty"><?php echo number_format($total_current_ex_Fact_Qty,0); ?></th>
                        <th  align="right" id="value_total_current_ex_fact_value"><?php echo  number_format($total_current_ex_fact_value,2); ?></th>
                        <th align="right" id="mt_total_ex_fact_qty"><?php echo number_format($mt_total_ex_fact_qty,0); ?></th>
                        <th align="right" id="value_mt_total_ex_fact_value"><?php echo number_format($mt_total_ex_fact_value,2); ?></th>
                        <th align="right"></th>
                    </tfoot>
                </table>
            </div>
            <br />
            <div style="width:2370px;">
                <table width="2370"  >
                    <tr>
                    <td colspan="21" class="form_caption"><strong style="font-size:16px;">Shipped Out Order Details Report</strong></td>
                    </tr>
                </table>
                <table width="2352" border="1" class="rpt_table" rules="all" id="">
                    <thead>
                        <th width="40">SL</th>
                        <th width="100">Buyer Name</th>
                        <th width="110">Order NO</th>
                        <th width="120">Challan NO</th>
                        <th width="100" >Invoice NO</th>
                        <th width="100" >LC/SC NO</th>
                        <th width="100">Style Ref. no.</th>
                        <th width="100">Style Description</th>
                        <th width="110">Item Name</th>
                        <th width="120">Shipment Date</th>
                        <th width="120">Ex-Fac. Date</th>
                        <th width="100">Days in Hand</th>
                        <th width="100">PO Qtny. (pcs)</th>
                        <th width="100">Unit Price</th>
                        <th width="130">PO Value</th>
                        <th width="100">Current Ex-Fact. Qty (pcs)</th>
                        <th width="130">Current Ex-Fact. Value</th>
                        <th width="100">Total Ex-Fact. Qty.</th>
                        <th width="130">Total Ex-Fact. Value</th>
                        <th width="100">Excess/ Shortage Qty</th>
                        <th width="130">Excess/ Shortage Value</th>
                        <th width="90">Total Ex-Fact. Qty. %</th>
                    </thead>
                </table>
            <div style="width:2370px; overflow-y:scroll; overflow-x:hidden; max-height:300px;"  id="scroll_body" >
            <?php echo $details_report; ?>
            <table width="2352" cellspacing="0" cellpadding="0"  border="1"  class="rpt_table" rules="all" id="table_footer">
                <tfoot>
                    <tr>
                        <th colspan="12" align="right"><strong>Total</strong></th>
                        <th width="100" id="total_po_qty"><?php number_format($total_po_qty,0);?></th>
                        <th width="100" align="right">&nbsp;</th>
                        <th width="130" align="right" id="value_total_po_valu"><?php number_format($total_po_valu,2); ?></th>
                        <th width="100" align="right" id="total_ex_qty"><?php number_format($total_ex_qty,0); ?></th>
                        <th width="130" align="right" id="value_total_ex_valu"><?php number_format($total_ex_valu,2);?></th>
                        <th width="100" align="right" id="g_total_ex_qty"><?php number_format($g_total_ex_qty,0);?></th>
                        <th width="130" align="right" id="value_g_total_ex_val"><?php number_format($g_total_ex_val,2);?></th>
                        <th width="100" align="right" id="total_eecess_storage_qty"><?php number_format($total_eecess_storage_qty,0);?></th>
                        <th width="130" align="right" id="value_total_eecess_storage_val"><?php number_format($total_eecess_storage_val,2);?></th>
                        <th width="90">&nbsp;</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            </div>
        </div>
	   
		<?php
	}
	else if($reportType==2)
	{
		
	$costing_per_arr = return_library_array("select job_no, costing_per from wo_pre_cost_mst","job_no","costing_per"); 
	$tot_cost_arr = return_library_array("select job_no, cm_for_sipment_sche from wo_pre_cost_dtls","job_no","cm_for_sipment_sche");
 
		$sql= "select b.id as po_id, (b.unit_price/c.total_set_qnty) as unit_price, c.total_set_qnty, c.id as job_id, c.job_no,a.ex_factory_qnty as ex_factory_qnty,a.ex_factory_date,c.buyer_name
		from pro_ex_factory_mst a, wo_po_break_down b, wo_po_details_master c 
		where a.po_break_down_id=b.id and b.job_no_mst=c.job_no and c.company_name like '$cbo_company_name' $str_cond  and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1
		order by ex_factory_date";
		
		//echo $sql;
		$sql_result=sql_select($sql);
		//print_r($sql_result);die;
		foreach($sql_result as $row)
		{
			if(!in_array($row[csf('job_no')],$temp_arr))
			{
				$dzn_qnty=0; $cm_value=0;
				if($costing_per_arr[$row[csf('job_no')]]==1) $dzn_qnty=12;
				else if($costing_per_arr[$row[csf('job_no')]]==3) $dzn_qnty=12*2;
				else if($costing_per_arr[$row[csf('job_no')]]==4) $dzn_qnty=12*3;
				else if($costing_per_arr[$row[csf('job_no')]]==5) $dzn_qnty=12*4;
				else $dzn_qnty=1;
				
				$dzn_qnty=$dzn_qnty*$row[csf('total_set_qnty')];
				$cm_value_rate=($tot_cost_arr[$row[csf('job_no')]]/$dzn_qnty);
				$temp_arr[]=$row[csf('job_no')];
			}
			$result_data_arr[date("Y-m",strtotime($row[csf("ex_factory_date")]))][$row[csf("buyer_name")]]['cm_value'] +=$cm_value_rate*$row[csf("ex_factory_qnty")];
			$result_data_arr[date("Y-m",strtotime($row[csf("ex_factory_date")]))][$row[csf("buyer_name")]]['ex_factory_qnty'] +=$row[csf("ex_factory_qnty")];
			$result_data_arr[date("Y-m",strtotime($row[csf("ex_factory_date")]))][$row[csf("buyer_name")]]['ex_factory_value'] +=($row[csf("ex_factory_qnty")]*$row[csf("unit_price")]);
			$buyer_tem_arr[$row[csf("buyer_name")]]=$row[csf("buyer_name")];
			
			
		}
		//var_dump($result_data_arr);
		$total_month=count($result_data_arr);
		$width=($total_month*300)+100; 
		$colspan=$total_month*3;
		//echo $total_month;die;
		?>
        <div style="width:100%;" id="scroll_body">
            <table width="<?php echo $width;?>"  cellspacing="0"  align="center">
                <tr>
                    <td align="center" colspan="<?php echo $colspan; ?>" class="form_caption">
                    <strong style="font-size:16px;">Company Name:<?php echo  $company_library[$cbo_company_name] ;?></strong>
                	</td>
                </tr>
                <tr class="form_caption">
                	<td colspan="<?php echo $colspan; ?>" align="center" class="form_caption"> <strong style="font-size:15px;">Ex-Factory Report Summary</strong></td>
                </tr>
            </table>
            <table width="<?php echo $width;?>" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" id="">
            <thead>
             <tr>
            <?php
			$m=1;
            foreach($result_data_arr as $yearMonth=>$vale)
            {
                $month_arr=explode("-",$yearMonth);
				$month_val=($month_arr[1]*1);
				if($m==1)
				{ 
					?>
					<th width="400" colspan="4"><?php echo $months[$month_val]; ?></th>
					<?php
				}
				else
				{
					?>
					<th width="300" colspan="3"><?php echo $months[$month_val]; ?></th>
					<?php
				}
				$m++;
            }
            ?>
          </tr>
           <tr>
                <th width="100">Buyer</th>
				 <?php
                foreach($result_data_arr as $yearMonth=>$vale)
                {
                    $month_arr=explode("-",$yearMonth);
                    ?>
                    <th width="100">Exfactory Qty</th>
                    <th width="100">Exfactory Value</th>
                    <th width="100">CM Value</th>
                    <?php
                }
                ?>
           </tr>
            </thead>
         </table>
        <table width="<?php echo $width;?>" cellspacing="0" cellpadding="0" border="0" rules="all" class="rpt_table" id="">
        <?php
        foreach($buyer_tem_arr as $buyer_id=>$val)
        {
			if ($i%2==0)  
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			?>
			<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>" >
                <td width="100"><?php echo $buyer_arr[$buyer_id]; ?></td>
                <?php
                foreach($result_data_arr as $month_id=>$result)
                {
                    $ex_factory_qnty=$result_data_arr[$month_id][$buyer_id]['ex_factory_qnty'];
                    $ex_factory_value=$result_data_arr[$month_id][$buyer_id]['ex_factory_value'];
                    $cm_value=$result_data_arr[$month_id][$buyer_id]['cm_value'];
                    ?>
                    <td width="100" align="right"><?php echo number_format($ex_factory_qnty,0); ?></td>
                    <td width="100" align="right"><?php echo number_format($ex_factory_value,2); ?></td>
                    <td width="100" align="right"><?php echo number_format($cm_value,2); ?></td>
                    <?php
                    $total_mon_data[$month_id]['ex_factory_qnty'] += $ex_factory_qnty;
                    $total_mon_data[$month_id]['ex_factory_value'] += $ex_factory_value;
                    $total_mon_data[$month_id]['cm_val'] += $cm_value;
                }
                ?>
			</tr>     
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th>Total:&nbsp;</th>
            <?php
            foreach($total_mon_data as $row)
            {
				?>
				<th><?php echo number_format($row['ex_factory_qnty'],0); ?></th>
				<th><?php echo number_format($row['ex_factory_value'],2); ?></th>
				<th><?php echo number_format($row['cm_val'],2); ?></th>
				<?php
            }
            ?>
        </table>
        </div>
		<?php
	}
	exit();
}

if($action=="ex_date_popup")
{
	echo load_html_head_contents("Report Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	$ex_factory_date=str_replace("'","",$ex_factory_date);
	$ex_factory_date_ref=explode("_",$ex_factory_date);
	$exfact_date=explode("*",$ex_factory_date_ref[0]);
	//echo $ex_factory_date."***".$company_id."***".$order_id;
	$country_arr=return_library_array( "select id,country_name from  lib_country", "id", "country_name"  );
	?>
	<div style="width:100%" align="center">
		<fieldset style="width:450px"> 
        <div class="form_caption" align="center"><strong>Ex-Factory Date Details</strong></div><br />
            <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                <thead>
                    <tr>
                        <th width="40">SL</th>
                        <th width="100">Date</th>
                        <th width="100">Chllan</th>
                        <th width="100">Country</th>
                        <th width="100">Qnty</th>
                     </tr>   
                </thead>
                <tbody>	 	
					<?php
						$i=1;
						if($ex_factory_date_ref[1]==2)
						{ 
							$sql_qnty="Select ex_factory_date, sum(ex_factory_qnty) as ex_factory_qnty,challan_no,country_id from pro_ex_factory_mst where po_break_down_id=$order_id and status_active=1 and is_deleted=0 group by ex_factory_date,challan_no,country_id order by ex_factory_date ";
						}
						else
						{
							$sql_qnty="Select ex_factory_date, sum(ex_factory_qnty) as ex_factory_qnty,challan_no,country_id from pro_ex_factory_mst where po_break_down_id=$order_id and status_active=1 and is_deleted=0 and ex_factory_date between  '$exfact_date[0]' and '$exfact_date[1]' group by ex_factory_date,challan_no,country_id order by ex_factory_date ";
						}
						//echo $sql_qnty;
						$sql_dtls=sql_select($sql_qnty);
						foreach($sql_dtls as $row_real)
						{ 
							 if ($i%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";                               
							 ?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td><?php echo $i; ?></td> 
									<td  align="center"><?php echo change_date_format($row_real[csf("ex_factory_date")]); ?></td>
                                    <td ><?php echo $row_real[csf("challan_no")]; ?></td>
                                    <td ><?php echo $country_arr[$row_real[csf("country_id")]]; ?></td>
									<td width="100" align="right"><?php echo number_format($row_real[csf("ex_factory_qnty")],2); ?>&nbsp;</td>
								</tr>
							<?php 
							$total_ex_qnty+=$row_real[csf("ex_factory_qnty")];
							$i++;
						}
                    ?>
                </tbody>
                <tfoot>
                	<tr>
                    	<th colspan="4" align="right"><strong>Total :</strong></th>
                        <th align="right"><?php echo number_format($total_ex_qnty,2); ?>
                    </tr>
                </tfoot>
            </table>
        </fieldset>
    </div>    
    <?php	
}
disconnect($con);
?>
