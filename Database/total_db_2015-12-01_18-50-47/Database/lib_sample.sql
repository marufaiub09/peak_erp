-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_sample`
--

DROP TABLE IF EXISTS `lib_sample`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_sample` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_name` varchar(50) DEFAULT NULL,
  `sample_type` tinyint(1) DEFAULT '0' COMMENT 'SampleType-0:Null 1:select 2:pp 3:Fit 4:Size Set',
  `inserted_by` int(11) NOT NULL COMMENT 'employee code will be add here',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'employee code will be add here',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '0' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `sample_name` (`sample_name`),
  KEY `sample_type` (`sample_type`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_sample`
--

LOCK TABLES `lib_sample` WRITE;
/*!40000 ALTER TABLE `lib_sample` DISABLE KEYS */;
INSERT INTO `lib_sample` VALUES (1,'Red Field',5,2,'2015-03-02 12:28:50',NULL,NULL,1,0),(2,'PP Sample',2,2,'2015-03-19 16:54:17',NULL,NULL,1,0),(3,'Fit Sample',3,2,'2015-03-19 16:54:29',NULL,NULL,1,0),(4,'Production',7,2,'2015-03-19 16:54:55',NULL,NULL,1,0),(5,'Tag',8,2,'2015-03-19 16:55:03',NULL,NULL,1,0),(6,'Size set Sample',4,2,'2015-03-19 16:55:16',NULL,NULL,1,0),(7,'Complete Body',5,2,'2015-03-19 16:55:36',NULL,NULL,1,0),(8,'Fabrics',5,2,'2015-03-23 10:52:50',NULL,NULL,1,0),(9,'Lab Test',5,2,'2015-03-23 10:53:11',NULL,NULL,1,0),(10,'For Gift',5,20,'2015-03-24 17:29:42',NULL,NULL,1,0),(11,'Shiping Sample',7,20,'2015-03-24 17:35:59',NULL,NULL,1,0),(12,'Cutting Part',5,2,'2015-04-01 16:36:11',NULL,NULL,1,0),(13,'Gold Seal Sample',5,2,'2015-04-29 16:33:20',2,'2015-11-26 16:26:42',1,0),(14,'Original Sample',6,2,'2015-05-17 15:49:41',NULL,NULL,1,0),(15,'Dyeing and Finishing',5,2,'2015-07-05 14:32:40',NULL,NULL,1,0),(16,'SMS Sample',6,20,'2015-08-22 15:10:38',NULL,NULL,1,0);
/*!40000 ALTER TABLE `lib_sample` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `sample_Trigger` AFTER INSERT ON `lib_sample`
 FOR EACH ROW BEGIN
     
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
