-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_btb_lc_pi`
--

DROP TABLE IF EXISTS `com_btb_lc_pi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_btb_lc_pi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_btb_lc_master_details_id` int(11) NOT NULL DEFAULT '0',
  `pi_id` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `com_btb_lc_master_details_id` (`com_btb_lc_master_details_id`),
  KEY `pi_id` (`pi_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_btb_lc_pi`
--

LOCK TABLES `com_btb_lc_pi` WRITE;
/*!40000 ALTER TABLE `com_btb_lc_pi` DISABLE KEYS */;
INSERT INTO `com_btb_lc_pi` VALUES (3,3,9,13,'2015-10-08 18:25:48',0,'0000-00-00 00:00:00',1,0,0),(5,5,11,13,'2015-10-08 18:38:28',0,'0000-00-00 00:00:00',1,0,0),(14,12,33,13,'2015-11-09 18:59:40',0,'0000-00-00 00:00:00',1,0,0),(15,13,34,13,'2015-11-10 15:54:20',0,'0000-00-00 00:00:00',1,0,0),(16,14,35,13,'2015-11-10 16:18:05',0,'0000-00-00 00:00:00',1,0,0),(17,15,37,13,'2015-11-10 16:53:48',0,'0000-00-00 00:00:00',1,0,0),(18,16,39,13,'2015-11-11 18:00:27',0,'0000-00-00 00:00:00',1,0,0),(19,16,38,13,'2015-11-11 18:00:27',0,'0000-00-00 00:00:00',1,0,0),(20,17,30,13,'2015-11-11 18:07:30',0,'0000-00-00 00:00:00',1,0,0),(22,19,29,13,'2015-11-11 18:19:32',0,'0000-00-00 00:00:00',1,0,0),(23,20,31,13,'2015-11-11 18:25:18',0,'0000-00-00 00:00:00',1,0,0),(26,7,17,13,'2015-11-12 10:26:51',0,'0000-00-00 00:00:00',1,0,0),(28,9,21,13,'2015-11-12 10:28:07',0,'0000-00-00 00:00:00',1,0,0),(29,11,25,13,'2015-11-12 10:28:53',0,'0000-00-00 00:00:00',1,0,0),(30,18,28,13,'2015-11-12 10:29:38',0,'0000-00-00 00:00:00',1,0,0),(31,10,27,13,'2015-11-12 10:31:01',0,'0000-00-00 00:00:00',1,0,0),(32,22,41,13,'2015-11-14 14:13:50',0,'0000-00-00 00:00:00',1,0,0),(34,24,43,13,'2015-11-14 14:37:46',0,'0000-00-00 00:00:00',1,0,0),(35,25,44,13,'2015-11-14 14:45:16',0,'0000-00-00 00:00:00',1,0,0),(36,26,45,13,'2015-11-14 14:54:40',0,'0000-00-00 00:00:00',1,0,0),(37,27,46,13,'2015-11-14 15:01:39',0,'0000-00-00 00:00:00',1,0,0),(40,6,5,13,'2015-11-15 10:21:41',0,'0000-00-00 00:00:00',1,0,0),(41,1,1,13,'2015-11-15 10:22:38',0,'0000-00-00 00:00:00',1,0,0),(42,2,8,13,'2015-11-15 10:23:09',0,'0000-00-00 00:00:00',1,0,0),(43,4,10,13,'2015-11-15 10:24:14',0,'0000-00-00 00:00:00',1,0,0),(44,8,22,13,'2015-11-15 10:25:49',0,'0000-00-00 00:00:00',1,0,0),(45,21,53,13,'2015-11-16 13:04:10',0,'0000-00-00 00:00:00',1,0,0),(46,23,54,13,'2015-11-16 13:13:23',0,'0000-00-00 00:00:00',1,0,0),(47,28,55,2,'2015-11-16 13:23:41',0,'0000-00-00 00:00:00',1,0,0),(48,29,50,13,'2015-11-17 10:01:01',0,'0000-00-00 00:00:00',1,0,0),(49,30,49,13,'2015-11-17 10:05:41',0,'0000-00-00 00:00:00',1,0,0),(50,31,52,13,'2015-11-17 10:08:17',0,'0000-00-00 00:00:00',1,0,0),(51,32,56,13,'2015-11-17 17:32:34',0,'0000-00-00 00:00:00',1,0,0),(52,33,57,13,'2015-11-19 10:17:36',0,'0000-00-00 00:00:00',1,0,0),(53,34,58,13,'2015-11-19 10:27:12',0,'0000-00-00 00:00:00',1,0,0),(54,35,51,13,'2015-11-22 17:40:57',0,'0000-00-00 00:00:00',1,0,0),(55,36,61,13,'2015-11-23 10:07:25',0,'0000-00-00 00:00:00',1,0,0),(56,37,62,13,'2015-11-23 10:24:37',0,'0000-00-00 00:00:00',1,0,0),(57,38,59,13,'2015-11-23 10:40:06',0,'0000-00-00 00:00:00',1,0,0),(58,39,63,13,'2015-11-23 17:35:17',0,'0000-00-00 00:00:00',1,0,0),(59,40,64,13,'2015-11-25 09:27:29',0,'0000-00-00 00:00:00',1,0,0),(60,41,65,13,'2015-11-25 09:34:02',0,'0000-00-00 00:00:00',1,0,0),(61,42,67,13,'2015-11-26 19:00:42',0,'0000-00-00 00:00:00',1,0,0);
/*!40000 ALTER TABLE `com_btb_lc_pi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
