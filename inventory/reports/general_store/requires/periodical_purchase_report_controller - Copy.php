<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_store")
{
	$data=explode('_',$data);
	//print_r ($data);  
	echo create_drop_down( "cbo_store_name", 120, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET($data[0],company_id) and FIND_IN_SET($data[1],item_category_id) order by store_name","id,store_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

if ($action=="load_drop_down_supplier")
{	  
	//echo create_drop_down( "cbo_supplier_name", 120, "select id,supplier_name from lib_supplier where FIND_IN_SET($data,tag_company) and FIND_IN_SET($data,party_type) order by supplier_name","id,supplier_name", 1, "--Select Supplier--", 0, "",0 );
	echo create_drop_down( "cbo_supplier_name", 120, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where FIND_IN_SET($data,a.tag_company) and a.id=b.supplier_id and b.party_type in (1,6,7,8,90) and a.status_active=1 and a.is_deleted=0 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", "", "" );  	 
	exit();
}

if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	//echo load_html_head_contents("Popup Info", "../../../../", 1, 1,'',1,'');
	extract($_REQUEST);
	$data=explode('_',$data);
	print_r ($data);  
	
?>	
    <script>
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	 function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			alert (tbl_row_count);
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ ) {
				 eval($('#tr_'+i).attr("onclick"));  
			}
		}
		
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_account_id').val( id );
		$('#item_account_val').val( ddd );
	} 
		  
		  
	</script>
     <input type="text" id="item_account_id" />
     <input type="text" id="item_account_val" />
 <?php
		$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
		$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
		
		$sql="SELECT id,item_account,item_category_id,item_group_id,item_description,supplier_id from  product_details_master where item_category_id=$data[1] and status_active=1 and is_deleted=0"; 
		$arr=array(1=>$item_category,2=>$itemgroupArr,4=>$supplierArr);
		echo  create_list_view("list_view", "Item Account,Item Category,Item Group,Item Description,Supplier,Product ID", "70,110,150,150,100,70","780","400",0, $sql , "js_set_value", "id,item_description", "", 0, "0,item_category_id,item_group_id,0,supplier_id,0", $arr , "item_account,item_category_id,item_group_id,item_description,supplier_id,id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
		exit();
}

if ($action=="item_group_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('item_name_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
        <input type="text" id="item_name_id" />
    <?php
	$sql="SELECT id,item_name from  lib_item_group where item_category=$data[1] and status_active=1 and is_deleted=0"; //id=$data[1] and
	
	echo  create_list_view("list_view", "Item Name", "350","500","330",0, $sql , "js_set_value", "id,item_name", "", 1, "0", $arr , "item_name", "periodical_purchase_report_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	if ($cbo_company_name==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_name'";
	if ($cbo_item_category_id==0) $item_category_id=""; else $item_category_id=" and a.item_category='$cbo_item_category_id'";
	if ($item_account_id==0) $item_code=""; else $item_code=" and b.prod_id in ($item_account_id)";
	if ($item_group_id==0) $group_id=""; else $group_id=" and c.item_group_id='$item_group_id'";
	if ($cbo_supplier_name==0) $supplier_id=""; else $supplier_id=" and a.supplier_id='$cbo_supplier_name'";
	if ($cbo_store_name==0){ $store_id="";}else{$store_id=" and a.store_id='$cbo_store_name'";}
	if ($item_group_id==0){ $group_id="";}else{$group_id=" and c.item_group_id='$item_group_id'";}
	
 	if( $from_date==0 && $to_date==0 ) $transaction_date=""; else $transaction_date= " and b.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'";	
 	
	
 	//library array-------------------
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name"); 
	$storeArr = return_library_array("select id,store_name from lib_store_location where status_active=1 and is_deleted=0","id","store_name"); 
	$itemnameArr = return_library_array("select id,item_name from lib_item_creation where status_active=1 and is_deleted=0","id","item_name");
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	$pi_numArr = return_library_array("select id,pi_number from  com_pi_master_details where status_active=1 and is_deleted=0","id","pi_number");
	$wo_numArr = return_library_array("select id,wo_number from  wo_non_order_info_mst where status_active=1 and is_deleted=0","id","wo_number");
	$req_numArr = return_library_array("select id,requ_no from  inv_purchase_requisition_mst where status_active=1 and is_deleted=0","id","requ_no");
	

		$sql = "select a.item_category,a.receive_basis,a.receive_date,a.currency_id,a.recv_number,a.supplier_id,a.store_id,a.booking_id,
				b.transaction_date,(b.cons_quantity) as cons_quantity,b.cons_uom,(b.cons_rate) as cons_rate,(b.cons_amount) as cons_amount,
				c.id as pro_id,c.item_description,c.item_category_id,c.item_group_id,c.item_size 	
				from inv_receive_master a,inv_transaction b,product_details_master c 	
				where a.id=b.mst_id and a.company_id=b.company_id and a.store_id=b.store_id and a.item_category=b.item_category and a.receive_date=b.transaction_date and b.item_category in (8,9,10,11) and a.entry_form=20 and b.transaction_type=1 and c.id=b.prod_id
				$company_id $item_category_id $item_code  $supplier_id $store_id $transaction_date $group_id 
	 
		order by a.store_id,a.item_category,a.receive_date,a.recv_number";
	 echo $sql; 
	$result = sql_select($sql);	
	$r=1;
	ob_start();	
	?>
	<div style="width:100%;"> 
     <fieldset style="width:800px;">
        <table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
            <tr class="form_caption" style="border:none;">
                <td colspan="10" align="center" style="border:none;font-size:20px; font-weight:bold" ><strong>Peroidical Purchase Report- Detail</strong></td> 
            </tr>
            <tr style="border:none;">
                <td colspan="10" align="center" style="border:none; font-size:17px;"><strong>
                    Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?>  </strong>                              
                </td>
            </tr>
            <tr style="border:none;">
                <td colspan="10" align="center" style="border:none;font-size:12px; font-weight:bold">
                    <?php if($from_date!="" || $to_date!="")echo "From : ".change_date_format($from_date)."   To : ".change_date_format($to_date)."" ;?>
                </td>
            </tr>
            <tr style="border:none;">
                <td colspan="10" align="center" style="border:none;font-size:12px; font-weight:bold">
                    <?php if($cbo_supplier_name!=0)echo "Supplier : ".$supplierArr[$cbo_supplier_name]."" ;?>
                </td>
            </tr>
            <?php
			$item_category_array=array();
			$item_group_array=array();
			$store_loc_array=array();
			
		foreach($result as $row)
		{
			if(!in_array($row[csf('store_id')],$store_loc_array)) // Store Header
			{
				$store_loc_array[]=$row[csf('store_id')];
				?>
               	<tr>
                	<td colspan="10"><b>Store : <?php echo $storeArr[$row[csf('store_id')]]; ?></td>
            	</tr> 
			<?php 
			$st=1;
			} 
			
			if(!in_array($row[csf('item_group_id')],$item_category_array)) // Item Header
			{
				$item_category_array[]=$row[csf('item_group_id')];
				//print_r ($item_category_array);
/*				if($st!=1)
				{
*/					?>
                        <tr>
                            <td colspan="5"><b>Item Category : <?php echo $item_category[$row[csf('item_category_id')]]; ?></td>
                             <td colspan="5"><b>Item Group : <?php echo $itemgroupArr[$row[csf('item_group_id')]];//$itemgroupArr[$item_group_id]; ?></td>
                        </tr>
					<?php
/*				} $st++ ;
*/				?>
                </table>
                    <div style="width:800px;" id="scroll_body" > 
                    <table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_2" >
                        <thead>
                            <th width="50" >SL</th>
                            <th width="200" >Item Name</th>
                            <th width="70" >UoM</th>
                            <th width="100" >Quantity</th>
                            <th width="70" >Rate</th>
                            <th width="100" >Amount</th>
                            <th width="100" >Avg. Rate</th>
                        </thead>
                        <tbody>
				<?php
                }
            
				if ($r%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
				if($row[csf("transaction_type")]==1) 
					$stylecolor='style="color:#A61000"';
				else
					$stylecolor='style="color:#000000"'; 								
				
				$cons_amount=$row[csf('cons_amount')];
				$cons_amount_sum += $cons_amount;
				
				$cons_quantity=$row[csf('cons_quantity')];
				$cons_quantity_sum += $cons_quantity;
				$avg_rate=$cons_amount/$cons_quantity;
				
            ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $r; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $r; ?>">
                    <td align="center"><?php echo $r;?></td>
                    <td><?php echo $row['item_description']; ?></td>
                    <td align="center"><?php echo $unit_of_measurement[$row[csf('cons_uom')]]; ?></td>
                    <td align="right"><?php echo number_format($row[csf('cons_quantity')]); ?></td>
                    <td align="right"><?php echo number_format($row[csf('cons_rate')]); ?></td>
                    <td align="right"><?php echo number_format($row[csf('cons_amount')]); ?></td>
                    <td align="right"><?php echo number_format($avg_rate,2,'.','');//number_format($avg_rate); ?></td>
                </tr>
               </tbody>  
			<?php
/*			if(!in_array($row[csf('store_id')],$store_loc_array)) // Store Header
			{
				$store_loc_array[]=$row[csf('store_id')];
				if($st!=1)
				{
				?>
            <tfoot>
            	<tr>
                    <td align="right" colspan="7" ><strong>Grand Total : </strong></td>
                    <td align="right"><?php echo number_format($cons_amount_sum,0,'',','); ?></td>
                </tr>
            </tfoot>
                <?php } $st++ ;}
*/				$r++;
			}
			
			?>
           
            <tfoot>
            	<tr>
                    <td align="right" colspan="3" ><strong>Grand Total : </strong></td>
                    <td align="right"><strong><?php echo number_format($cons_quantity_sum,0,'',','); ?></strong></td>
                    <td align="right" colspan="2"><strong><?php echo number_format($cons_amount_sum,0,'',','); ?></strong></td>
                </tr>
            </tfoot>
       </table> 
     </div>
    </fieldset>
   </div>
     <?php
    //die;
	$html = ob_get_contents();
	ob_clean();
	//$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
	foreach (glob("*.xls") as $filename) {
	//if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc, $html);
	echo "$html**$filename"; 
	exit();	
}
?>