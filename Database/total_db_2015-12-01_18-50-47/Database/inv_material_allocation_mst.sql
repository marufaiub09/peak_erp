-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_material_allocation_mst`
--

DROP TABLE IF EXISTS `inv_material_allocation_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_material_allocation_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_no` varchar(25) NOT NULL,
  `po_break_down_id` text NOT NULL,
  `item_category` int(11) NOT NULL DEFAULT '0',
  `allocation_date` date NOT NULL,
  `fabric_des` text NOT NULL,
  `pre_cost_fabric_cost_id` int(11) NOT NULL DEFAULT '0',
  `booking_no` varchar(25) NOT NULL,
  `color_id` int(11) NOT NULL,
  `dia` varchar(50) NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `qnty` double NOT NULL,
  `qnty_break_down` text NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_material_allocation_mst`
--

LOCK TABLES `inv_material_allocation_mst` WRITE;
/*!40000 ALTER TABLE `inv_material_allocation_mst` DISABLE KEYS */;
INSERT INTO `inv_material_allocation_mst` VALUES (1,'PAL-15-00145','185,186,187,188',1,'2015-04-27','',0,'PAL-Fb-15-00077',0,'',308,373,'49.73_74.60_99.47_149.20',1,'2015-04-27 11:09:24',0,'0000-00-00 00:00:00',1,0),(2,'PAL-15-00074','104',1,'2015-05-04','',0,'PAL-Fb-15-00042',0,'',9,2,'2.00',3,'2015-05-04 14:58:44',2,'2015-05-04 15:20:21',1,0),(3,'PAL-15-00074','104',1,'2015-05-04','',0,'PAL-Fb-15-00042',0,'',10,5,'',2,'2015-05-04 15:33:27',2,'2015-05-04 15:33:46',1,0),(4,'PAL-15-00074','104',1,'2015-05-04','',0,'PAL-Fb-15-00042',0,'',10,7,'7.00',2,'2015-05-04 16:14:12',2,'2015-05-04 16:28:54',1,0),(5,'PAL-15-00026','25',1,'2015-05-09','',0,'PAL-Fb-15-00020',0,'',139,3,'3.00',2,'2015-05-09 11:53:08',0,'0000-00-00 00:00:00',1,0),(6,'PAL-15-00178','235',1,'2015-05-12','',0,'PAL-Fb-15-00100',0,'',308,652,'652.00',17,'2015-05-12 17:48:33',0,'0000-00-00 00:00:00',1,0),(7,'PAL-15-00061','92',1,'2015-05-26','',0,'PAL-Fb-15-00006',0,'',962,10,'10.00',2,'2015-05-26 09:28:37',0,'0000-00-00 00:00:00',1,0),(8,'PAL-15-00182','239',1,'2015-05-27','',0,'PAL-Fb-15-00099',0,'',962,147,'147.00',17,'2015-05-27 16:41:03',0,'0000-00-00 00:00:00',1,0),(9,'PAL-15-00183','240',1,'2015-05-27','',0,'PAL-Fb-15-00098',0,'',962,775,'775.00',17,'2015-05-27 16:57:33',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `inv_material_allocation_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
