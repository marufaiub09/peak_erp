-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_grey_prod_delivery_mst`
--

DROP TABLE IF EXISTS `pro_grey_prod_delivery_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_grey_prod_delivery_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_number_prefix` varchar(15) NOT NULL,
  `sys_number_prefix_num` int(11) NOT NULL,
  `sys_number` varchar(25) NOT NULL,
  `entry_form` int(11) NOT NULL DEFAULT '0',
  `order_status` int(11) NOT NULL DEFAULT '1' COMMENT '1=with order, 2=without order',
  `fin_pord_type` int(11) NOT NULL DEFAULT '0' COMMENT '1=with order, 2=sample without order',
  `delevery_date` date NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `knitting_source` int(2) NOT NULL DEFAULT '0',
  `knitting_company` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL DEFAULT '0',
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `current_delivery` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_grey_prod_delivery_mst`
--

LOCK TABLES `pro_grey_prod_delivery_mst` WRITE;
/*!40000 ALTER TABLE `pro_grey_prod_delivery_mst` DISABLE KEYS */;
INSERT INTO `pro_grey_prod_delivery_mst` VALUES (1,'PAL-GDS-15-',1,'PAL-GDS-15-00001',53,1,0,'2015-01-07',1,0,0,1,0,0,1,'2015-01-08 00:49:54',0,'0000-00-00 00:00:00',1,0),(2,'PAL-GDS-15-',2,'PAL-GDS-15-00002',0,1,0,'2015-03-23',1,0,0,1,30,0,5,'2015-03-23 15:05:05',0,'0000-00-00 00:00:00',1,0),(3,'PAL-GDS-15-',3,'PAL-GDS-15-00003',0,1,0,'2015-03-29',1,0,0,1,34,0,5,'2015-03-29 12:36:59',0,'0000-00-00 00:00:00',1,0),(4,'PAL-GDS-15-',4,'PAL-GDS-15-00004',0,1,0,'2015-03-29',1,0,0,1,34,0,5,'2015-03-29 12:54:59',5,'2015-03-29 12:56:40',1,0),(5,'PAL-GDS-15-',5,'PAL-GDS-15-00005',0,1,0,'2015-03-29',1,0,0,1,30,0,5,'2015-03-29 12:57:49',0,'0000-00-00 00:00:00',1,0),(6,'PAL-GDS-15-',6,'PAL-GDS-15-00006',0,1,0,'2015-03-30',1,0,0,1,30,0,5,'2015-03-30 11:47:56',0,'0000-00-00 00:00:00',1,0),(7,'PAL-GDS-15-',7,'PAL-GDS-15-00007',0,1,0,'2015-03-30',1,0,0,1,0,0,5,'2015-03-30 14:56:29',0,'0000-00-00 00:00:00',1,0),(8,'PAL-GDS-15-',8,'PAL-GDS-15-00008',0,1,0,'2015-03-31',1,0,0,1,34,0,5,'2015-03-31 09:43:45',0,'0000-00-00 00:00:00',1,0),(9,'PAL-GDS-15-',9,'PAL-GDS-15-00009',0,1,0,'2015-03-31',1,0,0,1,9,0,5,'2015-03-31 10:38:36',0,'0000-00-00 00:00:00',1,0),(10,'PAL-GDS-15-',10,'PAL-GDS-15-00010',0,1,0,'2015-03-31',1,0,0,1,14,0,5,'2015-03-31 10:48:57',0,'0000-00-00 00:00:00',1,0),(11,'PAL-GDS-15-',11,'PAL-GDS-15-00011',0,1,0,'2015-03-31',1,0,0,1,30,0,5,'2015-03-31 10:53:04',0,'0000-00-00 00:00:00',1,0),(12,'PAL-GDS-15-',12,'PAL-GDS-15-00012',0,1,0,'2015-03-31',1,0,0,1,14,0,5,'2015-03-31 11:11:19',0,'0000-00-00 00:00:00',1,0),(13,'PAL-GDS-15-',13,'PAL-GDS-15-00013',0,1,0,'2015-04-28',1,0,0,1,2,0,5,'2015-04-28 12:56:36',0,'0000-00-00 00:00:00',1,0),(14,'PAL-GDS-15-',14,'PAL-GDS-15-00014',0,1,0,'2015-04-25',1,0,0,1,2,0,2,'2015-04-29 16:22:35',0,'0000-00-00 00:00:00',1,0),(15,'PAL-GDS-15-',15,'PAL-GDS-15-00015',0,1,0,'2015-06-01',1,0,0,1,14,0,2,'2015-06-02 11:13:40',0,'0000-00-00 00:00:00',1,0),(16,'PAL-GDS-15-',16,'PAL-GDS-15-00016',0,1,0,'2015-03-12',1,0,0,1,8,0,2,'2015-08-17 16:22:10',2,'2015-08-17 16:43:38',1,0);
/*!40000 ALTER TABLE `pro_grey_prod_delivery_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
