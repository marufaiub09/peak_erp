<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$cbo_company=str_replace("'","",$cbo_company_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$costing_per_arr = return_library_array("select job_no, costing_per from wo_pre_cost_mst","job_no","costing_per"); 
	$tot_cost_arr = return_library_array("select job_no, cm_for_sipment_sche from wo_pre_cost_dtls","job_no","cm_for_sipment_sche"); 
	
	ob_start();	
	?>
        <table width="2100px" cellpadding="0" cellspacing="0" id="caption" align="center">
            <tr>
            <td align="center" width="100%" colspan="20" class="form_caption"><strong style="font-size:18px">Company Name:<?php echo $company_library[$cbo_company]; ?></strong></td>
            </tr> 
            <tr>  
               <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:14px"><?php echo $report_title; ?></strong></td>
            </tr>
            <tr>  
               <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:14px"> <?php echo "From : ".change_date_format(str_replace("'","",$txt_date_from))." To : ".change_date_format(str_replace("'","",$txt_date_to))."" ;?></strong></td>
            </tr>  
        </table>
        <table width="2100px " border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all"> 
            <thead>
            	<tr>
                    <th width="35" rowspan="3" valign="middle">SL</th>
                    <th width="80" rowspan="3" valign="middle">Production Date</th>
                    <th colspan="14">Shipping Status </th>
                    <th colspan="6">Remarks</th>
                    <th colspan="3">Ex-Factory Dtls</th>
                </tr>
            	<tr>
                    <th width="80" rowspan="2" valign="middle">Knitting </th>
                    <th width="80" rowspan="2" valign="middle">Finish Fabric</th>
                    <th width="80" rowspan="2" valign="middle">Printing</th>
                    <th width="80" rowspan="2" valign="middle">Emb.</th>
                    <th colspan="3">Cutting</th>
                    <th colspan="3">Sewing</th>
                    <th colspan="3">Finish</th>
                    <th width="80" rowspan="2" valign="middle">Carton</th>
                    <th colspan="3">Order Value (On Sewing Qty)</th>
                    <th colspan="3">SMV (On Sewing Qnty)</th>
                    <th rowspan="2" width="100">Ex-Factory Qty</th>
                    <th rowspan="2" width="80">Ex-Factory Value</th>
                    <th rowspan="2" width="">CM Value</th>
                </tr>
            	<tr>
                    <th width="80">In House</th>
                    <th width="80">Sub Contact</th>
                    <th width="80">Total</th>
                    <th width="80">In House</th>
                    <th width="80">Sub Contact</th>
                    <th width="80">Total</th>
                    <th width="80">In House</th>
                    <th width="80">Sub Contact</th>
                    <th width="80">Total</th>
                    <th width="100">In House</th>
                    <th width="80">Sub Contact</th>
                    <th width="100">Total</th>
                    <th width="100">SMV Available</th>
                    <th width="100">SMV Produced</th>
                    <th width="100">Efficyency</th>
                </tr>
            </thead>
        </table>
        <div style="max-height:425px; overflow-y:scroll; width:2120px" id="scroll_body">
        <table cellspacing="0" border="1" class="rpt_table"  width="2100px" rules="all" id="scroll_body" >
      <?php
	  
		$smv_source=return_field_value("smv_source","variable_settings_production","company_name in ($cbo_company) and variable_list=25 and status_active=1 and is_deleted=0");
		//echo $smv_source;die;
		if($smv_source=="") $smv_source=0; else $smv_source=$smv_source;
		$item_smv_array=array();
		//$smv_source=2;
		if($smv_source==3)
		{
			$sql_item="select b.id, a.sam_style, a.gmts_item_id from ppl_gsd_entry_mst a, wo_po_break_down b where b.job_no_mst=a.po_job_no and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1";
			$resultItem=sql_select($sql_item);
			foreach($resultItem as $itemData)
			{
				$item_smv_array[$itemData[csf('id')]][$itemData[csf('gmts_item_id')]]=$itemData[csf('sam_style')];
			}
		}
		else
		{
			$sql_item="select b.id, a.set_break_down, c.gmts_item_id, c.set_item_ratio, c.smv_pcs, c.smv_pcs_precost from wo_po_details_master a, wo_po_break_down b, wo_po_details_mas_set_details c where b.job_no_mst=a.job_no and b.job_no_mst=c.job_no and a.company_name in($cbo_company) and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1";
			$resultItem=sql_select($sql_item);
			foreach($resultItem as $itemData)
			{
				$item_smv_array[$itemData[csf('id')]][$itemData[csf('gmts_item_id')]]['smv_pcs']=$itemData[csf('smv_pcs')];
				$item_smv_array[$itemData[csf('id')]][$itemData[csf('gmts_item_id')]]['smv_pcs_precost']=$itemData[csf('smv_pcs_precost')];
			}
		}
		
		$tpdArr=array(); $tsmvArr=array();
        $tpd_data_arr=sql_select( "select a.pr_date, sum(a.target_per_hour*a.working_hour) tpd, sum(a.man_power*a.working_hour) tsmv from prod_resource_dtls a, prod_resource_mst b where b.id=a.mst_id and b.company_id in($cbo_company) and a.pr_date between '$date_from' and '$date_to' and a.is_deleted=0 and b.is_deleted=0 group by  a.pr_date");
        foreach($tpd_data_arr as $row)
        {
			$production_date=date("Y-m-d", strtotime($row[csf('pr_date')])); 
           // $tpdArr[$production_date]+=$row[csf('tpd')];
			 $tsmvArr[change_date_format($production_date)]['smv']=$row[csf('tsmv')]*60;
        } //var_dump($tsmvArr[$production_date]['smv']);

		
		
		$job_array=array(); 
		$job_sql="select a.id, a.unit_price, b.job_no, b.total_set_qnty from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 ";
		$job_sql_result=sql_select($job_sql);
		foreach($job_sql_result as $row)
		{
			$job_array[$row[csf("id")]]['unit_price']=$row[csf("unit_price")];
			$job_array[$row[csf("id")]]['job_no']=$row[csf("job_no")];
			$job_array[$row[csf("id")]]['total_set_qnty']=$row[csf("total_set_qnty")];
		}	  
	  
		  $total_knit=0;
		  $total_finishing=0;
		  $total_print=0;
		  $total_emb=0;
		  $total_cutting=0;
		  $total_cutting_inhouse=0;
		  $total_cutting_subcontract=0;
		  $total_sew=0;
		  $total_sew_inhouse=0;
		  $total_sew_subcontract=0;
		  $total_finishg=0;
		  $total_finish_inhouse=0;
		  $total_finish_subcontract=0;
		  $total_carton=0;
		 
		  $dtls_sql="SELECT production_date, po_break_down_id as po_breakdown_id,item_number_id,
					sum(CASE WHEN production_type =1 THEN production_quantity END) AS cutting_qnty,
					sum(CASE WHEN production_type =1 and production_source=1 THEN production_quantity END) AS cutting_qnty_inhouse,
					sum(CASE WHEN production_type =1 and production_source=3 THEN production_quantity END) AS cutting_qnty_outbound, 
					
					sum(CASE WHEN production_type =3 and embel_name=1 THEN production_quantity END) AS printing_qnty,
					sum(CASE WHEN production_type =3 and embel_name=1 and production_source=1 THEN production_quantity END) AS printing_qnty_inhouse,
					sum(CASE WHEN production_type =3 and embel_name=1 and production_source=3 THEN production_quantity END) AS printing_qnty_outbound, 
					
					sum(CASE WHEN production_type =3 and embel_name=2 THEN production_quantity END) AS emb_qnty,
					sum(CASE WHEN production_type =3 and embel_name=2 and production_source=1 THEN production_quantity END) AS emb_qnty_inhouse,
					sum(CASE WHEN production_type =3 and embel_name=2 and production_source=3 THEN production_quantity END) AS emb_qnty_outbound,
					 
					sum(CASE WHEN production_type =5 THEN production_quantity END) AS sewing_qnty,
					sum(CASE WHEN production_type =5 and production_source=1 THEN production_quantity END) AS sewingout_qnty_inhouse,
					sum(CASE WHEN production_type =5 and production_source=3 THEN production_quantity END) AS sewingout_qnty_outbound, 
					
					sum(CASE WHEN production_type =8 THEN production_quantity END) AS finish_qnty,
					sum(CASE WHEN production_type =8 and production_source=1 THEN production_quantity END) AS finish_qnty_inhouse, 
					sum(CASE WHEN production_type =8 and production_source=3 THEN production_quantity END) AS finish_qnty_outbound,
					sum(CASE WHEN production_type =8  THEN carton_qty END) AS carton_qty 
					from pro_garments_production_mst 
					where company_id like '$cbo_company' and production_date between '$date_from' and '$date_to' and is_deleted=0 and status_active=1 group by production_date, po_break_down_id, item_number_id order by production_date asc";
			
			 $dtls_sql_result=sql_select($dtls_sql);
			 $prod_date=array();$po_id=""; $po_sewing_qty=array();
			 foreach($dtls_sql_result as $row)
			 {
				 //if($po_id=="")$po_id=$row[csf("po_breakdown_id")]; else $po_id=$po_id.",".$row[csf("po_breakdown_id")];
				 
				 $production_date=date("Y-m-d", strtotime($row[csf('production_date')])); 
				 $prod_date[change_date_format($row[csf("production_date")])]['po_breakdown_id'].=$row[csf("po_breakdown_id")].",";
				 $prod_date[change_date_format($row[csf("production_date")])]['production_date']=$row[csf("production_date")];
				 $prod_date[change_date_format($row[csf("production_date")])]['printing_qnty']+=$row[csf("printing_qnty")];
				 $prod_date[change_date_format($row[csf("production_date")])]['emb_qnty']+=$row[csf("emb_qnty")];
				 $prod_date[change_date_format($row[csf("production_date")])]['cutting_qnty_inhouse']+=$row[csf("cutting_qnty_inhouse")];
				 $prod_date[change_date_format($row[csf("production_date")])]['cutting_qnty_outbound']+=$row[csf("cutting_qnty_outbound")];
				 $prod_date[change_date_format($row[csf("production_date")])]['cutting_qnty']+=$row[csf("cutting_qnty")];
				 $prod_date[change_date_format($row[csf("production_date")])]['sewingout_qnty_inhouse']+=$row[csf("sewingout_qnty_inhouse")];
				 $prod_date[change_date_format($row[csf("production_date")])]['sewingout_qnty_outbound']+=$row[csf("sewingout_qnty_outbound")];
				 $prod_date[change_date_format($row[csf("production_date")])]['sewing_qnty']+=$row[csf("sewing_qnty")];
				 
				 $item_smv=0;
				if($smv_source==2)
				{
					$item_smv=$item_smv_array[$row[csf("po_breakdown_id")]][$row[csf("item_number_id")]]['smv_pcs_precost'];
				}
				else if($smv_source==3)
				{
					$item_smv=$item_smv_array[$row[csf("po_breakdown_id")]][$row[csf("item_number_id")]];	
				}
				else
				{
					$item_smv=$item_smv_array[$row[csf("po_breakdown_id")]][$row[csf("item_number_id")]]['smv_pcs'];	
				}
				 
				 $prod_date[change_date_format($row[csf("production_date")])]['sewing_qnty_smv']+=$row[csf("sewing_qnty")]*$item_smv;
				 
				 $prod_date[change_date_format($row[csf("production_date")])]['finish_qnty_inhouse']+=$row[csf("finish_qnty_inhouse")];
				 $prod_date[change_date_format($row[csf("production_date")])]['finish_qnty_outbound']+=$row[csf("finish_qnty_outbound")];
				 $prod_date[change_date_format($row[csf("production_date")])]['finish_qnty']+=$row[csf("finish_qnty")];
				 $prod_date[change_date_format($row[csf("production_date")])]['carton_qty']+=$row[csf("carton_qty")];
				 
				 $prod_date[change_date_format($row[csf("production_date")])]['sewingout_value_inhouse']+=$row[csf("sewingout_qnty_inhouse")]*$job_array[$row[csf("po_breakdown_id")]]['unit_price'];
				 $prod_date[change_date_format($row[csf("production_date")])]['sewingout_value_outbound']+=$row[csf("sewingout_qnty_outbound")]*$job_array[$row[csf("po_breakdown_id")]]['unit_price'];
				 
				 $cm_value=0;
				 $sewing_qnty=$row[csf("sewing_qnty")];
				 $job_no=$job_array[$row[csf("po_breakdown_id")]]['job_no'];
				 $total_set_qnty=$job_array[$row[csf("po_breakdown_id")]]['total_set_qnty'];
				 $costing_per=$costing_per_arr[$job_no];
				 
				 if($costing_per==1) $dzn_qnty=12;
				 else if($costing_per==3) $dzn_qnty=12*2;
				 else if($costing_per==4) $dzn_qnty=12*3;
				 else if($costing_per==5) $dzn_qnty=12*4;
				 else $dzn_qnty=1;
							
				 $dzn_qnty=$dzn_qnty*$total_set_qnty;
				 $cm_value=($tot_cost_arr[$job_no]/$dzn_qnty)*$sewing_qnty;
				
				 $prod_date[change_date_format($row[csf("production_date")])]['cm_value']+=$cm_value;
			}
			//var_dump($po_sewing_qty);
			//$knited_query="select a.id, a.transaction_date as production_date, sum(b.quantity) as kniting_qnty, b.po_breakdown_id from  inv_transaction a, order_wise_pro_details b where a.id=b.trans_id and a.company_id like '$cbo_company' and a.transaction_date between '$date_from' and '$date_to' and a.transaction_type=1   and b.entry_form=2 and a.item_category=13 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.transaction_date";
			$knited_query="select a.receive_date as production_date, sum(b.grey_receive_qnty) as kniting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and a.company_id='$cbo_company' and a.receive_date between '$date_from' and '$date_to' and a.entry_form=2 and a.item_category=13 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.receive_date";
			
			$knited_query_result=sql_select($knited_query);
			$count_knit=count($knited_query_result);
			foreach( $knited_query_result as $knit_row)
			{
				$prod_date[change_date_format($knit_row[csf("production_date")])]['kniting_qnty']=$knit_row[csf("kniting_qnty")];
			}
			//var_dump($prod_datek);
			//echo $count_print=count($prod_date);
			$finish_query="select a.receive_date as production_date, sum(b.receive_qnty) as finishing_qnty from  inv_receive_master a, pro_finish_fabric_rcv_dtls b where a.id=b.mst_id and a.company_id='$cbo_company' and a.receive_date between '$date_from' and '$date_to' and a.entry_form=7 and a.item_category=2 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.receive_date";
			//echo $finish_query;
			$finish_query_result=sql_select($finish_query);
			$count_finish=count($finish_query_result);
			foreach( $finish_query_result as $finish_row)
			{
				$prod_date[change_date_format($finish_row[csf("production_date")])]['finishing_qnty']=$finish_row[csf("finishing_qnty")];
			}
			//var_dump($prod_date);
			
			$exfactory_res = sql_select("select ex_factory_date, po_break_down_id, sum(ex_factory_qnty) as ex_factory_qnty from pro_ex_factory_mst where is_deleted=0 and status_active=1 and ex_factory_date between '$date_from' and '$date_to' group by ex_factory_date, po_break_down_id");
			foreach($exfactory_res as $ex_row)
			{
				$prod_date[change_date_format($ex_row[csf("ex_factory_date")])]['ex_factory']+=$ex_row[csf("ex_factory_qnty")];
				$prod_date[change_date_format($ex_row[csf("ex_factory_date")])]['ex_factory_val']+=$ex_row[csf("ex_factory_qnty")]*($job_array[$ex_row[csf("po_break_down_id")]]['unit_price']/$job_array[$ex_row[csf("po_break_down_id")]]['total_set_qnty']);
			}
			//var_dump($exfactory_arr);
			
			
			 ksort($prod_date);
			 $i=1;
			 $printing=0; $embing=0; $cuting_in=0; $cuting_out=0; $cuting=0; $sewing_in=0; $sewing_out=0; $sewing=0; $finish_in=0; $finish_out=0; $finish=0; $carton=0; $ord_in=0; $ord_out=0; $ord_tot=0;
			 
			/*for($j=0;$j<$datediff;$j++)
			{
				//$newDate =add_date(str_replace("'","",$txt_date_from),$j);
				$newdate =change_date_format(add_date(str_replace("'","",$txt_date_from),$j),'','',1).'<br>';
				echo $newdate;//$full_date=$newdate.'<br>';
				$days_months=explode('-',$newDate);
				//echo date("d-M",strtotime($newDate));
			}
			 
			foreach ( $prod_date as $date_all=>$val )*/
			
			for($j=0;$j<$datediff;$j++)
			{
				$date_all=add_date(str_replace("'","",$txt_date_from),$j);
				$newdate =change_date_format($date_all);
				?>
				<tr align="center"  bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                <?php
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
				$po_id=$prod_date[$newdate]['po_breakdown_id'];
				//$prod_date[$newdate]['sewing_qnty_smv'].'='.$tsmvArr[$newdate]['smv'];
				$produce_qty=$prod_date[$newdate]['sewing_qnty_smv'];
				// echo $produce_qty;
				//echo $tsmvArr[$newdate]['smv'];die;
				 $effiecy_aff_perc=$produce_qty/$tsmvArr[$newdate]['smv']*100;
				//$achv_smv+=$production_quantity*$item_smv;
	
				?>
                    <td width="35" ><?php echo $i; ?></td>
					<td width="80"><?php echo $newdate; $date=$date_all; //change_date_format($date_all); ?></td>
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'grey_receive_qnty')"><?php  echo number_format($prod_date[$newdate]['kniting_qnty'],2); ?></a><?php $total_knit+=$prod_date[$newdate]['kniting_qnty']; //echo $po_id;  ?></td>
					<td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'finish_receive_qnty')"> <?php echo number_format($prod_date[$newdate]['finishing_qnty'],2); ?></a><?php $total_finishing+=$prod_date[$newdate]['finishing_qnty'];?></td>
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'printreceived')"><?php echo number_format($prod_date[$newdate]['printing_qnty'],2); ?></a> <?php $total_print+=$prod_date[$newdate]['printing_qnty']; if($val['printing_qnty']>0) $printing++;  ?></td>
					<td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'emb_qnty')"><?php echo number_format($prod_date[$newdate]['emb_qnty'],2); ?></a><?php $total_emb+=$prod_date[$newdate]['emb_qnty']; if($prod_date[$newdate]['emb_qnty']>0) $embing++; ?></td>
                    
					<td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'cutting_inhouse')"><?php echo number_format($prod_date[$newdate]['cutting_qnty_inhouse'],2); ?></a><?php $total_cutting_inhouse+=$prod_date[$newdate]['cutting_qnty_inhouse']; if($prod_date[$newdate]['cutting_qnty_inhouse']>0) $cuting_in++; ?></td>
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'cutting_subcontract')"><?php echo number_format($prod_date[$newdate]['cutting_qnty_outbound'],2); ?></a><?php $total_cutting_outbound+=$prod_date[$newdate]['cutting_qnty_outbound']; if($prod_date[$newdate]['cutting_qnty_outbound']>0) $cuting_out++; ?></td>
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'cutting')"><?php echo number_format($prod_date[$newdate]['cutting_qnty'],2); ?></a><?php $total_cutting+=$prod_date[$newdate]['cutting_qnty']; if($prod_date[$newdate]['cutting_qnty']>0) $cuting++; ?></td>
                    
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'sewingout_inhouse')"><?php echo number_format($prod_date[$newdate]['sewingout_qnty_inhouse'],2); ?></a><?php  $total_sew_inhouse+=$prod_date[$newdate]['sewingout_qnty_inhouse']; if($prod_date[$newdate]['sewingout_qnty_inhouse']>0) $sewing_in++; ?></td>
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'sewingout_subcontract')"><?php echo number_format($prod_date[$newdate]['sewingout_qnty_outbound'],2); ?></a><?php $total_sew_outbound+=$prod_date[$newdate]['sewingout_qnty_outbound']; if($prod_date[$newdate]['sewingout_qnty_outbound']>0) $sewing_out++; ?></td>
					<td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'sewingout')"><?php echo number_format($prod_date[$newdate]['sewing_qnty'],2); ?></a><?php $total_sew+=$prod_date[$newdate]['sewing_qnty']; if($prod_date[$newdate]['sewing_qnty']>0) $sewing++; ?></td>
					
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'finish_inhouse')"><?php echo number_format($prod_date[$newdate]['finish_qnty_inhouse'],2); ?></a><?php $total_finishg_inhouse+=$prod_date[$newdate]['finish_qnty_inhouse']; if($prod_date[$newdate]['finish_qnty_inhouse']>0) $finish_in++; ?></td>
					<td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'finish_subcontract')"><?php echo number_format($prod_date[$newdate]['finish_qnty_outbound'],2); ?></a><?php $total_finish_outbound+=$prod_date[$newdate]['finish_qnty_outbound']; if($prod_date[$newdate]['finish_qnty_outbound']>0) $finish_out++; ?></td>
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'finish')"><?php echo number_format($prod_date[$newdate]['finish_qnty'],2); ?></a><?php $total_finish+=$prod_date[$newdate]['finish_qnty']; if($prod_date[$newdate]['finish_qnty']>0) $finish++; ?></td>
                    <td width="80" align="right"><a href="##" onclick="openmypage2(<?php echo "'".$date."'"  ?>,<?php echo "'".$cbo_company."'"  ?>,<?php echo "'".$po_id."'"  ?>,'carton')"><?php echo number_format($prod_date[$newdate]['carton_qty'],2); ?></a><?php $total_carton+=$prod_date[$newdate]['carton_qty']; if($prod_date[$newdate]['carton_qty']>0) $carton++; ?></td>
                    
                    <td width="100" align="right"><?php $swing_val_inhouse=$prod_date[$newdate]['sewingout_value_inhouse']; echo number_format($swing_val_inhouse,2);  $total_sewin_order_value+=$swing_val_inhouse; if($swing_val_inhouse>0) $ord_in++; ?></td>
                    <td width="80" align="right"><?php
					$swing_val_outbound=$prod_date[$newdate]['sewingout_value_outbound']; echo number_format($swing_val_outbound,2);  $total_sewout_order_value+=$swing_val_outbound; if($swing_val_outbound>0) $ord_out++; ?></td>
                    <td width="100" align="right"><?php echo number_format(($swing_val_inhouse+$swing_val_outbound),2);  $total_sew_order_value+=($swing_val_inhouse+$swing_val_outbound); if(($swing_val_inhouse+$swing_val_outbound)>0) $ord_tot++; ?></td>
                     <td width="100" align="right"><?php $swing_val_availble=$tsmvArr[$newdate]['smv']; echo number_format($swing_val_availble,2);  $total_swing_val_availble_value+=$swing_val_availble; ?></td>
                    <td width="100" align="right"><?php
					$swing_val_produced=$produce_qty; echo number_format($swing_val_produced,2);  $total_sewout_order_value_produced+=$swing_val_produced; ?></td>
                    <td width="100" align="right"><?php echo number_format(($effiecy_aff_perc),2);  $total_effiecy_aff_perc+=$effiecy_aff_perc; ?></td>

                    <td width="100" align="right"><?php $ex_factory_qty=$prod_date[$newdate]['ex_factory']; echo number_format($ex_factory_qty,2);  $total_ex_factory_qty+=$ex_factory_qty; if($ex_factory_qty>0) $ex_fac++; ?></td>
                    <td width="80" align="right"><?php 
						$ex_factory_value=$prod_date[$newdate]['ex_factory_val']; echo number_format($ex_factory_value,2);  $total_ex_factory_value+=$ex_factory_value; if($ex_factory_value>0) $ex_val++; ?></td>
                    <td width="" align="right">
						<?php 
							$cm_value=$prod_date[$newdate]['cm_value'];
                        	echo number_format($cm_value,2); $total_cm_value+=$cm_value;  if($cm_value>0) $cm_val++;
                        ?>
                    </td>
				</tr>
			<?php
		$i++;
				//}
	}
	?>
    </table>
    <table width="2100px " border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all"> 
        <tfoot>
            <tr>
                <th width="35" >&nbsp;</th>
                <th width="80" >Total</th>
                <th width="80" ><?php echo number_format($total_knit,2); ?></th>
                <th width="80" ><?php echo number_format($total_finishing,2); ?></th>
                <th width="80" ><?php echo number_format($total_print,2); ?></th>
                <th width="80" ><?php echo number_format($total_emb,2); ?></th>
                <th width="80"><?php echo number_format($total_cutting_inhouse,2); ?></th>
                <th width="80"><?php echo number_format($total_cutting_outbound,2); ?></th>
                <th width="80"><?php echo number_format($total_cutting,2); ?></th>
                <th width="80"><?php echo number_format($total_sew_inhouse,2); ?></th>
                <th width="80"><?php echo number_format($total_sew_outbound,2); ?></th>
                <th width="80"><?php echo number_format($total_sew,2); ?></th>
                <th width="80"><?php echo number_format($total_finishg_inhouse,2); ?></th>
                <th width="80"><?php echo number_format($total_finish_outbound,2); ?></th>
                <th width="80"><?php echo number_format($total_finish,2); ?></th>
                <th width="80"><?php echo number_format($total_carton,2); ?></th>
                <th width="100"><?php echo number_format($total_sewin_order_value,2); ?></th>
                <th width="80"><?php echo number_format($total_sewout_order_value,2); ?></th>
                <th width="100"><?php echo number_format($total_sew_order_value,2); ?></th>
                
                <th width="100"><?php echo number_format($total_swing_val_availble_value,2); ?></th>
                <th width="100"><?php echo number_format($total_sewout_order_value_produced,2); ?></th>
                <th width="100"><?php  //echo number_format($total_effiecy_aff_perc,2); ?></th>
                
                <th width="100"><?php echo number_format($total_ex_factory_qty,2); ?></th>
                <th width="80"><?php echo number_format($total_ex_factory_value,2); ?></th>
                <th width=""><?php echo number_format($total_cm_value,2); ?></th>
            </tr>
            <tr>
                <th width="35" >&nbsp;</th>
                <th width="80" >Avg.</th>
                <th width="80" ><?php echo number_format($total_knit/$count_knit,2); ?></th>
                <th width="80" ><?php echo number_format($total_finishing/$count_finish,2); ?></th>
                <th width="80" ><?php echo number_format($total_print/$printing,2); ?></th>
                <th width="80" ><?php echo number_format($total_emb/$embing,2); ?></th>
                <th width="80"><?php echo number_format($total_cutting_inhouse/$cuting_in,2); ?></th>
                <th width="80"><?php echo number_format($total_cutting_outbound/$cuting_out,2); ?></th>
                <th width="80"><?php echo number_format($total_cutting/ $cuting,2); ?></th>
                <th width="80"><?php echo number_format($total_sew_inhouse/$sewing_in,2); ?></th>
                <th width="80"><?php echo number_format($total_sew_outbound/$sewing_out,2); ?></th>
                <th width="80"><?php echo number_format($total_sew/$sewing,2); ?></th>
                <th width="80"><?php echo number_format($total_finishg_inhouse/$finish_in,2); ?></th>
                <th width="80"><?php echo number_format($total_finish_outbound/$finish_out,2); ?></th>
                <th width="80"><?php echo number_format($total_finish/$finish,2); ?></th>
                <th width="80"><?php echo number_format($total_carton/$carton,2); ?></th>
                <th width="100"><?php echo number_format($total_sewin_order_value/$ord_in,2); ?></th>
                <th width="80"><?php echo number_format($total_sewout_order_value/$ord_out,2); ?></th>
                <th width="100"><?php echo number_format($total_sew_order_value/$ord_tot,2); ?></th>
                <th width="100"><?php //echo number_format($total_sewin_order_value/$ord_in,2); ?></th>
                <th width="100"><?php //echo number_format($total_sewout_order_value/$ord_out,2); ?></th>
                <th width="100"><?php //echo number_format($total_sew_order_value/$ord_tot,2); ?></th>
                <th width="100"><?php echo number_format($total_ex_factory_qty/$ex_fac,2); ?></th>
                <th width="80"><?php echo number_format($total_ex_factory_value/$ex_val,2); ?></th>
                <th width=""><?php echo number_format($total_cm_value/$cm_val,2); ?></th>
            </tr>
        </tfoot>
    </table>
    </div>
    <?php 
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();      
}

if($action=="grey_receive_qnty")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date." ".$po_id;die;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Knitting <?php echo change_date_format($date);  ?></b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Knitting Qnty</th>
           </tr>
        </thead>
    <?php
		$order_array=array();
		$po_sql="select b.id, a.buyer_name, a.style_ref_no, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0";
		$po_sql_result=sql_select($po_sql);
		foreach ($po_sql_result as $row)
		{
			$order_array[$row[csf('id')]]['po_number']=$row[csf('po_number')];
			$order_array[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')];
			$order_array[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
		}
		
        $total_grey_receive_qnty=0;
		if($db_type==0)
		{
			$knited_query="select a.receive_date as production_date, sum(c.quantity) as kniting_qnty, c.po_breakdown_id from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id='$company_id' and a.receive_date='$date' and a.entry_form=2 and c.entry_form=2 and c.trans_type=1 and a.item_category=13 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 group by a.receive_date, c.po_breakdown_id";
		}
		else if($db_type==2)
		{	$date2=change_date_format($date,'','',1);
			$knited_query="select a.receive_date as production_date, sum(c.quantity) as kniting_qnty, c.po_breakdown_id from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id='$company_id' and a.receive_date='$date2' and a.entry_form=2 and c.entry_form=2 and c.trans_type=1 and a.item_category=13 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 group by a.receive_date, c.po_breakdown_id";
		}
		
		$knited_query_result=sql_select($knited_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($knited_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $order_array[$row[csf('po_breakdown_id')]]['po_number']; ?></td>
                <td width="100px"><?php echo $buyerArr[$order_array[$row[csf('po_breakdown_id')]]['buyer_name']]; ?></td>
                <td width="120px"><?php echo $order_array[$row[csf('po_breakdown_id')]]['style_ref_no']; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('kniting_qnty')],2); $total_grey_receive_qnty+=$row[csf('kniting_qnty')]; ?></td>
			</tr>
			<?php
			$i++;
        }//while ($row=mysql_fetch_array($company_sql)) 
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_grey_receive_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="finish_receive_qnty")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date." ".$po_id;die;//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Finishing <?php echo change_date_format($date);  ?></b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Finishing Qnty</th>
           </tr>
        </thead>
    <?php
		$order_array=array();
		$po_sql="select b.id, a.buyer_name, a.style_ref_no, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0";
		$po_sql_result=sql_select($po_sql);
		foreach ($po_sql_result as $row)
		{
			$order_array[$row[csf('id')]]['po_number']=$row[csf('po_number')];
			$order_array[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')];
			$order_array[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
		}        
		
		$total_finish_qnty=0;
		if($db_type==0)
		{
			$finish_query="select a.receive_date as production_date, sum(c.quantity) as finishing_qnty, c.po_breakdown_id from  inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id='$company_id' and a.receive_date='$date' and a.entry_form=7 and a.item_category=2 and c.entry_form=7 and c.trans_type=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.receive_date, c.po_breakdown_id";
		}
		else if($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$finish_query="select a.receive_date as production_date, sum(c.quantity) as finishing_qnty, c.po_breakdown_id from  inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id='$company_id' and a.receive_date='$date2' and a.entry_form=7 and a.item_category=2 and c.entry_form=7 and c.trans_type=1 and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 group by a.receive_date, c.po_breakdown_id";
		}
		
		$finish_query_result=sql_select($finish_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($finish_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $order_array[$row[csf('po_breakdown_id')]]['po_number']; ?></td>
                <td width="100px"><?php echo $buyerArr[$order_array[$row[csf('po_breakdown_id')]]['buyer_name']]; ?></td>
                <td width="120px"><?php echo$order_array[$row[csf('po_breakdown_id')]]['style_ref_no']; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('finishing_qnty')],2); $total_finish_qnty+=$row[csf('finishing_qnty')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_finish_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="printreceived")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Printing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Printing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_printing_qnty=0;
		if($db_type==0)
		{
			$printing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=3 and embel_name=1 and status_active=1 and is_deleted=0 ";
		}
		else if($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$printing_query="select sum(production_quantity) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=3 and embel_name=1 and status_active=1 and is_deleted=0  group by po_break_down_id";
		}
	
		$printing_query_result=sql_select($printing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($printing_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id)";
			}
			$query_po_break_down_result=sql_select($query_po_break_down);
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_printing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_printing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="emb_qnty")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Embellishment (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Emb. Qnty</th>
           </tr>
        </thead>
    <?php
        $total_embellishment_qnty=0;
		if($db_type==0)
		{
			$embellishment_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=3 and embel_name=2 and status_active=1 and is_deleted=0 ";
		}
		else if($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$embellishment_query="select  sum(production_quantity) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=3 and embel_name=2 and status_active=1 and is_deleted=0  group by po_break_down_id";
		}
		
		$embellishment_query_result=sql_select($embellishment_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($embellishment_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id) group by a.id,a.buyer_name,a.style_ref_no,b.po_number";
			}
			$query_po_break_down_result=sql_select($query_po_break_down);
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_embellishment_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_embellishment_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="cutting_inhouse")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Cutting (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Cutting Qnty</th>
           </tr>
        </thead>
    <?php
        $total_cutting_qnty=0;
		if($db_type==0)
		{
			$cutting_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=1 and production_source=1 and status_active=1 and is_deleted=0 ";
		}
		else if($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$cutting_query="select sum(production_quantity) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=1 and production_source=1 and status_active=1 and is_deleted=0 group by po_break_down_id";
		}
		//echo $printing_query;
		$cutting_query_result=sql_select($cutting_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($cutting_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id)";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_cutting_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_cutting_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="cutting_subcontract")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Cutting (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Cutting Qnty</th>
           </tr>
        </thead>
    <?php
        $total_cutting_qnty=0;
		if($db_type==0)
		{
			$cutting_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=1 and production_source=3 and status_active=1 and is_deleted=0 ";
		}
		else if($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$cutting_query="select sum(production_quantity) as production_quantity,po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=1 and production_source=3 and status_active=1 and is_deleted=0 group by po_break_down_id";
		}
		//echo $cutting_query;
		$cutting_query_result=sql_select($cutting_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($cutting_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id)";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_cutting_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_cutting_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="cutting")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Cutting (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Cutting Qnty</th>
           </tr>
        </thead>
    <?php
        $total_cutting_qnty=0;
		if($db_type==0)
		{
			$cutting_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=1 and status_active=1 and is_deleted=0 ";
		}
		else if($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$cutting_query="select sum(production_quantity) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=1 and status_active=1 and is_deleted=0  group by po_break_down_id";
		}
		
		$cutting_query_result=sql_select($cutting_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($cutting_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
				
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id) ";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_cutting_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_cutting_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="sewingout_subcontract")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Sewing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Sewing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_sewing_qnty=0;
		if($db_type==0)
		{
			$sewing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=5 and production_source=3 and status_active=1 and is_deleted=0 ";
		}
		else if($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$sewing_query="select sum(production_quantity) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=5 and production_source=3 and status_active=1 and is_deleted=0 group by po_break_down_id";
		}
		//echo $sewing_query;
		$sewing_query_result=sql_select($sewing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($sewing_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id) ";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_sewing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_sewing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="sewingout_inhouse")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Sewing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Sewing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_sewing_qnty=0;
		if($db_type==0)
		{
			$sewing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=5 and production_source=1 and status_active=1 and is_deleted=0 ";
		}
		elseif($db_type==2)
		{
			 $date2=change_date_format($date,'','',1);
			$sewing_query="select  sum(production_quantity) as production_quantity,  po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=5 and production_source=1 and status_active=1 and is_deleted=0  group by po_break_down_id";
		}
		
		$sewing_query_result=sql_select($sewing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($sewing_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id) ";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_sewing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_sewing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}
/*
if($action=="sewingout_subcontract")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Sewing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Sewing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_sewing_qnty=0;
		if($db_type==0)
			{
			$sewing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=5 and production_source=3 and status_active=1 and is_deleted=0 ";
			
			}
			
	 if($db_type==2)
			{
			$sewing_query="select  sum(production_quantity) as production_quantity,  po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=5 and production_source=3 and status_active=1 and is_deleted=0 group by po_break_down_id";
			
			}
		echo $sewing_query;
		$sewing_query_result=sql_select($sewing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($sewing_query_result as $row)  
        {
			if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";	
				$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id) group by b.po_number";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_sewing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_sewing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}
*/
if($action=="sewingout")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Sewing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Sewing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_sewing_qnty=0;
		if($db_type==0)
		{
			$sewing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=5 and status_active=1 and is_deleted=0 ";
		}
		elseif($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$sewing_query="select sum(production_quantity) as production_quantity,  po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=5 and status_active=1 and is_deleted=0 group by po_break_down_id ";
		}
		
		//echo $sewing_query;
		$sewing_query_result=sql_select($sewing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($sewing_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id) ";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_sewing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_sewing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="finish_inhouse")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Finishing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Finishing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_finishing_qnty=0;
		if($db_type==0)
		{
			$finishing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=8 and production_source=1 and status_active=1 and is_deleted=0 ";
		}
		elseif($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$finishing_query="select id,sum(production_quantity) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=8 and production_source=1 and status_active=1 and is_deleted=0 group by id,po_break_down_id";
		}
		
		$finishing_query_result=sql_select($finishing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($finishing_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id)";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_finishing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_finishing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="finish_subcontract")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Finishing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Finishing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_finishing_qnty=0;
		if($db_type==0)
		{
			$finishing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=8 and production_source=3 and status_active=1 and is_deleted=0 ";
		}
		elseif($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$finishing_query="select  sum(production_quantity) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=8 and production_source=3 and status_active=1 and is_deleted=0  group by po_break_down_id";
		}
		//echo $sewing_query;
		$finishing_query_result=sql_select($finishing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($finishing_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id)";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_finishing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_finishing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="finish")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Finishing (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Finishing Qnty</th>
           </tr>
        </thead>
    <?php
        $total_finishing_qnty=0;
		if($db_type==0)
		{
			$finishing_query="select id, sum(production_quantity) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=8 and status_active=1 and is_deleted=0 ";
		}
		elseif($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$finishing_query="select  sum(production_quantity) as production_quantity,po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=8 and status_active=1 and is_deleted=0 group by po_break_down_id";
		}
		//echo $sewing_query;
		$finishing_query_result=sql_select($finishing_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($finishing_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
				$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id)";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_finishing_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_finishing_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}

if($action=="carton")
{
	extract($_REQUEST);
	echo load_html_head_contents("Production Report", "../../../", 1, 1,$unicode,'','');	
	//echo $date;//company_id
	?>
    <table border="1" class="rpt_table" rules="all" width="100%">
        <thead>
            <tr>
                <th colspan="5"><b>Carton (<?php echo change_date_format($date);  ?>)</b></th>
            </tr>
            <tr>
            	<th width="30px">SL</th>
                <th width="100px">Po Number</th>
                <th width="100px">Buyer</th>
                <th width="120px">Style</th>
                <th width="90px">Carton Qnty</th>
           </tr>
        </thead>
    <?php
        $total_carton_qnty=0;
		if($db_type==0)
		{
			$carton_query="select id, sum(carton_qty) as production_quantity, production_date, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date' and production_type=8 and status_active=1 and is_deleted=0 ";
		}
		elseif($db_type==2)
		{
			$date2=change_date_format($date,'','',1);
			$carton_query="select  sum(carton_qty) as production_quantity, po_break_down_id from pro_garments_production_mst where company_id like '$company_id' and production_date='$date2' and production_type=8 and status_active=1 and is_deleted=0 group by po_break_down_id";
		}
		//echo $carton_query;
		$carton_query_result=sql_select($carton_query);
        $buyerArr = return_library_array("select id,buyer_name from lib_buyer ","id","buyer_name");
        $i=1;
        foreach ($carton_query_result as $row)  
        {
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
			
			$po_break_id=$row[csf('po_break_down_id')];
			if($po_break_id!="")
			{
				$query_po_break_down="select a.id,a.buyer_name,a.style_ref_no,b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_break_id) ";
				$query_po_break_down_result=sql_select($query_po_break_down);
			}
			
			foreach ($query_po_break_down_result as $rows)
			{
				$po=$rows[csf('po_number')];
				$buyer=$rows[csf('buyer_name')];
				$style=$rows[csf('style_ref_no')];
			}
			?>
			<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30px"><?php echo $i; ?></td>
                <td width="100px"><?php echo $po; ?></td>
                <td width="100px"><?php echo $buyerArr[$buyer]; ?></td>
                <td width="120px"><?php echo $style; ?></td>
                <td width="90px" align="right"><?php echo number_format($row[csf('production_quantity')],2); $total_carton_qnty+=$row[csf('production_quantity')]; ?></td>
			</tr>
			<?php
			$i++;
        }
        ?>
        <tfoot>
            <th colspan="4">Total</th>
            <th width="90px"><?php echo number_format($total_carton_qnty,2) ?></th>
        </tfoot>
    </table>
	<?php
    exit();	
}
?>