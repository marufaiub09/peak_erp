<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Scrap Sales Entry 
Functionality	:	
JS Functions	:
Created by		:	Kausar 
Creation date 	: 	10-05-2015
Updated by 		: 	
Update date		: 	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Scrap Sales Entry", "../../", 1, 1,'','1',''); 

?>
<script>

	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<?php echo $permission; ?>';

	function openmypage_ItemDescription()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var cbo_category_id = $('#cbo_category_id').val();
		
		if (form_validation('cbo_company_id*cbo_category_id','Company*Item Category')==false)
		{
			return;
		}
		else
		{ 	
			var title = 'Item Description Info Pop Up';	
			var page_link = 'requires/scrap_sales_entry_controller.php?cbo_company_id='+cbo_company_id+'&cbo_category_id='+cbo_category_id+'&action=itemDescription_popup';
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=540px,height=400px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]
				var prod_data=this.contentDoc.getElementById("hidden_prod_id").value;
				var data=prod_data.split("__");
				if(prod_data!="")
				{
					$('#txt_prodid').val(data[0]);
					$('#txt_itemdes').val(data[1]);
					$('#cbo_rejuomid').val(data[2]);	
					$('#txt_rejqty').val(data[3]);
					$('#trans_id').val(data[4]);
				}
			}
		}
	}
	
	function sales_qty_uom(val)
	{
		var rej_uom=$('#cbo_rejuomid').val();
		var rej_qty=$('#txt_rejqty').val();
		if(rej_uom==val)
		{
			$('#txt_salesqty').val(rej_qty);
		}
		else
		{
			$('#txt_salesqty').val('');
		}
	}
	
	function amount_calculation(val)
	{
		var sales_qty=$('#txt_salesqty').val()*1;
		var tot_amount=sales_qty*val;
		$('#txt_salesamount').val(tot_amount);
	}
	
	function openpage_customer()
	{
		var cbo_company_id = $('#cbo_company_id').val();

		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
			
		var title = 'All Customer Pop Up';	
		var page_link = 'requires/scrap_sales_entry_controller.php?cbo_company_id='+cbo_company_id+'&action=customer_search_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=400px,height=400px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var customer_id=this.contentDoc.getElementById("hidden_customer_id").value; //Access form field with id="emailfield"
			var customer_name=this.contentDoc.getElementById("hidden_customer_no").value; //Access form field with id="emailfield" 
			
			$('#txt_customer_id').val(customer_id);
			$('#txt_customer_no').val(customer_name);
			
			//show_list_view(hidden_order_id,'create_itemDesc_search_list_view','list_fabric_desc_container','requires/trims_issue_entry_controller','');
		}
	}
	
		
	function fnc_scrap_sales(operation)
	{
		if(operation==4)
		{
			 var report_title=$( "div.form_caption" ).html();
			//  generate_report_file( $('#cbo_company_id').val()+'*'+$('#update_id').val()+'*'+$('#txt_system_id').val()+'*'+report_title,'scrap_sales_entry_controller');

			print_report( $('#cbo_company_id').val()+'*'+$('#update_id').val()+'*'+$('#txt_system_id').val()+'*'+report_title, "scrap_sales_challan_print", "requires/scrap_sales_entry_controller" ) 
			 return;
		}
		else if(operation==0 || operation==1 || operation==2)
		{
			if(operation==2)
			{
				show_msg('13');
				return;
			}
			
			if( form_validation('cbo_company_id*cbo_category_id*txt_selling_date*cbo_pay_term*cbo_currency_id','Company*Item Category*Selling Date*Pay Term*Currency')==false )
			{
				return;
			}
			
			if($("#txt_salesqty").val()*1>$("#txt_rejqty").val()*1) //Aziz
			{
				alert("Reject qty Quantity Can not be Greater Than Sales Qty.");
				$("#txt_salesqty").focus();
				return;
			}
			
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('update_id*txt_system_id*cbo_company_id*cbo_category_id*txt_selling_date*cbo_pay_term*txt_customer_id*cbo_currency_id*txt_remarks*txt_prodid*trans_id*cbo_rejuomid*txt_rejqty*cbo_salesuomid*txt_salesqty*txt_salesrate*txt_salesamount*dtls_update_id',"../../");
			//alert (data);
			freeze_window(operation);
			
			http.open("POST","requires/scrap_sales_entry_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange =fnc_scrap_sales_respon;
		}
	}
	
	function fnc_scrap_sales_respon()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split('**');	
				
			show_msg(reponse[0]);
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[2];
				document.getElementById('txt_system_id').value = reponse[1];
				//$('#cbo_company_id').attr('disabled','disabled');
				
				reset_form('','','txt_prodid*txt_itemdes*cbo_rejuomid*txt_rejqty*cbo_salesuomid*txt_salesqty*txt_salesrate*txt_salesamount','','','');
				show_list_view(reponse[2],'show_dtls_listview','div_details_list_view','requires/scrap_sales_entry_controller','');
				//show_list_view(reponse[1],'show_dtls_listview','div_details_list_view','requires/scrap_sales_entry_controller','');
				set_button_status(0, permission, 'fnc_scrap_sales',1,1);
			}
			release_freezing();	
		}
	}
	
	function openmypage_systemId()
	{
		var company_id = $('#cbo_company_id').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var page_link='requires/scrap_sales_entry_controller.php?company_id='+company_id+'&action=scrap_sales_popup_search';
			var title='Trims Issue Form';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px,height=390px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var sys_id=this.contentDoc.getElementById("hidden_mst_id").value;

				if(sys_id!="")
				{
					//alert (sys_id)
					freeze_window(5);
					//reset_form('trimsissue_1','div_details_list_view','','','','cbo_company_id');
					get_php_form_data(sys_id, "populate_data_from_scrap_sales", "requires/scrap_sales_entry_controller" );
					
					show_list_view(sys_id,'show_dtls_listview','div_details_list_view','requires/scrap_sales_entry_controller','');
					set_button_status(0, permission, 'fnc_scrap_sales',1,1);
					release_freezing();
				}
							 
			}
		}
	}
		
</script>
<body onLoad="set_hotkey()">
<div  align="center" style="width:100%;">
	<?php echo load_freeze_divs ("../../",$permission);  ?><br />    		 
    <form name="scrapsales_1" id="scrapsales_1" autocomplete="off" >
    <div style="width:680px;">   
        <fieldset style="width:680px;">
        <legend>Scrap Sales Entry</legend>
        <br>
        	<fieldset style="width:680px;">
                <table width="678" cellspacing="2" cellpadding="0" border="0" id="tbl_master" align="center">
                    <tr>
                        <td colspan="3" align="right"><strong>System Challan No</strong></td>
                        <td colspan="3" align="left">
                        	 <input type="hidden" name="update_id" id="update_id" />
                            <input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:150px;" placeholder="Double click to search" onDblClick="openmypage_systemId();" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80" class="must_entry_caption"> Company </td>
                        <td width="130">
                            <?php 
                                echo create_drop_down( "cbo_company_id", 130, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", 0, "" );//load_drop_down( 'requires/trims_issue_entry_controller',this.value, 'load_drop_down_store', 'store_td' );
                            ?>
                        </td>
                        <td width="100" class="must_entry_caption">Item Category</td>
                        <td width="130">
                            <?php 
                                echo create_drop_down( "cbo_category_id", 130, $item_category,"",1, "-- Select Item --", $selected, "", "", "");
                            ?>
                        </td>
                        <td width="100" class="must_entry_caption">Selling Date</td>
                        <td width="120">
                            <input class="datepicker" type="date" style="width:110px" name="txt_selling_date" id="txt_selling_date"/>
                        </td>
                    </tr> 
                    <tr>
                    	<td class="must_entry_caption">Pay Term</td>
                        <td>
                            <?php 
                                echo create_drop_down( "cbo_pay_term", 130, $pay_mode,"",1, "--Select Pay Term--", 4, "", "", "1,4");
								//create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index, $new_conn, $field_name )
                            ?>
                        </td>
                        <td>Customer</td>
                        <td>
                            <input name="txt_customer_no" id="txt_customer_no" class="text_boxes" style="width:118px"  placeholder="Browse Customer" onDblClick="openpage_customer();" />
                            <input type="hidden" name="txt_customer_id" id="txt_customer_id" />
                        </td>
                        <td class="must_entry_caption">Currency</td>
                        <td>
                            <?php
                                echo create_drop_down( "cbo_currency_id", 120, $currency,"",1, "--Select Currency--", 1, "" );
                            ?>
                        </td> 
                    </tr>
                    <tr>
                    	<td>Remarks</td>
                        <td colspan="3">
                            <input name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:350px" />
                        </td> 
                    </tr>
                </table>
            </fieldset>
            <br>
            <fieldset style="width:660px">
            <legend>New Entry</legend>
            <table width="660" cellspacing="1" cellpadding="1" border="1" rules="all" class="rpt_table">
                <thead>
                    <th width="80">Product ID</th>
                    <th width="150">Item Description</th>
                    <th width="60">Reject UOM</th>
                    <th width="70">Sales Qty As Reject UOM</th>
                    <th width="60">Sales UOM</th>
                    <th width="70">Sales Qty</th>
                    <th width="60">Sales Rate</th>
                    <th>Amount</th>
				</thead>
                <tbody id="tbl_body">
                	<tr>
                    	<td>
                            <input type="hidden" name="dtls_update_id" id="dtls_update_id" />
                            <input type="text" name="txt_prodid" id="txt_prodid" class="text_boxes" style="width:60px" onDblClick="openmypage_ItemDescription()" placeholder="Browse" readonly />
                            <input type="hidden" name="trans_id" id="trans_id" />
                            
                            </td>
                        <td><input type="text" name="txt_itemdes" id="txt_itemdes" class="text_boxes" style="width:150px" readonly /></td>
                        <td><?php echo create_drop_down( "cbo_rejuomid", 60, $unit_of_measurement,"",1, "-Select-", 0, "",1 ); ?></td>
                        <td><input type="text" name="txt_rejqty" id="txt_rejqty" class="text_boxes_numeric" style="width:70px" readonly /></td>
                        <td><?php echo create_drop_down( "cbo_salesuomid", 60, $unit_of_measurement,"",1, "-Select-", 0, "sales_qty_uom(this.value);" ); ?></td>
                        <td><input type="text" name="txt_salesqty" id="txt_salesqty" class="text_boxes_numeric" style="width:70px" onBlur="amount_calculation($('#txt_salesrate').val());" /></td>
                        <td><input type="text" name="txt_salesrate" id="txt_salesrate" class="text_boxes_numeric" style="width:50px" onBlur="amount_calculation(this.value);" /></td>
                        <td><input type="text" name="txt_salesamount" id="txt_salesamount" class="text_boxes_numeric" style="width:70px" readonly /></td>
                    </tr>
                </tbody>
                <tr>
                    <td align="center" colspan="8" class="button_container" width="100%">
                        <?php
                            echo load_submit_buttons($permission, "fnc_scrap_sales", 0,1,"reset_form('scrapsales_1','div_details_list_view','','','')",1);
                        ?>
                    </td>
                </tr>
			</table>
            </fieldset>
		</fieldset>
        <div style="width:680px;" id="div_details_list_view"></div>

	</div>
	</form>
</div>   
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>