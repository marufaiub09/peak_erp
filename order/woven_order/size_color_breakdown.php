﻿<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will create Knit Garments Order Entry
Functionality	        :	
JS Functions	        :
Created by		:	Monzu 
Creation date 	        : 	13-10-2012
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Color Size Entry", "../../", 1, 1,$unicode,'','');
?>	
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<?php echo $permission; ?>';
	var str_size = [<?php echo substr(return_library_autocomplete( "select size_name from  lib_size", "size_name"  ), 0, -1); ?>];
	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color", "color_name"  ), 0, -1); ?>];
	function openmypage(page_link,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemail=this.contentDoc.getElementById("po_id") //Access form field with id="emailfield"
			if (theemail.value!="")
			{
				freeze_window(5);
				get_php_form_data( theemail.value, "populate_data_from_search_popup", "requires/size_color_breakdown_controller" );
				show_list_view(theemail.value,'populate_size_color_breakdown','size_color_breakdown','../woven_order/requires/size_color_breakdown_controller','');
				//set_button_status(0, permission, 'fnc_size_color_breakdown',1);
				release_freezing();
			} 
		}
	}
	
	function fnc_size_color_breakdown( operation )
	{
		var row_num=$('#size_color_break_down_list tr').length-1;
		var data_all="";
		var po_qnty="";
		var total=(document.getElementById('total').value)*1;
		var cbo_order_uom=document.getElementById('cbo_order_uom').value;
		var tot_set_qnty=(document.getElementById('tot_set_qnty').value)*1;
		var txt_total_order_qnty=(document.getElementById('txt_total_order_qnty').value)*1;
		if(cbo_order_uom==58)
		{
			po_qnty	=total*tot_set_qnty;
		}
		else
		{
			po_qnty	=total;
		}
		/*if(po_qnty>txt_total_order_qnty)
		{
			alert("Break Down Qnty Does Not Match with Order Qnty In Pcs");
			return;
		}*/
		for (var i=1; i<=row_num; i++)
		{
			if (form_validation('cbo_po_country*cbogmtsitem_'+i+'*txtcolor_'+i+'*txtsize_'+i+'*txtorderquantity_'+i+'*txtorderrate_'+i+'*txtorderexcesscut_'+i+'','Country*Company Name*Location Name*Buyer Name*Style Ref*Product Department*Item Catagory*Dealing Merchant*Packing')==false)
			{
				return;
			}
			if($('#txtorderrate_'+i).val()==0)
			{
				alert("Fill Up Rate");	
				$('#txtorderrate_'+i).focus();
				return;
			}
			eval(get_submitted_variables('txt_job_no*order_id*txt_tot_avg_rate*txt_tot_amount*cbo_order_uom*tot_set_qnty*txt_tot_excess_cut*txt_tot_plancut*cbo_po_country*hiddenid_'+i+'*cbogmtsitem_'+i+'*txtarticleno_'+i+'*txtcolor_'+i+'*txtsize_'+i+'*txtorderquantity_'+i+'*txtorderrate_'+i+'*txtorderamount_'+i+'*txtorderexcesscut_'+i+'*txtorderplancut_'+i+'*cbostatus_'+i));
			data_all=data_all+get_submitted_data_string('cbogmtsitem_'+i+'*hiddenid_'+i+'*txtarticleno_'+i+'*txtcolor_'+i+'*txtsize_'+i+'*txtorderquantity_'+i+'*txtorderrate_'+i+'*txtorderamount_'+i+'*txtorderexcesscut_'+i+'*txtorderplancut_'+i+'*cbostatus_'+i,"../../");
		}
		var data="action=save_update_delete&operation="+operation+'&total_row='+row_num+'&txt_job_no='+txt_job_no+'&order_id='+order_id+'&txt_avg_rate='+txt_tot_avg_rate+'&txt_total_amt='+txt_tot_amount+'&cbo_order_uom='+cbo_order_uom +'&tot_set_qnty='+tot_set_qnty+'&txt_avg_excess_cut='+txt_tot_excess_cut+'&txt_total_plan_cut='+txt_tot_plancut+'&cbo_po_country='+cbo_po_country+data_all;
		freeze_window(operation);
		http.open("POST","requires/size_color_breakdown_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_on_submit_reponse;
	}
	
	function fnc_on_submit_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=http.responseText.split('**');
			//show_list_view(document.getElementById('txt_job_no').value,'show_po_active_listview','po_list_view','../woven_order/requires/size_color_breakdown_controller','');
			// show_list_view(document.getElementById('order_id').value,'show_country_po_active_listview','country_po_active_listview','../woven_order/requires/size_color_breakdown_controller','');
			show_list_view(document.getElementById('order_id').value,'populate_size_color_breakdown','size_color_breakdown','../woven_order/requires/size_color_breakdown_controller','');
			show_list_view(document.getElementById('order_id').value+"_"+document.getElementById('cbo_po_country').value,'populate_size_color_breakdown_with_data','data_form','../woven_order/requires/size_color_breakdown_controller','');
			document.getElementById('cbo_po_country').value=0;
			set_button_status(0, permission, 'fnc_size_color_breakdown',1);
			calculate_total_amnt( 1 )
			release_freezing();
		}
	}
	
	function add_break_down_tr( i )
	{
		var row_num=$('#size_color_break_down_list tr').length-1;
		if (i==0)
		{
			i=1;
			$("#txtcolor_"+i).autocomplete({
			source: str_color
			});
			$("#txtsize_"+i).autocomplete({
			source:  str_size 
			}); 
			return;
		}
		if (row_num!=i)
		{
			return false;
		}
		if (form_validation('cbogmtsitem_'+i+'*txtcolor_'+i+'*txtsize_'+i+'*txtorderquantity_'+i+'*txtorderrate_'+i+'*txtorderexcesscut_'+i+'','Company Name*Location Name*Buyer Name*Style Ref*Product Department*Item Catagory*Dealing Merchant*Packing')==false)
		{
			return;
		}
		if($('#txtorderrate_'+i).val()==0)
		{
			alert("Fill Up Rate");	
			$('#txtorderrate_'+i).focus();
			return;
		}
		else
		{
			i++;
			$("#size_color_break_down_list tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			'name': function(_, name) { return name + i },
			'value': function(_, value) { return value }              
			});
			
			}).end().appendTo("#size_color_break_down_list");
			$('#cbogmtsitem_'+i).removeAttr("onChange").attr("onChange","calculate_total_amnt("+i+");check_duplicate("+i+",this.id)");
			$('#txtcolor_'+i).removeAttr("onChange").attr("onChange","check_duplicate("+i+",this.id)");
			$('#txtsize_'+i).removeAttr("onChange").attr("onChange","check_duplicate("+i+",this.id)");
			
			$('#increaseset_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
			$('#decreaseset_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'size_color_break_down_list');");
			$('#txtorderquantity_'+i).removeAttr("onBlur").attr("onBlur","set_excess_cut(this.value,document.getElementById('txtorderexcesscut_"+i+"').value,"+i+")");
			$('#txtorderrate_'+i).removeAttr("onBlur").attr("onBlur","calculate_total_amnt("+i+")");
			$('#txtorderexcesscut_'+i).removeAttr("onBlur").attr("onBlur","set_excess_cut(document.getElementById('txtorderquantity_"+i+"').value,this.value,"+i+")");
			$('#txtsize_'+i).val('');
			// onBlur="(document.getElementById('txtorderquantity_1').value, this.value, 1)"
			var j=i-1;
			//$('#txtcolor_'+i).removeAttr("onfocus").attr("onfocus","add_break_down_tr("+j+");");
			$('#cbogmtsitem_'+i).val($('#cbogmtsitem_'+j).val()); 
			$('#cbostatus_'+i).val($('#cbostatus_'+j).val());
			$('#hiddenid_'+i).val("");
			$('#txtorderquantity_'+i).val("");
			set_all_onclick();
			$("#txtcolor_"+i).autocomplete({
			source: str_color
			});
			$("#txtsize_"+i).autocomplete({
			source:  str_size 
			}); 
			calculate_total_amnt( i )
		}
	}
	
	function fn_deletebreak_down_tr(rowNo,table_id) 
	{   
		if(table_id=='size_color_break_down_list')
		{
			/*var numRow = $('table#size_color_break_down_list tbody tr').length; 
			if(numRow==rowNo && rowNo!=1)
			{
				if($('#hiddenid_'+rowNo).val()=="")
				{
					$('#size_color_break_down_list tbody tr:last').remove();
					calculate_total_amnt( rowNo-1 );
				}
				else
				{
					$('#size_color_break_down_list tbody tr:last').remove();
					calculate_total_amnt( rowNo-1 );
					//alert("Remove Restricted!");	
				}
			}*/
			if(rowNo!=1)
			{
				var permission_array=permission.split("_");
				var updateid=$('#hiddenid_'+rowNo).val();
				if(updateid !="" && permission_array[2]==1)
				{
				var booking=return_global_ajax_value(updateid, 'delete_row_color_size', '', 'requires/size_color_breakdown_controller');
				}
				var index=rowNo-1
				$("table#size_color_break_down_list tbody tr:eq("+index+")").remove()
				var numRow = $('table#size_color_break_down_list tbody tr').length; 
				for(i = rowNo;i <= numRow;i++)
				{
					$("#size_color_break_down_list tr:eq("+i+")").find("input,select").each(function() {
							$(this).attr({
								'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
								//'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i},
								'value': function(_, value) { return value }             
							}); 
						$('#cbogmtsitem_'+i).removeAttr("onChange").attr("onChange","calculate_total_amnt("+i+");check_duplicate("+i+",this.id)");
						$('#txtcolor_'+i).removeAttr("onChange").attr("onChange","check_duplicate("+i+",this.id)");
						$('#txtsize_'+i).removeAttr("onChange").attr("onChange","check_duplicate("+i+",this.id)");
						$('#increaseset_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
						$('#decreaseset_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'size_color_break_down_list');");
						$('#txtorderquantity_'+i).removeAttr("onBlur").attr("onBlur","set_excess_cut(this.value,document.getElementById('txtorderexcesscut_"+i+"').value,"+i+")");
						$('#txtorderrate_'+i).removeAttr("onBlur").attr("onBlur","calculate_total_amnt("+i+")");
						$('#txtorderexcesscut_'+i).removeAttr("onBlur").attr("onBlur","set_excess_cut(document.getElementById('txtorderquantity_"+i+"').value,this.value,"+i+")");
					})
				}
			}
			calculate_total_amnt( rowNo-1 )
		
		}
	}
	
	
	function populate_size_color_breakdown_with_data(data)
	{
		var data=data.split("_");
		document.getElementById('cbo_po_country').value=data[1];
		show_list_view(data[0]+"_"+data[1],'populate_size_color_breakdown_with_data','data_form','requires/size_color_breakdown_controller','');
		calculate_total_amnt( 1 )
		set_button_status(1, permission, 'fnc_size_color_breakdown',1);
	}
	function check_country(country_id)
	{
		var po_id=document.getElementById('order_id').value;
		var country=return_global_ajax_value(po_id+"_"+country_id, 'check_country', '', 'requires/size_color_breakdown_controller');
		if(country>0)
		{
			alert("This Country Data Already Inserted");
			document.getElementById('cbo_po_country').value=0;
		}
		
	}
	
	function set_excess_cut( val, excs, id )
	{
		document.getElementById('txtorderplancut_'+id).value=(val*1)+((excs*val)/100);
		calculate_total_amnt(id);
	}
	
	function calculate_total_amnt( id )
	{
		//alert(id);
		document.getElementById('txtorderamount_'+id).value=document.getElementById('txtorderrate_'+id).value*document.getElementById('txtorderquantity_'+id).value
		var po_qnty=(document.getElementById('total').value)*1;
		var item_id=(document.getElementById('cbogmtsitem_'+id).value);
		var cbo_order_uom=document.getElementById('cbo_order_uom').value;
		if(cbo_order_uom==58)
		{
			var set_breck_down=document.getElementById('set_breck_down').value;
			set_breck_down=set_breck_down.split("__");
			var item_id_value_array=new Array();
			for (var si=0;si<set_breck_down.length; si++)
			{
			var item_id_value=set_breck_down[si].split("_");
			item_id_value_array[item_id_value[0]]=item_id_value[1]
			}
			po_qnty=po_qnty*item_id_value_array[item_id];
			document.getElementById('set_item_qnty_level').innerHTML="Set Item Qnty";
			document.getElementById('set_item_qnty').innerHTML=item_id_value_array[item_id];
			document.getElementById('qnty_eq_in_pcs_level').innerHTML="Qnty in Pcs";
			document.getElementById('qnty_eq_in_pcs').innerHTML=po_qnty;
		}
		var row_num=$('#size_color_break_down_list tr').length-1;
		var tot=0;
		var item_tot=0;
		var avg_rate = 0; 
		var tot_amount = 0;
		var avg_excess_cut = 0;
		var tot_plan_cut = 0;
		for (var k=1;k<=row_num; k++)
		{
			if(item_id==document.getElementById('cbogmtsitem_'+k).value)
			{
				item_tot=(item_tot*1)+(document.getElementById('txtorderquantity_'+k).value*1);
			}
			tot=(tot*1)+(document.getElementById('txtorderquantity_'+k).value*1);
			avg_rate=((avg_rate*1)+(document.getElementById('txtorderrate_'+k).value*1));
			tot_amount=(tot_amount*1)+(document.getElementById('txtorderamount_'+k).value*1);
			avg_excess_cut=((avg_excess_cut*1)+(document.getElementById('txtorderexcesscut_'+k).value*1))
			tot_plan_cut=(tot_plan_cut*1)+(document.getElementById('txtorderplancut_'+k).value*1); 
		}
		avg_excess_cut=((tot_plan_cut-tot)/tot)*100;
		avg_rate=tot_amount/tot*1;
		$('#txt_total_order_qnty').val(tot);
		$('#txt_total_order_item_qnty').val(item_tot);
		$('#txt_total_order_item_yetto_qnty').val(po_qnty-item_tot);
		$('#txt_avg_rate').val(number_format_common(avg_rate, 3, 0));
		$('#txt_total_amt').val(tot_amount);
		$('#txt_avg_excess_cut').val(number_format_common(avg_excess_cut,6, 0,2));
		$('#txt_total_plan_cut').val(tot_plan_cut);
		
		if (item_tot>po_qnty)
		{
			alert('Breakdown Quantity Over The Po Qnty Not Allowed.');
			document.getElementById('txtorderquantity_'+id).value="";
			document.getElementById('txtorderplancut_'+id).value="";
			document.getElementById('txtorderamount_'+id).value="";
			$('#txtorderquantity_'+id).focus();
			return;
		}
	}
	
	function check_duplicate(id,td)
	{
		var item_id=(document.getElementById('cbogmtsitem_'+id).value);
		var txtcolor=(document.getElementById('txtcolor_'+id).value).toUpperCase();
		var txtsize=(document.getElementById('txtsize_'+id).value).toUpperCase();
		var row_num=$('#size_color_break_down_list tr').length-1;
		for (var k=1;k<=row_num; k++)
		{
			if(k==id)
			{
				continue;
			}
			else
			{
				if(item_id==document.getElementById('cbogmtsitem_'+k).value && txtcolor==document.getElementById('txtcolor_'+k).value && txtsize==document.getElementById('txtsize_'+k).value)
				{
				alert("Same Gmts Item, Same Color and Same Size Duplication Not Allowed.");
				document.getElementById(td).value="";
				document.getElementById(td).focus();
				}
			}
		}
	}
	
	function check_copy(val)
	{
		copied_table="";
		if (val==0)
		{
			$('#chk_copy').val(1);	// attr('checked',true);
			copied_table=$("#size_color_break_down_list tbody").html();
		}
		else
		$('#chk_copy').val(0); 	//attr('checked',false);	
		alert(copied_table);
	}
	
	function add_copied_po_breakdown()
	{
		$("#size_color_break_down_list tbody").html('');
		$("#size_color_break_down_list tbody").html(copied_table);
	}
</script>
</head>

<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
        <!-- Important Field outside Form --> 
        <input type="hidden" id="garments_nature" value="2">
        <!-- End Important Field outside Form -->
        <?php echo load_freeze_divs ("../../",$permission);  ?>
        <fieldset style="width:950px;">
            <legend>Color & Size Breakdown Entry</legend>
            <form name="sizecolormaster_1" id="sizecolormaster_1" autocomplete="off">
                <table  width="950" cellspacing="2" cellpadding="0" border="0">
                    <tr>
                        <td  width="130" height="" align="right"></td>              
                        <td  width="170" >
                        </td>
                        <td  width="130" align="right">Order No </td>
                        <td width="170">
                        <input style="width:160px;" type="text" title="Double Click to Search" onDblClick="openmypage('requires/size_color_breakdown_controller.php?action=order_popup','Job/Order Selection Form')" class="text_boxes" placeholder="Order No" name="txt_order_no" id="txt_order_no" readonly />
                        <input type="hidden" id="order_id" name="order_id" readonly />
                        </td>
                        <td width="130" align="right"></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td  width="130" height="" align="right">Job No</td>              
                        <td  width="170" >
                        <input style="width:160px;" type="text" title="Double Click to Search" onDblClick="openmypage('requires/size_color_breakdown_controller.php?action=order_popup','Job/Order Selection Form')" class="text_boxes"  name="txt_job_no" id="txt_job_no" disabled />
                        </td>
                        <td  width="130" align="right">Company Name </td>
                        <td width="170">
                        <?php
                        echo create_drop_down( "cbo_company_name", 172, "select comp.id,comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "",1 );
                        ?> 
                        </td>
                        <td width="130" align="right">Location Name</td>
                        <td id="location">
                        <?php 
                        echo create_drop_down( "cbo_location_name", 172, "select id,location_name from lib_location where status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select --", $selected, "",1 );		
                        ?>	
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Buyer Name</td>
                        <td id="buyer_td">
                        <?php 
                        echo create_drop_down( "cbo_buyer_name", 172, "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" ,1);   
                        ?>	  
                        </td>
                        <td align="right">Style Ref.</td>
                        <td>
                        <input class="text_boxes" type="text" style="width:160px" disabled placeholder="Double Click for Quotation" name="txt_style_ref" id="txt_style_ref"/>	
                        </td>
                        <td align="right">
                        Style Description
                        </td>
                        <td>	
                        <input class="text_boxes" type="text" style="width:160px;" disabled name="txt_style_description" id="txt_style_description"/>
                        </td>
                    </tr>
                    <tr>
                        <td height="" align="right">Pord. Dept.</td>   
                        <td >
                        <?php 
                        echo create_drop_down( "cbo_product_department", 172, $product_dept, "",1, "-- Select prod. Dept--", $selected, "" ,1);
                        ?>
                        </td>
                        <td align="right">Currency</td>
                        <td>
                        <?php 
                        echo create_drop_down( "cbo_currercy", 172, $currency, "", 1, "-- Select Currency--", 2, "",1 );
                        ?>	  
                        </td>
                        <td align="right">Agent </td>
                        <td id="agent_td">
                        <?php	 	echo create_drop_down( "cbo_agent", 172, "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id  and a.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3))  order by buyer_name","id,buyer_name", 1, "-- Select Agent --", $selected, "",1 );  
                        
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td  align="right">Region</td>
                        <td>
                        <?php 
                        echo create_drop_down( "cbo_region", 172, $region, "",1, "-- Select Region --", $selected, "",1 );
                        ?>	  
                        </td>
                        <td align="right">Team Leader</td>   
                        <td>
                        <?php  
                        echo create_drop_down( "cbo_team_leader", 172, "select id,team_leader_name from lib_marketing_team where status_active =1 and is_deleted=0 order by team_leader_name","id,team_leader_name", 1, "-- Select Team --", $selected, "",1 );
                        ?>		
                        </td>
                        <td align="right">Dealing Merchant</td>   
                        <td> 
                        <?php 
                        echo create_drop_down( "cbo_dealing_merchant", 172, "select id,team_member_name from lib_mkt_team_member_info where status_active =1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-- Select Team Member --", $selected, "",1 );
                        ?>	
                        </td>
                    </tr>
                    <tr>
                        <td  align="right">Shipment Date</td>
                        <td>
                        <input class="datepicker" type="text" style="width:160px;"  name="txt_ship_date" id="txt_ship_date" disabled/>
                        </td>
                        <td  align="right">Po Qnty</td>
                        <td>
                        <input class="text_boxes" type="text" style="width:100px;"  name="txt_po_qnty" id="txt_po_qnty" disabled/>
                        <?php 
                        echo create_drop_down( "cbo_order_uom",55, $unit_of_measurement, "",0, "", 1, "","1","1,58" );
                        ?>
                        </td>
                        <td  align="right">Plan Cut Qnty</td>
                        <td>
                        <input class="text_boxes" type="text" style="width:100px;"  name="txt_plan_cut_qnty" id="txt_plan_cut_qnty" disabled/>
                        <?php 
                        echo create_drop_down( "cbo_order_uom_2",55, $unit_of_measurement, "",0, "", 1, "","1","1,58" );
                        ?>
                        </td>
                    </tr>
                    <tr>
                        <td  align="right" class="must_entry_caption">Country</td>
                        <td>
                         <?php
                                              echo create_drop_down( "cbo_po_country", 90,"select id,country_name from lib_country where status_active=1 and is_deleted=0 order by country_name", "id,country_name", 1, "--- Select ", "","check_country(this.value)" ); 
						?>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="center" height="20" colspan="6" class="image_uploaders">
                        <input type="hidden" id="update_id">
                        <input type="hidden" id="set_breck_down" />     
                        <input type="hidden" id="item_id" />
                        <input type="hidden" id="tot_set_qnty" />  
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6" valign="middle" style="max-height:180px; min-height:15px;" id="po_list_view">
                        </td>
                        </tr>
                        <tr>
                        <td align="center" colspan="6" valign="middle" style="max-height:380px; min-height:15px;" id="size_color_breakdown11">
                        <?php 
                        //echo load_submit_buttons( $permission, "fnc_size_color_breakdown", 0,0 ,"reset_form('sizecolormaster_1','po_list_view*size_color_breakdown','')",1) ;                        ?>
                        </td>
                    </tr>
                </table>
            </form>
        </fieldset>
        <div id="size_color_breakdown">
        </div>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>