<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Order wise Production Report.
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	1-04-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Wise Production Report", "../../", 1, 1,$unicode,1,1);

?>	
<script>

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission = '<?php echo $permission; ?>';
 
var tableFilters1 = 
	{
		col_0: "none",col_26: "select",display_all_text: " -- All --",
		col_operation: { 
			id: ["total_order_quantity","total_cutting","total_emb_issue","total_emb_receive","total_sewing_input","total_sewing_out","total_iron_qnty","total_re_iron_qnty","total_finish_qnty","total_rej_value_td","total_out","total_shortage"],
			//col: [6,11,13,14,15,16,17,18,19,21,22,23],
			col: [8,13,15,16,17,18,19,20,21,23,24,25],
			operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
			write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
 	}	
	
var tableFilters2 = 
	{
		col_0: "none",col_28: "select",display_all_text: " -- All --",
		col_operation: { 
			id: ["total_order_quantity","total_cutting","total_emb_issue","total_emb_receive","total_sewing_input","total_sewing_out","total_iron_qnty","total_re_iron_qnty","total_finish_qnty","total_rej_value_td","total_out","total_shortage"],
			//col: [6,13,15,16,17,18,19,20,21,23,24,25],
			col: [8,15,17,18,19,20,21,22,23,25,26,27],
			operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
			write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
 	}			
var tableFilters3 = 
	{
		col_0: "none",col_27: "select",display_all_text: " -- All --",
		col_operation: { 
			id: ["total_order_quantity","total_cutting","total_emb_issue","total_emb_receive","total_sewing_input","total_sewing_out","total_iron_qnty","total_re_iron_qnty","total_finish_qnty","total_rej_value_td","total_out","total_shortage"],
			//col: [7,12,14,15,16,17,18,19,20,22,23,24],
			col: [9,14,16,17,18,19,20,21,22,24,25,26],
			operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
			write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
 	}	
	
var tableFilters4 = 
	{
		col_0: "none",col_29: "select",display_all_text: " -- All --",
		col_operation: { 
			id: ["total_order_quantity","total_cutting","total_emb_issue","total_emb_receive","total_sewing_input","total_sewing_out","total_iron_qnty","total_re_iron_qnty","total_finish_qnty","total_rej_value_td","total_out","total_shortage"],
			//col: [7,14,16,17,18,19,20,21,22,24,25,26],
			col: [9,16,18,19,20,21,22,23,24,26,27,28],
			operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
			write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
 	}
	
var tableFilters5 = 
	{
		col_0: "none",col_26: "select",display_all_text: " -- All --",
		col_operation: { 
			id: ["total_order_quantity","total_cutting","total_emb_issue","total_emb_receive","total_sewing_input","total_sewing_out","total_iron_qnty","total_re_iron_qnty","total_finish_qnty","total_rej_value_td","total_out","total_shortage"],
			//col: [6,11,13,14,15,16,17,18,19,21,22,23],
			col: [8,13,15,16,17,18,19,20,21,23,24,25],
			operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
			write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
 	}
	
	var tableFilters6 = 
	{
		col_0: "none",col_27: "select",display_all_text: " -- All --",
		col_operation: { 
			id: ["total_order_quantity","total_cutting","total_emb_issue","total_emb_receive","total_sewing_input","total_sewing_out","total_iron_qnty","total_re_iron_qnty","total_finish_qnty","total_rej_value_td","total_out","total_shortage"],
			//col: [7,12,14,15,16,17,18,19,20,22,23,24],
			col: [9,14,16,17,18,19,20,21,22,24,25,26],
			operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
			write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
 	}
				
function fn_report_generated()
{
	if (form_validation('cbo_company_name*cbo_type','Comapny Name*Report Type')==false)//*txt_date_from*txt_date_to----*From Date*To Date
	{
		return;
	}
	else
	{
		var data="action=report_generate"+get_submitted_data_string('cbo_garments_nature*cbo_company_name*cbo_buyer_name*cbo_team_name*cbo_team_member*cbo_location*cbo_floor*cbo_type*txt_date_from*txt_date_to*txt_order_no',"../../");
		freeze_window(3);
		http.open("POST","requires/order_wise_production_report_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fn_report_generated_reponse;
	}
}
	

function fn_report_generated_reponse()
{
 	if(http.readyState == 4) 
	{
  		var reponse=trim(http.responseText).split("****");
		 
		show_msg('3');
		release_freezing();
		$('#report_container2').html(reponse[0]);
		document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
		//append_report_checkbox('table_header_1',1);
		//append_report_checkbox('table_header_2',1); 			
		//setFilterGrid("table_body_1",-1,tableFilters1);
		//setFilterGrid("table_body_2",-1,tableFilters2);

		var type=$('#cbo_type').val();
		
		append_report_checkbox('table_header_1',1);
		if(type==1) setFilterGrid("table_body",-1,tableFilters1);
		else if(type==2) setFilterGrid("table_body",-1,tableFilters2);
		else if(type==3) setFilterGrid("table_body",-1,tableFilters3);
		else if(type==4) setFilterGrid("table_body",-1,tableFilters4);
		else if(type==5) setFilterGrid("table_body",-1,tableFilters5);
		else if(type==6) setFilterGrid("table_body",-1,tableFilters6);
 		//alert(document.getElementById('graph_data').value);
	 	//show_graph( "", document.getElementById('graph_data').value, "column", "chartdiv", "", "../../", '',300, 500 );
		//show_graph( "", document.getElementById('graph_data').value, "column", "chartdiv", "", "../../", '',300,500 );
		//show_chart(document.getElementById('graph_data').value);
		/*var dd=document.getElementById('graph_data').value;
		var ndata=dd.split("\n");
		var dar=""; var cap="";
		for(var i=0; i<ndata.length; i++)
		{
			var tdata=ndata[i].split(";");
			 if(dar=="") dar=500000; else dar=dar+"*"+tdata[1];
			  if(cap=="") cap=tdata[0]; else cap=cap+"*"+tdata[0];
		}*/
		//alert(dar)
		 //LogiChart( "chartdiv", dar,cap, 500, 300, 1 , "009900");
 	}
}


function openmypage_remark(po_break_down_id,item_id,country_id,action)
{
	var garments_nature = $("#cbo_garments_nature").val();
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/order_wise_production_report_controller.php?po_break_down_id='+po_break_down_id+'&item_id='+item_id+'&country_id='+country_id+'&action='+action, 'Remarks Veiw', 'width=550px,height=450px,center=1,resize=0,scrolling=0','../');
}

function openmypage_order(po_break_down_id,company_name,item_id,country_id,action)
{
	//var garments_nature = $("#cbo_garments_nature").val();
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/order_wise_production_report_controller.php?po_break_down_id='+po_break_down_id+'&company_name='+company_name+'&item_id='+item_id+'&country_id='+country_id+'&action='+action, 'Order Quantity', 'width=750px,height=350px,center=1,resize=0,scrolling=0','../');
}



function openmypage_country_ship_date(po_break_down_id,item_id,action,production_type,floor_id,dateOrLocWise,country_id)
{
	if(production_type==2 || production_type==3)
		var popupWidth = "width=1050px,height=350px,";
	else if (production_type==10)
		var popupWidth = "width=550px,height=420px,";
	else
		var popupWidth = "width=750px,height=420px,";
	
	if (production_type==2)
	{
		var popup_caption="Embl. Issue Details";
	}
	else if (production_type==3)
	{
		var popup_caption="Embl. Rec. Details";
	}
	else
	{
		var popup_caption="Production Quantity";
	}
		
	emailwindow=dhtmlmodal.open('EmailBox','iframe','requires/order_wise_production_report_controller.php?po_break_down_id='+po_break_down_id+'&item_id='+item_id+'&action='+action+'&production_type='+production_type+'&floor_id='+floor_id+'&dateOrLocWise='+dateOrLocWise+'&country_id='+country_id, popup_caption, popupWidth+'center=1,resize=0,scrolling=0','../');
}

function openmypage_rej(po_id,item_id,action,location_id,floor_id,reportType,country_id)
{
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/order_wise_production_report_controller.php?po_id='+po_id+'&item_id='+item_id+'&action='+action+'&location_id='+location_id+'&floor_id='+floor_id+'&reportType='+reportType+'&country_id='+country_id, 'Reject Quantity', 'width=510px,height=400px,center=1,resize=0,scrolling=0','../');
}

function openmypage(po_break_down_id,item_id,action,location_id,floor_id,dateOrLocWise,country_id)
{
	if(action==2 || action==3)
		var popupWidth = "width=1050px,height=350px,";
	else if (action==10)
		var popupWidth = "width=550px,height=420px,";
	else
		var popupWidth = "width=750px,height=420px,";
	
	if (action==2)
	{
		var popup_caption="Embl. Issue Details";
	}
	else if (action==3)
	{
		var popup_caption="Embl. Rec. Details";
	}
	else
	{
		var popup_caption="Production Quantity";
	}
		
	emailwindow=dhtmlmodal.open('EmailBox','iframe','requires/order_wise_production_report_controller.php?po_break_down_id='+po_break_down_id+'&item_id='+item_id+'&action='+action+'&location_id='+location_id+'&floor_id='+floor_id+'&dateOrLocWise='+dateOrLocWise+'&country_id='+country_id, popup_caption, popupWidth+'center=1,resize=0,scrolling=0','../');
}

function openmypage_rej(po_id,item_id,action,location_id,floor_id,reportType,country_id)
{
	var company_name=$('#cbo_company_name').val();
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/order_wise_production_report_controller.php?po_id='+po_id+'&item_id='+item_id+'&action='+action+'&location_id='+location_id+'&floor_id='+floor_id+'&reportType='+reportType+'&country_id='+country_id+'&company_name='+company_name, 'Reject Quantity', 'width=660px,height=400px,center=1,resize=0,scrolling=0','../');
}
	
function disable_order( val )
{
	$('#txt_order_no').val('');
	if(val==5)
	{
		$('#Order_td').html('Style Ref.');
	}
	else
	{
		$('#Order_td').html('Order No');
	}
}
	
	
function progress_comment_popup(po_id,template_id,tna_process_type)
{
	var data="action=update_tna_progress_comment"+
							'&po_id='+"'"+po_id+"'"+
							'&template_id='+"'"+template_id+"'"+
							'&tna_process_type='+"'"+tna_process_type+"'"+
							'&permission='+"'"+permission+"'";	
							
	http.open("POST","requires/order_wise_production_report_controller.php",true);
	
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = generate_progress_comment_reponse;	
}

function generate_progress_comment_reponse()
{
	if(http.readyState == 4) 
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><title></title></head><body>'+http.responseText+'</body</html>');//<link rel="stylesheet" href="../../css/style_common.css" type="text/css" />
		d.close();
	}
}
	 
</script>
</head>
<body onLoad="set_hotkey();">
<form id="dateWiseProductionReport_1">
    <div style="width:100%;" align="center">    
		<?php echo load_freeze_divs ("../../",'');  ?>
        <h3 style="width:1170px; margin-top:20px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel','')"> -Search Panel</h3> 
        <div id="content_search_panel" >      
            <fieldset style="width:1170px;">
                <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
                    <thead>                    
                        <th class="must_entry_caption">Company Name</th>
                        <th>Buyer Name</th>
                        <th>Team Name</th>
                        <th>Team Member</th>
                        <th>Location</th>
                        <th>Floor</th>
                        <th class="must_entry_caption">Type</th>
                        <th id="Order_td">Order No</th>
                        <th>Gmts. Nature</th>
                        <th colspan="2">Shipment Date</th>
                        <th><input type="reset" id="reset_btn" class="formbutton" style="width:70px" value="Reset" /></th>
                    </thead>
                    <tbody>
                        <tr class="general">
                            <td> 
								<?php
                               		echo create_drop_down( "cbo_company_name", 120, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/order_wise_production_report_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );load_drop_down( 'requires/order_wise_production_report_controller', this.value, 'load_drop_down_location', 'location_td' );" );
                                ?>
                            </td>
                            <td id="buyer_td">
								<?php 
                                	echo create_drop_down( "cbo_buyer_name", 120, $blank_array,"", 1, "-- Select Buyer --", $selected, "",1,"" );
                                ?>
                            </td>
                            <td>                
								<?php
                                	echo create_drop_down( "cbo_team_name", 100, "select id,team_name from lib_marketing_team  where status_active =1 and is_deleted=0  order by team_name","id,team_name", 1, "-Select Team-", $selected, " load_drop_down( 'requires/order_wise_production_report_controller', this.value, 'load_drop_down_team_member', 'team_td' )" );
                                ?>
                            </td>
                            <td id="team_td">
								<?php 
                                	echo create_drop_down( "cbo_team_member", 100, $blank_array,"", 1, "- Select Member-", $selected, "" );
                                ?>	
                            </td>
                            <td id="location_td">
								<?php 
                               		echo create_drop_down( "cbo_location", 120, $blank_array,"", 1, "-- Select --", $selected, "",1,"" );
                                ?>
                            </td>
                            <td id="floor_td">
								<?php 
                                	echo create_drop_down( "cbo_floor", 110, $blank_array,"", 1, "-- Select --", $selected, "",1,"" );
                                ?>
                            </td>
                            <td>
								<?php 
									$arr=array(1=>"Show Order Wise",2=>"Show Order Location & Floor Wise",3=>"Show Order Country Wise",4=>"Show Order Country Location & Floor Wise",5=>"Style Wise",6=>"Show Order Country Shipdate Wise");
									echo create_drop_down( "cbo_type", 160, $arr,"", 1, "-- Select --", 1, "disable_order(this.value);",0,"","" );//
                                ?>
                            </td>
                            <td>
                            	<input name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:70px" placeholder="Write" >
                            </td> 
                            <td>
								<?php 
									$arr = array(1=>"ALL",2=>"Woven",3=>"Knit");
									echo create_drop_down( "cbo_garments_nature", 70, $arr,"", 0, "-- Select --", $selected, "",0,"" );
                                ?>
                            </td>   
                            <td>
                            	<input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px" placeholder="From Date" >
                            </td>   
                            <td>
                            	<input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px"  placeholder="To Date" >
                            </td>
                            <td>
                            	<input type="button" id="show_button" class="formbutton" style="width:70px" value="Show" onClick="fn_report_generated(0)" />
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="11" align="center">
                            	<?php echo load_month_buttons(1); ?>
                            </td>
                        </tr>
                    <tfoot>
                </table> 
            </fieldset>
        </div>
    </div>
    <div id="report_container" align="center"></div>
    <div id="report_container2" align="left"></div>
</form>    
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	$('#cbo_location').val(0);
</script>
<script class="include" type="text/javascript" src="../../js/chart/logic_chart.js"></script>
</html>
