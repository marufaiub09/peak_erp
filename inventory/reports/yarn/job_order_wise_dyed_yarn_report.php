﻿<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Job/Order Wise Dyed Yarn Report
				
Functionality	:	
JS Functions	:
Created by		:	Fuad Shahriar
Creation date 	: 	20-03-2014
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Job/Order Wise Dyed Yarn Report","../../../", 1, 1, $unicode,1,1); 

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

function generate_report(type)
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	
	var report_title=$( "div.form_caption" ).html();
	var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*txt_job_no*hide_job_id*txt_process_loss',"../../../")+'&report_title='+report_title+'&type='+type;
	//alert (data);
	freeze_window(3);
	http.open("POST","requires/job_order_wise_dyed_yarn_report_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = generate_report_reponse;  
}

function generate_report_reponse()
{	
	if(http.readyState == 4) 
	{	 
 		var reponse=trim(http.responseText).split("####");
		$("#report_container2").html(reponse[0]);  
		document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
		
		show_msg('3');
		release_freezing();
	}
} 
 

function new_window()
{
	document.getElementById('scroll_body').style.overflow="auto";
	document.getElementById('scroll_body').style.maxHeight="none";
	
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');
	d.close(); 

	document.getElementById('scroll_body').style.overflowY="scroll";
	document.getElementById('scroll_body').style.maxHeight="380px";
}

function openmypage_job()
{
	if(form_validation('cbo_company_name','Company Name')==false)
	{
		return;
	}
	
	var companyID = $("#cbo_company_name").val();
	var buyer_name = $("#cbo_buyer_name").val();
	var page_link='requires/job_order_wise_dyed_yarn_report_controller.php?action=job_no_popup&companyID='+companyID+'&buyer_name='+buyer_name;
	var title='Job No Search';
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=630px,height=370px,center=1,resize=1,scrolling=0','../../');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var job_no=this.contentDoc.getElementById("hide_job_no").value;
		var job_id=this.contentDoc.getElementById("hide_job_id").value;
		
		$('#txt_job_no').val(job_no);
		$('#hide_job_id').val(job_id);	 
	}
	
}

</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
	<?php echo load_freeze_divs ("../../../",$permission); ?>		 
    <form name="DyedYarnReport_1" id="DyedYarnReport_1" autocomplete="off" > 
    <h3 align="left" id="accordion_h1" style="width:860px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
        <div id="content_search_panel">
            <fieldset style="width:860px;">
            <legend>Search Panel</legend> 
                <table class="rpt_table" width="850" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <thead>
                        <tr> 	 	
                            <th class="must_entry_caption">Company Name</th>
                            <th>Buyer Name</th>
                            <th>Job No</th>
                            <th>Process Loss %</th>
                            <th colspan="2"><input type="reset" name="res" id="res" value="Reset" style="width:100px" class="formbutton" onClick="reset_form('DyedYarnReport_1','report_container*report_container2','','','','');" /></th>
                        </tr>
                    </thead>
                    <tr align="center">
                        <td>
                            <?php 
                                echo create_drop_down( "cbo_company_name", 160, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/job_order_wise_dyed_yarn_report_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                            ?>                            
                        </td>
                        <td id="buyer_td">
							<?php 
                                echo create_drop_down( "cbo_buyer_name", 150, $blank_array,"", 1, "-- Select Buyer --", $selected, "",0,"" );
                            ?>
                        </td>
                        <td align="center">
                             <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:140px" placeholder="Browse" onDblClick="openmypage_job()" readonly>
                             <input type="hidden" name="hide_job_id" id="hide_job_id" readonly>
                        </td>
                        <td align="center">
                             <input type="text" name="txt_process_loss" id="txt_process_loss" class="text_boxes" style="width:100px" value="4">
                        </td>
                        <td>
                            <input type="button" name="search" id="search" value="Job Wise" onClick="generate_report(1)" style="width:100px" class="formbutton" />&nbsp;
                            <input type="button" name="search" id="search" value="Color Wise" onClick="generate_report(2)" style="width:100px" class="formbutton" />
                        </td>
                    </tr>
                </table> 
            </fieldset> 
        </div>
    </form>    
</div> 
<div id="report_container" align="center"></div>
<div id="report_container2"></div>    
</body>  
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
