<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Grey Production Entry
Functionality	:	
JS Functions	:
Created by		:	Fuad Shahriar 
Creation date 	: 	12/05/2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Grey Production Entry", "../", 1, 1,'','1',''); 

?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
	var permission='<?php echo $permission; ?>';

	var str_brand = [<?php echo substr(return_library_autocomplete( "select distinct(brand_name) from lib_brand", "brand_name"  ), 0, -1); ?>];
 	//var str_color = [<?phpecho substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];
	
	$(document).ready(function(e)
	{
          $("#txt_brand").autocomplete({
			 source: str_brand
		  });
		  
		 /* $("#txt_color").autocomplete({
			 source: str_color
		  });*/
     });
	 
	function set_receive_basis(type)
	{
		var recieve_basis = $('#cbo_receive_basis').val();
		
		$('#txt_yarn_issued').val('');
		$('#booking_without_order').val('');
		$('#txt_job_no').val('');
		$('#txt_receive_qnty').val('');
		$('#txt_reject_fabric_recv_qnty').val('');
		$('#all_po_id').val('');
		$('#save_data').val('');
		$('#txt_color').val('');
		$('#color_id').val('');
		$('#txt_deleted_id').val('');
		$('#roll_details_list_view').html('');
		
		$('#txt_receive_qnty').attr('readonly','readonly');
		$('#txt_receive_qnty').attr('onClick','openmypage_po();');	
		$('#txt_receive_qnty').attr('placeholder','Single Click');
		
		$('#txt_reject_fabric_recv_qnty').attr('readonly','readonly');
		$('#txt_reject_fabric_recv_qnty').attr('placeholder','Display');
		
        if(recieve_basis == 0 )
        {
			$('#txt_booking_no').val('');	
			$('#txt_booking_no_id').val('');	
			$('#txt_booking_no').attr('disabled','disabled');
			$('#cbo_buyer_name').removeAttr('disabled','disabled');	
			$('#cbo_body_part').removeAttr('disabled','disabled');	
			$('#fabric_desc_id').val('');
			$('#txt_fabric_description').val('');
			$('#txt_fabric_description').removeAttr('disabled','disabled');	
			
			//set_auto_complete(1);		
        }
        else
        {
			$('#txt_booking_no').val('');	
			$('#txt_booking_no_id').val('');
			$('#txt_booking_no').removeAttr('disabled','disabled');
			$('#cbo_buyer_name').val(0);
			$('#cbo_buyer_name').attr('disabled','disabled');
			$('#cbo_body_part').val(0);
			$('#cbo_body_part').attr('disabled','disabled');
			$('#fabric_desc_id').val('');
			$('#txt_fabric_description').val('');
			$('#txt_fabric_description').attr('disabled','disabled');
        }
		
		$('#list_fabric_desc_container').html('');
	}
	
	function openmypage_fabricBooking()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var recieve_basis = $('#cbo_receive_basis').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var title = 'Booking Selection Form';	
			var page_link = 'requires/grey_production_entry_controller.php?cbo_company_id='+cbo_company_id+'&recieve_basis='+recieve_basis+'&action=fabricBooking_popup';
			var popup_width="";
		    if(recieve_basis==1)  popup_width="1060px"; else  popup_width="1070px"; 
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width='+popup_width+',height=400px,center=1,resize=1,scrolling=0','');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var theemail=this.contentDoc.getElementById("hidden_booking_id").value;	 //Knit Id for Kintting Plan
				var theename=this.contentDoc.getElementById("hidden_booking_no").value; //all data for Kintting Plan
				var booking_without_order=this.contentDoc.getElementById("booking_without_order").value; //Access form field with id="emailfield"
				
				if(theemail!="")
				{
					freeze_window(5);
					set_receive_basis(1);
					if(recieve_basis==1)
					{
						get_php_form_data(theemail+"**"+booking_without_order, "populate_data_from_booking", "requires/grey_production_entry_controller" );
						show_list_view(theename+"**"+booking_without_order,'show_fabric_desc_listview','list_fabric_desc_container','requires/grey_production_entry_controller','');
					}
					else
					{
						var data=theename.split("**");
						
						$('#txt_booking_no').val(theemail);
						$('#txt_booking_no_id').val(theemail);
						$('#booking_without_order').val(booking_without_order);
						$('#cbo_body_part').val(data[0]);
						$('#fabric_desc_id').val(data[1]);
						$('#txt_fabric_description').val(data[2]);
						$('#txt_gsm').val(data[3]);
						$('#txt_width').val(data[4]);
						$('#txt_job_no').val(data[5]);
						$('#cbo_buyer_name').val(data[6]);
						$('#all_po_id').val(data[7]);
						$('#cbo_knitting_source').val(data[8]);
						$('#txt_stitch_length').val(data[10]);
						$('#cbo_color_range').val(data[11]);
						$('#txt_brand').val(data[14]);
						set_multiselect('cbo_yarn_count','0','1',data[12],'0');
						$('#txt_yarn_lot').val(data[13]);
						$('#txt_color').val(data[15]);
						$('#color_id').val(data[16]);
						
						load_drop_down( 'requires/grey_production_entry_controller',data[8]+'_'+document.getElementById('cbo_company_id').value, 'load_drop_down_knitting_com','knitting_com');
						$('#cbo_knitting_company').val(data[9]);
						show_list_view(theemail,'show_fabric_desc_listview_plan','list_fabric_desc_container','requires/grey_production_entry_controller','');
					}
					//set_auto_complete(2);
					release_freezing();
				} 
			}
		}
	}
	
	function set_form_data(data)
	{
		var data=data.split("**");
		$('#cbo_body_part').val(data[0]);
		$('#txt_fabric_description').val(data[1]);
		$('#txt_gsm').val(data[2]);
		$('#txt_width').val(data[3]);
		$('#fabric_desc_id').val(data[4]);
	}
	
	function set_form_data_plan(data)
	{
		var data=data.split("**");
		$('#cbo_body_part').val(data[0]);
		$('#txt_fabric_description').val(data[1]);
		$('#txt_gsm').val(data[2]);
		$('#txt_width').val(data[3]);
		$('#fabric_desc_id').val(data[4]);
		$('#cbo_color_range').val(data[5]);
		$('#txt_stitch_length').val(data[6]);
		set_multiselect('cbo_yarn_count','0','1',data[7],'0');
		$('#txt_yarn_lot').val(data[8]);
	}
	
	function openmypage_fabricDescription()
	{
		var title = 'Fabric Description Info';	
		var page_link = 'requires/grey_production_entry_controller.php?action=fabricDescription_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=370px,center=1,resize=1,scrolling=0','');
		
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemail=this.contentDoc.getElementById("hidden_desc_id").value;	 //Access form field with id="emailfield"
			var theename=this.contentDoc.getElementById("hidden_desc_no").value; //Access form field with id="emailfield"
			var theegsm=this.contentDoc.getElementById("hidden_gsm").value; //Access form field with id="emailfield"
			
			$('#txt_fabric_description').val(theename);
			$('#fabric_desc_id').val(theemail);
			$('#txt_gsm').val(theegsm);
		}
	}
	
	function openmypage_po()
	{
		var receive_basis=$('#cbo_receive_basis').val();
		var booking_no=$('#txt_booking_no').val();
		var cbo_company_id = $('#cbo_company_id').val();
		var dtls_id = $('#update_dtls_id').val();
		var roll_maintained = $('#roll_maintained').val();
		var barcode_generation = $('#barcode_generation').val();
		var save_data = $('#save_data').val();
		var all_po_id = $('#all_po_id').val();
		var txt_receive_qnty = $('#txt_receive_qnty').val(); 
		var txt_reject_fabric_recv_qnty = $('#txt_reject_fabric_recv_qnty').val(); 
		var distribution_method = $('#distribution_method_id').val();
		
		var cbo_body_part=$('#cbo_body_part').val();
		var txt_fabric_description=$('#txt_fabric_description').val();
		var txt_gsm=$('#txt_gsm').val();
		var txt_width=$('#txt_width').val();
		var fabric_desc_id=$('#fabric_desc_id').val();
		var txt_deleted_id=$('#txt_deleted_id').val();
			
		if((receive_basis==1 || receive_basis==2) && booking_no=="")
		{
			alert("Please Select Booking No. / Knit Plan");
			$('#txt_booking_no').focus();
			return false;
		}
		else if((receive_basis==1 || receive_basis==2) && txt_fabric_description=="")
		{
			alert("Please Select Fabric Description.");
			$('#txt_fabric_description').focus();
			return false;
		}
		else if(receive_basis==0 && cbo_company_id==0)
		{
			alert("Please Select Company.");
			$('#cbo_company_id').focus();
			return false;
		}
		
		if(roll_maintained==1) 
		{
			popup_width='940px';
		}
		else
		{
			popup_width='840px';
		}
	
		var title = 'PO Info';	
		var page_link = 'requires/grey_production_entry_controller.php?receive_basis='+receive_basis+'&cbo_company_id='+cbo_company_id+'&booking_no='+booking_no+'&dtls_id='+dtls_id+'&all_po_id='+all_po_id+'&roll_maintained='+roll_maintained+'&barcode_generation='+barcode_generation+'&save_data='+save_data+'&txt_receive_qnty='+txt_receive_qnty+'&prev_distribution_method='+distribution_method+'&cbo_body_part='+cbo_body_part+'&txt_gsm='+txt_gsm+'&txt_width='+txt_width+'&fabric_desc_id='+fabric_desc_id+'&txt_deleted_id='+txt_deleted_id+'&txt_reject_fabric_recv_qnty='+txt_reject_fabric_recv_qnty+'&action=po_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width='+popup_width+',height=430px,center=1,resize=1,scrolling=0','');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var save_string=this.contentDoc.getElementById("save_string").value;	 //Access form field with id="emailfield"
			var tot_grey_qnty=this.contentDoc.getElementById("tot_grey_qnty").value; //Access form field with id="emailfield"
			var tot_reject_qnty=this.contentDoc.getElementById("tot_reject_qnty").value;
			var number_of_roll=this.contentDoc.getElementById("number_of_roll").value; //Access form field with id="emailfield"
			var all_po_id=this.contentDoc.getElementById("all_po_id").value; //Access form field with id="emailfield"
			var distribution_method=this.contentDoc.getElementById("distribution_method").value;
			var hide_deleted_id=this.contentDoc.getElementById("hide_deleted_id").value;
			
			$('#save_data').val(save_string);
			$('#txt_receive_qnty').val(tot_grey_qnty);
			$('#txt_reject_fabric_recv_qnty').val(tot_reject_qnty);
			if(roll_maintained==1)
			{
				$('#txt_roll_no').val(number_of_roll);
				$('#txt_deleted_id').val(hide_deleted_id);
			}
			else
			{
				$('#txt_deleted_id').val('');
			}
			
			$('#all_po_id').val(all_po_id);
			$('#distribution_method_id').val(distribution_method);
		}
	}
	
	/*function check_machine(data)
	{
		var response=return_global_ajax_value( data, 'check_machine_no', '', 'requires/grey_production_entry_controller');
		var response=response.split("_");
		
		if(response[0]==0)
		{
			alert('Machine no not found in library.');
			$('#txt_machine_no').val(''); 
			$('#txt_machine_no_id').val('');
		}
		else
		{
			$('#txt_machine_no_id').val(response[1]);
		}
	}*/
	
	function fnc_grey_production_entry(operation)
	{
		if(operation==2)
		{
			show_msg('13');
			return;
		}
		
		if( form_validation('cbo_company_id*txt_receive_date','Company*Production Date')==false )
		{
			return;
		}
		
		if($('#fabric_store_auto_update').val()==1)
		{
			if( form_validation('cbo_store_name','Store Name')==false )
			{
				return;
			}
		}
		
		if( form_validation('cbo_knitting_source*cbo_knitting_company*cbo_body_part*txt_fabric_description*txt_receive_qnty*cbo_location_name','Knitting Source*Knitting Com*Body Part*Fabric Description*Grey Receive Qnty*Location Name')==false )
		{
			return;
		}
		
		if($('#cbo_receive_basis').val()==1 && $('#txt_booking_no').val()=="")
		{
			alert("Please Select Booking No");
			$('#txt_booking_no').focus();
			return;
		}
		if($('#cbo_knitting_source').val()==1 && $('#cbo_machine_name').val()==0)
		{
			alert("Please Select Machine Name");
			$('#cbo_machine_name').focus();
			return;
		}
		if($('#cbo_knitting_source').val()==1 && $('#txt_shift_name').val()==0)
		{
			alert("Please Select Shift Name");
			$('#txt_shift_name').focus();
			return;
		}
		
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_recieved_id*cbo_company_id*cbo_receive_basis*txt_receive_date*txt_receive_chal_no*txt_booking_no_id*txt_booking_no*cbo_store_name*cbo_knitting_source*cbo_knitting_company*cbo_location_name*txt_yarn_issue_challan_no*cbo_buyer_name*txt_yarn_issued*cbo_body_part*txt_fabric_description*fabric_desc_id*txt_gsm*txt_width*cbo_floor_id*cbo_machine_name*txt_roll_no*txt_remarks*txt_receive_qnty*txt_room*txt_reject_fabric_recv_qnty*txt_shift_name*txt_rack*cbo_uom*txt_self*txt_yarn_lot*txt_binbox*cbo_yarn_count*txt_brand*txt_color*color_id*cbo_color_range*txt_stitch_length*update_id*all_po_id*update_dtls_id*update_trans_id*save_data*previous_prod_id*hidden_receive_qnty*roll_maintained*fabric_store_auto_update*booking_without_order*txt_deleted_id',"../");
		
		freeze_window(operation);
		
		http.open("POST","requires/grey_production_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange =fnc_grey_production_entry_Reply_info;
	}
	
	function fnc_grey_production_entry_Reply_info()
	{
		if(http.readyState == 4) 
		{
			 //alert(http.responseText);return;
			var reponse=trim(http.responseText).split('**');	
			if(reponse[0]==30)
			{
				alert(reponse[1]);
				release_freezing();	
				return;
			}	
			show_msg(reponse[0]);
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_recieved_id').value = reponse[2];
				
				$('#cbo_company_id').attr('disabled','disabled');
				
				show_list_view(reponse[1],'show_grey_prod_listview','list_container_knitting','requires/grey_production_entry_controller','');
				
				var receive_basis=$('#cbo_receive_basis').val();
				if(receive_basis==2)
				{
					reset_form('greyproductionentry_1','roll_details_list_view','','','','update_id*txt_recieved_id*cbo_company_id*cbo_receive_basis*txt_receive_date*txt_receive_chal_no*txt_booking_no*txt_booking_no_id*cbo_buyer_name*cbo_store_name*cbo_knitting_source*cbo_knitting_company*cbo_location_name*txt_yarn_issue_challan_no*txt_job_no*txt_remarks*roll_maintained*barcode_generation*fabric_store_auto_update*booking_without_order*txt_yarn_issued*cbo_body_part*fabric_desc_id*txt_fabric_description*txt_gsm*txt_width*txt_stitch_length*cbo_color_range*cbo_yarn_count*txt_yarn_lot');
				}
				else
				{
					reset_form('greyproductionentry_1','roll_details_list_view','','','','update_id*txt_recieved_id*cbo_company_id*cbo_receive_basis*txt_receive_date*txt_receive_chal_no*txt_booking_no*txt_booking_no_id*cbo_buyer_name*cbo_store_name*cbo_knitting_source*cbo_knitting_company*cbo_location_name*txt_yarn_issue_challan_no*txt_job_no*txt_remarks*roll_maintained*barcode_generation*fabric_store_auto_update*booking_without_order*txt_yarn_issued');
				}
				
				set_button_status(0, permission, 'fnc_grey_production_entry',1);
				var roll_maintained=$('#roll_maintained').val();
				/*if(roll_maintained==1)	
				{
					put_data_dtls_part(trim(reponse[4]),'populate_grey_details_form_data', 'requires/grey_production_entry_controller');
				}
				else
				{
					set_button_status(0, permission, 'fnc_grey_production_entry',1);
				}*/
			}
			else if(reponse[0]==20)
			{
				alert(reponse[1]);
			}
			release_freezing();	
		}
	}
	
	function grey_receive_popup()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var page_link='requires/grey_production_entry_controller.php?cbo_company_id='+cbo_company_id+'&action=grey_receive_popup_search';
			var title='Grey Production Form';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=900px,height=390px,center=1,resize=1,scrolling=0','');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var grey_recv_id=this.contentDoc.getElementById("hidden_recv_id").value;
				if(trim(grey_recv_id)!="")
				{
					freeze_window(5);
					reset_form('greyproductionentry_1','list_container_knitting*list_fabric_desc_container','','','','roll_maintained*barcode_generation*fabric_store_auto_update');
					get_php_form_data(grey_recv_id, "populate_data_from_grey_recv", "requires/grey_production_entry_controller" );
					var txt_booking_no = $('#txt_booking_no').val();
					var booking_without_order = $('#booking_without_order').val();
					var cbo_receive_basis = $('#cbo_receive_basis').val();
					if(txt_booking_no!="" && cbo_receive_basis==1)
					{
						show_list_view(txt_booking_no+"**"+booking_without_order,'show_fabric_desc_listview','list_fabric_desc_container','requires/grey_production_entry_controller','');
					}
					else if(txt_booking_no!="" && cbo_receive_basis==2)
					{
						show_list_view(txt_booking_no,'show_fabric_desc_listview_plan','list_fabric_desc_container','requires/grey_production_entry_controller','');	
					}
					show_list_view(grey_recv_id,'show_grey_prod_listview','list_container_knitting','requires/grey_production_entry_controller','');
					release_freezing();
				}
			}
		}
	}
	
	function put_data_dtls_part(id,type,page_path)
	{
		//get_php_form_data(id+"**"+$('#roll_maintained').val(), type, page_path );
		var roll_maintained=$('#roll_maintained').val();
		var barcode_generation = $('#barcode_generation').val();
		get_php_form_data(id+"**"+roll_maintained, type, page_path );
		if(roll_maintained==1)
		{
			show_list_view("'"+id+"**"+barcode_generation+"'",'show_roll_listview','roll_details_list_view','requires/grey_production_entry_controller','');
		}
		else
		{
			$('#roll_details_list_view').html('');
		}
	}
	
	function set_auto_complete(type)
	{
		if(type==1)
		{
			$("#txt_color").autocomplete({
				 source: str_color
			});
		}
		else
		{
			var receive_basis=$('#cbo_receive_basis').val();
			var booking_id = $('#txt_booking_no_id').val();
			var booking_without_order = $('#booking_without_order').val();
			get_php_form_data(booking_id+"**"+booking_without_order+"**"+receive_basis, 'load_color', 'requires/grey_production_entry_controller');
		}
	}
	
	function openmypage_color()
	{
		var recieve_basis = $('#cbo_receive_basis').val();
		var booking_id = $('#txt_booking_no_id').val();
		var booking_without_order = $('#booking_without_order').val();
		if(recieve_basis==1 || recieve_basis==2)
		{
			if (form_validation('txt_booking_no','F. Booking/Knit Plan')==false)
			{
				return;
			}
		}
		var color_id = $('#color_id').val();
		var title = 'Color Info';	
		var page_link='requires/grey_production_entry_controller.php?recieve_basis='+recieve_basis+'&color_id='+color_id+'&booking_id='+booking_id+'&booking_without_order='+booking_without_order+'&action=color_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=350px,height=350px,center=1,resize=1,scrolling=0','');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemail=this.contentDoc.getElementById("hidden_color_id").value;	 //Access form field with id="emailfield"
			var theename=this.contentDoc.getElementById("hidden_color_no").value; //Access form field with id="emailfield"
			
			$('#txt_color').val(theename);
			$('#color_id').val(theemail);
		}
	}
	
	function issue_challan_no()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var page_link='requires/grey_production_entry_controller.php?cbo_company_id='+cbo_company_id+'&action=issue_challan_no_popup';
			var title='Issue Challan Info';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=400px,height=390px,center=1,resize=1,scrolling=0','');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var issue_challan=this.contentDoc.getElementById("issue_challan").value;
				if(trim(issue_challan)!="")
				{
					freeze_window(5);
					$('#txt_yarn_issue_challan_no').val(issue_challan);
					release_freezing();
				}
			}
		}
	}
	
	function fnc_check_issue(issue_num)
	{
		if(issue_num!="")
		{
			var issue_result = trim(return_global_ajax_value(issue_num, 'issue_num_check', '', 'requires/grey_production_entry_controller'));
			if(issue_result=="")
			{
				alert("Challan Number Not Found");
				$('#txt_yarn_issue_challan_no').val("");
			}
		}
	}
	
	function check_all_report()
	{
		$("input[name=chkBundle]").each(function(index, element) { 
				
				if( $('#check_all').prop('checked')==true) 
					$(this).attr('checked','true');
				else
					$(this).removeAttr('checked');
		});
	}
	
	function fnc_send_printer_text()
	{
		var dtls_id=$('#update_dtls_id').val();
		if(dtls_id=="")
		{
			alert("Save First");	
			return;
		}
		var data="";
		var error=1;
		$("input[name=chkBundle]").each(function(index, element) {
			if( $(this).prop('checked')==true)
			{
				error=0;
				var idd=$(this).attr('id').split("_");
				var roll_id=$('#txtRollTableId_'+idd[1] ).val();
				if(roll_id!="")
				{
					if(data=="") data=$('#txtRollTableId_'+idd[1] ).val(); else data=data+","+$('#txtRollTableId_'+idd[1] ).val();
				}
				else
				{
					$(this).prop('checked',false);
				}
			}
		});
	
		if( error==1 )
		{
			alert('No data selected');
			return;
		}
		
		data=data+"***"+dtls_id;
		var url=return_ajax_request_value(data, "report_barcode_text_file", "requires/grey_production_entry_controller");
		window.open("requires/"+trim(url)+".zip","##");
	}
	
	function fnc_barcode_generation()
	{
		var dtls_id=$('#update_dtls_id').val();
		if(dtls_id=="")
		{
			alert("Save First");	
			return;
		}
		var data="";
		var error=1;
		$("input[name=chkBundle]").each(function(index, element) {
			if( $(this).prop('checked')==true)
			{
				error=0;
				var idd=$(this).attr('id').split("_");
				var roll_id=$('#txtRollTableId_'+idd[1] ).val();
				if(roll_id!="")
				{
					if(data=="") data=$('#txtRollTableId_'+idd[1] ).val(); else data=data+","+$('#txtRollTableId_'+idd[1] ).val();
				}
				else
				{
					$(this).prop('checked',false);
				}
			}
		});
	
		if( error==1 )
		{
			alert('No data selected');
			return;
		}
		
		data=data+"***"+dtls_id;
		window.open("requires/grey_production_entry_controller.php?data=" + data+'&action=report_barcode_generation', true );
	}
	
	function load_location()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var cbo_knitting_source = $('#cbo_knitting_source').val();
		var cbo_knitting_company = $('#cbo_knitting_company').val();
		if(cbo_knitting_source==1)
		{
			load_drop_down( 'requires/grey_production_entry_controller',cbo_knitting_company, 'load_drop_down_location', 'location_td' );
		}
		else
		{
			load_drop_down( 'requires/grey_production_entry_controller',cbo_company_id, 'load_drop_down_location', 'location_td' );
		}
	}
	
	function load_floor()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var cbo_knitting_source = $('#cbo_knitting_source').val();
		var cbo_knitting_company = $('#cbo_knitting_company').val();
		var cbo_location_name = $('#cbo_location_name').val();
		if(cbo_knitting_source==1)
		{
			load_drop_down( 'requires/grey_production_entry_controller',cbo_knitting_company+'_'+cbo_location_name, 'load_drop_down_floor', 'floor_td' );
		}
		else
		{
			load_drop_down( 'requires/grey_production_entry_controller',cbo_company_id+'_'+cbo_location_name, 'load_drop_down_floor', 'floor_td' );
		}
	}
	
	function load_machine()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var cbo_knitting_source = $('#cbo_knitting_source').val();
		var cbo_knitting_company = $('#cbo_knitting_company').val();
		var cbo_floor_id = $('#cbo_floor_id').val();
		if(cbo_knitting_source==1)
		{
			load_drop_down( 'requires/grey_production_entry_controller',cbo_knitting_company+'_'+cbo_floor_id, 'load_drop_machine', 'machine_td' );
		}
		else
		{
			load_drop_down( 'requires/grey_production_entry_controller',cbo_company_id+'_'+cbo_floor_id, 'load_drop_machine', 'machine_td' );
		}
	}

</script>
<body onLoad="set_hotkey()">
<div style="width:100%;">
	<?php echo load_freeze_divs ("../",$permission); ?>
    <form name="greyproductionentry_1" id="greyproductionentry_1">
    	<div style="width:950px; float:left;" align="center">        
            <fieldset style="width:950px">
            <legend>Knitting Production Entry</legend>
			<fieldset style="width:930px">
            <table cellpadding="0" cellspacing="2" width="100%">
                <tr>
                    <td align="right" colspan="3"><strong>Production ID</strong></td>
                    <td>
                        <input type="hidden" name="update_id" id="update_id" />
                        <input type="text" name="txt_recieved_id" id="txt_recieved_id" class="text_boxes" style="width:145px" placeholder="Double Click" onDblClick="grey_receive_popup();" >
                    </td>
                </tr>
                <tr>
                    <td width="110" class="must_entry_caption"> Company </td>
                    <td width="150">
						<?php 
							echo create_drop_down( "cbo_company_id", 151, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", 0, "load_drop_down( 'requires/grey_production_entry_controller', document.getElementById('cbo_receive_basis').value+'_'+this.value, 'load_drop_down_buyer', 'buyer_td_id' );load_drop_down( 'requires/grey_production_entry_controller', this.value, 'load_drop_down_location', 'location_td' );load_drop_down( 'requires/grey_production_entry_controller', this.value, 'load_drop_down_store', 'store_td' );load_drop_down( 'requires/grey_production_entry_controller', this.value, 'load_drop_down_floor', 'floor_td' );load_drop_down( 'requires/grey_production_entry_controller', this.value, 'load_drop_machine', 'machine_td' );get_php_form_data(this.value,'roll_maintained','requires/grey_production_entry_controller' );" );
                        ?>
                    </td>
                    <td width="110"> Production Basis </td>
                    <td width="150">
                        <?php 
                        	$receive_basis=array(0=>"Independent",1=>"Fabric Booking",2=>"Knitting Plan");
                        	echo create_drop_down("cbo_receive_basis",152,$receive_basis,"",0,"",1,"set_receive_basis(1);");
                        ?>
                    </td>
                    <td width="110" class="must_entry_caption"> Production Date </td>
                    <td width="150">
                        <input class="datepicker" type="date" style="width:140px" name="txt_receive_date" id="txt_receive_date" readonly/>
                    </td>
                </tr> 
                <tr>
                    <td> Receive Challan No </td>
                    <td>
                        <input type="text" name="txt_receive_chal_no" id="txt_receive_chal_no" class="text_boxes" style="width:140px" >
                    </td>
                    <td>F. Booking/Knit Plan</td>
                    <td>
                    	<input type="text" name="txt_booking_no" id="txt_booking_no" class="text_boxes" style="width:140px"  placeholder="Double Click to Search" onDblClick="openmypage_fabricBooking();" readonly>
                        <input type="hidden" name="txt_booking_no_id" id="txt_booking_no_id" class="text_boxes">
                        <input type="hidden" name="booking_without_order" id="booking_without_order"/>
                    </td>
                    <td class="must_entry_caption"> Store Name </td>
                    <td id="store_td">
						<?php
                            echo create_drop_down( "cbo_store_name", 152, $blank_array,"",1, "--Select Store--", 1, "" );
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="must_entry_caption"> Knitting Source </td>
                    <td>
						<?php
							echo create_drop_down("cbo_knitting_source",150,$knitting_source,"", 1, "-- Select --", 0,"load_drop_down( 'requires/grey_production_entry_controller', this.value+'_'+document.getElementById('cbo_company_id').value, 'load_drop_down_knitting_com','knitting_com');",0,'1,3');
                        ?>
                    </td>
                    <td class="must_entry_caption">Knitting Com</td>
                    <td id="knitting_com">
						<?php
                        	echo create_drop_down( "cbo_knitting_company", 152, $blank_array,"",1, "--Select Knit Company--", 1, "" );
                        ?>
                    </td>
                    <td class="must_entry_caption">Location </td>                                              
                    <td id="location_td">
						<?php 
							echo create_drop_down( "cbo_location_name", 152, $blank_array,"", 1, "-- Select Location --", 0, "" );
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Yarn Issue Ch. No</td>                                              
                    <td> 
                    	<input type="text" name="txt_yarn_issue_challan_no" id="txt_yarn_issue_challan_no" placeholder="Browse" onDblClick="issue_challan_no();" class="text_boxes" style="width:140px" onBlur="fnc_check_issue(this.value);">
                    </td> 
                    <td>Yarn Issued </td>
                    <td>
                    	<input type="text" name="txt_yarn_issued" id="txt_yarn_issued" class="text_boxes_numeric" style="width:140px" readonly>
                    </td> 
                    <td>Job No</td>
                    <td>
                    	<input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:140px" readonly>
                    </td>
                </tr>
                <tr>
                    <td>Buyer</td>
                    <td id="buyer_td_id"> 
                        <?php
                           echo create_drop_down( "cbo_buyer_name", 152, $blank_array,"", 1, "-- Select Buyer --", 0, "",1 );
                        ?>
                    </td>
                    <td>Remarks </td>                                              
                    <td colspan="3"> 
                        <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:473px">
                    </td> 
                </tr>                                      
            </table>
			</fieldset>
            <table cellpadding="0" cellspacing="1" width="935" border="0">
                <tr>
                    <td width="64%" valign="top">
                        <fieldset>
                        <legend>New Entry</legend>
                            <table cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td class="must_entry_caption">Body Part</td>
                                    <td>
                                     <?php 
										echo create_drop_down( "cbo_body_part", 130, $body_part,"", 1, "-- Select Body Part --", 0, "",1 );
                                     ?>
                                    </td>
                                    <td>UOM</td>
                                    <td>
										<?php
                                            echo create_drop_down( "cbo_uom", 132, $unit_of_measurement,"", 0, "", '12', "",1,12 );
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="must_entry_caption">Fabric Description </td>
                                    <td colspan="3">
                                        <input type="text" name="txt_fabric_description" id="txt_fabric_description" class="text_boxes" style="width:400px" onDblClick="openmypage_fabricDescription()" placeholder="Double Click To Search" disabled="disabled" readonly/>
                                        <input type="hidden" name="fabric_desc_id" id="fabric_desc_id" class="text_boxes">
                                    </td>
                                </tr>
                                <tr>
                                    <td>GSM</td>
                                    <td>
                                        <input type="text" name="txt_gsm" id="txt_gsm" class="text_boxes_numeric" style="width:120px;"  />	
                                    </td>
                                    <td>Yarn Count</td>
                                    <td>
                                    <?php
                                        echo create_drop_down("cbo_yarn_count",132,"select id,yarn_count from lib_yarn_count where is_deleted=0 and status_active=1 order by yarn_count","id,yarn_count",0, "-- Select --", $selected, "");
                                    ?>
                                    </td>
                                </tr> 
                                <tr>
                                    <td>Dia / Width</td>
                                    <td>
                                        <input type="text" name="txt_width" id="txt_width" class="text_boxes" style="width:120px;text-align:right;" />	
                                    </td>
                                    <td>Brand</td>
                                    <td>
                                        <input type="text" name="txt_brand" id="txt_brand" class="text_boxes" style="width:120px" /> 
                                    </td>
                                </tr>
                                <tr>
                                	<td>Stitch Length</td>
                                    <td>
                                        <input type="text" name="txt_stitch_length" id="txt_stitch_length" class="text_boxes" style="width:120px;"/>
                                    </td>
                                    <td>Shift Name</td>
                                    <td>
                                        <!--<input type="text" name="txt_shift_name" id="txt_shift_name" class="text_boxes" style="width:120px;"  />-->
                                        <?php 
											echo create_drop_down( "txt_shift_name", 132, $shift_name,"", 1, "-- Select Shift --", 0, "",'' );
										?>	
                                    </td>
                                </tr>
                                <tr>
                                	<td>No of Roll</td>
                                    <td>
                                        <input type="text" name="txt_roll_no" id="txt_roll_no" class="text_boxes_numeric" style="width:120px" />
                                    </td>
                                    <td>Prod. Floor</td>
                                    <td id="floor_td">
                                    	<?php echo create_drop_down( "cbo_floor_id", 132, $blank_array,"", 1, "-- Select Floor --", 0, "",0 ); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td class="must_entry_caption">Grey Prod. Qnty</td>
                                    <td>
                                        <input type="text" name="txt_receive_qnty" id="txt_receive_qnty" class="text_boxes_numeric" style="width:120px;" onClick="openmypage_po()" placeholder="Single Click" readonly/>	
                                    </td>
                                    <td>Machine No.</td>
                                    <td id="machine_td">
                                    	<?php echo create_drop_down( "cbo_machine_name", 132, $blank_array,"", 1, "-- Select Machine --", 0, "",0 ); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td>Fabric Color</td>
                                    <td>
                                        <input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:120px;" placeholder="Browse" onDblClick="openmypage_color();" readonly/>
                                        <input type="hidden" name="color_id" id="color_id" />
                                    </td>
                                    <td>Room</td>
                                    <td>
                                        <input type="text" name="txt_room" id="txt_room" class="text_boxes_numeric" style="width:120px">
                                    </td>
                                </tr> 
                                <tr>
                                	<td>Color Range</td>
                                    <td>
                                        <?php
											echo create_drop_down( "cbo_color_range", 132, $color_range,"",1, "-- Select --", 0, "" );
										?>
                                    </td>
                                    <td>Rack</td>
                                    <td>
                                        <input type="text" name="txt_rack" id="txt_rack" class="text_boxes" style="width:120px">
                                    </td>
                                </tr>
                                <tr>
                                	<td>Reject Fabric Receive</td>
                                    <td>
                                        <input type="text" name="txt_reject_fabric_recv_qnty" id="txt_reject_fabric_recv_qnty" class="text_boxes_numeric" style="width:120px;" placeholder="Display" readonly />	
                                    </td>
                                    <td>Shelf</td>
                                    <td>
                                        <input type="text" name="txt_self" id="txt_self" class="text_boxes_numeric" style="width:120px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Yarn Lot</td>
                                    <td>
                                        <input type="text" name="txt_yarn_lot" id="txt_yarn_lot" class="text_boxes" style="width:120px" /> 
                                    </td>
                                	<td>Bin/Box</td>
                                    <td>
                                        <input type="text" name="txt_binbox" id="txt_binbox" class="text_boxes_numeric" style="width:120px">
                                    </td>
                                </tr>
                             </table>
                        </fieldset>
                    </td>                    
                    <td width="1%" valign="top"></td>
                    <td width="35%" valign="top">
                    	<div id="roll_details_list_view"></div>
                        <!--<fieldset style="display:none">
                        <legend>Display</legend>
                            <table  cellpadding="0" cellspacing="2" width="100%" >
                                <tr>
                                    <td>&nbsp;</td> <td>&nbsp;</td>
                                </tr>
                                <tr>
                                <tr>
                                    <td>Yarn to Knit Comp</td>
                                    <td>
                                        <input type="text"  class="text_boxes_numeric" name="txt_yern_to_knit_com" id="txt_yern_to_knit_com" style="width:135px" readonly />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Grey Received </td>
                                    <td>
                                        <input type="text"  class="text_boxes_numeric" name="txt_total_grey_recieved" id="txt_total_grey_recieved" style="width:135px" readonly />
                                     </td>
                                </tr>
                                <tr>
                                    <td> Reject Grey Fab. Rceived</td>
                                    <td>
                                        <input  type="text" class="text_boxes_numeric" name="txt_reject_fabric_receive" id="txt_reject_fabric_receive" style="width:135px" readonly/>
                                    </td>                               
                                </tr>
                                <tr>
                                    <td>Yet to Receive</td> 
                                    <td>
                                        <input type="text" class="text_boxes_numeric" name="txt_yet_recieved" id="txt_yet_recieved" style="width:135px" readonly />
                                    </td>  
                                </tr>                                    
                            </table>
                        </fieldset>	-->
                    </td>                       
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" align="center" class="button_container">
						<?php 
                            echo load_submit_buttons($permission, "fnc_grey_production_entry", 0,0,"reset_form('greyproductionentry_1','list_container_knitting*list_fabric_desc_container*roll_details_list_view','','cbo_receive_basis,1','disable_enable_fields(\'cbo_company_id\');set_receive_basis(2);')",1);//set_auto_complete(1);
                        ?>
                        <input type="hidden" name="save_data" id="save_data" readonly>
                        <input type="hidden" name="update_dtls_id" id="update_dtls_id" readonly>
                        <input type="hidden" name="update_trans_id" id="update_trans_id" readonly>
                        <input type="hidden" name="previous_prod_id" id="previous_prod_id" readonly>
                        <input type="hidden" name="hidden_receive_qnty" id="hidden_receive_qnty" readonly>
                        <input type="hidden" name="all_po_id" id="all_po_id" readonly>
                        <input type="hidden" name="roll_maintained" id="roll_maintained" readonly>
                        <input type="hidden" name="barcode_generation" id="barcode_generation" readonly>
                        <input type="hidden" name="fabric_store_auto_update" id="fabric_store_auto_update" readonly>
                        <input type="hidden" name="distribution_method_id" id="distribution_method_id" readonly />
                        <input type="hidden" name="txt_deleted_id" id="txt_deleted_id" readonly />
                    </td>	  
                </tr>
            </table>
            <div style="width:930px;" id="list_container_knitting"></div>
		</fieldset>
        </div>  
        <div style="width:10px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>
        <div id="list_fabric_desc_container" style="max-height:500px; width:340px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>
	</form>
</div>
</body>
<script>
	set_multiselect('cbo_yarn_count','0','0','','0');
</script>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>