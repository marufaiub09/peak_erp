<?php
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$country_library=return_library_array( "select id,country_name from lib_country", "id", "country_name"  );

//------------------------------------------------------------------------------------------------------
if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location", 210, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "-- Select Location --", $selected, "load_drop_down( 'requires/cutting_update_controller', this.value, 'load_drop_down_floor', 'floor_td' );",0 );
}

if ($action=="load_drop_down_floor")
{
	echo create_drop_down( "cbo_floor", 210, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 and location_id='$data' order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected, "",0 );
}
 
if ($action=="load_variable_settings")
{
	echo "$('#sewing_production_variable').val(0);\n";
	$sql_result = sql_select("select cutting_update,production_entry from variable_settings_production where company_name=$data and variable_list=1 and status_active=1");
 	foreach($sql_result as $result)
	{
		echo "$('#sewing_production_variable').val(".$result[csf("cutting_update")].");\n";
		echo "$('#styleOrOrderWisw').val(".$result[csf("production_entry")].");\n";
	}
 	exit();
}

if($action=="load_drop_down_cutt_company")
{
	$explode_data = explode("**",$data);
	$data = $explode_data[0];
	$selected_company = $explode_data[1];
	
	if($data==3)
		echo create_drop_down( "cbo_cutting_company", 210, "select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0 and find_in_set(22,party_type) order by supplier_name","id,supplier_name", 1, "--- Select ---", $selected, "" );
	else if($data==1)
		echo create_drop_down( "cbo_cutting_company", 210, "select id,company_name from lib_company where is_deleted=0 and status_active=1 order by company_name","id,company_name", 1, "--- Select ---", $selected_company, "",0,0 );
	else
		echo create_drop_down( "cbo_cutting_company", 210, $blank_array,"", 1, "--- Select ---", $selected, "",0 );
	exit();
}

if($action=="order_popup")
{
extract($_REQUEST);
echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
?>
	<script>
		$(document).ready(function(e) {
            $("#txt_search_common").focus();
        });
		
		function search_populate(str)
		{
			//alert(str); 
			if(str==0)
			{
				document.getElementById('search_by_th_up').innerHTML="Order No";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';
			}
			else if(str==1)
			{
				document.getElementById('search_by_th_up').innerHTML="Style Ref. Number";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';
			}
			else //if(str==2)
			{
				var buyer_name = '<option value="0">--- Select Buyer ---</option>';
				<?php
				$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer where find_in_set($company,tag_company) and status_active=1 and is_deleted=0 order by buyer_name",'id','buyer_name');
				foreach($buyer_arr as $key=>$val)
				{
					echo "buyer_name += '<option value=\"$key\">".($val)."</option>';";
				}
				?>
				document.getElementById('search_by_th_up').innerHTML="Select Buyer Name";
				document.getElementById('search_by_td').innerHTML='<select	name="txt_search_common" style="width:230px " class="combo_boxes" id="txt_search_common">'+ buyer_name +'</select>';
			}
		}
	
	function js_set_value(id,item_id,po_qnty,plan_qnty,country_id,job_num)
	{
		$("#hidden_mst_id").val(id);
		$("#hidden_grmtItem_id").val(item_id);
		$("#hidden_po_qnty").val(po_qnty);
		$("#hidden_plancut_qnty").val(plan_qnty);
		
		$("#hidden_country_id").val(country_id);
		$("#hid_job_num").val(job_num);
   		parent.emailwindow.hide();
 	}
	
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="780" cellspacing="0" cellpadding="0" class="rpt_table" align="center" border="1" rules="all">
    		<tr>
        		<td align="center" width="100%">
            		<table cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                   		 <thead>
                        	<th width="130">Search By</th>
                        	<th  width="180" align="center" id="search_by_th_up">Enter Order Number</th>
                        	<th width="200">Date Range</th>
                        	<th width="80"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
                    	</thead>
        				<tr>
                    		<td width="130">
							<?php
							$searchby_arr=array(0=>"Order No",1=>"Style Ref. Number",2=>"Buyer Name");
							echo create_drop_down( "txt_search_by", 130, $searchby_arr,"", 1, "-- Select Sample --", $selected, "search_populate(this.value)",0 );
  							?>
                    		</td>
                   			<td width="180" align="center" id="search_by_td">
								<input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" onKeyDown="if (event.keyCode == 13) document.getElementById('btn_show').click()" />
            				</td>
                    		<td align="center">
                            	<input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px"> To
					  			<input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 		</td>
            		 		<td align="center">
                     			<input type="button" name="btn_show" id="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $garments_nature; ?>, 'create_po_search_list_view', 'search_div', 'cutting_update_controller', 'setFilterGrid(\'tbl_po_list\',-1)')" style="width:100px;" />
                            </td>
        				</tr>
             		</table>
          		</td>
        	</tr>
        	<tr>
            	<td  align="center" height="40" valign="middle">
					<?php echo load_month_buttons(1);  ?>
                    <input type="hidden" id="hidden_mst_id">
                    <input type="hidden" id="hidden_grmtItem_id">
                    <input type="hidden" id="hidden_po_qnty">
                    <input type="hidden" id="hidden_plancut_qnty">
                    <input type="hidden" id="hidden_country_id">
                    <input type="text" id="hid_job_num">
          		</td>
            </tr>
    	</table>
        <div style="margin-top:10px" id="search_div"></div>
    </form>
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}
if($action=="create_po_search_list_view")
{
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	$garments_nature = $ex_data[5];
	//print_r ($ex_data);
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==0)
			$sql_cond = " and b.po_number like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==1)
			$sql_cond = " and a.style_ref_no like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==2)
			$sql_cond = " and a.buyer_name=trim('$txt_search_common')";		
 	}
	if($txt_date_from!="" || $txt_date_to!="") $sql_cond .= " and b.shipment_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if(trim($company)!="") $sql_cond .= " and a.company_name='$company'";
		
 	$sql = "select b.id,a.order_uom,a.buyer_name,a.company_name,a.total_set_qnty,a.set_break_down, a.job_no,a.style_ref_no,a.gmts_item_id,a.location_name,b.shipment_date,b.po_number,b.po_quantity ,b.plan_cut
			from wo_po_details_master a, wo_po_break_down b 
			where
			a.job_no = b.job_no_mst and
			a.status_active=1 and 
			a.is_deleted=0 and
			a.garments_nature=$garments_nature
			$sql_cond"; 
	//echo $sql;die;
	$result = sql_select($sql);
 	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	$po_country_arr=return_library_array( "select po_break_down_id, group_concat(distinct(country_id)) as country from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id",'po_break_down_id','country');
	
	$po_country_data_arr=array();
	$poCountryData=sql_select( "select po_break_down_id, item_number_id, country_id, sum(order_quantity) as qnty, sum(plan_cut_qnty) as plan_cut_qnty from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id, item_number_id, country_id");
	
	foreach($poCountryData as $row)
	{
		$po_country_data_arr[$row[csf('po_break_down_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]['po_qnty']=$row[csf('qnty')];
		$po_country_data_arr[$row[csf('po_break_down_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]['plan_cut_qnty']=$row[csf('plan_cut_qnty')];
	}
	
	?>
    <div style="width:930px;">
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="100">Shipment Date</th>
                <th width="100">Order No</th>
                <th width="100">Buyer</th>
                <th width="130">Style</th>
                <th width="140">Item</th>
                <th width="100">Country</th>
                <th width="100">Order Qnty</th>
                <th>Company Name</th>
            </thead>
     	</table>
     </div>
     <div style="width:930px; max-height:240px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="912" class="rpt_table" id="tbl_po_list">
			<?php
			$i=1;
            foreach( $result as $row )
            {
				$exp_grmts_item = explode("__",$row[csf("set_break_down")]);
				$numOfItem = count($exp_grmts_item);
				$set_qty=""; $grmts_item="";$job_num="";
				$job_num=$row[csf("job_no")];
				$country=explode(",",$po_country_arr[$row[csf("id")]]);
				$numOfCountry = count($country);
				
				for($k=0;$k<$numOfItem;$k++)								
				{
					if($row["total_set_qnty"]>1)
					{
						$grmts_item_qty = explode("_",$exp_grmts_item[$k]);
						$grmts_item = $grmts_item_qty[0];
						$set_qty = $grmts_item_qty[1];
					}else
					{
						$grmts_item_qty = explode("_",$exp_grmts_item[$k]);
						$grmts_item = $grmts_item_qty[0];
						$set_qty = $grmts_item_qty[1];
					}
					
					foreach($country as $country_id)
					{
						if ($i%2==0)  $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						
						//$po_qnty=$row[csf("po_quantity")]; $plan_cut_qnty=$row[csf("plan_cut")];
						$po_qnty=$po_country_data_arr[$row[csf('id')]][$grmts_item][$country_id]['po_qnty'];
						$plan_cut_qnty=$po_country_data_arr[$row[csf('id')]][$grmts_item][$country_id]['plan_cut_qnty'];
						
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value(<?php echo $row[csf("id")];?>,'<?php echo $grmts_item;?>','<?php echo $po_qnty;?>','<?php echo $plan_cut_qnty;?>','<?php echo $country_id;?>','<?php echo$job_num;?>');" > 
								<td width="40" align="center"><?php echo $i; ?></td>
								<td width="100" align="center"><?php echo change_date_format($row[csf("shipment_date")]);?></td>		
								<td width="100"><p><?php echo $row[csf("po_number")]; ?></p></td>
								<td width="100"><p><?php echo $buyer_arr[$row[csf("buyer_name")]]; ?></p></td>	
								<td width="130"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
								<td width="140"><p><?php  echo $garments_item[$grmts_item];?></p></td>	
								<td width="100"><p><?php echo $country_library[$country_id]; ?></p></td>
								<td width="100" align="right"><?php echo $po_qnty; //$po_qnty*$set_qty;?> </td>
								<td><?php  echo $company_arr[$row[csf("company_name")]];?> </td> 	
							</tr>
						<?php 
						$i++;
					}
				}
            }
   		?>
        </table>
    </div> 
	<?php	
exit();	
}

if($action=="populate_data_from_search_popup")
{
	$dataArr = explode("**",$data);
	$po_id = $dataArr[0];
	$item_id = $dataArr[1];
	$country_id = $dataArr[2];
 		
	$res = sql_select("select a.id,a.po_quantity,a.plan_cut, a.po_number,a.po_quantity,b.company_name, b.buyer_name, b.style_ref_no,b.gmts_item_id, b.order_uom, b.job_no,b.location_name  
			from wo_po_break_down a, wo_po_details_master b
			where a.job_no_mst=b.job_no and a.id=$po_id"); 
	
 	foreach($res as $result)
	{
		echo "$('#txt_order_no').val('".$result[csf('po_number')]."');\n";
		echo "$('#hidden_po_break_down_id').val('".$result[csf('id')]."');\n";
		echo "$('#cbo_buyer_name').val('".$result[csf('buyer_name')]."');\n";
		echo "$('#txt_style_no').val('".$result[csf('style_ref_no')]."');\n";
 		echo "$('#txt_cutting_qty').attr('placeholder','');\n";//initialize quatity input field
 		
		//$set_qty = return_field_value("set_item_ratio","wo_po_details_mas_set_details","job_no='".$result[csf('job_no')]."' and gmts_item_id='$item_id'");
		$plan_cut_qnty=return_field_value("sum(plan_cut_qnty)","wo_po_color_size_breakdown","po_break_down_id=".$result[csf('id')]." and item_number_id='$item_id' and country_id='$country_id' and status_active=1 and is_deleted=0");
		
		$total_produced = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$result[csf('id')]." and item_number_id='$item_id' and country_id='$country_id' and production_type=1 and is_deleted=0");
		
		echo "$('#txt_cumul_cutting').attr('placeholder','".$total_produced."');\n";
		echo "$('#txt_cumul_cutting').val('".$total_produced."');\n";
		$yet_to_produced = $plan_cut_qnty - $total_produced;
		echo "$('#txt_yet_cut').attr('placeholder','".$yet_to_produced."');\n";
		echo "$('#txt_yet_cut').val('".$yet_to_produced."');\n";
   	}
 	exit();	
}

if($action=="color_and_size_level")
{
		$dataArr = explode("**",$data);
		$po_id = $dataArr[0];
		$item_id = $dataArr[1];
		$variableSettings = $dataArr[2];
		$styleOrOrderWisw = $dataArr[3];
		$country_id = $dataArr[4];
		$job_num = $dataArr[5];
		
		$color_library=return_library_array( "select id, color_name from lib_color",'id','color_name');
		$size_library=return_library_array( "select id, size_name from lib_size",'id','size_name');
		//#############################################################################################//
		// order wise - color level, color and size level
		
		if( $variableSettings==2 ) // color level
		{
			$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, sum(plan_cut_qnty) as plan_cut_qnty, (select sum(CASE WHEN pro_garments_production_dtls.color_size_break_down_id=wo_po_color_size_breakdown.id then production_qnty ELSE 0 END) from pro_garments_production_dtls where is_deleted=0 and production_type=1 ) as production_qnty 
				from wo_po_color_size_breakdown
				where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 and status_active=1 group by color_number_id";
		}
		else if( $variableSettings==3 ) //color and size level
		{
			$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty, (select sum(CASE WHEN pro_garments_production_dtls.color_size_break_down_id=wo_po_color_size_breakdown.id then production_qnty ELSE 0 END) from pro_garments_production_dtls where is_deleted=0 and production_type=1 ) as production_qnty 
				from wo_po_color_size_breakdown
				where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 and status_active=1";// order by color_number_id,size_number_id
		}
		else // by default color and size level
		{
			$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty, (select sum(CASE WHEN pro_garments_production_dtls.color_size_break_down_id=wo_po_color_size_breakdown.id then production_qnty ELSE 0 END) from pro_garments_production_dtls where is_deleted=0 and production_type=1 ) as production_qnty 
				from wo_po_color_size_breakdown
				where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 and status_active=1";// order by color_number_id,size_number_id
		}
		
		//echo $sql;die;
		$colorResult = sql_select($sql);		
  		$colorHTML="";
		$colorID='';
		$chkColor = array(); 
		$i=0;$totalQnty=0;
 		foreach($colorResult as $color)
		{
			if( $variableSettings==2 ) // color level
			{ 
				$colorHTML .='<tr><td>'.$color_library[$color[csf("color_number_id")]].'</td><td><input type="text" name="txt_color" id="colSize_'.($i+1).'" style="width:80px"  class="text_boxes_numeric" placeholder="'.($color[csf("plan_cut_qnty")]-$color[csf("production_qnty")]).'" onblur="fn_colorlevel_total('.($i+1).')"></td></tr>';				
				$totalQnty += $color[csf("plan_cut_qnty")]-$color[csf("production_qnty")];
				$colorID .= $color[csf("color_number_id")].",";
			}
			else //color and size level
			{
				if( !in_array( $color[csf("color_number_id")], $chkColor ) )
				{
					if( $i!=0 ) $colorHTML .= "</table></div>";
					$i=0;
					$colorHTML .= '<h3 align="left" id="accordion_h'.$color[csf("color_number_id")].'" style="width:300px" class="accordion_h" onClick="accordion_menu( this.id,\'content_search_panel_'.$color[csf("color_number_id")].'\', \'\',1)">  <span id="accordion_h'.$color[csf("color_number_id")].'span">+</span>'.$color_library[$color[csf("color_number_id")]].' : <span id="total_'.$color[csf("color_number_id")].'"></span></h3>';
					$colorHTML .= '<div id="content_search_panel_'.$color[csf("color_number_id")].'" style="display:none" class="accord_close"><table id="table_'.$color[csf("color_number_id")].'">';
					$chkColor[] = $color[csf("color_number_id")];					
				}
					$did='colSize_'.$color[csf('color_number_id')].($i+1);
					$did="document.getElementById('".$did."').value";
					$colorhead='accordion_'.$color[csf("color_number_id")];
					$colorhead="document.getElementById('".$colorhead."').value";
					$sizehead='size_name'.$color[csf("color_number_id")];
					$sizehead="document.getElementById('".$sizehead."').value";
					$color_id='color_id'.$color[csf('color_number_id')].($i+1);
					$color_id="document.getElementById('".$color_id."').value";

 				//$index = $color[csf("size_number_id")].$color[csf("color_number_id")];
				$colorID .= $color[csf("size_number_id")]."*".$color[csf("color_number_id")].",";
 				$colorHTML .='<tr><td>'.$size_library[$color[csf("size_number_id")]].'<input type="hidden" name="size_name" id="size_name'.$color[csf("color_number_id")].'"  class="text_boxes" value="'.$size_library[$color[csf("size_number_id")]].'"><input type="hidden" name="accordion_" id="accordion_'.$color[csf("color_number_id")].'" class="text_boxes" value="'.$color_library[$color[csf("color_number_id")]].'"><input type="hidden" name="color_id" id="color_id'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes" value="'.$color[csf("id")].'"></td><td><input type="text" name="colorSize" id="colSize_'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes_numeric" style="width:100px" placeholder="'.($color[csf("plan_cut_qnty")]-$color[csf("production_qnty")]).'" onblur="fn_total('.$color[csf("color_number_id")].','.($i+1).')"></td><td><input type="button" name="button" id="button_'.$color[csf("color_number_id")].($i+1).'" value="Click For Bundle" class="formbutton" style="size:30px;" onclick="openmypage_bandle('.$did.','.$colorhead.','.$sizehead.','.$color_id.');" /></td></tr>';				
			}
			
			$i++; 
		}
		//echo $colorHTML;die; 
		if( $variableSettings==2 ){ $colorHTML = '<table id="table_color" class="rpt_table"><thead><th width="100">Color</th><th width="80">Quantity</th></thead><tbody>'.$colorHTML.'<tbody><tfoot><tr><th>Total</th><th><input type="text" id="total_color" placeholder="'.$totalQnty.'" class="text_boxes_numeric" style="width:80px" ></th></tr></tfoot></table>'; }
		echo "$('#breakdown_td_id').html('".addslashes($colorHTML)."');\n";
		$colorList = substr($colorID,0,-1);
		echo "$('#hidden_colorSizeID').val('".$colorList."');\n";
		//#############################################################################################//
		exit();
}

if($action=="show_dtls_listview")
{
	$dataArr = explode("**",$data);
	$po_id = $dataArr[0];
	$item_id = $dataArr[1];
	$country_id = $dataArr[2];
?>	
	<div style="width:100%;">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="150" align="center">Item Name</th>
                <th width="110" align="center">Country</th>
                <th width="115" align="center">Production Date</th>
                <th width="120" align="center">Production Qnty</th>                    
                <th width="110" align="center">Reporting Hour</th>
                <th width="120" align="center">Serving Company</th>
                <th width="" align="center">Location</th>
            </thead>
		</table>
	</div>
	<div style="width:100%;max-height:180px; overflow-y:scroll" id="sewing_production_list_view" align="left">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
		<?php  
			$i=1;
			$total_production_qnty=0;
			//echo "select id,po_break_down_id,item_number_id,production_date,production_quantity,production_source,production_hour,serving_company,location from pro_garments_production_mst where po_break_down_id='$po_id' and item_number_id='$item_id' and production_type='1' and status_active=1 and is_deleted=0 order by id"; die;
			$sqlResult =sql_select("select a.id,a.po_break_down_id,a.item_number_id,a.production_date,a.production_quantity,a.production_source,a.production_hour,a.serving_company,a.location,a.country_id,b.id as color_id from pro_garments_production_mst a,wo_po_color_size_breakdown b where a.po_break_down_id='$po_id' and a.po_break_down_id=b.po_break_down_id and a.item_number_id='$item_id' and a.country_id='$country_id' and a.production_type='1' and a.status_active=1 and a.is_deleted=0 group by a.id");
			foreach($sqlResult as $selectResult){
				
				if ($i%2==0)  
                	$bgcolor="#E9F3FF";
                else
               	 	$bgcolor="#FFFFFF";
 				$total_production_qnty+=$selectResult[csf('production_quantity')];
		?>
        
			<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="get_php_form_data('<?php echo $selectResult[csf('id')]."_".$selectResult[csf('color_id')]; ?>','populate_cutting_form_data','requires/cutting_update_controller');" > 
				<td width="40" align="center"><?php echo $i; ?></td>
                <td width="150" align="center"><p><?php echo $garments_item[$selectResult[csf('item_number_id')]]; ?></p></td>
                <td width="110" align="center"><p><?php echo $country_library[$selectResult[csf('country_id')]]; ?></p></td>
                <td width="115" align="center"><?php echo change_date_format($selectResult[csf('production_date')]); ?></td>
                <td width="120" align="right"><?php  echo $selectResult[csf('production_quantity')]; ?></td>
                
 				<?php  
					if($selectResult[csf('production_hour')]>12)
						$hour = ($selectResult[csf('production_hour')]-12)." PM";
					else if($selectResult[csf('production_hour')]==12)
						$hour = "00:00 AM";
					else
						$hour = $selectResult[csf('production_hour')]." AM";	
				?> 
                <td width="110" align="center"><?php echo $hour; ?></td>
				<?php
                       $source= $selectResult[csf('production_source')];
					   if($source==3)
						{
							$serving_company= return_field_value("supplier_name","lib_supplier","id='".$selectResult[csf('serving_company')]."'");
						}
						else
						{
							$serving_company= return_field_value("company_name","lib_company","id='".$selectResult[csf('serving_company')]."'");
						}
                ?>	
                <td width="120" align="center"><p><?php echo $serving_company; ?></p></td>
 				<?php 
 					$location_name= return_field_value("location_name","lib_location","id='".$selectResult[csf('location')]."'");
				?>
                <td width="" align="center"><?php echo $location_name; ?></td>
			</tr>
			<?php
			$i++;
			}
			?>
            <!--<tfoot>
            	<tr>
                	<th colspan="3"></th>
                    <th><!? echo $total_production_qnty; ?></th>
                    <th colspan="3"></th>
                </tr>
            </tfoot>-->
		</table>
        </div>
	<?php
}



if($action=="populate_cutting_form_data")
{
	//extract($_REQUEST);
	
	$data=explode('_',$data);
	//echo $data[0];
	//echo $data[1];
	//die;
	$sqlResult =sql_select("select id,po_break_down_id,item_number_id,challan_no,production_source,production_date,production_quantity,production_hour,entry_break_down_type,serving_company,location,floor_id,reject_qnty,remarks,total_produced,yet_to_produced,country_id from pro_garments_production_mst where id='$data[0]' and production_type='1' and status_active=1 and is_deleted=0 order by id");
	//$sqlResult=sql_select( "select id,bundle_mst_id,bundle_no,pcs_per_bundle,pcs_range_start,pcs_range_end,color_size_id from pro_bundle_dtls where color_size_id='$data' ");
  	foreach($sqlResult as $result)
	{ 
		echo "$('#txt_cutting_date').val('".change_date_format($result[csf('production_date')])."');\n";
		if($result[csf('production_hour')]>12)
		{
			$hour = $result[csf('production_hour')]-12;  $time=2;
 		}
		else if($result[csf('production_hour')]==12)
		{
			$hour = "00";  $time=1;
		}
		else
		{
			$hour = $result[csf('production_hour')]; $time=1;
		}
		 
		echo "$('#cbo_source').val('".$result[csf('production_source')]."');\n";
		echo "load_drop_down( 'requires/cutting_update_controller', ".$result[csf('production_source')].", 'load_drop_down_cutt_company', 'cutt_company_td' );\n";
		echo "$('#cbo_cutting_company').val('".$result[csf('serving_company')]."');\n";
		echo "$('#cbo_location').val('".$result[csf('location')]."');\n";
		echo "load_drop_down( 'requires/cutting_update_controller', ".$result[csf('location')].", 'load_drop_down_floor', 'floor_td' );\n";
		echo "$('#cbo_floor').val('".$result[csf('floor_id')]."');\n";
		
		echo "$('#txt_reporting_hour').val('".$hour."');\n";
		echo "$('#cbo_time').val('".$time."');\n";
		echo "$('#txt_challan_no').val('".$result[csf('challan_no')]."');\n";
		echo "$('#txt_cutting_qty').attr('placeholder','".$result[csf('production_quantity')]."');\n";
 		echo "$('#txt_cutting_qty').val('".$result[csf('production_quantity')]."');\n";
		echo "$('#txt_reject_qty').val('".$result[csf('reject_qnty')]."');\n";
  		echo "$('#txt_remark').val('".$result[csf('remarks')]."');\n";
		
		$plan_cut_qnty=return_field_value("sum(plan_cut_qnty)","wo_po_color_size_breakdown","po_break_down_id=".$result[csf('po_break_down_id')]." and item_number_id=".$result[csf('item_number_id')]." and country_id=".$result[csf('country_id')]." and status_active=1 and is_deleted=0");
		
		$total_produced = return_field_value("sum(production_quantity)","pro_garments_production_mst","po_break_down_id=".$result[csf('po_break_down_id')]." and item_number_id=".$result[csf('item_number_id')]." and country_id=".$result[csf('country_id')]." and production_type=1 and is_deleted=0");
		
		echo "$('#txt_cumul_cutting').val('".$total_produced."');\n";
		$yet_to_produced = $plan_cut_qnty - $total_produced;
		echo "$('#txt_yet_cut').val('".$yet_to_produced."');\n";
		
		/*echo "$('#txt_cumul_cutting').val('".$result[csf('total_produced')]."');\n";
		echo "$('#txt_yet_cut').val('".$result[csf('yet_to_produced')]."');\n"; */
	
		echo "$('#txt_mst_id').val('".$result[csf('id')]."');\n";
 		echo "set_button_status(1, permission, 'fnc_cutting_update_entry',1);\n";
	
		//break down of color and size------------------------------------------
 		//#############################################################################################//
		// order wise - color level, color and size level
		$color_library=return_library_array( "select id, color_name from lib_color",'id','color_name');
		$size_library=return_library_array( "select id, size_name from lib_size",'id','size_name');

		$variableSettings = $result[csf('entry_break_down_type')];
		if( $variableSettings!=1 ) // gross level
		{ 
			$po_id = $result[csf('po_break_down_id')];
			$item_id = $result[csf('item_number_id')];
			$country_id = $result[csf('country_id')];
			
			$sql_dtls = sql_select("select color_size_break_down_id,production_qnty,size_number_id, color_number_id from pro_garments_production_dtls a,wo_po_color_size_breakdown b where a.mst_id=$data[0] and a.status_active=1 and a.color_size_break_down_id=b.id and b.po_break_down_id='$po_id' and b.item_number_id='$item_id' and b.country_id='$country_id'");	
			foreach($sql_dtls as $row)
			{				  
				if( $variableSettings==2 ) $index = $row[csf('color_number_id')]; else $index = $row[csf('size_number_id')].$row[csf('color_number_id')];
			  	$amountArr[$index] = $row[csf('production_qnty')];
			}  
			
			if( $variableSettings==2 ) // color level
			{
				$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, sum(plan_cut_qnty) as plan_cut_qnty, (select sum(CASE WHEN pro_garments_production_dtls.color_size_break_down_id=wo_po_color_size_breakdown.id then production_qnty ELSE 0 END) from pro_garments_production_dtls where is_deleted=0 and  	production_type=1 ) as production_qnty 
					from wo_po_color_size_breakdown
					where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 group by color_number_id";
			}
			else if( $variableSettings==3 ) //color and size level
			{
				$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty, (select sum(CASE WHEN pro_garments_production_dtls.color_size_break_down_id=wo_po_color_size_breakdown.id then production_qnty ELSE 0 END) from pro_garments_production_dtls where is_deleted=0 and  	production_type=1 ) as production_qnty 
					from wo_po_color_size_breakdown
					where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0";// order by color_number_id,size_number_id
			}
			else // by default color and size level
			{
				$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty, (select sum(CASE WHEN pro_garments_production_dtls.color_size_break_down_id=wo_po_color_size_breakdown.id then production_qnty ELSE 0 END) from pro_garments_production_dtls where is_deleted=0 and  	production_type=1 ) as production_qnty 
					from wo_po_color_size_breakdown
					where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0";// order by color_number_id,size_number_id
			}
 
 			$colorResult = sql_select($sql);
 			//print_r($sql_dtls);die;
			$colorHTML="";
			$colorID='';
			$chkColor = array(); 
			$i=0;$totalQnty=0;$colorWiseTotal=0;
			foreach($colorResult as $color)
			{
				if( $variableSettings==2 ) // color level
				{  
					$amount = $amountArr[$color[csf("color_number_id")]];
					$colorHTML .='<tr><td>'.$color_library[$color[csf("color_number_id")]].'</td><td><input type="text" name="txt_color" id="colSize_'.($i+1).'" style="width:80px"  class="text_boxes_numeric" placeholder="'.($color[csf("plan_cut_qnty")]-$color[csf("production_qnty")]+$amount).'" value="'.$amount.'" onblur="fn_colorlevel_total('.($i+1).')"></td></tr>';				
					$totalQnty += $amount;
					$colorID .= $color[csf("color_number_id")].",";
				}
				else //color and size level
				{
					$index = $color[csf("size_number_id")].$color[csf("color_number_id")];
					$amount = $amountArr[$index];
					if( !in_array( $color[csf("color_number_id")], $chkColor ) )
					{
						if( $i!=0 ) $colorHTML .= "</table></div>";
						$i=0;
						$colorHTML .= '<h3 align="left" id="accordion_h'.$color[csf("color_number_id")].'" style="width:300px" class="accordion_h" onClick="accordion_menu( this.id,\'content_search_panel_'.$color[csf("color_number_id")].'\', \'\',1)"> <span id="accordion_h'.$color[csf("color_number_id")].'span">+</span>'.$color_library[$color[csf("color_number_id")]].': <span id="total_'.$color[csf("color_number_id")].'"></span></h3>';
						$colorHTML .= '<div id="content_search_panel_'.$color[csf("color_number_id")].'" style="display:none" class="accord_close"><table id="table_'.$color[csf("color_number_id")].'">';
						$chkColor[] = $color[csf("color_number_id")];
						$totalFn .= "fn_total(".$color[csf("color_number_id")].");";
					}
					$did='colSize_'.$color[csf('color_number_id')].($i+1);
					$did="document.getElementById('".$did."').value";
					$colorhead='accordion_'.$color[csf("color_number_id")];
					$colorhead="document.getElementById('".$colorhead."').value";
					$sizehead='size_name'.$color[csf("color_number_id")];
					$sizehead="document.getElementById('".$sizehead."').value";
					$color_id='color_id'.$color[csf('color_number_id')].($i+1);
					$color_id="document.getElementById('".$color_id."').value";

					//$colorhead="document.getElementById('".$colorhead."').'".$color_library[$color[csf("color_number_id")]]."'";
					
 					$colorID .= $color[csf("size_number_id")]."*".$color[csf("color_number_id")].",";
					$colorHTML .='<tr><td>'.$size_library[$color[csf("size_number_id")]].'<input type="hidden" name="size_name" id="size_name'.$color[csf("color_number_id")].'"  class="text_boxes" value="'.$color[csf("size_number_id")].'"><input type="hidden" name="accordion_" id="accordion_'.$color[csf("color_number_id")].'" class="text_boxes" value="'.$color[csf("color_number_id")].'"> <input type="hidden" name="color_id" id="color_id'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes" value="'.$color[csf("id")].'"></td><td><input type="text" name="colorSize" id="colSize_'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes_numeric" style="width:100px" placeholder="'.($color[csf("plan_cut_qnty")]-$color[csf("production_qnty")]+$amount).'" onblur="fn_total('.$color[csf("color_number_id")].','.($i+1).')" value="'.$amount.'" ></td><td><input type="button" name="button" value="Click For Bundle" class="formbutton" style="size:30px;" onclick="openmypage_bandle('.$color[csf("id")].','.$colorhead.','.$sizehead.','.$color[csf("color_number_id")].($i+1).');" /></td></tr>';				
					//$colorWiseTotal += $amount;
				}
				$i++; 
			}
			//echo $colorHTML;die; 
			if( $variableSettings==2 ){ $colorHTML = '<table id="table_color" class="rpt_table"><thead><th width="100">Color</th><th width="80">Quantity</th></thead><tbody>'.$colorHTML.'<tbody><tfoot><tr><th>Total</th><th><input type="text" id="total_color" placeholder="'.$totalQnty.'" value="'.$result[csf('production_quantity')].'" class="text_boxes_numeric" style="width:80px" ></th></tr></tfoot></table>'; }
			echo "$('#breakdown_td_id').html('".addslashes($colorHTML)."');\n";
			if( $variableSettings==3 )echo "$totalFn;\n";
			$colorList = substr($colorID,0,-1);
			echo "$('#hidden_colorSizeID').val('".$colorList."');\n";
		}//end if condtion
		//#############################################################################################//
	}
	//echo "select id,pcs_per_bundle,no_of_bundle,batch_no,color_shade from pro_bundle_mst where color_size_id=$data[1]";
	$qry_result=sql_select( "select a.id,a.pcs_per_bundle,a.no_of_bundle,a.batch_no,a.color_shade,b.id,bundle_mst_id,b.bundle_no,b.pcs_per_bundle,b.pcs_range_start,b.pcs_range_end,b.color_size_id,b.bundle_bar_code from pro_bundle_mst a,pro_bundle_dtls b where a.pro_gmts_pro_id=$data[0] and a.id=b.bundle_mst_id");
	 $k=1;
	 $count=count($qry_result);
	foreach ($qry_result as $row)
	{
		if($k==1)
		{
			echo "document.getElementById('hid_pcs_bundle').value 	 					= '".$row[csf("pcs_per_bundle")]."';\n"; 
			echo "document.getElementById('hid_tot_bundle').value 	 					= '".$row[csf("no_of_bundle")]."';\n"; 
			echo "document.getElementById('hid_batch_no').value 	 					= '".$row[csf("batch_no")]."';\n"; 
			echo "document.getElementById('hid_color_shade').value 	 					= '".$row[csf("color_shade")]."';\n"; 
			echo "document.getElementById('hid_updateid_mst').value 	 				= '".$row[csf("id")]."';\n"; 
		}
		
		if($id=="") $id=$row[csf("id")]; else $id.="*".$row[csf("id")];
		if($bundle_mst_id=="") $bundle_mst_id=$row[csf("bundle_mst_id")]; else $bundle_mst_id.="*".$row[csf("bundle_mst_id")];
		if($bundle_no=="") $bundle_no=$row[csf("bundle_bar_code")]; else $bundle_no.="*".$row[csf("bundle_bar_code")];
		if($pcs_per_bundle=="") $pcs_per_bundle=$row[csf("pcs_per_bundle")]; else $pcs_per_bundle.="*".$row[csf("pcs_per_bundle")];
		if($pcs_range_start=="") $pcs_range_start=$row[csf("pcs_range_start")]; else $pcs_range_start.="*".$row[csf("pcs_range_start")];
		if($pcs_range_end=="") $pcs_range_end=$row[csf("pcs_range_end")]; else $pcs_range_end.="*".$row[csf("pcs_range_end")];
		if($color_size_id=="") $color_size_id=$row[csf("color_size_id")]; else $color_size_id.="*".$row[csf("color_size_id")];
		if($k==$count)
		{
			echo "document.getElementById('hid_tbl_id').value 	 				= '".$id."';\n";
			echo "document.getElementById('hid_bundle_no').value 	 					= '".$bundle_no."';\n";
			echo "document.getElementById('hid_pcs_per_bundle').value 	 					= '".$pcs_per_bundle."';\n";
			echo "document.getElementById('hid_pcs_no_range').value 	 					= '".$pcs_range_start."';\n";
			echo "document.getElementById('hid_color_id').value 	 					= '".$color_size_id."';\n";
		}
 		//echo "document.getElementById('hid_updateid_mst').value 	 					= '".$id."';\n";
		$k++;
		
	}
}

//pro_garments_production_mst
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
 
	$sql=sql_select("select a.id,a.operation_count,b.id as gsd_dtls_id,b.operation_id from ppl_gsd_entry_dtls b,ppl_gsd_entry_mst a where a.id=b.mst_id and a.po_job_no=$hid_job_num");
	$operation_array=array();
	foreach($sql as $data)
	{
		$operation_array[]=$data[csf("operation_id")];
		$gsd_mst_array[$data[csf("operation_id")]]=$data[csf("id")];
		$gsd_dtls_array[$data[csf("operation_id")]]=$data[csf("gsd_dtls_id")];
		
	}
	
	if ($operation==0) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
 		
		
 		$id=return_next_id("id", "pro_garments_production_mst", 1);
		//production_type array	 
		if(str_replace("'","",$cbo_time)==1)$reportTime = $txt_reporting_hour;else $reportTime = 12+str_replace("'","",$txt_reporting_hour);
		$field_array="id, garments_nature, company_id, challan_no, po_break_down_id,item_number_id, country_id, production_source, serving_company, location, production_date, production_quantity, production_type, entry_break_down_type, production_hour, remarks, floor_id, reject_qnty,total_produced, yet_to_produced, inserted_by, insert_date";
		$data_array="(".$id.",".$garments_nature.",".$cbo_company_name.",".$txt_challan_no.",".$hidden_po_break_down_id.", ".$cbo_item_name.", ".$cbo_country_name.", ".$cbo_source.",".$cbo_cutting_company.",".$cbo_location.",".$txt_cutting_date.",".$txt_cutting_qty.",1,".$sewing_production_variable.",".$reportTime.",".$txt_remark.",".$cbo_floor.",".$txt_reject_qty.",".$txt_cumul_cutting.",".$txt_yet_cut.",".$user_id.",'".$pc_date_time."')";
		
		//echo "INSERT INTO pro_garments_production_mst (".$field_array.") VALUES ".$data_array;die;
		
 		$rID=sql_insert("pro_garments_production_mst",$field_array,$data_array,1);
		
		// pro_garments_production_dtls table entry here ----------------------------------------------//
		$field_array="id, mst_id,production_type,color_size_break_down_id,production_qnty";
  		
		if(str_replace("'","",$sewing_production_variable)==2)//color level wise
		{		
			$color_sizeID_arr=sql_select( "select id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name and color_mst_id!=0 order by id" );
			$colSizeID_arr=array(); 
			foreach($color_sizeID_arr as $val){
				$index = $val[csf("color_number_id")];
				$colSizeID_arr[$index]=$val[csf("id")];
			}	
			
			// $colorIDvalue concate as colorID*Value**colorID*Value -------------------------//
 			$rowEx = explode("**",$colorIDvalue); 
 			$dtls_id=return_next_id("id", "pro_garments_production_dtls", 1);
			$data_array="";$j=0;
			foreach($rowEx as $rowE=>$val)
			{
				$colorSizeNumberIDArr = explode("*",$val);
				//1 means cutting update
				if($j==0)$data_array = "(".$dtls_id.",".$id.",1,'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."')";
				else $data_array .= ",(".$dtls_id.",".$id.",1,'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."')";
				$dtls_id=$dtls_id+1;							
 				$j++;								
			}
 		}
		
		if(str_replace("'","",$sewing_production_variable)==3)//color and size wise
		{		
			$color_sizeID_arr=sql_select( "select id,size_number_id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name order by size_number_id,color_number_id" );
			$colSizeID_arr=array(); 
			foreach($color_sizeID_arr as $val){
				$index = $val[csf("size_number_id")].$val[csf("color_number_id")];
				$colSizeID_arr[$index]=$val['id'];
			}	
			
			//	colorIDvalue concate as sizeID*colorID*value***sizeID*colorID*value	--------------------------// 
 			$rowEx = explode("***",$colorIDvalue); 
			$dtls_id=return_next_id("id", "pro_garments_production_dtls", 1);
			$data_array="";$j=0;
			foreach($rowEx as $rowE=>$valE)
			{
				$colorAndSizeAndValue_arr = explode("*",$valE);
				$sizeID = $colorAndSizeAndValue_arr[0];
				$colorID = $colorAndSizeAndValue_arr[1];				
				$colorSizeValue = $colorAndSizeAndValue_arr[2];
				$index = $sizeID.$colorID;
 				//1 means cutting update
				if($j==0)$data_array = "(".$dtls_id.",".$id.",1,'".$colSizeID_arr[$index]."','".$colorSizeValue."')";
				else $data_array .= ",(".$dtls_id.",".$id.",1,'".$colSizeID_arr[$index]."','".$colorSizeValue."')";
				$dtls_id=$dtls_id+1;
 				$j++;
			}
		}
		
		if(str_replace("'","",$sewing_production_variable)==2 || str_replace("'","",$sewing_production_variable)==3)
		{  
 			$dtlsrID=sql_insert("pro_garments_production_dtls",$field_array,$data_array,1);
			if($dtlsrID) $flag=1; else $flag=0;
		} 
		
/*		if($hid_color_id!="")
		{
*/			$id_bundle=return_next_id("id", "pro_bundle_mst", 1);
			
			$field_array2="id, pro_gmts_pro_id,po_break_down_id, color_size_id, pcs_per_bundle,no_of_bundle, batch_no, color_shade, status_active, is_deleted, inserted_by, insert_date";
			$data_array2="(".$id_bundle.",".$id.",".$hidden_po_break_down_id.",".$hid_color_id.",".$hid_pcs_bundle.", ".$hid_tot_bundle.", ".$hid_batch_no.",".$hid_color_shade.",1,0,".$user_id.",'".$pc_date_time."')";
			
			//echo "INSERT INTO pro_bundle_mst (".$field_array2.") VALUES ".$data_array2;die;
			$rIDb=sql_insert("pro_bundle_mst",$field_array2,$data_array2,1);
		
			$id3=return_next_id( "id", "pro_bundle_dtls", 1 ) ;
			
			$field_array3="id,bundle_mst_id,bundle_no,pcs_per_bundle,pcs_range_start,pcs_range_end,color_size_id,bundle_bar_code,bundle_bar_code_prefix";
			
			$id4=return_next_id( "id", " operation_code", 1 ) ;
			$field_array4="id,op_code,bundle_dtls,bundle_mst,prod_mst,style,gsd_mst,gsd_dtls,op_bar_code_prefix";
	
			$cbo_company_name=str_pad(str_replace("'","",$cbo_company_name),2,"0",STR_PAD_LEFT); 
			//$cbo_buyer_name=str_pad(str_replace("'","",$cbo_buyer_name),3,"0",STR_PAD_LEFT);  
			$hid_job_num=str_replace("'","",explode("-",$hid_job_num));
			
			$bundle_number=$cbo_company_name."".$hid_job_num[1];
			
			$bundle_auto_num=return_field_value( "max(bundle_bar_code) as bundle_bar_code", "pro_bundle_dtls", " bundle_bar_code_prefix='$bundle_number'", "bundle_bar_code" );
			
			if($bundle_auto_num=="") $tmp=1; else $tmp=substr($bundle_auto_num,5,7)+1;
			
			$hid_pcs_per_bundle=explode("*",str_replace("'",'',$hid_pcs_per_bundle));
			$hid_bundle_no=explode("*",str_replace("'",'',$hid_bundle_no));
			$hid_pcs_no_range=explode("*",str_replace("'",'',$hid_pcs_no_range));
			//$hid_color_id=explode("*",str_replace("'",'',$hid_color_id));
			//print_r ($hid_color_id);die;
			 
			for ($i=0;$i<count($hid_pcs_per_bundle);$i++)
			{
			 	$bundle_number_org=$bundle_number."".str_pad($tmp,7,"0",STR_PAD_LEFT);
				if ($i!=0) $data_array3 .=",";
				 $data_array3.="(".$id3.",".$id_bundle.",'".$hid_pcs_per_bundle[$i]."','".$hid_pcs_per_bundle[$i]."','".$hid_pcs_no_range[$i]."',1,".$hid_color_id.",'".$bundle_number_org."','".$bundle_number."')";
				$id3=$id3+1;
				$tmp++;
				$operation_number_org="";
				foreach($operation_array as $opkey)
				{
					$operation_number_org=$bundle_number_org."".str_pad($opkey,3,"0",STR_PAD_LEFT);
				
					if ($data_array4!="") $data_array4 .=",";
				 	$data_array4.="(".$id4.",'".$operation_number_org."','".$id3."','".$id_bundle."',".$hidden_po_break_down_id.",1,'".$gsd_mst_array[$opkey]."','".$gsd_dtls_array[$opkey]."','".$bundle_number."')";
					$id4=$id4+1;
				}
			}
			 //echo "INSERT INTO pro_bundle_dtls (".$field_array3.") VALUES ".$data_array3; 
			// echo "INSERT INTO operation_code (".$field_array4.") VALUES ".$data_array4;die;
			$rID3=sql_insert("pro_bundle_dtls",$field_array3,$data_array3,1);
			
			if($data_array4!="") $rID4=sql_insert("operation_code",$field_array4,$data_array4,1);
		//}
		check_table_status( $_SESSION['menu_id'],0);
			  
		if(str_replace("'","",$sewing_production_variable)!=1)
		{
			if($rID && $dtlsrID && $rIDb && $rID3)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'","",$hidden_po_break_down_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$hidden_po_break_down_id);
			}
		}else{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'","",$hidden_po_break_down_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$hidden_po_break_down_id);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$rID;
		}
		disconnect($con);
		die;
	}
  	else if ($operation==1) // Update Here------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		//echo "aa";die;
		//table lock here 
		if ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**1"; die;}
		
		// pro_garments_production_mst table data entry here 
 		if(str_replace("'","",$cbo_time)==1)$reportTime = $txt_reporting_hour;else $reportTime = 12+str_replace("'","",$txt_reporting_hour);
		$field_array="company_id*challan_no*production_source*serving_company*location*production_date*production_quantity*production_type*entry_break_down_type*production_hour*remarks*floor_id*reject_qnty*total_produced*yet_to_produced*updated_by*update_date";
		$data_array="".$cbo_company_name."*".$txt_challan_no."*".$cbo_source."*".$cbo_cutting_company."*".$cbo_location."*'".change_date_format(str_replace("'","",$txt_cutting_date),'yyyy-mm-dd')."'*".$txt_cutting_qty."*1*".$sewing_production_variable."*".$reportTime."*".$txt_remark."*".$cbo_floor."*".$txt_reject_qty."*".$txt_cumul_cutting."*".$txt_yet_cut."*".$user_id."*'".$pc_date_time."'";
 		$rID=sql_update("pro_garments_production_mst",$field_array,$data_array,"id","".$txt_mst_id."",1);
		
		// pro_garments_production_dtls table data entry here 
		if(str_replace("'","",$sewing_production_variable)!=1 && str_replace("'","",$txt_mst_id)!='')// check is not gross level
		{
			//delete details table data
			$dtlsrDelete = execute_query("delete from pro_garments_production_dtls where mst_id=$txt_mst_id",1);			 						
			$field_array="id, mst_id, production_type, color_size_break_down_id, production_qnty";
			
			if(str_replace("'","",$sewing_production_variable)==2)//color level wise
			{		
				$color_sizeID_arr=sql_select( "select id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name and color_mst_id!=0 order by id" );
				$colSizeID_arr=array(); 
				foreach($color_sizeID_arr as $val){
					$index = $val[csf("color_number_id")];
					$colSizeID_arr[$index]=$val[csf("id")];
				}	
					
				// $colorIDvalue concate as colorID*Value**colorID*Value -------------------------//
 				$rowEx = explode("**",$colorIDvalue); 
				$dtls_id=return_next_id("id", "pro_garments_production_dtls", 1);
				$data_array="";$j=0;
				foreach($rowEx as $rowE=>$val)
				{
					$colorSizeNumberIDArr = explode("*",$val);
					//1 means cutting update
					if($j==0)$data_array = "(".$dtls_id.",".$txt_mst_id.",1,'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."')";
					else $data_array .= ",(".$dtls_id.",".$txt_mst_id.",1,'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."')";
					$dtls_id=$dtls_id+1;							
					$j++;								
				}
			}
			
			if(str_replace("'","",$sewing_production_variable)==3)//color and size wise
			{		
				$color_sizeID_arr=sql_select( "select id,size_number_id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name order by size_number_id,color_number_id" );
				$colSizeID_arr=array(); 
				foreach($color_sizeID_arr as $val){
					$index = $val[csf("size_number_id")].$val[csf("color_number_id")];
					$colSizeID_arr[$index]=$val['id'];
				}	
					
				//	colorIDvalue concate as sizeID*colorID*value***sizeID*colorID*value	--------------------------// 
				$rowEx = explode("***",$colorIDvalue);
				$dtls_id=return_next_id("id", "pro_garments_production_dtls", 1);
				$data_array="";$j=0;
				foreach($rowEx as $rowE=>$valE)
				{
					$colorAndSizeAndValue_arr = explode("*",$valE);
					$sizeID = $colorAndSizeAndValue_arr[0];
					$colorID = $colorAndSizeAndValue_arr[1];				
					$colorSizeValue = $colorAndSizeAndValue_arr[2];
					$index = $sizeID.$colorID;
					//1 means cutting update
					if($j==0)$data_array = "(".$dtls_id.",".$txt_mst_id.",1,'".$colSizeID_arr[$index]."','".$colorSizeValue."')";
					else $data_array .= ",(".$dtls_id.",".$txt_mst_id.",1,'".$colSizeID_arr[$index]."','".$colorSizeValue."')";
					$dtls_id=$dtls_id+1;
					$j++;
				}
			}
			// echo $data_array;die;
			//details table data insert here 
 			$dtlsrID=sql_insert("pro_garments_production_dtls",$field_array,$data_array,1);
		
		}//end cond
/*		if ($hid_updateid_mst!="")
		{
*/			$field_array2="color_size_id*pcs_per_bundle*no_of_bundle*batch_no*color_shade*status_active*is_deleted*updated_by*update_date";
			$data_array2="".$hid_color_id."*".$hid_pcs_bundle."*".$hid_tot_bundle."*".$hid_batch_no."*".$hid_color_shade."*1*0*".$user_id."*'".$pc_date_time."'";
			$rID5=sql_update("pro_bundle_mst",$field_array2,$data_array2,"id","".$hid_updateid_mst."",1);
/*		}
		else
		{*/
/*			$id_bundle=return_next_id("id", "pro_bundle_mst", 1);
			
			$field_array2="id, pro_gmts_pro_id,po_break_down_id, color_size_id, pcs_per_bundle,no_of_bundle, batch_no, color_shade, status_active, is_deleted, inserted_by, insert_date";
			$data_array2="(".$id_bundle.",".$id.",".$hidden_po_break_down_id.",".$hid_color_id.",".$hid_pcs_bundle.", ".$hid_tot_bundle.", ".$hid_batch_no.",".$hid_color_shade.",1,0,".$user_id.",'".$pc_date_time."')";
			
			//echo "INSERT INTO pro_bundle_mst (".$field_array2.") VALUES ".$data_array2;die;
			$rID5=sql_insert("pro_bundle_mst",$field_array2,$data_array2,1);
*/		//}

		//if ($hid_tbl_id!="")
		//{
			//$rID6 = sql_delete("pro_bundle_dtls","id","",'id ',$hid_updateid_mst,1);
			$rID6=execute_query( "delete from pro_bundle_dtls where bundle_mst_id in ($hid_updateid_mst)",0);
			
			$id3=return_next_id( "id", "pro_bundle_dtls", 1 ) ;
			
			$field_array3="id,bundle_mst_id,bundle_no,pcs_per_bundle,pcs_range_start,pcs_range_end,color_size_id,bundle_bar_code,bundle_bar_code_prefix";
	
			$cbo_company_name=str_pad(str_replace("'","",$cbo_company_name),3,"0",STR_PAD_LEFT); 
			$cbo_buyer_name=str_pad(str_replace("'","",$cbo_buyer_name),3,"0",STR_PAD_LEFT);  
			$hid_job_num=str_replace("'","",explode("-",$hid_job_num));
			
			$bundle_number=$hid_job_num[1]."".$hid_job_num[2]."".$cbo_company_name."".$cbo_buyer_name;
			
			$bundle_auto_num=return_field_value( "max(bundle_bar_code) as bundle_bar_code", "pro_bundle_dtls", " bundle_bar_code_prefix='$bundle_number'", "bundle_bar_code" );
			
			if($bundle_auto_num=="") $tmp=1; else $tmp=substr($bundle_auto_num,14,5)+1;
			
			$hid_pcs_per_bundle=explode("*",str_replace("'",'',$hid_pcs_per_bundle));
			$hid_bundle_no=explode("*",str_replace("'",'',$hid_bundle_no));
			$hid_pcs_no_range=explode("*",str_replace("'",'',$hid_pcs_no_range));
			//$hid_color_id=explode("*",str_replace("'",'',$hid_color_id));
			//print_r ($hid_color_id);die;
			for ($i=0;$i<count($hid_pcs_per_bundle);$i++)
			{
				 $bundle_number_org=$bundle_number."".str_pad($tmp,5,"0",STR_PAD_LEFT);
				if ($i!=0) $data_array3 .=",";
				 $data_array3.="(".$id3.",".$hid_updateid_mst.",'".$hid_pcs_per_bundle[$i]."','".$hid_pcs_per_bundle[$i]."','".$hid_pcs_no_range[$i]."',1,".$hid_color_id.",'".$bundle_number_org."','".$bundle_number."')";
				$id3=$id3+1;
				$tmp++;
			}

				//echo "INSERT INTO pro_bundle_dtls (".$field_array3.") VALUES ".$data_array3; die;
			$rID7=sql_insert("pro_bundle_dtls",$field_array3,$data_array3,1);			
		//}
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if(str_replace("'","",$sewing_production_variable)!=1)
			{
				if($rID && $dtlsrID && $rID5 && $rID6 && $rID7)
				{
					mysql_query("COMMIT");  
					echo "1**".str_replace("'","",$hidden_po_break_down_id);
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}else{
				if($rID)
				{
					mysql_query("COMMIT");  
					echo "1**".str_replace("'","",$hidden_po_break_down_id);
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$rID;
		}
		disconnect($con);
		die;
	}
 
	else if ($operation==2)  // Delete Here---------------------------------------------------------- 
	{
 		
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
				
		$rID = sql_delete("pro_garments_production_mst","status_active*is_deleted","0*1",'id ',$txt_mst_id,1);
 		$dtlsrID = sql_delete("pro_garments_production_dtls","status_active*is_deleted","0*1",'mst_id',$txt_mst_id,1);
					
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$hidden_po_break_down_id); 
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$hidden_po_break_down_id); 
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$rID;
		}
		disconnect($con);
		die;
	}
}

if($action=="bundle_preparation")
{
extract($_REQUEST);
echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
$data=explode('_',$data);
$data_main=explode('_',$data_main);
$data_break=explode('_',$data_break);
//print_r ($data_break);

		$color_library=return_library_array( "select id, color_name from lib_color",'id','color_name');
		$size_library=return_library_array( "select id, size_name from lib_size",'id','size_name');

?>
<script>
	function calculate_bundle( pcs_per_bund )
	{
		document.getElementById('txt_no_of_bendle').value="";
		var cuttiing_qnty=document.getElementById('hidden_val').value;
		var total_bundle=Math.ceil(cuttiing_qnty/pcs_per_bund);
		document.getElementById('txt_no_of_bendle').value=total_bundle;
		
		
		
		var grand_color=document.getElementById('hidden_color').value;
		var grand_size=document.getElementById('hidden_size').value;
		 
		var data_break=document.getElementById('hid_bundle_no').value+"_"+document.getElementById('hid_pcs_per_bundle').value+"_"+document.getElementById('hid_tbl_id').value+"_"+document.getElementById('hid_pcs_no_range').value;
		var data_main=document.getElementById('hidden_po_break_down_id').value;

		var data=cuttiing_qnty+"_"+total_bundle+"_"+pcs_per_bund+"_"+grand_color+"_"+grand_size+"_"+data_break+"_"+data_main;
		 
		var list_view_bundle = return_global_ajax_value( data, 'load_php_dtls_form', '', 'cutting_update_controller');
 
 		if(list_view_bundle!='')
		{
			$("#pre_tbl tbody").html('');
			$("#pre_tbl tbody tr").remove();
			$("#pre_tbl tbody").append(list_view_bundle);
		}
		document.getElementById('txt_tot_pcs').value=cuttiing_qnty;
	}
		
	function fnc_close()
	{
		var per_bundle="";
		var tot_bundle="";
		var batch_no="";
		var color_shade="";
		var color_id="";
		var updateid_mst="";
		
		var tot_row=$('#pre_tbl tbody tr').length;
		var sl_no="";
		var bundle_no="";
		var pcs_per_bundle="";
		var pcs_no_range="";
		var gmt_color="";
		var gmt_size="";
		var hidden_id="";
		//alert (tot_row);
		 for(var i=1; i<=tot_row; i++)
			{
				if(i>1)
				{
				 sl_no +="*";
				 bundle_no +="*";
				 pcs_per_bundle +="*";
				 pcs_no_range +="*";
				 gmt_color +="*";
				 gmt_size +="*";
				 hidden_id +="*";
				}
				sl_no += $("#txt_sl_no_"+i).val();
				bundle_no += $("#txt_bundle_no_"+i).val();
				pcs_per_bundle += $("#txt_pcs_per_bundle_"+i).val();
				pcs_no_range += $("#txt_pcs_no_range_"+i).val();
				gmt_color += $("#txt_gmt_color_"+i).val();
				gmt_size += $("#txt_gmt_size_"+i).val();
				hidden_id += $("#hiddenid_"+i).val();
			}
			per_bundle = $("#txt_pcs_per_bendle").val();
			tot_bundle = $("#txt_no_of_bendle").val();
			batch_no = $("#txt_batch_no").val();
			color_shade = $("#txt_color_shade").val();
			color_id = $("#hid_color_id").val();
			updateid_mst = $("#updateid_mst").val();
			//hidden_tot_row = $("#tot_row").val();
		
		//alert (hidden_tot_row);return;
		document.getElementById('hid_sl_no').value=sl_no;
		document.getElementById('hid_bundle_no').value=bundle_no;
		document.getElementById('hid_pcs_per_bundle').value=pcs_per_bundle;
		document.getElementById('hid_pcs_no_range').value=pcs_no_range;
		document.getElementById('hid_gmt_color').value=gmt_color;
		document.getElementById('hid_gmt_size').value=gmt_size;
		document.getElementById('hid_tbl_id').value=hidden_id;
		document.getElementById('hid_tot_row').value=tot_row;
		//alert (tot_row);
			
		parent.emailwindow.hide();
	}
</script>
  </head>
  <body>
  <div align="center" style="width:100%;" >
  <form name="bundlepre_1"  id="bundlepre_1" autocomplete="off">
  <br>
  <br>
   <fieldset style="width:610px;">
      <table  cellspacing="2" cellpadding="0" border="0" class="" align="center" width="600">
            <tr>
                <td width="120">Pcs Per Bundle</td>
                <td width="120">
                    <input type="hidden" id="hidden_val" style="size:80px;" value="<?php echo $field_val; ?>" />
                    <input type="hidden" id="hidden_color" style="size:80px;" value="<?php echo $color; ?>" />
                    <input type="hidden" id="hidden_size" style="size:80px;" value="<?php echo $size; ?>" />
                    <input type="hidden" id="updateid_mst" style="size:80px;" value="<?php echo $data_main[4]; ?>" />
                    <input type="hidden" id="hid_tot_row" style="size:80px;" value="" />
                    <input type="text" id="hid_color_id" style="size:80px;" value="<?php echo $color_id; ?>" />
                    <input type="text" name="txt_pcs_per_bendle" id="txt_pcs_per_bendle" class="text_boxes_numeric" style="size:80px;" value="<?php echo $data_main[0]; ?>" onBlur="calculate_bundle(this.value);" />
                </td>
                <td width="120">No. of Bundle</td>
                <td width="120">
                    <input type="text" name="txt_no_of_bendle" id="txt_no_of_bendle" class="text_boxes_numeric" style="size:80px;" value="<?php echo $data_main[1]; ?>" onChange="" readonly />
                </td>
            </tr>
            <tr>
                <td width="120">Batch No.</td>
                <td width="120">
                    <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes_numeric" value="<?php echo $data_main[2]; ?>" style="size:80px;" />
                </td>
                <td width="120">Color Shade</td>
                <td>
                    <input type="text" name="txt_color_shade" id="txt_color_shade" class="text_boxes" value="<?php echo $data_main[3]; ?>" style="size:80px;" />
                </td>
            </tr>
          </table>
        </fieldset>
        <br>
        <br>
        <fieldset style="width:580px;">
     	<legend>Bundle Details</legend>
          <table  cellspacing="2" cellpadding="0" border="0" class="" align="center" width="550" id="pre_tbl">
            <thead class="form_table_header">
                <th width="50" align="center">SL. No.</th>
                <th width="80" align="center">Bundle No.</th>
                <th width="60" align="center">Pcs Per Bundle</th>
                <th width="120" align="center">Pcs Number Range</th>
                <th width="120" align="center">Garment Color</th>
                <th width="120" align="center">Garment Size</th>
            </thead>
            <tbody>
                <tr>
                    <td align="center">
                        <input type="text" name="txt_sl_no_1" id="txt_sl_no_1" class="text_boxes_numeric" style="width:50px" readonly />
                    </td>
                    <td align="center">
                        <input type="text" name="txt_bundle_no_1" id="txt_bundle_no_1" class="text_boxes_numeric" style="width:80px" readonly/>
                    </td>
                    <td align="center">
                        <input type="text" name="txt_pcs_per_bundle_1" id="txt_pcs_per_bundle_1" class="text_boxes_numeric" style="width:60px" readonly />
                    </td>
                    <td align="center">
                        <input type="text" name="txt_pcs_no_range_1" id="txt_pcs_no_range_1" class="text_boxes_numeric" style="width:120px" readonly />
                    </td>
                    <td align="center">
                        <input type="text" name="txt_gmt_color_1" id="txt_gmt_color_1" class="text_boxes" style="width:120px" readonly />
                    </td>
                    <td align="center">
                        <input type="text" name="txt_gmt_size_1" id="txt_gmt_size_1" class="text_boxes" style="width:120px" readonly />
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td width="60">
                        <input type="text" name="txt_tot_pcs" id="txt_tot_pcs" class="text_boxes_numeric" value="" style="width:60px" readonly />
                    </td>
                    <td colspan="3">&nbsp;</td>
                <tr>
                <tr>
                <td colspan="6" align="center">
                <input type="button" name="main_close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                </td>
                </tr>
            </tfoot>
         </table>
         </fieldset>
         <table  cellspacing="2" cellpadding="0" border="0" class="" align="center" width="550">
         </table>
         <input type="text" id="hid_sl_no" name="hid_sl_no" value="<?php echo $data_break[0]; ?>" />
         <input type="text" id="hid_bundle_no" name="hid_bundle_no" value="<?php echo $data_break[1]; ?>" />
         <input type="text" id="hid_pcs_per_bundle" name="hid_pcs_per_bundle" value="<?php echo $data_break[2]; ?>" />
         <input type="text" id="hid_pcs_no_range" name="hid_pcs_no_range" value="<?php echo $data_break[3]; ?>" />
         <input type="text" id="hid_gmt_color" name="hid_gmt_color" value="<?php echo $data_break[4]; ?>" />
          <input type="hidden" id="hid_gmt_size" name="hid_gmt_size" value="<?php  echo $data_break[7]; ?>" />
         <input type="hidden" id="hidden_po_break_down_id" name="hidden_po_break_down_id" value="<?php echo $data_main[5]; ?>" />
         <input type="hidden" id="hid_tbl_id" name="hid_tbl_id" value="<?php echo $data_break[5]; ?>" />
  </form>
  </div>
  </body>
  <script>
 calculate_bundle(document.getElementById('txt_pcs_per_bendle').value);
  </script>
  <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
  </html>
<?php		
}

if ($action=="load_php_dtls_form")
{
	extract($_REQUEST); 
	
	$data = explode("_",$data);
	$cuttiing_qnty=$data[0];
	$total_bundle=$data[1];
	$pcs_per_bund=$data[2];
	$data_break_dtls=$data[5];
	$data_main_mst=$data[6];
	//+"_"+grand_color+"_"+grand_size+"_"+data_break+"_"+data_main;
	
	$fraction_bundle=$cuttiing_qnty%$pcs_per_bund;
	
	if( $data_main_mst=="")
	{
		$range=1;
		for ($k=1; $k<=$total_bundle; $k++)
		{
			if($k==$total_bundle) $pcs_per_bund=$fraction_bundle;
			?>
			<tr>
				<td align="center">
					<input type="text" name="txt_sl_no_<?php echo $k; ?>" id="txt_sl_no_<?php echo $k; ?>" value="<?php echo $k; ?>" class="text_boxes_numeric" style="width:50px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_bundle_no_<?php echo $k; ?>" id="txt_bundle_no_<?php echo $k; ?>" class="text_boxes_numeric" style="width:80px" readonly/>
				</td>
				<td align="center">
					<input type="text" name="txt_pcs_per_bundle_<?php echo $k; ?>" id="txt_pcs_per_bundle_<?php echo $k; ?>" class="text_boxes_numeric" value="<?php echo $pcs_per_bund; ?>" style="width:60px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_pcs_no_range_<?php echo $k; ?>" id="txt_pcs_no_range_<?php echo $k; ?>" value="<?php echo $range; ?>" class="text_boxes_numeric" style="width:120px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_gmt_color_<?php echo $k; ?>" id="txt_gmt_color_<?php echo $k; ?>" class="text_boxes" value="<?php echo $data[3];?>" style="width:120px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_gmt_size_<?php echo $k; ?>" id="txt_gmt_size_<?php echo $k; ?>" class="text_boxes" value="<?php echo $data[4];?>" style="width:120px" readonly />
	<!--					<input type="hidden" id="hiddenid_<?php// echo $k; ?>" name="hiddenid_<?php//echo $k; ?>" value="" style="width:15px;" class="text_boxes"/>
	-->				</td>
			</tr>
			<?php
			$range++;
		}
	}
	else // From Update
	{
		print_r ($data[7]);
		$data_break_dtls==$data_break[1];
		$data_main_mst=$data[6];
		
		$bundle_no = explode("*",$data[5]);
		$pcs_per_bund = explode("*",$data[6]);
		$tbl_id= explode("*",$data[7]);
		$pcs_no_range = explode("*",$data[8]);
		//$data_main_mst = explode("*",$data_main_mst);
		$color_library=return_library_array( "select id, color_name from lib_color",'id','color_name');
		$size_library=return_library_array( "select id, size_name from lib_size",'id','size_name');

		
	$fraction_bundle=$cuttiing_qnty%$pcs_per_bund;
	
		if( $data_main_mst!="")
		{
			$range=1;
			for ($k=1; $k<=$total_bundle; $k++)
			{
				if($k==$total_bundle) $pcs_per_bund=$fraction_bundle;
				?>
				<tr>
					<td align="center">
						<input type="text" name="txt_sl_no_<?php echo $k; ?>" id="txt_sl_no_<?php echo $k; ?>" value="<?php echo $k; ?>" class="text_boxes_numeric" style="width:50px" readonly />
					</td>
					<td align="center">
						<input type="text" name="txt_bundle_no_<?php echo $k; ?>" id="txt_bundle_no_<?php echo $k; ?>" value="<?php echo $bundle_no[$range]; ?>" class="text_boxes_numeric" style="width:80px" readonly/>
					</td>
					<td align="center">
						<input type="text" name="txt_pcs_per_bundle_<?php echo $k; ?>" id="txt_pcs_per_bundle_<?php echo $k; ?>" value="<?php echo $pcs_per_bund[$range]; ?>" class="text_boxes_numeric"  style="width:60px" readonly />
					</td>
					<td align="center">
						<input type="text" name="txt_pcs_no_range_<?php echo $k; ?>" id="txt_pcs_no_range_<?php echo $k; ?>" value="<?php echo $pcs_no_range[$range]; ?>" class="text_boxes_numeric" style="width:120px" readonly />
					</td>
					<td align="center">
						<input type="text" name="txt_gmt_color_<?php echo $k; ?>" id="txt_gmt_color_<?php echo $k; ?>" class="text_boxes" value="<?php echo $color_library[$data[3]];?>" style="width:120px" readonly />
					</td>
					<td align="center">
						<input type="text" name="txt_gmt_size_<?php echo $k; ?>" id="txt_gmt_size_<?php echo $k; ?>" class="text_boxes" value="<?php echo $size_library[$data[4]];?>" style="width:120px" readonly />
							<input type="hidden" id="hiddenid_<?php echo $k; ?>" name="hiddenid_<?php echo $k; ?>" value="<?php echo $tbl_id[$range];?>" style="width:15px;" class="text_boxes"/>
				</td>
				</tr>
				<?php
				$range++;
			}		
		}
	}


/*print_r( $fraction_bundle);die;

$hid_sl_no=explode('*',$data_break[0]);
//$hid_bundle_no=explode('*',$data_break[1]);
$hid_pcs_per_bundle=explode("*",$data[6]);
$hid_tbl_id=explode("*",$data[7]);
//print_r ($data);
//echo $data;
if ($hid_tbl_id[0]=="")
{
	if ($data[0]!="")
	{
	$k=1;
	//$totrows=count($hid_pcs_per_bundle);
	for($i=1; $i<=$data[0]; $i++)
		{
			?>
			 <tr>
				<td align="center">
					<input type="text" name="txt_sl_no_<?php echo $k; ?>" id="txt_sl_no_<?php echo $k; ?>" value="<?php echo $k; ?>" class="text_boxes_numeric" style="width:50px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_bundle_no_<?php echo $k; ?>" id="txt_bundle_no_<?php echo $k; ?>" class="text_boxes_numeric" style="width:80px" readonly/>
				</td>
				<td align="center">
					<input type="text" name="txt_pcs_per_bundle_<?php echo $k; ?>" id="txt_pcs_per_bundle_<?php echo $k; ?>" class="text_boxes_numeric" value="<?php echo $data[1]; ?>" style="width:60px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_pcs_no_range_<?php echo $k; ?>" id="txt_pcs_no_range_<?php echo $k; ?>" class="text_boxes_numeric" style="width:120px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_gmt_color_<?php echo $k; ?>" id="txt_gmt_color_<?php echo $k; ?>" class="text_boxes" value="<?php echo $data[3];?>" style="width:120px" readonly />
				</td>
				<td align="center">
					<input type="text" name="txt_gmt_size_<?php echo $k; ?>" id="txt_gmt_size_<?php echo $k; ?>" class="text_boxes" value="<?php echo $data[4];?>" style="width:120px" readonly />
<!--					<input type="hidden" id="hiddenid_<?php// echo $k; ?>" name="hiddenid_<?php//echo $k; ?>" value="" style="width:15px;" class="text_boxes"/>
-->				</td>
			</tr>
			<?php
			$k++;
		}
	}
}
else if($hid_tbl_id!="" && $hid_color_id==$color_id && $data[10]!="")
{
$k=0;
for($i=1; $i<=count($hid_tbl_id); $i++)
	{
?>
 <tr>
    <td align="center">
        <input type="text" name="txt_sl_no_<?php echo $i; ?>" id="txt_sl_no_<?php echo $i; ?>" value="<?php echo $i; ?>" class="text_boxes_numeric" style="width:50px" readonly />
    </td>
    <td align="center">
        <input type="text" name="txt_bundle_no_<?php echo $i; ?>" id="txt_bundle_no_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" readonly/>
    </td>
    <td align="center">
        <input type="text" name="txt_pcs_per_bundle_<?php echo $i; ?>" id="txt_pcs_per_bundle_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $hid_pcs_per_bundle[$k]; ?>" style="width:60px" readonly />
    </td>
    <td align="center">
        <input type="text" name="txt_pcs_no_range_<?php echo $i; ?>" id="txt_pcs_no_range_<?php echo $i; ?>" class="text_boxes_numeric" style="width:120px" readonly />
    </td>
    <td align="center">
        <input type="text" name="txt_gmt_color_<?php echo $i; ?>" id="txt_gmt_color_<?php echo $i; ?>" class="text_boxes" value="<?php echo $data[3];?>" style="width:120px" readonly />
    </td>
    <td align="center">
        <input type="text" name="txt_gmt_size_<?php echo $i; ?>" id="txt_gmt_size_<?php echo $i; ?>" class="text_boxes" value="<?php echo $data[4];?>" style="width:120px" readonly />
        <input type="hidden" id="hiddenid_<?php echo $i; ?>" name="hiddenid_<?php echo $i; ?>" value="<?php echo $hid_tbl_id[$k]; ?>" style="width:15px;" class="text_boxes"/>
    </td>
</tr>
	<?php
	$k++;
	}
}*/
}

if($action=="print_report_barcode")
{
	define('FPDF_FONTPATH', '../../ext_resource/pdf/fpdf/font/');
	require('../../ext_resource/pdf/code39.php');
	
	$pdf=new PDF_Code39();
	$pdf->AddPage();
	
	 $color_sizeID_arr=sql_select( "select  bundle_bar_code from pro_bundle_dtls" ); 
	//echo "<table border='5'  cellpadding='15' cellspacing='0'>";
	$i=5;
	$j=5;
	foreach($color_sizeID_arr as $val){
		//if($k==0) echo "<tr>";
		//$i++;
		 $pdf->Code39($i, $j, $val[csf("bundle_bar_code")]);
		 
		 //$i=$i+10;
		 $j=$j+19;
		//if($k==3){  echo "</tr>"; $i=0; }
	}  
	foreach (glob("../"."*.pdf") as $filename) {			
			@unlink($filename);
		}
	//echo "</table>";
	//$pdf->Output();
	$name = 'id_card_' . date('j-M-Y_h-iA') . '.pdf';
	$pdf->Output( "../".$name, 'F');
}
?>