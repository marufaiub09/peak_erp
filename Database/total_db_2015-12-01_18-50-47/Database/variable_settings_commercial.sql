-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `variable_settings_commercial`
--

DROP TABLE IF EXISTS `variable_settings_commercial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable_settings_commercial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` int(11) NOT NULL DEFAULT '0',
  `variable_list` int(11) NOT NULL DEFAULT '0',
  `capacity_in_value_hcode` varchar(30) NOT NULL,
  `capacity_in_value` double NOT NULL DEFAULT '0',
  `currency_hcode` varchar(30) NOT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `max_btb_limit_hcode` varchar(30) NOT NULL DEFAULT '0',
  `max_btb_limit` decimal(10,0) NOT NULL DEFAULT '0',
  `max_pc_limit_hcode` varchar(30) NOT NULL DEFAULT '0',
  `max_pc_limit` double NOT NULL DEFAULT '0',
  `cost_heads` int(11) NOT NULL DEFAULT '0',
  `cost_heads_status` tinyint(4) NOT NULL DEFAULT '0',
  `monitor_head_id` int(11) NOT NULL DEFAULT '0',
  `monitoring_standard_day` int(11) NOT NULL DEFAULT '0',
  `internal_file_source` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `company_name` (`company_name`),
  KEY `variable_list` (`variable_list`),
  KEY `cost_heads` (`cost_heads`),
  KEY `cost_heads_status` (`cost_heads_status`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variable_settings_commercial`
--

LOCK TABLES `variable_settings_commercial` WRITE;
/*!40000 ALTER TABLE `variable_settings_commercial` DISABLE KEYS */;
/*!40000 ALTER TABLE `variable_settings_commercial` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
