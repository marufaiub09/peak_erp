-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_wvn_finish_fab_iss_dtls`
--

DROP TABLE IF EXISTS `inv_wvn_finish_fab_iss_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_wvn_finish_fab_iss_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `trans_id` int(11) NOT NULL DEFAULT '0',
  `batch_lot` varchar(100) NOT NULL,
  `prod_id` int(11) NOT NULL DEFAULT '0',
  `issue_qnty` double NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL DEFAULT '0',
  `no_of_roll` int(11) NOT NULL DEFAULT '0',
  `order_id` varchar(50) NOT NULL COMMENT 'comma separated order id',
  `roll_save_string` varchar(100) NOT NULL,
  `order_save_string` varchar(100) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_wvn_finish_fab_iss_dtls`
--

LOCK TABLES `inv_wvn_finish_fab_iss_dtls` WRITE;
/*!40000 ALTER TABLE `inv_wvn_finish_fab_iss_dtls` DISABLE KEYS */;
INSERT INTO `inv_wvn_finish_fab_iss_dtls` VALUES (1,1928,9525,'6042',1906,25,6,0,'280','','280_25.00',16,'2015-07-27 15:48:25',1,'2015-07-29 11:41:56',1,0),(2,1928,9656,'6103',1907,0.01,6,0,'283','','283_0.01',1,'2015-07-29 11:31:07',1,'2015-07-29 11:43:31',1,0),(3,1971,9686,'5895',1903,1119,6,0,'279','','279_1119.00',16,'2015-07-29 17:49:42',0,'0000-00-00 00:00:00',1,0),(4,1971,9687,'6042',1903,1222,6,0,'279','','279_1222.00',16,'2015-07-29 17:51:15',0,'0000-00-00 00:00:00',1,0),(5,1973,9689,'6042',1907,25,6,0,'283','','283_25.00',16,'2015-07-29 18:02:10',0,'0000-00-00 00:00:00',1,0),(6,2208,10782,'6042',1906,3054,6,0,'280','','280_3054.00',16,'2015-08-09 12:28:53',0,'0000-00-00 00:00:00',1,0),(7,2271,11036,'6103',1907,1143,6,0,'283','','283_1143.00',16,'2015-08-11 16:22:56',0,'0000-00-00 00:00:00',1,0),(8,2295,11257,'6042',1907,1419,6,0,'283','','283_1419.00',16,'2015-08-12 19:32:27',0,'0000-00-00 00:00:00',1,0),(9,2305,11364,'W01',2106,138,6,0,'306','','306_138.00',16,'2015-08-14 12:00:29',0,'0000-00-00 00:00:00',1,0),(10,2336,11502,'6042',1907,48.99,6,0,'283','','283_48.99',16,'2015-08-17 10:45:47',0,'0000-00-00 00:00:00',1,0),(11,2490,12040,'006',2365,300,6,0,'317','','317_300.00',16,'2015-08-23 12:04:27',0,'0000-00-00 00:00:00',1,0),(12,2540,12310,'002',2323,280,6,0,'321','','321_280.00',16,'2015-08-25 10:28:59',0,'0000-00-00 00:00:00',1,0),(13,2541,12312,'003',2324,282,6,0,'321','','321_282.00',16,'2015-08-25 10:30:59',0,'0000-00-00 00:00:00',1,0),(14,2542,12313,'001',2322,282,6,0,'321','','321_282.00',16,'2015-08-25 10:33:04',0,'0000-00-00 00:00:00',1,0),(15,2545,12336,'007',2456,330,6,0,'317','','317_330.00',16,'2015-08-25 14:48:56',0,'0000-00-00 00:00:00',1,0),(16,3369,16968,'1306',3303,6834,6,0,'377','','377_6834.00',16,'2015-11-04 20:12:13',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `inv_wvn_finish_fab_iss_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
