<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will Cutting Delivery To Input Entry
				
Functionality	:	
JS Functions	:
Created by		:	Jahid
Creation date 	: 	25-11-2014
Purpose			:
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Cutting Delivery Info","../", 1, 1, $unicode,'','');

?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

function openmypage(page_link,title)
{
	var company = $("#cbo_company_name").val();
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1040px,height=370px,center=1,resize=0,scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var po_id=this.contentDoc.getElementById("hidden_mst_id").value;//po id
 		var item_id=this.contentDoc.getElementById("hidden_grmtItem_id").value; 
		var po_qnty=this.contentDoc.getElementById("hidden_po_qnty").value;
		var country_id=this.contentDoc.getElementById("hidden_country_id").value; 
		
 		if (po_id!="")
		{
 			freeze_window(5);
 			release_freezing();
 			$("#txt_order_qty").val(po_qnty);
			$("#cbo_item_name").val(item_id);
			$("#cbo_country_name").val(country_id);
 			childFormReset();//child from reset
			get_php_form_data(po_id+'**'+item_id+'**'+country_id, "populate_data_from_search_popup", "requires/cutting_delevar_to_input_controller" );
			
			var variableSettings=$('#sewing_production_variable').val();
			var styleOrOrderWisw=$('#styleOrOrderWisw').val();
			if(variableSettings!=1) 
			{ 
			get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/cutting_delevar_to_input_controller" ); 
			}
			else
			{
				$("#txt_ex_quantity").removeAttr("readonly");
				$("#txt_total_carton_qnty").removeAttr("readonly");
				
			}
			show_list_view(po_id,'show_country_listview','list_view_country','requires/cutting_delevar_to_input_controller','');
  			set_button_status(0, permission, 'fnc_cutDelivery',1,0);
		}
	}
}

function fnc_cutDelivery(operation)
{
	if(operation==4)
	{
		if ( form_validation('txt_system_id','System Number')==false )
		{
			alert("Please save the delivery first"); return;
		}		
		else
		{
			 var report_title=$( "div.form_caption" ).html();
			 print_report( $('#cbo_company_name').val()+'*'+$('#txt_system_id').val()+'*'+$('#txt_ex_factory_date').val()+'*'+report_title, "cut_delivery_print", "requires/cutting_delevar_to_input_controller" ) 
			 return;
		}
	}
	else if(operation==0 || operation==1 || operation==2)
	{
		if ( form_validation('cbo_company_name*txt_order_no*txt_ex_quantity*txt_ex_factory_date','Company Name*Order No*ex-factory Quantity*Date')==false )
		{
			return;
		}		
		else
		{
			
			var sewing_production_variable = $("#sewing_production_variable").val();
			var colorList = ($('#hidden_colorSizeID').val()).split(",");
			
			var i=0;var colorIDvalue='';var k=1;var color_id="";var m=1;
			var tem_arr=new Array();
			if(sewing_production_variable==2)//color level
			{
 				$("input[name=txt_color]").each(function(index, element) {
 					if( $(this).val()!='' )
					{
						if(i==0)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val()+"*"+$('txtbundle_'+colorList[i]+m).val();
						}
						else
						{
							colorIDvalue += "**"+colorList[i]+"*"+$(this).val()+"*"+$('txtbundle_'+colorList[i]+m).val();
						}
						k++;
					}
					i++;
					m++;
					
				});
			}
			else if(sewing_production_variable==3)//color and size level
			{
 				$("input[name=colorSize]").each(function(index, element) {
					
					color_id=colorList[i].split("*");
					
					if( jQuery.inArray( color_id[1], tem_arr ) == -1 ) 
					{
						tem_arr.push( color_id[1] );
						m=1;
					}
						
					if( $(this).val()!='' )
					{
						if(k==1)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val()+"*"+$('#txtbundle_'+color_id[1]+m).val();
						}
						else
						{
							colorIDvalue += "***"+colorList[i]+"*"+$(this).val()+"*"+$('#txtbundle_'+color_id[1]+m).val();
						}
						k++;
					}
 					i++;
					m++;
				});
			}
			//alert(colorIDvalue);return;
			
			var data="action=save_update_delete&operation="+operation+"&colorIDvalue="+colorIDvalue+get_submitted_data_string('garments_nature*cbo_company_name*cbo_country_name*sewing_production_variable*cbo_location_name*cbo_item_name*hidden_po_break_down_id*hidden_colorSizeID*txt_ex_factory_date*txt_ex_quantity*txt_total_carton_qnty*txt_challan_no*txt_remark*txt_finish_quantity*txt_cumul_quantity*txt_yet_quantity*txt_mst_id*txt_system_no*txt_system_id*cbo_buyer_name*cbo_delivery_basis*cbo_knitting_source*cbo_knitting_company',"../");
			//alert(data);return;
 			freeze_window(operation);
 			http.open("POST","requires/cutting_delevar_to_input_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_cutDelivery_Reply_info;
		}
	}
}
  
function fnc_cutDelivery_Reply_info()
{
 	if(http.readyState == 4)
	{
		/*release_freezing();
		alert(http.responseText);return;*/
		var variableSettings=$('#sewing_production_variable').val();
		var styleOrOrderWisw=$('#styleOrOrderWisw').val();
		var item_id=$('#cbo_item_name').val();
		var country_id = $("#cbo_country_name").val();
		
		var reponse=http.responseText.split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_cutDelivery('+ reponse[1]+')',8000); 
		}
		else if(reponse[0]==0)
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			
			$("#txt_system_id").val(trim(reponse[2]));
			$("#txt_system_no").val(trim(reponse[3]));
			$("#txt_challan_no").val(trim(reponse[4]));
			
			show_list_view(reponse[2],'show_dtls_listview_mst','ex_factory_list_view','requires/cutting_delevar_to_input_controller','');
			setFilterGrid("details_table",-1);
			reset_form('','breakdown_td_id','txt_order_no*txt_ex_quantity*hidden_break_down_html*hidden_colorSizeID*txt_total_carton_qnty*txt_remark*txt_finish_quantity*txt_cumul_quantity*txt_yet_quantity*txt_job_no*txt_style_no*txt_shipment_date*txt_order_qty*cbo_item_name*cbo_country_name*cbo_buyer_name','','');
			
			
			set_button_status(0, permission, 'fnc_cutDelivery',1,1);
			release_freezing();
		} 
		else if(reponse[0]==1)
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			
			show_list_view(reponse[2],'show_dtls_listview_mst','ex_factory_list_view','requires/cutting_delevar_to_input_controller','');
			setFilterGrid("details_table",-1);
			reset_form('','breakdown_td_id','txt_ex_quantity*hidden_break_down_html*hidden_colorSizeID*txt_total_carton_qnty*txt_remark*txt_finish_quantity*txt_cumul_quantity*txt_yet_quantity*txt_job_no*txt_style_no*txt_shipment_date*txt_order_qty*cbo_item_name*cbo_country_name*cbo_buyer_name','','');
			set_button_status(0, permission, 'fnc_cutDelivery',1,1);
			release_freezing();
		}
		else if(reponse[0]==2)
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			show_list_view(reponse[2],'show_dtls_listview_mst','ex_factory_list_view','requires/cutting_delevar_to_input_controller','');
			setFilterGrid("details_table",-1);
			//show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id,'show_dtls_listview','ex_factory_list_view','requires/cutting_delevar_to_input_controller','');		
			reset_form('','breakdown_td_id','txt_ex_factory_date*txt_ex_quantity*hidden_break_down_html*hidden_colorSizeID*txt_total_carton_qnty*txt_challan_no*txt_remark','','');
			set_button_status(0, permission, 'fnc_cutDelivery',1,1);
			release_freezing();
		}
 	}
} 

function childFormReset()
{
	reset_form('','','txt_ex_quantity*hidden_break_down_html*hidden_colorSizeID*txt_total_carton_qnty*txt_remark*txt_finish_quantity*txt_cumul_quantity*txt_yet_quantity','','');
	$('#txt_ex_quantity').attr('placeholder','');//placeholder value initilize
	$('#txt_finish_quantity').attr('placeholder','');//placeholder value initilize
	$('#txt_cumul_quantity').attr('placeholder','');//placeholder value initilize
	$('#txt_yet_quantity').attr('placeholder','');//placeholder value initilize ex_factory_list_view
 	//$("#ex_factory_list_view").html('');
	$("#breakdown_td_id").html('');
}

  
function fn_total(tableName,index,ref) // for color and size level
{
	//alert(tableName);
    var filed_value = $("#colSize_"+tableName+index).val();
	var bundle_qnty = $("#txtbundle_"+tableName+index).val();
	var placeholder_value = $("#colSize_"+tableName+index).attr('placeholder');
	if(ref==1)
	{
		if(filed_value*1 > placeholder_value*1)
		{
			page_link='requires/cutting_delevar_to_input_controller.php?action=confirm_popup'+'&placeholder_value='+placeholder_value+'&filed_value='+filed_value;;
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe',page_link,'Confirm Msg', 'width=200px,height=80px,center=1,resize=0,scrolling=0','');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var decision_ref=this.contentDoc.getElementById("hidden_ref").value;
				if(decision_ref==2)
				{
					$("#colSize_"+tableName+index).val('');
					$("#txtbundle_"+tableName+index).val('');
					var totalVal=0;
					$("input[name=colorSize]").each(function(index, element) {
						totalVal += ( $(this).val() )*1;
					});
					$("#txt_ex_quantity").val(totalVal);
					return;
					
				}
				else
				{
					//void(2);
					var totalRow = $("#table_"+tableName+" tr").length;
					//alert(tableName);
					math_operation( "total_"+tableName, "colSize_"+tableName, "+", totalRow);
					if($("#total_"+tableName).val()*1!=0)
					{
						$("#total_"+tableName).html($("#total_"+tableName).val());
					}
					var totalVal=0;
					$("input[name=colorSize]").each(function(index, element) {
						totalVal += ( $(this).val() )*1;
					});
					$("#txt_ex_quantity").val(totalVal);
				}
			}
		}
		else
		{
			/*var totalRow = $("#table_"+tableName+" tr").length;
			//alert(tableName);
			math_operation( "total_"+tableName, "colSize_"+tableName, "+", totalRow);
			if($("#total_"+tableName).val()*1!=0)
			{
				$("#total_"+tableName).html($("#total_"+tableName).val());
			}
			var totalVal=totalbundle= 0;
			$("input[name=colorSize]").each(function(index, element) {
				totalVal += ( $(this).val() )*1;
			});
			$("#txt_ex_quantity").val(totalVal);*/
			
			
		}
	}
	else
	{
		var totalbundle=0;
		
		$("input[name=txt_bundle]").each(function(index, element) {
			totalbundle += ( $(this).val() )*1;
		});
		$("#txt_total_carton_qnty").val(totalbundle);	
	}
	
	
	
	var totalRow = $("#table_"+tableName+" tr").length;
	//alert(tableName);
	math_operation( "total_"+tableName, "colSize_"+tableName, "+", totalRow);
	if($("#total_"+tableName).val()*1!=0)
	{
		$("#total_"+tableName).html($("#total_"+tableName).val());
	}
	
	var totalVal = 0;
	$("input[name=colorSize]").each(function(index, element) {
        totalVal += ( $(this).val() )*1;
    });
	$("#txt_ex_quantity").val(totalVal);
	
	var totalbundle=0;
	$("input[name=txt_bundle]").each(function(index, element) {
		totalbundle += ( $(this).val() )*1;
	});
	$("#txt_total_carton_qnty").val(totalbundle);
	
}

function fn_colorlevel_total(index) //for color level
{
	var filed_value = $("#colSize_"+index).val();
	var placeholder_value = $("#colSize_"+index).attr('placeholder');
	if(filed_value*1 > placeholder_value*1)
	{
		if( confirm("Qnty Excceded by"+(placeholder_value-filed_value)) )	
			void(0);
		else
		{
			$("#colSize_"+index).val('');
 		}
	}
	
    var totalRow = $("#table_color tbody tr").length;
	//alert(totalRow);
	math_operation( "total_color", "colSize_", "+", totalRow);
	$("#txt_ex_quantity").val( $("#total_color").val() );
} 

function put_country_data(po_id, item_id, country_id, po_qnty, plan_qnty)
{
	freeze_window(5);
	
	$("#cbo_item_name").val(item_id);
	$("#txt_order_qty").val(po_qnty);
	$("#cbo_country_name").val(country_id);

	childFormReset();//child from reset
	get_php_form_data(po_id+'**'+item_id+'**'+country_id, "populate_data_from_search_popup", "requires/cutting_delevar_to_input_controller" );
	
	var variableSettings=$('#sewing_production_variable').val();
	var styleOrOrderWisw=$('#styleOrOrderWisw').val();
	if(variableSettings!=1)
	{ 
		get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/cutting_delevar_to_input_controller" ); 
	}
	else
	{
		$("#txt_ex_quantity").removeAttr("readonly");
		$("#txt_total_carton_qnty").removeAttr("readonly");
		
	}
	set_button_status(0, permission, 'fnc_cutDelivery',1,0);
	release_freezing();
}

function delivery_sys_popup()
{
	var page_link='requires/cutting_delevar_to_input_controller.php?action=sys_surch_popup&company='+document.getElementById('cbo_company_name').value;
	var title="Delivery System Popup";
	var company = $("#cbo_company_name").val();
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=850px,height=370px,center=1,resize=0,scrolling=0','')
	emailwindow.onclose=function()
	{
		var delivery_id=this.contentDoc.getElementById("hidden_delivery_id").value;
		//alert(delivery_id);return;
		if(delivery_id !="")
		{
			get_php_form_data(delivery_id, "populate_muster_from_date", "requires/cutting_delevar_to_input_controller" );
			show_list_view(delivery_id,'show_dtls_listview_mst','ex_factory_list_view','requires/cutting_delevar_to_input_controller','');
			setFilterGrid("details_table",-1);
			set_button_status(0, permission, 'fnc_cutDelivery',1,1);
		}
		
	}

}
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;">
	<?php  echo load_freeze_divs ("../",$permission);  ?>
    <div style="width:930px; float:left" align="center">
        <form name="exFactory_1" id="exFactory_1" autocomplete="off" >     
        <fieldset style="width:930px;">
            <legend>Production Module</legend>
                <fieldset>                                       
                <table width="100%" border="0">
                	<tr>
                        <td align="right" colspan="3">Sys Challan No</td>
                        <td colspan="3"> 
                          <input name="txt_system_no" id="txt_system_no" class="text_boxes" type="text"  style="width:160px" onDblClick="delivery_sys_popup()" placeholder="Browse or Search" />
                          <input name="txt_system_id" id="txt_system_id" class="text_boxes" type="hidden"  style="width:160px"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="130" align="right" class="must_entry_caption">Company Name </td>
                        <td width="170">
                            <?php
                            echo create_drop_down( "cbo_company_name", 172, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", '', "load_drop_down( 'requires/cutting_delevar_to_input_controller', this.value, 'load_drop_down_location', 'location_td' ); get_php_form_data(this.value,'load_variable_settings','requires/cutting_delevar_to_input_controller');",0 ); ?>
                            <input type="hidden" name="sewing_production_variable" id="sewing_production_variable" value="" />
                            <input type="hidden" id="styleOrOrderWisw" />  
                        </td>
                        <td width="130" align="right">Location</td>
                        <td width="170" id="location_td">
                           <?php echo create_drop_down( "cbo_location_name", 172, $blank_array,"", 1, "-- Select Location --", $selected, "" );?>
                        </td>   
                        <td width="130" align="right" class="must_entry_caption">Delivery Date </td>
                        <td > 
                        <input name="txt_ex_factory_date" id="txt_ex_factory_date" class="datepicker" type="text"  style="width:160px;" >
                        </td>
                    </tr>
                    <tr>
                        <td align="right" >Delivery Basis</td>
                        <td>
                        <?php
							$delivery_basis_arr=array(1=>"Order No",2=>"Cut No"); 
                        	echo create_drop_down( "cbo_delivery_basis", 172, $delivery_basis_arr,"", 1, "-- Select--", 1,"","1" );
                        ?>
                        </td>
                        <td align="right"> Challan No</td>
                        <td > 
                          <input name="txt_challan_no" id="txt_challan_no" class="text_boxes" type="text"  style="width:160px" maxlength="50" readonly disabled />
                        </td>
                        <td align="right" >Cutting Source</td>
                        <td >
                            <?php
                                echo create_drop_down( "cbo_knitting_source", 170, $knitting_source,"", 1, "-- Select --", $selected, "load_drop_down( 'requires/cutting_delevar_to_input_controller', this.value+'**'+$('#cbo_company_name').val(), 'load_drop_down_knit_com', 'knitting_company_td' );","","1,3" );
                            ?>
                        </td>
                        
                    </tr>
                    <tr>
                    	<td  align="right">Cutting Company</td>
                        <td  id="knitting_company_td">
                            <?php
                                echo create_drop_down( "cbo_knitting_company", 170, $blank_array,"", 1, "-- Select --", $selected, "","","" );
                            ?>	
                        </td>
                    </tr>
                </table>
                </fieldset>
                <br /> 
                <table cellpadding="0" cellspacing="1" width="100%" id="child_table">
                    <tr>
                        <td width="30%" valign="top">
                          <fieldset>
                          <legend>New Entry</legend>
                            <table  cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td width="130" align="right" class="must_entry_caption">Order/Cut No</td>
                                    <td><input name="txt_order_no" id="txt_order_no"  placeholder="Double Click to Search" onDblClick="openmypage('requires/cutting_delevar_to_input_controller.php?action=order_popup&company='+document.getElementById('cbo_company_name').value+'&garments_nature='+document.getElementById('garments_nature').value,'Order Search')" class="text_boxes" style="width:150px" readonly />
                                    <input type="hidden" id="hidden_po_break_down_id" value="" /></td>
                                </tr>
                                <tr>
                                    <td align="right" class="must_entry_caption"> Delivery Qnty</td>
                                    <td>
                                        <input name="txt_ex_quantity" id="txt_ex_quantity" class="text_boxes_numeric" type="text"  style="width:150px;" readonly />
                                        <input type="hidden" id="hidden_break_down_html"  value="" readonly disabled />
                                        <input type="hidden" id="hidden_colorSizeID"  value="" readonly disabled />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Total Bundle Qnty</td>
                                    <td>
                                       <input name="txt_total_carton_qnty" id="txt_total_carton_qnty" type="text" class="text_boxes_numeric"  style="width:150px" readonly/>
                                    </td>
                                </tr>
                                <tr>
                                     <td width="102" align="right">Remarks</td> 
                                     <td width="165"> 
                                         <input name="txt_remark" id="txt_remark" type="text"  class="text_boxes" style="width:150px;" maxlength="450"  />
                                     </td>
                                </tr>
                           </table>
                        </fieldset>
                    </td>
                    <td width="1%" valign="top"></td>
                    <td width="28%" valign="top">
                          <fieldset>
                          <legend>Display</legend>
                              <table cellpadding="0" cellspacing="2" width="100%" >
                                 <tr>
                                      <td width="160" align="right"> Total Cut Qnty</td>
                                      <td>
                                          <input name="txt_finish_quantity" id="txt_finish_quantity"  class="text_boxes_numeric" type="text" style="width:100px" disabled readonly  />
                                      </td>
                                  </tr> 
                                  <tr>
                                      <td align="right">Total Delivery Qnty</td>
                                      <td>
                                          <input type="text" name="txt_cumul_quantity" id="txt_cumul_quantity" class="text_boxes_numeric"  style="width:100px" disabled readonly  />
                                      </td>
                                  </tr>
                                   <tr>
                                      <td align="right">Yet to Delivery Qnty</td>
                                      <td>
                                          <input type="text" name="txt_yet_quantity" id="txt_yet_quantity" class="text_boxes_numeric"  style="width:100px" disabled readonly />
                                      </td>
                                  </tr>
                                  <tr>
                                    <td align="right"> Job No.</td>
                                    <td>	
                                    <input style="width:100px;" type="text"   class="text_boxes" name="txt_job_no" id="txt_job_no" disabled  />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right"> Style </td>
                                    <td>	
                                    <input class="text_boxes" name="txt_style_no" id="txt_style_no" type="text" style="width:100px;" disabled />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right">Shipment Date</td>
                                    <td>
                                    <input class="text_boxes" name="txt_shipment_date" id="txt_shipment_date"   style="width:100px" disabled />
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right">Order Qty.</td>
                                    <td>
                                    <input class="text_boxes"  name="txt_order_qty" id="txt_order_qty" type="text" style="width:100px;" disabled/>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right">Item</td>   
                                    <td >
                                    <?php
                                    echo create_drop_down( "cbo_item_name", 112, $garments_item,"", 1, "-- Select Item --", $selected, "",1,0 );	
                                    ?>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right">Country</td>
                                    <td>
                                    <?php
                                    echo create_drop_down( "cbo_country_name", 112, "select id,country_name from lib_country","id,country_name", 1, "-- Select Country --", $selected, "",1 );
                                    ?> 
                                    </td>
                                  </tr>
                                  <tr>
                                    <td align="right">Buyer Name</td>
                                    <td  id="buyer_td"><?php echo create_drop_down( "cbo_buyer_name", 112, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",1 );?></td>
                                  </tr>
                               </table>
                          </fieldset>
                      </td>
                    <td width="41%" valign="top">
                        <div style="max-height:330px; overflow-y:scroll" id="breakdown_td_id" align="center"></div>
                    </td>    
                </tr>
                </table>
                <br />
                <table cellpadding="0" cellspacing="1" width="100%">
                    <tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <?php 
                                echo load_submit_buttons( $permission, "fnc_cutDelivery", 0,1,"reset_form('exFactory_1','ex_factory_list_view*list_view_country','','','childFormReset()')",1);
                            ?>
                             <input type="hidden" name="txt_mst_id" id="txt_mst_id" readonly >
                        </td>
                    </tr> 
                </table>
                <div style="width:930px; margin-top:5px;"  id="ex_factory_list_view" align="center">
</div>
           </fieldset>
        </form>
    </div>
	<div id="list_view_country" style="width:388px;float:left; padding-top:5px; margin-top:5px; position:relative; margin-left:10px"></div>   
</div>
</body> 
<script src="../includes/functions_bottom.js" type="text/javascript"></script>  
</html>