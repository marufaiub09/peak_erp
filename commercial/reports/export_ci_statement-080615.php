<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create CI Statement Report.
Functionality	:	
JS Functions	:
Created by		:	Jahid
Creation date 	: 	23-12-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Monthly Bank Submission/Export Status", "../../", 1, 1,'','','');
?>	

<script>

 	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	
	var tableFilters = 
	 {
		col_50: "none",
		col_operation: {
		id: ["value_total_grs_value","value_total_discount_value","value_total_bonous_value","value_total_claim_value","value_total_commission_value","value_total_net_invo_value","total_invoice_qty","total_carton_qty"],
	   col: [11,12,13,14,15,16,20,21],
	   operation: ["sum","sum","sum","sum","sum","sum","sum","sum"],
	   write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
	 } 
 
	var permission = '<?php echo $permission; ?>';
	
function generate_report()
	{
		if(form_validation('cbo_company_name','Company Name')==false)
		{
			return;
		}
		
		var report_title=$( "div.form_caption" ).html();	
		var data="action=report_generate"+get_submitted_data_string("cbo_company_name*cbo_buyer_name*cbo_lien_bank*cbo_location*cbo_based_on*txt_date_from*txt_date_to","../../")+'&report_title='+report_title;
		freeze_window(3);
		http.open("POST","requires/export_ci_statement_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fn_report_generated_reponse;
	}
	
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var response=trim(http.responseText);
			//alert(http.responseText);return;
			$('#report_container2').html(response);
			document.getElementById('report_container').innerHTML=report_convert_button('../../');
			append_report_checkbox('table_header_1',1);
			setFilterGrid("table_body",-1,tableFilters);
			show_msg('3');
			release_freezing();
		}
	}
	
	function openmypage(po_id,k)
		{
			page_link='requires/export_ci_statement_controller.php?action=po_id_details'+'&po_id='+po_id+'&k='+k;;
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe',page_link,'PO Info', 'width=480px,height=350px,center=1,resize=0,scrolling=0','../');
			emailwindow.onclose=function()
			{
				//alert("Jahid");
			}
		}
		
/*function total_value()
{
	var tamount=0;
	var total_row=$( "#table_body tbody tr" ).length-1;
	//alert(total_row);
	for(var i=1; i<=total_row;i++)
	 {
	tamount +=$("#net_invo_value_"+i).text()*1;
	};
	$("#total_net_invo").html(tamount);
}*/	
	
</script>
</head>
<body onLoad="set_hotkey();">
    <div style="width:100%;" align="center">
    <?php echo load_freeze_divs ("../../",''); ?>
    <form id="frm_lc_salse_contact" name="frm_lc_salse_contact">
    <div style="width:1070px;">
    <h3 align="left" id="accordion_h1" style="width:1070px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3> 
    <div id="content_search_panel"> 
    <fieldset style="width:1070px;">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center">
                <table class="rpt_table" cellspacing="0" cellpadding="0" width="1020">
                <thead>
                    <th width="140" class="must_entry_caption">Company</th>
                    <th width="140">Buyer</th>
                    <th width="130">Lien Bank</th>
                    <th width="130">Location</th>
                    <th width="120">Based On</th>
                    <th width="90" >Date From</th>
                    <th width="90" >Date To</th>
                    <th width="80"><input type="reset" name="res" id="res" value="Reset" style="width:70px" class="formbutton" onClick="reset_form('frm_lc_salse_contact','report_container*report_container2','','','')" /></th>
                </thead>
                <tbody>
                    <tr >
                        <td>
						<?php
                        	echo create_drop_down( "cbo_company_name", 155, "select comp.id, comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/export_ci_statement_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                        ?>
                        </td>
                        <td id="buyer_td"><?php 
                        	echo create_drop_down( "cbo_buyer_name", 155, $blank_array,"", 1, "-- All Buyer --", $selected, "",0,"" );
                        ?></td>
                        <td>
						<?php
                        	echo create_drop_down( "cbo_lien_bank", 145, "select bank_name,id from lib_bank where is_deleted=0 and status_active=1 and lien_bank=1 order by bank_name","id,bank_name", 1, "-- All Lien Bank --", 0, "" );
                        ?>
                        </td>
                        <td>	
                        <?php 
                        	echo create_drop_down( "cbo_location", 145, "select id,location_name from lib_location","id,location_name", 1, "-- Select Location --", $selected,"",0,"" );
                        ?>
                        </td>
                        <td>
                        <?php 
						$based_on_arr=array(1=>"Invoice Date",2=>"Exfactory Date",3=>"Actual Date");
                        	echo create_drop_down( "cbo_based_on", 135, $based_on_arr,"", 1, "------ Select ------", 1);
                        ?>
                        </td>
                        <td>
                        
                        <input name="txt_date_from" id="txt_date_from" class="datepicker"  style="width:80px">
                        </td>
                        <td>
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px">
                        </td>
                        <td>
                        <input type="button" name="search" id="search" value="Show" onClick="generate_report()" style="width:70px" class="formbutton" />
                        </td>
                    </tr>
                </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="15" align="center" valign="bottom"><?php echo load_month_buttons(1);  ?></td>
        </tr>
    </table>
    </fieldset>
    </div>
    </div>
    <div id="report_container" align="center"></div>
    <div id="report_container2"></div>
    </form>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>