-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ppl_planning_info_entry_dtls`
--

DROP TABLE IF EXISTS `ppl_planning_info_entry_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ppl_planning_info_entry_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `knitting_source` tinyint(4) NOT NULL DEFAULT '0',
  `knitting_party` int(11) NOT NULL DEFAULT '0',
  `color_id` varchar(50) NOT NULL COMMENT 'comma separated color ID',
  `color_range` tinyint(4) NOT NULL DEFAULT '0',
  `machine_dia` int(11) NOT NULL DEFAULT '0',
  `width_dia_type` tinyint(4) NOT NULL DEFAULT '0',
  `machine_gg` varchar(20) NOT NULL,
  `fabric_dia` varchar(15) NOT NULL,
  `program_qnty` double NOT NULL DEFAULT '0',
  `program_date` date NOT NULL,
  `stitch_length` varchar(30) NOT NULL,
  `spandex_stitch_length` varchar(30) NOT NULL,
  `draft_ratio` double NOT NULL DEFAULT '0',
  `machine_id` varchar(50) NOT NULL COMMENT 'comma separated Machine id',
  `machine_capacity` double NOT NULL DEFAULT '0',
  `distribution_qnty` double NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `feeder` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(255) NOT NULL,
  `save_data` text NOT NULL,
  `no_fo_feeder_data` text NOT NULL,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `advice` text NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Yes 0;No',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Yes 0:No ',
  PRIMARY KEY (`id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ppl_planning_info_entry_dtls`
--

LOCK TABLES `ppl_planning_info_entry_dtls` WRITE;
/*!40000 ALTER TABLE `ppl_planning_info_entry_dtls` DISABLE KEYS */;
INSERT INTO `ppl_planning_info_entry_dtls` VALUES (1,1,1,1,'2,7',5,32,1,'24','68',291,'2015-04-27','2.65','',0,'3',300,291,0,'2015-04-27','2015-04-27',0,'','3_3_300_291_0.97_27-04-2015_27-04-2015','',0,'',1,'2015-04-27 10:55:28',0,'0000-00-00 00:00:00',1,0),(2,2,3,28,'10,11,12,16,19,25,27,28,44,49,50',5,24,2,'24','45',186,'2015-04-27','','',0,'',0,0,0,'0000-00-00','0000-00-00',0,'','','',0,'',1,'2015-04-27 11:13:01',0,'0000-00-00 00:00:00',1,0),(3,3,1,1,'71',1,34,1,'24','70',2,'2015-05-04','2.75','',0,'4',350,2,4,'2015-05-04','2015-05-04',0,'','4_4_350_2_0.01_04-05-2015_04-05-2015','',0,'',2,'2015-05-04 15:13:19',2,'2015-05-04 16:40:26',1,0),(4,4,1,1,'7',5,36,1,'24','68',5,'2015-05-04','2.75','',0,'1',350,5,2,'2015-05-04','2015-05-04',0,'','1_1_350_5_0.01_04-05-2015_04-05-2015','',0,'',2,'2015-05-04 15:39:26',0,'0000-00-00 00:00:00',1,0),(5,3,3,112,'115',4,36,1,'24','70',7,'2015-05-04','2.78','',0,'',0,0,4,'0000-00-00','0000-00-00',0,'','1_1_350_7_0.02_01-05-2015_01-05-2015','',0,'',2,'2015-05-04 16:40:10',2,'2015-05-04 16:51:14',1,0),(6,5,1,1,'',5,34,1,'24','190',3,'2015-05-09','2.75','',0,'2',350,33,2,'2015-05-09','2015-05-09',0,'','2_2_350_33_0.09__','',0,'',2,'2015-05-09 12:03:10',0,'0000-00-00 00:00:00',1,0),(7,6,1,1,'393,394,395',5,38,1,'24','72',652,'2015-05-11','2.85','',0,'8',350,652,1,'2015-05-12','2015-05-13',1,'','8_8_350_652_1.86_12-05-2015_13-05-2015','',0,'',17,'2015-05-12 17:56:38',0,'0000-00-00 00:00:00',1,0),(8,7,1,1,'',5,28,1,'24','43',10,'2015-05-26','2.85','',0,'9',300,10,2,'2015-05-26','2015-05-26',0,'','9_9_300_10_0.03_26-05-2015_26-05-2015','',0,'',2,'2015-05-26 09:49:06',0,'0000-00-00 00:00:00',1,0),(9,8,1,1,'',5,30,2,'24','36',147.48,'2015-05-27','2.5','',0,'3',320,147.48,0,'2015-05-27','2015-05-27',0,'','3_3_320_147.48_0.46_27-05-2015_27-05-2015','',0,'',17,'2015-05-27 16:47:23',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `ppl_planning_info_entry_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
