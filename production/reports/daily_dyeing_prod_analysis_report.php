<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will Create Daily Dyeing Production Analysis Report
Functionality	:	
JS Functions	:
Created by		:	Kausar
Creation date 	: 	03-02-2014
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start(); 
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Daily Dyeing Production Analysis Report", "../../", 1, 1,'','','');

?>	
<script>

	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<?php echo $permission; ?>';

	var tableFilters = 
	{
		col_23: "none",
		col_operation: {
		id: ["total_dyeing_qnty","total_dye_chem_cost"],
		col: [12,21],
		operation: ["sum","sum"],
		write_method: ["innerHTML","innerHTML"]
		}
	} 

	function fn_report_generated()
	{
		if (form_validation('cbo_company_id*txt_date_from*txt_date_to','Comapny Name*From Date*To Date')==false)
		{
			return;
		}
		else
		{
			var data="action=report_generate"+get_submitted_data_string('cbo_company_id*cbo_unit_id*txt_date_from*txt_date_to',"../../");
			freeze_window(3);
			http.open("POST","requires/daily_dyeing_prod_analysis_repor_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_report_generated_reponse;
		}
	}
	
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****"); 
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../'); 
			//append_report_checkbox('table_header_1',1);
			
			//if( $("#cbo_type").val()==1 ){ setFilterGrid("table_body",-1,tableFilters1);}
			//else{ setFilterGrid("table_body",-1,tableFilters2);}
			setFilterGrid("tbl_dyeing",-1,tableFilters);
			show_msg('3');
			release_freezing();
		}
	}

	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}

</script>
</head>
<body onLoad="set_hotkey();">
	<div style="width:100%;" align="center">
	<?php echo load_freeze_divs ("../../",''); ?>
		 <form name="dailydyeingprodreport_1" id="dailydyeingprodreport_1"> 
         <h3 style="width:800px; margin-top:10px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel','')">-Search Panel</h3> 
         <div id="content_search_panel" style="width:800px" >      
             <fieldset>
                 <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
                    <thead>
                        <th class="must_entry_caption">Company Name</th>
                        <th>Unit Name</th>
                        <th class="must_entry_caption">Dyeing Receive Date</th>
                        <th><input type="reset" name="res" id="res" value="Reset" onClick="reset_form('dailydyeingprodreport_1','report_container*report_container2','','','')" class="formbutton" style="width:100px" /></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td> 
                                <?php
                                    echo create_drop_down( "cbo_company_id", 170, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down('requires/daily_dyeing_prod_analysis_repor_controller', this.value, 'load_drop_down_unit', 'unit_name_td' );" );
                                ?>
                            </td>
                            <td id="unit_name_td">
                                <?php
                                    echo create_drop_down("cbo_unit_id",150,$blank_array,"", 1, "-- All --", 0,"",0,'');
                                ?>
                            </td>
                            <td align="center">
                                 <input type="text" name="txt_date_from" id="txt_date_from" value="" class="datepicker" style="width:110px" placeholder="From Date"/>
                                 &nbsp;To&nbsp;
                                 <input type="text" name="txt_date_to" id="txt_date_to" value="" class="datepicker" style="width:110px" placeholder="To Date"/>
                            </td>
                            <td align="center"><input type="button" id="show_button" class="formbutton" style="width:100px" value="Show" onClick="fn_report_generated()" /></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" width="95%"><?php echo load_month_buttons(1); ?></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
		</form>
	</div>
    <div id="report_container" align="center"></div>
    <div id="report_container2" align="left"></div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>