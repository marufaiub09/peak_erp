-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_sales_contract`
--

DROP TABLE IF EXISTS `com_sales_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_sales_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_prefix` varchar(20) NOT NULL,
  `contact_prefix_number` int(11) NOT NULL DEFAULT '0',
  `contact_system_id` varchar(50) NOT NULL,
  `contract_no` varchar(50) NOT NULL,
  `contract_date` date NOT NULL,
  `beneficiary_name` int(10) NOT NULL DEFAULT '0',
  `buyer_name` int(10) NOT NULL DEFAULT '0',
  `applicant_name` int(11) NOT NULL DEFAULT '0',
  `notifying_party` varchar(50) NOT NULL,
  `consignee` varchar(50) NOT NULL,
  `convertible_to_lc` tinyint(2) NOT NULL DEFAULT '0',
  `lien_bank` int(10) NOT NULL DEFAULT '0',
  `lien_date` date NOT NULL,
  `contract_value` double NOT NULL DEFAULT '0',
  `currency_name` int(11) NOT NULL DEFAULT '0',
  `tolerance` double NOT NULL DEFAULT '0',
  `maximum_tolarence` float(11,4) NOT NULL DEFAULT '0.0000',
  `minimum_tolarence` float(11,4) NOT NULL DEFAULT '0.0000',
  `last_shipment_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `shipping_mode` tinyint(4) NOT NULL DEFAULT '0',
  `pay_term` tinyint(4) NOT NULL DEFAULT '0',
  `inco_term` tinyint(4) NOT NULL DEFAULT '0',
  `inco_term_place` varchar(50) NOT NULL,
  `contract_source` tinyint(2) NOT NULL DEFAULT '0',
  `port_of_entry` varchar(50) NOT NULL,
  `port_of_loading` varchar(50) NOT NULL,
  `port_of_discharge` varchar(50) NOT NULL,
  `internal_file_no` double NOT NULL DEFAULT '0',
  `shipping_line` varchar(50) NOT NULL,
  `doc_presentation_days` int(11) NOT NULL DEFAULT '0',
  `max_btb_limit` float(8,4) NOT NULL DEFAULT '0.0000',
  `max_btb_limit_value` double NOT NULL DEFAULT '0',
  `foreign_comn` float(8,4) NOT NULL DEFAULT '0.0000',
  `foreign_comn_value` double NOT NULL DEFAULT '0',
  `local_comn` float(8,4) NOT NULL DEFAULT '0.0000',
  `local_comn_value` double NOT NULL DEFAULT '0',
  `remarks` varchar(255) NOT NULL,
  `tenor` int(11) NOT NULL DEFAULT '0',
  `discount_clauses` tinytext NOT NULL,
  `converted_from` int(11) NOT NULL DEFAULT '0',
  `converted_btb_lc_list` varchar(75) NOT NULL,
  `claim_adjustment` double NOT NULL DEFAULT '0',
  `bank_file_no` varchar(100) NOT NULL,
  `sc_year` varchar(20) NOT NULL,
  `bl_clause` tinytext NOT NULL,
  `export_item_category` tinyint(4) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `contact_system_id` (`contact_system_id`),
  KEY `contract_no` (`contract_no`),
  KEY `contract_date` (`contract_date`),
  KEY `beneficiary_name` (`beneficiary_name`),
  KEY `buyer_name` (`buyer_name`),
  KEY `applicant_name` (`applicant_name`),
  KEY `notifying_party` (`notifying_party`),
  KEY `convertible_to_lc` (`convertible_to_lc`),
  KEY `lien_bank` (`lien_bank`),
  KEY `last_shipment_date` (`last_shipment_date`),
  KEY `expiry_date` (`expiry_date`),
  KEY `shipping_mode` (`shipping_mode`),
  KEY `internal_file_no` (`internal_file_no`),
  KEY `bank_file_no` (`bank_file_no`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`),
  KEY `sc_year` (`sc_year`),
  KEY `lien_date` (`lien_date`),
  KEY `export_item_category` (`export_item_category`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_sales_contract`
--

LOCK TABLES `com_sales_contract` WRITE;
/*!40000 ALTER TABLE `com_sales_contract` DISABLE KEYS */;
INSERT INTO `com_sales_contract` VALUES (1,'PAL-SC-15-',1,'PAL-SC-15-00001','PEAK/HAQUE/01/2015','2015-03-10',1,44,44,'53','53',1,2,'0000-00-00',60840,2,5,63882.0000,57798.0000,'2015-05-30','2015-06-14',1,3,2,'',1,'','','',1,'',5,0.0000,0,0.0000,0,0.0000,0,'',0,'',0,'',0,'','2015','without bank',1,13,'2015-05-13 10:25:02',13,'2015-05-25 10:07:06',1,0,0),(2,'PAL-SC-15-',2,'PAL-SC-15-00002','VDR/PEAK/14/2015','2015-10-13',1,48,62,'','',1,4,'2015-10-21',139989.5,2,5,146988.9688,132990.0312,'2015-12-15','2015-12-30',1,1,1,'CTG',1,'CTG','CTG','',7,'',15,70.0000,97992.65,0.0000,0,0.0000,0,'',0,'',0,'',0,'','2015','',1,13,'2015-10-24 14:15:19',13,'2015-11-29 10:19:05',1,0,0),(3,'PAL-SC-15-',3,'PAL-SC-15-00003','TVM-PAL-001/2015','2015-10-19',1,76,76,'','',2,4,'2015-10-29',87321.89,2,5,91687.9844,82955.7969,'2016-01-15','2016-01-30',1,3,1,'FOB',1,'','CTG','',39,'',10,0.0000,0,0.0000,0,0.0000,0,'',0,'',0,'',0,'','2015','',1,13,'2015-11-01 12:32:15',13,'2015-11-29 10:26:01',1,0,0),(4,'PAL-SC-15-',4,'PAL-SC-15-00004','00504CI03415','2015-11-02',1,83,83,'','83',1,4,'2015-11-02',99846,2,5,104838.2969,94853.7031,'2015-12-31','2016-01-15',1,1,1,'FOB',1,'CTG','CTG','',40,'',0,0.0000,0,0.0000,0,0.0000,0,'',0,'',0,'',0,'','2015','',1,13,'2015-11-10 10:28:47',13,'2015-11-29 11:13:11',1,0,0),(5,'PAL-SC-15-',5,'PAL-SC-15-00005','VDR/PEAK/16/2015','2015-11-05',1,84,62,'','',1,4,'2015-11-08',20166,2,5,21174.3008,19157.6992,'2016-01-15','2016-01-30',1,1,1,'FOB',1,'CTG','CTG','',5,'',0,0.0000,0,0.0000,0,0.0000,0,'',30,'',0,'',0,'','2015','',1,13,'2015-11-10 10:38:45',13,'2015-11-16 10:54:02',1,0,0),(6,'PAL-SC-15-',6,'PAL-SC-15-00006','RCPL UEFA 01/2015','2015-11-22',1,97,97,'','74',1,4,'2015-11-24',314496.8,2,5,330221.6250,298771.9688,'2016-01-31','2016-02-15',1,1,1,'FOB',1,'CTG','CTG','NAPLES PORT, ITALY.',6,'',15,0.0000,0,0.0000,0,0.0000,0,'',0,'',0,'',0,'','2015','',1,13,'2015-11-26 19:18:15',0,'0000-00-00 00:00:00',1,0,0);
/*!40000 ALTER TABLE `com_sales_contract` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
