<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will create for Pre Export Finance entry
					
Functionality	:	
				

JS Functions	:

Created by		:	Bilas 
Creation date 	: 	 
Updated by 		: 	Bilas
Update date		: 	Jahid 	   	   

QC Performed BY	:		

QC Date			:	

Comments		: according to requirment of Sayed bai such as validation change (Lc no popup & Bank Ref no popup) .

*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Pre Export Finance Form", "../../", 1, 1,'','1','');
?>	
 
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission='<?php echo $permission; ?>';


function openmypage_LcSc()
{
	var companyID = $("#cbo_company_name").val();
	var buyerID = $("#cbo_buyer_name").val();
 	var invoice_id_string = $("#invoice_id_string").val();
	var mst_tbl_id = $("#mst_tbl_id").val();
	var dtls_sub_ref_id = $("#dtls_sub_ref_id").val();
	//alert(mst_tbl_id);return;	  
	if (form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	
	var page_link='requires/export_doc_sub_entry_controller.php?action=lcSc_popup_search&companyID='+companyID+'&buyerID='+buyerID+'&invoice_id_string='+invoice_id_string+'&mst_tbl_id='+mst_tbl_id+'&dtls_sub_ref_id='+dtls_sub_ref_id;
	var title='Document Submission Form';
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=750px,height=400px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
	{ 
		var theform=this.contentDoc.forms[0];
		var all_invoice_id=this.contentDoc.getElementById("all_invoice_id").value;
		var all_sub_dtls_id=this.contentDoc.getElementById("all_sub_dtls_id").value;
		//alert(all_sub_dtls_id);
		$("#cbo_submission_type").val('');
		$("#cbo_submission_type").attr("disabled",false);
		$("#txt_negotiation_date").attr("disabled",false);
		if(all_sub_dtls_id!="")
		{
			//alert("jahid");
			$("#cbo_submission_type").val(1);
			fn_negotiation();
			$("#cbo_submission_type").attr("disabled",true);
			
		}
 		$("#invoice_id_string").val(all_invoice_id);
		if(trim(all_invoice_id)!="")
		{
			freeze_window(5); 
			get_php_form_data(all_invoice_id+'**'+all_sub_dtls_id, "show_invoice_list_view", "requires/export_doc_sub_entry_controller" );
 			release_freezing();
		}
					 
	}
}



 
function fnc_export_doc_sub_entry(operation)
{

	if (form_validation('cbo_company_name*cbo_buyer_name*lcsc_no*txt_submit_date','Company Name*Buyer Name*LC/SC No*Submission Date')==false )
	{
		return;
	}
	
	var submission_type=$('#cbo_submission_type').val();
	if(submission_type==2)
	{
		var txt_total=$('#txt_total').val()*1;
		var total_foreign_curr_hid=$('#total_foreign_curr_hid').val()*1;
		
		if(total_foreign_curr_hid>txt_total)
		{
			alert("Total LC/SC Currency Exceeds Total Net Invoice Value.");
			return;
		}
	}
	
	var dataString="";
	var invoiceRow = $('#invo_table tbody tr').length-1;
	for(var i=1;i<=invoiceRow;i++)
	{
		try 
		{
			dataString += "&txt_invoice_no"+i+"="+$("#txt_invoice_no"+i).val();
			dataString += "&hidden_invoice_id"+i+"="+$("#hidden_invoice_id"+i).val();
			dataString += "&hidden_sub_dtls_id"+i+"="+$("#hidden_sub_dtls_id"+i).val();
			dataString += "&txt_lcsc_id"+i+"="+$("#txt_lcsc_id"+i).val();
			dataString += "&hidden_is_lc"+i+"="+$("#hidden_is_lc"+i).val();
			dataString += "&txt_lcsc_no"+i+"="+$("#txt_lcsc_no"+i).val();
			dataString += "&txt_bl_no"+i+"="+$("#txt_bl_no"+i).val();
			dataString += "&txt_invoice_date"+i+"="+$("#txt_invoice_date"+i).val();
			dataString += "&txt_net_invo_value"+i+"="+$("#txt_net_invo_value"+i).val();
			dataString += "&hidden_po_numbers_id"+i+"="+$("#hidden_po_numbers_id"+i).val();
		}
		catch(e) 
		{
			//got error no operation
		}
	}
	
	var transRow = $('#trans_details tbody tr:last').attr('id');
	if(transRow && (transRow!="" || transRow!=null || transRow!="undefined") )
	{
		transRow = transRow.substring(2,transRow.length);
	}
	
	for(var i=1;i<=transRow;i++)
	{
		try
		{
			if (form_validation('cbo_account_head_'+i+'*'+'txt_domestic_curr_'+i+'*'+'txt_conversion_rate_'+i,'Account Head*Domestic Currency*Conversation Rate')==false )
			{
				return;
			}
			dataString += "&cbo_account_head_"+i+"="+$("#cbo_account_head_"+i).val();
			dataString += "&txt_ac_loan_no_"+i+"="+$("#txt_ac_loan_no_"+i).val();
			dataString += "&txt_domestic_curr_"+i+"="+$("#txt_domestic_curr_"+i).val();
			dataString += "&txt_conversion_rate_"+i+"="+$("#txt_conversion_rate_"+i).val();
			dataString += "&txt_lcsc_currency_"+i+"="+$("#txt_lcsc_currency_"+i).val();
		}
		catch(e)
		{
			//got error no operation
		}
	}
	
	//get_submitted_data_string('cbo_company_name*cbo_buyer_name*lcsc_no*lc_sc_id*txt_submit_date*cbo_submit_to*txt_bank_ref*txt_bank_ref_date*cbo_submission_type*txt_negotiation_date*txt_day_to_realize*txt_possible_reali_date*courier_receipt_no*txt_courier_company*txt_courier_date*txt_bnk_to_bnk_cour_no*txt_bnk_to_bnk_cour_date*cbo_lien_bank*cbo_currency*txt_remarks*total_dom_curr_hid*total_foreign_curr_hid*mst_tbl_id*invoice_tbl_id',"../../");		
	var data="action=save_update_delete&operation="+operation+'&invoiceRow='+invoiceRow+'&transRow='+transRow+dataString+get_submitted_data_string('cbo_company_name*cbo_buyer_name*lcsc_no*lc_sc_id*txt_submit_date*cbo_submit_to*txt_bank_ref*txt_bank_ref_date*cbo_submission_type*txt_negotiation_date*txt_day_to_realize*txt_possible_reali_date*courier_receipt_no*txt_courier_company*txt_courier_date*txt_bnk_to_bnk_cour_no*txt_bnk_to_bnk_cour_date*cbo_lien_bank*cbo_currency*txt_remarks*total_dom_curr_hid*total_foreign_curr_hid*mst_tbl_id*invoice_tbl_id',"../../");
	freeze_window(operation);		
	http.open("POST","requires/export_doc_sub_entry_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_export_doc_sub_entry_Reply_info;
}


function fnc_export_doc_sub_entry_Reply_info()
{
	
	if(http.readyState == 4) 
	{
		//release_freezing();
		//alert(http.responseText);return;
		var reponse=http.responseText.split('**');	
		if(reponse[0]==20)
		{
			alert(reponse[1]);
			release_freezing();
			return;
		}		 
		show_msg(trim(reponse[0])); 
		if( reponse[0]==0 || reponse[0]==1)
		{		
			get_php_form_data(reponse[1], "populate_master_from_data", "requires/export_doc_sub_entry_controller");
			set_button_status(1, permission, 'fnc_export_doc_sub_entry',1); 			 
		}
		if(reponse[0]==2)
		{
			$('#transaction_container').html("");
			$('#invoice_container').html("");
			$('#total_dom_curr_hid').val("");
			$('#total_foreign_curr_hid').val("");
			set_button_status(1, permission, 'fnc_export_doc_sub_entry',1); 			 
		}
		//fnResetForm();
		release_freezing();
	}
}


function pop_doc_submission()
{
	if (form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company_name = $("#cbo_company_name").val();
	var buyer_name = $("#cbo_buyer_name").val();
	var page_link='requires/export_doc_sub_entry_controller.php?action=doc_sub_popup&company_name='+company_name+'&buyer_name='+buyer_name; 
	var title="Search System Number Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=370px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var mstID=this.contentDoc.getElementById("hidden_system_number").value.split("**"); // master table id
		$("#cbo_submission_type").val('');
		$("#cbo_submission_type").attr("disabled",false);
		$("#txt_negotiation_date").attr("disabled",false);
		//alert(mstID[2]);
		/*if(mstID[2]==2)
		{
			$("#cbo_submission_type").val(mstID[2]);
			//fn_negotiation();
			$("#cbo_submission_type").attr("disabled",true);
			
		}*/
  		// master part call here		
		get_php_form_data(mstID[0], "populate_master_from_data", "requires/export_doc_sub_entry_controller");
		$('#dtls_sub_ref_id').val(mstID[1]);
		set_button_status(1, permission, 'fnc_export_doc_sub_entry',1);		
  	}
}



	 
	function fn_negotiation()
	{	     
		var submit_type = $('#cbo_submission_type').val();
		if(submit_type=='1')
		{
			$('#txt_negotiation_date').attr('disabled','disabled');
			$('#txt_negotiated_ammount').attr('disabled','disabled');
			$('#transaction_container').find('tr').remove();
			$('#transaction_container').append('<tr id="tr0"><td colspan="6"><b><center>Please Select LC/SC to view Transaction</center></b></td> </tr>');
		}                       
		else if(submit_type=='2')
		{
			var rowNo=0;
			$('#txt_negotiation_date').removeAttr('disabled');
			$('#txt_negotiated_ammount').removeAttr('disabled');
			var response = return_global_ajax_value(rowNo, "transaction_add_row", "", "requires/export_doc_sub_entry_controller" );
			$('#transaction_container').find('tr').remove();
			$('#transaction_container').append(response);
		}
	}

	
	function fn_inc_decr_row(DelrowNo,type)
	{
		if(type=='decrease')
		{
			$('#total_foreign_curr_hid').val(number_format(($('#total_foreign_curr_hid').val()*1)-($('#txt_lcsc_currency_'+DelrowNo).val()*1),4,".",""));
			$('#total_dom_curr_hid').val(number_format(($('#total_dom_curr_hid').val()*1)-($('#txt_domestic_curr_'+DelrowNo).val()*1),4,".",""));
		}
		
		var rownumber = $("#transaction_container tr:last").attr('id');
		var rowNo = rownumber.substr(2,rownumber.length);
		if(type=='increase')
		{
			var response = return_global_ajax_value(rowNo, "transaction_add_row", "", "requires/export_doc_sub_entry_controller" );
 			$('#transaction_container').append(response);
		}
		else if(type=='decrease')
		{
           var tblRow = $("#transaction_container tr").length; 
		   if(tblRow*1!=1 && rowNo==DelrowNo)
		   { 
		   		$("#tr"+DelrowNo).remove();
		   }
		}		
 	}
	
	
	function realy_days(field_id)
	{   
	   var from_date = $('#txt_bank_ref_date').val();
	   var days_to_realize = $('#txt_day_to_realize').val();
	   var to_date = $('#txt_possible_reali_date').val();	  
	   days_to_realize = days_to_realize*1-1;
	   
	   if(days_to_realize=="" || days_to_realize*1==-1) return;
	   if(from_date !="" )
	   {		
			if( (field_id == 'txt_day_to_realize' || field_id == 'txt_bank_ref_date') && days_to_realize!="")
			{ 
				var res_date = add_days( from_date, days_to_realize );
				$('#txt_possible_reali_date').val(res_date);
			}
			else if(field_id == 'txt_possible_reali_date' &&  to_date!="")
			{ 				 
				var datediff = date_diff( 'd', from_date, to_date )+1;					 
				$('#txt_day_to_realize').val(datediff);
			}
			
	   }               
	}
	
	
	function fn_calculate(id,rowNo)
	{
		var domesticCurr 	= $("#txt_domestic_curr_"+rowNo).val();
		var domesticRate 	= $("#txt_conversion_rate_"+rowNo).val();
		var lcscCurr 		= $("#txt_lcsc_currency_"+rowNo).val();
 		  
		if(id=="txt_domestic_curr_"+rowNo)
		{
			if( ($("#txt_conversion_rate_"+rowNo).val()=="" || $("#txt_conversion_rate_"+rowNo).val()==0) && ( $("#txt_lcsc_currency_"+rowNo).val()!="" || $("#txt_lcsc_currency_"+rowNo).val()>0) )
			{
				domesticRate = domesticCurr*1/lcscCurr*1;
				$("#txt_conversion_rate_"+rowNo).val(number_format_common(domesticRate,4,'',''));
			}
			else if( ($("#txt_conversion_rate_"+rowNo).val()!="" || $("#txt_conversion_rate_"+rowNo).val()>0) && ($("#txt_lcsc_currency_"+rowNo).val()=="" || $("#txt_lcsc_currency_"+rowNo).val()==0))
			{
				lcscCurr = domesticCurr*1/domesticRate*1;
				$("#txt_lcsc_currency_"+rowNo).val(number_format_common(lcscCurr,5,'',''));
			}
			else
			{
				lcscCurr = domesticCurr*1/domesticRate*1;
				$("#txt_lcsc_currency_"+rowNo).val(number_format_common(lcscCurr,5,'',''));
			}
		} 
		else if(id=="txt_conversion_rate_"+rowNo)
		{
			if( ($("#txt_domestic_curr_"+rowNo).val()=="" || $("#txt_domestic_curr_"+rowNo).val()==0) && ($("#txt_lcsc_currency_"+rowNo).val()!="" || $("#txt_lcsc_currency_"+rowNo).val()>0) )
			{
				domesticCurr = domesticRate*1*lcscCurr*1;
				$("#txt_domestic_curr_"+rowNo).val(number_format_common(domesticCurr,4,'',''));
			}
			else if( ($("#txt_domestic_curr_"+rowNo).val()!="" || $("#txt_domestic_curr_"+rowNo).val()>0) && ($("#txt_lcsc_currency_"+rowNo).val()=="" || $("#txt_lcsc_currency_"+rowNo).val()==0) )
			{
				lcscCurr = domesticCurr*1/domesticRate*1;
				$("#txt_lcsc_currency_"+rowNo).val(number_format_common(lcscCurr,5,'',''));
			}
			else
			{
				lcscCurr = domesticCurr*1/domesticRate*1;
				$("#txt_lcsc_currency_"+rowNo).val(number_format_common(lcscCurr,5,'',''));
			}
		} 
		else if(id=="txt_lcsc_currency_"+rowNo)
		{ 
			if( ($("#txt_domestic_curr_"+rowNo).val()=="" || $("#txt_domestic_curr_"+rowNo).val()==0) && ($("#txt_conversion_rate_"+rowNo).val()!="" || $("#txt_conversion_rate_"+rowNo).val()>0) )
			{
				domesticCurr = lcscCurr*1*domesticRate*1;
				$("#txt_domestic_curr_"+rowNo).val(number_format_common(domesticCurr,5,'',''));
			}
			else if( ($("#txt_domestic_curr_"+rowNo).val()!="" || $("#txt_domestic_curr_"+rowNo).val()>0) && ($("#txt_conversion_rate_"+rowNo).val()=="" || $("#txt_conversion_rate_"+rowNo).val()==0) )
			{
				domesticRate = domesticCurr*1/lcscCurr*1;
				$("#txt_conversion_rate_"+rowNo).val(number_format_common(domesticRate,4,'',''));
			}
			else
			{
				domesticCurr = lcscCurr*1*domesticRate*1;
				$("#txt_domestic_curr_"+rowNo).val(number_format_common(domesticCurr,5,'',''));
			}
		} 
		
		//total domestic and foreign currency function call
 		sum_of_currency();
	}
	
	
	function sum_of_currency()
	{
		//total domestic and foreign currency
		var totalDomCurr = totalForeignCurr = 0;
		$("#trans_details tbody tr").each(function() {
             totalDomCurr += $(this).find("input[name='txt_domestic_curr[]']").val()*1; 
			 totalForeignCurr += $(this).find("input[name='txt_lcsc_currency[]']").val()*1; 
        });
		$("#total_dom_curr_hid").val(number_format_common(totalDomCurr,4,'',''));
		$("#total_foreign_curr_hid").val(number_format_common(totalForeignCurr,5,'',''));
	}
	
	
	
	//reset/refresh function 
	function fnResetForm()
	{
		reset_form('docsubmFrm_1','','','','$(\'#transaction_container tr:not(:first)\').remove();','');
		$('#invo_table').find('tr:gt(0)').remove();
		$('#invoice_container').append('<tr id="tr0"><td colspan="6"><b><center>Please Select LC/SC to view invoice List</center></b></td></tr>');
		$('#trans_details').find('tr:gt(0)').remove();
		$('#trans_details thead').after('<tbody id="transaction_container"><tr id="tr0"><td colspan="6"><b><center>Please Select LC/SC to view Transaction</center></b></td></tr></tbody><tfoot><th colspan="2">Sum&nbsp;&nbsp;</th><td><input type="text" id="total_dom_curr_hid" class="text_boxes_numeric" style="width:100px" disabled readonly /></td><th>&nbsp;</th>	<td><input type="text" id="total_foreign_curr_hid" class="text_boxes_numeric" style="width:100px" disabled readonly /></td><th></th></tfoot>');
 		set_button_status(0, permission, 'fnc_export_doc_sub_entry',1);	
	}
	
                    	 	                        	
                        
    function fn_print_letter()
	{
		if (form_validation('mst_tbl_id','Save Data First')==false)
		{
			alert("Save Data First");
			return;
		}
		else
		{
			 print_report( $('#mst_tbl_id').val(), "bank_submit_letter", "requires/export_doc_sub_entry_controller" ) ;
		}
	}                   
                        	
                        
                     
</script>
 
 
</head> 
<body onLoad="set_hotkey();">
	<div style="width:100%;" align="center">																	
     	<?php echo load_freeze_divs ("../../",$permission); ?><br/>
        <fieldset style="width:930px; margin-bottom:10px;">
        <form name="docsubmFrm_1" id="docsubmFrm_1" autocomplete="off" method="POST"  >
        	 
            	<!------------------------------ 1st form Start here ------------------------------------------->
          <fieldset style="width:950px;">
            <legend>Commercial Module</legend>
              <table  width="950" cellspacing="2" cellpadding="0" border="0" id="tbl_master"> 
                 <tr>
                      <td  width="130" align="right" class="must_entry_caption">Company Name </td>
                      <td width="170">
                          <?php
								echo create_drop_down( "cbo_company_name", 150, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select --", 0, "" );//load_drop_down( 'requires/export_doc_sub_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );
							?> 
                      </td>
                      <td width="130" align="right" class="must_entry_caption" >Buyer</td>
                      <td width="160" id="buyer_td">
                            <?php 
								//echo create_drop_down( "cbo_buyer_name", 150, $blank_array,"", 1, "-- Select --", $selected, "" );
								echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy where buy.status_active =1 and buy.is_deleted=0 $buyer_cond order by buy.buyer_name","id,buyer_name", 1, " Display ", 0, "",1 );
							?>
                      </td>
                      <td width="130" align="right" class="must_entry_caption">LC/SC No </td>
                      <td width="170">
                          <input type="text" name="lcsc_no" id="lcsc_no" class="text_boxes" style="width:140px" placeholder="Double Click to Search"  readonly onDblClick="openmypage_LcSc();" />
                          <input type="hidden" name="lc_sc_id" id="lc_sc_id" readonly />
                          <input type="hidden" name="invoice_id_string" id="invoice_id_string" readonly />
                       </td>
                </tr>
                <tr>                                       
                     <td  width="130" align="right" class="must_entry_caption">Submission Date</td>
                     <td width="170">
                         <input style="width:140px " name="txt_submit_date" id="txt_submit_date" class="datepicker"  placeholder="Select Date" />
                     </td>
                     <td width="130" align="right" > Submitted To </td>
                     <td width="160">
                       	  <?php 
                                echo create_drop_down( "cbo_submit_to", 150, $submited_to,"", 1, "-- Select --", 1, "",1 );
                                ?>
                     </td>
                     <td width="130" align="right">Bank Ref/ Bill No </td>
                     <td width="170">
                          <input style="width:140px " name="txt_bank_ref" id="txt_bank_ref" class="text_boxes" placeholder="Double Click to Update"  ondblclick="pop_doc_submission();"><input type="hidden" name="dtls_sub_ref_id" id="dtls_sub_ref_id" readonly />
                     </td>
                </tr>
                <tr>
                  
                        <td width="130" align="right">Bank Ref Date </td>
                        <td width="170">
                          <input type="text" name="txt_bank_ref_date" id="txt_bank_ref_date" class="datepicker" onChange="realy_days(this.id)" style="width:140px"   />
                        </td>
                        <td width="130" align="right" >Submission Type </td>
                       <td width="160"> 
                           	<?php 
                                echo create_drop_down( "cbo_submission_type", 150, $submission_type,"", 1, "-- Select --", 0, "fn_negotiation()" );
                            ?>
                       </td>
                       <td width="130" align="right">Negotiation Date</td>
                       <td width="170"> 
                         <input type="text" name="txt_negotiation_date" id="txt_negotiation_date" class="datepicker" style="width:140px"   />
                       </td>
                </tr> 
                <tr>                                   
                        <td  width="130" align="right">Days to Realize </td>
                        <td width="170">
                            <input type="text" name="txt_day_to_realize" id="txt_day_to_realize" class="text_boxes_numeric" style="width:140px" onChange="realy_days(this.id)" />	
                        </td>
                        <td width="130" align="right" >Possible Reali Date</td>
                       <td width="160">  
                         <input type="text" name="txt_possible_reali_date" id= "txt_possible_reali_date" class="datepicker" onChange="realy_days(this.id)" style="width:140px" />
                       </td>
                       <td width="130" align="right">Courier Receipt No.</td>
                       <td width="170"><input type="text" name="courier_receipt_no" id="courier_receipt_no" class="text_boxes" style="width:140px" /></td>                                   
                </tr>
                <tr>                                   
                       	<td width="130" align="right">Courier Company </td>
                        <td width="170"><input type="text" name="txt_courier_company" id="txt_courier_company" class="text_boxes" style="width:140px" /></td>
                        <td width="130" align="right" >GSP Courier Date</td>
                       	<td width="160">  <input type="text" name="txt_courier_date" id="txt_courier_date" class="datepicker" style="width:140px" /> </td>
                        <td width="130" align="right">Bnk-2-Bnk Cour No</td>
                        <td width="170"><input type="text" name="txt_bnk_to_bnk_cour_no" id="txt_bnk_to_bnk_cour_no" class="text_boxes" style="width:140px" /></td>                                   
                </tr>
                <tr>                                   
                        <td  width="130" align="right">Bnk-2-Bnk Cour Date</td>
                        <td width="170"><input type="text" name="txt_bnk_to_bnk_cour_date" id="txt_bnk_to_bnk_cour_date" class="datepicker" style="width:140px" /></td>
                        <td width="130" align="right" >Lien Bank </td>
                        <td width="160">  
                          <?php  
                                echo create_drop_down( "cbo_lien_bank", 150, "select id,bank_name from lib_bank where lien_bank=1 and is_deleted = 0 AND status_active = 1 order by bank_name","id,bank_name", 1, "-- Select --", $selected, "",1 );
                            ?>
                       </td>
                       <td width="130" align="right">LC/SC Currency</td>
                       <td width="170"><?php
                               echo create_drop_down( "cbo_currency", 150, $currency,"", 1, "-- Select Currency --", $currencyID, "",1 );
                            ?></td>                                   
                </tr>
                <tr>                                   
                        <td  width="130" align="right">Remarks</td>
                        <td colspan="5"> <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:455px" /></td>
                </tr>
            </table>
            </fieldset>
            
            <br /> 
            
            <fieldset>
                <legend>Invoice List</legend>
                <table id="invo_table" width="800" class="rpt_table" rules="all">
                     <thead>
                        <tr>
                            <th width="50">SL</th>
                            <th width="100">Invoice No</th>
                            <th width="100">LC/SC No</th>
                            <th width="90">BL No</th>
                            <th width="93">Invoice Date</th>
                            <th width="103">Net Inv. Value</th>
                            <th width="200"> Order Numbers</th>
                        </tr>    
                    </thead>
    				<tbody id="invoice_container">
                        <tr id="tr0">
                            <td colspan="6"><b><center>Please Select LC/SC to view invoice List</center></b></td>
                        </tr>
                    </tbody>    
                </table>   
            </fieldset>         
            
            <br /> 
                   
            <fieldset>
                <legend>Transaction Details</legend>
                <table id='trans_details' width="800" class="rpt_table" rules="all">
                    <thead id='trans'>
                    	<tr>
                            <th width="220" >Account Head</th>
                            <th width="120" >AC/ Loan No</th>
                            <th width="120" >Domestic Currency</th>
                            <th width="120" >Conversation Rate</th>
                            <th width="120" >LC/SC Currency</th>
                            <th width="80" >Action</th>
                        </tr>    
                    </thead>
                    <tbody id="transaction_container">                         
                        <tr id="tr0">
                            <td colspan="6"><b><center>Please Select LC/SC to view Transaction</center></b></td>
                        </tr>
                    </tbody> 
                    <tfoot>
                    	<th colspan="2">Sum&nbsp;&nbsp;</th> 	                        	
                        <td><input type="text" id="total_dom_curr_hid" class="text_boxes_numeric" style="width:100px" disabled readonly /></td>	
                        <th>&nbsp;</th>	
                        <td><input type="text" id="total_foreign_curr_hid" class="text_boxes_numeric" style="width:100px" disabled readonly /></td>	
                        <th></th>
                     </tfoot>
                 </table>
            </fieldset>         
                      
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr> 
                   <td colspan="6" align="center"></td>				
                </tr>
                <tr>
                    <td align="center" colspan="6" valign="middle" class="button_container">
                          <!-- details table id for update -->
                          <input type="hidden" id="mst_tbl_id" name="mst_tbl_id" value="" />
                          <input type="hidden" id="invoice_tbl_id" name="invoice_tbl_id" value="" />
                           <!-- -->
                         <?php echo load_submit_buttons( $permission, "fnc_export_doc_sub_entry", 0,0,"fnResetForm()",1);?>
                         <input type="button" class="formbutton" id="btn_print_letter" value="Print letter" style="width:100px;" onClick="fn_print_letter()" >
                    </td>
               </tr> 
            </table>   
        	
         </form>
        </fieldset> 
 	</div>
</body> 
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>