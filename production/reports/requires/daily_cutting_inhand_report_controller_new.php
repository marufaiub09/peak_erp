﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$color_arr=return_library_array( "select id, color_name from  lib_color", "id", "color_name"  );
$line_arr=return_library_array( "select id, line_name from  lib_sewing_line", "id", "line_name");

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_short_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.id=b.buyer_id and b.tag_company=$data and a.status_active=1 and a.is_deleted=0 order by a.buyer_name","id,buyer_name", 1, "-- Select buyer --", 0, "","" );//load_drop_down( 'requires/daily_knitting_production_report_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_machine', 'machine_td' );$location_cond
  exit();	 
}

//item style------------------------------//
if($action=="style_wise_search")
{		  
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	if($company==0) $company_name=""; else $company_name="and a.company_name=$company";
	if($buyer==0) $buyer_name=""; else $buyer_name="and a.buyer_name=$buyer";
	if(str_replace("'","",$job_id)!="")  $job_cond="and b.id in(".str_replace("'","",$job_id).")";
    else  if (str_replace("'","",$job_no)=="") $job_cond=""; else $job_cond="and b.job_no_mst='".$job_no."'";

	$sql = "select a.id,a.style_ref_no,a.job_no_prefix_num,SUBSTRING_INDEX(a.insert_date, '-', 1) as year from wo_po_details_master a,wo_po_break_down b where a.job_no=b.job_no_mst $company_name $buyer_name $job_cond group by a.id"; 
	//echo $sql;die;
	echo create_list_view("list_view", "Style Refference,Job no,Year","190,100,100","440","310",0, $sql , "js_set_value", "id,style_ref_no", "", 1, "0", $arr, "style_ref_no,job_no_prefix_num,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}



//order wise browse------------------------------//
if($action=="job_wise_search")
{		  
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	if($company==0) $company_name=""; else $company_name=" and b.company_name=$company";//job_no
	if($buyer==0) $buyer_name=""; else $buyer_name="and b.buyer_name=$buyer";
	
	$sql = "select distinct a.id,a.po_number,a.job_no_mst,b.style_ref_no,b.job_no_prefix_num,SUBSTRING_INDEX(b.insert_date, '-', 1) as year from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company_name  $buyer_name";
	echo create_list_view("list_view", "Order Number,Job No,Year,Style Ref","150,100,100,100","500","310",0, $sql , "js_set_value", "id,job_no_mst", "", 1, "0", $arr, "po_number,job_no_prefix_num,year,style_ref_no", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}






//order wise browse------------------------------//
if($action=="order_wise_search")
{		  
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	//echo $job_no;die;
	if($company==0) $company_name=""; else $company_name=" and b.company_name=$company";
	if($buyer==0) $buyer_name=""; else $buyer_name="and b.buyer_name=$buyer";
	if(str_replace("'","",$job_id)!="")  $job_cond="and a.id in(".str_replace("'","",$job_id).")";
    else  if (str_replace("'","",$job_no)=="") $job_cond=""; else $job_cond="and a.job_no_mst='".$job_no."'";
	
	if(str_replace("'","",$style_id)!="")  $style_cond="and b.id in(".str_replace("'","",$style_id).")";
    else  if (str_replace("'","",$style_no)=="") $style_cond=""; else $style_cond="and b.style_ref_no='".$style_no."'";
	$sql = "select distinct a.id,a.po_number,b.style_ref_no,b.job_no_prefix_num,SUBSTRING_INDEX(b.insert_date, '-', 1) as year from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company_name $job_cond  $buyer_name $style_cond";
	//echo $sql;die;
	echo create_list_view("list_view", "Style Ref,Order Number,Job No, Year","150,150,100,100,","550","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "style_ref_no,po_number,job_no_prefix_num,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}



if($action=="generate_report")
{ 
   $process = array( &$_POST );
   extract(check_magic_quote_gpc( $process )); 
   $company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
   	if(str_replace("'","",$cbo_company_name)==0) $company_name=""; else $company_name=" and b.company_name=".str_replace("'","",$cbo_company_name)."";
	if(str_replace("'","",$cbo_buyer_name)==0)  $buyer_name=""; else $buyer_name="and b.buyer_name=".str_replace("'","",$cbo_buyer_name)."";
	
	if(str_replace("'","",$hidden_job_id)!="")  $job_cond="and a.id in(".str_replace("'","",$hidden_job_id).")";
	else  if (str_replace("'","",$txt_job_no)=="") $job_cond=""; else $job_cond="and a.job_no_mst='".str_replace("'","",$txt_job_no)."'";
	if(str_replace("'","",$hidden_style_id)!="")  $style_cond="and b.id in(".str_replace("'","",$hidden_style_id).")";
	else  if (str_replace("'","",$txt_style_no)=="") $style_cond=""; else $style_cond="and b.style_ref_no='".$txt_style_no."'";
	if (str_replace("'","",$hidden_order_id)!=""){ $order_cond="and a.id in (".str_replace("'","",$hidden_order_id).")";$job_cond=""; }
	else if (str_replace("'","",$txt_order_no)=="") $order_cond=""; else $order_cond="and a.po_number='".str_replace("'","",$txt_order_no)."'"; 
   
      $variable_arr=sql_select("select cutting_update,printing_emb_production,sewing_production,cutting_input from variable_settings_production where company_name=".str_replace("'","",$cbo_company_name)." and variable_list=1 and status_active=1");
        foreach($variable_arr as $row_var)
        {
           $variable_cutting=$row_var[csf('cutting_update')];
           $variable_print=$row_var[csf('printing_emb_production')];	
           $variable_delivery=$row_var[csf('cutting_input')];	
           $variable_sew=$row_var[csf('sewing_production')];		
        }
      
      if($variable_cutting!=$variable_print || $variable_cutting!=$variable_delivery || $variable_cutting!=$variable_sew )
        { 
          if($variable_cutting==1)
               {
                ?>	
                <script>
                  alert("Report Can't be generated dew to mixed variable setting for production update.(Pls. contact to system admin)");
                // return;
                </script>	
                <?php
                die;	
                }
                
         }
 $po_number_data=array();
 $production_data_arr=array();
 $po_number_id=array();
 if($variable_cutting==1)
         {
		  $pro_date_sql=sql_select ("SELECT a.id,a.job_no_mst,a.po_number,
                po_quantity,plan_cut as plan_qty,b.buyer_name,b.style_ref_no as style,b.job_no_prefix_num,SUBSTRING_INDEX(b.insert_date, '-', 1) as year 
                from 
                wo_po_break_down a,wo_po_details_master b
                where 
                a.job_no_mst=b.job_no  and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1   $company_name $buyer_name $style_cond $order_cond $job_cond "); 
		foreach($pro_date_sql as $row)
			{
				$po_number_data[$row[csf('id')]]['id']=$row[csf('id')];
				$po_number_data[$row[csf('id')]]['job_no']=$row[csf('job_no_mst')];
				$po_number_data[$row[csf('id')]]['po_number']=$row[csf('po_number')];
				$po_number_data[$row[csf('id')]]['po_quantity']=$row[csf('po_quantity')];
				$po_number_data[$row[csf('id')]]['plan_qty']=$row[csf('plan_qty')];
				$po_number_data[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')];
				$po_number_data[$row[csf('id')]]['style']=$row[csf('style')];
				$po_number_data[$row[csf('id')]]['job_prifix']=$row[csf('job_no_prefix_num')];
				$po_number_data[$row[csf('id')]]['year']=$row[csf('year')];
				$po_number_id[]=$row[csf('id')];
			}
	   $production_mst_sql= sql_select("SELECT po_break_down_id,
				sum(CASE WHEN production_type ='1' and production_date=".$txt_date_from." THEN production_quantity ELSE 0 END) AS cutting_qnty,
				sum(CASE WHEN production_type ='2' and production_date=".$txt_date_from." THEN production_quantity ELSE 0 END) AS printing_qnty,
				sum(CASE WHEN production_type ='3' and production_date=".$txt_date_from." THEN production_quantity ELSE 0 END) AS printreceived_qnty,
				sum(CASE WHEN production_type ='4' and production_date=".$txt_date_from." THEN production_quantity ELSE 0 END) AS sewingin_qnty,
				sum(CASE WHEN production_type ='9' and production_date=".$txt_date_from." THEN production_quantity ELSE 0 END) AS cut_delivery_qnty ,
				sum(CASE WHEN production_type ='1' and production_date<".$txt_date_from." THEN production_quantity ELSE 0 END) AS cutting_qnty_pre,
				sum(CASE WHEN production_type ='2' and production_date<".$txt_date_from." THEN production_quantity ELSE 0 END) AS printing_qnty_pre,
				sum(CASE WHEN production_type ='3' and production_date<".$txt_date_from." THEN production_quantity ELSE 0 END) AS printreceived_qnty_pre,
				sum(CASE WHEN production_type ='4' and production_date<".$txt_date_from." THEN production_quantity ELSE 0 END) AS sewingin_qnty_pre,
				sum(CASE WHEN production_type ='9' and production_date<".$txt_date_from." THEN production_quantity ELSE 0 END) AS cut_delivery_qnty_pre 
				from 
				pro_garments_production_mst 
				where  
				is_deleted=0 and status_active=1
				group by po_break_down_id "); 
		foreach($production_mst_sql as $val)
		     {
		  	    $production_data_arr[$val[csf('po_break_down_id')]]['cutting_qnty']=$val[csf('cutting_qnty')];
				$production_data_arr[$val[csf('po_break_down_id')]]['printing_qnty']=$val[csf('printing_qnty')];
				$production_data_arr[$val[csf('po_break_down_id')]]['printreceived_qnty']=$val[csf('printreceived_qnty')];
				$production_data_arr[$val[csf('po_break_down_id')]]['sewingin_qnty']=$val[csf('sewingin_qnty')];
				$production_data_arr[$val[csf('po_break_down_id')]]['cut_delivery_qnty']=$val[csf('cut_delivery_qnty')];
				$production_data_arr[$val[csf('po_break_down_id')]]['cutting_qnty_pre']=$val[csf('cutting_qnty_pre')];
				$production_data_arr[$val[csf('po_break_down_id')]]['printing_qnty_pre']=$val[csf('printing_qnty_pre')];
				$production_data_arr[$val[csf('po_break_down_id')]]['printreceived_qnty_pre']=$val[csf('printreceived_qnty_pre')];
				$production_data_arr[$val[csf('po_break_down_id')]]['sewingin_qnty_pre']=$val[csf('sewingin_qnty_pre')];
				$production_data_arr[$val[csf('po_break_down_id')]]['cut_delivery_qnty_pre']=$val[csf('cut_delivery_qnty_pre')]; 	
		     }
			 $po_number_id=implode(",",$po_number_id);
			 
     $sql_sewing=sql_select("SELECT b.po_break_down_id,b.color_number_id,sum( b.cons )/count( b.color_number_id ) AS conjunction,pcs
		    FROM wo_pre_cos_fab_co_avg_con_dtls b, wo_pre_cost_fabric_cost_dtls a
		    WHERE a.id = b.pre_cost_fabric_cost_dtls_id
		    AND b.po_break_down_id in (".str_replace("'","",$po_number_id).")
		    GROUP BY b.po_break_down_id,a.body_part_id");
	 $con_per_pcs=array();
     foreach($sql_sewing as $row_sew)
      {
	    $con_avg[$row_sew[csf('po_break_down_id')]]+= str_replace("'","",$row_sew[csf("conjunction")]);///str_replace("'","",$row_sew[csf("pcs")]);
	    $con_per_pcs[$row_sew[csf('po_break_down_id')]]=$con_avg[$row_sew[csf('po_break_down_id')]][$row_sew[csf('color_number_id')]]/str_replace("'","",$row_sew[csf("pcs")]);
		
     }
	   }
	   
	   
if($variable_cutting==2 || $variable_cutting==3)

      {
		    $pro_date_sql=sql_select ("SELECT distinct a.id,a.job_no_mst,a.po_number,
		    sum(d.order_quantity) as order_qty,sum(d.plan_cut_qnty) as plan_qty,b.buyer_name,b.style_ref_no as style,b.job_no_prefix_num,SUBSTRING_INDEX(b.insert_date, '-', 1) as year ,d.color_number_id,d.id as color_size_break_id
		    from 
		    wo_po_break_down a,wo_po_details_master b, wo_po_color_size_breakdown d
		    where 
		    a.job_no_mst=b.job_no and a.id=d.po_break_down_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 $company_name $buyer_name $style_cond $order_cond $job_cond   group by d.po_break_down_id,d.color_number_id");
			
					
	  foreach($pro_date_sql as $row)
		{
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['id']=$row[csf('id')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['job_no']=$row[csf('job_no_mst')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['po_number']=$row[csf('po_number')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['po_quantity']=$row[csf('order_qty')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['plan_qty']=$row[csf('plan_qty')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['buyer_name']=$row[csf('buyer_name')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['style']=$row[csf('style')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['job_prifix']=$row[csf('job_no_prefix_num')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['year']=$row[csf('year')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['color_id']=$row[csf('color_number_id')];
			$po_number_data[$row[csf('id')]][$row[csf('color_number_id')]]['color_size_break_id']=$row[csf('color_size_break_id')];
			$po_number_id[]=$row[csf('id')];
		}
	  $po_number_id=implode(",",array_unique($po_number_id));
	  $production_mst_sql=sql_select("select a.po_break_down_id,c.color_number_id,
		   sum(CASE WHEN b.production_type =1 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cutting_qnty,
		   sum(CASE WHEN b.production_type =1 and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cutting_qnty_pre,    
		   sum(CASE WHEN b.production_type =2 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printing_qnty,
		   sum(CASE WHEN b.production_type =2 and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printing_qnty_pre ,
		   sum(CASE WHEN b.production_type =3 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printreceived_qnty,
		   sum(CASE WHEN b.production_type =3 and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS printreceived_qnty_pre,
		   sum(CASE WHEN b.production_type =9 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cut_delivery_qnty,
		   sum(CASE WHEN b.production_type =9 and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS cut_delivery_qnty_pre,
		   sum(CASE WHEN b.production_type =4 and a.production_date=".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS sewingin_qnty,
		   sum(CASE WHEN b.production_type =4 and a.production_date<".$txt_date_from." THEN b.production_qnty ELSE 0 END) AS sewingin_qnty_pre 
		   from  pro_garments_production_dtls b,pro_garments_production_mst a,wo_po_color_size_breakdown c where a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.po_break_down_id=c.po_break_down_id and a.po_break_down_id in (".str_replace("'","",$po_number_id).") group by a.po_break_down_id,c.color_number_id");
		   
		
		   
  foreach($production_mst_sql as $val)
	    {
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['cutting_qnty']=$val[csf('cutting_qnty')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['printing_qnty']=$val[csf('printing_qnty')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['printreceived_qnty']=$val[csf('printreceived_qnty')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['sewingin_qnty']=$val[csf('sewingin_qnty')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['cut_delivery_qnty']=$val[csf('cut_delivery_qnty')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['cutting_qnty_pre']=$val[csf('cutting_qnty_pre')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['printing_qnty_pre']=$val[csf('printing_qnty_pre')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['printreceived_qnty_pre']=$val[csf('printreceived_qnty_pre')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['sewingin_qnty_pre']=$val[csf('sewingin_qnty_pre')];
		$production_data_arr[$val[csf('po_break_down_id')]][$val[csf('color_number_id')]]['cut_delivery_qnty_pre']=$val[csf('cut_delivery_qnty_pre')]; 
		$po_number_gmt[]=$val[csf('po_break_down_id')];	
	  }



  $sql_sewing=sql_select("SELECT b.po_break_down_id,b.color_number_id,sum( b.cons )/count( b.color_number_id ) AS conjunction,pcs
						   FROM wo_pre_cos_fab_co_avg_con_dtls b, wo_pre_cost_fabric_cost_dtls a
						   WHERE a.id = b.pre_cost_fabric_cost_dtls_id
						   AND b.po_break_down_id in (".str_replace("'","",$po_number_id).")
						   GROUP BY b.po_break_down_id,b.color_number_id,a.body_part_id");
	 $con_per_pcs=array();
     foreach($sql_sewing as $row_sew)
      {
	    $con_avg[$row_sew[csf('po_break_down_id')]][$row_sew[csf('color_number_id')]]+= str_replace("'","",$row_sew[csf("conjunction")]);///str_replace("'","",$row_sew[csf("pcs")]);
	    $con_per_pcs[$row_sew[csf('po_break_down_id')]][$row_sew[csf('color_number_id')]]=$con_avg[$row_sew[csf('po_break_down_id')]][$row_sew[csf('color_number_id')]]/str_replace("'","",$row_sew[csf("pcs")]);
     }
				   
   
       
   $sql_fabric_qty=sql_select( "SELECT a.po_breakdown_id,a.color_id,
		  sum(CASE WHEN b.transaction_date = ".$txt_date_from." AND a.trans_type =2  AND a.entry_form =18 and b.item_category=2 THEN a.quantity ELSE 0 END ) AS fabric_qty,
		  sum(CASE WHEN b.transaction_date <".$txt_date_from." AND a.trans_type =2 and b.item_category=2  AND a.entry_form =18 THEN a.quantity ELSE 0 END ) AS fabric_qty_pre,
		  sum(CASE WHEN b.transaction_date = ".$txt_date_from." AND a.trans_type =5  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_in_qty,
		  sum(CASE WHEN b.transaction_date <".$txt_date_from." AND a.trans_type =5  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_in_pre,
		  sum(CASE WHEN b.transaction_date = ".$txt_date_from." AND a.trans_type =6  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_out_qty,
		  sum(CASE WHEN b.transaction_date <".$txt_date_from." AND a.trans_type =6  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_out_pre
		  FROM order_wise_pro_details a,inv_transaction b
		  WHERE a.trans_id = b.id 
		  and b.status_active=1 and a.entry_form in(18,15) and  b.is_deleted=0 AND a.po_breakdown_id in (".str_replace("'","",$po_number_id).") group by a.po_breakdown_id,a.color_id");
				$fabric_pre=array();
				$fabric_today=array();  
				$total_fabric=array();
				$fabric_balance=array();
				$po_id_fab=array();
			foreach($sql_fabric_qty as $value)
			  {
				 $fabric_pre[$value[csf("po_breakdown_id")]][$value[csf("color_id")]]=$value[csf("fabric_qty_pre")];
				 $fabric_today[$value[csf("po_breakdown_id")]][$value[csf("color_id")]]=$value[csf("fabric_qty")]+$value[csf("trans_in_pre")]-$value[csf("trans_out_pre")];	
				 $total_fabric=$fabric_pre+$fabric_today;	
				// $fabric_balance=$total_fabric-$fabric_qty;	
		        $po_id_fab[]=$value[csf("po_breakdown_id")];
			  }	
		
		
   
 			   
   
 }
 
 

   $sew_line_arr=array();
   $sql_line=sql_select("select group_concat(distinct b.line_name) as line_name,a.po_break_down_id from pro_garments_production_mst a,lib_sewing_line b where a.sewing_line=b.id and a.po_break_down_id in (".str_replace("'","",$po_number_id).") and a.production_type='4'  group by a.po_break_down_id");
   foreach($sql_line as $row_sew)
	 {
		$sew_line_arr[$row_sew[csf('po_break_down_id')]]['line']= $row_sew[csf('line_name')];
		 
	 }
   ob_start();
   
 ?>
  <fieldset style="width:3250px;">
        	    <table width="1880"  cellspacing="0"   >
                    <tr class="form_caption" style="border:none;">
                            <td colspan="29" align="center" style="border:none;font-size:14px; font-weight:bold" > Daily Cutting And Input Inhand Report</td>
                     </tr>
                    <tr style="border:none;">
                            <td colspan="29" align="center" style="border:none; font-size:16px; font-weight:bold">
                                Company Name:<?php echo $company_library[str_replace("'","",$cbo_company_name)]; ?>                                
                            </td>
                      </tr>
                      <tr style="border:none;">
                            <td colspan="29" align="center" style="border:none;font-size:12px; font-weight:bold">
                            	<?php echo "Date: ". str_replace("'","",$txt_date_from) ;?>
                            </td>
                      </tr>
                </table>
             <br />	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="3220" class="rpt_table">
                <thead>
                	<tr>
                        <th width="40" rowspan="2">SL</th>
                        <th width="100" rowspan="2">Buyer</th>
                        <th width="60" rowspan="2">Job No</th>
                        <th width="50" rowspan="2">Year</th>
                        <th width="100" rowspan="2">Order No</th>
                        <th width="70" rowspan="2">Cut. Inch.</th>
                        <th width="70" rowspan="2">Tod</th>
                        <th width="100" rowspan="2">Style</th>
                        <th width="100" rowspan="2">Color</th>
                        <th width="70" rowspan="2">Order Qty.</th>
                        <th width="70" rowspan="2">Plan Cut Qty.</th>
                        <th width="70" rowspan="2">Fabric Qty</th>
                        <th width="320" colspan="4">Fabric Receive Qty.</th>
                        <th width="320" colspan="4">Cutting</th>
                        <th width="320" colspan="4">Delivery to Embellishment</th>
                        <th width="240" colspan="3">Receive from Embel</th>
                        <th width="80" rowspan="2">WIP in Print</th>
                        <th width="320" colspan="4">Receive From Cut</th>
                        <th width="400" colspan="5">Input</th>
                        <th width="60" rowspan="2">Inhand</th>
                        <th width="60" rowspan="2">Ready For Input</th>
                        <th width="60" rowspan="2">Line No</th>
                        <th  rowspan="2">Remarks</th>
                    </tr>
                    <tr>
                        <th width="80" rowspan="2">Prev.</th>
                        <th width="80" rowspan="2">Today </th>
                        <th width="80" rowspan="2">Total </th>
                        <th width="80" rowspan="2">Bal.</th>
                        <th width="80" rowspan="2">Prev.</th>
                        <th width="80" rowspan="2">Today </th>
                        <th width="80" rowspan="2">Total </th>
                        <th width="80" rowspan="2">Bal.</th>
                        <th width="80" rowspan="2">Prev.</th>
                        <th width="80" rowspan="2">Today </th>
                        <th width="80" rowspan="2">Total </th>
                        <th width="80" rowspan="2">Bal.</th>
                        <th width="80" rowspan="2">Prev.</th>
                        <th width="80" rowspan="2">Today </th>
                        <th width="80" rowspan="2">Total </th>
                        <th width="80" rowspan="2">Prev.</th>
                        <th width="80" rowspan="2">Today </th>
                        <th width="80" rowspan="2">Total </th>
                        <th width="80" rowspan="2">Bal.</th>
                        <th width="80" rowspan="2">Prev.</th>
                        <th width="80" rowspan="2">Today</th>
                        <th width="80" rowspan="2">Total</th>
                        <th width="80" rowspan="2">%</th>
                        <th width="80" rowspan="2">Balance</th>
                    </tr>
                </thead>
            </table>
             <div style="max-height:425px; overflow-y:scroll; width:3250px; margin-left:15px" id="scroll_body">
                    <table cellspacing="0" border="1" class="rpt_table"  width="3220" rules="all" id="table_body" >
                    <?php
                      $total_cut=0;
                      $total_print_iss=0;
                      $total_print_rec=0;
                      $total_sew_input=0;
                      $total_sew_out=0;
					  $total_delivery_cut=0;
                      $cutting_balance=0;
					  $print_issue_balance=0;
					  $print_rec_balance=0;
				  	  $deliv_cut_bal=0;
					  $total_sew_input_balance=0;
					  $input_percentage=0;
					  $inhand=0;
					  $buyer_total_order=0;
					  $buyer_total_plan=0;
					  $buyer_total_fabric_qty=0;
					  $buyer_total_fabric_pre=0;
					  $buyer_fabric_total=0;
					  $buyer_fabric_today_total=0;
					  $buyer_fabric_bal=0;
					  $buyer_pre_cut=0;
					  $buyer_today_cut=0;
					  $buyer_total_cut=0;
					  $buyer_cutting_balance=0;
					  $buyer_priv_print_iss=0;
					  $buyer_today_print_iss=0;
					  $buyer_print_issue_balance=0;
					  $buyer_priv_print_rec=0;
					  $buyer_today_print_rec=0;
					  $buyer_total_print_rec=0;
					  $buyer_print_rec_balance=0;
					  $buyer_priv_deliv_cut=0;
					  $buyer_today_deliv_cut=0;
					  $buyer_total_delivery_cut=0;
					  $buyer_deliv_cut_bal=0;
					  $buyer_priv_sew=0;
					  $buyer_today_sew=0;
					  $buyer_total_sew_=0;
					  $buyer_total_sew__bal=0;
					  $buyer_inhand=0;
					  $buyer_arr=array();
					  $job_arr=array();
                      $i=1;
if($variable_cutting==2 || $variable_cutting==3)
   {
	 $total_po_arr=array();
	 $total_po_arr=array_unique(array_merge($po_id_fab,$po_number_gmt));
	 $po_number_arr=implode(",",$total_po_arr);
	//var_dump($total_po_arr);die;
	 if($po_number_arr!="") $po_id_cond=" and a.id in (".str_replace("'","",$po_number_arr).")";
	// echo $po_id_cond;die;
/*	 $sql_fabric=sql_select( "SELECT a.po_breakdown_id,c.job_no_mst,d.buyer_name,e.color_number_id, 
				  sum(CASE WHEN b.transaction_date = ".$txt_date_from." AND a.trans_type =2  AND a.entry_form =18 and b.item_category=2 THEN a.quantity ELSE 0 END ) AS fabric_qty,
				  sum(CASE WHEN b.transaction_date <".$txt_date_from." AND a.trans_type =2 and b.item_category=2  AND a.entry_form =18 THEN a.quantity ELSE 0 END ) AS fabric_qty_pre,
				  sum(CASE WHEN b.transaction_date = ".$txt_date_from." AND a.trans_type =5  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_in_qty,
				  sum(CASE WHEN b.transaction_date <".$txt_date_from." AND a.trans_type =5  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_in_pre,
				  sum(CASE WHEN b.transaction_date = ".$txt_date_from." AND a.trans_type =6  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_out_qty,
				  sum(CASE WHEN b.transaction_date <".$txt_date_from." AND a.trans_type =6  AND a.entry_form =15 THEN a.quantity ELSE 0 END ) AS trans_out_pre
				  FROM order_wise_pro_details a,inv_transaction b,wo_po_break_down c,wo_po_details_master d, wo_po_color_size_breakdown e
				  WHERE a.trans_id = b.id and  c.job_no_mst=d.job_no and c.id=e.po_break_down_id  and a.po_breakdown_id=c.id  
				  and b.status_active=1 and a.entry_form in(18,15) and  b.is_deleted=0 $po_id_cond group by d.buyer_name,c.job_no_mst,a.po_breakdown_id,e.color_number_id");*/
				  
	 $sql_fabric=sql_select( "SELECT a.id,a.job_no_mst,b.buyer_name,c.color_number_id 
				  FROM  wo_po_break_down a,wo_po_details_master b, wo_po_color_size_breakdown c
				  WHERE   a.job_no_mst=b.job_no and a.id=c.po_break_down_id   
				  and a.status_active=1 and a.is_deleted=0 $po_id_cond group by b.buyer_name,a.job_no_mst,c.color_number_id ");
				
   }
/*   echo "SELECT a.id,a.job_no_mst,b.buyer_name,c.color_number_id 
				  FROM  wo_po_break_down a,wo_po_details_master b, wo_po_color_size_breakdown c
				  WHERE   a.job_no_mst=b.job_no and a.id=c.po_break_down_id   
				  and a.status_active=1 and a.is_deleted=0 $po_id_cond group by b.buyer_name,a.job_no_mst,c.color_number_id ";die;*/

  /* echo "SELECT a.po_breakdown_id,c.job_no_mst,d.buyer_name,e.color_number_id, 
				  sum(CASE WHEN b.transaction_date = ".$txt_date_from." AND a.trans_type =2  AND a.entry_form =18 and b.item_category=2 THEN a.quantity ELSE 0 END ) AS fabric_qty  FROM order_wise_pro_details a,inv_transaction b,wo_po_break_down c,wo_po_details_master d, wo_po_color_size_breakdown e
				  WHERE a.trans_id = b.id and  c.job_no_mst=d.job_no and c.id=e.po_break_down_id  and a.po_breakdown_id=c.id  
				  and b.status_active=1 and a.entry_form in(18,15) and  b.is_deleted=0 $po_id_cond group by a.po_breakdown_id,e.color_number_id";die;*/
  foreach($sql_fabric as $row_fab)	
	{ 
	
	
	
	   
				
		 if($fabric_today!=0 || $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty']!=0 || $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printing_qnty']!=0 || $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty']!=0 || $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty']!=0 || $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty']!=0)	
		      {
				
//for color and size level*******************************************************************************************************************************************************					   
      
	   if($i!=1)
					  {
					     if(!in_array($po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['job_no'],$job_arr))
							    {
								  
								?>
							      <tr bgcolor="#CCCCCC" id="">
                                        <td width="40"><?php // echo $i;?></td>
                                        <td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                                        <td width="60"></td>
                                        <td width="50"></td>
                                        <td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
                                        <td width="70"></td>
                                        <td width="70"></td>
                                        <td width="100"><strong>Job</strong></td>
                                        <td width="100" ><strong>Total:</strong></td>
                                        <td width="70" align="right"><?php echo $job_total_order; ?></td>
                                        <td width="70" align="right"><?php  echo $job_total_plan; ?></td>
                                        <td width="70" align="right"><?php  echo number_format($job_total_fabric_qty,2); ?></td>
                                        <td width="80" align="right"><?php  echo number_format($job_total_fabric_pre,2); ?></td>
                                        <td width="80" align="right"><?php  echo number_format($job_fabric_today_total,2); ?></td>
                                        <td width="80" align="right"><?php  echo number_format($job_fabric_total,2);?></td>
                                        <td width="80" align="right"><?php  echo number_format($job_fabric_bal,2); ?></td>
                                        <td width="80" align="right"><?php  echo $job_pre_cut; ?></td>
                                        <td width="80" align="right"><?php  echo $job_today_cut; ?></td>
                                        <td width="80" align="right"><?php  echo $job_total_cut; ?></td>
                                        <td width="80" align="right"><?php  echo $job_cutting_balance; ?></td>
                                        <td width="80" align="right"><?php  echo $job_priv_print_iss; ?></td>
                                        <td width="80" align="right"><?php  echo $job_today_print_iss; ?></td>
                                        <td width="80" align="right"><?php  echo $job_print_issue_balance; ?></td>
                                        <td width="80" align="right"><?php  echo $job_print_issue_balance; ?></td>
                                        <td width="80" align="right"><?php  echo $job_priv_print_rec; ?></td>
                                        <td width="80" align="right"><?php  echo $job_today_print_rec; ?></td>
                                        <td width="80" align="right"><?php  echo $job_total_print_rec; ?></td>
                                        <td width="80" align="right"><?php  echo $job_print_rec_balance;; ?></td>
                                        <td width="80" align="right"><?php  echo $job_priv_deliv_cut; ?></td>
                                        <td width="80" align="right"><?php  echo $job_today_deliv_cut; ?></td>
                                        <td width="80" align="right"><?php  echo $job_total_delivery_cut; ?></td>
                                        <td width="80" align="right"><?php  echo $job_deliv_cut_bal; ?></td>
                                        <td width="80" align="right"><?php  echo $job_priv_sew; ?></td>
                                        <td width="80" align="right"><?php  echo $job_today_sew; ?></td>
                                        <td width="80" align="right"> <?php echo $job_total_sew_input; ?></td>
                                        <td width="80" align="right"><?php //echo $input_percentage; ?></td>
                                        <td width="80" align="right"><?php  echo $job_total_sew__bal; ?></td>
                                        <td width="60" align="right"><?php echo $job_inhand; ?></td>
                                        <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                                        <td width="60" align="right"><?php //echo  $sewing_line ?></td>
                                        <td  align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
					              </tr>
								<?php  
								  $job_total_order=0;
								  $job_total_plan=0;
								  $job_total_fabric_qty=0;
								  $job_total_fabric_pre=0;
								  $job_fabric_total=0;
								  $job_fabric_today_total=0;
								  $job_fabric_bal=0;
								  $job_pre_cut=0;
								  $job_today_cut=0;
								  $job_total_cut=0;
								  $job_cutting_balance=0;
								  $job_priv_print_iss=0;
								  $job_today_print_iss=0;
								  $job_total_print_iss=0;
								  $job_print_issue_balance=0;
								  $job_priv_print_rec=0;
								  $job_today_print_rec=0;
								  $job_total_print_rec=0;
								  $job_print_rec_balance=0;
								  $job_priv_deliv_cut=0;
								  $job_today_deliv_cut=0;
								  $job_total_delivery_cut=0;
								  $job_deliv_cut_bal=0;
								  $job_priv_sew=0;
								  $job_today_sew=0;
								  $job_total_sew_input=0;
								  $job_total_sew__bal=0;
							  }
							
					  }
					
	  
	  
	               if($i!=1)
					  {	
					 if( !in_array($po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['buyer_name'],$buyer_arr))
							{
							?>
							
								<tr bgcolor="#999999" style=" height:15px">
									<td width="40"><?php // echo $i;?></td>
									<td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
									<td width="60"></td>
									<td width="50"></td>
									<td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
									<td width="70"></td>
									<td width="70"></td>
									<td width="100"><strong> Buyer</strong></td>
									<td width="100"><strong>Total:</strong></td>
									<td width="70" align="right"><?php echo $buyer_total_order; ?></td>
									<td width="70" align="right"><?php  echo $buyer_total_plan; ?></td>
									<td width="70" align="right"><?php  echo number_format($buyer_total_fabric_qty,2); ?></td>
									<td width="80" align="right"><?php  echo number_format($buyer_total_fabric_pre,2); ?></td>
									<td width="80" align="right"><?php  echo number_format($buyer_fabric_today_total,2); ?></td>
									<td width="80" align="right"><?php  echo number_format($buyer_fabric_total,2);?></td>
									<td width="80" align="right"><?php  echo number_format($buyer_fabric_bal,2); ?></td>
									<td width="80" align="right"><?php  echo $buyer_pre_cut; ?></td>
									<td width="80" align="right"><?php  echo $buyer_today_cut; ?></td>
									<td width="80" align="right"><?php  echo $buyer_total_cut; ?></td>
									<td width="80" align="right"><?php  echo $buyer_cutting_balance; ?></td>
									<td width="80" align="right"><?php  echo $buyer_priv_print_iss; ?></td>
									<td width="80" align="right"><?php  echo $buyer_today_print_iss; ?></td>
									<td width="80" align="right"><?php  echo $buyer_total_print_iss; ?></td>
									<td width="80" align="right"><?php  echo $buyer_print_issue_balance; ?></td>
									<td width="80" align="right"><?php  echo $buyer_priv_print_rec; ?></td>
									<td width="80" align="right"><?php  echo $buyer_today_print_rec; ?></td>
									<td width="80" align="right"><?php  echo $buyer_total_print_rec; ?></td>
									<td width="80" align="right"><?php  echo $buyer_print_rec_balance;; ?></td>
									<td width="80" align="right"><?php  echo $buyer_priv_deliv_cut; ?></td>
									<td width="80" align="right"><?php  echo $buyer_today_deliv_cut; ?></td>
									<td width="80" align="right"><?php  echo $buyer_total_delivery_cut; ?></td>
									<td width="80" align="right"><?php  echo $buyer_deliv_cut_bal; ?></td>
									<td width="80" align="right"><?php  echo $buyer_priv_sew; ?></td>
									<td width="80" align="right"><?php  echo $buyer_today_sew; ?></td>
									<td width="80" align="right"> <?php echo $buyer_total_sew_; ?></td>
									<td width="80" align="right"><?php //echo $input_percentage; ?></td>
									<td width="80" align="right"><?php  echo $buyer_total_sew_bal; ?></td>
									<td width="60" align="right"><?php echo $buyer_inhand; ?></td>
									<td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
									<td width="60" align="right"><?php //echo  $sewing_line ?></td>
									<td  align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
							  </tr>
							
							<?php 

							  $buyer_total_order=0;
							  $buyer_total_plan=0;
							  $buyer_total_fabric_qty=0;
							  $buyer_total_fabric_pre=0;
							  $buyer_fabric_total=0;
							  $buyer_fabric_today_total=0;
							  $buyer_fabric_bal=0;
							  $buyer_pre_cut=0;
							  $buyer_today_cut=0;
							  $buyer_total_cut=0;
							  $buyer_cutting_balance=0;
							  $buyer_priv_print_iss=0;
							  $buyer_today_print_iss=0;
							  $buyer_total_print_iss=0;
							  $buyer_print_issue_balance=0;
							  $buyer_priv_print_rec=0;
							  $buyer_today_print_rec=0;
							  $buyer_total_print_rec=0;
							  $buyer_print_rec_balance=0;
							  $buyer_priv_deliv_cut=0;
							  $buyer_today_deliv_cut=0;
							  $buyer_total_delivery_cut=0;
							  $buyer_deliv_cut_bal=0;
							  $buyer_priv_sew=0;
							  $buyer_today_sew=0;
							  $buyer_total_sew_=0;
							  $buyer_total_sew__bal=0;
							  $buyer_inhand=0;
						  }
					} 
		   
	  
				 $fabric_pre=$row_fab[csf("fabric_qty_pre")]+$row_fab[csf("trans_in_qty")]-$row_fab[csf("trans_out_qty")];	
				 $fabric_today=$row_fab[csf("fabric_qty")]+$row_fab[csf("trans_in_pre")]-$row_fab[csf("trans_out_pre")];
				 $total_fabric=$fabric_pre+$fabric_today;	
				 $fabric_balance=$total_fabric-$fabric_qty;		
					
				 if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
								// echo $con_per_pcs[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]];;die; 
				 $fabric_qty=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['plan_qty']*$con_per_pcs[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]];
				// if($today_cut!=0 || $today_print_iss!=0 || $today_print_rec!=0 || $today_deliv_cut!=0 || $today_sew!=0 || $fabric_today!=0 )
				 //  {
					 
				$total_cut=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty']+$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty_pre'];
				$cutting_balance=$total_cut-$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['plan_qty'];
				$total_print_iss=$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty']+$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty_pre'];
				$print_issue_balance=$total_print_iss-$total_cut;
				$total_print_rec=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty']+$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty_pre'];
				$print_rec_balance=$total_print_rec-$total_print_iss;
				$total_delivery_cut=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty']+$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty_pre'];
				$deliv_cut_bal=($total_cut-$total_print_iss+$total_print_rec)-$total_delivery_cut;
				$total_sew_input=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty']+$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty_pre'];
				$total_sew_input_balance=$total_sew_input-$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['plan_qty'];
				$input_percentage=($total_sew_input/$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['po_quantity'])*100;
				$inhand=$total_delivery_cut-$total_sew_input;
						//for job total *******************************************************************************************************
						$job_total_order+=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['po_quantity'];
						$job_total_plan+=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['plan_qty'];
						$job_total_fabric_qty+=$fabric_qty;
						$job_total_fabric_pre+=$fabric_pre;
						$job_fabric_today_total+=$fabric_today;
						$job_fabric_total+=$total_fabric;
						$job_fabric_bal+=$fabric_balance;
						$job_pre_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty_pre'];
						$job_today_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty'];
						$job_total_cut+=$total_cut;
						$job_cutting_balance+=$cutting_balance;
						$job_priv_print_iss+=$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty_pre'];
						$job_today_print_iss+=$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty'];
						$job_total_print_iss+=$total_print_iss;
						$job_print_issue_balance+=$print_issue_balance;
						$job_priv_print_rec+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty_pre'];
						$job_today_print_rec+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty'];
						$job_total_print_rec+=$total_print_rec;
						$job_print_rec_balance+=$print_rec_balance;
						$job_priv_deliv_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty_pre'];
						$job_today_deliv_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty'];
						$job_total_delivery_cut+=$total_delivery_cut;
						$job_deliv_cut_bal+=$deliv_cut_bal;
						$job_priv_sew+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty_pre'];
						$job_today_sew+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty'];
						$job_total_sew_input+=$total_sew_input;
						$job_total_sew_bal+=$total_sew_input_balance;
						$job_inhand+=$inhand;
						//buyer sub total **************************************************************************************************
					    $buyer_total_order+=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['po_quantity'];
						$buyer_total_plan+=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['plan_qty'];
						$buyer_total_fabric_qty+=$fabric_qty;
						$buyer_total_fabric_pre+=$fabric_pre;
						$buyer_fabric_today_total+=$fabric_today;
						$buyer_fabric_total+=$total_fabric;
						$buyer_fabric_bal+=$fabric_balance;
						$buyer_pre_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty_pre'];
						$buyer_today_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty'];
						$buyer_total_cut+=$total_cut;
						$buyer_cutting_balance+=$cutting_balance;
						$buyer_priv_print_iss+=$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty_pre'];
						$buyer_today_print_iss+=$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty'];
						$buyer_total_print_iss+=$total_print_iss;
						$buyer_print_issue_balance+=$job_print_issue_balance;
						$buyer_priv_print_rec+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty_pre'];
						$buyer_today_print_rec+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty'];
						$buyer_total_print_rec+=$total_print_rec;
						$buyer_print_rec_balance+=$print_rec_balance;
						$buyer_priv_deliv_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty_pre'];
						$buyer_today_deliv_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty'];
						$buyer_total_delivery_cut+=$total_delivery_cut;
						$buyer_deliv_cut_bal+=$deliv_cut_bal;
						$buyer_priv_sew+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty_pre'];
						$buyer_today_sew+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty'];
						$buyer_total_sew_+=$total_sew_input;
						$buyer_total_sew_bal+=$total_sew_input_balance;
						$buyer_inhand+=$inhand;
						// for grand total ********************************************************************************************************************
					    $grand_total_order+=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['po_quantity'];
						$grand_total_plan+=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['plan_qty'];
						$grand_total_fabric_qty+=$fabric_qty;
						$grand_total_fabric_pre+=$fabric_pre;
						$grand_fabric_today_total+=$fabric_today;
						$grand_fabric_total+=$total_fabric;
						$grand_fabric_bal+=$fabric_balance;
						$grand_pre_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty_pre'];
						$grand_today_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty'];
						$grand_total_cut+=$total_cut;
						$grand_cutting_balance+=$cutting_balance;
						$grand_priv_print_iss+=$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty_pre'];
						$grand_today_print_iss+=$production_data_arr[$val[csf('po_break_down_id')]][$row_fab[csf('color_number_id')]]['printing_qnty'];
						$grand_total_print_iss+=$total_print_iss;
						$grand_print_issue_balance+=$job_print_issue_balance;
						$grand_priv_print_rec+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty_pre'];
						$grand_today_print_rec+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty'];
						$grand_total_print_rec+=$total_print_rec;
						$grand_print_rec_balance+=$print_rec_balance;
						$grand_priv_deliv_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty_pre'];
						$grand_today_deliv_cut+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty'];
						$grand_total_delivery_cut+=$total_delivery_cut;
						$grand_deliv_cut_bal+=$deliv_cut_bal;
						$grand_priv_sew+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty_pre'];
						$grand_today_sew+=$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty'];
						$grand_total_sew_+=$total_sew_input;
						$grand_total_sew_bal+=$total_sew_input_balance;
						$grand_inhand+=$inhand;
					  
                    ?>
                    	<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_2nd<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_2nd<?php echo $i; ?>">
                            <td width="40"><?php echo $i;?></td>
                            <td width="100"><?php echo $buyer_short_library[$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['buyer_name']]; ?></td>
                            <td width="60" align="right"><?php echo $po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['job_prifix'];?></td>
                            <td width="50" align="right"><?php echo $po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['year'];?></td>
                            <td width="100" align="center"><?php echo $po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['po_number'];?></td>
                            <td width="70"><?php   ?></td>
                            <td width="70"><?php   ?></td>
                            <td width="100"><?php echo $po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['style']; ?></td>
                            <td width="100"><?php echo $color_arr[$row_fab[csf('color_number_id')]]; ?></td>
                            <td width="70" align="right"><?php  echo $po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['po_quantity']; ?></td>
                            <td width="70" align="right"><?php  echo $po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['plan_qty']; ?></td>
                            <td width="70" align="right"><?php  echo number_format($fabric_qty,2); ?></td>
                            <td width="80" align="right"><?php  echo number_format($fabric_pre,2); ?></td>
                            <td width="80" align="right"><?php  echo number_format($fabric_today,2); ?></td>
                            <td width="80" align="right"><?php  echo number_format($total_fabric,2);?></td>
                            <td width="80" align="right"><?php  echo number_format($fabric_balance,2); ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty_pre']; //$production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_id')]]['cutting_qnty_pre']; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cutting_qnty']; ?></td>
                            <td width="80" align="right"><?php  echo $total_cut; ?></td>
                            <td width="80" align="right"><?php  echo $cutting_balance; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printing_qnty_pre']; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printing_qnty']; ?></td>
                            <td width="80" align="right"><?php  echo $total_print_iss; ?></td>
                            <td width="80" align="right"><?php  echo $print_issue_balance; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty_pre']; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['printreceived_qnty']; ?></td>
                            <td width="80" align="right"><?php  echo $total_print_rec; ?></td>
                            <td width="80" align="right"><?php  echo $print_rec_balance;; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty_pre']; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['cut_delivery_qnty']; ?></td>
                            <td width="80" align="right"><?php  echo $total_delivery_cut; ?></td>
                            <td width="80" align="right"><?php  echo $deliv_cut_bal; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty_pre']; ?></td>
                            <td width="80" align="right"><?php  echo $production_data_arr[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['sewingin_qnty']; ?></td>
                            <td width="80" align="right"> <?php echo $total_sew_input; ?></td>
                            <td width="80" align="right"><?php  echo number_format($input_percentage,2); ?></td>
                            <td width="80" align="right"><?php  echo $total_sew_input_balance; ?></td>
                            <td width="60" align="right"><?php  echo $inhand; ?></td>
                            <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                            <td width="60" align="right"><?php  echo  $sew_line_arr[$row_fab[csf('po_breakdown_id')]]['line']; ?></td>
                            <td  align="right"><?php if($total_delivery_cut-$pro_date_sql_row[csf("plan_qty")]>0) echo "Receive Ok"; if($total_sew_input_balance-$pro_date_sql_row[csf("plan_qty")]>0) echo "Input Ok";  ?></td>
                 </tr>
						<?php	
						 $job_arr[]=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['job_no'];
						 $buyer_arr[]=$po_number_data[$row_fab[csf('po_breakdown_id')]][$row_fab[csf('color_number_id')]]['buyer_name'];
				     	 $i++;					
                      // } //end foreach 2nd
			 }
							  
			}
			?>
            </tr>
            <tr bgcolor="#CCCCCC" id="">
                <td width="40"><?php // echo $i;?></td>
                <td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                <td width="60"></td>
                <td width="50"></td>
                <td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
                <td width="70"></td>
                <td width="70"></td>
                <td width="100"><strong>Job</strong></td>
                <td width="100" ><strong>Total:</strong></td>
                <td width="70" align="right"><?php echo $job_total_order; ?></td>
                <td width="70" align="right"><?php  echo $job_total_plan; ?></td>
                <td width="70" align="right"><?php  echo number_format($job_total_fabric_qty,2); ?></td>
                <td width="80" align="right"><?php  echo number_format($job_total_fabric_pre,2); ?></td>
                <td width="80" align="right"><?php  echo number_format($job_fabric_today_total,2); ?></td>
                <td width="80" align="right"><?php  echo number_format($job_fabric_total,2);?></td>
                <td width="80" align="right"><?php  echo number_format($job_fabric_bal,2); ?></td>
                <td width="80" align="right"><?php  echo $job_pre_cut; ?></td>
                <td width="80" align="right"><?php  echo $job_today_cut; ?></td>
                <td width="80" align="right"><?php  echo $job_total_cut; ?></td>
                <td width="80" align="right"><?php  echo $job_cutting_balance; ?></td>
                <td width="80" align="right"><?php  echo $job_priv_print_iss; ?></td>
                <td width="80" align="right"><?php  echo $job_today_print_iss; ?></td>
                <td width="80" align="right"><?php  echo $job_print_issue_balance; ?></td>
                <td width="80" align="right"><?php  echo $job_print_issue_balance; ?></td>
                <td width="80" align="right"><?php  echo $job_priv_print_rec; ?></td>
                <td width="80" align="right"><?php  echo $job_today_print_rec; ?></td>
                <td width="80" align="right"><?php  echo $job_total_print_rec; ?></td>
                <td width="80" align="right"><?php  echo $job_print_rec_balance;; ?></td>
                <td width="80" align="right"><?php  echo $job_priv_deliv_cut; ?></td>
                <td width="80" align="right"><?php  echo $job_today_deliv_cut; ?></td>
                <td width="80" align="right"><?php  echo $job_total_delivery_cut; ?></td>
                <td width="80" align="right"><?php  echo $job_deliv_cut_bal; ?></td>
                <td width="80" align="right"><?php  echo $job_priv_sew; ?></td>
                <td width="80" align="right"><?php  echo $job_today_sew; ?></td>
                <td width="80" align="right"> <?php echo $job_total_sew_input; ?></td>
                <td width="80" align="right"><?php //echo $input_percentage; ?></td>
                <td width="80" align="right"><?php  echo $job_total_sew__bal; ?></td>
                <td width="60" align="right"><?php echo $job_inhand; ?></td>
                <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                <td width="60" align="right"><?php //echo  $sewing_line ?></td>
                <td  align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
          </tr>
          
         <tr bgcolor="#999999" style=" height:15px">
                <td width="40"><?php // echo $i;?></td>
                <td width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></td>
                <td width="60"></td>
                <td width="50"></td>
                <td width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></td>
                <td width="70"></td>
                <td width="70"></td>
                <td width="100"><strong> Buyer</strong></td>
                <td width="100"><strong>Total:</strong></td>
                <td width="70" align="right"><?php echo $buyer_total_order; ?></td>
                <td width="70" align="right"><?php  echo $buyer_total_plan; ?></td>
                <td width="70" align="right"><?php  echo number_format($buyer_total_fabric_qty,2); ?></td>
                <td width="80" align="right"><?php  echo number_format($buyer_total_fabric_pre,2); ?></td>
                <td width="80" align="right"><?php  echo number_format($buyer_fabric_today_total,2); ?></td>
                <td width="80" align="right"><?php  echo number_format($buyer_fabric_total,2);?></td>
                <td width="80" align="right"><?php  echo number_format($buyer_fabric_bal,2); ?></td>
                <td width="80" align="right"><?php  echo $buyer_pre_cut; ?></td>
                <td width="80" align="right"><?php  echo $buyer_today_cut; ?></td>
                <td width="80" align="right"><?php  echo $buyer_total_cut; ?></td>
                <td width="80" align="right"><?php  echo $buyer_cutting_balance; ?></td>
                <td width="80" align="right"><?php  echo $buyer_priv_print_iss; ?></td>
                <td width="80" align="right"><?php  echo $buyer_today_print_iss; ?></td>
                <td width="80" align="right"><?php  echo $buyer_total_print_iss; ?></td>
                <td width="80" align="right"><?php  echo $buyer_print_issue_balance; ?></td>
                <td width="80" align="right"><?php  echo $buyer_priv_print_rec; ?></td>
                <td width="80" align="right"><?php  echo $buyer_today_print_rec; ?></td>
                <td width="80" align="right"><?php  echo $buyer_total_print_rec; ?></td>
                <td width="80" align="right"><?php  echo $buyer_print_rec_balance;; ?></td>
                <td width="80" align="right"><?php  echo $buyer_priv_deliv_cut; ?></td>
                <td width="80" align="right"><?php  echo $buyer_today_deliv_cut; ?></td>
                <td width="80" align="right"><?php  echo $buyer_total_delivery_cut; ?></td>
                <td width="80" align="right"><?php  echo $buyer_deliv_cut_bal; ?></td>
                <td width="80" align="right"><?php  echo $buyer_priv_sew; ?></td>
                <td width="80" align="right"><?php  echo $buyer_today_sew; ?></td>
                <td width="80" align="right"> <?php echo $buyer_total_sew_; ?></td>
                <td width="80" align="right"><?php //echo $input_percentage; ?></td>
                <td width="80" align="right"><?php  echo $buyer_total_sew_bal; ?></td>
                <td width="60" align="right"><?php echo $buyer_inhand; ?></td>
                <td width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></td>
                <td width="60" align="right"><?php //echo  $sewing_line ?></td>
                <td  align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></td>
        </tr>
              
     <tfoot>
             <tr>
                <th width="40"><?php // echo $i;?></th>
                <th width="100"><?php //echo $buyer_short_library[$pro_date_sql_row[csf("buyer_name")]]; ?></th>
                <th width="60"></td>
                <th width="50"></td>
                <th width="100"><?php //echo $pro_date_sql_row[csf("po_number")];?></th>
                <th width="70"></th>
                <th width="70"></th>
                <th width="100"> <strong>Grand</strong></th>
                <th width="100"><strong>Total:</strong></th>
                <th width="70" align="right"><?php echo $grand_total_order; ?></th>
                <th width="70" align="right"><?php  echo $grand_total_plan; ?></th>
                <th width="70" align="right"><?php  echo number_format($grand_total_fabric_qty,2); ?></th>
                <th width="80" align="right"><?php  echo number_format($grand_total_fabric_pre,2); ?></th>
                <th width="80" align="right"><?php  echo number_format($grand_fabric_today_total,2); ?></th>
                <th width="80" align="right"><?php  echo number_format($grand_fabric_total,2);?></th>
                <th width="80" align="right"><?php  echo number_format($grand_fabric_bal,2); ?></th>
                <th width="80" align="right"><?php  echo $grand_pre_cut; ?></th>
                <th width="80" align="right"><?php  echo $grand_today_cut; ?></th>
                <th width="80" align="right"><?php  echo $grand_total_cut; ?></th>
                <th width="80" align="right"><?php  echo $grand_cutting_balance; ?></th>
                <th width="80" align="right"><?php  echo $grand_priv_print_iss; ?></th>
                <th width="80" align="right"><?php  echo $grand_today_print_iss; ?></th>
                <th width="80" align="right"><?php  echo $grand_total_print_iss; ?></th>
                <th width="80" align="right"><?php  echo $grand_print_issue_balance; ?></th>
                <th width="80" align="right"><?php  echo $grand_priv_print_rec; ?></th>
                <th width="80" align="right"><?php  echo $grand_today_print_rec; ?></th>
                <th width="80" align="right"><?php  echo $grand_total_print_rec; ?></th>
                <th width="80" align="right"><?php  echo $grand_print_rec_balance; ?></th>
                <th width="80" align="right"><?php  echo $grand_priv_deliv_cut; ?></th>
                <th width="80" align="right"><?php  echo $grand_today_deliv_cut; ?></th>
                <th width="80" align="right"><?php  echo $grand_total_delivery_cut; ?></th>
                <th width="80" align="right"><?php  echo $grand_deliv_cut_bal; ?></th>
                <th width="80" align="right"><?php  echo $grand_priv_sew; ?></th>
                <th width="80" align="right"><?php  echo $grand_today_sew; ?></th>
                <th width="80" align="right"><?php echo $grand_total_sew_; ?></th>
                <th width="80" align="right"><?php //echo $input_percentage; ?></th>
                <th width="80" align="right"><?php  echo $grand_total_sew_bal; ?></th>
                <th width="60" align="right"><?php echo $grand_inhand; ?></th>
                <th width="60" align="right"><?php // echo $row[csf("iron_qnty")]; ?></th>
                <th width="60" align="right"><?php //echo  $sewing_line ?></th>
                <th  align="right"><?php //$total_iron+=$row[csf("iron_qnty")]; echo $row[csf("iron_qnty")]; ?></th>
         </tr> 
  </tfoot>
								    
  </table> 
           </div>     
  	</div>
 </fieldset>
<?php

foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
		}
		?>
        
        

 <?php	

?>