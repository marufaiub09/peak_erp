﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//------------------------------------------------------------------------------------------------------
if ($action=="load_drop_down_supplier")
{
	echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) and FIND_IN_SET($data,tag_company) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	exit();  	 
}

if ($action=="load_details_container") //Yarn
{  
   	if($data==2) // independent
	{
		$i=1;
 	?>
	<div style="width:1050px;">
        <table cellspacing="0" width="100%" class="rpt_table" id="tbl_details" >
            <thead>
                <tr>
                    <th class="must_entry_caption">Color</th>
                    <th class="must_entry_caption">Count</th>
                    <th>Comp 1</th>
                    <th>%</th>
                    <th>Comp 2</th>
                    <th>%</th>
                    <th class="must_entry_caption">Yarn Type</th>
                    <th class="must_entry_caption">UOM</th>
                    <th class="must_entry_caption">Quantity</th>
                    <th class="must_entry_caption">Rate</th>
                    <th class="must_entry_caption">Value</th>
                    <th>Action</th>
                </tr>    
            </thead>
            <tbody>
                <tr class="general" id="<?php echo $i;?>">
                    <td width="80">
                        <input type="hidden" name="txt_po_<?php echo $i;?>" id="txt_po_<?php echo $i;?>" class="text_boxes" style="width:80px" value="" disabled readonly />
                        <input type="hidden" name="txt_po_brakdown_id_<?php echo $i;?>" id="txt_po_brakdown_id_<?php echo $i;?>" value="" disabled readonly />
                        <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value="<?php echo $row[csf("id")]; ?>" />
                        <input type="text" name="txt_color_<?php echo $i;?>" id="txt_color_<?php echo $i;?>" class="text_boxes" onKeyPress="colorName(<?php echo $i;?>)" onKeyUp="fn_copy_color(<?php echo $i;?>)" style="width:80px"/>
                        <input type="hidden" id="hidden_colorID_<?php echo $i;?>" value=""  />
                    </td>
                    <td width="80">
                        <?php
                            echo create_drop_down( "cbocount_".$i, 80, "select id,yarn_count from lib_yarn_count where is_deleted = 0 AND status_active = 1 ORDER BY yarn_count ASC","id,yarn_count", 1, "Select", 0, "",0 );
                        ?>
                    </td>
                    <td width="100">
                        <?php  echo create_drop_down( "cbocompone_".$i, 100, $composition,"", 1, "-- Select --", 0, "",0,"" ); ?></td>
                    <td width="50"><input type="text" id="percentone_<?php echo $i; ?>"  name="percentone_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_one')" value="" /></td>
                    <td width="100"><?php  echo create_drop_down( "cbocomptwo_".$i, 100, $composition,"", 1, "-- Select --", 0, "control_composition($i,this.id,'percent_two')",0,"" ); ?></td>
                    <td width="50"><input type="text" id="percenttwo_<?php echo $i; ?>"  name="percenttwo_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_two')" value=""  /></td>
                    <td width="80">
                        <?php
                            echo create_drop_down( "cbotype_".$i, 80, $yarn_type,"", 1, "Select", 0, "",0 );
                        ?> 
                    </td> 
                    <td width="50">
                        <?php
                            echo create_drop_down( "cbo_uom_".$i, 70, $unit_of_measurement,"", 1, "Select", 12, "",1 );
                        ?>      
                    </td>
                    <td width="50">
                        <input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;"/>
                    </td>
                    <td width="50">
                        <input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" />
                    </td>
                    <td width="80">
                        <input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" readonly />
                    </td>
                    <td width="80">
                         <input type="button" id="increaserow_<?php echo $i;?>" style="width:30px" class="formbutton" value="+" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'increase');" />
                         <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
                    </td> 
                </tr>
            </tbody>
        </table>
        <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
	</div>	
	<?php
		exit();
	}
	else //blank container 
	{
		echo "";
		exit();
	}
}

if ($action=="append_load_details_container")//Yarn details append table row
{   
	$i=$data;
	?>
    <tr class="general" id="<?php echo $i;?>">
        <td width="80">
            <input type="hidden" name="txt_po_<?php echo $i;?>" id="txt_po_<?php echo $i;?>" class="text_boxes" style="width:80px" value="" disabled readonly />
            <input type="hidden" name="txt_po_brakdown_id_<?php echo $i;?>" id="txt_po_brakdown_id_<?php echo $i;?>" value="" disabled readonly />
            <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value="<?php echo $row[csf("id")]; ?>" />
            
            <input type="text" name="txt_color_<?php echo $i;?>" id="txt_color_<?php echo $i;?>" class="text_boxes" onKeyPress="colorName(<?php echo $i;?>)" onKeyUp="fn_copy_color(<?php echo $i;?>)" style="width:80px"/>
            <input type="hidden" id="hidden_colorID_<?php echo $i;?>" value=""  />
        </td>
        <td width="80">
            <?php
                echo create_drop_down( "cbocount_".$i, 80, "select id,yarn_count from lib_yarn_count where is_deleted = 0 AND status_active = 1 ORDER BY yarn_count ASC","id,yarn_count", 1, "Select", 0, "",0 );
            ?>
        </td>
        <td width="100">
            <?php  echo create_drop_down( "cbocompone_".$i, 100, $composition,"", 1, "-- Select --", $row[csf("copm_one_id")], "",$disabled,"" ); ?></td>
        <td width="50"><input type="text" id="percentone_<?php echo $i; ?>"  name="percentone_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_one')" value="<?php echo $row[csf("percent_one")];  ?>" <?php if($disabled==0){echo "";}else{echo "disabled";}?>  /></td>
        <td width="100"><?php  echo create_drop_down( "cbocomptwo_".$i, 100, $composition,"", 1, "-- Select --", $row[csf("copm_two_id")], "control_composition($i,this.id,'percent_two')",$disabled,"" ); ?></td>
        <td width="50"><input type="text" id="percenttwo_<?php echo $i; ?>"  name="percenttwo_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_two')" value="<?php echo $row[csf("percent_two")];  ?>" <?php if($disabled==0){echo "";}else{echo "disabled";}?>  /></td>
        <td width="80">
            <?php
                echo create_drop_down( "cbotype_".$i, 80, $yarn_type,"", 1, "Select", 0, "",0 );
            ?> 
        </td> 
        <td width="50">
            <?php
                echo create_drop_down( "cbo_uom_".$i, 70, $unit_of_measurement,"", 1, "Select", 12, "",1 );
            ?>      
        </td>
        <td width="50">
            <input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;"/>
        </td>
        <td width="50">
            <input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" />
        </td>
        <td width="80">
            <input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" readonly />
        </td>
        <td width="80">
             <input type="button" id="increaserow_<?php echo $i;?>" style="width:30px" class="formbutton" value="+" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'increase');" />
             <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
        </td> 
    </tr>
	<?php
	exit();
}

// buyer order popoup here
if($action=="order_popup")
{
	extract($_REQUEST); 
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
	<script>
		$(document).ready(function(e) {
            $("#txt_search_common").focus();
        });
		function search_populate(str)
		{
			//alert(str); 
			if(str==0) 
			{		
				document.getElementById('search_by_th_up').innerHTML="Order No";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';		 
			}
			else if(str==1) 
			{
				document.getElementById('search_by_th_up').innerHTML="Style Ref. Number";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';
			}
			else //if(str==2)
			{
				var buyer_name = '<option value="0">--- Select Buyer ---</option>';
				<?php 
				$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
				foreach($buyer_arr as $key=>$val)
				{
					echo "buyer_name += '<option value=\"$key\">".($val)."</option>';";
				} 
				?>
				document.getElementById('search_by_th_up').innerHTML="Select Buyer Name";
				document.getElementById('search_by_td').innerHTML='<select	name="txt_search_common" style="width:230px " class="combo_boxes" id="txt_search_common">'+ buyer_name +'</select>';
			}																																													
		}
		
	var selected_id = new Array;
	var selected_name = new Array;
	var selected_job = new Array;
	
	function check_all_data()
	{
		var tbl_row_count = document.getElementById( 'table_body' ).rows.length;
		tbl_row_count = tbl_row_count - 1;
		//alert(tbl_row_count);return;
		for( var i = 1; i <= tbl_row_count; i++ ) {
			js_set_value( i );
		}
	}
	
	function toggle( x, origColor ){
		var newColor = 'yellow';
		if ( x.style ) { 
			x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
		}
	}
	
	function js_set_value( str ) 
	{
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
				selected_job.push( $('#txt_individual_job' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
 					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
				selected_job.splice( i, 1 );
			}
			var id =''; var name =''; var job = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
				job += ""+selected_job[i] + ",";
			}
			id 		= id.substr( 0, id.length - 1 );
			name 	= name.substr( 0, name.length - 1 );
			job 	= job.substr( 0, job.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
			$('#txt_selected_job').val( job );
	}
	
	function reset_hidden()
	{
		if($("#txt_selected").val()=="")
		{
			$("#txt_selected").val('');
			$("#txt_selected_id").val('');
			$("#txt_selected_job").val(''); 			
 		}
		else
		{
			var selectID = $('#txt_selected_id').val().split(",");
			var selectName = $('#txt_selected').val().split(",");
			var selectJob = $('#txt_selected_job').val().split(",");
			for(var i=0;i<selectID.length;i++)
			{
				selected_id.push( selectID[i] );
				selected_name.push( selectName[i] );
				selected_job.push( selectJob[i] );
			}
		}
	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
    <table width="800" cellspacing="0" cellpadding="0" class="rpt_table" border="1" rules="all" align="center">
        <thead>
            <th width="130">Search By</th>
            <th width="180" align="center" id="search_by_th_up">Enter Order Number</th>
            <th width="200">Date Range</th>
            <th width="80"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
        </thead>
        <tr class="general">
            <td width="130">  
				<?php 
                    $searchby_arr=array(0=>"Order No",1=>"Style Ref. Number",2=>"Buyer Name");
                    echo create_drop_down( "txt_search_by", 130, $searchby_arr,"", 1, "-- Select Sample --", $selected, "search_populate(this.value)",0 );
                ?>
            </td>
            <td width="180" align="center" id="search_by_td">				
                <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" onKeyDown="if (event.keyCode == 13) document.getElementById('btn_show').click()" />			
            </td>
            <td align="center">
                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px"> To
                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
            </td> 
            <td align="center">
                <input type="button" name="btn_show" id="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+'<?php echo $garments_nature; ?>'+'_'+'<?php echo $txt_buyer_po; ?>', 'create_po_search_list_view', 'search_div', 'yarn_work_order_controller', 'setFilterGrid(\'table_body\',-1)');reset_hidden()" style="width:100px;" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center" height="40" valign="middle">
                <?php echo load_month_buttons(1);  ?>
            </td>
        </tr>
    </table>
    <div style="margin-top:5px" id="search_div"></div>    
    <table width="800" cellspacing="0" cellpadding="0" style="border:none" align="center">
        <tr>
            <td align="center" height="30" valign="bottom">
                <div style="width:100%"> 
                    <div style="width:50%; float:left" align="left">
                        <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                    </div>
                    <div style="width:50%; float:left" align="left">
                        <input type="hidden" id="txt_selected_id" value="<?php echo $txt_buyer_po; ?>"  /> <!--po break down id here -->
                        <input type="hidden" id="txt_selected" value="<?php echo $txt_buyer_po_no; ?>"  /> <!--po number here -->
                        <input type="hidden" id="txt_selected_job" value="<?php echo $txt_job_selected; ?>"  /> <!--job number here -->
                        <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                    </div>
                </div>
            </td>
        </tr>
    </table> 
    </form>
    </div>
    </body>           
    <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?php
	exit();
}

if($action=="create_po_search_list_view")
{
 	extract($_REQUEST); 
	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	$garments_nature = $ex_data[5];
	$txt_buyer_po = $ex_data[6];
	//and a.garments_nature=$garments_nature
			
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==0)
			$sql_cond = " and b.po_number like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==1)
			$sql_cond = " and a.style_ref_no like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==2)
			$sql_cond = " and a.buyer_name=trim('$txt_search_common')";		
 	}
	if($txt_date_from!="" || $txt_date_to!="") $sql_cond .= " and b.shipment_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if(trim($company)!="") $sql_cond .= " and a.company_name='$company'";
	if(trim($txt_buyer_po)!="") $buyer_po_arr = explode(",",$txt_buyer_po);
 	
 	$sql = "select b.id, a.order_uom, a.buyer_name, a.company_name, a.total_set_qnty, a.set_break_down, a.job_no_prefix_num, a.job_no, a.style_ref_no, year(a.insert_date) as year, b.shipment_date, b.po_number, b.po_quantity
			from wo_po_details_master a, wo_po_break_down b 
			where a.job_no = b.job_no_mst and a.status_active=1 and  a.is_deleted=0 $sql_cond"; //and a.garments_nature=$garments_nature
	//echo $sql;
	$result = sql_select($sql);
 	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	?>
    <div style="width:920px;">
     	<table cellspacing="0" width="100%" class="rpt_table" border="1" rules="all">
            <thead>
                <th width="50">SL</th>
                <th width="70">Job No</th>
                <th width="80">Job Year</th>
                <th width="150">Order No</th>
                <th width="130">Buyer</th>
                <th width="150">Style</th>
                <th width="120">Order Qnty</th>
                <th>Shipment Date</th>
             </thead>
     	</table>
     </div>
     <div style="width:920px; max-height:230px;overflow-y:scroll;" >	 
        <table cellspacing="0" width="902" class="rpt_table" id="table_body" border="1" rules="all">
			<?php
			$i=1;
            foreach( $result as $row )
            {
                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				if( in_array($row[csf('id')],$buyer_po_arr) ) $bacgroundcolor="background-color: yellow;";else $bacgroundcolor="";
 				?>
                <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i; ?>" style="text-decoration:none;cursor:pointer;<?php echo $bacgroundcolor; ?>" onClick="js_set_value(<?php echo $i;?>)"> 
                    <td width="50" align="center">
                        <?php echo $i; ?>
                        <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $row[csf('id')]; ?>"/>
                        <input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $row[csf('po_number')]; ?>"/>
                        <input type="hidden" name="txt_individual_job" id="txt_individual_job<?php echo $i ?>" value="<?php echo $row[csf('job_no')]; ?>"/>
                    </td>
                    <td width="70" align="center"><?php echo $row[csf("job_no_prefix_num")]; ?></td>
                    <td width="80" align="center"><?php echo $row[csf("year")]; ?></td>
                    <td width="150" align="center"><?php echo $row[csf("po_number")]; ?></td>
                    <td width="130"><?php echo $buyer_arr[$row[csf("buyer_name")]];  ?></td>	
                    <td width="150"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
                    <td width="120" align="right"><?php echo $row[csf("po_quantity")];?> </td>
                    <td align="center"><?php echo change_date_format($row[csf("shipment_date")]);?></td>
                </tr>
				<?php 
				$i++;
             }
   			?>
		</table> 
	</div>     
	<?php	
	exit();	
}

if($action=="show_dtls_listview")
{
	extract($_REQUEST); 
	$data_exp = explode("***",$data);
	$break_down_id = $data_exp[0];
	$job_numbers ="'".implode("','",array_unique(explode(",",$data_exp[1])))."'";

	/*echo $sql="SELECT a.id, b.fabric_cost_dtls_id,c.pre_cost_fabric_cost_dtls_id,a.job_no,b.job_no,c.job_no,c.po_break_down_id, b.count_id,b.copm_one_id,b.percent_one,b.copm_two_id,b.percent_two,b.type_id,b.cons_ratio,b.rate,AVG(c.requirment) as avgrequirment,((AVG(c.requirment)*b.cons_ratio)/100) as avgpercent ,c.requirment,((c.requirment*b.cons_ratio)/100) as percent from wo_pre_cost_fabric_cost_dtls a,wo_pre_cost_fab_yarn_cost_dtls b,wo_pre_cos_fab_co_avg_con_dtls c WHERE  a.job_no=b.job_no and a.job_no=c.job_no and a.id=b.fabric_cost_dtls_id and a.id=c.pre_cost_fabric_cost_dtls_id and a.job_no=".$job_numbers." and c.po_break_down_id in ($break_down_id) and c.requirment!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and  b.is_deleted=0
	group by a.id,c.po_break_down_id,b.count_id,b.copm_one_id,b.type_id 
	order by c.po_break_down_id,b.id";die;*/
	
	$job_composition_wise_arr=array();
 	$yarn_count_arr=return_library_array("select id,yarn_count from lib_yarn_count",'id','yarn_count');
	$plancut_arr=array();
	$ponumber_arr=array();
	
	$plancut_po_number_arr=sql_select("select id,plan_cut,po_number from wo_po_break_down where id in ($break_down_id)");
	foreach($plancut_po_number_arr as $resultRow)
	{
		$plancut_arr[$resultRow[csf("id")]] = $resultRow[csf("plan_cut")];
		$ponumber_arr[$resultRow[csf("id")]] = $resultRow[csf("po_number")];
	}
	 
	$wo_sql=sql_select("select costing_per,job_no from wo_pre_cost_mst where job_no in ($job_numbers) group by job_no"); 
	foreach($wo_sql as $woResult)
	{
	  	$costing_per_qnty=0;
		switch($woResult[csf("costing_per")])
		{
			case 1:
				$costing_per_qnty=1*12;break;
			case 2:
				$costing_per_qnty=1*1;break;
			case 3:
				$costing_per_qnty=2*12;break;
			case 4:
				$costing_per_qnty=3*12;break;
			case 5:
				$costing_per_qnty=4*12;break;
		}
		
		$sql="SELECT b.id as bid, c.id as cid, a.id,b.fabric_cost_dtls_id,c.pre_cost_fabric_cost_dtls_id,a.job_no,b.job_no,c.job_no,c.po_break_down_id, b.count_id,b.copm_one_id,b.percent_one,b.copm_two_id,b.percent_two,b.type_id,b.cons_ratio,b.rate,AVG(c.requirment) as avgrequirment,((AVG(c.requirment)*b.cons_ratio)/100) as avgpercent ,c.requirment,((c.requirment*b.cons_ratio)/100) as percent from wo_pre_cost_fabric_cost_dtls a,wo_pre_cost_fab_yarn_cost_dtls b,wo_pre_cos_fab_co_avg_con_dtls c WHERE  a.job_no=b.job_no and a.job_no=c.job_no and  a.id=b.fabric_cost_dtls_id and a.id=c.pre_cost_fabric_cost_dtls_id and a.job_no='".$woResult[csf("job_no")]."' and c.po_break_down_id in ($break_down_id) and c.requirment!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and  b.is_deleted=0
	group by a.id,c.po_break_down_id,b.count_id,b.copm_one_id,b.type_id 
	order by c.po_break_down_id,b.id";  //fab_nature_id=$cbo_fabric_natu and fabric_source=$cbo_fabric_source and
		//echo $sql;die;
		$yarn_sql_array = sql_select($sql);
		$counter=0;
		if(count($yarn_sql_array)>0)
		{
			$yarn_description_value_po_count_wise=array();
			$unique_composition_rate_arr=array();
			foreach($yarn_sql_array  as $result_yarn)
			{
				//$arr_index=$result_yarn['count_id']."*".$result_yarn['copm_one_id']."*".$result_yarn['percent_one']."*".$result_yarn['type_id']."***".$result_yarn['po_break_down_id'];
				$arr_index=$result_yarn['count_id']."*".$result_yarn['copm_one_id']."*".$result_yarn['percent_one']."*".$result_yarn['copm_two_id']."*".$result_yarn['percent_two']."*".$result_yarn['type_id']."***".$result_yarn['po_break_down_id'];
				if (array_key_exists($arr_index, $yarn_description_value_po_count_wise)) 
				{
					$yarn_description_value_po_count_wise[$arr_index]+=$result_yarn['avgpercent'];
					$job_composition_wise_arr[$arr_index]['rate']+=$result_yarn['rate'];
					$job_composition_wise_arr[$arr_index]['counter']+=1;					
				}
				else
				{					
					$job_composition_wise_arr[$arr_index]['rate']=$result_yarn['rate'];
					$job_composition_wise_arr[$arr_index]['counter']=1;
					$yarn_description_value_po_count_wise[$arr_index]=$result_yarn['avgpercent'];			
				}				
			}//end foreach
			
			//print_r($yarn_description_value_po_count_wise);die;
 			foreach($yarn_description_value_po_count_wise  as $key=>$value)
			{
				$new_key=explode("***",$key);				
				$po_id = $new_key[1];
				$po_qnty = $plancut_arr[$po_id];
				$po_number = $ponumber_arr[$po_id];
				$job_composition_wise_arr[$new_key[0]."***".$new_key[1]]['po_num']=$po_number;
				$job_composition_wise_arr[$new_key[0]."***".$new_key[1]]['po_id']=$po_id;
 				$job_composition_wise_arr[$new_key[0]."***".$new_key[1]]['cons_qnty']=($value/$costing_per_qnty)*$po_qnty;
			}//end foreach
		}// end if condition of count($yarn_sql_array)
	}// end foreach($wo_sql) condition 
	//print_r($job_composition_wise_arr);
	//print_r($unique_composition_rate_arr);
	if( count($job_composition_wise_arr)==0 ){ echo "No Data Found";die;}
	?>
    <div style="width:1050px;" align="left">
    <table cellspacing="0" width="100%" class="rpt_table" id="tbl_details" >
        <thead>
            <tr>
                <th>PO</th>
                <th class="must_entry_caption">Color</th>
                <th>Count</th>
                <th>Comp 1</th>
                <th>%</th>
                <th>Comp 2</th>
                <th>%</th>
                <th>Yarn Type</th>
                <th>UOM</th>
                <th>Quantity</th>
                <th>Rate</th>
                <th>Value</th>
                <th>Action</th>
            </tr>    
        </thead>
    <?php
	$i=1;
	foreach($job_composition_wise_arr as $key=>$val)
	{
		//$yarn_count_arr[$result_yarn['count_id']]."*".$composition[$result_yarn['copm_one_id']]."*".$result_yarn['percent_one']."*".$yarn_type[$result_yarn['type_id']]
		$resultRow=explode("*",$key);
		$resultRow[csf("count_id")]=$resultRow[0];
		$resultRow[csf("copm_one_id")]=$resultRow[1];
		$resultRow[csf("percent_one")]=$resultRow[2];
		$resultRow[csf("copm_two_id")]=$resultRow[3]; // this is for in future
		$resultRow[csf("percent_two")]=$resultRow[4]; // this is for in future
		$resultRow[csf("type_id")]=$resultRow[5];
		
		$resultRow[csf("cons_qnty")]=sprintf ("%.2f", $val['cons_qnty']); 
		$avg_rate = $val['rate']/$val['counter'];
		$resultRow[csf("rate")]=sprintf ("%.2f", $avg_rate); 
		$amount = sprintf ("%.2f", $resultRow[csf("cons_qnty")]*$avg_rate);
		$po_number = $val['po_num'];
		$po_brakdown_id = $val['po_id'];
		?>		
        <tr class="general" id="<?php echo $i;?>">
            <td width="80">
                <input type="text" name="txt_po_<?php echo $i;?>" id="txt_po_<?php echo $i;?>" class="text_boxes" style="width:80px" value="<?php echo $po_number; ?>" disabled readonly />
                <input type="hidden" name="txt_po_brakdown_id_<?php echo $i;?>" id="txt_po_brakdown_id_<?php echo $i;?>" value="<?php echo $po_brakdown_id; ?>" disabled readonly />
                <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value="" />
            </td>
            <td width="80">
                <input type="text" name="txt_color_<?php echo $i;?>" id="txt_color_<?php echo $i;?>" class="text_boxes" onKeyPress="colorName(<?php echo $i;?>)" onKeyUp="fn_copy_color(<?php echo $i;?>)" style="width:80px" value="" />
                <input type="hidden" id="hidden_colorID_<?php echo $i;?>" value="" disabled  />
            </td>
            <td width="80">
                <?php
                    echo create_drop_down( "cbocount_".$i, 80, $yarn_count_arr,"", 1, "Select", $resultRow[csf("count_id")], "",1 );
                ?>
            </td>
            <td width="100">
                <?php  echo create_drop_down( "cbocompone_".$i, 100, $composition,"", 1, "-- Select --", $resultRow[csf("copm_one_id")], "",1,"" ); ?></td>
            <td width="40"><input type="text" id="percentone_<?php echo $i; ?>"  name="percentone_<?php echo $i; ?>" class="text_boxes_numeric" style="width:30px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_one')" value="<?php echo $resultRow[csf("percent_one")];  ?>" <?php if($disabled==0){echo "";}else{echo "disabled";}?> disabled  /></td>
            <td width="100"><?php  echo create_drop_down( "cbocomptwo_".$i, 100, $composition,"", 1, "-- Select --", $resultRow[csf("copm_two_id")], "control_composition($i,this.id,'percent_two')",1,"" ); ?></td>
            <td width="40"><input type="text" id="percenttwo_<?php echo $i; ?>"  name="percenttwo_<?php echo $i; ?>" class="text_boxes_numeric" style="width:30px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_two')" value="<?php echo $resultRow[csf("percent_two")];  ?>" <?php if($disabled==0){echo "";}else{echo "disabled";}?> disabled  /></td>
            <td width="80">
                <?php
                    echo create_drop_down( "cbotype_".$i, 80, $yarn_type,"", 1, "Select", $resultRow[csf("type_id")], "",1 );
                ?> 
            </td> 
            <td width="50">
                <?php
                    echo create_drop_down( "cbo_uom_".$i, 50, $unit_of_measurement,"", 1, "Select", 12, "",1 ); 
                ?>      
            </td>
            <td width="50">
                <input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;" value="<?php echo $resultRow[csf('cons_qnty')];?>" />
            </td>
            <td width="50">
                <input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:50px;" value="<?php echo $resultRow[csf('rate')];?>" />
            </td>
            <td width="80">
                <input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" value="<?php echo sprintf ("%.2f", $amount);?>" readonly />
            </td>
            <td width="80">
                 <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
            </td> 
        </tr>
		<?php 
		$i++;
		} 
		?>
    </table>
    <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
    </div>
	<?php	
	exit();
}

if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Order Search","../../../", 1, 1, $unicode,1);
	extract($_REQUEST);
	$terms_sql = sql_select("select id,terms from lib_terms_condition order by id");
	$terms_name = "";
	foreach( $terms_sql as $result )
	{ 
		$terms_name.= '{value:"'.$result[csf('terms')].'",id:'.$result[csf('id')]."},";
	}
	?>
	<script>
	
		function termsName(rowID)
		{
			$("#termsconditionID_"+rowID).val('');
			 
			$(function() {
				var terms_name = [<?php echo substr($terms_name, 0, -1); ?>]; 
				$("#termscondition_"+rowID).autocomplete({
					source: terms_name,			
					select: function (event, ui) { 
						$("#termscondition_"+rowID).val(ui.item.value); // display the selected text
						$("#termsconditionID_"+rowID).val(ui.item.id); // save selected id to hidden input
					} 
				});
			});
		}
	 
		function add_break_down_tr(i) 
		{
			var row_num=$('#tbl_termcondi_details tr').length-1;
			if (row_num!=i)
			{
				return false;
			}
			else
			{
				i++;
				
				$("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
					$(this).attr({
					  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
					  'name': function(_, name) { return name + i },
					  'value': function(_, value) { return value }              
					});  
				  }).end().appendTo("#tbl_termcondi_details"); 
				$("#tbl_termcondi_details tr:last td:first").html(i);    
				$('#termscondition_'+i).removeAttr("onKeyPress").attr("onKeyPress","termsName("+i+");"); 
				$('#termscondition_'+i).removeAttr("onKeyUp").attr("onKeyUp","termsName("+i+");");   
				$('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
				$('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
				$('#termscondition_'+i).val("");
				$('#termsconditionID_'+i).val("");
			}
		}
	
		function fn_deletebreak_down_tr(rowNo) 
		{   
			var numRow = $('table#tbl_termcondi_details tbody tr').length; 
			if(numRow==rowNo && rowNo!=1)
			{
				$('#tbl_termcondi_details tbody tr:last').remove();
			}
			
		}
	
		function fnc_work_order_terms_condition( operation )
		{
			var row_num=$('#tbl_termcondi_details tr').length-1;
			var data_all="";
			for (var i=1; i<=row_num; i++)
			{			
				if (form_validation('termscondition_'+i,'Term Condition')==false)
				{
					return;
				}
				
				data_all=data_all+get_submitted_data_string('txt_wo_number*termscondition_'+i+'*termsconditionID_'+i,"../../../");
			}
			var data="action=save_update_delete_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
			//freeze_window(operation);
			http.open("POST","yarn_work_order_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_yarn_order_terms_condition_reponse;
		}
	
		function fnc_yarn_order_terms_condition_reponse()
		{
			if(http.readyState == 4) 
			{
				var reponse=trim(http.responseText).split('**');
				if (reponse[0].length>2) reponse[0]=10;
				if(reponse[0]==0 || reponse[0]==1)
				{
					parent.emailwindow.hide();
				}
			}
		}
	
	</script>
	</head>
	<body>
	<div align="center" style="width:100%;" >
    <?php echo load_freeze_divs ("../../../",$permission,1); ?>
	<fieldset>
		   <form id="termscondi_1" autocomplete="off">
				<input type="hidden" id="txt_wo_number" name="txt_wo_number" value="<?php echo str_replace("'","",$txt_wo_number) ?>"/>
				<table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
                    <thead>
                        <tr>
                            <th width="50">Sl</th><th width="530">Terms</th><th ></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $terms_and_conditionID = return_field_value("terms_and_condition","wo_non_order_info_mst","wo_number = $txt_wo_number");  
                    $flag=0;
                    if($terms_and_conditionID=="") 
                        $condd = " is_default=1"; 
                    else
                    { 
                        $condd = " id in ($terms_and_conditionID)";
                        $flag=1;
                    }
                    $data_array=sql_select("select id, terms from lib_terms_condition where $condd order by id");
                    if( count($data_array)>0 )
                    {
                        $i=0;
                        foreach( $data_array as $row )
                        {
                            $i++;
                            ?>
                            <tr id="settr_1" align="center">
                                <td>
									<?php echo $i;?>
                                </td>
                                <td>
                                    <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row['terms']; ?>" onKeyPress="termsName(<?php echo $i;?>)" onKeyUp="termsName(<?php echo $i;?>)" /> 
                                    <input type="hidden" id="termsconditionID_<?php echo $i;?>"  name="termsconditionID_<?php echo $i;?>" value="<?php echo $row['id']; ?>"  readonly />
                                    </td>
                                    <td> 
                                    <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                    <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?>);" />
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <table width="650" cellspacing="0" class="" border="0">
                <tr>
                    <td align="center" height="15" width="100%"> </td>
                </tr>
                <tr>
                    <td align="center" width="100%" class="button_container">
						<?php
                            echo load_submit_buttons( $permission, "fnc_work_order_terms_condition", $flag,0 ,"reset_form('termscondi_1','','','','')",1) ; 
                        ?>
                    </td> 
                </tr>
            </table>
        </form>
    </fieldset>
	</div>
	</body>           
	<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if($action=="save_update_delete_terms_condition")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0) mysql_query("BEGIN");
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 
		$terms_sql = sql_select("select id,terms from lib_terms_condition order by id");
		$terms_name = array();
		foreach( $terms_sql as $result )
		{ 
			$terms_name[$result[csf('terms')]] = $result[csf('id')];
		}
		 
		$id=return_next_id( "id", "lib_terms_condition", 1 );
		$field_array = "id,terms"; $data_array = "";
		$idsArr = "";$j=0;
		for ($i=1;$i<=$total_row;$i++)
		{
			 $termscondition = "termscondition_".$i;
			 $termscondition = $$termscondition;
			 $termsconditionID = "termsconditionID_".$i;
			 $termsconditionID = $$termsconditionID;
			 if(str_replace("'","",$termsconditionID) == "")
			 {
				 $j++;
				 if ($j!=1){ $data_array .=",";}
				 $data_array .="(".$id.",".$termscondition.")";
				 $idsArr[]=$id;
				 $id=$id+1;				 
			 }
			 else
			 {
				 $idsArr[]=$termsconditionID;
			 }
		 }
		
 		if($data_array!="")
		{
			$CondrID=sql_insert("lib_terms_condition",$field_array,$data_array,1);
		}
		
		foreach($idsArr as &$value)
		{
		   $value = str_replace("'","",$value);
		}
		$idsArr = implode(",", $idsArr);
		$rID = sql_update("wo_non_order_info_mst","terms_and_condition","'$idsArr'","wo_number",$txt_wo_number,1);
		//echo $rID;die;		
		check_table_status( $_SESSION['menu_id'],0);		
		if($db_type==0)
		{
			if( $rID && $data_array!="" && $CondrID){
				mysql_query("COMMIT");  
				echo "0**";
			}
			else if($rID && $data_array==""){
				mysql_query("COMMIT");  
				echo "0**";
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**";
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)  // Update Here
	{
		$con = connect();
		if($db_type==0) mysql_query("BEGIN");
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 
		$terms_sql = sql_select("select id,terms from lib_terms_condition order by id");
		$terms_name = array();
		foreach( $terms_sql as $result )
		{ 
			$terms_name[$result[csf('terms')]] = $result[csf('id')];
		}
		 
		$id=return_next_id( "id", "lib_terms_condition", 1 );
		$field_array = "id,terms"; $data_array = "";
		$idsArr = "";$j=0;
		for ($i=1;$i<=$total_row;$i++)
		{
			 $termscondition = "termscondition_".$i;
			 $termscondition = $$termscondition;
			 $termsconditionID = "termsconditionID_".$i;
			 $termsconditionID = $$termsconditionID;
			 if(str_replace("'","",$termsconditionID) == "")
			 {
				 $j++;
				 if ($j!=1){ $data_array .=",";}
				 $data_array .="(".$id.",".$termscondition.")";
				 $idsArr[]=$id;
				 $id=$id+1;				 
			 }
			 else
			 {
				 $idsArr[]=$termsconditionID;
			 }
		 }
		
 		if($data_array!="")
		{
			$CondrID=sql_insert("lib_terms_condition",$field_array,$data_array,1);
		}
		
		foreach($idsArr as &$value)
		{
		   $value = str_replace("'","",$value);
		}
		$idsArr = implode(",", $idsArr);
		$rID = sql_update("wo_non_order_info_mst","terms_and_condition","'$idsArr'","wo_number",$txt_wo_number,1);
		//echo $rID;die;		
		check_table_status( $_SESSION['menu_id'],0);		
		if($db_type==0)
		{
			if( $rID && $data_array!="" && $CondrID){
				mysql_query("COMMIT");  
				echo "0**";
			}
			else if($rID && $data_array==""){
				mysql_query("COMMIT");  
				echo "0**";
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**";
		}
		disconnect($con);
		die;
	}	
}

if($action=="save_update_delete")
{	
	$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  ); 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		// master table netry here---------------------------------------
		$id=return_next_id("id", "wo_non_order_info_mst", 1);
		
		$new_wo_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', '', date("Y",time()), 5, "select wo_number_prefix,wo_number_prefix_num from wo_non_order_info_mst where company_name=$cbo_company_name order by wo_number_prefix_num desc ", "wo_number_prefix", "wo_number_prefix_num" ));
		
		$field_array="id, garments_nature, wo_number_prefix, wo_number_prefix_num, wo_number, company_name, buyer_po, wo_date, supplier_id, attention, buyer_name, style, wo_basis_id, item_category, currency_id, delivery_date, source, pay_mode, inserted_by, insert_date";
		$data_array="(".$id.",".$garments_nature.",'".$new_wo_number[1]."','".$new_wo_number[2]."','".$new_wo_number[0]."',".$cbo_company_name.",".$txt_buyer_po.",".$txt_wo_date.",".$cbo_supplier.",".$txt_attention.",".$txt_buyer_name.",".$txt_style.",".$cbo_wo_basis.",".$cbo_item_category.",".$cbo_currency.",".$txt_delivery_date.",".$cbo_source.",".$cbo_pay_mode.",'".$user_id."','".$pc_date_time."')";
		//echo $field_array."<br>".$data_array;die;
 		$rID=sql_insert("wo_non_order_info_mst",$field_array,$data_array,1);
		
		// details table entry here --------------------------------------
		$total_row = str_replace("'","",$total_row);
		$field_array="id, mst_id, po_breakdown_id, item_id, yarn_count, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_type, color_name, uom, supplier_order_quantity, rate, amount,inserted_by,insert_date";
		$dtlsid=return_next_id("id", "wo_non_order_info_dtls", 1);
		$data_array="";
		for($i=1;$i<=$total_row;$i++)
		{
			$po_breakdown_id = "txt_po_brakdown_id_".$i;
			$cbocount	 = "cbocount_".$i;
			$cbocompone	 = "cbocompone_".$i;
			$percentone	 = "percentone_".$i;
			$cbocomptwo	 = "cbocomptwo_".$i;
			$percenttwo	 = "percenttwo_".$i;
			$cbotype	 = "cbotype_".$i;
			
			if($i>1) $data_array.=",";
			
			$txt_color	 = "hidden_colorID_".$i;
			$txt_color	 = $$txt_color;	 
			if( str_replace("'","",$txt_color)=="" )
			{
				$txt_color_name = "txt_color_".$i;
				$txt_color = return_id( str_replace("'","",$$txt_color_name), $color_library, "lib_color", "id,color_name");
				$color_library[$txt_color]= strtoupper( str_replace("'","",$$txt_color_name) );
			}
			
			$cbo_uom	 = "cbo_uom_".$i;
			$txt_quantity  = "txt_quantity_".$i;
			$txt_rate    = "txt_rate_".$i;
			$txt_amount  = "txt_amount_".$i;
			if($$txt_quantity!="" || $$txt_rate!="")
			{
				$data_array.="(".$dtlsid.",".$id.",".$$po_breakdown_id.",0,".$$cbocount.",".$$cbocompone.",".$$percentone.",".$$cbocomptwo.",".$$percenttwo.",".$$cbotype.",".$txt_color.",".$$cbo_uom.",".$$txt_quantity.",".$$txt_rate.",".$$txt_amount.",'".$user_id."','".$pc_date_time."')";
				$dtlsid=$dtlsid+1;
			}
		}	
		//echo $field_array."<br>".$data_array;die;		
		$dtlsrID=sql_insert("wo_non_order_info_dtls",$field_array,$data_array,1);
		
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "0**".$new_wo_number[0];
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**";
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$rID;
		}
		disconnect($con);
		die;
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();
		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		// master table netry here---------------------------------------
		$mst_id = return_field_value("id","wo_non_order_info_mst","wo_number=$txt_wo_number");		
 		$field_array="buyer_po*wo_date*supplier_id*attention*buyer_name*style*wo_basis_id*item_category*currency_id*delivery_date*source*pay_mode*updated_by*update_date";
		$data_array="".$txt_buyer_po."*".$txt_wo_date."*".$cbo_supplier."*".$txt_attention."*".$txt_buyer_name."*".$txt_style."*".$cbo_wo_basis."*".$cbo_item_category."*".$cbo_currency."*".$txt_delivery_date."*".$cbo_source."*".$cbo_pay_mode."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
		$rID=sql_update("wo_non_order_info_mst",$field_array,$data_array,"id",$mst_id,1);
 		 
 		// details table entry here --------------------------------------
		$total_row = str_replace("'","",$total_row);
		$txt_delete_row = str_replace("'","",$txt_delete_row);
		if($txt_delete_row!="")
		{
			$sql_Delete = execute_query("UPDATE wo_non_order_info_dtls SET status_active=0,is_deleted=1 WHERE id in ($txt_delete_row)");
			//$sql_Delete = sql_multirow_update("wo_non_order_info_dtls","status_active*is_deleted","0*1","id",$txt_delete_row,1);
		}
		
		$field_array_insert="id, mst_id, po_breakdown_id,item_id, yarn_count, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_type, color_name, uom, supplier_order_quantity, rate, amount,inserted_by,insert_date";
		$field_array="po_breakdown_id*item_id*yarn_count*yarn_comp_type1st*yarn_comp_percent1st*yarn_comp_type2nd*yarn_comp_percent2nd*yarn_type*color_name*uom*supplier_order_quantity*rate*amount*inserted_by*insert_date";
		
		$data_array=array();
		$update_ID=array();
 		$data_array_insert="";
		for($i=1;$i<=$total_row;$i++)
		{
			$po_breakdown_id = "txt_po_brakdown_id_".$i;
			$cbocount	 = "cbocount_".$i; 
			$cbocompone	 = "cbocompone_".$i;
			$percentone	 = "percentone_".$i;
			$cbocomptwo	 = "cbocomptwo_".$i;
			$percenttwo	 = "percenttwo_".$i;
			$cbotype	 = "cbotype_".$i; 
			$cbo_uom	 = "cbo_uom_".$i;
			$txt_quantity  = "txt_quantity_".$i;
			$txt_rate    = "txt_rate_".$i;
			$txt_amount  = "txt_amount_".$i;
						
			$dtls_ID  	 = "txt_row_id_".$i;			
			$dtlsID = str_replace("'","",$$dtls_ID);
			
			if($$txt_quantity!="" || $$txt_rate!="") //check blank row  
			{
				$txt_color	 = "hidden_colorID_".$i;
				$txt_color	 = $$txt_color;	 
				if( str_replace("'","",$txt_color)=="" )
				{
					$txt_color_name = "txt_color_".$i;
					$txt_color = return_id( str_replace("'","",$$txt_color_name), $color_library, "lib_color", "id,color_name");
					$color_library[$txt_color]= strtoupper( str_replace("'","",$$txt_color_name) );
				}
							
				if($dtlsID>0) //update
				{
					$update_ID[]=$dtlsID;
					$data_array[$dtlsID]=explode("*",("".$$po_breakdown_id."*0*".$$cbocount."*".$$cbocompone."*".$$percentone."*".$$cbocomptwo."*".$$percenttwo."*".$$cbotype."*".$txt_color."*".$$cbo_uom."*".$$txt_quantity."*".$$txt_rate."*".$$txt_amount."*'".$user_id."'*'".$pc_date_time."'"));
				}
				else // new insert
				{
					$dtlsid=return_next_id("id", "wo_non_order_info_dtls", 1);
					if($data_array_insert!="")$data_array_insert=",";
					$data_array_insert.="(".$dtlsid.",".$mst_id.",".$$po_breakdown_id.",0,".$$cbocount.",".$$cbocompone.",".$$percentone.",".$$cbocomptwo.",".$$percenttwo.",".$$cbotype.",".$txt_color.",".$$cbo_uom.",".$$txt_quantity.",".$$txt_rate.",".$$txt_amount.",'".$user_id."','".$pc_date_time."')";
				}			
			}//end if cond
 		}
		//print_r($data_array);die;
		if($data_array_insert!="")
		{		
			$dtlsrIDI=sql_insert("wo_non_order_info_dtls",$field_array_insert,$data_array_insert,1);
		}
		if(count($update_ID)>0)
		{
			// bulk_update_sql_statement( $table, $id_column, $update_column, $data_values, $id_count )
			$dtlsrID=execute_query(bulk_update_sql_statement("wo_non_order_info_dtls","id",$field_array,$data_array,$update_ID));
		}
		//echo $dtlsrID;die;
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "1**";
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**";
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "1**".$rID;
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		// master table delete here---------------------------------------
		$mst_id = return_field_value("id","wo_non_order_info_mst","wo_number like $txt_wo_number");	
		if($mst_id=="" || $mst_id==0){ echo "15**0"; die;}
 		$rID = sql_update("wo_non_order_info_dtls",'status_active*is_deleted','0*1',"id",$mst_id,1);
		$dtlsrID = sql_update("wo_non_order_info_dtls",'status_active*is_deleted','0*1',"mst_id",$mst_id,1);
		
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "2**";
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**";
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "2**".$rID;
		}
		disconnect($con);
		die;
	}
}

if($action=="wo_popup")
{
	extract($_REQUEST); 
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
	<script>
		$(document).ready(function(e) {
            $("#txt_search_common").focus();
        });
		
		function search_populate(str)
		{
			//alert(str); 
			if(str==1) // wo number
			{		
				document.getElementById('search_by_th_up').innerHTML="Enter WO Number";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:140px " class="text_boxes" id="txt_search_common"	value=""  />';		 
			}
			else if(str==2) // supplier
			{
				var supplier_name = '<option value="0">--- Select ---</option>';
				<?php 
				$supplier_arr=return_library_array( "select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) order by supplier_name",'id','supplier_name');
				foreach($supplier_arr as $key=>$val)
				{
					echo "supplier_name += '<option value=\"$key\">".($val)."</option>';";
				} 
				?>
				document.getElementById('search_by_th_up').innerHTML="Select Supplier Name";
				document.getElementById('search_by_td').innerHTML='<select	name="txt_search_common" style="width:150px " class="combo_boxes" id="txt_search_common">'+ supplier_name +'</select>';
			}	
		}
			
		function js_set_value(wo_number)
		{
			$("#hidden_wo_number").val(wo_number);	
			parent.emailwindow.hide();
		}
			
    </script>
    </head>
    <body>
    <div align="center" style="width:100%;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="800" cellspacing="0" cellpadding="0" class="rpt_table" align="center">
            <tr>
                <td align="center" width="100%">
                    <table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                         <thead>
                            <th width="100">Item Category</th>
                            <th width="130">Search By</th>
                            <th width="150" align="center" id="search_by_th_up">Enter Order Number</th>
                            <th width="200">WO Date Range</th>
                            <th width="80"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
                        </thead>
                        <tr>
                            <td width="100"> 
                            <?php
                                echo create_drop_down( "cboitem_category", 100, $item_category,"", 1, "-- Select --", $itemCategory, "",1);
                            ?> 
                            </td>
                            <td width="130">  
                            <?php 
                            $searchby_arr=array(1=>"WO Number",2=>"Supplier");
                            echo create_drop_down( "txt_search_by", 130, $searchby_arr,"", 0, "-- Select Sample --", $selected, "search_populate(this.value)",0 );
                            ?>
                            </td>
                            <td width="150" align="center" id="search_by_td">				
                                <input type="text" style="width:140px" class="text_boxes"  name="txt_search_common" id="txt_search_common" onKeyDown="if (event.keyCode == 13) document.getElementById('btn_show').click()" />			
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px"> To
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
                            </td> 
                            <td align="center">
                                <input type="button" name="btn_show" id="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cboitem_category').value+'_'+document.getElementById('txt_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $garments_nature; ?>, 'create_wo_search_list_view', 'search_div', 'yarn_work_order_controller', 'setFilterGrid(\'list_view\',-1)');$('#selected_id').val('')" style="width:100px;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td  align="center" height="40" valign="middle">
                    <?php echo load_month_buttons(1);  ?>
                    <input type="hidden" id="hidden_wo_number" name="hidden_wo_number" value="" />
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" id="search_div"></td>
            </tr>
        </table> 
    </form>
    </div>
    </body>           
    <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?php
	exit();
}

if($action=="create_wo_search_list_view")
{
 	extract($_REQUEST); 
	$ex_data = explode("_",$data);
	$itemCategory = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$txt_date_from = $ex_data[3];
	$txt_date_to = $ex_data[4];
	$company = $ex_data[5];
 	$garments_nature = $ex_data[6];
				
	$sql_cond="";
	if(trim($itemCategory)!="") $sql_cond .= " and item_category='$itemCategory'";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1)
			$sql_cond = " and wo_number like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==2)
			$sql_cond = " and supplier_id=trim('$txt_search_common')";		
 	}
	if($txt_date_from!="" || $txt_date_to!="") $sql_cond .= " and wo_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if(trim($company)!="") $sql_cond .= " and company_name='$company'";
		
 	$sql = "select wo_number_prefix_num, wo_number, company_name, buyer_po, wo_date,supplier_id,attention,wo_basis_id,item_category,currency_id,delivery_date,source,pay_mode
			from wo_non_order_info_mst where status_active=1 and is_deleted=0 $sql_cond"; //and garments_nature=$garments_nature
	//echo $sql;die;
	$result = sql_select($sql);
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
 	
	$arr=array(0=>$company_arr,3=>$pay_mode,4=>$supplier_arr,5=>$wo_basis,6=>$source);
	echo  create_list_view("list_view", "Company, WO Number, WO Date, Pay Mode, Supplier, WO Basis, Source", "150,100,100,100,150,100,100","900","250",0, $sql, "js_set_value", "wo_number", "", 1, "company_name,0,0,pay_mode,supplier_id,wo_basis_id,source", $arr , "company_name,wo_number_prefix_num,wo_date,pay_mode,supplier_id,wo_basis_id,source", "","",'0,0,0,0,0,0,0,0');
 	exit();	
}

if($action=="populate_data_from_search_popup")
{
	$sql = "select wo_number, company_name, buyer_po, wo_date, supplier_id, attention, buyer_name, style, wo_basis_id,item_category,currency_id,delivery_date,source,pay_mode,is_approved
			from  wo_non_order_info_mst where wo_number='$data'";  
	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $resultRow)
	{
		echo "$('#cbo_company_name').val('".$resultRow[csf("company_name")]."');\n";
		echo "$('#cbo_company_name').attr('disabled',true);\n";
		echo "$('#cbo_item_category').val('".$resultRow[csf("item_category")]."');\n";
		echo "$('#cbo_supplier').val('".$resultRow[csf("supplier_id")]."');\n";
		echo "$('#txt_wo_date').val('".change_date_format($resultRow[csf("wo_date")])."');\n";
		echo "$('#cbo_currency').val('".$resultRow[csf("currency_id")]."');\n";
		echo "$('#cbo_wo_basis').val('".$resultRow[csf("wo_basis_id")]."');\n";
		echo "$('#cbo_wo_basis').attr('disabled',true);\n";
		echo "$('#cbo_pay_mode').val('".$resultRow[csf("pay_mode")]."');\n";
		echo "$('#cbo_source').val('".$resultRow[csf("source")]."');\n";
		echo "$('#txt_delivery_date').val('".change_date_format($resultRow[csf("delivery_date")])."');\n";
		echo "$('#txt_attention').val('".$resultRow[csf("attention")]."');\n";
		echo "$('#txt_buyer_name').val('".$resultRow[csf("buyer_name")]."');\n";
		echo "$('#txt_style').val('".$resultRow[csf("style")]."');\n";
		echo "$('#txt_buyer_po').val('".$resultRow[csf("buyer_po")]."');\n";
		
		if($resultRow[csf("wo_basis_id")]==3 && $resultRow[csf("buyer_po")]!="")
		{
			$sqlResult = sql_select("select po_number from wo_po_break_down where id in (".$resultRow[csf("buyer_po")].")");
			$poNumber="";$i=0;
			foreach($sqlResult as $res)
			{
				if($i>0)$poNumber .= ",";
				$poNumber .= $res[csf("po_number")];
				$i=1;
			}
		}
		echo "$('#txt_buyer_po_no').val('".$poNumber."');\n";		
		if($resultRow[csf("wo_basis_id")]!=3) echo "$('#txt_buyer_po_no').attr('disabled',true);\n";
		else echo "$('#txt_buyer_po_no').attr('disabled',false);\n";
		
		echo "document.getElementById('is_approved').value = '".$resultRow[csf("is_approved")]."';\n";
		
		if($resultRow[csf("is_approved")]==1)
		{
			echo "$('#approved').text('Approved');\n"; 
		}
		else
		{
			echo "$('#approved').text('');\n";
		}
	}
	exit();
}

if($action=="show_dtls_listview_update")
{
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sql = "select b.id,a.wo_basis_id, b.po_breakdown_id, b.requisition_no, b.item_id,b.yarn_count, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color_name, b.req_quantity, b.supplier_order_quantity, b.uom, b.rate, b.amount, c.po_number
			from 
				wo_non_order_info_mst a, wo_non_order_info_dtls b left join wo_po_break_down c on b.po_breakdown_id=c.id
			where
				a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.wo_number='$data' and a.id=b.mst_id";				   
	//echo $sql;die; //b.item_category,
	$result = sql_select($sql);
	$totalQnty=$totalValue=0;
	$i=1;
	foreach($result as $row)
	{
		$totalQnty += $row[csf("supplier_order_quantity")];
		$totalValue += $row[csf("amount")];
		if($i==1)
		{
		  ?>
        	<div style="width:1050px;">
				<table cellspacing="0" width="100%" class="rpt_table" id="tbl_details" >
					<thead>
						<tr>
							<?php if($row[csf("wo_basis_id")]==3 ){?>
                            <th>PO Number</th>
                            <?php } ?>
                            <th>Color</th>
							<th>Count</th>
							<th>Comp 1</th>
							<th>%</th>
							<th>Comp 2</th>
							<th>%</th>
							<th>Yarn Type</th>
							<th>UOM</th>
							<th>Quantity</th>
							<th>Rate</th>
							<th>Value</th>
                            <th>Action</th>
 						</tr>    
					</thead>
         <?php } ?>        
                    <tr class="general" id="<?php echo $i;?>">
                    	
                         <!-- This is for buyer po selected in WO Basis -->
						<?php if($row[csf("wo_basis_id")]==3){
                        echo "<td width=\"80\">";
                        } ?>
                            <input type="<?php if($row[csf("wo_basis_id")]==3)echo 'text'; else echo 'hidden';?>" name="txt_po_<?php echo $i;?>" id="txt_po_<?php echo $i;?>" class="text_boxes" style="width:80px" value="<?php echo $row[csf("po_number")]; ?>" disabled readonly />
                            <input type="hidden" name="txt_po_brakdown_id_<?php echo $i;?>" id="txt_po_brakdown_id_<?php echo $i;?>" value="<?php echo $row[csf("po_breakdown_id")]; ?>" disabled readonly />
                            <input type="hidden" name="txt_row_id_<?php echo $i;?>" id="txt_row_id_<?php echo $i;?>" value="<?php echo $row[csf("id")]; ?>" disabled />
						<?php if($row[csf("wo_basis_id")]==3){
                        echo "</td>";
                        } ?>
                         <!-- This is for buyer po selected in WO Basis END -->
                         <td width="80">
							<input type="text" name="txt_color_<?php echo $i;?>" id="txt_color_<?php echo $i;?>" class="text_boxes" onKeyPress="colorName(<?php echo $i;?>)" onKeyUp="fn_copy_color(<?php echo $i;?>)" style="width:80px" value="<?php echo $color_arr[$row[csf("color_name")]]; ?>" />
							<input type="hidden" id="hidden_colorID_<?php echo $i;?>" value="<?php echo $row[csf("color_name")]; ?>" disabled  />
                        </td>
						<td width="80">
							<?php
								echo create_drop_down( "cbocount_".$i, 80, "select id,yarn_count from lib_yarn_count where is_deleted = 0 AND status_active = 1 ORDER BY yarn_count ASC","id,yarn_count", 1, "Select", $row[csf("yarn_count")], "",1 );
							?>
						</td>
						<td width="100">
							<?php  echo create_drop_down( "cbocompone_".$i, 100, $composition,"", 1, "-- Select --", $row[csf("yarn_comp_type1st")], "",1,"" ); ?></td>
						<td width="40"><input type="text" id="percentone_<?php echo $i; ?>"  name="percentone_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_one')" value="<?php echo $row[csf("yarn_comp_percent1st")];  ?>" disabled  /></td>
						<td width="100"><?php  echo create_drop_down( "cbocomptwo_".$i, 100, $composition,"", 1, "-- Select --", $row[csf("yarn_comp_type2nd")], "control_composition($i,this.id,'percent_two')",1,"",0 ); ?></td>
						<td width="40"><input type="text" id="percenttwo_<?php echo $i; ?>"  name="percenttwo_<?php echo $i; ?>" class="text_boxes_numeric" style="width:40px" onChange="control_composition(<?php echo $i; ?>,this.id,'percent_two')" value="<?php echo $row[csf("yarn_comp_percent2nd")];  ?>"  disabled /></td>
						<td width="80">
							<?php
								echo create_drop_down( "cbotype_".$i, 80, $yarn_type,"", 1, "Select", $row[csf("yarn_type")], "",1 );
							?> 
						</td> 
						<td width="50">
							<?php
								echo create_drop_down( "cbo_uom_".$i, 70, $unit_of_measurement,"", 1, "Select", $row[csf("uom")], "",1 );
							?>      
						</td> 
						<td width="50">
							<input type="text" name="txt_quantity_<?php echo $i;?>" id="txt_quantity_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric" style="width:50px;" value="<?php echo $row[csf("supplier_order_quantity")];?>" />
						</td>
						<td width="40">
							<input type="text" name="txt_rate_<?php echo $i;?>" id="txt_rate_<?php echo $i;?>" onKeyUp="calculate_yarn_consumption_ratio(<?php echo $i;?>)"  class="text_boxes_numeric"  style="width:40px;" value="<?php echo $row[csf("rate")];?>"  />
						</td>
						<td width="80">
							<input type="text" name="txt_amount_<?php echo $i;?>" id="txt_amount_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" value="<?php echo $row[csf("amount")];?>"  readonly  />
						</td>						                      
                        <td width="80">
                         <?php if($row[csf("wo_basis_id")]!=3){?>
							 <input type="button" id="increaserow_<?php echo $i;?>" style="width:30px" class="formbutton" value="+" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'increase');" />
                         <?php } ?>    
                             <input type="button" id="decreaserow_<?php echo $i;?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_inc_decr_row(<?php echo $i;?>,'decrease');" />
						</td>                         
					</tr>
					<?php
                $i++;
			}
			?>
            <tfoot>
                <tr>
                    <th colspan="9" align="right">Sum</th>
                    <th align="right"><?php echo $totalQnty; ?></th>
                    <th></th>
                    <th align="right"><?php echo $totalValue; ?></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    <?php    
	exit();
}

if ($action=="yarn_work_order_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	$sql=" select a.id, a.wo_number, a.supplier_id, a.wo_date, a.wo_basis_id, a.delivery_date, a.source, a.attention, a.terms_and_condition, group_concat(b.po_breakdown_id) as po_breakdown, a.buyer_name, a.style from wo_non_order_info_mst a, wo_non_order_info_dtls b where a.id=b.mst_id and a.wo_number= '$data[1]' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id";
	//echo $sql;die;
 	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name");
	$supplier_library=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name");
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$country_arr=return_library_array( "select id, country_name from lib_country",'id','country_name');
	
	$dataArray=sql_select($sql);
	$job_no_mst=$dataArray[0][csf('po_breakdown')];
	//echo $job_no_mst;
	$sql_job=" select a.id, a.job_no, a.style_ref_no, a.job_quantity, a.buyer_name, a.agent_name, group_concat(b.id) as po_id from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($job_no_mst) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.job_no ";
	//echo $sql_job;//die;
	$dataAr=sql_select($sql_job);
	?>
	<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><?php echo $data[2]; ?> Challan</strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Work Order No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('wo_number')]; ?></td>
            <td width="125"><strong>Supplier:</strong></td><td width="175px"><?php echo $supplier_library[$dataArray[0][csf('supplier_id')]]; ?></td>
            <td width="120"><strong>Order Date:</strong></td> <td width="175px"><?php echo change_date_format($dataArray[0][csf('wo_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Wo Basis :</strong></td><td width="175px"><?php echo $wo_basis[$dataArray[0][csf('wo_basis_id')]]; ?></td>
            <td><strong>Source:</strong></td><td width="175px"><?php echo $source[$dataArray[0][csf('source')]]; ?></td>
            <td><strong>Delivery Date:</strong></td><td width="175px" colspan="2"><?php echo change_date_format($dataArray[0][csf('delivery_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Agent:</strong></td><td width="175px"><?php echo $buyer_arr[$dataAr[0][csf('agent_name')]]; ?></td>
            <td><strong>Qty:</strong></td><td width="175px" ><?php echo $dataAr[0][csf('job_quantity')]; ?></td>
            <td><strong>Job No:</strong></td><td width="175px"><?php  echo $dataAr[0][csf('job_no')]; ?></td>
        </tr>
        <tr>
            <td><strong>Buyer Name:</strong></td><td width="175px" colspan="3"><?php if ($dataArray[0][csf('wo_basis_id')]==3) echo $buyer_arr[$dataAr[0][csf('buyer_name')]]; else echo $dataArray[0][csf('buyer_name')]; ?></td>
            <td><strong>Style:</strong></td><td width="175px"><?php if ($dataArray[0][csf('wo_basis_id')]==3) echo $dataAr[0][csf('style_ref_no')]; else echo $dataArray[0][csf('style')]; ?></td>
        </tr>
        <tr>
            <td><strong>Attention:</strong></td><td width="175px"><?php echo $dataArray[0][csf('attention')]; ?></td>
            <td><strong>&nbsp;</strong></td><td width="175px" >&nbsp;</td>
            <td><strong>&nbsp;</strong></td><td width="175px">&nbsp;</td>
        </tr>
        
    </table>
         <br>
	<div style="width:100%;">
    <table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="90">PO No</th>
            <th width="80">Color</th>
            <th width="60">Count</th>
            <th width="320">Item Description</th>
            <th width="50" >UOM</th>
            <th width="90">Quantity </th>
            <th width="60">Rate</th> 
            <th width="100">Amount</th>
        </thead>
        <tbody>
<?php
	$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
	
	$i=1;
	$mst_id=$dataArray[0][csf('id')];

	$sql_dtls="Select a.id, a.po_breakdown_id, a.color_name, a.yarn_count, a.yarn_comp_type1st, a.yarn_comp_percent1st, a.yarn_comp_type2nd, a.yarn_comp_percent2nd, a.yarn_type, a.uom, a.supplier_order_quantity, a.rate, a.amount from wo_non_order_info_dtls a where a.mst_id='$mst_id' and a.status_active=1 and a.is_deleted=0";
	//echo $sql_dtls;
	$sql_result = sql_select($sql_dtls);	
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
			$order_quantity+=$row[csf('supplier_order_quantity')];
			$amount += $row[csf('amount')];
			$breakdown_id=$row[csf("po_breakdown_id")];
			//echo $breakdown_id;
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <?php
				if($row[csf("po_breakdown_id")]!=0 || $row[csf("po_breakdown_id")]!="")
				{
					$po_number=return_field_value("po_number"," wo_po_break_down","id=$breakdown_id","po_number");
				}
				$feb_des='';
				if($row[csf("yarn_comp_type2nd")]==0)
				{
					$feb_des=$composition[$row[csf("yarn_comp_type1st")]].' '.$row[csf("yarn_comp_percent1st")].' %, '.$yarn_type[$row[csf("yarn_type")]];
				}
				else if( $row[csf("yarn_comp_type2nd")]!=0)
				{
					$feb_des=$composition[$row[csf("yarn_comp_type1st")]].' '.$row[csf("yarn_comp_percent1st")].' %,'.$composition[$row[csf("yarn_comp_type2nd")]].' '.$row[csf("yarn_comp_percent2nd")].' %, '.$yarn_type[$row[csf("yarn_type")]];
				}
				else
				?>
                <td><p><?php echo $po_number; ?></p></td>
                <td align="center"><?php echo $color_arr[$row[csf("color_name")]]; ?></td>
                <td align="center"><?php echo $count_arr[$row[csf("yarn_count")]]; ?></td>
                <td align="center"><?php echo $feb_des; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf("uom")]]; ?></td>
                <td align="right"><?php echo number_format($row[csf("supplier_order_quantity")],2); ?></td>
                <td align="right"><?php echo number_format($row[csf("rate")],2); ?></td>
                <td align="right"><?php echo number_format($row[csf("amount")],2); ?></td>
			</tr>
			<?php $i++; } ?>
            <tr>
                <td colspan="6" align="right">Total :</td>
                <td align="right"><?php echo number_format($order_quantity,2); ?></td>
                <td>&nbsp;</td>
                <td align="right"><?php echo number_format($amount,2); ?></td>
            </tr>                           
        </tbody>
        <tfoot>
        <?php
			$trm_id=$dataArray[0][csf('terms_and_condition')];			
			//echo $trm_id;
			if($trm_id!='' ||$trm_id!=0)
			{
				$sqltrems="select a.id, a.terms from lib_terms_condition a where a.id in ( $trm_id )";
				$sqltrems_result = sql_select($sqltrems);
				$j=1;
				?>
                    <tr><td colspan="9" align="center"><b>Terms & Condition</b></td></tr>
                <?php
				foreach($sqltrems_result as $row)
				{
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>">
						<td align="center"><?php echo $j; ?></td>
						<td align="left" colspan="8"><p><?php echo $row[csf("terms")]; ?></p></td>
					</tr>
					<?php
					$j++;
				}
			}

		?>
        </tfoot>
    </table>
    <br>
     <?php
        echo signature_table(42, $data[0], "900px");
     ?>
	</div>
	</div>
	<?php
    exit();			
}
?>