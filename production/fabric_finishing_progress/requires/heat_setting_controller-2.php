<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$batch_arr = return_library_array("select id, batch_no from pro_batch_create_mst","id","batch_no");
$machine_name=return_library_array( "select machine_no,id from  lib_machine_name where is_deleted=0", "id", "machine_no"  );
$product_array=return_library_array( "select id, product_name_details from product_details_master where item_category_id=13",'id','product_name_details');


if ($action=="load_drop_floor")
{
	$data=explode('_',$data);
	$loca=$data[0];
	$com=$data[1];
	//echo "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]'  order by floor_name";die;
	echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]' and production_process=4 order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected,"load_drop_down( 'requires/slitting_squeezing_controller', document.getElementById('cbo_company_id').value+'**'+this.value, 'load_drop_machine', 'machine_td' );" );     	 
	exit();
}

if ($action=="load_drop_machine")
{
	$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
	$data=explode('**',$data);

	$com=$data[0];
	$floor=$data[1];
	if($db_type==2)
	{
	echo create_drop_down( "cbo_machine_name", 135, "select id,machine_no || '-' || brand as machine_name from lib_machine_name where category_id=4 and company_id=$com and floor_id=$floor and status_active=1 and is_deleted=0 and is_locked=0 order by machine_name","id,machine_name", 1, "-- Select Machine --", $selected, "get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+this.value, 'populate_data_from_machine', 'requires/slitting_squeezing_controller' );","" );
	}
	else if($db_type==0)
	{
	echo create_drop_down( "cbo_machine_name", 135, "select id,concat(machine_no, '-', brand) as machine_name from lib_machine_name where category_id=4 and company_id=$com and floor_id=$floor and status_active=1 and is_deleted=0 and is_locked=0 order by machine_name","id,machine_name", 1, "-- Select Machine --", $selected, "get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+this.value, 'populate_data_from_machine', 'requires/slitting_squeezing_controller' );","" );
	}
	exit();
	
}
if ($action=="populate_data_from_machine")
{ 
	$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
	$ex_data=explode('**',$data);
	
	 $sql_res="select id, floor_id, machine_group from lib_machine_name where id=$ex_data[2] and category_id=4 and company_id=$ex_data[0] and  floor_id=$ex_data[1] and status_active=1 and is_deleted=0 ";
	$nameArray=sql_select($sql_res);
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_machine_no').value 			= '".$floor_arr[$row[csf("floor_id")]]."';\n";
		echo "document.getElementById('txt_mc_group').value 			= '".$row[csf("machine_group")]."';\n";
	}
	exit();
}


if ($action=="batch_number_popup")
{
	echo load_html_head_contents("Batch Number Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id)
		{ 
			$('#hidden_batch_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
<div align="center" style="width:800px;">
    <form name="searchbatchnofrm"  id="searchbatchnofrm">
        <fieldset style="width:790px;">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" width="770" border="1" rules="all" class="rpt_table">
                <thead>
                    <th width="200px">Batch Date Range</th>
                    <th width="160px">Search By</th>
                    <th id="search_by_td_up" width="180">Enter Batch No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" value="">
                    </th>
                </thead>
                <tr>
                    <td align="center">
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px;">To<input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px;">
                    </td>
                    <td align="center" width="160px">
						<?php
							$search_by_arr=array(0=>"Batch No",1=>"Fabric Booking no.",2=>"Color");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td align="center" id="search_by_td" width="140px">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value, 'create_batch_search_list_view', 'search_div', 'heat_setting_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </table>
            <table width="100%" style="margin-top:5px;">
                <tr>
                    <td colspan="5">
                        <div style="width:100%; margin-left:3px;" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_batch_search_list_view")
{
	$data = explode("_",$data);
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$start_date =$data[2];
	$end_date =$data[3];
	$company_id =$data[4];

	if(trim($data[0])!="")
	{
		if($search_by==0)
			$search_field_cond="and batch_no like '$search_string'";
		else if($search_by==1)
			$search_field_cond="and booking_no like '$search_string'";
		else
			$search_field_cond="and color_id in(select id from lib_color where color_name like '$search_string')";
	}
	else
	{
		$search_field_cond="";
	}
	if($db_type==2)
		{	
		if ($start_date!="" &&  $end_date!="") $batch_date_con = " and batch_date between '".change_date_format($start_date, "yyyy-mm-dd", "-",1)."' and '".change_date_format($end_date, "mm-dd-yyyy", "-",1)."'"; else $batch_date_con ="";
		
		$sql_po=sql_select("select b.mst_id, a.job_no_mst,listagg(cast(a.po_number as varchar2(4000)),',') within group (order by a.po_number) as po_no from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
		}
		if($db_type==0)
		{	
		if ($start_date!="" &&  $end_date!="") $batch_date_con = " and batch_date between '".change_date_format($start_date, "yyyy-mm-dd", "-",1)."' and '".change_date_format($end_date, "yyyy-mm-dd", "-")."'"; else $batch_date_con ="";
		
		$sql_po=sql_select("select b.mst_id, a.job_no_mst,group_concat(a.po_number ) as po_no from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
		}
	$po_num=array();
	
	foreach($sql_po as $row_po_no)
	{
	$po_num[$row_po_no[csf('mst_id')]]['po_no']=$row_po_no[csf('po_no')];
	$po_num[$row_po_no[csf('mst_id')]]['job_no_mst']=$row_po_no[csf('job_no_mst')];
		
	} 	
	$sql = "select id, batch_no, batch_date, batch_weight, booking_no, extention_no, color_id, batch_against, re_dyeing_from from pro_batch_create_mst where entry_form=0 and company_id=$company_id and batch_for in(0,1) and batch_against<>4 and status_active=1 and is_deleted=0 $search_field_cond $batch_date_con"; 
	//echo $sql;//die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Batch No</th>
                <th width="80">Extention No</th>
                <th width="80">Batch Date</th>
                <th width="90">Batch Qnty</th>
                <th width="115">Job No</th>
                <th width="80">Color</th>
                <th>Po No</th>
            </thead>
        </table>
        <div style="width:770px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1;
				//$nameArray=sql_select( $sql );
				$nameArray=sql_select( $sql );
				
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					
					$po_no='';
					
					if($selectResult[csf('re_dyeing_from')]==0)
					{	
						/*$sql_po="select a.po_number as po_no,a.job_no_mst from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and b.mst_id=".$selectResult[csf('id')]."  and b.status_active=1 and b.is_deleted=0 $select_group";
						//echo $sql_po;die;
						$poArray=sql_select( $sql_po );
						foreach ($poArray as $row1)
						{
							if($po_no=='') $po_no=$row1[csf('po_no')]; else $po_no.=",".$row1[csf('po_no')];
						}*/
						
						$po_no=implode(",",array_unique(explode(",",$po_num[$selectResult[csf('id')]]['po_no'])));
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
							<td width="40" align="center"><?php echo $i; ?></td>	
							<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
                            <td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?></p></td>
							<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
							<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
                            <td width="115"><p><?php echo $po_num[$selectResult[csf('id')]]['job_no_mst']; ?></p></td>
							<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
							<td><?php echo $po_no; ?></td>	
						</tr>
						<?php
						$i++;
					}
					else
					{
						$sql_re= "select id, batch_no, batch_date, batch_weight, booking_no, extention_no, color_id, batch_against, re_dyeing_from from pro_batch_create_mst where  batch_for in(0,1) and entry_form=0 and batch_against<>4 and status_active=1 and is_deleted=0 and id=".$selectResult[csf('re_dyeing_from')]." group by id, batch_no, batch_date, batch_weight, booking_no,color_id, batch_against,re_dyeing_from ";
						$dataArray=sql_select( $sql_re );
						
						foreach($dataArray as $row)
						{
							if($row[csf('re_dyeing_from')]==0)
							{
								/*$sql_po="select a.po_number as po_no,a.job_no_mst from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and b.mst_id=".$row[csf('id')]." and b.status_active=1 and b.is_deleted=0 $select_group";
								$poArray=sql_select( $sql_po );
								foreach ($poArray as $row2)
								{
									if($po_no=='') $po_no=$row2[csf('po_no')]; else $po_no.=",".$row2[csf('po_no')];
								}*/
								
								$po_no=implode(",",array_unique(explode(",",$po_num[$selectResult[csf('id')]]['po_no'])));
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
									<td width="40" align="center"><?php echo $i; ?></td>	
									<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
									<td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?></p></td>
									<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
									<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
                                    <td width="115"><p><?php echo $po_num[$selectResult[csf('id')]]['job_no_mst']; ?></p></td>
									<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
									<td><?php echo $po_no; ?></td>	
								</tr>
								<?php
								$i++;
							}
						}
					}
				}
			?>
            </table>
        </div>
	</div>           
<?php

exit();
}

if($action=='populate_data_from_batch')
{ 	
	$ex_data=explode('_',$data);
	//$load_unload=$ex_data[0];
	//echo $batch_id;die;
	$batch_id_ex=$ex_data[0]; 
	$batch_no=$ex_data[1]; 
	if($db_type==0) $select_group_row="  order by a.id desc limit 0,1"; 
	else if($db_type==2) $select_group_row="and  rownum<=1 group by a.id,a.batch_no,a.batch_weight,a.color_id, 
	a.booking_without_order,a.batch_date,a.color_range_id,a.insert_date,a.batch_against ";
	
	if($db_type==0) $pop_batch="order by a.id";
	else if($db_type==2) $pop_batch=" group by a.id,a.batch_no, a.batch_weight,batch_date,a.color_id,a.color_range_id,a.insert_date,a.batch_against, a.booking_without_order  	
	order by a.id";
	if($db_type==0) $select_list=" group_concat(distinct(b.po_id)) as po_id"; 
	else if($db_type==2) $select_list="listagg(b.po_id,',') within group (order by b.po_id) as po_id";
	
	if($batch_no!='')
		{ 
		
	$data_array=sql_select("select MAX(a.id) as id,a.batch_no,a.batch_against,a.batch_date, a.batch_weight,Max(a.extention_no) as extention_no, 
	a.color_id,a.color_range_id,a.insert_date, a.booking_without_order, sum(b.batch_qnty) as batch_qnty, $select_list from pro_batch_create_mst a,pro_batch_create_dtls b where a.id=b.mst_id  and a.batch_no='$batch_no' $select_group_row");
		}
	else
	{
	$data_array=sql_select("select a.id,a.batch_no,a.batch_against, a.batch_weight,Max(a.extention_no) as extention_no,a.batch_date, 
	a.color_id,a.color_range_id,a.insert_date, a.booking_without_order, sum(b.batch_qnty) as batch_qnty, $select_list from pro_batch_create_mst a,pro_batch_create_dtls b where a.id='$batch_id_ex'  and a.id=b.mst_id  $pop_batch");	
	}
		
	if($db_type==0) $select_f_group=""; 
	else if($db_type==2) $select_f_group="group by a.job_no_mst, b.buyer_name";

	if($db_type==0) $select_listagg="group_concat(distinct(a.po_number)) as po_no"; 
	else if($db_type==2) $select_listagg="listagg(cast(a.po_number as varchar2(4000)),',') within group (order by a.po_number) as po_no";

	foreach ($data_array as $row)
	{  	//if($row[csf('batch_against')])
		$pro_id=implode(",",array_unique(explode(",",$row[csf('po_id')])));
		echo "document.getElementById('txt_batch_no').value 		= '".$row[csf("batch_no")]."';\n";
		echo "document.getElementById('hidden_batch_id').value 		= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_batch_ID').value 		= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_color').value 			= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('txt_ext_id').value 			= '".$row[csf("extention_no")]."';\n";
		echo "document.getElementById('txt_batch_against').value 	= '".$batch_against[$row[csf("batch_against")]]."';\n";
		echo "document.getElementById('txt_color_range').value 		= '".$color_range[$row[csf("color_range_id")]]."';\n";
		$insert_t=explode(' ',$row[csf("insert_date")]);
		//print_r($insert_t);
		echo "document.getElementById('txt_batch_time').value 		= '".change_date_format($insert_t[0]).'  '. $insert_t[1]."';\n";
		echo "document.getElementById('txt_batch_date').value 		= '".change_date_format($row[csf("batch_date")])."';\n";
		
		$result_job=sql_select("select $select_listagg, a.job_no_mst, b.buyer_name from wo_po_break_down a, 
		wo_po_details_master b where a.job_no_mst=b.job_no and a.id in(".$pro_id.") and b.status_active=1 and b.is_deleted=0 and a.status_active=1 
		and a.is_deleted=0 $select_f_group");
		$pro_id2=implode(",",array_unique(explode(",",$result_job[0][csf("po_no")])));
		echo "document.getElementById('txt_buyer').value 			= '".$buyer_arr[$result_job[0][csf("buyer_name")]]."';\n";
		echo "document.getElementById('txt_job_no').value 			= '".$result_job[0][csf("job_no_mst")]."';\n";
		echo "document.getElementById('txt_order_no').value 		= '".$pro_id2."';\n";
		
		$sql_batch=sql_select("select 		id,batch_no,batch_id,process_end_date,temparature,stretch,over_feed,feed_in,pinning,speed_min,end_hours,end_minutes,machine_id,floor_id,process_id,remarks from pro_fab_subprocess where entry_form=32 and batch_id='".$row[csf("id")]."' ");
		if(count($sql_batch)>0)
			{
				echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',1);\n"; 	
			}
			else
			{
				echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',0);\n"; 	
			}
		foreach($sql_batch as $r_batch)
		{
			
			echo "document.getElementById('txt_batch_no').value 			= '".$r_batch[csf("batch_no")]."';\n";
			echo "document.getElementById('txt_update_id').value 			= '".$r_batch[csf("id")]."';\n";
			//echo "document.getElementById('txt_update_dtls_id').value 			= '".$r_batch[csf("dtls_id")]."';\n";
			
			echo "document.getElementById('txt_process_end_date').value 	= '".change_date_format($r_batch[csf("process_end_date")])."';\n";
			echo "document.getElementById('cbo_sub_process').value 			= '".$r_batch[csf("process_id")]."';\n";
			echo "set_multiselect('cbo_sub_process','0','1','".$r_batch[csf('process_id')]."','0');\n";
			echo "document.getElementById('txt_end_minutes').value	= '".$r_batch[csf("end_minutes")]."';\n";
			
			echo "document.getElementById('txt_temparature').value	= '".$r_batch[csf("temparature")]."';\n";
			echo "document.getElementById('txt_stretch').value	= '".$r_batch[csf("stretch")]."';\n";
			echo "document.getElementById('txt_feed').value	= '".$r_batch[csf("over_feed")]."';\n";
			echo "document.getElementById('txt_feed_in').value	= '".$r_batch[csf("feed_in")]."';\n";
			echo "document.getElementById('txt_pinning').value	= '".$r_batch[csf("pinning")]."';\n";
			echo "document.getElementById('txt_speed').value	= '".$r_batch[csf("speed_min")]."';\n";
			echo "document.getElementById('txt_end_hours').value	= '".$r_batch[csf("end_hours")]."';\n";
			echo "document.getElementById('cbo_floor').value = '".$r_batch[csf("floor_id")]."';\n";
			echo "load_drop_down( 'requires/heat_setting_controller', document.getElementById('cbo_company_id').value+'**'+".$r_batch[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
			echo "document.getElementById('cbo_machine_name').value = '".$r_batch[csf("machine_id")]."';\n";
			echo "document.getElementById('txt_remarks').value	= '".$r_batch[csf("remarks")]."';\n";
		}
		
		exit();
	}
}

if($action=='show_fabric_desc_listview')
{
	//print($data);
 $ex_data=explode('_',$data);
	$batch_id=$ex_data[0];
	//$update_id=$ex_data[1];
	$update_id=$ex_data[2];
	if($db_type==0) $select_group=" group by item_description"; 
	else if($db_type==2) $select_group="group by item_description,width_dia_type,prod_id";//order by id desc limit 0,1
		$i=1;	
	$sql_result=sql_select("select b.id, b.gsm,b.width_dia_type,b.dia_width,b.const_composition,b.batch_qty,b.prod_id from pro_fab_subprocess_dtls b,pro_fab_subprocess a where a.id=b.mst_id and a.batch_id='$batch_id' and a.status_active=1 and a.is_deleted=0");
	if(count($sql_result)>0)
	{
		foreach($sql_result as $row)
		{
			$desc=explode(",",$row[csf('item_description')]);
		?>
			<tr class="general" id="row_<?php echo $i; ?>">
				
				<td title="<?php echo $desc[1]; ?>"><input type="text" name="txtconscomp_<?php echo $i; ?>" id="txtconscomp_<?php echo $i; ?>" class="text_boxes" style="width:250px;" value="<?php echo $row[csf('const_composition')] ; ?>" disabled/></td>
				<td><input type="text" name="txtgsm_<?php echo $i; ?>" id="txtgsm_<?php echo $i; ?>" class="text_boxes" style="width:60px;" value="<?php echo $row[csf('gsm')]; ?>" /></td>
				<td><input type="text" name="txtbodypart_<?php echo $i; ?>" id="txtbodypart_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo  $row[csf('dia_width')]; //$row[csf('width_dia_type')]; ?>" disabled/></td> 
				<td title="<?php echo $row[csf('width_dia_type')];?>"><input type="text" name="txtdiawidth_<?php echo $i; ?>" id="txtdiawidth_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo $row[csf('width_dia_type')];?>" disabled/></td>
				<td><input type="text" name="txtbatchqnty_<?php echo $i; ?>" id="txtbatchqnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:90px;" value="<?php echo $row[csf('batch_qty')]; ?>" disabled/>
				 <input type="hidden" name="txtprodid_<?php echo $i; ?>" id="txtprodid_<?php echo $i; ?>"  value="<?php echo $row[csf('prod_id')];?>" />
				  <input type="text" name="updateiddtls_<?php echo $i; ?>" id="updateiddtls_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('id')];?>" readonly />
				</td>
			</tr>
		<?php
			$b_qty+= $row[csf('batch_qnty')];
			$i++;
			
		}
	}
	else
	{
		
		$result=sql_select("select width_dia_type,item_description,width_dia_type,prod_id, sum(batch_qnty) as batch_qnty from pro_batch_create_dtls where mst_id=$batch_id and status_active=1 and is_deleted=0  $select_group");
		
		//$subprocess_dtls=return_field_value("id ", "pro_fab_subprocess_dtls", "id=$hidden_buyer_id  and status_active=1 and is_deleted=0","job_no");
	
	
		foreach($result as $row)
		{
			$desc=explode(",",$row[csf('item_description')]);
		?>
			<tr class="general" id="row_<?php echo $i; ?>">
				
				<td title="<?php echo $desc[1]; ?>"><input type="text" name="txtconscomp_<?php echo $i; ?>" id="txtconscomp_<?php echo $i; ?>" class="text_boxes" style="width:250px;" value="<?php echo $desc[1]; ?>" disabled/></td>
				<td><input type="text" name="txtgsm_<?php echo $i; ?>" id="txtgsm_<?php echo $i; ?>" class="text_boxes" style="width:60px;" value="<?php echo $desc[2]; ?>" /></td>
				<td><input type="text" name="txtbodypart_<?php echo $i; ?>" id="txtbodypart_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo  $desc[3]; //$row[csf('width_dia_type')]; ?>" disabled/></td> 
				<td title="<?php echo $fabric_typee[$row[csf('width_dia_type')]];?>"><input type="text" name="txtdiawidth_<?php echo $i; ?>" id="txtdiawidth_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo $fabric_typee[$row[csf('width_dia_type')]];?>" disabled/></td>
				<td><input type="text" name="txtbatchqnty_<?php echo $i; ?>" id="txtbatchqnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:90px;" value="<?php echo $row[csf('batch_qnty')]; ?>" disabled/>
				 <input type="hidden" name="txtprodid_<?php echo $i; ?>" id="txtprodid_<?php echo $i; ?>"  value="<?php echo $row[csf('prod_id')];?>" />
				  <input type="text" name="updateiddtls_<?php echo $i; ?>" id="updateiddtls_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('id')];?>" readonly />
				</td>
			</tr>
		<?php
			$b_qty+= $row[csf('batch_qnty')];
			$i++;
			
		}
	}?>
	 <tr>
      
        <td colspan="4" align="right"><b>Sum:</b> <?php //echo $b_qty; ?> </td>
         <td align="right"><b><?php echo $b_qty; ?> </b></td>
     </tr>
	<?php
	exit();
}


if($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if(str_replace("'","",$update_id)=="")
		$field_array="id,company_id,batch_no,batch_id,batch_ext_no,process_id,process_end_date,temparature,stretch,over_feed,feed_in,pinning,speed_min,end_hours,end_minutes,machine_id,floor_id,entry_form,remarks,inserted_by,insert_date";
		$id=return_next_id( "id", " pro_fab_subprocess", 1 ) ;
		$data_array="(".$id.",".$cbo_company_id.",".$txt_batch_no.",".$hidden_batch_id.",".$txt_ext_id.",".$cbo_sub_process.",".$txt_process_end_date.",".$txt_temparature.",".$txt_stretch.",".$txt_feed.",".$txt_feed_in.",".$txt_pinning.",".$txt_speed.",".$txt_end_hours.",".$txt_end_minutes.",".$cbo_machine_name.",".$cbo_floor.",32,".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		//print_r($data_array);die;
		
		$mst_update_id=str_replace("'","",$id);
		$field_array_dtls="id, mst_id, entry_page, prod_id, const_composition, gsm, dia_width, width_dia_type, batch_qty, inserted_by, insert_date";
		$id_dtls=return_next_id( "id", "pro_fab_subprocess_dtls", 1 ) ;
		//echo $total_row;die;
		for($i=1;$i<=$total_row;$i++)
		{
			$prod_id="txtprodid_".$i;
			$txtconscomp="txtconscomp_".$i;
			$txtgsm="txtgsm_".$i;
			$txtbodypart="txtbodypart_".$i;
			$txtdiawidth="txtdiawidth_".$i;
			$txtbatchqnty="txtbatchqnty_".$i;
			
			$Itemprod_id=str_replace("'","",$$prod_id);
			if($data_array_dtls!="") $data_array_dtls.=","; 
			$data_array_dtls.="(".$id_dtls.",".$mst_update_id.",32,".$Itemprod_id.",".$$txtconscomp.",".$$txtgsm.",".$$txtbodypart.",".$$txtdiawidth.",".$$txtbatchqnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_dtls=$id_dtls+1;
			//print_r($data_array_dtls);die;
		}
		$rID=sql_insert("pro_fab_subprocess",$field_array,$data_array,0);
		$rID2=sql_insert("pro_fab_subprocess_dtls",$field_array_dtls,$data_array_dtls,0);
		//echo "insert into pro_fab_subprocess_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;
		
		//check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  

				echo "0**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
		if($rID && $rID2)
			{
				oci_commit($con);
				echo "0**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		
		disconnect($con);
		die;
			
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		//$update_id=str_replace("'","",$txt_update_id);
		//$field_array_update="company_id*batch_no*batch_id*batch_ext_no*process_id*process_end_date*temparature*stretch*over_feed*feed_in*pinning*speed_min*end_hours*end_minutes*machine_id*floor_id*remarks*updated_by*update_date";
		//$data_array_update=$cbo_company_id."*".$txt_batch_no."*".$hidden_batch_id."*".$txt_ext_id."*".*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$id=str_replace("'",'',$txt_update_id);
		$field_array="process_id*process_end_date*temparature*stretch*over_feed*feed_in*pinning*speed_min*end_hours*end_minutes*machine_id*floor_id*remarks*updated_by*update_date";
		$data_array="".$cbo_sub_process."*".$txt_process_end_date."*".$txt_temparature."*".$txt_stretch."*".$txt_feed."*".$txt_feed_in."*".$txt_pinning."*".$txt_speed."*".$txt_end_hours."*".$txt_end_minutes."*".$cbo_machine_name."*".$cbo_floor."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		//echo "insert into subcon_inbound_bill_dtls (".$field_array.") values ".$data_array;die;
//print_r($data_array);die;
		$rID=sql_update("pro_fab_subprocess",$field_array,$data_array,"id",$id,0);
		
		//echo $data_array_update;die;
		
		/*$field_array_dtls_update="gsm*updated_by*update_date";
		for($i=1;$i<=$total_row;$i++)
		{
			//$prod_id="txtprodid_".$i;
			//$txtconscomp="txtconscomp_".$i;
			$txtgsm="txtgsm_".$i;
			//$txtbodypart="txtbodypart_".$i;
			//$txtdiawidth="txtdiawidth_".$i;
			//$txtbatchqnty="txtbatchqnty_".$i;
			$updateiddtls="updateiddtls_".$i;
			//$Itemprod_id=str_replace("'","",$$prod_id);
			//if($data_array_dtls!="") $data_array_dtls.=","; 
			//$data_array_dtls.="(".$id_dtls.",".$mst_update_id.",32,".$Itemprod_id.",".$$txtconscomp.",".$$txtgsm.",".$$txtbodypart.",".$$txtdiawidth.",".$$txtbatchqnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			//$id_dtls=$id_dtls+1;
			//print_r($data_array_dtls);die;
			
			$id_arr[]=str_replace("'",'',$$updateiddtls);
			$data_array_dtls_update[str_replace("'",'',$$updateiddtls)] = explode("*",($$txtgsm."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));
					
			$id_dtls=str_replace("'",'',$$updateiddtls);
				
		}*/	//print_r($id_arr);die;
		//$rID=sql_update("pro_fab_subprocess",$field_array_update,$data_array_update,"id",$txt_update_id,1);
		//if($data_array_dtls_update!="")
			//{
				//echo bulk_update_sql_statement( "pro_fab_subprocess_dtls", "id", $field_array_dtls_update, $data_array_dtls_update, $id_arr );die;
				//$rID2=sql_multirow_update( "pro_fab_subprocess_dtls", "id", $field_array_dtls_update, $data_array_dtls_update, $id_arr );
		//$rID2=sql_multirow_update("pro_fab_subprocess_dtls",$field_array_dtls_update,$data_array_dtls_update,"id",$id_arr,1);

				
			//}
			
		//check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "1**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		
		if($db_type==2)
		{
			if($rID )
			{
				oci_commit($con);
				echo "1**".str_replace("'","",$id);//."**".str_replace("'","",$id_dtls);
				
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$id);//."**".str_replace("'","",$id_dtls);
			}
		}
		
		disconnect($con);
		die;
		
	}
}

if($action=="check_batch_no")
{
	$data=explode("**",$data);
	$sql="select id, batch_no from pro_batch_create_mst where batch_no='".trim($data[1])."' and company_id='".trim($data[0])."' and is_deleted=0 and status_active=1";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('id')];
	}
	else
	{
		echo "0_";
	}
	
	exit();	
}

?>