<?php

header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');
$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//---------------------------------------------------- Start---------------------------------------------------------------------------
$color_library=return_library_array("select id,color_name from lib_color", "id", "color_name");
$company_library=return_library_array("select id,company_name from lib_company", "id", "company_name");
$buyer_arr=return_library_array("select id, short_name from lib_buyer",'id','short_name');
$trim_group= return_library_array("select id, item_name from lib_item_group",'id','item_name');

if ($action=="load_drop_down_buyer")
{
	//$data=explode('_',$data);
	echo create_drop_down( "cbo_buyer_id", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",0);  
	exit();
}
if ($action=="style_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
	?>
	 <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function check_all_data()
		{
			var row_num=$('#list_view tr').length-1;
			for(var i=1;  i<=row_num;  i++)
			{
				$("#tr_"+i).click();
			}
			
		}
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
	</script>
 <input type="hidden" id="txt_po_id" />
 <input type="hidden" id="txt_po_val" />
     <?php
	if ($data[0]==0) $company_name=""; else $company_name="company_name='$data[0]'";
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and buyer_name='$data[1]'";
	if($db_type==0) $year_field="YEAR(insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year";
	else $year_field="";
	$sql ="select id,style_ref_no,job_no_prefix_num as job_prefix,$year_field from wo_po_details_master where $company_name $buyer_name"; 
	echo create_list_view("list_view", "Style Ref. No.,Job No,Year","200,100,100","450","310",0, $sql , "js_set_value", "id,style_ref_no", "", 1, "0", $arr, "style_ref_no,job_prefix,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	exit();	 
}
if ($action=="order_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	$company=$data[0];
	$buyer=$data[1];
	$style=$data[2];
	
	//print ($data[1]);
	?>
	 <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function check_all_data()
		{
			var row_num=$('#list_view tr').length-1;
			for(var i=1;  i<=row_num;  i++)
			{
				$("#tr_"+i).click();
			}
			
		}
		
	function js_set_value(id)
	{ //alert(id);
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
	</script>
      <input type="hidden" id="txt_po_id" />
     <input type="hidden" id="txt_po_val" />
 <?php
	if ($data[0]==0) $company_id=""; else $company_id=" and company_name=$data[0]";
	if ($data[1]==0) $buyer_id=""; else $buyer_id=" and buyer_name=$data[1]";
	if ($data[2]==0) $style=""; else $style=" and b.id in($data[2])";
	
	if($db_type==0) $year_field2="and year(insert_date)='$data[3]'"; 
	else if($db_type==2) $year_field2="and to_char(a.insert_date,'YYYY')";
	if($data[3]!=0) $year_cond="$year_field2='$data[3]'"; else $year_cond="";
	if($db_type==0) $year_field="YEAR(b.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(b.insert_date,'YYYY') as year";
	else $year_field="";
	 $sql ="select distinct a.id,a.po_number,b.job_no_prefix_num as job_prefix,$year_field from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company_id  $buyer_id $style $year_cond"; 
	echo create_list_view("list_view", "Order Number,Job No, Year","150,100,50","440","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "po_number,job_prefix,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	exit();
}
// Style Wise Search.
if ($action=="report_generate_style")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_buyer=str_replace("'","",$cbo_buyer_id);
	
	if($cbo_buyer==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer";
	}
	
	if(str_replace("'","",trim($txt_order_no))=="")
	{
		$po_id_cond="";
	}
	else
	{
		if(str_replace("'","",$txt_order_no_id)!="")
		{
			$po_id_cond=" and b.id in(".str_replace("'","",$txt_order_no_id).")";
		}
		else
		{
			$po_number=trim(str_replace("'","",$txt_order_no))."%";
			$po_id_cond=" and b.po_number like '$po_number'";
		}
	}
	
	if(str_replace("'","",$txt_style)!="") $style=" and a.id in(".str_replace("'","",$txt_style).")"; else $style="";
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	if( $date_from!="" && $date_to!="") $pub_date= " and b.pub_shipment_date between '".$date_from."' and '".$date_to."'"; else $pub_date=""; 
	ob_start();	
	?>
    <fieldset style="width:1550px;">
        <table width="1550">
            <tr class="form_caption">
                <td colspan="17" align="center" style="border:none;font-size:16px; font-weight:bold"> <?php echo $report_title; ?></td>
            </tr>
            <tr class="form_caption">
                <td colspan="17" align="center"><?php echo $company_library[$cbo_company]; ?></td>
            </tr>
        </table>
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1548" class="rpt_table" >
			<thead>
				<th width="40">SL </th>
                <th width="70">Buyer Name</th>
                <th width="100">Job No</th>
                <th width="110">Style</th>
                <th width="140">Order No</th>
                <th width="90">Order Qty.</th>
                <th width="80">Order Qty.(Dzn)</th>
                <th width="130">Item Group</th>
                <th width="60">UOM</th>
                <th width="100">Item Color</th>
                <th width="60">Item Size</th>
                <th width="90">Recv. Qty</th>
                <th width="100">Recv. Value</th>
                <th width="90">Issue Qty.</th>
                <th width="100">Left Over</th>
                <th width="60">Rate</th>
                <th>Left Over Value</th>
            </thead>
        </table>
        <div style="width:1548px; overflow-y:scroll; max-height:350px;font-size:12px; " id="scroll_body">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1530" class="rpt_table" id="tbl_issue_status" >
		   <?php
				$i=1; $tot_receive_qty=0; $tot_receive_value=0; $tot_issue_qty=0; $total_left_over=0; $total_left_over_balance=0; $dataArrayRecv=array();
				
				$sql_recv=sql_select("select a.item_group_id, a.order_uom, a.item_color, a.item_size, a.cons_rate, b.po_breakdown_id as po_id, b.quantity from inv_trims_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and a.trans_id=b.trans_id and b.trans_type=1 and b.entry_form=24 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0"); 
				foreach($sql_recv as $row)
				{
					$dataArrayRecv[$row[csf('po_id')]].=$row[csf('item_group_id')]."_".$row[csf('order_uom')]."_".$row[csf('item_color')]."_".$row[csf('item_size')]."_".$row[csf('cons_rate')]."_".$row[csf('quantity')].",";
				}
				$issue_qty_arr=array();
				$issue_qty_data=sql_select("select b.po_breakdown_id,a.item_color_id,a.item_size, a.item_group_id,b.quantity as issue_qty from inv_trims_issue_dtls a , order_wise_pro_details b where a.id=b.dtls_id and  a.trans_id=b.trans_id and b.trans_type=2 and b.entry_form=25 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
				foreach($issue_qty_data as $row)
				{
					$issue_qty_arr[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]][$row[csf('item_color_id')]]['issue_qty']=$row[csf('issue_qty')];
				}
				
				if($db_type==0)
				{
					$sql="select a.style_ref_no, a.buyer_name, a.job_no, group_concat(b.po_number) as po_number, group_concat(b.id) as po_id, sum(a.total_set_qnty*b.po_quantity) as po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$cbo_company $buyer_id_cond $style $po_id_cond $pub_date group by a.job_no, a.style_ref_no, a.buyer_name order by a.id";
				} 
				else
				{
					$sql="select a.style_ref_no, a.buyer_name, a.job_no, LISTAGG(CAST(b.po_number AS VARCHAR2(4000)),',') WITHIN GROUP (ORDER BY b.id) as po_number, LISTAGG(CAST(b.id AS VARCHAR2(4000)),',') WITHIN GROUP (ORDER BY b.id) as po_id, sum(a.total_set_qnty*b.po_quantity) as po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$cbo_company $buyer_id_cond $style $po_id_cond $pub_date group by a.id, a.job_no, a.style_ref_no, a.buyer_name, a.total_set_qnty order by a.id";
				}
				//echo $sql;//die;
				$result=sql_select($sql);
				foreach ($result as $row)
				{
					$x=0; $z=0; $dataArray=array(); $uomArray=array(); $rowspan_array=array(); $rowspan_color_array=array();
					$job_po_id=explode(",",$row[csf('po_id')]);
					foreach($job_po_id as $po_id)
					{
						$dataRecv=explode(",",substr($dataArrayRecv[$po_id],0,-1));
						foreach($dataRecv as $recvRow)
						{
							$recvRow=explode("_",$recvRow);
							$item_group_id=$recvRow[0];
							$order_uom=$recvRow[1];
							$item_color=$recvRow[2];
							$item_size=$recvRow[3];
							if($item_size=="") $item_size=0;
							$cons_rate=$recvRow[4];
							$quantity=$recvRow[5];
							$recv_value=$cons_rate*$quantity;
							
							if($quantity>0)
							{
								if($dataArray[$item_group_id][$item_color][$item_size]['qty']=="")
								{ 
									$rowspan_array[$item_group_id]+=1;
									$rowspan_color_array[$item_group_id][$item_color]+=1;
									
									$z++;
								}
								
								$dataArray[$item_group_id][$item_color][$item_size]['qty']+=$quantity;
								$dataArray[$item_group_id][$item_color][$item_size]['val']+=$recv_value;
								$uomArray[$item_group_id]=$order_uom;
							}
						}
					}
					
					if($z>0)
					{
						foreach($dataArray as $item_group_id=>$item_group_data)
						{ 
							$s=0;
							foreach($item_group_data as $item_color_id=>$item_color_data)
							{
								$c=0;
								foreach($item_color_data as $item_size=>$item_size_data)
								{
									$recv_qnty=$item_size_data['qty'];
									$recv_value=$item_size_data['val'];
									//echo $row[csf('po_id')];
									$issue_qty=0;
									foreach($job_po_id as $po_id)
									{
										$issue_qty+=$issue_qty_arr[$po_id][$item_group_id][$item_color_id]['issue_qty'];
									}
									if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
								?> 
									<tr bgcolor="<?php echo $bgcolor; ?>"  onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
										<?php
										if($x==0)
										{
										?>
											<td width="40" rowspan="<?php echo $z; ?>"><?php echo $i;?> </td>
											<td width="70" rowspan="<?php echo $z; ?>"><p><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></p></td>
											<td width="100" rowspan="<?php echo $z; ?>"><p><?php echo $row[csf('job_no')]; ?></p></td>
											<td width="110" rowspan="<?php echo $z; ?>"><p><?php echo $row[csf('style_ref_no')]; ?></p></td>
											<td width="140" rowspan="<?php echo $z; ?>"><p><?php echo $row[csf('po_number')]; ?></p></td>
											<td width="90" align="right" rowspan="<?php echo $z; ?>"><?php echo $row[csf('po_quantity')]; ?></td>
											<td width="80" align="right" rowspan="<?php echo $z; ?>"><?php echo number_format($row[csf('po_quantity')]/12,2); ?></td>
										<?php	
										}
										
										if($s==0)
										{
										?>
											<td width="130" rowspan="<?php echo $rowspan_array[$item_group_id]; ?>"><p><?php echo $trim_group[$item_group_id]; ?></p></td>
											<td width="60" align="center" rowspan="<?php echo $rowspan_array[$item_group_id]; ?>"><p><?php echo $unit_of_measurement[$uomArray[$item_group_id]]; ?></p></td>
										<?php	
										}
										if($c==0)
										{
										?>
											<td width="100" rowspan="<?php echo $rowspan_color_array[$item_group_id][$item_color_id]; ?>"><p><?php echo $color_library[$item_color_id]; ?></p></td>
										<?php	
										}
										?>
										<td width="60"><p><?php if($item_size=="0") echo "&nbsp;"; else echo $item_size; ?></p></td>
										<td width="90" align="right"><?php echo number_format($recv_qnty,2); ?></td>
										<td width="100" align="right"><?php echo number_format($recv_value,2); ?></td>
										<td width="90" align="right"><?php echo number_format($issue_qty,2); ?></td>
										<td width="100" align="right"><?php $left_over=$recv_qnty-$issue_qty;echo number_format($left_over,2); ?></td>
										<td width="60" align="right"><?php $tot_rate=$recv_value/$recv_qnty; echo number_format($tot_rate,2); ?></td>
										<td align="right"><?php $tot_left_val=$left_over*$cons_rate;echo number_format($tot_left_val,2); ?></td>
									</tr>
								<?php    
								$total_recv_qty+=$recv_qnty; 
								$total_recv_value+=$recv_value; 
								$total_issue_qty+=$issue_qty;
								$total_left_val+=$tot_left_val;
								$total_left_over+=$left_over; 
								   
									$i++;
									$x++;
									$s++;
									$c++;
								}
							}
						}
						$total_order_qty+=$row[csf('po_quantity')];   
					}
					else
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?> 
                        <tr bgcolor="<?php echo $bgcolor; ?>"  onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                            <td width="40"><?php echo $i;?> </td>
                            <td width="70"><p><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('job_no')]; ?></p></td>
                            <td width="110"><p><?php echo $row[csf('style_ref_no')]; ?></p></td>
                            <td width="140"><p><?php echo $row[csf('po_number')]; ?></p></td>
                            <td width="90" align="right"><?php echo $row[csf('po_quantity')]; ?></td>
                            <td width="80" align="right"><?php echo number_format($row[csf('po_quantity')]/12,2); ?></td>
                            <td width="130">&nbsp;</td>
                            <td width="60" align="center">&nbsp;</td>
                            <td width="100">&nbsp;</td>
                            <td width="60">&nbsp;</td>
                            <td width="90" align="right">&nbsp;</td>
                            <td width="100" align="right">&nbsp;</td>
                            <td width="90" align="right">&nbsp;</td>
                            <td width="100" align="right">&nbsp;</td>
                            <td width="60" align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                    <?php        
                        $i++;	
					}
				}
				?>
             	<tr class="tbl_bottom">
               		<td colspan="5" align="right">Total</td>
                    <td align="right"><?php echo number_format($total_order_qty,2); ?></td>
                    <td colspan="5" align="right"></td>
                    
              		<td align="right"><?php echo number_format($total_recv_qty,2); ?></td>
               		<td align="right"><?php echo number_format($total_recv_value,2); ?></td>
               		<td align="right"><?php echo number_format($total_issue_qty,2); ?></td>
               		<td><?php echo number_format($total_left_over,2); ?></td>
               		<td>&nbsp;</td>
               		<td align="right"><?php echo number_format($total_left_val,2); ?></td>
				</tr>        
			</table>
		</div>
	</fieldset>
<?php
    $html = ob_get_contents();
    ob_clean();
    foreach (glob("*.xls") as $filename) {
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}
if($action=="receive_item_color_size_popup")
{
	echo load_html_head_contents("Receive Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<fieldset style="width:470px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="450" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="100">Receive ID</th>
                    <th width="75">Receive Date</th>
                    <th width="80">Recv. Qty</th>
				</thead>
                <tbody>
                <?php
					if ($itemcolor!=0) $itemcolor_cond=" and b.item_color=$itemcolor"; else $itemcolor_cond="";
					if ($item_size!=0) $item_size_cond=" and b.item_size=$item_size"; else $item_size_cond="";
					$i=1;
					$mrr_sql="select a.id, a.recv_number, a.receive_date, c.quantity as quantity
					from inv_receive_master a, inv_trims_entry_dtls b, order_wise_pro_details c 
					where a.id=b.mst_id  and a.entry_form=24 and b.id=c.dtls_id and c.trans_type=1 and   c.po_breakdown_id in($po_id)  and a.company_id='$companyID' and b.item_group_id='$item_group' $itemcolor_cond $item_size_cond and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 group by    c.po_breakdown_id,b.item_group_id,c.quantity,a.recv_number,a.id,a.receive_date";
					//echo $mrr_sql;
					$dtlsArray=sql_select($mrr_sql);
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('recv_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('receive_date')]); ?></p></td>
                           
                            <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}
if($action=="issue_color_size_popup")
{
	echo load_html_head_contents("Issue Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<fieldset style="width:470px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="450" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="100">Issue ID</th>
                    <th width="75">Issue Date</th>
                    <th width="80">Issue Qty</th>
				</thead>
                <tbody>
                <?php
					if ($itemcolor!=0) $itemcolor_cond=" and b.item_color_id=$itemcolor"; else $itemcolor_cond="";
					if ($item_size!=0) $item_size_cond=" and b.item_size=$item_size"; else $item_size_cond="";
					$i=1;
					$mrr_sql="select a.id, a.issue_number, a.issue_date,c.quantity as quantity
					from  inv_issue_master a,inv_trims_issue_dtls b, order_wise_pro_details c,product_details_master p 
					where a.id=b.mst_id  and a.entry_form=25 and p.id=b.prod_id and b.id=c.dtls_id and  c.trans_type=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and  c.po_breakdown_id in($po_id)  and a.company_id='$companyID' and p.item_group_id='$item_group' $itemcolor_cond  $item_size_cond group by c.po_breakdown_id,p.item_group_id,c.quantity,a.issue_number,a.id,a.issue_date ";
					//echo $mrr_sql;
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('issue_date')]); ?></p></td>
                           
                            <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}
?>