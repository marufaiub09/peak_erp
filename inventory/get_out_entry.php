<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Out Order Entry
				
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	01-10-2013
Updated by 		: 	Kausar->Maruf 	(Creating print report)	
Update date		: 	11-01-2014->21-12-2015		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Receive Info","../", 1, 1, $unicode,1,1); 

?>
<script>



var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
 
  	
// popup for SYSTEM ID----------------------
function openmypage_system()
{
	
	get_php_form_data(sysNumber, "populate_master_from_data", "requires/get_out_entry_controller" );
	show_list_view(sysNumber,'show_dtls_list_view','list_container','requires/get_out_entry_controller','');
			
	
}


function fnc_getout_entry(operation)
{
	if( form_validation('txt_gate_pass','Gate Pass ID')==false )
		{
			return;
		}
		
		var dataString = "txt_gate_pass*txt_gate_out_date*txt_gate_out_time";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
		//alert(data)
		//freeze_window(operation);
		http.open("POST","requires/get_out_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_getout_entry_reponse;
	
}

function fnc_getout_entry_reponse()
{	
	if(http.readyState == 4) 
	{
		//alert(http.responseText);
		var response=trim(http.responseText).split('**');
		if(response[0]==0)
		{
			document.getElementById('txt_gate_pass').value='';
			document.getElementById('list_container_gate_out_dtls').innerHTML='<p class="bang" style="background:#CCC; padding:0 20px;"><span style="color:#FF0000;font-size:20px"></span>আপনার তথ্য জমা হয়েছে। </p><p class="eng" style=" background:#CCC; padding:0 5px; font-size:20px"><span style="color:#FF0000;"></span> Data Save Successfully</p>';
			$('#txt_gate_pass').focus(); 
			show_msg(trim(response[0]));
		}
	   	if (response[0]==40)
		{
			document.getElementById('txt_gate_pass').value='';
			document.getElementById('list_container_gate_out_dtls').innerHTML='<p class="bang" style="background:#CCC; padding:0 5px;font-size:20px"><span style="color:#FF0000;"> </span>আপনি যে Gate Pass নাম্বার টি বাছাই করেছেন তা আগে একবার পাঠানো হয়েছে।</p><p class="eng" style=" background:#CCC; padding:0 5px;font-size:20px"><span style="color:#FF0000;"></span>Second Time Gate Out Not Allowed.</p>';
			$('#txt_gate_pass').focus(); 
		}
		 if (response[0]==30)
		{
			document.getElementById('txt_gate_pass').value='';
			document.getElementById('list_container_gate_out_dtls').innerHTML='<p class="bang" style="background:#CCC; padding:0 5px;font-size:20px"><span style="color:#FF0000;"></span> দুঃখিত, ২৪ ঘণ্টার পর Gate out করতে হলে আপনাকে অনুমতি নিতে হবে। </p><p class="eng" style=" background:#CCC; padding:0 5px;font-size:20px"><span style="color:#FF0000;"></span>After 24 hours Gate Out Not Allowed, Please contract autority.</p>';
			$('#txt_gate_pass').focus(); 
		}
		else if(response[0]==10 )
		{
			show_msg(trim(response[0]));
			document.getElementById('txt_gate_pass_id').value='';
			document.getElementById('list_container_gate_out_dtls').innerHTML='<p class="bang" style="background:#CCC; padding:0 20px;"><span style="color:#FF0000;"></span>দুখিত! আপনার তথ্য জমা করা সম্ভব হয়নি। </p><p class="eng" style=" background:#CCC; padding:0 20px;"><span style="color:#FF0000;"></span>Sorry! Data don\'t Save.</p>';
			$('#txt_gate_pass_id').focus();
			release_freezing();
			return;
		}
		
		
	
		//show_list_view(response[2],'show_dtls_list_view','list_container','requires/get_out_entry_controller','');
		//reset_form('','','txt_item_description*cbo_uom*txt_quantity*txt_rate*txt_amount*txt_remarks','','','');
		//set_button_status(0, permission, 'fnc_getout_entry',1,1);
		release_freezing();
 	}
}




$('#txt_gate_pass_id').live('keydown', function(e) {
   
    if (e.keyCode === 13) {
        e.preventDefault();
		  gate_out_scan(this.value); 
    }
});
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt_gate_out_time').value = h+":"+m+":"+s+" "+'<?php echo date('a',time()+6*60*60);?>';
    var t = setTimeout(function(){startTime()},500);
}
function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}


	//$('#txt_gate_pass').focus();  
  
</script>
<body onLoad="set_hotkey();startTime()">
    <div style="width:100%;" align="center">
      <?php echo load_freeze_divs ("../",$permission);  ?>
      <form name="getout_1" id="getout_1"  autocomplete="off">
    <div style="width:90%;">
    <legend>Get Out</legend>
    <fieldset style="width:900px;">
        <table width="900" cellpadding="0" cellspacing="2" id="tbl_master">
           
            <tr>
                <td width="100" align="right" class="must_entry_caption">Gate Pass ID</td>
                <td width="150">
					<input type="text" name="txt_gate_pass" id="txt_gate_pass" class="text_boxes" style="width:150px" placeholder="Scan Gate Pass ID" 				 />
                </td>
                <td width="100" align="right">Date & Time</td>
                <td width="300">
                 <input class="datepicker" type="text" style="width:100px;" name="txt_gate_out_date" id="txt_gate_out_date"  value="<?php echo date('d-m-y'); ?>" /> 
                 <input class="text_boxes" type="text" style="width:90px;" name="txt_gate_out_time" id="txt_gate_out_time"  value="<?php echo date('h:m:t a',time()+6*60*60); ?>" /> 
                </td>
                <td width="100" align="right" ><input type="button" name="re_button" id="re_button" value="Save" style="width:100px" onClick="fnc_getout_entry(0)" class="formbutton"  /> </td>
                <td width="140">
					<input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /> 
                </td>
            </tr>
           
        </table>
    </fieldset>
    <br>
       
    <fieldset style="900px;">
         <div id="list_container_gate_out_dtls"></div>
       
    </fieldset>
    </div>
      </form>
    </div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>