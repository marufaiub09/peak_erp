﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
$supplier_arr = return_library_array("select id, supplier_name from lib_supplier","id","supplier_name");
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$batch_arr = return_library_array("select id, batch_no from pro_batch_create_mst","id","batch_no");


if ($action=="load_drop_down_store")
{
	if($db_type==0)
	{	  
		echo create_drop_down( "cbo_store_name", 170, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET($data,company_id) and FIND_IN_SET(3,item_category_id) order by store_name","id,store_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
	}
	else
	{
		echo create_drop_down( "cbo_store_name", 170, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=3 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "-- Select --",0, "",0); 
		exit();
	}
}

if($action=="load_drop_down_sewing_com")
{
	$data = explode("_",$data);
	$company_id=$data[1];

	if($data[0]==1)
	{
		echo create_drop_down( "cbo_sewing_company", 170, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "--Select Sewing Company--", "$company_id", "","" );
	}
	else if($data[0]==3)
	{
		if($db_type==0)
		{
			echo create_drop_down( "cbo_sewing_company", 170, "select id, supplier_name from lib_supplier where find_in_set(21,party_type) and find_in_set($company_id,tag_company) and status_active=1 and is_deleted=0","id,supplier_name", 1, "--Select Sewing Company--", 1, "" );
		}
		else
		{
			echo create_drop_down( "cbo_sewing_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type=21 and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "--Select Sewing Company--", 1, "" );
		}
	}
	else
	{
		echo create_drop_down( "cbo_sewing_company", 170, $blank_array,"",1, "--Select Sewing Company--", 1, "" );
	}
	
	exit();
}

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", 0, '',1 );  
	exit();
		
}

/*if($action=="roll_maintained")
{
	$roll_maintained=return_field_value("fabric_roll_level","variable_settings_production","company_name ='$data' and variable_list=3 and is_deleted=0 and status_active=1");

	if($roll_maintained=="") $roll_maintained=0; else $roll_maintained=$roll_maintained;
	
	echo "document.getElementById('roll_maintained').value 	= '".$roll_maintained."';\n";
	
	echo "$('#cbo_buyer_name').val('0');\n";
	echo "$('#txt_issue_qnty').val('');\n";
	echo "$('#hidden_issue_qnty').val('');\n";
	echo "$('#txt_issue_req_qnty').val('');\n";
	echo "$('#hidden_prod_id').val('');\n";
	echo "$('#all_po_id').val('');\n";
	echo "$('#save_data').val('');\n";
	echo "$('#save_string').val('');\n";
	echo "$('#txt_order_numbers').val('');\n";
	echo "$('#txt_fabric_received').val('');\n";
	echo "$('#txt_cumulative_issued').val('');\n";
	echo "$('#txt_yet_to_issue').val('');\n";
	echo "$('#previous_prod_id').val('');\n";
	
	if($roll_maintained==1 || $data==0)
	{
		echo "$('#txt_no_of_roll').val('');\n";
		echo "$('#txt_no_of_roll').attr('disabled','disabled');\n";
		echo "$('#txt_no_of_roll').attr('placeholder','Display');\n";
		echo "$('#fabricDesc_td').html('".'<input type="text" name="txt_fabric_desc" id="txt_fabric_desc" class="text_boxes" style="width:300px;" readonly placeholder="Double Click To Search" onDblClick="openmypage_fabricDescription(1);" />'."');\n";
	}
	else
	{
		echo "$('#txt_no_of_roll').removeAttr('disabled','disabled');\n";
		echo "$('#txt_no_of_roll').removeAttr('placeholder');\n";
		//echo "$('#fabricDesc_td').html('".create_drop_down( "txt_fabric_desc", 310, $blank_array,'', 1, '-- Select Fabric Description --','0', '','','' )."');\n";
				echo "$('#fabricDesc_td').html('".'<input type="text" name="txt_fabric_desc" id="txt_fabric_desc" class="text_boxes" style="width:300px;" readonly placeholder="Double Click To Search" onDblClick="openmypage_fabricDescription(0);" />'."');\n";

	}
	
	exit();	
}*/

if ($action=="batch_lot_popup")
{
	echo load_html_head_contents("Batch/Lot Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(batchLot_no)
		{
			$('#hidden_batchLot_no').val(batchLot_no);
			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
<div align="center" style="width:400px;">
    <form name="searchbatchnofrm"  id="searchbatchnofrm">
        <fieldset style="width:370px;">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" rules="1" border="1" width="370" class="rpt_table">
                <thead>
                    <th>Search By</th>
                    <th id="search_by_td_up">Enter Batch/Lot No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" name="hidden_batchLot_no" id="hidden_batchLot_no" class="text_boxes" value="">
                    </th>
                </thead>
                <tr class="general">
                    <td align="center">
						<?php
							$search_by_arr=array(0=>"Batch/Lot No");
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",'',0 );
                        ?>
                    </td>
                    <td align="center" id="search_by_td">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_company_id').value, 'create_batchlot_search_list_view', 'search_div', 'woven_finish_fabric_issue_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
           <div style="width:100%; margin-top:10px; margin-left:3px;" id="search_div" align="left"></div>
        </fieldset>
    </form>
</div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_batchlot_search_list_view")
{
	$data = explode("_",$data);
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$company_id =$data[2];

	$search_field_cond="";
	if(trim($data[0])!="")
	{
		$search_field_cond="and batch_lot like '$search_string'";
	}

	if($db_type==0)
	{
		$sql = "select batch_lot from inv_transaction where company_id=$company_id and item_category=3 and transaction_type=1 and status_active=1 and is_deleted=0 and batch_lot<>'' $search_field_cond group by batch_lot"; 
	}
	else
	{
		$sql = "select batch_lot from inv_transaction where company_id=$company_id and item_category=3 and transaction_type=1 and status_active=1 and is_deleted=0 and batch_lot is not null $search_field_cond group by batch_lot"; 
	}
	
	//echo $sql;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="350" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th>Batch/lot No</th>
            </thead>
        </table>
        <div style="width:350px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="330" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1;
				$nameArray=sql_select( $sql );
				foreach($nameArray as $selectResult)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $selectResult[csf('batch_lot')]; ?>')"> 
						<td width="40" align="center"><?php echo $i; ?></td>	
						<td><p><?php echo $selectResult[csf('batch_lot')]; ?></p></td>
					</tr>
					<?php
					$i++;
				}
			?>
            </table>
        </div>
	</div>           
<?php

exit();
}

if($action=='show_fabric_desc_listview')
{
	$data_array=sql_select("select a.id, a.product_name_details, a.current_stock from product_details_master a, inv_transaction b where a.id=b.prod_id and b.batch_lot='$data' and a.item_category_id=3 and b.item_category=3 and b.transaction_type=1 and a.current_stock>0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock");
	
	?>
    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="390">
        <thead>
            <th width="30">SL</th>
            <th width="70">Product ID</th>
            <th width="200">Fabric Description</th>
            <th>Stock Qty</th>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach($data_array as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" onClick='set_form_data("<?php echo $row[csf('id')]."**".$row[csf('product_name_details')]."**".$row[csf('current_stock')]; ?>")' style="cursor:pointer" >
                    <td><?php echo $i; ?></td>
                    <td><p><?php echo $row[csf('id')]; ?></p></td>
                    <td><p><?php echo $row[csf('product_name_details')]; ?></p></td>
                    <td align="right"><?php echo number_format($row[csf('current_stock')],2,'.',''); ?></td>
                </tr>
            <?php 
            $i++; 
            } 
            ?>
        </tbody>
    </table>
<?php
exit();
}


if ($action=="po_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	
	extract($_REQUEST);
	$data=explode("_",$data);
	/*$po_id=$data[0]; $type=$data[1];
	if($type==1) 
	{
		$save_data=$data[2];
		$prev_distribution_method=$data[3]; 
		$txt_issue_req_qnty=$data[4]; 
	}*/
?>
	<script>
	
		//var roll_maintained='<?php// echo $roll_maintained; ?>';
		/*function fn_show_check()
		{
			if( form_validation('cbo_buyer_name','Buyer Name')==false )
			{
				return;
			}	
					
			show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<?phpecho $cbo_company_id; ?>+'_'+document.getElementById('cbo_buyer_name').value+'_'+'<?phpecho $all_po_id; ?>', 'create_po_search_list_view', 'search_div', 'woven_finish_fabric_issue_controller', 'setFilterGrid(\'tbl_list_search\',-1);hidden_field_reset();');
			set_all();
		}*/
		
		function distribute_qnty(str)
		{
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_issue_qnty=$('#txt_prop_issue_qnty').val()*1;
				var tblRow = $("#tbl_list_search tr").length;
				var len=totalIssue=0;
				
				if(txt_prop_issue_qnty>0)
				{
					$("#tbl_list_search").find('tr').each(function()
					{
						len=len+1;
						
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;
						
						var issue_qnty=(perc*txt_prop_issue_qnty)/100;
						
						totalIssue = totalIssue*1+issue_qnty*1;
						totalIssue = totalIssue.toFixed(2);						
						if(tblRow==len)
						{
							var balance = txt_prop_issue_qnty-totalIssue;
							if(balance!=0) issue_qnty=issue_qnty+(balance);							
						}
						
						$(this).find('input[name="txtIssueQnty[]"]').val(issue_qnty.toFixed(2));
	
					});
				}
			}
			else
			{
				$('#txt_prop_issue_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					$(this).find('input[name="txtIssueQnty[]"]').val('');
				});
			}
		}

		var selected_id = new Array();

		 function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i,1 );
			}
		}

		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_po_row_id').value;
			if(old!="")
			{
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{
					js_set_value( old[i],0 )
				}
			}
		}

		function js_set_value( str )
		{

			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );

			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );

			$('#po_id').val( id );
		}

		/*function show_finish_fabric_issue()
		{
			var po_id=$('#po_id').val();
			show_list_view ( po_id+'_'+'1'+'_'+'<?phpecho $save_data; ?>'+'_'+'<?phpecho $prev_distribution_method; ?>'+'_'+'<?phpecho $txt_issue_req_qnty; ?>', 'po_popup', 'search_div', 'woven_finish_fabric_issue_controller','');
			distribute_qnty($('#cbo_distribiution_method').val());
		}

		function hidden_field_reset()
		{
			$('#po_id').val('');
			$('#save_string').val( '' );
			$('#tot_finish_qnty').val( '' );
			selected_id = new Array();
		}*/

		function fnc_close()
		{
			var save_data=''; var tot_issue_qnty='';
			var po_id_array = new Array(); var buyer_id =''; var po_no='';

			$("#tbl_list_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtPoName=$(this).find('input[name="txtPoName[]"]').val();
				var txtIssueQnty=$(this).find('input[name="txtIssueQnty[]"]').val();
				var buyerId=$(this).find('input[name="buyerId[]"]').val();

				if(txtIssueQnty*1>0)
				{
					if(save_data=="")
					{
						save_data=txtPoId+"_"+txtIssueQnty;
					}
					else
					{
						save_data+=","+txtPoId+"_"+txtIssueQnty;
					}
					
					if( jQuery.inArray(txtPoId, po_id_array) == -1 )
					{
						po_id_array.push(txtPoId);
						if(po_no=="") po_no=txtPoName; else po_no+=","+txtPoName;
					}
					
					if( buyer_id=="" )
					{
						buyer_id=buyerId;
					}
					
					tot_issue_qnty=tot_issue_qnty*1+txtIssueQnty*1;
				}
			});

			$('#save_data').val( save_data );
			$('#tot_issue_qnty').val(tot_issue_qnty);
			$('#all_po_id').val( po_id_array );
			$('#all_po_no').val( po_no );
			$('#buyer_id').val( buyer_id );
			$('#distribution_method').val( $('#cbo_distribiution_method').val());

			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:10px">
        	<input type="hidden" name="save_data" id="save_data" class="text_boxes" value="">
            <input type="hidden" name="tot_issue_qnty" id="tot_issue_qnty" class="text_boxes" value="">
            <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
            <input type="hidden" name="all_po_no" id="all_po_no" class="text_boxes" value="">
            <input type="hidden" name="buyer_id" id="buyer_id" class="text_boxes" value="">
            <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
            <div style="width:600px; margin-top:10px; margin-bottom:10px" align="center">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
                    <thead>
                        <th>Total Issue Qnty</th>
                        <th>Distribution Method</th>
                    </thead>
                    <tr class="general">
                        <td><input type="text" name="txt_prop_issue_qnty" id="txt_prop_issue_qnty" class="text_boxes_numeric" value="<?php echo $txt_issue_qnty; ?>" style="width:120px" onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
                        <td>
                            <?php
                                $distribiution_method=array(1=>"Proportionately",2=>"Manually");
                                echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"",$prev_distribution_method, "distribute_qnty(this.value);",0 );
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
                <thead>
                    <th width="200">PO No</th>
                    <th width="170">PO Qnty</th>
                    <th>Issue Qnty</th>
                </thead>
            </table>
            <div style="width:600px; max-height:280px; overflow-y:scroll" id="list_container" align="left">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">
                <?php
                    $i=1; $tot_po_qnty=0; $finish_qnty_array=array();
					$explSaveData = explode(",",$save_data); 	
					for($z=0;$z<count($explSaveData);$z++)
					{
						$po_wise_data = explode("_",$explSaveData[$z]);
						$order_id=$po_wise_data[0];
						$finish_qnty=$po_wise_data[1];
						
						$finish_qnty_array[$order_id]=$finish_qnty;
					}
					
                    $sql="select b.id, a.buyer_name, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from inv_transaction i, order_wise_pro_details o, wo_po_break_down b, wo_po_details_master a where i.id=o.trans_id and o.po_breakdown_id=b.id and a.job_no=b.job_no_mst and i.item_category=3 and i.transaction_type=1 and i.status_active=1 and i.is_deleted=0 and o.status_active=1 and o.is_deleted=0 and i.batch_lot='$txt_batch_lot' and i.prod_id=$hidden_prod_id and o.entry_form=17";
                    $nameArray=sql_select($sql);
                    foreach($nameArray as $row)
                    {
                        if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                        
                        $tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
						$iss_qty=$finish_qnty_array[$row[csf('id')]];
                     ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
                            <td width="200">
                                <p><?php echo $row[csf('po_number')]; ?></p>
                                <input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
                                <input type="hidden" name="txtPoName[]" id="txtPoName_<?php echo $i; ?>" value="<?php echo $row[csf('po_number')]; ?>">
                                <input type="hidden" name="buyerId[]" id="buyerId_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('buyer_name')]; ?>">
                            </td>
                            <td width="170" align="right">
                                <?php echo $row[csf('po_qnty_in_pcs')]; ?>
                                <input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
                            </td>
                            <td align="center">
                                <input type="text" name="txtIssueQnty[]" id="txtIssueQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:100px" value="<?php echo $iss_qty; ?>">
                            </td>
                        </tr>
                    <?php
                    $i++;
                    }
                    ?>
                    <input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
                </table>
            </div>
            <table width="620">
                 <tr>
                    <td align="center" >
                        <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                    </td>
                </tr>
            </table>
		</fieldset>
	</form>
</body>

<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}


if($action=="populate_data_about_order")
{
	$data=explode("**",$data);
	$order_id=$data[0];
	$prod_id=$data[1];
	
	
	$sql=sql_select("select sum(case when entry_form=17 then quantity end) as finish_fabric_recv, sum(case when entry_form=19 then quantity end) as finish_fabric_issue from order_wise_pro_details where po_breakdown_id in($order_id) and prod_id=$prod_id and is_deleted=0 and status_active=1");
	
	$finish_fabric_recv=$sql[0][csf('finish_fabric_recv')];
	$finish_fabric_issued=$sql[0][csf('finish_fabric_issue')];
	$yet_issue=$sql[0][csf('finish_fabric_recv')]-$sql[0][csf('finish_fabric_issue')];
	
	if($db_type==0)
	{
		$order_nos=return_field_value("group_concat(po_number) as po_number","wo_po_break_down","id in($order_id)","po_number");	
	}
	else if($db_type==2)
	{
		$order_nos=return_field_value("listagg((CAST(po_number as varchar2(4000))),',') within group (order by po_number) as po_number","wo_po_break_down","id in($order_id)","po_number");		
	}
	
	echo "$('#txt_order_numbers').val('".$order_nos."');\n";
	echo "$('#txt_fabric_received').val('".$finish_fabric_recv."');\n";
	echo "$('#txt_cumulative_issued').val('".$finish_fabric_issued."');\n";
	echo "$('#txt_yet_to_issue').val('".$yet_issue."');\n";
	
	exit();	
}

if ($action=="finishFabricIssue_popup")
{
	echo load_html_head_contents("Finish Fabric Issue Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
		
		function js_set_value(data)
		{
			$('#finish_fabric_issue_id').val(data);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:805px;">
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:800px;margin-left:3px">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" width="550" class="rpt_table">
                <thead>
                    <th>Search By</th>
                    <th width="240" id="search_by_td_up">Please Enter Issue No</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="finish_fabric_issue_id" id="finish_fabric_issue_id" class="text_boxes" value="">
                    </th>
                </thead>
                <tr class="general">
                    <td>
						<?php
							$search_by_arr=array(1=>"Issue No",2=>"Challan No.");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td id="search_by_td">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td>
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<?php echo $cbo_company_id; ?>, 'create_issue_search_list_view', 'search_div', 'woven_finish_fabric_issue_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
        	<div style="margin-top:10px" id="search_div"></div> 
		</fieldset>
	</form>
</div>    
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=='create_issue_search_list_view')
{
	$data = explode("_",$data);
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$company_id =$data[2];
	
	if($search_by==1)
		$search_field="issue_number";	
	else
		$search_field="challan_no";
	
 	$sql="select id, issue_number, challan_no, company_id, issue_date, issue_purpose, buyer_id, sample_type from inv_issue_master where item_category=3 and company_id=$company_id and $search_field like '$search_string' and entry_form=19 and status_active=1 and is_deleted=0";
	
	$company_short_name_arr = return_library_array("select id, company_short_name from lib_company","id","company_short_name");
	$sample_type_arr = return_library_array("select id, sample_name from lib_sample","id","sample_name");
	$arr=array(2=>$company_short_name_arr,4=>$yarn_issue_purpose,5=>$buyer_arr,6=>$sample_type_arr);

	echo  create_list_view("tbl_list_search", "Issue No,Challan No,Company,Issue Date,Issue Purpose,Buyer, Sample Type", "120,90,80,80,110,100","795","250",0, $sql, "js_set_value", "id", "", 1, "0,0,company_id,0,issue_purpose,buyer_id,sample_type", $arr, "issue_number,challan_no,company_id,issue_date,issue_purpose,buyer_id,sample_type", '','','0,0,0,3,0,0,0');
	
	exit();
}

if($action=='populate_data_from_issue_master')
{
	
	$data_array=sql_select("select issue_number, challan_no, company_id, issue_date, issue_purpose, buyer_id, sample_type, knit_dye_source, knit_dye_company from inv_issue_master where id='$data'");
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('update_id').value 					= '".$data."';\n";
		echo "document.getElementById('txt_system_id').value 				= '".$row[csf("issue_number")]."';\n";
		echo "document.getElementById('cbo_issue_purpose').value 			= '".$row[csf("issue_purpose")]."';\n";
		
		echo "active_inactive(".$row[csf("issue_purpose")].",0);\n";
		
		echo "document.getElementById('cbo_sample_type').value 				= '".$row[csf("sample_type")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "document.getElementById('txt_challan_no').value 				= '".$row[csf("challan_no")]."';\n";
		echo "document.getElementById('txt_issue_date').value 				= '".change_date_format($row[csf("issue_date")])."';\n";
		echo "document.getElementById('cbo_sewing_source').value 			= '".$row[csf("knit_dye_source")]."';\n";
		
		echo "load_drop_down( 'requires/woven_finish_fabric_issue_controller', '".$row[csf('knit_dye_source')]."'+'_'+'".$row[csf('company_id')]."', 'load_drop_down_sewing_com','sewingcom_td');\n";
		
		echo "document.getElementById('cbo_sewing_company').value 			= '".$row[csf("knit_dye_company")]."';\n";
		echo "document.getElementById('cbo_buyer_name').value 				= '".$row[csf("buyer_id")]."';\n";
		
		echo "$('#cbo_company_id').attr('disabled','disabled');\n";
		echo "$('#cbo_issue_purpose').attr('disabled','disabled');\n";
		
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_fabric_issue_entry',1,1);\n"; 
		
		exit();
	}
}

if($action=="show_finish_fabric_issue_listview")
{
	$product_arr = return_library_array("select id, product_name_details from product_details_master where item_category_id=3","id","product_name_details");
	$po_arr = return_library_array("select id, po_number from wo_po_break_down","id","po_number");
	$sql="select id, batch_lot, prod_id, issue_qnty, store_id, no_of_roll, order_id from inv_wvn_finish_fab_iss_dtls where mst_id='$data' and status_active =1 and is_deleted =0";
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="820" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
              	<th width="100">Batch/lot</th>
                <th width="200">Fabric Description</th>
                <th width="100">Issue Quantity</th>
                <th width="80">No Of Roll</th>
                <th width="110">Store</th>
                <th>Order Numbers</th>
            </thead>
        </table>
        <div style="width:820px; overflow-y:scroll; max-height:240px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1; 
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $row)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					
					$order_nos='';	
					$order_id=explode(",",$row[csf('order_id')]);
					foreach($order_id as $po_id)
					{
						if($po_id>0) $order_nos.=$po_arr[$po_id].",";
					}
					$order_nos=chop($order_nos,",");
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>)"> 
                            <td width="40" align="center"><?php echo $i; ?></td>	
                            <td width="100"><p><?php echo $row[csf('batch_lot')]; ?></p></td>
                            <td width="200"><p><?php echo $product_arr[$row[csf('prod_id')]]; ?></p></td>
                            <td width="100" align="right"><?php echo $row[csf('issue_qnty')]; ?></td>
                            <td width="80" align="right"><?php echo $row[csf('no_of_roll')]; ?></td> 
                            <td width="110"><p><?php echo $store_arr[$row[csf('store_id')]]; ?></p></td>
                            <td><p><?php echo $order_nos; ?></p></td>
                        </tr>
                    <?php
                    $i++;
				}
			?>
            </table>
        </div>
	</div>   
    <?php
	exit();
}

if($action=='populate_issue_details_form_data')
{
	$data=explode("**",$data);
	$id=$data[0];
	$roll_maintained=$data[1];
	
	$data_array=sql_select("select id, mst_id, trans_id, batch_lot, prod_id, issue_qnty, store_id, no_of_roll, order_id, order_save_string, roll_save_string from inv_wvn_finish_fab_iss_dtls where id='$id'");
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('update_dtls_id').value 				= '".$row[csf("id")]."';\n";
		echo "document.getElementById('cbo_store_name').value 				= '".$row[csf("store_id")]."';\n";
		echo "document.getElementById('txt_batch_lot').value 				= '".$row[csf("batch_lot")]."';\n";

		$prodData=sql_select("select current_stock, product_name_details from product_details_master where id='".$row[csf('prod_id')]."'");
		$product_details=$prodData[0][csf("product_name_details")];
		$current_stock=$prodData[0][csf("current_stock")];
		
		echo "document.getElementById('txt_fabric_desc').value 				= '".$product_details."';\n";
		echo "document.getElementById('hidden_prod_id').value 				= '".$row[csf("prod_id")]."';\n";
		echo "document.getElementById('previous_prod_id').value 			= '".$row[csf("prod_id")]."';\n";
		echo "document.getElementById('txt_issue_qnty').value 				= '".$row[csf("issue_qnty")]."';\n";
		echo "document.getElementById('hidden_issue_qnty').value 			= '".$row[csf("issue_qnty")]."';\n";
		echo "document.getElementById('all_po_id').value 					= '".$row[csf("order_id")]."';\n";
		echo "document.getElementById('save_string').value 					= '".$row[csf("roll_save_string")]."';\n";
		echo "document.getElementById('save_data').value 					= '".$row[csf("order_save_string")]."';\n";
		echo "document.getElementById('txt_no_of_roll').value 				= '".$row[csf("no_of_roll")]."';\n";
		echo "document.getElementById('txt_global_stock').value 			= '".$current_stock."';\n";
		echo "document.getElementById('update_trans_id').value 				= '".$row[csf('trans_id')]."';\n";
		
		if($row[csf("order_id")]!="")
		{
			echo "get_php_form_data('".$row[csf('order_id')]."'+'**'+'".$row[csf('prod_id')]."', 'populate_data_about_order', 'requires/woven_finish_fabric_issue_controller' );\n";
		}
		echo "show_list_view('".$row[csf('batch_lot')]."', 'show_fabric_desc_listview','list_fabric_desc_container','requires/woven_finish_fabric_issue_controller','');\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_fabric_issue_entry',1,1);\n"; 
		exit();
	}
}

//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		$finish_fabric_issue_num=''; $finish_update_id=''; $product_id=$hidden_prod_id;
		
		$stock_sql=sql_select("select current_stock, color from product_details_master where id=$product_id");
		
		$curr_stock_qnty=$stock_sql[0][csf('current_stock')];
		$color_id=$stock_sql[0][csf('color')];

		if(str_replace("'","",$txt_issue_qnty)>$curr_stock_qnty)
		{
			echo "17**0"; 
			die;			
		}
		
		if(str_replace("'","",$update_id)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$new_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'WFFI', date("Y",time()), 5, "select issue_number_prefix, issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_id and entry_form=19 and $year_cond=".date('Y',time())." order by id desc ", "issue_number_prefix", "issue_number_prefix_num" ));
		 	
			$id=return_next_id( "id", "inv_issue_master", 1 ) ;
					 
			$field_array="id, issue_number_prefix, issue_number_prefix_num, issue_number, issue_purpose, entry_form, item_category, company_id, sample_type, issue_date, challan_no, knit_dye_source, knit_dye_company, buyer_id, inserted_by, insert_date";
			
			$data_array="(".$id.",'".$new_system_id[1]."',".$new_system_id[2].",'".$new_system_id[0]."',".$cbo_issue_purpose.",19,3,".$cbo_company_id.",".$cbo_sample_type.",".$txt_issue_date.",".$txt_challan_no.",".$cbo_sewing_source.",".$cbo_sewing_company.",".$cbo_buyer_name.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into inv_issue_master (".$field_array.") values ".$data_array;die;
			/*$rID=sql_insert("inv_issue_master",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0; */
			
			$finish_fabric_issue_num=$new_system_id[0];
			$finish_update_id=$id;
		}
		else
		{
			$field_array_update="sample_type*issue_date*challan_no*knit_dye_source*knit_dye_company*buyer_id*updated_by*update_date";
			$data_array_update=$cbo_sample_type."*".$txt_issue_date."*".$txt_challan_no."*".$cbo_sewing_source."*".$cbo_sewing_company."*".$cbo_buyer_name."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			/*$rID=sql_update("inv_issue_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($rID) $flag=1; else $flag=0; */
			
			$finish_fabric_issue_num=str_replace("'","",$txt_system_id);
			$finish_update_id=str_replace("'","",$update_id);
		}
		
		$id_trans=return_next_id( "id", "inv_transaction", 1 ) ;
		$field_array_trans="id, mst_id, company_id, prod_id, item_category, transaction_type, transaction_date, cons_uom, cons_quantity, issue_challan_no, store_id, batch_lot, inserted_by, insert_date";
		
		$data_array_trans="(".$id_trans.",".$finish_update_id.",".$cbo_company_id.",".$product_id.",3,2,".$txt_issue_date.",0,".$txt_issue_qnty.",".$txt_challan_no.",".$cbo_store_name.",".$txt_batch_lot.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
		//echo "insert into inv_transaction (".$field_array_trans.") values ".$data_array_trans;die;
		/*$rID2=sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} */
		
		$id_dtls=return_next_id( "id", "inv_wvn_finish_fab_iss_dtls", 1 ) ;
		$field_array_dtls="id, mst_id, trans_id, prod_id, issue_qnty, store_id, batch_lot, no_of_roll, order_id, roll_save_string, order_save_string, inserted_by, insert_date";
		
		$data_array_dtls="(".$id_dtls.",".$finish_update_id.",".$id_trans.",".$product_id.",".$txt_issue_qnty.",".$cbo_store_name.",".$txt_batch_lot.",".$txt_no_of_roll.",".$all_po_id.",".$save_string.",".$save_data.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
		//echo "insert into inv_finish_fabric_issue_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;
		/*$rID3=sql_insert("inv_wvn_finish_fab_iss_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1) 
		{
			if($rID3) $flag=1; else $flag=0; 
		} */
		
		$field_array_prod_update="last_issued_qnty*current_stock*updated_by*update_date";
		
		$curr_stock_qnty=$curr_stock_qnty-str_replace("'","",$txt_issue_qnty);		
		$data_array_prod_update=$txt_issue_qnty."*".$curr_stock_qnty."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0);
		if($flag==1) 
		{
			if($prod) $flag=1; else $flag=0; 
		}*/ 
		
		if(str_replace("'","",$roll_maintained)==1 && (str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==8 || str_replace("'","",$cbo_issue_purpose)==9))
		{
			$id_roll = return_next_id( "id", "pro_roll_details", 1 );
			
			$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, roll_id, inserted_by, insert_date";
		
			$save_string=explode(",",str_replace("'","",$save_string));
			for($i=0;$i<count($save_string);$i++)
			{
				if($i==0) $add_comma=""; else $add_comma=",";
				$roll_dtls=explode("_",$save_string[$i]);
				$roll_id=$roll_dtls[0];
				$roll_no=$roll_dtls[1];
				$roll_qnty=$roll_dtls[2];
				$order_id=$roll_dtls[3];
				
				$data_array_roll.="$add_comma(".$id_roll.",".$finish_update_id.",".$id_dtls.",'".$order_id."',19,'".$roll_qnty."','".$roll_no."','".$roll_id."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				$id_roll = $id_roll+1;
			}
			
			/*if($data_array_roll!="")
			{
				//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
				$rID4=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				}
			}*/
		}
		
		if(str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==9)
		{
			$field_array_proportionate="id, trans_id, trans_type, entry_form, dtls_id, po_breakdown_id, prod_id, color_id, quantity, inserted_by, insert_date";
			$id_prop = return_next_id( "id", "order_wise_pro_details", 1 );

			$save_data=explode(",",str_replace("'","",$save_data));
			for($i=0;$i<count($save_data);$i++)
			{
				$order_dtls=explode("_",$save_data[$i]);
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if($i==0) $add_comma=""; else $add_comma=",";
				
				$data_array_prop.="$add_comma(".$id_prop.",".$id_trans.",2,19,".$id_dtls.",'".$order_id."',".$product_id.",'".$color_id."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				$id_prop = $id_prop+1;
			}
			
			//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;	
			/*if($data_array_prop!="")
			{
				$rID5=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
				if($flag==1) 
				{
					if($rID5) $flag=1; else $flag=0; 
				} 
			}*/
		}
		
		//fast
		
		if(str_replace("'","",$update_id)=="")
		{
			$rID=sql_insert("inv_issue_master",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0; 
		}
		else
		{
			$rID=sql_update("inv_issue_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($rID) $flag=1; else $flag=0; 
		}
		
		$rID2=sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		}
		
		//echo "10**insert into inv_wvn_finish_fab_iss_dtls (".$field_array_dtls.") values ".$data_array_dtls;die; 
		$rID3=sql_insert("inv_wvn_finish_fab_iss_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1) 
		{
			if($rID3) $flag=1; else $flag=0; 
		} 
		
		$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0); //echo $prod;die;
		if($flag==1) 
		{
			if($prod) $flag=1; else $flag=0; 
		}
		
		if(str_replace("'","",$roll_maintained)==1 && (str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==8 || str_replace("'","",$cbo_issue_purpose)==9))
		{
			if($data_array_roll!="")
			{
				//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;
					
				$rID4=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				}
			}
		}
		
		//echo str_replace("'","",$roll_maintained);die;
		
		if(str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==9)
		{
			if($data_array_prop!="")
			{
				//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;
				
				$rID5=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
				if($flag==1) 
				{
					if($rID5) $flag=1; else $flag=0; 
				} 
			}
		}
		
		//echo str_replace("'","",$cbo_issue_purpose);die;
		//last
		//echo "10**".$flag;die;
		check_table_status( $_SESSION['menu_id'],0);

		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**0**".$finish_update_id."**".$finish_fabric_issue_num;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "0**0**".$finish_update_id."**".$finish_fabric_issue_num;
			}
			else
			{
				oci_rollback($con);
				echo "5**0**";
			}
		}
		//check_table_status( $_SESSION['menu_id'],0);
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }

		if(str_replace("'","",$roll_maintained)==1) $product_id=$hidden_prod_id; else $product_id=$hidden_prod_id;
		
		//echo $txt_issue_qnty;die;
		
		/*$stock=return_field_value("current_stock","product_details_master","id=$product_id");
		$adjust_curr_stock=$stock+str_replace("'", '',$hidden_issue_qnty);
		
		$adjust_prod=sql_update("product_details_master","current_stock",$adjust_curr_stock,"id",$product_id,0);
		if($adjust_prod) $flag=1; else $flag=0; 
		
		$stock_sql=sql_select("select current_stock, color from product_details_master where id=$product_id");
		
		$curr_stock_qnty=$stock_sql[0][csf('current_stock')];
		$color_id=$stock_sql[0][csf('color')];*/

		$stock_sql=sql_select("select current_stock, color from product_details_master where id=$product_id");
		$curr_stock_qnty=$stock_sql[0][csf('current_stock')];
		$color_id=$stock_sql[0][csf('color')];
		$field_array_prod_update="last_issued_qnty*current_stock*updated_by*update_date";
		
		if($product_id==$previous_prod_id)
		{
			$curr_stock_qnty=$curr_stock_qnty-str_replace("'","",$txt_issue_qnty)+str_replace("'", '',$hidden_issue_qnty);		
			$data_array_prod_update=$txt_issue_qnty."*".$curr_stock_qnty."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$latest_current_stock=$curr_stock_qnty+str_replace("'", '',$hidden_issue_qnty);		
			/*$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0);
			if($flag==1) 
			{
				if($prod) $flag=1; else $flag=0; 
			} */
		}
		else
		{
			$stock=return_field_value("current_stock","product_details_master","id=$previous_prod_id");
			$adjust_curr_stock=$stock+str_replace("'", '',$hidden_issue_qnty);
			/*$adjust_prod=sql_update("product_details_master","current_stock",$adjust_curr_stock,"id",$previous_prod_id,0);
			if($flag==1) 
			{
				if($adjust_prod) $flag=1; else $flag=0; 
			} */
			
			$latest_current_stock=$curr_stock_qnty;
			
			$curr_stock_qnty=$curr_stock_qnty-str_replace("'","",$txt_issue_qnty);		
			$data_array_prod_update=$txt_issue_qnty."*".$curr_stock_qnty."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			/*$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0);
			if($flag==1) 
			{
				if($prod) $flag=1; else $flag=0; 
			} */
		}
		
		if(str_replace("'","",$txt_issue_qnty)>$latest_current_stock)
		{
			echo "17**0"; 
			die;			
		}
		
		//New by shajjad

		/*if(str_replace("'","",$txt_issue_qnty)>$curr_stock_qnty)
		{
			echo "17**0"; 
			die;			
		}*/
		
		$field_array_update="sample_type*issue_date*challan_no*knit_dye_source*knit_dye_company*buyer_id*updated_by*update_date";
			
		$data_array_update=$cbo_sample_type."*".$txt_issue_date."*".$txt_challan_no."*".$cbo_sewing_source."*".$cbo_sewing_company."*".$cbo_buyer_name."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$rID=sql_update("inv_issue_master",$field_array_update,$data_array_update,"id",$update_id,1);
		if($flag==1)
		{
			if($rID) $flag=1; else $flag=0; 
		}*/
		
		$field_array_trans="prod_id*transaction_date*store_id*cons_quantity*issue_challan_no*batch_lot*updated_by*update_date";
		$data_array_trans=$product_id."*".$txt_issue_date."*".$cbo_store_name."*".$txt_issue_qnty."*".$txt_challan_no."*".$txt_batch_lot."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";

		/*$rID2=sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_trans_id);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		}*/
		
		$field_array_dtls="prod_id*issue_qnty*store_id*batch_lot*no_of_roll*order_id*roll_save_string*order_save_string*updated_by*update_date";
		
		$data_array_dtls=$product_id."*".$txt_issue_qnty."*".$cbo_store_name."*".$txt_batch_lot."*".$txt_no_of_roll."*".$all_po_id."*".$save_string."*".$save_data."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";

		/*$rID3=sql_update("inv_wvn_finish_fab_iss_dtls",$field_array_dtls,$data_array_dtls,"id",$update_dtls_id,0);
		if($flag==1) 
		{
			if($rID3) $flag=1; else $flag=0; 
		}*/
		
		//$field_array_prod_update="last_issued_qnty*current_stock*updated_by*update_date";
		
		//$curr_stock_qnty=$curr_stock_qnty-str_replace("'","",$txt_issue_qnty);		
		//$data_array_prod_update=$txt_issue_qnty."*".$curr_stock_qnty."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0);
		if($flag==1) 
		{
			if($prod) $flag=1; else $flag=0; 
		} */
		
		/*$delete_roll=execute_query( "delete from pro_roll_details where dtls_id=$update_dtls_id and entry_form=19",0);
		if($flag==1) 
		{
			if($delete_roll) $flag=1; else $flag=0; 
		} 
		
		$delete_prop=execute_query( "delete from order_wise_pro_details where dtls_id=$update_dtls_id and trans_id=$update_trans_id and entry_form=19",0);
		if($flag==1) 
		{
			if($delete_prop) $flag=1; else $flag=0; 
		}*/
		
		if(str_replace("'","",$roll_maintained)==1 && (str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==8 || str_replace("'","",$cbo_issue_purpose)==9))
		{
			$id_roll = return_next_id( "id", "pro_roll_details", 1 );
			
			$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, roll_id, inserted_by, insert_date";
		
			$save_string=explode(",",str_replace("'","",$save_string));
			for($i=0;$i<count($save_string);$i++)
			{
				if($i==0) $add_comma=""; else $add_comma=",";
				$roll_dtls=explode("_",$save_string[$i]);
				$roll_id=$roll_dtls[0];
				$roll_no=$roll_dtls[1];
				$roll_qnty=$roll_dtls[2];
				$order_id=$roll_dtls[3];
				
				$data_array_roll.="$add_comma(".$id_roll.",".$update_id.",".$update_dtls_id.",'".$order_id."',18,'".$roll_qnty."','".$roll_no."','".$roll_id."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				$id_roll = $id_roll+1;
			}
			
			/*if($data_array_roll!="")
			{
				//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
				$rID4=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				}
			}*/
		}
		
		if(str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==9)
		{
			$field_array_proportionate="id, trans_id, trans_type, entry_form, dtls_id, po_breakdown_id, prod_id, color_id, quantity, inserted_by, insert_date";
			$id_prop = return_next_id( "id", "order_wise_pro_details", 1 );

			$save_data=explode(",",str_replace("'","",$save_data));
			for($i=0;$i<count($save_data);$i++)
			{
				$order_dtls=explode("_",$save_data[$i]);
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if($i==0) $add_comma=""; else $add_comma=",";
				
				$data_array_prop.="$add_comma(".$id_prop.",".$update_trans_id.",2,19,".$update_dtls_id.",'".$order_id."',".$product_id.",'".$color_id."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				$id_prop = $id_prop+1;
			}
			
			//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;	
			/*if($data_array_prop!="")
			{
				$rID5=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
				if($flag==1) 
				{
					if($rID5) $flag=1; else $flag=0; 
				} 
			}*/
		}
		
		//first
		
		
			if(str_replace("'","",$product_id)==str_replace("'","",$previous_prod_id))
			{
				$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0); 
				if($prod) $flag=1; else $flag=0; 
			}
			else
			{
				$adjust_prod=sql_update("product_details_master","current_stock",$adjust_curr_stock,"id",$previous_prod_id,0);
				if($adjust_prod) $flag=1; else $flag=0; 
				
				$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0);
				if($flag==1) 
				{
					if($prod) $flag=1; else $flag=0; 
				} 
			}
			
			$rID=sql_update("inv_issue_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($flag==1)
			{
				if($rID) $flag=1; else $flag=0; 
			}
			
			$rID2=sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_trans_id);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			}
			
			$rID3=sql_update("inv_wvn_finish_fab_iss_dtls",$field_array_dtls,$data_array_dtls,"id",$update_dtls_id,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			}
			
			/*$prod=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$product_id,0);
			if($flag==1) 
			{
				if($prod) $flag=1; else $flag=0; 
			}*/
			
			$delete_roll=execute_query( "delete from pro_roll_details where dtls_id=$update_dtls_id and entry_form=19",0);
			if($flag==1) 
			{
				if($delete_roll) $flag=1; else $flag=0; 
			} 
			
			$delete_prop=execute_query( "delete from order_wise_pro_details where dtls_id=$update_dtls_id and trans_id=$update_trans_id and entry_form=19",0);
			if($flag==1) 
			{
				if($delete_prop) $flag=1; else $flag=0; 
			} 
			
			if(str_replace("'","",$roll_maintained)==1 && (str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==8 || str_replace("'","",$cbo_issue_purpose)==9))
			{
				if($data_array_roll!="")
				{
					//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
					$rID4=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
					if($flag==1) 
					{
						if($rID4) $flag=1; else $flag=0; 
					}
				}
			}
			if(str_replace("'","",$cbo_issue_purpose)==4 || str_replace("'","",$cbo_issue_purpose)==9)
			{
				if($data_array_prop!="")
				{
					$rID5=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
					if($flag==1) 
					{
						if($rID5) $flag=1; else $flag=0; 
					} 
				}
			}
		
		
		//last

		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**0**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**1";
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con); 
				echo "1**0**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_system_id);
			}
			else
			{
				oci_rollback($con);
				echo "6**1";
			}
		}	
		disconnect($con);
		die;
 	}
}

if ($action=="woven_finish_fabric_issue_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql="select id, issue_number, issue_purpose, sample_type, issue_date, challan_no, knit_dye_source, knit_dye_company, buyer_id from  inv_issue_master where id='$data[1]' and company_id='$data[0]' and entry_form=19";
	//echo $sql;die;
	$dataArray=sql_select($sql);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$supplier_arr=return_library_array( "select id, supplier_name from  lib_supplier", "id", "supplier_name"  );
	$sample_arr=return_library_array( "select id, sample_name from  lib_sample", "id", "sample_name"  );
	$product_arr = return_library_array("select id, product_name_details from product_details_master where item_category_id=3","id","product_name_details");
?>
<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u><?php echo $data[2]; ?> Report</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Issue ID :</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="130"><strong>Issue Purpose:</strong></td> <td width="175px"><?php echo $yarn_issue_purpose[$dataArray[0][csf('issue_purpose')]]; ?></td>
            <td width="125"><strong>Sample Type:</strong></td><td width="175px"><?php echo $sample_arr[$dataArray[0][csf('sample_type')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Issue Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
            <td><strong>Challan No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Sewing Source:</strong></td> <td width="175px"><?php echo $knitting_source[$dataArray[0][csf('knit_dye_source')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Sewing Com:</strong></td><td width="175px"><?php if ($dataArray[0][csf('knit_dye_source')]==1) echo $company_library[$dataArray[0][csf('knit_dye_company')]]; else if ($dataArray[0][csf('knit_dye_source')]==3) echo $supplier_arr[$dataArray[0][csf('knit_dye_company')]];  ?></td>
            <td><strong>Buyer Name:</strong></td><td width="175px"><?php echo $buyer_library[$dataArray[0][csf('buyer_id')]]; ?></td>
            <td><strong>&nbsp;</strong></td> <td width="175px"><?php //echo $currency[$dataArray[0][csf('currency_id')]]; ?></td>
        </tr>
    </table>
        <br>
    <div style="width:100%;">
    <table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="80" >Batch/Lot</th>
            <th width="180" >Fabric Description</th>
            <th width="80" >Issue Quantity</th>
            <th width="70" >No Of Roll</th>
            <th width="100" >Order Numbers</th>
            <th width="110" >Store</th>
        </thead>
        <tbody> 
<?php
	
	$sql_dtls="select id, batch_lot, prod_id, issue_qnty, store_id, no_of_roll, order_id from  inv_wvn_finish_fab_iss_dtls where mst_id='$data[1]' and status_active=1 and is_deleted=0";
	//$sql_dtls="select id, prod_id, issue_qnty, no_of_roll, order_id, store_id from inv_finish_fabric_issue_dtls where mst_id='$data[1]' and status_active=1 and is_deleted= 0";
	
	$sql_result= sql_select($sql_dtls);
	$i=1;
	foreach($sql_result as $row)
	{
		if ($i%2==0)$bgcolor="#E9F3FF";						
		else $bgcolor="#FFFFFF";
	
		if($row[csf('order_id')]!="")
		{
			if($db_type==0)
			{
				$order_nos=return_field_value("group_concat(po_number) as po_no","wo_po_break_down","id in(".$row[csf('order_id')].")","po_no");
			}
			else
			{
				$order_nos=return_field_value("listagg((CAST(po_number as varchar2(4000))),',') within group (order by po_number) as po_number","wo_po_break_down","id in(".$row[csf('order_id')].")","po_number");		
			}
		}
		else
			$order_nos='';
		
		$totalQnty +=$row[csf("issue_qnty")];
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <td><?php echo $row[csf("batch_lot")]; ?></td>
                <td><?php echo $product_arr[$row[csf("prod_id")]]; ?></td>
                <td align="right"><?php echo $row[csf("issue_qnty")]; ?></td>
                <td align="center"><?php echo $row[csf("no_of_roll")]; ?></td>
                <td><?php echo $order_nos; ?></td>
                <td><?php echo $store_library[$row[csf("store_id")]]; ?></td>
			</tr>
	<?php $i++; 
    } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" align="right"><strong>Total :</strong></td>
                <td align="right"><?php echo $totalQnty; ?></td>
                <td align="right" colspan="3"><?php // echo $totalAmount; ?></td>
            </tr>                           
        </tfoot>
      </table>
        <br>
		 <?php
            echo signature_table(22, $data[0], "900px");
         ?>
      </div>
   </div> 
<?php
exit();	
}
?>
