<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_location")
{
	//echo $data; die;
	//echo create_drop_down( "cbo_location_name", 262, "select location_name,id from lib_location where  company_id='$data' and is_deleted=0  and status_active=1  order by location_name",'id,location_name', 1, '--- Select Location ---', 0, '' );
	 
echo create_drop_down( "cbo_location_name", 262, "select location_name,id from lib_location where company_id='$data' and is_deleted=0  and status_active=1  order by location_name",'id,location_name', 1, '--- Select Location ---', 0, "load_drop_down( 'requires/sewing_line_controller', this.value, 'load_drop_down_floor', 'floor' )"  );
}

if ($action=="load_drop_down_floor")
{
	//echo $data; die;
	echo create_drop_down( "cbo_floor_name", 262, "select floor_name,id from  lib_prod_floor where location_id='$data' and is_deleted=0  and status_active=1  order by floor_name",'id,floor_name', 1, '--- Select Floor ---', 0, '' );
	 

}

if ($action=="sewing_line_list_view")
{
	$floor=return_library_array( "select floor_name,id from  lib_prod_floor where is_deleted=0", "id", "floor_name"  );
	$arr=array(2=>$floor);
	echo  create_list_view ( "list_view", "Company Name,Location Name,Floor Name,Sewing Line, Line Serial", "150,150,100,100","650","220",1, "select c.company_name,l.location_name,a.floor_name, a.sewing_line_serial,a.line_name,a.id from lib_sewing_line a, lib_company c, lib_location l  where a.company_name=c.id and a.location_name=l.id and  a.is_deleted=0 order by a.sewing_line_serial", "get_php_form_data", "id","'load_php_data_to_form'", 1, "0,0,floor_name", $arr , "company_name,location_name,floor_name,line_name,sewing_line_serial", "../production/requires/sewing_line_controller", 'setFilterGrid("list_view",-1);' ) ;	

}

if ($action=="load_php_data_to_form")
{
	$nameArray=sql_select( "select company_name,location_name,floor_name,sewing_line_serial,line_name,status_active,id from  lib_sewing_line where id='$data'" );
	foreach ($nameArray as $inf)
	{
		echo "load_drop_down( 'requires/sewing_line_controller', '".($inf[csf("company_name")])."', 'load_drop_down_location', 'location' );\n";
		echo "load_drop_down( 'requires/sewing_line_controller', '".($inf[csf("location_name")])."', 'load_drop_down_floor', 'floor' );\n";
		echo "document.getElementById('cbo_company_name').value = '".($inf[csf("company_name")])."';\n";    
		echo "document.getElementById('cbo_location_name').value  = '".($inf[csf("location_name")])."';\n"; 
		echo "document.getElementById('cbo_floor_name').value  = '".($inf[csf("floor_name")])."';\n";
		echo "document.getElementById('txt_sewing_line_serial').value  = '".($inf[csf("sewing_line_serial")])."';\n";
		echo "document.getElementById('txt_line_name').value  = '".($inf[csf("line_name")])."';\n";
		echo "document.getElementById('cbo_status').value  = '".($inf[csf("status_active")])."';\n";
		echo "document.getElementById('update_id').value  = '".($inf[csf("id")])."';\n"; 
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_sewing_line_info',1);\n";  

	}
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		if (is_duplicate_field( "line_name", "lib_sewing_line", " line_name=$txt_line_name and company_name=$cbo_company_name and location_name=$cbo_location_name and floor_name=$cbo_floor_name and is_deleted=0" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			//cbo_company_name,cbo_location_name,cbo_floor_name,txt_sewing_line_serial,txt_line_name,cbo_status,update_id
			//company_name,location_name,floor_name,sewing_line_serial,line_name,status_active,id
			
			$id=return_next_id( "id", " lib_sewing_line", 1 ) ;
			$field_array="id,company_name,location_name,floor_name,sewing_line_serial,line_name,inserted_by,insert_date,status_active,is_deleted";
			$data_array="(".$id.",".$cbo_company_name.",".$cbo_location_name.",".$cbo_floor_name.",".$txt_sewing_line_serial.",".$txt_line_name.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',".$cbo_status.",0)";
			$rID=sql_insert("lib_sewing_line",$field_array,$data_array,1);
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo "0**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			
			if($db_type==2 || $db_type==1 )
			{
				 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		}
	}
	
	else if ($operation==1)   // Update Here
	{
		if (is_duplicate_field( "line_name", "lib_sewing_line", " line_name=$txt_line_name and company_name=$cbo_company_name and location_name=$cbo_location_name and id!=$update_id and floor_name=$cbo_floor_name and is_deleted=0" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			$field_array="company_name*location_name*floor_name*sewing_line_serial*line_name*updated_by*update_date*status_active*is_deleted";
			$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_floor_name."*".$txt_sewing_line_serial."*".$txt_line_name."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*".$cbo_status."*0";
			
			 
			$rID=sql_update("lib_sewing_line",$field_array,$data_array,"id","".$update_id."",1);
			 
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo "1**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
		   disconnect($con);
		   die;
		}
		
	}
	
	else if ($operation==2)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array="updated_by*update_date*status_active*is_deleted";
	    $data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
		
		$rID=sql_delete("lib_sewing_line",$field_array,$data_array,"id","".$update_id."",1);
		
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "1**".$rID;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$rID;
			}
		}
		if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "2**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
		  disconnect($con);
		  die;
		
	}
}


?>