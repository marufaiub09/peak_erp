<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create subcontract kniting production
Functionality	:	
JS Functions	:
Created by		:	sohel
Creation date 	: 	08-05-2013
Updated by 		: 		
Update date		: 
Oracle Convert 	:	Kausar		
Convert date	: 	22-05-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Grey Production Entry", "../",1, 1,$unicode,1,'');
?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
	var permission='<?php echo $permission; ?>';

	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];

	$(document).ready(function(e)
	 {
            $("#txt_color").autocomplete({
			 source: str_color
		  });
     });
	 
	function openmypage_production()
	{
		var data=document.getElementById('cbo_company_id').value+"_"+document.getElementById('cbo_party_name').value;
		emailwindow=dhtmlmodal.open('EmailBox','iframe', 'requires/subcon_kniting_production_controller.php?action=production_id_popup&data='+data,'Kniting Production Popup', 'width=900px, height=400px, center=1, resize=0, scrolling=0','')
		emailwindow.onclose=function()
		{
			var theemail=this.contentDoc.getElementById("product_id");
			if (theemail.value!="")
			{
				freeze_window(5);
				get_php_form_data(theemail.value, "load_php_data_to_form_mst", "requires/subcon_kniting_production_controller" );
				show_list_view(theemail.value,'kniting_production_list_view','kniting_production_list_view','requires/subcon_kniting_production_controller','setFilterGrid("list_view",-1)');
				
				document.getElementById('cbo_company_id').disabled=true;
				document.getElementById('cbo_party_name').disabled=true;
				$('#cbo_body_part').focus();
				 reset_form('','','','','','txt_production_id*cbo_company_id*cbo_location_name*cbo_party_name*txt_production_date*txt_prod_chal_no*txt_yarn_issue_challan_no*txt_remarks*update_id**');
				release_freezing();
			}
		}
	}


	function subcon_kniting_production(operation)
	{ 
		if ( form_validation('cbo_company_id*cbo_party_name*txt_production_date*cbo_process*txt_febric_description*txt_gsm*txt_width*txt_order_no*txt_product_qnty*cbo_yarn_count*cbo_machine_name','Company Name*Party Name*production date*process*febric description*GSM*Dia Width*Order No*Product Qnty*Yarn Count*Machine Name')==false )
		{
			return;
		}
		else
		{
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_production_id*cbo_company_id*cbo_location_name*cbo_party_name*txt_production_date*txt_prod_chal_no*txt_yarn_issue_challan_no*cbo_floor_id*txt_remarks*update_id*cbo_process*txt_yarn_lot*txt_febric_description*hidd_comp_id*txt_gsm*cbo_yarn_count*txt_width*cbo_dia_width_type*txt_brand*txt_roll_qnty*cbo_shift_id*txt_order_no*order_no_id*cbo_machine_name*txt_product_qnty*txt_job_no*txt_reject_qnty*text_new_remarks*cbo_uom*txt_stitch_len*cbo_color_range*txt_color*update_id_dtl',"../");
			
			freeze_window(operation);
			http.open("POST","requires/subcon_kniting_production_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = subcon_kniting_production_reponse;
		}
	}

	function subcon_kniting_production_reponse()
	{
		if(http.readyState == 4) 
		{
			//alert (http.responseText);//return;
			var reponse=trim(http.responseText).split('**');
			if(reponse[0]==0 || reponse[0]==1)
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_production_id').value = reponse[2];
				show_list_view(reponse[1],'kniting_production_list_view','kniting_production_list_view','requires/subcon_kniting_production_controller','setFilterGrid("list_view",-1)');
				show_msg(reponse[0]);
				
				$('#cbo_body_part').focus();
				reset_form('knitingproduction_1','','','','','txt_production_id*cbo_company_id*cbo_location_name*cbo_party_name*txt_production_date*txt_prod_chal_no*txt_yarn_issue_challan_no*txt_remarks*update_id**');
				$('#list_fabric_desc_container').html('')
			}
			set_button_status(0, permission, 'subcon_kniting_production',1);
			release_freezing();
		}
	}

	function openmypage_order_no()
	{
		if( form_validation('cbo_company_id*cbo_party_name','Company Name*Party Name')==false )
		{
			return;
		}
		else
		{
			var data=document.getElementById('cbo_company_id').value+"_"+document.getElementById('cbo_party_name').value+"_"+document.getElementById('cbo_process').value;
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/subcon_kniting_production_controller.php?action=order_no_popup&data='+data,'Order Selection Form', 'width=800px,height=420px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				freeze_window(5);
				var theemail=this.contentDoc.getElementById("selected_job");
				get_php_form_data( theemail.value, "load_php_data_to_form_dtls_order", "requires/subcon_kniting_production_controller" );
				show_list_view(document.getElementById('order_no_id').value+"_"+document.getElementById('process_id').value,'show_fabric_desc_listview','list_fabric_desc_container','requires/subcon_kniting_production_controller','');
				release_freezing();		
			}
		}
	}

	function set_form_data(data)
	{
		var data=data.split("**");
		$('#hidd_comp_id').val(data[0]);
		$('#txt_febric_description').val(data[1]);
		$('#txt_gsm').val(data[2]);
	}

</script>
<body onLoad="set_hotkey();">
<div style="width:100%;">   
	<?php echo load_freeze_divs ("../",$permission);  ?>
    <form name="knitingproduction_1" id="knitingproduction_1">
        <fieldset style="width:850px ">
        <legend>Kniting  Production</legend>
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td colspan="3">
                        <fieldset>
                        <table cellpadding="0" cellspacing="2" width="100%">
                            <tr>
                                <td align="right" colspan="3"><strong> Production ID </strong></td>
                                <td width="140" align="justify">
                                    <input type="hidden" name="update_id" id="update_id" />
                                    <input type="text" name="txt_production_id" id="txt_production_id" class="text_boxes" style="width:140px" placeholder="Double Click" onDblClick="openmypage_production();" tabindex="1" readonly >
                                </td>
                            </tr>
                            <tr>
                                <td width="120" class="must_entry_caption" >Company Name</td>
                                <td width="150">
									<?php 
										echo create_drop_down( "cbo_company_id",150,"select id,company_name from lib_company comp where is_deleted=0 and status_active=1 $company_cond order by company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down( 'requires/subcon_kniting_production_controller', this.value, 'load_drop_down_location', 'location_td'); load_drop_down( 'requires/subcon_kniting_production_controller', this.value, 'load_drop_down_party_name', 'party_td' );load_drop_down( 'requires/subcon_kniting_production_controller', this.value, 'load_drop_down_floor', 'floor_td' ); load_drop_down( 'requires/subcon_kniting_production_controller', this.value, 'load_drop_down_machine', 'machine_td' );","","","","","",2);	
                                    ?>
                                </td>
                                <td width="120">Location </td>                                              
                                <td width="150" id="location_td">
									<?php 
										echo create_drop_down( "cbo_location_name", 150, $blank_array,"", 1, "--Select Location--", $selected,"","","","","","",3);
                                    ?>
                                </td>
                                <td width="120" class="must_entry_caption">Party Name</td>
                                <td width="140" id="party_td">
									<?php
										echo create_drop_down( "cbo_party_name", 150, $blank_array,"", 1, "-- Select Party --", $selected, "",0,"","","","",4);
                                    ?> 
                                </td>
                            </tr> 
                            <tr>
                                <td width="120" class="must_entry_caption">Production Date</td>
                                <td width="140">
                                    <input class="datepicker" type="text" style="width:140px" name="txt_production_date" id="txt_production_date" tabindex="5" />
                                </td>
                                <td width="120"> Prod. Challan No </td>
                                <td width="140">
                                    <input type="text" name="txt_prod_chal_no" id="txt_prod_chal_no" class="text_boxes" style="width:140px" tabindex="6" >
                                </td>
                                <td width="120">Yarn Issue Ch. No</td>                                              
                                <td width="140"> 
                                    <input type="text" name="txt_yarn_issue_challan_no" id="txt_yarn_issue_challan_no" class="text_boxes" style="width:140px" tabindex="7" >
                                </td> 
                            </tr>
                            <tr>
                                <td width="120">Remarks </td>                                              
                                <td colspan="3"> 
                                    <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:420px" maxlength="150" title="Maximum 150 Character" tabindex="8">
                                </td> 
                            </tr>                                      
                        </table>
                        </fieldset>
                    </td>
                </tr>
                <tr align="center">
                    <td width="64%" valign="top" style="margin-left:10px;">
                        <fieldset style="width:600px">
                        <legend>New Entry</legend>
                        <table  cellpadding="0" cellspacing="2" width="100%" align="center">
                            <tr>
                                <td style="width:120px ;" class="must_entry_caption">Order No</td>
                                <td>
                                	<input type="hidden" name="update_id_dtl" id="update_id_dtl" />
                                	<input type="hidden" name="order_no_id" id="order_no_id" />
                                    <input type="hidden" name="process_id" id="process_id" />
                                    <input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:140px;" placeholder="Double Click to Search" onDblClick="openmypage_order_no();" readonly tabindex="14" />	
                                </td>
                                <td  style="width:120px;">Yarn Lot</td>
                                <td  style="width:140px;">
                                    <input type="text" name="txt_yarn_lot" id="txt_yarn_lot" class="text_boxes" style="width:140px"  tabindex="17" /> 
                                </td>  
                            </tr>
                            <tr>
                                <td class="must_entry_caption">Process</td>
                                <td>
									<?php 
										echo create_drop_down("cbo_process", 150, $conversion_cost_head_array,"", 1, "--Select Process--",$selected,"", "","1,3,4","","","",9);
                                    ?>
                                </td>
                                <td class="must_entry_caption">Yarn Count</td>
                                <td>
									<?php
										echo create_drop_down("cbo_yarn_count",150,"select id,yarn_count from  lib_yarn_count where is_deleted=0 and status_active=1 order by yarn_count","id,yarn_count",1, "-- Select --", $selected, "","","","","","",18);
                                    ?>
                                </td>   
                            </tr>
                            <tr>
                                <td class="must_entry_caption">Const. Compo.</td>
                                <td colspan="3"><input type="hidden" name="hidd_comp_id" id="hidd_comp_id" />
                                    <input type="text" name="txt_febric_description" id="txt_febric_description" class="text_boxes" style="width:460px" tabindex="10" readonly />
                                </td>
                            </tr> 
                            <tr>
                                <td class="must_entry_caption">GSM</td>
                                <td>
                                    <input type="text" name="txt_gsm" id="txt_gsm" class="text_boxes_numeric" style="width:140px;"  tabindex="11"/>	
                                </td>
                                <td>Brand</td>
                                <td>
                                    <input type="text" name="txt_brand" id="txt_brand" class="text_boxes" style="width:140px" tabindex="19"/> 
                                </td>
                            </tr>
                            <tr>
                                <td class="must_entry_caption">Dia / Width</td>
                                <td>
                                    <input type="text" name="txt_width" id="txt_width" placeholder="Dia Only" class="text_boxes_numeric" style="width:60px" tabindex="12" /> 
									<?php
										echo create_drop_down( "cbo_dia_width_type", 70, $fabric_typee,"",1, "-Width-", 0, "" );
                                	?>	
                                </td>
                                <td>Shift Name</td>
                                <td>
                                    <?php 
                                        echo create_drop_down( "cbo_shift_id", 150, $shift_name,"", 1, "-- Select Shift --", 0, "",'' );
                                    ?>	
                                </td>
                            </tr>
                            <tr>
                                <td>Prod. Floor</td>
                                <td id="floor_td">
                                    <?php echo create_drop_down( "cbo_floor_id", 150, $blank_array,"", 1, "-- Select Floor --", 0, "",0 ); ?>
                                </td>
                                <td class="must_entry_caption">Machine No.</td>
                                <td id="machine_td" >
                                    <?php echo create_drop_down( "cbo_machine_name", 150, $blank_array,"", 1, "-- Select Machine --", 0, "",0 ); ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="must_entry_caption">Grey Product Qty</td>
                                <td>
                                    <input type="text" name="txt_product_qnty" id="txt_product_qnty" class="text_boxes_numeric" style="width:140px;"  tabindex="15"/>	
                                </td>
                                <td>UOM</td>
                                <td>
									<?php
										echo create_drop_down( "cbo_uom", 150, $unit_of_measurement,"",1,"--Select UOM--", 12,"", 1,"","" );
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Reject Fab Receive</td>
                                <td>
                                    <input type="text" name="txt_reject_qnty" id="txt_reject_qnty" class="text_boxes_numeric" style="width:140px;" tabindex="16" />	
                                </td>
                                <td>Job No</td>
                                <td>
                                    <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:140px" disabled>
                                </td>
                            </tr>
                            <tr>
                                <td>No of Roll</td>
                                <td>
                                    <input type="text" name="txt_roll_qnty" id="txt_roll_qnty" class="text_boxes_numeric" style="width:140px ;text-align:right"  tabindex="13" />
                                </td>
                                <td>Stitch Length</td>
                                <td>
                                    <input type="text" name="txt_stitch_len" id="txt_stitch_len" class="text_boxes" style="width:140px" >
                                </td>
                            </tr>
                            <tr>
                                <td>Color Range</td>
                                <td>
									<?php
										echo create_drop_down( "cbo_color_range", 150, $color_range,"",1,"--Select Range--", 0,"", 0,"","" );
                                    ?>
                                </td>
                                <td>Fab. Color</td>
                                <td>
                                    <input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:140px" >
                                </td>
                            </tr> 
                        </table>
                        </fieldset>
                    </td>
                    <td width="30%" valign="top" style="margin-left:10px;">
                        <fieldset style="width:200px">
                        <legend>New Entry Remarks</legend>
                        <table  cellpadding="0" cellspacing="2" width="100%" align="center">
                            <tr>
                              <td align="left"><textarea id="text_new_remarks" name="text_new_remarks" class="text_area" title="Maximum 1000 Character" maxlength="1000" style="width:180px; height:192px" placeholder="Remarks Here. Maximum 1000 Character." ></textarea></td>
                            </tr>
                        </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="center" class="button_container">
						<?php 
							echo load_submit_buttons($permission, "subcon_kniting_production", 0,0,"reset_form('knitingproduction_1','kniting_production_list_view','','','disable_enable_fields(\'cbo_company_id*cbo_party_name\',0)')",1);
                        ?> 
                    </td>	  
                </tr>
            </table> 
        </fieldset>
        </form>
        <div style="width:800px; margin-top:10px" id="kniting_production_list_view" align="center"></div>
        <div id="list_fabric_desc_container" style="max-height:500px; width:350px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>
    </div>
</body>
<script>
	set_multiselect('cbo_yarn_count','0','0','','0');
</script>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>