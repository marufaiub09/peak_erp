-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_buyer_tag_company`
--

DROP TABLE IF EXISTS `lib_buyer_tag_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_buyer_tag_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_id` int(11) NOT NULL,
  `tag_company` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `buyer_id` (`buyer_id`),
  KEY `tag_company` (`tag_company`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_buyer_tag_company`
--

LOCK TABLES `lib_buyer_tag_company` WRITE;
/*!40000 ALTER TABLE `lib_buyer_tag_company` DISABLE KEYS */;
INSERT INTO `lib_buyer_tag_company` VALUES (4,4,1),(5,5,1),(8,3,1),(9,7,1),(11,9,1),(12,8,1),(13,10,1),(14,11,1),(15,12,1),(16,13,1),(17,14,1),(18,15,1),(19,16,1),(21,18,1),(22,19,1),(23,20,1),(25,22,1),(26,23,1),(27,24,1),(28,25,1),(29,26,1),(31,28,1),(32,29,1),(33,17,1),(35,1,1),(36,30,1),(37,6,1),(38,31,1),(39,32,1),(41,34,1),(42,35,1),(43,33,1),(48,36,1),(49,37,1),(50,38,1),(51,27,1),(53,40,1),(54,39,1),(55,41,1),(56,2,1),(58,43,1),(60,45,1),(62,47,1),(63,21,1),(65,48,1),(66,49,1),(67,50,1),(68,51,1),(70,53,1),(71,44,1),(73,55,1),(74,56,1),(75,57,1),(78,60,1),(80,61,1),(81,54,1),(83,46,1),(84,63,1),(85,42,1),(86,58,1),(87,64,1),(88,65,1),(89,66,1),(90,67,1),(92,69,1),(93,70,1),(94,71,1),(95,72,1),(96,73,1),(97,59,1),(98,68,1),(100,75,1),(101,52,1),(103,77,1),(104,76,1),(105,78,1),(106,79,1),(107,80,1),(108,81,1),(109,82,1),(111,84,1),(112,85,1),(113,86,1),(114,83,1),(115,62,1),(116,87,1),(117,88,1),(118,89,1),(120,91,1),(121,92,1),(122,90,1),(123,93,1),(124,94,1),(125,95,1),(126,96,1),(127,97,1),(128,74,1);
/*!40000 ALTER TABLE `lib_buyer_tag_company` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
