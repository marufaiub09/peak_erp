<?php
/************************************************Comments************************************
Purpose			: 	This form will create Dyes And Chemical Receive Entry
Functionality	:	
JS Functions	:
Created by		:	MONZU 
Creation date 	: 	17-08-2013
Updated by 		: 	Kausar	
Update date		: 	09-12-2013 (Creating Report)	   
QC Performed BY	:		
QC Date			:	
Comments		:
**********************************************************************************************/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
echo load_html_head_contents("Dyes And Chemical Receive","../../", 1, 1, $unicode,1,1); 
//--------------------------------------------------------------------------------------------------------------------
// last yarn receive exchange rate, currency,store name
$sql = sql_select("select store_id,exchange_rate,currency_id,max(id) from inv_receive_master where item_category in(5,6,7) group by store_id,exchange_rate,currency_id");
$storeName=$exchangeRate=$currencyID=0;
foreach($sql as $row)
{
	$storeName=$row[csf("store_id")];
	$exchangeRate=$row[csf("exchange_rate")];
	$currencyID=$row[csf("currency_id")];
}
?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

function rcv_basis_reset()
{
	document.getElementById('cbo_receive_basis').value=0;
	reset_form('chemicaldyesreceive_1','list_container_yarn*list_product_container','','','','cbo_company_name');
	fn_independent(0)
}


	
	
// popup for WO/PI----------------------	
function openmypage(page_link,title)
{
	if( form_validation('cbo_company_name*cbo_receive_basis','Company Name*Receive Basis')==false )
	{
		return;
	}
	
 		var company = $("#cbo_company_name").val();
		var receive_basis = $("#cbo_receive_basis").val();
		 
		page_link='requires/chemical_dyes_receive_controller.php?action=wopi_popup&company='+company+'&receive_basis='+receive_basis;
 		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=400px, center=1, resize=0, scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var rowID=this.contentDoc.getElementById("hidden_tbl_id").value; // wo/pi table id
			var wopiNumber=this.contentDoc.getElementById("hidden_wopi_number").value; // wo/pi number
			var hidden_is_non_ord_sample=this.contentDoc.getElementById("hidden_is_non_ord_sample").value; // wo/pi number
			if (rowID!="")
			{
 				freeze_window(5);
				$("#txt_wo_pi").val(wopiNumber);
				$("#txt_wo_pi_id").val(rowID);
				$("#booking_without_order").val(hidden_is_non_ord_sample);
				get_php_form_data(receive_basis+"**"+rowID+"**"+hidden_is_non_ord_sample+"**"+wopiNumber, "populate_data_from_wopi_popup", "requires/chemical_dyes_receive_controller" );
				show_list_view(receive_basis+"**"+rowID+"**"+hidden_is_non_ord_sample+"**"+wopiNumber,'show_product_listview','list_product_container','requires/chemical_dyes_receive_controller','');
				release_freezing();	 
 			}
		}		
}

// popup for WO/PI----------------------	
function openmypage_gate()
{
	if( form_validation('cbo_company_name*cbo_receive_basis','Company Name*Receive Basis')==false )
	{
		return;
	}
	
 		var company = $("#cbo_company_name").val();
		var receive_basis = $("#cbo_receive_basis").val();
		 title="";
		page_link='requires/chemical_dyes_receive_controller.php?action=gate_search&company='+company+'&receive_basis='+receive_basis;
 		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px, height=400px, center=1, resize=0, scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			
			var wopiNumber=this.contentDoc.getElementById("hidden_system_id").value; // wo/pi number
 				freeze_window(5);
				$("#txt_gate_entry").val(wopiNumber);
				release_freezing();	 
 			}
				
}

// enable disable field for independent
function fn_independent(val)
{
	$('#txt_wo_pi').val('');
	reset_form('chemicaldyesreceive_1','list_container_yarn*list_product_container','','','','cbo_company_name*cbo_receive_basis');
	$("#txt_rack").attr("disabled",true);
    $("#txt_self").attr("disabled",true);
    $("#txt_binbox").attr("disabled",true);

	if(val==0)
	{
		$("#txt_lc_no").attr("disabled",false);
		$('#txt_lc_no').removeAttr('placeholder','Display');
		$('#cbo_supplier').val(0);
		$("#cbo_supplier").attr("disabled",false);
		$('#cbo_supplier option:eq(0)').text('-Select-')
		$('#cbo_currency').val(0);
		$("#cbo_currency").attr("disabled",false);
		$('#cbo_currency option:eq(0)').text('-Select-')
		$('#cbo_source').val(0);
		$("#cbo_source").attr("disabled",false);
		$('#cbo_source option:eq(0)').text('-Select-')
		$("#txt_wo_pi").attr("disabled",false);
		$('#txt_wo_pi').removeAttr('placeholder','No Need');
		$('#txt_wo_pi').attr('placeholder','Double Click');
		$('#cbo_receive_purpose').val(0);
		$('#cbo_loan_party').val(0);
		$('#cbo_receive_purpose').attr('disabled',false);
		$('#cbo_loan_party').attr('disabled',false);
		$('#cbo_receive_purpose option:eq(0)').text('-Select-')
		$('#cbo_loan_party option:eq(0)').text('-Select-')
		
		$('#cbo_item_category_id').val(0);
		$("#cbo_item_category_id").attr("disabled",false);
		$('#cbo_item_category_id option:eq(0)').text('-Select-')
		
		$('#cbo_item_group_id').val(0);
		$("#cbo_item_group_id").attr("disabled",false);
		$('#cbo_item_group_id option:eq(0)').text('-Select-')
		
		$('#txt_description').val("");
		$("#txt_description").attr("disabled",false);
		$("#txt_description").removeAttr("onClick","openmypage_ItemDescription()");
		$('#txt_description').removeAttr('placeholder','Display');
		
		$('#txt_rate').val(0);
		$("#txt_rate").attr("disabled",false);
		$('#txt_rate').removeAttr('placeholder','Display');
		
		
	}
	if(val==1)
	{
		$("#txt_lc_no").attr("disabled",true);
		$('#txt_lc_no').attr('placeholder','Display');
		$('#cbo_supplier').val(0);
		$("#cbo_supplier").attr("disabled",true);
		$('#cbo_supplier option:eq(0)').text('Display')
		$('#cbo_currency').val(0);
		$("#cbo_currency").attr("disabled",true);
		$('#cbo_currency option:eq(0)').text('Display')
		$('#cbo_source').val(0);
		$("#cbo_source").attr("disabled",true);
		$('#cbo_source option:eq(0)').text('Display')
		$("#txt_wo_pi").attr("disabled",false);
		$('#txt_wo_pi').removeAttr('placeholder','No Need');
		$('#txt_wo_pi').attr('placeholder','Double Click');
		$('#cbo_receive_purpose').val(0);
		$('#cbo_loan_party').val(0);
		$('#cbo_receive_purpose').attr('disabled','disabled');
		$('#cbo_loan_party').attr('disabled','disabled');
		$('#cbo_receive_purpose option:eq(0)').text('No Need')
		$('#cbo_loan_party option:eq(0)').text('No Need')
		
		$('#cbo_item_category_id').val(0);
		$("#cbo_item_category_id").attr("disabled",true);
		$('#cbo_item_category_id option:eq(0)').text('Display')
		
		$('#cbo_item_group_id').val(0);
		$("#cbo_item_group_id").attr("disabled",true);
		$('#cbo_item_group_id option:eq(0)').text('Display')
		
		$('#txt_description').val("");
		$("#txt_description").attr("disabled",true);
		$("#txt_description").removeAttr("onClick","openmypage_ItemDescription()");
		$('#txt_description').attr('placeholder','Display');
		
		$('#txt_rate').val(0);
		$("#txt_rate").attr("disabled",true);
		$('#txt_rate').attr('placeholder','Display');
	}
	if(val==2)
	{
		$("#txt_lc_no").attr("disabled",true);
		$('#txt_lc_no').attr('placeholder','No Need');
		$('#cbo_supplier').val(0);
		$("#cbo_supplier").attr("disabled",true);
		$('#cbo_supplier option:eq(0)').text('Display')
		
		$('#cbo_currency').val(0);
		$("#cbo_currency").attr("disabled",true);
		$('#cbo_currency option:eq(0)').text('Display')
		
		$('#cbo_source').val(0);
		$("#cbo_source").attr("disabled",true);
		$('#cbo_source option:eq(0)').text('Display')
		$("#txt_wo_pi").attr("disabled",false);
		$('#txt_wo_pi').removeAttr('placeholder','No Need');
		$('#txt_wo_pi').attr('placeholder','Double Click');
		$('#cbo_receive_purpose').val(0);
		$('#cbo_loan_party').val(0);
		$('#cbo_receive_purpose').attr('disabled','disabled');
		$('#cbo_loan_party').attr('disabled','disabled');
		$('#cbo_receive_purpose option:eq(0)').text('No Need')
		$('#cbo_loan_party option:eq(0)').text('No Need')
		$('#cbo_item_category_id').val(0);
		$("#cbo_item_category_id").attr("disabled",true);
		$('#cbo_item_category_id option:eq(0)').text('Display')
		$('#cbo_item_group_id').val(0);
		$("#cbo_item_group_id").attr("disabled",true);
		$('#cbo_item_group_id option:eq(0)').text('Display')
		
		$('#txt_description').val("");
		$("#txt_description").attr("disabled",true);
		$("#txt_description").removeAttr("onClick","openmypage_ItemDescription()");
		$('#txt_description').attr('placeholder','Display');
		
		$('#txt_rate').val(0);
		$("#txt_rate").attr("disabled",true);
		$('#txt_rate').attr('placeholder','Display');
	}
	
	if(val==4)
	{
		$("#txt_lc_no").attr("disabled",true);
		$('#txt_lc_no').attr('placeholder','No Need');
		$('#cbo_supplier').val(0);
		$("#cbo_supplier").attr("disabled",false);
		$('#cbo_supplier option:eq(0)').text('-Select-');
		$('#cbo_currency').val(0);
		$("#cbo_currency").attr("disabled",false);
		$('#cbo_currency option:eq(0)').text('-Select-')
		$('#txt_exchange_rate').removeAttr('disabled','disabled');
		$('#cbo_source').val(0);
		$("#cbo_source").attr("disabled",false);
		$('#cbo_source option:eq(0)').text('-Select-')
		$("#txt_wo_pi").attr("disabled",true);
		$('#txt_wo_pi').attr('placeholder','No Need');
		$('#cbo_receive_purpose').val(0);
		$('#cbo_loan_party').val(0);
		$('#cbo_receive_purpose').removeAttr('disabled','disabled'); 
		$('#cbo_loan_party').removeAttr('disabled','disabled');
		$('#cbo_receive_purpose option:eq(0)').text('-Select Purpose-')
		$('#cbo_loan_party option:eq(0)').text('-Select Loan Party-')
		$('#cbo_item_category_id').val(0);
		$("#cbo_item_category_id").attr("disabled",false);
		$('#cbo_item_category_id option:eq(0)').text('-Select-')
		$('#cbo_item_group_id').val(0);
		$("#cbo_item_group_id").attr("disabled",false);
		$('#cbo_item_group_id option:eq(0)').text('-Select-')
		
		$('#txt_description').val("");
		$("#txt_description").attr("disabled",false);
	    $('#txt_description').attr('placeholder','Click');
		$("#txt_description").attr("onClick","openmypage_ItemDescription()");
		$('#txt_rate').val(0);
		$("#txt_rate").attr("disabled",false);
		$('#txt_rate').removeAttr('placeholder','Display');
		
	}
	if(val==6)
	{
		$("#txt_lc_no").removeAttr("disabled",true);
		$('#txt_lc_no').attr('placeholder','Click');
		$('#cbo_supplier').val(0);
		$("#cbo_supplier").attr("disabled",false);
		$('#cbo_supplier option:eq(0)').text('-Select-');
		$('#cbo_currency').val(0);
		$("#cbo_currency").attr("disabled",false);
		$('#cbo_currency option:eq(0)').text('-Select-')
		$('#txt_exchange_rate').removeAttr('disabled','disabled');
		$('#cbo_source').val(0);
		$("#cbo_source").attr("disabled",false);
		$('#cbo_source option:eq(0)').text('-Select-')
		$("#txt_wo_pi").attr("disabled",true);
		$('#txt_wo_pi').attr('placeholder','No Need');
		$('#cbo_receive_purpose').val(0);
		$('#cbo_loan_party').val(0);
		$('#cbo_receive_purpose').attr('disabled','disabled');
		$('#cbo_loan_party').attr('disabled','disabled');
		$('#cbo_receive_purpose option:eq(0)').text('No Need')
		$('#cbo_loan_party option:eq(0)').text('No Need')
		$('#cbo_item_category_id').val(0);
		$("#cbo_item_category_id").attr("disabled",false);
		$('#cbo_item_category_id option:eq(0)').text('-Select-')
		$('#cbo_item_group_id').val(0);
		$("#cbo_item_group_id").attr("disabled",false);
		$('#cbo_item_group_id option:eq(0)').text('-Select-')
		$('#txt_description').val("");
		$("#txt_description").attr("disabled",false);
		$("#txt_description").attr("onClick","openmypage_ItemDescription()");
		$('#txt_description').attr('placeholder','Click');
		
		$('#txt_rate').val(0);
		$("#txt_rate").attr("disabled",false);
		$('#txt_rate').removeAttr('placeholder','Display');
	}
}

function set_exchange_rate(currency_id)
{
	if(currency_id==1)
	{
		$('#txt_exchange_rate').val(1);
		$('#txt_exchange_rate').attr('disabled','disabled');
		 fn_calile()
	}
	if(currency_id!=1)
	{
		var response=return_global_ajax_value( currency_id, 'set_exchange_rate', '', 'requires/chemical_dyes_receive_controller');
		$('#txt_exchange_rate').val(response);
		$('#txt_exchange_rate').removeAttr('disabled','disabled')
		 fn_calile()
	}
}


// LC pop up script here-----------------------------------
function popuppage_lc()
{
	
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/chemical_dyes_receive_controller.php?action=lc_popup&company='+company; 
	var title="Search LC Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px,height=370px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];//("search_order_frm"); //Access the form inside the modal window
		var rowID=this.contentDoc.getElementById("hidden_tbl_id").value; // lc table id
		var wopiNumber=this.contentDoc.getElementById("hidden_wopi_number").value; // lc number
		$("#txt_lc_no").val(wopiNumber);
		$("#hidden_lc_id").val(rowID);		  
	}
 	
}


// calculate ILE ---------------------------
function fn_calile()
{
	/*if( form_validation('cbo_company_name*txt_exchange_rate*cbo_source*txt_rate','Company Name*Exchange Rate*Source*Rate')==false )
	{
		return;
	}
	*/
	var company=$('#cbo_company_name').val();	
	var source=$('#cbo_source').val();	
	var rate=$('#txt_rate').val();	
	var cbo_item_category_id=$('#cbo_item_category_id').val();	 
	var responseHtml = return_ajax_request_value(company+'**'+source+'**'+rate+'**'+cbo_item_category_id, 'show_ile', 'requires/chemical_dyes_receive_controller');
	var splitResponse="";
	if(responseHtml!="")
	{
		splitResponse = responseHtml.split("**");
		$("#ile_td").html('ILE% '+splitResponse[0]);
		$("#txt_ile").val(splitResponse[1]);
	}
	else
	{
		$("#ile_td").html('ILE% 0');
		$("#txt_ile").val(0);
	}
	
	//amount and book currency calculate--------------//
	var quantity 		= $("#txt_receive_qty").val();
	var exchangeRate 	= $("#txt_exchange_rate").val();
	var ile_cost 		= $("#txt_ile").val();
	var amount = quantity*1*(rate*1+ile_cost*1); 
	var bookCurrency = (rate*1+ile_cost*1)*exchangeRate*1*quantity*1;
	$("#txt_amount").val(number_format_common(amount,"","",1));
	$("#txt_book_currency").val(number_format_common(bookCurrency,"","",1));
	
}


function fn_room_rack_self_box()
{ 
	if( $("#txt_room").val()*1 > 0 )  
		disable_enable_fields( 'txt_rack', 0, '', '' ); 
	else
	{
		reset_form('','','txt_rack*txt_self*txt_binbox','','','');
		disable_enable_fields( 'txt_rack*txt_self*txt_binbox', 1, '', '' ); 
	}
	if( $("#txt_rack").val()*1 > 0 )  
		disable_enable_fields( 'txt_self', 0, '', '' ); 
	else
	{
		reset_form('','','txt_self*txt_binbox','','','');
		disable_enable_fields( 'txt_self*txt_binbox', 1, '', '' ); 	
	}
	if( $("#txt_self").val()*1 > 0 )  
		disable_enable_fields( 'txt_binbox', 0, '', '' ); 
	else
	{
		reset_form('','','txt_binbox','','','');
		disable_enable_fields( 'txt_binbox', 1, '', '' ); 	
	}
}

function fnc_chemical_dyes_receive_entry(operation)
{
	if(operation==4)
	{
		var report_title=$( "div.form_caption" ).html();
		var data= $('#cbo_company_name').val()+'*'+$('#update_id').val()+'*'+report_title+'*'+$('#cbo_receive_basis').val();
		var action='chemical_dyes_receive_print';
		window.open("requires/chemical_dyes_receive_controller.php?data=" + data+'&action='+action, true );
		
		/*var report_title=$( "div.form_caption" ).html();txt_mrr_no
		print_report( $('#cbo_company_name').val()+'*'+$('#update_id').val()+'*'+report_title+'*'+$('#cbo_receive_basis').val(), "chemical_dyes_receive_print", "requires/chemical_dyes_receive_controller" ) */
		return;
	}
	else if(operation==0 || operation==1 || operation==2)
	{
		if(operation==2)
		{
			show_msg('13');
			return;
		}
		if( form_validation('cbo_company_name*cbo_receive_basis*txt_receive_date*txt_challan_no*cbo_store_name*cbo_supplier*cbo_currency*cbo_source*txt_receive_qty*txt_rate','Company Name*Receive Basis*Receive Date*Challan No*Store Name*Supplier*Currency*Source*Receive Quantity*Rate')==false )
		{
			return;
		}	
		
		else if( $("#txt_rate").val()=="" || $("#txt_rate").val()==0 )
		{
			$("#txt_rate").val('');
			form_validation('txt_rate','Rate');
			return;
		}
		if($("#txt_exchange_rate").val()<=0)
		{
			alert("Exchange Rate Should Be More Then Zero");
			$("#txt_exchange_rate").focus();
			return;
		}
		var dataString = "txt_mrr_no*update_id*cbo_company_name*cbo_receive_basis*txt_wo_pi*txt_wo_pi_id*cbo_receive_purpose*cbo_loan_party*txt_gate_entry*txt_receive_date*txt_challan_no*cbo_location_name*cbo_store_name*cbo_supplier*txt_lc_no*hidden_lc_id*cbo_currency*txt_exchange_rate*cbo_source*cbo_item_category_id*cbo_item_group_id*txt_description*txt_product_id*txt_lot*cbo_uom*txt_receive_qty*txt_rate*txt_ile*txt_amount*txt_book_currency*txt_bla_order_qty*txt_expire_date*txt_room*txt_rack*txt_self*txt_binbox*update_dtls_id";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../../");
	
		//freeze_window(operation);
		http.open("POST","requires/chemical_dyes_receive_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_chemical_dyes_receive_entry_reponse;
	}
}

function fnc_chemical_dyes_receive_entry_reponse()
{	
	if(http.readyState == 4) 
		{
			//alert(http.responseText);
			var reponse=trim(http.responseText).split('**');	
				
			show_msg(reponse[0]);
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_mrr_no').value = reponse[2];
				$('#cbo_company_name').attr('disabled','disabled');
				//show_list_view('".$row[csf("recv_number")]."**".$row[csf("id")]."','show_dtls_list_view','list_container_yarn','requires/chemical_dyes_receive_controller','');\
				show_list_view(reponse[2]+'**'+reponse[1],'show_dtls_list_view','list_container_yarn','requires/chemical_dyes_receive_controller','');
			}
			reset_form('','','cbo_item_category_id*cbo_item_group_id*txt_description*txt_product_id*txt_lot*cbo_uom*txt_receive_qty*txt_rate*txt_ile*txt_amount*txt_book_currency*txt_bla_order_qty*txt_expire_date*txt_room*txt_rack*txt_self*txt_binbox*update_dtls_id','','','');
			set_button_status(reponse[3], permission, 'fnc_chemical_dyes_receive_entry',1,1);	
			release_freezing();	
		}
}


function open_mrrpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/chemical_dyes_receive_controller.php?action=mrr_popup&company='+company; 
	var title="Search MRR Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=400px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var mrrNumber=this.contentDoc.getElementById("hidden_recv_number").value; // mrr number
 		$("#txt_mrr_no").val(mrrNumber);
		
		// master part call here
		get_php_form_data(mrrNumber, "populate_data_from_data", "requires/chemical_dyes_receive_controller");
		//set_button_status(0, permission, 'fnc_chemical_dyes_receive_entry',1,1);
		$("#tbl_master").find('input,select').attr("disabled", true);	
		disable_enable_fields( 'txt_mrr_no', 0, "", "" );	
 	}
}


//form reset/refresh function here
function fnResetForm()
{
	fn_independent(0)
	set_button_status(0, permission, 'fnResetForm',1);
	reset_form('chemicaldyesreceive_1','list_container_yarn*list_product_container','','','','');
	disable_enable_fields( 'cbo_company_name*cbo_receive_basis*txt_gate_entry*txt_receive_date*txt_challan_no*cbo_location_name*cbo_store_name*txt_exchange_rate*cbo_uom', 0, "", "" );
}

	function openmypage_ItemDescription()
	{
		
		var cbo_company_name=$('#cbo_company_name').val();
		var cbo_item_category_id=$('#cbo_item_category_id').val();
		var cbo_item_group_id=$('#cbo_item_group_id').val();
		var title = 'Item Description Info';	
		var page_link = 'requires/chemical_dyes_receive_controller.php?action=ItemDescription_popup&cbo_company_name='+cbo_company_name+'&cbo_item_category_id='+cbo_item_category_id+'&cbo_item_group_id='+cbo_item_group_id;
		var empty="";
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1000px,height=370px,center=1,resize=1,scrolling=0','../');
		
		emailwindow.onclose=function()
		{
			var cbo_receive_basis=$('#cbo_receive_basis').val();
			cbo_receive_basis
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var theemail=this.contentDoc.getElementById("hidden_prod_id").value;	 //Access form field with id="emailfield"
			get_php_form_data(cbo_receive_basis+"**"+empty+"**"+empty+"**"+empty+"**"+theemail,"wo_pi_product_form_input","requires/chemical_dyes_receive_controller")
		}
	}
	
	function company_anable()
	  {
		$("#cbo_company_name").attr("disabled",false);  
		  
	  }
	
	
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../../",$permission);  ?><br />    		 
    <form name="chemicaldyesreceive_1" id="chemicaldyesreceive_1" autocomplete="off" > 
    <div style="width:80%;">       
    <table width="80%" cellpadding="0" cellspacing="2" align="left">
     	<tr>
        	<td width="80%" align="center" valign="top">   
            	<fieldset style="width:1000px; float:left;">
                <legend>Dyes And Chemical Receive</legend>
                <br />
                 	<fieldset style="width:950px;">                                       
                        <table  width="950" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                            <tr>
                           		<td colspan="6" align="center">&nbsp;<b>MRR Number</b>
                                	<input type="text" name="txt_mrr_no" id="txt_mrr_no" class="text_boxes" style="width:155px" placeholder="Double Click To Search" onDblClick="open_mrrpopup()" readonly /> <input type="hidden" name="update_id" id="update_id" />
                                </td>
                           </tr>
                           <tr>
                                <td  width="130" class="must_entry_caption">Company Name </td>
                                    <td width="170">
                                        <?php 
                                         echo create_drop_down( "cbo_company_name", 170, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "rcv_basis_reset();load_drop_down( 'requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_supplier', 'supplier' );load_drop_down( 'requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_store', 'store_td' );load_drop_down( 'requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_location', 'location_td')" );
                                        ?>
                                </td>
                                    <td width="94" class="must_entry_caption"> Receive Basis </td>
                                   <td width="160">
                                        <?php 
										//load_drop_down( 'requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_supplier', 'supplier'); load_drop_down('requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_currency', 'currency'); load_drop_down('requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_source', 'sources'); load_drop_down('requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_lc', 'lc_no');
                                        echo create_drop_down( "cbo_receive_basis", 170, $receive_basis_arr,"", 1, "- Select Receive Basis -", $selected, "fn_independent(this.value)","","1,2,4,6" );
                                        ?>
                                   </td>
                                   <td width="80" >WO / PI </td>
                                        <td width="140">
                                            <input class="text_boxes"  type="text" name="txt_wo_pi" id="txt_wo_pi" onDblClick="openmypage('xx','Order Search')"  placeholder="Double Click" style="width:160px;"  readonly  /> 
                                            <input type="hidden" id="txt_wo_pi_id" name="txt_wo_pi_id" value="" />
                                        </td>
                                   
                            </tr>
                            
                            <tr>
                                <td  width="130" >Receive Purpose </td>
                                    <td width="170">
                                         <?php 
                                        
                                        echo create_drop_down( "cbo_receive_purpose", 170, $yarn_issue_purpose,"", 1, "-- Select Purpose --", 0, "", "","5");
                                        ?>
                                </td>
                                    <td width="94" >  Loan Party </td>
                                   <td width="160">
                                        <?php 
										//load_drop_down( 'requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_supplier', 'supplier'); load_drop_down('requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_currency', 'currency'); load_drop_down('requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_source', 'sources'); load_drop_down('requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_lc', 'lc_no');
                                        echo create_drop_down( "cbo_loan_party", 170, "select id,other_party_name from lib_other_party where  status_active=1 and is_deleted=0 order by other_party_name","id,other_party_name", 1, "- Select Loan Party -", $selected, "","","" );
                                        ?>
                                   </td>
                                   <td width="80" >Gate Entry No </td>
                                        <td width="140">
                                            <input class="text_boxes"  type="text" name="txt_gate_entry" id="txt_gate_entry" onDblClick="openmypage_gate()"  placeholder="Double Click" style="width:160px;"    /> 
                                            
                                        </td>
                                   
                            </tr>
                            <tr>
                            <td  width="130" class="must_entry_caption">Receive Date </td>
                                   <td width="170">
                                       <input type="text" name="txt_receive_date" id="txt_receive_date" class="datepicker" style="width:160px;" placeholder="Select Date" />
                                   </td>
                           
                                   <td width="94" class="must_entry_caption" > Challan No </td>
                                   <td width="160">
                                       <input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:160px" >
                                   </td>
                                   <td  width="130" > Location </td>
                                   <td width="170" id="location_td">
                                       <?php 
							           echo create_drop_down( "cbo_location_name", 170, $blank_array,"", 1, "-- Select Location --", 0, "" );
                                       ?>
                                   </td>
                                   
                                   
                            </tr>
                            <tr>
                                   <td width="130" class="must_entry_caption">Store Name</td>
                                   <td width="170" id="store_td">
                                        <?php 
                                        echo create_drop_down( "cbo_store_name", 170, "select lib_store_location.id,lib_store_location.store_name,lib_store_location.company_id,lib_store_location_category.category_type from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0  and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id order by lib_store_location.store_name","id,store_name", 1, "-- Select Store --", $storeName, "" );
                                        ?>
                                   </td>
                          
                                   <td width="94" class="must_entry_caption"> Supplier </td>
                                   <td id="supplier" width="160"> 
                                    <?php
                                       //echo create_drop_down( "cbo_supplier", 170, $blank_array,"", 1, "--- Select Supplier ---", $selected, "",1);
									   echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET(3,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "","" );
                                    ?>
                                   </td>
                                   <td width="130" > L/C No </td>
                                   <td id="lc_no" width="170">
                                    <input class="text_boxes"  type="text" name="txt_lc_no" id="txt_lc_no" style="width:160px;" placeholder="Display" onDblClick="popuppage_lc()" readonly   />  
                                    <input type="hidden" name="hidden_lc_id" id="hidden_lc_id" />
                                   </td>
                                    
                                   
                            </tr>
                             <tr>
                                   <td width="130" class="must_entry_caption">Currency</td>
                                   <td width="170" id="currency"> 
                                    <?php
                                       echo create_drop_down( "cbo_currency", 170, $currency,"", 1, "-- Select Currency --", $currencyID, "set_exchange_rate(this.value)","" );
                                    ?>
                                   </td>
                           
                                <td  width="130" >Exchange Rate</td>
                                    <td width="170">
                                        <input type="text" name="txt_exchange_rate" id="txt_exchange_rate" class="text_boxes_numeric" style="width:160px" value="<?php echo $exchangeRate;?>" onKeyUp="fn_calile()" />	
                                    </td>
                                    <td width="94" class="must_entry_caption">Source</td>
                                   <td width="160" id="sources">  
                                    <?php
                                        echo create_drop_down( "cbo_source", 170, $source,"", 1, "-- Select --", $selected, "","" );
                                    ?>
                                   </td>
                            </tr>
                        </table>
                    </fieldset>
                    <br />
                    <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                    <tr>
                    <td width="49%" valign="top">
                    	<fieldset style="width:950px;">  
                        <legend>New Receive Item</legend>                                     
                            <table width="220" cellspacing="2" cellpadding="0" border="0" style="float:left">
                            	
                                <tr>    
                                    <td >Item Categ</td>
                                    <td>         
                                        <?php 
										echo create_drop_down( "cbo_item_category_id", 130,$item_category,"", 1, "-- Select --", $selected, "load_drop_down( 'requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_item_group', 'item_group_td' )","","5,6,7","","","");
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td >Item Group</td>
                                    <td id="item_group_td" >
                                        <?php 
										echo create_drop_down( "cbo_item_group_id", 130,"select id,item_name  from lib_item_group where status_active=1","id,item_name", 1, "-- Select --", "", "","","","","","");
                                        ?>
                                    </td>
                                </tr>
                                
                                <tr>   
                                        <td >Item Desc.</td>
                                        <td>
                                            <input type="text" name="txt_description" id="txt_description" class="text_boxes" style="width:120px;"   />
                                            <input type="hidden" name="txt_product_id" id="txt_product_id" class="text_boxes" style="width:120px;"   />
                                        </td>
                                </tr> 
                                <tr>    
                                        <td width="110" >Lot</td>
                                    	<td width="130">
                                        <input type="text" name="txt_lot" id="txt_lot" class="text_boxes" style="width:120px;"/>
                                        </td> 
                                </tr>   
                            </table>
                            
                            
                            
                            <table width="240" cellspacing="2" cellpadding="0" border="0" style="float:left">
                                
                                
                            	
                                <tr>                 
                                    <td width="140" >UOM</td>
                                    <td width="140">
                                    	<?php
                                    		echo create_drop_down( "cbo_uom", 130, $unit_of_measurement,"", 1, "Display", "", "",1 );
										?>
                                    </td>
                                </tr>
                                <tr>    
                                        <td class="must_entry_caption">Recv. Qnty.</td>   
                                        <td >
                                            <input name="txt_receive_qty" id="txt_receive_qty"  class="text_boxes_numeric" type="text" style="width:120px;" onKeyUp="fn_calile()" />
                                        </td> 
                                </tr>
                                <tr>    
                                        <td class="must_entry_caption">Rate</td>   
                                        <td >
                                        	<input name="txt_rate" id="txt_rate" class="text_boxes_numeric" type="text" style="width:120px;" onKeyUp="fn_calile()" value="0"  />
                                        </td>
                                </tr>
                                <tr>   
                                        <td id="ile_td">ILE%</td>   
                                        <td >
                                        	<input name="txt_ile" id="txt_ile" class="text_boxes_numeric" type="text" style="width:120px;" placeholder="Display" readonly />
                                        </td>
                                </tr>  
                                  
                            </table>
                            
                            <table width="280" cellspacing="2" cellpadding="0" border="0" style="float:left">
                               
                                
                                <tr> 
                                    <td >Amount</td>
                                    <td><input type="text" name="txt_amount" id="txt_amount" class="text_boxes_numeric" style="width:120px;" readonly disabled /></td>
                                </tr>
                                <tr> 
                                    <td >Book Currency.</td>
                                    <td>
                                      	<input type="text" name="txt_book_currency" id="txt_book_currency" class="text_boxes_numeric" style="width:120px;" readonly disabled />
                                    </td>
                                </tr>
                                <tr> 
                                      <td >Balance PI/ WO </td>
                                      <td><input class="text_boxes_numeric"  name="txt_bla_order_qty" id="txt_bla_order_qty" type="text" style="width:120px;" readonly /></td>
                                </tr>
                                <tr>                 
                                    <td width="100" >Expire Date</td>
                                    <td width="100"><input class="datepicker"  name="txt_expire_date" id="txt_expire_date" type="text" style="width:120px;"  /></td>
                                </tr>
                            </table>
                            <table width="200" cellspacing="2" cellpadding="0" border="0">
                                <tr> 
                                    <td >Room</td>
                                    <td><input class="text_boxes"  name="txt_room" id="txt_room" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" /></td>
                                </tr>
                                <tr> 
                                    <td >Rack</td>
                                    <td><input class="text_boxes"  name="txt_rack" id="txt_rack" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" disabled /></td>
                                </tr>
                                <tr> 
                                      <td >Self</td>
                                      <td><input class="text_boxes"  name="txt_self" id="txt_self" type="text" style="width:100px;" onKeyUp="fn_room_rack_self_box()" disabled /></td>
                                </tr>
                                <tr> 
                                      <td >Bin/Box</td>
                                      <td><input class="text_boxes"  name="txt_binbox" id="txt_binbox" type="text" style="width:100px;" disabled /></td>
                                 </tr> 
                            </table>
                    </fieldset>
                    </td>
                    </tr>
                </table>                
               	<table cellpadding="0" cellspacing="1" width="100%">
                	<tr> 
                       <td colspan="6" align="center"></td>				
                	</tr>
                	<tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <!-- details table id for update -->
                             <input type="hidden" name="update_dtls_id" id="update_dtls_id" readonly>
							 <?php echo load_submit_buttons( $permission, "fnc_chemical_dyes_receive_entry", 0,1,"fnResetForm()",1);?>
                        </td>
                   </tr> 
                </table>                 
              	</fieldset>
              	<fieldset>
    			<div style="width:990px;" id="list_container_yarn"></div>
    		  	</fieldset>
           </td>
         </tr>
    </table>
    </div>
    <div id="list_product_container" style="max-height:500px; width:20%; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>  
	</form>
</div>    
</body>  
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
