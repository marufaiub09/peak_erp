<?php
include('../../includes/common.php');
session_start();

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }


if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 140, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", $selected, "" );	
}

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "" );
	exit();	 
} 

if($action=="process_name_popup")
{
  	echo load_html_head_contents("Process Name Info","../../", 1, 1, '','1','');
	extract($_REQUEST);
?>
	<script>
	
		$(document).ready(function(e) {
			setFilterGrid('tbl_list_search',-1);
		});
		
		var selected_id = new Array(); var selected_name = new Array(); var buyer_id=''; var style_ref_array= new Array();
		
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_process_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		
		function js_set_value( str ) 
		{
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hidden_process_id').val(id);
			$('#hidden_process_name').val(name);
		}
    </script>

</head>

<body>
<div align="center">
	<fieldset style="width:370px;margin-left:10px">
    	<input type="hidden" name="hidden_process_id" id="hidden_process_id" class="text_boxes" value="">
        <input type="hidden" name="hidden_process_name" id="hidden_process_name" class="text_boxes" value="">
        <form name="searchprocessfrm_1"  id="searchprocessfrm_1" autocomplete="off">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="350" class="rpt_table" >
                <thead>
                    <th width="50">SL</th>
                    <th>Process Name</th>
                </thead>
            </table>
            <div style="width:350px; overflow-y:scroll; max-height:280px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="332" class="rpt_table" id="tbl_list_search" >
                <?php
                    $i=1; $process_row_id=''; $not_process_id_print_array=array();
					$not_process_id_print_array=array(2,30,40,72,74,75,76,100,101,134);
					//$not_process_id_print_array=array(1,2,3,4,101,120,121,122,123,124);
					//$process_id_print_array=array(25,31,32,33,34,39,60,63,64,65,66,68,69,70,71,82,83,84,89,90,91,93,125,129,132,133);
					$hidden_process_id=explode(",",$txt_process_id);
                    foreach($conversion_cost_head_array as $id=>$name)
                    {
						if(!in_array($id,$not_process_id_print_array))
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							 
							if(in_array($id,$hidden_process_id)) 
							{ 
								if($process_row_id=="") $process_row_id=$i; else $process_row_id.=",".$i;
							}
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
								<td width="50" align="center"><?php echo "$i"; ?>
									<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $id; ?>"/>	
									<input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $name; ?>"/>
								</td>	
								<td><p><?php echo $name; ?></p></td>
							</tr>
							<?php
							$i++;
						}
                    }
                ?>
                    <input type="hidden" name="txt_process_row_id" id="txt_process_row_id" value="<?php echo $process_row_id; ?>"/>
                </table>
            </div>
             <table width="350" cellspacing="0" cellpadding="0" style="border:none" align="center">
                <tr>
                    <td align="center" height="30" valign="bottom">
                        <div style="width:100%"> 
                            <div style="width:50%; float:left" align="left">
                                <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                            </div>
                            <div style="width:50%; float:left" align="left">
                                <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	set_all();
</script>
</html>
<?php
exit();
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0) // Insert Start Here=================================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$id1=return_next_id( "id", "subcon_ord_dtls",1) ; 
		if(str_replace("'",'',$update_id)=="")
		{
			if($db_type==0)
			{
				$new_job_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', '', date("Y",time()), 5, "select job_no_prefix,job_no_prefix_num from subcon_ord_mst where company_id=$cbo_company_name and YEAR(insert_date)=".date('Y',time())." order by id desc ", "job_no_prefix", "job_no_prefix_num" ));
			}
			else if($db_type==2)
			{
				$new_job_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', '', date("Y",time()), 5, "select job_no_prefix,job_no_prefix_num from subcon_ord_mst where company_id=$cbo_company_name and TO_CHAR(insert_date,'YYYY')=".date('Y',time())." order by id desc ", "job_no_prefix", "job_no_prefix_num" ));
			}
					
			$id=return_next_id("id","subcon_ord_mst",1);
			$field_array="id,subcon_job,job_no_prefix,job_no_prefix_num,company_id,location_id,party_id,currency_id,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_job_no[0]."','".$new_job_no[1]."','".$new_job_no[2]."',".$cbo_company_name.",".$cbo_location_name.",".$cbo_party_name.",".$cbo_currency.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			//echo "INSERT INTO subcon_ord_mst (".$field_array.") VALUES ".$data_array; die;
			$rID=sql_insert("subcon_ord_mst",$field_array,$data_array,0);
			$txt_job_no=$new_job_no[0];
		}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="company_id*location_id*party_id*currency_id*updated_by*update_date";
			$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_party_name."*".$cbo_currency."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			$rID=sql_update("subcon_ord_mst",$field_array,$data_array,"id",$update_id,0);  
			$txt_job_no=str_replace("'",'',$txt_job_no); 
		}
		$field_array2="id,job_no_mst,order_no,order_quantity,order_uom,rate,amount,order_rcv_date,delivery_date,cust_buyer,cust_style_ref,main_process_id,process_id,smv,status_active,remarks,delay_for,inserted_by,insert_date";
		$data_array2="(".$id1.",'".$txt_job_no."',".$txt_order_no.",".$txt_order_quantity.",".$cbo_uom.",".$txt_rate.",".$txt_amount.",".$txt_order_receive_date.",".$txt_order_delivery_date.",".$txt_cust_buyer.",".$txt_style_ref.",".$cbo_process_name.",".$txt_process_id.",".$txt_smv.",".$cbo_status.",".$txt_details_remark.",".$cbo_delay_cause.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
		//echo "INSERT INTO subcon_ord_dtls (".$field_array2.") VALUES ".$data_array2; die;
		$rID2=sql_insert("subcon_ord_dtls",$field_array2,$data_array2,0);
		 //echo $rID2;die;
//===========================================================================================================================================================
		$color_library_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
		$size_library_arr=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );
		
		$id3=return_next_id( "id", "subcon_ord_breakdown", 1 ) ; 
		$field_array3="id,mst_id,order_id,item_id,color_id,size_id,qnty,rate,amount,excess_cut,plan_cut,process_loss";
		
		$hidden_item=explode("*",str_replace("'",'',$hidden_item));				$hidden_amount=explode("*",str_replace("'",'',$hidden_amount));
		$hidden_color=explode("*",str_replace("'",'',$hidden_color));
		$hidden_size=explode("*",str_replace("'",'',$hidden_size));				$hidden_excess_cut=explode("*",str_replace("'",'',$hidden_excess_cut));
		$hidden_qnty=explode("*",str_replace("'",'',$hidden_qnty));				$hidden_plan_cut=explode("*",str_replace("'",'',$hidden_plan_cut));
		$hidden_rate=explode("*",str_replace("'",'',$hidden_rate));				$hidden_loss=explode("*",str_replace("'",'',$hidden_loss));
		
		for ($i=0;$i<count($hidden_item);$i++)
		{
			$color_tbl_id = return_id($hidden_color[$i], $color_library_arr, "lib_color", "id,color_name");
			$size_tbl_id = return_id($hidden_size[$i], $size_library_arr, "lib_size", "id,size_name");
			
			if ($i!=0) $data_array3 .=",";
			$data_array3.="(".$id3.",".$id.",".$id1.",'".$hidden_item[$i]."','".$color_tbl_id."','".$size_tbl_id."','".$hidden_qnty[$i]."','".$hidden_rate[$i]."','".$hidden_amount[$i]."','".$hidden_excess_cut[$i]."','".ceil($hidden_plan_cut[$i])."','".$hidden_loss[$i]."')";
			$id3=$id3+1;
		} 
		//echo "INSERT INTO subcon_ord_breakdown (".$field_array3.") VALUES ".$data_array3; die;
		$rID3=sql_insert("subcon_ord_breakdown",$field_array3,$data_array3,1);
		//echo $rID3;die;

	//==========================================================================================================================================================
		if($db_type==0)
		{
			if($rID && $rID2 && $rID3)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$id);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2 && $rID3)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$id);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$id);
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
			
		$field_array="company_id*location_id*party_id*currency_id*updated_by*update_date";
		$data_array="".$cbo_company_name."*".$cbo_location_name."*".$cbo_party_name."*".$cbo_currency."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$field_array2="order_no*order_quantity*order_uom*rate*amount*order_rcv_date*delivery_date*cust_buyer*cust_style_ref*main_process_id*process_id*smv*status_active*remarks*delay_for*updated_by*update_date";
		$data_array2="".$txt_order_no."*".$txt_order_quantity."*".$cbo_uom."*".$txt_rate."*".$txt_amount."*".$txt_order_receive_date."*".$txt_order_delivery_date."*".$txt_cust_buyer."*".$txt_style_ref."*".$cbo_process_name."*".$txt_process_id."*".$txt_smv."*".$cbo_status."*".$txt_details_remark."*".$cbo_delay_cause."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		
		$color_library_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
		$size_library_arr=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );
		
		$field_array_up="item_id*color_id*size_id*qnty*rate*amount*excess_cut*plan_cut*process_loss";
		
		$hidden_item=explode("*",str_replace("'",'',$hidden_item));				$hidden_amount=explode("*",str_replace("'",'',$hidden_amount));
		$hidden_color=explode("*",str_replace("'",'',$hidden_color));
		$hidden_size=explode("*",str_replace("'",'',$hidden_size));				$hidden_excess_cut=explode("*",str_replace("'",'',$hidden_excess_cut));
		$hidden_qnty=explode("*",str_replace("'",'',$hidden_qnty));				$hidden_plan_cut=explode("*",str_replace("'",'',$hidden_plan_cut));
		$hidden_rate=explode("*",str_replace("'",'',$hidden_rate));				$hidden_loss=explode("*",str_replace("'",'',$hidden_loss));
																				$hidden_tbl_id_break=explode("*",str_replace("'",'',$hidden_tbl_id_break));
																				//print_r ($hidden_tbl_id_break);die;
		$id3=return_next_id( "id", "subcon_ord_breakdown", 1 ) ;
		$add_comma=0;
		for ($i=0;$i<count($hidden_item);$i++)
		{
			$color_tbl_id = return_id($hidden_color[$i], $color_library_arr, "lib_color", "id,color_name");
			$size_tbl_id = return_id($hidden_size[$i], $size_library_arr, "lib_size", "id,size_name");
			//print_r ($hidden_tbl_id_break);die;
			if($hidden_tbl_id_break!="")
			{
				$id_arr[]=$hidden_tbl_id_break[$i]; //die;//hidden_tbl_id_break[$i];
				$data_array_up[str_replace("'",'',$hidden_tbl_id_break[$i])] =explode(",",("'".$hidden_item[$i]."','".$color_tbl_id."','".$size_tbl_id."','".$hidden_qnty[$i]."','".$hidden_rate[$i]."','".$hidden_amount[$i]."','".$hidden_excess_cut[$i]."','".ceil($hidden_plan_cut[$i])."','".$hidden_loss[$i]."'"));
			}
			else
			{
				$field_array3="id,mst_id,order_id,item_id,color_id,size_id,qnty,rate,amount,excess_cut,plan_cut,process_loss";
				if ($add_comma!=0) $data_array3 .=",";
				$data_array3 .="(".$id3.",".$update_id.",".$update_id2.",'".$hidden_item[$i]."','".$color_tbl_id."','".$size_tbl_id."','".$hidden_qnty[$i]."','".$hidden_rate[$i]."','".$hidden_amount[$i]."','".$hidden_excess_cut[$i]."','".ceil($hidden_plan_cut[$i])."','".$hidden_loss[$i]."')";
				$id3=$id3+1;
				$add_comma++;	
			}
		} 
		
		$rID=sql_update("subcon_ord_mst",$field_array,$data_array,"id",$update_id,0);  
		if($rID) $flag=1; else $flag=0;
		
		$rID2=sql_update("subcon_ord_dtls",$field_array2,$data_array2,"id",$update_id2,0); 
		if($rID2) $flag=1; else $flag=0;
		
		echo $id_break=implode(',',$hidden_tbl_id_break);
		//print_r ($hidden_tbl_id_break);die;
		if($id_break!="")
		{
			$rID3=execute_query(bulk_update_sql_statement( "subcon_ord_breakdown", "id",$field_array_up,$data_array_up,$id_arr),1);
			if($rID3) $flag=1; else $flag=0;
			
			$deleted_id=str_replace("'",'',$txt_deleted_id);
			if ($deleted_id!="")
			{
				$rID5=execute_query( "delete from subcon_ord_breakdown where id in ($deleted_id)",0);
				if($rID5) $flag=1; else $flag=0;
			}		
		}
		else
		{
			$rID4=sql_insert("subcon_ord_breakdown",$field_array3,$data_array3,1);
			if($rID4) $flag=1; else $flag=0;		
		}
		//if($data_array3!="")
		//{
			//echo "INSERT INTO subcon_ord_breakdown (".$field_array3.") VALUES ".$data_array3; die;
			//$rID3=sql_insert("subcon_ord_breakdown",$field_array3,$data_array3,1);
		//}
		//$rID4=sql_insert("subcon_ord_breakdown",$field_array3,$data_array3,1);
		//if($rID4) $flag=1; else $flag=0;		
	//==============================================================================================================================================
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$update_id);
			}
			/*else if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$update_id);
			}*/
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2)
		{  
		   
			if($flag==1)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$update_id);
			}
			 else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$update_id);
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // delete here=======================================================================
	{
		$con = connect();
		
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		 
		//if ( $delete_master_info==1 )
		//{
			$field_array="status_active*is_deleted*updated_by*update_date";
			$data_array="0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			//$rID=sql_update("subcon_ord_mst",$field_array,$data_array,"id",$update_id,1);  
			//$rID=sql_update("subcon_ord_dtls",$field_array,$data_array,"job_no_mst",$update_id,1);  
			$rID = sql_delete("subcon_ord_dtls","status_active*is_deleted*updated_by*update_date","0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'",'id',$update_id2,1);	
		//}
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$update_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2)
		{
			if($rID)
			{
				oci_commit($con);
				echo "2**".str_replace("'",'',$txt_job_no)."**".str_replace("'",'',$update_id);;
			}
			else
			{
				oci_rollback($con);
				echo "10**";
			}
		}
		disconnect($con);
		die; 
	}
}

if ($action=="job_popup")
{
	echo load_html_head_contents("Job Popup Info","../../", 1, 1, $unicode,'','');
	?>
	<script>
		function js_set_value(id)
		{ 
			$("#hidden_mst_id").val(id);
			document.getElementById('selected_job').value=id;
			parent.emailwindow.hide();
		}
		
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
                <table width="740" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead> 
                        <tr>
                            <th colspan="6"><?php echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" ); ?></th>
                        </tr>
                    	<tr>               	 
                            <th width="140">Company Name</th>
                            <th width="140">Party Name</th>
                            <th width="170">Date Range</th>
                            <th width="100">Job No</th>
                            <th width="100">Order No</th>
                            <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>
                        </tr>           
                    </thead>
                    <tbody>
                        <tr>
                        <td> <input type="hidden" id="selected_job"><?php $data=explode("_",$data); ?>  <!--  echo $data;-->
                            <?php 
                               echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $data[0], "load_drop_down( 'sub_contract_order_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );"); ?>
                        </td>
                        <td id="buyer_td">
                            <?php echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $data[2], "" );   	 
                            ?>
                        </td>
                        <td align="center">
                            <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                            <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                        </td>
                        <td align="center">
                    		<input type="text" name="txt_search_job" id="txt_search_job" class="text_boxes" style="width:100px" placeholder="Search Job" />
                        </td> 
                        <td align="center">
                    		<input type="text" name="txt_search_order" id="txt_search_order" class="text_boxes" style="width:100px" placeholder="Search Order" />
                        </td> 
                        <td align="center">
                            <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_job').value+'_'+document.getElementById('txt_search_order').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_job_search_list_view', 'search_div', 'sub_contract_order_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" /></td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" height="40" valign="middle">
								<?php echo load_month_buttons(1);  ?>
                                <input type="hidden" name="hidden_mst_id" id="hidden_mst_id" class="datepicker" style="width:70px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                    </tbody>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if($action=="create_job_search_list_view")
{	
	$data=explode('_',$data);
	$party_id=str_replace("'","",$data[1]);
	$search_job=str_replace("'","",$data[4]);
	$search_order=trim(str_replace("'","",$data[5]));
	$search_type =$data[6];
	
	if($data[0]!=0) $company=" and company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	
	if($search_type==1)
	{
		if($search_job!='') $search_job_cond=" and a.job_no_prefix_num='$search_job'"; else $search_job_cond="";
		if($search_order!='') $search_order_cond=" and b.order_no='$search_order'"; else $search_order_cond="";
	}
	else if($search_type==4 || $search_type==0)
	{
		if($search_job!='') $search_job_cond=" and a.job_no_prefix_num like '%$search_job%'"; else $search_job_cond="";
		if($search_order!='') $search_order_cond=" and b.order_no like '%$search_order%'"; else $search_order_cond="";
	}
	else if($search_type==2)
	{
		if($search_job!='') $search_job_cond=" and a.job_no_prefix_num like '$search_job%'"; else $search_job_cond="";
		if($search_order!='') $search_order_cond=" and b.order_no like '$search_order%'"; else $search_order_cond="";
	}
	else if($search_type==3)
	{
		if($search_job!='') $search_job_cond=" and a.job_no_prefix_num like '%$search_job'"; else $search_job_cond="";
		if($search_order!='') $search_order_cond=" and b.order_no like '%$search_order'"; else $search_order_cond="";
	}	
	
	if($party_id!=0) $party_id_cond=" and party_id='$party_id'"; else $party_id_cond="";
	
	if($db_type==0)
	{ 
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2],'yyyy-mm-dd')."' and '".change_date_format($data[3],'yyyy-mm-dd')."'"; else $order_rcv_date ="";
	}
	else
	{
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2], "", "",1)."' and '".change_date_format($data[3], "", "",1)."'"; else $order_rcv_date ="";
	}
	
	$party_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$arr=array (3=>$production_process,4=>$party_arr,5=>$service_type);

	if($db_type==0)
	{
		$sql= "select a.id, a.subcon_job, a.job_no_prefix_num, YEAR(a.insert_date) as year, a.company_id, a.location_id, a.party_id, a.status_active, b.id, b.job_no_mst, b.order_no, b.order_rcv_date, b.delivery_date, b.main_process_id, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active in (1,2,3) $order_rcv_date $company $search_job_cond $search_order_cond $party_id_cond order by a.id DESC";
	}
	else if($db_type==2)
	{
		$sql= "select a.id, a.subcon_job, a.job_no_prefix_num, TO_CHAR(a.insert_date,'YYYY') as year, a.company_id, a.location_id, a.party_id, a.status_active, b.id, b.job_no_mst, b.order_no, b.order_rcv_date, b.delivery_date, b.main_process_id, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active in (1,2,3) $order_rcv_date $company $search_job_cond $search_order_cond $party_id_cond order by a.id DESC";
	}
		 //echo $sql;die;
	 echo  create_list_view("list_view", "Job No,Year,Order No,Process,Party Name,Order Date,Delivery Date","70,80,100,100,100,70,70","740","250",0,$sql, "js_set_value","subcon_job","",1,"0,0,0,main_process_id,party_id,0,0",$arr,"job_no_prefix_num,year,order_no,main_process_id,party_id,order_rcv_date,delivery_date", "",'','0,0,0,0,0,3,3') ;
	exit();		 
} 

if ($action=="load_php_data_to_form")
{
	$nameArray=sql_select( "select id,subcon_job,company_id,location_id,party_id,currency_id from subcon_ord_mst where subcon_job='$data'" );
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_job_no').value 			= '".$row[csf("subcon_job")]."';\n";  
		echo "document.getElementById('cbo_company_name').value 	= '".$row[csf("company_id")]."';\n";  
		echo "$('#cbo_company_name').attr('disabled','true')".";\n";
		echo "load_drop_down( 'requires/sub_contract_order_entry_controller', document.getElementById('cbo_company_name').value, 'load_drop_down_location', 'location_td' );\n";	
		echo "document.getElementById('cbo_location_name').value 	= '".$row[csf("location_id")]."';\n";
		echo "load_drop_down( 'requires/sub_contract_order_entry_controller', document.getElementById('cbo_company_name').value, 'load_drop_down_buyer', 'buyer_td' );\n";
		echo "document.getElementById('cbo_party_name').value		= '".$row[csf("party_id")]."';\n"; 	
		//echo "document.getElementById('txt_process_id').value		= '".$row[csf("service_type")]."';\n"; 
		echo "document.getElementById('cbo_currency').value			= '".$row[csf("currency_id")]."';\n"; 
	    echo "document.getElementById('update_id').value          	= '".$row[csf("id")]."';\n";	
		echo "set_button_status(0,'".$_SESSION['page_permission']."', 'fnc_job_order_entry',1);\n";	
	}
	exit();	
}

if ($action=="load_php_data_to_form_dtls")
{
	$nameArray=sql_select( "select id, order_no, order_quantity, order_uom, rate, amount, order_rcv_date, delivery_date, cust_buyer, cust_style_ref, main_process_id, process_id, smv, status_active, remarks, delay_for from subcon_ord_dtls where id='$data'" );

	foreach ($nameArray as $row)  //number_format($number, 2, '.', '');
	{
		$process_name='';
		$process_id_array=explode(",",$row[csf("process_id")]);
		foreach($process_id_array as $val)
		{
			if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=",".$conversion_cost_head_array[$val];
		}
			
		echo "document.getElementById('txt_order_no').value 	 				= '".$row[csf("order_no")]."';\n"; 
		echo "document.getElementById('txt_order_quantity').value				= '".$row[csf("order_quantity")]."';\n";  
		echo "document.getElementById('cbo_uom').value			 				= '".$row[csf("order_uom")]."';\n";  
		echo "document.getElementById('txt_rate').value		 					= '".number_format($row[csf("rate")],2,'.','')."';\n";
		echo "document.getElementById('txt_amount').value		 				= '".$row[csf("amount")]."';\n";  
		echo "document.getElementById('txt_order_receive_date').value			= '".change_date_format($row[csf("order_rcv_date")])."';\n";  
		echo "document.getElementById('txt_order_delivery_date').value			= '".change_date_format($row[csf("delivery_date")])."';\n";  
		echo "document.getElementById('txt_cust_buyer').value		 			= '".$row[csf("cust_buyer")]."';\n"; 
		echo "document.getElementById('txt_style_ref').value 		 			= '".$row[csf("cust_style_ref")]."';\n";  
		echo "document.getElementById('cbo_process_name').value				 	= '".$row[csf("main_process_id")]."';\n";
		//echo "set_multiselect('cbo_process','0','1','".$row[csf("process_id")]."','__populate_sub_group_info__requires/sub_contract_order_entry_controller');\n";	
		echo "document.getElementById('txt_process_name').value 				= '".$process_name."';\n";
		echo "document.getElementById('txt_process_id').value			 		= '".$row[csf("process_id")]."';\n";
		echo "document.getElementById('txt_smv').value			 				= '".$row[csf("smv")]."';\n";	
		echo "document.getElementById('cbo_status').value			 			= '".$row[csf("status_active")]."';\n";
		echo "document.getElementById('txt_details_remark').value				= '".$row[csf("remarks")]."';\n";
		echo "set_multiselect('cbo_delay_cause','0','1','".$row[csf("delay_for")]."','0');\n"; 
		echo "document.getElementById('update_id2').value             			= '".$row[csf("id")]."';\n";	
		echo "set_button_status(1,'".$_SESSION['page_permission']."', 'fnc_job_order_entry',1);\n";	
	}
		
	$collor_arr=return_library_array( "select id,color_name from lib_color",'id','color_name');
	$size_arr=return_library_array( "select id,size_name from lib_size",'id','size_name');
		
	$qry_result=sql_select( "select id,mst_id,order_id,item_id,color_id,size_id,qnty,rate,amount,excess_cut,plan_cut,process_loss from subcon_ord_breakdown where order_id='$data'");	
	foreach ($qry_result as $row)
	{
		if($id=="") $id=$row[csf("id")]; else $id.="*".$row[csf("id")];
		if($item_id=="") $item_id=$row[csf("item_id")]; else $item_id.="*".$row[csf("item_id")];
		if($color_fild_id=="") $color_fild_id=$collor_arr[$row[csf("color_id")]]; else $color_fild_id.="*".$collor_arr[$row[csf("color_id")]];
		if($size_number_id=="") $size_number_id=$size_arr[$row[csf("size_id")]]; else $size_number_id.="*".$size_arr[$row[csf("size_id")]];
		if($qnty_val=="") $qnty_val=$row[csf("qnty")]; else $qnty_val.="*".$row[csf("qnty")];
		if($rate_val=="") $rate_val=$row[csf("rate")]; else $rate_val.="*".$row[csf("rate")];
		if($amount_val=="") $amount_val=$row[csf("amount")]; else $amount_val.="*".$row[csf("amount")];
		if($excesscut_val=="") $excesscut_val=$row[csf("excess_cut")]; else $excesscut_val.="*".$row[csf("excess_cut")];
		if($plancut_val=="") $plancut_val=$row[csf("plan_cut")]; else $plancut_val.="*".$row[csf("plan_cut")];
		if($process_loss=="") $process_loss=$row[csf("process_loss")]; else $process_loss.="*".$row[csf("process_loss")];
	}
	echo "document.getElementById('hidden_item').value 	 					= '".$item_id."';\n";
	echo "document.getElementById('hidden_color').value 	 				= '".$color_fild_id."';\n";
	echo "document.getElementById('hidden_size').value 	 					= '".$size_number_id."';\n";
	echo "document.getElementById('hidden_qnty').value 	 					= '".$qnty_val."';\n";
	echo "document.getElementById('hidden_rate').value 	 					= '".$rate_val."';\n";
	echo "document.getElementById('hidden_amount').value 	 					= '".$amount_val."';\n";
	echo "document.getElementById('hidden_excess_cut').value 	 				= '".$excesscut_val."';\n";
	echo "document.getElementById('hidden_plan_cut').value 	 					= '".$plancut_val."';\n";
	echo "document.getElementById('hidden_loss').value 	 						= '".$process_loss."';\n";
	echo "document.getElementById('hidden_tbl_id_break').value 	 				= '".$id."';\n";
	exit();	
}


if($action=="subcontract_dtls_list_view")
{
	$sql = "select id,job_no_mst,order_no,order_quantity,order_uom,rate,amount,order_rcv_date,delivery_date,cust_buyer,cust_style_ref,main_process_id,status_active from subcon_ord_dtls where status_active<>3 and job_no_mst='$data'"; 
		
	$arr=array(2=>$unit_of_measurement,9=>$production_process,10=>$row_status);
	echo  create_list_view("list_view", "Order No,Order Qty,Order UOM,Rate/Unit,Amount,Ord Receive Date,Delivery Date,Cust Buyer,Cust Style Ref.,process,Status", "100,80,50,70,80,120,110,80,80,140,80","1070","320",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form_dtls'", 1, "0,0,order_uom,0,0,0,0,0,0,main_process_id,status_active", $arr , "order_no,order_quantity,order_uom,rate,amount,order_rcv_date,delivery_date,cust_buyer,cust_style_ref,main_process_id,status_active", "requires/sub_contract_order_entry_controller","","0,2,0,2,2,3,3,0,0,0,0") ;
	
	exit();	
}

if($action=="order_qnty_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
	$data=explode('_',$data);
	$order_no=$data[0];
	$process_id=$data[1];
	$order_id=$data[2];
	$company_id=str_replace("'","",$data[6]);
	$party_id=str_replace("'","",$data[7]);
	$break_id=$data[8];
	//echo $party_id;die;
	?>
	<script>
		var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name ", "color_name" ), 0, -1); ?> ];
		var str_size = [<?php echo substr(return_library_autocomplete( "select size_name from lib_size group by size_name ", "size_name" ), 0, -1); ?> ];

		function set_auto_complete(type)
		{
			if(type=='color_return')
			{
				$("#txtcolor_1").autocomplete({
				source: str_color
				});
			}
		}

		function set_auto_complete_size(type)
		{
			if(type=='size_return')
			{
				$("#txtsize_1").autocomplete({
				source: str_size
				});
			}
		}

		function add_share_row( i ) 
		{
			var row_num=$('#tbl_share_details_entry tr').length-1;
			if (row_num!=i)
			{
				return false;
			}
			i++;
			$("#tbl_share_details_entry tr:last").clone().find("input,select").each(function() {
				$(this).attr({
					'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
					'name': function(_, name) { return name + i },
					'value': function(_, value) { return value }              
				});
			}).end().appendTo("#tbl_share_details_entry");
			$('#increaseset_'+i).removeAttr("onClick").attr("onClick","add_share_row("+i+");");
			$('#txtorderquantity_'+i).removeAttr("onKeyUp").attr("onKeyUp","sum_total_qnty("+i+");");
			$('#txtorderrate_'+i).removeAttr("onKeyUp").attr("onKeyUp","sum_total_qnty("+i+");");
			$('#txtitem_'+i).removeAttr("onDblClick").attr("onDblClick","openmypage_item("+i+");");
			$('#excess_'+i).removeAttr("onKeyUp").attr("onKeyUp","sum_total_qnty("+i+");");
			
			//$('#decreaseset_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+','+'"tbl_share_details_entry"'+");");
			$('#decreaseset_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
			$('#txtsize_'+i).val('');
			$('#loss_'+i).val('');
			$('#hiddenid_'+i).val('');
			set_all_onclick();
			$("#txtcolor_"+i).autocomplete({
				source: str_color
			});
				$("#txtsize_"+i).autocomplete({
				source: str_size
			});
			sum_total_qnty(i);
		}
		
		/*function fn_deletebreak_down_tr(rowNo) 
		{   
			var numRow = $('table#tbl_share_details_entry tbody tr').length; 
			if(numRow==rowNo && rowNo!=1)
			{
				$('#tbl_share_details_entry tbody tr:last').remove();
			}
			sum_total_qnty(rowNo);
		}*/
		
	function fn_deletebreak_down_tr(rowNo) 
	{ 
		var numRow = $('table#tbl_share_details_entry tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			var updateIdDtls=$('#hiddenid_'+rowNo).val();
			var txt_deleted_id=$('#txt_deleted_id').val();
			var selected_id='';
		
			if(updateIdDtls!='')
			{
				if(txt_deleted_id=='') selected_id=updateIdDtls; else selected_id=txt_deleted_id+','+updateIdDtls;
				$('#txt_deleted_id').val( selected_id );
			}
			
			$('#tbl_share_details_entry tbody tr:last').remove();
		}
		else
		{
			return false;
		}
		
		sum_total_qnty(rowNo);
	}

		function fnc_close()
		{
			var tot_row=$('#tbl_share_details_entry tbody tr').length;
			var txt_item="";var txt_color=""; var txt_size=""; var txt_order_quantity=""; var txt_order_rate=""; var txt_order_amount=""; var excess_cut=""; var plan_cut="";  
			var process_loss="";var hidden_id="";
			var total_qnty="";var total_rate="";var total_amount="";
			var process_id=document.getElementById('hidden_process_id').value;
			
			for(var i=1; i<=tot_row; i++)
			{
				if (form_validation('txtorderquantity_'+i,'Quantity')==false )
				{
					return;
				}
				/*if(process_id==2 || process_id==3 || process_id==4 || process_id==6)
				{
					if (form_validation('loss_'+i,'Ps.Loss%')==false )
					{
						return;
					}
				}*/
				
				if(i>1)
				{
					txt_item +="*";
					txt_color +="*";
					txt_size +="*";
					txt_order_quantity +="*";
					txt_order_rate +="*";
					txt_order_amount +="*";
					excess_cut +="*";
					plan_cut +="*";
					process_loss +="*";
					hidden_id +="*";
				}
				txt_item += $("#hidditemid_"+i).val();
				txt_color += $("#txtcolor_"+i).val();
				txt_size += $("#txtsize_"+i).val();
				txt_order_quantity += $("#txtorderquantity_"+i).val();
				txt_order_rate += $("#txtorderrate_"+i).val();
				txt_order_amount += $("#txtorderamount_"+i).val();
				excess_cut += $("#excess_"+i).val();
				plan_cut += $("#plan_"+i).val();
				process_loss += $("#loss_"+i).val();
				hidden_id += $("#hiddenid_"+i).val();
			}
			document.getElementById('hidden_itemid').value=txt_item;
			document.getElementById('hidden_color').value=txt_color;
			document.getElementById('hidden_size').value=txt_size;
			document.getElementById('hidden_order_quantity').value=txt_order_quantity;
			document.getElementById('hidden_order_rate').value=txt_order_rate;
			document.getElementById('hidden_order_amount').value=txt_order_amount;
			document.getElementById('hidden_excess').value=excess_cut;
			document.getElementById('hidden_plan').value=plan_cut;
			document.getElementById('hidden_loss').value=process_loss;
			document.getElementById('hidden_tbl_id').value=hidden_id;
			document.getElementById('txt_deleted_id').value;
			
			parent.emailwindow.hide();
		}

		function sum_total_qnty(id)
		{
			var tot_row=$('#tbl_share_details_entry tbody tr').length;
			
			math_operation( "hidden_tot_rate", "txtorderrate_", "+", tot_row );
			
			$("#txtorderamount_"+id).val(($("#txtorderquantity_"+id).val()*1)*($("#txtorderrate_"+id).val()*1));
			$("#txt_average_rate").val(($("#hidden_tot_rate").val()*1)/tot_row*1);

			if($("#hidden_process_id").val()==1 || $("#hidden_process_id").val()==5 || $("#hidden_process_id").val()==8 || $("#hidden_process_id").val()==9 || $("#hidden_process_id").val()==10 || $("#hidden_process_id").val()==11)
			{
				$("#plan_"+id).val((($("#txtorderquantity_"+id).val()*1)+(($("#txtorderquantity_"+id).val()*1)*(($("#excess_"+id).val()*1)/100))));
			}
			
			math_operation( "txt_total_order_qnty", "txtorderquantity_", "+", tot_row );
			math_operation( "txt_total_order_amount", "txtorderamount_", "+", tot_row );
			
			// set_all_onclick();
		}

		<?php $color_library_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name");  ?>

		function openmypage_item(id)
		{
			var data=document.getElementById('hiddencompany').value+'_'+document.getElementById('hiddenparty').value+'_'+document.getElementById('hidden_process_id').value;
			page_link='sub_contract_order_entry_controller.php?action=order_qnty_item_popup&data='+data;
			emailwindow=dhtmlmodal.open('EmailBox','iframe',page_link,'Item Popup', 'width=600px, height=300px, center=1, resize=0, scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theemail=this.contentDoc.getElementById("hidd_item_id");
				//alert (theemail.value);return;
				var item_val=theemail.value.split("_");
				//alert(item_val[3]);
				if (theemail.value!="")
				{
					//alert (item_val[1]);
					//if(item_val[2]==undefined){ document.getElementById('txtcolor_'+id).value="";} else {document.getElementById('txtcolor_'+id).value=color_arr[item_val[2]];}
					
					document.getElementById('hidditemid_'+id).value=item_val[0];
					document.getElementById('txtitem_'+id).value=item_val[1];
					//if (document.getElementById('hidden_process_id').value!=1 || document.getElementById('hidden_process_id').value!=5)
					//{
						document.getElementById('txtorderrate_'+id).value=item_val[2];
						sum_total_qnty(id);
						document.getElementById('txtcolor_'+id).value=item_val[3];
					//}
					get_php_form_data(id+'*'+item_val[3], "populate_rate", "sub_contract_order_entry_controller" );
				}
			}
		}

        </script>
		</head>
		<body onLoad="set_auto_complete('color_return');set_auto_complete_size('size_return');">
		<div align="center" style="width:100%;" >
            <form name="qntypopup_1"  id="qntypopup_1" autocomplete="off">
                <table class="rpt_table" width="1050px" cellspacing="0" cellpadding="0" rules="all" id="tbl_share_details_entry">
                    <thead>
                        <th>Item Description</th>
                        <th>Color</th>
                        <th>GMTS Size</th>
                        <th class="must_entry_caption">Order Qty</th>
                        <th>Rate</th>
                        <th>Amount</th>
                        <th>Excess Cut%</th>
                        <th>Plan Cut Qty</th>
                        <th>Ps.Loss%</th>
                        <th>&nbsp;</th>
                    </thead>
                <tbody>
                    <input type="hidden" name="hiddencompany" id="hiddencompany" value="<?php echo $company_id;?>">
                    <input type="hidden" name="hiddenparty" id="hiddenparty" value="<?php echo $party_id;?>">
                    <input type="hidden" name="hidden_process_id" id="hidden_process_id" value="<?php echo $process_id; ?>">
                    <input type="text" name="txt_deleted_id" id="txt_deleted_id" class="text_boxes_numeric" style="width:90px" readonly />
                    
				<?php
					if($order_id=="")
					{
						$data_break=explode('_',$data_break);
						$break_item=explode('*',$data_break[0]);
						$break_color=explode('*',$data_break[1]);
						$break_size=explode('*',$data_break[2]);
						$break_qunty=explode('*',$data_break[3]);
						$break_rate=explode('*',$data_break[4]);
						$break_amount=explode('*',$data_break[5]);
						$break_excess=explode('*',$data_break[6]);
						$break_plan=explode('*',$data_break[7]);
						$break_loss=explode('*',$data_break[8]);
						
						if($process_id==2 || $process_id==3 || $process_id==4 || $process_id==6 || $process_id==7)
						{
							$garments_item=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');		
						}
						else
						{
							$garments_item;
						}
						$k=0;
						for($i=0; $i<count($break_item); $i++)
						{
							$k++;
							?>
							<tr>
								<td>
                                    <input type="hidden" name="hidditemid_<?php echo $k;?>" id="hidditemid_<?php echo $k;?>" style="width:40px" value="<?php echo $break_item[$i];?>">
                                    
                                    <input type="text" id="txtitem_<?php echo $k;?>" name="txtitem_<?php echo $k;?>" class="text_boxes" style="width:180px"  onDblClick="openmypage_item(<?php echo $k;?>);" placeholder="Double Click" value="<?php echo $garments_item[$break_item[$i]];?>" readonly /> 
								</td>
								<td>
                                    <input type="text" id="txtcolor_<?php echo $k;?>" name="txtcolor_<?php echo $k;?>" class="text_boxes" style="width:100px" value="<?php echo $break_color[$i]; ?>" /> 
								</td>
								<td>
                                    <input type="text" id="txtsize_<?php echo $k;?>" name="txtsize_<?php echo $k;?>" class="text_boxes" style="width:100px" value="<?php echo $break_size[$i]; ?>"/> 
								</td>
								<td>
                                    <input type="text" id="txtorderquantity_<?php echo $k;?>" name="txtorderquantity_<?php echo $k;?>" class="text_boxes_numeric" style="width:80px" onKeyUp="sum_total_qnty(<?php echo $k;?>)" value="<?php echo $break_qunty[$i]; ?>" /> 
								</td>
								<td>
                                    <input type="text" id="txtorderrate_<?php echo $k;?>" name="txtorderrate_<?php echo $k;?>"  class="text_boxes_numeric" style="width:70px" onKeyUp="sum_total_qnty(<?php echo $k;?>)" value="<?php echo $break_rate[$i]; ?>"/>
								</td>
								<td>
                                    <input type="text" id="txtorderamount_<?php echo $k;?>" name="txtorderamount_<?php echo $k;?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $break_amount[$i]; ?>" readonly/> 
								</td>
								<?php 
									if($process_id==1 || $process_id==5 || $process_id==8 || $process_id==9 || $process_id==10 || $process_id==11)
									{
										?>
										 <td>
                                            <input type="text" id="excess_<?php echo $k;?>" name="excess_<?php echo $k;?>" class="text_boxes_numeric" style="width:80px" onKeyUp="sum_total_qnty(<?php echo $k;?>)" value="<?php echo $break_excess[$i]; ?>"/> 
										</td>
										 <td>
                                            <input type="text" id="plan_<?php echo $k;?>" name="plan_<?php echo $k;?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $break_plan[$i]; ?>" readonly/> 
										</td>
									<?php
                                    }
                                    else
									{
                                    ?>
										 <td>
                                            <input type="text" id="excess_<?php echo $k;?>" name="excess_<?php echo $k;?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $break_excess[$i]; ?>" disabled/> 
										</td>
										 <td>
                                            <input type="text" id="plan_<?php echo $k;?>" name="plan_<?php echo $k;?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $break_plan[$i]; ?>" readonly disabled/> 
										</td>
										<?php
									}
								?>
								<td>
                                    <input type="text" id="loss_<?php echo $k;?>" name="loss_<?php echo $k;?>" class="text_boxes_numeric" style="width:45px" value="<?php echo $break_loss[$i]; ?>"/> 
								</td>
								 <td>
                                 	<input type="hidden" id="hiddenid_<?php echo $k; ?>" name="hiddenid_<?php echo $k; ?>"  style="width:15px;" class="text_boxes"/>
                                    <input type="button" id="increaseset_<?php echo $k;?>" style="width:30px" class="formbutton" value="+" onClick="add_share_row(<?php echo $k;?>)" />
                                    <input type="button" id="decreaseset_<?php echo $k;?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $k;?> ,'tbl_share_details_entry' );"/>
								</td>  
							</tr>
							 <?php 
						}
					}
					else 
					{
						$qry_result=sql_select( "select id, mst_id, order_id, item_id, color_id, size_id, qnty, rate, amount, excess_cut, plan_cut, process_loss from subcon_ord_breakdown where order_id='$order_id'");
						$k=0;
						$color_arr=return_library_array( "select id,color_name from lib_color",'id','color_name');
						$size_arr=return_library_array( "select id,size_name from  lib_size",'id','size_name');
						foreach ($qry_result as $row)
						{
							$k++;
							$id=$row[csf("id")];
							$item_id=$row[csf("item_id")];
							//echo $color_arr[$row[csf("color_id")]];
							$color_fild_id=$color_arr[$row[csf("color_id")]];
							$size_number_id=$size_arr[$row[csf("size_id")]];
							$qnty_val=$row[csf("qnty")];
							$rate_val=$row[csf("rate")];
							$amount_val=$row[csf("amount")];
							$excesscut_val=$row[csf("excess_cut")];	
							$plancut_val=$row[csf("plan_cut")];	
							$process_loss=$row[csf("process_loss")];
			
							if($process_id==2 || $process_id==3 || $process_id==4 || $process_id==6 || $process_id==7 )
							{
								$garments_item=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');		
							}
							else
							{
								$garments_item;
							}
							?>
							<tr class="general" >
								<td>
                                	<input type="hidden" name="hidditemid_<?php echo $k; ?>" id="hidditemid_<?php echo $k; ?>"  value="<?php echo $item_id; ?>"style="width:40px">
                                    <input type="text" id="txtitem_<?php echo $k; ?>" name="txtitem_<?php echo $k; ?>" value="<?php echo $garments_item[$item_id]; ?>" class="text_boxes" style="width:180px"  onDblClick="openmypage_item(<?php echo $k; ?>);" placeholder="Double Click" /> 
								</td>
								<td>
                                    <input type="text" id="txtcolor_<?php echo $k; ?>" name="txtcolor_<?php echo $k; ?>" value="<?php echo $color_fild_id; ?>" class="text_boxes" style="width:100px" /> 
								</td>
								<td>
                                    <input type="text" id="txtsize_<?php echo $k; ?>" name="txtsize_<?php echo $k; ?>"  value="<?php echo $size_number_id; ?>" class="text_boxes" style="width:100px"/> 
								</td>
								<td>
                                    <input type="text" id="txtorderquantity_<?php echo $k; ?>" name="txtorderquantity_<?php echo $k; ?>" value="<?php echo $qnty_val; ?>" class="text_boxes_numeric" style="width:80px" onKeyUp="sum_total_qnty(<?php echo $k; ?>)"/> 
								</td>
								<td>
                                    <input type="text" id="txtorderrate_<?php echo $k; ?>" name="txtorderrate_<?php echo $k; ?>" value="<?php echo $rate_val; ?>"  class="text_boxes_numeric" style="width:70px" onKeyUp="sum_total_qnty(<?php echo $k; ?>)"/>
								</td>
								<td>
                                    <input type="text" id="txtorderamount_<?php echo $k; ?>" name="txtorderamount_<?php echo $k; ?>" value="<?php echo $amount_val; ?>" class="text_boxes_numeric" style="width:80px"  readonly/> 
								</td>
								<?php 
									if($process_id==1 || $process_id==5 || $process_id==8 || $process_id==9 || $process_id==10 || $process_id==11)
									{
										?>
										 <td>
                                            <input type="text" id="excess_<?php echo $k; ?>" name="excess_<?php echo $k; ?>" value="<?php echo $excesscut_val; ?>" class="text_boxes_numeric" style="width:80px" onKeyUp="sum_total_qnty(<?php echo $k; ?>)"/> 
										</td>
										 <td>
                                            <input type="text" id="plan_<?php echo $k; ?>" name="plan_<?php echo $k; ?>"  value="<?php echo $plancut_val; ?>" class="text_boxes_numeric" style="width:80px"  readonly/> 
										</td>
									<?php
                                    }
                                    else
									{
                                    ?>
										 <td>
                                            <input type="text" id="excess_<?php echo $k; ?>" name="excess_<?php echo $k; ?>" class="text_boxes_numeric" style="width:80px" disabled/> 
										</td>
										 <td>
                                            <input type="text" id="plan_<?php echo $k; ?>" name="plan_<?php echo $k; ?>" class="text_boxes_numeric" style="width:80px"  readonly disabled/> 
										</td>
										<?php
									}
								?>
								<td>
                                    <input type="text" id="loss_<?php echo $k; ?>" name="loss_<?php echo $k; ?>" value="<?php echo $process_loss; ?>" class="text_boxes_numeric" style="width:45px"/> 
								</td>
								<td>
                                    <input type="hidden" id="hiddenid_<?php echo $k; ?>" name="hiddenid_<?php echo $k; ?>" value="<?php echo $id; ?>" style="width:15px;" class="text_boxes"/>
                                    <input type="button" id="increaseset_<?php echo $k; ?>" style="width:30px" class="formbutton" value="+" onClick="add_share_row(<?php echo $k; ?>)" />
                                    <input type="button" id="decreaseset_<?php echo $k; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $k; ?> ,'tbl_share_details_entry' );"/>
								</td>  
						   </tr>
							<?php	
						}
					}
				?> 
				</tbody>
            </table>
       		<table width="1000px" border="0" cellspacing="0" cellpadding="0" class="" rules="all">
                <tfoot>
                    <td width="120px">&nbsp;</td>
                    <td width="120px">&nbsp;</td>
                    <td width="120px">&nbsp;</td>
                    <td width="80px" align="center" >
                        <input type="text" id="txt_total_order_qnty"  name="txt_total_order_qnty"  class="text_boxes_numeric" value="<?php echo $data[3];?>" style="width:80px;" placeholder="Total qnty" disabled />
                    </td>
                    <td width="80px" align="center"><input type="hidden" name="hidden_tot_rate" id="hidden_tot_rate" ></td>
                    <td width="80px" align="center">
                        <input type="text" id="txt_total_order_amount" name="txt_total_order_amount" class="text_boxes_numeric" value="<?php echo $data[5];?>" style="width:80px;" placeholder="Total amount" disabled />
                    </td>
                    <td width="80px">&nbsp;</td>
                    <td width="80px">&nbsp;</td>
                    <td width="40px">&nbsp;</td>
                    <td width="65">&nbsp;</td>
                </tfoot>
            </table>
            <table>
                <tr>
                    <td>Average Rate</td>
                    <td> <input type="text" id="txt_average_rate"  name="txt_average_rate" class="text_boxes_numeric" value="<?php echo $data[4]; ?>" style="width:98px;" placeholder="Average rate" disabled /></td>
                </tr>
            </table>
            <br>
            <br>
            <table>
                <tr>
                    <td align="center"><input type="button" name="main_close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" /></td>
                </tr>
            </table>
             <input type="hidden" name="hidden_itemid" id="hidden_itemid">
             <input type="hidden" name="hidden_color" id="hidden_color">
             <input type="hidden" name="hidden_size" id="hidden_size">
             <input type="hidden" name="hidden_order_quantity" id="hidden_order_quantity">
             <input type="hidden" name="hidden_order_rate" id="hidden_order_rate">
             <input type="hidden" name="hidden_order_amount" id="hidden_order_amount">
             <input type="hidden" name="hidden_excess" id="hidden_excess">
             <input type="hidden" name="hidden_plan" id="hidden_plan">
             <input type="hidden" id="hidden_loss" name="hidden_loss" />
             <input type="hidden" name="hidden_tbl_id" id="hidden_tbl_id">
        </form>
    </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?php
	exit();
}

//$production_process=array(1=>"Cutting",2=>"Knitting",3=>"Dyeing",4=>"Finishing",5=>"Sewing",6=>"Fabric Printing",7=>"Washing",8=>"Printing",9=>"Embroidery");	
if($action=="order_qnty_item_popup")
{
		echo load_html_head_contents("Material Description Form", "../../", 1, 1,'',1,'');
		extract($_REQUEST);
		$ex_data=explode('_',$data);
		$company=$ex_data[0];
		$party=$ex_data[1];
		$process=$ex_data[2];
	if($process==2 || $process==3 || $process==4 || $process==6 || $process==7)
	{
		?>
		<script>
			function js_set_value(id)
			{ 
				document.getElementById('hidd_item_id').value=id;
				parent.emailwindow.hide();
			}
		</script>
		</head>
		<body>
            <div align="center" style="width:100%;" >
                <form name="searchpofrm"  id="searchpofrm">
                    <fieldset style="width:600px">
                    <input type="hidden" id="hidd_item_id" />
						<?php
							$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"); 
							if ($party!=0)
							{
								$sql ="select id, const_comp, uom_id, customer_rate, color_id from lib_subcon_charge  where comapny_id='$company' and buyer_id='$party' and rate_type_id='$process' and status_active=1
								union
								select id, const_comp, uom_id, customer_rate, color_id from lib_subcon_charge  where comapny_id='$company' and buyer_id=0 and rate_type_id='$process' and status_active=1
								";
							}
							else if ($process!=0 || $process!='')
							{
								$sql ="select id, const_comp, uom_id, customer_rate, color_id from lib_subcon_charge  where comapny_id='$company' and buyer_id='$party' and rate_type_id='$process' and status_active=1";
							}
							//echo $sql;die;
							$arr=array(1=>$unit_of_measurement,2=>$color_arr);
							echo  create_list_view("list_view", "Constuction Composition,UOM,Color,Rate", "230,60,120,60","550","250",0, $sql, "js_set_value", "id,const_comp,customer_rate,color_id", "", 1, "0,uom_id,color_id,0", $arr ,"const_comp,uom_id,color_id,customer_rate", "","","0,0,0,0") ;			
                        ?>
                    </fieldset>
                </form>
            </div>
		</body>           
		<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
		</html>
	<?php	
	}
	elseif($process==1 || $process==5 || $process==8 || $process==9 || $process==10 || $process==11)
	{
		echo load_html_head_contents("Material Description Form", "../../", 1, 1,'',1,'');
		extract($_REQUEST);
		?>
		<script>
			function js_set_value(id)
			{ 
				document.getElementById('hidd_item_id').value=id;
				parent.emailwindow.hide();
			}
		</script>
		</head>
		<body>
            <div align="center" style="width:100%;" >
                <form name="searchpofrm"  id="searchpofrm">
                <input type="hidden" id="hidd_item_id" />
                    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="500px" class="rpt_table">
                        <thead>
                            <th width="50" >SL</th>
                            <th width="100" >Garments Item</th>
                        </thead>
						<?php
							$i=1;
							$garments_item;
							foreach( $garments_item as $key=>$val )
							{
								if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";

								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value('<?php echo $key."_".$val."_".'0'."_".'0'; ?>');" >
                                    <td width="50" align="center"><?php echo $i; ?></td>
                                    <td width="100" align="left"><?php echo $val; ?></td>		
								</tr>
								<?php 
								$i++;
							}
                        ?>
                    </table>
                </form>
            </div>
		</body>           
		<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
		</html>
		<?php	
	}
	exit();
}

if($action=="populate_rate")
{
	$to_data=explode("*",$data);
	$color_library_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"); 
	$color_name=$color_library_arr[$to_data[1]];
	echo "document.getElementById('txtcolor_'+$to_data[0]).value='$color_name';";
	exit();
}
?>