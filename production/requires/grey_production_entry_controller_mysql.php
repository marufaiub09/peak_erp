<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

 //-------------------START ----------------------------------------
$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$count_arr = return_library_array("select id, yarn_count from lib_yarn_count","id","yarn_count");
$machine_arr = return_library_array("select id, machine_no from lib_machine_name","id","machine_no");
	
if($action=="load_drop_down_buyer")
{
	$data=explode("_",$data);
	echo create_drop_down( "cbo_buyer_name", 152, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[1]' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",$data[0] );  
	exit();
}

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 152, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "load_drop_down( 'requires/grey_production_entry_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_down_floor', 'floor_td' );" );
	exit();
}

if ($action=="load_drop_down_store")
{
	echo create_drop_down( "cbo_store_name", 152, "select id, store_name from lib_store_location where company_id='$data' and find_in_set(13,item_category_id) and status_active=1 and is_deleted=0 order by store_name","id,store_name", 1, "--Select Store--", 0, "" );
	exit();
}

if ($action=="load_drop_down_floor")
{
	$data=explode("_",$data);
	$company_id=$data[0];
	$location_id=$data[1];
	if($location_id==0 || $location_id=="") $location_cond=""; else $location_cond=" and b.location_id=$location_id";
	
	echo create_drop_down( "cbo_floor_id", 132, "select a.id, a.floor_name from lib_prod_floor a, lib_machine_name b where a.id=b.floor_id and b.category_id=1 and b.company_id=$company_id and b.status_active=1 and b.is_deleted=0 and a.production_process=2 $location_cond group by a.id order by a.floor_name","id,floor_name", 1, "-- Select Floor --", 0, "load_drop_down( 'requires/grey_production_entry_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_machine', 'machine_td' );","" );
  exit();	 
}


if ($action=="load_drop_machine")
{
	$data=explode("_",$data);
	$company_id=$data[0];
	$floor_id=$data[1];
	if($floor_id==0 || $floor_id=="") $floor_cond=""; else $floor_cond=" and floor_id=$floor_id";
	
	echo create_drop_down( "cbo_machine_name", 132, "select id,concat(machine_no,'-',brand) as machine_name from lib_machine_name where category_id=1 and company_id=$company_id and status_active=1 and is_deleted=0 and is_locked=0 $floor_cond order by machine_name","id,machine_name", 1, "-- Select Machine --", 0, "","" );
	exit();
}

if($action=="load_drop_down_knitting_com")
{
	$data = explode("_",$data);
	$company_id=$data[1];
	
	if($data[0]==1)
	{
		echo create_drop_down( "cbo_knitting_company", 152, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "--Select Knit Company--", "$company_id", "","" );
	}
	else if($data[0]==3)
	{	
		echo create_drop_down( "cbo_knitting_company", 152, "select id, supplier_name from lib_supplier where find_in_set(20,party_type) and find_in_set($company_id,tag_company) and status_active=1 and is_deleted=0 order by supplier_name","id,supplier_name", 1, "--Select Knit Company--", 1, "" );
	}
	else
	{
		echo create_drop_down( "cbo_knitting_company", 152, $blank_array,"",1, "--Select Knit Company--", 1, "" );
	}
	exit();
}

if ($action=="fabricBooking_popup")
{
	echo load_html_head_contents("WO Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
	
		function js_set_value(id,booking_no,type)
		{
			$('#hidden_booking_id').val(id);
			$('#hidden_booking_no').val(booking_no);
			$('#booking_without_order').val(type);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:930px;">
	<form name="searchwofrm"  id="searchwofrm" autocomplete=off>
		<fieldset style="width:930px; margin-left:3px">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" border="1" rules="all" width="720" class="rpt_table">
                <thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="200">Enter Booking No</th>
                    <th>
                    	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                    	<input type="hidden" name="hidden_booking_id" id="hidden_booking_id" class="text_boxes" value="">  
                        <input type="hidden" name="hidden_booking_no" id="hidden_booking_no" class="text_boxes" value=""> 
                        <input type="hidden" name="booking_without_order" id="booking_without_order" class="text_boxes" value=""> 
                    </th> 
                </thead>
                <tr>
                    <td align="center">
                    	<?php
							echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",$data[0] ); 
						?>       
                    </td>
                    <td align="center">	
                    	<?php
							if($recieve_basis==1)
							{
								$search_by_arr=array(1=>"Booking No",2=>"Buyer Order",3=>"Job No",4=>"Booking Date");
								$dd="change_search_event(this.value, '0*0*0*3', '0*0*0*2', '../../') ";	
							}
							else
							{
								$search_by_arr=array(1=>"Booking No",2=>"Buyer Order",3=>"Job No",4=>"Fabric Desc.",5=>"Program ID");
								$dd="change_search_event(this.value, '0*0*0*0*0', '0*0*0*0*0', '../../') ";
							}
							echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                    </td>                 
                    <td align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 						
            		<td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_company_id').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+<?php echo $recieve_basis; ?>, 'create_booking_search_list_view', 'search_div', 'grey_production_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                     </td>
                </tr>
           </table>
            <div style="margin-top:10px;" id="search_div" align="left"></div> 
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_booking_search_list_view")
{
	
	$data = explode("_",$data);
	
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$company_id =$data[2];
	$buyer_id =$data[3];
	$recieve_basis=$data[4];
	
	if($recieve_basis==1)
	{
		if($buyer_id==0) { echo "Please Select Buyer First."; die; }
		
		$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
		
		if(trim($data[0])!="")
		{
			if($search_by==1)
			{
				$search_field_cond="and a.booking_no like '$search_string'";
				$search_field_cond_sample="and s.booking_no like '$search_string'";
			}
			else if($search_by==2)	
			{
				$search_field_cond="and b.po_number like '$search_string'";
				$search_field_cond_sample="";
			}
			else if($search_by==3)	
			{
				$search_field_cond="and b.job_no_mst like '$search_string'";
				$search_field_cond_sample="";
			}
			else	
			{
				$search_field_cond="and a.booking_date like '".change_date_format(trim($data[0]), "yyyy-mm-dd", "-")."'";
				$search_field_cond_sample="and s.booking_date like '".change_date_format(trim($data[0]), "yyyy-mm-dd", "-")."'";
			}
		}
		else
		{
			$search_field_cond="";
		}
		
		if(trim($data[0])!="" && ($search_by==2 || $search_by==3))
		{
			$sql = "select a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, b.job_no_mst, 0 as type from wo_booking_mst a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_id=$company_id and a.buyer_id=$buyer_id and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $search_field_cond group by a.id"; 
		}
		else
		{
			$sql = "select a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, b.job_no_mst, 0 as type from wo_booking_mst a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_id=$company_id and a.buyer_id=$buyer_id and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $search_field_cond group by a.id
				union all
				SELECT s.id, s.booking_no, s.booking_date, s.buyer_id, '' as po_break_down_id, s.item_category, s.delivery_date, '' as job_no_mst, 1 as type FROM wo_non_ord_samp_booking_mst s WHERE s.company_id=$company_id and s.buyer_id=$buyer_id and s.status_active =1 and s.is_deleted=0 and s.item_category=2 $search_field_cond_sample 
		"; 
		}
		//echo $sql;
		$result = sql_select($sql);
		?>
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="910" class="rpt_table">
			<thead>
				<th width="30">SL</th>
				<th width="115">Booking No</th>
				<th width="80">Booking Date</th>               
				<th width="100">Buyer</th>
				<th width="85">Item Category</th>
				<th width="80">Delivary date</th>
				<th width="90">Job No</th>
				<th width="90">Order Qnty</th>
				<th width="80">Shipment Date</th>
				<th>Order No</th>
			</thead>
		</table>
		<div style="width:928px; max-height:280px; overflow-y:scroll" id="list_container_batch" align="left">	 
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="910" class="rpt_table" id="tbl_list_search">  
			<?php
				$i=1;
				foreach ($result as $row)
				{  
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";	 
					
					$po_qnty_in_pcs=''; $po_no=''; $min_shipment_date='';
					
					if($row[csf('po_break_down_id')]!="")
					{
						$po_sql="select b.po_number, b.pub_shipment_date, b.po_quantity, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($row[po_break_down_id]) group by b.id";									
						$nameArray=sql_select($po_sql);
						foreach ($nameArray as $po_row)
						{
							if($po_no=="") $po_no=$po_row[csf('po_number')]; else $po_no.=",".$po_row[csf('po_number')];
							
							if($min_shipment_date=='')
							{
								$min_shipment_date=$po_row[csf('pub_shipment_date')];
							}
							else
							{
								if($po_row[csf('pub_shipment_date')]<$min_shipment_date) $min_shipment_date=$po_row[csf('pub_shipment_date')]; else $min_shipment_date=$min_shipment_date;
							}
							
							$po_qnty_in_pcs+=$po_row[csf('po_qnty_in_pcs')];
						}
					}
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>,'<?php echo $row[csf('booking_no')]; ?>','<?php echo $row[csf('type')]; ?>');"> 
						<td width="30"><?php echo $i; ?></td>
						<td width="115"><p><?php echo $row[csf('booking_no')]; ?></p></td>
						<td width="80" align="center"><?php echo change_date_format($row[csf('booking_date')]); ?></td>               
						<td width="100"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
						<td width="85"><p><?php echo $item_category[$row[csf('item_category')]]; ?></p></td>
						<td width="80" align="center"><?php echo change_date_format($row[csf('delivery_date')]); ?>&nbsp;</td>
						<td width="90"><p><?php echo $row[csf('job_no_mst')]; ?>&nbsp;</p></td>
						<td width="90" align="right"><?php echo $po_qnty_in_pcs; ?>&nbsp;</td>
						<td width="80" align="center"><?php echo change_date_format($min_shipment_date); ?>&nbsp;</td>
						<td><p><?php echo $po_no; ?>&nbsp;</p></td>
					</tr>
				<?php
				$i++;
				}
				?>
			</table>
		</div>
	<?php	
	}
	else
	{
		$po_array=array();
		$po_sql=sql_select("select id, job_no_mst, po_number from wo_po_break_down");
		foreach($po_sql as $row)
		{
			$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
			$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no_mst')]; 
		}
		
		$rqsn_array=array();
		$reqsn_dataArray=sql_select("select a.knit_id, a.requisition_no, group_concat(distinct(yarn_count_id)) as yarn_count_id, group_concat(distinct(lot)) as lot from ppl_yarn_requisition_entry a, product_details_master b where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 group by a.knit_id");
		foreach($reqsn_dataArray as $row)
		{
			$rqsn_array[$row[csf('knit_id')]]['rqsn_no']=$row[csf('requisition_no')];
			$rqsn_array[$row[csf('knit_id')]]['count']=$row[csf('yarn_count_id')];
			$rqsn_array[$row[csf('knit_id')]]['lot']=$row[csf('lot')];
		}
		
		$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');

		$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_id group by dtls_id", "dtls_id", "po_id"  );

		if($buyer_id==0) $buyer="%%"; else $buyer=$buyer_id;
		
		if(trim($data[0])!="")
		{
			if($search_by==1)
			{
				$search_field_cond="and a.booking_no like '$search_string'";
			}
			else if($search_by==2 || $search_by==3)	
			{
				if($search_by==2) $search_field='po_number'; else $search_field='job_no_mst';
				
				$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","$search_field like '$search_string' and status_active=1 and is_deleted=0","po_id");
				
				$search_field_cond="and c.po_id in(".$po_id.")";
			}
			else if($search_by==4)
			{
				$search_field_cond="and a.fabric_desc like '$search_string'";
			}
			else	
			{
				if(trim($data[0])=="") $search_field_cond=""; else $search_field_cond="and b.id='".trim($data[0])."'";
			}
		}
		else
		{
			$search_field_cond="";
		}
		
		$sql = "select a.id, a.booking_no, a.buyer_id, a.body_part_id, a.determination_id, a.fabric_desc, a.gsm_weight, a.dia, b.color_range, b.id as knit_id, b.knitting_source, b.knitting_party, b.stitch_length from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id=$company_id and a.buyer_id like '$buyer' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $search_field_cond group by b.id"; 
		//echo $sql;
		$result = sql_select($sql);
		?>
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="910" class="rpt_table">
			<thead>
				<th width="30">SL</th>
                <th width="60">Plan Id</th>
                <th width="60">Program Id</th>
                <th width="50">Reqsn. No</th>
				<th width="110">Booking No</th>
				<th width="70">Buyer</th>
				<th width="100">PO No</th>
				<th width="90">Job No</th>
                <th width="130">Fabric Desc</th>
				<th width="55">Gsm</th>
				<th width="55">Dia</th>
				<th>Color Range</th>
			</thead>
		</table>
		<div style="width:928px; max-height:280px; overflow-y:scroll" id="list_container_batch" align="left">	 
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="910" class="rpt_table" id="tbl_list_search">
            <?php
				$i=1;
				foreach ($result as $row)
				{  
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";	 
						
					$reqn_no=$rqsn_array[$row[csf('knit_id')]]['rqsn_no'];
					
					$po_id=explode(",",$plan_details_array[$row[csf('knit_id')]]);
					$po_no=''; $job_no='';
					
					foreach($po_id as $val)
					{
						if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
						if($job_no=='') $job_no=$po_array[$val]['job_no'];
					}
					
					$data=$row[csf('body_part_id')]."**".$row[csf('determination_id')]."**".$row[csf('fabric_desc')]."**".$row[csf('gsm_weight')]."**".$row[csf('dia')]."**".$job_no."**".$row[csf('buyer_id')]."**".$plan_details_array[$row[csf('knit_id')]]."**".$row[csf('knitting_source')]."**".$row[csf('knitting_party')]."**".$row[csf('stitch_length')]."**".$row[csf('color_range')]."**".$rqsn_array[$row[csf('knit_id')]]['count']."**".$rqsn_array[$row[csf('knit_id')]]['lot'];
					
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf('knit_id')]; ?>,'<?php echo $data; ?>','0');"> 
						<td width="30"><?php echo $i; ?></td>
						<td width="60" align="center"><?php echo $row[csf('id')]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf('knit_id')]; ?></td>
						<td width="50" align="center"><?php echo $reqn_no; ?>&nbsp;</td>
                        <td width="110"><p><?php echo $row[csf('booking_no')]; ?></p></td>               
						<td width="70"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                        <td width="100"><p><?php echo $po_no; ?></p></td>
                        <td width="90"><p><?php echo $job_no; ?></p></td>
						<td width="130"><p><?php echo $row[csf('fabric_desc')]; ?></p></td>
						<td width="55"><p><?php echo $row[csf('gsm_weight')]; ?></p></td>
						<td width="55"><p><?php echo $row[csf('dia')]; ?></p></td>
						<td><p><?php echo $color_range[$row[csf('color_range')]]; ?></p></td>
					</tr>
				<?php
				$i++;
				}
				?>
            </table>
        </div>  
	<?php
	}
	
exit();
}
if($action=='populate_data_from_booking')
{
	$data=explode("**",$data);
	$booking_id=$data[0];
	$is_sample=$data[1];
	
	if($is_sample==0)
	{
		$sql="select booking_no, buyer_id, job_no from wo_booking_mst where id='$booking_id'";
	}
	else
	{
		$sql="select booking_no, buyer_id, '' as job_no from wo_non_ord_samp_booking_mst where id='$booking_id'";
	}
	
	$data_array=sql_select($sql);
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('txt_booking_no').value 				= '".$row[csf("booking_no")]."';\n";
		echo "document.getElementById('txt_booking_no_id').value 			= '".$booking_id."';\n";
		echo "document.getElementById('cbo_buyer_name').value 				= '".$row[csf("buyer_id")]."';\n";
		echo "document.getElementById('txt_job_no').value 					= '".$row[csf("job_no")]."';\n";
		echo "document.getElementById('booking_without_order').value 		= '".$is_sample."';\n";
		
		$yarn_issued=return_field_value("sum(b.cons_quantity) as qnty","inv_issue_master a, inv_transaction b","a.id=b.mst_id and a.entry_form=3 and a.item_category=1 and a. 	booking_no='$row[booking_no]' and b.item_category=1 and b.transaction_type=2 and a.status_active=1 and a.is_deleted=0","qnty");
		
		echo "document.getElementById('txt_yarn_issued').value 				= '".$yarn_issued."';\n";
		
		if($is_sample==1)
		{
			echo "$('#txt_receive_qnty').removeAttr('readonly','readonly');\n";
			echo "$('#txt_receive_qnty').removeAttr('onClick','onClick');\n";	
			echo "$('#txt_receive_qnty').removeAttr('placeholder','placeholder');\n";		
		}
		else
		{
			echo "$('#txt_receive_qnty').attr('readonly','readonly');\n";
			echo "$('#txt_receive_qnty').attr('onClick','openmypage_po();');\n";	
			echo "$('#txt_receive_qnty').attr('placeholder','Single Click');\n";	
		}
		
		exit();
	}
}

if($action=='show_fabric_desc_listview')
{
	$data=explode("**",$data);
	$booking_no=$data[0];
	$is_sample=$data[1];
	
	$composition_arr=array();
 	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		/*foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]].$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
			}
		}*/
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}

	//$data_array=sql_select("select b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, b.dia_width, sum(a.grey_fab_qnty) as qnty from wo_booking_dtls a, view_wo_fabric_booking_data_park b where a.pre_cost_fabric_cost_dtls_id=b.pre_cost_fabric_cost_dtls_id and a.booking_no='$data' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, b.dia_width");

	if($is_sample==0)
	{
		$sql="select a.id, b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, a.dia_width, sum(a.grey_fab_qnty) as qnty from wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.booking_no='$booking_no' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by b.lib_yarn_count_deter_id, b.body_part_id, b.gsm_weight, a.dia_width";
	}
	else
	{
		$sql="select lib_yarn_count_deter_id, body_part as body_part_id, gsm_weight, dia_width, construction, composition, sum(grey_fabric) as qnty from wo_non_ord_samp_booking_dtls where booking_no='$booking_no' and status_active=1 and is_deleted=0 group by lib_yarn_count_deter_id, body_part, gsm_weight, dia_width, construction, composition";
	}
	//echo $sql;
	$data_array=sql_select($sql);
	
	?>
    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300">
        <thead>
            <th>SL</th>
            <th>Fabric Description</th>
            <th width="60">Qnty</th>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach($data_array as $row)
            {  
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";
				
				/*$determination_sql=sql_select("select a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id and a.id=$row[lib_yarn_count_deter_id]");
				
				if($determination_sql[0][csf('construction')]!="")
				{
					$comp=$determination_sql[0][csf('construction')].", ";
				}
				
				foreach( $determination_sql as $d_row )
				{
					$comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
				}
				
				$fabric_desc=$body_part[$row[csf('body_part_id')]].", ".$comp.", ".$row[csf('gsm_weight')].", ".$row[csf('dia_width')];*/
				
				$cons_composition='';
				
				if($is_sample==1 && $row[csf('lib_yarn_count_deter_id')]==0)
				{
					$fabric_desc=$body_part[$row[csf('body_part_id')]].", ".$row[csf('construction')].", ".$row[csf('composition')].", ".$row[csf('gsm_weight')];
					$cons_composition=$row[csf('construction')].", ".$row[csf('composition')];
				}
				else
				{
					$fabric_desc=$body_part[$row[csf('body_part_id')]].", ".$composition_arr[$row[csf('lib_yarn_count_deter_id')]].", ".$row[csf('gsm_weight')];
					$cons_composition=$composition_arr[$row[csf('lib_yarn_count_deter_id')]];
				}
				
				if($row[csf('dia_width')]!="")
				{
					$fabric_desc.=", ".$row[csf('dia_width')];	
				}
				
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" onClick='set_form_data("<?php echo $row[csf('body_part_id')]."**".$cons_composition."**".$row[csf('gsm_weight')]."**".$row[csf('dia_width')]."**".$row[csf('lib_yarn_count_deter_id')]; ?>")' style="cursor:pointer" >
                    <td><?php echo $i; ?></td>
                    <td><?php echo $fabric_desc; ?></td>
                    <td align="right"><?php echo number_format($row[csf('qnty')],2); ?></td>
                </tr>
            <?php 
            $i++; 
            } 
            ?>
        </tbody>
    </table>
<?php
exit();
}
if ($action=="fabricDescription_popup")
{
	echo load_html_head_contents("Fabric Description Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
		
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
		
		function js_set_value(id,comp,gsm)
		{
			$('#hidden_desc_id').val(id);
			$('#hidden_desc_no').val(comp);
			$('#hidden_gsm').val(gsm);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:720px;margin-left:10px">
			<?php
                $data_array=sql_select("select id, construction, fab_nature_id, gsm_weight from lib_yarn_count_determina_mst where status_active=1 and is_deleted=0");
            ?>
            <input type="hidden" name="hidden_desc_id" id="hidden_desc_id" class="text_boxes" value="">
            <input type="hidden" name="hidden_desc_no" id="hidden_desc_no" class="text_boxes" value="">  
            <input type="hidden" name="hidden_gsm" id="hidden_gsm" class="text_boxes" value="">  
            
            <div style="margin-left:10px; margin-top:10px">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="680">
                    <thead>
                        <th width="50">SL</th>
                        <th width="100">Fabric Nature</th>
                        <th width="150">Construction</th>
                        <th width="100">GSM/Weight</th>
                        <th>Composition</th>
                    </thead>
                </table>
                <div style="width:700px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="680" id="tbl_list_search">  
                        <?php 
                        $i=1;
                        foreach($data_array as $row)
                        {  
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
                            
							if($row[csf('construction')]!="")
							{    
                            	$comp=$row[csf('construction')].", ";
							}
                            $determ_sql=sql_select("select copmposition_id, percent from lib_yarn_count_determina_dtls where mst_id=$row[id] and status_active=1 and is_deleted=0");
                            foreach( $determ_sql as $d_row )
                            {
                                $comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
                            }
                            
                         ?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value(<?php echo $row[csf('id')]; ?>,'<?php echo $comp; ?>','<?php echo $row[csf('gsm_weight')]; ?>')" style="cursor:pointer" >
                                <td width="50"><?php echo $i; ?></td>
                                <td width="100"><?php echo $item_category[$row[csf('fab_nature_id')]]; ?></td>
                                <td width="150"><p><?php echo $row[csf('construction')]; ?></p></td>
                                <td width="100"><?php echo $row[csf('gsm_weight')]; ?></td>
                                <td><p><?php echo $comp; ?></p></td>
                            </tr>
                        <?php 
                        $i++; 
                        } 
                        ?>
                    </table>
                </div> 
            </div>
		</fieldset>
	</form>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if ($action=="po_popup")
{
	echo load_html_head_contents("PO Info", "../../", 1, 1,'','','');
	extract($_REQUEST);

	$data=explode("_",$data);
	$po_id=$data[0]; $type=$data[1];
	
	if($type==1) 
	{
		$dtls_id=$data[2]; 
		$roll_maintained=$data[3]; 
		$save_data=$data[4]; 
		$prev_distribution_method=$data[5]; 
		$receive_basis=$data[6]; 
	}
	
	$recv_qnty_array=array();
	if($receive_basis==1 || $receive_basis==2)
	{
		$recvData=sql_select("select po_breakdown_id, 
					sum(case when entry_form=2 then quantity end) as grey_fabric_recv,
					sum(case when entry_form=13 and trans_type=5 then quantity end) as grey_fabric_trans_recv, 
					sum(case when entry_form=13 and trans_type=6 then quantity end) as grey_fabric_trans_issued
	 from order_wise_pro_details a, product_details_master b where a.prod_id=b.id and b.detarmination_id='$fabric_desc_id' and b.gsm='$txt_gsm' and b.dia_width='$txt_width' and a.trans_id<>0 and a.is_deleted=0 and a.status_active=1 group by a.po_breakdown_id");	
	 	foreach($recvData as $row)
		{
			$recv_qnty_array[$row[csf('po_breakdown_id')]]=$row[csf('grey_fabric_recv')]+$row[csf('grey_fabric_trans_recv')]-$row[csf('grey_fabric_trans_issued')];	
		}
	}
?> 

	<script>
		var receive_basis=<?php echo $receive_basis; ?>;
		var roll_maintained=<?php echo $roll_maintained; ?>;
		
		function fn_show_check()
		{
			if( form_validation('cbo_buyer_name','Buyer Name')==false )
			{
				return;
			}			
			show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+<?php echo $cbo_company_id; ?>+'_'+document.getElementById('cbo_buyer_name').value+'_'+'<?php echo $all_po_id; ?>', 'create_po_search_list_view', 'search_div', 'grey_production_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1);hidden_field_reset();');
			set_all();
		}
		
		function distribute_qnty(str)
		{
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_grey_qnty=$('#txt_prop_grey_qnty').val()*1;
				var tblRow = $("#tbl_list_search tr").length;
				var len=totalGrey=0;
				
				$("#tbl_list_search").find('tr').each(function()
				{
					len=len+1;
					
					var txtOrginal=$(this).find('input[name="txtOrginal[]"]').val()*1;
					var isDisbled=$(this).find('input[name="txtGreyQnty[]"]').is(":disabled");
					
					if(txtOrginal==0)
					{
						$(this).remove();
					}
					else if(isDisbled==false && txtOrginal==1)
					{
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;
						
						var grey_qnty=(perc*txt_prop_grey_qnty)/100;
						totalGrey = totalGrey*1+grey_qnty*1;
						totalGrey = totalGrey.toFixed(2);
												
						if(tblRow==len)
						{
							var balance = txt_prop_grey_qnty-totalGrey;
							if(balance!=0) grey_qnty=grey_qnty+(balance);							
						}
						
						$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
					}
				});
			}
			else
			{
				$('#txt_prop_grey_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					if($(this).find('input[name="txtGreyQnty[]"]').is(":disabled")==false)
					{
						$(this).find('input[name="txtGreyQnty[]"]').val('');
					}
				});
			}
		}
		
		function roll_duplication_check(row_id)
		{
			var row_num=$('#tbl_list_search tr').length;
			var po_id=$('#txtPoId_'+row_id).val();
			var roll_no=$('#txtRoll_'+row_id).val();
			
			if(roll_no*1>0)
			{
				for(var j=1; j<=row_num; j++)
				{
					if(j==row_id)
					{
						continue;
					}
					else
					{
						var po_id_check=$('#txtPoId_'+j).val();
						var roll_no_check=$('#txtRoll_'+j).val();	
			
						if(po_id==po_id_check && roll_no==roll_no_check)
						{
							alert("Duplicate Roll No.");
							$('#txtRoll_'+row_id).val('');
							return;
						}
					}
				}
				
				var txtRollTableId=$('#txtRollTableId_'+row_id).val();
				var data=po_id+"**"+roll_no+"**"+txtRollTableId;
				var response=return_global_ajax_value( data, 'roll_duplication_check', '', 'grey_production_entry_controller');
				var response=response.split("_");
				
				if(response[0]!=0)
				{
					var po_number=$('#tr_'+row_id).find('td:first').text();
					alert("This Roll Already Used. Duplicate Not Allowed");
					$('#txtRoll_'+row_id).val('');
					return;
				}
			}
			
		}
		
		function add_break_down_tr( i )
		{ 
			var cbo_distribiution_method=$('#cbo_distribiution_method').val();
			var isDisbled=$('#txtGreyQnty_'+i).is(":disabled");
			
			if(cbo_distribiution_method==2 && isDisbled==false)
			{
				//var row_num=$('#tbl_list_search tr').length;
				var row_num=$('#txt_tot_row').val();
				row_num++;
				
				var clone= $("#tr_"+i).clone();
				clone.attr({
					id: "tr_" + row_num,
				});
				
				clone.find("input,select").each(function(){
					  
				$(this).attr({ 
				  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ row_num },
				  'name': function(_, name) { return name },
				  'value': function(_, value) { return value }              
				});
				 
				}).end();
				
				$("#tr_"+i).after(clone);
				
				$('#txtOrginal_'+row_num).removeAttr("value").attr("value","0");
				
				$('#txtRoll_'+row_num).removeAttr("value").attr("value","");
				$('#txtGreyQnty_'+row_num).removeAttr("value").attr("value","");
				$('#txtRoll_'+row_num).removeAttr("onBlur").attr("onBlur","roll_duplication_check("+row_num+");");
					
				$('#increase_'+row_num).removeAttr("value").attr("value","+");
				$('#decrease_'+row_num).removeAttr("value").attr("value","-");
				$('#increase_'+row_num).removeAttr("onclick").attr("onclick","add_break_down_tr("+row_num+");");
				$('#decrease_'+row_num).removeAttr("onclick").attr("onclick","fn_deleteRow("+row_num+");");
				
				$('#txt_tot_row').val(row_num);
				set_all_onclick();
			}
		}
		
		function fn_deleteRow(rowNo) 
		{ 
			var cbo_distribiution_method=$('#cbo_distribiution_method').val();
			
			if(cbo_distribiution_method==2)
			{
				var txtOrginal=$('#txtOrginal_'+rowNo).val()*1;
				if(txtOrginal==0)
				{
					$("#tr_"+rowNo).remove();
				}
			}
		}
		
		var selected_id = new Array();
		
		 function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i,1 );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_po_row_id').value;
			if(old!="")
			{   
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{  
					js_set_value( old[i],0 ) 
				}
			}
		}
		
		function js_set_value( str, check_or_not ) 
		{
			if(check_or_not==1)
			{
				var roll_used=$('#roll_used'+str).val();
				if(roll_used==1)
				{
					var po_number=$('#search' + str).find("td:eq(3)").text();
					alert("Batch Roll Found Against PO- "+po_number);
					return;
				}
			}
			
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#po_id').val( id );
		}
		
		function show_grey_prod_recv() 
		{ 
			var po_id=$('#po_id').val();
			show_list_view ( po_id+'_'+'1'+'_'+'<?php echo $dtls_id; ?>'+'_'+'<?php echo $roll_maintained; ?>'+'_'+'<?php echo $save_data; ?>'+'_'+'<?php echo $prev_distribution_method; ?>'+'_'+'<?php echo $receive_basis; ?>', 'po_popup', 'search_div', 'grey_production_entry_controller', '');
		}
		
		function hidden_field_reset()
		{
			$('#po_id').val('');
			$('#save_string').val( '' );
			$('#tot_grey_qnty').val( '' );
			$('#number_of_roll').val( '' );
			selected_id = new Array();
		}
		
		function fnc_close()
		{
			var save_string='';	 var tot_grey_qnty=''; var no_of_roll=''; 
			var po_id_array = new Array();
			
			$("#tbl_list_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtGreyQnty=$(this).find('input[name="txtGreyQnty[]"]').val();
				var txtRoll=$(this).find('input[name="txtRoll[]"]').val();
				var txtRollId=$(this).find('input[name="txtRollId[]"]').val();
				var txtRollTableId=$(this).find('input[name="txtRollTableId[]"]').val();
				
				tot_grey_qnty=tot_grey_qnty*1+txtGreyQnty*1;
				
				if(roll_maintained==0)
				{
					txtRoll=0;
				}
				
				if(txtRoll*1>0)
				{
					no_of_roll=no_of_roll*1+1;	
				}
				
				if(txtGreyQnty*1>0)
				{
					
					if(save_string=="")
					{
						save_string=txtPoId+"**"+txtGreyQnty+"**"+txtRoll+"**"+txtRollId+"**"+txtRollTableId;
					}
					else
					{
						save_string+=","+txtPoId+"**"+txtGreyQnty+"**"+txtRoll+"**"+txtRollId+"**"+txtRollTableId;
					}
					
					if( jQuery.inArray( txtPoId, po_id_array) == -1 ) 
					{
						po_id_array.push(txtPoId);
					}
				}
			});
			
			$('#save_string').val( save_string );
			$('#tot_grey_qnty').val( tot_grey_qnty.toFixed(2) );
			$('#number_of_roll').val( no_of_roll );
			$('#all_po_id').val( po_id_array );
			$('#distribution_method').val( $('#cbo_distribiution_method').val() );	
			
			parent.emailwindow.hide();
		}
    </script>

</head>

<body>
	<?php
	if($type!=1)
	{
	?>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:10px">
        	<input type="hidden" name="save_string" id="save_string" class="text_boxes" value="">
            <input type="hidden" name="tot_grey_qnty" id="tot_grey_qnty" class="text_boxes" value="">
            <input type="hidden" name="number_of_roll" id="number_of_roll" class="text_boxes" value="">
            <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
            <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
	<?php
	}
	
	if($receive_basis==0 && $type!=1)
	{
	?>
		<table cellpadding="0" cellspacing="0" width="620" class="rpt_table">
			<thead>
				<th class="must_entry_caption">Buyer</th>
				<th>Search By</th>
				<th>Search</th>
				<th>
					<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
					<input type="hidden" name="po_id" id="po_id" value="">
				</th> 
			</thead>
			<tr class="general">
				<td align="center">
					<?php
						echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",$data[0] ); 
					?>       
				</td>
				<td align="center">	
					<?php
						$search_by_arr=array(1=>"PO No",2=>"Job No");
						echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
					?>
				</td>                 
				<td align="center">				
					<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
				</td> 						
				<td align="center">
					<input type="button" name="button2" class="formbutton" value="Show" onClick="fn_show_check();" style="width:100px;" />
				</td>
			</tr>
		</table>
		<div id="search_div" style="margin-top:10px">
       		<?php
			if($save_data!="")
			{
			?>
				<div style="width:600px; margin-top:10px" align="center">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
						<thead>
							<th>Total Grey Qnty</th>
							<th>Distribution Method</th>
						</thead>
						<tr class="general">
							<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php echo $txt_receive_qnty; ?>" style="width:120px" onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"></td>
							<td>
								<?php 
									$distribiution_method=array(1=>"Proportionately",2=>"Manually");
									echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0, "",$prev_distribution_method, "distribute_qnty(this.value);",0 );
								?>
							</td>
						</tr>
					</table>
				</div>
				<div style="margin-left:10px; margin-top:10px">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580">
						<thead>
							<th width="120">PO No</th>
							<th width="90">PO Qnty</th>
                            <th width="80">Ship. Date</th>
							<th>Grey Qnty</th>
							<?php
							if($roll_maintained==1)
							{
							?>
								<th width="100">Roll</th>
								<th width="65"></th>
							<?php
							}
							?>
						</thead>
					</table>
					<div style="width:600px; max-height:200px; overflow-y:scroll" id="list_container" align="left"> 
						<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="580" id="tbl_list_search">  
							<?php 
							$i=1; $tot_po_qnty=0; $po_array=array();  

							$explSaveData = explode(",",$save_data); 	
							for($z=0;$z<count($explSaveData);$z++)
							{
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
									
								$po_wise_data = explode("**",$explSaveData[$z]);
								$order_id=$po_wise_data[0];
								$grey_qnty=$po_wise_data[1];
								$roll_no=$po_wise_data[2];
								$roll_not_delete_id=$po_wise_data[3];
								$roll_id=$po_wise_data[4];
								
								$po_data=sql_select("select b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
								
								if($roll_maintained==1)
								{
									$roll_used=return_field_value("roll_used","pro_roll_details","id='$roll_id'");
									
									if(!(in_array($order_id,$po_array)))
									{
										if($roll_not_delete_id==0) $tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										if($roll_used==1) $orginal_val=1; else $orginal_val=0;
									}
									
									if($roll_used==1)
									{
										$disable="disabled='disabled'";
										$roll_not_delete_id=$roll_not_delete_id;
									}
									else
									{
										$disable="";
										$roll_not_delete_id=0;
									}
								}
								else
								{
									if(!(in_array($order_id,$po_array)))
									{
										$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										$orginal_val=0;
									}
									
									$roll_not_delete_id=0;
									$roll_id=0;
									$disable="";	
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="120">
										<p><?php echo $po_data[0][csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
                                        <!--txtRollId is used for not delete row which is used in batch -->
                                        <input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
                                        <!--txtRollTableId is used for Duplication Check -->
									</td>
									<td width="90" align="right">
										<?php echo $po_data[0][csf('po_qnty_in_pcs')] ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
									</td>
                                    <td width="80" align="center"><?php echo change_date_format($po_data[0][csf('pub_shipment_date')]); ?></td>
									<td align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?> >
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center" width="100">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++;
							}
							?>
							<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
                            <input type="hidden" name="txt_tot_row" id="txt_tot_row" class="text_boxes" value="<?php echo $i-1; ?>">
						</table>
					</div>
					<table width="620">
						 <tr>
							<td align="center" >
								<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
							</td>
						</tr>
					</table>
				</div>
			<?php
			}
			?>
        </div>
	<?php
	}
	else
	{
	?>
		<div style="width:600px; margin-top:10px" align="center">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
				<thead>
					<th>Total Grey Qnty</th>
					<th>Distribution Method</th>
				</thead>
				<tr class="general">
					<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php echo $txt_receive_qnty; ?>" style="width:120px" onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)" ></td>
					<td>
						<?php 
							$distribiution_method=array(1=>"Proportionately",2=>"Manually");
							echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0, "",$prev_distribution_method, "distribute_qnty(this.value);",0 );
						?>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:5px; margin-top:10px">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="600">
				<thead>
					<th width="110">PO No</th>
					<th width="90">PO Qnty</th>
                    <th width="80">Ship. Date</th>
                    <?php
					if($receive_basis==1 || $receive_basis==2)
					{
						echo '<th width="80">Req. Qnty</th>';
					}
					?>
					<th>Grey Qnty</th>
                    <?php
					if($roll_maintained==1)
					{
					?>
                        <th width="80">Roll</th>
                        <th width="65"></th>
					<?php
                    }
                    ?>
				</thead>
			</table>
			<div style="width:618px; max-height:200px; overflow-y:scroll" id="list_container" align="left"> 
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="600" id="tbl_list_search">  
					<?php 
					$i=1; $tot_po_qnty=0; $po_array=array();
					
					if($save_data!="" && $receive_basis==0)
					{ 
						$po_id = explode(",",$po_id);
						$explSaveData = explode(",",$save_data);
						for($z=0;$z<count($explSaveData);$z++)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$po_wise_data = explode("**",$explSaveData[$z]);
							$order_id=$po_wise_data[0];
							$grey_qnty=$po_wise_data[1];
							$roll_no=$po_wise_data[2];
							$roll_not_delete_id=$po_wise_data[3];
							$roll_id=$po_wise_data[4];
							
							if(in_array($order_id,$po_id))
							{
								$po_data=sql_select("select b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
								 
								if($roll_maintained==1)
								{
									$roll_used=return_field_value("roll_used","pro_roll_details","id='$roll_id'");
									
									if(!(in_array($order_id,$po_array)))
									{
										if($roll_not_delete_id==0) $tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										if($roll_used==1) $orginal_val=1; else $orginal_val=0;
									}
									
									if($roll_used==1)
									{
										$disable="disabled='disabled'";
										$roll_not_delete_id=$roll_not_delete_id;
									}
									else
									{
										$disable="";
										$roll_not_delete_id=0;
									}
										
								}
								else
								{
									if(!(in_array($order_id,$po_array)))
									{
										$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
										$orginal_val=1;
										$po_array[]=$order_id;
									}
									else
									{
										$orginal_val=0;
									}
									
									$roll_id=0;
									$roll_not_delete_id=0;
									$disable="";
								}
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="110">
										<p><?php echo $po_data[0][csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="90" align="right">
										<?php echo $po_data[0][csf('po_qnty_in_pcs')] ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
									</td>
                                    <td width="80" align="center"><?php echo change_date_format($po_data[0][csf('pub_shipment_date')]); ?></td>
									<td align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?> >
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center" width="80">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++;
							}
						}
						
						$result=implode(",",array_diff($po_id, $po_array));
						if($result!="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($result) order by b.pub_shipment_date, b.id";
							$nameArray=sql_select($po_sql);
							foreach($nameArray as $row)
							{  
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$orginal_val=1;
								$roll_id=0;
								$roll_not_delete_id=0;

								$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
								
							 ?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="110">
										<p><?php echo $row[csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="90" align="right">
										<?php echo $row[csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
									</td>
                                    <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
									<td align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="" >
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center" width="80">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="" onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++; 
							} 
						}
					}
					else if($save_data!="" && $receive_basis==1)
					{
						$all_po_id = explode(",",$all_po_id);
						$explSaveData = explode(",",$save_data); 
						
						$booking_qnty_array=return_library_array("select a.po_break_down_id, sum(a.grey_fab_qnty) as qnty from wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.booking_no='$booking_no' and b.lib_yarn_count_deter_id='$fabric_desc_id' and b.body_part_id='$cbo_body_part' and b.gsm_weight='$txt_gsm' and a.dia_width='$txt_width' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by a.po_break_down_id","po_break_down_id","qnty");

						for($z=0;$z<count($explSaveData);$z++)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$po_wise_data = explode("**",$explSaveData[$z]);
							$order_id=$po_wise_data[0];
							$grey_qnty=$po_wise_data[1];
							$roll_no=$po_wise_data[2];
							$roll_not_delete_id=$po_wise_data[3];
							$roll_id=$po_wise_data[4];
							
							$req_qnty=$booking_qnty_array[$order_id];
							$bl_qnty=$req_qnty-$recv_qnty_array[$order_id];
							
							$po_data=sql_select("select b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
							
							if($roll_maintained==1)
							{
								$roll_used=return_field_value("roll_used","pro_roll_details","id='$roll_id'");
								
								if(!(in_array($order_id,$po_array)))
								{
									if($roll_not_delete_id==0) $tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$orginal_val=1;
									$po_array[]=$order_id;
								}
								else
								{
									if($roll_used==1) $orginal_val=1; else $orginal_val=0;
								}
								
								if($roll_used==1)
								{
									$disable="disabled='disabled'";
									$roll_not_delete_id=$roll_not_delete_id;
								}
								else
								{
									$disable="";
									$roll_not_delete_id=0;
								}
									
							}
							else
							{
								if(!(in_array($order_id,$po_array)))
								{
									$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$orginal_val=1;
									$po_array[]=$order_id;
								}
								else
								{
									$orginal_val=0;
								}
								
								$roll_id=0;
								$roll_not_delete_id=0;
								$disable="";
							}
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="110">
									<p><?php echo $po_data[0][csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
									<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
									<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
									<!--This is used for not delete row which is used in batch -->
									<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
									<!--This is used for Duplication Check -->
								</td>
								<td width="90" align="right">
									<?php echo $po_data[0][csf('po_qnty_in_pcs')] ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
								</td>
                                <td width="80" align="center"><?php echo change_date_format($po_data[0][csf('pub_shipment_date')]); ?></td>
                                <td width="80" align="right"><?php echo number_format($req_qnty,2,'.',''); ?></td>
								<td align="center">
									<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?> placeholder="<?php echo number_format($bl_qnty,2,'.',''); ?>">
								</td>
								<?php
								if($roll_maintained==1)
								{
								?>
									<td align="center" width="80">
										<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
									</td>
									<td width="65">
										<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
										<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
									</td>
								<?php
								}
								?>
							</tr>
						<?php 
						$i++;
						}
						
						$booking_po_id=return_field_value("group_concat(distinct(po_break_down_id)) as po_id","wo_booking_dtls","booking_no='$booking_no' and status_active=1 and is_deleted=0","po_id");
						$booking_po_id=explode(",",$booking_po_id);
						$result=implode(",",array_diff($booking_po_id,$all_po_id));
						
						if($result!="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($result) order by b.pub_shipment_date, b.id";	
							$nameArray=sql_select($po_sql);
							foreach($nameArray as $row)
							{  
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$orginal_val=1;
								$roll_id=0;
								$roll_not_delete_id=0;
								$disable="";
								$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
								
								$req_qnty=$booking_qnty_array[$row[csf('id')]];
								$bl_qnty=$req_qnty-$recv_qnty_array[$row[csf('id')]];
								
							 ?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="110">
										<p><?php echo $row[csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="90" align="right">
										<?php echo $row[csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
									</td>
                                    <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($req_qnty,2,'.',''); ?></td>
									<td align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="" placeholder="<?php echo number_format($bl_qnty,2,'.',''); ?>" >
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center" width="80">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="" onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++; 
							} 
						}
					}
					else if($save_data!="" && $receive_basis==2)
					{
						$all_po_id = explode(",",$all_po_id);
						$explSaveData = explode(",",$save_data); 	
						
						$program_qnty_array=return_library_array("select po_id, sum(program_qnty) as qnty from ppl_planning_entry_plan_dtls where dtls_id='$booking_no' and determination_id='$fabric_desc_id' and body_part_id='$cbo_body_part' and gsm_weight='$txt_gsm' and dia='$txt_width' and status_active=1 and is_deleted=0 group by po_id","po_id","qnty");
						for($z=0;$z<count($explSaveData);$z++)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$po_wise_data = explode("**",$explSaveData[$z]);
							$order_id=$po_wise_data[0];
							$grey_qnty=$po_wise_data[1];
							$roll_no=$po_wise_data[2];
							$roll_not_delete_id=$po_wise_data[3];
							$roll_id=$po_wise_data[4];
							
							$req_qnty=$program_qnty_array[$order_id];
							$bl_qnty=$req_qnty-$recv_qnty_array[$order_id];
							
							$po_data=sql_select("select b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=$order_id");
							
							if($roll_maintained==1)
							{
								$roll_used=return_field_value("roll_used","pro_roll_details","id='$roll_id'");
								
								if(!(in_array($order_id,$po_array)))
								{
									if($roll_not_delete_id==0) $tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$orginal_val=1;
									$po_array[]=$order_id;
								}
								else
								{
									if($roll_used==1) $orginal_val=1; else $orginal_val=0;
								}
								
								if($roll_used==1)
								{
									$disable="disabled='disabled'";
									$roll_not_delete_id=$roll_not_delete_id;
								}
								else
								{
									$disable="";
									$roll_not_delete_id=0;
								}
									
							}
							else
							{
								if(!(in_array($order_id,$po_array)))
								{
									$tot_po_qnty+=$po_data[0][csf('po_qnty_in_pcs')];
									$orginal_val=1;
									$po_array[]=$order_id;
								}
								else
								{
									$orginal_val=0;
								}
								
								$roll_id=0;
								$roll_not_delete_id=0;
								$disable="";
							}
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="110">
									<p><?php echo $po_data[0][csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $order_id; ?>">
									<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
									<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
									<!--This is used for not delete row which is used in batch -->
									<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
									<!--This is used for Duplication Check -->
								</td>
								<td width="90" align="right">
									<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_data[0][csf('po_qnty_in_pcs')]; ?>">
								</td>
                                <td width="80" align="center"><?php echo change_date_format($po_data[0][csf('pub_shipment_date')]); ?></td>
                                <td width="80" align="right"><?php echo number_format($req_qnty,2,'.',''); ?></td>
								<td align="center">
									<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?> placeholder="<?php echo number_format($bl_qnty,2,'.',''); ?>">
								</td>
								<?php
								if($roll_maintained==1)
								{
								?>
									<td align="center" width="80">
										<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
									</td>
									<td width="65">
										<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
										<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
									</td>
								<?php
								}
								?>
							</tr>
						<?php 
						$i++;
						}
						
						$plan_po_id=return_field_value("group_concat(distinct(po_id)) as po_id","ppl_planning_entry_plan_dtls","dtls_id='$booking_no' and status_active=1 and is_deleted=0","po_id");
						$plan_po_id=explode(",",$plan_po_id);
						$result=implode(",",array_diff($plan_po_id,$all_po_id));
						
						if($result!="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($result) order by b.pub_shipment_date, b.id";	
							$nameArray=sql_select($po_sql);
							foreach($nameArray as $row)
							{  
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$orginal_val=1;
								$roll_id=0;
								$roll_not_delete_id=0;
								$disable="";
								$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
								
								$req_qnty=$program_qnty_array[$row[csf('id')]];
								$bl_qnty=$req_qnty-$recv_qnty_array[$row[csf('id')]];
								
							 ?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="110">
										<p><?php echo $row[csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="90" align="right">
										<?php echo $row[csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
									</td>
                                    <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($req_qnty,2,'.',''); ?></td>
									<td align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="" placeholder="<?php echo number_format($bl_qnty,2,'.',''); ?>">
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center" width="80">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="" onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++; 
							} 
						}
					}
					else
					{ 
						if($receive_basis==2)
						{
							$i=1;
							$orginal_val=1;
							$roll_id=0;
							$roll_not_delete_id=0;
							$disable="";

							//$po_data=sql_select("select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in($all_po_id) group by b.id");
							$program_qnty_array=return_library_array("select po_id, sum(program_qnty) as qnty from ppl_planning_entry_plan_dtls where dtls_id='$booking_no' and determination_id='$fabric_desc_id' and body_part_id='$cbo_body_part' and gsm_weight='$txt_gsm' and dia='$txt_width' and status_active=1 and is_deleted=0 group by po_id","po_id","qnty");
							$po_data=sql_select("select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b, ppl_planning_entry_plan_dtls c where a.job_no=b.job_no_mst and b.id=c.po_id and c.dtls_id=$booking_no group by b.id");
							
							foreach($po_data as $row)
							{
								$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
								$req_qnty=$program_qnty_array[$row[csf('id')]];
								$bl_qnty=$req_qnty-$recv_qnty_array[$row[csf('id')]];
								
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
							?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="110">
										<p><?php echo $row[csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_1" value="<?php echo $row[csf('id')]; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_1" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_1" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="90" align="right">
										<?php echo $row[csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
									</td>
                                    <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                                    <td width="80" align="right"><?php echo number_format($req_qnty,2,'.',''); ?></td>
									<td align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?> placeholder="<?php echo number_format($bl_qnty,2,'.',''); ?>">
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center" width="80">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
								$i++; 
							}
						}
						else
						{
							if($type==1)
							{
								if($po_id!="")
								{
									$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id) order by b.pub_shipment_date, b.id";
								}
							}
							else
							{
								$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b, wo_booking_dtls c where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' and c.status_active=1 and c.is_deleted=0 group by b.id order by b.pub_shipment_date, b.id";
								
								$booking_qnty_array=return_library_array("select a.po_break_down_id, sum(a.grey_fab_qnty) as qnty from wo_booking_dtls a, wo_pre_cost_fabric_cost_dtls b where a.pre_cost_fabric_cost_dtls_id=b.id and a.booking_no='$booking_no' and b.lib_yarn_count_deter_id='$fabric_desc_id' and b.body_part_id='$cbo_body_part' and b.gsm_weight='$txt_gsm' and a.dia_width='$txt_width' and a.job_no=b.job_no and a.status_active=1 and a.is_deleted=0 group by a.po_break_down_id","po_break_down_id","qnty");
							}
							
							$nameArray=sql_select($po_sql);
							foreach($nameArray as $row)
							{  
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$orginal_val=1;
								$roll_id=0;
								$roll_not_delete_id=0;
								$disable="";
								$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
								
							 ?>
								<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
									<td width="110">
										<p><?php echo $row[csf('po_number')]; ?></p>
										<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
										<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="<?php echo $orginal_val; ?>">
										<input type="hidden" name="txtRollId[]" id="txtRollId_<?php echo $i; ?>" value="<?php echo $roll_not_delete_id; ?>">
										<!--This is used for not delete row which is used in batch -->
										<input type="hidden" name="txtRollTableId[]" id="txtRollTableId_<?php echo $i; ?>" value="<?php echo $roll_id; ?>">
										<!--This is used for Duplication Check -->
									</td>
									<td width="90" align="right">
										<?php echo $row[csf('po_qnty_in_pcs')]; ?>
										<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
									</td>
                                    <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                                    <?php
									if($receive_basis==1)
									{
										$req_qnty=$booking_qnty_array[$row[csf('id')]];
										$bl_qnty=$req_qnty-$recv_qnty_array[$row[csf('id')]];
										echo '<td width="80" align="right">'.number_format($req_qnty,2,'.','').'</td>';
									}
									else {$bl_qnty=''; $req_qnty='';}
									?>
									<td align="center">
										<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="<?php echo $grey_qnty; ?>" <?php echo $disable; ?> placeholder="<?php if($receive_basis==1) echo number_format($bl_qnty,2,'.',''); ?>">
									</td>
									<?php
									if($roll_maintained==1)
									{
									?>
										<td align="center" width="80">
											<input type="text" name="txtRoll[]" id="txtRoll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:65px" value="<?php if($roll_no!=0) echo $roll_no; ?>" <?php echo $disable; ?> onBlur="roll_duplication_check(<?php echo $i; ?>);" />
										</td>
										<td width="65">
											<input type="button" id="increase_<?php echo $i; ?>" name="increase[]" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr( <?php echo $i; ?> )" />
											<input type="button" id="decrease_<?php echo $i; ?>" name="decrease[]" style="width:30px" class="formbutton" value="-" onClick="fn_deleteRow(<?php echo $i; ?>);" />
										</td>
									<?php
									}
									?>
								</tr>
							<?php 
							$i++; 
							} 
						}
					}
					?>
					<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
                    <input type="hidden" name="txt_tot_row" id="txt_tot_row" class="text_boxes" value="<?php echo $i-1; ?>">
				</table>
			</div>
			<table width="620">
				 <tr>
					<td align="center">
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
		</div>
	<?php
	}
	if($type!=1)
	{
	?>
		</fieldset>
	</form>
    <?php
	}
	?>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="create_po_search_list_view")
{
	$data = explode("_",$data);
	
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	
	if($search_by==1)
		$search_field='b.po_number';
	else
		$search_field='a.job_no';
		
	$company_id =$data[2];
	$buyer_id =$data[3];
	
	$all_po_id=$data[4];
	
	if($all_po_id!="")
		$po_id_cond=" or b.id in($all_po_id)";
	else 
		$po_id_cond="";
	
	$hidden_po_id=explode(",",$all_po_id);

	if($buyer_id==0) { echo "Please Select Buyer First."; die; }
	
	$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name=$buyer_id and $search_field like '$search_string' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $po_id_cond group by b.id"; 
	//echo $sql;die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Job No</th>
                <th width="110">Style No</th>
                <th width="110">PO No</th>
                <th width="90">PO Quantity</th>
                <th width="50">UOM</th>
                <th>Shipment Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:240px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1; $po_row_id='';
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
						
					$roll_used=0;
					
					if(in_array($selectResult[csf('id')],$hidden_po_id)) 
					{
						if($po_row_id=="") $po_row_id=$i; else $po_row_id.=",".$i;
						
						$roll_data_array=sql_select("select roll_no from pro_roll_details where po_breakdown_id=$selectResult[id] and roll_used=1 and entry_form=1 and status_active=1 and is_deleted=0");
						if(count($roll_data_array)>0)
						{
							$roll_used=1;
						}
						else
							$roll_used=0;
					}
							
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i; ?>,1)"> 
                            <td width="40" align="center">
								<?php echo $i; ?>
                            	<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>
                            	<input type="hidden" name="roll_used" id="roll_used<?php echo $i ?>" value="<?php echo $roll_used; ?>"/>	
                            </td>	
                            <td width="100"><p><?php echo $selectResult[csf('job_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                            <td width="90" align="right"><?php echo $selectResult[csf('po_qnty_in_pcs')]; ?></td> 
                            <td width="50" align="center"><p><?php echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                            <td align="center"><?php echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                        </tr>
                    <?php
                    $i++;
				}
			?>
				<input type="hidden" name="txt_po_row_id" id="txt_po_row_id" value="<?php echo $po_row_id; ?>"/>
            </table>
        </div>
         <table width="620" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                            <input type="button" name="close" onClick="show_grey_prod_recv();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>           
<?php
	
exit();
}

/*if($action=="check_machine_no")
{
	$sql="select id, machine_no from lib_machine_name where machine_no='".trim($data)."' and is_deleted=0 and status_active=1 and category_id=1";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('id')];
	}
	else
	{
		echo "0_";
	}
	
exit();	
}*/

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		$grey_recv_num=''; $grey_update_id=''; $flag=1;
		
		if(str_replace("'","",$update_id)=="")
		{
			$new_grey_recv_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'GPE', date("Y",time()), 5, "select recv_number_prefix, recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_id and entry_form='2' order by recv_number_prefix_num desc ", "recv_number_prefix", "recv_number_prefix_num" ));
		 	
			$id=return_next_id( "id", "inv_receive_master", 1 ) ;
					 
			$field_array="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, receive_basis, company_id, receive_date, challan_no, booking_id, booking_no, booking_without_order, store_id, location_id, knitting_source, knitting_company, buyer_id, yarn_issue_challan_no, remarks, inserted_by, insert_date";
			
			$data_array="(".$id.",'".$new_grey_recv_system_id[1]."',".$new_grey_recv_system_id[2].",'".$new_grey_recv_system_id[0]."',2,13,".$cbo_receive_basis.",".$cbo_company_id.",".$txt_receive_date.",".$txt_receive_chal_no.",".$txt_booking_no_id.",".$txt_booking_no.",".$booking_without_order.",".$cbo_store_name.",".$cbo_location_name.",".$cbo_knitting_source.",".$cbo_knitting_company.",".$cbo_buyer_name.",".$txt_yarn_issue_challan_no.",".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into inv_receive_master (".$field_array.") values ".$data_array;die;
			$rID=sql_insert("inv_receive_master",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;
			
			$grey_recv_num=$new_grey_recv_system_id[0];
			$grey_update_id=$id;
		}
		else
		{
			$field_array_update="receive_basis*receive_date*challan_no*booking_id*booking_no*booking_without_order*store_id*location_id*knitting_source*knitting_company*buyer_id*yarn_issue_challan_no*remarks*updated_by*update_date";
			
			$data_array_update=$cbo_receive_basis."*".$txt_receive_date."*".$txt_receive_chal_no."*".$txt_booking_no_id."*".$txt_booking_no."*".$booking_without_order."*".$cbo_store_name."*".$cbo_location_name."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$cbo_buyer_name."*".$txt_yarn_issue_challan_no."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
			if($rID) $flag=1; else $flag=0; 
			
			$grey_recv_num=str_replace("'","",$txt_recieved_id);
			$grey_update_id=str_replace("'","",$update_id);
		}
		
		$brand_id=return_id( $txt_brand, $brand_arr, "lib_brand", "id,brand_name");
		if($brand_id=="") $brand_id=0;
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		if($color_id=="") $color_id=0;
		
		if(str_replace("'","",$fabric_desc_id)=="") $fabric_desc_id=0;
		
		if(str_replace("'","",$cbo_receive_basis)==2 && str_replace("'","",$booking_without_order)==1 && str_replace("'","",$fabric_desc_id)==0)
		{
			$fabric_description=trim(str_replace("'","",$txt_fabric_description));
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=13 and detarmination_id=$fabric_desc_id and item_description='$fabric_description' and gsm=$txt_gsm and dia_width=$txt_width and status_active=1 and is_deleted=0");
		}
		else
		{
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=13 and detarmination_id=$fabric_desc_id and gsm=$txt_gsm and dia_width=$txt_width and status_active=1 and is_deleted=0");
		}
		
		if(count($row_prod)>0)
		{
			$prod_id=$row_prod[0][csf('id')];
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock_qnty=$row_prod[0][csf('current_stock')];
	
				$curr_stock_qnty=$stock_qnty+str_replace("'", '',$txt_receive_qnty);
				$avg_rate_per_unit=0;
				$stock_value=0;
	
				$field_array_prod_update="store_id*avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*brand*lot*updated_by*update_date";
				$data_array_prod_update=$cbo_store_name."*".$avg_rate_per_unit."*".$txt_receive_qnty."*".$curr_stock_qnty."*".$stock_value."*".$brand_id."*".$txt_yarn_lot."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				
				$rID2=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
				if($flag==1) 
				{
					if($rID2) $flag=1; else $flag=0; 
				} 
			}
		}
		else
		{
			$prod_id=return_next_id( "id", "product_details_master", 1 ) ;
			
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock_qnty=$txt_receive_qnty;
				$last_purchased_qnty=$txt_receive_qnty;
			}
			else
			{
				$stock_qnty=0;
				$last_purchased_qnty=0;
			}
			
			$avg_rate_per_unit=0;
			$stock_value=0;
			$prod_name_dtls=trim(str_replace("'","",$txt_fabric_description)).", ".trim(str_replace("'","",$txt_gsm)).", ".trim(str_replace("'","",$txt_width));
			$field_array_prod="id, company_id, store_id, item_category_id, detarmination_id, item_description, product_name_details, unit_of_measure, avg_rate_per_unit, last_purchased_qnty, current_stock, stock_value, brand, gsm, dia_width, lot, inserted_by, insert_date";
			
			$data_array_prod="(".$prod_id.",".$cbo_company_id.",".$cbo_store_name.",13,".$fabric_desc_id.",".$txt_fabric_description.",'".$prod_name_dtls."',".$cbo_uom.",".$avg_rate_per_unit.",".$last_purchased_qnty.",".$stock_qnty.",".$stock_value.",".$brand_id.",".$txt_gsm.",".$txt_width.",".$txt_yarn_lot.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into product_details_master (".$field_array_prod.") values ".$data_array_prod."chd".$txt_fabric_description;die;
			$rID2=sql_insert("product_details_master",$field_array_prod,$data_array_prod,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$id_trans=return_next_id( "id", "inv_transaction", 1 ) ;
			
			$order_rate=0; $order_amount=0; $cons_rate=0; $cons_amount=0;
			
			$field_array_trans="id, mst_id, receive_basis, pi_wo_batch_no, company_id, prod_id, item_category, transaction_type, transaction_date, store_id, brand_id, order_uom, order_qnty, order_rate, order_amount, cons_uom, cons_quantity, cons_reject_qnty, cons_rate, cons_amount, floor_id, machine_id, room, rack, self, bin_box, inserted_by, insert_date";
			
			$data_array_trans="(".$id_trans.",".$grey_update_id.",".$cbo_receive_basis.",".$txt_booking_no_id.",".$cbo_company_id.",".$prod_id.",13,1,".$txt_receive_date.",".$cbo_store_name.",".$brand_id.",".$cbo_uom.",".$txt_receive_qnty.",".$order_rate.",".$order_amount.",".$cbo_uom.",".$txt_receive_qnty.",".$txt_reject_fabric_recv_qnty.",".$cons_rate.",".$cons_amount.",".$cbo_floor_id.",".$cbo_machine_name.",".$txt_room.",".$txt_rack.",".$txt_self.",".$txt_binbox.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			$rID3=sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
		}
		else
		{
			$id_trans=0;
		}

		$id_dtls=return_next_id( "id", "pro_grey_prod_entry_dtls", 1 ) ;
		
		$cbo_yarn_count=explode(",",str_replace("'","",$cbo_yarn_count));
		asort($cbo_yarn_count);
		$cbo_yarn_count=implode(",",$cbo_yarn_count);
		
		$txt_yarn_lot=explode(",",str_replace("'","",$txt_yarn_lot));
		asort($txt_yarn_lot);
		$txt_yarn_lot=implode(",",$txt_yarn_lot);
		$rate=0; $amount=0;
		
		$field_array_dtls="id, mst_id, trans_id, prod_id, body_part_id, febric_description_id, gsm, width, no_of_roll, order_id, grey_receive_qnty, reject_fabric_receive, rate, amount, uom, yarn_lot, yarn_count, brand_id, shift_name, floor_id, machine_no_id, room, rack, self, bin_box, color_id, color_range_id, stitch_length, inserted_by, insert_date";
		
		$data_array_dtls="(".$id_dtls.",".$grey_update_id.",".$id_trans.",".$prod_id.",".$cbo_body_part.",".$fabric_desc_id.",".$txt_gsm.",".$txt_width.",".$txt_roll_no.",".$all_po_id.",".$txt_receive_qnty.",".$txt_reject_fabric_recv_qnty.",".$rate.",".$amount.",".$cbo_uom.",'".$txt_yarn_lot."','".$cbo_yarn_count."',".$brand_id.",".$txt_shift_name.",".$cbo_floor_id.",".$cbo_machine_name.",".$txt_room.",".$txt_rack.",".$txt_self.",".$txt_binbox.",".$color_id.",".$cbo_color_range.",".$txt_stitch_length.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
		$rID4=sql_insert("pro_grey_prod_entry_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1) 
		{
			if($rID4) $flag=1; else $flag=0; 
		} 
		
		$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, inserted_by, insert_date";
		
		$save_string=explode(",",str_replace("'","",$save_data));
		
		$po_array=array();
		
		for($i=0;$i<count($save_string);$i++)
		{
			$order_dtls=explode("**",$save_string[$i]);
			
			$order_id=$order_dtls[0];
			$order_qnty_roll_wise=$order_dtls[1];
			$roll_no=$order_dtls[2];
			
			if($i==0) $add_comma=""; else $add_comma=",";
			
			if( $id_roll=="" ) $id_roll = return_next_id( "id", "pro_roll_details", 1 ); else $id_roll = $id_roll+1;
			
			$data_array_roll.="$add_comma(".$id_roll.",".$grey_update_id.",".$id_dtls.",'".$order_id."',1,'".$order_qnty_roll_wise."','".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			if(array_key_exists($order_id,$po_array))
			{
				$po_array[$order_id]+=$order_qnty_roll_wise;
			}
			else
			{
				$po_array[$order_id]=$order_qnty_roll_wise;
			}
		}
		
		//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	$booking_without_order
		if($data_array_roll!="" && str_replace("'","",$roll_maintained)==1 && str_replace("'","",$booking_without_order)!=1)
		{
			$rID5=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			} 
		}
		
		$field_array_proportionate="id, trans_id, trans_type, entry_form, dtls_id, po_breakdown_id, prod_id, color_id, quantity, inserted_by, insert_date";
		$i=0;
		foreach($po_array as $key=>$val)
		{
			if($i==0) $add_comma=""; else $add_comma=",";
			
			if( $id_prop=="" ) $id_prop = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_prop = $id_prop+1;
			
			$order_id=$key;
			$order_qnty=$val;
			
			$data_array_prop.="$add_comma(".$id_prop.",".$id_trans.",1,2,".$id_dtls.",'".$order_id."','".$prod_id."','".$color_id."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			$i++;
		}
		
		//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;	
		if($data_array_prop!="" && str_replace("'","",$booking_without_order)!=1)
		{
			$rID6=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
			if($flag==1) 
			{
				if($rID6) $flag=1; else $flag=0; 
			} 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$grey_update_id."**".$grey_recv_num."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				echo "0**".$grey_update_id."**".$grey_recv_num."**0";
			}
			else
			{
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		check_table_status( $_SESSION['menu_id'],0);
				
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$flag=1;
		$field_array_update="receive_basis*receive_date*challan_no*booking_id*booking_no*booking_without_order*store_id*location_id*knitting_source*knitting_company*buyer_id*yarn_issue_challan_no*remarks*updated_by*update_date";
			
		$data_array_update=$cbo_receive_basis."*".$txt_receive_date."*".$txt_receive_chal_no."*".$txt_booking_no_id."*".$txt_booking_no."*".$booking_without_order."*".$cbo_store_name."*".$cbo_location_name."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$cbo_buyer_name."*".$txt_yarn_issue_challan_no."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,0);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} 
		
		if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$stock= return_field_value("current_stock","product_details_master","id=$previous_prod_id");
			$adjust_curr_stock=$stock-str_replace("'", '',$hidden_receive_qnty);
			$cur_st_value=0;
			$cur_st_rate=0;
			
			$field_array_adjust="current_stock*avg_rate_per_unit*stock_value";
				
			$data_array_adjust=$adjust_curr_stock."*".$cur_st_value."*".$cur_st_rate;
			
			$rID_adjust=sql_update("product_details_master",$field_array_adjust,$data_array_adjust,"id",$previous_prod_id,0);
			if($flag==1) 
			{
				if($rID_adjust) $flag=1; else $flag=0; 
			} 
		}
		
		$brand_id=return_id( $txt_brand, $brand_arr, "lib_brand", "id,brand_name");
		if($brand_id=="") $brand_id=0;
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		if($color_id=="") $color_id=0;
		
		if(str_replace("'","",$fabric_desc_id)=="") $fabric_desc_id=0;
		
		if(str_replace("'","",$cbo_receive_basis)==2 && str_replace("'","",$booking_without_order)==1 && str_replace("'","",$fabric_desc_id)==0)
		{
			$fabric_description=trim(str_replace("'","",$txt_fabric_description));
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=13 and detarmination_id=$fabric_desc_id and item_description='$fabric_description' and gsm=$txt_gsm and dia_width=$txt_width and status_active=1 and is_deleted=0");
		}
		else
		{
			$row_prod=sql_select("select id, current_stock from product_details_master where company_id=$cbo_company_id and item_category_id=13 and detarmination_id=$fabric_desc_id and gsm=$txt_gsm and dia_width=$txt_width and status_active=1 and is_deleted=0");
		}
		
		if(count($row_prod)>0)
		{
			$prod_id=$row_prod[0][csf('id')];
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock_qnty=$row_prod[0][csf('current_stock')];
	
				$curr_stock_qnty=$stock_qnty+str_replace("'", '',$txt_receive_qnty);
				$avg_rate_per_unit=0;
				$stock_value=0;
	
				$field_array_prod_update="store_id*avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*brand*lot*updated_by*update_date";
				
				$data_array_prod_update=$cbo_store_name."*".$avg_rate_per_unit."*".$txt_receive_qnty."*".$curr_stock_qnty."*".$stock_value."*".$brand_id."*".$txt_yarn_lot."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				
				$rID2=sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prod_id,0);
				if($flag==1) 
				{
					if($rID2) $flag=1; else $flag=0; 
				}
			}
		}
		else
		{
			$prod_id=return_next_id( "id", "product_details_master", 1 ) ;
			
			if(str_replace("'","",$fabric_store_auto_update)==1)
			{
				$stock_qnty=$txt_receive_qnty;
				$last_purchased_qnty=$txt_receive_qnty;
			}
			else
			{
				$stock_qnty=0;
				$last_purchased_qnty=0;
			}
			
			$avg_rate_per_unit=0;
			$stock_value=0;
			
			
			$prod_name_dtls=trim(str_replace("'","",$txt_fabric_description)).", ".trim(str_replace("'","",$txt_gsm)).", ".trim(str_replace("'","",$txt_width));
			$field_array_prod="id, company_id, store_id, item_category_id, detarmination_id, item_description, product_name_details, unit_of_measure, avg_rate_per_unit, last_purchased_qnty, current_stock, stock_value, brand, gsm, dia_width, lot, inserted_by, insert_date";
			
			$data_array_prod="(".$prod_id.",".$cbo_company_id.",".$cbo_store_name.",13,".$fabric_desc_id.",".$txt_fabric_description.",'".$prod_name_dtls."',".$cbo_uom.",".$avg_rate_per_unit.",".$last_purchased_qnty.",".$stock_qnty.",".$stock_value.",".$brand_id.",".$txt_gsm.",".$txt_width.",".$txt_yarn_lot.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			$rID2=sql_insert("product_details_master",$field_array_prod,$data_array_prod,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		if(str_replace("'","",$fabric_store_auto_update)==1)
		{
			$order_rate=0; $order_amount=0; $cons_rate=0; $cons_amount=0;
			
			$field_array_trans_update="receive_basis*pi_wo_batch_no*prod_id*transaction_date*store_id*brand_id*order_qnty*order_rate*order_amount*cons_quantity*cons_reject_qnty*cons_rate*cons_amount*floor_id*machine_id*room*rack*self*bin_box*updated_by*update_date";
			
			$data_array_trans_update=$cbo_receive_basis."*".$txt_booking_no_id."*".$prod_id."*".$txt_receive_date."*".$cbo_store_name."*".$brand_id."*".$txt_receive_qnty."*".$order_rate."*".$order_amount."*".$txt_receive_qnty."*".$txt_reject_fabric_recv_qnty."*".$cons_rate."*".$cons_amount."*".$cbo_floor_id."*".$cbo_machine_name."*".$txt_room."*".$txt_rack."*".$txt_self."*".$txt_binbox."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			$rID4=sql_update("inv_transaction",$field_array_trans_update,$data_array_trans_update,"id",$update_trans_id,0);
			if($flag==1) 
			{
				if($rID4) $flag=1; else $flag=0; 
			} 
		}
		
		$cbo_yarn_count=explode(",",str_replace("'","",$cbo_yarn_count));
		asort($cbo_yarn_count);
		$cbo_yarn_count=implode(",",$cbo_yarn_count);
		
		$txt_yarn_lot=explode(",",str_replace("'","",$txt_yarn_lot));
		asort($txt_yarn_lot);
		$txt_yarn_lot=implode(",",$txt_yarn_lot);
		$rate=0; $amount=0;
		
		$field_array_dtls_update="prod_id*body_part_id*febric_description_id*gsm*width*no_of_roll*order_id*grey_receive_qnty*reject_fabric_receive*rate*amount*uom*yarn_lot*yarn_count*brand_id*shift_name*floor_id*machine_no_id*room*rack*self*bin_box*color_id*color_range_id*stitch_length*updated_by*update_date";
		
		$data_array_dtls_update=$prod_id."*".$cbo_body_part."*".$fabric_desc_id."*".$txt_gsm."*".$txt_width."*".$txt_roll_no."*".$all_po_id."*".$txt_receive_qnty."*".$txt_reject_fabric_recv_qnty."*".$rate."*".$amount."*".$cbo_uom."*'".$txt_yarn_lot."'*'".$cbo_yarn_count."'*".$brand_id."*".$txt_shift_name."*".$cbo_floor_id."*".$cbo_machine_name."*".$txt_room."*".$txt_rack."*".$txt_self."*".$txt_binbox."*".$color_id."*".$cbo_color_range."*".$txt_stitch_length."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";

		$rID5=sql_update("pro_grey_prod_entry_dtls",$field_array_dtls_update,$data_array_dtls_update,"id",$update_dtls_id,0);
		if($flag==1) 
		{
			if($rID5) $flag=1; else $flag=0; 
		} 
		
		$field_array_roll="id, mst_id, dtls_id, po_breakdown_id, entry_form, qnty, roll_no, inserted_by, insert_date";
		
		$save_string=explode(",",str_replace("'","",$save_data));
		
		$po_array=array();
		$not_delete_roll_table_id=''; $j=0;
		
		for($i=0;$i<count($save_string);$i++)
		{
			$order_dtls=explode("**",$save_string[$i]);
			
			$order_id=$order_dtls[0];
			$order_qnty_roll_wise=$order_dtls[1];
			$roll_no=$order_dtls[2];
			$roll_not_delete_id=$order_dtls[3];
			$roll_id=$order_dtls[4];

			if($roll_not_delete_id==0)
			{
				if($j==0) $add_comma=""; else $add_comma=",";
				
				if( $id_roll=="" ) $id_roll = return_next_id( "id", "pro_roll_details", 1 ); else $id_roll = $id_roll+1;
				
				$data_array_roll.="$add_comma(".$id_roll.",".$update_id.",".$update_dtls_id.",'".$order_id."',1,'".$order_qnty_roll_wise."','".$roll_no."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$j++;
			}
			else
			{
				if($not_delete_roll_table_id=="") $not_delete_roll_table_id=$roll_id; else $not_delete_roll_table_id.=",".$roll_id;
			}
			
			if(array_key_exists($order_id,$po_array))
			{
				$po_array[$order_id]+=$order_qnty_roll_wise;
			}
			else
			{
				$po_array[$order_id]=$order_qnty_roll_wise;
			}
		}
		
		if(str_replace("'","",$roll_maintained)==1)
		{
			if($not_delete_roll_table_id=="") $delete_cond=""; else $delete_cond="and id not in($not_delete_roll_table_id)"; 
	
			$delete_roll=execute_query( "delete from pro_roll_details where dtls_id=$update_dtls_id and entry_form=1 $delete_cond",0);
			if($flag==1) 
			{
				if($delete_roll) $flag=1; else $flag=0; 
			} 
			
			//echo $flag;
			//echo "insert into pro_roll_details (".$field_array_roll.") values ".$data_array_roll;die;	
			if($data_array_roll!="" && str_replace("'","",$booking_without_order)!=1)
			{
				$rID6=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,0);
				if($flag==1) 
				{
					if($rID6) $flag=1; else $flag=0; 
				} 
			}
		}
		
		$delete_prop=execute_query( "delete from order_wise_pro_details where dtls_id=$update_dtls_id and trans_id=$update_trans_id and entry_form=2",0);
		if($flag==1) 
		{
			if($delete_prop) $flag=1; else $flag=0; 
		}
		
		$field_array_proportionate="id, trans_id, trans_type, entry_form, dtls_id, po_breakdown_id, prod_id, color_id, quantity, inserted_by, insert_date";

		$i=0;
		foreach($po_array as $key=>$val)
		{
			if($i==0) $add_comma=""; else $add_comma=",";
			
			if( $id_prop=="" ) $id_prop = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_prop = $id_prop+1;
			
			$order_id=$key;
			$order_qnty=$val;
			
			$data_array_prop.="$add_comma(".$id_prop.",".$update_trans_id.",1,2,".$update_dtls_id.",'".$order_id."','".$prod_id."','".$color_id."','".$order_qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			$i++;
		}
		
		//echo "insert into order_wise_pro_details (".$field_array_proportionate.") values ".$data_array_prop;die;	
		if($data_array_prop!="" && str_replace("'","",$booking_without_order)!=1)
		{
			$rID7=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,0);
			if($flag==1) 
			{
				if($rID7) $flag=1; else $flag=0; 
			} 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'", '', $update_id)."**".str_replace("'", '', $txt_recieved_id)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**0**1";
			}
		}

		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				echo "1**".str_replace("'", '', $update_id)."**".str_replace("'", '', $txt_recieved_id)."**0";
			}
			else
			{
				echo "6**0**0**1";
			}
		}
		disconnect($con);
		die;
	}
	
}


if ($action=="grey_receive_popup_search")
{
	echo load_html_head_contents("Grey Receive Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?> 

	<script>
	
		function js_set_value(id)
		{
			$('#hidden_recv_id').val(id);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:880px;">
	<form name="searchwofrm"  id="searchwofrm">
		<fieldset style="width:875px; margin-left:5px">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="820" class="rpt_table">
                <thead>
                    <th>Buyer</th>
                    <th>Received Date Range</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="200">Enter Receive No</th>
                    <th>
                    	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                    	<input type="hidden" name="hidden_recv_id" id="hidden_recv_id" class="text_boxes" value="">  
                    </th> 
                </thead>
                <tr>
                    <td align="center">
                    	<?php
							echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",$data[0] ); 
						?>       
                    </td>
                    <td align="center">
                    	<input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
					  	<input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
					</td>
                    <td align="center">	
                    	<?php
                       		$search_by_arr=array(0=>"Booking No",1=>"Received ID",2=>"Challan No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"", 0, "--Select--", 1,$dd,0 );
						?>
                    </td>     
                    <td align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 						
            		<td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value+'_'+document.getElementById('cbo_buyer_name').value, 'create_grey_recv_search_list_view', 'search_div', 'grey_production_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                     </td>
                </tr>
                <tr>
                	<td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
           </table>
           <div style="width:100%; margin-top:15px; margin-left:3px" id="search_div" align="left"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_grey_recv_search_list_view")
{
	
	$data = explode("_",$data);
	
	$search_string="%".trim($data[0])."%";
	$search_by=$data[1];
	$start_date =$data[2];
	$end_date =$data[3];
	$company_id =$data[4];
	$buyer_id =$data[5];

	if($buyer_id==0) echo $buyer_name=""; else $buyer_name=" and buyer_id=$buyer_id ";//{ echo "Please Select Buyer First."; die; }
	
	if($start_date!="" && $end_date!="")
	{
		$date_cond="and receive_date between '".change_date_format(trim($start_date), "yyyy-mm-dd", "-")."' and '".change_date_format(trim($end_date), "yyyy-mm-dd", "-")."'";
	}
	else
	{
		$date_cond="";
	}
	
	if(trim($data[0])!="")
	{
		if($search_by==0)
			$search_field_cond="and booking_no like '$search_string'";
		else if($search_by==1)	
			$search_field_cond="and recv_number like '$search_string'";
		else	
			$search_field_cond="and challan_no like '$search_string'";
	}
	else
	{
		$search_field_cond="";
	}
	
	$sql = "select id, recv_number_prefix_num, recv_number, booking_no, buyer_id, knitting_source, knitting_company, receive_date, challan_no from inv_receive_master where entry_form=2 and status_active=1 and is_deleted=0 and company_id=$company_id $buyer_name $search_field_cond $date_cond"; 
	//echo $sql;die;
	$result = sql_select($sql);

	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$supllier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="850" class="rpt_table">
        <thead>
            <th width="40">SL</th>
            <th width="115">Received ID</th>
            <th width="115">Booking No/ Knit Id</th>               
            <th width="100">Knitting Source</th>
            <th width="110">Knitting Company</th>
            <th width="80">Receive date</th>
            <th width="90">Receive Qnty</th>
            <th width="90">Challan No</th>
            <th>Buyer</th>
        </thead>
	</table>
	<div style="width:870px; max-height:240px; overflow-y:scroll" id="list_container_batch" align="left">	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="850" class="rpt_table" id="tbl_list_search">  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	 
                if($row[csf('knitting_source')]==1)
					$knit_comp=$company_arr[$row[csf('knitting_company')]]; 
				else
					$knit_comp=$supllier_arr[$row[csf('knitting_company')]];
				
				$recv_qnty=return_field_value("sum(grey_receive_qnty)","pro_grey_prod_entry_dtls","mst_id='$row[id]' and status_active=1 and is_deleted=0");
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>);"> 
                    <td width="40"><?php echo $i; ?></td>
                    <td width="115"><p><?php echo $row[csf('recv_number_prefix_num')]; ?></p></td>
                    <td width="115"><p><?php echo $row[csf('booking_no')]; ?></p></td>               
                    <td width="100"><p><?php echo $knitting_source[$row[csf('knitting_source')]]; ?></p></td>
                    <td width="110"><p><?php echo $knit_comp; ?></p></td>
                    <td width="80" align="center"><?php echo change_date_format($row[csf('receive_date')]); ?></td>
                    <td width="90" align="right"><?php echo number_format($recv_qnty,2); ?></td>
                    <td width="90"><p><?php echo $row[csf('challan_no')]; ?></p></td>
                    <td><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
    </div>
<?php	
exit();
}

if($action=='populate_data_from_grey_recv')
{
	
	$data_array=sql_select("select id, recv_number, company_id, receive_basis, booking_id, booking_no, booking_without_order, buyer_id, store_id, location_id, knitting_source, knitting_company, receive_date, challan_no, yarn_issue_challan_no, remarks from inv_receive_master where id='$data'");
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('txt_recieved_id').value 				= '".$row[csf("recv_number")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "document.getElementById('cbo_receive_basis').value 			= '".$row[csf("receive_basis")]."';\n";

		echo "$('#cbo_company_id').attr('disabled','true')".";\n";
		echo "set_receive_basis();\n";
		
		echo "document.getElementById('txt_receive_date').value 			= '".change_date_format($row[csf("receive_date")])."';\n";
		echo "document.getElementById('txt_receive_chal_no').value 			= '".$row[csf("challan_no")]."';\n";
		echo "document.getElementById('txt_booking_no').value 				= '".$row[csf("booking_no")]."';\n";
		echo "document.getElementById('txt_booking_no_id').value 			= '".$row[csf("booking_id")]."';\n";
		echo "document.getElementById('booking_without_order').value 		= '".$row[csf("booking_without_order")]."';\n";
		
		if($row[csf("booking_without_order")]==1)
		{
			echo "$('#txt_receive_qnty').removeAttr('readonly','readonly');\n";
			echo "$('#txt_receive_qnty').removeAttr('onClick','onClick');\n";	
			echo "$('#txt_receive_qnty').removeAttr('placeholder','placeholder');\n";		
		}
		else
		{
			echo "$('#txt_receive_qnty').attr('readonly','readonly');\n";
			echo "$('#txt_receive_qnty').attr('onClick','openmypage_po();');\n";	
			echo "$('#txt_receive_qnty').attr('placeholder','Single Click');\n";	
		}
		
		echo "document.getElementById('cbo_buyer_name').value 				= '".$row[csf("buyer_id")]."';\n";
		echo "document.getElementById('cbo_store_name').value 				= '".$row[csf("store_id")]."';\n";
		echo "document.getElementById('cbo_knitting_source').value 			= '".$row[csf("knitting_source")]."';\n";
		
		echo "load_drop_down( 'requires/grey_production_entry_controller', $row[knitting_source]+'_'+$row[company_id], 'load_drop_down_knitting_com','knitting_com');\n";
		
		if($row[csf("receive_basis")]==1)
		{
			$job_no=return_field_value("job_no","wo_booking_mst","id='$row[booking_id]'");
			echo "set_auto_complete(2);\n";
		}
		else if($row[csf("receive_basis")]==2)
		{
			$job_no=return_field_value("a.job_no_mst as job_no","wo_po_break_down a, ppl_planning_entry_plan_dtls b","a.id=b.po_id and b.dtls_id='$row[booking_id]' group by a.job_no_mst","job_no");
			echo "set_auto_complete(2);\n";
		}
		else
		{
			echo "set_auto_complete(1);\n";
			$job_no='';
		}
		
		echo "document.getElementById('cbo_knitting_company').value 		= '".$row[csf("knitting_company")]."';\n";
		
		echo "load_drop_down( 'requires/grey_production_entry_controller',document.getElementById('cbo_company_id').value+'_'+".$row[csf("location_id")].", 'load_drop_down_floor', 'floor_td' );\n";

		echo "document.getElementById('cbo_location_name').value 			= '".$row[csf("location_id")]."';\n";
		echo "document.getElementById('txt_yarn_issue_challan_no').value 	= '".$row[csf("yarn_issue_challan_no")]."';\n";
		echo "document.getElementById('txt_job_no').value 					= '".$job_no."';\n";
		echo "document.getElementById('txt_remarks').value 					= '".$row[csf("remarks")]."';\n";
		echo "document.getElementById('update_id').value 					= '".$row[csf("id")]."';\n";
		
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_grey_production_entry',1);\n";  
		exit();
	}
}

if($action=="show_grey_prod_listview")
{
	$fabric_desc_arr=return_library_array("select id, item_description from product_details_master where item_category_id=13","id","item_description");
	$composition_arr=array();
 	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}

	$sql="select id, prod_id, body_part_id, febric_description_id, gsm, width, grey_receive_qnty, reject_fabric_receive, uom, yarn_lot, no_of_roll, brand_id, shift_name, machine_no_id from pro_grey_prod_entry_dtls where mst_id='$data' and status_active = '1' and is_deleted = '0'";
	$result=sql_select($sql);
	
	//$arr=array(0=>$body_part,1=>$composition_arr,6=>$unit_of_measurement,9=>$brand_arr,10=>$shift_name,11=>$machine_arr);
	//echo create_list_view("list_view", "Body Part,Fabric Description,GSM,Dia / Width,Grey Rec. Qnty, Reject Feb. Qty, uom, Yarn Lot,No of Roll,Brand,Shift Name, Machine No", "80,120,60,60,80,80,60,60,60,80,80,60","930","200",0, $sql, "put_data_dtls_part", "id", "'populate_grey_details_form_data'", 0, "body_part_id,febric_description_id,0,0,0,0,uom,0,0,brand_id,shift_name,machine_no_id", $arr, "body_part_id,febric_description_id,gsm,width,grey_receive_qnty,reject_fabric_receive,uom,yarn_lot,no_of_roll,brand_id,shift_name,machine_no_id", "requires/grey_production_entry_controller",'','0,0,0,0,2,1,0,0,0,0,0,0');
?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="930" class="rpt_table">
        <thead>
            <th width="80">Body Part</th>
            <th width="120">Fabric Description</th>
            <th width="60">GSM</th>
            <th width="60">Dia / Width</th>
            <th width="80">Grey Recv. Qnty</th>
            <th width="80">Reject Feb. Qty</th>
            <th width="50">uom</th>
            <th width="80">Yarn Lot</th>
            <th width="60">No of Roll</th>
            <th width="80">Brand</th>
            <th width="80">Shift Name</th>
            <th>Machine No</th>
        </thead>
	</table>
	<div style="width:930px; max-height:200px; overflow-y:scroll" id="list_container_batch" align="left">	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="910" class="rpt_table" id="list_view">  
        <?php
            $i=1;
            foreach($result as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	 
					
                if($row[csf('febric_description_id')]==0)
					$fabric_desc=$fabric_desc_arr[$row[csf('prod_id')]]; 
				else
					$fabric_desc=$composition_arr[$row[csf('febric_description_id')]];
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="put_data_dtls_part(<?php echo $row[csf('id')]; ?>,'populate_grey_details_form_data', 'requires/grey_production_entry_controller');"> 
                    <td width="80"><p><?php echo $body_part[$row[csf('body_part_id')]]; ?></p></td>
                    <td width="120"><p><?php echo $fabric_desc; ?></p></td>
                    <td width="60"><p><?php echo $row[csf('gsm')]; ?>&nbsp;</p></td>
                    <td width="60"><p><?php echo $row[csf('width')]; ?>&nbsp;</p></td>
                    <td width="80" align="right"><?php echo number_format($row[csf('grey_receive_qnty')],2); ?></td>
                    <td width="80" align="right"><?php echo number_format($row[csf('reject_fabric_receive')],2); ?>&nbsp;</td>
                    <td width="50"><p><?php echo $unit_of_measurement[$row[csf('uom')]]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $row[csf('yarn_lot')]; ?>&nbsp;</p></td>
                    <td width="60"><p><?php echo $row[csf('no_of_roll')]; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $brand_arr[$row[csf('brand_id')]]; ?></p></td>
                    <td width="80"><p><?php echo $shift_name[$row[csf('shift_name')]]; ?></p></td>
                    <td><p><?php echo $machine_arr[$row[csf('machine_no_id')]]; ?>&nbsp;</p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
    </div>
<?php	
	exit();
}


if($action=='populate_grey_details_form_data')
{
	$data=explode("**",$data);
	$id=$data[0];
	$roll_maintained=$data[1];
	
	$data_array=sql_select("select id, body_part_id, trans_id,	prod_id, febric_description_id, no_of_roll, gsm, width, grey_receive_qnty, reject_fabric_receive, uom, yarn_lot, yarn_count, brand_id, shift_name, floor_id, machine_no_id, order_id, room, rack, self, bin_box, color_id, color_range_id, stitch_length from pro_grey_prod_entry_dtls where id='$id'");
	foreach ($data_array as $row)
	{ 
		$comp='';
		if($row[csf('febric_description_id')]==0)
		{
			$comp=return_field_value("item_description","product_details_master","id=".$row[csf('prod_id')]);
		}
		else
		{
			$determination_sql=sql_select("select a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id and a.id=$row[febric_description_id]");
					
			if($determination_sql[0][csf('construction')]!="")
			{
				$comp=$determination_sql[0][csf('construction')].", ";
			}
			
			foreach( $determination_sql as $d_row )
			{
				$comp.=$composition[$d_row[csf('copmposition_id')]]." ".$d_row[csf('percent')]."% ";
			}
		}
		
		echo "document.getElementById('cbo_body_part').value 				= '".$row[csf("body_part_id")]."';\n";
		echo "document.getElementById('txt_fabric_description').value 		= '".$comp."';\n";
		echo "document.getElementById('fabric_desc_id').value 				= '".$row[csf("febric_description_id")]."';\n";
		echo "document.getElementById('txt_gsm').value 						= '".$row[csf("gsm")]."';\n";
		echo "document.getElementById('txt_width').value 					= '".$row[csf("width")]."';\n";
		echo "document.getElementById('txt_roll_no').value 					= '".$row[csf("no_of_roll")]."';\n";
		echo "document.getElementById('txt_receive_qnty').value 			= '".$row[csf("grey_receive_qnty")]."';\n";
		echo "document.getElementById('txt_reject_fabric_recv_qnty').value 	= '".$row[csf("reject_fabric_receive")]."';\n";
		echo "document.getElementById('txt_color').value 					= '".$color_arr[$row[csf("color_id")]]."';\n";  
		echo "document.getElementById('cbo_color_range').value 				= '".$row[csf("color_range_id")]."';\n";
		echo "document.getElementById('txt_stitch_length').value 			= '".$row[csf("stitch_length")]."';\n";
		echo "document.getElementById('cbo_uom').value 						= '".$row[csf("uom")]."';\n";
		echo "document.getElementById('txt_yarn_lot').value 				= '".$row[csf("yarn_lot")]."';\n";
		echo "document.getElementById('cbo_yarn_count').value 				= '".$row[csf("yarn_count")]."';\n";
		echo "set_multiselect('cbo_yarn_count','0','1','".$row[csf('yarn_count')]."','0');\n";
		echo "document.getElementById('txt_brand').value 					= '".$brand_arr[$row[csf("brand_id")]]."';\n";
		echo "document.getElementById('txt_shift_name').value 				= '".$row[csf("shift_name")]."';\n";
		echo "document.getElementById('cbo_floor_id').value 				= '".$row[csf("floor_id")]."';\n";
		
		echo "load_drop_down( 'requires/grey_production_entry_controller',document.getElementById('cbo_company_id').value+'_'+".$row[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
		
		echo "document.getElementById('cbo_machine_name').value 			= '".$row[csf("machine_no_id")]."';\n";
		echo "document.getElementById('txt_room').value 					= '".$row[csf("room")]."';\n";
		echo "document.getElementById('txt_rack').value 					= '".$row[csf("rack")]."';\n";
		echo "document.getElementById('txt_self').value 					= '".$row[csf("self")]."';\n";
		echo "document.getElementById('txt_binbox').value 					= '".$row[csf("bin_box")]."';\n";
		echo "document.getElementById('hidden_receive_qnty').value 			= '".$row[csf("grey_receive_qnty")]."';\n";
		echo "document.getElementById('all_po_id').value 					= '".$row[csf("order_id")]."';\n";
		echo "document.getElementById('previous_prod_id').value 			= '".$row[csf("prod_id")]."';\n";
		echo "document.getElementById('update_dtls_id').value 				= '".$row[csf("id")]."';\n";
		echo "document.getElementById('update_trans_id').value 				= '".$row[csf("trans_id")]."';\n";
		
		$save_string='';
		if($roll_maintained==1)
		{
			$data_roll_array=sql_select("select id, roll_used, po_breakdown_id, qnty, roll_no from pro_roll_details where dtls_id='$id' and entry_form=1 and status_active=1 and is_deleted=0");
			foreach($data_roll_array as $row_roll)
			{ 
				if($row_roll[csf('roll_used')]==1) $roll_id=$row_roll[csf('id')]; else $roll_id=0;
				//$roll_id=$row_roll[csf('id')];
				
				if($save_string=="")
				{
					$save_string=$row_roll[csf("po_breakdown_id")]."**".$row_roll[csf("qnty")]."**".$row_roll[csf("roll_no")]."**".$roll_id."**".$row_roll[csf("id")];
				}
				else
				{
					$save_string.=",".$row_roll[csf("po_breakdown_id")]."**".$row_roll[csf("qnty")]."**".$row_roll[csf("roll_no")]."**".$roll_id."**".$row_roll[csf("id")];
				}
			}
		}
		else
		{
			$data_po_array=sql_select("select po_breakdown_id, quantity from order_wise_pro_details where dtls_id='$id' and entry_form=2 and status_active=1 and is_deleted=0");
			foreach($data_po_array as $row_po)
			{ 
				if($save_string=="")
				{
					$save_string=$row_po[csf("po_breakdown_id")]."**".$row_po[csf("quantity")];
				}
				else
				{
					$save_string.=",".$row_po[csf("po_breakdown_id")]."**".$row_po[csf("quantity")];
				}
			}
		}
		
		echo "document.getElementById('save_data').value 				= '".$save_string."';\n";
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_grey_production_entry',1);\n";  
		exit();
	}
}

if($action=="roll_duplication_check")
{
	$data=explode("**",$data);
	$po_id=$data[0];
	$roll_no=trim($data[1]);
	$roll_id=$data[2];
	
	if($roll_id=="" || $roll_id=="0")
	{
		$sql="select a.recv_number from inv_receive_master a, pro_roll_details b where a.id=b.mst_id and b.po_breakdown_id='$po_id' and b.roll_no='$roll_no' and a.is_deleted=0 and a.status_active=1 and b.entry_form=1 and b.is_deleted=0 and b.status_active=1";
	}
	else
	{
		$sql="select a.recv_number from inv_receive_master a, pro_roll_details b where a.id=b.mst_id and b.po_breakdown_id='$po_id' and b.roll_no='$roll_no' and a.is_deleted=0 and a.status_active=1 and b.entry_form=1 and b.id<>$roll_id and b.is_deleted=0 and b.status_active=1";
	}
	//echo $sql;
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('recv_number')];
	}
	else
	{
		echo "0_";
	}
	
	exit();	
}

if($action=="roll_maintained")
{
	$roll_maintained=return_field_value("fabric_roll_level","variable_settings_production","company_name ='$data' and variable_list=3 and is_deleted=0 and status_active=1");
	if($roll_maintained==1) $roll_maintained=$roll_maintained; else $roll_maintained=0;
	
	$fabric_store_auto_update=return_field_value("auto_update","variable_settings_production","company_name ='$data' and variable_list=15 and item_category_id=13 and is_deleted=0 and status_active=1");
	if($fabric_store_auto_update==2) $fabric_store_auto_update=0; else $fabric_store_auto_update=1;
	
	echo "document.getElementById('roll_maintained').value 					= '".$roll_maintained."';\n";
	echo "document.getElementById('fabric_store_auto_update').value 		= '".$fabric_store_auto_update."';\n";
	
	echo "reset_form('greyproductionentry_1','list_fabric_desc_container','','','set_receive_basis();','update_id*txt_recieved_id*cbo_company_id*cbo_receive_basis*txt_receive_date*txt_receive_chal_no*cbo_store_name*cbo_knitting_source*cbo_knitting_company*txt_remarks*roll_maintained*fabric_store_auto_update*txt_yarn_issue_challan_no*txt_shift_name*txt_width*txt_gsm*cbo_floor_id*cbo_machine_name*txt_room*txt_rack*txt_reject_fabric_recv_qnty*txt_self*cbo_uom*txt_binbox*txt_yarn_lot*cbo_yarn_count*txt_brand*txt_stitch_length*txt_color*cbo_color_range');\n";
	
	exit();	
}

if($action=="load_color")
{
	$data=explode("**",$data);
	$booking_id=$data[0];
	$is_sample=$data[1];
	$receive_basis=$data[2];
	
	if($receive_basis==1)
	{
		if($is_sample==0)
		{
			$sql="select c.color_name from wo_booking_mst a, wo_booking_dtls b, lib_color c where a.booking_no=b.booking_no and b.fabric_color_id=c.id and a.id=$booking_id and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.fabric_color_id";
		}
		else
		{
			$sql="select c.color_name from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b, lib_color c where a.booking_no=b.booking_no and b.fabric_color=c.id and a.id=$booking_id and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.fabric_color";
		}
	}
	else
	{
		$sql="select c.color_name from wo_booking_mst a, wo_booking_dtls b, lib_color c, ppl_planning_info_entry_mst d, ppl_planning_info_entry_dtls e where d.id=e.mst_id and  a.booking_no=b.booking_no and a.booking_no=d.booking_no and b.fabric_color_id=c.id and e.id=$booking_id and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.fabric_color_id";
	}
	//echo $sql;die;
	echo "var str_color = [". substr(return_library_autocomplete( $sql, "color_name" ), 0, -1). "];\n";
	echo "$('#txt_color').autocomplete({
						 source: str_color
					  });\n";
	exit();	
}
?>
