<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------
	$location_library=return_library_array( "select id,location_name from lib_location", "id", "location_name");
	$buyer_library=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name");
	$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name");
	$batch_library=return_library_array( "select id,batch_no from pro_batch_create_mst", "id", "batch_no");
	$batch_color_library=return_library_array( "select id,color_id from pro_batch_create_mst", "id", "color_id");
	$gsm_library=return_library_array( "select id,gsm from  product_details_master", "id", "gsm");
	$machine_library=return_library_array( "select id,machine_no from lib_machine_name", "id", "machine_no");
	$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$order_arr=return_library_array( "select id, po_number from wo_po_break_down", "id", "po_number"  );
	$order_qty_arr=return_library_array( "select id, po_quantity from wo_po_break_down", "id", "po_quantity"  );
if ($action=="load_drop_down_unit")
{
	echo create_drop_down( "cbo_unit_id", 150, "Select id, location_name from   lib_location where company_id=$data and status_active=1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select --", $selected, "",0 );     	 
}
if ($action=="order_wise_search")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print ($data[1]);
	?>
	 <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function check_all_data()
		{
			var row_num=$('#list_view tr').length-1;
			for(var i=1;  i<=row_num;  i++)
			{
				$("#tr_"+i).click();
			}
		}
	function js_set_value(id)
	{ //alert(id);
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
	</script>
      <input type="hidden" id="txt_po_id" />
     <input type="hidden" id="txt_po_val" />
 <?php
	if ($data[0]==0) $company_id=""; else $company_id=" and company_name=$data[0]";
	if ($data[1]=="") $job_no_cond="";  
    else
	 { 
	    if($db_type==0) $job_no_cond="  and FIND_IN_SET(b.job_no_prefix_num,'$data[1]')";
		if($db_type==2) $job_no_cond="  and b.job_no_prefix_num in($data[1])";
	 }
	if($db_type==0) $year_field="YEAR(b.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(b.insert_date,'YYYY') as year";
	$sql ="select distinct a.id,a.po_number,b.job_no_prefix_num as job_prefix,$year_field from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company_name  $job_no_cond "; 
	echo create_list_view("list_view", "Order Number,Job No, Year","150,100,50","450","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "po_number,job_prefix,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	exit();
}
if($action=="jobnumbershow")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
<script type="text/javascript">
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#hide_job_id').val( id );
		$('#hide_job_no').val( ddd );
	}		
</script>
<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
<input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
<?php
	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	//$year="year(insert_date)";
	if($db_type==0) $year_field_by="year(insert_date) as year "; 
	else if($db_type==2) $year_field_by="to_char(insert_date,'YYYY') as year ";
	if($db_type==0) $month_field_by="and month(insert_date)"; 
	else if($db_type==2) $month_field_by="and to_char(insert_date,'MM')";
	if($db_type==0) $year_field="and year(insert_date)=$year_id"; 
	else if($db_type==2) $year_field="and to_char(insert_date,'YYYY')";
	$year_id=str_replace("'","",$cbo_year_id);
	if($db_type==2) $yearcond=" and to_char(insert_date,'YYYY')=$year_id";
	else if($db_type==0) $yearcond=" and SUBSTRING_INDEX(`insert_date`, '-', 1)=$year_id";
	//echo $year_cond;
	if($cbo_unit_id!=0) $unit_id_cond="and location_name=$cbo_unit_id"; else $unit_id_cond="";
	if($year_id!=0) $year_cond="$year_field"; else $year_cond="";
	$arr=array (0=>$company_arr,1=>$buyer_arr);
	$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, $year_field_by, style_ref_no from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id  $unit_id_cond $yearcond order by job_no";
	echo create_list_view("list_view", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","620","270",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "","setFilterGrid('list_view',-1)","0","",1) ;
   exit(); 
}
if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_unit=str_replace("'","",$cbo_unit_id);
	$txt_job_id=str_replace("'","",$txt_job_id);
	$txt_job_no=str_replace("'","",$txt_job_no);
	$txt_order_id=str_replace("'","",$txt_order_id);
	$txt_order_no=str_replace("'","",$txt_order_no);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	if($db_type==0) 
	{
		$date_from=change_date_format($date_from,'yyyy-mm-dd');
		$date_to=change_date_format($date_to,'yyyy-mm-dd');
	}
	else if($db_type==2) 
	{
		$date_from=change_date_format($date_from,'','',1);
		$date_to=change_date_format($date_to,'','',1);
	}
	else  
	{
		$date_from=""; $date_to="";
	}
	if($date_from!=="" and $date_to!=="")
	{
	$date_cond=" and a.receive_date between '$date_from' and '$date_to'";	
	}
	if($cbo_unit==0) $unit_name=""; else $unit_name="and a.location_id=$cbo_unit";
	if ($txt_job_no=="") $job_no_cond=""; else $job_no_cond=" and d.job_no_prefix_num in ($txt_job_no) ";
	if(str_replace("'","",$txt_order_no_id)!="") $order_cond=" and e.id in(".$txt_order_id.")";
	else if(str_replace("'","",$txt_order_no)!="") $order_cond=" and e.po_number in('".$txt_order_no."')"; 
	else $order_cond="";
	$job_no_arr=array();
	$sql_po=sql_select("select b.id, b.job_no_mst,b.po_number,b.po_quantity,a.total_set_qnty,a.style_ref_no,a.buyer_name from wo_po_details_master a, wo_po_break_down b where b.job_no_mst=a.job_no and  a.status_active=1 and a.is_deleted=0");
	foreach($sql_po as $row)
	{
	$po_qty=$row[csf('po_quantity')]*$row[csf('total_set_qnty')];
	$job_no_arr[$row[csf('id')]]['job_no_mst']=$row[csf('job_no_mst')];
	$job_no_arr[$row[csf('id')]]['po_quantity']=$po_qty;
	$job_no_arr[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
	$job_no_arr[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')];
	} //var_dump($job_no_arr);die;
	$batch_no_arr=array();
	$sql_batch=sql_select("select a.id, b.batch_qnty,a.batch_no,a.booking_no from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and  a.status_active=1 and a.is_deleted=0 ");
	foreach($sql_batch as $row)
	{
	$batch_no_arr[$row[csf('id')]]['batch_qnty']=$row[csf('batch_qnty')];
	$batch_no_arr[$row[csf('id')]]['batch_no']=$row[csf('batch_no')];
	$batch_no_arr[$row[csf('id')]]['booking_no']=$row[csf('booking_no')];
	} //var_dump($batch_no_arr);die;
	ob_start();
?>	
  <div id="scroll_body" align="center" style="height:auto; width:auto; margin:0 auto; padding:0;">
        <table width="1600px" cellpadding="0" cellspacing="0" id="caption" align="center">
            <tr>
               <td align="center" width="100%" colspan="18" class="form_caption" ><strong style="font-size:18px"><?php echo $company_arr[$cbo_company];?></strong></td>
            </tr> 
            <tr>  
               <td align="center" width="100%" colspan="18" class="form_caption" ><strong style="font-size:14px"><?php echo $report_title; ?></strong></td>
            </tr>
            <tr>  
               <td align="center" width="100%" colspan="18" class="form_caption" ><strong style="font-size:14px"> <?php echo "From : ".change_date_format(str_replace("'","",$txt_date_from))." To : ".change_date_format(str_replace("'","",$txt_date_to))."" ;?></strong></td>
            </tr>  
        </table>
        <table width="1600px" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" >
        	<thead>
                <th width="40">SL</th>
                <th width="120">Production ID</th>
                <th width="80">Production Date</th>
                <th width="80">Unit Name</th>
                <th width="120">Booking No</th>
                <th width="120">Job No</th>
                <th width="100">Buyer Name</th>
                <th width="100">Style Ref.</th>
                <th width="100">Order No</th>
                <th width="80">Order Qty(Pcs)</th>
                <th width="60">F.GSM</th>
                <th width="100">Fabric Color</th>
                <th width="100">Batch/Lot No</th>
                <th width="80">Batch Qty.</th>
                <th width="80">QC Pass Qty</th>
                <th width="80">Fab Process Loss Qty</th>
                <th width="60">Fab Process Loss %</th>
                <th>Remarks</th>
            </thead>
        </table>
        <div style="width:1620px; overflow-y:scroll; max-height:350px; " >
        <table width="1600px" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" id="tbl_dyeing">
        <?php
		$sql_dtls=sql_select("select a.recv_number, a.receive_date,a.location_id,b.order_id,b.batch_id,b.gsm,b.receive_qnty as qc_pass_qty,b.color_id	from  inv_receive_master a,pro_finish_fabric_rcv_dtls b,order_wise_pro_details c,wo_po_details_master d, wo_po_break_down e where a.company_id=$cbo_company and d.job_no=e.job_no_mst and e.id=c.po_breakdown_id  and a.id=b.mst_id  and b.id=c.dtls_id and a.entry_form=7 and a.item_category=2 and a.status_active=1 $date_cond $unit_name $job_no_cond $order_cond and a.is_deleted=0 group by a.recv_number, a.receive_date,a.location_id,b.batch_id,b.gsm,b.color_id,b.receive_qnty,b.order_id order by a.recv_number");
		$i=1;	
		foreach($sql_dtls as $row)
		{
			  if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$order_id=$row[csf('order_id')];
				//print_r($row[csf('order_id')]);
				$order_number_arr='';
				$job_no='';
				$buyer='';
				$po_qty='';
				$style='';
				$order_number=array_unique(explode(",",$order_id));
				foreach($order_number as $po_id)
				{	if($po_id>0)
					{
					if($order_number_arr=="") $order_number_arr=$order_arr[$po_id]; else $order_number_arr.=",".$order_arr[$po_id];
					if($job_no=="") $job_no=$job_no_arr[$po_id]['job_no_mst']; else $job_no.=",".$job_no_arr[$po_id]['job_no_mst'];
					if($buyer=="") $buyer=$job_no_arr[$po_id]['buyer_name']; else $buyer.=",".$job_no_arr[$po_id]['buyer_name'];
					if($po_qty=="") $po_qty=$job_no_arr[$po_id]['po_quantity']; else $po_qty.=",".$job_no_arr[$po_id]['po_quantity'];
					if($style=="") $style=$job_no_arr[$po_id]['style_ref_no']; else $style.=",".$job_no_arr[$po_id]['style_ref_no'];
				
					}
				}
				$job_mst_id=array_unique(explode(",",$job_no));
				$job_num='';
				foreach($job_mst_id as $job_id)
				{	
					if($job_num=="") $job_num=$job_id; else $job_num.=", ".$job_id;
				}
				 $buyer_id_arr=array_unique(explode(",",$buyer));
				 //print_r($buyer_id_arr);
				$buyer_id='';
				foreach($buyer_id_arr as $buyerid)
				{	
					if($buyer_id=="") $buyer_id=$buyerid; else $buyer_id.=", ".$buyerid;
				}
				 $style_arr=array_unique(explode(",",$style));
				$style_ref_arr='';
				foreach($style_arr as $style_ref)
				{	
					if($style_ref_arr=="") $style_ref_arr=$style_ref; else $style_ref_arr.=", ".$style_ref;
				}
				$order_qty_arr_m='';
				$order_qty_arr=explode(",",$po_qty);
				foreach($order_qty_arr as $po_qty)
				{	
				$order_qty_arr_m+=$po_qty;
				}
				$batch_no=$batch_no_arr[$row[csf('batch_id')]]['batch_no'];
				$batch_qty=$batch_no_arr[$row[csf('batch_id')]]['batch_qnty'];
				$process_loss_qty=$batch_qty-$row[csf('qc_pass_qty')];
				$process_loss_qty_percent=($process_loss_qty/$batch_qty)*100;
				$booking_no=$batch_no_arr[$row[csf('batch_id')]]['booking_no'];
		?>
          <tr bgcolor="<?php echo $bgcolor; ?>"  onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                <td  width="40"><?php echo $i;?> </td>
                <td  width="120"><p><?php echo $row[csf('recv_number')];?></p> </td>
                <td width="80"><p><?php echo $row[csf('receive_date')];?></p>  </td>
                <td width="80"><p><?php echo $location_library[$row[csf('location_id')]];?></p> </td>
                <td  width="120"><p><?php echo $booking_no;?></p>  </td>
                <td  width="120"><p><?php echo $job_num;?></p>  </td>
                <td  width="100"><p><?php echo $buyer_library[$buyer_id];?></p> </td>
                <td width="100"><p><?php echo $style_ref_arr;?></p> </td>
                <td width="100"><p><?php echo $order_number_arr;?></p>  </td>
                <td width="80" align="right"><p><?php echo number_format($order_qty_arr_m);?></p>  </td>
                <td width="60"> <p><?php echo $row[csf('gsm')];?></p> </td>
                <td width="100"><p><?php echo $color_library[$row[csf('color_id')]];?></p> </td>
                <td width="100"><p><?php echo $batch_no; ?></p>  </td>
                <td width="80"  align="right"> <p><?php echo number_format($batch_qty,2); ?></p> </td>
                <td width="80"  align="right"><p><?php echo number_format($row[csf('qc_pass_qty')],2);?></p>  </td>
                <td width="80"  align="right"><p><?php echo number_format($process_loss_qty,2); ?></p>  </td>
                <td width="60"  align="center"> <p><?php echo number_format($process_loss_qty_percent,2); ?></p> </td>
                <td><p></p>  </td>
            </tr>
            <?php
			$total_qc_qty+=$row[csf('qc_pass_qty')];
			$total_batch_qty+=$batch_qty;
			$total_process_loss_qty+=$process_loss_qty;
			$i++;
			}
			?>
        </table>
        <table class="rpt_table" width="1600" cellpadding="0" cellspacing="0" border="1" rules="all">
        <tfoot>
            <tr>
                <th width="40">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="60">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="100"><?php //echo number_format($total_qc_qty,2); ?></th>
                <th width="80"><?php echo number_format($total_batch_qty,2); ?></th>
                <th width="80"><?php echo number_format($total_qc_qty,2); ?></th>
                <th width="80"><?php echo number_format($total_process_loss_qty,2); ?></th>
                <th width="60">&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </tfoot>
		</table>
       </div>
  </div>  
<?php
exit();	
}
?>