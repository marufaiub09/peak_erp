-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_material_allocation_dtls`
--

DROP TABLE IF EXISTS `inv_material_allocation_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_material_allocation_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `job_no` varchar(25) NOT NULL,
  `po_break_down_id` int(11) NOT NULL DEFAULT '0',
  `booking_no` varchar(25) NOT NULL,
  `color_id` int(11) NOT NULL,
  `dia` varchar(50) NOT NULL,
  `item_category` int(11) NOT NULL DEFAULT '0',
  `allocation_date` date NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `qnty` double NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `booking_no` (`booking_no`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_material_allocation_dtls`
--

LOCK TABLES `inv_material_allocation_dtls` WRITE;
/*!40000 ALTER TABLE `inv_material_allocation_dtls` DISABLE KEYS */;
INSERT INTO `inv_material_allocation_dtls` VALUES (1,1,'PAL-15-00145',185,'PAL-Fb-15-00077',0,'',1,'2015-04-27',308,49.73,1,'2015-04-27 11:09:24',0,'0000-00-00 00:00:00',1,0),(2,1,'PAL-15-00145',186,'PAL-Fb-15-00077',0,'',1,'2015-04-27',308,74.6,1,'2015-04-27 11:09:24',0,'0000-00-00 00:00:00',1,0),(3,1,'PAL-15-00145',187,'PAL-Fb-15-00077',0,'',1,'2015-04-27',308,99.47,1,'2015-04-27 11:09:24',0,'0000-00-00 00:00:00',1,0),(4,1,'PAL-15-00145',188,'PAL-Fb-15-00077',0,'',1,'2015-04-27',308,149.2,1,'2015-04-27 11:09:24',0,'0000-00-00 00:00:00',1,0),(7,2,'PAL-15-00074',104,'PAL-Fb-15-00042',0,'',1,'2015-05-04',9,2,2,'2015-05-04 15:20:21',0,'0000-00-00 00:00:00',1,0),(9,4,'PAL-15-00074',104,'PAL-Fb-15-00042',0,'',1,'2015-05-04',10,7,2,'2015-05-04 16:28:54',0,'0000-00-00 00:00:00',1,0),(10,5,'PAL-15-00026',25,'PAL-Fb-15-00020',0,'',1,'2015-05-09',139,3,2,'2015-05-09 11:53:08',0,'0000-00-00 00:00:00',1,0),(11,6,'PAL-15-00178',235,'PAL-Fb-15-00100',0,'',1,'2015-05-12',308,652,17,'2015-05-12 17:48:33',0,'0000-00-00 00:00:00',1,0),(12,7,'PAL-15-00061',92,'PAL-Fb-15-00006',0,'',1,'2015-05-26',962,10,2,'2015-05-26 09:28:37',0,'0000-00-00 00:00:00',1,0),(13,8,'PAL-15-00182',239,'PAL-Fb-15-00099',0,'',1,'2015-05-27',962,147,17,'2015-05-27 16:41:03',0,'0000-00-00 00:00:00',1,0),(14,9,'PAL-15-00183',240,'PAL-Fb-15-00098',0,'',1,'2015-05-27',962,775,17,'2015-05-27 16:57:33',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `inv_material_allocation_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
