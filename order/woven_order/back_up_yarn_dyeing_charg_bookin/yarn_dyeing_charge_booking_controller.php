<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$permission=$_SESSION['page_permission'];

//---------------------------------------------------- Start---------------------------------------------------------------------------
//$po_number=return_library_array( "select id,po_number from wo_po_break_down", "id", "po_number"  );
//$size_library=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$count_arr=return_library_array( "Select id, yarn_count from  lib_yarn_count where  status_active=1",'id','yarn_count');
$brand_arr=return_library_array( "Select id, brand_name from  lib_brand where  status_active=1",'id','brand_name');




if ($action=="variable_chack")
{
	extract($_REQUEST);
	//echo "$company";
	$company=str_replace("'","",$company);
	$variable_setting=return_field_value("color_from_library","variable_order_tracking","variable_list=24 and company_name=$company");
	echo $variable_setting;

}

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 145, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
} 



if ($action=="order_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
	//echo $company;
	?>
<script>
	function js_set_value(str)
	{
		$("#hidden_tbl_id").val(str); // wo/pi id
		parent.emailwindow.hide();
	}
</script>
 
<div align="center" style="width:580px;" >
<form name="searchjob"  id="searchjob" autocomplete="off">
	<table width="500" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="145">Company</th>
                <th width="145">Buyer</th>
                <th width="100">Job No</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:90px" class="formbutton" onClick="reset_form('searchjob','search_div','')"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
					<?php 
                    	echo create_drop_down( "cbo_company_name", 145, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", str_replace("'","",$company)/*$selected */, "load_drop_down( 'yarn_dyeing_charge_booking_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );");
                    ?>&nbsp;
                    </td>
                    <td align="center" id="buyer_td">				
					<?php
                    	$blank_array="select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name";
						//echo $blank_array;
						
						
						//echo create_drop_down( "cbo_buyer_name", 130, $blank_array, 1, "-- Select Buyer --", str_replace("'","",$cbo_buyer_name), "" );
						echo create_drop_down( "cbo_buyer_name", 145, $blank_array,"id,buyer_name", 1, "-- Select Buyer --",0);
                    ?>	
                    </td>    
                    <td align="center">
                        <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:90px" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_job_no').value+'_'+<?php echo $company; ?>, 'create_job_search_list_view', 'search_div', 'yarn_dyeing_charge_booking_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:90px;" />				
                    </td>
            	</tr>
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>

<?php
}

if ($action=="create_job_search_list_view")
{
	$data=explode("_",$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_buyer_name=str_replace("'","",$data[1]);
	$txt_job_no=str_replace("'","",$data[2]);
	//echo $cbo_company_name."**".$cbo_buyer_name."**".$txt_job_no;
	if($cbo_company_name!=0) $cbo_company_name="and a.company_name='$cbo_company_name'"; else $cbo_company_name="";
	if($cbo_buyer_name!=0) $cbo_buyer_name="and a.buyer_name='$cbo_buyer_name'"; else $cbo_buyer_name="";
	if($txt_job_no!="") $txt_job_no="and a.job_no='$txt_job_no'"; else $txt_job_no="";
	
	$sql="select a.id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, group_concat(distinct b.po_number) as po_number from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst $cbo_company_name $cbo_buyer_name $txt_job_no group by a.job_no";
	//echo $sql;
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$arr=array(2=>$buyer_arr);
	echo  create_list_view("list_view", "Job No, Year ,Buyer, Style Ref.NO, Order No.","110,80,100,120,130","590","260",0, $sql , "js_set_value", "id,job_no", "", 1, "0,0,buyer_name,0,0", $arr, "job_no,company_name,buyer_name,style_ref_no,po_number", "",'','0,0,0,0,0,0') ;	
	echo '<input type="hidden" id="hidden_tbl_id">';
}



if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	//echo $cbo_source;die;
	//echo $update_id;die;
	if ($operation==0) 
	{
		
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$color_id=return_id( $txt_yern_color, $color_arr, "lib_color", "id,color_name");
		 
		if(str_replace("'","",$update_id)!="") //update
		{
			$id= return_field_value("id"," wo_yarn_dyeing_mst","id=$update_id");//check sys id for update or insert	
			$field_array="company_id*supplier_id*booking_date*delivery_date*currency*ecchange_rate*pay_mode*source*attention*updated_by*update_date*status_active*is_deleted";
			$data_array="".$cbo_company_name."*".$cbo_supplier_name."*".$txt_booking_date."*".$txt_delivery_date."*".$cbo_currency."*".$txt_exchange_rate."*".$cbo_pay_mode."*".$cbo_source."*".$txt_attention."*'".$user_id."'*'".$pc_date_time."'*1*0";
			$rID=sql_update("wo_yarn_dyeing_mst",$field_array,$data_array,"id",$id,1);	
			$return_no=str_replace("'",'',$txt_booking_no);
		}
		
		else // new insert
 		{			
			$id=return_next_id("id", "wo_yarn_dyeing_mst", 1);			
			// inv_gate_out_mst master table entry here START---------------------------------------//	
			//function return_mrr_number( $company, $location, $category, $year, $num_length, $main_query, $str_fld_name, $num_fld_name, $old_mrr_no )	
			$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'YDW', date("Y"), 5, "select yarn_dyeing_prefix,yarn_dyeing_prefix_num from wo_yarn_dyeing_mst where company_id=$cbo_company_name and YEAR(insert_date)=".date('Y',time())." order by yarn_dyeing_prefix_num DESC ", "yarn_dyeing_prefix", "yarn_dyeing_prefix_num",""));
			$field_array="id,yarn_dyeing_prefix,yarn_dyeing_prefix_num,ydw_no,company_id,supplier_id,booking_date,delivery_date,currency,ecchange_rate,pay_mode,source,attention,inserted_by,insert_date,status_active,is_deleted";
			$data_array="(".$id.",'".$new_sys_number[1]."','".$new_sys_number[2]."','".$new_sys_number[0]."',".$cbo_company_name.",".$cbo_supplier_name.",".$txt_booking_date.",".$txt_delivery_date.",".$cbo_currency.",".$txt_exchange_rate.",".$cbo_pay_mode.",".$cbo_source.",".$txt_attention.",'".$user_id."','".$pc_date_time."',1,0)";
			//echo $field_array."<br>".$data_array;die;
			$rID=sql_insert("wo_yarn_dyeing_mst",$field_array,$data_array,1); 		
			// inv_gate_in_mst master table entry here END---------------------------------------// 
			$return_no=str_replace("'",'',$new_sys_number[0]);
		}
		
		$dtlsid=return_next_id("id", "wo_yarn_dyeing_dtls", 1);		
  		$field_array="id,mst_id,job_no,product_id,job_no_id,count,yarn_description,yarn_color,color_range,uom,yarn_wo_qty,dyeing_charge,amount,no_of_bag,no_of_cone,min_require_cone,status_active,is_deleted";
		$data_array="(".$dtlsid.",".$id.",".$txt_job_no.",".$txt_pro_id.",".$txt_job_id.",".$cbo_count.",".$txt_item_des.",".$color_id.",".$cbo_color_range.",".$cbo_uom.",".$txt_wo_qty.",".$txt_dyeing_charge.",".$txt_amount.",".$txt_bag.",".$txt_cone.",".$txt_min_req_cone.",1,0)";
 		//echo $field_array."<br>".$data_array;die;
		//echo "0**"."INSERT INTO inv_gate_out_dtls (".$field_array.") VALUES ".$data_array;die;
 		$dtlsrID=sql_insert("wo_yarn_dyeing_dtls",$field_array,$data_array,1); 	
		
		//echo "mm";die;
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				//echo "10**";
				echo "10**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
			}
			
		}
		
		if($db_type==1 || $db_type==2)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				//echo "10**";
				echo "10**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
			}
			
		}
		
		disconnect($con);
		die;
		
	}
	
	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		check_table_status( $_SESSION['menu_id'],0);
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		if( str_replace("'","",$update_id) == "")
		{
			echo "15";exit(); 
		}
		
		$color_id=return_id( $txt_yern_color, $color_arr, "lib_color", "id,color_name");
		
		//wo_yarn_dyeing_mst master table UPDATE here START----------------------//	".$txt_pro_id.",
		$field_array="company_id*supplier_id*booking_date*delivery_date*currency*ecchange_rate*pay_mode*source*attention*updated_by*update_date*status_active*is_deleted";
		$data_array="".$cbo_company_name."*".$cbo_supplier_name."*".$txt_booking_date."*".$txt_delivery_date."*".$cbo_currency."*".$txt_exchange_rate."*".$cbo_pay_mode."*".$cbo_source."*".$txt_attention."*'".$user_id."'*'".$pc_date_time."'*1*0";
		$rID=sql_update("wo_yarn_dyeing_mst",$field_array,$data_array,"id",$update_id,1);	
		
		//inv_gate_in_mst master table UPDATE here END---------------------------------------// 
		
		//inv_gate_in_dtls details table UPDATE here START-----------------------------------//	
		
				
 		$field_array = "job_no*product_id*job_no_id*count*yarn_description*yarn_color*color_range*uom*yarn_wo_qty*dyeing_charge*amount*no_of_bag*no_of_cone*min_require_cone";
 		$data_array = "".$txt_job_no."*".$txt_pro_id."*".$txt_job_id."*".$cbo_count."*".$txt_item_des."*".$color_id."*".$cbo_color_range."*".$cbo_uom."*".$txt_wo_qty."*".$txt_dyeing_charge."*".$txt_amount."*".$txt_bag."*".$txt_cone."*".$txt_min_req_cone."";
		//echo $field_array."<br>".$data_array;die;
 		$dtlsrID = sql_update("wo_yarn_dyeing_dtls",$field_array,$data_array,"id",$dtls_update_id,1); 
		//inv_gate_in_dtls details table UPDATE here END-----------------------------------//
 		
		//release lock table
		$return_no=str_replace("'",'',$txt_booking_no);
		check_table_status( $_SESSION['menu_id'],0);
		
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "1**".str_replace("'",'',$return_no)."**".str_replace("'",'',$update_id);
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**".str_replace("'",'',$return_no)."**".str_replace("'",'',$update_id);
		}
		disconnect($con);
		die;
 	}
	
	
	
	
}


if($action=="show_dtls_list_view")
{
	
	//echo "$data";die;
 	$sql = "select id, job_no,count,yarn_description,yarn_color,color_range,uom,yarn_wo_qty,dyeing_charge,amount,no_of_bag, no_of_cone,min_require_cone from wo_yarn_dyeing_dtls where mst_id='$data'"; 
	$arr=array(1=>$count_arr,3=>$color_arr,4=>$unit_of_measurement);
	//function create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path, $filter_grid_fnc, $fld_type_arr, $summary_flds, $check_box_all )
 	echo create_list_view("list_view", "Job No,Count,Description,Color,UOM,WO QTY,Charge,Amount,No of Bag,No of Cone,Minimum Require Cone","80,70,130,100,70,100,100,120,80,80,100","1080","260",0, $sql, "get_php_form_data", "id", "'child_form_input_data','requires/yarn_dyeing_charge_booking_controller'", 1, "0,count,0,yarn_color,uom,0,0,0,0,0,0", $arr, "job_no,count,yarn_description,yarn_color,uom,yarn_wo_qty,dyeing_charge,amount,no_of_bag,no_of_cone,min_require_cone", "","",'0,0,0,0,0,1,1,2,1,1,1',"");	
	exit();
		
}

if($action=="child_form_input_data")
{
	//echo $data;
	
	$sql = "select id,job_no,product_id,job_no_id,count,yarn_description,yarn_color,color_range,uom,yarn_wo_qty,dyeing_charge,amount,no_of_bag, no_of_cone,min_require_cone from wo_yarn_dyeing_dtls where id='$data'";
	//echo $sql;
	$sql_re=sql_select($sql);
	foreach($sql_re as $row)
	{
		echo "$('#txt_job_no').val('".$row[csf("job_no")]."');\n";
		echo "$('#txt_job_id').val('".$row[csf("job_no_id")]."');\n";
		echo "$('#txt_pro_id').val(".$row[csf("product_id")].");\n";
		$lot=return_field_value("lot"," product_details_master","id=".$row[csf("product_id")]."","lot");
		if($row[csf("product_id")]>0)
		{
		echo "$('#txt_lot').val('$lot');\n";
		echo "$('#cbo_count').val(0).attr('disabled',true);\n";
		echo "$('#txt_item_des').val('".$row[csf("yarn_description")]."').attr('disabled',true);\n";
		}
		else
		{
		echo "$('#txt_lot').val('$lot');\n";
		echo "$('#cbo_count').val(".$row[csf("count")].");\n";
		echo "$('#txt_item_des').val('".$row[csf("yarn_description")]."');\n";
		}
		echo "$('#txt_yern_color').val('".$color_arr[$row[csf("yarn_color")]]."');\n";
		echo "$('#cbo_color_range').val(".$row[csf("color_range")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("uom")].");\n";
		echo "$('#txt_wo_qty').val(".$row[csf("yarn_wo_qty")].");\n";
		echo "$('#txt_dyeing_charge').val(".$row[csf("dyeing_charge")].");\n";
		echo "$('#txt_amount').val(".$row[csf("amount")].");\n";		
 		echo "$('#txt_bag').val(".$row[csf("no_of_bag")].");\n";
		echo "$('#txt_cone').val(".$row[csf("no_of_cone")].");\n";
		echo "$('#txt_min_req_cone').val(".$row[csf("min_require_cone")].");\n";
		//update id here
		echo "$('#dtls_update_id').val(".$row[csf("id")].");\n";		
		echo "set_button_status(1, permission, 'fnc_yarn_dyeing',1,0);\n";
	}
}



if ($action=="yern_dyeing_booking_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		$("#hidden_sys_number").val(str);
		parent.emailwindow.hide(); 
	}
</script>
</head>
<body>
    <div align="center" style="width:590px;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="580" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="200">Supplier Name</th>
                <th width="220">Date Range</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td align="center">
					<?php
                    	echo create_drop_down( "cbo_supplier_name", 172, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and a.status_active =1 and b.party_type in(2,21) group by a.id order by supplier_name","id,supplier_name", 1, "-- Select Supplier --", $selected, "",0 );
                    ?>
                    </td>
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                       <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_sys_search_list_view', 'search_div', 'yarn_dyeing_charge_booking_controller', 'setFilterGrid(\'list_view4\',-1)')" style="width:100px;" />				
                    </td>
                </tr>
                <tr>                  
                    <td align="center" height="40" valign="middle" colspan="4">
                        <?php echo load_month_buttons(1);  ?>
                        <!-- Hidden field here-------->
                       <!-- input type="hidden" id="hidden_tbl_id" value="" ---->
                       
                        <input type="hidden" id="hidden_sys_number" value="hidden_sys_number" />
                        <!-- ---------END-------------> 
                    </td>
                </tr>    
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"></div> 
        </form>
    </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_sys_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$fromDate = $ex_data[1];
	$toDate = $ex_data[2];
	$company = $ex_data[3];
	//echo $company;die;
 	//$sql_cond="";
	if( $supplier!=0 )  $supplier=" and supplier_id='$supplier'"; else  $supplier="";
	
	if( $fromDate!=0 && $toDate!=0 ) $sql_cond= "and booking_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
	
	if( $company!=0 )  $company=" and company_id='$company'"; else  $company="";
	
	$sql = "select id, yarn_dyeing_prefix_num, ydw_no, company_id, supplier_id, booking_date, delivery_date, currency, ecchange_rate, pay_mode,source, attention from  wo_yarn_dyeing_mst where status_active=1 and is_deleted=0 $supplier $company $sql_cond";
	//echo $sql; die;
	//echo $sql_cond; die;
	$sample_arr = return_library_array( "select id, sample_name from lib_sample",'id','sample_name');
	$arr=array(1=>$supplier_arr);
	//function create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path, $filter_grid_fnc, $fld_type_arr, $summary_flds, $check_box_all ,$new_conn ){}
	echo create_list_view("list_view4", "Booking no,Supplier Name,Booking Date,Delevary Date","60,180,120,120","530","260",0, $sql , "js_set_value", "id,ydw_no,company_id,supplier_id,booking_date,delivery_date,currency,ecchange_rate,pay_mode,attention,source", "", 1, "0,supplier_id,0,0", $arr, "yarn_dyeing_prefix_num,supplier_id,booking_date,delivery_date", "yarn_dyeing_charge_booking_controller",'','0,0,3,3') ;	
	exit();
	
}
if($action=="dyeing_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$company=str_replace("'","",$company);
	?>
<script>
	function js_set_value(str)
	{
		$("#hidden_rate").val(str);
		parent.emailwindow.hide(); 
	}
</script>
</head>
<body>
    <div align="center" style="width:590px;" >
        <fieldset>
            <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
                <table width="570" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                    <thead>
                        <tr>
                            <th width="40">Sl No.</th>
                            <th width="170">Const. Compo.</th>
                            <th width="100">Process Name</th>
                            <th width="100">Color</th>
                            <th width="90">Rate</th>
                            <th>UOM</th> 
                        </tr>
                    </thead>
                </table>
                <?php
				$sql="select id,comapny_id,const_comp,process_type_id,process_id,color_id,width_dia_id,in_house_rate,uom_id,rate_type_id,status_active from lib_subcon_charge where comapny_id=$company and process_id=30 and status_active=1";
				//echo $sql;
				$sql_result=sql_select($sql);
				?>
            	<div style="width:570px; overflow-y:scroll; max-height:240px;font-size:12px; overflow-x:hidden; cursor:pointer;">
                    <table width="570" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center" id="table_charge">
                        <tbody>
                        <?php
						$i=1;
						foreach($sql_result as $row)
						{
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							?>
                            <tr bgcolor="<?php  echo $bgcolor; ?>" onClick="js_set_value(<?php echo $row[csf("in_house_rate")]; ?>)">
                                <td width="40" align="center"><?php echo $i;  ?></td>
                                <td width="170"><?php echo $row[csf("const_comp")]; ?></td>
                                <td width="100" align="center"><?php echo $conversion_cost_head_array[$row[csf("process_id")]]; ?></td>
                                <td width="100"><?php echo $color_arr[$row[csf("color_id")]]; ?></td>
                                <td width="90" align="right"><?php echo number_format($row[csf("in_house_rate")],2); ?></td>
                                <td><?php echo $unit_of_measurement[$row[csf("uom_id")]]; ?></td> 
                            </tr>
                            <?php
							$i++;
						}
						?>
                          <input type="hidden" id="hidden_rate" />  
                        </tbody>
                    </table>
                </div>
            </form>
        </fieldset>
    </div>
</body>
<script  type="text/javascript">setFilterGrid("table_charge",-1)</script>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>    
    
    <?php
}


if($action=="lot_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$company=str_replace("'","",$company);
	$job_no=str_replace("'","",$job_no);
	//echo $job_no;die;
/*	$product_id=return_field_value("group_concat(distinct item_id) as pro_id","inv_material_allocation_dtls","job_no='$job_no'","pro_id");
	
	if(!$product_id) 
	{
		echo "<h1>No Product Found";die;
		
	}
	else
	{*/
		
		//echo "yes";die;
		?>
	<script>
		function js_set_value(str)
		{
			$("#hidden_product").val(str);
			parent.emailwindow.hide(); 
		}
	</script>
	</head>
	<body>
		<div align="center" style="width:580px;" >
			<fieldset>
				<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
					<table width="570" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
						<thead>
							<tr>
								<th width="40">Sl No.</th>
                                <th width="100">Lot No</th>
                                <th width="90">Brand</th>
								<th width="200">Product Name Details</th>
                                <th width="80">Stock</th>
								<th>UOM</th> 
							</tr>
						</thead>
					</table>
					<?php
					
					$sql="select id,product_name_details,lot,item_code,unit_of_measure,yarn_count_id,brand,current_stock from product_details_master where company_id='$company' and item_category_id=1";
					//echo $sql;
					$sql_result=sql_select($sql);
					?>
            			<div style="width:570px; overflow-y:scroll; max-height:250px;font-size:12px; overflow-x:hidden;" >
						<table width="570" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center" id="table_charge" style="cursor:pointer">
							<tbody>
							<?php
							$i=1;
							foreach($sql_result as $row)
							{
								if ($i%2==0)
								$bgcolor="#E9F3FF";
								else
								$bgcolor="#FFFFFF";
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value('<?php echo $row[csf("product_name_details")]; ?>,<?php echo $row[csf("yarn_count_id")]; ?>,<?php echo $row[csf("lot")]; ?>,<?php echo $row[csf("id")]; ?>')">
									<td width="40" align="center"><?php echo $i;  ?></td>
                                    <td width="100" align="center"><?php echo $row[csf("lot")]; ?></td>
                                    <td width="90"><?php echo $brand_arr[$row[csf("brand")]]; ?></td>
									<td width="200"><?php echo $row[csf("product_name_details")]; ?></td>
									<td width="80" align="right"><?php echo $row[csf("current_stock")]; ?></td>
									<td><?php echo $unit_of_measurement[$row[csf("unit_of_measure")]]; ?></td> 
								</tr>
								<?php
								$i++;
							}
							?>
							  <input type="hidden" id="hidden_product" style="width:200px;" />  
							</tbody>
						</table>
                        </div>
				</form>
			</fieldset>
		</div>
	</body>
	<script  type="text/javascript">setFilterGrid("table_charge",-1)</script>           
	<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>    
		
		<?php
	//}
}




if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Order Search","../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
	<script>
function add_break_down_tr(i) 
 {
	var row_num=$('#tbl_termcondi_details tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
	 
		 $("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});  
		  }).end().appendTo("#tbl_termcondi_details");
		 $('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
		  $('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
		  $('#termscondition_'+i).val("");
	}
		  
}

function fn_deletebreak_down_tr(rowNo) 
{   
	
	
		var numRow = $('table#tbl_termcondi_details tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_termcondi_details tbody tr:last').remove();
		}
	
}

function fnc_fabric_booking_terms_condition( operation )
{
	    var row_num=$('#tbl_termcondi_details tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('termscondition_'+i,'Term Condition')==false)
			{
				return;
			}
			
			data_all=data_all+get_submitted_data_string('txt_booking_no*termscondition_'+i,"../../../");
		}
		var data="action=save_update_delete_fabric_booking_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
		//freeze_window(operation);
		http.open("POST","trims_booking_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_booking_terms_condition_reponse;
}

function fnc_fabric_booking_terms_condition_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
			if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				//$('#txt_terms_condision_book_con').val(reponse[1]);
				parent.emailwindow.hide();
			}
	}
}
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
<?php echo load_freeze_divs ("../../../",$permission);  ?>
<fieldset>
        	<form id="termscondi_1" autocomplete="off">
           <input type="text" id="txt_booking_no" name="txt_booking_no" value="<?php echo str_replace("'","",$txt_booking_no) ?>" class="text_boxes" readonly />
           <input type="text" id="txt_terms_condision_book_con" name="txt_terms_condision_book_con" >
            
            
            <table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
                	<thead>
                    	<tr>
                        	<th width="50">Sl</th><th width="530">Terms</th><th ></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$data_array=sql_select("select id, terms from  wo_booking_terms_condition where booking_no=$txt_booking_no");// quotation_id='$data'
					if ( count($data_array)>0)
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							?>
                            	<tr id="settr_1" align="center">
                                    <td>
                                    <?php echo $i;?>
                                    </td>
                                    <td>
                                    <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row['terms']; ?>" /> 
                                    </td>
                                    <td> 
                                    <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                    <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?>);" />
                                    </td>
                                </tr>
                            <?php
						}
					}
					else
					{
					$data_array=sql_select("select id, terms from  lib_terms_condition");// quotation_id='$data'
					foreach( $data_array as $row )
						{
							$i++;
					?>
                    <tr id="settr_1" align="center">
                                    <td>
                                    <?php echo $i;?>
                                    </td>
                                    <td>
                                    <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row['terms']; ?>" /> 
                                    </td>
                                    <td>
                                    <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                    <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?> );" />
                                    </td>
                                </tr>
                    <?php 
						}
					} 
					?>
                </tbody>
                </table>
                
                <table width="650" cellspacing="0" class="" border="0">
                	<tr>
                        <td align="center" height="15" width="100%"> </td>
                    </tr>
                	<tr>
                        <td align="center" width="100%" class="button_container">
						        <?php
									echo load_submit_buttons( $permission, "fnc_fabric_booking_terms_condition", 0,0 ,"reset_form('termscondi_1','','','','')",1) ; 
									?>
                        </td> 
                    </tr>
                </table>
            </form>
        </fieldset>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="save_update_delete_fabric_booking_terms_condition")
{
$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id=return_next_id( "id", "wo_booking_terms_condition", 1 ) ;
		 $field_array="id,booking_no,terms";
		 for ($i=1;$i<=$total_row;$i++)
		 {
			 $termscondition="termscondition_".$i;
			if ($i!=1) $data_array .=",";
			$data_array .="(".$id.",".$txt_booking_no.",".$$termscondition.")";
			$id=$id+1;
		 }
		// echo  $data_array;
		$rID_de3=execute_query( "delete from wo_booking_terms_condition where  booking_no =".$txt_booking_no."",0);

		 $rID=sql_insert("wo_booking_terms_condition",$field_array,$data_array,1);
		 check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$txt_booking_no;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$txt_booking_no;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$txt_booking_no;
		}
		disconnect($con);
		die;
	}	
}



if($action=="show_trim_booking_report")
{
	//echo "uuuu";die;
	extract($_REQUEST);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$update_id=str_replace("'","",$update_id);
	$txt_booking_no=str_replace("'","",$txt_booking_no);
	$buyer_name_arr=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
	$brand_arr=return_library_array( "select id,brand_name from  lib_brand",'id','brand_name');
	//$po_qnty_tot=return_field_value( "sum(plan_cut)", "wo_po_break_down","id in(".str_replace("'","",$txt_order_no_id).")");
	?>
	<div style="width:1130px" align="center">       
       <table width="100%" cellpadding="0" cellspacing="0" border="0">
           <tr>
               <td width="100"> 
               </td>
               <td width="1000">                                     
                    <table width="100%" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td align="center" style="font-size:20px;">
                              <?php
                                    echo $company_library[$cbo_company_name];
                              ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size:14px">  
                            <?php
                            $nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$cbo_company_name"); 
                            foreach ($nameArray as $result)
                            { 
                            ?>
                                            Plot No: <?php echo $result['plot_no']; ?> 
                                            Level No: <?php echo $result['level_no']?>
                                            Road No: <?php echo $result['road_no']; ?> 
                                            Block No: <?php echo $result['block_no'];?> 
                                            City No: <?php echo $result['city'];?> 
                                            Zip Code: <?php echo $result['zip_code']; ?> 
                                            Province No: <?php echo $result['province'];?> 
                                            Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
                                            Email Address: <?php echo $result['email'];?> 
                                            Website No: <?php echo $result['website'];
                            }
                              
							               ?>   
                               </td> 
                            </tr>
                            <tr>
                            <td align="center" style="font-size:20px">  
                            <strong>Yarn Dyeing Work Order Sheet</strong>
                             </td> 
                            </tr>
                      </table>
                </td>       
            </tr>
       </table>
		<?php
		//echo "select a.id, a.ydw_no,a.booking_date,a.supplier_id,a.attention  from wo_yarn_dyeing_mst a where a.id=$update_id";die;
        $nameArray=sql_select( "select a.id, a.ydw_no,a.booking_date,a.supplier_id,a.attention,a.delivery_date,a.currency from wo_yarn_dyeing_mst a where a.id=$update_id"); 
        foreach ($nameArray as $result)
        {
			$work_order=$result['ydw_no'];
			$supplier_id=$result['supplier_id'];
			$booking_date=$result['booking_date'];
			$currency=$result['currency'];
			$attention=$result['attention'];
			$delivery_date=$result['delivery_date'];
        }
        ?>
       <table width="700" style="" align="left">
       		<tr>
            	<td width="350">
                        <table width="350" style="" align="left">
                            <tr>
                                <td  style="font-size:12px" width="100"><b>To</b>   </td>
                                <td width="250">:&nbsp;&nbsp;<?php echo $supplier_arr[$supplier_id];?>    </td>
                            </tr>  
                            <tr>
                                <td  style="font-size:12px"><b>Wo No.</b></td>
                                <td >:&nbsp;&nbsp;<?php echo $work_order;?> </td>
                            </tr>
                            <tr>
                                <td style="font-size:12px"><b>Booking Date</b></td>
                                <td >:&nbsp;&nbsp;<?php echo change_date_format($booking_date); ?></td>
                            </tr> 
                        </table>
                </td>
                <td width="350">
                		<table width="350" style="" align="left">
                            <tr>
                                <td  style="font-size:12px" width="100"><b>Currency</b>   </td>
                                <td width="250">:&nbsp;&nbsp;<?php echo $currency;?>    </td>
                            </tr>
                            <tr>
                                <td style="font-size:12px"><b>Attention</b></td>
                                <td >:&nbsp;&nbsp;<?php echo $attention; ?></td>
                            </tr> 
                            <tr>
                                <td  style="font-size:12px"><b>Delivery Date</b></td>
                                <td >:&nbsp;&nbsp;<?php echo change_date_format($delivery_date);?> </td>
                            </tr>
                        </table>
                </td>
            </tr>
       </table>
       
          
        <table width="1130" style="" align="left" border="1"  cellpadding="0" cellspacing="0" rules="all">
            <tr>
             	<td width="30" align="center"><strong>Sl</strong></td>
                <td width="100" align="center"><strong>Job No</strong></td>
                <td width="100" align="center"><strong>Buyer</strong></td>
                <td width="110" align="center"><strong>Order No</strong></td>
                <td width="40" align="center"><strong>Yarn Count</strong></td>
                <td width="60" align="center"><strong>Lot</strong></td>
                <td width="205" align="center"><strong>Yarn Description</strong></td>
                <td width="60" align="center"><strong>Brand</strong></td>
                <td width="75" align="center"><strong>Color</strong></td>
                <td width="100" align="center"><strong>Color Range</strong></td>
                <td width="60" align="center"><strong>WO Qty</strong></td>
                <td width="40" align="center"><strong>Dyeing Rate</strong></td>
                <td width="75" align="center"><strong>Amount</strong></td>
                <td  align="center"><strong>Min Req. Cone</strong></td>
            </tr>
            <?php
			$sql="select a.id, a.ydw_no,b.job_no,b.yarn_color,b.yarn_description,b.count,b.color_range,b.yarn_wo_qty,b.dyeing_charge,b.amount, b.min_require_cone
			from 
					wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b 
			where 
					a.id=b.mst_id and a.id=$update_id";
			//echo $sql;die;
			$sql_result=sql_select($sql);$total_qty=0;$total_amount=0;$i=1;$buyer=0;$order_no="";
			foreach($sql_result as $row)
			{
				$order_sql=sql_select("select  a.buyer_name, b.po_number from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.job_no='".$row[csf("job_no")]."'");
				foreach($order_sql as $row_po)
				{
					$buyer=$row_po[csf("buyer_name")];
					$order_no=$row_po[csf("po_number")];
				}
				$product_id=return_field_value("group_concat(distinct item_id) as pro_id","inv_material_allocation_dtls","job_no='".$row[csf("job_no")]."'","pro_id");
				if($product_id)
				{
					$sql_brand=sql_select("select lot,brand from product_details_master where id in($product_id)");
					foreach($sql_brand as $row_barand)
					{
						$lot_amt=$row_barand[csf("lot")];
						$brand=$row_barand[csf("brand")];
					}
					
				}

				
			?>
            <tr>
            	<td align="center"><?php echo $i; ?></td>
                <td><?php echo $row[csf("job_no")]; ?></td>
                <td><?php echo $buyer_name_arr[$buyer]; ?></td>
                <td><?php echo $order_no; ?></td>
                <td align="center"><?php echo $count_arr[$row[csf("count")]]; ?></td>
                <td align="center"><?php echo $lot_amt; ?></td>
                <td><?php echo $row[csf("yarn_description")]; ?></td>
                <td><?php echo $brand_arr[$brand]; ?></td>
                <td><?php echo $color_arr[$row[csf("yarn_color")]]; ?></td>
                <td><?php echo $color_range[$row[csf("color_range")]]; ?></td>
                <td align="right"><?php echo $row[csf("yarn_wo_qty")]; $total_qty+=$row[csf("yarn_wo_qty")]; ?></td>
                <td align="right"><?php echo $row[csf("dyeing_charge")]; ?></td>
                <td align="right"><?php echo number_format($row[csf("amount")],2);  $total_amount+=$row[csf("amount")];?></td>
                <td align="center"><?php echo $row[csf("min_require_cone")]; ?></td>
            </tr>
            <?php
			$i++;
			}
			?>
             <tr>
                <td colspan="10" align="right"><strong>Total:</strong>&nbsp;&nbsp;</td>
                <td align="right"><?php echo $total_qty; ?></td>
                <td></td>
                <td align="right"><?php echo number_format($total_amount,2); ?></td>
                <td></td>
            </tr>
        </table>
        
        
          <!--==============================================AS PER GMTS COLOR START=========================================  -->
        <table width="1130" style="" align="left"><tr><td><strong>Note:</strong></td></tr></table>
        
        <table  width="1130" class="rpt_table"    border="0" cellpadding="0" cellspacing="0">
        <thead>
            <tr style="border:1px solid black;">
                <th width="3%" style="border:1px solid black;">Sl</th><th width="97%" style="border:1px solid black;">Spacial Instruction</th>
            </tr>
        </thead>
        <tbody>
        <?php
		//echo "select id, terms from  wo_booking_terms_condition where booking_no ='$txt_booking_no'";
        $data_array=sql_select("select id, terms from  wo_booking_terms_condition where booking_no ='$txt_booking_no'");// quotation_id='$data'
        if ( count($data_array)>0)
        {
            $i=0;
            foreach( $data_array as $row )
            {
                $i++;
                ?>
                    <tr id="settr_1" align="" style="border:1px solid black;">
                        <td style="border:1px solid black;">
                        <?php echo $i;?>
                        </td>
                        <td style="border:1px solid black;">
                        <?php echo $row['terms']; ?>
                        </td>
                    </tr>
                <?php
            }
        }
        else
        {
			$i=0;
        $data_array=sql_select("select id, terms from  lib_terms_condition");// quotation_id='$data'
        foreach( $data_array as $row )
            {
                $i++;
        ?>
        <tr id="settr_1" align="" style="border:1px solid black;">
                        <td style="border:1px solid black;">
                        <?php echo $i;?>
                        </td>
                        <td style="border:1px solid black;">
                        <?php echo $row['terms']; ?>
                        </td>
                        
                    </tr>
        <?php 
            }
        } 
        ?>
    </tbody>
    </table>
                
        
         <br/>
         <br/>
         <br/>
         <br/>
        
    </div>
    <div>
		<?php
        	echo signature_table(43, $cbo_company_name, "1130px");
        ?>
    </div>
<?php
}

?>
