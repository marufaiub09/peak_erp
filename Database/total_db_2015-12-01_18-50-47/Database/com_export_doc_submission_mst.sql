-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_export_doc_submission_mst`
--

DROP TABLE IF EXISTS `com_export_doc_submission_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_export_doc_submission_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `submit_date` date NOT NULL,
  `entry_form` tinyint(4) NOT NULL DEFAULT '0',
  `submit_to` int(11) NOT NULL DEFAULT '0',
  `bank_ref_no` varchar(50) NOT NULL,
  `bank_ref_date` date NOT NULL,
  `days_to_realize` int(11) NOT NULL DEFAULT '0',
  `possible_reali_date` date NOT NULL,
  `courier_receipt_no` varchar(50) NOT NULL,
  `courier_company` varchar(50) NOT NULL,
  `courier_date` date NOT NULL,
  `bnk_to_bnk_cour_no` varchar(50) NOT NULL,
  `bnk_to_bnk_cour_dt` date NOT NULL,
  `lien_bank` int(11) NOT NULL DEFAULT '0',
  `lc_currency` int(11) NOT NULL DEFAULT '0',
  `submit_type` int(1) NOT NULL DEFAULT '0',
  `negotiation_date` date NOT NULL,
  `total_negotiated_amount` double NOT NULL,
  `total_lcsc_currency` double NOT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_posted_account` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_export_doc_submission_mst`
--

LOCK TABLES `com_export_doc_submission_mst` WRITE;
/*!40000 ALTER TABLE `com_export_doc_submission_mst` DISABLE KEYS */;
INSERT INTO `com_export_doc_submission_mst` VALUES (1,1,61,'2015-10-25',40,1,'M','0000-00-00',0,'0000-00-00','','','0000-00-00','','0000-00-00',0,2,0,'0000-00-00',0,0,'',1,'2015-10-25 13:45:30',0,'0000-00-00 00:00:00',1,0,0),(2,1,68,'2015-11-05',40,1,'','2015-11-05',0,'0000-00-00','2718460006','DHL','0000-00-00','2718460006','2015-11-05',2,2,1,'0000-00-00',0,0,'',13,'2015-11-07 10:34:40',13,'2015-11-11 09:42:32',1,0,0),(3,1,42,'2015-11-02',40,1,'FDBC-116/2015','0000-00-00',18,'0000-00-00','','','0000-00-00','','0000-00-00',4,2,1,'0000-00-00',0,0,'',13,'2015-11-15 16:11:07',0,'0000-00-00 00:00:00',1,0,0),(4,1,42,'2015-11-02',40,1,'FDBC-115/2015','0000-00-00',0,'0000-00-00','','','0000-00-00','','0000-00-00',4,2,1,'0000-00-00',0,0,'',13,'2015-11-15 16:19:14',0,'0000-00-00 00:00:00',1,0,0),(5,1,34,'2015-11-10',40,1,'FDBP-253/2015','0000-00-00',0,'0000-00-00','','','0000-00-00','','0000-00-00',4,2,2,'2015-11-11',16000,0,'',13,'2015-11-15 16:30:09',13,'2015-11-15 16:31:12',1,0,0);
/*!40000 ALTER TABLE `com_export_doc_submission_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
