<?
	session_start();
	if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
	require_once('../../includes/common.php');
	extract($_REQUEST);
	$_SESSION['page_permission']=$permission;
	//----------------------------------------------------------------------------------------------------------------
	echo load_html_head_contents("Yarn Count Determination", "../../", 1, 1,$unicode,'','');
?>
 
<script type="text/javascript">

	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<? echo $permission; ?>';
	function fnc_yarn_count_determination( operation )
	{
		if (form_validation('cbofabricnature*txtconstruction','Fab Nature*Constrution')==false)
		{
			return;
		}
		
		else 
		{
			var row_num=$('#tbl_yarn_count tr').length-1;
			var data_all="";
			for (var i=1; i<=row_num; i++)
			{
				data_all=data_all+get_submitted_data_string('cbofabricnature*txtconstruction*txtgsmweight*cbocolortype*stichlength*processloss*update_mst_id*cbocompone_'+i+'*percentone_'+i+'*cbocountcotton_'+i+'*cbotypecotton_'+i+'*updateid_'+i,"../../");
			}
		    var data="action=save_update_delete&operation="+operation+'&total_row='+row_num+data_all;
			freeze_window(operation);
			http.open("POST","requires/yarn_count_determination_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_yarn_count_determination_reponse;
		}
	}
	
	function fnc_yarn_count_determination_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split('**');
			
			
			
			if(reponse[0]==15) 
			{ 
				 setTimeout('fnc_yarn_count_determination('+ reponse[1]+')',8000); 
			}
			else
			{
				//alert(reponse[0]);
				show_msg(trim(reponse[0]));
				show_list_view(reponse[1],'search_list_view','yarn_count_container','../merchandising_details/requires/yarn_count_determination_controller','setFilterGrid("list_view",-1)');
				reset_form('yarncountdetermination_1','','');
				set_button_status(0, permission, 'fnc_yarn_count_determination',1);
				release_freezing();
			}
		}
	}
	

function add_break_down_tr(i) 
 {
	var row_num=$('#tbl_yarn_count tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_yarn_count tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});  
		  }).end().appendTo("#tbl_yarn_count");
		 $('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
		 $('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+");");
		 
		 $('#cbocompone_'+i).removeAttr("onChange").attr("onChange","check_duplicate("+i+",this.id);");
		 $('#cbocountcotton_'+i).removeAttr("onChange").attr("onChange","check_duplicate("+i+",this.id);");
		 $('#cbotypecotton_'+i).removeAttr("onChange").attr("onChange","check_duplicate("+i+",this.id);");
		 $('#percentone_'+i).removeAttr("onChange").attr("onChange","sum_percent()");

		  $('#cbocompone_'+i).val("");
		  $('#percentone_'+i).val("");
		  $('#cbocountcotton_'+i).val("");
		  $('#cbotypecotton_'+i).val("");
		  $('#updateid_'+i).val("");
	}
}

function fn_deletebreak_down_tr(rowNo,table_id) 
{   
		var numRow = $('table#tbl_yarn_count tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_yarn_count tbody tr:last').remove();
		}
}

function show_detail_form(mst_id)
{
			show_list_view(mst_id,'show_detail_form','form_div','requires/yarn_count_determination_controller','');
}


function check_duplicate(id,td)
		{
			var cbocompone=document.getElementById('cbocompone_'+id).value;
			var cbocountcotton=document.getElementById('cbocountcotton_'+id).value;
			var cbotypecotton=document.getElementById('cbotypecotton_'+id).value;
			var row_num=$('#tbl_yarn_count tr').length-1;
			for (var k=1;k<=row_num; k++)
			{
				if(k==id)
				{
					continue;
				}
				else
				{
					if(cbocompone==document.getElementById('cbocompone_'+k).value && cbocountcotton==document.getElementById('cbocountcotton_'+k).value && cbotypecotton==document.getElementById('cbotypecotton_'+k).value)
					{
						alert("Same Gmts Composition, Same Count and Same Type Duplication Not Allowed.");
						document.getElementById(td).value=0;
						document.getElementById(td).focus();
					}
				}
			}
		}
		
function sum_percent()
{
	var i=0;
	 var tot_percent=0;
	 var row_num=$('#tbl_yarn_count tr').length-1;
	 for (var k=1;k<=row_num; k++)
	 {
		 tot_percent+=(document.getElementById('percentone_'+k).value)*1;
		 i++
	 }
	 if(tot_percent>100)
	 {
		 alert("Total Percentage More Than 100 Not Allowed");
		 document.getElementById('percentone_'+i).value=""; 
	 }
}
</script>
</head>	
<body onLoad="set_hotkey()">
    <div align="center" style="width:100%; position:relative; margin-bottom:5px; margin-top:5px">
		<? echo load_freeze_divs ("../../",$permission);  ?>	     
        <form name="yarncountdetermination_1" id="yarncountdetermination_1" autocomplete="off">
            <fieldset style="width:680px;">
                <legend>Yarn Count Determination </legend>
                
                <table width="100%" border="0" cellpadding="0" cellspacing="2">
                   
                    	<tr>
                        <td width="100" class="must_entry_caption">
                        Fab Nature
                        </td>
                        <td width="210">
                         <?  echo create_drop_down( "cbofabricnature",200, $item_category,"", 0, "", '', "",$disabled,"2,3" ); ?>
                        </td>
                        <td width="100" class="must_entry_caption">
                        Construction
                        </td>
                        <td width="200">
                        <input type="text" id="txtconstruction"  name="txtconstruction" class="text_boxes" style="width:200px" value=""  />     
                        </td>
                        
                        </tr>
                        <tr>
                        <td>
                        Color Range
                        </td>
                        <td>
                       <?  echo create_drop_down( "cbocolortype", 200, $color_range,"", 1, "-- Select --", '', "",$disabled,"" ); ?>                    
                        </td>
                        <td>
                        GSM
                        </td>
                        <td>
                       <input type="text" id="txtgsmweight" name="txtgsmweight" class="text_boxes_numeric" style="width:200px"  value=""  />                    
                        </td>
                        
                        </tr>
                        <tr>
                        <td colspan="4" align="center" class="button_container">
                            <input type="hidden" id="update_mst_id" value=""/> 
                        </td>				
                        </tr>
                    </table>
                    </fieldset>
                  
                    
                    
                  
                    <fieldset style="width:680px;">
                    <legend>Yarn Count Determination </legend>
                    <div id="form_div">
                    <table width="100%" border="0" id="tbl_yarn_count" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" >
                    <thead>
                    	<tr>
                        <th  width="150">Composition</th> <th width="50">%</th> <th width="150">Count</th><th width="150">Type</th> <th width="">  </th> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="yarncost_1" align="center">
                                    <td width="150">
									<?  echo create_drop_down( "cbocompone_1", 150, $composition,"", 1, "-- Select --", '', "check_duplicate(1,this.id)",'','' ); ?>
                                    </td>
                                    <td width="50">
                                    <input type="text" id="percentone_1"  name="percentone_1" onChange="sum_percent()" class="text_boxes" style="width:50px"  value="" />
                                    </td>
                                    <td width="70">
									<? 
									echo create_drop_down( "cbocountcotton_1", 150, "select id,yarn_count from  lib_yarn_count where is_deleted=0 and status_active=1 order by yarn_count", "id,yarn_count",1," -- Select Count --", '', 'check_duplicate(1,this.id)','','' ); 
									?>
                                    </td>
                                    <td width="100">
									<?  
									echo create_drop_down( "cbotypecotton_1", 150, $yarn_type,"", 1, "-- Select --", '', 'check_duplicate(1,this.id)','','' ); 
									?>
                                    </td>
                                    <td> 
                                    <input type="button" id="increase_1" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(1)" />
									<input type="button" id="decrease_1" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(1);" />
                                    <input type="hidden" id="updateid_1" name="updateid_1"  class="text_boxes" style="width:20px" value=""  />  
                                    </td>  
                      </tr>
                      </tbody>
                     </table>
                     </div>
                     <br/>
                    <table width="100%" border="" cellpadding="0" cellspacing="0"  rules="all">
                        <tr>
                            <td  align="center" width="150">
                            Stich Length 
                            </td>
                            <td align="center" width="50">
                            <input type="text" id="stichlength" name="stichlength" class="text_boxes_numeric" style="width:50px" value="">
                            </td>
                            <td width="150" align="center">
                            Process Loss
                            </td>
                            <td width="150">
                            <input type="text" id="processloss" name="processloss" class="text_boxes_numeric" style="width:50px" value="">                      
                            </td>
                            <td>
                            </td>				
                        </tr>	
                        <tr>
                        <td colspan="5" align="center" class="button_container">
                        <? 
                        echo load_submit_buttons( $permission, "fnc_yarn_count_determination", 0,0 ,"reset_form('yarncountdetermination_1','','')",1);
                        ?> 
                        </td>				
                        </tr>	
                    </table>
                     </fieldset>
                    </form>	
                    <div id="yarn_count_container">
                    <?
					    $composition_arr=array();
					    $lib_yarn_count=return_library_array( "select yarn_count,id from lib_yarn_count", "id", "yarn_count");
						$arr=array (0=>$item_category, 3=>$color_range,6=>$composition,8=>$lib_yarn_count,9=>$yarn_type);
						$sql="select a.fab_nature_id,a.construction,a.gsm_weight,a.color_range_id,a.stich_length,a.process_loss,b.copmposition_id,b.percent,b.count_id,b.type_id,a.id from  lib_yarn_count_determina_mst a,  lib_yarn_count_determina_dtls b where a.id=b.mst_id and  a.is_deleted=0 order by b.id";
						$data_array=sql_select($sql);
						if (count($data_array)>0)
					    {
							foreach( $data_array as $row )
							{
								if(array_key_exists($row[csf('id')],$composition_arr))
								{
									$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ".$lib_yarn_count[$row[csf('count_id')]]." ".$yarn_type[$row[csf('type_id')]].",";
								}
								else
								{
									$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ".$lib_yarn_count[$row[csf('count_id')]]." ".$yarn_type[$row[csf('type_id')]].",";
								}
							}
						}
						//print_r($composition_arr);
												
												$sql="select a.id,a.fab_nature_id,a.construction,a.gsm_weight,a.color_range_id,a.stich_length,a.process_loss from  lib_yarn_count_determina_mst a,  lib_yarn_count_determina_dtls b where a.id=b.mst_id and  a.is_deleted=0 group by a.id,a.fab_nature_id,a.construction,a.gsm_weight,a.color_range_id,a.stich_length,a.process_loss order by a.id";
												
												//echo $sql="select a.id from  lib_yarn_count_determina_mst a,  lib_yarn_count_determina_dtls b where a.id=b.mst_id and  a.is_deleted=0 group by a.id order by a.id";
												
												

						$arr=array (0=>$item_category, 3=>$color_range,6=>$composition_arr,8=>$lib_yarn_count,9=>$yarn_type);

						echo  create_list_view ( "list_view", "Fab Nature,Construction,GSM/Weight,Color Range,Stich Length,Process Loss,Composition", "100,100,100,100,90,50,300","950","350",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form'",1, "fab_nature_id,0,0,color_range_id,0,0,id", $arr , "fab_nature_id,construction,gsm_weight,color_range_id,stich_length,process_loss,id", "../merchandising_details/requires/yarn_count_determination_controller",'setFilterGrid("list_view",-1);','0,0,1,0,1,1,0') ;
						?>
                    </div>
                    <?
			/*$field_array1= "id,fab_nature_id, construction,gsm_weight,color_range_id,stich_length,process_loss,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted";
			
			
			$field_array2= "id,mst_id,copmposition_id,percent,count_id,type_id,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted";
		 
                    $sql="select id,fab_nature_id,construction,copm_one_id,percent_one,copm_two_id, percent_two,gsm_weight,color_range_id,cotton_count_id,cotton_type_id,denier_count_id,denier_type_id,stich_length,status_active,is_deleted,inserted_by,insert_date,updated_by,update_date from  lib_yarn_count_determination";
					$data_array=sql_select($sql);
					foreach ($data_array as $row)
					{
					 $data_array1="(".$row['id'].",".$row['fab_nature_id'].",'".$row['construction']."',".$row['gsm_weight'].",".$row['color_range_id'].",'".$row['stich_length']."',0,".$row['inserted_by'].",'".$row['insert_date']."',".$row['updated_by'].",'".$row['update_date']."',".$row['status_active'].",".$row['is_deleted'].")";
					 $rID=sql_insert("lib_yarn_count_determina_mst",$field_array1,$data_array1,1);
					 $id_dtls=return_next_id( "id", "lib_yarn_count_determina_dtls", 1 ) ;
					 $data_array2 ="(".$id_dtls.",".$row['id'].",".$row['copm_one_id'].",".$row['percent_one'].",".$row['cotton_count_id'].",".$row['cotton_type_id'].",".$row['inserted_by'].",'".$row['insert_date']."',".$row['updated_by'].",'".$row['update_date']."',".$row['status_active'].",".$row['is_deleted'].")";
					 
			$rID_1=sql_insert("lib_yarn_count_determina_dtls",$field_array2,$data_array2,1);
			
					 $id_dtls1=return_next_id( "id", "lib_yarn_count_determina_dtls", 1 ) ;
					 $data_array3 ="(".$id_dtls1.",".$row['id'].",".$row['copm_two_id'].",".$row['percent_two'].",".$row['denier_count_id'].",".$row['denier_type_id'].",".$row['inserted_by'].",'".$row['insert_date']."',".$row['updated_by'].",'".$row['update_date']."',".$row['status_active'].",".$row['is_deleted'].")";
					 
			$rID_1=sql_insert("lib_yarn_count_determina_dtls",$field_array2,$data_array3,1);


						
					}*/
					?>
        
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
