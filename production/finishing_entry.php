<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create garments finish entry
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	27-02-2013
Updated by 		: 	Kausar (Creating Print Report )	
Update date		: 	09-01-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;


//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Packing And Finishing Entry","../", 1, 1, $unicode,'','');

?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";

 
function openmypage(page_link,title)
{
	if ( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	else
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1040px,height=370px,center=1,resize=0,scrolling=0','')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var po_id=this.contentDoc.getElementById("hidden_mst_id").value;//po id
			var item_id=this.contentDoc.getElementById("hidden_grmtItem_id").value;
			var po_qnty=this.contentDoc.getElementById("hidden_po_qnty").value;	
			var country_id=this.contentDoc.getElementById("hidden_country_id").value;
				
			if (po_id!="")
			{
				//freeze_window(5);
				$("#txt_order_qty").val(po_qnty);
				$("#cbo_item_name").val(item_id);
				$("#cbo_country_name").val(country_id);
				
				childFormReset();//child from reset
				get_php_form_data(po_id+'**'+item_id+'**'+country_id, "populate_data_from_search_popup", "requires/finishing_entry_controller" );
 				
				var variableSettings=$('#sewing_production_variable').val();
				var styleOrOrderWisw=$('#styleOrOrderWisw').val();
				var variableSettingsReject=$('#finish_production_variable_rej').val();
				if(variableSettings!=1){ 
					get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id+'**'+variableSettingsReject, "color_and_size_level", "requires/finishing_entry_controller");
				}
				else
				{
					$("#txt_finishing_qty").removeAttr("readonly");
				}
				
				if(variableSettingsReject!=1)
				{
					$("#txt_reject_qnty").attr("readonly");
				}
				else
				{
					$("#txt_reject_qnty").removeAttr("readonly");
				}
			
				show_list_view(po_id+'**'+item_id+'**'+country_id,'show_dtls_listview','list_view_container','requires/finishing_entry_controller','');
				show_list_view(po_id,'show_country_listview','list_view_country','requires/finishing_entry_controller','');
				set_button_status(0, permission, 'fnc_finishing_entry',1,0);
				release_freezing();
			}
		}
	}//end else
}//end function



function fnc_finishing_entry(operation)
{
	if(operation==4)
	{
		 var report_title=$( "div.form_caption" ).html();
		 print_report( $('#cbo_company_name').val()+'*'+$('#txt_mst_id').val()+'*'+report_title, "finishing_entry_print", "requires/finishing_entry_controller" ) 
		 return;
	}
	else if(operation==0 || operation==1 || operation==2)
	{
		if ( form_validation('cbo_company_name*txt_order_no*cbo_finish_company*txt_finishing_date*txt_finishing_qty','Company Name*Order No*Finishing Company*Finishing Date*Finishing Quantity')==false )
		{
			return;
		}		
		else
		{
			//freeze_window(operation);			
			var sewing_production_variable = $("#sewing_production_variable").val();
			var colorList = ($('#hidden_colorSizeID').val()).split(",");
			var variableSettingsReject=$('#finish_production_variable_rej').val();
			
			var i=0; var k=0; var colorIDvalue=''; var colorIDvalueRej='';
			if(sewing_production_variable==2)//color level
			{
 				$("input[name=txt_color]").each(function(index, element) {
 					if( $(this).val()!='' )
					{
						if(i==0)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val();
						}
						else
						{
							colorIDvalue += "**"+colorList[i]+"*"+$(this).val();
						}
					}
					i++;
				});
			}
			else if(sewing_production_variable==3)//color and size level
			{
 				$("input[name=colorSize]").each(function(index, element) {
					if( $(this).val()!='' )
					{
						if(i==0)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val();
						}
						else
						{
							colorIDvalue += "***"+colorList[i]+"*"+$(this).val();
						}
					}
 					i++;
				});
			}
			
			if(variableSettingsReject==2)//color level
			{
				$("input[name=txtColSizeRej]").each(function(index, element) {
 					if( $(this).val()!='' )
					{
						if(k==0)
						{
							colorIDvalueRej = colorList[k]+"*"+$(this).val();
						}
						else
						{
							colorIDvalueRej += "**"+colorList[k]+"*"+$(this).val();
						}
					}
					k++;
				});
				//alert (colorIDvalueRej);return;
			}
			else if(variableSettingsReject==3)//color and size level
			{
				$("input[name=colorSizeRej]").each(function(index, element) {
					if( $(this).val()!='' )
					{
						if(k==0)
						{
							colorIDvalueRej = colorList[k]+"*"+$(this).val();
						}
						else
						{
							colorIDvalueRej += "***"+colorList[k]+"*"+$(this).val();
						}
					}
 					k++;
				});
			}
			 
			var data="action=save_update_delete&operation="+operation+"&colorIDvalue="+colorIDvalue+"&colorIDvalueRej="+colorIDvalueRej+get_submitted_data_string('garments_nature*cbo_company_name*cbo_country_name*sewing_production_variable*finish_production_variable_rej*hidden_po_break_down_id*hidden_colorSizeID*cbo_buyer_name*txt_style_no*cbo_item_name*txt_order_qty*cbo_source*cbo_finish_company*cbo_location*cbo_floor*txt_finishing_date*txt_reporting_hour*txt_finishing_qty*txt_carton_qty*txt_alter_qnty*txt_reject_qnty*txt_challan*txt_remark*txt_finish_input_qty*txt_cumul_finish_qty*txt_yet_to_finish*hidden_break_down_html*txt_mst_id',"../");
 			
 			http.open("POST","requires/finishing_entry_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_finishing_entry_Reply_info;
		}
	}
}
  
function fnc_finishing_entry_Reply_info()
{
 	if(http.readyState == 4) 
	{
		// alert(http.responseText);
		var variableSettings=$('#sewing_production_variable').val();
		var styleOrOrderWisw=$('#styleOrOrderWisw').val();
		var variableSettingsReject=$('#finish_production_variable_rej').val();
		var item_id=$('#cbo_item_name').val();
		var country_id = $("#cbo_country_name").val();
		
		var reponse=http.responseText.split('**');		 
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_fabric_cost_dtls('+ reponse[1]+')',8000); 
		}
		if(reponse[0]==0)
		{ 
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
 			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id,'show_dtls_listview','list_view_container','requires/finishing_entry_controller','');
			reset_form('','','txt_reporting_hour*txt_finishing_qty*txt_carton_qty*txt_alter_qnty*txt_reject_qnty*txt_challan*txt_remark*txt_finish_input_qty*txt_cumul_finish_qty*txt_yet_to_finish*hidden_break_down_html*txt_mst_id','txt_finishing_date,<?php echo date("d-m-Y"); ?>','');
			get_php_form_data(po_id+'**'+$('#cbo_item_name').val()+'**'+country_id, "populate_data_from_search_popup", "requires/finishing_entry_controller" );
 			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id+'**'+variableSettingsReject, "color_and_size_level", "requires/finishing_entry_controller" ); 
			}
			else
			{
				$("#txt_finishing_qty").removeAttr("readonly");
			}
			
			if(variableSettingsReject!=1)
			{
				$("#txt_reject_qnty").attr("readonly");
			}
			else
			{
				$("#txt_reject_qnty").removeAttr("readonly");
			}
				
			release_freezing();
		}
		else if(reponse[0]==1)
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id,'show_dtls_listview','list_view_container','requires/finishing_entry_controller','');
			reset_form('','','txt_reporting_hour*txt_finishing_qty*txt_carton_qty*txt_alter_qnty*txt_reject_qnty*txt_challan*txt_remark*txt_finish_input_qty*txt_cumul_finish_qty*txt_yet_to_finish*hidden_break_down_html*txt_mst_id','txt_finishing_date,<?php echo date("d-m-Y"); ?>','');
			get_php_form_data(po_id+'**'+$('#cbo_item_name').val()+'**'+country_id, "populate_data_from_search_popup", "requires/finishing_entry_controller" );
			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id+'**'+variableSettingsReject, "color_and_size_level", "requires/finishing_entry_controller" ); 
			}
			else
			{
				$("#txt_finishing_qty").removeAttr("readonly");
			}
			
			if(variableSettingsReject!=1)
			{
				$("#txt_reject_qnty").attr("readonly");
			}
			else
			{
				$("#txt_reject_qnty").removeAttr("readonly");
			}

			set_button_status(0, permission, 'fnc_finishing_entry',1,0);
			release_freezing();
		}
		else if(reponse[0]==2)
		{
			var po_id = reponse[1];
			show_msg(trim(reponse[0]));
			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id,'show_dtls_listview','list_view_container','requires/finishing_entry_controller','');
			reset_form('','','txt_reporting_hour*txt_finishing_qty*txt_carton_qty*txt_alter_qnty*txt_reject_qnty*txt_challan*txt_remark*txt_finish_input_qty*txt_cumul_finish_qty*txt_yet_to_finish*hidden_break_down_html*txt_mst_id','txt_finishing_date,<?php echo date("d-m-Y"); ?>','');
			get_php_form_data(po_id+'**'+$('#cbo_item_name').val()+'**'+country_id, "populate_data_from_search_popup", "requires/finishing_entry_controller" );
			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id+'**'+variableSettingsReject, "color_and_size_level", "requires/finishing_entry_controller" ); 
			}
			else
			{
				$("#txt_finishing_qty").removeAttr("readonly");
			}
			
			if(variableSettingsReject!=1)
			{
				$("#txt_reject_qnty").attr("readonly");
			}
			else
			{
				$("#txt_reject_qnty").removeAttr("readonly");
			}
			set_button_status(0, permission, 'fnc_finishing_entry',1,0);
			release_freezing();
		}
 	}
} 

function childFormReset()
{
	//txt_finishing_date  txt_reporting_hour cbo_time txt_finishing_qty txt_carton_qty txt_alter_qnty txt_reject_qnty txt_remark txt_finish_input_qty txt_cumul_finish_qty txt_yet_to_finish
	reset_form('','','txt_reporting_hour*txt_finishing_qty*txt_carton_qty*txt_alter_qnty*txt_reject_qnty*txt_challan*txt_remark*txt_finish_input_qty*txt_cumul_finish_qty*txt_yet_to_finish*hidden_break_down_html*txt_mst_id','','');
 	$('#txt_cumul_finish_qty').attr('placeholder','');//placeholder value initilize
	$('#txt_yet_to_finish').attr('placeholder','');//placeholder value initilize
	$('#list_view_container').html('');//listview container
	$("#breakdown_td_id").html('');

}  

function fn_hour_check(val)
{
	if(val*1>12)
	{
		alert("You Cross 12!!This is 12 Hours.");
		$("#txt_reporting_hour").val('');
	}
}

function fn_total(tableName,index) // for color and size level
{
    var filed_value = $("#colSize_"+tableName+index).val();
	var placeholder_value = $("#colSize_"+tableName+index).attr('placeholder');
	if(filed_value*1 > placeholder_value*1)
	{
		if( confirm("Qnty Excceded by"+(placeholder_value-filed_value)) )	
			void(0);
		else
		{
			$("#colSize_"+tableName+index).val('');
 		}
	}
	
	var totalRow = $("#table_"+tableName+" tr").length;
	//alert(tableName);
	math_operation( "total_"+tableName, "colSize_"+tableName, "+", totalRow);
	if($("#total_"+tableName).val()*1!=0)
	{
		$("#total_"+tableName).html($("#total_"+tableName).val());
	}
	var totalVal = 0;
	$("input[name=colorSize]").each(function(index, element) {
        totalVal += ( $(this).val() )*1;
    });
	$("#txt_finishing_qty").val(totalVal);
}

function fn_total_rej(tableName,index) // for color and size level
{
    var filed_value = $("#colSizeRej_"+tableName+index).val();
	
	
	var totalRow = $("#table_"+tableName+" tr").length;
	//alert(tableName);
	math_operation( "total_"+tableName, "colSizeRej_"+tableName, "+", totalRow);
	
	var totalValRej = 0;
	$("input[name=colorSizeRej]").each(function(index, element) {
        totalValRej += ( $(this).val() )*1;
    });
	$("#txt_reject_qnty").val(totalValRej);
}

function fn_colorlevel_total(index) //for color level
{
	
	var filed_value = $("#colSize_"+index).val();
	var placeholder_value = $("#colSize_"+index).attr('placeholder');
	if(filed_value*1 > placeholder_value*1)
	{
		if( confirm("Qnty Excceded by"+(placeholder_value-filed_value)) )	
			void(0);
		else
		{
			$("#colSize_"+index).val('');
 		}
	}
	
    var totalRow = $("#table_color tbody tr").length;
	//alert(totalRow);
	math_operation( "total_color", "colSize_", "+", totalRow);
	$("#txt_finishing_qty").val( $("#total_color").val() );
} 

function fn_colorRej_total(index) //for color level
{
	var filed_value = $("#colSizeRej_"+index).val();
    var totalRow = $("#table_color tbody tr").length;
	//alert(totalRow);
	math_operation( "total_color_rej", "colSizeRej_", "+", totalRow);
	$("#txt_reject_qnty").val( $("#total_color_rej").val() );
}

function put_country_data(po_id, item_id, country_id, po_qnty, plan_qnty)
{
	freeze_window(5);
	
	$("#cbo_item_name").val(item_id);
	$("#txt_order_qty").val(po_qnty);
	$("#cbo_country_name").val(country_id);
 				
	childFormReset();//child from reset
	get_php_form_data(po_id+'**'+item_id+'**'+country_id, "populate_data_from_search_popup", "requires/finishing_entry_controller" );
	
	var variableSettings=$('#sewing_production_variable').val();
	var styleOrOrderWisw=$('#styleOrOrderWisw').val();
	var variableSettingsReject=$('#finish_production_variable_rej').val();
	if(variableSettings!=1)
	{ 
		get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id+'**'+variableSettingsReject, "color_and_size_level", "requires/finishing_entry_controller");
	}
	else
	{
		$("#txt_finishing_qty").removeAttr("readonly");
	}
	
	if(variableSettingsReject!=1)
	{
		$("#txt_reject_qnty").attr("readonly");
	}
	else
	{
		$("#txt_reject_qnty").removeAttr("readonly");
	}
			
	show_list_view(po_id+'**'+item_id+'**'+country_id,'show_dtls_listview','list_view_container','requires/finishing_entry_controller','');
	set_button_status(0, permission, 'fnc_finishing_entry',1,0);
	release_freezing();
}

function fnc_valid_time(val,field_id)
{
	var val_length=val.length;
	if(val_length==2)
	{
		document.getElementById(field_id).value=val+":";
	}
	
	var colon_contains=val.contains(":");
	if(colon_contains==false)
	{
		if(val>23)
		{
			document.getElementById(field_id).value='23:';
		}
	}
	else
	{
		var data=val.split(":");
		var minutes=data[1];
		var str_length=minutes.length;
		var hour=data[0]*1;
		
		if(hour>23)
		{
			hour=23;
		}
		
		if(str_length>=2)
		{
			minutes= minutes.substr(0, 2);
			if(minutes*1>59)
			{
				minutes=59;
			}
		}
		
		var valid_time=hour+":"+minutes;
		document.getElementById(field_id).value=valid_time;
	}
}

function numOnly(myfield, e, field_id)
{
	var key;
	var keychar;
	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);

	// control keys
	if ((key==null) || (key==0) || (key==8) || (key==9) || (key==13) || (key==27) )
	return true;
	// numbers
	else if ((("0123456789:").indexOf(keychar) > -1))
	{
		var dotposl=document.getElementById(field_id).value.lastIndexOf(":");
		if(keychar==":" && dotposl!=-1)
		{
			return false;
		}
		return true;
	}
	else
		return false;
}
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;">
	<?php echo load_freeze_divs ("../",$permission);  ?>
 	<div style="width:930px; float:left" align="center">
        <fieldset style="width:930px;">
        <legend>Production Module</legend>  
            <form name="finishingentry_1" id="finishingentry_1" autocomplete="off" >
 				<fieldset>
                    <table width="100%" border="0">
                        <tr>
                             <td width="100" class="must_entry_caption">Company</td>
                             <td>                                
								 <?php 
                                 echo create_drop_down( "cbo_company_name", 170, "select id,company_name from lib_company where status_active =1 and is_deleted=0 order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/finishing_entry_controller', this.value, 'load_drop_down_location', 'location_td' );get_php_form_data(this.value,'load_variable_settings','requires/finishing_entry_controller');" );
                                 ?>	 
                                 <input type="hidden" id="sewing_production_variable" />	 
                                 <input type="hidden" id="styleOrOrderWisw" /> 
                                 <input type="hidden" id="finish_production_variable_rej" />
							 </td>
                             <td width="100" class="must_entry_caption">Order No</td>
                             <td width="175">
							 <input name="txt_order_no" placeholder="Double Click to Search" id="txt_order_no" onDblClick="openmypage('requires/finishing_entry_controller.php?action=order_popup&company='+document.getElementById('cbo_company_name').value+'&garments_nature='+document.getElementById('garments_nature').value,'Order Search')"  class="text_boxes" style="width:155px " readonly />
                             <input type="hidden" id="hidden_po_break_down_id" value="" />
							 </td>
                             <td width="130" >Country</td>
                             <td width="170">
                                <?php
                                    echo create_drop_down( "cbo_country_name", 170, "select id,country_name from lib_country","id,country_name", 1, "-- Select Country --", $selected, "",1 );
                                ?> 
                             </td>
                        </tr>
                        <tr>    
                             <td width="">Buyer</td>
                              <td width="170">
								  <?php
                                  echo create_drop_down( "cbo_buyer_name", 170, "select id,buyer_name from lib_buyer where is_deleted=0 and status_active=1 order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",1,0 );
                                  ?>
							 </td>
                             <td width="120">Job No</td>
                             <td width="170">
                             	<input name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:155px " disabled readonly />	
							 </td>
                             <td width="">Style</td>
                             <td width="">
							 	<input name="txt_style_no" id="txt_style_no" class="text_boxes"  style="width:158px " disabled readonly />
							 </td>
                        </tr>
                        <tr>  
                        	 <td width=""> Item </td>
                             <td width="170">
								<?php
                                 echo create_drop_down( "cbo_item_name", 170, $garments_item,"", 1, "-- Select Item --", $selected, "",1,0 );	
                                 ?>
							 </td>  
                             <td width="">Order Qnty</td>
                             <td width="170">
							 <input name="txt_order_qty" id="txt_order_qty" class="text_boxes_numeric" style="width:155px "  disabled readonly>
							 </td>
                             <td width="">Source</td>
                             <td width="">
								 <?php
                                 echo create_drop_down( "cbo_source", 170, $knitting_source,"", 1, "-- Select Source --", $selected, "load_drop_down( 'requires/finishing_entry_controller', this.value+'**'+$('#cbo_company_name').val(), 'load_drop_down_source', 'finishing_td' );", 0, '1,3' );
                                 ?>
                             </td>
                        </tr>
                        <tr>
                         	 <td width="" class="must_entry_caption">Finish. Company</td>
                             <td id="finishing_td" width="170">
                             <?php
                             echo create_drop_down( "cbo_finish_company", 170, $blank_array,"", 1, "-- Select finishing Company --", $selected, "" );
							 ?>
						     </td>
                             <td width="">Location</td>
                             <td width="170" id="location_td">
								 <?php
                                 echo create_drop_down( "cbo_location", 167, $blank_array, "", 1, "-- Select Location --", $selected, "" );
                                 ?>
							 </td>
                             <td width="">Floor</td>
                              <td width="" id="floor_td">
                              <?php 
                              echo create_drop_down( "cbo_floor", 170, $blank_array, "", 1, "-- Select Floor --", $selected, "" );
							  ?>
                              </td>
                        </tr>
                    </table>
                </fieldset>
                <br />                 
                <table cellpadding="0" cellspacing="1" width="100%">
                    <tr>
                    	<td width="35%" valign="top">
                           <fieldset>
                            <legend>New Entry</legend>
                                <table  cellpadding="0" cellspacing="2" width="100%">
                                    <tr>
                                    	<td width="90" class="must_entry_caption">Finishing Date</td>
                                         <td width="160"> 
                                         <input name="txt_finishing_date" id="txt_finishing_date" class="datepicker" type="text" value="<?php echo date("d-m-Y")?>" style="width:100px;"  />
                                        </td>
                                   </tr>
                                   <tr>
                                         <td width="">Reporting Hour</td>
                                         <td width="">  
                                           <input name="txt_reporting_hour" id="txt_reporting_hour" class="text_boxes" style="width:100px" placeholder="24 Hour Format" onBlur="fnc_valid_time(this.value,'txt_reporting_hour');" onKeyUp="fnc_valid_time(this.value,'txt_reporting_hour');" onKeyPress="return numOnly(this,event,this.id);" maxlength="8" />
                                        </td>
                                     </tr>
                                     <tr>
                                         <td width="" class="must_entry_caption">Finishing Qty</td>
                                         <td width=""><input name="txt_finishing_qty" id="txt_finishing_qty" class="text_boxes_numeric"  style="width:100px; text-align:right" readonly />
                                         <input type="hidden" id="hidden_break_down_html"  value="" readonly disabled />
                                         <input type="hidden" id="hidden_colorSizeID"  value="" readonly disabled />
                                         </td>
                                     </tr>
                                     <tr>
                                       <td>Total Carton Qty</td>
                                       <td><input type="text" name="txt_carton_qty" id="txt_carton_qty" class="text_boxes_numeric"   style="width:100px; text-align:right" /></td>
                                     </tr>
                                     <tr>
                                       <td>Alter Qnty</td>
                                       <td><input type="text" name="txt_alter_qnty" id="txt_alter_qnty" class="text_boxes_numeric" style="width:100px; text-align:right" /></td>
                                     </tr>
                                     <tr>
                                       <td>Reject Qnty</td>
                                       <td><input type="text" name="txt_reject_qnty" id="txt_reject_qnty" class="text_boxes_numeric" style="width:100px; text-align:right" /></td>
                                     </tr>
                                    <tr>
                                         <td width="">Challan No</td> 
                                         <td>
                                           <input type="text" name="txt_challan" id="txt_challan" class="text_boxes" style="width:100px" />
                                         </td>
                                    </tr>
                                     <tr>
                                          <td width="">Remarks</td> 
                                          <td width="" colspan="4"> 
                                          <input type="text" name="txt_remark" id="txt_remark" class="text_boxes" style="width:170px" />
                                          </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td width="1%" valign="top">
                        </td>
                         <td width="25%" valign="top">
                            <fieldset>
                            <legend>Display</legend>
                                <table  cellpadding="0" cellspacing="2" width="100%" >
                                	<tr>
                                  		<td width="120"> Iron Qty</td> 
                                        <td>
                                        	<input type="text" name="txt_finish_input_qty" id="txt_finish_input_qty" class="text_boxes_numeric" style="width:80px" readonly disabled  />
                                        </td>
                                   </tr>
                                   <tr>
                                        <td width="">Cumul. Finish Qty</td>
                                        <td>
                                        	<input type="text" name="txt_cumul_finish_qty" id="txt_cumul_finish_qty" class="text_boxes_numeric" style="width:80px"  readonly disabled />
                                        </td>
                                   </tr>
                                   <tr>
                                        <td width="">Yet to Finish</td>
                                        <td>
                                       	 	<input type="text" name="txt_yet_to_finish" id="txt_yet_to_finish" class="text_boxes_numeric" style="width:80px" readonly disabled />
                                        </td>
                                   </tr>
                                </table>
                            </fieldset>	
                        </td>
                        <td width="43%" valign="top" >
                            <div style="max-height:300px; overflow-y:scroll" id="breakdown_td_id" align="center"></div>
                        </td>
                    </tr>
                     <tr>
		   				<td align="center" colspan="9" valign="middle" class="button_container">
           				<?php
						$date=date('d-m-Y');
	       				echo load_submit_buttons( $permission, "fnc_finishing_entry", 0,1 ,"reset_form('finishingentry_1','list_view_country','','txt_finishing_date,".$date."','childFormReset()')",1); 
		   				?>
                        <input type="hidden" name="txt_mst_id" id="txt_mst_id" readonly />
           				</td>
           				<td>&nbsp;</td>					
		  			</tr>
                </table>
                <div style="width:930px; margin-top:5px;"  id="list_view_container" align="center"></div>
            </form>
        </fieldset>
	</div>
	<div id="list_view_country" style="width:370px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative; margin-left:10px"></div>        
</div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>