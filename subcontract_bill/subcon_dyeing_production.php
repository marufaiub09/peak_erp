<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Dyeing Production Entry

Functionality	:	
JS Functions	:
Created by		:	Kausar
Creation date 	: 	24-05-2014
Updated by 		: 		
Update date		: 
Oracle Convert 	:	Kausar		
Convert date	: 	24-05-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/ 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Dyeing Production Entry Info","../", 1, 1, "",'1','');
?>

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
	
	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];
	$(document).ready(function(e)
	 {
            $("#txt_color").autocomplete({
			 source: str_color
		  });
     });

	function openmypage_batchnum()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var batch_no = $('#txt_batch_no').val();

		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{
			var page_link='requires/subcon_dyeing_production_controller.php?cbo_company_id='+cbo_company_id+'&action=batch_number_popup';
			var title='Batch Number Popup';

			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=820px,height=390px,center=1,resize=1,scrolling=0','');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var batch_id=this.contentDoc.getElementById("hidden_batch_id").value;
				//alert( batch_id)
				if(batch_id!="")
				{
					freeze_window(5);
					get_php_form_data(document.getElementById('cbo_load_unload').value+'_'+batch_id, "populate_data_from_batch", "requires/subcon_dyeing_production_controller" );
					show_list_view(batch_id,'show_fabric_desc_listview','list_fabric_desc_container','requires/subcon_dyeing_production_controller','');
					
					release_freezing();
				}
				get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/subcon_dyeing_production_controller' );
			}
		}
	}
	
	function check_batch()
	{
		var batch_no=$('#txt_batch_no').val();
		var cbo_company_id = $('#cbo_company_id').val();
		if(batch_no!="")
		{
			if (form_validation('cbo_company_id','Company')==false)
			{
				return;
			}
			
			var response=return_global_ajax_value( cbo_company_id+"**"+batch_no, 'check_batch_no', '', 'requires/subcon_dyeing_production_controller');
		
			var response=response.split("_");
			//alert(response[0]);return;
			if(response[0]==0)
			{
				alert('Batch no not found.');
				
				$('#txt_batch_no').val('');
				$('#hidden_batch_id').val(''); 
				//$('#cbo_company_id').val(''); 
				$('#txt_update_id').val(''); 
				$('#cbo_sub_process').val('');
				$('#txt_process_end_date').val('');
				$('#txt_end_hours').val('');
				$('#txt_end_minutes').val('');
				$('#cbo_machine_name').val('');
				$('#txt_remarks').val('');
				reset_form('dyeingproduction_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
			}
			else
			{
				$('#hidden_batch_id').val(response[1]);
				get_php_form_data(document.getElementById('cbo_load_unload').value+'_'+response[1]+'_'+batch_no, "populate_data_from_batch", "requires/subcon_dyeing_production_controller" );
				show_list_view(response[1],'show_fabric_desc_listview','list_fabric_desc_container','requires/subcon_dyeing_production_controller','');
				get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/subcon_dyeing_production_controller' );
			}
		}
	}
	
	function fnc_pro_fab_subprocess( operation )
	{	
		if( form_validation('cbo_load_unload','Load Unload')==false )
		{
			return;
		} 
		
		if (document.getElementById('cbo_load_unload').value==1)
		{
			if( form_validation('cbo_load_unload*cbo_company_id*txt_batch_no*cbo_ltb_btb*txt_process_start_date*txt_start_hours*txt_start_minutes*cbo_floor*cbo_machine_name','Load Unload*Company*Batch No*LTB/BTB*Process Date*Hours*Minutes*Floor*Machine')==false )
			{
				return;
			} 
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_load_unload*txt_process_start_date*txt_start_hours*txt_start_minutes*cbo_company_id*txt_batch_no*hidden_batch_id*cbo_sub_process*cbo_machine_name*txt_remarks*txt_ext_id*txt_update_id*cbo_floor*cbo_ltb_btb*txt_water_flow',"../");
			//alert (data);
			freeze_window(operation);
			http.open("POST","requires/subcon_dyeing_production_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_pro_fab_subprocess_response;
		}
		
		if (document.getElementById('cbo_load_unload').value==2)//uploading here
		{
			if( form_validation('cbo_load_unload*cbo_company_id*txt_batch_no*cbo_ltb_btb*txt_process_end_date*txt_process_date*txt_end_hours*txt_end_minutes*cbo_floor*cbo_machine_name*cbo_result_name','Load Unload*Company*Batch No*LTB/BTB*Production Date*Process Date* Hours*Minutes*Floor*Machine*Result')==false )
			{
				return;
			} 
			var batch_no=$('#txt_batch_no').val();
			var cbo_company_id = $('#cbo_company_id').val();
			var batch_id = $('#hidden_batch_id').val();
			var response=return_global_ajax_value( cbo_company_id+"**"+batch_no+"**"+batch_id, 'check_batch_no_load', '', 'requires/subcon_dyeing_production_controller');
			var response=response.split("_");
			if(response[0]==0)
			{
				alert('Without Load  Unload Not Allow ');
			}
			else
			{
				var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_load_unload*txt_process_end_date*txt_process_date*txt_end_hours*txt_end_minutes*cbo_company_id*txt_batch_no*hidden_batch_id*cbo_sub_process*cbo_machine_name*cbo_result_name*txt_remarks*txt_ext_id*txt_update_id*cbo_floor*cbo_ltb_btb*txt_water_flow',"../");
				//alert (data);
				freeze_window(operation);
				http.open("POST","requires/subcon_dyeing_production_controller.php",true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_pro_fab_subprocess_response;
			}
		}
	}
		
	function fnc_pro_fab_subprocess_response()
	{
		if(http.readyState == 4) 
		{
			//alert (http.responseText);
			var reponse=trim(http.responseText).split('**');
			show_msg(reponse[0]);
			//document.getElementById('txt_batch_no').value = reponse[1];
			document.getElementById('txt_update_id').value = reponse[1];
			reset_form('dyeingproduction_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
			set_button_status(0, permission, 'fnc_pro_fab_subprocess',1,1);
			release_freezing();
		}
	}
	
	function load_list_view (str)
	{
		show_list_view(str,'on_change_data','load_unload_container','requires/subcon_dyeing_production_controller','');
		set_multiselect('cbo_sub_process','0','0','','0');
		//alert("MMMM")
		set_all_onclick();
		set_button_status(0, permission, 'fnc_pro_fab_subprocess',1);
	}
	
	function fnResetForm()
	{
		reset_form('dyeingproduction_1','load_unload_container','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
	}

	function fnc_move_cursor(val,id, field_id,lnth,max_val)
	{
		var str_length=val.length;
		if(str_length==lnth)
		{
			$('#'+field_id).select();
			$('#'+field_id).focus();
		}
		
		if(val>max_val)
		{
			document.getElementById(id).value=max_val;
		}
	}
	
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
<?php echo load_freeze_divs ("../",$permission); ?>
    <form name="dyeingproduction_1" id="dyeingproduction_1" autocomplete="off" >
    <div style="width:1300px; float:left;">   
        <fieldset style="width:1250px;">
        <table cellpadding="0" cellspacing="1" width="1250" border="0" align="left" height="auto" id="master_tbl">
            <tr>
                <td width="29%" valign="top">
                <fieldset>
                <legend>Input Area</legend>  
                    <table width="150px" cellpadding="0" cellspacing="2" align="right"  >
                        <tr>
                            <td align="center" width="120" class="must_entry_caption"><b>Load/Un-load</b></td>
                            <td  style="float:left" width="120">
                            <?php   
								echo create_drop_down( "cbo_load_unload", 120, $loading_unloading,'', '1', '---- Select ----', '',"load_list_view(this.value)",'','','','','',1);
                            ?>                
                            </td>
                        </tr>
                    </table>
                    <div style="width:auto; float:left; min-height:40px; margin:auto" align="center" id="load_unload_container">
                    </div> 
                </fieldset>
                </td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="70%" valign="top">
                    <table cellpadding="0" cellspacing="1" width="100%" border="0" align="left">
                        <tr>
                            <td colspan="3"> <center> <legend>Reference Display</legend></center> </td>
                        </tr>
                        <tr>
                            <td width="45%" valign="top">
                            <fieldset style="height:auto;">
                                <table width="400" align="left" id="tbl_body1" >
                                    <tr>
                                        <td width="70">Batch ID</td>
                                        <td width="110">
                                            <input type="text" name="txt_batch_ID" id="txt_batch_ID" class="text_boxes" style="width:100px;"  readonly />
                                        </td>
                                        <td width="90">Loading Date</td>
                                        <td width="110">
                                            <input type="text" name="txt_dying_started" id="txt_dying_started" class="text_boxes" style="width:100px;" readonly  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ext. No.</td>
                                        <td>
                                            <input type="text" name="txt_ext_id" id="txt_ext_id" class="text_boxes" style="width:100px;" readonly />
                                        </td>
                                        <td>Loading Time</td>
                                        <td>
                                            <input type="text" name="txt_dying_end_load" id="txt_dying_end_load" class="text_boxes" style="width:100px;" readonly  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Job No</td>
                                        <td>
                                            <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:100px;"  readonly />
                                        </td>
                                        <td >M/C Floor</td>
                                        <td id="machine_fg_td">
                                            <input type="text" name="txt_machine_no" id="txt_machine_no" class="text_boxes" style="width:100px;" readonly />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >Buyer</td>
                                        <td>
                                            <input type="text" name="txt_buyer" id="txt_buyer" class="text_boxes" style="width:100px;"  readonly />
                                        </td>
                                        <td >M/C Group</td>
                                        <td id="">
                                            <input type="text" name="txt_mc_group" id="txt_mc_group" class="text_boxes" style="width:100px;" value="<?php echo $data;?>" readonly />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Order No.</td>
                                        <td>
                                            <input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:100px;" readonly />
                                        </td>
                                        <td>Color</td>
                                        <td>
                                            <input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:100px;"  readonly />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>BTB/LTB</td>
                                        <td>
                                            <input type="text" name="txt_ltb_btb" id="txt_ltb_btb" class="text_boxes" style="width:100px;" readonly />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            </td>
                            <td width="1%" valign="top">&nbsp;</td>
                            <td width="54%" valign="top">
                            <fieldset>
                                <table width="513" align="right" class="rpt_table" rules="all">
                                    <thead>
                                        <th>Const/Composition</th> 
                                        <th>GSM</th>
                                        <th>Dia/Width</th> 
                                        <th>D/W Type</th>
                                        <th>Batch Qty</th>
                                    </thead>
                                    <tbody id="list_fabric_desc_container">
                                        <tr class="general" id="row_1">
                                            <td><input type="text" name="txt_cons_comp_1" id="txt_cons_comp_1" class="text_boxes" style="width:170px;" readonly disabled /></td>
                                            <td><input type="text" name="txt_gsm_1" id="txt_gsm_1" class="text_boxes" style="width:60px;"readonly disabled /> </td>
                                            <td><input type="text" name="txt_body_part_1" id="txt_body_part_1" class="text_boxes" style="width:110px;" readonly  disabled/></td>
                                            <td><input type="text" name="txt_dia_width_1" id="txt_dia_width_1" class="text_boxes" style="width:70px;" readonly disabled/></td>
                                            <td><input type="text" name="txt_batch_qnty_1" id="txt_batch_qnty_1" class="text_boxes" style="width:80px;" readonly disabled/></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="right"><strong>Sum:</strong></td>
                                            <td><input type="text" name="txt_total_batch_qty" id="txt_total_batch_qty" class="text_boxes_numeric" style="width:80px" readonly /></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                <fieldset style="width:610px;">
                    <table style="width:610">
                        <tr>
                            <td width="100">Remarks:</td>
                            <td>
                                <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:608px;"    />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                </td>
                <td align="right">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="4" class="button_container">
					<?php
						echo load_submit_buttons($permission, "fnc_pro_fab_subprocess", 0,0,"fnResetForm()",1);
                    ?>
                </td>
            </tr>
        </table>
        </fieldset>
        </div>
	</form>
</div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>