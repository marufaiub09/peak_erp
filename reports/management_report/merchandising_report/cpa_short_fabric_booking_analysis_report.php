<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create CPA/Short Fabric Booking Analysis Report.
Functionality	:	
JS Functions	:
Created by		:	Aziz 
Creation date 	: 	01-09-2014
Updated by 		: 	Maruf	
Update date		:      09-12-2015		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
//echo load_html_head_contents("Cost Breakdown Report","../../", 1, 1, $unicode,1,1);
echo load_html_head_contents("CPA/Short Fabric Booking Analysis Report","../../../", 1, 1, $unicode,1,1);
?>	
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../../logout.php";  
	 var tableFilters = 
			 {
				 col_46: "none",
				col_operation: {
					id: ["total_order_qnty","total_order_amount","total_yarn_cost","total_purchase_cost","total_knitting_cost","total_yarn_dyeing_cost","total_fabric_dyeing_cost","total_heat_setting_cost"],
			   col: [9,10,12,13,14,15,16,17],
			   operation: ["sum","sum","sum","sum","sum","sum","sum","sum"],
			   write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
				}	
			}
	function fn_cpa_report_generated()
	{
		var job_no=document.getElementById('txt_job_no').value;	
		var booking_no=document.getElementById('txt_booking_no').value;
		if(job_no!="" || booking_no!="")
		{
			if(form_validation('cbo_company_name','Company')==false)
			{
				return;
                                //alert('hello');
			}
		}
		else
		{
			if(form_validation('cbo_company_name*txt_date_from*txt_date_to','Company*From date Fill*To date Fill')==false)
			{
				return;
                                
			}
		}
			var data="action=cpa_report_generate"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*txt_date_from*txt_date_to*txt_job_no*txt_job_id*txt_booking_no*txt_booking_id*cbo_search_date',"../../../");
			//alert(data);return;
			freeze_window(3);
			http.open("POST","requires/cpa_short_fabric_booking_analysis_report_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fn_cpa_report_generated_reponse;
	}
	function fn_cpa_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split("****");
			var tot_rows=reponse[2];
			$('#report_container2').html(reponse[0]);
			document.getElementById('report_container').innerHTML=report_convert_button('../../../'); 
			append_report_checkbox('table_header_1',1);
			setFilterGrid("table_body",-1);
			//alert(document.getElementById('graph_data').value);
			//show_graph( '', document.getElementById('graph_data').value, "pie", "chartdiv", "", "../../../", '',580,700 );
			release_freezing();
			show_msg('3');
		}
	}
	
	function openmypage_job()
	{
		if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}
		var companyID = $("#cbo_company_name").val();
		var buyer_name = $("#cbo_buyer_name").val();
		var cbo_year_id = $("#cbo_year").val();
		//var cbo_month_id = $("#cbo_month").val();
		var page_link='requires/cpa_short_fabric_booking_analysis_report_controller.php?action=job_no_popup&companyID='+companyID+'&buyer_name='+buyer_name+'&cbo_year_id='+cbo_year_id;
		var title='Job No Search';
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=630px,height=370px,center=1,resize=1,scrolling=0','../../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var job_no=this.contentDoc.getElementById("hide_job_no").value;
			var job_id=this.contentDoc.getElementById("hide_job_id").value;
			$('#txt_job_no').val(job_no);
			$('#txt_job_id').val(job_id);	 
		}
	}
	function openmypage_booking()
	{
			//var job_no = $("#txt_job_no").val();
		if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}
		var data=document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_job_no').value;
		//alert (data);
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/cpa_short_fabric_booking_analysis_report_controller.php?action=booking_no_popup&data='+data,'Booking No Popup', 'width=1050px,height=420px,center=1,resize=0','../../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var theemail=this.contentDoc.getElementById("selected_booking");
			//var response=theemail.value.split('_');
			if (theemail.value!="")
			{
				freeze_window(5);
				//document.getElementById("txt_booking_id").value=theemail.value;
			    document.getElementById("txt_booking_no").value=theemail.value;
				release_freezing();
			}
		}
	}
	function ms_booking_no_popup(type,company,booking_no,po_id,job_no,fabric_source,fabric_nature)
	{
		var data="action=show_fabric_booking_report"+
					'&txt_booking_no='+"'"+booking_no+"'"+
					'&cbo_company_name='+"'"+company+"'"+
					'&txt_order_no_id='+"'"+po_id+"'"+
					'&cbo_fabric_natu='+"'"+fabric_nature+"'"+
					'&cbo_fabric_source='+"'"+fabric_source+"'"+
					'&txt_job_no='+"'"+job_no+"'";
					//alert(data);
		if(type==1)	
		{			
			http.open("POST","../../../order/woven_order/requires/short_fabric_booking_controller.php",true);
		}
		else if(type==2)
		{
			http.open("POST","../../../order/woven_order/requires/fabric_booking_controller.php",true);
		}
		else
		{
			http.open("POST","../../../order/woven_order/requires/sample_booking_controller.php",true);
		}
		
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = function()
		{
			if(http.readyState == 4) 
		    {
			var w = window.open("Surprise", "_blank");
			var d = w.document.open();
			d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title></head><body>'+http.responseText+'</body</html>');//<link rel="stylesheet" href="../../css/style_common.css" type="text/css" />
			d.close();
		   }
		}
	}
	function search_populate(str)
	{
		if(str==1)
		{
			document.getElementById('search_by_th_up').innerHTML="Shipment Date";
			$('#search_by_th_up').css('color','blue');
		}
		else if(str==2)
		{
			document.getElementById('search_by_th_up').innerHTML="Booking Date";
			$('#search_by_th_up').css('color','blue');
		}
	}
</script>
</head>
<body onLoad="set_hotkey();">
<form id="cost_breakdown_rpt">
    <div style="width:100%;" align="center">
        <?php echo load_freeze_divs ("../../../","");  ?>
         <h3 align="left" id="accordion_h1" style="width:1100px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
         <div id="content_search_panel"> 
       
         <fieldset style="width:1100px;">
        	
            <table class="rpt_table" width="1100" cellpadding="1" cellspacing="2" align="center">
               <thead>                    
                    <th class="must_entry_caption">Company Name</th>
                    <th>Buyer Name</th>
                    <th>Job No.</th>
                     <th>Booking No</th>
                     <th>Search By</th>
                     <th  id="search_by_th_up" class="must_entry_caption">Shipment Date</th>
                    <th><input type="reset" id="reset_btn" class="formbutton" style="width:100px" value="Reset" /></th>
                 </thead>
                <tbody>
                <tr class="general">
                    <td> 
                        <?php
                            echo create_drop_down( "cbo_company_name", 170, "select comp.id,comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/order_wise_budget_report_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                        ?>
                    </td>
                    <td id="buyer_td">
                        <?php 
                            echo create_drop_down( "cbo_buyer_name", 170, $blank_array,"", 1, "-- All Buyer --", $selected, "",0,"" );
                        ?>
                    </td>
                   
                    <td>
                            <input type="text" id="txt_job_no" name="txt_job_no" class="text_boxes" style="width:110px" onDblClick="openmypage_job();" placeholder="Write/Browse Job" />
                            <input type="hidden" id="txt_job_id" name="txt_job_id" class="text_boxes"/>
                    </td>
                    
                    <td>
                            <input type="text" id="txt_booking_no" name="txt_booking_no" class="text_boxes" style="width:110px" onDblClick="openmypage_booking();" placeholder="Write/Browse Booking"  />
                            <input type="hidden" id="txt_booking_id" name="txt_booking_id"/>
                    </td>
                     <td width="" align="center">
                        <?php  
                            $search_by = array(1=>'Shipment Date',2=>'Booking Date');
							$dd="search_populate(this.value)";
							echo create_drop_down( "cbo_search_date", 120, $search_by,"",0, "--Select--", $selected,$dd,0 );
                        ?>
                     </td>
                    <td><input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" >&nbsp; To
                    <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px"  placeholder="To Date" ></td>
                    <td>
                        <input type="button" id="show_button" class="formbutton" style="width:100px" value="Show" onClick="fn_cpa_report_generated()" />
                    </td>
                </tr>
                </tbody>
            </table>
            <table>
            	<tr>
                	<td>
 						<?php echo load_month_buttons(1); ?>
                   	</td>
                </tr>
            </table> 
        </fieldset>
    </div>
     </div>
    <div id="report_container" align="center"></div>
    <div id="report_container2"></div>
 </form>    
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
