<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//system id popup here----------------------// 
if ($action=="system_id_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		$("#hidden_sys_number").val(str);
		parent.emailwindow.hide(); 
	}
</script>
</head>
<body>
    <div align="center" style="width:100%;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="750" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="150">Company</th>
                <th width="150" align="center" id="search_by_td_up">Issue Basis</th>
                <th width="250">Date Range</th>
                <th width="100"><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
                    	<?php 
							 echo create_drop_down( "cbo_company_name", 150, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0  order by company_name","id,company_name", 1, "-- Select Company --", $selected,"","0" );
						?> 
                    </td>
                    <td width="250" align="center" id="search_by_td">				
						<?php 
							$get_pass_basis=array(1=>"Independent",2=>"Challan(Yarn)",3=>"Challan(Gray Fabric)",4=>"Challan(Finish Fabric)",5=>"Challan(General Item)",6=>"Challan(Trims)",6=>"Challan(Dyes & Chemical)",7=>"Challan(Trims)");
								echo create_drop_down( "cbo_basis", 150, $get_pass_basis,"",1, "-- Select --", 0, "" ); 
						?>                 
                     </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:100px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:100px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                       <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_basis').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value, 'create_sys_search_list_view', 'search_div', 'get_out_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
                </tr>
                <tr>                  
                    <td align="center" height="40" valign="middle" colspan="4">
                        <?php echo load_month_buttons(1);  ?>
                        <!-- Hidden field here-------->
                       <!-- input type="hidden" id="hidden_tbl_id" value="" ---->
                       
                        <input type="hidden" id="hidden_sys_number" value="hidden_sys_number" />
                        <!-- ---------END-------------> 
                    </td>
                </tr>    
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
    </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_sys_search_list_view")
{
	$ex_data = explode("_",$data);
	$company = str_replace("'","",$ex_data[0]);
	$issue_basis =str_replace("'","", $ex_data[1]);
	$fromDate =str_replace("'","",$ex_data[2]);
	$toDate = str_replace("'","",$ex_data[3]);
	
 	//$sql_cond="";
	if( $company!=0 )  $company_cond=" and company_id=$company"; else  $company_cond="";
	if( $issue_basis!=0 )  $issue_cond=" and basis=$issue_basis"; else  $issue_cond="";
	 if($db_type==0)
	 {
	if( $fromDate!=0 && $toDate!=0 ) $sql_cond= " and out_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
	 }
	 if($db_type==2)
	 {
	if( $fromDate!=0 && $toDate!=0 ) $sql_cond= " and out_date  between '".change_date_format($fromDate,'mm-dd-yyyy','-',1)."' and '".change_date_format($toDate,'mm-dd-yyyy','-',1)."'";
	 }
	
	
	
	
	$sql = "select sys_number_prefix_num, sys_number, sent_by, sent_to, out_date,department_id,section,challan_no from  inv_gate_out_mst	where status_active=1 and is_deleted=0 $company_cond $issue_cond  $sql_cond ";
	//echo $sql;
	//echo $sql_cond; die;
	$sample_arr = return_library_array( "select id, sample_name from lib_sample",'id','sample_name');
	$arr=array(1=>$sample_arr,2=>$item_category,7=>$currency);
	echo create_list_view("list_view", "Gate Out No,,Item Catagory,Sent By,Sent To,Out Date,Challan No,Currency","70,150,110,100,100,80,70,70","800","260",0, $sql , "js_set_value", "sys_number", "", 1, "0,sample_id,item_category_id,0,0,0,0,currency_id", $arr, "sys_number,sample_id,item_category_id,sent_by,sent_to,out_date,challan_no,currency_id", "",'','') ;	
	exit();
	
}


if($action=="populate_master_from_data")
{
	$sql="select sys_number,company_id,sample_id,item_category_id,sent_by,sent_to,out_date,challan_no,currency_id,gate_pass_no,time_hour,time_minute from inv_gate_out_mst where sys_number='$data'";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{	
		echo "$('#cbo_sample').val(".$row[csf("sample_id")].");\n";
		echo "$('#cbo_item_category').val(".$row[csf("item_category_id")].");\n"; 		
		//echo "$('#hidden_type').val(".$row[csf("piworeq_type")].");\n";
		echo "$('#txt_sent_by').val('".$row[csf("sent_by")]."');\n";
		echo "$('#txt_sent_to').val('".$row[csf("sent_to")]."');\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("out_date")])."');\n";	
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_currency').val('".$row[csf("currency_id")]."');\n";	
		echo "$('#txt_gate_pass_no').val(".$row[csf("gate_pass_no")].");\n";
		echo "$('#txt_start_hours').val(".$row[csf("time_hour")].");\n";	
		echo "$('#txt_start_minuties').val(".$row[csf("time_minute")].");\n"; 		  	
		//right side list view 
		//echo "show_list_view(".$row[csf("piworeq_type")]."+'**'+".$row[csf("pi_wo_req_id")].",'show_product_listview','list_product_container','requires/get_out_entry_controller','');\n";
	}
	exit();	
}


//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN");}
		
		$sql_se="select gate_pass_id from  inv_gate_out_scan where gate_pass_id=$txt_gate_pass";
		$res=sql_select($sql_se);
		  foreach($res as $row)
		     {
				$system_number=$row[csf('gate_pass_id')];
				 
			 }
		if( $system_number!=""){ echo 40;die;}
		$sql_pre="select out_date,time_hour,time_minute from   inv_gate_pass_mst where sys_number=$txt_gate_pass";
		//echo $sql_pre;die;
		$result=sql_select($sql_pre);
		  foreach($result as $val)
		     {
				 $out_date=change_date_format($val[csf('out_date')]);
				 $to_time=$out_date." ".$val[csf('time_hour')].":".$val[csf('time_minute')].":00";
				 $to_time = strtotime("$to_time");
				 $today_date=date("d-m-Y H:m:s ");
				 $today_date = strtotime("$today_date");
				$date_difference=$today_date-$to_time;
				$diff_hour=floor($date_difference/3600);
				
			 }
		if($diff_hour>24)  {echo 30;die;}
		
		//=====================================save start =================================================================================
		$id=return_next_id("id", "inv_gate_out_scan", 1);			
  		$field_array2="id,gate_pass_id,out_date,out_time,inserted_by,insert_date,status_active,is_deleted";
		$data_array2="(".$id.",".$txt_gate_pass.",".$txt_gate_out_date.",".$txt_gate_out_time.",'".$user_id."','".$pc_date_time."',1,0)";
		//echo "insert into inv_gate_out_scan($field_array2)values".$data_array2;
		$rID=sql_insert("inv_gate_out_scan",$field_array2,$data_array2,1); 
		
		if($db_type==0)
			{	
				if($rID )
				{
					mysql_query("COMMIT");  
					echo "0**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
				}
				else
				{
					mysql_query("ROLLBACK"); 
					//echo "10**";
					echo "10**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
				}
			}
			
		if($db_type==2)
			 {	
				if($rID )
				{
					oci_commit($con);
					echo "0**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
				}
				else
				{
					
					oci_rollback($con);
					echo "10**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
				}
			}
		disconnect($con);
		die;
				
	}	
			
}

if($action=="show_dtls_list_view")
{
	
 	$sql = "select b.id as id,b.item_description,b.quantity,b.uom,b.rate,b.amount,b.remarks from inv_gate_out_mst a,inv_gate_out_dtls b where a.id=b.mst_id and a.sys_number='$data' and b.status_active=1 and b.is_deleted=0 	"; 
	//echo $data; die;
	$arr=array(2=>$unit_of_measurement);
	//function create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path, $filter_grid_fnc, $fld_type_arr, $summary_flds, $check_box_all )
 	echo create_list_view("list_view", "Item Description,Qnty,UOM,Rate,Amount,Remarks","250,70,40,100,150,190","850","260",0, $sql, "get_php_form_data", "id", "'child_form_input_data','requires/get_out_entry_controller'", 1, "0,0,uom,0,0,0", $arr, "item_description,quantity,uom,rate,amount,remarks", "","",'0,2,1,2,2,0',"2  ,quantity,'','',amount,''");	
	exit();
		
} 
if($action=="child_form_input_data")
{
	//$data = details table ID 	
	$sql="select id,item_description,quantity,uom,rate,amount,remarks from inv_gate_out_dtls where id=$data"; 
	$result = sql_select($sql);
	
	foreach($result as $row)
	{
		
		echo "$('#txt_item_description').val('".$row[csf("item_description")]."');\n";
		echo "$('#txt_quantity').val(".$row[csf("quantity")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("uom")].");\n";
		echo "$('#txt_rate').val(".$row[csf("rate")].");\n";
		echo "$('#txt_amount').val(".$row[csf("amount")].");\n";		
 		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		//update id here
		echo "$('#update_id').val(".$row[csf("id")].");\n";		
		//echo "show_list_view(".$row[csf("wo_po_type")]."+'**'+".$row[csf("wo_pi_no")].",'show_product_listview','list_product_container','requires/yarn_receive_controller','');\n";
		echo "set_button_status(1, permission, 'fnc_getout_entry',1,1);\n";
	}
	exit();
}

if ($action=="get_out_entry_print")
{
	extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from   lib_store_location", "id", "store_name"  );
	$sample_library=return_library_array( "select id, sample_name from  lib_sample", "id", "sample_name"  );
	
	$sql="select id, sys_number, company_id, sample_id, item_category_id, sent_by, sent_to, out_date, challan_no, currency_id, gate_pass_no, time_hour, time_minute from   inv_gate_out_mst where company_id='$data[0]' and sys_number='$data[1]' and status_active=1 and is_deleted=0 ";
	//echo $sql;
	$dataArray=sql_select($sql);
?>
<div style="width:930px;" align="center">
    <table width="900" cellspacing="0" align="center" border="0">
        <tr>
            <td colspan="7" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
            <td colspan="7" align="center">
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>
        </tr>
        <tr>
            <td colspan="7" align="center" style="font-size:x-large"><strong><u><?php echo $data[2]; ?> Challan</u></strong></td>
        </tr>
        <tr>
            <td width="160"><strong>System ID:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('sys_number')]; ?></td>
            <td width="120"><strong>Sample:</strong></td><td width="175px" ><?php echo $sample_library[$dataArray[0][csf('sample_id')]]; ?></td>
            <td width="125"><strong>Item Category:</strong></td><td width="175px" colspan="2"><?php echo  $item_category[$dataArray[0][csf('item_category_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Sent By:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('sent_by')]; ?></td>
            <td><strong>Sent To:</strong></td><td width="175px" ><?php echo $dataArray[0][csf('sent_to')]; ?></td>
            <td><strong>Out Date:</strong></td><td width="175px" colspan="2"><?php echo change_date_format($dataArray[0][csf('out_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Challan No :</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Gate Pass No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('gate_pass_no')]; ?></td>
            <td><strong>Out-Time:</strong></td><td width="85px" ><?php echo $dataArray[0][csf('time_hour')]." HH"; ?></td><td width="85px"><?php echo $dataArray[0][csf('time_minute')]." Min"; ?></td>
        </tr>
    </table>
    <br>
    <table align="center" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="180" align="center">Item Description</th>
            <th width="50" align="center">UOM</th>
            <th width="80" align="center">Quantity</th>
            <th width="80" align="center">Rate</th> 
            <th width="80" align="center">Amount </th>
            <th width="180" align="center">Remarks</th>
        </thead>
<?php
    $i=1;
	$gate_id=$dataArray[0][csf('id')];
	$sql_dtls= " select id, item_description, quantity, uom, rate, amount, remarks from  inv_gate_out_dtls where mst_id=$gate_id and status_active=1 and is_deleted=0 ";
	//echo $sql_dtls;
	$sql_result=sql_select($sql_dtls);
	
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td><?php echo $i; ?></td>
                <td><?php  echo $row[csf('item_description')]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                <td align="right"><?php echo $row[csf('quantity')]; ?></td>
                <td align="right"><?php echo number_format($row[csf('rate')],2,'.',''); ?></td>
                <td align="right"><?php echo number_format($row[csf('amount')],2,'.',''); ?></td>
                <td><?php echo $row[csf('remarks')]; ?></td>
			</tr>
		<?php
    $i++;
    }
	?>
      </table>
</div>
<div>
		<?php
        	echo signature_table(34, $data[0], "900px");
        ?>
</div>    
<?php

exit();
}

?>