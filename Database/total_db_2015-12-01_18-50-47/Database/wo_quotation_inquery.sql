-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_quotation_inquery`
--

DROP TABLE IF EXISTS `wo_quotation_inquery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_quotation_inquery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_number_prefix` varchar(15) CHARACTER SET utf8 NOT NULL,
  `system_number_prefix_num` int(11) NOT NULL,
  `system_number` varchar(20) CHARACTER SET utf8 NOT NULL,
  `company_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `season` varchar(100) CHARACTER SET utf8 NOT NULL,
  `inquery_date` date NOT NULL,
  `buyer_request` varchar(50) CHARACTER SET utf8 NOT NULL,
  `remarks` varchar(200) CHARACTER SET utf8 NOT NULL,
  `dealing_marchant` int(11) NOT NULL DEFAULT '0',
  `gmts_item` int(11) NOT NULL,
  `est_ship_date` date NOT NULL,
  `fabrication` varchar(300) NOT NULL,
  `offer_qty` double NOT NULL,
  `color` varchar(100) NOT NULL,
  `req_quotation_date` date NOT NULL,
  `target_sam_sub_date` date NOT NULL,
  `actual_sam_send_date` date NOT NULL,
  `actual_req_quot_date` date NOT NULL,
  `style_refernce` varchar(50) CHARACTER SET utf8 NOT NULL,
  `insert_by` int(4) NOT NULL,
  `insert_date` date NOT NULL,
  `update_by` int(4) NOT NULL,
  `update_date` date NOT NULL,
  `status_active` int(4) NOT NULL DEFAULT '1',
  `is_deleted` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_quotation_inquery`
--

LOCK TABLES `wo_quotation_inquery` WRITE;
/*!40000 ALTER TABLE `wo_quotation_inquery` DISABLE KEYS */;
INSERT INTO `wo_quotation_inquery` VALUES (1,'PAL-QIN-15-',1,'PAL-QIN-15-00001',1,34,'','2015-11-01','','',9,0,'0000-00-00','',0,'','0000-00-00','0000-00-00','0000-00-00','0000-00-00','JY7504',2,'2015-11-01',2,'2015-11-18',1,0);
/*!40000 ALTER TABLE `wo_quotation_inquery` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:47
