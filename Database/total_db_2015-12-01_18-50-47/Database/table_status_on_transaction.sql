-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `table_status_on_transaction`
--

DROP TABLE IF EXISTS `table_status_on_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_status_on_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_name` int(11) NOT NULL,
  `is_locked` int(11) NOT NULL DEFAULT '0',
  `set_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_name` (`form_name`),
  KEY `is_locked` (`is_locked`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_status_on_transaction`
--

LOCK TABLES `table_status_on_transaction` WRITE;
/*!40000 ALTER TABLE `table_status_on_transaction` DISABLE KEYS */;
INSERT INTO `table_status_on_transaction` VALUES (1,66,0,'0000-00-00 00:00:00'),(2,68,0,'0000-00-00 00:00:00'),(3,206,0,'0000-00-00 00:00:00'),(4,203,0,'0000-00-00 00:00:00'),(5,210,0,'0000-00-00 00:00:00'),(6,204,0,'0000-00-00 00:00:00'),(7,205,0,'0000-00-00 00:00:00'),(8,137,0,'0000-00-00 00:00:00'),(9,172,0,'0000-00-00 00:00:00'),(10,174,0,'0000-00-00 00:00:00'),(11,207,0,'0000-00-00 00:00:00'),(12,209,0,'0000-00-00 00:00:00'),(13,154,0,'0000-00-00 00:00:00'),(14,156,0,'0000-00-00 00:00:00'),(15,73,0,'0000-00-00 00:00:00'),(16,70,0,'0000-00-00 00:00:00'),(17,455,0,'0000-00-00 00:00:00'),(18,181,0,'0000-00-00 00:00:00'),(19,184,0,'0000-00-00 00:00:00'),(26,76,0,'0000-00-00 00:00:00'),(27,69,0,'0000-00-00 00:00:00'),(28,452,0,'0000-00-00 00:00:00'),(29,157,0,'0000-00-00 00:00:00'),(30,419,0,'0000-00-00 00:00:00'),(31,144,0,'0000-00-00 00:00:00'),(32,542,0,'0000-00-00 00:00:00'),(33,63,0,'0000-00-00 00:00:00'),(34,543,0,'0000-00-00 00:00:00'),(35,544,0,'0000-00-00 00:00:00'),(36,545,0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `table_status_on_transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
