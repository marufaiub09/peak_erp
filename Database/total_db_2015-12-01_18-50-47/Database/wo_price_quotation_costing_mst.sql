-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_price_quotation_costing_mst`
--

DROP TABLE IF EXISTS `wo_price_quotation_costing_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_price_quotation_costing_mst` (
  `id` int(11) NOT NULL DEFAULT '0',
  `quotation_id` int(11) NOT NULL DEFAULT '0',
  `costing_per_id` int(11) NOT NULL DEFAULT '0',
  `order_uom_id` int(11) NOT NULL DEFAULT '0',
  `fabric_cost` double NOT NULL,
  `fabric_cost_percent` double NOT NULL,
  `trims_cost` double NOT NULL,
  `trims_cost_percent` double NOT NULL,
  `embel_cost` double NOT NULL,
  `embel_cost_percent` double NOT NULL,
  `wash_cost` double NOT NULL,
  `wash_cost_percent` double NOT NULL,
  `comm_cost` double NOT NULL,
  `comm_cost_percent` double NOT NULL,
  `lab_test` double NOT NULL,
  `lab_test_percent` double NOT NULL,
  `inspection` double NOT NULL,
  `inspection_percent` double NOT NULL,
  `cm_cost` double NOT NULL,
  `cm_cost_percent` double NOT NULL,
  `freight` double NOT NULL,
  `freight_percent` double NOT NULL,
  `currier_pre_cost` double NOT NULL,
  `currier_percent` double NOT NULL,
  `certificate_pre_cost` double NOT NULL,
  `certificate_percent` double NOT NULL,
  `common_oh` double NOT NULL,
  `common_oh_percent` double NOT NULL,
  `depr_amor_pre_cost` double NOT NULL,
  `depr_amor_po_price` double NOT NULL,
  `interest_pre_cost` double NOT NULL,
  `interest_po_price` double NOT NULL,
  `income_tax_pre_cost` double NOT NULL,
  `income_tax_po_price` double NOT NULL,
  `total_cost` double NOT NULL,
  `total_cost_percent` double NOT NULL,
  `commission` double NOT NULL,
  `commission_percent` double NOT NULL,
  `final_cost_dzn` double NOT NULL,
  `final_cost_dzn_percent` double NOT NULL,
  `final_cost_pcs` double NOT NULL,
  `final_cost_set_pcs_rate` double NOT NULL DEFAULT '0',
  `a1st_quoted_price` double NOT NULL,
  `a1st_quoted_price_percent` double NOT NULL,
  `a1st_quoted_price_date` date NOT NULL,
  `revised_price` double NOT NULL,
  `revised_price_date` date NOT NULL,
  `confirm_price` double NOT NULL,
  `confirm_price_set_pcs_rate` double NOT NULL DEFAULT '0',
  `confirm_price_dzn` double NOT NULL,
  `confirm_price_dzn_percent` double NOT NULL,
  `margin_dzn` double NOT NULL,
  `margin_dzn_percent` double NOT NULL,
  `price_with_commn_dzn` double NOT NULL,
  `price_with_commn_percent_dzn` double NOT NULL,
  `price_with_commn_pcs` double NOT NULL,
  `price_with_commn_percent_pcs` double NOT NULL,
  `confirm_date` date NOT NULL,
  `asking_quoted_price` double NOT NULL,
  `asking_quoted_price_percent` double NOT NULL,
  `terget_qty` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  KEY `quotation_id` (`quotation_id`),
  KEY `order_uom_id` (`order_uom_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_price_quotation_costing_mst`
--

LOCK TABLES `wo_price_quotation_costing_mst` WRITE;
/*!40000 ALTER TABLE `wo_price_quotation_costing_mst` DISABLE KEYS */;
INSERT INTO `wo_price_quotation_costing_mst` VALUES (1,1,1,1,13.9762,52.37,4.675,17.52,0.9,3.37,0,0,0.1896,0.71,0,0,0,0,4.3269,16.21,0.5,1.87,0.5,1.87,0.05,0.19,0.5,1.87,0,0,0,0,0,0,25.6177,96,0,0,0.089,4,2.1348,0,2.2238,4,'0000-00-00',0,'0000-00-00',0,0,26.6856,100,1.0679,4,26.6856,0,2.2238,0,'2015-11-01',2.2238,8.33,4,2,'2015-11-01 10:51:33',2,'2015-11-22 12:05:44',1,0);
/*!40000 ALTER TABLE `wo_price_quotation_costing_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:47
