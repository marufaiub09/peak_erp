<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_name=$_SESSION['logic_erp']['user_id'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_floor")
{
	echo create_drop_down( "cbo_floor_id", 160, "select a.id, a.floor_name from lib_prod_floor a, lib_machine_name b where a.id=b.floor_id and b.category_id=1 and b.company_id=$data and b.status_active=1 and b.is_deleted=0 $location_cond group by a.id, a.floor_name order by a.floor_name","id,floor_name", 1, "-- Select Floor --", 0, "","" );
  exit();	 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 

	$datediff=datediff('d',str_replace("'","",$txt_date_from),str_replace("'","",$txt_date_to));
	
	if(str_replace("'","",$cbo_floor_id)==0) $floor_cond=""; else $floor_cond=" and floor_id=$cbo_floor_id";
	
	$floor_arr=return_library_array( "select id, floor_name from lib_prod_floor",'id','floor_name');
	
	$machine_data_array=array();
	$machine_data=sql_select("select id, floor_id, machine_no, dia_width, gauge, prod_capacity from lib_machine_name where company_id=$cbo_company_name and category_id=1 and status_active=1 and is_deleted=0 $floor_cond order by floor_id, dia_width");//, cast(machine_no as unsigned)
	foreach($machine_data as $row)
	{
		$machine_data_array[$row[csf('id')]]['no']=$row[csf('machine_no')];
		$machine_data_array[$row[csf('id')]]['floor']=$row[csf('floor_id')];
		$machine_data_array[$row[csf('id')]]['dia']=$row[csf('dia_width')];
		$machine_data_array[$row[csf('id')]]['gg']=$row[csf('gauge')];
		$machine_data_array[$row[csf('id')]]['capacity']=$row[csf('prod_capacity')];
	}
	
	$tbl_width=560+$datediff*90;
	ob_start();
	?>
	<fieldset style="width:<?php echo $tbl_width+20; ?>px;">
		<table cellpadding="0" cellspacing="0" width="<?php echo $tbl_width; ?>">
			<tr>
			   <td align="center" width="100%" colspan="<?php echo $datediff+5; ?>" style="font-size:16px"><strong><?php echo $report_title; ?></strong></td>
			</tr>
		</table>	
		<table style="margin-left:1px" cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $tbl_width; ?>" class="rpt_table" >
			<thead>
				<th width="40">SL</th>
				<th width="100">Floor No</th>
				<th width="90">Machine No</th>
				<th width="90">Machine Dia</th>
				<th width="90">Machine GG</th>
                <th width="100">Capacity<br/><font style="font-size:9px; font-weight:100">(As Per Library)</font></th>
				<?php
					$date_array=array();
					$s=1;
                    for($j=0;$j<$datediff;$j++)
					{
                        $newdate =add_date(str_replace("'","",$txt_date_from),$j);
						$date_array[$j]=$newdate;
						if($s==$datediff) $width=""; else $width="width=90";
						echo '<th '.$width.'>'.change_date_format($newdate).'</th>';//echo date("d-M",strtotime($newdate))."'".date("y",strtotime($newdate));
                   		$s++;
				    }  
				?>
			</thead>
		</table>
		<div style="width:<?php echo $tbl_width; ?>px; overflow-y:scroll; max-height:330px;" id="buyer_list_view" align="center">
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $tbl_width-18; ?>" class="rpt_table" id="tbl_list_search">
				<tbody>
				<?php 
                    $i=1; $machine_date_array=array(); $tot_capacity=0; $tot_qnty_array=array();
                    $dataArray=sql_select("select dtls_id, machine_id, distribution_date, fraction_date, sum(days_complete) as days_complete, sum(qnty) as qnty, 'Y' as status from ppl_entry_machine_datewise group by machine_id, distribution_date, dtls_id, fraction_date");
					foreach ($dataArray as $row)
					{
						$distribution_date=date("Y-m-d",strtotime($row[csf('distribution_date')]));
						$machine_date_array[$row[csf('machine_id')]][$distribution_date]['st']=$row[csf('status')]; 
						$machine_date_array[$row[csf('machine_id')]][$distribution_date]['fr']=$row[csf('fraction_date')];
						$machine_date_array[$row[csf('machine_id')]][$distribution_date]['dc']=$row[csf('days_complete')]; 
						$machine_date_array[$row[csf('machine_id')]][$distribution_date]['qnty']=$row[csf('qnty')]; 
						$machine_date_array[$row[csf('machine_id')]][$distribution_date]['dtls_id']=$row[csf('dtls_id')];
					}
					
					$capacity_arr=array();
					$dataArray=sql_select("select dtls_id, machine_id, capacity from ppl_planning_info_machine_dtls");
					foreach ($dataArray as $row)
					{
						$capacity_arr[$row[csf('machine_id')]][$row[csf('dtls_id')]]=$row[csf('capacity')]; 
					}
					//var_dump($machine_date_array);
                    foreach($machine_data_array as $key=>$val)
                    {
                        if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$tot_capacity+=$machine_data_array[$key]['capacity'];
                        ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
                            <td width="40"><p><?php echo $i; ?></td>
                            <td width="100"><p><?php echo $floor_arr[$machine_data_array[$key]['floor']]; ?>&nbsp;</p></td>
                            <td width="90"><p><?php echo $machine_data_array[$key]['no']; ?>&nbsp;</p></td>
                            <td width="90"><p><?php echo $machine_data_array[$key]['dia']; ?>&nbsp;</p></td>
                            <td width="90"><p><?php echo $machine_data_array[$key]['gg']; ?>&nbsp;</p></td>
                            <td width="100" align="right"><?php echo number_format($machine_data_array[$key]['capacity'],2,'.',''); ?>&nbsp;</td>
                            <?php
							$s=1;
							foreach($date_array as $date)
							{
								if($s==count($date_array)) $width=""; else $width="width=90";
								
								$qnty=$machine_date_array[$key][$date]['qnty'];
								if($machine_date_array[$key][$date]['fr']==1)
								{
									$suffix="<br>(".number_format($machine_date_array[$key][$date]['dc'],2)." days)&nbsp;";
								}
								else 
								{
									$suffix="";
								}
								
								$td_color='';
								$capacity=$capacity_arr[$key][$machine_date_array[$key][$date]['dtls_id']];

								if($qnty>0)
								{
									if($qnty>=$capacity) 
									{
										$td_color='green';
									}
									else if($qnty<$capacity) 
									{
										$td_color='yellow';
									}
								}
								else 
								{
									$td_color='red';
								}
								
								echo '<td align="right" bgcolor="'.$td_color.'" '.$width.'><a href="##" style="color:#000" onclick="openmypage('.$machine_date_array[$key][$date]['dtls_id'].')">'.$qnty.'</a>&nbsp;'.$suffix.'</td>';
								
								$tot_qnty_array[$date]+=$qnty;
								
								$s++;
							}  
							?>
						</tr>
                        <?php
                        $i++;
                    }
                ?>
				</tbody>
        	</table>	
		</div>
        <table style="margin-left:1px" cellspacing="0" cellpadding="0" border="1" rules="all" width="<?php echo $tbl_width-18; ?>" class="rpt_table" >
            <tfoot>
                <tr>
                	<th width="40">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="90">&nbsp;</th>
                    <th width="90">&nbsp;</th>
                    <th align="right" width="90">Total</th>
                    <th align="right" width="100" id="value_capacity"><?php echo number_format($tot_capacity,2,'.',''); ?>&nbsp;</th>
                    <?php
					$s=1;
                    foreach($date_array as $date)
                    {
						if($s==count($date_array)) $width=""; else $width="width=90";
                        echo '<th align="right" '.$width.' id="value_qnty_'.$s.'">'.number_format($tot_qnty_array[$date],2,'.','').'&nbsp;</th>';
						$s++;
                    }  
                    ?>
                </tr>
                <tr>
                    <th colspan="5" align="right">%</th>
                    <th align="right">&nbsp;</th>
                    <?php
                    foreach($date_array as $date)
                    {
                        $perc=($tot_qnty_array[$date]*100)/$tot_capacity;
                        echo '<th align="right">'.number_format($perc,2,'.','').'&nbsp;</th>';
                    }  
                    ?>
                </tr>
            </tfoot>
        </table>
	</fieldset>      
<?php

	foreach (glob("$user_name*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_name."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename="requires/".$user_name."_".$name.".xls";
	echo "$total_data####$filename####".count($date_array);
	exit();
}

if($action=="plan_deails")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	
	$color_array=return_library_array( "select id, color_name from lib_color", "id", "color_name"  );
	$buyer_array=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );

?>
	<fieldset style="width:570px; margin-left:7px">
    	<b>Order Details</b>
        <table border="1" class="rpt_table" rules="all" width="550" cellpadding="0" cellspacing="0">
            <thead>
                <th width="40">SL</th>
                <th width="120">Job No</th>
                <th width="130">Buyer</th>
                <th width="140">Order No</th>
                <th>Shipment Date</th>
            </thead>
         </table>
         <div style="width:567px; max-height:170px; overflow-y:scroll" id="scroll_body">
             <table border="1" class="rpt_table" rules="all" width="550" cellpadding="0" cellspacing="0">
                <?php
                $i=1;
                $sql="select a.buyer_id, b.job_no_mst, b.po_number, b.pub_shipment_date from ppl_planning_entry_plan_dtls a, wo_po_break_down b where a.po_id=b.id and a.dtls_id=$program_id order by b.id, b.pub_shipment_date";
                $result=sql_select($sql);
                foreach($result as $row)
                {
                    if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
                ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
                        <td width="40"><?php echo $i; ?></td>
                        <td width="120"><p><?php echo $row[csf('job_no_mst')]; ?></p></td>
                        <td width="130"><p><?php echo $buyer_array[$row[csf('buyer_id')]]; ?></p></td>
                        <td width="140"><p><?php echo $row[csf('po_number')]; ?></p></td>
                        <td align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                    </tr>
                <?php
                $i++;
                }
                ?>
            </table>
        </div>	
        <br />
        <b>Fabric Details</b>
        <table border="1" class="rpt_table" rules="all" width="570" cellpadding="0" cellspacing="0">
            <thead>
                <th width="70">Fabric Dia</th>
                <th width="60">GSM</th>
                <th width="160">Description</th>
                <th width="60">Stitch Length</th>
                <th width="90">Color Range</th>
                <th>Fabric Color</th>
            </thead>
             <?php
			 	$query="select a.fabric_desc, a.gsm_weight, b.fabric_dia, b.color_id, b.color_range, b.start_date, b.end_date, b.stitch_length from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b where a.id=b.mst_id and b.id=$program_id";
                $dataArray=sql_select($query);
				$color='';
				$color_id=explode(",",$dataArray[0][csf('color_id')]);
				foreach($color_id as $val)
				{
					if($color=='') $color=$color_array[$val]; else $color.=",".$color_array[$val];
				}
			?>
            <tr bgcolor="#FFFFFF">
                <td width="70"><p><?php echo $dataArray[0][csf('fabric_dia')]; ?>&nbsp;</p></td>
                <td width="60"><p><?php echo $dataArray[0][csf('gsm_weight')]; ?>&nbsp;</p></td>
                <td width="160"><p><?php echo $dataArray[0][csf('fabric_desc')]; ?>&nbsp;</p></td>
                <td width="60"><p><?php echo $dataArray[0][csf('stitch_length')]; ?>&nbsp;</p></td>
                <td width="90"><p><?php echo $color_range[$dataArray[0][csf('color_range')]]; ?>&nbsp;</p></td>
                <td><p><?php echo $color; ?>&nbsp;</p></td>
            </tr>
         </table>
         <br />
         <b>TNA Details</b>
         <table border="1" class="rpt_table" rules="all" width="350" cellpadding="0" cellspacing="0">
            <thead>
                <th width="170">Kniting Start Date</th>
                <th>Kniting End Date</th>
            </thead>
            <tr bgcolor="#FFFFFF">
                <td align="center"><p><?php if($dataArray[0][csf('start_date')]!="0000-00-00") echo change_date_format($dataArray[0][csf('start_date')]); ?>&nbsp;</p></td>
                <td align="center"><p><?php if($dataArray[0][csf('end_date')]!="0000-00-00") echo change_date_format($dataArray[0][csf('end_date')]); ?>&nbsp;</p></td>
            </tr>
         </table>
	</fieldset>   
<?php
exit();
}
?>