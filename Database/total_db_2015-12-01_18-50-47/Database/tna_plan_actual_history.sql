-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tna_plan_actual_history`
--

DROP TABLE IF EXISTS `tna_plan_actual_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tna_plan_actual_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) DEFAULT NULL,
  `job_no` varchar(25) DEFAULT NULL,
  `po_number_id` int(11) DEFAULT NULL,
  `task_number` int(11) DEFAULT NULL,
  `task_start_date` date DEFAULT NULL,
  `task_finish_date` date DEFAULT NULL,
  `actual_start_date` date DEFAULT NULL,
  `actual_finish_date` date DEFAULT NULL,
  `status_active` int(1) DEFAULT '1',
  `is_deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tna_plan_actual_history`
--

LOCK TABLES `tna_plan_actual_history` WRITE;
/*!40000 ALTER TABLE `tna_plan_actual_history` DISABLE KEYS */;
INSERT INTO `tna_plan_actual_history` VALUES (1,2,'PAL-15-00406',464,30,NULL,NULL,'2015-09-06','2015-09-10',1,0),(2,5,'PAL-15-00258',314,70,NULL,NULL,'2015-08-22','2015-08-20',1,0),(3,2,'PAL-15-00339',392,1,NULL,NULL,'2015-08-21','2015-08-21',1,0),(4,2,'PAL-15-00340',393,1,NULL,NULL,'2015-08-22','2015-08-22',1,0),(5,2,'PAL-15-00370',423,31,NULL,NULL,'2015-08-27','2015-08-29',1,0),(6,1,'PAL-15-00613',667,31,NULL,NULL,'2015-11-12','0000-00-00',1,0),(7,1,'PAL-15-00613',667,1,NULL,NULL,'2015-11-12','0000-00-00',1,0),(8,1,'PAL-15-00613',667,30,NULL,NULL,'2015-11-12','0000-00-00',1,0),(9,7,'PAL-15-00705',758,1,NULL,NULL,'2015-11-12','2015-11-12',1,0),(10,7,'PAL-15-00706',759,1,NULL,NULL,'2015-11-12','2015-11-12',1,0),(11,2,'PAL-15-00434',486,85,NULL,NULL,'2015-10-23','0000-00-00',1,0);
/*!40000 ALTER TABLE `tna_plan_actual_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
