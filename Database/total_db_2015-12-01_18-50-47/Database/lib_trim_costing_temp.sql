-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_trim_costing_temp`
--

DROP TABLE IF EXISTS `lib_trim_costing_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_trim_costing_temp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `related_buyer` varchar(23) NOT NULL,
  `user_code` varchar(33) NOT NULL,
  `trims_group` varchar(33) NOT NULL,
  `cons_uom` varchar(44) NOT NULL,
  `cons_dzn_gmts` double NOT NULL DEFAULT '0',
  `purchase_rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `apvl_req` varchar(55) NOT NULL,
  `supplyer` varchar(44) NOT NULL,
  `inserted_by` int(4) NOT NULL,
  `inserted_date` datetime DEFAULT NULL,
  `updated_by` int(4) NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status_active` int(10) NOT NULL,
  `is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_code` (`user_code`),
  KEY `apvl_req` (`apvl_req`),
  KEY `supplyer` (`supplyer`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_trim_costing_temp`
--

LOCK TABLES `lib_trim_costing_temp` WRITE;
/*!40000 ALTER TABLE `lib_trim_costing_temp` DISABLE KEYS */;
INSERT INTO `lib_trim_costing_temp` VALUES (1,'13','','3','2',1,0.3,0.3,'2','0',1,'2015-01-29 05:21:57',0,NULL,1,0),(2,'13','','1','2',1,0.1,0.1,'2','0',1,'2015-01-29 05:22:29',0,NULL,1,0),(3,'13','','2','2',1,0.2,0.2,'2','0',1,'2015-01-29 05:23:12',0,NULL,1,0),(4,'13','','6','2',1,0.2,0.2,'2','0',1,'2015-01-29 05:23:52',0,NULL,1,0),(5,'13','','13','2',1,0.06,0.06,'2','0',1,'2015-01-29 05:24:22',0,NULL,1,0),(6,'13','','47','2',1,0.5,0.5,'2','0',1,'2015-01-29 05:27:09',0,NULL,1,0),(7,'13','','17','52',1.44,0.28,0.4032,'2','0',1,'2015-01-29 05:31:50',0,NULL,1,0),(8,'14','','33','2',1.05,0.15,0.1575,'2','0',2,'2015-10-24 14:35:22',2,'2015-10-24 14:37:09',0,1),(9,'14','','32','4',0.0175,14,0.245,'2','150',2,'2015-10-24 14:38:48',2,'2015-10-24 14:39:56',0,1);
/*!40000 ALTER TABLE `lib_trim_costing_temp` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
