<?
/*-------------------------------------------- Comments
Purpose			: 	This form will create Woven Garments Price Quotation Entry Entry
Functionality	:	
JS Functions	:
Created by		:	Rashed 
Creation date 	: 	18-10-2012
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//-------------------------------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Sample Info","../../", 1, 1, $unicode,'','');
?>	
<script>
/*document.onclick = function (e) {
	alert(e)
    e = e || window.event;      
    var target = e.target || e.srcElement;
    if (target !== btn && (!target.contains(modal) || target !== modal)) {
        modal.style.display = 'none';
    }  
}*/
/*$(document).bind('DOMSubtreeModified', function () {
   if ($('.validation_errors').length) {
       alert("test");
   }
});*/

/*$("input").each(function() {
    $(this).attr("originalValue", $(this).val()); 
});
*/

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
var permission = '<? echo $permission; ?>';
var str_construction = [<? echo substr(return_library_autocomplete( "select construction from wo_pri_quo_fabric_cost_dtls group by construction ", "construction"), 0, -1); ?> ];
var str_composition = [<? echo substr(return_library_autocomplete( "select composition from wo_pri_quo_fabric_cost_dtls group by composition", "composition"), 0, -1); ?>];
var str_incoterm_place = [<? echo substr(return_library_autocomplete( "select incoterm_place from  wo_price_quotation group by incoterm_place", "incoterm_place"), 0, -1); ?>];
var str_factory = [<? echo substr(return_library_autocomplete( "select factory from  wo_price_quotation group by factory", "factory"), 0, -1); ?>];
// Common For All----------------------------------------------------
function set_auto_complete(type)
{
	if(type=='price_quation_mst')
	{
			$("#txt_incoterm_place").autocomplete({
			source: str_incoterm_place
			});
			$("#txt_factory").autocomplete({
			source:  str_factory 
			}); 
	}
	if(type=='tbl_fabric_cost')
	{
		var row_num=$('#tbl_fabric_cost tr').length-1;
		for (var i=1; i<=row_num; i++)
		{
			$("#txtconstruction_"+i).autocomplete({
			source: str_construction
			});
			$("#txtcomposition_"+i).autocomplete({
			source:  str_composition 
			}); 
		}
	}
}

function show_sub_form(update_id, action, extra_str)
{
	if(update_id=="")
	{
		$('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			 { 
				$(this).html('Quotation id is Empty').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			 });
	}
	else
	{
		if(action=="show_fabric_cost_listview")
		{
			show_list_view(update_id+'_'+document.getElementById('cbo_company_name').value,action,'cost_container','../woven_order/requires/quotation_entry_controller','');
			sum_yarn_required();
			var approved_status=document.getElementById('cbo_approved_status').value;
			if(approved_status==1)
			{
			document.getElementById('save3').disabled=true;
		    document.getElementById('update3').disabled=true;
			document.getElementById('Delete3').disabled=true;
			document.getElementById('save4').disabled=true;
		    document.getElementById('update4').disabled=true;
			document.getElementById('Delete4').disabled=true;
			document.getElementById('save5').disabled=true;
		    document.getElementById('update5').disabled=true;
			document.getElementById('Delete5').disabled=true;
			}
			else
			{
			document.getElementById('save3').disabled=false;
		    document.getElementById('update3').disabled=false;
			document.getElementById('Delete3').disabled=false;
			document.getElementById('save4').disabled=false;
		    document.getElementById('update4').disabled=false;
			document.getElementById('Delete4').disabled=false;
			document.getElementById('save5').disabled=false;
		    document.getElementById('update5').disabled=false;
			document.getElementById('Delete5').disabled=false;
			}
		    set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost');
			//yarn
			set_sum_value( 'txtconsratio_sum', 'consratio_', 'tbl_yarn_cost' );
	        set_sum_value( 'txtconsumptionyarn_sum', 'consqnty_', 'tbl_yarn_cost' );
	        set_sum_value( 'txtamountyarn_sum', 'txtamountyarn_', 'tbl_yarn_cost' );
			//yarn end
			// conversion
			set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
	        set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
	        set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
			//
			set_auto_complete('tbl_fabric_cost')
		}
		if(action=="show_trim_cost_listview")
		{
			//alert(extra_str);
			show_list_view(update_id+'*'+extra_str,action,'cost_container','../woven_order/requires/quotation_entry_controller','');
			set_sum_value( 'txtconsdzntrim_sum', 'txtconsdzngmts_', 'tbl_trim_cost' );
	        set_sum_value( 'txtratetrim_sum', 'txttrimrate_', 'tbl_trim_cost' );
	        set_sum_value( 'txttrimamount_sum', 'txttrimamount_', 'tbl_trim_cost' );
		}
		if(action=="show_embellishment_cost_listview")
		{
			show_list_view(update_id,action,'cost_container','../woven_order/requires/quotation_entry_controller','');
	        set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_embellishment_cost' );
		}
		if(action=="show_wash_cost_listview")
		{
			show_list_view(update_id+'_'+document.getElementById('cbo_company_name').value,action,'cost_container','../woven_order/requires/quotation_entry_controller','');
	        set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_wash_cost' );
		}
		
		if(action=="show_commission_cost_listview")
		{
			show_list_view(update_id,action,'cost_container_commission','../woven_order/requires/quotation_entry_controller','');
			set_sum_value( 'txtratecommission_sum', 'txtcommissionrate_', 'tbl_commission_cost' );
	        set_sum_value( 'txtamountcommission_sum', 'txtcommissionamount_', 'tbl_commission_cost' );
		}
		if(action=="show_comarcial_cost_listview")
		{
			show_list_view(update_id,action,'cost_container','../woven_order/requires/quotation_entry_controller','');
			set_sum_value( 'txtratecomarcial_sum', 'txtcomarcialrate_', 'tbl_comarcial_cost' );
	        set_sum_value( 'txtamountcomarcial_sum', 'txtcomarcialamount_', 'tbl_comarcial_cost' );
		}
		
		
	}
}

function set_sum_value(des_fil_id,field_id,table_id)
{
	if(table_id=='tbl_fabric_cost')
	{
	var rowCount = $('#tbl_fabric_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd );
	//number_format_common((txt_confirm_price_pre_cost_dzn-txt_final_cost_dzn_pre_cost), 1, 0, currency)
	document.getElementById('txt_fabric_pre_cost').value=number_format_common((document.getElementById('txtamount_sum').value*1+(document.getElementById('txtamountyarn_sum').value)*1+(document.getElementById('txtconamount_sum').value)*1), 1, 0, document.getElementById('cbo_currercy').value);
	calculate_main_total();
	}
	if(table_id=='tbl_yarn_cost')
	{
	var rowCount = $('#tbl_yarn_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd );
	document.getElementById('txt_fabric_pre_cost').value=number_format_common(((document.getElementById('txtamount_sum').value)*1+(document.getElementById('txtamountyarn_sum').value)*1+(document.getElementById('txtconamount_sum').value)*1), 1, 0, document.getElementById('cbo_currercy').value);
	calculate_main_total();
	}
	if(table_id=='tbl_conversion_cost')
	{
	var rowCount = $('#tbl_conversion_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd );
	document.getElementById('txt_fabric_pre_cost').value=number_format_common(((document.getElementById('txtamount_sum').value)*1+(document.getElementById('txtamountyarn_sum').value)*1+(document.getElementById('txtconamount_sum').value)*1), 1, 0, document.getElementById('cbo_currercy').value);
	calculate_main_total();
	}
	if(table_id=='tbl_trim_cost')
	{
	var rowCount = $('#tbl_trim_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd );
	document.getElementById('txt_trim_pre_cost').value=document.getElementById('txttrimamount_sum').value;
	calculate_main_total()
	}
	if(table_id=='tbl_embellishment_cost')
	{
	var rowCount = $('#tbl_embellishment_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd);
	document.getElementById('txt_embel_pre_cost').value=document.getElementById('txtamountemb_sum').value;
	calculate_main_total()
	}
	if(table_id=='tbl_wash_cost')
	{
	var rowCount = $('#tbl_wash_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd);
	document.getElementById('txt_wash_pre_cost').value=document.getElementById('txtamountemb_sum').value;
	calculate_main_total()
	}
	if(table_id=='tbl_commission_cost')
	{
	var rowCount = $('#tbl_commission_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd);
	document.getElementById('txt_commission_pre_cost').value=document.getElementById('txtamountcommission_sum').value;
	calculate_main_total()
	calculate_price_with_commision_dzn()
	}
	if(table_id=='tbl_comarcial_cost')
	{
	var rowCount = $('#tbl_comarcial_cost tr').length-1;
	var ddd={ dec_type:1, comma:0, currency:document.getElementById('cbo_currercy').value}
	math_operation( des_fil_id, field_id, '+', rowCount,ddd );
	document.getElementById('txt_comml_pre_cost').value=document.getElementById('txtamountcomarcial_sum').value;
	calculate_main_total()
	}
	
	
}
function fn_deletebreak_down_tr(rowNo,table_id) 
{   
	
	if(table_id=='tbl_fabric_cost')
	{
		//var numRow = $('table#tbl_fabric_cost tbody tr').length; 
		/*if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_fabric_cost tbody tr:last').remove();
		}*/
		if(rowNo!=1)
		{
			var permission_array=permission.split("_");
			var updateid=$('#updateid_'+rowNo).val();
			if(updateid !="" && permission_array[2]==1)
			{
			var booking=return_global_ajax_value(updateid, 'delete_row_fabric_cost', '', 'requires/quotation_entry_controller');
			}
			var index=rowNo-1
			$("table#tbl_fabric_cost tbody tr:eq("+index+")").remove()
			var numRow = $('table#tbl_fabric_cost tbody tr').length; 
			for(i = rowNo;i <= numRow;i++)
			{
				$("#tbl_fabric_cost tr:eq("+i+")").find("input,select").each(function() {
						$(this).attr({
							'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
							//'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i},
							'value': function(_, value) { return value }             
						}); 
						
					  $("#tbl_fabric_cost tr:last").removeAttr('id').attr('id','fabriccosttbltr_'+i);
					  $('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
					  $('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_fabric_cost');");
					  $('#txtgsmweight_'+i).removeAttr("onBlur").attr("onBlur","sum_yarn_required()");
					  $('#fabricdescription_'+i).removeAttr("onDblClick").attr("onDblClick","open_fabric_decription_popup("+i+")");
					  $('#txtconsumption_'+i).removeAttr("onBlur").attr("onBlur","math_operation( 'txtamount_"+i+"', 'txtconsumption_"+i+"*txtrate_"+i+"', '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );sum_yarn_required( );set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost')");
					  $('#txtconsumption_'+i).removeAttr("onClick").attr("onClick","open_consumption_popup( 'requires/quotation_entry_controller.php?action=consumption_popup', 'Consumtion Entry Form', 'txtbodypart_"+i+"', 'cbofabricnature_"+i+"','txtgsmweight_"+i+"','"+i+"','updateid_"+i+"')");
					  $('#cbofabricsource_'+i).removeAttr("onChange").attr("onChange","enable_disable( this.value,'txtrate_*txtamount_', "+i+")");
					  $('#txtrate_'+i).removeAttr("onBlur").attr("onBlur","math_operation( 'txtamount_"+i+"', 'txtconsumption_"+i+"*txtrate_"+i+"', '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost')");
					  $('#txtamount_'+i).removeAttr("onBlur").attr("onBlur","set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost')");
				})
			}
		} 
		sum_yarn_required()
		set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost');
	}
	
	if(table_id=='tbl_yarn_cost')
	{
		if(rowNo!=1)
		{
			var permission_array=permission.split("_");
			var updateid=$('#updateidyarncost_'+rowNo).val();
			if(updateid !="" && permission_array[2]==1)
			{
			var booking=return_global_ajax_value(updateid, 'delete_row_yarn_cost', '', 'requires/quotation_entry_controller');
			}
			var index=rowNo-1
			$("table#tbl_yarn_cost tbody tr:eq("+index+")").remove()
			var numRow = $('table#tbl_yarn_cost tbody tr').length; 
			for(i = rowNo;i <= numRow;i++)
			{
				$("#tbl_yarn_cost tr:eq("+i+")").find("input,select").each(function() {
						$(this).attr({
							'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
							'value': function(_, value) { return value }             
						}); 
						
					  $('#increaseyarn_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_yarn_cost("+i+");");
					  $('#decreaseyarn_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_yarn_cost');");
					  $('#percentone_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'percent_one');");
					  $('#cbocompone_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'comp_one');");
					  $('#percenttwo_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'percent_two');");
					  $('#cbocomptwo_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'comp_two');");
					  $('#consratio_'+i).removeAttr("onChange").attr("onChange","calculate_yarn_consumption_ratio('consratio_"+i+"','consqnty_"+i+"','txtrateyarn_"+i+"','txtamountyarn_"+i+"','calculate_consumption')");
					  $('#consqnty_'+i).removeAttr("onChange").attr("onChange","calculate_yarn_consumption_ratio('consratio_"+i+"','consqnty_"+i+"','txtrateyarn_"+i+"','txtamountyarn_"+i+"','calculate_ratio')");
					  $('#txtrateyarn_'+i).removeAttr("onChange").attr("onChange","calculate_yarn_consumption_ratio('consratio_"+i+"','consqnty_"+i+"','txtrateyarn_"+i+"','txtamountyarn_"+i+"','calculate_amount')");
				})
			}
		}
		set_sum_value( 'txtconsratio_sum', 'consratio_', 'tbl_yarn_cost' );
	    set_sum_value( 'txtconsumptionyarn_sum', 'consqnty_', 'tbl_yarn_cost' );
	    set_sum_value( 'txtamountyarn_sum', 'txtamountyarn_', 'tbl_yarn_cost' );
	}
	
	if(table_id=='tbl_conversion_cost')
	{
		//var numRow = $('table#tbl_conversion_cost tbody tr').length; 
		/*if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_conversion_cost tbody tr:last').remove();
		}*/
		var conversion_from_chart=return_global_ajax_value(document.getElementById('cbo_company_name').value, 'conversion_from_chart', '', 'requires/quotation_entry_controller');
		//alert(conversion_from_chart)
		//return;
		if(rowNo!=1)
		{
			var permission_array=permission.split("_");
			var updateid=$('#updateidcoversion_'+rowNo).val();
			if(updateid !="" && permission_array[2]==1)
			{
			var booking=return_global_ajax_value(updateid, 'delete_row_conversion_cost', '', 'requires/quotation_entry_controller');
			}
			var index=rowNo-1
			$("table#tbl_conversion_cost tbody tr:eq("+index+")").remove()
			var numRow = $('table#tbl_conversion_cost tbody tr').length; 
			for(i = rowNo;i <= numRow;i++)
			{
				$("#tbl_conversion_cost tr:eq("+i+")").find("input,select").each(function() {
						$(this).attr({
							'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
							//'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i},
							'value': function(_, value) { return value }             
						}); 
						
					  $('#cbocosthead_'+i).removeAttr("onChange").attr("onChange","set_conversion_qnty("+i+")");
					  //$('#cbotypeconversion_'+i).removeAttr("onChange").attr("onChange","set_conversion_charge_unit(this.value,'txtchargeunit_', "+i+")");
					  $('#increaseconversion_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_conversion_cost("+i+","+conversion_from_chart+");");
					  $('#decreaseconversion_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_conversion_cost');");
					  $('#txtreqqnty_'+i).removeAttr("onChange").attr("onChange","calculate_conversion_cost( "+i+" )");
					  $('#txtchargeunit_'+i).removeAttr("onChange").attr("onChange","calculate_conversion_cost( "+i+" )");
					  if(conversion_from_chart==1)
					  {
						 $('#cbotypeconversion_'+i).removeAttr("onChange").attr("onChange","set_conversion_charge_unit_pop_up("+i+")");
						 $('#txtchargeunit_'+i).removeAttr("onClick").attr("onClick","set_conversion_charge_unit_pop_up("+i+")");  
					  }
				})
			}
		}
		  set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
	      set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
	      set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
	}
	
	if(table_id=='tbl_trim_cost')
	{
		/*var numRow = $('table#tbl_trim_cost tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_trim_cost tbody tr:last').remove();
		}*/
		if(rowNo!=1)
		{
			var permission_array=permission.split("_");
			var updateid=$('#updateidtrim_'+rowNo).val();
			if(updateid !="" && permission_array[2]==1)
			{
			var booking=return_global_ajax_value(updateid, 'delete_row_trim_cost', '', 'requires/quotation_entry_controller');
			}
			var index=rowNo-1
			$("table#tbl_trim_cost tbody tr:eq("+index+")").remove()
			var numRow = $('table#tbl_trim_cost tbody tr').length; 
			for(i = rowNo;i <= numRow;i++)
			{
				$("#tbl_trim_cost tr:eq("+i+")").find("input,select").each(function() {
						$(this).attr({
							'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
							//'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i},
							'value': function(_, value) { return value }             
						}); 
						
					  $('#cbogroup_'+i).removeAttr("onChange").attr("onChange","set_trim_cons_uom( this.value,"+i+" )");
					  $('#increasetrim_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_trim_cost("+i+");");
					  $('#decreasetrim_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_trim_cost');");
					  $('#txtconsdzngmts_'+i).removeAttr("onChange").attr("onChange","calculate_trim_cost( "+i+" )");
					 // $('#txtconsdzngmts_'+i).removeAttr("onDblClick").attr("onDblClick","open_calculator( "+i+" )");
					  $('#txttrimrate_'+i).removeAttr("onChange").attr("onChange","calculate_trim_cost( "+i+" )");
				})
			}
		}
		 set_sum_value( 'txtconsdzntrim_sum', 'txtconsdzngmts_', 'tbl_trim_cost' );
	     set_sum_value( 'txtratetrim_sum', 'txttrimrate_', 'tbl_trim_cost' );
	     set_sum_value( 'txttrimamount_sum', 'txttrimamount_', 'tbl_trim_cost' );
	}
	
	if(table_id=='tbl_embellishment_cost')
	{
		/*var numRow = $('table#tbl_embellishment_cost tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_embellishment_cost tbody tr:last').remove();
		}*/
		if(rowNo!=1)
		{
			var permission_array=permission.split("_");
			var updateid=$('#embupdateid_'+rowNo).val();
			if(updateid !="" && permission_array[2]==1)
			{
			var booking=return_global_ajax_value(updateid, 'delete_row_embellishment_cost', '', 'requires/quotation_entry_controller');
			}
			var index=rowNo-1
			$("table#tbl_embellishment_cost tbody tr:eq("+index+")").remove()
			var numRow = $('table#tbl_embellishment_cost tbody tr').length; 
			for(i = rowNo;i <= numRow;i++)
			{
				$("#tbl_embellishment_cost tr:eq("+i+")").find("input,select").each(function() {
						$(this).attr({
							'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
							//'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i},
							'value': function(_, value) { return value }             
						}); 
						
					  $('#increaseemb_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_embellishment_cost("+i+");");
					  $('#decreaseemb_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_embellishment_cost');");
					  $('#txtembconsdzngmts_'+i).removeAttr("onChange").attr("onChange","calculate_emb_cost( "+i+" )");
					  $('#txtembrate_'+i).removeAttr("onChange").attr("onChange","calculate_emb_cost( "+i+" )");
					  $('#cboembname_'+i).removeAttr("onChange").attr("onChange","cbotype_loder( "+i+" )");
					  $('#cboembtype_'+i).removeAttr("onChange").attr("onChange","check_duplicate( "+i+" )");
				})
			}
		}
	    set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_embellishment_cost' );
		calculate_main_total()
	}
	
	if(table_id=='tbl_wash_cost')
	{
				var conversion_from_chart=return_global_ajax_value(document.getElementById('cbo_company_name').value, 'conversion_from_chart', '', 'requires/quotation_entry_controller');

		/*var numRow = $('table#tbl_wash_cost tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_wash_cost tbody tr:last').remove();
		}*/
		if(rowNo!=1)
		{
			var permission_array=permission.split("_");
			var updateid=$('#embupdateid_'+rowNo).val();
			if(updateid !="" && permission_array[2]==1)
			{
			var booking=return_global_ajax_value(updateid, 'delete_row_wash_cost', '', 'requires/quotation_entry_controller');
			}
			var index=rowNo-1
			$("table#tbl_wash_cost tbody tr:eq("+index+")").remove()
			var numRow = $('table#tbl_wash_cost tbody tr').length; 
			for(i = rowNo;i <= numRow;i++)
			{
				$("#tbl_wash_cost tr:eq("+i+")").find("input,select").each(function() {
						$(this).attr({
							'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
							//'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i},
							'value': function(_, value) { return value }             
						}); 
						
					  
					  
					  $('#increaseemb_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_wash_cost("+i+");");
					  $('#decreaseemb_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_wash_cost');");
					  $('#txtembconsdzngmts_'+i).removeAttr("onChange").attr("onChange","calculate_wash_cost( "+i+" )");
					  $('#txtembrate_'+i).removeAttr("onChange").attr("onChange","calculate_wash_cost( "+i+" )");
					  if(conversion_from_chart==1)
					  {
						  $('#txtembrate_'+i).removeAttr("onClick").attr("onClick","set_wash_charge_unit_pop_up( "+i+" )");
					  }
					  $('#cboembtype_'+i).removeAttr("onChange").attr("onChange","check_duplicate( "+i+" )");
				})
			}
		}
	    set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_wash_cost' );
		calculate_main_total()
	}
	if(table_id=='tbl_commission_cost')
	{
		var numRow = $('table#tbl_commission_cost tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_commission_cost tbody tr:last').remove();
		}
		/*else
		{
																																																																																																																																																																																																																																																																																																																																									reset_form('','','txtordernumber_'+rowNo+'*txtorderqnty_'+rowNo+'*txtordervalue_'+rowNo+'*txtattachedqnty_'+rowNo+'*txtattachedvalue_'+rowNo+'*txtstyleref_'+rowNo+'*txtitemname_'+rowNo+'*txtjobno_'+rowNo+'*hiddenwopobreakdownid_'+rowNo+'*hiddenunitprice_'+rowNo+'*totalOrderqnty*totalOrdervalue*totalAttachedqnty*totalAttachedvalue');
		} */
		set_sum_value( 'txtratecommission_sum', 'txtcommissionrate_', 'tbl_commission_cost' );
	    set_sum_value( 'txtamountcommission_sum', 'txtcommissionamount_', 'tbl_commission_cost' );
		calculate_main_total()

	}
	if(table_id=='tbl_comarcial_cost')
	{
		/*var numRow = $('table#tbl_comarcial_cost tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_comarcial_cost tbody tr:last').remove();
		}*/
		if(rowNo!=1)
		{
			var permission_array=permission.split("_");
			var updateid=$('#comarcialupdateid_'+rowNo).val();
			if(updateid !="" && permission_array[2]==1)
			{
			var booking=return_global_ajax_value(updateid, 'delete_row_comarcial_cost', '', 'requires/quotation_entry_controller');
			}
			var index=rowNo-1
			$("table#tbl_comarcial_cost tbody tr:eq("+index+")").remove()
			var numRow = $('table#tbl_comarcial_cost tbody tr').length; 
			for(i = rowNo;i <= numRow;i++)
			{
				$("#tbl_comarcial_cost tr:eq("+i+")").find("input,select").each(function() {
						$(this).attr({
							'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
							//'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i},
							'value': function(_, value) { return value }             
						}); 
						
					  
					  
					  $('#increasecomarcial_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_comarcial_cost("+i+");");
					  $('#decreasecomarcial_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_comarcial_cost');");
					  $('#txtcomarcialrate_'+i).removeAttr("onChange").attr("onChange","calculate_comarcial_cost( "+i+",'rate' )");
					  $('#txtcomarcialamount_'+i).removeAttr("onChange").attr("onChange","calculate_comarcial_cost( "+i+",'amount' )");
					  
					 // $('#cbocomarcialbase_'+i).removeAttr("onChange").attr("onChange","calculate_comarcial_cost( "+i+" )");
			
					  //$('#txtembrate_'+i).removeAttr("onChange").attr("onChange","calculate_emb_cost( "+i+" )");
				})
			}
		}
		set_sum_value( 'txtratecomarcial_sum', 'txtcomarcialrate_', 'tbl_comarcial_cost' );
	    set_sum_value( 'txtamountcomarcial_sum', 'txtcomarcialamount_', 'tbl_comarcial_cost' );
	}
}



	
function show_hide_content(row, id) 
	{
		$('#content_'+row).toggle('slow', function() {
			 //get_php_form_data( id, 'set_php_form_data', '../woven_order/requires/size_color_breakdown_controller' );
		});
	}
	


function enable_disable(value,fld_arry, i)
{
	var fld_arry=fld_arry.split('*');
	if(value==2)
	{
		for(var j=0;j<fld_arry.length;j++)
		{
		document.getElementById(fld_arry[j]+i).disabled=false;
		}
	}
	else
	{
		for(var j=0;j<fld_arry.length;j++)
		{
		document.getElementById(fld_arry[j]+i).disabled=true;
		document.getElementById(fld_arry[j]+i).value="";
 set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost');
		}
	}
}
// Common For All End ----------------------------------------------------
// Start Fabric cost------------------------------------------------------

function add_break_down_tr(i) 
 {
	var row_num=$('#tbl_fabric_cost tr').length-1;
	if (i==0)
	{
		i=1;
		 $("#txtconstruction_"+i).autocomplete({
			 source: str_construction
		  });
		   $("#txtcomposition_"+i).autocomplete({
			 source:  str_composition 
		  }); 
		  return;
	}
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
	 
		 $("#tbl_fabric_cost tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_fabric_cost");
		  $("#tbl_fabric_cost tr:last").removeAttr('id').attr('id','fabriccosttbltr_'+i);
		  $('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
		  $('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_fabric_cost');");
		  $('#txtgsmweight_'+i).removeAttr("onBlur").attr("onBlur","sum_yarn_required()");
		  $('#fabricdescription_'+i).removeAttr("onDblClick").attr("onDblClick","open_fabric_decription_popup("+i+")");
		  $('#txtconsumption_'+i).removeAttr("onBlur").attr("onBlur","math_operation( 'txtamount_"+i+"', 'txtconsumption_"+i+"*txtrate_"+i+"', '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );sum_yarn_required( );set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost')");
		  $('#txtconsumption_'+i).removeAttr("onClick").attr("onClick","open_consumption_popup( 'requires/quotation_entry_controller.php?action=consumption_popup', 'Consumtion Entry Form', 'txtbodypart_"+i+"', 'cbofabricnature_"+i+"','txtgsmweight_"+i+"','"+i+"','updateid_"+i+"')");
		  $('#cbofabricsource_'+i).removeAttr("onChange").attr("onChange","enable_disable( this.value,'txtrate_*txtamount_', "+i+")");
		  $('#txtrate_'+i).removeAttr("onBlur").attr("onBlur","math_operation( 'txtamount_"+i+"', 'txtconsumption_"+i+"*txtrate_"+i+"', '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost')");
		  $('#txtamount_'+i).removeAttr("onBlur").attr("onBlur","set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost')");
		  var j=i-1;
		  $('#cbogmtsitem_'+i).val($('#cbogmtsitem_'+j).val()); 
		  $('#cbofabricnature_'+i).val($('#cbofabricnature_'+j).val());
		  $('#cbocolortype_'+i).val($('#cbocolortype_'+j).val());
		  $('#cbofabricsource_'+i).val($('#cbofabricsource_'+j).val());
		  $('#cbostatus_'+i).val($('#cbostatus_'+j).val());
		  $('#txtconsumption_'+i).val("");
		  $('#txtrate_'+i).val("");
		  $('#txtamount_'+i).val("");
		  $('#consbreckdown_'+i).val("");
		  $('#msmntbreackdown_'+i).val("");
		  $('#updateid_'+i).val("");
		  $('#processlossmethod_'+i).val(""); 
		  $('#txtfinishconsumption_'+i).val("");
		  $('#txtavgprocessloss_'+i).val("");
		  $('#txtbodypart_'+i).val("");
		  $('#txtgsmweight_'+i).val("");
		  set_all_onclick();
		  sum_yarn_required()
		  set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost');
		  $("#txtconstruction_"+i).autocomplete({
			 source: str_construction
		  });
		   $("#txtcomposition_"+i).autocomplete({
			 source: str_composition 
		  });  
	}
}

function open_fabric_decription_popup(i)
{
	var cbofabricnature=document.getElementById('cbofabricnature_'+i).value;
	var libyarncountdeterminationid =document.getElementById('libyarncountdeterminationid_'+i).value
	var page_link='requires/quotation_entry_controller.php?action=fabric_description_popup&fabric_nature='+cbofabricnature+'&libyarncountdeterminationid='+libyarncountdeterminationid;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Fabric Description', 'width=1060px,height=450px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
		{
			var fab_des_id=this.contentDoc.getElementById("fab_des_id");
			var fab_nature_id=this.contentDoc.getElementById("fab_nature_id");
			var fab_desctiption=this.contentDoc.getElementById("fab_desctiption");
			var fab_gsm=this.contentDoc.getElementById("fab_gsm");
			var yarn_desctiption=this.contentDoc.getElementById("yarn_desctiption");
			var construction=this.contentDoc.getElementById("construction");
			var composition=this.contentDoc.getElementById("composition");
			document.getElementById('libyarncountdeterminationid_'+i).value=fab_des_id.value;
			document.getElementById('fabricdescription_'+i).value=fab_desctiption.value;
			document.getElementById('fabricdescription_'+i).title=fab_desctiption.value;
			document.getElementById('cbofabricnature_'+i).value=fab_nature_id.value;
			document.getElementById('txtgsmweight_'+i).value=fab_gsm.value;
			document.getElementById('yarnbreackdown_'+i).value=yarn_desctiption.value;
			document.getElementById('txtconstruction_'+i).value=construction.value;
			document.getElementById('txtcomposition_'+i).value=composition.value;
		}
}

function open_consumption_popup(page_link,title,body_part_id,cbofabricnature_id,txtgsmweight_id,trorder,updateid_fc)
{
	//alert('MOnzu')
	var cbo_company_id=document.getElementById('cbo_company_name').value;
    var cbo_costing_per=document.getElementById('cbo_costing_per').value;
	var hid_fab_cons_in_quotation_variable =document.getElementById('consumptionbasis_'+trorder).value;
	var body_part_id =document.getElementById(body_part_id).value;
	var txtgsmweight=document.getElementById(txtgsmweight_id).value;
	var cbofabricnature_id =document.getElementById(cbofabricnature_id).value;
	var cons_breck_downn=document.getElementById('consbreckdown_'+trorder).value;
	var msmnt_breack_downn=document.getElementById('msmntbreackdown_'+trorder).value;
    var marker_breack_down=document.getElementById('markerbreackdown_'+trorder).value;
    var calculated_conss=document.getElementById('txtconsumption_'+trorder).value;
	var garments_nature = document.getElementById('garments_nature').value;

	document.getElementById('tr_ortder').value=trorder;
	if(body_part_id==0 )
	{
		alert("Select Body Part Id")
	}
	else if(cbofabricnature_id==0 )
	{
		alert("Select Fabric Nature Id");
	}
	else
	{
		var page_link=page_link+'&body_part_id='+body_part_id+'&cbo_costing_per='+cbo_costing_per+'&cbo_company_id='+cbo_company_id+'&cbofabricnature_id='+cbofabricnature_id+'&cons_breck_downn='+cons_breck_downn+'&msmnt_breack_downn='+msmnt_breack_downn+'&calculated_conss='+calculated_conss+'&hid_fab_cons_in_quotation_variable='+hid_fab_cons_in_quotation_variable+'&txtgsmweight='+txtgsmweight+'&garments_nature='+garments_nature+'&marker_breack_down='+marker_breack_down;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=860px,height=450px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var trorder= document.getElementById('tr_ortder').value;
			var cons_breck_down=this.contentDoc.getElementById("cons_breck_down");
			var msmnt_breack_down=this.contentDoc.getElementById("msmnt_breack_down");
            var marker_breack_down=this.contentDoc.getElementById("marker_breack_down");
            var calculated_cons=this.contentDoc.getElementById("calculated_cons");
			var finish_avg_cons=this.contentDoc.getElementById("avg_cons");
			var avg_process_loss=this.contentDoc.getElementById("calculated_procloss");
			var process_loss_method_id=this.contentDoc.getElementById("process_loss_method_id");
			
			document.getElementById('txtconsumption_'+trorder).value=calculated_cons.value;
			document.getElementById('txtfinishconsumption_'+trorder).value=finish_avg_cons.value;
			document.getElementById('txtavgprocessloss_'+trorder).value=avg_process_loss.value;
			document.getElementById('processlossmethod_'+trorder).value=process_loss_method_id.value;
			document.getElementById('consbreckdown_'+trorder).value=cons_breck_down.value;
			document.getElementById('msmntbreackdown_'+trorder).value=msmnt_breack_down.value;
			document.getElementById('markerbreackdown_'+trorder).value=marker_breack_down.value;
			math_operation( 'txtamount_'+trorder, 'txtconsumption_'+trorder+'*'+'txtrate_'+trorder, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
			sum_yarn_required()
			set_sum_value( 'txtamount_sum', 'txtamount_','tbl_fabric_cost')
			update_related_data(1)
		}	
	}
}

function change_caption( value, td_id )
{
	if(value==2)
	{
		document.getElementById(td_id).innerHTML="GSM";
	}
	else
	{
		document.getElementById(td_id).innerHTML="Yarn Weight";
	}
	
}
function sum_yarn_required()
{
	    var row_num=$('#tbl_fabric_cost tr').length-1;
		
		var yarn_for_knit=0;
		var yarn_for_woven=0;
		var total_knit_fabric_required=0;
		var total_woven_fabric_required=0;
		for (var i=1; i<=row_num; i++)
		{
			var cbofabricnature=document.getElementById('cbofabricnature_'+i).value
			var cbofabricsource=document.getElementById('cbofabricsource_'+i).value
			if(cbofabricnature==2 && cbofabricsource==1)
			{
				yarn_for_knit=yarn_for_knit+(document.getElementById('txtconsumption_'+i).value)*1
			}
			if(cbofabricnature==3 && cbofabricsource==1)
			{
				yarn_for_woven=yarn_for_woven+(document.getElementById('txtgsmweight_'+i).value)*1
			}
			if(cbofabricnature==2)
			{
				total_knit_fabric_required=total_knit_fabric_required+(document.getElementById('txtconsumption_'+i).value)*1
			}
			if(cbofabricnature==3)
			{
				total_woven_fabric_required=total_woven_fabric_required+(document.getElementById('txtconsumption_'+i).value)*1
			}
			
		}
		document.getElementById('tot_yarn_needed').value=yarn_for_woven+yarn_for_knit; 
		document.getElementById('tot_yarn_needed_span').innerHTML=yarn_for_woven+yarn_for_knit;
		//document.getElementById('consqnty_1').value=yarn_for_woven+yarn_for_knit;

		document.getElementById('txtwoven_sum').value=total_woven_fabric_required; 
		document.getElementById('txtknit_sum').value=total_knit_fabric_required; 

        //update_related_data(1)
		
	
}

function fnc_fabric_cost_dtls( operation )
{
		if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	  var bgcolor='-moz-linear-gradient(bottom, rgb(254,151,174) 0%, rgb(255,255,255) 10%, rgb(254,151,174) 96%)';

	    var row_num=$('#tbl_fabric_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('cbogmtsitem_'+i+'*txtbodypart_'+i+'*cbofabricnature_'+i+'*cbocolortype_'+i+'*txtconstruction_'+i+'*txtcomposition_'+i+'*txtconsumption_'+i+'*cbofabricsource_'+i,'Gmts Item *Body Part*Fabric Nature*Color Type*Construction*Composition*Consunption*Fabric Source')==false)
			{
				return;
			}
			
			if ( $('#cbofabricnature_'+i).val()=='3' && $('#cbofabricsource_'+i).val()=='1' &&  (form_validation('txtgsmweight_'+i,'Yarn Weight')==false || $('#txtgsmweight_'+i).val()=='0') )
			{
			 document.getElementById('txtgsmweight_'+i).focus();
	  		 document.getElementById('txtgsmweight_'+i).style.backgroundImage=bgcolor;
			 $('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			 { 
				$(this).html('Please Fill up Yarn Weight field Value').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			 });
			 
				return;
			}
			
			if ($('#cbofabricsource_'+i).val()=='2' &&  (form_validation('txtrate_'+i,'Rate')==false || $('#txtrate_'+i).val()=='0') )
			{
			 document.getElementById('txtrate_'+i).focus();
	  		 document.getElementById('txtrate_'+i).style.backgroundImage=bgcolor;
			 $('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			 { 
				$(this).html('Please Fill up Rate field Value').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			 });
			 
				return;
			}
			
			if ($('#cbofabricsource_'+i).val()=='2' &&  (form_validation('txtamount_'+i,'Amount')==false || $('#txtamount_'+i).val()=='0') )
			{
			 document.getElementById('txtamount_'+i).focus();
	  		 document.getElementById('txtamount_'+i).style.backgroundImage=bgcolor;
			 $('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			 { 
				$(this).html('Please Fill up Amount field Value').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			 });
			 
				return;
			}
			
			//eval(get_submitted_variables('cbo_company_name*cbo_costing_per*consumptionbasis_'+i+'*update_id*cbogmtsitem_'+i+'*txtbodypart_'+i+'*cbofabricnature_'+i+'*cbocolortype_'+i+'*txtconstruction_'+i+'*txtcomposition_'+i+'*txtgsmweight_'+i+'*txtconsumption_'+i+'*cbofabricsource_'+i+'*txtrate_'+i+'*txtamount_'+i+'*txtfinishconsumption_'+i+'*txtavgprocessloss_'+i+'*cbostatus_'+i+'*consbreckdown_'+i+'*msmntbreackdown_'+i+'*updateid_'+i+'*processlossmethod_'+i+'*tot_yarn_needed*txtwoven_sum*txtknit_sum*txtamount_sum'));
			
			data_all=data_all+get_submitted_data_string('cbo_company_name*cbo_costing_per*consumptionbasis_'+i+'*update_id*cbogmtsitem_'+i+'*txtbodypart_'+i+'*cbofabricnature_'+i+'*cbocolortype_'+i+'*libyarncountdeterminationid_'+i+'*txtconstruction_'+i+'*txtcomposition_'+i+'*fabricdescription_'+i+'*txtgsmweight_'+i+'*txtconsumption_'+i+'*cbofabricsource_'+i+'*txtrate_'+i+'*txtamount_'+i+'*txtfinishconsumption_'+i+'*txtavgprocessloss_'+i+'*cbostatus_'+i+'*consbreckdown_'+i+'*msmntbreackdown_'+i+'*yarnbreackdown_'+i+'*updateid_'+i+'*cbowidthdiatype_'+i+'*markerbreackdown_'+i+'*processlossmethod_'+i+'*tot_yarn_needed*txtwoven_sum*txtknit_sum*txtamount_sum',"../../");
		}
		var data="action=save_update_delet_fabric_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_cost_dtls_reponse;
	
}

function fnc_fabric_cost_dtls_reponse()
{ 	
    var reponse=trim(http.responseText).split('**');
	if(reponse[0]==15) 
	{ 
		 setTimeout('fnc_fabric_cost_dtls('+ reponse[1]+')',8000); 
	}
	if(http.readyState == 4) 
	{
	   // var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		show_sub_form(document.getElementById('update_id').value, 'show_fabric_cost_listview');
		show_hide_content('fabric_cost', '') 
		/*if(reponse[0]==1)
		{
			update_related_data(reponse[0])
		}*/
		if(reponse[0]==0 || reponse[0]==1)
		{
			    update_related_data(reponse[0])
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
	}
}

function update_related_data(operation)
{
	$('#accordion_h_yarn').click();
	 var row_num=$('#tbl_yarn_cost tr').length-1;
	for(var i=1; i<=row_num; i++)
	{
	calculate_yarn_consumption_ratio('consratio_'+i,'consqnty_'+i,'txtrateyarn_'+i,'txtamountyarn_'+i,'calculate_consumption')
	}
	$('#accordion_h_conversion').click();
	var row_num=$('#tbl_conversion_cost tr').length-1;
	//alert(row_num)
	for(var i=1; i<=row_num; i++)
	{
		
	set_conversion_qnty(i)
	}
	
}
// End Fabric cost------------------------------------------------------
// Start Yarn Cost------------------------------------------------------
function add_break_down_tr_yarn_cost( i )
{
	var row_num=$('#tbl_yarn_cost tr').length-1;
	if (i==0)
	{
		i=1;
		 $("#txtconstruction_"+i).autocomplete({
			 source: str_construction
		  });
		   $("#txtcomposition_"+i).autocomplete({
			 source:  str_composition 
		  }); 
		  return;
	}
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_yarn_cost tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_yarn_cost");
		  
		  $('#increaseyarn_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_yarn_cost("+i+");");
		  $('#decreaseyarn_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_yarn_cost');");
		  
		  $('#percentone_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'percent_one');");
		  $('#cbocompone_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'comp_one');");
		  $('#percenttwo_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'percent_two');");
		  $('#cbocomptwo_'+i).removeAttr("onChange").attr("onChange","control_composition("+i+",this.id,'comp_two');");
		  
		  $('#consratio_'+i).removeAttr("onChange").attr("onChange","calculate_yarn_consumption_ratio('consratio_"+i+"','consqnty_"+i+"','txtrateyarn_"+i+"','txtamountyarn_"+i+"','calculate_consumption')");
		  $('#consqnty_'+i).removeAttr("onChange").attr("onChange","calculate_yarn_consumption_ratio('consratio_"+i+"','consqnty_"+i+"','txtrateyarn_"+i+"','txtamountyarn_"+i+"','calculate_ratio')");
		  $('#txtrateyarn_'+i).removeAttr("onChange").attr("onChange","calculate_yarn_consumption_ratio('consratio_"+i+"','consqnty_"+i+"','txtrateyarn_"+i+"','txtamountyarn_"+i+"','calculate_amount')");
		  $('#updateidyarncost_'+i).val("");
		  set_sum_value( 'txtconsratio_sum', 'consratio_', 'tbl_yarn_cost' );
	      set_sum_value( 'txtconsumptionyarn_sum', 'consqnty_', 'tbl_yarn_cost' );
	      set_sum_value( 'txtamountyarn_sum', 'txtamountyarn_', 'tbl_yarn_cost' );
	}
	
}

function control_composition(id,td,type)
{
	var cbocompone=(document.getElementById('cbocompone_'+id).value);
	var cbocomptwo=(document.getElementById('cbocomptwo_'+id).value);
	var percentone=(document.getElementById('percentone_'+id).value)*1;
	var percenttwo=(document.getElementById('percenttwo_'+id).value)*1;
	var row_num=$('#tbl_yarn_cost tr').length-1;
	
	if(type=='percent_one' && percentone>100)
	{
		alert("Greater Than 100 Not Allwed")
		document.getElementById('percentone_'+id).value="";
	}
	
	if(type=='percent_one' && percentone<=0)
	{
		alert("0 Or Less Than 0 Not Allwed")
		document.getElementById('percentone_'+id).value="";
		document.getElementById('percentone_'+id).disabled=true;
		document.getElementById('cbocompone_'+id).value=0;
		document.getElementById('cbocompone_'+id).disabled=true;
		document.getElementById('percenttwo_'+id).value=100;
	    document.getElementById('percenttwo_'+id).disabled=false;
		document.getElementById('cbocomptwo_'+id).disabled=false;

	}
	if(type=='percent_one' && percentone==100)
	{
		document.getElementById('percenttwo_'+id).value="";
		document.getElementById('cbocomptwo_'+id).value=0;
		document.getElementById('percenttwo_'+id).disabled=true;
		document.getElementById('cbocomptwo_'+id).disabled=true;

	}
	
	if(type=='percent_one' && percentone < 100 && percentone > 0 )
	{
		document.getElementById('percenttwo_'+id).value=100-percentone;
	    document.getElementById('percenttwo_'+id).disabled=false;
		document.getElementById('cbocomptwo_'+id).disabled=false;
		//document.getElementById('cbocomptwo_'+id).value=0;
	}
	
	if(type=='comp_one' && cbocompone==cbocomptwo  )
	{
		alert("Same Composition Not Allowed");
		document.getElementById('cbocompone_'+id).value=0;
		//document.getElementById('percenttwo_'+id).value=100-percentone;
		//document.getElementById('cbocomptwo_'+id).value=0;
	}
	
	
	
	
	
	if(type=='percent_two' && percenttwo>100)
	{
		alert("Greater Than 100 Not Allwed")
		document.getElementById('percenttwo_'+id).value="";
		//document.getElementById('cbocompone_'+id).value=0;
	}
	if(type=='percent_two' && percenttwo<=0)
	{
		alert("0 Or Less Than 0 Not Allwed")
		document.getElementById('percenttwo_'+id).value="";
		document.getElementById('percenttwo_'+id).disabled=true;
		document.getElementById('cbocomptwo_'+id).value=0;
		document.getElementById('cbocomptwo_'+id).disabled=true;
		document.getElementById('percentone_'+id).value=100;
		document.getElementById('percentone_'+id).disabled=false;
		document.getElementById('cbocompone_'+id).disabled=false;
	}
	if(type=='percent_two' && percenttwo==100)
	{
		document.getElementById('percentone_'+id).value="";
		document.getElementById('cbocompone_'+id).value=0;
		document.getElementById('percentone_'+id).disabled=true;
		document.getElementById('cbocompone_'+id).disabled=true;
	}
	
	if(type=='percent_two' && percenttwo<100 && percenttwo>0)
	{
		document.getElementById('percentone_'+id).value=100-percenttwo;
		document.getElementById('percentone_'+id).disabled=false;
		document.getElementById('cbocompone_'+id).disabled=false;

		//document.getElementById('cbocompone_'+id).value=0;
	}
	
	if(type=='comp_two' && cbocomptwo==cbocompone)
	{
		alert("Same Composition Not Allowed");
		document.getElementById('cbocomptwo_'+id).value=0;
		//document.getElementById('percentone_'+id).value=100-percenttwo;
		//document.getElementById('cbocompone_'+id).value=0;
	}
	
	/*for (var k=1;k<=row_num; k++)
	{
		if(k==id)
		{
		continue;
		}
		else
		{
			if(item_id==document.getElementById('cbogmtsitem_'+k).value && txtcolor==document.getElementById('txtcolor_'+k).value && txtsize==document.getElementById('txtsize_'+k).value)
			{
				alert("Same Gmts Item, Same Color and Same Size Duplication Not Allowed.");
				document.getElementById(td).value="";
				document.getElementById(td).focus();
			}
			
		}
		
		
	}*/
}

function calculate_yarn_consumption_ratio(consratio_id,consqnty_id,txtrateyarn_id,txtamountyarn_id,type)
{
	var tot_yarn_needed=document.getElementById('tot_yarn_needed').value*1
	if(type=='calculate_consumption')
	{
	 var consratio=document.getElementById(consratio_id).value*1
	 var consqnty=(tot_yarn_needed*consratio)/100;
	 document.getElementById(consqnty_id).value=consqnty;
	}
	if(type=='calculate_ratio')
	{
	 var consqnty=document.getElementById(consqnty_id).value*1
	 var consratio=(consqnty/tot_yarn_needed)*100;
	 document.getElementById(consratio_id).value=consratio;
	}
	if(type=='calculate_consumption' || type=='calculate_ratio' || type=='calculate_amount' )
	{
	document.getElementById(txtamountyarn_id).value=number_format_common((document.getElementById(consqnty_id).value*1)*(document.getElementById(txtrateyarn_id).value*1),1,0,document.getElementById('cbo_currercy').value);
	}
	set_sum_value( 'txtconsratio_sum', 'consratio_', 'tbl_yarn_cost' );
	set_sum_value( 'txtconsumptionyarn_sum', 'consqnty_', 'tbl_yarn_cost' );
	set_sum_value( 'txtamountyarn_sum', 'txtamountyarn_', 'tbl_yarn_cost' );
	
}

function fnc_fabric_yarn_cost_dtls( operation )
{
	    if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	    var row_num=$('#tbl_yarn_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('update_id','Company Name')==false)
			{
				return;
			}
			
			eval(get_submitted_variables('update_id*cbocount_'+i+'*cbocompone_'+i+'*percentone_'+i+'*cbocomptwo_'+i+'*percenttwo_'+i+'*cbotype_'+i+'*consratio_'+i+'*consqnty_'+i+'*txtrateyarn_'+i+'*txtamountyarn_'+i+'*cbostatusyarn_'+i+'*updateidyarncost_'+i+'*txtconsumptionyarn_sum*txtamountyarn_sum'));
			data_all=data_all+get_submitted_data_string('update_id*cbocount_'+i+'*cbocompone_'+i+'*percentone_'+i+'*cbocomptwo_'+i+'*percenttwo_'+i+'*cbotype_'+i+'*consratio_'+i+'*consqnty_'+i+'*txtrateyarn_'+i+'*txtamountyarn_'+i+'*cbostatusyarn_'+i+'*updateidyarncost_'+i+'*txtconsumptionyarn_sum*txtamountyarn_sum',"../../");
		}
		var data="action=save_update_delet_fabric_yarn_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_yarn_cost_dtls_reponse;
}

function fnc_fabric_yarn_cost_dtls_reponse()
{
	
	if(http.readyState == 4) 
	{
		var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_fabric_yarn_cost_dtls('+ reponse[1]+')',8000); 
		}
		else
		{
		if (reponse[0].length>2) reponse[0]=10;
		show_sub_form(document.getElementById('update_id').value, 'show_fabric_cost_listview');
		show_hide_content('yarn_cost', '') 
		if(reponse[0]==0 || reponse[0]==1)
		{
			    update_related_data(reponse[0])
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
		//release_freezing();
		}
		
	}
}
// End Yarn Cost------------------------------------------------------
// Start Conversion Cost------------------------------------------------------
function add_break_down_tr_conversion_cost( i,conversion_from_chart )
{
	var row_num=$('#tbl_conversion_cost tr').length-1;
	if (i==0)
	{
		i=1;
		 $("#txtconstruction_"+i).autocomplete({
			 source: str_construction
		  });
		   $("#txtcomposition_"+i).autocomplete({
			 source:  str_composition 
		  }); 
		  return;
	}
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_conversion_cost tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_conversion_cost");
		  $('#cbocosthead_'+i).removeAttr("onChange").attr("onChange","set_conversion_qnty("+i+")");
		  //$('#cbotypeconversion_'+i).removeAttr("onChange").attr("onChange","set_conversion_charge_unit(this.value,'txtchargeunit_', "+i+")");
		  $('#increaseconversion_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_conversion_cost("+i+","+conversion_from_chart+");");
		  $('#decreaseconversion_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_conversion_cost');");
		  $('#txtreqqnty_'+i).removeAttr("onChange").attr("onChange","calculate_conversion_cost( "+i+" )");
		  $('#txtchargeunit_'+i).removeAttr("onChange").attr("onChange","calculate_conversion_cost( "+i+" )");
		  if(conversion_from_chart==1)
		  {
			 $('#cbotypeconversion_'+i).removeAttr("onChange").attr("onChange","set_conversion_charge_unit_pop_up("+i+")");
			 $('#txtchargeunit_'+i).removeAttr("onClick").attr("onClick","set_conversion_charge_unit_pop_up("+i+")");  
		  }
		  $('#updateidcoversion_'+i).val("");
		  set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
	      set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
	      set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
	}
	
}
function set_conversion_charge_unit(cost_head,txtchargeunit_id,i)
{
	var conversion_qnty=return_global_ajax_value(cost_head, 'set_conversion_charge', '', 'requires/quotation_entry_controller');
	document.getElementById(txtchargeunit_id+i).value = conversion_qnty
	math_operation( 'txtamountconversion_'+i, 'txtreqqnty_'+i+'*txtchargeunit_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
	set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
	set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
	set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
	
}
function set_conversion_charge_unit_pop_up(i)
{
	
	var cbo_company_name=document.getElementById('cbo_company_name').value;
	var cbotypeconversion=document.getElementById('cbotypeconversion_'+i).value
	var txt_exchange_rate=document.getElementById('txt_exchange_rate').value
	var coversionchargelibraryid=document.getElementById('coversionchargelibraryid_'+i).value
	if(cbotypeconversion==35)
	{
		return;
	}
	//alert(coversionchargelibraryid);
	if(cbo_company_name==0)
	{
	alert("Select Company");
	return;
	}
	if(cbotypeconversion==0)
	{
	alert("Select Process");
	return;
	}
	if(txt_exchange_rate==0 || txt_exchange_rate=="")
	{
	alert("Select Exchange Rate");
	return;
	}
	else
	{
	var page_link='requires/quotation_entry_controller.php?action=conversion_chart_popup&cbo_company_name='+cbo_company_name+'&cbotypeconversion='+cbotypeconversion+'&coversionchargelibraryid='+coversionchargelibraryid;
	//alert(page_link)
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Conversion Chart', 'width=1060px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
		{
			var charge_id=this.contentDoc.getElementById("charge_id");
			var charge_value=this.contentDoc.getElementById("charge_value");
			
			document.getElementById('coversionchargelibraryid_'+i).value=charge_id.value;
			document.getElementById('txtchargeunit_'+i).value=number_format_common(charge_value.value/txt_exchange_rate,5,0,document.getElementById('cbo_currercy').value);
			math_operation( 'txtamountconversion_'+i, 'txtreqqnty_'+i+'*txtchargeunit_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
	        set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
	        set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
	        set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
			
		}
	}
	//alert(i)
}
function set_conversion_qnty(i)
{
  var cbocosthead= document.getElementById('cbocosthead_'+i).value;
  
  if(cbocosthead !=0)
  {
  //var conversion_qnty=return_global_ajax_value(cbocosthead, 'set_conversion_qnty', '', 'requires/quotation_entry_controller');
  //alert("txtconsumption_"+cbocosthead)
  var conversion_qnty = document.getElementsByName("txtconsumption_"+cbocosthead)[0].value; 
  }
  if(cbocosthead ==0)
  {
  var conversion_qnty=document.getElementById('txtknit_sum').value
  }
  
  document.getElementById('txtreqqnty_'+i).value=conversion_qnty;
  math_operation( 'txtamountconversion_'+i, 'txtreqqnty_'+i+'*txtchargeunit_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
  set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
  set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
  set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
}
function calculate_conversion_cost(i)
{
	        math_operation( 'txtamountconversion_'+i, 'txtreqqnty_'+i+'*txtchargeunit_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
			set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
	        set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
	        set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
}

function fnc_fabric_conversion_cost_dtls( operation )
{
	    if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	    var row_num=$('#tbl_conversion_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('update_id','Company Name')==false)
			{
				return;
			}
			
			//eval(get_submitted_variables('update_id*cbocosthead_'+i+'*cbotypeconversion_'+i+'*txtreqqnty_'+i+'*txtchargeunit_'+i+'*txtamountconversion_'+i+'*cbostatusconversion_'+i+'*updateidcoversion_'+i+'*txtconreqnty_sum*txtconchargeunit_sum*txtconamount_sum'));
			data_all=data_all+get_submitted_data_string('update_id*cbocosthead_'+i+'*cbotypeconversion_'+i+'*txtreqqnty_'+i+'*txtchargeunit_'+i+'*txtamountconversion_'+i+'*cbostatusconversion_'+i+'*updateidcoversion_'+i+'*coversionchargelibraryid_'+i+'*txtconreqnty_sum*txtconchargeunit_sum*txtconamount_sum',"../../");
		}
		var data="action=save_update_delet_fabric_conversion_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_conversion_cost_dtls_reponse;
}

function fnc_fabric_conversion_cost_dtls_reponse()
{
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_fabric_conversion_cost_dtls('+ reponse[1]+')',8000); 
		}
		else
		{
		if (reponse[0].length>2) reponse[0]=10;
		show_sub_form(document.getElementById('update_id').value, 'show_fabric_cost_listview');
		show_hide_content('conversion_cost', '') 
		if(reponse[0]==0 || reponse[0]==1)
		{
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
		//release_freezing();
		}
	}
}
// End Conversion Cost------------------------------------------------------
// Start Trim Cost------------------------------------------------------
function add_break_down_tr_trim_cost( i )
{
	var row_num=$('#tbl_trim_cost tr').length-1;
	if (i==0)
	{
		i=1;
		 $("#txtconstruction_"+i).autocomplete({
			 source: str_construction
		  });
		   $("#txtcomposition_"+i).autocomplete({
			 source:  str_composition 
		  }); 
		  return;
	}
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_trim_cost tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_trim_cost");
		  
		  //$('#txttrimamount_'+i).removeAttr("onfocus").attr("onfocus","add_break_down_tr_trim_cost("+i+");");
		  $('#cbogroup_'+i).removeAttr("onChange").attr("onChange","set_trim_cons_uom( this.value,"+i+" )");
		  $('#increasetrim_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_trim_cost("+i+");");
		  $('#decreasetrim_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_trim_cost');");
		  $('#txtconsdzngmts_'+i).removeAttr("onChange").attr("onChange","calculate_trim_cost( "+i+" )");
		  //$('#txtconsdzngmts_'+i).removeAttr("onDblClick").attr("onDblClick","open_calculator( "+i+" )");
		  $('#txttrimrate_'+i).removeAttr("onChange").attr("onChange","calculate_trim_cost( "+i+" )");
		  $('#updateidtrim_'+i).val("");
		  $('#cbonominasupplier_'+i).val("");
		  set_sum_value( 'txtconsdzntrim_sum', 'txtconsdzngmts_', 'tbl_trim_cost' );
	      set_sum_value( 'txtratetrim_sum', 'txttrimrate_', 'tbl_trim_cost' );
	      set_sum_value( 'txttrimamount_sum', 'txttrimamount_', 'tbl_trim_cost' );
	}
	
}

function calculate_trim_cost(i)
{
	        math_operation( 'txttrimamount_'+i, 'txtconsdzngmts_'+i+'*txttrimrate_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
			set_sum_value( 'txtconsdzntrim_sum', 'txtconsdzngmts_', 'tbl_trim_cost' );
	        set_sum_value( 'txtratetrim_sum', 'txttrimrate_', 'tbl_trim_cost' );
	        set_sum_value( 'txttrimamount_sum', 'txttrimamount_', 'tbl_trim_cost' );
}

function set_trim_cons_uom(trim_group_id,i)
{
	
	/*var http = createObject();
	http.onreadystatechange = function() {
		if( http.readyState == 4 && http.status == 200 ) {
			document.getElementById('cboconsuom_'+i).value = http.responseText;
		}
	}
	http.open( "GET","requires/quotation_entry_controller.php?trim_group_id=" +trim_group_id+ "&action=set_cons_uom" , false );
	http.send();*/
	var cbo_cons_uom=return_global_ajax_value(trim_group_id, 'set_cons_uom', '', 'requires/quotation_entry_controller');
  	document.getElementById('cboconsuom_'+i).value = cbo_cons_uom;
	
}

function open_calculator(i)
{
	var cbogroup=document.getElementById('cbogroup_'+i).value;	
	var cbo_costing_per=document.getElementById('cbo_costing_per').value;	
	var calculator_parameter=return_global_ajax_value( cbogroup, 'calculator_parameter', '', 'requires/quotation_entry_controller');
	if(trim(calculator_parameter)!=0)
	{
		var page_link="requires/quotation_entry_controller.php?calculator_parameter="+trim(calculator_parameter)+"&action=calculator_type&cbo_costing_per="+cbo_costing_per;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Calculator', 'width=350px,height=200px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var txt_cons_for_cone=this.contentDoc.getElementById("txt_cons_for_cone");
			var txt_clacolator_param_value=this.contentDoc.getElementById("txt_clacolator_param_value");
			document.getElementById('txtconsdzngmts_'+i).value=txt_cons_for_cone.value;
			calculate_trim_cost(i);
		}		
	}
}

function fnc_trim_cost_dtls( operation )
{
		if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	    var row_num=$('#tbl_trim_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('update_id','Company Name')==false)
			{
				return;
			}
			
			eval(get_submitted_variables('update_id*cbogroup_'+i+'*cboconsuom_'+i+'*txtconsdzngmts_'+i+'*txttrimrate_'+i+'*txttrimamount_'+i+'*cboapbrequired_'+i+'*cbonominasupplier_'+i+'*cbotrimstatus_'+i+'*updateidtrim_'+i+'*txtconsdzntrim_sum*txtratetrim_sum*txttrimamount_sum'));
			data_all=data_all+get_submitted_data_string('update_id*cbogroup_'+i+'*cboconsuom_'+i+'*txtconsdzngmts_'+i+'*txttrimrate_'+i+'*txttrimamount_'+i+'*cboapbrequired_'+i+'*cbonominasupplier_'+i+'*cbotrimstatus_'+i+'*updateidtrim_'+i+'*txtconsdzntrim_sum*txtratetrim_sum*txttrimamount_sum',"../../");
		}
		var data="action=save_update_delet_trim_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_trim_cost_dtls_reponse;
}

function fnc_trim_cost_dtls_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_trim_cost_dtls('+ reponse[1]+')',8000); 
		}
		else
		{
		if (reponse[0].length>2) reponse[0]=10;
		show_sub_form(document.getElementById('update_id').value, 'show_trim_cost_listview');
		if(reponse[0]==0 || reponse[0]==1)
		{
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
		//release_freezing();
		}
		
	}
}
// End Trim Cost------------------------------------------------------
// Start Embellishment Cost------------------------------------------------------
function add_break_down_tr_embellishment_cost( i )
{
	var row_num=$('#tbl_embellishment_cost tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_embellishment_cost tr:last").clone().find("input,select,td").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_embellishment_cost");
		  
		 // $('#txtembamount_'+i).removeAttr("onfocus").attr("onfocus","add_break_down_tr_embellishment_cost("+i+");");
		  //$('#embtypetd_'+i).removeAttr("id").attr("id","'fabriccosttbltr_'+i");
		  $('#increaseemb_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_embellishment_cost("+i+");");
		  $('#decreaseemb_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_embellishment_cost');");
		  $('#txtembconsdzngmts_'+i).removeAttr("onChange").attr("onChange","calculate_emb_cost( "+i+" )");
		  $('#txtembrate_'+i).removeAttr("onChange").attr("onChange","calculate_emb_cost( "+i+" )");
		  $('#cboembname_'+i).removeAttr("onChange").attr("onChange","cbotype_loder( "+i+" )");
		  $('#cboembtype_'+i).removeAttr("onChange").attr("onChange","check_duplicate( "+i+" )");
		  $('#embupdateid_'+i).val("");
		  $('#cboembname_'+i).val(0);
		  //set_sum_value( 'txtconsdznemb_sum', 'txtembconsdzngmts_', 'tbl_embellishment_cost' );
	      //set_sum_value( 'txtrateemb_sum', 'txtembrate_', 'tbl_embellishment_cost' );
	      set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_embellishment_cost' );
		  calculate_main_total();
		  
	}
	
}

function cbotype_loder( i )
{
	var cboembname=document.getElementById('cboembname_'+i).value
	load_drop_down( 'requires/quotation_entry_controller', cboembname+'_'+i, 'load_drop_down_embtype', 'embtypetd_'+i );
	check_duplicate(i)
}

function check_duplicate(id)
{
	var cboembname=(document.getElementById('cboembname_'+id).value);
	var cboembtype=(document.getElementById('cboembtype_'+id).value);
	var row_num=$('#tbl_embellishment_cost tr').length-1;
	for (var k=1;k<=row_num; k++)
	{
		if(k==id)
		{
		continue;
		}
		else
		{
			if(cboembname==document.getElementById('cboembname_'+k).value && cboembtype==document.getElementById('cboembtype_'+k).value)
			{
				alert("Same Name, Same Type  Duplication Not Allowed.");
				document.getElementById('cboembtype_'+id).value=0;
				//document.getElementById(td).focus();
			}
			
		}
		
		
	}
}
function calculate_emb_cost(i)
{
	        math_operation( 'txtembamount_'+i, 'txtembconsdzngmts_'+i+'*txtembrate_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
			//set_sum_value( 'txtconsdznemb_sum', 'txtembconsdzngmts_', 'tbl_embellishment_cost' );
	        //set_sum_value( 'txtrateemb_sum', 'txtembrate_', 'tbl_embellishment_cost' );
	        set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_embellishment_cost' );
}

function fnc_embellishment_cost_dtls( operation )
{
	    if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	    var row_num=$('#tbl_embellishment_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('update_id','Company Name')==false)
			{
				return;
			}
			
			eval(get_submitted_variables('update_id*cboembname_'+i+'*cboembtype_'+i+'*txtembconsdzngmts_'+i+'*txtembrate_'+i+'*txtembamount_'+i+'*cboembstatus_'+i+'*embupdateid_'+i+'*txtamountemb_sum'));
			data_all=data_all+get_submitted_data_string('update_id*cboembname_'+i+'*cboembtype_'+i+'*txtembconsdzngmts_'+i+'*txtembrate_'+i+'*txtembamount_'+i+'*cboembstatus_'+i+'*embupdateid_'+i+'*txtamountemb_sum',"../../");
		}
		var data="action=save_update_delet_embellishment_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_embellishment_cost_dtls_reponse;
}

function fnc_embellishment_cost_dtls_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_embellishment_cost_dtls('+ reponse[1]+')',8000); 
		}
		else
		{
		if (reponse[0].length>2) reponse[0]=10;
		show_sub_form(document.getElementById('update_id').value, 'show_embellishment_cost_listview');
		if(reponse[0]==0 || reponse[0]==1)
		{
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
		//release_freezing();
		}
		
	}
}
// End Embellishment Cost------------------------------------------------------

// Start Wash Cost------------------------------------------------------
function add_break_down_tr_wash_cost( i,conversion_from_chart )
{
	var row_num=$('#tbl_wash_cost tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_wash_cost tr:last").clone().find("input,select,td").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_wash_cost");
		  
		 // $('#txtembamount_'+i).removeAttr("onfocus").attr("onfocus","add_break_down_tr_embellishment_cost("+i+");");
		  //$('#embtypetd_'+i).removeAttr("id").attr("id","'fabriccosttbltr_'+i");
		  $('#increaseemb_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_wash_cost("+i+");");
		  $('#decreaseemb_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_wash_cost');");
		  $('#txtembconsdzngmts_'+i).removeAttr("onChange").attr("onChange","calculate_wash_cost( "+i+" )");
		  $('#txtembrate_'+i).removeAttr("onChange").attr("onChange","calculate_wash_cost( "+i+" )");
		  if(conversion_from_chart==1)
		  {
			  $('#txtembrate_'+i).removeAttr("onClick").attr("onClick","set_wash_charge_unit_pop_up( "+i+" )");
		  }
		  //$('#cboembname_'+i).removeAttr("onChange").attr("onChange","cbotype_loder( "+i+" )");
		  $('#cboembtype_'+i).removeAttr("onChange").attr("onChange","check_duplicate( "+i+" )");
		  $('#embupdateid_'+i).val("");
		  $('#txtembrate_'+i).val("");
		  $('#txtembamount_'+i).val("");
		  $('#embratelibid_'+i).val("");
		  $('#cboembtype_'+i).val(0);
		  //set_sum_value( 'txtconsdznemb_sum', 'txtembconsdzngmts_', 'tbl_embellishment_cost' );
	      //set_sum_value( 'txtrateemb_sum', 'txtembrate_', 'tbl_embellishment_cost' );
	      set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_wash_cost' );
		  calculate_main_total();
		  
	}
	
}



function check_duplicate(id)
{
	var cboembname=(document.getElementById('cboembname_'+id).value);
	var cboembtype=(document.getElementById('cboembtype_'+id).value);
	var row_num=$('#tbl_wash_cost tr').length-1;
	for (var k=1;k<=row_num; k++)
	{
		if(k==id)
		{
		continue;
		}
		else
		{
			if(cboembname==document.getElementById('cboembname_'+k).value && cboembtype==document.getElementById('cboembtype_'+k).value)
			{
				alert("Same Name, Same Type  Duplication Not Allowed.");
				document.getElementById('cboembtype_'+id).value=0;
				//document.getElementById(td).focus();
			}
			
		}
		
		
	}
}
function set_wash_charge_unit_pop_up(i)
{
	
	var cbo_company_name=document.getElementById('cbo_company_name').value;
	//var cbotypeconversion=document.getElementById('cbotypeconversion_'+i).value
	var txt_exchange_rate=document.getElementById('txt_exchange_rate').value
	var embratelibid=document.getElementById('embratelibid_'+i).value;
	if(cbo_company_name==0)
	{
	alert("Select Company");
	return;
	}
	
	if(txt_exchange_rate==0 || txt_exchange_rate=="")
	{
	alert("Select Exchange Rate");
	return;
	}
	else
	{
	var page_link='requires/quotation_entry_controller.php?action=wash_chart_popup&cbo_company_name='+cbo_company_name+'&embratelibid='+embratelibid;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Conversion Chart', 'width=1060px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
		{
			var charge_id=this.contentDoc.getElementById("charge_id");
			var charge_value=this.contentDoc.getElementById("charge_value");
			
			document.getElementById('embratelibid_'+i).value=charge_id.value;
			document.getElementById('txtembrate_'+i).value=number_format_common(charge_value.value/txt_exchange_rate,5,0,document.getElementById('cbo_currercy').value);
			//math_operation( 'txtamountconversion_'+i, 'txtreqqnty_'+i+'*txtchargeunit_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
	        //set_sum_value( 'txtconreqnty_sum', 'txtreqqnty_', 'tbl_conversion_cost' );
	        //set_sum_value( 'txtconchargeunit_sum', 'txtchargeunit_', 'tbl_conversion_cost' );
	        //set_sum_value( 'txtconamount_sum', 'txtamountconversion_', 'tbl_conversion_cost' );
			calculate_wash_cost(i)
		}
	}
	//alert(i)
}
function calculate_wash_cost(i)
{
	        math_operation( 'txtembamount_'+i, 'txtembconsdzngmts_'+i+'*txtembrate_'+i, '*','',{dec_type:1,comma:0,currency:document.getElementById('cbo_currercy').value} );
			//set_sum_value( 'txtconsdznemb_sum', 'txtembconsdzngmts_', 'tbl_embellishment_cost' );
	        //set_sum_value( 'txtrateemb_sum', 'txtembrate_', 'tbl_embellishment_cost' );
	        set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_wash_cost' );
}

function fnc_wash_cost_dtls( operation )
{
	    if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	    var row_num=$('#tbl_wash_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('update_id','Company Name')==false)
			{
				return;
			}
			
			//eval(get_submitted_variables('update_id*cboembname_'+i+'*cboembtype_'+i+'*txtembconsdzngmts_'+i+'*txtembrate_'+i+'*txtembamount_'+i+'*cboembstatus_'+i+'*embupdateid_'+i+'*txtamountemb_sum'));
			data_all=data_all+get_submitted_data_string('update_id*cboembname_'+i+'*cboembtype_'+i+'*txtembconsdzngmts_'+i+'*txtembrate_'+i+'*txtembamount_'+i+'*cboembstatus_'+i+'*embupdateid_'+i+'*embratelibid_'+i+'*txtamountemb_sum',"../../");
		}
		//alert(data_all);
		//return;
		var data="action=save_update_delet_wash_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_wash_cost_dtls_reponse;
}

function fnc_wash_cost_dtls_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_wash_cost_dtls('+ reponse[1]+')',8000); 
		}
		else
		{
		if (reponse[0].length>2) reponse[0]=10;
		show_sub_form(document.getElementById('update_id').value, 'show_wash_cost_listview');
		if(reponse[0]==0 || reponse[0]==1)
		{
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
		//release_freezing();
		}
		
	}
}
// End Wash Cost------------------------------------------------------
// Start Commision Cost------------------------------------------------------
function add_break_down_tr_commission_cost( i )
{
	var row_num=$('#tbl_commission_cost tr').length-1;
	if (i==0)
	{
		i=1;
		 $("#txtconstruction_"+i).autocomplete({
			 source: str_construction
		  });
		   $("#txtcomposition_"+i).autocomplete({
			 source:  str_composition 
		  }); 
		  return;
	}
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_commission_cost tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_commission_cost");
		  
		 // $('#txtembamount_'+i).removeAttr("onfocus").attr("onfocus","add_break_down_tr_embellishment_cost("+i+");");
		  
		  $('#increasecommission_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_commission_cost("+i+");");
		  $('#decreasecommission_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_commission_cost');");
		  $('#txtcommissionrate_'+i).removeAttr("onChange").attr("onChange","calculate_commission_cost( "+i+" )");
		  $('#cbocommissionbase_'+i).removeAttr("onChange").attr("onChange","calculate_commission_cost( "+i+" )");

		  //$('#txtembrate_'+i).removeAttr("onChange").attr("onChange","calculate_emb_cost( "+i+" )");
		  $('#commissionupdateid_'+i).val("");
		  set_sum_value( 'txtratecommission_sum', 'txtcommissionrate_', 'tbl_commission_cost' );
	      set_sum_value( 'txtamountcommission_sum', 'txtcommissionamount_', 'tbl_commission_cost' );
	}
	
}

function calculate_commission_cost(i)
{
	       
		   var currency=(document.getElementById('cbo_currercy').value)*1;
		   var commission_base=document.getElementById('cbocommissionbase_'+i).value;
		   var cbo_costing_per=document.getElementById('cbo_costing_per').value;
		   var txtcommissionrate=(document.getElementById('txtcommissionrate_'+i).value)*1;
           var amount=0;
		   if(commission_base==1)
		   {
			 var total_cost=(document.getElementById('txt_confirm_price_pre_cost_dzn').value)*1;
			 if(total_cost==0 || total_cost=="")
			 {
				 alert("Please Insert Price Before Comn/Dzn")
			 }
			 var txtcommissionrate_percent=txtcommissionrate/100;

//alert('('+total_cost+'/('+1+'-'+txtcommissionrate_percent+'))-'+total_cost);
             var amount=(total_cost/(1-txtcommissionrate_percent))-total_cost;
		   }
		   
		   if(commission_base==2)
		   {
			   //var amount=txtcommissionrate*1;
			   if(cbo_costing_per==1)
			   {
				var amount=txtcommissionrate*12*1;
			   }
			   if(cbo_costing_per==2)
			   {
				var amount=txtcommissionrate*1;
			   }
			   if(cbo_costing_per==3)
			   {
				var amount=txtcommissionrate*12*2;
			   }
			   if(cbo_costing_per==4)
			   {
				var amount=txtcommissionrate*12*3;
			   }
			   if(cbo_costing_per==5)
			   {
				var amount=txtcommissionrate*12*4;
			   }
		   }
		   if(commission_base==3)
		   {
			  // var amount=txtcommissionrate/12;
			   if(cbo_costing_per==1)
			   {
				var amount=txtcommissionrate*1*1;
			   }
			   if(cbo_costing_per==2)
			   {
				var amount=txtcommissionrate/12;
			   }
			   if(cbo_costing_per==3)
			   {
				var amount=txtcommissionrate*1*2;
			   }
			   if(cbo_costing_per==4)
			   {
				var amount=txtcommissionrate*1*3;
			   }
			   if(cbo_costing_per==5)
			   {
				var amount=txtcommissionrate*1*4;
			   }
		   }
		  // alert(amount)
		   document.getElementById('txtcommissionamount_'+i).value=number_format_common(amount,1,0,cbo_currercy);
		   set_sum_value( 'txtratecommission_sum', 'txtcommissionrate_', 'tbl_commission_cost' );
	       set_sum_value( 'txtamountcommission_sum', 'txtcommissionamount_', 'tbl_commission_cost' );
}

function recalculate_commision_cost()
{
 
    var row_num=$('#tbl_commission_cost tr').length-1;	
	for(var i=1; i<=row_num; i++)
	{
	calculate_commission_cost(i)
	}
	fnc_commission_cost_dtls( 1 )
}

function fnc_commission_cost_dtls( operation )
{
	    if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	    var row_num=$('#tbl_commission_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('update_id','Company Name')==false)
			{
				return;
			}
			
			eval(get_submitted_variables('update_id*cboparticulars_'+i+'*cbocommissionbase_'+i+'*txtcommissionrate_'+i+'*txtcommissionamount_'+i+'*cbocommissionstatus_'+i+'*commissionupdateid_'+i+'*txtratecommission_sum*txtamountcommission_sum'));
			data_all=data_all+get_submitted_data_string('update_id*cboparticulars_'+i+'*cbocommissionbase_'+i+'*txtcommissionrate_'+i+'*txtcommissionamount_'+i+'*cbocommissionstatus_'+i+'*commissionupdateid_'+i+'*txtratecommission_sum*txtamountcommission_sum',"../../");
		}
		var data="action=save_update_delet_commission_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_commission_cost_dtls_reponse;
}

function fnc_commission_cost_dtls_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_commission_cost_dtls('+ reponse[1]+')',8000); 
		}
		else
		{
		if (reponse[0].length>2) reponse[0]=10;
		show_sub_form(document.getElementById('update_id').value, 'show_commission_cost_listview');
		if(reponse[0]==0 || reponse[0]==1)
		{
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
		//release_freezing();
		}
		
	}
}
// End Commision Cost------------------------------------------------------
// Start Comarcial Cost------------------------------------------------------

function add_break_down_tr_comarcial_cost( i )
{
	var row_num=$('#tbl_comarcial_cost tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
		 $("#tbl_comarcial_cost tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});
			 
		  }).end().appendTo("#tbl_comarcial_cost");
		  
		 // $('#txtembamount_'+i).removeAttr("onfocus").attr("onfocus","add_break_down_tr_embellishment_cost("+i+");");
		  
		  $('#increasecomarcial_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr_comarcial_cost("+i+");");
		  $('#decreasecomarcial_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+",'tbl_comarcial_cost');");
		  $('#txtcomarcialrate_'+i).removeAttr("onChange").attr("onChange","calculate_comarcial_cost( "+i+",'rate' )");
		  $('#txtcomarcialamount_'+i).removeAttr("onChange").attr("onChange","calculate_comarcial_cost( "+i+",'amount' )");
		  
		 // $('#cbocomarcialbase_'+i).removeAttr("onChange").attr("onChange","calculate_comarcial_cost( "+i+" )");

		  //$('#txtembrate_'+i).removeAttr("onChange").attr("onChange","calculate_emb_cost( "+i+" )");
		  $('#comarcialupdateid_'+i).val("");
		  set_sum_value( 'txtratecomarcial_sum', 'txtcomarcialrate_', 'tbl_comarcial_cost' );
	      set_sum_value( 'txtamountcomarcial_sum', 'txtcomarcialamount_', 'tbl_comarcial_cost' );
	}
	
}
function calculate_comarcial_cost(i,type)
{
	var update_id=document.getElementById('update_id').value;
	var currency=(document.getElementById('cbo_currercy').value)*1;
	var txtcomarcialrate=(document.getElementById('txtcomarcialrate_'+i).value)*1;
	var txtcomarcialamount=(document.getElementById('txtcomarcialamount_'+i).value)*1;
	var sum_fab_yarn_trim=return_global_ajax_value(update_id, 'sum_fab_yarn_trim_value', '', 'requires/quotation_entry_controller');
	var amount=number_format_common(sum_fab_yarn_trim, 1, 0, currency);
	if(type=='rate')
	{
		//alert(amount)
		var com_amount=amount*(txtcomarcialrate/100);
		document.getElementById('txtcomarcialamount_'+i).value=number_format_common(com_amount, 1, 0, currency);
	}
	if(type=='amount')
	{
		var com_rate=(txtcomarcialamount*100)/amount;
		document.getElementById('txtcomarcialrate_'+i).value=number_format_common(com_rate, 1, 0, currency);
	}
	
	       
		  /* var commission_base=document.getElementById('cbocomarcialbase_'+i).value;
		   var cbo_costing_per=document.getElementById('cbo_costing_per').value;
		   var txtcommissionrate=(document.getElementById('txtcomarcialrate_'+i).value)*1;
           var amount=0;
		   if(commission_base==1)
		   {
			 var total_cost=(document.getElementById('txt_total_pre_cost').value);
			 var txtcommissionrate_percent=txtcommissionrate/100;
			 //alert(txtcommissionrate_percent);
             var amount=(total_cost/(1-txtcommissionrate_percent))-total_cost;
		   }
		   
		   if(commission_base==2)
		   {
			   if(cbo_costing_per==1)
			   {
				var amount=txtcommissionrate*12*1;
			   }
			   if(cbo_costing_per==2)
			   {
				var amount=txtcommissionrate*1;
			   }
			   if(cbo_costing_per==3)
			   {
				var amount=txtcommissionrate*12*2;
			   }
			   if(cbo_costing_per==4)
			   {
				var amount=txtcommissionrate*12*3;
			   }
			   if(cbo_costing_per==5)
			   {
				var amount=txtcommissionrate*12*4;
			   }
		   }
		   if(commission_base==3)
		   {
			   if(cbo_costing_per==1)
			   {
				var amount=txtcommissionrate*1*1;
			   }
			   if(cbo_costing_per==2)
			   {
				var amount=txtcommissionrate/12;
			   }
			   if(cbo_costing_per==3)
			   {
				var amount=txtcommissionrate*1*2;
			   }
			   if(cbo_costing_per==4)
			   {
				var amount=txtcommissionrate*1*3;
			   }
			   if(cbo_costing_per==5)
			   {
				var amount=txtcommissionrate*1*4;
			   }
		   }
		   document.getElementById('txtcomarcialamount_'+i).value=amount;

		   //math_operation( 'txtembamount_'+i, 'txtembconsdzngmts_'+i+'*txtembrate_'+i, '*','' );*/
			set_sum_value( 'txtratecomarcial_sum', 'txtcomarcialrate_', 'tbl_comarcial_cost' );
	        set_sum_value( 'txtamountcomarcial_sum', 'txtcomarcialamount_', 'tbl_comarcial_cost' );
	        //set_sum_value( 'txtamountemb_sum', 'txtembamount_', 'tbl_embellishment_cost' );
}

function fnc_comarcial_cost_dtls( operation )
{
	   if(operation==2)
		{
			alert("Delete Restricted")
			return;
		}
	    var row_num=$('#tbl_comarcial_cost tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('update_id','Company Name')==false)
			{
				return;
			}
			
			//eval(get_submitted_variables('update_id*cboitem_'+i+'*txtcomarcialrate_'+i+'*txtcomarcialamount_'+i+'*cbocomarcialstatus_'+i+'*comarcialupdateid_'+i+'*txtratecomarcial_sum*txtamountcomarcial_sum'));
			data_all=data_all+get_submitted_data_string('update_id*cboitem_'+i+'*txtcomarcialrate_'+i+'*txtcomarcialamount_'+i+'*cbocomarcialstatus_'+i+'*comarcialupdateid_'+i+'*txtratecomarcial_sum*txtamountcomarcial_sum',"../../");
		}
		var data="action=save_update_delet_comarcial_cost_dtls&operation="+operation+'&total_row='+row_num+data_all;
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_comarcial_cost_dtls_reponse;
}

function fnc_comarcial_cost_dtls_reponse()
{
	
	if(http.readyState == 4) 
	{
	   var reponse=trim(http.responseText).split('**');
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_comarcial_cost_dtls('+ reponse[1]+')',8000); 
		}
		else
		{
		if (reponse[0].length>2) reponse[0]=10;
		show_sub_form(document.getElementById('update_id').value, 'show_comarcial_cost_listview');
		if(reponse[0]==0 || reponse[0]==1)
		{
				fnc_quotation_entry_dtls1( 1 )
				release_freezing();
		}
		//release_freezing();
		}
		
	}
}
// End Comarcial Cost------------------------------------------------------



















// Start Master Form-----------------------------------------
function openmypage(page_link,title)
{
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1100px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
		var theemail=this.contentDoc.getElementById("selected_id") //Access form field with id="emailfield"
		if (theemail.value!="")
		{
			freeze_window(5);
			reset_form('quotationdtls_2','cost_container','','cbo_currercy,2*cbo_costing_per,1*cbo_approved_status,2','')
			get_php_form_data( theemail.value, "populate_data_from_search_popup", "requires/quotation_entry_controller" );
			summary()
			set_button_status(1, permission, 'fnc_quotation_entry',1);
			set_button_status(1, permission, 'fnc_quotation_entry_dtls',2);
			release_freezing();
		}
		 
	}
}

function openmypage_inquery()
{
	var page_link='requires/quotation_entry_controller.php?action=inquery_id_popup';
	var title='Quotation ID Selection Form' ;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=900px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
		var theemail=this.contentDoc.getElementById("selected_id").value.split('_');
		//alert(theemail[2]);
		 //Access form field with id="emailfield"
		if (theemail!="")
		{
			document.getElementById('txt_inquery_id').value=theemail[0];
			document.getElementById('cbo_company_name').value=theemail[1];
			load_drop_down( 'requires/quotation_entry_controller',theemail[1], 'load_drop_down_buyer', 'buyer_td' );
			document.getElementById('cbo_buyer_name').value=theemail[2];
			document.getElementById('txt_style_ref').value=theemail[3];
			document.getElementById('txt_inquery_prifix').value=theemail[4];
			document.getElementById('txt_season').value=theemail[5];

		}
		 
	}
}

function open_set_popup(unit_id)
{
			var txt_quotation_id=document.getElementById('update_id').value;
			var set_breck_down=document.getElementById('set_breck_down').value;
			var tot_set_qnty=document.getElementById('tot_set_qnty').value;
			var page_link="requires/quotation_entry_controller.php?txt_quotation_id="+trim(txt_quotation_id)+"&action=open_set_list_view&set_breck_down="+set_breck_down+"&tot_set_qnty="+tot_set_qnty+'&unit_id='+unit_id;
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, "Item Details", 'width=860px,height=450px,center=1,resize=1,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var set_breck_down=this.contentDoc.getElementById("set_breck_down") //Access form field with id="emailfield"
				var item_id=this.contentDoc.getElementById("item_id") //Access form field with id="emailfield"
				var tot_set_qnty=this.contentDoc.getElementById("tot_set_qnty") //Access form field with id="emailfield"
				document.getElementById('set_breck_down').value=set_breck_down.value;
				document.getElementById('item_id').value=item_id.value;
				document.getElementById('tot_set_qnty').value=tot_set_qnty.value;

			}		
		
}
function set_exchange_rate(currency)
{
	if(currency==1)
	{
		document.getElementById('txt_exchange_rate').value=1;
	}
	else
	{
		document.getElementById('txt_exchange_rate').value=80;	
	}
	
}

function calculate_lead_time()
{
  var txt_est_ship_date= document.getElementById('txt_est_ship_date').value;
  var txt_op_date= document.getElementById('txt_op_date').value;
  var lead_time = return_ajax_request_value(txt_est_ship_date+"_"+txt_op_date, 'lead_time_calculate', 'requires/quotation_entry_controller')	
  //alert(lead_time);
  document.getElementById('txt_lead_time').value=trim(lead_time)
}
function fnc_quotation_entry( operation )
{
	if(operation==2)
	{
		alert("Delete Restricted")
		return;
	}
	if (form_validation('cbo_company_name*cbo_buyer_name*txt_style_ref*cbo_currercy*txt_exchange_rate*cbo_costing_per*cbo_order_uom*item_id*txt_quotation_date','Company Name*Buyer Name*Style Ref*Currency*Exchange Rate*Costing Per*UOM*Item*Quot Date')==false)
	{
		return;
	}
	else
	{
		//eval(get_submitted_variables('txt_quotation_id*cbo_company_name*cbo_buyer_name*txt_style_ref*txt_revised_no*cbo_pord_dept*txt_style_desc*cbo_currercy*cbo_agent*txt_offer_qnty*cbo_region*cbo_color_range*cbo_inco_term*txt_incoterm_place*txt_machine_line*txt_prod_line_hr*cbo_costing_per*txt_quotation_date*txt_est_ship_date*txt_factory*txt_remarks*garments_nature*cbo_order_uom*item_id*set_breck_down*tot_set_qnty*update_id*cbo_approved_status'));
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_quotation_id*cbo_company_name*cbo_buyer_name*txt_style_ref*txt_revised_no*cbo_pord_dept*txt_product_code*txt_style_desc*cbo_currercy*cbo_agent*txt_offer_qnty*cbo_region*cbo_color_range*cbo_inco_term*txt_incoterm_place*txt_machine_line*txt_prod_line_hr*cbo_costing_per*txt_quotation_date*txt_est_ship_date*txt_factory*txt_remarks*garments_nature*cbo_order_uom*item_id*set_breck_down*tot_set_qnty*update_id*cbo_approved_status*cm_cost_predefined_method_id*txt_exchange_rate*txt_sew_smv*txt_cut_smv*txt_sew_efficiency_per*txt_cut_efficiency_per*txt_efficiency_wastage*txt_season*txt_op_date*txt_inquery_id*txt_m_list_no*txt_bh_marchant',"../../");
		
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_quotation_entry_reponse;
	}
}

function fnc_quotation_entry_reponse()
{
	if(http.readyState == 4) 
	{
	
		var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		document.getElementById('update_id').value  = reponse[2];
		document.getElementById('txt_quotation_id').value  = reponse[2];
		if(reponse[3]==1)
		{
			document.getElementById('cbo_approved_status').value = '2';
			document.getElementById('approve1').value = 'Approved';
			document.getElementById('cbo_company_name').disabled=false;
			document.getElementById('cbo_buyer_name').disabled=false;
			document.getElementById('txt_style_ref').disabled=false; 
			document.getElementById('txt_revised_no').disabled=false; 
			document.getElementById('cbo_pord_dept').disabled=false;
			document.getElementById('txt_style_desc').disabled=false;
			document.getElementById('cbo_currercy').disabled=false;
			document.getElementById('cbo_agent').disabled=false;
			document.getElementById('txt_offer_qnty').disabled=false;
			document.getElementById('cbo_region').disabled=false;
			document.getElementById('cbo_color_range').disabled=false;
			document.getElementById('cbo_inco_term').disabled=false;
			document.getElementById('txt_incoterm_place').disabled=false;
			document.getElementById('txt_machine_line').disabled=false;
			document.getElementById('txt_prod_line_hr').disabled=false;
			document.getElementById('cbo_costing_per').disabled=false;
			document.getElementById('txt_quotation_date').disabled=false;
			document.getElementById('txt_est_ship_date').disabled=false;
			document.getElementById('txt_factory').disabled=false;
			document.getElementById('txt_remarks').disabled=false;
			document.getElementById('cbo_order_uom').disabled=false;
			document.getElementById('image_button').disabled=false;
			document.getElementById('set_button').disabled=false;
			document.getElementById('save1').disabled=false;
			document.getElementById('update1').disabled=false;
			document.getElementById('Delete1').disabled=false;
			//===================
			document.getElementById('txt_lab_test_pre_cost').disabled=false;
			document.getElementById('txt_inspection_pre_cost').disabled=false;
			document.getElementById('txt_cm_pre_cost').disabled=false;
			document.getElementById('txt_freight_pre_cost').disabled=false;
			document.getElementById('txt_common_oh_pre_cost').disabled=false;
			document.getElementById('txt_1st_quoted_price_pre_cost').disabled=false;
			document.getElementById('txt_first_quoted_price_date').disabled=false;
			document.getElementById('txt_revised_price_pre_cost').disabled=false;
			document.getElementById('txt_revised_price_date').disabled=false;
			document.getElementById('txt_confirm_price_pre_cost').disabled=false;
			document.getElementById('txt_confirm_date_pre_cost').disabled=false;
			document.getElementById('save2').disabled=false;
			document.getElementById('update2').disabled=false;
			document.getElementById('Delete2').disabled=false;

		}
		if(reponse[3]==2)
		{
			document.getElementById('cbo_approved_status').value = '1';
			document.getElementById('approve1').value = 'Un-Approved';
			document.getElementById('cbo_company_name').disabled=true;
			document.getElementById('cbo_buyer_name').disabled=true;
			document.getElementById('txt_style_ref').disabled=true;
			document.getElementById('txt_revised_no').disabled=true; 
			document.getElementById('cbo_pord_dept').disabled=true;
			document.getElementById('txt_style_desc').disabled=true;
			document.getElementById('cbo_currercy').disabled=true;
			document.getElementById('cbo_agent').disabled=true;
			document.getElementById('txt_offer_qnty').disabled=true;
			document.getElementById('cbo_region').disabled=true;
			document.getElementById('cbo_color_range').disabled=true;
			document.getElementById('cbo_inco_term').disabled=true;
			document.getElementById('txt_incoterm_place').disabled=true;
			document.getElementById('txt_machine_line').disabled=true;
			document.getElementById('txt_prod_line_hr').disabled=true;
			document.getElementById('cbo_costing_per').disabled=true;
			document.getElementById('txt_quotation_date').disabled=true;
			document.getElementById('txt_est_ship_date').disabled=true;
			document.getElementById('txt_factory').disabled=true;
			document.getElementById('txt_remarks').disabled=true;
			document.getElementById('cbo_order_uom').disabled=true;
			document.getElementById('image_button').disabled=true;
			document.getElementById('set_button').disabled=true;
			document.getElementById('save1').disabled=true;
			document.getElementById('update1').disabled=true;
			document.getElementById('Delete1').disabled=true;
			//===================
			document.getElementById('txt_lab_test_pre_cost').disabled=true;
			document.getElementById('txt_inspection_pre_cost').disabled=true;
			document.getElementById('txt_cm_pre_cost').disabled=true;
			document.getElementById('txt_freight_pre_cost').disabled=true;
			document.getElementById('txt_common_oh_pre_cost').disabled=true;
			document.getElementById('txt_1st_quoted_price_pre_cost').disabled=true;
			document.getElementById('txt_first_quoted_price_date').disabled=true;
			document.getElementById('txt_revised_price_pre_cost').disabled=true;
			document.getElementById('txt_revised_price_date').disabled=true;
			document.getElementById('txt_confirm_price_pre_cost').disabled=true;
			document.getElementById('txt_confirm_date_pre_cost').disabled=true;
			document.getElementById('save2').disabled=true;
			document.getElementById('update2').disabled=true;
			document.getElementById('Delete2').disabled=true;
         }
		set_button_status(1, permission, 'fnc_quotation_entry',1);
		show_list_view(reponse[2],'show_fabric_cost_listview','cost_container','../woven_order/requires/quotation_entry_controller','');
		if(reponse[0]==0 || reponse[0]==1)
		{
				fnc_quotation_entry_dtls1( reponse[0] )
				release_freezing();
		}
		if(reponse[0]==2)
		{
			    reset_form('quotationmst_1','cost_container','','cbo_currercy,2*cbo_costing_per,1*cbo_approved_status,2','')
				reset_form('quotationdtls_2','','txt_expected_1*txt_expected_2*txt_expected_3*txt_expected_4*txt_expected_5*txt_expected_6*txt_confirmed_1*txt_confirmed_2*txt_confirmed_3*txt_confirmed_4*txt_confirmed_5*txt_confirmed_6*txt_deviation_1*txt_deviation_2*txt_deviation_3*txt_deviation_4*txt_deviation_5*txt_deviation_6','','')
				summary()
			    set_button_status(0, permission, 'fnc_quotation_entry',1);
				release_freezing();
		}
	}
}

function copy_quatation(operation)
{
	if (form_validation('cbo_company_name*cbo_buyer_name*txt_style_ref*cbo_currercy*txt_exchange_rate*cbo_costing_per*cbo_order_uom*item_id*txt_quotation_date','Company Name*Buyer Name*Style Ref*Currency*Exchange Rate*Costing Per*UOM*Item*Quot Date')==false)
	{
		return;
	}
	else
	{
		//eval(get_submitted_variables('txt_quotation_id*cbo_company_name*cbo_buyer_name*txt_style_ref*txt_revised_no*cbo_pord_dept*txt_style_desc*cbo_currercy*cbo_agent*txt_offer_qnty*cbo_region*cbo_color_range*cbo_inco_term*txt_incoterm_place*txt_machine_line*txt_prod_line_hr*cbo_costing_per*txt_quotation_date*txt_est_ship_date*txt_factory*txt_remarks*garments_nature*cbo_order_uom*item_id*set_breck_down*tot_set_qnty*update_id*cbo_approved_status'));
		var data="action=copy_quatation&operation="+operation+get_submitted_data_string('txt_quotation_id*cbo_company_name*cbo_buyer_name*txt_style_ref*txt_revised_no*cbo_pord_dept*txt_product_code*txt_style_desc*cbo_currercy*cbo_agent*txt_offer_qnty*cbo_region*cbo_color_range*cbo_inco_term*txt_incoterm_place*txt_machine_line*txt_prod_line_hr*cbo_costing_per*txt_quotation_date*txt_est_ship_date*txt_factory*txt_remarks*garments_nature*cbo_order_uom*item_id*set_breck_down*tot_set_qnty*update_id*cbo_approved_status*cm_cost_predefined_method_id*txt_exchange_rate*txt_sew_smv*txt_cut_smv*txt_sew_efficiency_per*txt_cut_efficiency_per*txt_efficiency_wastage*txt_season',"../../");
		
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = copy_quatation_reponse;
	}
}

function copy_quatation_reponse()
{
	if(http.readyState == 4) 
	{
		var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		document.getElementById('update_id').value  = reponse[2];
		document.getElementById('txt_quotation_id').value  = reponse[2];
		document.getElementById('update_id_dtls').value  = reponse[3];
		
		if(reponse[3]==1)
		{
			document.getElementById('cbo_approved_status').value = '2';
			document.getElementById('approve1').value = 'Approved';
			document.getElementById('cbo_company_name').disabled=false;
			document.getElementById('cbo_buyer_name').disabled=false;
			document.getElementById('txt_style_ref').disabled=false; 
			document.getElementById('txt_revised_no').disabled=false; 
			document.getElementById('cbo_pord_dept').disabled=false;
			document.getElementById('txt_style_desc').disabled=false;
			document.getElementById('cbo_currercy').disabled=false;
			document.getElementById('cbo_agent').disabled=false;
			document.getElementById('txt_offer_qnty').disabled=false;
			document.getElementById('cbo_region').disabled=false;
			document.getElementById('cbo_color_range').disabled=false;
			document.getElementById('cbo_inco_term').disabled=false;
			document.getElementById('txt_incoterm_place').disabled=false;
			document.getElementById('txt_machine_line').disabled=false;
			document.getElementById('txt_prod_line_hr').disabled=false;
			document.getElementById('cbo_costing_per').disabled=false;
			document.getElementById('txt_quotation_date').disabled=false;
			document.getElementById('txt_est_ship_date').disabled=false;
			document.getElementById('txt_factory').disabled=false;
			document.getElementById('txt_remarks').disabled=false;
			document.getElementById('cbo_order_uom').disabled=false;
			document.getElementById('image_button').disabled=false;
			document.getElementById('set_button').disabled=false;
			document.getElementById('save1').disabled=false;
			document.getElementById('update1').disabled=false;
			document.getElementById('Delete1').disabled=false;
			//===================
			document.getElementById('txt_lab_test_pre_cost').disabled=false;
			document.getElementById('txt_inspection_pre_cost').disabled=false;
			document.getElementById('txt_cm_pre_cost').disabled=false;
			document.getElementById('txt_freight_pre_cost').disabled=false;
			document.getElementById('txt_common_oh_pre_cost').disabled=false;
			document.getElementById('txt_1st_quoted_price_pre_cost').disabled=false;
			document.getElementById('txt_first_quoted_price_date').disabled=false;
			document.getElementById('txt_revised_price_pre_cost').disabled=false;
			document.getElementById('txt_revised_price_date').disabled=false;
			document.getElementById('txt_confirm_price_pre_cost').disabled=false;
			document.getElementById('txt_confirm_date_pre_cost').disabled=false;
			document.getElementById('save2').disabled=false;
			document.getElementById('update2').disabled=false;
			document.getElementById('Delete2').disabled=false;

		}
		if(reponse[3]==2)
		{
			document.getElementById('cbo_approved_status').value = '1';
			document.getElementById('approve1').value = 'Un-Approved';
			document.getElementById('cbo_company_name').disabled=true;
			document.getElementById('cbo_buyer_name').disabled=true;
			document.getElementById('txt_style_ref').disabled=true;
			document.getElementById('txt_revised_no').disabled=true; 
			document.getElementById('cbo_pord_dept').disabled=true;
			document.getElementById('txt_style_desc').disabled=true;
			document.getElementById('cbo_currercy').disabled=true;
			document.getElementById('cbo_agent').disabled=true;
			document.getElementById('txt_offer_qnty').disabled=true;
			document.getElementById('cbo_region').disabled=true;
			document.getElementById('cbo_color_range').disabled=true;
			document.getElementById('cbo_inco_term').disabled=true;
			document.getElementById('txt_incoterm_place').disabled=true;
			document.getElementById('txt_machine_line').disabled=true;
			document.getElementById('txt_prod_line_hr').disabled=true;
			document.getElementById('cbo_costing_per').disabled=true;
			document.getElementById('txt_quotation_date').disabled=true;
			document.getElementById('txt_est_ship_date').disabled=true;
			document.getElementById('txt_factory').disabled=true;
			document.getElementById('txt_remarks').disabled=true;
			document.getElementById('cbo_order_uom').disabled=true;
			document.getElementById('image_button').disabled=true;
			document.getElementById('set_button').disabled=true;
			document.getElementById('save1').disabled=true;
			document.getElementById('update1').disabled=true;
			document.getElementById('Delete1').disabled=true;
			//===================
			document.getElementById('txt_lab_test_pre_cost').disabled=true;
			document.getElementById('txt_inspection_pre_cost').disabled=true;
			document.getElementById('txt_cm_pre_cost').disabled=true;
			document.getElementById('txt_freight_pre_cost').disabled=true;
			document.getElementById('txt_common_oh_pre_cost').disabled=true;
			document.getElementById('txt_1st_quoted_price_pre_cost').disabled=true;
			document.getElementById('txt_first_quoted_price_date').disabled=true;
			document.getElementById('txt_revised_price_pre_cost').disabled=true;
			document.getElementById('txt_revised_price_date').disabled=true;
			document.getElementById('txt_confirm_price_pre_cost').disabled=true;
			document.getElementById('txt_confirm_date_pre_cost').disabled=true;
			document.getElementById('save2').disabled=true;
			document.getElementById('update2').disabled=true;
			document.getElementById('Delete2').disabled=true;
         }
		set_button_status(1, permission, 'fnc_quotation_entry',1);
		show_list_view(reponse[2],'show_fabric_cost_listview','cost_container','../woven_order/requires/quotation_entry_controller','');
		release_freezing();
	}
}

function cm_cost_predefined_method(company_id)
{
	//alert(company_id)
	var cm_cost_method=return_global_ajax_value(company_id, 'cm_cost_predefined_method', '', 'requires/quotation_entry_controller');
	//alert(cm_cost_method);
	if(cm_cost_method ==0)
	{
		//document.getElementById('txt_cm_pre_cost').disabled=false;
		$("#txt_cm_pre_cost").attr("disabled",false);
		$("#txt_sew_smv").attr("disabled",true);
		$("#txt_sew_efficiency_per").attr("disabled",true);
		$("#txt_cut_smv").attr("disabled",true);
		$("#txt_cut_efficiency_per").attr("disabled",true);
	}
	if(cm_cost_method ==1)
	{
		$("#txt_sew_smv").attr("disabled",false);
		$("#txt_sew_efficiency_per").attr("disabled",false);
		$("#txt_cut_smv").attr("disabled",true);
		$("#txt_cut_efficiency_per").attr("disabled",true);
		$("#txt_cm_pre_cost").attr("disabled",true);
		//document.getElementById('txt_cm_pre_cost').disabled=true;
	}
	if(cm_cost_method ==2)
	{
		$("#txt_sew_smv").attr("disabled",false);
		$("#txt_sew_efficiency_per").attr("disabled",false);
		$("#txt_cut_smv").attr("disabled",false);
		$("#txt_cut_efficiency_per").attr("disabled",false);
		$("#txt_cm_pre_cost").attr("disabled",true);
		//document.getElementById('txt_cm_pre_cost').disabled=true;
	}
	if(cm_cost_method ==3)
	{
		$("#txt_sew_smv").attr("disabled",true);
		$("#txt_sew_efficiency_per").attr("disabled",true);
		$("#txt_cut_smv").attr("disabled",true);
		$("#txt_cut_efficiency_per").attr("disabled",true);
		$("#txt_cm_pre_cost").attr("disabled",true);
		//document.getElementById('txt_cm_pre_cost').disabled=true;
	}
	document.getElementById('cm_cost_predefined_method_id').value=cm_cost_method;
	
}

/*function asking_profit_percent(company_id)
{
	var asking_profit=return_global_ajax_value(company_id, 'asking_profit_percent', '', 'requires/quotation_entry_controller');
	//alert(asking_profit);
}*/
//// End Master Form-----------------------------------------
// Start Dtls Form ------------------------------------------

function  change_caption_cost_dtls( value, type )
{
	
	if(type=="change_caption_dzn")
	{
		if(value==1)
		{
		
		//document.getElementById('final_cost_td_dzn').innerHTML="Asking Profit / 1 Dzn";
		document.getElementById('confirm_price_td_dzn').innerHTML="Price Before Commn/ 1 Dzn";
		document.getElementById('prod_cost_td_dzn').innerHTML="Prd. Cost /1 Dzn";
		document.getElementById('margin_dzn').innerHTML="Margin/1 Dzn";
		document.getElementById('commission_dzn').innerHTML="Commn/1 Dzn";
		document.getElementById('price_with_comm_dzn_td').innerHTML="Price with Commn/ 1 Dzn";
		}
		if(value==2)
		{
		
		//document.getElementById('final_cost_td_dzn').innerHTML="Asking Profit / 1 Pcs";
		document.getElementById('confirm_price_td_dzn').innerHTML="Price Before Commn/ 1 Pcs";
		document.getElementById('prod_cost_td_dzn').innerHTML="Prd. Cost / 1 Pcs";
		document.getElementById('margin_dzn').innerHTML="Margin/ 1 Pcs";
		document.getElementById('commission_dzn').innerHTML="Commn/1 pcs";
		document.getElementById('price_with_comm_dzn_td').innerHTML="Price with Commn/ 1 Pcs";
		}
		if(value==3)
		{
		
		//document.getElementById('final_cost_td_dzn').innerHTML="Asking Profit / 2 Dzn";
		document.getElementById('confirm_price_td_dzn').innerHTML="Price Before Commn/ 2 Dzn";
		document.getElementById('prod_cost_td_dzn').innerHTML="Prd. Cost /2 Dzn";
		document.getElementById('margin_dzn').innerHTML="Margin/ 2 Dzn";
		document.getElementById('commission_dzn').innerHTML="Commn/2 Dzn";
		document.getElementById('price_with_comm_dzn_td').innerHTML="Price with Commn/2 Dzn";
		}
		if(value==4)
		{
		
		//document.getElementById('final_cost_td_dzn').innerHTML="Asking Profit / 3 Dzn";
		document.getElementById('confirm_price_td_dzn').innerHTML="Price Before Commn/ 3 Dzn";
		document.getElementById('prod_cost_td_dzn').innerHTML="Prd. Cost / 3 Dzn";
		document.getElementById('margin_dzn').innerHTML="Margin/ 3 Dzn";
		document.getElementById('commission_dzn').innerHTML="Commn/ 3 Dzn";
		document.getElementById('price_with_comm_dzn_td').innerHTML="Price with Commn/ 3 Dzn";
		}
		if(value==5)
		{
		
		//document.getElementById('final_cost_td_dzn').innerHTML="Asking Profit / 4 Dzn";
		document.getElementById('confirm_price_td_dzn').innerHTML="Price Before Commn/ 4 Dzn";
		document.getElementById('prod_cost_td_dzn').innerHTML="Prd. Cost / 4 Dzn";
		document.getElementById('margin_dzn').innerHTML="Margin/ 4 Dzn";
		document.getElementById('commission_dzn').innerHTML="Commn/4 Dzn";
		document.getElementById('price_with_comm_dzn_td').innerHTML="Price with Commn / 4 Dzn";
		}

	}
	if(type=="change_caption_pcs")
	{
		
		if(value==1)
		{
		document.getElementById('final_cost_td_pcs_set').innerHTML="Final Cost/ Pcs ";
		document.getElementById('confirm_price_td_set_pcs').innerHTML="Price Before Comn/ Pcs";
		document.getElementById('asking_profit_td_pcs').innerHTML="Asking Profit/Pcs";
		document.getElementById('asking_quoted_price_psc_set').innerHTML="Asking Quoted Price/ Pcs";

		document.getElementById('first_quoted_price_psc_set').innerHTML="1st Quoted Price/ Pcs";
		document.getElementById('revised_quoted_price_psc_set').innerHTML="Revised Price/ Pcs";
		document.getElementById('price_with_comm_pcs_td').innerHTML="Price with Commn/Pcs";

		}
		if(value==58)
		{
		document.getElementById('final_cost_td_pcs_set').innerHTML="Final Cost/ Set ";
		document.getElementById('confirm_price_td_set_pcs').innerHTML="Price Before Comn/ Set";
		document.getElementById('asking_profit_td_pcs').innerHTML="Asking Profit/ Set";
		document.getElementById('asking_quoted_price_psc_set').innerHTML="Asking Quoted Price/ Set";

		document.getElementById('first_quoted_price_psc_set').innerHTML="1st Quoted Price/ Pcs";
		document.getElementById('revised_quoted_price_psc_set').innerHTML="Revised Price/ Set";
		document.getElementById('price_with_comm_pcs_td').innerHTML="Price with Commn/ Set";
		}
	}
	
}

function calculate_cm_cost_with_method()
{
	//1. CM Cost = SMV*CPM+ (SMV*CPM)* Efficiency Wastage%
//2. CM Cost= CU(SMV*CPM/ Efficiency %)+ SF(SMV*CPM/ Efficiency %)
//3. CM Cost = {(MCE/26)/NFM)*MPL)}/[{(PHL)*WH}]*Costing Per/Exchange Rate


	var cm_cost=0;
	var cbo_company_name=(document.getElementById('cbo_company_name').value)*1;
	var cm_cost_predefined_method_id=document.getElementById('cm_cost_predefined_method_id').value;
	var txt_sew_smv=parseFloat(document.getElementById('txt_sew_smv').value);
	var txt_cut_smv=parseFloat(document.getElementById('txt_cut_smv').value);
	var txt_sew_efficiency_per=parseFloat(document.getElementById('txt_sew_efficiency_per').value);
	var txt_cut_efficiency_per=parseFloat(document.getElementById('txt_cut_efficiency_per').value);
	//var txt_efficiency_wastage= parseFloat(document.getElementById('txt_efficiency_wastage').value);
	
	var cbo_currercy= document.getElementById('cbo_currercy').value;
	var txt_exchange_rate= document.getElementById('txt_exchange_rate').value;
	var txt_machine_line= document.getElementById('txt_machine_line').value;
	var txt_prod_line_hr= document.getElementById('txt_prod_line_hr').value;
	var cbo_costing_per= document.getElementById('cbo_costing_per').value;
	var cbo_costing_per_value=0
	if(cbo_costing_per==1)
	{
		cbo_costing_per_value=12;
	}
	if(cbo_costing_per==2)
	{
		cbo_costing_per_value=1;
	}
	if(cbo_costing_per==3)
	{
		cbo_costing_per_value=24;
	}
	if(cbo_costing_per==4)
	{
		cbo_costing_per_value=36;
	}
	if(cbo_costing_per==5)
	{
		cbo_costing_per_value=48;
	}
	 


	var cpm=return_global_ajax_value(cbo_company_name, 'cost_per_minute', '', 'requires/quotation_entry_controller');
	//alert(cpm);
	var data=cpm.split("_");
	//alert(cm_cost_predefined_method_id)
	if(cm_cost_predefined_method_id==1)
	{
		
		if(data[3]==0 || data[3]=="" )
		{
		   alert("Insert Cost Per Minute in Library>Merchandising Detailes>Financial Parameter Setup");
		   return;
		}
		var txt_efficiency_wastage=100-txt_sew_efficiency_per;
		document.getElementById('txt_efficiency_wastage').value=txt_efficiency_wastage;
		
		var cm_cost=(txt_sew_smv*data[3]*cbo_costing_per_value)+((txt_sew_smv*data[3]*cbo_costing_per_value)*(txt_efficiency_wastage/100));
		cm_cost=cm_cost/txt_exchange_rate;
	}
	if(cm_cost_predefined_method_id==2)
	{
		
		if(data[3]==0 ||data[3]=="" )
		{
		   alert("Insert Cost Per Minute in Library>Merchandising Detailes>Financial Parameter Setup");
		   return;
		}
		var cut_per=txt_cut_efficiency_per/100;
		var sew_per=txt_sew_efficiency_per/100;
		var cu=(txt_cut_smv*data[3]*cbo_costing_per_value)/cut_per
		var su=(txt_sew_smv*data[3]*cbo_costing_per_value)/sew_per
		var cm_cost=(cu+su)/txt_exchange_rate;
	}
	
	
	if(cm_cost_predefined_method_id==3)
	{
		//3. CM Cost = {(MCE/26)/NFM)*MPL)}/[{(PHL)*WH}]*Costing Per/Exchange Rate

		//var cpm=return_global_ajax_value(cbo_company_name, 'cost_per_minute', '', 'requires/quotation_entry_controller');
		//alert("EX"+txt_exchange_rate);
		if(cbo_currercy==0 || cbo_currercy=="")
		{
		   alert("Insert Currency");
		   return;
		}
		if(txt_exchange_rate==0 || txt_exchange_rate=="")
		{
		   alert("Insert Exchange Rate");
		   return;
		}
		if(txt_machine_line==0 || txt_machine_line=="")
		{
		   alert("Insert Machine/Line");
		   return;
		}
		if(txt_prod_line_hr==0 || txt_prod_line_hr=="")
		{
		   alert("Insert Prod/Line/Hr");
		   return;
		}
		if(cbo_costing_per==0 || cbo_costing_per=="")
		{
		   alert("Insert Costing Per");
		   return;
		}
		//alert(cm_cost_predefined_method_id)
		if(data[0]==0)
		{
		   alert("Insert Monthly CM Expense in Library>Merchandising Detailes>Financial Parameter Setup");
		   return;
		}
		if(data[1]==0)
		{
		   alert("Insert No. of Factory Machine  in Library>Merchandising Detailes>Financial Parameter Setup");
		   return;
		}
		if(data[2]==0)
		{
		   alert("Insert Working Hour in Library>Merchandising Detailes>Financial Parameter Setup");
		   return;
		}
		var per_day_cost=data[0]/26;
		var per_machine_cost=per_day_cost/data[1];
		var per_line_cost=per_machine_cost*txt_machine_line;
		var total_production_per_line=txt_prod_line_hr*data[2];
		var per_product_cost=per_line_cost/total_production_per_line;
		if(cbo_costing_per==1)
		{
			//final_cost_psc=txt_final_cost_dzn_pre_cost/12;
			var cm_cost=(per_product_cost*12)/txt_exchange_rate;
		}
		if(cbo_costing_per==2)
		{
			//final_cost_psc=txt_final_cost_dzn_pre_cost/12;
			var cm_cost=(per_product_cost*1)/txt_exchange_rate;
		}
		if(cbo_costing_per==3)
		{
			//final_cost_psc=txt_final_cost_dzn_pre_cost/12;
			var cm_cost=(per_product_cost*24)/txt_exchange_rate;
		}
		if(cbo_costing_per==4)
		{
			//final_cost_psc=txt_final_cost_dzn_pre_cost/12;
			var cm_cost=(per_product_cost*36)/txt_exchange_rate;
		}
		if(cbo_costing_per==5)
		{
			//final_cost_psc=txt_final_cost_dzn_pre_cost/12;
			var cm_cost=(per_product_cost*48)/txt_exchange_rate;
		}
		//alert(cm_cost);
		
		//var su=(txt_sew_smv*cpm)/txt_sew_efficiency_per
		//var cm_cost=per_product_cost*;
		//number_format_common(final_cost_psc, 1, 0, currency);
	}
	if(cm_cost!=Infinity)
	{
	   document.getElementById('txt_cm_pre_cost').value=number_format_common(cm_cost,1,0,cbo_currercy)	;
	   calculate_main_total()
	
	}
	
}
function calculate_main_total()
{
	var currency=(document.getElementById('cbo_currercy').value)*1;
	var dblTot_fa=(document.getElementById('txt_fabric_pre_cost').value*1)+(document.getElementById('txt_trim_pre_cost').value*1)+(document.getElementById('txt_embel_pre_cost').value*1)+(document.getElementById('txt_wash_pre_cost').value*1)+(document.getElementById('txt_comml_pre_cost').value*1)+(document.getElementById('txt_lab_test_pre_cost').value*1)+(document.getElementById('txt_inspection_pre_cost').value*1)+(document.getElementById('txt_cm_pre_cost').value*1)+(document.getElementById('txt_freight_pre_cost').value*1)+(document.getElementById('txt_currier_pre_cost').value*1)+(document.getElementById('txt_certificate_pre_cost').value*1)+(document.getElementById('txt_common_oh_pre_cost').value*1);
	document.getElementById('txt_total_pre_cost').value=number_format_common(dblTot_fa, 1, 0, currency);
	document.getElementById('txt_cost_dzn').value=number_format_common(dblTot_fa, 1, 0, currency);
	//document.getElementById('txt_final_cost_dzn_pre_cost').value=number_format_common((dblTot_fa+(document.getElementById('txt_commission_pre_cost').value)*1), 1, 0, currency);
	calculate_final_cost_pcs();
	calculate_asking_quoted_price()
	calculate_first_quoted_price_pcs('percent')
	clculate_margin_dzn();
	calculate_percent_on_po_price()
}

function calculate_final_cost_pcs()
{
	var update_id=document.getElementById('update_id').value;
	var company_id=document.getElementById('cbo_company_name').value;
	var currency=(document.getElementById('cbo_currercy').value)*1;
	var cbo_costing_per=document.getElementById('cbo_costing_per').value;
	var cbo_order_uom=document.getElementById('cbo_order_uom').value;
	var txt_total_pre_cost=(document.getElementById('txt_total_pre_cost').value)*1
	var final_cost_psc=0;
	if(cbo_costing_per==1)
	{
		final_cost_psc=txt_total_pre_cost/12;
	}
	if(cbo_costing_per==2)
	{
		final_cost_psc=txt_total_pre_cost/1;
	}
	if(cbo_costing_per==3)
	{
		final_cost_psc=txt_total_pre_cost/(2*12);
	}
	if(cbo_costing_per==4)
	{
		final_cost_psc=txt_total_pre_cost/(3*12);
	}
	if(cbo_costing_per==5)
	{
		final_cost_psc=txt_total_pre_cost/(4*12);
	}
	document.getElementById('txt_final_cost_pcs_po_price').value=number_format_common(final_cost_psc, 1, 0, currency);
	if(cbo_order_uom==58||cbo_order_uom==57)
	{
	var tot_set_qnty=(document.getElementById('tot_set_qnty').value)*1
	document.getElementById('txt_final_cost_set_pcs_rate').value=number_format_common((final_cost_psc/tot_set_qnty), 1, 0, currency);
	}
	if(cbo_order_uom==1)
	{
	document.getElementById('txt_final_cost_set_pcs_rate').value="";
	}
	var asking_profit_percent=0;
	var txt_final_cost_dzn_po_price=document.getElementById('txt_final_cost_dzn_po_price').value;
	//alert(txt_final_cost_dzn_po_price)
	if(txt_final_cost_dzn_po_price==0)
	{
		     asking_profit_percent=return_global_ajax_value(company_id, 'asking_profit_percent', '', 'requires/quotation_entry_controller');
			 document.getElementById('txt_final_cost_dzn_po_price').value=asking_profit_percent;
			// alert(asking_profit_percent)
	}
	else
	{
		asking_profit_percent=txt_final_cost_dzn_po_price;
	}
	var margin_method=1-(asking_profit_percent/100);
	var asking_profit=(number_format_common(final_cost_psc, 1, 0, currency)/margin_method)-number_format_common(final_cost_psc, 1, 0, currency);
	document.getElementById('txt_final_cost_dzn_pre_cost').value=number_format_common(asking_profit, 1, 0, currency);
	
	document.getElementById('asking_profit_td_pcs').innerHTML="Asking Profit ("+asking_profit_percent+"%)";
	//var cost_with_profit=(number_format_common(final_cost_psc,1,0,currency)*1)+(number_format_common(asking_profit,1,0,currency)*1);
	//alert(cost_with_profit)
	//var first_quoted_commision=return_global_ajax_value(update_id+"_"+cost_with_profit, 'first_quoted_commision', '', 'requires/quotation_entry_controller');
	 //alert(number_format_common(first_quoted_commision,1,0,currency));
	//document.getElementById('txt_commission_pre_cost').value=number_format_common(first_quoted_commision,1,0,currency);
    
}

function calculate_asking_quoted_price()
{
  var currency=(document.getElementById('cbo_currercy').value)*1;
  var txt_final_cost_pcs_po_price=(document.getElementById('txt_final_cost_pcs_po_price').value)*1;
  var txt_final_cost_dzn_pre_cost=(document.getElementById('txt_final_cost_dzn_pre_cost').value)*1;
  //var txt_commission_pre_cost=(document.getElementById('txt_commission_pre_cost').value)*1;	
  document.getElementById('txt_asking_quoted_price').value=number_format_common((txt_final_cost_pcs_po_price+txt_final_cost_dzn_pre_cost),1,0,currency)
}

function calculate_first_quoted_price_pcs(type)
{   
    var permission = '<? echo $permission; ?>';
	var txt_1st_quoted_po_price=(document.getElementById('txt_1st_quoted_po_price').value)*1
	var asking_profit_percent = (document.getElementById('txt_final_cost_dzn_po_price').value)*1;
	//alert(asking_profit_percent)
	permission=permission.split('_');
	/*if(permission[3]==2 && txt_1st_quoted_po_price < asking_profit_percent)
	{
		alert("Asking profit "+asking_profit_percent+" %\nYou are trying "+txt_1st_quoted_po_price+" % profit. Why?");
		document.getElementById('txt_1st_quoted_po_price').value="";
		return;
	}*/
	//else
	//{
		/*var update_id=document.getElementById('update_id').value;
		var currency=(document.getElementById('cbo_currercy').value)*1;
		var cbo_costing_per=document.getElementById('cbo_costing_per').value;
		var cbo_order_uom=document.getElementById('cbo_order_uom').value;
		var final_cost_psc=(document.getElementById('txt_final_cost_pcs_po_price').value)*1
		var margin_method=1-(txt_1st_quoted_po_price/100);
		//alert("("+final_cost_psc+"/"+margin_method+")-"+final_cost_psc);
		var first_quoted_profit=(final_cost_psc/margin_method)-final_cost_psc;
		//alert(number_format_common(first_quoted_profit,1,0,currency))
		var txt_1st_quoted_price_pre_cost_with_profit=final_cost_psc+first_quoted_profit;
		var txt_1st_quoted_price_pre_cost_with_profit_four_disit=number_format_common((final_cost_psc+first_quoted_profit),1,0,currency);
		//alert(txt_1st_quoted_price_pre_cost_with_profit_four_disit);
		var first_quoted_commision=return_global_ajax_value(update_id+"_"+txt_1st_quoted_price_pre_cost_with_profit_four_disit, 'first_quoted_commision', '', 'requires/quotation_entry_controller');
		var first_quoted_commision_four_disit=number_format_common(first_quoted_commision,1,0,currency);
		var txt_1st_quoted_price_pre_cost=number_format_common((txt_1st_quoted_price_pre_cost_with_profit_four_disit*1+first_quoted_commision_four_disit*1),1,0,currency)
		//alert(first_quoted_commision_four_disit)
		//alert(number_format_common(first_quoted_profit,1,0,currency));
		document.getElementById('txt_1st_quoted_price_pre_cost').value=txt_1st_quoted_price_pre_cost;
		calculate_confirm_price_dzn()*/
		
		var currency=(document.getElementById('cbo_currercy').value)*1;
		var final_cost_psc=(document.getElementById('txt_final_cost_pcs_po_price').value)*1
		if(type=='percent')
		{
		var margin_method=1-(txt_1st_quoted_po_price/100);
		//alert("("+final_cost_psc+"/"+margin_method+")-"+final_cost_psc);
		var txt_1st_quoted_price_pre_cost=(final_cost_psc/margin_method);
		document.getElementById('txt_1st_quoted_price_pre_cost').value=number_format_common(txt_1st_quoted_price_pre_cost,1,0,currency);
		}
		
		if(type=='percent')
		{
		var margin_method=1-(txt_1st_quoted_po_price/100);
		//alert("("+final_cost_psc+"/"+margin_method+")-"+final_cost_psc);
		var txt_1st_quoted_price_pre_cost=(final_cost_psc/margin_method);
		document.getElementById('txt_1st_quoted_price_pre_cost').value=number_format_common(txt_1st_quoted_price_pre_cost,1,0,currency);
		}
		if(type=='value')
		{
		var txt_1st_quoted_price_pre_cost=(document.getElementById('txt_1st_quoted_price_pre_cost').value)*1
		var percent=((txt_1st_quoted_price_pre_cost-final_cost_psc)/txt_1st_quoted_price_pre_cost)*100;
		//var margin_method=1-(txt_1st_quoted_po_price/100);
		//alert("("+final_cost_psc+"/"+margin_method+")-"+final_cost_psc);
		//var txt_1st_quoted_price_pre_cost=(final_cost_psc/margin_method);
		//alert(permission[3])
		
		//this portation needed
		/*if(permission[3]==2 && percent < asking_profit_percent)
		{
			alert("Asking profit "+asking_profit_percent+" %\nYou are trying "+txt_1st_quoted_po_price+" % profit. Why?");
			document.getElementById('txt_1st_quoted_po_price').value="";
			ocument.getElementById('txt_1st_quoted_price_pre_cost').value="";
			return;
		}*/
		//this portation needed end
		document.getElementById('txt_1st_quoted_po_price').value=number_format_common(percent,1,0,currency);
		$('#txt_1st_quoted_po_price').removeAttr("title").attr("title",number_format_common(percent,1,0,currency));

		}
		calculate_confirm_price_dzn()
	//}
}



function calculate_confirm_price_dzn()
{
  var update_id=document.getElementById('update_id').value;
  var currency=(document.getElementById('cbo_currercy').value)*1;
  var cbo_order_uom=document.getElementById('cbo_order_uom').value;
  var cbo_costing_per=document.getElementById('cbo_costing_per').value;	
  var txt_confirm_price_pre_cost=(document.getElementById('txt_confirm_price_pre_cost').value)*1;
  if(txt_confirm_price_pre_cost=="" && txt_confirm_price_pre_cost==0)
  {
	  var txt_revised_price_pre_cost=(document.getElementById('txt_revised_price_pre_cost').value)*1;
	  if(txt_revised_price_pre_cost!="" && txt_revised_price_pre_cost!=0)
	  {
		  txt_confirm_price_pre_cost=txt_revised_price_pre_cost;
	  }
	  else
	  {
	  var txt_confirm_price_pre_cost=(document.getElementById('txt_1st_quoted_price_pre_cost').value)*1;
	  }
  }
  //alert(txt_confirm_price_pre_cost)

  if(cbo_costing_per==1)
  {
  //document.getElementById('txt_confirm_price_pre_cost_dzn').value=number_format_common((txt_confirm_price_pre_cost*12), 1, 0, currency);
  var txt_confirm_price_pre_cost_dzn=number_format_common((txt_confirm_price_pre_cost*12), 1, 0, currency);
  }
  if(cbo_costing_per==2)
  {
  var txt_confirm_price_pre_cost_dzn=number_format_common((txt_confirm_price_pre_cost*1), 1, 0, currency);
  }
  if(cbo_costing_per==3)
  {
  var txt_confirm_price_pre_cost_dzn=number_format_common((txt_confirm_price_pre_cost*12*2), 1, 0, currency);
  }
  if(cbo_costing_per==4)
  {
  var txt_confirm_price_pre_cost_dzn=number_format_common((txt_confirm_price_pre_cost*12*3), 1, 0, currency);
  }
  if(cbo_costing_per==5)
  {
  var txt_confirm_price_pre_cost_dzn=number_format_common((txt_confirm_price_pre_cost*12*4), 1, 0, currency);
  }
  document.getElementById('txt_confirm_price_pre_cost_dzn').value=txt_confirm_price_pre_cost_dzn;
  if(cbo_order_uom==58 || cbo_order_uom==57)
	{
	var tot_set_qnty=(document.getElementById('tot_set_qnty').value)*1
	document.getElementById('txt_confirm_price_set_pcs_rate').value=number_format_common((txt_confirm_price_pre_cost/tot_set_qnty), 1, 0, currency);
	}
	if(cbo_order_uom==1)
	{
	document.getElementById('txt_confirm_price_set_pcs_rate').value="";
	}
	
	var cofirm_price_commision=return_global_ajax_value(update_id+"_"+cbo_costing_per+"_"+txt_confirm_price_pre_cost_dzn, 'cofirm_price_commision', '', 'requires/quotation_entry_controller');
	
	document.getElementById('txt_commission_pre_cost').value=number_format_common(cofirm_price_commision,1,0,currency);
	
   clculate_margin_dzn();
   calculate_price_with_commision_dzn()
   calculate_percent_on_po_price();
}
function clculate_margin_dzn()
{
	var currency=document.getElementById('cbo_currercy').value
	var txt_confirm_price_pre_cost_dzn=(document.getElementById('txt_confirm_price_pre_cost_dzn').value)*1;
	var txt_total_pre_cost=(document.getElementById('txt_total_pre_cost').value)*1;
	document.getElementById('txt_margin_dzn_pre_cost').value=number_format_common((txt_confirm_price_pre_cost_dzn-txt_total_pre_cost), 1, 0, currency);
	/*var currency=document.getElementById('cbo_currercy').value
	var txt_confirm_price_pre_cost_dzn=(document.getElementById('txt_confirm_price_pre_cost_dzn').value)*1;
	var txt_total_pre_cost=(document.getElementById('txt_total_pre_cost').value)*1;
	var txt_commission_pre_cost=(document.getElementById('txt_commission_pre_cost').value)*1*/
	//document.getElementById('txt_margin_dzn_pre_cost').value=number_format_common((txt_confirm_price_pre_cost_dzn-(txt_total_pre_cost+txt_commission_pre_cost*12)), 1, 0, currency);
}

function calculate_percent_on_po_price()
{
	var cbo_costing_per_value=0;
	 var cbo_costing_per=document.getElementById('cbo_costing_per').value;
	 if(cbo_costing_per==1)
	 {
		cbo_costing_per_value=1*12; 
	 }
	 if(cbo_costing_per==2)
	 {
		cbo_costing_per_value=1*1; 
	 }
	 if(cbo_costing_per==3)
	 {
		cbo_costing_per_value=2*12; 
	 }
	 if(cbo_costing_per==4)
	 {
		cbo_costing_per_value=3*12; 
	 }
	 if(cbo_costing_per==5)
	 {
		cbo_costing_per_value=4*12; 
	 }
	//var cofirm_price_commision_four_disit=document.getElementById('txt_txt_cost_comm_dzn_po_price_dzn').value;
  var txt_confirm_price_pre_cost_dzn=(document.getElementById('txt_confirm_price_pre_cost_dzn').value)*1;
  document.getElementById('txt_fabric_po_price').value=number_format_common(((((document.getElementById('txt_fabric_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_trim_po_price').value=number_format_common(((((document.getElementById('txt_trim_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_embel_po_price').value=number_format_common(((((document.getElementById('txt_embel_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
 
  document.getElementById('txt_wash_po_price').value=number_format_common(((((document.getElementById('txt_wash_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_comml_po_price').value=number_format_common(((((document.getElementById('txt_comml_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_lab_test_po_price').value=number_format_common(((((document.getElementById('txt_lab_test_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_inspection_po_price').value=number_format_common(((((document.getElementById('txt_inspection_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_cm_po_price').value=number_format_common(((((document.getElementById('txt_cm_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_freight_po_price').value=number_format_common(((((document.getElementById('txt_freight_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  
  document.getElementById('txt_currier_po_price').value=number_format_common(((((document.getElementById('txt_currier_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  
   document.getElementById('txt_certificate_po_price').value=number_format_common(((((document.getElementById('txt_certificate_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  
  document.getElementById('txt_common_oh_po_price').value=number_format_common(((((document.getElementById('txt_common_oh_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_total_po_price').value=number_format_common(((((document.getElementById('txt_total_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_cost_dzn_po_price').value=number_format_common(((((document.getElementById('txt_total_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_commission_po_price').value=number_format_common(((((document.getElementById('txt_commission_pre_cost').value)*1)/(txt_confirm_price_pre_cost_dzn+(document.getElementById('txt_commission_pre_cost').value)*1))*100), 7, 0);
   document.getElementById('txt_asking_quoted_po_price').value=number_format_common(((((document.getElementById('txt_asking_quoted_price').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  //document.getElementById('txt_final_cost_dzn_po_price').value=number_format_common(((((document.getElementById('txt_final_cost_dzn_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_confirm_price_po_price_dzn').value=number_format_common(((((document.getElementById('txt_confirm_price_pre_cost_dzn').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
  document.getElementById('txt_margin_dzn_po_price').value=number_format_common(((((document.getElementById('txt_margin_dzn_pre_cost').value)*1)/txt_confirm_price_pre_cost_dzn)*100), 7, 0);
}

function calculate_price_with_commision_dzn()
{
	var cbo_costing_per=document.getElementById('cbo_costing_per').value;	
	var currency=document.getElementById('cbo_currercy').value
    var txt_confirm_price_pre_cost_dzn= document.getElementById('txt_confirm_price_pre_cost_dzn').value*1;
    var txt_commission_pre_cost= document.getElementById('txt_commission_pre_cost').value*1;
	var price_with_commision_dzn=number_format_common((txt_confirm_price_pre_cost_dzn+txt_commission_pre_cost),1,0,currency)
	//alert(price_with_commision_dzn)
	document.getElementById('txt_with_commission_pre_cost_dzn').value=price_with_commision_dzn;
	var price_with_commision_pcs=0;
	if(cbo_costing_per==1)
	{
	//document.getElementById('txt_confirm_price_pre_cost_dzn').value=number_format_common((txt_confirm_price_pre_cost*12), 1, 0, currency);
	 price_with_commision_pcs=number_format_common((price_with_commision_dzn/12), 1, 0, currency);
	}
	if(cbo_costing_per==2)
	{
	 price_with_commision_pcs=number_format_common((price_with_commision_dzn/1), 1, 0, currency);
	}
	if(cbo_costing_per==3)
	{
	 price_with_commision_pcs=number_format_common((price_with_commision_dzn/24), 1, 0, currency);
	}
	if(cbo_costing_per==4)
	{
	 price_with_commision_pcs=number_format_common((price_with_commision_dzn/36), 1, 0, currency);
	}
	if(cbo_costing_per==5)
	{
	 price_with_commision_pcs=number_format_common((price_with_commision_dzn/48), 1, 0, currency);
	}
	document.getElementById('txt_with_commission_pre_cost_pcs').value=price_with_commision_pcs;
}



function fnc_quotation_entry_dtls( operation )
{
	if(operation==2)
	{
		alert("Delete Restricted")
		return;
	}
	if (form_validation('cbo_company_name','Company Name')==false)
	{
		return;
	}
	//return_global_ajax_value(update_id, 'txt_commission_pre_cost', '', 'requires/quotation_entry_controller');
	var currency=document.getElementById('cbo_currercy').value
	var update_id=document.getElementById('update_id').value;
	var txt_commission_pre_cost=document.getElementById('txt_commission_pre_cost').value*1;
	var cofirm_price_commision=return_global_ajax_value(update_id, 'txt_commission_pre_cost', '', 'requires/quotation_entry_controller');
	//alert(cofirm_price_commision)
	var cofirm_price_commision_4disit=number_format_common(cofirm_price_commision,1,0,currency)*1;
	if(cofirm_price_commision_4disit !=txt_commission_pre_cost)
	{
		//alert("Commision cost mismatch\n Just Click Update Button On Commission Cost")
		$('#txt_commission_pre_cost').focus()
		recalculate_commision_cost()
		//alert("Click Update Button on Commission Cost")
		//document.getElementById('txt_commission_pre_cost').style.backgroundColor("#FF0000");
		//document.getElementById('txtamountcommission_sum').style.backgroundColor("#FF0000");
		//return;
	}
	
	var txt_confirm_price_pre_cost=document.getElementById('txt_confirm_price_pre_cost').value
	var txt_confirm_date_pre_cost=document.getElementById('txt_confirm_date_pre_cost').value
	if (txt_confirm_price_pre_cost !="" && form_validation('txt_confirm_date_pre_cost','Confirm Date')==false)
	{
		return;
	}
	if (txt_confirm_date_pre_cost !="" && form_validation('txt_confirm_price_pre_cost','Price Before Comn')==false)
	{
		return;
	}
	/*if(txt_confirm_price_pre_cost !="" && txt_confirm_date_pre_cost=="")
	{
		alert("Insert Confirm Date");
		return;
	}*/
	
	if(txt_confirm_price_pre_cost =="" && txt_confirm_date_pre_cost!="")
	{
		alert("Insert Confirm Price");
		return;
	}
														
	else
	{
		//eval(get_submitted_variables('update_id*cbo_costing_per*cbo_order_uom*update_id_dtls*txt_fabric_pre_cost*txt_fabric_po_price*txt_trim_pre_cost*txt_trim_po_price*txt_embel_pre_cost*txt_embel_po_price*txt_comml_pre_cost*txt_comml_po_price*txt_lab_test_pre_cost*txt_lab_test_po_price*txt_inspection_pre_cost*txt_inspection_po_price*txt_cm_pre_cost*txt_cm_po_price*txt_freight_pre_cost*txt_freight_po_price*txt_common_oh_pre_cost*txt_common_oh_po_price*txt_total_pre_cost*txt_total_po_price*txt_commission_pre_cost*txt_commission_po_price*txt_final_cost_dzn_pre_cost*txt_final_cost_dzn_po_price*txt_final_cost_pcs_po_price*txt_final_cost_set_pcs_rate*txt_1st_quoted_price_pre_cost*txt_first_quoted_price_date*txt_revised_price_pre_cost*txt_revised_price_date*txt_confirm_price_pre_cost*txt_confirm_price_set_pcs_rate*txt_confirm_price_pre_cost_dzn*txt_confirm_price_po_price_dzn*txt_margin_dzn_pre_cost*txt_margin_dzn_po_price*txt_confirm_date_pre_cost'));
		var data="action=save_update_delete_quotation_entry_dtls&operation="+operation+get_submitted_data_string('update_id*cbo_costing_per*cbo_order_uom*update_id_dtls*txt_fabric_pre_cost*txt_fabric_po_price*txt_trim_pre_cost*txt_trim_po_price*txt_embel_pre_cost*txt_embel_po_price*txt_wash_pre_cost*txt_wash_po_price*txt_comml_pre_cost*txt_comml_po_price*txt_lab_test_pre_cost*txt_lab_test_po_price*txt_inspection_pre_cost*txt_inspection_po_price*txt_cm_pre_cost*txt_cm_po_price*txt_freight_pre_cost*txt_freight_po_price*txt_currier_pre_cost*txt_currier_po_price*txt_certificate_pre_cost*txt_certificate_po_price*txt_common_oh_pre_cost*txt_common_oh_po_price*txt_total_pre_cost*txt_total_po_price*txt_commission_pre_cost*txt_commission_po_price*txt_final_cost_dzn_pre_cost*txt_final_cost_dzn_po_price*txt_final_cost_pcs_po_price*txt_final_cost_set_pcs_rate*txt_1st_quoted_price_pre_cost*txt_1st_quoted_po_price*txt_first_quoted_price_date*txt_revised_price_pre_cost*txt_revised_price_date*txt_confirm_price_pre_cost*txt_confirm_price_set_pcs_rate*txt_confirm_price_pre_cost_dzn*txt_confirm_price_po_price_dzn*txt_margin_dzn_pre_cost*txt_margin_dzn_po_price*txt_confirm_date_pre_cost*txt_asking_quoted_price*txt_asking_quoted_po_price*txt_with_commission_pre_cost_dzn*txt_with_commission_po_price_dzn*txt_with_commission_pre_cost_pcs*txt_with_commission_po_price_pcs*txt_terget_qty',"../../");
		freeze_window(operation);
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_quotation_entry_dtls_reponse;
	}
}

function fnc_quotation_entry_dtls_reponse()
{
	if(http.readyState == 4) 
	{
		var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		document.getElementById('update_id_dtls').value  = reponse[2];
		set_button_status(1, permission, 'fnc_quotation_entry_dtls',2);
		summary()
		release_freezing();
	}
}


function fnc_quotation_entry_dtls1( operation )
{	
	if (form_validation('cbo_company_name','Company Name')==false)
	{
		return;
	}
	var currency=document.getElementById('cbo_currercy').value
	var update_id=document.getElementById('update_id').value;
	var txt_commission_pre_cost=document.getElementById('txt_commission_pre_cost').value*1;
	var cofirm_price_commision=return_global_ajax_value(update_id, 'txt_commission_pre_cost', '', 'requires/quotation_entry_controller');
	var cofirm_price_commision_4disit=number_format_common(cofirm_price_commision,1,0,currency)*1;
	if(cofirm_price_commision_4disit != txt_commission_pre_cost)
	{
		//alert("Commision cost mismatch\n Just Click Update Button On Commission Cost")
		$('#txt_commission_pre_cost').focus()
		recalculate_commision_cost()
		//document.getElementById('txt_commission_pre_cost').style.backgroundColor("#FF0000");
		//document.getElementById('txtamountcommission_sum').style.backgroundColor("#FF0000");
		//return;
	}
	var txt_confirm_price_pre_cost=document.getElementById('txt_confirm_price_pre_cost').value
	var txt_confirm_date_pre_cost=document.getElementById('txt_confirm_date_pre_cost').value
	if (txt_confirm_price_pre_cost !="" && form_validation('txt_confirm_date_pre_cost','Confirm Date')==false)
	{
		return;
	}
	if (txt_confirm_date_pre_cost !="" && form_validation('txt_confirm_price_pre_cost','Price Before Comn')==false)
	{
		return;
	}
	else
	{
		//eval(get_submitted_variables('update_id*cbo_costing_per*cbo_order_uom*update_id_dtls*txt_fabric_pre_cost*txt_fabric_po_price*txt_trim_pre_cost*txt_trim_po_price*txt_embel_pre_cost*txt_embel_po_price*txt_comml_pre_cost*txt_comml_po_price*txt_lab_test_pre_cost*txt_lab_test_po_price*txt_inspection_pre_cost*txt_inspection_po_price*txt_cm_pre_cost*txt_cm_po_price*txt_freight_pre_cost*txt_freight_po_price*txt_common_oh_pre_cost*txt_common_oh_po_price*txt_total_pre_cost*txt_total_po_price*txt_commission_pre_cost*txt_commission_po_price*txt_final_cost_dzn_pre_cost*txt_final_cost_dzn_po_price*txt_final_cost_pcs_po_price*txt_final_cost_set_pcs_rate*txt_1st_quoted_price_pre_cost*txt_first_quoted_price_date*txt_revised_price_pre_cost*txt_revised_price_date*txt_confirm_price_pre_cost*txt_confirm_price_set_pcs_rate*txt_confirm_price_pre_cost_dzn*txt_confirm_price_po_price_dzn*txt_margin_dzn_pre_cost*txt_margin_dzn_po_price*txt_confirm_date_pre_cost'));
		var data="action=save_update_delete_quotation_entry_dtls&operation="+operation+get_submitted_data_string('update_id*cbo_costing_per*cbo_order_uom*update_id_dtls*txt_fabric_pre_cost*txt_fabric_po_price*txt_trim_pre_cost*txt_trim_po_price*txt_embel_pre_cost*txt_embel_po_price*txt_wash_pre_cost*txt_wash_po_price*txt_comml_pre_cost*txt_comml_po_price*txt_lab_test_pre_cost*txt_lab_test_po_price*txt_inspection_pre_cost*txt_inspection_po_price*txt_cm_pre_cost*txt_cm_po_price*txt_freight_pre_cost*txt_freight_po_price*txt_currier_pre_cost*txt_currier_po_price*txt_certificate_pre_cost*txt_certificate_po_price*txt_common_oh_pre_cost*txt_common_oh_po_price*txt_total_pre_cost*txt_total_po_price*txt_commission_pre_cost*txt_commission_po_price*txt_final_cost_dzn_pre_cost*txt_final_cost_dzn_po_price*txt_final_cost_pcs_po_price*txt_final_cost_set_pcs_rate*txt_1st_quoted_price_pre_cost*txt_1st_quoted_po_price*txt_first_quoted_price_date*txt_revised_price_pre_cost*txt_revised_price_date*txt_confirm_price_pre_cost*txt_confirm_price_set_pcs_rate*txt_confirm_price_pre_cost_dzn*txt_confirm_price_po_price_dzn*txt_margin_dzn_pre_cost*txt_margin_dzn_po_price*txt_confirm_date_pre_cost*txt_asking_quoted_price*txt_asking_quoted_po_price*txt_with_commission_pre_cost_dzn*txt_with_commission_po_price_dzn*txt_with_commission_pre_cost_pcs*txt_with_commission_po_price_pcs*txt_terget_qty',"../../");
		
		http.onreadystatechange = function(){
			if(http.readyState == 4 && http.status == 200) 
			{
				var reponse=trim(http.responseText).split('**');
				if (reponse[0].length>2) reponse[0]=10;
				show_msg(reponse[0]);
				document.getElementById('update_id_dtls').value  = reponse[2];
				
				set_button_status(1, permission, 'fnc_quotation_entry_dtls',2);
				summary()
			}
		}
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
	}
}
// End Dtls Form ------------------------------------------
//created by Bilas-------------------------------------------------------


function summary()
{
	var update_id=document.getElementById('update_id').value;
	var cbo_costing_per=document.getElementById('cbo_costing_per').value*1;	
	var currency=document.getElementById('cbo_currercy').value*1
	var txt_asking_quoted_price = document.getElementById('txt_asking_quoted_price').value*1
	var txt_with_commission_pre_cost_pcs = document.getElementById('txt_with_commission_pre_cost_pcs').value*1
	var txt_final_cost_pcs_po_price = document.getElementById('txt_final_cost_pcs_po_price').value*1
	var txt_commission_pre_cost = document.getElementById('txt_commission_pre_cost').value*1
	var txt_commission_po_price = document.getElementById('txt_commission_po_price').value/100;
	var txt_offer_qnty = document.getElementById('txt_offer_qnty').value*1
	if(cbo_costing_per==1)
	{
		cbo_costing_per_value=12;
	}
	if(cbo_costing_per==2)
	{
		cbo_costing_per_value=1;
	}
	if(cbo_costing_per==3)
	{
		cbo_costing_per_value=24;
	}
	if(cbo_costing_per==4)
	{
		cbo_costing_per_value=36;
	}
	if(cbo_costing_per==5)
	{
		cbo_costing_per_value=48;
	}
	
	var txt_asking_quoted_price_commision=return_global_ajax_value(update_id+"_"+cbo_costing_per+"_"+txt_asking_quoted_price, 'cofirm_price_commision', '', 'requires/quotation_entry_controller');
	//alert()
	var txt_asking_quoted_price_commision_4=number_format_common(txt_asking_quoted_price_commision,1,0,currency)*1;
	var txt_expected_1=number_format_common((txt_asking_quoted_price+txt_asking_quoted_price_commision_4),1,0,currency);
	document.getElementById('txt_expected_1').value=txt_expected_1;
	document.getElementById('txt_confirmed_1').value=txt_with_commission_pre_cost_pcs;
	document.getElementById('txt_expected_2').value=txt_final_cost_pcs_po_price;
	document.getElementById('txt_confirmed_2').value=txt_final_cost_pcs_po_price;
	var txt_confirmed_3=number_format_common((txt_commission_pre_cost/cbo_costing_per_value),1,0,currency);
	document.getElementById('txt_confirmed_3').value=number_format_common((txt_commission_pre_cost/cbo_costing_per_value),1,0,currency);
	document.getElementById('txt_deviation_2').value=txt_final_cost_pcs_po_price-txt_final_cost_pcs_po_price;
	
	var confirm_4=txt_with_commission_pre_cost_pcs-txt_final_cost_pcs_po_price-(number_format_common((txt_commission_pre_cost/cbo_costing_per_value),1,0,currency)*1)
	//alert(confirm_4)
	
	document.getElementById('txt_confirmed_4').value=number_format_common((confirm_4),1,0,currency);
	var txt_deviation_1=txt_with_commission_pre_cost_pcs*1-(number_format_common((txt_asking_quoted_price+txt_asking_quoted_price_commision_4),1,0,currency)*1);
	document.getElementById('txt_deviation_1').value=number_format_common(txt_deviation_1,1,0,currency);
	var txt_expected_3=(number_format_common((txt_asking_quoted_price+txt_asking_quoted_price_commision_4),1,0,currency)*1)*txt_commission_po_price;
	document.getElementById('txt_expected_3').value=number_format_common(txt_expected_3,1,0,currency);
	var txt_expected_4=number_format_common((txt_expected_1*1-txt_final_cost_pcs_po_price*1-txt_expected_3*1),1,0,currency);
	document.getElementById('txt_expected_4').value=txt_expected_4;
	// alert(document.getElementById('txt_confirmed_4').value);
	document.getElementById('txt_deviation_3').value=number_format_common((txt_expected_3*1-txt_confirmed_3*1),1,0,currency);
	document.getElementById('txt_deviation_4').value=number_format_common((confirm_4*1-txt_expected_4*1),1,0,currency);
	
	document.getElementById('txt_expected_5').value=number_format_common(txt_expected_4*cbo_costing_per_value,1,0,currency);
	document.getElementById('txt_confirmed_5').value=number_format_common((confirm_4*cbo_costing_per_value),1,0,currency);
	document.getElementById('txt_deviation_5').value=number_format_common((number_format_common((confirm_4*cbo_costing_per_value),1,0,currency)*1-number_format_common(txt_expected_4*cbo_costing_per_value,1,0,currency)*1),1,0,currency);
	document.getElementById('txt_expected_6').value=number_format_common(txt_expected_4*txt_offer_qnty,1,0,currency);
	document.getElementById('txt_confirmed_6').value=number_format_common((confirm_4*txt_offer_qnty),1,0,currency);
	document.getElementById('txt_deviation_6').value=number_format_common((number_format_common((confirm_4*txt_offer_qnty),1,0,currency)*1-number_format_common(txt_expected_4*txt_offer_qnty,1,0,currency)*1),1,0,currency);
}

function generate_report(type)
{
	//alert(type)
	if (form_validation('txt_quotation_id','Please Select The Job Number.')==false)
	{
		return;
	}
	else
	{
		eval(get_submitted_variables('txt_quotation_id*cbo_company_name*cbo_buyer_name*txt_style_ref*txt_quotation_date'));
		var data="action=generate_report&type="+type+"&"+get_submitted_data_string('txt_quotation_id*cbo_company_name*cbo_buyer_name*txt_style_ref*txt_quotation_date',"../../");
		
		http.open("POST","requires/quotation_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_generate_report_reponse;
	}
}


function fnc_generate_report_reponse()
{
	if(http.readyState == 4) 
	{
		$('#data_panel').html( http.responseText );
		var w = window.open("Surprise", "_blank");
		var d = w.document.open();
		d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel').innerHTML+'</body</html>');
		d.close();
	}
}

</script>

 
</head>
 
<body onLoad="set_hotkey();set_auto_complete('price_quation_mst')" >
    <div style="width:100%;" align="center">
        <? echo load_freeze_divs ("../../",$permission);  ?>
        <h3 align="left" class="accordion_h" onClick="show_hide_content('master_form', '')"> -Master Part </h3>
       <div id="content_master_form">  
        <table width="100%"  cellpadding="0" cellspacing="2" align="center">
            <tr>
                <td width="70%" align="left" valign="top">  <!--   Form Left Container -->
                    <fieldset style="width:1070px;">
                        <legend>Price Quotation</legend>
                        <form name="quotationmst_1" id="quotationmst_1" autocomplete="off"> 
                            <div style="width:1070px;">  
                                <table  width="100%" cellspacing="2" cellpadding=""  border="0">
                                	<tr>
                                        <td align="right"  class="must_entry_caption" colspan="4">Quotation ID</td>
                                        <td  colspan="4">
                                        <input type="text" id="txt_quotation_id" name="txt_quotation_id" class="text_boxes" style="width:150px;" readonly placeholder="Browse Quotation" onDblClick="openmypage('requires/quotation_entry_controller.php?action=quotation_id_popup','Quotation ID Selection Form')"/>
                                        </td>
                                    </tr>
                                    <tr>
                                       <td align="right" width="150" >Inquery ID</td>
                                        <td  width="150">
                                        <input type="text" id="txt_inquery_prifix" name="txt_inquery_prifix" class="text_boxes" style="width:150px;" readonly placeholder="Browse Inquery" onDblClick="openmypage_inquery()"/>
                                        <input type="hidden" id="txt_inquery_id" name="txt_inquery_id" class="text_boxes" style="width:150px;"  />
                                        </td>
                                        <td  align="right"  width="150" class="must_entry_caption">Company</td>
                                        <td  width="150">
                                        <?
                                        echo create_drop_down( "cbo_company_name", 160, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/quotation_entry_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );  load_drop_down( 'requires/quotation_entry_controller', this.value, 'load_drop_down_agent', 'agent_td');cm_cost_predefined_method(this.value)" );
                                        ?>
                                        </td>
                                        <td align="right"  width="180" class="must_entry_caption">Buyer</td>
                                        <td id="buyer_td"  width="150">
                                        <? 
                                        echo create_drop_down( "cbo_buyer_name", 160, $blank_array,"", 1, "-- Select Buyer --", $selected, "" );
                                        ?>
                                        </td>
                                        <td align="right"  width="130" class="must_entry_caption">Style Ref</td>
                                        <td>
                                        <input class="text_boxes" type="text" style="width:150px;" name="txt_style_ref" id="txt_style_ref" maxlength="75" title="Maximum 75 Character" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Revised No</td>
                                        <td><input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_revised_no" id="txt_revised_no"/></td>
                                        <td align="right">Pord. Dept.</td>
                                        <td>
                                        <? 
                                        echo create_drop_down( "cbo_pord_dept", 100, $product_dept,"", 1, "-- Select --",0, "" );
                                        ?>
                                        <input class="text_boxes" type="text" style="width:40px;" name="txt_product_code" id="txt_product_code" maxlength="10" title="Maximum 10 Character" />
                                        </td>
                                        <td align="right">Style Desc.</td>
                                        <td ><input class="text_boxes" type="text" style="width:150px;" name="txt_style_desc" id="txt_style_desc" maxlength="100" title="Maximum 100 Character"/></td>
                                        <td align="right">M. List No</td>
                                        <td ><input class="text_boxes" type="text" style="width:150px;" name="txt_m_list_no" id="txt_m_list_no" /></td>
                                    </tr> 
                                    <tr>
                                        <td align="right" class="must_entry_caption">Currency</td>
                                        <td>
                                        <? 
                                        echo create_drop_down( "cbo_currercy",60, $currency,"", 0, "", 2, "set_exchange_rate(this.value)" ,"","");
                                        ?>	
                                        ER. <input class="text_boxes_numeric" type="text" style="width:60px;" value="80" name="txt_exchange_rate" id="txt_exchange_rate" onChange="calculate_cm_cost_with_method()"/>  
                                        </td>
                                        <td align="right">Agent</td>
                                        <td id="agent_td">
                                        <?	  
                                        echo create_drop_down( "cbo_agent", 160, $blank_array,"", 1, "-- Select Agent --", $selected, "" );
                                        ?>
                                        </td>
                                        <td align="right">Offer Qnty.</td>
                                        <td><input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_offer_qnty" id="txt_offer_qnty"/></td>
                                        <td align="right">Region</td>
                                        <td>
                                        <? 
                                        echo create_drop_down( "cbo_region", 160, $region, 1, "-- Select Region --", 0, "" );
                                        ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="right">Color Range</td>
                                        <td>
                                        <? 
                                        echo create_drop_down( "cbo_color_range", 160, $color_range,"", 1, "-- Select--", 0, "" );
                                        ?>
                                        </td>
                                        <td  align="right">Incoterm</td>
                                        <td>
                                        <? 
                                        echo create_drop_down( "cbo_inco_term", 160, $incoterm,"", 0, "",1,"" );
                                        ?>
                                        </td>
                                        <td align="right">Incoterm Place</td>
                                        <td><input class="text_boxes" type="text" style="width:150px;" name="txt_incoterm_place" id="txt_incoterm_place" maxlength="100" title="Maximum 100 Character"/></td>
                                        <td align="right">Machine/Line</td>
                                        <td><input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_machine_line" id="txt_machine_line" /></td> 
                                    </tr> 
                                    <tr>
                                        <td align="right">Prod/Line/Hr</td>
                                        <td><input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_prod_line_hr" id="txt_prod_line_hr" /></td>
                                        
                                        
                                        <td align="right" class="must_entry_caption">Costing Per</td>
                                        <td>
                                        <? 
                                        echo create_drop_down( "cbo_costing_per", 160, $costing_per, "",1, "-- Select--", 1, "change_caption_cost_dtls( this.value, 'change_caption_dzn' )","","" );
                                        
                                        ?>
                                        </td>
                                        <td align="right" class="must_entry_caption">Quot. Date</td>
                                        <td><input class="datepicker" type="text" style="width:150px;" name="txt_quotation_date" id="txt_quotation_date"/></td>
                                        
                                        <td align="right">OP Date</td>
                                        <td><input class="datepicker" type="text" style="width:150px;" name="txt_op_date" id="txt_op_date" onChange="calculate_lead_time()"/></td>
                                    
                                    </tr>
                                    <tr>
                                        <td align="right">Factory</td>
                                        <td><input class="text_boxes" type="text" style="width:150px;" name="txt_factory" id="txt_factory" maxlength="100" title="Maximum 100 Character"/></td>
                                        
                                        <td align="right" class="must_entry_caption">Order UOM </td> 
                                        <td>
                                        <? 
                                        echo create_drop_down( "cbo_order_uom",60, $unit_of_measurement, "",0, "", 1, "change_caption_cost_dtls( this.value, 'change_caption_pcs' )","","1,58" );
                                        ?>
                                        <input type="button" id="set_button" class="image_uploader" style="width:95px;" value="Item Details" onClick="open_set_popup(document.getElementById('cbo_order_uom').value)" />
                                        <input type="hidden" id="set_breck_down" />     
                                        <input type="hidden" id="item_id" /> 
                                        <input type="hidden" id="tot_set_qnty" />    
                                        </td>
                                        <td align="right">Images</td>
                                        <td><input type="button" id="image_button" class="image_uploader" style="width:160px" value="CLICK TO ADD IMAGE" onClick="file_uploader ( '../../', document.getElementById('update_id').value,'', 'quotation_entry', 0 ,1)" />                                          
                                        
                                        </td> 
                                        <td align="right">Est. Ship Date</td>
                                        <td>
                                        <input class="datepicker" type="text" style="width:70px;" name="txt_est_ship_date" id="txt_est_ship_date" onChange="calculate_lead_time()"/>
                                        <input class="text_boxes" type="text" style="width:60px;"  name="txt_lead_time" id="txt_lead_time"  readonly/>
                                        </td>
                                    </tr>
                                    <tr> 
                                        <td align="right">Sew. SMV</td>
                                        <td >
                                        <input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_sew_smv" id="txt_sew_smv" onChange="calculate_cm_cost_with_method()" />  
                                        </td>
                                        <td align="right">Sew Effi. %</td>
                                        <td>
                                        <input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_sew_efficiency_per" id="txt_sew_efficiency_per" onChange="calculate_cm_cost_with_method()" />  
                                        </td>
                                        <td align="right">Cut. SMV</td>
                                        <td >
                                        <input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_cut_smv" id="txt_cut_smv" onChange="calculate_cm_cost_with_method()" />  
                                        </td>
                                        
                                        <td align="right">Cut Efficiency %</td>
                                        <td>
                                        <input class="text_boxes_numeric" type="text" style="width:150px;" name="txt_cut_efficiency_per" id="txt_cut_efficiency_per" onChange="calculate_cm_cost_with_method()"  />  
                                        </td>
                                    </tr>
                                    <tr> 
                                        <td align="right">Season</td>
                                        <td>
                                        <input class="text_boxes" type="text" style="width:150px;" name="txt_season" id="txt_season" maxlength="50" title="Maximum 500 Character"/>  
                                        </td>
                                        <td align="right">Remarks</td>
                                        
                                        <td>
                                        <input class="text_boxes" type="text" style="width:150px;" name="txt_remarks" id="txt_remarks" maxlength="500"/>  
                                        </td>
                                        <td align="right">BH Marchant</td>
                                        
                                        <td>
                                        <input class="text_boxes" type="text" style="width:150px;" name="txt_bh_marchant" id="txt_bh_marchant" maxlength="500"/>  
                                        </td>
                                        <td align="right">Approved</td>
                                        <td width=""> 
                                        <? 
                                        echo create_drop_down( "cbo_approved_status", 160, $yes_no,"", 0, "", 2, "",1,"" );
                                        ?>
                                        </td>
                                    </tr>
                                    <tr> 
                                        <td align="right" style="display:none">Efficiency Wastage%</td>
                                        <td >
                                        <input class="text_boxes_numeric" type="hidden" style="width:150px;" name="txt_efficiency_wastage" id="txt_efficiency_wastage" onChange="calculate_cm_cost_with_method()" readonly />  
                                        </td>
                                        <td align="center" height="10" valign="middle"  colspan="6">  </td>
                                    </tr>
                                    <tr> 
                                        <td align="right" valign="top" class="button_container" colspan="5"> 
                                        <input type="hidden" id="cm_cost_predefined_method_id" value="" />
                                        <input type="hidden" id="update_id" value="" />
                                        <? 
                                        $dd="disable_enable_fields( 'cbo_company_name*cbo_buyer_name*txt_style_ref*txt_revised_no*cbo_pord_dept*txt_style_desc*cbo_currercy*cbo_agent*txt_offer_qnty*cbo_region*cbo_color_range*cbo_inco_term*txt_incoterm_place*txt_machine_line*txt_prod_line_hr*cbo_costing_per*txt_quotation_date*txt_est_ship_date*txt_factory*txt_remarks*garments_nature*cbo_order_uom*set_button*image_button*save1*update1*Delete1*txt_lab_test_pre_cost*txt_inspection_pre_cost*txt_cm_pre_cost*txt_freight_pre_cost*txt_common_oh_pre_cost*txt_1st_quoted_price_pre_cost*txt_first_quoted_price_date*txt_revised_price_pre_cost*txt_revised_price_date*txt_confirm_price_pre_cost*txt_confirm_date_pre_cost*save2*update2*Delete2', 0 )";
                                        echo load_submit_buttons( $permission, "fnc_quotation_entry", 0,0 ,"reset_form('quotationmst_1*quotationdtls_2','cost_container','','cbo_currercy,2*cbo_costing_per,1*cbo_approved_status,2',$dd)",1,1) ; ?>  
                                        
                                        </td>
                                        <td align="left" valign="top" class="button_container" colspan="5">
                                        <input type="button" id="copy_btn" class="formbutton" value="Copy" onClick="copy_quatation(5)" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </fieldset>
                </td>
                <td width="15%" valign="top" >
                    <fieldset style="margin-left:5px">
                        <legend>Summary</legend>
                        <table width="100%" cellspacing="2" cellpadding="0" class="rpt_table" rules="all">
                            <thead>
                                <th width="60" align="center">Particulars</th>
                                <th width="60" align="center">Asking</th>
                                <th width="60" align="center">Confirmed </th>
                                <th width="60" align="center"> Deviation </th>
                            </thead>
                            <tr>
                                <td width="60" align="">Price With Commn/Pcs </td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_expected_1" id="txt_expected_1" style="width:60px;"  readonly/></td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_confirmed_1" id="txt_confirmed_1" style="width:60px;"  readonly/> </td>
                                <td width="60" align="center"> <input class="text_boxes_numeric" type="text" name="txt_deviation_1" id="txt_deviation_1" style="width:60px;"  readonly/> </td>
                            </tr>
                            <tr>
                                <td width="60" align="">Prod. Cost/Pcs</td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_expected_2" id="txt_expected_2" style="width:60px;"  readonly/></td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_confirmed_2" id="txt_confirmed_2" style="width:60px;"  readonly/> </td>
                                <td width="60" align="center"> <input class="text_boxes_numeric" type="text" name="txt_deviation_2" id="txt_deviation_2" style="width:60px;"  readonly/> </td>
                            </tr>
                            <tr>
                                <td width="60" align="">Commn/Pcs</td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_expected_3" id="txt_expected_3" style="width:60px;"  readonly/></td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_confirmed_3" id="txt_confirmed_3" style="width:60px;"  readonly/> </td>
                                <td width="60" align="center"> <input class="text_boxes_numeric" type="text" name="txt_deviation_3" id="txt_deviation_3" style="width:60px;"  readonly/> </td>
                            </tr>
                            <tr>
                                <td width="60" align="">Margin/Pcs</td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_expected_4" id="txt_expected_4" style="width:60px;"  readonly/></td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_confirmed_4" id="txt_confirmed_4" style="width:60px;"  readonly/> </td>
                                <td width="60" align="center"> <input class="text_boxes_numeric" type="text" name="txt_deviation_4" id="txt_deviation_4" style="width:60px;"  readonly/> </td>
                            </tr>
                            <tr>
                                <td width="60" align="">Margin/Dzn</td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_expected_5" id="txt_expected_5" style="width:60px;"  readonly/></td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_confirmed_5" id="txt_confirmed_5" style="width:60px;"  readonly/> </td>
                                <td width="60" align="center"> <input class="text_boxes_numeric" type="text" name="txt_deviation_5" id="txt_deviation_5" style="width:60px;"  readonly/> </td>
                            </tr>
                            <tr>
                                <td width="60" align="">Margin for Offer Qnty</td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_expected_6" id="txt_expected_6" style="width:60px;"  readonly/></td>
                                <td width="60" align="center"><input class="text_boxes_numeric" type="text" name="txt_confirmed_6" id="txt_confirmed_6" style="width:60px;"  readonly/> </td>
                                <td width="60" align="center"> <input class="text_boxes_numeric" type="text" name="txt_deviation_6" id="txt_deviation_6" style="width:60px;"  readonly/> </td>
                            </tr>
                        </table>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        
                    </fieldset>
                </td>
            </tr>
        </table>
        </div>
 <!--********************************Master Form End**************************************-->           
            
<div style="height:20px;"></div>

    <fieldset>
        <form id="quotationdtls_2" autocomplete="off"> 
            <table width="1650px" cellspacing="2" cellpadding="0" class="rpt_table" rules="all">
                <tr>
                    <thead>
                    <th width="80" align="center">Cost Components</th>
                    <th width="40">Fabric Cost</th>
                    <th width="40">
                    Trims Cost&nbsp;<input type="checkbox" id="is_tmplete" name="is_tmplete" onClick="show_sub_form(document.getElementById('update_id').value,'show_trim_cost_listview',document.getElementById('cbo_buyer_name').value+'*'+0);"/>
                    </th>
                    <th width="40">
                    Embel. Cost
                    </th>
                    <th width="40">
                    Gmts. Wash
                    </th>
                    <th width="40">
                    Comml. Cost
                    </th>
                    <th width="40">
                    Lab Test
                    </th>
                    <th width="40">
                    Inspe ction
                    </th>
                    <th width="40">
                    CM Cost
                    </th>
                    <th width="40">
                    Freight
                    </th>
                    <th width="40">
                    Currier Cost
                    </th>
                    <th width="40">
                    Certif. Cost
                    </th>
                    <th width="40">
                    Common OH
                    </th>
                    <th width="40">
                    Total Cost
                    </th>
                    <th id="final_cost_td_pcs_set" width="40">
                    Final Cost /Pcs
                    </th>
                    <th id="asking_profit_td_pcs" width="40">
                    Asking Profit /Pcs 
                    </th>
                    <th id="asking_quoted_price_psc_set" width="40">
                    Asking Quoted Price /Pcs
                    </th>
                    <th id="first_quoted_price_psc_set" width="100">
                    1st Quoted Price /Pcs
                    </th>
                    <th id="revised_quoted_price_psc_set" width="40">
                    Revised Price /Pcs
                    </th>
                    <th id="confirm_price_td_set_pcs" width="40">
                    Price Before Comn/Pcs
                    </th>
                    <th id="confirm_price_td_dzn" width="40">
                    Price Before Comn /Dzn
                    </th>
                    <th id="prod_cost_td_dzn" width="40">
                    Prd. Cost /Dzn
                    </th>
                    <th id="margin_dzn" width="40">
                    Margin /Dzn
                    </th>
                    <th id="commission_dzn" width="40">
                    Commi./ Dzn
                    </th>
                    <th id="price_with_comm_dzn_td" width="40">
                    Price with Commn /Dzn
                    </th>
                    <th id="price_with_comm_pcs_td" width="40">
                    Price with Commn /Pcs
                    </th>
                    <th width="40">
                    Target price
                    </th>
                    <th width="80">
                    Confirm Date
                    </th>
                    </thead>
                </tr>
                <tr>
                    <td width="40" align="center"><strong>Mkt. Costing</strong></td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_fabric_pre_cost" id="txt_fabric_pre_cost" style="width:40px;" onFocus=" show_sub_form(document.getElementById('update_id').value,'show_fabric_cost_listview','');" onChange="calculate_main_total()" readonly/> 
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_trim_pre_cost" id="txt_trim_pre_cost" style="width:40px;" onFocus=" show_sub_form(document.getElementById('update_id').value,'show_trim_cost_listview',document.getElementById('cbo_buyer_name').value+'*'+1);" onChange="calculate_main_total()" readonly/>
                    </td>
                    <td align="center" width="40">
                    <input style="width:40px;" class="text_boxes_numeric" type="text" name="txt_embel_pre_cost" id="txt_embel_pre_cost" onFocus=" show_sub_form(document.getElementById('update_id').value,'show_embellishment_cost_listview','');" onChange="calculate_main_total()"  readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input style="width:40px;" class="text_boxes_numeric" type="text" name="txt_wash_pre_cost" id="txt_wash_pre_cost" onFocus=" show_sub_form(document.getElementById('update_id').value,'show_wash_cost_listview','');" onChange="calculate_main_total()"  readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_comml_pre_cost" id="txt_comml_pre_cost" style="width:40px;" onFocus=" show_sub_form(document.getElementById('update_id').value,'show_comarcial_cost_listview','');" onChange="calculate_main_total()" readonly/>
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_lab_test_pre_cost" id="txt_lab_test_pre_cost" style="width:40px;" onChange="calculate_main_total()"/>                      </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_inspection_pre_cost" id="txt_inspection_pre_cost" style="width:40px;" onChange="calculate_main_total()"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_cm_pre_cost" id="txt_cm_pre_cost" style="width:40px;" onChange="calculate_main_total()"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_freight_pre_cost" id="txt_freight_pre_cost" style="width:40px;" onChange="calculate_main_total()"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_currier_pre_cost" id="txt_currier_pre_cost" style="width:40px;" onChange="calculate_main_total()"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_certificate_pre_cost" id="txt_certificate_pre_cost" style="width:40px;" onChange="calculate_main_total()"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_common_oh_pre_cost" id="txt_common_oh_pre_cost" style="width:40px;" onChange="calculate_main_total()"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_total_pre_cost" id="txt_total_pre_cost" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_final_cost_pcs_po_price" id="txt_final_cost_pcs_po_price" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_final_cost_dzn_pre_cost" id="txt_final_cost_dzn_pre_cost" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_asking_quoted_price" id="txt_asking_quoted_price" style="width:40px;" readonly/>
                    </td>
                    <td align="center" width="100">
                    <input class="text_boxes_numeric" type="text" name="txt_1st_quoted_price_pre_cost" id="txt_1st_quoted_price_pre_cost" onChange="calculate_first_quoted_price_pcs('value')" style="width:40px;" />  <input class="text_boxes_numeric" type="text" name="txt_1st_quoted_po_price" id="txt_1st_quoted_po_price" onChange="calculate_first_quoted_price_pcs('percent')" style="width:24px;"/>
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_revised_price_pre_cost" id="txt_revised_price_pre_cost" onChange="calculate_confirm_price_dzn()" style="width:40px;"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_confirm_price_pre_cost" id="txt_confirm_price_pre_cost" style="width:40px;" onChange="calculate_confirm_price_dzn()"/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_confirm_price_pre_cost_dzn" id="txt_confirm_price_pre_cost_dzn" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_cost_dzn" id="txt_cost_dzn" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_margin_dzn_pre_cost" id="txt_margin_dzn_pre_cost" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_commission_pre_cost" id="txt_commission_pre_cost" style="width:40px;" onFocus=" show_sub_form(document.getElementById('update_id').value,'show_commission_cost_listview','');" readonly/>
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_with_commission_pre_cost_dzn" id="txt_with_commission_pre_cost_dzn" style="width:40px;"  readonly/>
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_with_commission_pre_cost_pcs" id="txt_with_commission_pre_cost_pcs" style="width:40px;"  readonly/>
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_terget_qty" id="txt_terget_qty" style="width:40px;"/>  
                    </td>
                    <td align="center" width="80">
                    <input class="datepicker" type="text" name="txt_confirm_date_pre_cost" id="txt_confirm_date_pre_cost" style="width:80px;"/>  
                    </td>
                </tr>
                <tr>
                    <td width="40" align="center"> <strong>% To Q.price </strong></td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_fabric_po_price" id="txt_fabric_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_trim_po_price" id="txt_trim_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/> 
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_embel_po_price" id="txt_embel_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_wash_po_price" id="txt_wash_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40" >
                    <input class="text_boxes_numeric" type="text"  name="txt_comml_po_price" id="txt_comml_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_lab_test_po_price" id="txt_lab_test_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td> 
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_inspection_po_price" id="txt_inspection_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/> 
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_cm_po_price" id="txt_cm_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_freight_po_price" id="txt_freight_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td> 
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_currier_po_price" id="txt_currier_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_certificate_po_price" id="txt_certificate_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_common_oh_po_price" id="txt_common_oh_po_price" style="width:40px;" onChange="calculate_main_total()" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_total_po_price" id="txt_total_po_price" style="width:40px;" readonly/>  
                    </td> 
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_final_cost_set_pcs_rate" id="txt_final_cost_set_pcs_rate" style="width:40px;" readonly/>  
                    </td>  
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_final_cost_dzn_po_price" id="txt_final_cost_dzn_po_price" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_asking_quoted_po_price" id="txt_asking_quoted_po_price" style="width:40px;" readonly/>  
                    </td> 
                    <td align="center" width="100">
                    <input class="datepicker"   type="text" name="txt_first_quoted_price_date" id="txt_first_quoted_price_date" placeholder="Date" style="width:80px;font-size:10px" />
                    </td> 
                    <td align="center" width="40">
                    <input class="datepicker" type="text" name="txt_revised_price_date" id="txt_revised_price_date" placeholder="Date" style="width:40px;font-size:7px"/>
                    
                    </td> 
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_confirm_price_set_pcs_rate" id="txt_confirm_price_set_pcs_rate" style="width:40px;" readonly />  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_confirm_price_po_price_dzn" id="txt_confirm_price_po_price_dzn" style="width:40px;" readonly />  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_cost_dzn_po_price" id="txt_cost_dzn_po_price" style="width:40px;" readonly />  
                    </td>  
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text" name="txt_margin_dzn_po_price" id="txt_margin_dzn_po_price" style="width:40px;" readonly/>  
                    </td>  
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_commission_po_price" id="txt_commission_po_price" style="width:40px;" readonly/>  
                    </td> 
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_with_commission_po_price_dzn" id="txt_with_commission_po_price_dzn" style="width:40px;" readonly/>  
                    </td>
                    <td align="center" width="40">
                    <input class="text_boxes_numeric" type="text"  name="txt_with_commission_po_price_pcs" id="txt_with_commission_po_price_pcs" style="width:40px;" readonly/>  
                    </td>
                    <td width="40">
                    <input type="hidden" id="update_id_dtls" name="update_id_dtls" style="width:40px"/>
                    </td>
                    <td width="80">&nbsp;
                    
                    </td>
                    </tr>
                    
                    <tr>
                    <td align="right" colspan="13" valign="top">
                    <? echo load_submit_buttons( $permission, "fnc_quotation_entry_dtls", 0,0 ,"reset_form('quotationdtls_2','cost_container','')",2) ; ?>  
                    </td>
                    <td align="left" colspan="15" valign="top">
                    <input type="button" id="report_btn" class="formbutton" value="Quotation Rpt" onClick="generate_report('preCostRpt')" />
                    <input type="button" id="report_btn" class="formbutton" value="Quotation Rpt2" onClick="generate_report('preCostRpt2')" />
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
                    
                    
    <div style="width:100%;">  
        <table  width="100%" border="0">
            <tr valign="top">
                <td width="100%" valign="top">
                <div style="width:100%" id="cost_container"></div>
                <div style="width:100%" id="cost_container_commission"></div>
                </td> 
            </tr>
        </table>
    </div>
               
         
<div style="display:none" id="data_panel"></div>
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
