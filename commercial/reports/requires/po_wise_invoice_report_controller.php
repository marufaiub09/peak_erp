﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if($db_type==2 || $db_type==1 )
{
	$select_date=" to_char(a.insert_date,'YYYY')";
	$group_concat="wm_concat";
}
else if ($db_type==0)
{
	$select_date=" year(a.insert_date)";
	$group_concat="group_concat";
}

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
$buyer_name_arr=return_library_array( "select id, buyer_name from  lib_buyer",'id','buyer_name');
$lc_arr=return_library_array( "select id, export_lc_no from com_export_lc",'id','export_lc_no');
$sc_arr=return_library_array( "select id, contract_no from com_sales_contract",'id','contract_no');
$deling_marchent_arr=return_library_array( "select id, team_member_name from lib_mkt_team_member_info",'id','team_member_name');
if($db_type==2 || $db_type==1 )
{
$article_arr=return_library_array("select LISTAGG(cast(article_number as varchar2(4000)), ',') WITHIN GROUP (ORDER BY article_number) as article_number, po_break_down_id from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 and article_number is not null group by po_break_down_id","po_break_down_id","article_number");
}
else if ($db_type==0)
{
$article_arr=return_library_array("select group_concat(distinct(article_number)) as article_number, po_break_down_id from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 and article_number!='' group by po_break_down_id","po_break_down_id","article_number");
}

$submission_status_id_arr=return_library_array( "select invoice_id, doc_submission_mst_id from com_export_doc_submission_invo",'invoice_id','doc_submission_mst_id');
$realize_status_id_arr=return_library_array( "select invoice_bill_id, id from com_export_proceed_realization",'invoice_bill_id','id');
//load drop down Buyer
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}




//style search------------------------------//
if($action=="style_refarence_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	//echo $style_id;die;

	?>
    <script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		var selected_no = new Array;
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
			//alert(strCon);
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );
					selected_no.push( str );				
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 );
					selected_no.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = ''; var num='';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ',';
					num += selected_no[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				num 	= num.substr( 0, num.length - 1 );
				//alert(num);
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
				$('#txt_selected_no').val( num );
		}
		
		
		
		/*function js_set_value( str ) { //alert(str);
		toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
		
		if( jQuery.inArray( $('#txt_serial_id' + str).val(), selected_id ) == -1 ) {
			selected_id.push( $('#txt_serial_id' + str).val() );
			selected_no.push( $('#txt_serial_no' + str).val() );
 		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == $('#txt_serial_id' + str).val() ) break;
			}
			selected_id.splice( i, 1 );
			selected_no.splice( i, 1 );
		}
		var id = '';	var no = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			no += selected_no[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		no = no.substr( 0, no.length - 1 );
  		$('#txt_string_id').val( id );
		$('#txt_string_no').val( no );
	}
	 
	function fn_onClosed()
	{
		var txt_string = $('#txt_string').val();
		if(txt_string==""){ alert("Please Select The Serial"); return;}
		parent.emailwindow.hide();
	}*/
		
    </script>
    <?php
	$buyer=str_replace("'","",$buyer);
	$company=str_replace("'","",$company);
	if($buyer!=0) $buyer_cond="and a.buyer_name=$buyer"; else $buyer_cond="";
	$sql = "select a.id,a.style_ref_no,a.job_no,a.job_no_prefix_num,$select_date as year from wo_po_details_master a where a.company_name=$company $buyer_cond  and is_deleted=0 order by job_no_prefix_num"; 
	//echo $sql; die;
	echo create_list_view("list_view", "Style Ref No,Job No,Year","160,90,100","400","310",0, $sql , "js_set_value", "id,style_ref_no", "", 1, "0", $arr, "style_ref_no,job_no_prefix_num,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	echo "<input type='hidden' id='txt_selected_no' />";
	
	?>
    <script language="javascript" type="text/javascript">
	var style_no='<?php echo $txt_style_ref_no;?>';
	var style_id='<?php echo $txt_style_ref_id;?>';
	var style_des='<?php echo $txt_style_ref;?>';
	//alert(style_id);
	if(style_no!="")
	{
		style_no_arr=style_no.split(",");
		style_id_arr=style_id.split(",");
		style_des_arr=style_des.split(",");
		var str_ref="";
		for(var k=0;k<style_no_arr.length; k++)
		{
			str_ref=style_no_arr[k]+'_'+style_id_arr[k]+'_'+style_des_arr[k];
			js_set_value(str_ref);
		}
	}
	</script>
    
    <?php
	
	exit();
}


//order search------------------------------//
if($action=="order_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
    <script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		var selected_no = new Array;
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
			//alert(strCon);
				var splitSTR = strCon.split("_");
				var str_or = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str_or ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );
					selected_no.push( str_or );				
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 );
					selected_no.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = ''; var num='';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ',';
					num += selected_no[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				num 	= num.substr( 0, num.length - 1 );
				//alert(num);
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
				$('#txt_selected_no').val( num );
		}
    </script>
    <?php
	$buyer=str_replace("'","",$buyer);
	$company=str_replace("'","",$company);
	$style_all=str_replace("'","",$style_all);
	
	if($buyer!=0) $buyer_cond="and b.buyer_name=$buyer"; else $buyer_cond="";
	if($style_all!="") $style_cond="and b.id in($style_all)"; else $style_cond="";
	$sql = "select a.id,a.po_number,a.job_no_mst,b.style_ref_no,b.job_no_prefix_num,$select_date as year from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and b.company_name=$company $buyer_cond  $style_cond and a.status_active=1"; 
	//echo $sql; die;
	echo create_list_view("list_view", "Order NO,Job No,Year,Style Ref No","150,80,70,150","500","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "po_number,job_no_prefix_num,year,style_ref_no", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	echo "<input type='hidden' id='txt_selected_no' />";
	
	?>
    <script language="javascript" type="text/javascript">
	var style_no='<?php echo $txt_order_id_no;?>';
	var style_id='<?php echo $txt_order_id;?>';
	var style_des='<?php echo $txt_order;?>';
	//alert(style_id);
	if(style_no!="")
	{
		style_no_arr=style_no.split(",");
		style_id_arr=style_id.split(",");
		style_des_arr=style_des.split(",");
		var str_ref="";
		for(var k=0;k<style_no_arr.length; k++)
		{
			str_ref=style_no_arr[k]+'_'+style_id_arr[k]+'_'+style_des_arr[k];
			js_set_value(str_ref);
		}
	}
	</script>
    
    <?php
	
	exit();
}



//report generated here--------------------//
if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	$txt_style_ref=str_replace("'","",$txt_style_ref);
	$txt_style_ref_id=str_replace("'","",$txt_style_ref_id);
	$txt_order=str_replace("'","",$txt_order);
	$txt_order_id=str_replace("'","",$txt_order_id);
	
	if($cbo_buyer_name!=0) $cbo_buyer_name="and b.buyer_name=$cbo_buyer_name"; else $cbo_buyer_name="";
	if($txt_style_ref!="")
	{
		if($txt_style_ref_id!="")
		{
			$txt_style_ref_id="and b.id in($txt_style_ref_id)";
		}
		else
		{
			$txt_style_ref_id="and b.style_ref_no='$txt_style_ref'"; 

		}
	}
	else
	{
		 $txt_style_ref_id="";
	}
	if($txt_order!="") 
	{
		if($txt_order_id!="")
		{
			$txt_order_id="and a.id in($txt_order_id)";
		}
		else
		{
			$txt_order_id="and a.po_number='$txt_order'";
		}
	}
	else
	{
		$txt_order_id="";
	}
	
	
	
	  
	
	/*$sql="select a.id as order_id,a.job_no_mst,a.po_number,b.buyer_name,b.style_ref_no,b.dealing_marchant,c.id as invoice_id,c.is_lc,c.lc_sc_id,c.invoice_no,c.invoice_quantity,c.invoice_value,c.exp_form_no,a.status_active
		  from
		  		wo_po_break_down a, wo_po_details_master b, com_export_invoice_shipping_mst c,com_export_invoice_shipping_dtls d
		  where
		  		a.job_no_mst=b.job_no and a.id=d.po_breakdown_id and d.mst_id=c.id and b.company_name=$cbo_company_name $cbo_buyer_name $txt_style_ref_id $txt_order_id  and a.status_active in(1,3) and b.is_deleted=0 group by c.id";*/
				
	/*$sql="select  a.id as order_id, a.job_no_mst as job_no_mst,$group_concat( a.po_number) as po_number, b.buyer_name,$group_concat(distinct b.style_ref_no) as style_ref_no,$group_concat(distinct b.dealing_marchant) as dealing_marchant,c.mst_id as invoice_id,a.status_active,c.id as inv_dtls_id, sum(c.current_invoice_qnty) as invoice_quantity, sum(c.current_invoice_value) as invoice_value,d.is_lc,$group_concat(distinct d.lc_sc_id) as lc_sc_id,$group_concat(distinct d.invoice_no) as invoice_no,$group_concat(distinct d.exp_form_no) as exp_form_no
		  from
		  		wo_po_break_down a, wo_po_details_master b, com_export_invoice_ship_dtls c,com_export_invoice_ship_mst d
		  where
		  		a.job_no_mst=b.job_no and a.id=c.po_breakdown_id and c.mst_id=d.id and b.company_name=$cbo_company_name $cbo_buyer_name $txt_style_ref_id $txt_order_id  and a.status_active in(1,3) and c.current_invoice_value!=0 and b.is_deleted=0 
		group by 
				c.id,c.mst_id,b.buyer_name,a.id, a.job_no_mst ,a.status_active,d.is_lc 
		order by a.id";*/
				
	//echo $sql;die;			
	
	$sql_order=sql_select("select  a.id as order_id, a.job_no_mst as job_no_mst,a.po_number, b.buyer_name, b.style_ref_no, b.dealing_marchant,a.status_active
		  from
		  		wo_po_break_down a, wo_po_details_master b
		  where
		  		a.job_no_mst=b.job_no and b.company_name=$cbo_company_name $cbo_buyer_name $txt_style_ref_id $txt_order_id  and a.status_active in(1,3) and b.is_deleted=0 
		order by a.id");
	$order_data_arr=array();$po_id=0;
	foreach($sql_order as $row)
	{
		if($po_id==0) $po_id=$row[csf("order_id")]; else $po_id=$po_id.",".$row[csf("order_id")];
		$order_data_arr[$row[csf("order_id")]]["order_id"]=$row[csf("order_id")];
		$order_data_arr[$row[csf("order_id")]]["job_no_mst"]=$row[csf("job_no_mst")];
		$order_data_arr[$row[csf("order_id")]]["po_number"]=$row[csf("po_number")];
		$order_data_arr[$row[csf("order_id")]]["buyer_name"]=$row[csf("buyer_name")];
		$order_data_arr[$row[csf("order_id")]]["style_ref_no"]=$row[csf("style_ref_no")];
		$order_data_arr[$row[csf("order_id")]]["dealing_marchant"]=$row[csf("dealing_marchant")];
		$order_data_arr[$row[csf("order_id")]]["status_active"]=$row[csf("status_active")];
	}
	//var_dump($order_data_arr);die;
	//echo $sql;die;
	
	$sql_invoice=sql_select("select  c.po_breakdown_id,c.mst_id as invoice_id,c.id as inv_dtls_id,c.current_invoice_qnty,c.current_invoice_value,d.is_lc,d.lc_sc_id,d.invoice_no,d.exp_form_no
		  from
		  		com_export_invoice_ship_dtls c,com_export_invoice_ship_mst d
		  where
		  		c.mst_id=d.id and c.current_invoice_value >0 and c.po_breakdown_id in($po_id)");
	
	$invoice_data_arr=array();
	foreach($sql_invoice as $row)
	{
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["invoice_id"]=$row[csf("invoice_id")];
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["inv_dtls_id"]=$row[csf("inv_dtls_id")];
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["current_invoice_qnty"] +=$row[csf("current_invoice_qnty")];
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["current_invoice_value"] +=$row[csf("current_invoice_value")];
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["is_lc"] =$row[csf("is_lc")];
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["lc_sc_id"] =$row[csf("lc_sc_id")];
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["invoice_no"] =$row[csf("invoice_no")];
		$invoice_data_arr[$row[csf("po_breakdown_id")]][$row[csf("invoice_id")]]["exp_form_no"] =$row[csf("exp_form_no")];
	}
	//$sql_result=sql_select($sql);
	//var_dump($invoice_data_arr);die;
	
		$i=1;
		ob_start();	
		?>
    	
        <div style="width:1000"> 
			<table width="950" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
                	<tr class="form_caption" style="border:none;">
                        <td colspan="8" align="center" style="border:none;font-size:16px; font-weight:bold" >Order Wise Export Invoice Report </td> 
                    </tr>
                    <tr style="border:none;">
                            <td colspan="8" align="center" style="border:none; font-size:14px;">
                                Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
                            </td>
                    </tr>
               </table>
               <br />
			<table width="950" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
            	<thead>
                    <tr>
                        <th width="80" >SL</th>


                        <th width="120" >Invoice No</th>
                        <th width="120" >Lc No</th>
                        <th width="120" >Invoice Qty</th>
                        <th width="120">Invoice Amount</th>
                        <th width="120">Submit Status</th>
                        <th width="120">Realize Status</th>
                        <th >Dealing Merchandiser</th>
                    </tr>
                </thead>
           </table> 
          <!--<div style="width:830px; overflow-y:scroll; max-height:250px;" id="scroll_body"> -->
          <div style="width:1000px; overflow-y: scroll; max-height:250px;" id="scroll_body">
			<!--<table style="width:800px" border="0" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_body" >-->
            <table width="950" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">

                <?php
				$order_arr=array();$i=1;$sub_total_inv_qty=0;$sub_total_inv_value=0;$grand_total_qty=0;$grand_total_val=0;
				foreach($order_data_arr as $order_id=>$val)
				{
					?>
					<tr bgcolor="#ECDFE9">
						<td width="80"  style="border-color:#ECDFE9" align="right"><b>Order No:</b>&nbsp;</td>
						<td width="120"  style="border-color:#ECDFE9"><?php echo $val[("po_number")]; ?> </td>
						<td width="120"  style="border-color:#ECDFE9" align="right"><b>Buyer:</b>&nbsp;</td>
						<td width="120"  style="border-color:#ECDFE9"><?php echo $buyer_name_arr[$val[("buyer_name")]]; ?></td>
						<td width="120"  style="border-color:#ECDFE9" align="right"><b>Style Ref No</b>&nbsp;</td>
						<td width="120"  style="border-color:#ECDFE9" >:<?php echo $val[("style_ref_no")]; ?></td>
						<td width="120" style="border-color:#ECDFE9" align="right"><b>Article No:</b>&nbsp;</td>
						<td>
						<?php  
							//$article=explode(",",$article_arr[$order_id]);
							$article=implode(",",array_unique(explode(",",$article_arr[$order_id])));
							//return_field_value("a.article_number","wo_po_color_size_breakdown a, wo_po_details_master b","a.job_no_mst=b.job_no and a.job_no_mst='".$val[("job_no_mst")]."'","article_number");
							 echo $article."&nbsp;"; if($val[("status_active")]==3) echo '<span style="color:#F00; font-size:14px;">Cancelled</span>';
						?>
                        </td>
					</tr>
					<?php
					$j=1;
					
					foreach($invoice_data_arr[$order_id] as $invoice_id=>$row)
					{
						if ($j%2==0)
						$bgcolor="#E9F3FF";
						else
						$bgcolor="#FFFFFF";
						?>
						<tr bgcolor="<?php echo $bgcolor;?>"  onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
							<td  align="center"><?php echo $j; ?></td>
							<td ><?php echo $row[("invoice_no")]; ?> </td>
							<td >
							<?php
							if($row[("is_lc")]==1)
							{
							 echo $lc_arr[$row[("lc_sc_id")]];
							}
							else
							{
								echo $sc_arr[$row[("lc_sc_id")]];
							}
							?></td>
							<td  align="right"><?php echo $row[("current_invoice_qnty")]; ?> &nbsp;</td>
							<td  align="right"><?php echo $row[("current_invoice_value")];?>&nbsp;</td>
							<td align="center" >
							<?php
								//$submission_status_id=return_field_value("a.doc_submission_mst_id","com_export_doc_submission_invo a","a.invoice_id='".$row[("invoice_id")]."'","doc_submission_mst_id");
								$submission_status_id=$submission_status_id_arr[$row[("invoice_id")]];
								
								if($submission_status_id!="") echo "Submitted"; else echo ""; 	
							?>
                            </td>
                            <td align="center">
							<?php
								//$realize_status_id=return_field_value("a.id"," com_export_proceed_realization a","a.is_invoice_bill=1 and a.invoice_bill_id='$submission_status_id'","id");
								if($realize_status_id_arr[$submission_status_id]!="") {echo "Realized";} else {echo "";} 	
							?>
                            </td>
                            <td align="center"><?php echo $deling_marchent_arr[$val[("dealing_marchant")]]; ?></td>
						</tr>
						<?php
						$sub_total_inv_qty +=$row[("current_invoice_qnty")];
						$sub_total_inv_value +=$row[("current_invoice_value")];
						
						$grand_total_qty +=$row[("current_invoice_qnty")];
						$grand_total_val +=$row[("current_invoice_value")];
						$j++;$i++;
					}
					?>
                    
                     <tr bgcolor="#ccc">
                            <td  colspan="3" align="right" ><b>Sub-total</b></td>
                            <td align="right"><b><?php echo $sub_total_inv_qty; ?> </b></td>
                            <td  align="right"><b><?php echo number_format($sub_total_inv_value,2); ?></b></td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                        </tr>
                    <?php
					$sub_total_inv_qty="";
					$sub_total_inv_value="";
				}
                ?>    
                <tr bgcolor="#ccc">
                    <td  colspan="3" align="right" ><b>Grand Total</b></td>
                    <td  align="right"><b><?php echo $grand_total_qty; ?></b> </td>
                    <td  align="right"><b><?php echo number_format($grand_total_val,2); ?></b></td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                </tr>
            <!--<script language="javascript"> setFilterGrid('table_body',-1)</script>  -->
            </table>

		 </div>
       </div>    
		<?php	 
		
		$html = ob_get_contents();
		ob_clean();
		$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
		foreach (glob("*.xls") as $filename) {
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
		}
		//---------end------------//
		$name=time();
		$filename=$user_id."_".$name.".xls";
		$create_new_doc = fopen($filename, 'w');	
		$is_created = fwrite($create_new_doc, $html);
		echo "$html**$filename"; 
		exit();
		
		/*foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();*/
	
	 

disconnect($con);
}
?>

