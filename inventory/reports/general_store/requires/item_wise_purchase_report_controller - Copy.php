<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_store")
{
	$data=explode('_',$data);
	//print_r ($data);  
	echo create_drop_down( "cbo_store_name", 120, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET($data[0],company_id) and FIND_IN_SET($data[1],item_category_id) order by store_name","id,store_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier_name", 120, "select id,supplier_name from lib_supplier where FIND_IN_SET($data,tag_company) and FIND_IN_SET(2,party_type) order by supplier_name","id,supplier_name", 1, "--Select Supplier--", 0, "",0 );  	 
	exit();
}

if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	//echo load_html_head_contents("Popup Info", "../../../../", 1, 1,'',1,'');
	extract($_REQUEST);
?>	
    <script>
/*		  function js_set_value(id)
		  { 
			  document.getElementById('item_account_id').value=id;
			  //parent.emailwindow.hide();
		  }
*/		  
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	 function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			alert (tbl_row_count);
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ ) {
				 eval($('#tr_'+i).attr("onclick"));  
			}
		}
		
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		alert (id);
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		str=str[1];
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
		}
		var id = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		
		$('#item_account_id').val( id );
		//$('#item_account_val').val( val );
	} 
		  
		  
	</script>
     <input type="text" id="item_account_id" />
<!--      <input type="text" id="item_account_val" />
-->    <?php
		$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
		$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
		
		$sql="SELECT id,item_account,item_category_id,item_group_id,item_description,supplier_id from  product_details_master where status_active=1 and is_deleted=0"; 
		$arr=array(1=>$item_category,2=>$itemgroupArr,4=>$supplierArr);
		echo  create_list_view("list_view", "Item Account,Item Category,Item Group,Item Description,Supplier,Product ID", "70,110,150,150,100,70","780","400",0, $sql , "js_set_value", "id,item_description", "", 0, "0,item_category_id,item_group_id,0,supplier_id,0", $arr , "item_account,item_category_id,item_group_id,item_description,supplier_id,id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
		exit();
}

/*if ($action=="item_account_dtls_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	//echo load_html_head_contents("Popup Info", "../../../../", 1, 1,'',1,'');
	extract($_REQUEST);
}
*/

if ($action=="item_group_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>	
    <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('item_group_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
        <input type="text" id="item_group_id" />
    <?php
	$sql="SELECT id,item_group_id from  product_details_master where status_active=1 and is_deleted=0"; 
	$item_group_arr=return_library_array( "select id,item_name from lib_item_group", "id","item_name"  );
	$arr=array(0=>$item_group_arr);
	
	echo  create_list_view("list_view", "Item Name", "350","500","330",0, $sql , "js_set_value", "id,item_group_id", "", 1, "item_group_id", $arr , "item_group_id", "item_wise_purchase_report_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}

if($action=="load_php_data_to_form_item_group")
{ 
//$sql="SELECT id,item_group_id from  product_details_master where status_active=1 and is_deleted=0"; 
$item_group_arr=return_library_array( "select id,item_name from lib_item_group", "id","item_name"  );
$nameArray=sql_select( "SELECT id,item_group_id from  product_details_master where id=$data" );
	foreach ($nameArray as $row)
	{
		echo "document.getElementById('txt_item_group').value 			= '".$item_group_arr[$row[csf("item_group_id")]]."';\n";  
	}
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	if ($cbo_company_name==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_name'";
	if ($cbo_item_category_id==0) $item_category_id=""; else $item_category_id=" and a.item_category='$cbo_item_category_id'";
	if ($item_account_id==0) $item_code=""; else $item_code=" and c.item_code='$item_account_id'";
	if ($item_group_id==0) $item_group_id=""; else $item_group_id=" and c.item_group_id='$item_group_id'";
	if ($cbo_supplier_name==0) $supplier_id=""; else $supplier_id=" and a.supplier_id='$cbo_supplier_name'";
	if ($cbo_store_name==0){ $store_id="";}else{$store_id=" and a.store_id='$cbo_store_name'";}
	
 	if( $from_date==0 && $to_date==0 ) $transaction_date=""; else $transaction_date= " and b.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'";
 	
	
 	//library array-------------------
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name"); 
	$storeArr = return_library_array("select id,store_name from lib_store_location where status_active=1 and is_deleted=0","id","store_name"); 
	$itemnameArr = return_library_array("select id,item_name from lib_item_creation where status_active=1 and is_deleted=0","id","item_name");
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	$pi_numArr = return_library_array("select id,pi_number from  com_pi_master_details where status_active=1 and is_deleted=0","id","pi_number");
	$wo_numArr = return_library_array("select id,wo_number from  wo_non_order_info_mst where status_active=1 and is_deleted=0","id","wo_number");
	$req_numArr = return_library_array("select id,requ_no from  inv_purchase_requisition_mst where status_active=1 and is_deleted=0","id","requ_no");
	//$req_numArr = return_library_array("select id,requ_no from  inv_purchase_requisition_mst where status_active=1 and is_deleted=0","id","requ_no");
 	 
	 $sql = "select a.id,a.item_category,a.receive_basis,a.receive_date,a.currency_id,a.recv_number,a.supplier_id,a.store_id,a.booking_id,		b.transaction_date,b.cons_quantity,b.cons_uom,b.cons_rate,b.cons_amount,		c.id as pro_id,c.item_description,c.product_name_details,c.item_group_id from inv_receive_master a,inv_transaction b,product_details_master c where a.id=b.mst_id and c.id=b.prod_id $company_id $item_category_id $item_code $item_group_id $supplier_id $store_id $transaction_date order by b.transaction_date"; 
	echo $sql;
	
 	// receive MRR array------------------------------------------------ 
	if($cbo_store_name!=0)
	{
		//echo "select a.id,a.item_category,a.receive_basis,a.receive_date,a.currency_id,a.recv_number,a.supplier_id,a.store_id,a.booking_id,		b.transaction_date,b.cons_quantity,b.cons_uom,b.cons_rate,b.cons_amount,		c.id as pro_id,c.item_description,c.product_name_details,c.item_group_id from inv_receive_master a,inv_transaction b,product_details_master c where a.id=b.mst_id and c.id=b.prod_id $company_id $item_category_id $item_code $item_group_id $supplier_id $store_id $transaction_date order by b.transaction_date";die;
		
		//echo "select a.id,a.transaction_date,a.cons_quantity,a.cons_uom,a.cons_rate,a.cons_amount,a.pi_wo_batch_no,b.item_code,b.item_account,b.item_category_id,b.item_group_id,	 from inv_transaction a,product_details_master b,inv_receive_master c where a.prod_id=b.id and a.mst_id=c.id $company_id $store_id $supplier_id $transaction_date $item_category_id $item_code $item_group_id order by a.transaction_date";
		$sql = "select a.id,a.item_category,a.receive_basis,a.receive_date,a.currency_id,a.recv_number,a.supplier_id,a.store_id,a.booking_id,		b.transaction_date,b.cons_quantity,b.cons_uom,b.cons_rate,b.cons_amount,		c.id as pro_id,c.item_description,c.product_name_details,c.item_group_id from inv_receive_master a,inv_transaction b,product_details_master c where a.id=b.mst_id and c.id=b.prod_id $company_id $item_category_id $item_code $item_group_id $supplier_id $store_id $transaction_date order by b.transaction_date"; 
	echo $sql; //and a.transaction_type=1	 
	$result = sql_select($sql);	
	$r=1;
	ob_start();	
	?>
	<div style="width:100%;"> 
     <fieldset style="width:800px;">
        <table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
            <tr class="form_caption" style="border:none;">
                <td colspan="10" align="center" style="border:none;font-size:20px; font-weight:bold" ><strong>Item Wise Purchase/Receive Details</strong></td> 
            </tr>
            <tr style="border:none;">
                <td colspan="10" align="center" style="border:none; font-size:17px;"><strong>
                    Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?>  </strong>                              
                </td>
            </tr>
            <tr style="border:none;">
                <td colspan="10" align="center" style="border:none;font-size:12px; font-weight:bold">
                    <?php if($from_date!="" || $to_date!="")echo "From : ".change_date_format($from_date)." - To : ".change_date_format($to_date)."" ;?>
                </td>
            </tr>
            <?php
				foreach($result as $row)
				{
					$itemcode=$row[csf('item_code')];
/*					$itemaccount=$row[csf('item_account')];
*/					$itemcategory=$row[csf('item_category_id')];
					$itemgroup=$row[csf('item_group_id')];
				}
			?>
            <tr>
                <td colspan="3"><b>Item Code : <?php echo $itemnameArr[$itemcode]; ?></td>
                <td colspan="4"><b>Item Name : <?php echo $itemgroupArr[$itemgroup]; ?></td>
                <td colspan="3"><b>Item Category : <?php echo $item_category[$itemcategory]; ?></td>
<!--                <td colspan="2"><b>Group : <?php // echo $itemgroupArr[$itemgroup]; ?></td>
-->            </tr>
            <tr>
                <td colspan="10"><b>Store : <?php echo $storeArr[$cbo_store_name]; ?></td>
            </tr> 
        </table>
        <div style="width:800px;" id="scroll_body" > 
        <table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_2" >
            <thead>
                <th width="30" >SL</th>
                <th width="80" >Receive Date</th>
                <th width="100" >Supplier</th>
                <th width="80" >Quantity</th>
                <th width="70" >UoM</th>
                <th width="70" >Currency</th>
                <th width="70" >Rate</th>
                <th width="70" >Amount</th>
                <th width="70" >Basis Ref.</th>
                <th width="120" >MRIR</th>                    
            </thead>
            <tbody>
            <?php
			foreach($result as $rows)
			{
				if ($r%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
				if($rows[csf("transaction_type")]==1) 
					$stylecolor='style="color:#A61000"';
				else
					$stylecolor='style="color:#000000"'; 								
				
				$cons_amount=$rows[csf('cons_amount')];
				$cons_amount_sum += $cons_amount;
				
            ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $r; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $r; ?>">
                    <td align="center"><?php echo $r;?></td>
                    <td><?php echo change_date_format($rows[csf('transaction_date')]); ?></td>
                    <td><?php echo $supplierArr[$rows['supplier_id']]; ?></td>
                    <td align="right"><?php echo number_format($rows[csf('cons_quantity')]); ?></td>
                    <td align="center"><?php echo $unit_of_measurement[$rows[csf('cons_uom')]]; ?></td>
                    <td align="center"><?php echo $currency[$rows[csf('currency_id')]]; ?></td>
                    <td align="right"><?php echo number_format($rows[csf('cons_rate')]); ?></td>
                    <td align="right"><?php echo number_format($rows[csf('cons_amount')]); ?></td>
                    <?php 
						$receive_basis=$rows[csf('receive_basis')];
						if ($receive_basis==1)
						{
					?>
							<td><?php echo $pi_numArr[$rows[csf('pi_wo_batch_no')]]; ?></td>
                    <?php
						}
						else if($receive_basis==2)
						{
					?>
							<td><?php echo $wo_numArr[$rows[csf('pi_wo_batch_no')]]; ?></td>
                    <?php
						}
						else if($receive_basis==4)
						{
					?>
							<td><?php echo $receive_basis_arr[$rows[csf('receive_basis')]]; ?></td>
                    <?php
						}
						else if($receive_basis==6)
						{
					?>
							<td><?php echo $receive_basis_arr[$rows[csf('receive_basis')]]; ?></td>
                    <?php
						}
						else if($receive_basis==7)
						{
					?>
							<td><?php echo $req_numArr[$rows[csf('pi_wo_batch_no')]]; ?></td>
                    <?php
						}
					?>
                    <td><?php echo $rows[csf('recv_number')]; ?></td>
                </tr>
			<?php
				$r++;
			}
			?>
            </tbody>
            <tfoot>
                <td align="right" colspan="7" >Total</td>
                <td align="right"><?php echo number_format($cons_amount_sum,0,'',','); ?></td>
            </tfoot>
       </table> 
     </div>
    </fieldset>
   </div>
     <?php
} 
else
{
	$sql = "select a.id,a.store_name,b.item_code,b.item_category_id,b.item_group_id,c.store_id,c.item_category from lib_store_location a,product_details_master b, inv_receive_master c where a.id=c.store_id and c.entry_form=20 and a.status_active=1 and a.is_deleted=0 and FIND_IN_SET($cbo_company_name,c.company_id) group by c.store_id order by a.store_name  "; 
	//echo $sql;
	$result = sql_select($sql);
	
	$r=1;
	ob_start();	
	?>
	<div style="width:100%;">
	<fieldset style="width:800px;">
		<table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" > 
        <thead>
			<tr class="form_caption" style="border:none;">
				<td colspan="10" align="center" style="border:none;font-size:20px; font-weight:bold" ><strong>Item Wise Purchase/Receive Details</strong></td> 
			</tr>
			<tr style="border:none;">
				<td colspan="10" align="center" style="border:none; font-size:17px;"><strong>
					Company Name : <?php echo $companyArr[str_replace("'","",$cbo_company_name)]; ?> </strong>                             
				</td>
			</tr>
			<tr style="border:none;">
				<td colspan="10" align="center" style="border:none;font-size:12px; font-weight:bold">
					<?php if($from_date!="" || $to_date!="")echo "From ".change_date_format($from_date)." To ".change_date_format($to_date)."" ;?>
				</td>
			</tr>
			<?php
			$cons_amount_sum='';
			$cons_amount_total_sum='';
				foreach($result as $key)
				{
					$store=$key[csf("id")];
					if ($store==0) $store_nam=""; else $store_nam=" and c.store_id='$store'";
					
					$sql_store ="select a.id,a.item_category,a.receive_basis,a.receive_date,a.currency_id,a.recv_number,a.supplier_id,a.store_id,a.booking_id,		b.transaction_date,b.cons_quantity,b.cons_uom,b.cons_rate,b.cons_amount,		c.id as pro_id,c.item_description,c.product_name_details,c.item_group_id 	from inv_receive_master a,inv_transaction b,product_details_master c 	where a.id=b.mst_id and c.id=b.prod_id $company_id $item_category_id $item_code $item_group_id $supplier_id $transaction_date order by b.transaction_date";//b.prod_id in ($item_account_id) and 
					
					//$sql_store = "select a.id,a.transaction_date,a.cons_quantity,a.cons_uom,a.cons_rate,a.cons_amount,a.store_id,a.supplier_id,a.pi_wo_batch_no,b.item_code,b.item_account,b.item_category_id,b.item_group_id,	c.item_category,c.receive_basis,c.receive_purpose,c.receive_date,c.currency_id,c.recv_number from inv_transaction a,product_details_master b,inv_receive_master c where a.prod_id=b.id and a.mst_id=c.id and b.item_category_id in (1,2,3,4,12,13,14) and c.company_id=a.company_id $store_nam order by a.transaction_date "; 
	//echo $sql_stor; 		
				$result_store = sql_select($sql_store);	
			?>
            <tr>
                <td colspan="3" width="250"><b>Item Code : </b><?php echo $key[csf('item_code')]; ?></td>
                <td colspan="4" width="250"><b>Item Name : </b><?php echo $itemgroupArr[$key[csf('item_account')]]; ?></td>
                <td colspan="3" width="250"><b>Item Category : </b><?php echo $item_category[$key[csf('item_category')]]; ?></td>
            </tr>            
			<tr>
				<td colspan="10" ><b>Store : <?php echo $storeArr[$key[csf("id")]]; ?></b></td>
			</tr> 
            </thead>
		</table>
		<div style="width:800px;" id="scroll_body" > 
		<table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_2" >
			<thead>
				<th width="30" >SL</th>
				<th width="80" >Receive Date</th>
				<th width="100" >Supplier</th>
				<th width="80" >Quantity</th>
				<th width="70" >UoM</th>
				<th width="70" >Currency</th>
				<th width="70" >Rate</th>
				<th width="70" >Amount</th>
				<th width="70" >Basis Ref.</th>
				<th width="120" >MRIR</th>                    
			</thead>
			
			<?php
				foreach($result_store as $rows)
				{
					
					if ($r%2==0) $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
					if($rows[csf("transaction_type")]==1) 
						$stylecolor='style="color:#A61000"';
					else
						$stylecolor='style="color:#000000"'; 								
					
					$cons_amount=$rows[csf('cons_amount')];
					$cons_amount_sum += $cons_amount;
					
					$cons_amount_tot=$cons_amount_sum;
					$cons_amount_total_sum += $cons_amount_sum;
			?>
                     <tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $r; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $r; ?>">
                        <td align="center"><?php echo $r;?></td>
                        <td><?php echo change_date_format($rows['transaction_date']); ?></td>
                        <td><?php echo $supplierArr[$rows['supplier_id']]; ?></td>
                        <td align="right"><?php echo number_format($rows['cons_quantity']); ?></td>
                        <td align="center"><?php echo $unit_of_measurement[$rows['cons_uom']]; ?></td>
                        <td align="center"><?php echo $currency[$rows['currency_id']]; ?></td>
                        <td align="right"><?php echo number_format($rows['cons_rate']); ?></td>
                        <td align="right"><?php echo number_format($rows['cons_amount']); ?></td>
						<?php 
                            $receive_basis=$rows[csf('receive_basis')];
                            if ($receive_basis==1)
                            {
                        ?>
                                <td><?php echo $pi_numArr[$rows[csf('booking_id')]]; ?></td>
                        <?php
                            }
                            else if($receive_basis==2)
                            {
                        ?>
                                <td><?php echo $wo_numArr[$rows[csf('booking_id')]]; ?></td> 
                        <?php
                            }
                            else if($receive_basis==4)
                            {
                        ?>
                                <td><?php echo $receive_basis_arr[$rows[csf('receive_basis')]]; ?></td>
                        <?php
                            }
                            else if($receive_basis==6)
                            {
                        ?>
                                <td><?php echo $receive_basis_arr[$rows[csf('receive_basis')]]; ?></td>
                        <?php
                            }
                            else if($receive_basis==7)
                            {
                        ?>
                                <td><?php echo $req_numArr[$rows[csf('booking_id')]]; ?></td>
                        <?php
                            }
                        ?>
                        <td><?php echo $rows['recv_number']; ?></td>
                    </tr>
			<?php
					$r++;
				}
			?>
			
			<tr>
				<td align="right" colspan="7" ><strong>Total : </strong></td>
				<td align="right"><strong><?php echo number_format($cons_amount_sum,0,'',','); ?></strong></td>
			</tr>
		</table> 
	 </div>
	  <?php
		}
	  ?> 
		<table style="width:780px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" >
			<tfoot>
				<td align="right" colspan="7" width="500" ><strong>Grand Total : </strong></td>
				<td align="right" width="70"><strong><?php echo number_format($cons_amount_total_sum,0,'',','); ?></strong></td>
				<td colspan="2" width="190" >&nbsp;</td>
			</tfoot>
		</table>
	</fieldset>
   </div>
   <?php
}   
	$html = ob_get_contents();
	ob_clean();
	//$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
	foreach (glob("*.xls") as $filename) {
	//if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc, $html);
	echo "$html**$filename"; 
	exit();	
}
?>