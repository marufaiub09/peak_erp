﻿<?
/*-------------------------------------------- Comments
Purpose			: 	This form will create Gmts Shipment Schedule Report
				
Functionality	:	
JS Functions	:
Created by		:	Monzu 
Creation date 	: 	11/01/2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Info","../../../", 1, 1, $unicode);
?>	

<script>
var permission='<? echo $permission; ?>';
	function show_inner_filter(e)
    {
    	if (e!=13) {var unicode=e.keyCode? e.keyCode : e.charCode } else {unicode=13;}
 		if (unicode==13 )
		{
         	generate_report('report_container2',2);
		}
    }	
		
	function generate_report(div,stype)
	{
		freeze_window(3);
		if (stype==1) // main call
		{
			document.getElementById(div).innerHTML="";
			var cbo_company_name=document.getElementById('cbo_company_name').value;
			var data=cbo_company_name+"_____";
		}
		else
		{
			var cbo_company_name=document.getElementById('cbo_company_name').value;
			var txt_job_number=document.getElementById('txt_job_number').value;
			var txt_po_number=document.getElementById('txt_po_number').value;
			var txt_style_number=document.getElementById('txt_style_number').value;
			var txt_order_qnty=document.getElementById('txt_order_qnty').value;
			var buyer_name=document.getElementById('buyer_name').value;
			var data=cbo_company_name+"_"+txt_job_number+"_"+txt_style_number+"_"+txt_po_number+"_"+txt_order_qnty+"_"+buyer_name;	
		}
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		if (cbo_company_name==0)
		{
			$("#messagebox").removeClass().addClass('messagebox').text('Please Select a Company....').fadeIn(1000);
			return false; 
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200)
			{
				var response=(xmlhttp.responseText).split('****');
				$('#data_panel').html( '<br><b>Convert To </b><a href="requires/' + response[1] + '" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>' );
				$('#data_panel').append( '&nbsp;&nbsp;&nbsp;<input type="button" onclick="new_window()" value="HTML Preview" name="Print" class="formbutton" style="width:100px"/>' );
				$('#report_container').html(response[0]);
				
				//setFilterGrid("tbl_ship_pending");
				
				release_freezing();
			}
		}
		xmlhttp.open("GET","requires/shipment_pending_report_controller.php?data="+data+"&action=shipment_pending_report",true);
		xmlhttp.send();
	}
	
	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		 
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
		d.close();
 		//document.getElementById('report_div').style.overflowY="scroll";
		//document.getElementById('report_div').style.maxHeight="450px";
	}	
	
	function generate_report1()
	{
		var stype=1;
		var myColValues=TF_GetColValues("table-body",28);
		myColValues="'"+myColValues.join()+"'";
		var txt_date_from=document.getElementById('txt_date_from').value;
		var txt_date_to=document.getElementById('txt_date_to').value;
		if (stype==1) // main call
		{
			document.getElementById('report_container2').innerHTML="";
			var cbo_company_name=document.getElementById('cbo_company_name').value;
			var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
			var cbo_order_status=2;
			var cbo_team_name=document.getElementById('cbo_team_name').value;
			var cbo_team_member=document.getElementById('cbo_team_member').value;
			var cbo_category_by=document.getElementById('cbo_category_by').value;
			
			var data=cbo_company_name+"_"+cbo_buyer_name+"_"+txt_date_from+"_"+txt_date_to+"_"+cbo_order_status+"_"+cbo_team_name+"_"+cbo_team_member+"_"+cbo_category_by+'_'+myColValues;
		}
		
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		var response=(xmlhttp.responseText).split('####');
		//alert (response[1]);
		document.getElementById('report_container2').innerHTML=response[0];
		document.getElementById('report_container').innerHTML=report_convert_button('../../../'); 

		//append_report_checkbox('table_header_1',1);
			/*var tableFilters = {
				col_operation: {
					 id: ["total_order_qnty_pcs","total_order_qnty","value_total_order_value","total_ex_factory_qnty","total_short_access_qnty","value_total_short_access_value","value_yarn_req_tot"],
							   col: [14,15,18,20,21,22,23],
							   operation: ["sum","sum","sum","sum","sum","sum","sum"],
							   write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
				}, 
				ex_function:{
					fn_name:generate_report1
				}
			}*/
			setFilterGrid("table-body",-1,tableFilters);
			document.getElementById('content_summary3_panel').innerHTML=document.getElementById('shipment_performance').innerHTML
						percent_set()

		}
		}
		xmlhttp.open("GET","requires/shipment_schedule_controller.php?data="+data+"&type=report_generate",true);
		xmlhttp.send();
	}
	
	function percent_set()
	{
		//alert("monzu");
		var tot_row=document.getElementById('tot_row').value;
		var tot_value_js=document.getElementById('total_value').value;
		
			for(var i=1;i<tot_row;i++)
		{
			var value_js=document.getElementById('value_'+i).value;
			var percent_value_js=((value_js*1)/(tot_value_js*1))*100
			document.getElementById('value_percent_'+i).innerHTML=percent_value_js.toFixed(2);
		}
	}

</script>
</head>
<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
    <? echo load_freeze_divs ("../../../");  ?>
   <h3 align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
       <div id="content_search_panel" > 
            <form>
                <fieldset style="width:100%" >
    <legend>Fillter Panel</legend>
        <table width="100%" cellpadding="0" cellspacing="2" >
                <tr class="form_caption">
                <td align="center" height="50" valign="middle"><font size="3"><strong>Shipment Pending Report</strong></font></td>
                </tr>
                <tr>
                <td align="center" height="30" valign="middle">
                <div id="messagebox" style="background:#F99" align="center"></div>
                </td>
                </tr>
                <tr>
                	<td align="center">
                    	<table class="rpt_table" width="300">
                        	<thead>
                            	<th>Company Name</th>
                            </thead>
                            <tr class="general">
                            	<td>
                                	<?
                                           echo create_drop_down( "cbo_company_name", 172, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, " " );
                                            ?>                                     
                                 </td>
                                 <td width="5%">
                                 	<input type="button" name="show" id="show" onClick="generate_report('report_container',1);" class="formbutton" style="width:80px" value="Show" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
 </fieldset>
 </form>
 </div>
       <div id="data_panel" align="center"></div>
       <div id="report_container"></div>
 </div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>