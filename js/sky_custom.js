$(document).ready(function () {
    var header_height = $('.header-area').innerHeight();
    var nav_height = $('.nav-area').innerHeight();
    var foot_height = $('.footer-area').innerHeight();
    var window_height = $(window).height();

    $('.content-area, #menu_conts').css({
        'height' : (window_height - (header_height+nav_height+foot_height)) +'px'
    });
    var ch = $('.content-area').innerHeight();

    /*sidebar show hide*/
    if ($('.nav > li > a').hasClass('active')){
        $('#menu_conts').css('width', '15%').show('slow');
        $('#main_body').css('width', '85%');
    }

    $('.chart-sub-menu a').each(function () {
        $('.chart-sub-menu a').removeClass('active');
        this.addClass('active');
    });
});