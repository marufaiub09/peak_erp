<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will Create Batch Reprot
Functionality	:	
JS Functions	:
Created by		:	Aziz
Creation date 	: 	10-05-2014
Updated by 		: 	
Update date		: 	
QC Performed BY	:		
QC Date			:	
Comments		:

*/
session_start(); 
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Daily Knitting Production Report", "../../", 1, 1,'','','');
?>	
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<?php echo $permission; ?>';
	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];
	$(document).ready(function(e)
	 {
            $("#txt_color").autocomplete({
			 source: str_color
		  });
     });

var tableFilters = 
	{
		col_30: "none",
		col_operation: {
		id: ["btg"],
		col: [12],
		operation: ["sum"],
		write_method: ["innerHTML"]
		}
	} 
function fn_dyeing_report_generated(operation)
{
		//alert(operation);
	var b_number=document.getElementById('batch_number').value;
	var batch_no=document.getElementById('batch_number_show').value;
		//alert(batch_number);
	var order_no=document.getElementById('order_no').value;	
	var order_no_hidden=document.getElementById('hidden_order_no').value;	
	var j_number=document.getElementById('job_number_show').value;	
	var j_number_hidden=document.getElementById('job_number').value;	
	if(j_number!="" || j_number_hidden!="" || b_number!="" || batch_no!="" || order_no!="" || order_no_hidden!="")
	{
		if(form_validation('cbo_company_name','Company')==false)
		{
			return;
		}
	}
	else
	{
		if(form_validation('cbo_company_name*txt_date_from*txt_date_to','Company*From date Fill*To date Fill')==false)
		{
			return;
		}
	}
			freeze_window(5);
		    var data="action=dyeing_production_report&operation="+operation+get_submitted_data_string('cbo_company_name*cbo_buyer_name*job_number_show*job_number*batch_number*batch_number_show*order_no*hidden_order_no*cbo_type*cbo_year*txt_color*txt_date_from*txt_date_to*cbo_result_name*cbo_batch_type',"../../");
			//alert(data);
  			http.open("POST","requires/dyeing_production_report_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_show_batch_report;
	}
  
	function fnc_show_batch_report()
	{
		if(http.readyState ==4) 
		{
			// alert(http.responseText);
			var reponse=trim(http.responseText).split("****");
			//alert(reponse);
			document.getElementById('report_container2').innerHTML=http.responseText;
			document.getElementById('report_container').innerHTML=report_convert_button('../../');
			var batch_type=document.getElementById('cbo_batch_type').value;
			if(batch_type==1)
			{
				setFilterGrid("table_body",-1,tableFilters);
			}
			else if(batch_type==2)
			{
				setFilterGrid("table_body2",-1,tableFilters);
			}
			else
			{
				setFilterGrid("table_body",-1,tableFilters);
				setFilterGrid("table_body2",-1,tableFilters);
			}
			show_msg('3');
			release_freezing();
			}
 	}
<!--BatchNumber -->
function batchnumber()
{ 
	if(form_validation('cbo_company_name','Company')==false)
	{
		return;
	}
	var company_name=document.getElementById('cbo_company_name').value;
	var batch_number=document.getElementById('batch_number_show').value;
	var batch_type=document.getElementById('cbo_batch_type').value;
	var page_link="requires/dyeing_production_report_controller.php?action=batchnumbershow&company_name="+company_name+"&batch_type="+batch_type;
	var title="Batch Number";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=400px,center=1,resize=0,scrolling=0','../')

	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("selected_id").value;
		var batch=theemail.split("_");
		document.getElementById('batch_number').value=batch[0];
		document.getElementById('batch_number_show').value=batch[1];
		release_freezing();
	}
}
<!--jobnumber -->
function jobnumber(id)
{ 
	if(form_validation('cbo_company_name','Company')==false)
	{
		return;
	}
	var company_name=document.getElementById('cbo_company_name').value;
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	
	
	var batch_type=document.getElementById('cbo_batch_type').value;
	var year=document.getElementById('cbo_year').value;
	var page_link="requires/dyeing_production_report_controller.php?action=jobnumbershow&company_id="+company_name+"&cbo_buyer_name="+cbo_buyer_name+"&year="+year+"&batch_type="+batch_type;
	var title="Job Number";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=615px,height=420px,center=1,resize=0,scrolling=0','../')

	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("selected_id").value;
		//var job=theemail.split("_");
		document.getElementById('job_number').value=theemail;
		document.getElementById('job_number_show').value=theemail;
		release_freezing();
	}
}
function openmypage_order(id)
{ 
	if(form_validation('cbo_company_name','Company')==false)
	{
		return;
	}
	var company_name=document.getElementById('cbo_company_name').value;
	var buyer_name=document.getElementById('cbo_buyer_name').value;
	var year=document.getElementById('cbo_year').value;
	var job_number=document.getElementById('job_number_show').value;
	var batch_number=document.getElementById('batch_number_show').value;
	var batch_type=document.getElementById('cbo_batch_type').value;
	//var ext_number=document.getElementById('txt_ext_no').value;
	var year=document.getElementById('cbo_year').value;
	var page_link="requires/dyeing_production_report_controller.php?action=order_number_popup&company_name="+company_name+"&buyer_name="+buyer_name+"&year="+year+"&batch_type="+batch_type;
	var title="Order Number";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=515px,height=420px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("selected_id").value;
		//var job=theemail.split("_");
		document.getElementById('hidden_order_no').value=theemail;
		document.getElementById('order_no').value=theemail;
		release_freezing();
	}
}
	function toggle( x, origColor ) {
		var newColor = 'green';
		if ( x.style ) {
			x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
		}
	}
		
function js_set_value( str ) {
	toggle( document.getElementById( 'tr_' + str), '#FFF' );
}

function openmypage_color(id)
{ 
	if(form_validation('cbo_company_name','Company')==false)
	{
		return;
	}
	var company_name=document.getElementById('cbo_company_name').value;
	var buyer_name=document.getElementById('cbo_buyer_name').value;
	var txtcolor=document.getElementById('txt_color').value;
	var job_number=document.getElementById('job_number_show').value;
	var batch_number=document.getElementById('batch_number_show').value;
	//var ext_number=document.getElementById('txt_ext_no').value;
	var year=document.getElementById('cbo_year').value;
	var page_link="requires/dyeing_production_report_controller.php?action=check_color_id&txtcolor="+txtcolor;
	var title="Color Name";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=320px,height=350px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{ 
		var theemail=this.contentDoc.getElementById("selected_id").value;
		var split_value=theemail.split('_');
		//alert(theemail);
		document.getElementById('hidden_color_id').value=split_value[0];
		document.getElementById('txt_color').value=split_value[1];
		release_freezing();
	}
}

	function change_color(v_id,e_color)
	{
		if (document.getElementById(v_id).bgColor=="#33CC00")
		{
			document.getElementById(v_id).bgColor=e_color;
		}
		else
		{
			document.getElementById(v_id).bgColor="#33CC00";
		}
	}

</script>
</head>
<body onLoad="set_hotkey();">
	<div style="width:100%;" align="center">
	<?php echo load_freeze_divs ("../../",''); ?>
		 <form name="dailyYarnStatusReport_1" id="dailyYarnStatusReport_1"> 
         <h3 style="width:1200px; margin-top:10px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu(this.id,'content_search_panel','')">-Search Panel</h3>         <div id="content_search_panel" >      
             <fieldset style="width:1200px;">
                 <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
                        <thead>
                       		<th class="must_entry_caption">Report Type</th>
                            <th>Batch Type</th>
                            <th class="must_entry_caption">Company Name</th>
                            <th>Buyer</th>
                            <th>Year</th>
                            <th>Job No</th>
                            <th>Batch No</th>
                            <th>Order No</th>
                            <th>Color</th>
                            <th>Result</th>
                            <th class="must_entry_caption">Dyeing Date</th>
                            <th><input type="reset" name="res" id="res" value="Reset" onClick="reset_form('dailyYarnStatusReport_1','report_container*report_container2','','','')" class="formbutton" style="width:70px" /></th>
                        </thead>
                        <tbody>
                            <tr>
                            	<td>
                                <?php 
								$search_by_arr=array(1=>"Dyeing WIP",2=>"Daily Dyeing Production",3=>"Daily Right First Time",4=>"Daily Dyeing Reprocess Summary");
								echo create_drop_down( "cbo_type",120, $search_by_arr,"",0, "", "",'',0 );
								?>
                                </td>
                            	<td>
                                <?php 
								$batch_type_arr=array(1=>"Self Batch",2=>"SubCon Batch");
								echo create_drop_down( "cbo_batch_type",70, $batch_type_arr,"",1, "--All--", 0,"load_drop_down('requires/dyeing_production_report_controller',document.getElementById('cbo_company_name').value+'_'+this.value, 'load_drop_down_buyer', 'cbo_buyer_name_td' );",0 );
								?>
                                </td>
                                <td> 
                                    <?php
                                        echo create_drop_down( "cbo_company_name", 120, "select id,company_name from lib_company where status_active=1 and is_deleted=0  order by company_name","id,company_name", 1, "-- Select Company --", 0, "load_drop_down('requires/dyeing_production_report_controller',this.value+'_'+document.getElementById('cbo_batch_type').value, 'load_drop_down_buyer', 'cbo_buyer_name_td' );" );
                                    ?>
                                </td>
                                <td id="cbo_buyer_name_td">
                                	<?php
									   echo create_drop_down( "cbo_buyer_name", 120, $blank_array,"", 1, "-- Select Buyer --", $selected, "",0,"" );
									?>
                                </td>
                                 <td>
                                	<?php
                                       echo create_drop_down( "cbo_year", 50, create_year_array(),"", 1,"-- All --", date("Y",time()), "",0,"" );
									?>
                                </td>
                                <td>
                                     <input type="text"  name="job_number_show" id="job_number_show" class="text_boxes" style="width:70px;" tabindex="1" placeholder="Write/Browse" onDblClick="jobnumber();">
                                     <input type="hidden" name="job_number" id="job_number">
                                 </td>
                                <td>
                                     <input type="text"  name="batch_number_show" id="batch_number_show" class="text_boxes" style="width:70px;" tabindex="1" placeholder="Write/Browse" onDblClick="batchnumber();">
                                     <input type="hidden" name="batch_number" id="batch_number">
                                </td>
                                  <td>
                                     <input type="text"  name="order_no" id="order_no" class="text_boxes" style="width:70px;" tabindex="1" placeholder="Write/Browse" onDblClick="openmypage_order()">
                                     <input type="hidden" name="hidden_order_no" id="hidden_order_no">
                                </td>
                                 <td>
                        <input type="text"  name="txt_color" id="txt_color" class="text_boxes" style="width:70px;" tabindex="1" placeholder="Write/Browse" onDblClick="openmypage_color()">           <input type="hidden" name="hidden_color_id" id="hidden_color_id">
                                </td>
                                 <td>
                                	<?php
									echo create_drop_down("cbo_result_name", 100, $dyeing_result,"", 1, "-- Select Result --", 0, "",0 ,"","","","",""); 
                                ?>
                                </td>
                                <td align="center">
                                     <input type="text" name="txt_date_from" id="txt_date_from" value="" class="datepicker" style="width:60px" placeholder="From Date"/>
                                     &nbsp;To&nbsp;
                                     <input type="text" name="txt_date_to" id="txt_date_to" value="" class="datepicker" style="width:60px" placeholder="To Date"/>
                                </td>
                                <td><input type="button" id="show_button" class="formbutton" style="width:70px" value="Show" onClick="fn_dyeing_report_generated(0)" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
            	<tr>
                	<td colspan="9">
 						<?php echo load_month_buttons(1); ?>
                   	</td>
                </tr>
            </table> 
            <br />
                </fieldset>
            </div>
           <div id="report_container"></div>
    	   <div id="report_container2"></div>
		</form>
	</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>