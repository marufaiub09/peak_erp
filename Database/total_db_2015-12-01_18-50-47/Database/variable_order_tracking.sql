-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `variable_order_tracking`
--

DROP TABLE IF EXISTS `variable_order_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `variable_order_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` int(11) NOT NULL DEFAULT '0',
  `variable_list` int(11) NOT NULL DEFAULT '0',
  `sales_year_started_hcode` varchar(30) NOT NULL DEFAULT '0',
  `sales_year_started` int(11) NOT NULL DEFAULT '0',
  `tna_integrated` int(11) NOT NULL DEFAULT '0',
  `profit_calculative` int(11) NOT NULL DEFAULT '0',
  `process_loss_method` int(11) NOT NULL DEFAULT '0',
  `item_category_id` int(11) NOT NULL DEFAULT '0',
  `consumption_basis` int(11) NOT NULL DEFAULT '0',
  `copy_quotation` int(11) NOT NULL DEFAULT '0',
  `conversion_from_chart` int(11) NOT NULL DEFAULT '0',
  `cm_cost_method` int(11) NOT NULL DEFAULT '0',
  `color_from_library` int(11) NOT NULL DEFAULT '2',
  `publish_shipment_date` int(11) NOT NULL DEFAULT '1',
  `exeed_budge_qty` double NOT NULL,
  `exeed_budge_amount` double NOT NULL,
  `amount_exceed_level` int(4) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `commercial_cost_method` int(11) NOT NULL,
  `commercial_cost_percent` double NOT NULL,
  `editable` int(11) NOT NULL DEFAULT '1',
  `gmt_num_rep_sty` int(11) NOT NULL DEFAULT '2',
  `duplicate_ship_date` int(2) NOT NULL DEFAULT '2',
  `image_mandatory` int(1) NOT NULL,
  `tna_process_type` int(11) NOT NULL DEFAULT '1',
  `po_update_period` int(3) NOT NULL,
  `po_current_date` int(5) NOT NULL,
  `inquery_id_mandatory` int(2) NOT NULL DEFAULT '2',
  `trim_rate` int(11) NOT NULL DEFAULT '2',
  `cm_cost_method_quata` int(11) NOT NULL DEFAULT '0',
  `budget_exceeds_quot` int(11) NOT NULL DEFAULT '2',
  `s_f_booking_befor_m_f` tinyint(4) NOT NULL DEFAULT '0',
  `colar_culff_percent` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `company_name` (`company_name`),
  KEY `variable_list` (`variable_list`),
  KEY `tna_integrated` (`tna_integrated`),
  KEY `item_category_id` (`item_category_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `variable_order_tracking`
--

LOCK TABLES `variable_order_tracking` WRITE;
/*!40000 ALTER TABLE `variable_order_tracking` DISABLE KEYS */;
INSERT INTO `variable_order_tracking` VALUES (1,1,22,'0',0,0,0,0,0,0,0,0,1,0,0,0,0,0,2,'2015-01-04 02:06:52',0,NULL,1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(2,1,15,'0',0,0,1,0,0,0,0,0,0,0,0,0,0,0,2,'2015-01-04 02:07:03',0,NULL,1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(3,1,18,'',0,0,0,2,2,0,0,0,0,2,1,0,0,0,2,'2015-01-04 02:07:37',1,'2015-01-29 04:04:17',1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(4,1,18,'',0,0,0,1,3,0,0,0,0,2,1,0,0,0,2,'2015-01-04 02:07:37',1,'2015-01-29 04:04:17',1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(5,1,18,'',0,0,0,1,4,0,0,0,0,2,1,0,0,0,2,'2015-01-04 02:07:37',1,'2015-01-29 04:04:17',1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(6,1,18,'',0,0,0,1,12,0,0,0,0,2,1,0,0,0,2,'2015-01-04 02:07:37',1,'2015-01-29 04:04:17',1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(7,1,18,'',0,0,0,1,13,0,0,0,0,2,1,0,0,0,2,'2015-01-04 02:07:37',1,'2015-01-29 04:04:17',1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(8,1,18,'',0,0,0,1,14,0,0,0,0,2,1,0,0,0,2,'2015-01-04 02:07:37',1,'2015-01-29 04:04:17',1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(9,1,12,'0',1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,'2015-01-04 02:08:11',0,NULL,1,0,0,0,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(10,1,27,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,'2015-01-29 01:31:58',1,'2015-01-29 01:32:51',1,0,0,3,0,1,2,0,0,1,0,0,2,2,0,2,0,1),(11,1,23,'0',0,0,0,0,0,0,0,0,0,2,0,0,0,0,1,'2015-02-28 10:38:55',20,'2015-03-02 11:25:25',1,0,0,0,0,0,2,0,0,1,0,0,2,2,0,2,0,1),(12,1,31,'0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,'2015-09-08 16:05:11',2,'2015-10-08 09:35:36',1,0,0,0,0,0,0,0,0,1,0,0,0,2,0,2,0,1),(13,1,20,'0',0,0,0,0,0,0,2,0,0,0,0,0,0,0,2,'2015-11-23 13:16:42',2,'2015-11-26 15:26:31',1,0,0,0,0,0,0,0,0,0,0,0,0,2,0,2,0,1);
/*!40000 ALTER TABLE `variable_order_tracking` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
