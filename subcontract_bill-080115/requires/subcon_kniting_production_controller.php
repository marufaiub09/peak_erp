<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$process_knitting="2";
$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 150, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "--Select Location--", $selected, "load_drop_down( 'requires/subcon_kniting_production_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_down_floor', 'floor_td' );","","","","","",3 );
	exit();	 
}

if ($action=="load_drop_down_party_name")
{
echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "","","","","","",4 );
	exit();	
}

if ($action=="load_drop_down_floor")
{
	$data=explode("_",$data);
	$company_id=$data[0];
	$location_id=$data[1];
	if($location_id==0 || $location_id=="") $location_cond=""; else $location_cond=" and b.location_id=$location_id";
	
	echo create_drop_down( "cbo_floor_id", 150, "select a.id, a.floor_name from lib_prod_floor a, lib_machine_name b where a.id=b.floor_id and b.category_id=1 and b.company_id=$company_id and b.status_active=1 and b.is_deleted=0 and a.production_process=2 $location_cond group by a.id, a.floor_name order by a.floor_name","id,floor_name", 1, "-- Select Floor --", 0, "load_drop_down( 'requires/subcon_kniting_production_controller',document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_down_machine', 'machine_td' );","" );
  exit();	 
}

if ($action=="load_drop_down_machine")
{
	$data_ex=explode("_",$data);
	$company_id=$data_ex[0];
	$floor_id=$data_ex[1];
	if($floor_id==0 || $floor_id=="") $floor_cond=""; else $floor_cond=" and floor_id=$floor_id";
	if($db_type==0)
	{
		$sql="select id,concat(machine_no,'-',brand) as machine_name from lib_machine_name where category_id=1 and company_id=$company_id and status_active=1 and is_deleted=0 and is_locked=0 $floor_cond order by machine_name";
	}
	else if($db_type==2)
	{
		$sql="select id, machine_no || '-' || brand as machine_name from lib_machine_name where category_id=1 and company_id=$company_id and status_active=1 and is_deleted=0 and is_locked=0 $floor_cond order by machine_name";
	}
	echo create_drop_down( "cbo_machine_name", 150, $sql,"id,machine_name", 1, "-- Select Machine --", 0, "","" );
	exit();
}


if ($action=="production_id_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	$ex_data=explode("_",$data);
	?>
	  <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('product_id').value=id;
			  parent.emailwindow.hide();
		  }
	  </script>
    </head>
    <body>
        <div align="center" style="width:100%;" >
        <form name="searchjobfrm_1"  id="searchjobfrm_1" autocomplete="off">
        <table width="850" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>                	 
                <th width="150">Company Name</th>
                <th width="150">Party Name</th>
                <th width="110">Production ID</th>
                <th width="200">Date Range</th>
                <th width="100"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>           
            </thead>
            <tbody>
                <tr>
                    <td> <input type="hidden" id="product_id">  
						<?   
							echo create_drop_down( "cbo_company_name", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $ex_data[0],"load_drop_down( 'subcon_kniting_production_controller', this.value, 'load_drop_down_party_name', 'party_td' );",0 );
                        ?>
                    </td>
                    <td id="party_td">
                        <? echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$ex_data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $ex_data[1], "" );   	 
                        ?>
                    </td>
                    <td>
                        <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:95px" />
                    </td>
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px">
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px">
                    </td> 
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_party_name').value, 'kniting_production_id_search_list_view', 'search_div', 'subcon_kniting_production_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle">
						<? echo load_month_buttons(1);  ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" valign="top" id=""><div id="search_div"></div></td>
                </tr>
            </tbody>
        </table>    
        </form>
        </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?
	exit();
}

if ($action=="kniting_production_id_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company_name=" and a.company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!="" &&  $data[2]!="") $return_date = "and a.product_date between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'"; else $return_date="";
	if ($data[3]!='') $issue_cond=" and a.prefix_no_num='$data[3]'"; else $issue_cond="";
	if ($data[4]!=0) $buyer_cond=" and a.party_id='$data[4]'"; else $buyer_cond="";
	
	$style_ref_arr=return_library_array( "select id, cust_style_ref from subcon_ord_dtls",'id','cust_style_ref');
	$po_arr=return_library_array( "select id,order_no from subcon_ord_dtls",'id','order_no');
	$return_to=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
	
	if($db_type==0)
	{
		$sql= "select a.id, a.product_no, a.prefix_no_num, year(a.insert_date)as year, a.party_id, a.product_date, a.prod_chalan_no, a.yrn_issu_chalan_no, group_concat(distinct(b.order_id)) as order_id, sum(product_qnty) as product_qnty from subcon_production_mst a, subcon_production_dtls b where a.id=b.mst_id and a.product_type=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $company_name $buyer_cond $return_date $issue_cond group by a.id order by a.id DESC";
	}
	else if($db_type==2)
	{
		$sql= "select a.id, a.product_no, a.prefix_no_num, TO_CHAR(a.insert_date,'YYYY') as year, a.party_id, a.product_date, a.prod_chalan_no, a.yrn_issu_chalan_no, listagg((cast(b.order_id as varchar2(4000))),',') within group (order by b.order_id) as order_id, sum(product_qnty) as product_qnty from subcon_production_mst a, subcon_production_dtls b where a.id=b.mst_id and a.product_type=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $company_name $buyer_cond $return_date $issue_cond group by a.id, a.product_no, a.prefix_no_num, a.insert_date, a.party_id, a.product_date, a.prod_chalan_no, a.yrn_issu_chalan_no order by a.id DESC";
	}
	
	$result = sql_select($sql);
	?>
    <div>
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="867" class="rpt_table">
            <thead>
                <th width="30" >SL</th>
                <th width="60" >Product ID</th>
                <th width="60" >Year</th>
                <th width="100" >Party </th>
                <th width="70" >Date</th>
                <th width="80" >Product Challan</th>
                <th width="80" >Issue Chalan</th>
                <th width="130" >Order No</th>
                <th width="130" >Style</th>
                <th>Qty</th>
            </thead>
     	</table>
     <div style="width:870px; max-height:270px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="850" class="rpt_table" id="list_view">	
        <?
            $i=1;
            foreach( $result as $row )
            {
                if ($i%2==0)  $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$order_no='';
				$style_ref='';
				$order_id=array_unique(explode(",",$row[csf("order_id")]));
				foreach($order_id as $val)
				{
					if($order_no=="") $order_no=$po_arr[$val]; else $order_no.=", ".$po_arr[$val];
					if($style_ref=="") $style_ref=$style_ref_arr[$val]; else $style_ref.=", ".$style_ref_arr[$val];
				}
				?>
					<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value(<? echo $row[csf("id")];?>);" > 
						<td width="30" align="center"><? echo $i; ?></td>
						<td width="60" align="center"><? echo $row[csf("prefix_no_num")]; ?></td>
                        <td width="60" align="center"><? echo $row[csf("year")]; ?></td>
                        <td width="100"><p><? echo $return_to[$row[csf("party_id")]]; ?></p></td>		
						<td width="70"><? echo change_date_format($row[csf("product_date")]); ?></td>
						<td width="80"><p><? echo $row[csf("prod_chalan_no")]; ?></p></td>	
                        <td width="80"><? echo $row[csf("yrn_issu_chalan_no")]; ?></td>	
                        <td width="130"><p><? echo  $order_no; ?></p></td>	
                        <td width="130"><p><? echo $style_ref; ?></p></td>	
						<td align="right"><? echo number_format($row[csf("product_qnty")],2,'.',''); ?></td>
					</tr>
				<? 
				$i++;
            }
   		?>
			</table>
		</div>
     </div>        
        
	<?
	exit();
}

if ($action=="load_php_data_to_form_mst")
{
	$nameArray=sql_select( "select id,product_no,company_id,location_id,party_id,product_date,prod_chalan_no,yrn_issu_chalan_no,remarks from subcon_production_mst where id='$data'" ); 
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_production_id').value 			= '".$row[csf("product_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "load_drop_down( 'requires/subcon_kniting_production_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_location', 'location_td' );\n";
		echo "document.getElementById('cbo_location_name').value			= '".$row[csf("location_id")]."';\n"; 
		echo "load_drop_down( 'requires/subcon_kniting_production_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_party_name', 'party_td' );\n"; 
		echo "document.getElementById('cbo_party_name').value				= '".$row[csf("party_id")]."';\n"; 
		echo "document.getElementById('txt_production_date').value 			= '".change_date_format($row[csf("product_date")])."';\n";   
		echo "document.getElementById('txt_prod_chal_no').value				= '".$row[csf("prod_chalan_no")]."';\n"; 
		echo "document.getElementById('txt_yarn_issue_challan_no').value	= '".$row[csf("yrn_issu_chalan_no")]."';\n"; 
		echo "document.getElementById('txt_remarks').value					= '".$row[csf("remarks")]."';\n"; 
	    echo "document.getElementById('update_id').value            		= '".$row[csf("id")]."';\n";
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'subcon_kniting_production',1);\n";
	}
	exit();	
}

if ($action=="cons_comp_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('comp_id').value=id;
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="searchjobfrm_1"  id="searchjobfrm_1" autocomplete="off">
                <table width="550" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="150">Company Name</th>
                        <th width="200">Date Range</th>
                        <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>           
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="comp_id">  
								<?   
									echo create_drop_down( "cbo_company_name", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $data,"",0 );
                                ?>
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value, 'kniting_production_cons_comp_list_view', 'search_div', 'subcon_kniting_production_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" height="40" valign="middle">
								<? echo load_month_buttons(1);  ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?
	exit();
}

if ($action=="kniting_production_cons_comp_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company_name=" and comapny_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	
	$sql= "select id,comapny_id,body_part,const_comp,gsm,yarn_description,in_house_rate,uom_id,status_active from lib_subcon_charge where is_deleted=0 and rate_type_id=2 $company_name";
	
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$arr=array (0=>$body_part,4=>$unit_of_measurement);
	
	echo  create_list_view ( "list_view", "Body Part,Construction & Composition,GSM,Yarn Description,UOM", "100,160,60,120,60","550","220",1, $sql, "js_set_value", "id","", 1, "body_part,0,0,0,uom_id", $arr , "body_part,const_comp,gsm,yarn_description,uom_id", "subcon_kniting_production_controller", 'setFilterGrid("list_view",-1);','0,0,2,0,0' ) ;	
	exit();
}

if ($action=="load_php_data_to_form_comp")
{
	$nameArray=sql_select( "select id,const_comp,gsm from lib_subcon_charge where id='$data'" ); 
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_febric_description').value 			= '".$row[csf("const_comp")]."';\n";
	    echo "document.getElementById('txt_gsm').value            				= '".$row[csf("gsm")]."';\n";
		echo "document.getElementById('hidd_comp_id').value            			= '".$row[csf("id")]."';\n";
	}
	exit();
}

if ($action=="kniting_production_list_view")
{
	$yearn_count_arr=return_library_array( "select id,yarn_count from lib_yarn_count",'id','yarn_count');
	$machine_arr=return_library_array( "select id,machine_no from lib_machine_name",'id','machine_no');
	
	$sql = "select a.id, a.mst_id, a.process, a.fabric_description, a.gsm, a.dia_width, a.no_of_roll, a.product_qnty, a.yarn_lot, a.yrn_count_id, b.order_no, a.machine_id from subcon_production_dtls a,subcon_ord_dtls b where a.order_id=b.id and a.status_active=1 and a.mst_id='$data'"; 
	

	$result = sql_select($sql);
	?>
    <div>
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="917" class="rpt_table">
            <thead>
                <th width="30" >SL</th>
                <th width="70" >Process</th>
                <th width="70" >Order</th>
                <th width="150" >Cont. Composition </th>
                <th width="60" >GSM</th>
                <th width="60">Width</th>
                <th width="50" >No of Roll</th>
                <th width="80" >Prod Qty</th>
                <th width="80" >Y-Lot</th>
                <th width="130" >Y-Count</th>
                <th>Mac-No</th>
            </thead>
     	</table>
     <div style="width:920px; max-height:270px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="900" class="rpt_table" id="list_view">	
        <?
            $i=1;
            foreach( $result as $row )
            {
                if ($i%2==0)  $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$yarn_count=array_unique(explode(",",$row[csf('yrn_count_id')]));
				$count_name="";
				foreach($yarn_count as $val)
				{
					if($count_name=="") $count_name=$yearn_count_arr[$val]; else $count_name.=", ".$yearn_count_arr[$val];
				}	
				?>
					<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="get_php_form_data(<? echo $row[csf("id")]; ?>,'load_php_data_to_form_dtls','requires/subcon_kniting_production_controller');" > 
						<td width="30" align="center"><? echo $i; ?></td>
						<td width="70"><? echo $conversion_cost_head_array[$row[csf("process")]]; ?></td>
                        <td width="70" align="center"><? echo $row[csf("order_no")]; ?></td>
                        <td width="150"><p><? echo $row[csf("fabric_description")]; ?></p></td>		
						<td width="60" align="center"><? echo $row[csf("gsm")]; ?></td>
						<td width="60" align="center"><p><? echo $row[csf("dia_width")]; ?></p></td>	
                        <td width="50" align="right"><? echo $row[csf("no_of_roll")]; ?></td>	
                        <td width="80" align="right"><? echo number_format($row[csf("product_qnty")],2,'.',''); ?></td>	
                        <td width="80"><p><? echo $row[csf("yarn_lot")]; ?></p></td>	
                        <td width="130"><p><? echo $count_name; ?></p></td>	
						<td><? echo $machine_arr[$row[csf("machine_id")]]; ?></td>
					</tr>
				<? 
				$i++;
            }
   		?>
			</table>
		</div>
     </div>        
	<?	
	exit();
}

if ($action=="load_php_data_to_form_dtls")
{
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$nameArray=sql_select( "select a.id, a.process, a.fabric_description, a.cons_comp_id, a.gsm, a.dia_width, a.no_of_roll, a.product_qnty, a.reject_qnty, a.uom_id, a.yarn_lot, a.yrn_count_id, a.floor_id, a.machine_id, a.brand,a.shift, a.stitch_len, a.color_range, a.color_id, a.remarks, a.status_active, b.id as order_tbl_id, b.job_no_mst, b.order_no, b.order_uom, b.main_process_id from subcon_production_dtls a,subcon_ord_dtls b where a.order_id=b.id and a.id='$data'" );
	foreach ($nameArray as $row)
	{	
	
		echo "document.getElementById('cbo_process').value		 				= '".$row[csf("process")]."';\n";
		echo "document.getElementById('txt_febric_description').value		 	= '".$row[csf("fabric_description")]."';\n"; 
		echo "document.getElementById('hidd_comp_id').value		 				= '".$row[csf("cons_comp_id")]."';\n"; 
		echo "document.getElementById('txt_gsm').value							= '".$row[csf("gsm")]."';\n"; 
		echo "document.getElementById('txt_width').value						= '".$row[csf("dia_width")]."';\n";  
		echo "document.getElementById('txt_roll_qnty').value					= '".$row[csf("no_of_roll")]."';\n";  
		echo "document.getElementById('txt_order_no').value		 				= '".$row[csf("order_no")]."';\n";
		echo "document.getElementById('order_no_id').value		 				= '".$row[csf("order_tbl_id")]."';\n";
		echo "document.getElementById('process_id').value		 				= '".$row[csf("main_process_id")]."';\n"; 
		echo "document.getElementById('txt_product_qnty').value		 			= '".$row[csf("product_qnty")]."';\n";
		echo "document.getElementById('txt_reject_qnty').value            		= '".$row[csf("reject_qnty")]."';\n";
		echo "document.getElementById('cbo_uom').value		 					= '".$row[csf("uom_id")]."';\n"; 
		
		echo "document.getElementById('txt_yarn_lot').value						= '".$row[csf("yarn_lot")]."';\n"; 
		echo "document.getElementById('cbo_yarn_count').value					= '".$row[csf("yrn_count_id")]."';\n";  
		echo "set_multiselect('cbo_yarn_count','0','1','".$row[csf("yrn_count_id")]."','0');\n"; 
		echo "document.getElementById('txt_brand').value						= '".$row[csf("brand")]."';\n";  
		echo "document.getElementById('cbo_shift_id').value		 				= '".$row[csf("shift")]."';\n";
		echo "load_drop_down( 'requires/subcon_kniting_production_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_floor', 'floor_td' );\n";
		echo "document.getElementById('cbo_floor_id').value		 				= '".$row[csf("floor_id")]."';\n";
		echo "load_drop_down( 'requires/subcon_kniting_production_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_machine', 'machine_td' );\n";
		echo "document.getElementById('cbo_machine_name').value		 			= '".$row[csf("machine_id")]."';\n";
		//echo "document.getElementById('hidden_machine_no').value		 			= '".$row[csf("machin_no_id")]."';\n";
		echo "document.getElementById('txt_job_no').value		 				= '".$row[csf("job_no_mst")]."';\n";
		
		echo "document.getElementById('txt_stitch_len').value		 			= '".$row[csf("stitch_len")]."';\n";
		echo "document.getElementById('cbo_color_range').value		 			= '".$row[csf("color_range")]."';\n";
		echo "document.getElementById('txt_color').value		 				= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('text_new_remarks').value            		= '".$row[csf("remarks")]."';\n";
		echo "show_list_view(document.getElementById('order_no_id').value+'_'+document.getElementById('process_id').value,'show_fabric_desc_listview','list_fabric_desc_container','requires/subcon_kniting_production_controller','');\n";	
		echo "document.getElementById('update_id_dtl').value            		= '".$row[csf("id")]."';\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'subcon_kniting_production',1);\n";	
	}	
	exit();
}

/*if ($action=="is_machine_no_or_not")
{
	$nameArray=sql_select( "select id,machine_no from lib_machine_name where machine_no=$data" ); 
	if(!$nameArray){echo "1";}
	exit();
}
*/
if ($action=="order_no_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	//$data=explode('_',$data);
	//echo $data[1];
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('selected_job').value=id;
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="searchjobfrm_1"  id="searchjobfrm_1" autocomplete="off">
                <table width="750" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>
                        <tr>
                            <th colspan="6"><? echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" ); ?></th>
                        </tr>
                    	<tr>               	 
                            <th width="140">Company Name</th>
                            <th width="140">Party Name</th>
                            <th width="170">Date Range</th>
                            <th width="100">Search Job</th>
                            <th width="100">Search Order</th>
                            <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>
                        </tr>         
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="selected_job">  
								<?   
									$data=explode("_",$data);
									echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $data[0],"",1);
                                ?>
                            </td>
                            <td>
								<? echo create_drop_down( "cbo_party_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[0]' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --",$data[1], "",1 );   	 
								?>
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                            </td> 
                            <td >
                                 <input type="text" name="txt_search_job" id="txt_search_job" class="text_boxes" style="width:100px" placeholder="Job" />
                            </td>
                            <td align="center" id="search_by_td">
                                <input type="text" name="txt_search_order" id="txt_search_order" class="text_boxes" style="width:100px" placeholder="Order" />
                            </td>
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_job').value+'_'+document.getElementById('txt_search_order').value+'_'+document.getElementById('cbo_string_search_type').value, 'job_search_list_view', 'search_div', 'subcon_kniting_production_controller', 'setFilterGrid(\'tbl_list_search\',-1)')" style="width:70px;" />
                            </td>

                        </tr>
                        <tr>
                            <td colspan="6" align="center" height="40" valign="middle">
								<? echo load_month_buttons(1);  ?>
                                <input type="hidden" name="hidden_mst_id" id="hidden_mst_id" class="datepicker" style="width:70px">
                            </td>
                        </tr>
                    </tbody>
                </table> 
                <div id="search_div"></div>   
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?
	exit();	
}

if ($action=="job_search_list_view")
{
	$data=explode('_',$data);
	$search_job=str_replace("'","",$data[4]);
	$search_order=trim(str_replace("'","",$data[5]));
	$search_type =$data[6];
	
	if ($data[0]!=0) $company=" and company_id='$data[0]'"; else  $company="";
	if ($data[1]!=0) $buyer=" and party_id='$data[1]'"; else $buyer="";
	
	if($search_type==1)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num='$search_job'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no='$search_order'";
	}
	else if($search_type==4 || $search_type==0)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '%$search_job%'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '%$search_order%'";
	}
	else if($search_type==2)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '$search_job%'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '$search_order%'";
	}
	else if($search_type==3)
	{
		if($search_job=='') $search_job_cond=""; else $search_job_cond="and a.job_no_prefix_num like '%$search_job'";  
		if($search_order=='') $search_order_cond=""; else $search_order_cond=" and b.order_no like '%$search_order'";
	}	
	
	if($db_type==0)
	{ 
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2],'yyyy-mm-dd')."' and '".change_date_format($data[3],'yyyy-mm-dd')."'"; else $order_rcv_date ="";
	}
	else
	{
		if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2], "", "",1)."' and '".change_date_format($data[3], "", "",1)."'"; else $order_rcv_date ="";
	}
	
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	if($db_type==0)
	{
		$sql= "select b.id as ord_id, a.subcon_job, a.job_no_prefix_num, year(a.insert_date)as year, b.process_id, a.party_id, b.order_no, b.delivery_date, b.cust_style_ref, b.main_process_id, b.order_quantity, b.order_uom from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $order_rcv_date $company $buyer $search_job_cond $search_order_cond order by a.id DESC";
	}
	else if($db_type==2)
	{
		$sql= "select b.id as ord_id, a.subcon_job, a.job_no_prefix_num, TO_CHAR(a.insert_date,'YYYY') as year, b.process_id, a.party_id, b.order_no, b.delivery_date, b.cust_style_ref, b.main_process_id, b.order_quantity, b.order_uom from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $order_rcv_date $company $buyer $search_job_cond $search_order_cond order by a.id DESC";
	}
	//echo $sql;die;
?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="768" class="rpt_table" >
            <thead>
                <th width="30">SL</th>
                <th width="50">Job </th>
                <th width="60">Year</th>
                <th width="130">Style No</th>
                <th width="100">PO No</th>
                <th width="100">Process</th>
                <th width="80">PO Quantity</th>
                <th width="40">UOM</th>
                <th>Delivery Date</th>
            </thead>
        </table>
        <div style="width:768px; overflow-y:scroll; max-height:280px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="tbl_list_search" >
            <?
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					$processid=explode(",",$selectResult[csf('process_id')]);
					$knit_array=array(1,3,4);
					//$query_arr=array_intersect($knit_array,$processid);
					//print_r ($query_arr); 
					if(array_intersect($knit_array,$processid))
					{
				?>
                    <tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<? echo $i;?>" onClick="js_set_value(<? echo $selectResult[csf('ord_id')]; ?>)"> 
                        <td width="30" align="center"><? echo $i; ?></td>	
                        <td width="50" align="center"><p><? echo $selectResult[csf('job_no_prefix_num')]; ?></p></td>
                        <td width="60" align="center"><? echo $selectResult[csf('year')]; ?></td>
                        <td width="130"><p><? echo $selectResult[csf('cust_style_ref')]; ?></p></td>
                        <td width="100"><p><? echo $selectResult[csf('order_no')]; ?></p></td>
                        <td width="100"><p><? echo $production_process[$selectResult[csf('main_process_id')]]; ?></p></td>
                        <td width="80" align="right"><? echo number_format( $selectResult[csf('order_quantity')],2); ?>&nbsp;</td> 
                        <td width="40" align="center"><p><? echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                        <td align="center"><? echo change_date_format($selectResult[csf('delivery_date')]); ?></td>	
                    </tr>
                <?
                	$i++;
					}
				}
			?>
            </table>
        </div>
	</div>           
<?	
	exit();
}

if($action=="load_php_data_to_form_dtls_order")
{
	$nameArray=sql_select( "select id, order_no, order_uom, main_process_id, job_no_mst from subcon_ord_dtls where id='$data'" );
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_order_no').value		= '".$row[csf("order_no")]."';\n"; 
		echo "document.getElementById('txt_job_no').value		= '".$row[csf("job_no_mst")]."';\n"; 
		echo "document.getElementById('cbo_uom').value			= '".$row[csf("order_uom")]."';\n";
		echo "document.getElementById('process_id').value		= '".$row[csf("main_process_id")]."';\n";
		echo "document.getElementById('order_no_id').value		= '".$row[csf("id")]."';\n"; 
	}
	exit();
}

if($action=="show_fabric_desc_listview")
{
	$data=explode('_',$data);
	$order_id=$data[0];
	$process_id=$data[1];
	$item_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');	
	$gsm_arr=return_library_array( "select id,gsm from lib_subcon_charge",'id','gsm');
	
	$production_qty_array=array();
	$prod_sql="Select cons_comp_id, sum(product_qnty) as product_qnty from subcon_production_dtls where order_id='$data[0]' and product_type=2 and status_active=1 and is_deleted=0 group by cons_comp_id";
	$prod_data_sql=sql_select($prod_sql);
	foreach($prod_data_sql as $row)
	{
		$production_qty_array[$row[csf('cons_comp_id')]]=$row[csf('product_qnty')];
	}
	//var_dump($production_qty_array);
	$sql = "select item_id, sum(qnty) as qnty from subcon_ord_breakdown where order_id='$data[0]' group by item_id"; 
	$data_array=sql_select($sql);
	?>
     <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="350">
        <thead>
            <th width="15">SL</th>
            <th>Fabric Description</th>
            <th width="60">Order Qty</th>
            <th width="40">Prod. Qty</th>
            <th width="60">Bal. Qty</th>
        </thead>
        <tbody>
            <? 
            $i=1;
            foreach($data_array as $row)
            {  
                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";

				
				
				if($process_id==2 || $process_id==3 || $process_id==4 || $process_id==6 || $process_id==7)
				{
					$item_name=$item_arr[$row[csf('item_id')]];	
					$gsm_val=$gsm_arr[$row[csf('item_id')]];	
				}
				else
				{
					$item_name=$garments_item[$row[csf('item_id')]];
					$gsm_val='';
				}				
             ?>
                <tr bgcolor="<? echo $bgcolor; ?>" onClick='set_form_data("<? echo $row[csf('item_id')]."**".$item_name."**".$gsm_val; ?>")' style="cursor:pointer" >
                    <td><? echo $i; ?></td>
                    <td><? echo $item_name; ?></td>
                    <td align="right"><? echo number_format($row[csf('qnty')]); ?></td>
                    <td align="right"><? echo number_format($production_qty_array[$row[csf('item_id')]]); ?></td>
                    <td align="right"><? echo number_format($row[csf('qnty')]-$production_qty_array[$row[csf('item_id')]]); ?></td>
                </tr>
            <? 
            $i++; 
            } 
            ?>
        </tbody>
    </table>
<?    
	exit();
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$process_knitting="2";
	if ($operation==0)   // Insert Here==============================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)";	
		}
		else if($db_type==2)
		{
			$year_cond=" and TO_CHAR(insert_date,'YYYY')";	
		}
		$new_return_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'KNT', date("Y",time()), 5, "select id,prefix_no,prefix_no_num from  subcon_production_mst where company_id=$cbo_company_id and product_type='$process_knitting' $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
	
		if(str_replace("'",'',$update_id)=="")
		{
			$id=return_next_id( "id", "subcon_production_mst", 1 ) ; 
			$field_array="id,prefix_no,prefix_no_num,product_no,product_type,company_id,location_id,party_id,product_date,prod_chalan_no,yrn_issu_chalan_no,remarks,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_return_no[1]."','".$new_return_no[2]."','".$new_return_no[0]."','".$process_knitting."',".$cbo_company_id.",".$cbo_location_name.",".$cbo_party_name.",".$txt_production_date.",".$txt_prod_chal_no.",".$txt_yarn_issue_challan_no.",".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";  
			
			$rID=sql_insert("subcon_production_mst",$field_array,$data_array,0);
			$return_no=$new_return_no[0];
		}
		else
		{
			$id=$update_id;
			$field_array="location_id*party_id*product_date*prod_chalan_no*yrn_issu_chalan_no*remarks*updated_by*update_date";
			$data_array="".$cbo_location_name."*".$cbo_party_name."*".$txt_production_date."*".$txt_prod_chal_no."*".$txt_yarn_issue_challan_no."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			//echo  $data_array; die;
			$rID=sql_update("subcon_production_mst",$field_array,$data_array,"id",$update_id,0); 
			$return_no=$txt_production_id;
		}
		
		$color_name_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		
		$id1=return_next_id("id","subcon_production_dtls",1); 
		$febric_description=str_replace("(",'[',str_replace(")",']',$txt_febric_description));
		$field_array2="id, mst_id, job_no, order_id, product_type, process, fabric_description, cons_comp_id, gsm, dia_width, no_of_roll, product_qnty, reject_qnty, uom_id, yarn_lot, yrn_count_id, brand,shift, floor_id, machine_id, stitch_len, color_range, color_id, remarks, inserted_by, insert_date";
		$data_array2="(".$id1.",".$id.",".$txt_job_no.",".$order_no_id.",'".$process_knitting."',".$cbo_process.",".$febric_description.",".$hidd_comp_id.",".$txt_gsm.",".$txt_width.",".$txt_roll_qnty.",".$txt_product_qnty.",".$txt_reject_qnty.",".$cbo_uom.",".$txt_yarn_lot.",".$cbo_yarn_count.",".$txt_brand.",".$cbo_shift_id.",".$cbo_floor_id.",".$cbo_machine_name.",".$txt_stitch_len.",".$cbo_color_range.",".$color_name_id.",".$text_new_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
		//echo $data_array2; die;
		$rID2=sql_insert("subcon_production_dtls",$field_array2,$data_array2,1);//die;
	
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here============================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="product_no*location_id*party_id*product_date*prod_chalan_no*yrn_issu_chalan_no*remarks*updated_by*update_date";
		$data_array="".$txt_production_id."*".$cbo_location_name."*".$cbo_party_name."*".$txt_production_date."*".$txt_prod_chal_no."*".$txt_yarn_issue_challan_no."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		$rID=sql_update("subcon_production_mst",$field_array,$data_array,"id",$update_id,0);
		
		$color_id=return_id( $txt_color, $color_arr, "lib_color", "id,color_name");
		
		$field_array2="job_no*order_id*process*fabric_description*cons_comp_id*gsm*dia_width*no_of_roll*product_qnty*reject_qnty*uom_id*yarn_lot*yrn_count_id*brand*shift*floor_id*machine_id*stitch_len*color_range*color_id*remarks*updated_by*update_date";
		$febric_description=str_replace("(",'[',str_replace(")",']',$txt_febric_description));
		$data_array2="".$txt_job_no."*".$order_no_id."*".$cbo_process."*".$febric_description."*".$hidd_comp_id."*".$txt_gsm."*".$txt_width."*".$txt_roll_qnty."*".$txt_product_qnty."*".$txt_reject_qnty."*".$cbo_uom."*".$txt_yarn_lot."*".$cbo_yarn_count."*".$txt_brand."*".$cbo_shift_id."*".$cbo_floor_id."*".$cbo_machine_name."*".$txt_stitch_len."*".$cbo_color_range."*'".$color_id."'*".$text_new_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		$rID2=sql_update("subcon_production_dtls",$field_array2,$data_array2,"id",$update_id_dtl,1);  
		
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$update_id)."**".str_replace("'",'',$txt_production_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$update_id)."**".str_replace("'",'',$txt_production_id);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$update_id)."**".str_replace("'",'',$txt_production_id);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$update_id)."**".str_replace("'",'',$txt_production_id);
			}
		}	
		disconnect($con);
 		die;
	}
	else if ($operation==2)   // Delete Here ============================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="updated_by*update_date*status_active*is_deleted";
		$data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
		$rID=sql_delete("subcon_production_dtls",$field_array,$data_array,"id","".$update_id_dtl."",1);
			
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id_dtl);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$update_id)."**".str_replace("'",'',$update_id_dtl);
			}
		}
		disconnect($con);
		die;
	}
}
?>