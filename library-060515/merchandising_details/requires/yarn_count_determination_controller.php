<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$permission=$_SESSION['page_permission'];

if ($action=="search_list_view")
{
	$composition_arr=array();
					    $lib_yarn_count=return_library_array( "select yarn_count,id from lib_yarn_count", "id", "yarn_count");
						$arr=array (0=>$item_category, 3=>$color_range,6=>$composition,8=>$lib_yarn_count,9=>$yarn_type);
						$sql="select a.fab_nature_id,a.construction,a.gsm_weight,a.color_range_id,a.stich_length,a.process_loss,b.copmposition_id,b.percent,b.count_id,b.type_id,a.id from  lib_yarn_count_determina_mst a,  lib_yarn_count_determina_dtls b where a.id=b.mst_id and  a.is_deleted=0 order by b.id";
						$data_array=sql_select($sql);
						if (count($data_array)>0)
					    {
							foreach( $data_array as $row )
							{
								if(array_key_exists($row[csf('id')],$composition_arr))
								{
									$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ".$lib_yarn_count[$row[csf('count_id')]]." ".$yarn_type[$row[csf('type_id')]].",";
								}
								else
								{
									$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ".$lib_yarn_count[$row[csf('count_id')]]." ".$yarn_type[$row[csf('type_id')]].",";
								}
							}
						}
						//print_r($composition_arr);
						
												$sql="select a.id,a.fab_nature_id,a.construction,a.gsm_weight,a.color_range_id,a.stich_length,a.process_loss from  lib_yarn_count_determina_mst a,  lib_yarn_count_determina_dtls b where a.id=b.mst_id and  a.is_deleted=0 group by a.id,a.fab_nature_id,a.construction,a.gsm_weight,a.color_range_id,a.stich_length,a.process_loss order by a.id";
						
						$arr=array (0=>$item_category, 3=>$color_range,6=>$composition_arr,8=>$lib_yarn_count,9=>$yarn_type);

						echo  create_list_view ( "list_view", "Fab Nature,Construction,GSM/Weight,Color Range,Stich Length,Process Loss,Composition", "100,100,100,100,90,50,300","950","350",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form'",1, "fab_nature_id,0,0,color_range_id,0,0,id", $arr , "fab_nature_id,construction,gsm_weight,color_range_id,stich_length,process_loss,id", "../merchandising_details/requires/yarn_count_determination_controller",'setFilterGrid("list_view",-1);','0,0,1,0,1,1,0') ;
}
    
if ($action=="load_php_data_to_form")

{
	$nameArray=sql_select( "select fab_nature_id,construction,gsm_weight,color_range_id,stich_length,process_loss,id from  lib_yarn_count_determina_mst where id='$data'" );
	foreach ($nameArray as $inf)
	{
		echo "document.getElementById('cbofabricnature').value  = '".($inf[csf("fab_nature_id")])."';\n";
		echo "document.getElementById('txtconstruction').value = '".($inf[csf("construction")])."';\n";    
		echo "document.getElementById('txtgsmweight').value = '".($inf[csf("gsm_weight")])."';\n";
		echo "document.getElementById('cbocolortype').value = '".($inf[csf("color_range_id")])."';\n";   
		echo "document.getElementById('stichlength').value  = '".($inf[csf("stich_length")])."';\n";
		echo "document.getElementById('processloss').value  = '".($inf[csf("process_loss")])."';\n";
		echo "document.getElementById('update_mst_id').value  = '".($inf[csf("id")])."';\n";
	    echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_yarn_count_determination',1);\n";  
	    echo "show_detail_form('".$inf[csf("id")]."');\n"; 
	}
}

if($action =="show_detail_form")
{
?>
                    <table width="100%" border="0" id="tbl_yarn_count" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" >
                    <thead>
                    	<tr>
                        <th  width="150">Composition</th> <th width="50">%</th> <th width="150">Count</th><th width="150">Type</th> <th width="">  </th> 
                        </tr>
                    </thead>
                    <tbody>
<?
$data_array=sql_select("select id, copmposition_id, percent,count_id,type_id from  lib_yarn_count_determina_dtls where mst_id='$data'");
if ( count($data_array)>0)
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							?>
                            <tr id="yarncost_1" align="center">
                                    <td width="150">
									<?  echo create_drop_down( "cbocompone_".$i, 150, $composition,"", 1, "-- Select --", $row[csf("copmposition_id")], "check_duplicate(".$i.",this.id )",'','' ); ?>
                                    </td>
                                    <td width="50">
                                    <input type="text" id="percentone_<? echo $i; ?>"  name="percentone_<? echo $i; ?>" class="text_boxes" style="width:50px" onChange="sum_percent()"  value="<? echo  $row[csf("percent")]; ?>" />
                                    </td>
                                    <td width="70">
									<? 
									echo create_drop_down( "cbocountcotton_".$i, 150, "select id,yarn_count from  lib_yarn_count where is_deleted=0 and status_active=1 order by yarn_count", "id,yarn_count",1," -- Select Count --",  $row[csf("count_id")], "check_duplicate(".$i.",this.id )",'','' ); 
									?>
                                    </td>
                                    <td width="100">
									<?  
									echo create_drop_down( "cbotypecotton_".$i, 150, $yarn_type,"", 1, "-- Select --", $row[csf("type_id")], "check_duplicate(".$i.",this.id )",'','' ); 
									?>
                                    </td>
                                    <td> 
                                    <input type="button" id="increase_<? echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<? echo $i; ?>)" />
									<input type="button" id="decrease_<? echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<? echo $i; ?>);" />
                                    <input type="hidden" id="updateid_<? echo $i; ?>" name="updateid_<? echo $i; ?>"  class="text_boxes" style="width:20px" value=" <? echo $row[csf("id")]; ?>"  />  
                                    </td>  
                      </tr>
                            <?
						}
						?>
                        </tbody>
                        </table>
                        <?
					}

}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if ($operation==0)  // Insert Here
	{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			//if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
			$id=return_next_id( "id", "lib_yarn_count_determina_mst", 1 ) ;
			$field_array1= "id,fab_nature_id, construction,gsm_weight,color_range_id,stich_length,process_loss,inserted_by,insert_date,status_active,is_deleted";
			$data_array1="(".$id.",".$cbofabricnature.",".$txtconstruction.",".$txtgsmweight.",".$cbocolortype.",".$stichlength.",".$processloss.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
			$id_dtls=return_next_id( "id", "lib_yarn_count_determina_dtls", 1 ) ;
			$field_array2= "id,mst_id, copmposition_id,percent,count_id,type_id,inserted_by,insert_date,status_active,is_deleted";
			for ($i=1;$i<=$total_row;$i++)
		    {
				$cbocompone="cbocompone_".$i;
				$percentone="percentone_".$i;
				$cbocountcotton="cbocountcotton_".$i;
				$cbotypecotton="cbotypecotton_".$i;
				$updateid="updateid_".$i;
				if ($i!=1) $data_array2 .=",";
				$data_array2 .="(".$id_dtls.",".$id.",".$$cbocompone.",".$$percentone.",".$$cbocountcotton.",".$$cbotypecotton.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1',0)";
				$id_dtls=$id_dtls+1;
		    }
			
			//echo "INSERT INTO lib_yarn_count_determina_mst(".$field_array1.") VALUES ".$data_array1;die;
			
			
			$rID=sql_insert("lib_yarn_count_determina_mst",$field_array1,$data_array1,0);
			$rID_1=sql_insert("lib_yarn_count_determina_dtls",$field_array2,$data_array2,1);
			//check_table_status( $_SESSION['menu_id'],0);
			if($db_type==0)
			{
				if($rID && $rID_1){
					mysql_query("COMMIT");  
					echo "0**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			
			if($db_type==2 || $db_type==1 )
			{
				if($rID && $rID_1)
				{
					oci_commit($con);  
					echo "0**".$rID;
				}
			else{
					oci_rollback($con); 
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
	}
	
	else if ($operation==1)   // Update Here
	{
		
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			//if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**1"; die;}
			$field_array1= "fab_nature_id*construction*gsm_weight*color_range_id*stich_length*process_loss*updated_by*update_date*status_active*is_deleted";
			$data_array1="".$cbofabricnature."*".$txtconstruction."*".$txtgsmweight."*".$cbocolortype."*".$stichlength."*".$processloss."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'1'*'0'";
			
			
			
			$rID_de1=execute_query( "delete from lib_yarn_count_determina_dtls where  mst_id =".$update_mst_id."",0);
			
			$id_dtls=return_next_id( "id", "lib_yarn_count_determina_dtls", 1 ) ;
			
			//echo "shajjad";die;
			
			$field_array2= "id,mst_id, copmposition_id,percent,count_id,type_id,inserted_by,insert_date,status_active,is_deleted";
			for ($i=1;$i<=$total_row;$i++)
		    {
				$cbocompone="cbocompone_".$i;
				$percentone="percentone_".$i;
				$cbocountcotton="cbocountcotton_".$i;
				$cbotypecotton="cbotypecotton_".$i;
				$updateid="updateid_".$i;
				if ($i!=1) $data_array2 .=",";
				$data_array2 .="(".$id_dtls.",".$update_mst_id.",".$$cbocompone.",".$$percentone.",".$$cbocountcotton.",".$$cbotypecotton.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1',0)";
				$id_dtls=$id_dtls+1;
		    }
			
			//echo "INSERT INTO lib_yarn_count_determina_mst(".$field_array1.") VALUES ".$data_array1;die;
			
			$rID=sql_update("lib_yarn_count_determina_mst",$field_array1,$data_array1,"id","".$update_mst_id."",0);
			$rID_1=sql_insert("lib_yarn_count_determina_dtls",$field_array2,$data_array2,1);
			
			
			
			//check_table_status( $_SESSION['menu_id'],0);
			if($db_type==0)
			{
				if($rID && $rID_1){
					mysql_query("COMMIT");  
					echo "1**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
				
				if($rID && $rID_1)
				{
					oci_commit($con);  
					echo "1**".$rID;
				}
				else{
					oci_rollback($con); 
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		//}
		
	}
	
	
	
	else if ($operation==2) // Delete Here
	{
		
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$field_array1="updated_by*update_date*status_active*is_deleted";
			$data_array1="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
			$rID=sql_delete("lib_yarn_count_determina_mst",$field_array1,$data_array1,"id","".$update_mst_id."",1);
			$field_array2="updated_by*update_date*status_active*is_deleted";
			$data_array2="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
			$rID1=sql_delete("lib_yarn_count_determina_dtls",$field_array2,$data_array2,"mst_id","".$update_mst_id."",1);
			
			if($db_type==0)
			{
				if($rID && $rID1 ){
					mysql_query("COMMIT");  
					echo "2**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID && $rID1 )
				{
					oci_commit($con);   
					echo "2**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
	    }
	}
	

?>