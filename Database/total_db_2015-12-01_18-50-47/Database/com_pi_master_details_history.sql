-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_pi_master_details_history`
--

DROP TABLE IF EXISTS `com_pi_master_details_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_pi_master_details_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL,
  `approved_no` int(11) NOT NULL,
  `item_category_id` int(11) NOT NULL DEFAULT '0',
  `importer_id` int(11) NOT NULL DEFAULT '0',
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `pi_number` varchar(30) NOT NULL,
  `pi_date` date NOT NULL DEFAULT '0000-00-00',
  `last_shipment_date` date NOT NULL DEFAULT '0000-00-00',
  `pi_validity_date` date NOT NULL DEFAULT '0000-00-00',
  `currency_id` tinyint(3) NOT NULL DEFAULT '0',
  `source` int(11) NOT NULL DEFAULT '0',
  `hs_code` varchar(30) NOT NULL,
  `internal_file_no` int(11) NOT NULL DEFAULT '0',
  `intendor_name` int(11) NOT NULL DEFAULT '0',
  `pi_basis_id` int(1) NOT NULL DEFAULT '0',
  `remarks` varchar(500) NOT NULL,
  `total_amount` double NOT NULL DEFAULT '0',
  `upcharge` double NOT NULL DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `net_total_amount` double NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:Unapproved; 1:Approved',
  `approved_by` int(11) NOT NULL DEFAULT '0',
  `approved_date` datetime NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active 0:inactive',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Yes 0:No',
  PRIMARY KEY (`id`),
  KEY `item_category_id` (`item_category_id`),
  KEY `importer_id` (`importer_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `pi_number` (`pi_number`),
  KEY `pi_date` (`pi_date`),
  KEY `last_shipment_date` (`last_shipment_date`),
  KEY `pi_validity_date` (`pi_validity_date`),
  KEY `source` (`source`),
  KEY `hs_code` (`hs_code`),
  KEY `internal_file_no` (`internal_file_no`),
  KEY `approved` (`approved`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_pi_master_details_history`
--

LOCK TABLES `com_pi_master_details_history` WRITE;
/*!40000 ALTER TABLE `com_pi_master_details_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_pi_master_details_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
