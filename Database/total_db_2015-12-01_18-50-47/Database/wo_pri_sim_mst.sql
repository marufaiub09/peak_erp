-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pri_sim_mst`
--

DROP TABLE IF EXISTS `wo_pri_sim_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pri_sim_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `style_ref` varchar(75) NOT NULL,
  `revised_no` int(11) NOT NULL DEFAULT '0',
  `pord_dept` int(11) NOT NULL DEFAULT '0',
  `product_code` varchar(15) NOT NULL,
  `style_desc` varchar(100) NOT NULL,
  `currency` int(11) NOT NULL DEFAULT '0',
  `agent` int(11) NOT NULL DEFAULT '0',
  `offer_qnty` int(11) NOT NULL DEFAULT '0',
  `region` int(11) NOT NULL DEFAULT '0',
  `color_range` int(11) NOT NULL DEFAULT '0',
  `incoterm` int(11) NOT NULL DEFAULT '0',
  `incoterm_place` varchar(100) NOT NULL,
  `machine_line` varchar(100) NOT NULL,
  `prod_line_hr` varchar(100) NOT NULL,
  `fabric_source` int(11) NOT NULL DEFAULT '0',
  `costing_per` int(11) NOT NULL DEFAULT '0',
  `quot_date` date NOT NULL,
  `est_ship_date` date NOT NULL,
  `op_date` date NOT NULL,
  `factory` varchar(100) NOT NULL,
  `remarks` varchar(500) NOT NULL,
  `garments_nature` int(11) NOT NULL DEFAULT '0',
  `order_uom` int(11) NOT NULL DEFAULT '1',
  `gmts_item_id` varchar(100) NOT NULL,
  `set_break_down` varchar(100) NOT NULL,
  `total_set_qnty` int(11) NOT NULL DEFAULT '0',
  `cm_cost_predefined_method_id` int(11) NOT NULL DEFAULT '0',
  `exchange_rate` double NOT NULL,
  `sew_smv` double NOT NULL,
  `cut_smv` double NOT NULL,
  `sew_effi_percent` double NOT NULL,
  `cut_effi_percent` double NOT NULL,
  `efficiency_wastage_percent` double NOT NULL,
  `season` varchar(50) NOT NULL,
  `m_list` varchar(300) NOT NULL,
  `bh_merchant` varchar(300) NOT NULL,
  `quotation_status` int(2) NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `approved_by` int(11) NOT NULL DEFAULT '0',
  `approved_date` date NOT NULL,
  `fper` double NOT NULL,
  `yper` double NOT NULL,
  `cper` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `buyer_id` (`buyer_id`),
  KEY `revised_no` (`revised_no`),
  KEY `pord_dept` (`pord_dept`),
  KEY `quot_date` (`quot_date`),
  KEY `est_ship_date` (`est_ship_date`),
  KEY `approved` (`approved`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pri_sim_mst`
--

LOCK TABLES `wo_pri_sim_mst` WRITE;
/*!40000 ALTER TABLE `wo_pri_sim_mst` DISABLE KEYS */;
/*!40000 ALTER TABLE `wo_pri_sim_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:47
