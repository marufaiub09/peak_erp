<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');


$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id=$_SESSION['logic_erp']['user_id'];
$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');


if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	
	$datediff_n = datediff( 'd', $txt_date_from, $txt_date_to);
    $date_arr=array();       
	for($k=0; $k<$datediff_n; $k++)
	{
		$newdate_n=add_date(str_replace("'","",$txt_date_from),$k);
		$yare_month=date("Y-m",strtotime($newdate_n));
		$date_arr[$yare_month]=$yare_month;
	}
	ob_start();
?>
<div style="width:1450px;" id="scroll_body">
<fieldset style="width:100%">
    <table width="1440" cellpadding="0" cellspacing="0" id="caption" align="left">
        <tr>
            <td align="center" width="100%" colspan="16" class="form_caption" ><strong style="font-size:18px">Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
        </tr> 
        <tr>  
            <td align="center" width="100%" colspan="16" class="form_caption" ><strong style="font-size:18px"><?php echo $report_title; ?></strong></td>
        </tr> 
    </table>
    
    
    <table width="1440" cellpadding="0" cellspacing="0" class="rpt_table" border="1" rules="all" id="" align="left">
    	<thead>
        	<tr>
            	<th width="70" rowspan="2">Month</th>
            	<th width="100" rowspan="2">Contract Value</th>
                <th width="100" rowspan="2">Export Lc</th>
                <th colspan="3">Lien</th>
                <th colspan="5">Export</th>
                <th colspan="5">Import</th>
            </tr>
            <tr>
                <th width="90">SC </th>
                <th width="90">LC </th>
                <th width="90">Total </th>
                <th width="90">Ex-Factory Qnty (Invoice)</th>
                <th width="90">Ex-Factory Value (Invoice)</th>
                <th width="90">Bank Submit (Collection)</th>
                <th width="90">Bank Submit (Purchess)</th>
                <th width="90">Realization</th>
                <th width="90">BTB Value</th>
                <th width="90">Company Acceptance</th>
                <th width="90">Bank Acceptance</th>
                <th width="90">Maturity Value</th>
                <th >Payment Value</th>
            </tr>
        </thead>
        <tbody>
        <?php
		$cbo_month_from=str_pad($cbo_month_from,2,"0",STR_PAD_LEFT);
		//$export_sc=return_field_value("$group_concat(distinct doc_submission_mst_id) as id","com_export_doc_submission_invo","is_lc=1 and lc_sc_id in(".implode(',',$lc_id_arr).")","id");
		if($db_type==2)
		{

			$sql_sc=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as sc_id, to_char(contract_date,'YYYY-MM') as date_sc, sum(contract_value) as contract_value,sum(case when lien_bank!=0 then contract_value else 0 end) as lien_contract_val from com_sales_contract where status_active=1 and is_deleted=0 and beneficiary_name='$cbo_company_name' and contract_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'  group by to_char(contract_date,'YYYY-MM')");
			
			$sql_lc=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as lc_id, to_char(lc_date,'YYYY-MM') as date_lc, sum(lc_value) as lc_value, to_char(lc_date,'YYYY-MM') as date_lc, sum(case when lien_bank!=0 then lc_value else 0 end) as lien_lc_val from com_export_lc where status_active=1 and is_deleted=0 and beneficiary_name='$cbo_company_name' and lc_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'  group by to_char(lc_date,'YYYY-MM')");
			
			$sql_invoice=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as invoice_id, to_char(ex_factory_date,'YYYY-MM') as date_invoice, sum(invoice_quantity) as invoice_quantity, sum(invoice_value) as invoice_value, sum(net_invo_value) as net_invo_value from com_export_invoice_ship_mst where status_active=1 and is_deleted=0 and benificiary_id='$cbo_company_name' and ex_factory_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' and ex_factory_date is not null  group by   to_char(ex_factory_date,'YYYY-MM')");
			
			
			
			$sql_submission=sql_select("select LISTAGG(CAST(a.id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.id) as submission_id, to_char(a.submit_date,'YYYY-MM') as date_sub, sum(case when a.submit_type=1 then b.net_invo_value  else 0 end) as sub_collection, sum(case when a.submit_type=2 then b.net_invo_value  else 0 end) as sub_purchase  from com_export_doc_submission_mst a,  com_export_doc_submission_invo b where a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id=b.doc_submission_mst_id and submit_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' and ENTRY_FORM=40  group by to_char(a.submit_date,'YYYY-MM')");
			//echo "select LISTAGG(CAST(a.id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.id) as rlz_id, to_char(a.received_date,'YYYY-MM') as date_rlz, sum(b.document_currency) as document_currency from  com_export_proceed_realization a, com_export_proceed_rlzn_dtls b where a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id=b.mst_id and a.received_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'  group by to_char(a.received_date,'YYYY-MM')";die;
			$sql_realization=sql_select("select LISTAGG(CAST(a.id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.id) as rlz_id, to_char(a.received_date,'YYYY-MM') as date_rlz, sum(b.document_currency) as document_currency from  com_export_proceed_realization a, com_export_proceed_rlzn_dtls b where a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id=b.mst_id and a.received_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'  group by to_char(a.received_date,'YYYY-MM')");
			
			$btb_lc=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as btb_lc_id, to_char(lc_date,'YYYY-MM') as date_btb, sum(lc_value) as btb_lc_value from com_btb_lc_master_details where status_active=1 and is_deleted=0 and lc_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'  group by to_char(lc_date,'YYYY-MM')");
			
			/*$sql_acceptance=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as btb_inv_id, to_char(invoice_date,'YYYY-MM') as date_accp, sum(case when company_acc_date is not null then document_value else 0 end) as company_accp_value,sum(case when bank_acc_date is not null then document_value else 0 end) as bank_accp_value,sum(case when company_acc_date is not null then document_value else 0 end) as company_accp_value,sum(case when maturity_date is not null then document_value else 0 end) as matured_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and invoice_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'  group by to_char(invoice_date,'YYYY-MM')");*/
			
			$company_acceptance=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as btb_inv_id, to_char(company_acc_date,'YYYY-MM') as date_company_accp, sum(document_value) as company_accp_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and company_acc_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' and company_acc_date is not null  group by to_char(company_acc_date,'YYYY-MM')");
			
			
			$bank_acceptance=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as btb_inv_id, to_char(bank_acc_date,'YYYY-MM') as date_bank_accp, sum(document_value) as bank_accp_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and bank_acc_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' and bank_acc_date is not null group by to_char(bank_acc_date,'YYYY-MM')");
			
			$maturity_acceptance=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as btb_inv_id, to_char(maturity_date,'YYYY-MM') as date_maturity_accp,sum(document_value) as matured_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and maturity_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' and maturity_date is not null  group by to_char(maturity_date,'YYYY-MM')");
			
			$sql_payment=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as pay_id, to_char(payment_date,'YYYY-MM') as date_pay, sum(accepted_ammount) as accepted_ammount from  com_import_payment where status_active=1 and is_deleted=0 and payment_date between  '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' group by to_char(payment_date,'YYYY-MM')");
		}
		else if($db_type==0)
		{
			$sql_sc=sql_select("select group_concat(id) as sc_id, date_format(contract_date,'%Y-%m') as date_sc, sum(contract_value) as contract_value,sum(case when lien_bank!=0 then contract_value else 0 end) as lien_contract_val from com_sales_contract where status_active=1 and is_deleted=0 and beneficiary_name='$cbo_company_name' and contract_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'  group by date_format(contract_date,'%Y-%m')");
			
			$sql_lc=sql_select("select group_concat(id) as lc_id, date_format(lc_date,'%Y-%m') as date_lc, sum(lc_value) as lc_value,sum(case when lien_bank!=0 then lc_value else 0 end) as lien_lc_val from com_export_lc where status_active=1 and is_deleted=0 and beneficiary_name='$cbo_company_name' and lc_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'  group by date_format(lc_date,'%Y-%m')");
			
			

			$sql_invoice=sql_select("select group_concat(id) as invoice_id, date_format(ex_factory_date,'%Y-%m') as date_invoice, sum(invoice_quantity) as invoice_quantity, sum(invoice_value) as invoice_value , sum(net_invo_value) as net_invo_value from com_export_invoice_ship_mst where status_active=1 and is_deleted=0 and benificiary_id='$cbo_company_name' and ex_factory_date!='0000-00-00' and ex_factory_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'  group by date_format(ex_factory_date,'%Y-%m')");
			
			
			$sql_submission=sql_select("select group_concat(a.id) as submission_id, date_format(submit_date,'%Y-%m') as date_sub, sum(case when a.submit_type=1 then b.net_invo_value  else 0 end) as sub_collection, sum(case when a.submit_type=2 then b.net_invo_value  else 0 end) as sub_purchase  from com_export_doc_submission_mst a,  com_export_doc_submission_invo b where a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id=b.doc_submission_mst_id and submit_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."' and ENTRY_FORM=40  group by date_format(submit_date,'%Y-%m')");
			
			$sql_realization=sql_select("select group_concat(a.id) as rlz_id, date_format(received_date,'%Y-%m') as date_rlz, sum(b.document_currency) as document_currency from  com_export_proceed_realization a, com_export_proceed_rlzn_dtls b where a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id=b.mst_id and a.received_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'  group by date_format(received_date,'%Y-%m')");
			
			$btb_lc=sql_select("select group_concat(id) as btb_lc_id, date_format(lc_date,'%Y-%m') as date_btb, sum(lc_value) as btb_lc_value from com_btb_lc_master_details where status_active=1 and is_deleted=0 and lc_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'  group by date_format(lc_date,'%Y-%m')");
			
			/*$sql_acceptance=sql_select("select group_concat(id) as btb_inv_id, date_format(invoice_date,'%Y-%m') as date_accp, sum(case when company_acc_date!='0000-00-00' then document_value else 0 end) as company_accp_value,sum(case when bank_acc_date!='0000-00-00' then document_value else 0 end) as bank_accp_value,sum(case when company_acc_date!='0000-00-00' then document_value else 0 end) as company_accp_value,sum(case when maturity_date!='0000-00-00' then document_value else 0 end) as matured_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and invoice_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'  group by date_format(invoice_date,'%Y-%m')");*/
			
			$company_acceptance=sql_select("select group_concat(id) as btb_inv_id,  date_format(company_acc_date,'%Y-%m') as date_company_accp, sum(document_value) as company_accp_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and company_acc_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."' and company_acc_date!='0000-00-00' group by date_format(company_acc_date,'%Y-%m')");
			
			
			$bank_acceptance=sql_select("select group_concat(id) as btb_inv_id,  date_format(bank_acc_date,'%Y-%m') as date_bank_accp, sum(document_value) as bank_accp_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and bank_acc_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."' and bank_acc_date!='0000-00-00' group by date_format(bank_acc_date,'%Y-%m')");
			
			$maturity_acceptance=sql_select("select group_concat(id) as btb_inv_id,  date_format(maturity_date,'%Y-%m') as date_maturity_accp,sum(document_value) as matured_value from com_import_invoice_mst where status_active=1 and is_deleted=0 and maturity_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."' and maturity_date!='0000-00-00'  group by date_format(maturity_date,'%Y-%m')");
			
			$sql_payment=sql_select("select group_concat(id) as pay_id, date_format(payment_date,'%Y-%m') as date_pay, sum(accepted_ammount) as accepted_ammount from  com_import_payment where status_active=1 and is_deleted=0 and payment_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'  group by date_format(payment_date,'%Y-%m')");
			
		}
		
		foreach($sql_sc as $row)
		{
			$sc_data_arr[$row[csf("date_sc")]]["sc_id"]=$row[csf("sc_id")];
			$sc_data_arr[$row[csf("date_sc")]]["contract_value"]=$row[csf("contract_value")];
			$sc_data_arr[$row[csf("date_sc")]]["lien_contract_val"]=$row[csf("lien_contract_val")];
		}
		
		foreach($sql_lc as $row)
		{
			$lc_data_arr[$row[csf("date_lc")]]["lc_id"]=$row[csf("lc_id")];
			$lc_data_arr[$row[csf("date_lc")]]["lc_value"]=$row[csf("lc_value")];
			$lc_data_arr[$row[csf("date_lc")]]["lien_lc_val"]=$row[csf("lien_lc_val")];
		}
		
		foreach($sql_invoice as $row)
		{
			$invoice_data_arr[$row[csf("date_invoice")]]["invoice_id"]=$row[csf("invoice_id")];
			$invoice_data_arr[$row[csf("date_invoice")]]["invoice_quantity"]=$row[csf("invoice_quantity")];
			$invoice_data_arr[$row[csf("date_invoice")]]["invoice_value"]=$row[csf("invoice_value")];
			$invoice_data_arr[$row[csf("date_invoice")]]["net_invo_value"]=$row[csf("net_invo_value")];
		}
		
		foreach($sql_submission as $row)
		{
			$sub_data_arr[$row[csf("date_sub")]]["submission_id"]=$row[csf("submission_id")];
			$sub_data_arr[$row[csf("date_sub")]]["sub_collection"]=$row[csf("sub_collection")];
			$sub_data_arr[$row[csf("date_sub")]]["sub_purchase"]=$row[csf("sub_purchase")];
		}
		
		foreach($sql_realization as $row)
		{
			$rlz_data_arr[$row[csf("date_rlz")]]["rlz_id"]=$row[csf("rlz_id")];
			$rlz_data_arr[$row[csf("date_rlz")]]["document_currency"]=$row[csf("document_currency")];
		}
		
		foreach($btb_lc as $row)
		{
			$btb_data_arr[$row[csf("date_btb")]]["btb_lc_id"]=$row[csf("btb_lc_id")];
			$btb_data_arr[$row[csf("date_btb")]]["btb_lc_value"]=$row[csf("btb_lc_value")];
		}
		
		foreach($company_acceptance as $row)
		{
			$com_accep_data_arr[$row[csf("date_company_accp")]]["btb_inv_id"] =$row[csf("btb_inv_id")];
			$com_accep_data_arr[$row[csf("date_company_accp")]]["company_accp_value"] =$row[csf("company_accp_value")];
		}
		
		foreach($bank_acceptance as $row)
		{
			$bank_accep_data_arr[$row[csf("date_bank_accp")]]["btb_inv_id"] =$row[csf("btb_inv_id")];
			$bank_accep_data_arr[$row[csf("date_bank_accp")]]["bank_accp_value"] =$row[csf("bank_accp_value")];
		}
		
		foreach($maturity_acceptance as $row)
		{
			$mature_accep_data_arr[$row[csf("date_maturity_accp")]]["btb_inv_id"] =$row[csf("btb_inv_id")];
			$mature_accep_data_arr[$row[csf("date_maturity_accp")]]["matured_value"] =$row[csf("matured_value")];
		}
		
		foreach($sql_payment as $row)
		{
			$pay_data_arr[$row[csf("date_pay")]]["pay_id"]=$row[csf("pay_id")];
			$pay_data_arr[$row[csf("date_pay")]]["accepted_ammount"]=$row[csf("accepted_ammount")];
		}
		
		$k=1;
		foreach($date_arr as $year_month)
		{
			if ($k%2==0)
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			?>
        	<tr bgcolor="<?php echo $bgcolor; ?>">
            	<td><?php $month_id=explode("-",$year_month); echo $months[(int)$month_id[1]];?></td>
            	<td align="right"><?php echo number_format($sc_data_arr[$year_month]["contract_value"],2); $total_sc +=$sc_data_arr[$year_month]["contract_value"];?></td>
                <td align="right"><?php echo number_format($lc_data_arr[$year_month]["lc_value"],2); $total_lc +=$lc_data_arr[$year_month]["lc_value"]; ?></td>
                <td align="right"><?php echo number_format($sc_data_arr[$year_month]["lien_contract_val"],2); $total_lein_sc +=$sc_data_arr[$year_month]["lien_contract_val"]; ?></td>
                <td align="right"><?php echo number_format($lc_data_arr[$year_month]["lien_lc_val"],2); $total_lein_lc +=$lc_data_arr[$year_month]["lien_lc_val"]; ?></td>
                <td align="right"><?php $total_lein=$sc_data_arr[$year_month]["lien_contract_val"]+$lc_data_arr[$year_month]["lien_lc_val"]; echo number_format($total_lein,2); $total_lein_sc_lc +=$total_lein; ?></td>
                <td align="right"><?php echo number_format($invoice_data_arr[$year_month]["invoice_quantity"],0); $total_inv_qnty +=$invoice_data_arr[$year_month]["invoice_quantity"]; ?></td>
                <td align="right"><?php echo number_format($invoice_data_arr[$year_month]["net_invo_value"],2); $total_inv_val +=$invoice_data_arr[$year_month]["invoice_value"]; ?></td>
                <td align="right"><?php echo number_format($sub_data_arr[$year_month]["sub_collection"],2); $total_sub_collection +=$sub_data_arr[$year_month]["sub_collection"]; ?></td>
                <td align="right"><?php echo number_format($sub_data_arr[$year_month]["sub_purchase"],2); $total_sub_purchase +=$sub_data_arr[$year_month]["sub_purchase"]; ?></td>
                <td align="right"><?php echo number_format($rlz_data_arr[$year_month]["document_currency"],2); $total_rlz +=$rlz_data_arr[$year_month]["document_currency"]; ?></td>
                <td align="right"><?php echo number_format($btb_data_arr[$year_month]["btb_lc_value"],2); $total_btb +=$btb_data_arr[$year_month]["btb_lc_value"]; ?></td>
                <td align="right"><?php echo number_format($com_accep_data_arr[$year_month]["company_accp_value"] ,2); $total_accp_company +=$com_accep_data_arr[$year_month]["company_accp_value"] ; ?></td>
                <td align="right"><?php echo number_format($bank_accep_data_arr[$year_month]["bank_accp_value"],2); $total_accp_bank +=$bank_accep_data_arr[$year_month]["bank_accp_value"];?></td>
                <td align="right"><?php echo number_format($mature_accep_data_arr[$year_month]["matured_value"],2); $total_accp_matured +=$mature_accep_data_arr[$year_month]["matured_value"]; ?></td>
                <td align="right"><?php echo number_format($pay_data_arr[$year_month]["accepted_ammount"],2); $total_pay +=$pay_data_arr[$year_month]["accepted_ammount"];?></td>
            </tr>
            <?php
			$k++;
		}
		?>
        
        </tbody>
        <tfoot>
        	<th></th>
            <th align="right"><?php echo number_format($total_sc,2); ?></th>
            <th align="right"><?php echo number_format($total_lc,2); ?></th>
            <th align="right"><?php echo number_format($total_lein_sc,2); ?></th>
            <th align="right"><?php echo number_format($total_lein_lc,2); ?></th>
            <th align="right"><?php echo number_format($total_lein_sc_lc,2); ?></th>
            <th align="right"><?php echo number_format($total_inv_qnty,0); ?></th>
            <th align="right"><?php echo number_format($total_inv_val,2); ?></th>
            <th align="right"><?php echo number_format($total_sub_collection,2); ?></th>
            <th align="right"><?php echo number_format($total_sub_purchase,2); ?></th>
            <th align="right"><?php echo number_format($total_rlz,2); ?></th>
            <th align="right"><?php echo number_format($total_btb,2); ?></th>
            <th align="right"><?php echo number_format($total_accp_company,2); ?></th>
            <th align="right"><?php echo number_format($total_accp_bank,2); ?></th>
            <th align="right"><?php echo number_format($total_accp_matured,2); ?></th>
            <th align="right"><?php echo number_format($total_pay,2); ?></th>
        </tfoot>
        
        
    </table>
    
    
</fieldset>
</div>
<?php
foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
}


?>
