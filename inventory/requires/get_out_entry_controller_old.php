﻿<?
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
//system id popup here----------------------// 
if ($action=="system_id_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		$("#hidden_sys_number").val(str);
		parent.emailwindow.hide(); 
	}
</script>
</head>
<body>
    <div align="center" style="width:100%;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="150">Sample</th>
                <th width="150" align="center" id="search_by_td_up">Item Catagory</th>
                <th width="200">Date Range</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
                    	<? 
							echo create_drop_down( "cbo_sample", 150, "select id,sample_name from lib_sample where status_active=1 order by sample_name","id,sample_name",1, "-- Select --", 0, "" ); 
						?> 
                    </td>
                    <td width="250" align="center" id="search_by_td">				
						<?
							echo create_drop_down( "cbo_item_category", 140, $item_category,"", 1, "--- Select ---", $selected,0 ,"");
						?>                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                       <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_sample').value+'_'+document.getElementById('cbo_item_category').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<? echo $company; ?>, 'create_sys_search_list_view', 'search_div', 'get_out_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
                </tr>
                <tr>                  
                    <td align="center" height="40" valign="middle" colspan="4">
                        <? echo load_month_buttons(1);  ?>
                        <!-- Hidden field here-------->
                       <!-- input type="hidden" id="hidden_tbl_id" value="" ---->
                       
                        <input type="hidden" id="hidden_sys_number" value="hidden_sys_number" />
                        <!-- ---------END-------------> 
                    </td>
                </tr>    
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
    </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}

if($action=="create_sys_search_list_view")
{
	$ex_data = explode("_",$data);
	$sample = $ex_data[0];
	$item_catagory = $ex_data[1];
	$fromDate = $ex_data[2];
	$toDate = $ex_data[3];
	$company = $ex_data[4];
 	//$sql_cond="";
	if( $sample!=0 )  $sample=" and sample_id='$sample'"; else  $sample="";
	if( $item_catagory!=0 )  $item_catagory=" and item_category_id='$item_catagory'"; else  $item_catagory="";
	 if($db_type==0)
	 {
	if( $fromDate!=0 && $toDate!=0 ) $sql_cond= " and out_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
	 }
	 if($db_type==2)
	 {
	if( $fromDate!=0 && $toDate!=0 ) $sql_cond= " and out_date  between '".change_date_format($fromDate,'mm-dd-yyyy','/',1)."' and '".change_date_format($toDate,'mm-dd-yyyy','/',1)."'";
	 }
	if( $company!=0 )  $company=" and company_id='$company'"; else  $company="";
	
	 if($db_type==0) $grp_field=" group by id";
	 if($db_type==2) $grp_field="group by id,sys_number_prefix_num, sys_number, sample_id, item_category_id, sent_by, sent_to, out_date,challan_no,currency_id ";
	  else $grp_field="";
	
	$sql = "select sys_number_prefix_num, sys_number, sample_id, item_category_id, sent_by, sent_to, out_date,challan_no,currency_id from  inv_gate_out_mst	where status_active=1 and is_deleted=0 $sample $item_catagory $company $sql_cond $grp_field";
	//echo $sql;
	//echo $sql_cond; die;
	$sample_arr = return_library_array( "select id, sample_name from lib_sample",'id','sample_name');
	$arr=array(1=>$sample_arr,2=>$item_category,7=>$currency);
	echo create_list_view("list_view", "Gate Out No,Sample Name,Item Catagory,Sent By,Sent To,Out Date,Challan No,Currency","70,150,110,100,100,80,70,70","800","260",0, $sql , "js_set_value", "sys_number", "", 1, "0,sample_id,item_category_id,0,0,0,0,currency_id", $arr, "sys_number_prefix_num,sample_id,item_category_id,sent_by,sent_to,out_date,challan_no,currency_id", "",'','') ;	
	exit();
	
}


if($action=="populate_master_from_data")
{
	$sql="select sys_number,company_id,sample_id,item_category_id,sent_by,sent_to,out_date,challan_no,currency_id,gate_pass_no,time_hour,time_minute from inv_gate_out_mst where sys_number='$data'";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{	
		echo "$('#cbo_sample').val(".$row[csf("sample_id")].");\n";
		echo "$('#cbo_item_category').val(".$row[csf("item_category_id")].");\n"; 		
		//echo "$('#hidden_type').val(".$row[csf("piworeq_type")].");\n";
		echo "$('#txt_sent_by').val('".$row[csf("sent_by")]."');\n";
		echo "$('#txt_sent_to').val('".$row[csf("sent_to")]."');\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("out_date")])."');\n";	
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_currency').val('".$row[csf("currency_id")]."');\n";	
		echo "$('#txt_gate_pass_no').val(".$row[csf("gate_pass_no")].");\n";
		echo "$('#txt_start_hours').val(".$row[csf("time_hour")].");\n";	
		echo "$('#txt_start_minuties').val(".$row[csf("time_minute")].");\n"; 		  	
		//right side list view 
		//echo "show_list_view(".$row[csf("piworeq_type")]."+'**'+".$row[csf("pi_wo_req_id")].",'show_product_listview','list_product_container','requires/get_out_entry_controller','');\n";
	}
	exit();	
}


//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN");}
				 
		if(str_replace("'","",$txt_system_id)!="") //update
		{
			$id= return_field_value("id","inv_gate_out_mst","sys_number=$txt_system_id");//check sys id for update or insert	
			$field_array="sample_id*item_category_id*sent_by*sent_to*out_date*challan_no*currency_id*gate_pass_no*time_hour*time_minute*updated_by*update_date*status_active*is_deleted";
			$data_array="".$cbo_sample."*".$cbo_item_category."*".$txt_sent_by."*".$txt_sent_to."*".$txt_receive_date."*".$txt_challan_no."*".$cbo_currency."*".$txt_gate_pass_no."*".$txt_start_hours."*".$txt_start_minuties."*'".$user_id."'*'".$pc_date_time."'*1*0";
			//$rID=sql_update("inv_gate_out_mst",$field_array,$data_array,"id",$id,1);	
			$return_no=str_replace("'",'',$txt_system_id);
		}
		else // new insert
 		{			
			$id=return_next_id("id", "inv_gate_out_mst", 1);			
			// inv_gate_out_mst master table entry here START---------------------------------------//	
			if($db_type==2) 
			{
				//echo "select sys_number_prefix,sys_number_prefix_num from inv_gate_out_mst where company_id=$cbo_company_name and and TO_CHAR(insert_date,'YYYY')=".date('Y',time())." order by id DESC";	
			$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GOE', date("Y",time()), 5, "select sys_number_prefix,sys_number_prefix_num from inv_gate_out_mst where company_id=$cbo_company_name and  TO_CHAR(insert_date,'YYYY')=".date('Y',time())." order by id DESC ", "sys_number_prefix", "sys_number_prefix_num" ));
			}
			if($db_type==0)
			{	
			$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GOE', date("Y",time()), 5, "select sys_number_prefix,sys_number_prefix_num from inv_gate_out_mst where company_id=$cbo_company_name and YEAR(insert_date)=".date('Y',time())." order by id DESC ", "sys_number_prefix", "sys_number_prefix_num" ));
			}
			$field_array="id,sys_number_prefix,sys_number_prefix_num,sys_number,company_id,sample_id,item_category_id,sent_by,sent_to,out_date,challan_no,currency_id,gate_pass_no,time_hour,time_minute,inserted_by,insert_date,status_active,is_deleted";
			$data_array="(".$id.",'".$new_sys_number[1]."','".$new_sys_number[2]."','".$new_sys_number[0]."',".$cbo_company_name.",".$cbo_sample.",".$cbo_item_category.",".$txt_sent_by.",".$txt_sent_to.",".$txt_receive_date.",".$txt_challan_no.",".$cbo_currency.",".$txt_gate_pass_no.",".$txt_start_hours.",".$txt_start_minuties.",'".$user_id."','".$pc_date_time."',1,0)";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_insert("inv_gate_out_mst",$field_array,$data_array,0); 		
			// inv_gate_in_mst master table entry here END---------------------------------------// 
			$return_no=str_replace("'",'',$new_sys_number[0]);
		}		
 					
		//inv_gate_out_dtls details table entry here START---------------------------------------//		
		$dtlsid=return_next_id("id", "inv_gate_out_dtls", 1);		
  		$field_array2="id,mst_id,item_description,quantity,uom,rate,amount,remarks,inserted_by,insert_date,status_active,is_deleted";
		$data_array2="(".$dtlsid.",".$id.",".$txt_item_description.",".$txt_quantity.",".$cbo_uom.",".$txt_rate.",".$txt_amount.",".$txt_remarks.",'".$user_id."','".$pc_date_time."',1,0)";
 		//echo $field_array."<br>".$data_array;die;
		//echo "0**"."INSERT INTO inv_gate_out_dtls (".$field_array.") VALUES ".$data_array;die;
 		//$dtlsrID=sql_insert("inv_gate_out_dtls",$field_array,$data_array,1); 		
		//inv_gate_in_dtls details table entry here END---------------------------------------// 
		
			 
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		//Test for insert/update
		if(str_replace("'","",$txt_system_id)!="") //update
		{
			$rID=sql_update("inv_gate_out_mst",$field_array,$data_array,"id",$id,1);	
		}
		else
		{
		$rID=sql_insert("inv_gate_out_mst",$field_array,$data_array,0); 		
		}
		$dtlsrID=sql_insert("inv_gate_out_dtls",$field_array2,$data_array2,1); 
		
		if($db_type==0)
			{	
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "0**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
		}
		else
		{
			mysql_query("ROLLBACK"); 
			//echo "10**";
			echo "10**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
		}
			}
			
			if($db_type==2)
			{	
		if($rID && $dtlsrID)
		{
			oci_commit($con);
			echo "0**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
		}
		else
		{
			
			oci_rollback($con);
			echo "10**"."**".str_replace("'",'',$return_no)."**".str_replace("'",'',$id);
		}
			}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		/*check_table_status( $_SESSION['menu_id'],0);
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}*/
		
		//check update id
		if( str_replace("'","",$txt_system_id) == "")
		{
			echo "15";exit(); 
		}
		//inv_gate_in_mst master table UPDATE here START----------------------//	
			$field_array="sample_id*item_category_id*sent_by*sent_to*out_date*challan_no*currency_id*gate_pass_no*time_hour*time_minute*updated_by*update_date*status_active*is_deleted";
			$data_array="".$cbo_sample."*".$cbo_item_category."*".$txt_sent_by."*".$txt_sent_to."*".$txt_receive_date."*".$txt_challan_no."*".$cbo_currency."*".$txt_gate_pass_no."*".$txt_start_hours."*".$txt_start_minuties."*'".$user_id."'*'".$pc_date_time."'*1*0";
 		//echo $field_array."<br>".$data_array;die;
 		//$rID=sql_update("inv_gate_out_mst",$field_array,$data_array,"sys_number",$txt_system_id,1);	
		//inv_gate_in_mst master table UPDATE here END---------------------------------------// 
		
		//inv_gate_in_dtls details table UPDATE here START-----------------------------------//			
 		$field_array2 = "item_description*quantity*uom*rate*amount*remarks*updated_by*update_date*status_active*is_deleted";
 		$data_array2 = "".$txt_item_description."*".$txt_quantity."*".$cbo_uom."*".$txt_rate."*".$txt_amount."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'*1*0";
		//echo $field_array."<br>".$data_array;die;
		$rID=sql_update("inv_gate_out_mst",$field_array,$data_array,"sys_number",$txt_system_id,1);	
 		$dtlsrID = sql_update("inv_gate_out_dtls",$field_array2,$data_array2,"id",$update_id,1); 
		//inv_gate_in_dtls details table UPDATE here END-----------------------------------//
 		
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
			{	
		if($rID && $dtlsrID)
		{
			mysql_query("COMMIT");  
			echo "1**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$id);
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$id);
		}
			}
			if($db_type==2)
			{	
		if($rID && $dtlsrID)
		{
			oci_commit($con);
			echo "1**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$id);
		}
		else
		{
			oci_rollback($con);
			echo "10**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$id);
		}
			}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		// master table delete here---------------------------------------
		$id=return_next_id("id", "inv_gate_out_mst", 1);
		$mst_id = return_field_value("id","inv_gate_out_mst","sys_number=$txt_system_id");	
		//echo $mst_id;die;
		if($mst_id=="" || $mst_id==0){ echo "15**0"; die;}
 		$rID= sql_update("inv_gate_out_mst",'status_active*is_deleted','0*1',"id",$mst_id,1);
		$dtlsrID= sql_update("inv_gate_out_dtls",'status_active*is_deleted','0*1',"mst_id",$mst_id,1);
		if($db_type==0)
		{	
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "2**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$mst_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$mst_id);
			}
		}
		if($db_type==2)
		{	
			if($rID && $dtlsrID)
			{
				oci_commit($con);
				echo "2**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$mst_id);
			}
			else
			{
				oci_rollback($con);
				echo "10**"."**".str_replace("'",'',$txt_system_id)."**".str_replace("'",'',$mst_id);
			}
		}
		disconnect($con);
		die;
	}		
}

if($action=="show_dtls_list_view")
{
	
 	$sql = "select b.id as id,b.item_description,b.quantity,b.uom,b.rate,b.amount,b.remarks from inv_gate_out_mst a,inv_gate_out_dtls b where a.id=b.mst_id and a.sys_number='$data' and b.status_active=1 and b.is_deleted=0 	"; 
	//echo $data; die;
	$arr=array(2=>$unit_of_measurement);
	//function create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path, $filter_grid_fnc, $fld_type_arr, $summary_flds, $check_box_all )
 	echo create_list_view("list_view", "Item Description,Qnty,UOM,Rate,Amount,Remarks","250,70,40,100,150,190","850","260",0, $sql, "get_php_form_data", "id", "'child_form_input_data','requires/get_out_entry_controller'", 1, "0,0,uom,0,0,0", $arr, "item_description,quantity,uom,rate,amount,remarks", "","",'0,2,1,2,2,0',"2  ,quantity,'','',amount,''");	
	exit();
		
} 
if($action=="child_form_input_data")
{
	//$data = details table ID 	
	$sql="select id,item_description,quantity,uom,rate,amount,remarks from inv_gate_out_dtls where id=$data"; 
	$result = sql_select($sql);
	
	foreach($result as $row)
	{
		
		echo "$('#txt_item_description').val('".$row[csf("item_description")]."');\n";
		echo "$('#txt_quantity').val(".$row[csf("quantity")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("uom")].");\n";
		echo "$('#txt_rate').val(".$row[csf("rate")].");\n";
		echo "$('#txt_amount').val(".$row[csf("amount")].");\n";		
 		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		//update id here
		echo "$('#update_id').val(".$row[csf("id")].");\n";		
		//echo "show_list_view(".$row[csf("wo_po_type")]."+'**'+".$row[csf("wo_pi_no")].",'show_product_listview','list_product_container','requires/yarn_receive_controller','');\n";
		echo "set_button_status(1, permission, 'fnc_getout_entry',1,1);\n";
	}
	exit();
}

if ($action=="get_out_entry_print")
{
	extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from   lib_store_location", "id", "store_name"  );
	$sample_library=return_library_array( "select id, sample_name from  lib_sample", "id", "sample_name"  );
	
	$sql="select id, sys_number, company_id, sample_id, item_category_id, sent_by, sent_to, out_date, challan_no, currency_id, gate_pass_no, time_hour, time_minute from   inv_gate_out_mst where company_id='$data[0]' and sys_number='$data[1]' and status_active=1 and is_deleted=0 ";
	//echo $sql;
	$dataArray=sql_select($sql);
?>
<div style="width:930px;" align="center">
    <table width="900" cellspacing="0" align="center" border="0">
        <tr>
            <td colspan="7" align="center" style="font-size:xx-large"><strong><? echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
            <td colspan="7" align="center">
				<?
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <? echo $result['plot_no']; ?> 
						Level No: <? echo $result['level_no']?>
						Road No: <? echo $result['road_no']; ?> 
						Block No: <? echo $result['block_no'];?> 
						City No: <? echo $result['city'];?> 
						Zip Code: <? echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <? echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <? echo $result['email'];?> 
						Website No: <? echo $result['website'];
					}
                ?> 
            </td>
        </tr>
        <tr>
            <td colspan="7" align="center" style="font-size:x-large"><strong><u><? echo $data[2]; ?> Challan</u></strong></td>
        </tr>
        <tr>
            <td width="160"><strong>System ID:</strong></td> <td width="175px"><? echo $dataArray[0][csf('sys_number')]; ?></td>
            <td width="120"><strong>Sample:</strong></td><td width="175px" ><? echo $sample_library[$dataArray[0][csf('sample_id')]]; ?></td>
            <td width="125"><strong>Item Category:</strong></td><td width="175px" colspan="2"><? echo  $item_category[$dataArray[0][csf('item_category_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Sent By:</strong></td> <td width="175px"><? echo $dataArray[0][csf('sent_by')]; ?></td>
            <td><strong>Sent To:</strong></td><td width="175px" ><? echo $dataArray[0][csf('sent_to')]; ?></td>
            <td><strong>Out Date:</strong></td><td width="175px" colspan="2"><? echo change_date_format($dataArray[0][csf('out_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Challan No :</strong></td><td width="175px"><? echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Gate Pass No:</strong></td><td width="175px"><? echo $dataArray[0][csf('gate_pass_no')]; ?></td>
            <td><strong>Out-Time:</strong></td><td width="85px" ><? echo $dataArray[0][csf('time_hour')]." HH"; ?></td><td width="85px"><? echo $dataArray[0][csf('time_minute')]." Min"; ?></td>
        </tr>
    </table>
    <br>
    <table align="center" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="180" align="center">Item Description</th>
            <th width="50" align="center">UOM</th>
            <th width="80" align="center">Quantity</th>
            <th width="80" align="center">Rate</th> 
            <th width="80" align="center">Amount </th>
            <th width="180" align="center">Remarks</th>
        </thead>
<?
    $i=1;
	$gate_id=$dataArray[0][csf('id')];
	$sql_dtls= " select id, item_description, quantity, uom, rate, amount, remarks from  inv_gate_out_dtls where mst_id=$gate_id and status_active=1 and is_deleted=0 ";
	//echo $sql_dtls;
	$sql_result=sql_select($sql_dtls);
	
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			?>
			<tr bgcolor="<? echo $bgcolor; ?>">
                <td><? echo $i; ?></td>
                <td><?  echo $row[csf('item_description')]; ?></td>
                <td align="center"><? echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                <td align="right"><? echo $row[csf('quantity')]; ?></td>
                <td align="right"><? echo number_format($row[csf('rate')],2,'.',''); ?></td>
                <td align="right"><? echo number_format($row[csf('amount')],2,'.',''); ?></td>
                <td><? echo $row[csf('remarks')]; ?></td>
			</tr>
		<?php
    $i++;
    }
	?>
      </table>
</div>
<div>
		<?
        	echo signature_table(34, $data[0], "900px");
        ?>
</div>    
<?

exit();
}

?>