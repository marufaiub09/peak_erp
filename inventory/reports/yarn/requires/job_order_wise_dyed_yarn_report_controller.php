﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
 if($db_type==0) $group_concat="group_concat";
 if($db_type==2) $group_concat="wm_concat";
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );     	 
	exit();
}

if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_job_id').val( id );
			$('#hide_job_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Job No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
                    <input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value, 'create_job_no_search_list_view', 'search_div', 'job_order_wise_dyed_yarn_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_job_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	
	$arr=array (0=>$company_arr,1=>$buyer_arr);
	if($db_type==0) $insert_year="year(insert_date)";	
	if($db_type==2) $insert_year="to_char(insert_date,'yyyy')";
	$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no, $insert_year as year from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond order by job_no";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","600","240",0, $sql , "js_set_value", "id,job_no", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0,0','',1) ;
	
   exit(); 
} 

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$supplier_arr=return_library_array( "select id, short_name from lib_supplier",'id','short_name');
	
	$txt_process_loss=str_replace("'","",$txt_process_loss);
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";
	}
	
	if(str_replace("'","",$hide_job_id)=="")
	{
		$job_no_cond="";
	}
	else
	{
		$job_no_cond="and a.id in(".str_replace("'","",$hide_job_id).")";
	}

 if($db_type==0)
    {
	$order_arr=return_library_array( "select job_no_mst, group_concat(distinct(po_number)) as order_no from wo_po_break_down where status_active=1 and is_deleted=0 group by job_no_mst", "job_no_mst", "order_no" );
	}
 if($db_type==2)
    {
	$order_arr=return_library_array( "select job_no_mst, listagg(po_number,',') within group (order by po_number) as order_no from wo_po_break_down where status_active=1 and is_deleted=0 group by job_no_mst", "job_no_mst", "order_no" );
	$order_arr=array_unique($order_arr);
	}
	//print_r($order_arr);
	$color_array=return_library_array( "select id, color_name from lib_color",'id','color_name');
		
	if($type==1)
	{
		$issue_arr=array();
	
		$issueDataArr=sql_select("select a.job_no, sum(a.cons_quantity) as issue_qnty, b.booking_id from inv_transaction a, inv_issue_master b where a.mst_id=b.id and b.entry_form=3 and b.issue_basis=1 and b.issue_purpose=2 and a.company_id=$cbo_company_name and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=2 group by a.job_no, b.booking_id");
		foreach($issueDataArr as $row)
		{
			$issue_arr[$row[csf('job_no')]][$row[csf('booking_id')]]=$row[csf('issue_qnty')];
		}
		
		/*$recv_arr=array();
		$recvDataArr=sql_select("select a.job_no, sum(a.cons_quantity) as recv_qnty, b.booking_id from inv_transaction a, inv_receive_master b where a.mst_id=b.id and b.entry_form=1 and b.receive_basis=2 and b.receive_purpose=2 and a.company_id=$cbo_company_name and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=1 group by a.job_no, b.booking_id");
		
		foreach($recvDataArr as $row)
		{
			$recv_arr[$row[csf('job_no')]][$row[csf('booking_id')]]=$row[csf('recv_qnty')];
		}*/
		
		$BookingColorArray=array(); $color_req_array=array();
		$BookingData=sql_select("select mst_id, sum(yarn_wo_qty) as qnty, yarn_color, job_no from wo_yarn_dyeing_dtls where status_active=1 and is_deleted=0 group by job_no, mst_id, yarn_color");
		foreach($BookingData as $row)
		{
			$BookingColorArray[$row[csf('mst_id')]].=$row[csf('yarn_color')].",";
			$color_req_array[$row[csf('job_no')]][$row[csf('mst_id')]][$row[csf('yarn_color')]]=$row[csf('qnty')];
		}

		$recv_arr=array();
		$recvDataArr=sql_select("select a.job_no, sum(a.cons_quantity) as recv_qnty, b.booking_id, c.color from inv_transaction a, inv_receive_master b, product_details_master c where a.mst_id=b.id and a.prod_id=c.id and b.entry_form=1 and b.receive_basis=2 and b.receive_purpose=2 and a.company_id=$cbo_company_name and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=1 group by a.job_no, b.booking_id, c.color");
		foreach($recvDataArr as $row)
		{
			$recv_arr[$row[csf('job_no')]][$row[csf('booking_id')]][$row[csf('color')]]=$row[csf('recv_qnty')];
		}
		
		ob_start();
		?>
		<fieldset style="width:1000px; margin-left:10px">
			<table cellpadding="0" cellspacing="0" width="1000">
				<tr>
				   <td align="center" width="100%" colspan="16" style="font-size:16px"><strong><?php echo $report_title; ?></strong></td>
				</tr>
				<tr>
				   <td align="center" width="100%" colspan="16" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
				</tr>
			</table>
			<table width="982" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
				<thead>
					<tr>
						<th width="35" rowspan="2">SL</th>
						<th width="80" rowspan="2">WO No</th>
						<th width="240" colspan="3">Grey Yarn</th>
						<th width="530" colspan="7">Dyed Yarn</th>
						<th rowspan="2">Party Name</th>
					</tr>
					<tr>
						<th width="80">Total Required</th>
						<th width="80">Delivery</th>
						<th width="80">Balance</th>
						<th width="90">Order Qty. (Less <?php echo $txt_process_loss; ?>% process loss)</th>
                        <th width="80">Req. Qty.</th>
                        <th width="70">Color</th>
						<th width="80">Received Qty.</th>
                        <th width="80">Color Wise Balance</th>
						<th width="80">Process Loss</th>
						<th width="70">Act. Pro. Loss(%)</th>
					</tr>
				</thead>
			</table>
			<div style="width:1000px; overflow-y: scroll; max-height:380px;" id="scroll_body">
				<table width="982" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">    
					<?php
						$i=1; $z=0; $job_array=array();
						$tot_recv_bl_qnty=0; $total_req_qnty=0; $tot_delivery_qnty=0; $total_grey_bl_qnty=0; $total_process_loss_qnty=0; $total_recv_qnty=0; $total_dyed_bl_qnty=0;
						if($db_type==0)
						{
						     $sql="select a.job_no, a.buyer_name, a.style_ref_no, c.id, c.ydw_no, c.supplier_id, sum(b.yarn_wo_qty) as qnty from wo_po_details_master a, wo_yarn_dyeing_dtls b, wo_yarn_dyeing_mst c where a.id=b.job_no_id and b.mst_id=c.id and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.company_name=$cbo_company_name $buyer_id_cond $job_no_cond group by a.id, c.id order by a.job_no";
						}
					if($db_type==2)
						{
						    $sql="select a.job_no, a.buyer_name, a.style_ref_no, c.id, c.ydw_no, c.supplier_id, sum(b.yarn_wo_qty) as qnty from wo_po_details_master a, wo_yarn_dyeing_dtls b, wo_yarn_dyeing_mst c where a.id=b.job_no_id and b.mst_id=c.id and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.company_name=$cbo_company_name $buyer_id_cond $job_no_cond group by a.id,c.id, a.job_no, a.buyer_name, a.style_ref_no,  c.ydw_no, c.supplier_id order by a.job_no";
						}
						//echo $sql;
						$result=sql_select($sql);
						foreach($result as $row)
						{
							if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							
							$colorArray=explode(",",substr($BookingColorArray[$row[csf('id')]],0,-1));
							$s=0; $row_span=count($colorArray);
							
							$delivery_qnty=$issue_arr[$row[csf('job_no')]][$row[csf('id')]];
							$grey_balance=$row[csf('qnty')]-$delivery_qnty;
							$process_loss_qnty=$delivery_qnty-($delivery_qnty*$txt_process_loss/100);
							
							$job_tot_recv_qnty=array_sum($recv_arr[$row[csf('job_no')]][$row[csf('id')]]);
							$dyed_balance=$process_loss_qnty-$job_tot_recv_qnty;
							$actual_process_loss=($delivery_qnty-$job_tot_recv_qnty)/$delivery_qnty*100;
							
							if(!in_array($row[csf('job_no')],$job_array))
							{
								if($i!=1)
								{
								?>
									<tr bgcolor="#CCCCCC">
										<td colspan="2" align="right"><b>Job Total</b></td>
                                        <td align="right"><?php echo number_format($job_req_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td align="right"><?php echo number_format($job_delivery_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td align="right"><?php echo number_format($job_grey_bl_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td align="right"><?php echo number_format($job_process_loss_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td align="right"><?php echo number_format($job_color_req_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td>&nbsp;</th>
                                        <td align="right"><?php echo number_format($job_recv_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td align="right"><?php echo number_format($job_recv_bl_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td align="right"><?php echo number_format($job_dyed_bl_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td colspan="2">&nbsp;</td>	
									</tr>
								<?php
									$job_req_qnty=0;
									$job_delivery_qnty=0;
									$job_grey_bl_qnty=0;
									$job_process_loss_qnty=0;
									$job_color_req_qnty=0;
									$job_recv_qnty=0;
									$job_recv_bl_qnty=0;
									$job_dyed_bl_qnty=0;
								}	
							?>
								<tr><td colspan="13" bgcolor="#EEEEEE"><b><?php echo "Job No:- ".$row[csf('job_no')]."; Buyer:- ".$buyer_arr[$row[csf('buyer_name')]]."; Style Ref:- ".$row[csf('style_ref_no')]."; Order No:- ".$order_arr[$row[csf('job_no')]]; ?></b></td></tr>
							<?php	
								$job_array[$i]=$row[csf('job_no')];
							}	
								
							foreach($colorArray as $color_id)
							{
								$recv_qnty=$recv_arr[$row[csf('job_no')]][$row[csf('id')]][$color_id];
								$req_qnty=$color_req_array[$row[csf('job_no')]][$row[csf('id')]][$color_id];
								$req_qnty=$req_qnty-($req_qnty*$txt_process_loss/100);
								
								$recv_bl_qnty=$req_qnty-$recv_qnty;
							?>
                                <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $z; ?>','<?php echo $bgcolor; ?>')" id="tr<?php echo $z;?>">
                                	<?php
									if($s==0)
									{
									?>
                                        <td width="35" rowspan="<?php echo $row_span; ?>"><?php echo $i; ?></td>
                                        <td width="80" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('ydw_no')]; ?></p></td>
                                        <td width="80" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($row[csf('qnty')],2,'.',''); ?>&nbsp;</td>
                                        <td width="80" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($delivery_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td width="80" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($grey_balance,2,'.',''); ?>&nbsp;</td>
                                        <td width="90" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($process_loss_qnty,2,'.','');  ?>&nbsp;</td>
                                    <?php
										$total_req_qnty+=$row[csf('qnty')];
										$tot_delivery_qnty+=$delivery_qnty;
										$total_grey_bl_qnty+=$grey_balance;
										$total_process_loss_qnty+=$process_loss_qnty;
										
										$job_req_qnty+=$row[csf('qnty')];
										$job_delivery_qnty+=$delivery_qnty;
										$job_grey_bl_qnty+=$grey_balance;
										$job_process_loss_qnty+=$process_loss_qnty;
									}
									?>
                                    <td width="80" align="right"><?php echo number_format($req_qnty,2,'.','');  ?>&nbsp;</td>
                                    <td width="70"><p><?php echo $color_array[$color_id]; ?>&nbsp;</p></td>
                                    <td width="80" align="right"><?php echo number_format($recv_qnty,2,'.','');  ?>&nbsp;</td>
                                    <td width="80" align="right"><?php echo number_format($recv_bl_qnty,2,'.','');  ?>&nbsp;</td>
                                    <?php
									if($s==0)
									{
									?>
                                        <td width="80" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($dyed_balance,2,'.','');  ?>&nbsp;</td> 
                                        <td width="70" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($actual_process_loss,2,'.',''); ?>&nbsp;</td>
                                        <td rowspan="<?php echo $row_span; ?>"><p><?php echo $supplier_arr[$row[csf('supplier_id')]]; ?></td>
                                    <?php
										$total_dyed_bl_qnty+=$dyed_balance;
										$job_dyed_bl_qnty+=$dyed_balance;
									}
									?>
                                </tr>   
							<?php  
								$total_color_req_qnty+=$req_qnty;
								$job_color_req_qnty+=$req_qnty;
								$total_recv_qnty+=$recv_qnty;
								$job_recv_qnty+=$recv_qnty;
								$job_recv_bl_qnty+=$recv_bl_qnty;
								$tot_recv_bl_qnty+=$recv_bl_qnty;
								
								$z++;
								$s++; 
							}
							$i++;
						}
						
						if(count($result)>0)
						{
						?>
                            <tr bgcolor="#CCCCCC">
                                <td colspan="2" align="right"><b>Job Total</b></td>
                                <td align="right"><?php echo number_format($job_req_qnty,2,'.',''); ?>&nbsp;</td>
                                <td align="right"><?php echo number_format($job_delivery_qnty,2,'.',''); ?>&nbsp;</td>
                                <td align="right"><?php echo number_format($job_grey_bl_qnty,2,'.',''); ?>&nbsp;</td>
                                <td align="right"><?php echo number_format($job_process_loss_qnty,2,'.',''); ?>&nbsp;</td>
                                <td align="right"><?php echo number_format($job_color_req_qnty,2,'.',''); ?>&nbsp;</td>
                                <td>&nbsp;</th>
                                <td align="right"><?php echo number_format($job_recv_qnty,2,'.',''); ?>&nbsp;</td>
                                <td align="right"><?php echo number_format($job_recv_bl_qnty,2,'.',''); ?>&nbsp;</td>
                                <td align="right"><?php echo number_format($job_dyed_bl_qnty,2,'.',''); ?>&nbsp;</td>
                                <td colspan="2">&nbsp;</td>	
                            </tr>
                        <?php	
						}
					?>
					<tfoot>
						<th colspan="2" align="right">Total</th>
						<th align="right"><?php echo number_format($total_req_qnty,2,'.',''); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($tot_delivery_qnty,2,'.',''); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($total_grey_bl_qnty,2,'.',''); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($total_process_loss_qnty,2,'.',''); ?>&nbsp;</th>
                        <th align="right"><?php echo number_format($total_color_req_qnty,2,'.',''); ?>&nbsp;</th>
                        <th>&nbsp;</th>
						<th align="right"><?php echo number_format($total_recv_qnty,2,'.',''); ?>&nbsp;</th>
                        <th align="right"><?php echo number_format($tot_recv_bl_qnty,2,'.',''); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($total_dyed_bl_qnty,2,'.',''); ?>&nbsp;</th>
						<th colspan="2">&nbsp;</th>
					</tfoot>
				</table>
			</div>
		</fieldset>      
	<?php
	}
	else
	{
		$issue_arr=array(); $color_wise_qnty_array=array();
		
		$issueDataArr=sql_select("select a.job_no, a.dyeing_color_id, sum(a.cons_quantity) as issue_qnty, b.booking_id from inv_transaction a, inv_issue_master b where a.mst_id=b.id and b.entry_form=3 and b.issue_basis=1 and b.issue_purpose=2 and a.company_id=$cbo_company_name and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=2 group by a.job_no, b.booking_id, a.dyeing_color_id");
		foreach($issueDataArr as $row)
		{
			$issue_arr[$row[csf('job_no')]][$row[csf('booking_id')]]+=$row[csf('issue_qnty')];
			$color_wise_qnty_array[$row[csf('job_no')]][$row[csf('booking_id')]][$row[csf('dyeing_color_id')]]=$row[csf('issue_qnty')];
		}
	if($db_type==0)
	   {	
		$BookingColorArray=return_library_array("select mst_id, $group_concat(distinct(yarn_color)) as color from wo_yarn_dyeing_dtls where status_active=1 and is_deleted=0 group by mst_id",'mst_id','color');
	   }
  if($db_type==2)
	   {	
		$BookingColorArray=return_library_array("select mst_id, listagg(yarn_color,',') within group (order by yarn_color) as color from wo_yarn_dyeing_dtls where status_active=1 and is_deleted=0 group by mst_id",'mst_id','color');
		$BookingColorArray=array_unique($BookingColorArray);
	   }
		
		$recv_arr=array();
		$recvDataArr=sql_select("select a.job_no, sum(a.cons_quantity) as recv_qnty, b.booking_id, c.color from inv_transaction a, inv_receive_master b, product_details_master c where a.mst_id=b.id and a.prod_id=c.id and b.entry_form=1 and b.receive_basis=2 and b.receive_purpose=2 and a.company_id=$cbo_company_name and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=1 group by a.job_no, b.booking_id, c.color");
		
		foreach($recvDataArr as $row)
		{
			$recv_arr[$row[csf('job_no')]][$row[csf('booking_id')]][$row[csf('color')]]=$row[csf('recv_qnty')];
		}
		
	?>
		<fieldset style="width:1710px;">
			<table cellpadding="0" cellspacing="0" width="1700">
				<tr>
				   <td align="center" width="100%" colspan="14" style="font-size:16px"><strong><?php echo $report_title; ?></strong></td>
				</tr>
				<tr>
				   <td align="center" width="100%" colspan="14" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
				</tr>
			</table>
			<table width="1682" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
				<thead>
					<tr>
						<th width="40" rowspan="2">SL</th>
						<th width="100" rowspan="2">Job No</th>
						<th width="80" rowspan="2">Buyer Name</th>
						<th width="170" rowspan="2">Order No</th>
						<th width="110" rowspan="2">Style No</th>
						<th width="120" rowspan="2">WO No</th>
						<th width="400" colspan="4">Grey Yarn</th>
                        <th width="100" rowspan="2">Color</th>
						<th width="400" colspan="4">Dyed Yarn</th>
						<th rowspan="2">Party Name</th>
					</tr>
					<tr>
						<th width="100">Total Required</th>
						<th width="100">Delivery</th>
						<th width="100">Balance</th>
                        <th width="100">Grey (Color-wise)</th>
						<th width="100">Order Qty. (Less <?php echo $txt_process_loss; ?>% process loss)</th>
						<th width="100">Received Qty.</th>
						<th width="100">Balance</th>
						<th width="100">Actual Process Loss(%)</th>
					</tr>
				</thead>
			</table>
			<div style="width:1700px; overflow-y: scroll; max-height:380px;" id="scroll_body">
				<table width="1682" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">    
					<?php
						$i=1; $z=1; $total_req_qnty=0; $tot_delivery_qnty=0; $total_grey_bl_qnty=0; $total_process_loss_qnty=0; $total_recv_qnty=0; $total_dyed_bl_qnty=0;
					if($db_type==0)
						{	
							$sql="select a.job_no, a.buyer_name, a.style_ref_no, c.id, c.ydw_no, c.supplier_id, sum(b.yarn_wo_qty) as qnty from wo_po_details_master a, wo_yarn_dyeing_dtls b, wo_yarn_dyeing_mst c where a.id=b.job_no_id and b.mst_id=c.id and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.company_name=$cbo_company_name $buyer_id_cond $job_no_cond group by a.id, c.id";
						}
					if($db_type==2)
						{	
							$sql="select a.job_no, a.buyer_name, a.style_ref_no, c.id, c.ydw_no, c.supplier_id, sum(b.yarn_wo_qty) as qnty from wo_po_details_master a, wo_yarn_dyeing_dtls b, wo_yarn_dyeing_mst c where a.id=b.job_no_id and b.mst_id=c.id and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.company_name=$cbo_company_name $buyer_id_cond $job_no_cond group by a.id,c.id,a.job_no,a.buyer_name,a.style_ref_no,c.ydw_no,c.supplier_id order by a.job_no";
						}
						//echo $sql;
						$result=sql_select($sql);
						foreach($result as $row)
						{
							if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							
							$delivery_qnty=$issue_arr[$row[csf('job_no')]][$row[csf('id')]];
							$grey_balance=$row[csf('qnty')]-$delivery_qnty;
							
							$colorArray=explode(",",$BookingColorArray[$row[csf('id')]]);
							if($color_wise_qnty_array[$row[csf('job_no')]][$row[csf('id')]][0]>0)
							{
								array_unshift($colorArray, "0"); 
							}
							$s=0; $row_span=count($colorArray);
							foreach($colorArray as $color_id)
							{
								$grey_color_qnty=$color_wise_qnty_array[$row[csf('job_no')]][$row[csf('id')]][$color_id];
								$process_loss_qnty=$grey_color_qnty-($grey_color_qnty*$txt_process_loss/100);
								$recv_qnty=$recv_arr[$row[csf('job_no')]][$row[csf('id')]][$color_id];
								$dyed_balance=$process_loss_qnty-$recv_qnty;
								$actual_process_loss=($grey_color_qnty-$recv_qnty)/$grey_color_qnty*100;
							?>
								<tr valign="middle" bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $z; ?>','<?php echo $bgcolor; ?>')" id="tr<?php echo $z;?>">
                                	<?php
									if($s==0)
									{
									?>
                                        <td width="40" rowspan="<?php echo $row_span; ?>"><?php echo $i; ?></td>
                                        <td width="100" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('job_no')]; ?></p></td>
                                        <td width="80" rowspan="<?php echo $row_span; ?>"><p><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?>&nbsp;</p></td>
                                        <td width="170" rowspan="<?php echo $row_span; ?>"><p><?php echo $order_arr[$row[csf('job_no')]]; ?>&nbsp;</p></td>
                                        <td width="110" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('style_ref_no')]; ?>&nbsp;</p></td>
                                        <td width="120" rowspan="<?php echo $row_span; ?>"><p><?php echo $row[csf('ydw_no')]; ?></p></td>
                                        <td width="100" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($row[csf('qnty')],2,'.',''); ?>&nbsp;</td>
                                        <td width="100" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($delivery_qnty,2,'.',''); ?>&nbsp;</td>
                                        <td width="100" align="right" rowspan="<?php echo $row_span; ?>"><?php echo number_format($grey_balance,2,'.',''); ?>&nbsp;</td>
                                    <?php
										$total_req_qnty+=$row[csf('qnty')];
										$tot_delivery_qnty+=$delivery_qnty;
										$total_grey_bl_qnty+=$grey_balance;
									}
									?>
									<td width="100" align="right"><?php echo number_format($grey_color_qnty,2,'.',''); ?>&nbsp;</td>
									<td width="100" align="center"><?php if($color_id==0) echo "Select"; else echo $color_array[$color_id]; ?>&nbsp;</td>
									<td width="100" align="right"><?php echo number_format($process_loss_qnty,2,'.','');  ?>&nbsp;</td>
									<td width="100" align="right"><?php echo number_format($recv_qnty,2,'.','');  ?>&nbsp;</td>
									<td width="100" align="right"><?php echo number_format($dyed_balance,2,'.','');  ?>&nbsp;</td> 
									<td width="100" align="right"><?php echo number_format($actual_process_loss,2,'.',''); ?>&nbsp;</td>
                                    <?php
									if($s==0)
									{
									?>
										<td rowspan="<?php echo $row_span; ?>"><p><?php echo $supplier_arr[$row[csf('supplier_id')]]; ?></td>
                                    <?php
									}
									?>
								</tr>   
							<?php  
								$total_color_wise_qnty+=$grey_color_qnty;
								$total_process_loss_qnty+=$process_loss_qnty;
								$total_recv_qnty+=$recv_qnty;
								$total_dyed_bl_qnty+=$dyed_balance;
								
								$s++;
								$z++;
							}
							$i++;
						}
					?>
					<tfoot>
						<th colspan="6" align="right">Total</th>
						<th align="right"><?php echo number_format($total_req_qnty,2); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($tot_delivery_qnty,2); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($total_grey_bl_qnty,2); ?>&nbsp;</th>
                        <th align="right"><?php echo number_format($total_color_wise_qnty,2); ?>&nbsp;</th>
                        <th>&nbsp;</th>
						<th align="right"><?php echo number_format($total_process_loss_qnty,2); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($total_recv_qnty,2); ?>&nbsp;</th>
						<th align="right"><?php echo number_format($total_dyed_bl_qnty,2); ?>&nbsp;</th>
						<th colspan="2">&nbsp;</th>
					</tfoot>
				</table>
			</div>
		</fieldset>      
	<?php	
	}
    foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
}
?>
