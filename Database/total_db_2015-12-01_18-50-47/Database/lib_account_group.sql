-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_account_group`
--

DROP TABLE IF EXISTS `lib_account_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_account_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_group_code` varchar(50) DEFAULT NULL,
  `main_group` int(11) NOT NULL DEFAULT '0',
  `sub_group` varchar(100) DEFAULT NULL,
  `statement_type` int(11) NOT NULL DEFAULT '0',
  `account_type` int(11) NOT NULL DEFAULT '0' COMMENT '1=Credit;2=Debit',
  `cash_flow_group` int(11) NOT NULL DEFAULT '0',
  `retained_earnings` tinyint(1) DEFAULT '0' COMMENT '0=no,1=yes',
  `inserted_by` varchar(50) DEFAULT NULL,
  `insert_date` int(11) NOT NULL DEFAULT '0',
  `updated_by` varchar(50) DEFAULT NULL,
  `update_date` int(11) NOT NULL DEFAULT '0',
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_account_group`
--

LOCK TABLES `lib_account_group` WRITE;
/*!40000 ALTER TABLE `lib_account_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `lib_account_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
