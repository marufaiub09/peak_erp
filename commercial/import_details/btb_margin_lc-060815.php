<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will create for BTB entry
					
Functionality	:	
				

JS Functions	:

Created by		:	Rashed 
Creation date 	: 	18-11-2012
Updated by 		: 	Fuad Shahriar	
Update date		: 	04-04-2013	   

QC Performed BY	:		

QC Date			:	

Comments		:

*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("BTB / Margin LC","../../", 1, 1, $unicode,'',''); 

?> 	

<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
	var permission='<?php echo $permission; ?>';
	
	var str_port_of_loading 	= [<?php echo substr(return_library_autocomplete( "select distinct(port_of_loading) from com_btb_lc_master_details", "port_of_loading"  ), 0, -1); ?>];
	var str_port_of_discharge 	= [<?php echo substr(return_library_autocomplete("select distinct(port_of_discharge) from com_btb_lc_master_details", "port_of_discharge"), 0, -1); ?>];
	var str_inco_term_place 	= [<?php echo substr(return_library_autocomplete( "select distinct(inco_term_place) from com_btb_lc_master_details", "inco_term_place"  ), 0, -1); ?>];
	
	$(document).ready(function(e)
	{
		$("#txt_port_loading").autocomplete({
			source: str_port_of_loading
		});
		$("#txt_port_discharge").autocomplete({
			source: str_port_of_discharge
		});
		$("#txt_inco_term_place").autocomplete({
			source: str_inco_term_place
		});
		
	});
	
	function openmypage(type,row_num)
	{
		if(type==2)
		{ 
			var item_category = $('#cbo_item_category_id').val(); 
			var txt_hidden_pi_id = $('#txt_hidden_pi_id').val(); 
			var btb_id = $('#update_id').val(); 
			
			if (form_validation('cbo_item_category_id','Item Category')==false)
			{
				return;
			}
			else
			{ 	
				var title = 'PI Selection Form';	
				var page_link = 'requires/btb_margin_lc_controller.php?item_category_id='+item_category+'&txt_hidden_pi_id='+txt_hidden_pi_id+'&btb_id='+btb_id+'&action=pi_popup';
				  
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=920px,height=450px,center=1,resize=1,scrolling=0','../')
				
				emailwindow.onclose=function()
				{
					var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
					var pi_id=this.contentDoc.getElementById("txt_selected_id").value; 
					var pi_no=this.contentDoc.getElementById("txt_selected").value;
				 
					if (pi_id!="")
					{ 
						$('#txt_hidden_pi_id').val(pi_id);
						$('#txt_pi').val(pi_no);
						
						freeze_window(5);
						get_php_form_data(pi_id, "set_value_pi_select", "requires/btb_margin_lc_controller" );
						
						show_list_view(pi_id+'_'+item_category,'show_pi_details_list','pi_details_list','requires/btb_margin_lc_controller',''); 
						release_freezing();
					} 
					else
					{
						$('#txt_pi').val('');
						$('#txt_hidden_pi_id').val('');	
						reset_form('','','txt_pi_value*cbo_supplier_id*txt_last_shipment_date*cbo_pi_currency_id*cbo_lc_currency_id');
						reset_form('','pi_details_list');
					}
				}
			}
		}
		else if(type==1)
		{
			var page_link='requires/btb_margin_lc_controller.php?action=btb_lc_search';
			var title='BTB L/C Search Form';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=1,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var btb_id=this.contentDoc.getElementById("hidden_btb_id").value;
				if(trim(btb_id)!="")
				{
					freeze_window(5);
					get_php_form_data( btb_id, "populate_data_from_btb_lc", "requires/btb_margin_lc_controller" ); 

					show_list_view(trim(btb_id),'show_lc_listview','sc_lc_list_view','requires/btb_margin_lc_controller','');
	
					var item_category = $('#cbo_item_category_id').val();  
					var txt_hidden_pi_id = $('#txt_hidden_pi_id').val(); 
					
					if(txt_hidden_pi_id!="")
					{
						show_list_view(txt_hidden_pi_id+'_'+item_category,'show_pi_details_list','pi_details_list','requires/btb_margin_lc_controller',''); 
					}
					
					var numRow = $('table#tbl_lc_list tbody tr').length; 
					var button_status='';
					
					if(numRow==1)
					{
						var lc_sc_no=$('#txtlcsc_1').val();
						if(lc_sc_no=="")
						{
							$('#txt_tot_row').val(0);
							button_status=0;	
						}
						else
						{
							$('#txt_tot_row').val(numRow);
							button_status=1;		
						}
					}
					else
					{
						$('#txt_tot_row').val(numRow);	
						button_status=1;	
					}
					
					var ddd={ dec_type:4, comma:0, currency:$('#cbo_pi_currency_id').val()}
					
					math_operation( "totalLcScValue", "txtlcscvalue_", "+", numRow,ddd );
					math_operation( "totalCurrentDistri", "txtcurdistribution_", "+", numRow,ddd );
					math_operation( "totalCumuDisri", "txtcumudistribution_", "+", numRow,ddd );
					calculate_occupied(numRow);
					set_button_status(button_status, permission, 'fnc_lc_details',2); 
					release_freezing();
					
				}
							 
			}
		}
		else if(type==3)
		{
			//var update_id = document.getElementById('update_id').value;
			var cbo_importer_id = document.getElementById('cbo_importer_id').value;
			var lc_sc="";
			var numRow = $('table#tbl_lc_list tbody tr').length; 
			for(var c=1; c<=numRow; c++)
			{
				var lc_sc_id=$('#txtLcScid_'+c).val();
				if(lc_sc_id!="")
				{
					var type=$('#txtlcscflagId_'+c).val();
					if(lc_sc=="") lc_sc=lc_sc_id+"__"+type; else lc_sc=lc_sc+","+lc_sc_id+"__"+type;
				}
			}

			page_link = 'requires/btb_margin_lc_controller.php?action=lc_popup&company_id='+cbo_importer_id+'&lc_sc='+lc_sc,'LC/SC Selection Form';
			 
			if(form_validation('txt_system_id','System ID')==false )
			{
				return;
			}
			else
			{
				emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'LC/SC Information', 'width=900px,height=360px,center=1,resize=1,scrolling=0','../');
				emailwindow.onclose=function()
				{
					var theform=this.contentDoc.forms[0];
					var lc_sc_id=this.contentDoc.getElementById("txt_selected_id").value; 
					var tot_row=$('#txt_tot_row').val();
					var data=tot_row+"**"+lc_sc_id;

					var list_view_orders = return_global_ajax_value( data, 'lc_list_for_attach', '', 'requires/btb_margin_lc_controller');
					var lc_sc_no=$('#txtlcsc_'+row_num).val();
					
					if(lc_sc_no=="")
					{
						$("#tr_"+row_num).remove();
					}
					if(tot_row==0)
					{
						$('#sc_lc_list_view').html(list_view_orders);
					}
					else
					{
						$("#tbl_lc_list tbody:last").append(list_view_orders);	
					}
					
					var numRow = $('table#tbl_lc_list tbody tr').length; 
					
					$('#txt_tot_row').val(numRow);
					var ddd={ dec_type:4, comma:0, currency:$('#cbo_pi_currency_id').val()}
					math_operation( "totalLcScValue", "txtlcscvalue_", "+", numRow,ddd );
					calculate_all(numRow);
				}
			}
		}
		
	}
	 
 	function calculate_all(tot_row)
	{
		current_distribution(tot_row);
		cummulative_distribution(tot_row);
		calculate_occupied(tot_row);
	}
	
	function current_distribution(tot_row)
	{
		var btb_val=$('#txt_lc_value').val();
		var tot_lc_sc_val=$('#totalLcScValue').val();
		var cbo_pi_currency_id=$('#cbo_pi_currency_id').val();
		var totalDistribution=0;
		
		for(i=1;i<=tot_row;i++)
		{
			var lc_sc_val=$('#txtlcscvalue_'+i).val();
			var current_distri = (btb_val/tot_lc_sc_val)*lc_sc_val;
			current_distri=number_format_common(current_distri,'','',cbo_pi_currency_id);
			
			totalDistribution = totalDistribution*1+current_distri*1;
			totalGrey = number_format_common(totalDistribution,'','',cbo_pi_currency_id);
			
			if(i==tot_row)
			{
				var balance = btb_val*1-totalDistribution*1;
				balance=number_format_common(balance,'','',cbo_pi_currency_id);
				if(balance!=0) current_distri=current_distri*1+(balance*1);	
			}
			
			$('#txtcurdistribution_'+i).val(current_distri);
		}
		var ddd={ dec_type:4, comma:0, currency:cbo_pi_currency_id}
		math_operation( "totalCurrentDistri", "txtcurdistribution_", "+", tot_row, ddd );
	}

	
	function cummulative_distribution(tot_row)
	{
		var cbo_pi_currency_id=$('#cbo_pi_currency_id').val();
		
		for(i=1;i<=tot_row;i++)
		{
			var current_distri = $('#txtcurdistribution_'+i).val();
			var cumudistri=$('#hiddencumudistribution_'+i).val();
			var curr_cumudistri=cumudistri*1+current_distri*1;
			curr_cumudistri=number_format_common(curr_cumudistri,'','',cbo_pi_currency_id);
			$('#txtcumudistribution_'+i).val(curr_cumudistri);
		}
		var ddd={ dec_type:4, comma:0, currency:cbo_pi_currency_id}
		math_operation( "totalCumuDisri", "txtcumudistribution_", "+", tot_row, ddd );
	}
	
	function calculate_occupied(tot_row)
	{
		for(i=1;i<=tot_row;i++)
		{
			var lc_sc_val=$('#txtlcscvalue_'+i).val()*1;
			if(lc_sc_val>0)
			{
				var cumudistri=$('#txtcumudistribution_'+i).val()*1;
				var occupied=(cumudistri/lc_sc_val)*100;
			}
			else
			{
				var occupied=0;
			}
			
			$('#txtoccupied_'+i).val(occupied.toFixed(4));
		}
		 
	}
	
	function distribution_value(mehtod,row_id)
	{
		var row_num = $('table#tbl_lc_list tbody tr').length;
		if(row_id==0)
		{
			if(mehtod==1)
			{
				$('#tbl_lc_list input[name="txtcurdistribution[]"]').removeAttr('disabled', 'disabled');
				for (var i=1; i<=row_num; i++)
				{
					var type=$('#txtcaltype_'+i).val();
					if(type==1)
					{
						var cumu_val = $('#hiddencumudistribution_'+i).val()*1;
						$('#txtcumudistribution_'+i).val(cumu_val.toFixed(2));
						$('#txtcurdistribution_'+i).val('');
					}
					else if(type==2)
					{
						var current_distri = $('#txtcurdistribution_'+i).val();
						var cumudistri=$('#txtcumudistribution_'+i).val();
						var curr_cumudistri=cumudistri*1-current_distri*1;
						$('#txtcumudistribution_'+i).val(curr_cumudistri.toFixed(2));
						$('#txtcurdistribution_'+i).val('');
					}
				}
				$('#totalCurrentDistri').val(''); 
				var ddd={ dec_type:4, comma:0, currency:$('#cbo_pi_currency_id').val()}
				math_operation( "totalCumuDisri", "txtcumudistribution_", "+", row_num,ddd );
				calculate_occupied(row_num);
				
			}
			else
			{
				$('#tbl_lc_list input[name="txtcurdistribution[]"]').attr('disabled', 'disabled');
				current_distribution(row_num);
				for (var i=1; i<=row_num; i++)
				{
					var type=$('#txtcaltype_'+i).val();
					if(type==1)
					{
						var txtcurdistribution = $('#txtcurdistribution_'+i).val();
						var cumudistri = $('#hiddencumudistribution_'+i).val();
						var curr_cumudistri=cumudistri*1+txtcurdistribution*1;
						$('#txtcumudistribution_'+i).val(curr_cumudistri.toFixed(2));	
					}
					else if(type==2)
					{
						var txtcurdistribution = $('#txtcurdistribution_'+i).val();
						var cumudistri = $('#hiddencumudistribution_'+i).val();
						var hidecurdistribution = $('#hidecurdistribution_'+i).val();
						var curr_cumudistri=(cumudistri*1+txtcurdistribution*1)-hidecurdistribution*1;
						$('#txtcumudistribution_'+i).val(curr_cumudistri.toFixed(2));
					}
				}
				var ddd={ dec_type:4, comma:0, currency:$('#cbo_pi_currency_id').val()}
				math_operation( "totalCumuDisri", "txtcumudistribution_", "+", row_num,ddd );
				calculate_occupied(row_num);
			}
		}
		else
		{
			var type=$('#txtcaltype_'+row_id).val();
			var txtcurdistribution=$('#txtcurdistribution_'+row_id).val();
			var txtcumudistribution=$('#hiddencumudistribution_'+row_id).val();
			var curr_cumudistri=txtcumudistribution*1+txtcurdistribution*1;
			
			$('#txtcumudistribution_'+row_id).val(curr_cumudistri.toFixed(2));
			
			var lc_sc_val=$('#txtlcscvalue_'+row_id).val();
			var occupied=(curr_cumudistri/lc_sc_val)*100;
			$('#txtoccupied_'+row_id).val(occupied.toFixed(2));
			
			var ddd={ dec_type:4, comma:0, currency:$('#cbo_pi_currency_id').val()}
			math_operation( "totalCumuDisri", "txtcumudistribution_", "+", row_num,ddd );
			math_operation( "totalCurrentDistri", "txtcurdistribution_", "+", row_num,ddd );
		}
	}

	function fn_add_row_lc(i)
	{
		var row_num = $('table#tbl_lc_list tbody tr').length;
		if(i==0)
		{
			return false;
		}
		else if(row_num!=i)
		{
			return false;
		}
		else if(row_num==i && $('#txtlcsc_'+i).val()=="")
		{
			return false;
		}
		else
		{
			i++;
	
			 $("#tbl_lc_list tbody tr:last").clone().find("input,select").each(function() {
				$(this).attr({
				  'id': function(_, id) { var id=id.split("_"); return id[0]+"_"+i; },
				  'name': function(_, name) { var name=name.split("_"); return name[0]+"_"+i;  },
				  'value': function(_, value) { return ""; }              
				});
				 
			  }).end().appendTo("#tbl_lc_list");
			  
			  $("#tbl_lc_list tbody tr:last").removeAttr('id').attr('id','tr_'+i);
			  
			  $('#txtlcsc_'+i).removeAttr("onDblClick").attr("onDblClick","onDblClick= openmypage(3,"+i+")");
			  $('#increase_'+i).removeAttr("value").attr("value","+");
			  $('#decrease_'+i).removeAttr("value").attr("value","-");
			  $('#increase_'+i).removeAttr("onclick").attr("onclick","javascript:fn_add_row_lc("+i+");");
			  $('#decrease_'+i).removeAttr("onclick").attr("onclick","javascript:fn_deleteRow("+i+");");
		}
	}
	
	
	function fn_deleteRow(rowNo)
	{   
		var numRow = $('table#tbl_lc_list tbody tr').length; 
		
		if(numRow==rowNo && rowNo!=1)
		{
		    $('#tbl_lc_list tbody tr:last').remove();
		  	var numRow = $('table#tbl_lc_list tbody tr').length; 
			$('#txt_tot_row').val(numRow);
			var ddd={ dec_type:4, comma:0, currency:$('#cbo_pi_currency_id').val()}
			math_operation( "totalLcScValue", "txtlcscvalue_", "+", numRow,ddd );
			calculate_all(numRow);
		}
		else
		{
			return false;
		}
		
	}
	
	function fnc_btb_mst( operation )
	{  
		if(operation==2)
		{
			show_msg('13');
			return;
		}
		
		var lc_basis = $('#cbo_lc_basis_id').val();
		var txt_last_shipment_date = $('#txt_last_shipment_date').val(); 
		var cbo_payterm_id = $('#cbo_payterm_id').val(); 
		var cbo_lc_type_id = $('#cbo_lc_type_id').val(); 
		
		if (form_validation('cbo_importer_id*application_date*cbo_issuing_bank*cbo_item_category_id*cbo_lc_basis_id*cbo_supplier_id*cbo_lc_type_id*txt_lc_value*cbo_inco_term_id*cbo_payterm_id*cbo_delevery_mode','Importer*Application Date*Issuing Bank*Item Category*L/C Basis*Supplier*L/C Type*L/C Value*Inco Term*Pay Term*Delevery Mode')==false)
		{
			return;
		}
		
		if(lc_basis == 1)
		{
			if(form_validation('txt_pi_value','PI Value')==false)
			{
				return;
			}
		}
		if($('#txt_bank_code').val()!='' || $('#txt_lc_year').val()!='' || $('#txt_category').val()!='' || $('#txt_lc_serial').val()!='')
		{  
			if(form_validation('txt_lc_date*txt_lc_expiry_date','L/C Date*L/C Expiry Date')==false)
			{
				return;
			}
		}
		if(txt_last_shipment_date!='')
		{
			
			if(form_validation('txt_lc_expiry_date','L/C Expiry Date')==false)
			{
				return;
			}
			
			if( date_compare($('#txt_last_shipment_date').val(), $('#txt_lc_expiry_date').val() )==false )
			{
				alert("Expiry date must be equal or higher than last shipment date");
				$('#txt_lc_expiry_date').focus();
				return;
			}
		}
		if(cbo_payterm_id==2)// || cbo_payterm_id==4 || cbo_payterm_id==6 || cbo_payterm_id==7 || cbo_payterm_id==9
		{
			if(form_validation('txt_tenor','Tenor')==false)
			{
				return;
			}
		}
		if(cbo_lc_type_id==2)
		{
			if(form_validation('txt_margin_deposit','Margin Deposit %')==false)
			{
				return;
			}
		} 
		 
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_system_id*cbo_importer_id*application_date*cbo_issuing_bank*cbo_item_category_id*cbo_lc_basis_id*txt_pi*txt_hidden_pi_id*txt_pi_value*cbo_pi_currency_id*cbo_supplier_id*cbo_lc_type_id*txt_bank_code*txt_lc_year*txt_category*txt_lc_serial*txt_lc_date*txt_last_shipment_date*txt_lc_expiry_date*txt_lc_value*cbo_lc_currency_id*cbo_inco_term_id*txt_inco_term_place*cbo_payterm_id*txt_tenor*txt_tolerance*cbo_delevery_mode*txt_doc_perc_days*txt_port_loading*txt_port_discharge*txt_etd_date*txt_lca_no*txt_lcaf_no*txt_imp_form_no*txt_insurance_company*txt_cover_note_no*txt_cover_note_date*txt_psi_company*cbo_maturit_from_id*txt_margin_deposit*cbo_origin_id*txt_shiping_mark*txt_gmt_qnty*cbo_gmt_uom_id*txt_ud_no*txt_ud_date*cbo_credit_advice_id*cbo_partial_ship_id*cbo_transhipment_id*cbo_credit_advice_id*txt_conf_bank*cbo_bond_warehouse_id*txt_remarks*cbo_status*update_id',"../../");
		 
		freeze_window(operation);
		http.open("POST","requires/btb_margin_lc_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_btb_mst_reponse;
	}

	function fnc_btb_mst_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split('**');
			show_msg(trim(reponse[0]));
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_system_id').value = reponse[2];
				$('#cbo_importer_id').attr('disabled','disabled');
				set_button_status(1, permission, 'fnc_btb_mst',1); 
			}
			release_freezing();
		}
	}

	function fnc_lc_details( operation )
	{  
		if(operation==2)
		{
			show_msg('13');
			return;
		}
		
		var update_id = $('#update_id').val();
	 	var row_num=$('#tbl_lc_list tbody tr').length;
		var totalCurrentDistri = $('#totalCurrentDistri').val();
		var txt_lc_value = $('#txt_lc_value').val();
		
		if(form_validation('txt_system_id','System ID')==false)
		{
			return;
		}
		else if(totalCurrentDistri*1>txt_lc_value*1)
		{
			alert("Total Current Distribution Value Can Not Exceed LC Value");
			$('#totalCurrentDistri').focus();
			return;
		}
		else
		{
			var data_all=""; var j=0;		
			for (var i=1; i<=row_num; i++)
			{ 
				if($('#txtLcScid_'+i).val()!="")
				{
					j++;
				}
				
				data_all=data_all+get_submitted_data_string('txtLcScid_'+i+'*txtlcscflagId_'+i+'*txtcurdistribution_'+i+'*cbostatus_'+i,"../../",i);
			} 
			
			if(j==0)
			{
				alert("Please Select SC/LC No");
				return;
			}
			
			var data="action=save_update_delete_dtls&operation="+operation+'&total_row='+row_num+'&update_id='+update_id + data_all;
			 
			freeze_window(operation); 
			http.open("POST","requires/btb_margin_lc_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_lc_details_reponse;
		}
	}
		 
	function fnc_lc_details_reponse()
	{
		if(http.readyState == 4) 
		{
			var response=http.responseText.split('**'); 
			show_msg(trim(response[0]));
			if(response[0]==0 || response[0]==1)
			{
				show_list_view($('#update_id').val(),'show_lc_listview','sc_lc_list_view','requires/btb_margin_lc_controller','');
				var numRow = $('table#tbl_lc_list tbody tr').length; 
				
				var ddd={ dec_type:4, comma:0, currency:$('#cbo_pi_currency_id').val()}
				
				math_operation( "totalLcScValue", "txtlcscvalue_", "+", numRow,ddd );
				math_operation( "totalCurrentDistri", "txtcurdistribution_", "+", numRow,ddd );
				math_operation( "totalCumuDisri", "txtcumudistribution_", "+", numRow,ddd );
				calculate_occupied(numRow);
				
				set_button_status(1, permission, 'fnc_lc_details',2); 
			}
			release_freezing();
			
		}
	}

	function set_port_loading_value(id)
	{
		if(id == 3 ||id == 4 ||id == 99)
		{
			$('#txt_port_loading').val('From Suppliers Factory');
			$('#txt_port_discharge').val('To Importers Factory');
		}
		else
		{
			$('#txt_port_loading').val('');
			$('#txt_port_discharge').val('');
		}  
	}
	
	function check_value_same(lc_val)
	{
		var pi_val = $('#txt_pi_value').val();
		
		if(lc_val!=pi_val)
		{
			alert('PI value and L/C value should be same');
			$('#txt_lc_value').val('');
			$('#txt_lc_value').focus();
		}
	}
	
	function active_inactive(str)
	{
		document.getElementById('txt_lc_value').value="";
		document.getElementById('txt_pi').value="";
		document.getElementById('txt_hidden_pi_id').value="";
		document.getElementById('txt_pi_value').value="";
		
		if(str==1)
		{
			document.getElementById('txt_lc_value').disabled=true;
			document.getElementById('txt_pi').disabled=false;
		}
		else
		{
			document.getElementById('txt_lc_value').disabled=false;
			document.getElementById('txt_pi').disabled=true;
		}
	}
	
	function fnc_move_cursor(val, field_id, lnth)
	{
		var str_length=val.length;
		if(str_length==lnth)
		{
			$('#'+field_id).select();
			$('#'+field_id).focus();
		}
	}
</script>
<style>
	#tbl_btb input
	{
		width:155px;
	}
</style>

<body onLoad="set_hotkey()">
    <div align="center" style="width:900px;">
        <?php echo load_freeze_divs ("../../",$permission); ?>
        <div>
            <form name="btbmargin_1" id="btbmargin_1" autocomplete="off"> 
                <fieldset style="width:1024px;">
                    <legend>BTB / Margin LC</legend>
                    <table width="100%" border="0" cellpadding="0" cellspacing="2" id="tbl_btb">
                    	<tr>
                        	<td></td>
                            <td></td>
                            <td align="right"><strong>System ID</strong></td>
                            <td><input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" placeholder="Double Click to Search L/C" onDblClick="openmypage(1,'')" readonly /></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr height="10"></tr>
                        <tr> 
                            <td width="150" class="must_entry_caption">Importer</td>
                            <td width="120">
                                 <?php echo create_drop_down( "cbo_importer_id", 165,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, '----Select----',0,"",0); ?>       
                             
                            </td>
                            <td width="150" class="must_entry_caption">Application Date</td>
                            <td width="120"><input type="text" name="application_date" id="application_date" class="datepicker" /></td>
                            <td width="150" class="must_entry_caption">Issuing Bank</td>
                            <td>	
                                <?php echo create_drop_down( "cbo_issuing_bank", 165,"select id,bank_name from lib_bank where is_deleted=0 and status_active=1 and issusing_bank = 1 order by bank_name",'id,bank_name', 1, '----Select----',0,0,0); ?>       
                            </td>						
                        </tr>
                        <tr>
                        	<td class="must_entry_caption">Item Category</td>
                            <td> 
                                 <?php echo create_drop_down( "cbo_item_category_id", 165, $item_category,'', 1, '----Select----',0,"load_drop_down( 'requires/btb_margin_lc_controller',document.getElementById('cbo_importer_id').value+'_'+this.value, 'load_supplier_dropdown', 'supplier_td' );",0); ?>  
                            </td>
                            <td class="must_entry_caption">LC Basis</td>
                            <td> 
                                 <?php echo create_drop_down( "cbo_lc_basis_id", 165, $lc_basis,'', 0,'',1,"active_inactive(this.value)",0); ?>  
                            </td>
                            <td>Pro Forma Invoice</td>
                            <td>
                            <input type="text" name="txt_pi" id="txt_pi" class="text_boxes" placeholder="Double Click for PI" onDblClick="openmypage(2,'')" readonly />
                            <input type="hidden" name="txt_hidden_pi_id" id="txt_hidden_pi_id" class="text_boxes"  />
                            </td>
                        </tr>
                        <tr>
                        	<td>PI Value</td>
                            <td>
                            	<input type="text" name="txt_pi_value" id="txt_pi_value" class="text_boxes_numeric"  style="width:80px" disabled />
                                <?php echo create_drop_down( "cbo_pi_currency_id",70,$currency,'',0,'',2,"",0); ?>  
                            </td>
                        	<td class="must_entry_caption">Supplier</td>
                            <td id="supplier_td">	
                                <?php echo create_drop_down( "cbo_supplier_id", 165,$blank_array,'', 1, 'Select',0,0,0); ?>       
                            </td>
                            <td class="must_entry_caption">L/C Type</td>
                             <td>
                                 <?php echo create_drop_down( "cbo_lc_type_id",165,$lc_type,'',1,'-Select',1,"",0); ?>  
                            </td>		
                        </tr>
                        <tr>
                        	<td>L/C Number</td>
                            <td>
                            	<input type="text" name="txt_bank_code" id="txt_bank_code" class="text_boxes_numeric" maxlength="4" style="width:30px" onKeyUp="fnc_move_cursor(this.value,'txt_lc_year',4)" />
                                <input type="text" name="txt_lc_year" id="txt_lc_year" class="text_boxes_numeric" maxlength="2"  style="width:16px" onKeyUp="fnc_move_cursor(this.value,'txt_category',2)"/>
                                <input type="text" name="txt_category" id="txt_category" class="text_boxes_numeric" maxlength="2" style="width:16px" onKeyUp="fnc_move_cursor(this.value,'txt_lc_serial',2)"/>
                                <input type="text" name="txt_lc_serial" id="txt_lc_serial" class="text_boxes" maxlength="25" style="width:45px"/>
                            </td>
                            <td class="must_entry_caption">L/C Date</td>
                            <td><input type="text" name="txt_lc_date" id="txt_lc_date" class="datepicker" value=""/></td>
                            <td>Last Shipment Date</td>
                            <td><input type="text" name="txt_last_shipment_date" id="txt_last_shipment_date" class="datepicker" value="" onChange="add_days(this.value,15,1,'txt_lc_expiry_date')" /></td>
                        </tr>
                        <tr>
                        	<td height="24" class="must_entry_caption">L/C Expiry Date</td>
                          <td><input type="text" name="txt_lc_expiry_date"  id="txt_lc_expiry_date" class="datepicker" value="" /></td>
                            <td class="must_entry_caption">L/C Value</td>
                            <td>
                            	<input type="text" name="txt_lc_value" id="txt_lc_value" class="text_boxes_numeric"  style="width:80px" disabled />
                                <?php echo create_drop_down( "cbo_lc_currency_id",70,$currency,'',0,'',2,0,0); ?>  
                            </td>
                            <td class="must_entry_caption">Inco Term</td>
							<td><?php echo create_drop_down( "cbo_inco_term_id",165,$incoterm,'',1,'-Select-',0,0,0); ?> </td>
                        </tr>
                        <tr>
                        	<td>Inco Term Place</td>
							<td><input type="text"  name="txt_inco_term_place" id="txt_inco_term_place" class="text_boxes" /></td>
                            <td class="must_entry_caption">Pay Term</td>
							<td><?php echo create_drop_down( "cbo_payterm_id",165,$pay_term,'',1,'-Select-',0,"",0,'');//set_port_loading_value(this.value)1,2 ?></td>  
                            <td class="must_entry_caption">Tenor</td>
							<td><input type="text"  name="txt_tenor" id="txt_tenor" class="text_boxes_numeric" /></td>
                        </tr>
                        <tr>
                        	<td>Tolerance %</td>
							<td><input type="text"  name="txt_tolerance" id="txt_tolerance" class="text_boxes_numeric" value="5" /></td>
                            <td class="must_entry_caption">Delivery Mode</td>
							<td><?php echo create_drop_down( "cbo_delevery_mode",165,$shipment_mode,'',1,'-Select-',1,0,0); ?></td>  
                            <td>Doc Present Days</td>
							<td><input type="text"  name="txt_doc_perc_days" id="txt_doc_perc_days" class="text_boxes" /></td>
                        </tr>
                        <tr>
                        	<td>Port of Loading</td>
							<td><input type="text"  name="txt_port_loading" id="txt_port_loading" class="text_boxes" maxlength="50" /></td>
                            <td>Port of Discharge</td>
							<td><input type="text"  name="txt_port_discharge" id="txt_port_discharge" class="text_boxes"  maxlength="50" /></td>
                            <td>ETD Date</td>
                            <td><input type="text" name="txt_etd_date" id="txt_etd_date" class="datepicker" value="" /></td>
                        </tr>
                        <tr>
                        	<td>LCA No</td>
                            <td><input type="text" name="txt_lca_no" id="txt_lca_no" class="text_boxes" value=""  maxlength="30" /></td>
                            <td>LCAF No</td>
                            <td><input type="text" name="txt_lcaf_no" id="txt_lcaf_no" class="text_boxes" value=""  maxlength="30" /></td>
                            <td>IMP Form No</td>
                            <td><input type="text" name="txt_imp_form_no" id="txt_imp_form_no" class="text_boxes" value=""  maxlength="50" /></td>
                        </tr>
                        <tr>
                        	<td>Insurance Company</td>
                            <td><input type="text" name="txt_insurance_company" id="txt_insurance_company" class="text_boxes" value=""  maxlength="50" /></td>
                            <td>Cover Note No</td>
                            <td><input type="text" name="txt_cover_note_no" id="txt_cover_note_no" class="text_boxes" value=""  maxlength="50" /></td>
                            <td>Cover Note Date</td>
                            <td><input type="text" name="txt_cover_note_date" id="txt_cover_note_date" class="datepicker" value="" /></td>
                        </tr>
                        <tr>
                        	<td>PSI Company</td>
							<td><input type="text" name="txt_psi_company" id="txt_psi_company" class="text_boxes" value=""  maxlength="50" /></td>
                            <td>Maturity From</td>
							<td><?php echo create_drop_down( "cbo_maturit_from_id",165,$maturity_from,'',1,'-Select-',0,0,0); ?></td>
                            <td class="must_entry_caption">Margin Deposit %</td>
							<td><input type="text" name="txt_margin_deposit" id="txt_margin_deposit" class="text_boxes_numeric" value="" /></td>
                        </tr>
                         <tr>
                        	<td>Origin</td>
							<td><?php echo create_drop_down( "cbo_origin_id",165,"SELECT id,country_name from lib_country order by country_name",'id,country_name',1,'-Select-',0,0,0); ?></td>
                            <td>Shipping Mark</td>
							<td><input type="text" name="txt_shiping_mark" id="txt_shiping_mark" class="text_boxes" value=""  maxlength="200" /></td>
                            <td>Garments Qnty & UOM</td>
							<td>
                            	<input type="text" name="txt_gmt_qnty" id="txt_gmt_qnty" class="text_boxes_numeric" value="" style="width:90px;" />
                                <?php echo create_drop_down( "cbo_gmt_uom_id",60,$unit_of_measurement,'',0,'',1,'',1,0); ?>
                            </td>
                        </tr>
                         <tr>
                        	<td>UD No</td>
							<td><input type="text" name="txt_ud_no" id="txt_ud_no" class="text_boxes" value=""  maxlength="50" /></td>
                            <td>UD Date</td>
							<td><input type="text" name="txt_ud_date" id="txt_ud_date" class="datepicker" value="" /></td>
							<td>Credit To Be Advised</td>
							<td><?php echo create_drop_down( "cbo_credit_advice_id",165,$credit_to_be_advised,'',0,'',1,0,0); ?></td>
                        </tr>
                        <tr>
                          <td>Partial Shipment</td>
                          <td><?php echo create_drop_down( "cbo_partial_ship_id",165,$yes_no,'',0,'',2,0,0); ?></td>
                          <td>Transhipment</td>
                          <td><?php echo create_drop_down( "cbo_transhipment_id",165,$yes_no,'',0,'',1,0,0); ?></td>
                          <td>Add Confirmation Req.</td>
                          <td><?php echo create_drop_down( "cbo_credit_advice_id",165,$yes_no,'',0,'',2,0,0); ?></td>
                       	</tr>
                        <tr>
                          <td>Add Confirming Bank</td>
                          <td><input type="text" name="txt_conf_bank" id="txt_conf_bank" class="text_boxes" value=""  maxlength="50" /></td>
                          <td>Bonded Warehouse</td>
                          <td><?php echo create_drop_down( "cbo_bond_warehouse_id",165,$yes_no,'',0,'',2,0,0); ?></td>
                          <td>Status</td>
                          <td>
                            <?php echo create_drop_down( "cbo_status", 165, $row_status,'', 0, '',1,0,'1'); ?> 
                          </td>
                        </tr>
                        <tr>
                        	<td>Remarks</td>
                        	<td colspan="5"><input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" value="" style="width:795px"  maxlength="200"/></td>
                        </tr> 
                         <tr>
                         	<td colspan="6" height="15"></td>
                         </tr>
                        <tr>
                            <td colspan="6" height="50" valign="middle" align="center" class="button_container">						
                                <input type="hidden" name="update_id" id="update_id" value=""/> 
                                <?php 
							  		echo load_submit_buttons( $permission, "fnc_btb_mst", 0,0 ,"reset_form('btbmargin_1*lcform_2','pi_details_list','','','disable_enable_fields(\'cbo_importer_id*txt_last_shipment_date*txt_lc_expiry_date*cbo_delevery_mode*cbo_inco_term_id*txt_inco_term_place*cbo_delevery_mode*txt_port_of_loading*txt_port_of_discharge*cbo_payterm_id*txt_tenor*txt_pi*txt_remarks\',0)');active_inactive(1);$('#tbl_order_list tbody tr:not(:first)').remove();",1) ; 
							    ?>
                             </td>                          			
                        </tr>                        
                    </table>
                </fieldset>
            </form>
            <form name="lcform_2" id="lcform_2" autocomplete="off">
                <fieldset style="width:1054px; margin-top:10px;">
                    <legend>LC/SC attached</legend>
                    	<div id="lc_details_container" style="max-height:350px; overflow:auto;">                  
                      		<table>
                            	<tr>
                                    <td align="center" colspan="8" style="padding-bottom:10px;">
                                        <strong>Distribution Method:</strong>
                                      <input type="radio" name="distribution_type" id="distribution_type_0" value="0" onClick="distribution_value(this.value,0)" checked /><label for="distribution_type_0">Proportionately</label>
                                        <input type="radio" name="distribution_type" id="distribution_type_1" value="1" onClick="distribution_value(this.value,0)"  /><label for="distribution_type_1">Manually</label>
                                    </td>
                                </tr>
                            </table>
                          <div style="max-height:130px;overflow:auto;">  
                            <table width="100%" cellspacing="0" cellpadding="0" class="rpt_table" id="tbl_lc_list">
                                <thead>
                                    <tr>
                                        <th width="120">SC/LC No</th>
                                        <th width="120">Buyer</th>
                                        <th width="70">LC/SC</th>
                                        <th width="120">LC/SC Value</th>
                                        <th width="120">Current Distribution</th>
                                        <th width="120">Cumulative Distribution</th>
                                        <th width="100">Occupied %</th> 
                                        <th width="100">Status</th> 
                                        <th></th>
                                    </tr>                         
                                </thead>
                                <tbody id="sc_lc_list_view">
                                    <tr class="general">
                                        <td>
                                        <input type="text" name="txtlcsc_1" id="txtlcsc_1" class="text_boxes" style="width:100px"  onDblClick= "openmypage(3,1)" readonly= "readonly" placeholder="Double For Search" />
                                        <input type="hidden" name="txtLcScid_1" id="txtLcScid_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
                                        </td>
                                        <td><input type="text" name="txtbuyer_1" id="txtbuyer_1" class="text_boxes" style="width:120px;" readonly= "readonly" /></td>
                                        <td>
                                        <input type="text" name="txtlcscflag_1" id="txtlcscflag_1" class="text_boxes" style="width:70px;" readonly= "readonly" />
                                        <input type="hidden" name="txtlcscflagId_1" id="txtlcscflagId_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
                                        </td>
                                        <td><input type="text" name="txtlcscvalue_1" id="txtlcscvalue_1" class="text_boxes_numeric" style="width:120px;" readonly= "readonly"/></td>
                                        <td><input type="text" name="txtcurdistribution[]" id="txtcurdistribution_1" class="text_boxes_numeric" style="width:120px" disabled="disabled"/></td>
                                        <td>
                                        <input type="text" name="txtcumudistribution_1" id="txtcumudistribution_1" class="text_boxes_numeric" style="width:120px" readonly= "readonly"/>
                                        <input type="hidden" name="hiddencumudistribution_1" id="hiddencumudistribution_1" class="text_boxes_numeric" style="width:120px"/>
                                        </td>
                                        <td><input type="text" name="txtoccupied_1" id="txtoccupied_1"  style="width:100px" class="text_boxes_numeric" readonly= "readonly"/></td>  
                                             
                                        <td>                             
                                            <?php 
                                                 echo create_drop_down( "cbostatus_1", 100, $row_status,"", 0, "", 1, "" );
                                            ?>
                                        </td> 
                                        <td width="65">
                                            <input type="button" id="increase_1" name="increase_1" style="width:25px" class="formbuttonplasminus" value="+" onClick="fn_add_row_lc(0)" />
                                            <input type="button" id="decrease_1" name="decrease_1" style="width:25px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(0)" />
                                            <input type="hidden" name="txtcaltype_1" id="txtcaltype_1" class="text_boxes" style="width:100px" value="0" readonly= "readonly" />
                                       </td>                       
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr class="tbl_bottom">
                                      <td colspan="3">Total</td>
                                      <td align="center"><input type="text" name="totalLcScValue" id="totalLcScValue" class="text_boxes_numeric" style="width:120px;" readonly= "readonly" /></td>
                                      <td align="center"><input type="text" name="totalCurrentDistri" id="totalCurrentDistri" class="text_boxes_numeric" style="width:120px;" readonly= "readonly" /></td> 
                                      <td align="center"><input type="text" name="totalCumuDisri" id="totalCumuDisri" class="text_boxes_numeric" style="width:120px;" readonly= "readonly" /></td>
                                      <td colspan="3"><input type="hidden" name="txt_tot_row" id="txt_tot_row" class="text_boxes_numeric"  readonly= "readonly" value="0" /></td>
                                    </tr> 
                                </tfoot>
                            </table> 
                         </div> 
           				<div style="max-height:100px;overflow:hidden;">   
                           <table>
                           		 <tr>
                                    <td colspan="11" height="50" valign="middle" align="center" class="button_container">
                                    <?php echo load_submit_buttons( $permission, "fnc_lc_details", 0,0 ,"reset_form('lcform_2','','','','$(\'#tbl_lc_list tbody tr:not(:first)\').remove();')",2) ; ?>
                                    </td>
                                </tr>
                           </table>
                   		</div> 
                     </div>   
                </fieldset>
            </form>
             <form name="pimasterform_3" id="pimasterform_3" autocomplete="off">
                <fieldset style="width:1050px; margin-top:10px;">
                    <legend>PI Item List</legend>
                    <div id="pi_details_list" style="max-height:200px; overflow:auto;"></div>
                </fieldset>
            </form> 
        </div>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>