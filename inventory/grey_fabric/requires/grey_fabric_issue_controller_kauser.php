﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------
//load drop down knitting company
if ($action=="load_drop_down_knit_com")
{
	$exDataArr = explode("**",$data);	
	$knit_source=$exDataArr[0];
	$company=$exDataArr[1];
	$issuePurpose=$exDataArr[2];
	if($company=="" || $company==0) $company_cod = ""; else $company_cod = " and id=$company";
	if($knit_source==1)
		echo create_drop_down( "cbo_dyeing_company", 170, "select id,company_name from lib_company where status_active=1 and is_deleted=0 $company_cod order by company_name","id,company_name", 1, "-- Select --", $company, "" );
	else if($knit_source==3 && $issuePurpose==1)
		echo create_drop_down( "cbo_dyeing_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,20) and a.status_active=1 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3 && $issuePurpose==2)
		echo create_drop_down( "cbo_dyeing_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,21,24) and a.status_active=1 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3)		
		echo create_drop_down( "cbo_dyeing_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and a.status_active=1 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else
		echo create_drop_down( "cbo_dyeing_company", 170, $blank_array,"", 1, "-- Select --", 0, "",0 );
	exit();	
}


//load drop down store
if ($action=="load_drop_down_store")
{	  
	echo create_drop_down( "cbo_store_name", 170, "select id,store_name from lib_store_location where status_active=1 and is_deleted=0 and FIND_IN_SET($data,company_id) and FIND_IN_SET(13,item_category_id) order by store_name","id,store_name", 1, "-- Select Store--", 0, "",0 );  	 
	exit();
}

//load drop down Color
if ($action=="load_drop_down_color")
{
	$data=explode("_",$data);
	$all_po_id=$data[0];
	$color_id=$data[1];
	//echo $data[0];
/*	if ($all_po_id!='' || $all_po_id!=0)
	{
		$sql="select c.id, c.color_name from wo_booking_mst a, wo_booking_dtls b, lib_color c where a.booking_no=b.booking_no and b.fabric_color_id=c.id and a.po_break_down_id in($all_po_id) and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by c.id";
		echo create_drop_down( "cbo_color_id", 170, $sql,"id,color_name", 1, "-- Select Color --", $color_id, "",0 );
	}
	else
	{
*/		 $sql="select c.id, c.color_name from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b, lib_color c where a.booking_no =b.booking_no and b.gmts_color=c.id  and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by c.id";
		echo create_drop_down( "cbo_color_id", 170, $sql,"id,color_name", 1, "-- Select Color --", $color_id, "",0 ); 
	//}
	exit();
}

if ($action=="is_roll_maintain")
{	  
	$sql = sql_select("select variable_list, fabric_roll_level, batch_maintained from variable_settings_production
			where company_name=$data and variable_list in (3,13) and status_active=1 and is_deleted=0"); 
				
	//echo $sql;
	echo "$('#hidden_is_roll_maintain').val(0);\n";
	echo "$('#hidden_is_batch_maintain').val(0);\n";
	foreach($sql as $key=>$val)
	{
		if($val[csf('variable_list')]==3) echo "$('#hidden_is_roll_maintain').val(".$val[csf("fabric_roll_level")].");\n";
 		if($val[csf('variable_list')]==13) echo "$('#hidden_is_batch_maintain').val(".$val[csf("batch_maintained")].");\n";
	}	
	echo "new_item_controll();\n";
	exit();
}
//load drop down supplier
if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier", 170, "select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company=$data and b.party_type in (1,20,90) and c.status_active=1 and c.is_deleted=0","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

//batch pop up here-------------//
if ($action=="batch_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
   	 
?>
     
<script>
	
	function js_set_value(id,batchNo)
	{		
		$("#txt_batch_id").val(id);
		$("#txt_batch_no").val(batchNo);
		parent.emailwindow.hide();
 	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter Batch No</th>                    
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">
                        <?php  
                            $sql="select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$cbo_company_id $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name";
							//echo $sql;
							//$search_by = array(1=>'Batch No', 2=>'Buyer');
							$search_by = array(1=>'Batch No');
							$dd="change_search_event(this.value, '0*1', '0*".$sql."', '../../../')";
							echo create_drop_down("cbo_search_by", 120, $search_by, "", 0, "--Select--", "", $dd, 0);
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td"> 
                        <input type="text" id="txt_search_common" name="txt_search_common" class="text_boxes" style="width:100px" />
                    </td>                    
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" style="width:100px;" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+'<?php echo $cbo_company_id; ?>', 'create_batch_search_list', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'list_view\',-1)');" />				
                    	
                        <!-- Hidden field here-------->
                        <input type="hidden" id="txt_batch_id" value="" /> 
                        <input type="hidden" id="txt_batch_no" value="" />
                        <!-- ---------END------------->
                    </td>
            	</tr>  
                 
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_batch_search_list")
{ 
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
 	$company = $ex_data[2]; // company id
 
  	$cond="";
  	if($txt_search_by==1 && $txt_search_common!="")
		$cond = " and a.batch_no like '%$txt_search_common%'"; 
	if($txt_search_by==2 && $txt_search_common!=0)
		$cond = " and a.buyer_name='$txt_search_common'"; 
 	
	$color_arr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	
  	$sql = "select a.id, a.batch_no, a.booking_no, a.color_id, GROUP_CONCAT(c.po_number) as po_number
 			from 
				pro_batch_create_mst a, pro_batch_create_dtls b left join wo_po_break_down c on b.po_id=c.id
			where  
				a.id=b.mst_id and a.company_id=$company $cond
				group by a.batch_no 
				order by a.batch_no";
 	//echo $sql;die;
	$result = sql_select($sql);
	
  	?> 
    <div align="left" style="margin-left:50px">    
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
        <thead>
            <th width="50">SL</th>
            <th width="100">Batch No</th>
            <th width="100">Color</th>               
            <th width="120">Booking No</th>
            <th width="250">Po Number</th>
        </thead>
	</table>
    
	<div style="width:770px; max-height:250px; overflow-y:scroll" id="list_container_batch" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="list_view">  
        <?php
            $i=1;
            $previouDataRow="";
			foreach ($result as $row)
            {  
                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
				 
          		?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')];?>,'<?php echo $row[csf('batch_no')];?>')"  > 
                    <td width="50"><?php echo $i; ?>  </td>  
                    <td width="100"><p><?php echo $row[csf('batch_no')]; ?></p></td>
                    <td width="100"><p><?php echo $color_arr[$row[csf('color_id')]]; ?></p></td>               
                    <td width="120"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                    <td width="250"><p><?php echo $row[csf('po_number')]; ?></p></td> 
                 </tr> 
				<?php 
                $i++;
            }
        	?> 
        </table>        
      </div>
    </div>
	<?php	
	exit();	
}

//load drop down knitting company
if ($action=="load_drop_down_knit_com")
{
	$exDataArr = explode("**",$data);	
	$knit_source=$exDataArr[0];
	$company=$exDataArr[1];
	$issuePurpose=$exDataArr[2];
	if($company=="" || $company==0) $company_cod = ""; else $company_cod = " and id=$company";
	if($knit_source==1)
		echo create_drop_down( "cbo_knitting_company", 170, "select id,company_name from lib_company where status_active=1 and is_deleted=0 $company_cod order by company_name","id,company_name", 1, "-- Select --", $company, "" );
	else if($knit_source==3 && $issuePurpose==1)
		echo create_drop_down( "cbo_knitting_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,20) and a.status_active=1 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3 && $issuePurpose==2)
		echo create_drop_down( "cbo_knitting_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,21,24) and a.status_active=1 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3)		
		echo create_drop_down( "cbo_knitting_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and a.status_active=1 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	exit();	
}
 
// wo/pi popup here----------------------// 
if ($action=="fabbook_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	
	function fn_check()
	{
		/*if(form_validation('cbo_buyer_name','Buyer Name')==false )
			return;
		else*/
			show_list_view ( document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $issue_purpose; ?>, 'create_fabbook_search_list_view', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'list_view\',-1)');
	}
		
	function js_set_value(booking_dtls)
	{ 
 		//$("#hidden_booking_id").val(booking_id);  
		$("#hidden_booking_number").val(booking_dtls);  
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Buyer Name</th>
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                           $sql = "";
						   echo create_drop_down( "cbo_buyer_name", 170, "select a.id,a.buyer_name from lib_buyer a, lib_buyer_party_type b where a.id=b.buyer_id and a.status_active=1 and a.is_deleted=0 and FIND_IN_SET($company,tag_company) and b.party_type in (1,3,21,90) group by a.id order by buyer_name ","id,buyer_name", 1, "-- Select --", $selected, "" );
                        ?>
                    </td>
                    <td>
                        <?php  
                            $search_by = array(1=>'Booking No', 2=>'Buyer Order', 3=>'Job No');
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../../')";
							echo create_drop_down( "cbo_search_by", 120, $search_by, "", 0, "--Select--", "", $dd, 0);
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="fn_check()" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_booking_id" value="" />
                    <input type="hidden" id="hidden_booking_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_fabbook_search_list_view")
{
	
 	$ex_data = explode("_",$data);
	$buyer = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = trim($ex_data[2]);
	$txt_date_from = $ex_data[3];
	$txt_date_to = $ex_data[4];
	$company = $ex_data[5];
	$booking_type = $ex_data[6];
 	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for Booking No
		{
			$sql_cond .= " and a.booking_no LIKE '%$txt_search_common%'";	
 		}
		else if(trim($txt_search_by)==2) // for buyer order
		{
			$sql_cond .= " and b.po_number LIKE '%$txt_search_common%'";	// wo_po_break_down			
 		}
		else if(trim($txt_search_by)==3) // for job no
		{
			$sql_cond .= " and a.job_no LIKE '%$txt_search_common%'";				
 		}
 	} 
 	
	if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if( trim($buyer)!=0 ) $sql_cond .= " and a.buyer_id='$buyer'";
	if( trim($company)!=0 ) $sql_cond .= " and a.company_id='$company'";
	if( trim($booking_type)==1 ) $sql_cond .= " and a.booking_type!=4";
	else if( trim($booking_type)==4 ) $sql_cond .= " and a.booking_type=4";
 	
  	if( trim($booking_type)==8 )
	{
		$sql = "select a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no as  job_no_mst
			from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b 
			where 
				a.booking_no=b.booking_no and 
 				a.item_category=2 and 
 				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 
				$sql_cond 
				group by a.id"; 
	}
	else
	{
		$sql = "select a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, b.job_no_mst 
			from wo_booking_mst a, wo_po_break_down b 
			where 
				a.job_no=b.job_no_mst and 
 				a.item_category=2 and 
 				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 
				$sql_cond 
				group by a.id"; 
	}
	//item_category=2 knit fabrics 
	//echo $sql;die;
	$result = sql_select($sql);
	$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	?>
    <div align="left">
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" >
        <thead>
            <th width="30">SL</th>
            <th width="105">Booking No</th>
            <th width="90">Book. Date</th>               
            <th width="100">Buyer</th>
            <th width="90">Item Cat.</th>
            <th width="90">Job No</th>
            <th width="90">Order Qnty</th>
            <th width="80">Ship. Date</th>
            <th >Order No</th>
        </thead>
	</table>
    
	<div style="width:990px; max-height:240px; overflow-y:scroll" id="list_container_batch" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" id="list_view">  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	 
                 
                $po_qnty_in_pcs=0; $po_no='';$po_no_id=''; $min_shipment_date='';
                if( trim($booking_type)!=8 )
				{
					$po_sql = "select a.style_ref_no, b.po_number,b.id, b.pub_shipment_date, b.po_quantity, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($row[po_break_down_id]) group by b.id";	 							
					$nameArray=sql_select($po_sql);
					$style_ref_no="";
					foreach ($nameArray as $po_row)
					{
						if($po_no=="") $po_no=$po_row[csf('po_number')]; else $po_no.=",".$po_row[csf('po_number')];
						if($po_no_id=="") $po_no_id=$po_row[csf('id')]; else $po_no_id.=",".$po_row[csf('id')];
						
						if($min_shipment_date=='')
						{
							$min_shipment_date=$po_row[csf('pub_shipment_date')];
						}
						else
						{
							if($po_row[csf('pub_shipment_date')]<$min_shipment_date) $min_shipment_date=$po_row[csf('pub_shipment_date')]; else $min_shipment_date=$min_shipment_date;
						}
						
						$po_qnty_in_pcs+=$po_row[csf('po_qnty_in_pcs')];
						$style_ref_no = $po_row[csf('style_ref_no')];
					}
				}
				if ( trim($booking_type)==8 && $row[csf('id')]!=0)
				{
					$color_name = return_field_value("group_concat(color_name) as color_name","wo_non_order_info_dtls","mst_id=".$row[csf('id')]." and status_active=1 and is_deleted=0","color_name");
				}
				$color_name_arr=array();
				if ($color_name!='')
				{
					$color_name_arr=explode(',',$color_name);
				}
				//var_dump($color_name_arr);
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]; ?>_<?php echo $row[csf('booking_no')]; ?>_<?php echo $row[csf('buyer_id')]; ?>_<?php echo $style_ref_no; ?>_<?php echo $po_no; ?>_<?php echo $po_no_id; ?>_<?php echo $color_name_arr; ?>');">
                    <td width="30"><?php echo $i; ?></td>
                    <td width="105"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                    <td width="90" align="center"><?php echo change_date_format($row[csf('booking_date')]); ?></td>               
                    <td width="100"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                    <td width="90"><p><?php echo $item_category[$row[csf('item_category')]]; ?></p></td>
                    <td width="90"><p><?php echo $row[csf('job_no_mst')]; ?></p></td>
                    <td width="90" align="right"><?php echo $po_qnty_in_pcs; ?></td>
                    <td width="80" align="center"><?php echo change_date_format($min_shipment_date); ?></td>
                    <td><p><?php echo $po_no; ?></p></td>
                </tr>
        	<?php
            $i++;
            }
        	?>
        </table>
      </div>
    </div>
	<?php	
	exit();		
}



//Roll Pop up Search Here----------------------------------// 
if ($action=="roll_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
	
	//for update mode -----------
	$concate = $cbo_company_id;
	if(!$hidden_roll_id) $$hidden_roll_id="";
	if(!$hidden_roll_qnty) $hidden_roll_qnty="";
	if(!$txt_batch_id) $txt_batch_id="";
	$concate .= "**".$hidden_roll_id."**".$hidden_roll_qnty."**".$txt_batch_id;
	 
?>
     
<script>
	
	function fn_show_check()
	{		
		show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+'<?php echo $concate; ?>', 'create_roll_search_list', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'list_view\',-1)');
		set_previous_data();
	}
	
	
	function set_previous_data()
	{		 
		var old=document.getElementById('previous_data_row_id').value;
		if(old!="")
		{   
			old=old.split(",");
			for(var i=0; i<old.length; i++)
			{  
				fn_onkeyup(old[i]);
				//js_set_value( old[i] ) 
			}
		}
	}
		 
	
	function js_set_value( str ) 
	{ 
		var chkProduct = $("#txt_prod_id"+str).val();
		var po_number = $("#txt_po_number"+str).val();
		var flag=0;
		$("input[name='chk[]']:checked").each(function ()
		{
			 var rid = $(this).attr('id'); 
			 var ridArr = rid.split("_");
			 var str = ridArr[1];
			 if($("#txt_prod_id"+str).val()!=chkProduct && chkProduct!="")
			 {
				 alert("Product Mix Not Allow");
				 flag=1;
				 return false;
			 }
			 /*else if($("#txt_po_number"+str).val()!=po_number && po_number!="")
			 {
				 alert("Order No Mix Not Allow");
				 flag=1;
				 return false;
			 }  */
		});
		
		if(flag==1) return;
		 
		if($("#txt_issue_qnty_"+str).val()*1 <= 0 || $("#txt_issue_qnty_"+str).val()=='-')
		{
			document.getElementById('chk_'+str).checked=false;
			document.getElementById( 'search' + str ).style.backgroundColor = '#FFFFCC';
			return;
		}
		  
		if( document.getElementById("chk_"+str).checked )
		{
			document.getElementById('chk_'+str).checked=false;
			document.getElementById( 'search' + str ).style.backgroundColor = '#FFFFCC';
		}
		else
		{
			document.getElementById('chk_'+str).checked=true;
			document.getElementById( 'search' + str ).style.backgroundColor = 'yellow';
 		}
		 
 	}
	
	
	function fn_onkeyup(str)
	 { 
		 
		 if( $("#txt_issue_qnty_"+str).val()*1 > $("#txt_balance_qnty_"+str).val()*1 )
		 {
			 $("#txt_issue_qnty_"+str).val($("#txt_balance_qnty_"+str).val());
			 return;
		 }
		 else if($("#txt_issue_qnty_"+str).val()*1<=0 || $("#txt_issue_qnty_"+str).val()=='-')
		 {
			document.getElementById('chk_'+str).checked=false;
			document.getElementById( 'search' + str ).style.backgroundColor = '#FFFFCC';
			return;
		 }
		 
		 var chkProduct = $("#txt_prod_id"+str).val();
		 var po_number = $("#txt_po_number"+str).val();
		 $("input[name='chk[]']:checked").each(function ()
		 {
			 var rid = $(this).attr('id'); 
			 var ridArr = rid.split("_");
			 var str = ridArr[1];
			 if($("#txt_prod_id"+str).val()!=chkProduct && chkProduct!="")
			 {
				 alert("Product Mix Not Allow");
				 return false;
			 }
			 /*else if($("#txt_po_number"+str).val()!=po_number && po_number!="")
			 {
				 alert("Order No Mix Not Allow");
				 return false;
			 }*/ 
			 
		 });
		 
		 document.getElementById('chk_'+str).checked=true;
		 document.getElementById('search'+str).style.backgroundColor='yellow';
	 }
	 
	 
	var selected_id = new Array;
	var issue_qnty = new Array;
	var chkProduct = "";
	
	function fnonClose()
	 {
		$("input[name='chk[]']:checked").each(function ()
		{
				 
			var rid = $(this).attr('id'); 
			var ridArr = rid.split("_");
			var str = ridArr[1];
			 
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				issue_qnty.push( $('#txt_issue_qnty_' + str).val() ); 
			}
			else 
			{
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() )
					{ 
 						break;
					}
				}
				selected_id.splice( i, 1 );
				issue_qnty.splice( i, 1 ); 
			}
			
			var id = qnty = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';	
				qnty += issue_qnty[i] + ',';				
			}
			id = id.substr( 0, id.length - 1 );		
			qnty = qnty.substr( 0, qnty.length - 1 );			
			
			$('#txt_selected_id').val( id );	
			$('#txt_issue_qnty').val( qnty ); 
		});
		
		parent.emailwindow.hide();
	 }
	
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Select Buyer</th>                    
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">
                        <?php  
                            $sql="select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$cbo_company_id $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name";
							//echo $sql;
							$search_by = array(1=>'Buyer', 2=>'Job No', 3=>'Style Ref');
							$dd="change_search_event(this.value, '1*0*0', '".$sql."*0*0', '../../../')";
							echo create_drop_down("cbo_search_by", 120, $search_by, "", 1, "--Select--", "", $dd, 0);
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">		
                    	<?php
							//echo create_drop_down( "txt_search_common", 120, $blank_array, "id,buyer_name", 0, "--Select--", "", "", 0);
						?> 
                        <input type="text" id="txt_search_common" name="txt_search_common" class="text_boxes" style="width:100px" />
                    </td>                    
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" style="width:100px;" onClick="fn_show_check()" />				
                    	
                        <!-- Hidden field here-------->
                        <input type="hidden" id="txt_selected_id" value="" /> 
                        <input type="hidden" id="txt_issue_qnty" value="" />
                        <!-- ---------END------------->
                    </td>
            	</tr>  
                 
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        <table width="750">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnonClose();" style="width:100px" />
					</td>
				</tr>
			</table>
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

 
if($action=="create_roll_search_list")
{ 
	
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$expArr = explode("**",$ex_data[2]);
 	$company = $expArr[0]; // company id
	$rolID = $expArr[1];  //roll id
	$rollQnty = $expArr[2]; //roll qnty
	$batchID = $expArr[3]; //batch ID
	
	$cond="";
	$issue_qnty_arr = array();
	if($rolID!="")
	{
		$issueQnty = explode(",",$rollQnty); //roll wise input issue qnty
		$exp_rollID = explode(",",$rolID); 
		for($j=0;$j<count($exp_rollID);$j++)
		{
			$issue_qnty_arr[$exp_rollID[$j]] = $issueQnty[$j];
		}
	}
	
	$rollCond=$rollCondIssue="";
	if($rolID!="") 
	{ 
		//$rollCond = " and c.id not in ($rolID)"; //use for update mode
		$rollCondIssue = " and roll_id not in ($rolID)"; //use for update mode
	}  
	
	if($txt_search_by==1 && $txt_search_common!="")
		$cond = " and a.buyer_name='$txt_search_common'"; 
	elseif($txt_search_by==2 && $txt_search_common!="")
		$cond = " and a.job_no='$txt_search_common'"; 
	elseif($txt_search_by==3 && $txt_search_common!="")
		$cond = " and a.style_ref_no='$txt_search_common'"; 		
	 
	if($batchID!="")
	{
		$rollNo = sql_select("select group_concat(b.po_id) as po_id, group_concat(b.roll_no) as roll_no from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.id='$batchID' and b.roll_no<>0");
		foreach ($rollNo as $row);
		$rollNo = $row[csf('roll_no')];
		$po_id = $row[csf('po_id')]; 
		if($rollNo!="") $cond .= " and c.roll_no in ($rollNo)";
		if($po_id!="") $cond .= " and b.id in ($po_id)";
	}
	else $mstID="";	
		
	
  	$sql = "select b.po_number, c.id, c.po_breakdown_id, c.roll_no, d.prod_id, e.product_name_details, e.lot,sum(c.qnty) as rcvqnty 
			from 
				wo_po_details_master a, wo_po_break_down b, pro_roll_details c, pro_grey_prod_entry_dtls d, product_details_master e
			where  
				a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.dtls_id=d.id and d.prod_id=e.id and c.entry_form=1 and c.roll_no!=0 and e.current_stock>0
				$cond 
				group by c.po_breakdown_id,c.roll_no
				order by e.product_name_details";
 	//echo $sql;die;
	$result = sql_select($sql);
	
  	?> 
    <div align="left" style="margin-left:50px">    
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
        <thead>
            <th width="50">SL</th>
            <th width="60">Roll No</th>
            <th width="100">Order No</th>               
            <th width="250">Fabric Description</th>
            <th width="100">Yarn Lot</th>
            <th width="100">Roll Qnty</th>
            <th width="100">Issue Qnty</th> 
        </thead>
	</table>
    
	<div style="width:770px; max-height:250px; overflow-y:scroll" id="list_container_batch" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="list_view">  
        <?php
            $i=1;
            $previouDataRow="";
			foreach ($result as $row)
            {  
                 
				//echo "sum(qnty) pro_roll_details po_breakdown_id=".$row[csf('po_breakdown_id')]." and roll_no=".$row[csf('roll_no')]." and entry_form=16 $rollCondIssue";
				$issueqnty = return_field_value("sum(qnty)","pro_roll_details","po_breakdown_id=".$row[csf('po_breakdown_id')]." and roll_no=".$row[csf('roll_no')]." and entry_form=16 $rollCondIssue");
				//echo $issueqnty."==";
				if($issueqnty=="") $issueqnty=0; 
				$balanceQnty = $row[csf('rcvqnty')]-$issueqnty;
				//if($balanceQnty<=0)return;
				
								
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
				$isChecked="";
				if($issue_qnty_arr[$row[csf('id')]]!="")
				{
					 $row[csf('qnty')]=$issue_qnty_arr[$row[csf('id')]];
					 $bgcolor="yellow"; 
					 $isChecked = "checked";					
					 if($previouDataRow == "") $previouDataRow=$i; else $previouDataRow .= ",".$i;				 
				}
				 
          		?>
                <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>" style="cursor:pointer" > 
                    <td width="50"><?php echo $i; ?>
                    	<input type="checkbox" id="chk_<?php echo $i;?>" name="chk[]" class="check" style="width:4.5em; height:1em; visibility:hidden" onClick="js_set_value(<?php echo $i;?>)" <?php echo $isChecked; ?> />
                    	<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $row[csf('id')]; ?>"/> 
                        <input type="hidden" id="txt_prod_id<?php echo $i ?>" value="<?php echo $row[csf('prod_id')]; ?>" /> 
                        <input type="hidden" id="txt_po_number<?php echo $i ?>" value="<?php echo $row[csf('po_number')]; ?>" />                     
                    </td>
                    <td width="60" align="right" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('roll_no')]; ?></p></td>
                    <td width="100" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('po_number')]; ?></p></td>               
                    <td width="250" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
                    <td width="100" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('lot')]; ?></p></td>
                     
                    <td width="100" onClick="js_set_value(<?php echo $i;?>)"><input type="text" id="txt_balance_qnty_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $balanceQnty; ?>" style="width:70px" disabled readonly /></td>                    
                    
                    <td width="100"><input type="text" name="txt_issue_qnty[]" id="txt_issue_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($isChecked=="checked") echo $row[csf('qnty')]; else echo $balanceQnty; ?>"  onKeyUp="fn_onkeyup(<?php echo $i;?>)" /></td>   
                                    
                 </tr> 
				<?php 
                $i++;
            }
        	?>
            	<!-- for update, contain row id which is seleted -->
                <input type="hidden" id="previous_data_row_id" value="<?php echo $previouDataRow; ?>" />
        </table>        
      </div>
    </div>
	<?php	
	exit();	
	
}





// child form data populate after call after close roll pop up
if($action == "populate_child_from_data")  
{
	$i=0;
	$exp_data = explode("**",$data);
	$roll_ID = $exp_data[0];
	if($roll_ID=="") exit; // if roll id emtpy
	$issueQnty = explode(",",$exp_data[1]); //roll wise input issue qnty
	$exp_rollID = explode(",",$roll_ID); 
	
	$issue_qnty_arr = array();
	for($j=0;$j<count($exp_rollID);$j++)
	{
		$issue_qnty_arr[$exp_rollID[$j]] = $issueQnty[$j];
	}
	
	
	//echo $issueQnty; 
	$sql = "select GROUP_CONCAT(c.id) as id, GROUP_CONCAT(c.roll_no) as roll_no, GROUP_CONCAT(c.po_breakdown_id) as po_breakdown_id, count(c.roll_no) as roll_no_count, c.qnty, d.prod_id, e.product_name_details, e.lot, e.yarn_count_id
			from pro_roll_details c, pro_grey_prod_entry_dtls d, product_details_master e
			where				
				c.entry_form=1 and 
				c.dtls_id=d.id and 
				d.prod_id=e.id and 
				c.roll_no!=0 and
				c.id in ($roll_ID) group by d.prod_id"; 
	//echo $sql;
	$result = sql_select($sql);  
	foreach($result as $row)
	{ 
		$i=$i+1;
		
		$exp_concat = explode(",",$row[csf("id")]);
		$issue_qnty = 0;
		$roll_wise_qnty="";
		foreach($exp_concat as $key=>$val)
		{
			if($roll_wise_qnty!="") $roll_wise_qnty.=",";
			$roll_wise_qnty .= $issue_qnty_arr[$val];
			$issue_qnty += $issue_qnty_arr[$val];
		} 
		
		$lot=return_field_value("group_concat(distinct(yarn_lot)) as lot","pro_grey_prod_entry_dtls","prod_id='".$row[csf('prod_id')]."' and yarn_lot<>'' and status_active=1 and is_deleted=0","lot");
		$count_id=return_field_value("group_concat(distinct(yarn_count)) as count","pro_grey_prod_entry_dtls","prod_id='".$row[csf('prod_id')]."' and yarn_count<>'' and yarn_count<>0 and status_active=1 and is_deleted=0","count");
		
		$count_id=array_unique(explode(",",$count_id));	$count='';
		foreach($count_id as $val)
		{
			if($count=='') $count=$val; else $count.=",".$val;
		}
		
		echo "$('#txtNoOfRoll').val('".$row[csf('roll_no_count')]."');\n";
		
		echo "$('#txtRollNo').val('".$row[csf('id')]."');\n";
		echo "$('#txtRollPOid').val('".$row[csf('po_breakdown_id')]."');\n";
		echo "$('#txtRollPOQnty').val('".$roll_wise_qnty."');\n";
		
		echo "$('#txtItemDescription').val('".$row[csf('product_name_details')]."');\n";
		echo "$('#hiddenProdId').val('".$row[csf('prod_id')]."');\n";
		echo "$('#txtReqQnty').val('".$issue_qnty."');\n";
		echo "$('#txtYarnLot').val('".$lot."');\n";
		
		//echo "$('#cbo_yarn_count').val('".$row[csf('yarn_count_id')]."');\n";
		echo "set_multiselect('cbo_yarn_count','0','1','".$count."','0');\n";
		//echo "disable_enable_fields('show_textcbo_yarn_count','1','','');\n";
		echo "if($('#cbo_issue_purpose').val()==3 || $('#cbo_issue_purpose').val()==8) $('#txtIssueQnty').val('".$issue_qnty."');\n";
		echo "get_php_form_data('".$row[csf('po_breakdown_id')]."'+\"**\"+".$row[csf('prod_id')].", \"populate_data_about_order\", \"requires/grey_fabric_issue_controller\" );"; 
	}
	
	exit();
	
}
 

if($action=="populate_data_about_order")
{
	$data=explode("**",$data);
	$order_id=$data[0];
	$prod_id=$data[1];
	$issue_purpose=$data[2];
	if ($issue_purpose==8)
	{
		$sql=sql_select("select sum(case when transaction_type in (1,4) then cons_quantity end) as grey_fabric_recv, sum(case when transaction_type=5 then cons_quantity end) as grey_fabric_issued from inv_transaction where item_category=13 and prod_id=$prod_id and is_deleted=0 and status_active=1");
	}
	else
	{
		$sql=sql_select("select 
					sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, sum(case when entry_form in(16) then quantity end) as grey_fabric_issued,
					sum(case when entry_form in(13) and trans_type=5 then quantity end) as grey_fabric_trans_recv, sum(case when entry_form in(13) and trans_type=6 then quantity end) as grey_fabric_trans_issued
	 from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and po_breakdown_id in($order_id) and is_deleted=0 and status_active=1");
	}
	$grey_fabric_recv=$sql[0][csf('grey_fabric_recv')]+$sql[0][csf('grey_fabric_trans_recv')];
	$grey_fabric_issued=$sql[0][csf('grey_fabric_issued')]+$sql[0][csf('grey_fabric_trans_issued')];
	$yet_issue=$grey_fabric_recv-$grey_fabric_issued;
	
	//$order_nos=return_field_value("group_concat(po_number)","wo_po_break_down","id in($order_id)");
	//echo "$('#txt_order_numbers').val('".$order_nos."');\n";
	 
	echo "$('#txt_fabric_received').val('".$grey_fabric_recv."');\n";
	echo "$('#txt_cumulative_issued').val('".$grey_fabric_issued."');\n";
	echo "$('#txt_yet_to_issue').val('".$yet_issue."');\n"; 
 	
	if($order_id!="")
	{
		$orderSQL = sql_select("select a.buyer_name,a.style_ref_no,GROUP_CONCAT(b.id) as b_id,GROUP_CONCAT(b.po_number) as b_ponum from wo_po_details_master a,wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($order_id) group by a.buyer_name");
		echo "$('#txt_style_ref').val('".$orderSQL[0][csf("style_ref_no")]."');\n";
		echo "$('#cbo_buyer_name').val('".$orderSQL[0][csf("buyer_name")]."');\n";
		echo "$('#txt_order_no').val('".$orderSQL[0][csf("b_ponum")]."');\n";
		echo "$('#hidden_order_id').val('".$orderSQL[0][csf("b_id")]."');\n";
	}
	exit();	
}

  
  
 
if($action=="itemDescription_popup")
{ 
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  //echo $cbo_issue_purpose;
	?>
    <script>
		function js_set_value(prod_id)
		{			
			$("#txt_selected_id").val(prod_id);
			parent.emailwindow.hide();			
		}
		
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
	</script>
	<?php
	
	if($cbo_basis==1)
	{
		/*$sql = "select a.id, a.product_name_details,a.current_stock			 
			from 			
				product_details_master a, pro_grey_prod_entry_dtls b, inv_receive_master c 
			where  
				a.id=b.prod_id and b.mst_id=c.id and a.company_id=$cbo_company_id and a.current_stock>0 and a.item_category_id=13 and c.booking_no='$txt_booking_no' and c.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0";	*/
		
		$po_id=return_field_value("po_break_down_id","wo_booking_mst","booking_no='$txt_booking_no'");
		//echo $po_id;		
		if($po_id!="")
		{
			$sql = "select a.id, a.product_name_details,a.current_stock			 
				from 			
					product_details_master a, order_wise_pro_details b
				where  
					a.id=b.prod_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.po_breakdown_id in($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id";	
		}
		else if ($po_id=="" && $cbo_issue_purpose==8)
		{
			$sql = "select a.id, a.product_name_details,a.current_stock			 
				from 			
					product_details_master a, inv_receive_master b, pro_grey_prod_entry_dtls c
				where  
					a.id=c.prod_id and b.id=c.mst_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.booking_without_order=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id";	
		}
		else
		{
			echo "No Order Found Against this Booking No.";die;
		}
	}
	else
	{
		$sql = "select id,product_name_details,current_stock			 
			from 			
				product_details_master 
			where  
				company_id=$cbo_company_id and current_stock>0 and item_category_id=13 and status_active=1 and is_deleted=0";				
	}
	
	$result = sql_select($sql);
	
	if(count($result)<1) {echo "No Production Found Against these Order Nos.";die;}
	
 	$yarn_count = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');	
	$buyer_arr = return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	
	$po_array=array();
	if ($cbo_issue_purpose==8)
	{
		$po_sql=sql_select("select a.buyer_id as buyer_name, b.style_id as style_ref_no from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$cbo_company_id");
	}
	else
	{
		$po_sql=sql_select("select a.buyer_name, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$cbo_company_id");
	}
	foreach($po_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')];
		$po_array[$row[csf('id')]]['buyer']=$row[csf('buyer_name')];
	}
	
	$product_lot_array=return_library_array("select prod_id, group_concat(distinct(yarn_lot)) as lot from pro_grey_prod_entry_dtls where yarn_lot<>'' group by prod_id",'prod_id','lot');
	
	$product_count_array=return_library_array("select prod_id, group_concat(distinct(yarn_count)) as count from pro_grey_prod_entry_dtls where yarn_count<>'' and yarn_count<>0 group by prod_id",'prod_id','count');
	
	$po_id_array=return_library_array("select prod_id, group_concat(distinct(po_breakdown_id)) as po_id from order_wise_pro_details where po_breakdown_id<>0 and entry_form in(2,22,23) group by prod_id",'prod_id','po_id');
	
	$rack_array=return_library_array("select prod_id, group_concat(rack) as rack  from pro_grey_prod_entry_dtls where rack<>'' group by prod_id",'prod_id','rack');
	
	$self_array=return_library_array("select prod_id, group_concat(self) as self  from pro_grey_prod_entry_dtls where self<>'' and self<>0 group by prod_id",'prod_id','self');
	
   	//$arr = array(3=>$yarn_count); 
	echo "<div align=\"center\" style=\"width:100%\">";
	echo "<input type=\"hidden\" id=\"txt_selected_id\" />\n";
	?>
    <div style="width:900px; max-height:310px; overflow-y:scroll" id="list_container_batch" >	 
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table">
        <thead>
            <th width="30">SL</th>
            <th width="150">Item Description</th>
            <th width="130">Order No</th>
            <th width="60">Buyer</th>
            <th width="50">Job</th>              
            <th width="100">Style Ref.</th>
            <th width="80">Yarn Lot</th>
            <th>Count</th>
            <th width="90">Rack/Self</th>
        </thead>
    </table>
	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="870" class="rpt_table" id="tbl_list_search" >  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
					
				$po_id=explode(",",$po_id_array[$row[csf('id')]]);
				$po_no=''; $style_ref=array(); $buyer_array=array(); $buyer_name='';
				if($rack_array[$row[csf('id')]]!='' && $self_array[$row[csf('id')]]!=0)
				{
					$self_reck=	$rack_array[$row[csf('id')]].'/'.$self_array[$row[csf('id')]];
				}
				else if ($rack_array[$row[csf('id')]]=='' && $self_array[$row[csf('id')]]!=0)
				{
					$self_reck=$self_array[$row[csf('id')]];
				}
				else if ($rack_array[$row[csf('id')]]!='' && $self_array[$row[csf('id')]]==0)
				{
					$self_reck=$rack_array[$row[csf('id')]];
				}
				else
				{
					$self_reck='';
				}
				
				foreach($po_id as $val)
				{
					if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=", ".$po_array[$val]['no'];

					if(!in_array($po_array[$val]['style_ref'],$style_ref))
					{
						$style_ref[]=$po_array[$val]['style_ref'];
					}
					
					if(!in_array($po_array[$val]['buyer'],$buyer_array))
					{
						if($buyer_name=='') $buyer_name=$buyer_arr[$po_array[$val]['buyer']]; else $buyer_name.=",".$buyer_arr[$po_array[$val]['buyer']];
						$buyer_array[]=$po_array[$val]['buyer'];
					}
				}	 
				
				$count_id=array_unique(explode(",",$product_count_array[$row[csf('id')]]));	$count='';
				foreach($count_id as $val)
				{
					if($count=='') $count=$yarn_count[$val]; else $count.=",".$yarn_count[$val];
				}
				
				$job_arr=return_field_value("job_no_prefix_num","wo_po_details_master","style_ref_no='".implode(",",$style_ref)."' and status_active=1 and is_deleted=0 ","job_no_prefix_num");
			
            ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')]; ?>)"> 
                    <td width="30"><?php echo $i; ?></td>
                    <td width="150"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
                    <td width="130"><p><?php echo $po_no; ?></p></td>  
                    <td width="60" align="center"><p><?php echo $buyer_name; ?></p></td>
                    <td width="50" align="center"><p><?php echo $job_arr; ?></p></td> 
                    <td width="100"><p><?php echo implode(",",$style_ref); ?></p></td>
                    <td width="80"><p><?php echo $product_lot_array[$row[csf('id')]]; ?></p></td>
                    <td><p><?php echo $count; ?></p></td>
                    <td width="90"><p><?php echo $self_reck; ?></p></td>
                </tr>
            <?php
            $i++;
            }
            ?>
        </table><!--<td width="90" align="right"><?phpecho number_format($row[csf('current_stock')],2); ?></td>-->
    </div>
    <?php
	//echo  create_list_view("tbl_list_search", "Item Description,Quantity,Yarn Lot,Count", "350,100,100,100","700","250",0, $sql, "js_set_value", "id", "'populate_child_from_data_item_desc'", 1, "0,0,0,yarn_count_id", $arr, "product_name_details,current_stock,lot,yarn_count_id", 'grey_fabric_issue_controller','setFilterGrid(\'tbl_list_search\',-1);','0,2,0,0');
  	echo "</div>";
	exit();	
	
} 



 
// child form data populate after call after close item description pop up
if($action=="populate_child_from_data_item_desc") 
{
	$yarn_count = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');	
	$prod_id = $data;	 
	//echo $prod_id; 
	$sql = "select id,product_name_details,current_stock
			from product_details_master
			where id=$prod_id";				
				
	//echo $sql;die;
	$result = sql_select($sql);  
	foreach($result as $row)
	{ 
		$lot=return_field_value("group_concat(distinct(yarn_lot)) as lot","pro_grey_prod_entry_dtls","prod_id='".$row[csf('id')]."' and yarn_lot<>'' and status_active=1 and is_deleted=0","lot");
		$count_id=return_field_value("group_concat(distinct(yarn_count)) as count","pro_grey_prod_entry_dtls","prod_id='".$row[csf('id')]."' and yarn_count<>'' and yarn_count<>0 and status_active=1 and is_deleted=0","count");
		$count_id=array_unique(explode(",",$count_id));	$count='';
		foreach($count_id as $val)
		{
			if($count=='') $count=$val; else $count.=",".$val;
		}
		
 		echo "$('#txtItemDescription').val('".$row[csf('product_name_details')]."');\n";
		echo "$('#hiddenProdId').val('".$row[csf('id')]."');\n";
		echo "$('#txtYarnLot').val('".$lot."');\n";
		//echo "$('#cbo_yarn_count').val('".$count."');\n"; 
		echo "set_multiselect('cbo_yarn_count','0','1','".$count."','0');\n";
		//echo "disable_enable_fields('show_textcbo_yarn_count','1','','');\n";
	}
	
	exit();
		
}

 
 
 
 
if ($action=="po_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);  
	
	 
	$data=explode("_",$data); 
	$po_id=$data[0]; //order ID 
	if($data[1])$type=$data[1]; else $type=0; //is popup search or not	
	$prevQnty=$data[2]; //previous input qnty po wise
	if($data[3]!="")$prev_method=$data[3];  
	else $prev_method=$distribution_method;  
	if($data[4]!="")$issueQnty=$data[4]; 
	if($data[5]!="")$prod_id=$data[5]; 
	else $issueQnty=$issueQnty; 
	
	if($isRoll==1) $readonlyCond="readonly"; else $readonlyCond="";
	 
?> 

	<script> 
		var receive_basis=<?php echo $receive_basis; ?>;
		
		function fn_show_check()
		{
			if( form_validation('cbo_buyer_name','Buyer Name')==false )
			{
				return;
			}			
			show_list_view ( $('#txt_search_common').val()+'_'+$('#cbo_search_by').val()+'_'+<?php echo $cbo_company_id; ?>+'_'+$('#cbo_buyer_name').val()+'_'+'<?php echo $all_po_id; ?>'+'_'+<?php echo $receive_basis;?>, 'create_po_search_list_view', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'tbl_list_search\',-1);hidden_field_reset();');
			set_all();
		}
		
		function distribute_qnty(str)
		{ 
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_grey_qnty=$('#txt_prop_grey_qnty').val()*1;
				var tblRow = $("#tbl_list_search tr").length;
				var len=totalGrey=0;
				
				$("#tbl_list_search").find('tr').each(function()
				{
					len=len+1;
					
					var txtOrginal=$(this).find('input[name="txtOrginal[]"]').val()*1;
					if(txtOrginal==0)
					{
						$(this).remove();
					}
					else
					{
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;						
						var grey_qnty=(perc*txt_prop_grey_qnty)/100;
						totalGrey = totalGrey*1+grey_qnty*1;
						totalGrey = totalGrey.toFixed(2);						
						if(tblRow==len)
						{
							var balance = txt_prop_grey_qnty-totalGrey;
							if(balance!=0) grey_qnty=grey_qnty+(balance);							
						}
						
						if(grey_qnty>0)
						{
							$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
						}
						else
						{
							$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
						}
					}
				}); 
			}
			else
			{
				$('#txt_prop_grey_qnty').val('');
				$("#tbl_list_search").find('tr').each(function()
				{
					$(this).find('input[name="txtGreyQnty[]"]').val('');
				});
			}
			
			sum_total();
		}
		
				
		var selected_id = new Array();
		
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			
			var old=document.getElementById('txt_po_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#po_id').val( id );
			
		}
		
		function show_grey_prod_recv() 
		{ 
 			var po_id=$('#po_id').val();
			var prev_save_string=$('#prev_save_string').val();
			var prev_method=$('#prev_method').val();
			var prev_total_qnty=$('#prev_total_qnty').val();			
			show_list_view ( po_id+'_'+'1'+'_'+prev_save_string+'_'+prev_method+'_'+prev_total_qnty+'_'+<?php echo $prod_id; ?>, 'po_popup', 'search_div', 'grey_fabric_issue_controller', '');
			distribute_qnty($('#cbo_distribiution_method').val());
		}
		
		function hidden_field_reset()
		{
			$('#po_id').val('');
			$('#save_string').val( '' );
			$('#tot_grey_qnty').val( '' );
			selected_id = new Array();
		}
		
		function fnc_close()
		{
			
			var checkEqual =  $("#txt_total_sum").val()*1-$("#txt_prop_grey_qnty").val()*1;
			//if( checkEqual!=0 ){ alert("Issue Qnty and Sum Qnty Not Match"); return; }
			
			var save_string='';	 var tot_grey_qnty=0; var no_of_roll=''; 
			var po_id_array = new Array();
			
			$("#tbl_list_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtGreyQnty=$(this).find('input[name="txtGreyQnty[]"]').val();				
				tot_grey_qnty=tot_grey_qnty*1+txtGreyQnty*1;
				 			
				if(txtGreyQnty*1>0)
				{
					if(save_string=="")
					{
						save_string=txtPoId+"**"+txtGreyQnty;
					}
					else
					{
						save_string+=","+txtPoId+"**"+txtGreyQnty;
					}
					
					if( jQuery.inArray( txtPoId, po_id_array) == -1 ) 
					{
						po_id_array.push(txtPoId);
					}
				}
			});
			
			if(save_string=="")
			{
				alert("Please Select At Least One Item");
				return;
			}
			
			$('#save_string').val( save_string );
			$('#tot_grey_qnty').val( tot_grey_qnty );
			$('#all_po_id').val( po_id_array );
			$('#distribution_method').val( $('#cbo_distribiution_method').val() );			
			parent.emailwindow.hide();
		}
		
		function sum_total()
		{
			var tblRow = $("#tbl_list_search tr").length; 
			var ddd={dec_type:1}
			math_operation( "txt_total_sum", "txtGreyQnty_", "+", tblRow, ddd );
		}
		
		$(document).ready(function(e) {
           // distribute_qnty($('#cbo_distribiution_method').val());
			sum_total();
        });
		
    </script>

</head>
<body>
	
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:5px">
        <?php if($type!=1){?>
        	<!-- previous data here---->
            <input type="hidden" name="prev_save_string" id="prev_save_string" class="text_boxes" value="<?php echo $save_data; ?>">
            <input type="hidden" name="prev_total_qnty" id="prev_total_qnty" class="text_boxes" value="<?php echo $issueQnty; ?>">
            <input type="hidden" name="prev_method" id="prev_method" class="text_boxes" value="<?php echo $distribution_method; ?>">
            <!--- END-------->
        	<input type="hidden" name="save_string" id="save_string" class="text_boxes" value="<?php echo $save_data; ?>">
            <input type="hidden" name="tot_grey_qnty" id="tot_grey_qnty" class="text_boxes" value="">
            <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
            <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
		<?php } ?>
	<?php   
	if($receive_basis==2)
	{
	?>
		<table cellpadding="0" cellspacing="0" width="620" class="rpt_table">
			<thead>
				<th>Buyer</th>
				<th>Search By</th>
				<th>Search</th>
				<th>
					<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
					<input type="hidden" name="po_id" id="po_id" value="">
				</th> 
			</thead>
			<tr class="general">
				<td align="center">
					<?php
						echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "","" ); 
					?>       
				</td>
				<td align="center">	
					<?php
						$search_by_arr=array(1=>"PO No",2=>"Job No");
						echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
					?>
				</td>                 
				<td align="center">				
					<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
				</td> 						
				<td align="center">
					<input type="button" name="button2" class="formbutton" value="Show" onClick="fn_show_check()" style="width:100px;" />
				</td>
			</tr>
		</table>
		<div id="search_div" style="margin-top:10px">
        <?php 
			if($all_po_id!="" || $po_id!="")
			{ 
			?>    
				<div style="width:600px; margin-top:10px" align="center">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
						<thead>
							<th>Total Issue Qnty</th>
							<th>Distribution Method</th>
						</thead>
						<tr class="general">
							<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"  <?php echo $readonlyCond; ?> /></td>
							<td>
								<?php
									$distribiution_method=array(1=>"Proportionately",2=>"Manually");
									echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);",0 );
								?>
							</td>
						</tr>
					</table>
				</div>
				<div style="margin-left:1px; margin-top:10px">
					<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610">
						<thead>
                            <th width="120">PO No</th>
                            <th width="80">Ship. Date</th>
                            <th width="90">Gmts. Qnty</th>
                            <th width="90">Prod. Qnty</th>
                            <th width="90">Cumu. Issued Qnty</th>
                            <th>Issue Qnty</th>
                        </thead>
					</table>
					<div style="width:630px; max-height:280px; overflow-y:scroll" id="list_container" align="left">  
						<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610" id="tbl_list_search">   
					<?php
					 
						if($po_id=="")$po_id=$all_po_id; else $po_id=$po_id;
						$i=1; $tot_po_qnty=0;
						
						if($po_id!="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date
							from wo_po_details_master a, wo_po_break_down b 
							where a.job_no=b.job_no_mst and b.id in ($po_id) group by b.id";
							
							$propData=sql_select("select po_breakdown_id, sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, sum(case when entry_form in(16) then quantity end) as grey_fabric_issued from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and po_breakdown_id in($po_id) and is_deleted=0 and status_active=1 group by po_breakdown_id");
							foreach($propData as $prop_row)
							{
								$propDataArray[$prop_row[csf('po_breakdown_id')]]['recv']=$prop_row[csf('grey_fabric_recv')];
								$propDataArray[$prop_row[csf('po_breakdown_id')]]['iss']=$prop_row[csf('grey_fabric_issued')];
								$yet_issue=$prop_row[csf('grey_fabric_recv')]-$prop_row[csf('grey_fabric_issued')];
								$propDataArray[$prop_row[csf('po_breakdown_id')]]['bl']=$yet_issue;
							}
						}				 
						//echo $po_sql; 
						$po_data_array=array();					
						$explSaveData = explode(",",$save_data);
						foreach($explSaveData as $val)
						{
							$woQnty = explode("**",$val);
							$po_data_array[$woQnty[0]]=$woQnty[1];	
						}
						
						$nameArray=sql_select($po_sql);
						foreach($nameArray as $row)
						{  
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
								
							$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
							
							//$woQnty = explode("**",$explSaveData[$i-1]); print_r($woQnty);
							//if($woQnty[0]==$row[csf('id')]) $qnty = $woQnty[1]; else $qnty = "";						
							$qnty = $po_data_array[$row[csf('id')]];
						 ?>
							<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
								<td width="120">
									<p><?php echo $row[csf('po_number')]; ?></p>
									<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
									<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
								</td>
                                <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
								<td width="90" align="right">
									<?php echo $row[csf('po_qnty_in_pcs')]; ?>
									<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
								</td>
                                <td width="90" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['recv'],2,'.',''); ?></td>
                            	<td width="90" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['iss'],2,'.',''); ?></td>
								<td align="center">
									<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php  echo $qnty; ?>" onKeyUp="sum_total();" placeholder="<?php echo number_format($propDataArray[$row[csf('id')]]['bl'],2,'.',''); ?>">
								</td>					
								
							</tr>
						<?php 
						$i++; 
						} 
						?>
						<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
 					</table> 
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610"> 
                        <tr class="tbl_bottom">
                        	<td width="120">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="90" align="right">&nbsp;</td><td width="90" align="right">&nbsp;</td><td width="90" align="right">Sum</td><td style="text-align:center"><input type="text" id="txt_total_sum" class="text_boxes_numeric" style="width:80px" readonly /></td>
                        </tr>
                   </table>                 
				</div>
					<table width="620" id="table_id">
						 <tr>
							<td align="center" >
								<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
							</td>
						</tr>
					</table>
				</div>
          <?php } ?>  
            
       </div>     
	<?php
	}
	else
	{  
	?>
		<div style="width:600px; margin-top:10px" align="center">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
				<thead>
					<th>Total Issue Qnty</th>
					<th>Distribution Method</th>
				</thead>
				<tr class="general">
					<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty($('#cbo_distribiution_method').val())" <?php echo $readonlyCond; ?> /></td>
					<td>
						<?php
							$distribiution_method=array(1=>"Proportionately",2=>"Manually");
                            echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);",0 );
						?>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:1px; margin-top:10px">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610">
				<thead>
                    <th width="120">PO No</th>
                    <th width="80">Ship. Date</th>
                    <th width="90">Gmts. Qnty</th>
                    <th width="90">Prod. Qnty</th>
                    <th width="90">Cumu. Issued Qnty</th>
					<th>Issue Qnty</th>
				</thead>
			</table>
			<div style="width:630px; max-height:220px; overflow-y:scroll" id="list_container" align="left"> 
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610" id="tbl_list_search">  
					<?php 
					$i=1; $tot_po_qnty=0; $propDataArray=array();
					
					$propData=sql_select("select po_breakdown_id, sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, sum(case when entry_form in(16) then quantity end) as grey_fabric_issued from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and is_deleted=0 and status_active=1 group by po_breakdown_id");
					foreach($propData as $prop_row)
					{
						$propDataArray[$prop_row[csf('po_breakdown_id')]]['recv']=$prop_row[csf('grey_fabric_recv')];
						$propDataArray[$prop_row[csf('po_breakdown_id')]]['iss']=$prop_row[csf('grey_fabric_issued')];
						$yet_issue=$prop_row[csf('grey_fabric_recv')]-$prop_row[csf('grey_fabric_issued')];
						$propDataArray[$prop_row[csf('po_breakdown_id')]]['bl']=$yet_issue;
					}
					//print_r($propDataArray);		
 					if($type==1 && $po_id!="")
					{
						if($booking_no=="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($po_id) group by b.id";
						}
						else
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_booking_dtls c, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' and b.id in ($po_id) group by b.id";	
						}
					}
					else
					{
						if($booking_no=="")
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst group by b.id";
						}
						else
						{
							$po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date from wo_po_details_master a, wo_booking_dtls c, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' group by b.id";	
						}
					} 
					//echo $po_sql;
					if($save_string=="" && $type==1) $save_data=$prevQnty;
					$explSaveData = explode(",",$save_data);
					$po_data_array=array();					
					$explSaveData = explode(",",$save_data);
					foreach($explSaveData as $val)
					{
						$woQnty = explode("**",$val);
						$po_data_array[$woQnty[0]]=$woQnty[1];	
					}
					//print_r($explSaveData);
 					$nameArray=sql_select($po_sql);
					foreach($nameArray as $row)
					{  
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
							
						$tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
						
						//$woQnty = explode("**",$explSaveData[$i-1]);
						//if($woQnty[0]==$row[csf('id')]) $qnty = $woQnty[1]; else $qnty = "";
						$qnty = $po_data_array[$row[csf('id')]];						
						
					 ?>
						<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
							<td width="120">
								<p><?php echo $row[csf('po_number')]; ?></p>
								<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
								<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
							</td>
                            <td width="80" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
							<td width="90" align="right">
								<?php echo $row[csf('po_qnty_in_pcs')]; ?>
								<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
							</td>
                            <td width="90" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['recv'],2,'.',''); ?></td>
                            <td width="90" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['iss'],2,'.',''); ?></td>
							<td align="center">
								<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $qnty; ?>" onKeyUp="sum_total();" placeholder="<?php echo number_format($propDataArray[$row[csf('id')]]['bl'],2,'.',''); ?>" >
							</td>					
							
						</tr>
					<?php 
					$i++; 
					} 
					?>
					<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
 				</table>
               <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610"> 
                        <tr class="tbl_bottom">
                            <td width="120">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="90" align="right">&nbsp;</td><td width="90" align="right">&nbsp;</td><td width="90" align="right">Sum</td><td style="text-align:center"><input type="text" id="txt_total_sum" class="text_boxes_numeric" style="width:80px" readonly /></td>
                        </tr>
               </table> 
			</div>
			<table width="610" id="table_id">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
		</div>
        
         
	<?php
	}	
	?>
		</fieldset>
	</form>
        
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="create_po_search_list_view")
{
	$data = explode("_",$data);
	
	$search_string=trim($data[0]);
	$search_by=$data[1];
	
	$search_con="";
	if($search_by==1 && $search_string!="")
		$search_con = " and b.po_number like '%$search_string%'";
	else if($search_by==2 && $search_string!="")
		$search_con =" and a.job_no like '%$search_string%'"; 
		
	$company_id =$data[2];
	$buyer_id =$data[3];	
	$all_po_id=$data[4];
	$receiveBasis=$data[5];
	
	if($all_po_id!="")
		$po_id_cond=" or b.id in($all_po_id)";
	else 
		$po_id_cond="";
	
	$hidden_po_id=explode(",",$all_po_id);
	 
	if($buyer_id==0) { echo "<b>Please Select Buyer First</b>"; die; }
	
	if($receiveBasis==1)
	{
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date 
		from wo_po_details_master a, wo_po_break_down b, wo_booking_dtls c 
		where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and a.company_name=$company_id and a.buyer_name=$buyer_id $search_con and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0  group by b.id"; //$po_id_cond
	}
	else
	{
		$sql = "select a.job_no, a.style_ref_no, a.order_uom, b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date 
		from wo_po_details_master a, wo_po_break_down b
		where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name=$buyer_id $search_con and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0  group by b.id"; //$po_id_cond

	}
	//echo $sql;die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Job No</th>
                <th width="110">Style No</th>
                <th width="110">PO No</th>
                <th width="90">PO Quantity</th>
                <th width="50">UOM</th>
                <th>Shipment Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:220px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1; $po_row_id='';
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					 
					if(in_array($selectResult[csf('id')],$hidden_po_id)) 
					{ 
						if($po_row_id=="") $po_row_id=$i; else $po_row_id.=",".$i;
					}
							
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
                            <td width="40" align="center"><?php echo "$i"; ?>
                             <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>	
                            </td>	
                            <td width="100"><p><?php echo $selectResult[csf('job_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                            <td width="110"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                            <td width="90" align="right"><?php echo $selectResult[csf('po_qnty_in_pcs')]; ?></td> 
                            <td width="50" align="center"><p><?php echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                            <td align="center"><?php echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                        </tr>
                    <?php
                    $i++;
				}
			?>
				<input type="hidden" name="txt_po_row_id" id="txt_po_row_id" value="<?php echo $po_row_id; ?>"/>
            </table>
        </div>
         <table width="620" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                            <input type="button" name="close" onClick="show_grey_prod_recv();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>           
<?php
	
exit();
}





//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
				
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
				
		
		//batch duplication check---------------------------------------------//
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3)
		{
			$chk = is_duplicate_field("batch_no","pro_batch_create_mst","batch_no=$txt_batch_no and batch_no!=0");
			if($chk==1)
			{
				echo "20**Duplicate Batch Number.";
				exit();
			}
		}
		
		
		//table lock here 	 
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		
		 
		//####################################################-------------------- 	
		//batch creation table insert here------------------------//
		if(str_replace("'","",$txt_batch_id)!="") $batchID = $txt_batch_id; else $batchID = 0;
		
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3)
		{	
			 //pro_batch_create_mst table insert here------------------------//
			 //batch_against,batch_for,batch_weight
			 $batchID = return_next_id("id", "pro_batch_create_mst", 1);
			 $field_array = "id,batch_no,entry_form,batch_date,company_id,booking_no_id,booking_no,booking_without_order,batch_weight,inserted_by,insert_date";
			 $data_array = "(".$batchID.",".$txt_batch_no.",16,".$txt_issue_date.",".$cbo_company_name.",".$txt_booking_id.",".$txt_booking_no.",0,".$txtIssueQnty.",'".$user_id."','".$pc_date_time."')";
			 
			 $batchQry=sql_insert("pro_batch_create_mst",$field_array,$data_array,1);	
			 //pro_batch_create_mst table insert end------------------------//
			
			
			 //pro_batch_create_dtls table insert start------------------------//		  
 			 $batchDtlsID = return_next_id("id", "pro_batch_create_dtls", 1);
			 $field_array = "id,mst_id,po_id,item_description,roll_no,batch_qnty,inserted_by,insert_date";
 			 $data_array = "(".$batchDtlsID.",".$batchID.",".$hiddenProdId.",".$txtItemDescription.",".$txtRollNo.",".$txtIssueQnty.",'".$user_id."','".$pc_date_time."')";				
			
			 $batchQryDtls=sql_insert("pro_batch_create_dtls",$field_array,$data_array,1);
			 	
			 //pro_batch_create_dtls table insert end------------------------//
		  
		}
		else
		{
		 	$batchQry=true;$batchQryDtls=true;
		}
		//batch creation table insert end------------------------//
			
		//echo "10**".$field_array."=".$data_array."##".$batchQry; 
		//mysql_query("ROLLBACK");die; 	
			
		$mrr_no='';
		//issue master table entry here Start---------------------------------------//
		if( str_replace("'","",$txt_system_no) == "" ) //new insert
		{	
			$id=return_next_id("id", "inv_issue_master", 1);		
			$new_mrr_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'KGI', date("Y",time()), 5, "select issue_number_prefix,issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_name and entry_form=16 and YEAR(insert_date)=".date('Y',time())." order by issue_number_prefix_num DESC ", "issue_number_prefix", "issue_number_prefix_num" ));
			
			$field_array="id,issue_number_prefix, issue_number_prefix_num, issue_number, issue_basis, issue_purpose, entry_form, item_category, company_id, buyer_id, style_ref, booking_id, booking_no, batch_no, issue_date, knit_dye_source, knit_dye_company, challan_no, order_id, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_mrr_number[1]."','".$new_mrr_number[2]."','".$new_mrr_number[0]."',".$cbo_basis.",".$cbo_issue_purpose.",16,13,".$cbo_company_name.",".$cbo_buyer_name.",".$txt_style_ref.",".$txt_booking_id.",".$txt_booking_no.",".$batchID.",".$txt_issue_date.",".$cbo_dyeing_source.",".$cbo_dyeing_company.",".$txt_challan_no.",".$hidden_order_id.",'".$user_id."','".$pc_date_time."')";		
			$rID=sql_insert("inv_issue_master",$field_array,$data_array,1); 
			$mrr_no=$new_mrr_number[0];
		}else{
			$id = str_replace("'","",$hidden_system_id);
			$field_array="issue_basis*issue_purpose*entry_form*item_category*company_id*buyer_id*style_ref*booking_id*booking_no*batch_no*issue_date*knit_dye_source*knit_dye_company*challan_no*order_id*updated_by*update_date";
			$data_array="".$cbo_basis."*".$cbo_issue_purpose."*16*13*".$cbo_company_name."*".$cbo_buyer_name."*".$txt_style_ref."*".$txt_booking_id."*".$txt_booking_no."*".$txt_batch_no."*".$txt_issue_date."*".$cbo_dyeing_source."*".$cbo_dyeing_company."*".$txt_challan_no."*".$hidden_order_id."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;."-".;
			$rID=sql_update("inv_issue_master",$field_array,$data_array,"id",$id,0);
			$mrr_no=str_replace("'","",$txt_system_no);
		}
		//issue master table entry here END---------------------------------------//
		//if($rID) $flag=1; else $flag=0;
	//	echo "10**"."insert into inv_issue_master (".$field_array.") values ".$data_array.$flag;die;
		
		//####################################################------------------------
		//inv_grey_fabric_issue_dtls table insert start------------------------------//
		$dtls_id=return_next_id("id", "inv_grey_fabric_issue_dtls", 1);
		$field_array="id,mst_id,distribution_method,no_of_roll,roll_no,roll_po_id,prod_id,roll_wise_issue_qnty,issue_qnty,color_id,yarn_lot,yarn_count,store_name,remarks,inserted_by,insert_date";		
 		$data_array="(".$dtls_id.",".$id.",".$distribution_method_id.",".$txtNoOfRoll.",".$txtRollNo.",".$txtRollPOid.",".$hiddenProdId.",".$txtRollPOQnty.",".$txtIssueQnty.",".$cbo_color_id.",".$txtYarnLot.",".$cbo_yarn_count.",".$cbo_store_name.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
		$dtlsrID=sql_insert("inv_grey_fabric_issue_dtls",$field_array,$data_array,1); 		
		//inv_grey_fabric_issue_dtls table insert end------------------------------//
		
		
		
		
		//####################################################-----------------
		//this is for transaction table insert-------------
		$transactionID = return_next_id("id", "inv_transaction", 1);
		$tr_field_array = "id,mst_id,company_id,prod_id,item_category,transaction_type,transaction_date,store_id,cons_quantity,inserted_by,insert_date";		
		$tr_data_array = "(".$transactionID.",".$id.",".$cbo_company_name.",".$hiddenProdId.",13,2,".$txt_issue_date.",".$cbo_store_name.",".$txtIssueQnty.",'".$user_id."','".$pc_date_time."')";
		$transID = sql_insert("inv_transaction",$tr_field_array,$tr_data_array,1);
		//inventory TRANSACTION table data entry  END-------------------------------//
		
		
		
		//####################################################----------------- 
		//product master table data UPDATE START-----------------------------//  
		$txtIssueQnty = str_replace("'","",$txtIssueQnty);
		$currentStock = return_field_value("current_stock","product_details_master","id=".$hiddenProdId);
		$currentStock = $currentStock-$txtIssueQnty;
 		$prod_update_data = "".$txtIssueQnty."*".$currentStock."*'".$user_id."'*'".$pc_date_time."'";
		$prod_field_array = "last_issued_qnty*current_stock*updated_by*update_date";
 		$prodUpdate = sql_update("product_details_master",$prod_field_array,$prod_update_data,"id",$hiddenProdId,0);
		//product master table data UPDATE END-------------------------------//	
			
		 
		
		
		//####################################################--------------------
		//order_wise_pro_details table data insert Start---------------// 
		$proportQ=true; 
		$data_array_prop="";
		$save_string=explode(",",str_replace("'","",$save_data));
		if(count($save_string)>0 && str_replace("'","",$save_data)!="")  
		{		 
			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,dtls_id,po_breakdown_id,prod_id,quantity,inserted_by,insert_date";
			$po_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if( array_key_exists($order_id,$po_array) )
					$po_array[$order_id]+=$order_qnty;
				else
					$po_array[$order_id]=$order_qnty;
			}
			$i=0; 
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$data_array_prop.="(".$id_proport.",".$transactionID.",2,16,".$dtls_id.",".$order_id.",".$hiddenProdId.",".$order_qnty.",".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}
		}//end if
		//order_wise_pro_details table data insert END -----//
		
		  
		 
		 
		 
		//####################################################--------------------
		//roll table entry----------------------------------------//
		if( str_replace("'","",$hidden_is_roll_maintain)==1)
		{
			$data_array="";
			$rollDtlsID = return_next_id("id", "pro_roll_details", 1);
			$field_array = "id,mst_id,dtls_id,po_breakdown_id,entry_form,roll_no,roll_id,qnty,inserted_by,insert_date";
			
			$poIDarr = explode(",",str_replace("'","",$txtRollPOid));
			$rollNoarr = explode(",",str_replace("'","",$txtRollNo));
			$rollPoQntyarr = explode(",",str_replace("'","",$txtRollPOQnty));			
			$lopSize = count($poIDarr); 
			for($i=0;$i<$lopSize;$i++)
			{  			 					
				$rollno = return_field_value("roll_no","pro_roll_details","id=".$rollNoarr[$i]);
				if($i>0) $data_array .=",";   
				$data_array .= "(".$rollDtlsID.",".$id.",".$dtls_id.",".$poIDarr[$i].",16,'".$rollno."',".$rollNoarr[$i].",".$rollPoQntyarr[$i].",'".$user_id."','".$pc_date_time."')";
			 	$rollDtlsID = $rollDtlsID+1;
			}	
			$rollDtls=sql_insert("pro_roll_details",$field_array,$data_array,1);	
		}
		else
		{
			 $rollDtls=true; 
		}		 
		//roll table entry end------------------------------------//
		
		 
		//echo "20**".$rID." && ".$dtlsrID." && ".$prodUpdate." && ".$proportQ." && ".$batchQry." && ".$batchQryDtls." && ".$rollDtls; 
		//mysql_query("ROLLBACK");die; 
 		 
		 
		 
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		if($rID && $dtlsrID && $transID && $prodUpdate && $proportQ && $batchQry && $batchQryDtls && $rollDtls)
		{
			mysql_query("COMMIT");  
			echo "0**".$mrr_no."**".$id;
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**".$mrr_no."**".$id;
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$mrr_no."**".$id;
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		
				
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		
		 
		//check update id
		if( str_replace("'","",$txt_system_no)=="" )
		{
			echo "15";exit(); 
		}
		 
		
		//batch duplication check---------------------------------------------//
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3)
		{
			$chk = is_duplicate_field("batch_no","pro_batch_create_mst","batch_no=$txt_batch_no and id!=$txt_batch_id and batch_no!=0");
			if($chk==1)
			{
				echo "20**Duplicate Batch Number.";
				exit();
			}
		}
		
		
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		
 		//####################################################-------------------- 	
		//batch creation table insert here------------------------//
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3 && str_replace("'","",$txt_batch_id)!="" )
		{	
			 
			 //pro_batch_create_mst table update here------------------------//
 			 $field_array = "batch_no*entry_form*batch_date*company_id*booking_no_id*booking_no*booking_without_order*batch_weight*updated_by*update_date";
			 $data_array = "".$txt_batch_no."*16*".$txt_issue_date."*".$cbo_company_name."*".$txt_booking_id."*".$txt_booking_no."*0*".$txtIssueQnty."*'".$user_id."'*'".$pc_date_time."'";			 
 			 $batchQry=sql_update("pro_batch_create_mst",$field_array,$data_array,"id",$txt_batch_id,0);
			 //pro_batch_create_mst table update end------------------------//
			
			
			 //pro_batch_create_dtls table insert start------------------------//		  
 			 $field_array = "po_id*item_description*roll_no*batch_qnty*updated_by*update_date";
 			 $data_array = "".$hiddenProdId."*".$txtItemDescription."*".$txtRollNo."*".$txtIssueQnty."*'".$user_id."'*'".$pc_date_time."'";				
 			 $batchQryDtls=sql_update("pro_batch_create_dtls",$field_array,$data_array,"mst_id",$txt_batch_id,0);
			 //pro_batch_create_dtls table insert end------------------------//
		   
		}
		else
		{
		 	$batchQry=true;$batchQryDtls=true;
		}
		//batch creation table insert end------------------------//
		
		 
		 
  		//################################################################
		//issue master update START--------------------------------------//
		$id = str_replace("'","",$hidden_system_id);
		$field_array="issue_basis*issue_purpose*entry_form*item_category*company_id*buyer_id*style_ref*booking_id*booking_no*batch_no*issue_date*knit_dye_source*knit_dye_company*challan_no*order_id*updated_by*update_date";
		$data_array="".$cbo_basis."*".$cbo_issue_purpose."*16*13*".$cbo_company_name."*".$cbo_buyer_name."*".$txt_style_ref."*".$txt_booking_id."*".$txt_booking_no."*".$txt_batch_id."*".$txt_issue_date."*".$cbo_dyeing_source."*".$cbo_dyeing_company."*".$txt_challan_no."*".$hidden_order_id."*'".$user_id."'*'".$pc_date_time."'";
		//echo "20**".$field_array."<br>".$data_array;
		$rID=sql_update("inv_issue_master",$field_array,$data_array,"id",$id,0);
		//issue master update END---------------------------------------// 
		 
		 
		 
		//####################################################----------------- 
		//product table update data before -----------------------------// 
		$before_prod_id = return_field_value("prod_id","inv_grey_fabric_issue_dtls","id=".$dtls_tbl_id);
		$sqlRes = sql_select("select b.id,b.current_stock,a.issue_qnty from inv_grey_fabric_issue_dtls a, product_details_master b where a.prod_id=b.id and a.id=$dtls_tbl_id"); 
 		foreach($sqlRes as $resR);		 
		$before_prod_id = $resR[csf("id")];
		$before_issue_qnty = $resR[csf("issue_qnty")];
		$before_current_stock = $resR[csf("current_stock")];
		$adjust_current_stock = $before_current_stock+$before_issue_qnty;
		
		
		
		
		//####################################################------------------------
		//inv_grey_fabric_issue_dtls table update start------------------------------//
		$dtls_id=$dtls_tbl_id;
		$field_array="distribution_method*no_of_roll*roll_no*roll_po_id*prod_id*roll_wise_issue_qnty*issue_qnty*color_id*yarn_lot*yarn_count*store_name*remarks*updated_by*update_date";		
 		$data_array="".$distribution_method_id."*".$txtNoOfRoll."*".$txtRollNo."*".$txtRollPOid."*".$hiddenProdId."*".$txtRollPOQnty."*".$txtIssueQnty."*".$cbo_color_id."*".$txtYarnLot."*".$cbo_yarn_count."*".$cbo_store_name."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
 		$dtlsrID=sql_update("inv_grey_fabric_issue_dtls",$field_array,$data_array,"id",$dtls_id,0);		
		//inv_grey_fabric_issue_dtls table update end------------------------------// 
		 
		 
		
		//####################################################-----------------
		//this is for transaction table update start -------------
		$transactionID = $trans_tbl_id;
		$tr_field_array = "company_id*prod_id*item_category*transaction_type*transaction_date*store_id*cons_quantity*updated_by*update_date";		
		$tr_data_array = "".$cbo_company_name."*".$hiddenProdId."*13*2*".$txt_issue_date."*".$cbo_store_name."*".$txtIssueQnty."*'".$user_id."'*'".$pc_date_time."'";
 		$transID = sql_update("inv_transaction",$tr_field_array,$tr_data_array,"id",$transactionID,0);	
		//inventory TRANSACTION table data update  END-------------------------------//
		
		
		//####################################################----------------- 
		//product master table data UPDATE START-----------------------------//  
		$hiddenProdId = str_replace("'","",$hiddenProdId); //current product id
		$txtIssueQnty = str_replace("'","",$txtIssueQnty); //current issue qnty
				
		$updateID_array=array();
		$update_data=array();
		if($before_prod_id==$hiddenProdId) 
		{
			$adjust_current_stock = $adjust_current_stock - $txtIssueQnty;
			$prod_update_data = "".$txtIssueQnty."*".$adjust_current_stock."*'".$user_id."'*'".$pc_date_time."'";
			$prod_field_array = "last_issued_qnty*current_stock*updated_by*update_date";
 			$prodUpdate = sql_update("product_details_master",$prod_field_array,$prod_update_data,"id",$hiddenProdId,0);
		}
		else
		{
			
			//before adjust
			$updateID_array[]=$before_prod_id; 
			$update_data[$before_prod_id]=explode("*",("0*".$adjust_current_stock."*'".$user_id."'*'".$pc_date_time."'"));
			
			//current adjust
			$currentStock = return_field_value("current_stock","product_details_master","id=".$hiddenProdId);
			$currentStock = $currentStock-$txtIssueQnty;
			$updateID_array[]=$hiddenProdId; 
			$update_data[$hiddenProdId]=explode("*",("".$txtIssueQnty."*".$currentStock."*'".$user_id."'*'".$pc_date_time."'"));
			$prod_field_array = "last_issued_qnty*current_stock*updated_by*update_date";
			$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array,$update_data,$updateID_array));
		}
		//product master table data UPDATE END-------------------------------// 
		 		 
 		
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
 		$deletePropor = execute_query("DELETE FROM order_wise_pro_details WHERE dtls_id = $dtls_tbl_id and entry_form=16");
		$deleteRoll = execute_query("DELETE FROM pro_roll_details WHERE dtls_id = $dtls_tbl_id and entry_form=16");
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
		
 		
 		//####################################################--------------------
		//order_wise_pro_details table data insert Start---------------// 
		$proportQ=true; 
		$data_array_prop="";
		$save_string=explode(",",str_replace("'","",$save_data));
		if(count($save_string)>0 && str_replace("'","",$save_data)!="")  
		{		 
			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,dtls_id,po_breakdown_id,prod_id,quantity,inserted_by,insert_date";
			$po_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if( array_key_exists($order_id,$po_array) )
					$po_array[$order_id]+=$order_qnty;
				else
					$po_array[$order_id]=$order_qnty;
			}
			$i=0; 
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$data_array_prop.="(".$id_proport.",".$transactionID.",2,16,".$dtls_id.",".$order_id.",".$hiddenProdId.",".$order_qnty.",".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}
		}//end if
		//order_wise_pro_details table data insert END -----//
		
		 
		  
		 
		 
		//####################################################--------------------
		//roll table entry----------------------------------------//
		if( str_replace("'","",$hidden_is_roll_maintain)==1)
		{
			$data_array="";
			$rollDtlsID = return_next_id("id", "pro_roll_details", 1);
			$field_array = "id,mst_id,dtls_id,po_breakdown_id,entry_form,roll_no,roll_id,qnty,inserted_by,insert_date";
			
			$poIDarr = explode(",",str_replace("'","",$txtRollPOid));
			$rollNoarr = explode(",",str_replace("'","",$txtRollNo));
			$rollPoQntyarr = explode(",",str_replace("'","",$txtRollPOQnty));			
			$lopSize = count($poIDarr); 
			for($i=0;$i<$lopSize;$i++)
			{  			 					
				$rollno = return_field_value("roll_no","pro_roll_details","id=".$rollNoarr[$i]);
				if($i>0) $data_array .=",";   
				$data_array .= "(".$rollDtlsID.",".$id.",".$dtls_id.",".$poIDarr[$i].",16,'".$rollno."',".$rollNoarr[$i].",".$rollPoQntyarr[$i].",'".$user_id."','".$pc_date_time."')";
			 	$rollDtlsID = $rollDtlsID+1;
			}
			$rollDtls=sql_insert("pro_roll_details",$field_array,$data_array,1);	
		}
		else
		{
			 $rollDtls=true;
		}
		//roll table entry end------------------------------------//
		

		

		//echo "20**".$rID." && ".$dtlsrID." && ".$prodUpdate." && ".$proportQ." && ".$batchQry." && ".$batchQryDtls." && ".$rollDtls; 
		//mysql_query("ROLLBACK");die; 
		
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($rID && $dtlsrID && $transID && $prodUpdate && $proportQ && $batchQry && $batchQryDtls && $rollDtls)
		{
			mysql_query("COMMIT");  
			echo "1**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$hidden_system_id);
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo "10**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$hidden_system_id);
		}
		if($db_type==2 || $db_type==1 )
		{
			echo "1**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$hidden_system_id);
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		 //-------
	}		
}





if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(sys_number)
	{
 		$("#hidden_sys_number").val(sys_number); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Issue Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
 							 echo create_drop_down( "cbo_supplier", 150, "select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td> 
                        <?php  
                            $search_by = array(1=>'Issue No',2=>'Challan No',3=>'In House',4=>'Out Bound Subcontact',5=>'Job No',6=>'Wo No',7=>'Buyer');
							$dd="change_search_event(this.value, '0*0*1*1*0*0*1', '0*0*select id,company_name from lib_company where status_active=1 and is_deleted=0 order by company_name*select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) order by supplier_name*0*0*select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$company $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_sys_number" value="hidden_sys_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div" style="margin-top:10px"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
 	$sql_cond="";
	if( str_replace("'","",$fromDate)!="" && str_replace("'","",$toDate)!="" ) $sql_cond .= " and issue_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
	if($supplier!="" && $supplier*1!=0) $sql_cond .= " and supplier_id='$supplier'";
	if($company!="" && $company*1!=0) $sql_cond .= " and company_id='$company'";
	
 	$supplier_arr = return_library_array("select id, supplier_name from lib_supplier",'id','supplier_name');
	$company_arr = return_library_array("select id, company_name from lib_company",'id','company_name');
	$buyer_arr = return_library_array("select id, buyer_name from lib_buyer",'id','buyer_name');
	$store_arr = return_library_array("select id, store_name from lib_store_location",'id','store_name');
	
 	if($txt_search_common!="" || $txt_search_common!=0)
	{
		if($txt_search_by==1)
		{	
			$sql_cond .= " and issue_number like '%$txt_search_common%'";			
		}
		else if($txt_search_by==2)
		{		
			$sql_cond .= " and challan_no like '%$txt_search_common%'";	
 		}
		else if($txt_search_by==3)
		{
			$sql_cond .= " and knit_dye_source=1 and knit_dye_company='$txt_search_common'";
		}
		else if($txt_search_by==4)
		{
			$sql_cond .= " and knit_dye_source=2 and knit_dye_company='$txt_search_common'";
		}
		else if($txt_search_by==5)
		{
			$sql_cond .= " and buyer_job_no like '%$txt_search_common%'";	
		}
		else if($txt_search_by==6)
		{
			$sql_cond .= " and booking_no like '%$txt_search_common%'";	
		}
		else if($txt_search_by==7)
		{
			$sql_cond .= " and buyer_id = '$txt_search_common'";	
		}
	}
		
	$sql = "select id,issue_number,issue_basis,issue_purpose,entry_form,item_category,company_id,location_id,supplier_id,store_id,buyer_id,buyer_job_no,style_ref,booking_id,booking_no,issue_date,sample_type,knit_dye_source,knit_dye_company,challan_no,loan_party,lap_dip_no,gate_pass_no,item_color,color_range,remarks,other_party 
			from inv_issue_master 
			where status_active=1 and entry_form=16 $sql_cond order by issue_number";
	//echo $sql;
	$result = sql_select( $sql );
	?>
    	<div>
            <div style="width:945px;">
                <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" border="1" rules="all">
                    <thead>
                        <th width="30">SL</th>
                        <th width="120">Issue No</th>
                        <th width="80">Date</th>     
                        <th width="100">Purpose</th>        
                        <th width="80">Challan No</th>
                        <th width="100">Issue Qnty</th>
                        <th width="120">Booking No</th>
                        <th width="120">Dyeing Company</th>
                        <th width="">Buyer</th>
                     </thead>
                </table>
             </div>
            <div style="width:945px;overflow-y:scroll;max-height:230px;" id="search_div" >
                <table cellspacing="0" cellpadding="0" width="927" class="rpt_table" id="list_view" border="1" rules="all">
        <?php	
            $i=1;   
            foreach( $result as $row ){
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
					
				$issuQnty = return_field_value("sum(cons_quantity)","inv_transaction","mst_id=".$row[csf("id")]." and item_category=13 and transaction_type=2 and status_active=1 and is_deleted=0");	
        ?>
                        
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer"  onclick="js_set_value('<?php echo $row[csf("issue_number")];?>');"> 
                            <td width="30"><?php echo $i; ?></td>	
                            <td width="120"><p><?php echo $row[csf("issue_number")];?></p></td>              	            			
                            <td width="80"><p><?php echo change_date_format($row[csf("issue_date")]); ?></p></td>								
                            <td width="100"><p><?php echo $yarn_issue_purpose[$row[csf("issue_purpose")]]; ?></p></td>					
                            <td width="80"><p><?php echo $row[csf("challan_no")]; ?></p></td>
                            <td width="100" align="right"><p><?php echo number_format($issuQnty,2,'.',''); ?>&nbsp;</p></td>
                            <td width="120"><p><?php echo $row[csf("booking_no")]; ?></p></td>
                            <td width="120"><p><?php 
                                if($row[csf("knit_dye_source")]==1) $knit_com=$company_arr[$row[csf("knit_dye_company")]]; else $knit_com=$supplier_arr[$row[csf("knit_dye_company")]];
                             echo $knit_com; 
                             ?></p>
                            </td>
                            <td width=""><p><?php echo $buyer_arr[$row[csf("buyer_id")]]; ?></p></td>
                         </tr>
                        <?php
                        $i++;
                        }
                        ?>
                </table>
            </div>
        </div>
    <?php
	 
	exit();
	
}

 
 

if($action=="populate_data_from_data")
{
	
	$sql = "select id,issue_number,issue_basis,issue_purpose,entry_form,item_category,company_id,location_id,supplier_id,store_id,buyer_id,buyer_job_no,style_ref,booking_id,booking_no,batch_no,issue_date,sample_type,knit_dye_source,knit_dye_company,challan_no,loan_party,lap_dip_no,gate_pass_no,item_color,color_range,remarks,other_party,order_id
			from inv_issue_master 
			where issue_number='$data' and entry_form=16";
	//echo $sql;
	$res = sql_select($sql);	
	foreach($res as $row)
	{		
		echo "$('#hidden_system_id').val(".$row[csf("id")].");\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		//echo"get_php_form_data( 'requires/grey_fabric_issue_controller', ".$row[csf("company_id")].", 'load_drop_down_store', 'store_td' );\n";
		echo"get_php_form_data(".$row[csf("company_id")].", 'is_roll_maintain', 'requires/grey_fabric_issue_controller');\n"; 
 		//echo"load_drop_down( 'requires/grey_fabric_issue_controller', ".$row[csf("company_id")].", 'load_drop_down_store', 'store_td' );\n";
		echo "$('#cbo_basis').val(".$row[csf("issue_basis")].");\n";
		echo "$('#cbo_issue_purpose').val(".$row[csf("issue_purpose")].");\n";
		echo "$('#txt_issue_date').val('".change_date_format($row[csf("issue_date")])."');\n";
		echo "$('#txt_booking_id').val(".$row[csf("booking_id")].");\n";
		echo "$('#txt_booking_no').val('".$row[csf("booking_no")]."');\n"; 
		echo "$('#cbo_dyeing_source').val(".$row[csf("knit_dye_source")].");\n";
		echo "load_drop_down( 'requires/grey_fabric_issue_controller', ".$row[csf("knit_dye_source")]."+'**'+".$row[csf("company_id")].", 'load_drop_down_knit_com', 'dyeing_company_td' );\n";
		echo "$('#cbo_dyeing_company').val(".$row[csf("knit_dye_company")].");\n";
		 	
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		$batchNo = return_field_value("batch_no","pro_batch_create_mst","id=".$row[csf("batch_no")]."");
		echo "$('#txt_batch_no').val('".$batchNo."');\n";	
		echo "$('#txt_batch_id').val(".$row[csf("batch_no")].");\n";	
		
		echo "$('#cbo_buyer_name').val(".$row[csf("buyer_id")].");\n";
		echo "$('#txt_style_ref').val('".$row[csf("style_ref")]."');\n";
		
		if($row[csf("order_id")]!="")
		{
			$orSql = sql_select("select po_number from wo_po_break_down where id in (".$row[csf("order_id")].")");
			$orderNumbers="";
			foreach($orSql as $key=>$val)
			{
				if($orderNumbers!="") $orderNumbers .=",";
				$orderNumbers .= $val[csf("po_number")];
			}
			echo "$('#txt_order_no').val('".$orderNumbers."');\n";
			echo "$('#hidden_order_id').val('".$row[csf("order_id")]."');\n";	
		}
		//echo "enable_disable();\n";
				
  	}
		
	exit();	
}




if($action=="show_dtls_list_view")
{
		
	$sql = "select b.id,a.issue_number,a.challan_no,b.issue_qnty,b.color_id,b.yarn_lot,b.yarn_count,b.store_name,d.product_name_details
			from inv_issue_master a, inv_grey_fabric_issue_dtls b, product_details_master d 
			where a.id=b.mst_id and b.prod_id=d.id and a.entry_form=16 and a.id=$data";
	//echo $sql;
	$result = sql_select($sql);	
	
 	$yarn_count = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');	
	$store_arr = return_library_array("select id, store_name from lib_store_location",'id','store_name');
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	//$arr=array(5=>$yarn_count,6=>$store_arr);
	
	?>
    <table cellspacing="0" cellpadding="0" width="1000" class="rpt_table" border="1" rules="all">
        <thead>
            <th width="35">SL</th>
            <th width="125">Issue No</th>
            <th width="220">Item Description</th>     
            <th width="100">Quantity</th>        
            <th width="90">Challan No</th>
            <th width="90">Color</th>
            <th width="100">Yarn Lot</th>
            <th width="100">Count</th>
            <th>Store</th>
         </thead>
    </table>
    <div style="width:1000px;overflow-y:scroll;max-height:210px;" id="search_div" >
        <table cellspacing="0" cellpadding="0" width="982" class="rpt_table" id="list_view" border="1" rules="all">
        <?php	
            $i=1;   
            foreach( $result as $row )
			{
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
				
				$count_id=array_unique(explode(",",$row[csf("yarn_count")])); $count='';
				foreach($count_id as $val)
				{
					if($count=='') $count=$yarn_count[$val]; else $count.=",".$yarn_count[$val];
				}	
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer"  onclick="get_php_form_data('<?php echo $row[csf("id")]; ?>','populate_child_from_data_for_update','requires/grey_fabric_issue_controller');"> 
                    <td width="35"><?php echo $i; ?></td>	
                    <td width="125"><p><?php echo $row[csf("issue_number")];?></p></td>              	            			
                    <td width="220"><p><?php echo $row[csf("product_name_details")]; ?></p></td>								
                    <td width="100" align="right"><p><?php echo number_format($row[csf("issue_qnty")],2); ?></p></td>					
                    <td width="90"><p><?php echo $row[csf("challan_no")]; ?>&nbsp;</p></td>
                    <td width="90"><p><?php echo $color_arr[$row[csf("color_id")]]; ?>&nbsp;</p></td>
                    <td width="100"><p><?php echo $row[csf("yarn_lot")]; ?>&nbsp;</p></td>
                    <td width="100"><p><?php echo $count; ?>&nbsp;</p></td>
                    <td><p><?php echo $store_arr[$row[csf("store_name")]]; ?>&nbsp;</p></td>
                 </tr>
			<?php
            $i++;
            }
            ?>
        </table>
    </div>
    <?php
	//echo  create_list_view("tbl_list_search", "Issue No,Item Description,Quantity,Challan No,Yarn Lot,Count,Store", "130,300,90,90,90,100,100","1000","250",0, $sql, "get_php_form_data", "id", "'populate_child_from_data_for_update'", 1, "0,0,0,0,0,yarn_count,store_name", $arr, "issue_number,product_name_details,issue_qnty,challan_no,yarn_lot,yarn_count,store_name", 'requires/grey_fabric_issue_controller','','0,0,2,0,0,0,0');
	 
	exit();
}



if($action=="populate_child_from_data_for_update")
{
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sql = "select b.id, a.issue_number, a.challan_no, a.order_id, b.distribution_method, b.no_of_roll, b.roll_no, b.roll_po_id, b.roll_wise_issue_qnty, b.prod_id, b.issue_qnty, b.color_id, b.yarn_lot, b.yarn_count, b.store_name, b.remarks, d.product_name_details,d.current_stock
			from inv_issue_master a, inv_grey_fabric_issue_dtls b, product_details_master d 
			where a.id=b.mst_id and b.prod_id=d.id and a.entry_form=16 and b.id=$data";
	//echo $sql;
	$result = sql_select($sql);		
	foreach($result as $row)
	{			
		echo "$('#cbo_store_name').val('".$row[csf('store_name')]."');\n";
		echo "$('#txtNoOfRoll').val('".$row[csf('no_of_roll')]."');\n";
		echo "$('#txtRollNo').val('".$row[csf('roll_no')]."');\n";
		echo "$('#txtRollPOid').val('".$row[csf('roll_po_id')]."');\n";
		echo "$('#txtRollPOQnty').val('".$row[csf('roll_wise_issue_qnty')]."');\n";
		
		echo "$('#txtItemDescription').val('".$row[csf('product_name_details')]."');\n";
		echo "$('#hiddenProdId').val('".$row[csf('prod_id')]."');\n";
		
		echo "$('#txtReqQnty').val('".$row[csf('issue_qnty')]."');\n";
		echo "$('#txtIssueQnty').val('".$row[csf('issue_qnty')]."');\n";
		echo "$('#hiddenIssueQnty').val('".$row[csf('issue_qnty')]."');\n";
		
		echo "$('#txtYarnLot').val('".$row[csf('yarn_lot')]."');\n";
		//echo "$('#cbo_yarn_count').val('".$row[csf('yarn_count')]."');\n";
		if($row[csf('yarn_count')]=='' || $row[csf('yarn_count')]==0) $count=""; else $count=$row[csf('yarn_count')];
		echo "set_multiselect('cbo_yarn_count','0','1','".$count."','0');\n";
		echo "$('#txt_remarks').val('".$row[csf('remarks')]."');\n";
		//echo "disable_enable_fields('show_textcbo_yarn_count','1','','');\n";
		//issue qnty popup data arrange 
		$sqlIN = sql_select("select trans_id,po_breakdown_id,quantity from order_wise_pro_details where dtls_id=".$row[csf("id")]." and entry_form=16 and trans_type=2");
		$poWithValue="";
		$poID="";
		$transaction_id="";
		foreach($sqlIN as $res)
		{
			if($poWithValue!="") $poWithValue .=",";
			if($poID!="") $poID .=",";
			$poWithValue .= $res[csf("po_breakdown_id")]."**".$res[csf("quantity")];
			$poID .=$res[csf("po_breakdown_id")];
			$transaction_id = $res[csf("trans_id")];
		}
		echo "$('#save_data').val('".$poWithValue."');\n"; 
		echo "$('#all_po_id').val('".$poID."');\n"; 
		echo "$('#distribution_method_id').val('".$row[csf('distribution_method')]."');\n";
		
		echo "load_drop_down( 'requires/grey_fabric_issue_controller', $poID+'_'+$row[color_id], 'load_drop_down_color','color_td');\n";
		echo "$('#cbo_color_id').val('".$row[csf('color_id')]."');\n";
		//--------hidden id for update------- 
		echo "$('#dtls_tbl_id').val('".$row[csf('id')]."');\n";
 		echo "$('#trans_tbl_id').val('".$transaction_id."');\n";
		echo "$('#hidden_yet_issue_qnty').val(".($row[csf('current_stock')]+$row[csf('issue_qnty')]).");\n";
		
		echo "get_php_form_data('".$row[csf('order_id')]."'+\"**\"+".$row[csf('prod_id')].", \"populate_data_about_order\", \"requires/grey_fabric_issue_controller\" );";
		echo "set_button_status(1, permission, 'fnc_grey_fabric_issue_entry',1,1);\n";
	}
	exit();
}

if ($action=="grey_fabric_issue_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql="select id, issue_number, issue_date, issue_basis, issue_purpose, knit_dye_source, knit_dye_company, booking_id, batch_no, buyer_id, challan_no, style_ref, order_id from  inv_issue_master where id='$data[1]' and company_id='$data[0]'";
	//echo $sql;die;
	
	$dataArray=sql_select($sql);
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$batch_arr=return_library_array( "select id, batch_no from  pro_batch_create_mst", "id", "batch_no"  );
	$booking_arr=return_library_array( "select id, booking_no from  wo_booking_mst", "id", "booking_no"  );
	$po_arr=return_library_array( "select id, po_number from  wo_po_break_down", "id", "po_number"  );
 	$yarn_count = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');	
	
?>
<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u><?php echo $data[2]; ?> Challan</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Issue ID :</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="125"><strong>Issue Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
            <td width="130"><strong>Issue Basis :</strong></td> <td width="175px"><?php echo $issue_basis[$dataArray[0][csf('issue_basis')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Issue Purpose:</strong></td><td width="175px"><?php echo $yarn_issue_purpose[$dataArray[0][csf('issue_purpose')]]; ?></td>
            <td><strong>Dyeing Source:</strong></td><td width="175px"><?php echo $knitting_source[$dataArray[0][csf('knit_dye_source')]]; ?></td>
            <td><strong>Dyeing Company:</strong></td> <td width="175px"><?php if ($dataArray[0][csf('knit_dye_source')]==1) echo $company_library[$dataArray[0][csf('knit_dye_company')]];else if ($dataArray[0][csf('knit_dye_source')]==3) echo $supplier_library[$dataArray[0][csf('knit_dye_company')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Fab Booking No:</strong></td><td width="175px"><?php echo $booking_arr[$dataArray[0][csf('booking_id')]]; ?></td>
            <td><strong>Batch Number:</strong></td><td width="175px"><?php echo $batch_arr[$dataArray[0][csf('batch_no')]]; ?></td>
            <td><strong>Buyer Name:</strong></td><td width="175px"><?php echo $buyer_library[$dataArray[0][csf('buyer_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Challan No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Style Ref.:</strong></td><td width="175px"><?php echo $dataArray[0][csf('style_ref')]; ?></td>
            <td><strong>Order No:</strong></td><td width="175px"><?php echo $po_arr[$dataArray[0][csf('order_id')]]; ?></td>
        </tr>
    </table>
        <br>
<div style="width:100%;">
    <table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="20">SL</th>
            <th width="140" >Item Des.</th>
            <th width="50" >GSM</th>
            <th width="50" >Gray Dia</th>
            <th width="50" >Fin. Dia</th>
            <th width="100" >Color</th>
            <th width="50" >Roll</th>
            <th width="40" >UoM</th>
            <th width="80" >Issue Qnty</th> 
            <th width="70" >Yarn Lot</th>
            <th width="50" >Count</th>
            <th width="110" >Store</th> 
            <th width="" >Remarks</th> 
        </thead>
        <tbody> 
   
<?php
	$color_arr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sql_dtls = "select b.id, a.booking_id, b.issue_qnty, b.no_of_roll, b.yarn_lot, b.yarn_count, b.store_name, b.color_id, b.remarks, d.product_name_details, d.gsm, d.dia_width as gray_dia, d.color, d.unit_of_measure from inv_issue_master a, inv_grey_fabric_issue_dtls b, product_details_master d where a.id=b.mst_id and b.prod_id=d.id and a.entry_form=16 and a.id=$data[1]";
	//echo $sql;
	$sql_result= sql_select($sql_dtls);
	$i=1;
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
			$issue_qnty=$row[csf('issue_qnty')];
			$issue_qnty_sum += $issue_qnty;
			$item_des=explode(',',$row[csf("product_name_details")]);
			//print_r ($item_des);
			if($item_des[0]!='' && $item_des[1]!='')
			{
				$item_name_details=$item_des[0].', '.$item_des[1];
			}
			else
			{
				$item_name_details='';
			}
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><?php echo $i; ?></td>
                <td><p><?php echo $item_name_details; //echo $row[csf("product_name_details")]; ?></p></td>
                <td align="center"><p><?php echo $row[csf("gsm")]; ?></p></td>
                <td align="center"><p><?php echo $row[csf("gray_dia")]; ?></p></td>
                <td align="center"><p><?php //echo $row[csf("booking_id")]; ?></p></td>
                <td align="center"><p><?php echo $color_arr[$row[csf("color_id")]]; ?></p></td>
                <td align="center"><p><?php echo $row[csf("no_of_roll")]; ?></p></td>
                <td align="center"><p><?php echo $unit_of_measurement[$row[csf("unit_of_measure")]]; ?></p></td>
                <td align="right"><p><?php echo $row[csf("issue_qnty")]; ?></p></td>
                <td align="center"><p><?php echo $row[csf("yarn_lot")]; ?></p></td>
                <td align="center"><p><?php echo $yarn_count[$row[csf("yarn_count")]]; ?></p></td>
                <td align="center"><p><?php echo $store_library[$row[csf("store_name")]]; ?></p></td>
                <td><p><?php echo $row[csf("remarks")]; ?></p></td>
			</tr>
			<?php $i++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="8" align="right"><strong>Total :</strong></td>
                <td align="right"><?php echo $issue_qnty_sum; ?></td>
                <td align="right" colspan="4"><?php //echo $req_qny_edit_sum; ?></td>
            </tr>                           
        </tfoot>
      </table>
        <br>
		 <?php
            echo signature_table(17, $data[0], "900px");
         ?>
      </div>
   </div>          
<?php
exit();
}
?>


