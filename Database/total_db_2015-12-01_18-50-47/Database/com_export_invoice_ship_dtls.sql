-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_export_invoice_ship_dtls`
--

DROP TABLE IF EXISTS `com_export_invoice_ship_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_export_invoice_ship_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL,
  `po_breakdown_id` int(11) NOT NULL,
  `current_invoice_rate` double NOT NULL DEFAULT '0',
  `current_invoice_qnty` int(11) NOT NULL DEFAULT '0',
  `current_invoice_value` double NOT NULL DEFAULT '0',
  `production_source` tinyint(1) NOT NULL DEFAULT '0',
  `color_size_rate_data` text NOT NULL,
  `actual_po_infos` text NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `mst_id` (`mst_id`),
  KEY `po_breakdown_id` (`po_breakdown_id`)
) ENGINE=InnoDB AUTO_INCREMENT=465 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_export_invoice_ship_dtls`
--

LOCK TABLES `com_export_invoice_ship_dtls` WRITE;
/*!40000 ALTER TABLE `com_export_invoice_ship_dtls` DISABLE KEYS */;
INSERT INTO `com_export_invoice_ship_dtls` VALUES (1,1,146,4.3,2200,9460,1,'','',1,'2015-02-26 11:42:39',0,'0000-00-00 00:00:00',1,0),(3,2,21,2.75,1260,3465,1,'','',1,'2015-05-09 13:37:50',0,'0000-00-00 00:00:00',1,0),(4,3,146,4.3,45,193.5,1,'','',13,'2015-05-12 18:25:27',0,'0000-00-00 00:00:00',1,0),(11,4,163,0.13,110600,14378,1,'','',13,'2015-05-13 10:36:30',0,'0000-00-00 00:00:00',1,0),(12,4,164,0.13,184600,23998,1,'','',13,'2015-05-13 10:36:30',0,'0000-00-00 00:00:00',1,0),(13,4,165,0.13,147600,19188,1,'','',13,'2015-05-13 10:36:30',0,'0000-00-00 00:00:00',1,0),(15,5,237,1.95,9537,18597.15,1,'','',13,'2015-05-31 10:37:15',0,'0000-00-00 00:00:00',1,0),(22,6,268,4.2,360,1512,1,'','',13,'2015-10-06 16:20:52',0,'0000-00-00 00:00:00',1,0),(23,6,269,3.9,264,1029.6,1,'','',13,'2015-10-06 16:20:52',0,'0000-00-00 00:00:00',1,0),(38,8,316,3.7,140,518,1,'','',1,'2015-10-19 14:49:38',0,'0000-00-00 00:00:00',1,0),(39,8,318,4.2,300,1260,1,'','',1,'2015-10-19 14:49:38',0,'0000-00-00 00:00:00',1,0),(63,9,315,3.6,1992,7171.2,1,'','',13,'2015-10-20 10:19:01',0,'0000-00-00 00:00:00',1,0),(64,9,320,4.1,4160,17056,1,'','',13,'2015-10-20 10:19:01',0,'0000-00-00 00:00:00',1,0),(71,10,368,3.4,4000,13600,1,'','',13,'2015-10-22 09:48:37',0,'0000-00-00 00:00:00',1,0),(72,10,370,4.6,2050,9430,1,'','',13,'2015-10-22 09:48:37',0,'0000-00-00 00:00:00',1,0),(187,11,625,1,8,8,1,'','',1,'2015-10-25 13:44:19',0,'0000-00-00 00:00:00',1,0),(253,13,0,0,0,2292.78,0,'','',13,'2015-10-29 10:24:45',0,'0000-00-00 00:00:00',1,0),(272,14,388,3.22,1800,5796,1,'','',13,'2015-10-31 15:13:46',0,'0000-00-00 00:00:00',1,0),(273,14,389,2.42,2024,4898.08,1,'','',13,'2015-10-31 15:13:46',0,'0000-00-00 00:00:00',1,0),(274,14,384,2.56,1676,4290.56,1,'','',13,'2015-10-31 15:13:46',0,'0000-00-00 00:00:00',1,0),(275,14,385,2.56,1644,4208.64,1,'','',13,'2015-10-31 15:13:46',0,'0000-00-00 00:00:00',1,0),(276,14,386,2.56,1720,4403.2,1,'','',13,'2015-10-31 15:13:46',0,'0000-00-00 00:00:00',1,0),(277,14,387,3.18,1802,5730.36,1,'','',13,'2015-10-31 15:13:46',0,'0000-00-00 00:00:00',1,0),(290,15,408,3.49,1520,5304.8,1,'','',13,'2015-10-31 17:31:53',0,'0000-00-00 00:00:00',1,0),(291,15,422,6.48,1496,9694.08,1,'','',13,'2015-10-31 17:31:53',0,'0000-00-00 00:00:00',1,0),(292,15,429,6.88,2260,15548.8,1,'','',13,'2015-10-31 17:31:53',0,'0000-00-00 00:00:00',1,0),(293,15,472,6.88,2190,15067.2,1,'','',13,'2015-10-31 17:31:53',0,'0000-00-00 00:00:00',1,0),(313,7,397,2.78,1521,4228.38,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(314,7,398,2.78,1500,4170,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(315,7,399,2.78,1500,4170,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(316,7,400,2.78,2460,6838.8,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(317,7,401,2.18,3664,7987.52,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(318,7,403,2.18,3347,7296.46,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(319,7,404,2.38,909,2163.42,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(320,7,406,2.64,1478,3901.92,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(321,7,409,2.38,1000,2380,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(322,7,410,3.11,2259,7025.49,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(323,7,412,2.38,1500,3570,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(324,7,413,2.38,1516,3608.08,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(325,7,414,2.38,1500,3570,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(326,7,415,2.64,1500,3960,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(327,7,416,2.64,1516,4002.24,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(328,7,417,2.18,3660,7978.8,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(329,7,418,3.11,2272,7065.92,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(330,7,426,2.78,1513,4206.14,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(331,7,427,2.78,1500,4170,1,'','',13,'2015-11-04 09:00:38',0,'0000-00-00 00:00:00',1,0),(332,12,402,2.78,2458,6833.24,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(333,12,405,2.18,3446,7512.28,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(334,12,407,3.49,1521,5308.29,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(335,12,411,2.64,1322,3490.08,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(336,12,419,5.82,2048,11919.36,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(337,12,420,5.82,2051,11936.82,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(338,12,421,6.48,1965,12733.2,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(339,12,428,2.78,1431,3978.18,1,'','',13,'2015-11-11 09:34:26',0,'0000-00-00 00:00:00',1,0),(358,16,392,2.6,6287,16346.2,1,'','',13,'2015-11-16 09:27:38',0,'0000-00-00 00:00:00',1,0),(359,16,423,1.59,5230,8315.7,1,'','',13,'2015-11-16 09:27:38',0,'0000-00-00 00:00:00',1,0),(360,16,486,1.4,4038,5653.2,1,'','',13,'2015-11-16 09:27:38',0,'0000-00-00 00:00:00',1,0),(361,17,393,2.6,5961,15498.6,1,'','',13,'2015-11-16 09:31:53',0,'0000-00-00 00:00:00',1,0),(374,19,312,4.4,3480,15312,1,'','',13,'2015-11-16 18:20:56',0,'0000-00-00 00:00:00',1,0),(375,19,314,3.5,2976,10416,1,'','',13,'2015-11-16 18:20:56',0,'0000-00-00 00:00:00',1,0),(376,18,333,3.6,2104,7574.4,1,'','',13,'2015-11-16 18:28:29',0,'0000-00-00 00:00:00',1,0),(377,18,332,3.5,2104,7364,1,'','',13,'2015-11-16 18:28:29',0,'0000-00-00 00:00:00',1,0),(380,20,395,2.35,20345,47810.75,1,'','',13,'2015-11-19 11:46:13',0,'0000-00-00 00:00:00',1,0),(384,21,390,2.4,21894,52545.6,1,'','',13,'2015-11-19 11:51:58',0,'0000-00-00 00:00:00',1,0),(391,22,376,10.3,4274,44022.2,1,'','',13,'2015-11-19 12:01:57',0,'0000-00-00 00:00:00',1,0),(392,22,377,11.8,5390,63602,1,'','',13,'2015-11-19 12:01:57',0,'0000-00-00 00:00:00',1,0),(395,23,526,1.85,4008,7414.8,1,'','',13,'2015-11-24 10:21:51',0,'0000-00-00 00:00:00',1,0),(402,24,335,4,264,1056,1,'','',13,'2015-11-24 10:31:09',0,'0000-00-00 00:00:00',1,0),(403,24,336,3.6,192,691.2,1,'','',13,'2015-11-24 10:31:09',0,'0000-00-00 00:00:00',1,0),(410,25,331,3.9,3099,12086.1,1,'','',13,'2015-11-24 10:57:45',0,'0000-00-00 00:00:00',1,0),(429,26,372,2.5,1988,4970,1,'','',13,'2015-11-24 11:29:20',0,'0000-00-00 00:00:00',1,0),(430,26,373,2.45,2014,4934.3,1,'','',13,'2015-11-24 11:29:20',0,'0000-00-00 00:00:00',1,0),(431,26,371,2.45,4264,10446.8,1,'','',13,'2015-11-24 11:29:20',0,'0000-00-00 00:00:00',1,0),(436,27,334,4,4484,17936,1,'','',13,'2015-11-26 12:29:57',0,'0000-00-00 00:00:00',1,0),(439,28,526,1.85,800,1480,1,'','',13,'2015-11-28 12:32:02',0,'0000-00-00 00:00:00',1,0),(460,29,328,4.3,2240,9632,1,'','',13,'2015-11-28 16:52:39',0,'0000-00-00 00:00:00',1,0),(461,29,329,8.45,530,4478.5,1,'','',13,'2015-11-28 16:52:39',0,'0000-00-00 00:00:00',1,0),(462,29,356,9.2,1327,12208.4,1,'','',13,'2015-11-28 16:52:39',0,'0000-00-00 00:00:00',1,0),(463,29,357,6.8,3709,25221.2,1,'','',13,'2015-11-28 16:52:39',0,'0000-00-00 00:00:00',1,0),(464,29,358,6.85,1933,13241.05,1,'','',13,'2015-11-28 16:52:39',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `com_export_invoice_ship_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
