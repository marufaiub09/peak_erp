﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
if ($action=="load_supplier_dropdown")
{
	//echo $data;
	$data = explode('_',$data);
	
	if ($data[1]==0) echo create_drop_down( "cbo_supplier_id", 165, $blank_array,'', 1, '----Select----',0,0,0);
	
	if($data[1]==1)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =2",'id,supplier_name', 1, '----Select----',0,0,0);		
	}
	if($data[1]==2 || $data[1]==3 || $data[1]==13 || $data[1]==14)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =9",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==4)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(4,5)",'id,supplier_name', 1, '----Select----',0,0,0);
		
	}
	if($data[1]==5 || $data[1]==6 || $data[1]==7)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type=3",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==8)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 7",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==9 || $data[1]==10)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 6",'id,supplier_name', 1, '----Select----',0,0,0);
	} 
	if($data[1]==11)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 8",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	if($data[1]==12)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(20,21,22,23,24,30,31,32,35,36,37,38,39)",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	
	exit();  
}
if($action=="set_maturity_date")
{
	$data=explode("_",$data);
	$date=change_date_format($data[0],'yyyy-mm-dd','-');
	echo date('d-m-Y', strtotime($date. ' + '.$data[1].' days'));
}

if($action=="check_duplicate_invoice")
{
	$data=explode("_",$data);
	$invoice_no=return_field_value("invoice_no","com_import_invoice_mst","invoice_no='$data[0]' and  btb_lc_id in($data[1]) and status_active=1 and is_deleted=0");
	echo trim($invoice_no);
	die;
}
 
 
if($action=="open_import_lc_popup")
{
	echo load_html_head_contents("BTB / Import LC List", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<script>
		/*function js_set_value(id)
		{
			$('#hidden_btb_id').val(id);
			parent.emailwindow.hide();
		}*/
		var selected_id = new Array;
		//var selected_name = new Array();	
	 function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length-1; 

			tbl_row_count = tbl_row_count;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				//js_set_value( i );
				document.getElementById( 'tr_' + i ).click();
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			var str_array=str.split("_");
			toggle( document.getElementById( 'tr_' + str_array[0] ), '#FFFFCC' );
			
			if( jQuery.inArray( str_array[1], selected_id ) == -1 ) {
				selected_id.push( str_array[1] );
				//selected_name.push( str_array[2] );

				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str_array[1] ) break;
				}
				selected_id.splice( i, 1 );
				//selected_name.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				//name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			//name = name.substr( 0, name.length - 1 );

			
			$('#hidden_btb_id').val( id );
			//$('#hidden_btb_number').val( id );
			
		}
	
    </script>
</head>

<body>
<div align="center" style="width:1000px;">
	<form name="searchscfrm"  id="searchscfrm">
		<fieldset style="width:100%;">
            <legend>Enter search words</legend>           
            	<table cellpadding="0" cellspacing="0" width="950" class="rpt_table">
                	<thead>
                    	<th>Item Category</th>
                    	<th>Company</th>
                        <th>Supplier</th>
                        <th>Issuing Bank</th>
                        <th>L/C Type</th>
                        <th>Currency</th>
                        <th>
                        	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                            <input type="hidden" id="hidden_btb_id" />
                        	<!--<input type="hidden" name="hidden_btb_number" id="hidden_btb_number" value="" />-->
                        </th>
                    </thead>
                    <tr class="general">
                    	
                        <td> 
                             <?php echo create_drop_down( "cbo_item_category_id", 165, $item_category,'', 1, '--Select--',0,"",0); ?>  
                        </td>
                        <td>
                           <?php 
								echo create_drop_down( "txt_company_id",165,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, 'Select',0,"load_drop_down( 'import_document_acceptance_non_lc_controller',this.value+'_'+document.getElementById('cbo_item_category_id').value,'load_supplier_dropdown','supplier_td' );",0); 
							?>  
                         </td>
                         <td align="center" id="supplier_td">
                          <?php echo create_drop_down( "cbo_supplier_id", 165,$blank_array,'', 0, '',0,0,0); ?>       
                          
                         </td>            
						<td> 
                        	 <?php echo create_drop_down( "cbo_issuing_bank", 165,"select id,bank_name from lib_bank where is_deleted=0 and status_active=1 and issusing_bank = 1 order by bank_name",'id,bank_name', 1, '-Select-',0,0,""); ?>
                        </td>						
						<td>
							<?php echo create_drop_down( "cbo_lc_type_id",165,$lc_type,'',1,'-Select-',"","",""); ?>
                            
            			</td> 
                        <td>
							<?php echo create_drop_down( "cbo_lc_currency_id",70,$currency,'',"",'',2,0,""); ?>
                            
            			</td>                       
                         <td>
                 		  	<input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_company_id').value+'**'+document.getElementById('cbo_item_category_id').value+'**'+document.getElementById('cbo_supplier_id').value+'**'+document.getElementById('cbo_issuing_bank').value+'**'+document.getElementById('cbo_lc_type_id').value+'**'+document.getElementById('cbo_lc_currency_id').value, 'create_btb_search_list_view', 'search_div', 'import_document_acceptance_non_lc_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                         </td>
					</tr>
               </table>
               <table width="100%" style="margin-top:5px" align="center">
					<tr>
                    	<td colspan="5" id="search_div" align="center"></td>
                    </tr>
                </table> 
            </fieldset>
		</form>
	</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
 
}

if($action=="open_invoice_popup")
{
	echo load_html_head_contents("BTB / Import LC List", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		function js_set_value(id)
		{
			//alert(id)
			var id_array=id.split("_");
			$('#hidden_invoice_id').val(id_array[0]);
			$('#hidden_btb_id').val(id_array[1]);
			
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:800px;">
	<form name="searchscfrm"  id="searchscfrm">
		<fieldset style="width:100%;">
            <legend>Enter search words</legend>           
            	<table cellpadding="0" cellspacing="0" width="800" class="rpt_table">
                	<thead>
                    	<th>Item Category</th>
                    	<th>Company</th>
                        <th>Supplier</th>
                        <th>Invoice Date</th>
                        <th>
                        	<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                        	<input type="hidden" name="hidden_invoice_id" id="hidden_invoice_id" value="" />
                            <input type="hidden" id="hidden_btb_id" />
                        </th>
                    </thead>
                    <tr class="general">
                    	
                        <td> 
                             <?php echo create_drop_down( "cbo_item_category_id", 165, $item_category,'', 1, '--Select--',0,"",0); ?>  
                        </td>
                        <td>
                           <?php 
								echo create_drop_down( "txt_company_id",165,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, 'Select',0,"load_drop_down( 'import_document_acceptance_non_lc_controller',this.value+'_'+document.getElementById('cbo_item_category_id').value,'load_supplier_dropdown','supplier_td' );",0); 
							?>  
                         </td>
                         <td align="center" id="supplier_td">
                          <?php echo create_drop_down( "cbo_supplier_id", 165,$blank_array,'', 0, '',0,0,0); ?>       
                          
                         </td>            
						<td> 
                        	 <input type="text" name="btb_start_date" id="btb_start_date" class="datepicker" style="width:70px;" />To
                             <input type="text" name="btb_end_date" id="btb_end_date" class="datepicker" style="width:70px;" />
                        </td>						
						                     
                         <td>
                 		  	<input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_company_id').value+'**'+document.getElementById('cbo_item_category_id').value+'**'+document.getElementById('cbo_supplier_id').value+'**'+document.getElementById('btb_start_date').value+'**'+document.getElementById('btb_end_date').value, 'create_invoice_list_view', 'search_div', 'import_document_acceptance_non_lc_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                         </td>
					</tr>
               </table>
               <table width="100%" style="margin-top:5px" align="center">
					<tr>
                    	<td colspan="5" id="search_div" align="center"></td>
                    </tr>
                </table> 
            </fieldset>
		</form>
	</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
 
}



if($action=="create_btb_search_list_view")
{
	$data=explode('**',$data);
	$company_id = $data[0];
	$item_category_id = $data[1];
	$supplier_id = $data[2];
	$issuing_bank_id = $data[3];
	$lc_type_id = $data[4]; 
	$lc_currency_id = $data[5];
	
	if($company_id==0)
	{
		echo 'Select Importer';die;
	}

	if ($company_id!=0) $company=$company_id; else $company='%%';
	if ($item_category_id!=0) $item_category_id=$item_category_id; else $item_category_id='%%';
	if ($supplier_id!=0) $supplier=$supplier_id; else $supplier='%%';
	if ($issuing_bank_id!=0) $bank_name=$bank_name; else $bank_name='%%';
	if ($lc_type_id!=0) $lc_type_id=$lc_type_id; else $lc_type_id='%%';
	if ($lc_currency_id!=0) $lc_currency_id=$lc_currency_id; else $lc_currency_id='%%';

	/*if($item_category_id==0)
	{
		echo 'Select Item Category';die;
	}
	if($supplier_id==0)
	{
		echo 'Select Supplier';die;
	}
	if($issuing_bank_id==0)
	{
		echo 'Select Issuing Bank';die;
	}
	if($lc_type_id==0)
	{
		echo 'Select LC Type';die;
	}
	if($lc_currency_id==0)
	{
		echo 'Select Currency';die;
	}*/
	
	/*if($item_category_id!=0)
	{
		$cat_id=" and item_category_id = '$data[1]'";
	}
	else
	{
		$cat_id="";
	}*/
	
	/*if ($company_id!=0) $company=$company_id;
	if ($item_category_id!=0) $item_category=$item_category_id;
	if ($supplier_id!=0) $supplier=$supplier_id;
	if ($cbo_issuing_bank!=0) $cbo_issuing_bank=$cbo_issuing_bank; 
	
	if ($lc_end_date!='') $end_date=$lc_end_date; else $end_date='%%';
	if ($system_id!='') $system_number=$system_id; else $system_number='%%';

	if($lc_start_date!='' && $lc_end_date!='')
	{
		$date = "and application_date between '".$start_date."' and '".$end_date."'";
	}
	else
	{
		$date = "";
	}*/
	 
	$sql = "SELECT id,btb_prefix_number, btb_system_id, lc_number, supplier_id, application_date, last_shipment_date, lc_date, lc_value, item_category_id, supplier_id, importer_id,payterm_id FROM com_btb_lc_master_details WHERE  issuing_bank_id like '".$bank_name."' and importer_id like '".$company_id."' and supplier_id like '".$supplier."' and item_category_id like '".$item_category_id."' and lc_type_id like '".$lc_type_id."' and currency_id like '".$lc_currency_id."' and payterm_id=3 $date and status_active = 1 and is_deleted = 0 order by id";
	
	$supplier_lib=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name'); 
	 
	$arr=array(1=>$supplier_lib,7=>$pay_term);
			
	echo  create_list_view("list_view", "System Id,Supplier,L/C Number,L/C Date,L/C Value,Application Date,Last Ship Date,Pay Term", "90,150,150,100,120,100,100,100","1000","240",0, $sql , "js_set_value", "id,lc_number", "", 1, "0,supplier_id,0,0,0,0,0,payterm_id", $arr , "btb_prefix_number,supplier_id,lc_number,lc_date,lc_value,application_date,last_shipment_date,payterm_id", "",'','0,0,0,3,2,3,3,0',"",1) ;
	
	exit();			 
}


if($action=="create_invoice_list_view")
{
	$data=explode('**',$data);//0->company,1->Item Category, 2->Supplier,3->Start Date 4->End Date, 5->Search Text 
	$company_id = $data[0];
	$item_category_id = $data[1];
	$supplier_id = $data[2];
	$invoice_start_date = $data[3];
	$invoice_end_date = $data[4];
	
	if($company_id==0)
	{
		echo 'Select Importer';die;
	}
	if($item_category_id==0)
	{
		echo 'Select Item Category';die;
	}
	
	
	if ($company_id!=0) $company=$company_id;
	if ($item_category_id!=0) $item_category=$item_category_id;
	if ($supplier_id!=0) $supplier=$supplier_id; else $supplier='%%';
	if ($invoice_start_date!='') $start_date=$invoice_start_date; else $start_date='%%';
	if ($invoice_end_date!='') $end_date=$invoice_end_date; else $end_date='%%';
	if($lc_start_date!='' && $lc_end_date!='')
	{
		$date = "and b.invoice_date between '".$start_date."' and '".$end_date."'";
	}
	else
	{
		$date = "";
	}
	 
	$sql = "SELECT  a.importer_id,a.supplier_id,a.lc_number,a.lc_value,b.invoice_no,b.invoice_date ,b.	bank_ref,b.document_value,b.id,b.btb_lc_id as lc_id  FROM com_btb_lc_master_details a, com_import_invoice_mst b WHERE a.id=b.btb_lc_id and  a.importer_id = '".$company."' and a.supplier_id like '".$supplier."' and a.item_category_id = '".$item_category."' and is_lc=2 $date and a.status_active = 1 and a.is_deleted = 0 and b.status_active = 1 and b.is_deleted = 0";
	
	$supplier_lib=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name'); 
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$arr=array(0=>$company_library,1=>$supplier_lib);
	//function create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path, $filter_grid_fnc, $fld_type_arr, $summary_flds, $check_box_all )
		
	echo  create_list_view("list_view", "Company,Supplier,L/C Number,L/C Value,Invoice No,Invoice Date,Bank Ref.,Document Amount", "90,100,100,100,100,80,100,100","820","240",0, $sql , "js_set_value", "id,lc_id", "", 1, "importer_id,supplier_id,0,0,0,0,0,0", $arr , "importer_id,supplier_id,lc_number,lc_value,invoice_no,invoice_date,bank_ref,document_value", "",'','0,0,0,2,0,3,0,2') ;
		 
}

if($action=='populate_data_from_btb_lc')
{
	$data=explode("_",$data);
	
	$data_array=sql_select("select id,btb_system_id,lc_number,issuing_bank_id,lc_value,currency_id,tolerance,tenor,maturity_from_id,supplier_id,importer_id,payterm_id,lc_type_id,item_category_id,port_of_discharge,port_of_loading,inco_term_place,inco_term_id,delivery_mode_id from com_btb_lc_master_details where id in($data[0])"); 
	$lc_number="";
	$lc_id="";
	$lc_value=0;
	//echo count($data_array);
	//die;
	foreach ($data_array as $row)
	{ 
		if($data[1]==1)
		{
			$internal_file_no="";
			$is_lc_sc_sql = sql_select("SELECT lc_sc_id, is_lc_sc FROM com_btb_export_lc_attachment where import_mst_id='".$row[csf("id")]."'");
			list($is_lc_sc_sql_row)=$is_lc_sc_sql;
			if($is_lc_sc_sql_row[csf("is_lc_sc")] == 0)
			{
				$internal_file_sql = sql_select("SELECT internal_file_no FROM com_export_lc where id='".$is_lc_sc_sql_row[csf("lc_sc_id")]."'");
				list($internal_file_sql_row)=$internal_file_sql;
				$internal_file_no=$internal_file_no.$internal_file_sql_row[csf("internal_file_no")].",";
			}
			else if($is_lc_sc_sql_row[csf("is_lc_sc")] == 1)
			{
				$internal_file_sql = sql_select("SELECT internal_file_no FROM com_sales_contract where id='".$is_lc_sc_sql_row[csf("lc_sc_id")]."'");
				list($internal_file_sql_row)=$internal_file_sql;
				$internal_file_no=$internal_file_no.$internal_file_sql_row[csf("internal_file_no")].",";
			}
			$lc_number=$lc_number.$row[csf("lc_number")].",";
			$lc_id=$lc_id.$row[csf("id")].",";
			$lc_value+=$row[csf("lc_value")];
			echo "document.getElementById('txt_lc_number').value 			= '".rtrim($lc_number,",")."';\n";
			echo "document.getElementById('btb_lc_id').value 			= '".rtrim($lc_id,",")."';\n";
			echo "document.getElementById('cbo_issuing_bank').value 			= '".$row[csf("issuing_bank_id")]."';\n";
			echo "document.getElementById('txt_lc_value').value 				= '".$lc_value."';\n";
			echo "document.getElementById('cbo_lc_currency_id').value 			= '".$row[csf("currency_id")]."';\n";
			//echo "document.getElementById('hid_tolarance').value 			= '".$row[csf("tolerance")]."';\n";
			echo "document.getElementById('cbo_importer_id').value 				= '".$row[csf("importer_id")]."';\n";
			//echo "document.getElementById('hid_tenor').value 			= '".$row[csf("tenor")]."';\n";
			//echo "document.getElementById('hid_maturity_from').value 			= '".$row[csf("maturity_from_id")]."';\n";


			echo "load_drop_down( 'requires/import_document_acceptance_non_lc_controller', '".$row[csf('importer_id')]."'+'_'+'".$row[csf('item_category_id')]."', 'load_supplier_dropdown', 'supplier_td');\n";
			echo "document.getElementById('cbo_supplier_id').value 			= '".$row[csf("supplier_id")]."';\n";
			echo "document.getElementById('cbo_supplier_id').disabled 			= true;\n";
			echo "document.getElementById('cbo_payterm_id').value 		= '".$row[csf("payterm_id")]."';\n";
			echo "document.getElementById('cbo_lc_type_id').value 			= '".$row[csf("lc_type_id")]."';\n";
			echo "$('#internal_file_no').val( '".rtrim($internal_file_no,",")."' );\n";
			echo "$('#port_of_discharge').val( '$row[port_of_discharge]' );\n";   
			echo "$('#port_of_loading').val( '$row[port_of_loading]' );\n";   
			echo "$('#inco_term_place').val( '$row[inco_term_place]' );\n";   
			echo "$('#cbo_inco_term').val( '$row[inco_term_id]' );\n";   
			echo "$('#cbo_shipment_mode').val( '$row[delivery_mode_id]' );\n";
			//exit();
		}
		if($data[1]==2)
		{
			$internal_file_no="";
			$is_lc_sc_sql = sql_select("SELECT lc_sc_id, is_lc_sc FROM com_btb_export_lc_attachment where import_mst_id='".$row[csf("id")]."'");
			list($is_lc_sc_sql_row)=$is_lc_sc_sql;
			if($is_lc_sc_sql_row[csf("is_lc_sc")] == 0)
			{
				$internal_file_sql = sql_select("SELECT internal_file_no FROM com_export_lc where id='".$is_lc_sc_sql_row[csf("lc_sc_id")]."'");
				list($internal_file_sql_row)=$internal_file_sql;
				$internal_file_no=$internal_file_no.$internal_file_sql_row[csf("internal_file_no")].",";
			}
			else if($is_lc_sc_sql_row[csf("is_lc_sc")] == 1)
			{
				$internal_file_sql = sql_select("SELECT internal_file_no FROM com_sales_contract where id='".$is_lc_sc_sql_row[csf("lc_sc_id")]."'");
				list($internal_file_sql_row)=$internal_file_sql;
				$internal_file_no=$internal_file_no.$internal_file_sql_row[csf("internal_file_no")].",";
			}
			$lc_number=$lc_number.$row[csf("lc_number")].",";
			$lc_id=$lc_id.$row[csf("id")].",";
			$lc_value+=$row[csf("lc_value")];
			echo "document.getElementById('txt_lc_number').value 			= '".rtrim($lc_number,",")."';\n";
			echo "document.getElementById('btb_lc_id').value 			= '".rtrim($lc_id,",")."';\n";
			echo "document.getElementById('cbo_issuing_bank').value 			= '".$row[csf("issuing_bank_id")]."';\n";
			echo "document.getElementById('txt_lc_value').value 				= '".$lc_value."';\n";
			echo "document.getElementById('cbo_lc_currency_id').value 			= '".$row[csf("currency_id")]."';\n";
			//echo "document.getElementById('hid_tolarance').value 			= '".$row[csf("tolerance")]."';\n";
			//echo "document.getElementById('hid_tenor').value 			= '".$row[csf("tenor")]."';\n";
			//echo "document.getElementById('hid_maturity_from').value 			= '".$row[csf("maturity_from_id")]."';\n";
			echo "document.getElementById('cbo_importer_id').value 				= '".$row[csf("importer_id")]."';\n";
			echo "load_drop_down( 'requires/import_document_acceptance_non_lc_controller', '".$row[csf('importer_id')]."'+'_'+'".$row[csf('item_category_id')]."', 'load_supplier_dropdown', 'supplier_td');\n";
			echo "document.getElementById('cbo_supplier_id').value 			= '".$row[csf("supplier_id")]."';\n";
			echo "document.getElementById('cbo_supplier_id').disabled 			= true;\n";
			echo "document.getElementById('cbo_payterm_id').value 		= '".$row[csf("payterm_id")]."';\n";
			echo "document.getElementById('cbo_lc_type_id').value 			= '".$row[csf("lc_type_id")]."';\n";
			echo "$('#internal_file_no').val( '".rtrim($internal_file_no,",")."' );\n";
			//echo "$('#port_of_discharge').val( '$row[port_of_discharge]' );\n";   
			//echo "$('#port_of_loading').val( '$row[port_of_loading]' );\n";   
			//echo "$('#inco_term_place').val( '$row[inco_term_place]' );\n";   
			//echo "$('#cbo_inco_term').val( '$row[inco_term_id]' );\n";   
			//echo "$('#cbo_shipment_mode').val( '$row[delivery_mode_id]' );\n";
			//exit();
		}
	}
}


if($action=='populate_data_from_invoice')
{
	$data_array=sql_select("select id,btb_lc_id,invoice_no,invoice_date,document_value,shipment_date,company_acc_date,bank_acc_date,bank_ref,acceptance_time,retire_source,remarks,bill_no,bill_date,shipment_mode,document_status,copy_doc_receive_date,original_doc_receive_date,doc_to_cnf,feeder_vessel,mother_vessel,eta_date,ic_receive_date,shipping_bill_no,inco_term,inco_term_place,port_of_loading,port_of_discharge,bill_of_entry_no,psi_reference_no,maturity_date,container_no,pkg_quantity from  com_import_invoice_mst where id='$data'"); 
	foreach ($data_array as $row)
	{ 
			echo "document.getElementById('btb_lc_id').value 			= '".$row[csf("btb_lc_id")]."';\n";
			echo "document.getElementById('invoice_id').value 	= '".$row[csf("id")]."';\n";
			echo "document.getElementById('txt_invoice_number').value 	= '".$row[csf("invoice_no")]."';\n";
			echo "document.getElementById('txt_invoice_date').value 	= '".change_date_format($row[csf("invoice_date")])."';\n";
			echo "document.getElementById('txt_document_value').value 	= '".$row[csf("document_value")]."';\n";
			echo "document.getElementById('txt_shipment_date').value 	= '".change_date_format($row[csf("shipment_date")])."';\n";
			echo "document.getElementById('txt_company_acc_date').value 	= '".change_date_format($row[csf("company_acc_date")])."';\n";
			echo "document.getElementById('txt_bank_acceptance_date').value 	= '".change_date_format($row[csf("bank_acc_date")])."';\n";
			echo "document.getElementById('txt_bank_ref').value 	= '".$row[csf("bank_ref")]."';\n";
			echo "document.getElementById('cbo_acceptance_time').value 	= '".$row[csf("acceptance_time")]."';\n";
			echo "document.getElementById('cbo_retire_source').value 	= '".$row[csf("retire_source")]."';\n";
			echo "document.getElementById('txt_remarks').value 	= '".$row[csf("remarks")]."';\n";
			
            echo "document.getElementById('bill_no').value 	= '".$row[csf("bill_no")]."';\n";
			echo "document.getElementById('bill_date').value 	= '".change_date_format($row[csf("bill_date")])."';\n";
			echo "document.getElementById('cbo_shipment_mode').value 	= '".$row[csf("shipment_mode")]."';\n";
			
			echo "document.getElementById('cbo_document_status').value 	= '".$row[csf("document_status")]."';\n";
			echo "document.getElementById('copy_doc_receive_date').value 	= '".change_date_format($row[csf("copy_doc_receive_date")])."';\n";
			echo "document.getElementById('original_doc_receive_date').value 	= '".change_date_format($row[csf("original_doc_receive_date")])."';\n";
			
			echo "document.getElementById('doc_to_cnf').value 	= '".change_date_format($row[csf("doc_to_cnf")])."';\n";
			echo "document.getElementById('feeder_vessel').value 	= '".$row[csf("feeder_vessel")]."';\n";
			echo "document.getElementById('mother_vessel').value 	= '".$row[csf("mother_vessel")]."';\n";
			
			echo "document.getElementById('eta_date').value 	= '".change_date_format($row[csf("eta_date")])."';\n";
			echo "document.getElementById('ic_receive_date').value 	= '".change_date_format($row[csf("ic_receive_date")])."';\n";
			echo "document.getElementById('shipping_bill_no').value 	= '".$row[csf("shipping_bill_no")]."';\n";
			
			
			echo "document.getElementById('cbo_inco_term').value 	= '".$row[csf("inco_term")]."';\n";
			echo "document.getElementById('inco_term_place').value 	= '".$row[csf("inco_term_place")]."';\n";
			echo "document.getElementById('port_of_loading').value 	= '".$row[csf("port_of_loading")]."';\n";
			
			echo "document.getElementById('port_of_discharge').value 	= '".$row[csf("port_of_discharge")]."';\n";
			echo "document.getElementById('bill_of_entry_no').value 	= '".$row[csf("bill_of_entry_no")]."';\n";
			
			
			echo "document.getElementById('psi_reference_no').value 	= '".$row[csf("psi_reference_no")]."';\n";
			echo "document.getElementById('maturity_date').value 	= '".change_date_format($row[csf("maturity_date")])."';\n";
			
			
			echo "document.getElementById('container_no').value 	= '".$row[csf("container_no")]."';\n";
			echo "document.getElementById('pkg_quantity').value 	= '".$row[csf("pkg_quantity")]."';\n";
			
			exit();
	}
}


if( $action == "pi_listview" ) 
{
    $data = explode('_',$data);
	
	if($db_type==0)
	{
		if($data[1] == 1)
		{
			 $sql="select DISTINCT m.*,sum(current_acceptance_value) as cumulative_accept_amount   from   ( select  t2.*, SUM( t3.amount ) AS pi_value, t1.pi_id,t1.com_btb_lc_master_details_id   from  com_btb_lc_pi AS t1 , com_pi_master_details AS t2, com_pi_item_details AS t3 WHERE  t2.id = t1.pi_id and  t3.pi_id = t1.pi_id and t1.com_btb_lc_master_details_id in( $data[0])  AND t1.is_deleted = 0 AND t1.status_active = 1 and t3.is_deleted = 0 AND t3.status_active = 1 GROUP BY t3.pi_id  ) m  LEFT JOIN com_import_invoice_dtls i ON   m.pi_id=i.pi_id GROUP BY id";
		}
		if($data[1] == 2)
		{ 
		//echo"monzu";
			 $sql="select DISTINCT m.*, current_acceptance_value,sum(current_acceptance_value) as cumulative_accept_amount   from   ( select  t2.*, SUM( t3.amount ) AS pi_value, t1.pi_id ,t1.com_btb_lc_master_details_id  from  com_btb_lc_pi AS t1 , com_pi_master_details AS t2, com_pi_item_details AS t3 WHERE  t2.id = t1.pi_id and  t3.pi_id = t1.pi_id and t1.com_btb_lc_master_details_id in( $data[0] ) AND t1.is_deleted = 0 AND t1.status_active = 1 and t3.is_deleted = 0 AND t3.status_active = 1 GROUP BY t3.pi_id  ) m  LEFT JOIN com_import_invoice_dtls i ON   m.pi_id=i.pi_id  GROUP BY id";
		}
	}
	else
	{
		$sql="select b.id as pi_id, b.pi_number, b.item_category_id, a.com_btb_lc_master_details_id, SUM(c.net_pi_amount) AS pi_value, sum(i.current_acceptance_value) as cumulative_accept_amount from com_btb_lc_pi a, com_pi_master_details b, com_pi_item_details c LEFT JOIN com_import_invoice_dtls i ON c.pi_id=i.pi_id where b.id=a.pi_id and b.id = c.pi_id and a.com_btb_lc_master_details_id  in( $data[0]) AND a.is_deleted = 0 AND a.status_active = 1 and c.is_deleted = 0 AND c.status_active = 1 GROUP BY b.id, b.pi_number, b.item_category_id, a.com_btb_lc_master_details_id";	
	}
?>
	
	<fieldset style="width:1024px;">
		<legend>BTB / Import LC PI List</legend>
        <div style="width:1000px;" align="left">  
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="rpt_table">
			<thead>
                <th width="140">LC Number</th>
				<th width="140" class="must_entry_caption">PI Number</th>
				<th width="140">Item Category</th>
				<th width="100">PI Value</th>
				<th width="100">Current Acceptance Value  </th>
				<th width="100">MRR Value</th>
				<th width="120">Cumulative Accepted Amount</th>
                <th width="">Balance</th>
			</thead>
		</table>
        </div>
		<div style="width:1000px;">
			<table width="100%" border="0" id="tbl_list_search" cellpadding="0" cellspacing="0" class="rpt_table">
			<?php
            $tot_current_acceptance_value = 0;
			$tot_cumu_acceptance_value = 0;
			//$result = mysql_query( $sql ) or die( $sql . "<br />" . mysql_error() );
			$data_array=sql_select($sql);
			$i = 0;            
			foreach ($data_array as $row)
	        {
				//print_r($row);
                //extract($row);
				$i++;
               $cumulative_receive_value = return_field_value("sum(order_amount)","inv_transaction","pi_wo_batch_no=".$row[csf('pi_id')]." and receive_basis=1 and transaction_type=1 group by pi_wo_batch_no ");
				
				if($data[1] == 2)
				{
					$current_acceptance_value=return_field_value("current_acceptance_value","com_import_invoice_dtls","import_invoice_id=$data[2] and btb_lc_id='".$row[csf('com_btb_lc_master_details_id')]."' and pi_id =".$row[csf('pi_id')]." and status_active =1 and is_deleted=0");
					$tot_current_acceptance_value = $tot_current_acceptance_value+ $current_acceptance_value;
					$invoice_dtls_id=return_field_value("id","com_import_invoice_dtls","import_invoice_id=$data[2] and btb_lc_id='".$row[csf('com_btb_lc_master_details_id')]."' and 	pi_id =".$row[csf('pi_id')]." and status_active =1 and is_deleted=0");
				}
				$lc_number = return_field_value("lc_number","com_btb_lc_master_details","id=".$row[csf('com_btb_lc_master_details_id')]);
				$tolerance = return_field_value("tolerance","com_btb_lc_master_details","id=".$row[csf('com_btb_lc_master_details_id')]);
				$tot_cumu_acceptance_value = $tot_cumu_acceptance_value+$row[csf("cumulative_accept_amount")];
				$balance=$row[csf("pi_value")] - $row[csf("cumulative_accept_amount")];
				$tot_cumu_avalance_value+=$balance;
			?>
				<tr>
                    <td width="140">
						<input type="text" name="lc_number[]" id="lc_number_<?php echo $i; ?>" class="text_boxes" value="<?php echo $lc_number; ?>" style="width:125px;" readonly />
						<input type="hidden" name="lc_id[]" id="lc_id_<?php echo $i; ?>" value="<?php echo $row[csf('com_btb_lc_master_details_id')]; ?>" />
                        <input type="hidden" name="tolerance[]" id="tolerance_<?php echo $i; ?>" value="<?php echo $tolerance; ?>" />
                        
					</td>
					<td width="140">
						<input type="text" name="pi_number[]" id="pi_number_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('pi_number')]; ?>" style="width:125px;" readonly />
						<input type="hidden" name="pi_id[]" id="pi_id_<?php echo $i; ?>" value="<?php echo $row[csf('pi_id')]; ?>" />
                        
					</td>
					<td width="140">
						<input type="text" name="item_category[]" id="item_category_<?php echo $i; ?>" class="text_boxes" value="<?php echo $item_category[$row[csf('item_category_id')]]; ?>" style="width:125px;" readonly />
						<input type="hidden" name="item_category_id[]" id="item_category_id_<?php echo $i; ?>" value="<?php echo $row[csf('item_category_id')]; ?>" />
					</td>
					<td width="100">
                    	<input type="text" name="pi_value[]" id="pi_value_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('pi_value')]; ?>" style="width:85px;" readonly>
                    </td>
					<td width="100">
                    	<input type="text" name="current_acceptance_value[]" id="current_acceptance_value_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $current_acceptance_value;?>" onKeyUp="calculate(<?php echo $i; ?>)"   style="width:85px;"  />
                        <input type="hidden" id="hide_current_acceptance_value_<?php echo $i; ?>" value="<?php echo $current_acceptance_value;?>" />
                    </td>
					<td width="100">
                    	<input type="text" name="cumulative_receive_value[]" id="cumulative_receive_value_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo  $cumulative_receive_value;?>" style="width:85px;" readonly />
                    </td>
					<td width="120">
                    	<input type="text" name="cumulative_accept_amount[]" id="cumulative_accept_amount_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $row[csf('cumulative_accept_amount')];?>" style="width:105px;" readonly onClick="show_me_cumu_stat(<?php echo $row[pi_id]; ?>)" />
                        <input type="hidden" id="hide_cumulative_accept_amount_<?php echo $i; ?>" value="<?php  echo $row[csf('cumulative_accept_amount')];?>" />
                    </td>
                    <td width="">
                    	<input type="text" style="width:130px" name="cumulative_accept_balance[]" id="cumulative_accept_balance_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $balance;?>"  readonly />
                       <input type="hidden" id="invoice_dtls_id_<?php echo $i; ?>" value="<?php  echo $invoice_dtls_id;?>" />

                    </td>
				</tr>
			<?php } ?>
                <tfoot>
                    <th colspan='4' style="text-align:right"><b>Total:</b></th>
                    <th style="text-align:right"><input type="text" id="tot_current_acceptance_value" class="text_boxes_numeric" style="width:85px;"  disabled value="<?php echo $tot_current_acceptance_value; ?>" /></th>
                    <th ></th>
                    <th ><input type="text" id="tot_cumula_acceptance_value" class="text_boxes_numeric" style="width:105px;"  disabled value="<?php echo $tot_cumu_acceptance_value; ?>" /></th>
                    <th ><input type="text" id="tot_cumula_balance_value" style="width:130px" class="text_boxes_numeric"   disabled value="<?php echo $tot_cumu_avalance_value; ?>" /></th>
               </tfoot>
			</table>
		</div>
	</fieldset>
	<?php
	exit();
}


if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$id=return_next_id( "id", "com_import_invoice_mst", 1 ) ;
		
		$field_array="id,btb_lc_id,is_lc,invoice_no,invoice_date,document_value,shipment_date,company_acc_date,bank_acc_date,bank_ref,acceptance_time,retire_source,remarks,bill_no,bill_date,shipment_mode,document_status,copy_doc_receive_date,original_doc_receive_date,doc_to_cnf,feeder_vessel,mother_vessel,eta_date,ic_receive_date,shipping_bill_no,inco_term,inco_term_place,port_of_loading,port_of_discharge,bill_of_entry_no,psi_reference_no,maturity_date,container_no,pkg_quantity,inserted_by,insert_date";
		$data_array ="(".$id.",".$btb_lc_id.",2,".$txt_invoice_number.",".$txt_invoice_date.",".$txt_document_value.",".$txt_shipment_date.",".$txt_company_acc_date.",".$txt_bank_acceptance_date.",".$txt_bank_ref.",".$cbo_acceptance_time.",".$cbo_retire_source.",".$txt_remarks.",".$bill_no.",".$bill_date.",".$cbo_shipment_mode.",".$cbo_document_status.",".$copy_doc_receive_date.",".$original_doc_receive_date.",".$doc_to_cnf.",".$feeder_vessel.",".$mother_vessel.",".$eta_date.",".$ic_receive_date.",".$shipping_bill_no.",".$cbo_inco_term.",".$inco_term_place.",".$port_of_loading.",".$port_of_discharge.",".$bill_of_entry_no.",".$psi_reference_no.",".$maturity_date.",".$container_no.",".$pkg_quantity.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
		if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}	
			
		$id_dtls=return_next_id( "id", "com_import_invoice_dtls", 1 ) ;
		$field_array1="id,import_invoice_id,btb_lc_id,is_lc,pi_id,current_acceptance_value,inserted_by,insert_date";
		for ($i=1;$i<=$total_row;$i++)
		{
			$lc_id="lc_id_".$i;
			$pi_id="pi_id_".$i;
			$current_acceptance_value="current_acceptance_value_".$i;
			$invoice_dtls_id="invoice_dtls_id_".$i;
			if ($i!=1) $data_array1 .=",";
			$data_array1 .="(".$id_dtls.",".$id.",".$$lc_id.",2,".$$pi_id.",".$$current_acceptance_value.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_dtls=$id_dtls+1;
		}
		
		$rID=sql_insert("com_import_invoice_mst",$field_array,$data_array,0);
		$rID2=sql_insert("com_import_invoice_dtls",$field_array1,$data_array1,1);
		
		check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'","",$btb_lc_id)."**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$btb_lc_id)."**".$id;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "0**".str_replace("'","",$btb_lc_id)."**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$btb_lc_id)."**".$id;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array_up="btb_lc_id*is_lc*invoice_no*invoice_date*document_value*shipment_date*company_acc_date*bank_acc_date*bank_ref*acceptance_time*retire_source*remarks*bill_no*bill_date*shipment_mode*document_status*copy_doc_receive_date*original_doc_receive_date*doc_to_cnf*feeder_vessel*mother_vessel*eta_date*ic_receive_date*shipping_bill_no*inco_term*inco_term_place*port_of_loading*port_of_discharge*bill_of_entry_no*psi_reference_no*maturity_date*container_no*pkg_quantity*updated_by*update_date";
		$data_array_up ="".$btb_lc_id."*2*".$txt_invoice_number."*".$txt_invoice_date."*".$txt_document_value."*".$txt_shipment_date."*".$txt_company_acc_date."*".$txt_bank_acceptance_date."*".$txt_bank_ref."*".$cbo_acceptance_time."*".$cbo_retire_source."*".$txt_remarks."*".$bill_no."*".$bill_date."*".$cbo_shipment_mode."*".$cbo_document_status."*".$copy_doc_receive_date."*".$original_doc_receive_date."*".$doc_to_cnf."*".$feeder_vessel."*".$mother_vessel."*".$eta_date."*".$ic_receive_date."*".$shipping_bill_no."*".$cbo_inco_term."*".$inco_term_place."*".$port_of_loading."*".$port_of_discharge."*".$bill_of_entry_no."*".$psi_reference_no."*".$maturity_date."*".$container_no."*".$pkg_quantity."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**1"; die;}	
		
		$field_array_up1="import_invoice_id*btb_lc_id*is_lc*pi_id*current_acceptance_value*updated_by*update_date";
		$field_array1="id,import_invoice_id,btb_lc_id,is_lc,pi_id,current_acceptance_value,inserted_by,insert_date";
		$add_comma=1;
		$id_dtls=return_next_id( "id", "com_import_invoice_dtls", 1 ) ;
		
		for ($i=1;$i<=$total_row;$i++)
		{
			$lc_id="lc_id_".$i;
			$pi_id="pi_id_".$i;
			$current_acceptance_value="current_acceptance_value_".$i;
			$invoice_dtls_id="invoice_dtls_id_".$i;
			if(str_replace("'",'',$$invoice_dtls_id)!="")
			{
				$id_arr[]=str_replace("'",'',$$invoice_dtls_id);
				$data_array_up1[str_replace("'",'',$$invoice_dtls_id)] =explode("*",("".$invoice_id."*".$$lc_id."*2*".$$pi_id."*".$$current_acceptance_value."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));
			}
			
			if(str_replace("'",'',$$invoice_dtls_id)=="")
			{
				if ($add_comma!=1) $data_array1 .=",";
				$data_array1 .="(".$id_dtls.",".$invoice_id.",".$$lc_id.",2,".$$pi_id.",".$$current_acceptance_value.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id_dtls=$id_dtls+1;
				$add_comma++;
			}
		}
		
		//echo bulk_update_sql_statement( "com_import_invoice_dtls", "id", $field_array_up1, $data_array_up1, $id_arr );
		$rID2=execute_query(bulk_update_sql_statement( "com_import_invoice_dtls", "id", $field_array_up1, $data_array_up1, $id_arr ));
		$rID3=true;
		if($data_array1!="")
		{
			$rID3=sql_insert("com_import_invoice_dtls",$field_array1,$data_array1,0);
		}
		$rID=sql_update("com_import_invoice_mst",$field_array_up,$data_array_up,"id","".$invoice_id."",1);
		
		check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID  && $rID2  && $rID3)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID  && $rID2  && $rID3)
			{
				oci_commit($con); 
				echo "1**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
			else
			{
				oci_rollback($con);  
				echo "10**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		if($db_type==0)
		{
		mysql_query("BEGIN");
		}
		if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**2"; die;}
		$field_array="status_active*is_deleted";
		$data_array="'0'*'1'";
		$rID=sql_delete("com_import_invoice_mst",$field_array,$data_array,"id","".$invoice_id."",0);
		$rID2=sql_delete("com_import_invoice_dtls",$field_array,$data_array,"import_invoice_id","".$invoice_id."",1);
		check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID2)
			{
				oci_commit($con);  
				echo "2**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
			else
			{
				oci_rollback($con); 
				echo "10**".str_replace("'","",$btb_lc_id)."**".str_replace("'","",$invoice_id);
			}
		}
		disconnect($con);
		die;
	}
}

if($action=="cumulative_details_popup")
{
	echo load_html_head_contents("Cumulative Datails List", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	
</head>

<body>
<div align="center" style="width:500px;">
	
		<fieldset style="width:100%;">
            <legend>Cumulative Details</legend>           
            	<table cellpadding="0" cellspacing="0" width="500" class="rpt_table">
                	<thead>
                        <th>PI Number</th>
                    	<th>Invoice Number</th>
                        <th>Invoice Date</th>
                        <th>Invoice Value</th>
                    </thead>
                    <tbody>
                    <?php
					$total=0;
					$sql="select a.invoice_no,a.invoice_date,b.pi_id,b.current_acceptance_value from com_import_invoice_mst a,com_import_invoice_dtls b where a.id=b.import_invoice_id  and pi_id=$pi_id ";
					$data_array=sql_select($sql);
					foreach($data_array as $row)
					{
						$pi_number=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("pi_id")]);
					?>
                    <tr>
                    <td><?php echo $pi_number; ?></td>
                    <td><?php echo $row[csf("invoice_no")]; ?></td>
                    <td><?php echo $row[csf("invoice_date")]; ?></td>
                    <td align="right"><?php echo $row[csf("current_acceptance_value")];$total+= $row[csf("current_acceptance_value")]; ?></td>
                    </tr>
                    <?php
					}
					?>
                    </tbody>
                    <tfoot>
                    <th></th>
                    <th></th>
                    <th>Total:</th>
                    <th><?php echo $total;?></th>
                    </tfoot>
                    </table>
                    
            </fieldset>
		
	</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();	
}

?>


 