<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$costing_per_id_library=return_library_array( "select job_no, costing_per from wo_pre_cost_mst", "job_no", "costing_per");
$consumtion_library=return_library_array( "select job_no, avg_finish_cons from wo_pre_cost_fabric_cost_dtls", "job_no", "avg_finish_cons");
$order_arr=return_library_array("select id, po_number from wo_po_break_down","id","po_number");
$report_arr=array(1=>'Knit Finish',2=>'Woven Finish');

if($action=="load_drop_down_buyer")
{
	$data=explode("_",$data);
	if($data[1]==1) $party="1,3,21,90"; else $party="80";
	echo create_drop_down( "cbo_buyer_id", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[0]' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in ($party)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $selected, "","" );
	exit();
}

if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#hide_job_id').val( id );
		$('#hide_job_no').val( ddd );
	}		
	
/*		function js_set_value(str)
		{
			var splitData = str.split("_");
			//alert (splitData[1]);
			$("#hide_job_id").val(splitData[0]); 
			$("#hide_job_no").val(splitData[1]); 
			parent.emailwindow.hide();
		}
*/    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Job No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:100px;" onClick="reset_form('styleRef_form','search_div','','','','');"></th> 					<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                    <input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+'<?php echo $cbo_year_id; ?>', 'create_job_no_search_list_view', 'search_div', 'order_wise_color_finish_fabric_stock_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_job_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	$year_id=$data[4];
	//$month_id=$data[5];
	//echo $company_id;die;
	
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";
	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	//$year="year(insert_date)";
	if($db_type==0) $year_field_by="year(insert_date) as year "; 
	else if($db_type==2) $year_field_by="to_char(insert_date,'YYYY') as year ";
	if($db_type==0) $month_field_by="and month(insert_date)"; 
	else if($db_type==2) $month_field_by="and to_char(insert_date,'MM')";
	if($db_type==0) $year_field="and year(insert_date)=$year_id"; 
	else if($db_type==2) $year_field="and to_char(insert_date,'YYYY')";
	if($year_id!=0) $year_cond="$year_field='$year_id'"; else $year_cond="";
	//if($month_id!=0) $month_cond="$month_field_by=$month_id"; else $month_cond="";
	$arr=array (0=>$company_arr,1=>$buyer_arr);
		
	$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no, $year_field_by from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $year_cond  order by job_no";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","620","270",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0,0','',1) ;
   exit(); 
} 
if ($action=="booking_no_popup")
	{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	$company_id=$data[0];
	$buyer_id=$data[1];
	$year_id=$data[2];
	
?>	
	<script>
	function js_set_value(booking_no)
	{
		document.getElementById('selected_booking').value=booking_no;
		//alert(booking_no);
		parent.emailwindow.hide();
	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="750" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
    	<tr>
        	<td align="center" width="100%">
            	<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                    <thead>                	 
                       <th width="150">Buyer Name</th><th width="200">Date Range</th><th></th>           
                    </thead>
        			<tr>
                    <input type="hidden" id="selected_booking">
                   	<td>
                     <?php 
					echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$company_id $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_id,"",0 );
							?>
                    </td>
                    <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">
					  <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view (document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+'<?php echo $company_id; ?>','create_booking_search_list_view', 'search_div', 'order_wise_color_finish_fabric_stock_controller','setFilterGrid(\'list_view\',-1)')" style="width:100px;" /></td>
        		</tr>
             </table>
          </td>
        </tr>
        <tr>
            <td  align="center" height="40" valign="middle">
            <?php 
			echo create_drop_down( "cbo_year_selection", 70, $year,"", 1, "-- Select --", date('Y'), "",0 );		
			?>
			<?php echo load_month_buttons();  ?>
            </td>
            </tr>
        <tr>
            <td align="center"valign="top" id="search_div"> 
            </td>
        </tr>
    </table>    
    </form>
   </div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
	}
	if ($action=="create_booking_search_list_view")
	{
		$data=explode('_',$data);
		$company=$data[3];
		//if ($data[3]!=0) $company="  company_id='$data[3]'"; else { echo "Please Select Company First."; die; }
		if ($data[0]!=0) $buyer=" and buyer_id='$data[0]'"; else { echo "Please Select Buyer First."; die; }
		//if ($data[4]!=0) $job_no=" and job_no='$data[4]'"; else $job_no='';
		if($db_type==0)
		{
		if ($data[1]!="" &&  $data[2]!="") $booking_date  = "and booking_date  between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'"; else $booking_date ="";
		}
		if($db_type==2)
		{
		if ($data[1]!="" &&  $data[2]!="") $booking_date  = "and booking_date  between '".change_date_format($data[1],'','',1)."' and '".change_date_format($data[2],'','',1)."'"; else $booking_date ="";
		}
		$po_array=array();
		$sql_po= sql_select("select booking_no,po_break_down_id from wo_booking_mst  where company_id='$company' $buyer $booking_date and booking_type=1 and is_short=2 and   status_active=1  and is_deleted=0 order by booking_no");
		foreach($sql_po as $row)
		{
			 $po_id=explode(",",$row[csf("po_break_down_id")]);
			//print_r( $po_id);
			$po_number_string="";
			foreach($po_id as $key=> $value )
			{
				$po_number_string.=$order_arr[$value].",";
			}
			$po_array[$row[csf("po_break_down_id")]]=rtrim($po_number_string,",");
		} //echo $po_array[$row[csf("po_break_down_id")]];
		 $approved=array(0=>"No",1=>"Yes");
		 $is_ready=array(0=>"No",1=>"Yes",2=>"No");
		$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
		$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
		$suplier=return_library_array( "select id, short_name from lib_supplier",'id','short_name');
		$po_num=return_library_array( "select job_no, job_no_prefix_num from wo_po_details_master",'job_no','job_no_prefix_num');
		$arr=array (2=>$comp,3=>$buyer_arr,4=>$po_num,5=>$po_array,6=>$item_category,7=>$fabric_source,8=>$suplier,9=>$approved,10=>$is_ready);
		 $sql= "select booking_no_prefix_num, booking_no,booking_date,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved from wo_booking_mst  where company_id=$company $buyer $booking_date and booking_type=1 and is_short in(1,2) and  status_active=1  and 	is_deleted=0 order by booking_no"; 
		echo  create_list_view("list_view", "Booking No,Booking Date,Company,Buyer,Job No.,PO number,Fabric Nature,Fabric Source,Supplier,Approved,Is-Ready", "80,80,70,100,90,200,80,80,50,50","1020","320",0, $sql , "js_set_value", "booking_no_prefix_num", "", 1, "0,0,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", $arr , "booking_no_prefix_num,booking_date,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", '','','0,0,0,0,0,0,0,0,0,0,0','','');
		
exit(); 
}// Booking Search End
if ($action=="order_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
    <script>
	 
	function js_set_value(str)
	{
		var splitData = str.split("_");
		//alert (splitData[1]);
		$("#order_no_id").val(splitData[0]); 
		$("#order_no_val").val(splitData[1]); 
		parent.emailwindow.hide();
	}
		  
	</script>
     <input type="hidden" id="order_no_id" />
     <input type="hidden" id="order_no_val" />
 <?php
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and b.buyer_name=$data[1]";
	if ($data[2]=="") $order_no=""; else $order_no=" and a.po_number=$data[2]";
	$job_no=str_replace("'","",$txt_job_id);
	if($db_type==0)  $find_inset=" and FIND_IN_SET(b.job_no_prefix_num,'$data[2]')";	
	if($db_type==2 || $db_type==1)$find_inset=" and b.job_no_prefix_num in($data[2]) "; 
	if ($data[2]=="") $job_no_cond=""; else $job_no_cond="  $find_inset";
	if($db_type==0) $year_field_by="and YEAR(b.insert_date)"; 
	else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
	if(trim($data[3])!=0) $year_cond=" $year_field_by='$data[3]'"; else $year_cond="";
	$sql="select a.id, a.po_number, b.job_no_prefix_num, b.job_no, b.buyer_name, b.style_ref_no from wo_po_details_master b, wo_po_break_down a  where b.job_no=a.job_no_mst and b.company_name=$data[0] and b.is_deleted=0 $buyer_name $job_no_cond $year_cond ORDER BY b.job_no";
	//echo $sql;
	$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$arr=array(1=>$buyer);
	
	echo  create_list_view("list_view", "Job No,Buyer,Style Ref.,Order No", "110,110,150,180","610","350",0, $sql, "js_set_value", "id,po_number", "", 1, "0,buyer_name,0,0,0", $arr , "job_no_prefix_num,buyer_name,style_ref_no,po_number", "order_wise_color_finish_fabric_stock_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
	disconnect($con);
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_report_type=str_replace("'","",$cbo_report_type);
	$buyer_id=str_replace("'","",$cbo_buyer_id);
	$book_no=str_replace("'","",$txt_book_no);
	$book_id=str_replace("'","",$txt_book_id);
	$job_no=str_replace("'","",$txt_job_no);
	$order_no_id=str_replace("'","",$txt_order_id);
	$order_no=str_replace("'","",$txt_order_no);
	
	//echo $cbo_report_type;die;
	//if(str_replace("'","",$cbo_buyer_id)!="" && str_replace("'","",$cbo_buyer_id)!=0) $buyer_id_cond=" and a.buyer_name=$cbo_buyer_id";
	if($buyer_id==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$buyer_id";
	}
	//echo $buyer_id_cond;die;$order_no=str_replace("'","",$txt_order_no);
	
	if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in ($job_no) ";
	if ($book_no=="") $booking_no_cond=""; else $booking_no_cond=" and f.booking_no_prefix_num='$book_no'";
	
	if(str_replace("'","",$txt_order_id)!="")  $order_cond=" and b.id in ($order_no_id)";
	else if(str_replace("'","",$txt_order_no)!="") $order_cond=" and b.po_number in ('$order_no')"; 
	else $order_cond='';
	//echo $order_id_cond;die;
	$date_from=str_replace("'","",$txt_date_from);
	if( $date_from=="") $ship_date=""; else $ship_date= " and b.pub_shipment_date <=".$txt_date_from."";
	//==================================================
	ob_start();
	if($cbo_report_type==1) // Knit Finish Start
	{
		$knit_fin_recv_array=array();
		$sql_recv=sql_select("select c.po_breakdown_id,c.color_id,b.body_part_id, sum(CASE WHEN c.entry_form in (7,37) and a.item_category=2  THEN c.quantity END) AS finish_receive_qnty from inv_receive_master a, pro_finish_fabric_rcv_dtls b,order_wise_pro_details c where a.id=b.mst_id and  b.id=c.dtls_id and c.entry_form in (7,37) and c.trans_id!=0 and a.item_category=2 and a.entry_form in(7,37) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by c.po_breakdown_id,c.color_id,b.body_part_id");
		foreach($sql_recv as $row)
		{
			 $knit_fin_recv_array[$row[csf('po_breakdown_id')]][$row[csf('body_part_id')]][$row[csf('color_id')]]['receive_qnty']=$row[csf('finish_receive_qnty')];
			 $knit_fin_recv_array[$row[csf('po_breakdown_id')]][$row[csf('body_part_id')]][$row[csf('color_id')]]['receive_return']=$row[csf('finish_receive_return_qnty')];
		} //print_r($fin_recv_array);die; 
		$issue_qnty=array();
		$sql_issue=sql_select("select b.po_breakdown_id,b.color_id, sum(b.quantity) as issue_qnty  from inv_finish_fabric_issue_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.trans_id!=0 and  a.status_active=1 and a.is_deleted=0 and b.entry_form=18 group by b.po_breakdown_id,b.color_id");
		foreach( $sql_issue as $row_iss )
		{
		$issue_qnty[$row_iss[csf('po_breakdown_id')]][$row_iss[csf('color_id')]]['issue_qnty']=$row_iss[csf('issue_qnty')];
		}
		$fin_recv_return_array=array();
		$sql_recv=sql_select("select c.po_breakdown_id,c.color_id,sum(CASE WHEN c.entry_form in (46) and a.item_category=2  THEN c.quantity END) AS finish_receive_return_qnty from  inv_issue_master a,order_wise_pro_details c,inv_transaction d where a.id=d.mst_id and d.id=c.trans_id and  c.entry_form in (46) and c.trans_id!=0 and a.item_category=2 and a.entry_form in(46) and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.po_breakdown_id,c.color_id");
		foreach($sql_recv as $row)
		{
			 $fin_recv_return_array[$row[csf('po_breakdown_id')]][$row[csf('color_id')]]['receive_return']=$row[csf('finish_receive_return_qnty')];
		} 
		$actual_cut_qnty=array();
		$sql_actual_cut_qty=sql_select(" select a.po_break_down_id,c.color_number_id, sum(b.production_qnty) as actual_cut_qty  from pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c where a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.production_type=1 and a.is_deleted=0 and a.status_active=1  group by  a.po_break_down_id,c.color_number_id");
		foreach( $sql_actual_cut_qty as $row_actual)
		{
			$actual_cut_qnty[$row_actual[csf('po_break_down_id')]][$row_actual[csf('color_number_id')]]['actual_cut_qty']=$row_actual[csf('actual_cut_qty')];
		} 
		$dia_array=array();
		$color_dia_array=sql_select( "select po_break_down_id,pre_cost_fabric_cost_dtls_id,color_number_id,dia_width from  wo_pre_cos_fab_co_avg_con_dtls");
		foreach($color_dia_array as $row)
		{
		$dia_array[$row[csf('pre_cost_fabric_cost_dtls_id')]][$row[csf('po_break_down_id')]][$row[csf('color_number_id')]]['dia']=$row[csf('dia_width')];	
		} //var_dump($dia_array);
		$booking_qnty=array();
		$sql_booking=sql_select(" select b.po_break_down_id,b.fabric_color_id, sum(b.fin_fab_qnty ) as fin_fab_qnty  from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2  and a.is_short=2 and a.booking_type=1 and a.is_deleted=0 and a.status_active=1 and b.booking_type=1 and b.status_active=1 and b.is_deleted=0 group by b.po_break_down_id,b.fabric_color_id");
		foreach( $sql_booking as $row_book)
		{
			$booking_qnty[$row_book[csf('po_break_down_id')]][$row_book[csf('fabric_color_id')]]['fin_fab_qnty']=$row_book[csf('fin_fab_qnty')];
		}
	?>
    <fieldset style="width:1890px;">
        <table cellpadding="0" cellspacing="0" width="1890">
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="21" style="font-size:18px"><strong><?php echo $report_title; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="21" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="21" style="font-size:14px"><strong> <?php if($date_from!="") echo "Upto : ".change_date_format(str_replace("'","",$txt_date_from)) ;?></strong></td>
            </tr>
        </table>
        <div align="left"><b>Main Fabric </b></div>
		<table width="1890" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
            <thead>
                <th width="30">SL</th>
                <th width="100">Buyer</th>
                <th width="90">Job</th>
                <th width="80">Order</th>
                <th width="100">Style</th> 
                <th width="80">Booking Type</th>
                <th width="80">Booking No</th>
                <th width="100">Body Part</th>
                <th width="80">Color Type</th>
                <th width="100">Fab.Construction</th>
                <th width="100">Fab.Composition</th>
                <th width="60">GSM</th>
                <th width="60">Fab.Dia</th>
                <th width="80">Order Qty(Pcs)</th>
                <th width="110">Color</th>
                <th width="80">Req. Qty</th>
                <th width="80">Total Recv.</th>
                <th width="80">Recv. Balance</th>
                <th width="80">Total Issued</th>
                <th width="80">Stock</th>
                <th width="80">Cons/Pcs</th>
                <th width="80">Possible Cut Pcs</th>
                <th width="">Actual Cut</th>
            </thead>
        </table>
        <div style="width:1910px; max-height:350px; overflow-y:scroll;" id="scroll_body">
			<table width="1890" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="table_body" > 
            <?php
			// Knit Finish Start
					$sql_main="Select a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, sum(distinct d.order_quantity/a.total_set_qnty) as order_quantity_set, b.id as po_id, b.po_number,d.color_number_id as color_id,c.body_part_id,c.id as pre_cost_fab_dtls_id,c.avg_finish_cons,c.construction,c.gsm_weight,c.composition,c.color_type_id,f.booking_no_prefix_num,g.booking_type  from  wo_po_details_master a, wo_po_break_down b,  wo_pre_cost_fabric_cost_dtls  c, wo_po_color_size_breakdown d,wo_booking_mst f,wo_booking_dtls g,wo_pre_cos_fab_co_avg_con_dtls h where a.job_no=b.job_no_mst and c.job_no=a.job_no and d.job_no_mst=c.job_no  and b.id=d.po_break_down_id  and g.po_break_down_id=b.id and  a.job_no=g.job_no and g.booking_no=f.booking_no and c.id=g.pre_cost_fabric_cost_dtls_id and c.id=h.pre_cost_fabric_cost_dtls_id and d.id=h.color_size_table_id and h.po_break_down_id=g.po_break_down_id and h.color_size_table_id=g.color_size_table_id and a.job_no=h.job_no and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and  c.status_active=1 and c.is_deleted=0 and g.booking_type=1 and f.item_category=2  and g.is_short in(2) and a.company_name=$cbo_company_id and c.body_part_id in(1,14,15,16,17,20)  $ship_date $buyer_id_cond $job_no_cond $order_cond $booking_no_cond  group by b.id,c.body_part_id,d.color_number_id,a.job_no_prefix_num,c.avg_finish_cons, a.job_no, a.buyer_name, a.style_ref_no, b.po_number,b.plan_cut,d.color_number_id,f.booking_no_prefix_num,g.booking_type,c.gsm_weight,c.id,c.body_part_id,c.construction,c.composition,c.color_type_id order by a.buyer_name,b.id,c.construction,c.composition,d.color_number_id,a.job_no, b.po_number";
			//echo $sql_main;die;
			$i=1; 
			$nameArray=sql_select( $sql_main );
			foreach ($nameArray as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$dzn_qnty=0;
				if($costing_per_id_library[$row[csf('job_no')]]==1)
				{
					$dzn_qnty=12;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==3)
				{
					$dzn_qnty=12*2;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==4)
				{
					$dzn_qnty=12*3;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==5)
				{
					$dzn_qnty=12*4;
				}
				else
				{
					$dzn_qnty=1;
				}
				//echo $dzn_qnty.'='.$row[csf("cons")];
				
				$order_id=$row[csf('po_id')];
				$color_id=$row[csf("color_id")];
				$body_part_id=$row[csf("body_part_id")];
				$cons_avg=$row[csf("avg_finish_cons")];
				//echo $row[csf("avg_finish_cons")].'='.$row[csf("color_number_avg")];
				$tot_cons_avg=$cons_avg/$dzn_qnty;
				//echo $row[csf("avg_finish_cons")];
				//$req_qty=($row[csf("avg_finish_cons")]/$dzn_qnty)*$row[csf("order_quantity_set")];
				$reg_booking_qty=$booking_qnty[$order_id][$color_id]['fin_fab_qnty'];
				 $knit_recv_qty=$knit_fin_recv_array[$row[csf('po_id')]][$row[csf("body_part_id")]][$row[csf("color_id")]]['receive_qnty'];
				//echo $knit_recv_qty3.'ddd';
				$knit_recv_return_qty=$fin_recv_return_array[$order_id][$color_id]['receive_return'];
				$tot_knit_recv_qty=$knit_recv_qty-$knit_recv_return_qty;
				$tot_recv_bal=$req_qty-$tot_knit_recv_qty;
				$tot_knit_issue_qty=$issue_qnty[$order_id][$color_id]['issue_qnty'];
				
				$tot_stock=$tot_knit_recv_qty-$tot_knit_issue_qty;
				$tot_actual_cut_qty=$actual_cut_qnty[$order_id][$color_id]['actual_cut_qty'];
				if($row[csf("booking_type")]==1) $book_type="Main"; else $book_type="Short"; 
				$dia=$dia_array[$row[csf('pre_cost_fab_dtls_id')]][$order_id][$color_id]['dia'];
				?>
                <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                    <td width="30"><?php echo $i; ?></td>
                    <td width="100"><p><?php echo $buyer_arr[$row[csf("buyer_name")]]; ?></p></td>
                    <td width="90"><p><?php echo $row[csf("job_no_prefix_num")]; ?></p></td>
                    <td width="80"><p><?php echo $order_arr[$order_id]; ?></p></td>
                    <td width="100"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
                    <td width="80" align="center"><p><?php echo $book_type; ?></p></td>
                    <td width="80"><p><?php echo $row[csf("booking_no_prefix_num")]; ?></p></td>
                    <td width="100"><p><?php echo $body_part[$row[csf("body_part_id")]]; ?></p></td>
                    <td width="80"><p><?php echo $color_type[$row[csf("color_type_id")]]; ?></p></td>
                    <td width="100"><p><?php echo $row[csf("construction")]; ?></p></td>
                    <td width="100"><p><?php  echo $row[csf("composition")]; ?></p></td>
                    <td width="60"><p><?php echo $row[csf("gsm_weight")]; ?></p></td>
                    <td width="60"><p><?php echo $dia; ?></p></td>
                      
                    <td width="80" align="right"><p><?php echo number_format($row[csf("order_quantity_set")],2,'.',''); ?>&nbsp;</p></td>
                    <td width="110"><p><?php echo $color_arr[$row[csf("color_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($reg_booking_qty,2); ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_knit_recv_qty,2);?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_recv_bal,2); ?>&nbsp;</p></td> 
                    <td width="80" align="right"><p><?php echo number_format($tot_knit_issue_qty,2); ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_stock,2); ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php  echo number_format($tot_cons_avg,4); ?></p></td>
                    <td width="80" align="right"><p><?php $possible_cut=$tot_knit_recv_qty/$tot_cons_avg; echo number_format($possible_cut,2); ?></p></td>
                    <td width="" align="right"><p><?php echo number_format($tot_actual_cut_qty,2);?></p></td>
                </tr>
            <?php	
            $i++;	
			$total_order_qnty+=$row[csf("order_quantity_set")];
			$total_recv_qty+=$tot_knit_recv_qty;
			$total_issue_qty+=$tot_knit_issue_qty;
			$total_balance_qty+=$tot_recv_bal;
			$total_stock_qty+=$tot_stock;
			$total_actual_cut_qty+=$tot_actual_cut_qty;
			$total_reg_booking_qty+=$reg_booking_qty;
			}
			?>
            </table>
            <table width="1890" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" > 
                <tfoot>
                    <th width="30"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="80"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="100"></th>
                    <th width="60"></th>
                    <th width="60"></th>
                    <th width="80" align="right" id=""><?php echo number_format($total_order_qnty,2,'.',''); ?></th>
                    <th width="110">&nbsp;</th>
                    <th width="80" align="right"><?php echo number_format($total_reg_booking_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_recv_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_balance_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_issue_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_stock_qty,2,'.',''); ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="80" align="right"><?php //echo number_format($total_possible_cut_pcs,2,'.',''); ?></th>
                    <th width="" align="right"><?php echo number_format($total_actual_cut_qty,2,'.',''); ?></th>
                </tfoot>
            </table> 
        </div> 
         </fieldset> 
         <br/>    
         <fieldset style="width:1670px;">     
        <div align="left"><b>Other Fabric </b>
        </div>
		<table width="1670" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
            <thead>
                <th width="30">SL</th>
                <th width="100">Buyer</th>
                <th width="90">Job</th>
                <th width="80">Order No</th>
                <th width="100">Style</th> 
                <th width="80">Booking Type</th>
                <th width="80">Booking No</th>
                <th width="100">Body Part</th>
				<th width="80">Color Type</th>
                <th width="100">Fab.Construction</th>
                <th width="100">Fab.Composition</th>
                <th width="60">GSM</th>
                <th width="60">Fab.Dia</th>
                <th width="80">Order Qty(Pcs)</th>
                <th width="110">Color</th>
                <th width="80">Req. Qty</th>
                <th width="80">Total Recv.</th>
                <th width="80">Recv. Balance</th>
                <th width="80">Total Issued</th>
                <th width="">Stock</th>
            </thead>
        </table>
        <div style="width:1690px; max-height:350px; overflow-y:scroll;" id="scroll_body2">
			<table width="1670" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="table_body2" > 
            <?php
			// Knit Finish Start
					$sql_other="Select a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, sum(distinct d.order_quantity/a.total_set_qnty) as order_quantity_set, b.id as po_id, b.po_number,d.color_number_id as color_id,c.body_part_id,c.id as pre_cost_fab_dtls_id,c.avg_finish_cons,c.construction,c.composition,c.gsm_weight,c.color_type_id,f.booking_no_prefix_num as booking_no,g.booking_type  from  wo_po_details_master a, wo_po_break_down b, wo_pre_cost_fabric_cost_dtls  c, wo_po_color_size_breakdown d,wo_booking_mst f,wo_booking_dtls g where a.job_no=b.job_no_mst and c.job_no=a.job_no and d.job_no_mst=c.job_no  and b.id=d.po_break_down_id  and g.po_break_down_id=b.id and  a.job_no=g.job_no and g.booking_no=f.booking_no and c.id=g.pre_cost_fabric_cost_dtls_id and g.color_size_table_id=d.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and  c.status_active=1 and c.is_deleted=0 and g.booking_type=1 and g.is_short in(1,2) and a.company_name=$cbo_company_id and c.body_part_id not  in(1,14,15,16,17,20)  $ship_date $buyer_id_cond $job_no_cond $order_cond $booking_no_cond  group by b.id,c.body_part_id,d.color_number_id,a.job_no_prefix_num,c.avg_finish_cons, a.job_no, a.buyer_name, a.style_ref_no, b.po_number,b.plan_cut,d.color_number_id,f.booking_no_prefix_num,g.booking_type ,c.id,c.body_part_id,c.gsm_weight,c.construction,c.composition,c.color_type_id order by a.buyer_name,b.id,c.construction,c.composition,d.color_number_id,a.job_no, b.po_number";	
			//echo $sql_other;die;
			$k=1; 
			$other_data=sql_select( $sql_other );
			foreach ($other_data as $rows)
			{
				if ($k%2==0) $bgcolor2="#E9F3FF"; else $bgcolor2="#FFFFFF";
				
				$dzn_qnty=0;
				if($costing_per_id_library[$rows[csf('job_no')]]==1)
				{
					$dzn_qnty=12;
				}
				else if($costing_per_id_library[$rows[csf('job_no')]]==3)
				{
					$dzn_qnty=12*2;
				}
				else if($costing_per_id_library[$rows[csf('job_no')]]==4)
				{
					$dzn_qnty=12*3;
				}
				else if($costing_per_id_library[$rows[csf('job_no')]]==5)
				{
					$dzn_qnty=12*4;
				}
				else
				{
					$dzn_qnty=1;
				}
				//echo $dzn_qnty.'='.$row[csf("cons")];
				$order_id_other=$rows[csf('po_id')];
				$color_id_other=$rows[csf("color_id")];
				$cons_avg_other=$rows[csf("avg_finish_cons")];
				$tot_cons_avg_other=$cons_avg_other/$dzn_qnty;
				//echo $rows[csf("avg_finish_cons")];
				$req_qty_other=($rows[csf("avg_finish_cons")]/$dzn_qnty)*$rows[csf("order_quantity_set")];
				$body_part_id_other=$rows[csf("body_part_id")];
				$req_qty_other=($rows[csf("avg_finish_cons")]/$dzn_qnty)*$rows[csf("order_quantity_set")];
				$knit_recv_qty_other=$fin_recv_array[$order_id_other][$body_part_id_other][$color_id_other]['receive_qnty'];
				
				$knit_recv_return_qty_other=$fin_recv_return_array[$order_id_other][$color_id_other]['receive_return'];
				$tot_knit_recv_qty_other=$knit_recv_qty_other-$knit_recv_return_qty_other;
				$tot_recv_bal_other=$req_qty_other-$tot_knit_recv_qty_other;
				//echo $issue_qnty[$order_id_other][$color_id_other]['issue_qnty'];
				$tot_knit_issue_qty_other=$issue_qnty[$order_id_other][$color_id_other]['issue_qnty'];
				$tot_stock_other=$tot_knit_recv_qty_other-$tot_knit_issue_qty_other;
				$tot_actual_cut_qty_other=$actual_cut_qnty[$order_id_other][$color_id_other]['actual_cut_qty'];
				if($row[csf("booking_type")]==1) $book_type_other="Main"; else $book_type_other="Short"; 
				$dia_other=$dia_array[$rows[csf('pre_cost_fab_dtls_id')]][$order_id_other][$color_id_other]['dia'];
				
				//c.construction,c.composition,c.color_type_id,g.booking_no,g.booking_type 
				?>
                <tr bgcolor="<?php echo $bgcolor2;?>" onClick="change_color('trother<?php echo $k;?>','<?php echo $bgcolor2;?>')" id="trother<?php echo $k;?>">
                    <td width="30"><?php echo $k; ?></td>
                    <td width="100"><p><?php echo $buyer_arr[$rows[csf("buyer_name")]]; ?></p></td>
                    <td width="90"><p><?php echo $rows[csf("job_no_prefix_num")]; ?></p></td>
                    <td width="80"><p><?php echo $rows[csf("po_number")]; ?></p></td>
                    <td width="100"><p><?php echo $rows[csf("style_ref_no")]; ?></p></td>
                    <td width="80"><p><?php echo $book_type_other; ?></p></td>
                    <td width="80"><p><?php echo $rows[csf("booking_no")]; ?></p></td>
                    <td width="100"><p><?php echo $body_part[$rows[csf("body_part_id")]]; ?></p></td>
                      <td width="80"><p><?php echo $color_type[$rows[csf("color_type_id")]]; ?></p></td>
                      <td width="100"><p><?php echo $rows[csf("construction")]; ?></p></td>
                       <td width="100"><p><?php echo $rows[csf("composition")]; ?></p></td>
                       <td width="60"><p><?php echo $rows[csf("gsm_weight")]; ?></p></td>
                      <td width="60"><p><?php echo $dia_other; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($rows[csf("order_quantity_set")],2,'.',''); ?>&nbsp;</p></td>
                    <td width="110"><p><?php echo $color_arr[$rows[csf("color_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($req_qty_other,2); ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_knit_recv_qty_other,2); ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_recv_bal_other,2); ?>&nbsp;</p></td> 
                    <td width="80" align="right"><p><?php echo number_format($tot_knit_issue_qty_other,2); ?></p></td>
                    <td width="" align="right"><p><?php echo number_format($tot_stock_other,2); ?></p></td>
                </tr>
            <?php	
            $k++;	
			$total_order_qnty_other+=$rows[csf("order_quantity_set")];
			$total_req_qty_other+=$req_qty_other;
			$total_knit_recv_qty_other+=$tot_knit_recv_qty_other;
			$total_recv_bal_other+=$tot_recv_bal_other;
			$total_knit_stock_other_qty+=$tot_stock_other;
			$total_tot_knit_issue_qty_other+=$tot_knit_issue_qty_other;
			}
			?>
            </table>
            <table width="1670" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" > 
                <tfoot>
                    <th width="30"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="80"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="60">&nbsp;</th>
                    <th width="60">&nbsp;</th>
                    <th width="80" align="right" id=""><?php echo number_format($total_order_qnty_other,2,'.',''); ?></th>
                    <th width="110">&nbsp;</th>
                    <th width="80" align="right"><?php echo number_format($total_req_qty_other,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_knit_recv_qty_other,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_recv_bal_other,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_tot_knit_issue_qty_other,2,'.',''); ?></th>
                    <th width="" align="right"><?php echo number_format($total_knit_stock_other,2,'.',''); ?></th>
                </tfoot>
            </table> 
        </div>   
    </fieldset>         
	<?php
	}//Knit end
	else if($cbo_report_type==2) // Woven Finish Start
	{
		$fin_woven_recv_array=array();
		$sql_recv=sql_select("select c.po_breakdown_id,c.color_id,sum(CASE WHEN c.entry_form in (17) and a.item_category=3  THEN c.quantity END) AS finish_receive_qnty from inv_receive_master a,product_details_master b, order_wise_pro_details c where a.entry_form=c.entry_form and  b.id=c.prod_id and c.entry_form in (17) and c.trans_id!=0 and a.item_category=3 and a.entry_form in(17) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by c.po_breakdown_id,c.color_id");
		foreach($sql_recv as $row)
		{
			 $fin_woven_recv_array[$row[csf('po_breakdown_id')]][$row[csf('color_id')]]['receive_qnty']=$row[csf('finish_receive_qnty')];
		} //print_r($fin_recv_array);die; 
		$issue_qnty=array();
		$sql_issue=sql_select("select b.po_breakdown_id,b.color_id, sum(b.quantity) as issue_qnty  from inv_finish_fabric_issue_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.trans_id!=0 and  a.status_active=1 and a.is_deleted=0 and b.entry_form=19 group by b.po_breakdown_id,b.color_id");
		foreach( $sql_issue as $row_iss )
		{
		$issue_qnty[$row_iss[csf('po_breakdown_id')]][$row_iss[csf('color_id')]]['issue_qnty']=$row_iss[csf('issue_qnty')];
		}
		$actual_cut_qnty=array();
		$sql_actual_cut_qty=sql_select(" select a.po_break_down_id,c.color_number_id, sum(b.production_qnty) as actual_cut_qty  from pro_garments_production_mst a, pro_garments_production_dtls b, wo_po_color_size_breakdown c where a.id=b.mst_id and b.color_size_break_down_id=c.id  and a.production_type=1 and a.is_deleted=0 and a.status_active=1  group by  a.po_break_down_id,c.color_number_id");
		foreach( $sql_actual_cut_qty as $row_actual)
		{
			$actual_cut_qnty[$row_actual[csf('po_break_down_id')]][$row_actual[csf('color_number_id')]]['actual_cut_qty']=$row_actual[csf('actual_cut_qty')];
		} 
	?>
    <fieldset style="width:1330px;">
        <table cellpadding="0" cellspacing="0" width="1330">
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:18px"><strong><?php echo $report_title; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?><br><?php $report_type=(str_replace("'","",$cbo_report_type)); if($report_type==2) echo 'Woven Finish'; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="14" style="font-size:14px"><strong> <?php if($date_from!="") echo "Upto : ".change_date_format(str_replace("'","",$txt_date_from)) ;?></strong></td>
            </tr>
        </table>
        <div align="left"><b>Main Fabric </b></div>
		<table width="1330" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
            <thead>
                <th width="30">SL</th>
                <th width="100">Buyer</th>
                <th width="90">Job</th>
                <th width="80">Order</th>
                <th width="100">Style</th> 
                <th width="100">Body Part</th>
                <th width="80">Order Qty(Pcs)</th>
                <th width="110">Color</th>
                <th width="80">Req. Qty</th>
                <th width="80">Total Recv.</th>
                <th width="80">Recv. Balance</th>
                <th width="80">Total Issued</th>
                <th width="80">Stock</th>
                <th width="80">Cons/Pcs</th>
                <th width="80">Possible Cut Pcs</th>
                <th width="">Actual Cut</th>
            </thead>
        </table>
        <div style="width:1350px; max-height:350px; overflow-y:scroll;" id="scroll_body">
			<table width="1330" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="table_body" > 
            <?php
			// Knit Finish Start
					$sql_main="Select a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, sum(d.order_quantity/a.total_set_qnty) as order_quantity_set, b.id as po_id, b.po_number,d.color_number_id as color_id,c.body_part_id,c.avg_finish_cons  from  wo_po_details_master a, wo_po_break_down b,  wo_pre_cost_fabric_cost_dtls  c, wo_po_color_size_breakdown d
					where a.job_no=b.job_no_mst and c.job_no=a.job_no and d.job_no_mst=c.job_no  and b.id=d.po_break_down_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1
					and  c.status_active=1 and c.is_deleted=0 and 
					a.company_name=$cbo_company_id and c.body_part_id in(1,20)  $ship_date $buyer_id_cond $job_no_cond $order_id_cond group by b.id,c.body_part_id,d.color_number_id,a.job_no_prefix_num,c.avg_finish_cons, a.job_no, a.buyer_name, a.style_ref_no, b.po_number,b.plan_cut,d.color_number_id,c.body_part_id order by a.buyer_name,b.id,a.job_no, b.po_number";
			//echo $sql_query;die;
			$i=1; 
			$nameArray=sql_select( $sql_main );
			foreach ($nameArray as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$dzn_qnty=0;
				if($costing_per_id_library[$row[csf('job_no')]]==1)
				{
					$dzn_qnty=12;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==3)
				{
					$dzn_qnty=12*2;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==4)
				{
					$dzn_qnty=12*3;
				}
				else if($costing_per_id_library[$row[csf('job_no')]]==5)
				{
					$dzn_qnty=12*4;
				}
				else
				{
					$dzn_qnty=1;
				}
				//echo $dzn_qnty.'='.$row[csf("cons")];
				$order_id=$row[csf('po_id')];
				$color_id=$row[csf("color_id")];
				$body_part_id=$row[csf("body_part_id")];
				$cons_avg=$row[csf("avg_finish_cons")];
				$tot_cons_avg=$cons_avg/$dzn_qnty;
				$woven_req_qty=($row[csf("avg_finish_cons")]/$dzn_qnty)*$row[csf("order_quantity_set")];
				$woven_recv_qty=$fin_woven_recv_array[$order_id][$color_id]['receive_qnty'];
				//$woven_recv_return_qty=$fin_woven_recv_array[$order_id][$color_id]['receive_return'];
				$tot_woven_recv_qty=$woven_recv_qty;
				$tot_woven_recv_bal=$woven_req_qty-$tot_woven_recv_qty;
				$tot_woven_issue_qty=$issue_qnty[$order_id][$color_id]['issue_qnty'];
				$tot_woven_stock=$tot_woven_recv_qty-$tot_woven_issue_qty;
				$tot_woven_actual_cut_qty=$actual_cut_qnty[$order_id][$color_id]['actual_cut_qty'];
				?>
                <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                    <td width="30"><?php echo $i; ?></td>
                    <td width="100"><p><?php echo $buyer_arr[$row[csf("buyer_name")]]; ?></p></td>
                    <td width="90"><p><?php echo $row[csf("job_no_prefix_num")]; ?></p></td>
                    <td width="80"><p><?php echo $order_arr[$order_id]; ?></p></td>
                    <td width="100"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
                    <td width="100"><p><?php echo $body_part[$row[csf("body_part_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($row[csf("order_quantity_set")],2,'.',''); ?>&nbsp;</p></td>
                    <td width="110"><p><?php echo $color_arr[$row[csf("color_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($woven_req_qty,2); ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_woven_recv_qty,2);?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_woven_recv_bal,2); ?>&nbsp;</p></td> 
                    <td width="80" align="right"><p><?php echo number_format($tot_woven_issue_qty,2); ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_woven_stock,2); ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php  echo number_format($tot_cons_avg,2); ?></p></td>
                    <td width="80" align="right"><p><?php $possible_cut=$tot_knit_issue_qty/($tot_cons_avg/$dzn_qnty); echo number_format($possible_cut,2); ?></p></td>
                    <td width="" align="right"><p><?php echo number_format($tot_woven_actual_cut_qty,2);?></p></td>
                </tr>
            <?php	
            $i++;	
			$total_order_qnty+=$row[csf("order_quantity_set")];
			$total_woven_recv_qty+=$tot_woven_recv_qty;
			$total_woven_issue_qty+=$tot_woven_issue_qty;
			$total_woven_balance_qty+=$tot_woven_recv_bal;
			$total_woven_stock_qty+=$tot_woven_stock;
			$total_woven_actual_cut_qty+=$tot_woven_actual_cut_qty;
			}
			?>
            </table>
            <table width="1330" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" > 
                <tfoot>
                    <th width="30"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="100"></th>
                    <th width="80" align="right" id=""><?php echo number_format($total_order_qnty,2,'.',''); ?></th>
                    <th width="110">&nbsp;</th>
                    <th width="80" align="right"><?php //echo number_format($total_req_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_woven_recv_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_woven_balance_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_woven_issue_qty,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_woven_stock_qty,2,'.',''); ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="80" align="right"><?php //echo number_format($total_possible_cut_pcs,2,'.',''); ?></th>
                    <th width="" align="right"><?php echo number_format($total_woven_actual_cut_qty,2,'.',''); ?></th>
                </tfoot>
            </table> 
        </div> 
        <div align="left"><b>Other Fabric </b>
        </div>
		<table width="1110" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
            <thead>
                <th width="30">SL</th>
                <th width="100">Buyer</th>
                <th width="90">Job</th>
                <th width="80">Order No</th>
                <th width="100">Style</th> 
                <th width="100">Body Part</th>
                <th width="80">Order Qty(Pcs)</th>
                <th width="110">Color</th>
                <th width="80">Req. Qty</th>
                <th width="80">Total Recv.</th>
                <th width="80">Recv. Balance</th>
                <th width="80">Total Issued</th>
                <th width="">Stock</th>
            </thead>
        </table>
        <div style="width:1130px; max-height:350px; overflow-y:scroll;" id="scroll_body">
			<table width="1110" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="table_body2" > 
            <?php
			// Knit Finish Start
					$sql_other2="Select a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no, sum(d.order_quantity/a.total_set_qnty) as order_quantity_set, b.id as po_id, b.po_number,d.color_number_id as color_id,c.body_part_id,c.avg_finish_cons  from  wo_po_details_master a, wo_po_break_down b,  wo_pre_cost_fabric_cost_dtls  c, wo_po_color_size_breakdown d
					where a.job_no=b.job_no_mst and c.job_no=a.job_no and d.job_no_mst=c.job_no  and b.id=d.po_break_down_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1
					and  c.status_active=1 and c.is_deleted=0 and 
					a.company_name=$cbo_company_id and c.body_part_id  not in(1,20)  $ship_date $buyer_id_cond $job_no_cond $order_id_cond group by b.id,c.body_part_id,d.color_number_id,a.job_no_prefix_num,c.avg_finish_cons, a.job_no, a.buyer_name, a.style_ref_no, b.po_number,b.plan_cut,d.color_number_id,c.body_part_id order by a.buyer_name,b.id,a.job_no, b.po_number";	
			//echo $sql_other;die;
			$k=1; 
			$other_data=sql_select( $sql_other );
			foreach ($other_data as $rows)
			{
				if ($k%2==0) $bgcolor2="#E9F3FF"; else $bgcolor2="#FFFFFF";
				
				$dzn_qnty=0;
				if($costing_per_id_library[$rows[csf('job_no')]]==1)
				{
					$dzn_qnty=12;
				}
				else if($costing_per_id_library[$rows[csf('job_no')]]==3)
				{
					$dzn_qnty=12*2;
				}
				else if($costing_per_id_library[$rows[csf('job_no')]]==4)
				{
					$dzn_qnty=12*3;
				}
				else if($costing_per_id_library[$rows[csf('job_no')]]==5)
				{
					$dzn_qnty=12*4;
				}
				else
				{
					$dzn_qnty=1;
				}
				//echo $dzn_qnty.'='.$row[csf("cons")];
				$order_id_other=$rows[csf('po_id')];
				$color_id_other=$rows[csf("color_id")];
				$body_part_id_other=$rows[csf("body_part_id")];
				//$tot_cons_avg_other=$cons_avg/$rows[csf("pcs")];
				$woven_req_qty_other=($rows[csf("cons")]/$dzn_qnty)*$rows[csf("order_quantity_set")];
				$woven_recv_qty_other=$fin_woven_recv_array[$order_id_other][$body_part_id_other][$color_id_other]['receive_qnty'];
				$tot_woven_recv_qty_other=$woven_recv_qty_other;
				$tot_woven_recv_bal_other=$woven_req_qty_other-$tot_woven_recv_qty_other;
				$tot_woven_issue_qty_other=$issue_qnty[$order_id_other][$color_id_other]['issue_qnty'];
				$tot_woven_stock_other=$tot_woven_recv_qty_other-$tot_woven_issue_qty_other;
				//$tot_actual_cut_qty_other=$actual_cut_qnty[$order_id_other][$color_id_other]['actual_cut_qty'];
				?>
                <tr bgcolor="<?php echo $bgcolor2;?>" onClick="change_color('trother<?php echo $k;?>','<?php echo $bgcolor;?>')" id="trother<?php echo $k;?>">
                    <td width="30"><?php echo $k; ?></td>
                    <td width="100"><p><?php echo $buyer_arr[$rows[csf("buyer_name")]]; ?></p></td>
                    <td width="90"><p><?php echo $rows[csf("job_no_prefix_num")]; ?></p></td>
                    <td width="80"><p><?php echo $rows[csf("po_number")]; ?></p></td>
                    <td width="100"><p><?php echo $rows[csf("style_ref_no")]; ?></p></td>
                    <td width="100"><p><?php echo $body_part[$rows[csf("body_part_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($rows[csf("order_quantity_set")],2,'.',''); ?>&nbsp;</p></td>
                    <td width="110"><p><?php echo $color_arr[$rows[csf("color_id")]]; ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($woven_req_qty_other,2); ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_knit_recv_qty_other,2); ?></p></td>
                    <td width="80" align="right"><p><?php echo number_format($tot_woven_recv_bal_other,2); ?>&nbsp;</p></td> 
                    <td width="80" align="right"><p><?php echo number_format($tot_woven_issue_qty_other,2); ?></p></td>
                    <td width="" align="right"><p><?php echo number_format($tot_woven_stock_other,2); ?></p></td>
                </tr>
            <?php	
            $k++;	
			$total_order_qnty_other+=$rows[csf("order_quantity_set")];
			$total_woven_req_qty_other+=$woven_req_qty_other;
			$total_woven_recv_qty_other+=$tot_knit_recv_qty_other;
			$total_woven_recv_bal_other+=$tot_woven_recv_bal_other;
			$total_woven_stock_other+=$tot_woven_stock_other;
			$total_woven_issue_qty_other+=$tot_woven_issue_qty_other;
			}
			?>
            </table>
            <table width="1110" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" > 
                <tfoot>
                    <th width="30"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="80"></th>
                    <th width="100"></th>
                    <th width="100"></th>
                    <th width="80" align="right" id=""><?php echo number_format($total_order_qnty_other,2,'.',''); ?></th>
                    <th width="110">&nbsp;</th>
                    <th width="80" align="right"><?php echo number_format($total_woven_req_qty_other,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_woven_recv_qty_other,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_woven_recv_bal_other,2,'.',''); ?></th>
                    <th width="80" align="right"><?php echo number_format($total_woven_issue_qty_other,2,'.',''); ?></th>
                    <th width="" align="right"><?php echo number_format($total_woven_stock_other,2,'.',''); ?></th>
                </tfoot>
            </table> 
        </div>   
    </fieldset>         
	<?php
	}
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
   
    exit();
}

?>