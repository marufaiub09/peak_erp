-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_po_embell_approval`
--

DROP TABLE IF EXISTS `wo_po_embell_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_po_embell_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_no_mst` varchar(25) NOT NULL,
  `po_break_down_id` int(11) NOT NULL DEFAULT '0',
  `embellishment_id` int(11) NOT NULL DEFAULT '0',
  `embellishment_type_id` int(11) NOT NULL DEFAULT '0',
  `color_name_id` int(11) NOT NULL DEFAULT '0',
  `target_approval_date` date NOT NULL,
  `sent_to_supplier` date NOT NULL,
  `submitted_to_buyer` date NOT NULL,
  `approval_status` int(2) NOT NULL DEFAULT '0',
  `approval_status_date` date NOT NULL,
  `supplier_name` int(11) NOT NULL DEFAULT '0',
  `embellishment_comments` text NOT NULL,
  `current_status` tinyint(4) NOT NULL DEFAULT '1',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `status_active` int(1) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `garments_nature` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_no_mst` (`job_no_mst`),
  KEY `embellishment_type_id` (`embellishment_type_id`),
  KEY `target_approval_date` (`target_approval_date`),
  KEY `sent_to_supplier` (`sent_to_supplier`),
  KEY `submitted_to_buyer` (`submitted_to_buyer`),
  KEY `approval_status` (`approval_status`),
  KEY `supplier_name` (`supplier_name`),
  KEY `is_deleted` (`is_deleted`),
  KEY `status_active` (`status_active`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_po_embell_approval`
--

LOCK TABLES `wo_po_embell_approval` WRITE;
/*!40000 ALTER TABLE `wo_po_embell_approval` DISABLE KEYS */;
INSERT INTO `wo_po_embell_approval` VALUES (1,'PAL-15-00434',486,1,1,309,'2015-11-01','0000-00-00','0000-00-00',3,'2015-11-01',11,'',1,0,1,2,'2015-11-18 13:14:42',0,'0000-00-00 00:00:00',2),(2,'PAL-15-00434',486,1,1,531,'2015-11-01','0000-00-00','0000-00-00',3,'2015-11-01',11,'',1,0,1,2,'2015-11-18 13:14:42',0,'0000-00-00 00:00:00',2),(3,'PAL-15-00434',486,1,1,533,'2015-11-01','0000-00-00','0000-00-00',3,'2015-11-01',11,'',1,0,1,2,'2015-11-18 13:14:42',0,'0000-00-00 00:00:00',2);
/*!40000 ALTER TABLE `wo_po_embell_approval` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
