<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_id", 140, "select id,location_name from lib_location where status_active =1 and is_deleted=0 and company_id='$data' order by location_name","id,location_name", 1, "--Select Location--", $selected, "load_drop_down( 'requires/machine_wise_kniting_prod_report_controller', document.getElementById('cbo_company_id').value+'_'+this.value, 'load_drop_down_floor', 'floor_td' );",0 );
	exit();     	 
}

if ($action=="load_drop_down_floor")
{
	$ex_data=explode('_',$data);
	if($ex_data[1]!=0) $location_cond=" and b.location_id='$ex_data[1]'"; else $location_cond="";
	
	echo create_drop_down( "cbo_floor_id", 140, "select a.id, a.floor_name from lib_prod_floor a, lib_machine_name b where a.id=b.floor_id and b.category_id=1 and b.company_id='$ex_data[0]' and b.status_active=1 and b.is_deleted=0  $location_cond group by a.id, a.floor_name order by a.floor_name","id,floor_name", 1, "-- Select Floor --", 0, "","" );
  exit();	 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$cbo_company=str_replace("'","",$cbo_company_id);
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
		
	$table_width=485+($datediff*70);
	ob_start();	
?>
	<div>
        <table width="<?php echo $table_width; ?>" cellpadding="0" cellspacing="0" id="caption" align="center">
            <tr>
               <td align="center" width="100%" colspan="7" class="form_caption" ><strong style="font-size:16px"><?php echo $company_library[$cbo_company]; ?></strong></td>
            </tr> 
            <tr>  
               <td align="center" width="100%" colspan="7" class="form_caption" ><strong style="font-size:13px"><?php echo $report_title;  echo "; From : ".change_date_format(str_replace("'","",$txt_date_from))." To : ".change_date_format(str_replace("'","",$txt_date_to))."" ;?></strong></td>
            </tr>
        </table>
        <?php
	
	$machine_name=str_replace("'","",$txt_machine_id);
	$floor_name=str_replace("'","",$cbo_floor_id);
	$company_short_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name"  );
	if (str_replace("'","",$cbo_location_id)==0) $location_id=""; else $location_id=" and location_id=$cbo_location_id";
	if (str_replace("'","",$txt_machine_id)==0 || str_replace("'","",$txt_machine_id)=='') $machine_id=""; else $machine_id=" and id in ( $machine_name )";
	if ($floor_name==0 || $floor_name=='') $floor_id=""; else $floor_id=" and floor_id in ( $floor_name )";
	
	$sql_machine_dtls="Select id, machine_no, brand, origin, prod_capacity, dia_width, floor_id from lib_machine_name where category_id=1 and company_id=$cbo_company_id and status_active=1 and is_deleted=0 $location_id $floor_id $machine_id order by id";
	//echo $sql_machine_dtls;
	$sql_machine=sql_select($sql_machine_dtls);
	$count_data=count($sql_machine);
	//echo $count_data;
	$machin_arr=array();
	$machine_dtls_array=array();
	
	foreach ( $sql_machine as $row )
	{
		$machin_arr[$row[csf('id')]]=$row[csf('id')];
		$machine_dtls_array[$row[csf('id')]]['machine_no']=$row[csf('machine_no')];
		$machine_dtls_array[$row[csf('id')]]['dia_width']=$row[csf('dia_width')];
		$machine_dtls_array[$row[csf('id')]]['floor_id']=$row[csf('floor_id')];
		$machine_dtls_array[$row[csf('id')]]['prod_capacity']=$row[csf('prod_capacity')];
	}
	//var_dump ($machin_arr);
	
	$machineArr=return_library_array( "select id,machine_no from lib_machine_name", "id", "machine_no"  );
	$company_short_library=return_library_array( "select id,company_short_name from lib_company", "id", "company_short_name"  );
	$floor_library=return_library_array( "select id,floor_name from  lib_prod_floor", "id", "floor_name"  );
	?>
	<div>
	<table width="<?php echo $table_width; ?> " border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all"> 
		<thead>
			<tr>
				<th width="35">SL</th>
				<th width="80">Machine No</th>
				<th width="70">Floor</th>
				<th width="60">Mach. Dia</th>
				<th width="70">Capacity</th>
				<?php
				//$date_data_array=array();
				for($j=0;$j<$datediff;$j++)
				{
					$newDate =add_date(str_replace("'","",$txt_date_from),$j);
					//$full_date=change_date_format($newdate);
					$days_months=explode('-',$newDate);
				?>
					<th width="70"><p><?php echo date("d-M",strtotime($newDate)); ?></p></th>
				<?php
				}  
				?>
				 <th width="80">Total</th>
				 <th width="80">Avg/Day</th>
		   </tr>
		</thead>
	</table>
    <div style="width:<?php echo $table_width+17; ?>px; overflow-y:scroll; max-height:300px;" id="scroll_body">
	<table align="center" cellspacing="0" width="<?php echo $table_width; ?> "  border="1" rules="all" class="rpt_table" >
	<?php 
	$cbo_location=str_replace("'","",$cbo_location_id);
	$machine=str_replace("'","",$txt_machine_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	$floor_name=str_replace("'","",$cbo_floor_id);
	if ($cbo_location==0) $location_id =""; else $location_id =" and a.location_id=$cbo_location ";
	if ($machine=="") $machine_no =""; else $machine_no =" and b.machine_no_id in ( $machine ) ";
	if ($machine=="") $machine_no_sub =""; else $machine_no_sub =" and b.machine_id in ( $machine ) ";
	if( $date_from==0 && $date_to==0 ) $receive_date=""; else $receive_date= " and a.receive_date between ".$txt_date_from." and ".$txt_date_to."";
	if( $date_from==0 && $date_to==0 ) $receive_date_sub=""; else $receive_date_sub= " and a.product_date between ".$txt_date_from." and ".$txt_date_to."";

	if ($floor_name==0 || $floor_name=='') $floor_id_val=""; else $floor_id_val=" and b.floor_id=$floor_name";
	
	$sql_result="Select a.receive_date, b.machine_no_id, b.floor_id, sum(b.grey_receive_qnty) as grey_receive_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and a.entry_form=2 and a.item_category=13 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.machine_no_id!=0 and a.company_id=$cbo_company $location_id $machine_no $receive_date group by a.receive_date, b.machine_no_id, b.floor_id ";
	//echo $sql_result;
	$sql_dtls=sql_select($sql_result);
	$date_data_arr=array();
	$date_total_arr=array();
	foreach ($sql_dtls as $row)
	{
		$date_data_arr[$row[csf('machine_no_id')]][change_date_format($row[csf('receive_date')],'','',1)]=$row[csf('grey_receive_qnty')];
		$date_data_arr[$row[csf('machine_no_id')]][$row[csf('floor_id')]]=$row[csf('floor_id')];
		$date_total_arr[change_date_format($row[csf('receive_date')],'','',1)]+=$row[csf('grey_receive_qnty')];
	}
	//var_dump ($date_data_arr);
	$sql_result_subcon="Select a.product_date, b.machine_id, sum(b.product_qnty) as product_qnty from  subcon_production_mst a, subcon_production_dtls b where a.id=b.mst_id and a.product_type=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.machine_id!=0 and a.company_id=$cbo_company $location_id $machine_no_sub $receive_date_sub group by a.product_date, b.machine_id ";
	
	$sql_dtls_sub=sql_select($sql_result_subcon);
	$date_data_sub_arr=array();
	$date_total_sub_arr=array();
	
	foreach ($sql_dtls_sub as $row)
	{
		$date_data_sub_arr[$row[csf('machine_id')]][change_date_format($row[csf('product_date')],'','',1)]=$row[csf('product_qnty')];
		//$date_data_arr[$row[csf('machine_id')]][$row[csf('floor_id')]]=$row[csf('floor_id')];
		$date_total_sub_arr[change_date_format($row[csf('product_date')],'','',1)]+=$row[csf('product_qnty')];
	}
	//var_dump ($date_data_sub_arr);
	
	$idle_machine_array=array();
	$idol_sql="select id, machine_entry_tbl_id, machine_no, from_date, from_hour, from_minute, to_date, to_hour, to_minute, machine_idle_cause, remarks from pro_cause_of_machine_idle where status_active=1 and is_deleted=0 and machine_idle_cause in (1,2,3,6,7,8)";
	$idol_sql_result=sql_select($idol_sql); $timeDiffstart='';
	foreach ($idol_sql_result as $row)
	{
		$from_date=change_date_format($row[csf('from_date')],'','',1);	$from_hour=$row[csf('from_hour')]; $from_minute=$row[csf('from_minute')];
		$to_date=change_date_format($row[csf('to_date')],'','',1);	$to_hour=$row[csf('to_hour')]; $to_minute=$row[csf('to_minute')];
		
		$start_time='';
		$start_time=$from_hour.':'.$from_minute.':'.'00';
		
		$end_time='';
		$end_time=$to_hour.':'.$to_minute.':'.'00';
		$datediff_n = datediff( 'd', $from_date, $to_date);
		if ($datediff_n==1)
		{
			$p_time=$end_time;
			$timeDiffstart=datediff(n,$start_time,$p_time);
			$timeDiffend=datediff(n,$p_time,$start_time);
			//echo $timeDiffstart.'kk<br>';
		}
		else
		{
			$p_time="00:00:00";
			$timeDiffstart=datediff(n,$p_time,$start_time);
			$timeDiffend=datediff(n,$p_time,$end_time);
		}
		if ($datediff_n==1)
		{
			$idle_machine_array[$row[csf('machine_entry_tbl_id')]][$from_date]=$timeDiffstart;
		}
		else
		{	
			for($k=0; $k<$datediff_n; $k++)
			{
				$newdate_n =change_date_format(add_date(str_replace("'","",$from_date),$k),'','',1);
				//echo $to_date.'=='.$newdate_n.'<br>';
				if($from_date==$newdate_n)
				{
					$idle_machine_array[$row[csf('machine_entry_tbl_id')]][$newdate_n]=$timeDiffstart;
				}
				else if($to_date!=$newdate_n)
				{
					$idle_machine_array[$row[csf('machine_entry_tbl_id')]][$newdate_n]=1440;
				}
				else if($to_date==$newdate_n)
				{
					$idle_machine_array[$row[csf('machine_entry_tbl_id')]][$newdate_n]=$timeDiffend;
				}
			}
		}
	}
	//var_dump($idle_machine_array);

	$i=1;
	$avg_capacity=0;
	$avg_date_capacity=array();
	$total_qty_date=array();
	$date_total_arr_all=array();
	$tot_count=0;
	$machine_wise_capacity_tot=array();
	$machine_wise_capacity=array();
	$total_capacity_loss=array();

	foreach ( $machin_arr as $machine_id=>$machine_val )
	{
		if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
		
		$machine_no=$machine_dtls_array[$machine_val]['machine_no'];
		$machine_dia=$machine_dtls_array[$machine_val]['dia_width'];
		$machine_capacity=$machine_dtls_array[$machine_val]['prod_capacity'];
		$machine_floor=$machine_dtls_array[$machine_val]['floor_id'];
		//echo $machine_no;
		$date_data_array=array();
		$tot_quenty=0;
		$avg_production=0;
		$avg_day=0;
		$newdate =add_date(str_replace("'","",$txt_date_from),$j);
		//$date_data_array[$j]=$newdate;
		//asort($machine_no);
	?>
		<tr bgcolor="<?php echo $bgcolor; ?>" style="height:35px;" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
			<td width="35"><?php echo $i; ?></td>
			<td width="80"><p><?php echo $machine_no;?></p></td>
			<td width="70"><p><?php echo $floor_library[$machine_floor]; ?></p></td>
			<td width="60"><p><?php echo $machine_dia; ?></p></td>
			<td width="70" align="right"><?php echo $machine_capacity; $tot_capacity+=$machine_capacity; if($machine_capacity>0) $avg_capacity++; ?>&nbsp;</td>
			<?php

			for($j=0;$j<$datediff;$j++)
			{
				$newdate =change_date_format(add_date(str_replace("'","",$txt_date_from),$j),'','',1);
				if ($date_data_arr[$machine_id][$newdate]+$date_data_sub_arr[$machine_id][$newdate]=="")
					$tdcolor="#FF000B";
				else if ($date_data_arr[$machine_id][$newdate]+$date_data_sub_arr[$machine_id][$newdate]<$machine_capacity)
					$tdcolor="#FFF000";
				else if ($date_data_arr[$machine_id][$newdate]+$date_data_sub_arr[$machine_id][$newdate]>=$machine_capacity)
					$tdcolor="#009933";
					
				$total_qty_date[$machine_id][$newdate]=$date_data_arr[$machine_id][$newdate]+$date_data_sub_arr[$machine_id][$newdate];
				?>
					<td width="70" bgcolor="<?php echo $tdcolor; ?>" align="right" id="date_data_arr" ><a href="##" onclick="openmypage_idle('<?php echo $machine_val; ?>','<?php echo $newdate;?>','idle_for');" >
					<?php 
					$machine_wise_capacity[$machine_id][$newdate]=(($machine_capacity/24)/60)*($idle_machine_array[$machine_id][$newdate]);
					$machine_wise_capacity_tot[$machine_id][$newdate]=$machine_capacity-$machine_wise_capacity[$machine_id][$newdate];
					
					if ($total_qty_date[$machine_id][$newdate]<$machine_capacity && $total_qty_date[$machine_id][$newdate]!='' )
					{
						echo number_format($total_qty_date[$machine_id][$newdate],2).'&nbsp;<br><i>'.number_format((($total_qty_date[$machine_id][$newdate]/$machine_capacity)*100),2).'%</i>'; //.'=='.$machine_wise_capacity_tot[$machine_id][$newdate]
					}
					else 
					{
						echo number_format($total_qty_date[$machine_id][$newdate],2).'&nbsp;<br><i>'.number_format((($total_qty_date[$machine_id][$newdate]/$machine_capacity)*100),2).'%</i>'; 
					}
					$date_total_arr_all[$newdate]+=$total_qty_date[$machine_id][$newdate];
					 if ($total_qty_date[$machine_id][$newdate]>0) $avg_production++; ?>&nbsp;</a></td>
				<?php
				$tot_quenty+=$total_qty_date[$machine_id][$newdate];
				$avg_day=$tot_quenty/$avg_production;
				$total_capacity_loss[$newdate]+=$machine_wise_capacity_tot[$machine_id][$newdate];

				if ($avg_day=="")
					$tdcolor="#FF0000";
				else if ($avg_day<$machine_capacity)
					$tdcolor="#FFF000";
				else if ($avg_day>=$machine_capacity)
					$tdcolor="#009933";
					
			 if ($total_qty_date[$machine_id][$newdate]>0)$avg_date_capacity[$newdate]++;  
			} 
			?>
            <td width="80" align="right"><?php echo number_format($tot_quenty,2); if ($tot_quenty>0) $tot_count++; ?>&nbsp;</td>
            <td width="80" align="right" bgcolor="<?php echo $tdcolor; ?>" ><?php echo number_format($avg_day,2).'&nbsp;<br><i>'.number_format((($avg_day/$machine_capacity)*100),2).'%</i>'; $tot_avg_day+=$avg_day; ?>&nbsp;</td>
		</tr>
		 <?php
		$i++;
		 $grand_tot_qnty+=$tot_quenty;
	}
	?>
    <tfoot>
    	<tr>
        	<td colspan="4" align="right"><strong>Production Total</strong></td>
            <td align="right">&nbsp;</td>
			<?php
			
			for($j=0;$j<$datediff;$j++)
			{
				$newdate =change_date_format(add_date(str_replace("'","",$txt_date_from),$j),'','',1);
				//$date_data_array[$newdate]=$newdate;
				
				?>
				<td align="right"><?php echo number_format($date_total_arr_all[$newdate],2); ?>&nbsp;</td>
                <?php
			}
			?>
            <td align="right"><strong><?php echo number_format($grand_tot_qnty,2); ?></strong>&nbsp;</td>
            <td align="right"><strong><?php //echo number_format($tot_avg_day,2); ?></strong>&nbsp;</td>
        </tr>
        <tr>
        	<td colspan="4" align="right"><strong>Capacity Total</strong></td>
            <td align="right"><?php echo number_format($tot_capacity,0); ?>&nbsp;</td>
			<?php
			$day_wise_capacity_tot=array();
			$day_wise_capacity=array();
			for($j=0;$j<$datediff;$j++)
			{
				$newdate =change_date_format(add_date(str_replace("'","",$txt_date_from),$j),'','',1);
				//$date_data_array[$newdate]=$newdate;
				$date_total_arr_all[$newdate]=$date_total_arr[$newdate]+$date_total_sub_arr[$newdate];
				
				$day_wise_capacity[$newdate]=(($tot_capacity/24)/60)*($idle_machine_array[$newdate]);
				$day_wise_capacity_tot[$newdate]=$tot_capacity-$day_wise_capacity[$newdate];
				?>
				<td align="right"><?php echo number_format($total_capacity_loss[$newdate],2); $total_capacity+=$total_capacity_loss[$newdate]; ?>&nbsp;</td>
                <?php
			}
			?>
            <td align="right"><strong><?php echo number_format($total_capacity,2); ?></strong>&nbsp;</td>
            <td align="right"><strong><?php //echo number_format($tot_avg_day,2); ?></strong>&nbsp;</td>
        </tr>	   	
        <tr>
        	<td colspan="4" align="right"><strong>Achievement % On Day Capacity</strong></td>
            <td align="right"><strong><?php //echo number_format($tot_capacity/$avg_capacity,2); ?></strong></td>
			<?php
			for($j=0;$j<$datediff;$j++)
			{
				$newdate =change_date_format(add_date(str_replace("'","",$txt_date_from),$j),'','',1);
				$date_data_array[$newdate]=$newdate;
				if (($date_total_arr_all[$newdate]/$avg_date_capacity[$newdate])=="")
					$tdcolor="#FF0000";
				else if (($date_total_arr_all[$newdate]/$avg_date_capacity[$newdate])<($tot_capacity/$avg_capacity))
					$tdcolor="#FFF000";
				else if (($date_total_arr_all[$newdate]/$avg_date_capacity[$newdate])>=($tot_capacity/$avg_capacity))
					$tdcolor="#009933";
				?>
				<td align="right" bgcolor="<?php echo $tdcolor; ?>"><?php $ach_day_capacity=($date_total_arr_all[$newdate]/$total_capacity_loss[$newdate])*100; echo number_format($ach_day_capacity,2).' %'; ?>&nbsp;</td>
                <?php
			}
			?>
            <td align="right" bgcolor="<?php echo $tdcolor;?>"><strong><?php $tot_per=($grand_tot_qnty/$total_capacity)*100; echo number_format($tot_per,2).'%';  ?></strong>&nbsp;</td>
            <td align="right"><strong><?php //$tot_per=($tot_avg_day/$tot_capacity)*100; echo number_format($tot_per,2).'%'; ?></strong>&nbsp;</td>
        </tr>
	   	<tr>
        	<td colspan="4" align="right"><strong>Opportunity (Loss)/Gain</strong></td>
            <td align="right"><strong><?php //echo number_format($tot_capacity/$avg_capacity,2); ?></strong></td>
			<?php
			for($j=0;$j<$datediff;$j++)
			{
				$newdate =change_date_format(add_date(str_replace("'","",$txt_date_from),$j),'','',1);
				//$date_data_array[$newdate]=$newdate;
				if (($date_total_arr_all[$newdate]/$avg_date_capacity[$newdate])=="")
					$tdcolor="#FF0000";
				else if (($date_total_arr_all[$newdate]/$avg_date_capacity[$newdate])<($tot_capacity/$avg_capacity))
					$tdcolor="#FFF000";
				else if (($date_total_arr_all[$newdate]/$avg_date_capacity[$newdate])>=($tot_capacity/$avg_capacity))
					$tdcolor="#009933";
				?>
				<td align="right" bgcolor="<?php echo $tdcolor;?>">
				<?php $day_capacity=($date_total_arr_all[$newdate]/$total_capacity_loss[$newdate])*100;
				   $day_cap_per=$day_capacity-100;
				    
				if ($day_capacity<100)
				{ 
					$exp_cap=preg_replace("#\-\#", "() ", $day_cap_per);
					$less_greter='('.$exp_cap[0].')';
				} 
				else 
				{
					$less_greter=$day_cap_per;
				}
				   echo number_format($day_cap_per,2).' %'; 
				?>&nbsp;</td>
                <?php
			}
			?>
            <td align="right" bgcolor="<?php echo $tdcolor;?>"><strong><?php echo number_format($tot_per-100,2).'%'; ?></strong>&nbsp;</td>
            <td align="right"><strong>&nbsp;</strong>&nbsp;</td>
        </tr>
     </tfoot>
    </table>
        <div style="font-style:italic; color:#FF0000; font-size:18px; margin-left:20px" ><p><i><b>Note:</b>Machine Capacity has been exploded from total capacity due to production hampered for fowlling reasons: </i></p>
        	<ul>
                <li>Disorder</li>
                <li>Routine Maintenance</li>
                <li>Job Not Available</li>
                <li>Worker Unrest</li>
                <li>Off-Day</li>
                <li>Material Not Available</li>
            </ul>
        </div>
    </div>
    </div>
    <?php
	foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	//echo "$total_data####$filename";
	exit();      
}

if($action=="machine_no_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	$im_data=explode('_',$data);
	//print_r ($im_data);
?>
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hid_machine_id').val( id );
			$('#hid_machine_name').val( name );
		}
		
		function hidden_field_reset()
		{
			$('#hid_machine_id').val('');
			$('#hid_machine_name').val( '' );
			selected_id = new Array();
			selected_name = new Array();
		}
    </script>
</head>
<input type="hidden" name="hid_machine_id" id="hid_machine_id" />
<input type="hidden" name="hid_machine_name" id="hid_machine_name" />
<?php	
	$sql = "select a.id, a.machine_no, a.brand, a.origin, a.machine_group, b.floor_name  from lib_machine_name a, lib_prod_floor b where a.floor_id=b.id and a.category_id=1 and a.company_id=$im_data[0] and a.status_active=1 and a.is_deleted=0 and a.is_locked=0 order by a.machine_no, b.floor_name ";
	//echo  $sql;
	
	echo create_list_view("tbl_list_search", "Machine Name,Machine Group,Floor Name", "200,110,110","450","350",0, $sql , "js_set_value", "id,machine_no", "", 1, "0,0,0", $arr , "machine_no,machine_group,floor_name", "",'setFilterGrid(\'tbl_list_search\',-1);','0,0,0','',1) ;
	
   exit(); 
}

if($action=="idle_for")
{
	echo load_html_head_contents("Cause of Machine Idle Report", "../../../", 1, 1,$unicode,'','');
 	extract($_REQUEST);
	if($db_type==0)
	{
		$cng_date=change_date_format($date,"yyyy-mm-dd", "-",1);
	}
	elseif($db_type==2)
	{
		$cng_date=$date;
	}
	$sql= "SELECT id, machine_entry_tbl_id, machine_no, from_date, from_hour, from_minute, to_date, to_hour, to_minute, machine_idle_cause, remarks from  pro_cause_of_machine_idle where machine_entry_tbl_id='$machine_id' and '$cng_date' between from_date and to_date and is_deleted=0 and status_active=1";
	
?>
	<fieldset style="width:550px; margin-left:5px">
        <table border="1" class="rpt_table" rules="all" width="530" cellpadding="0" cellspacing="0">
            <thead>
                <th width="30">SL</th>
                <th width="120">From Date and Time</th>
                <th width="120">To Date and Time</th>
                <th width="100">Cause of Machine Idle</th>
                <th width="140">Remarks</th>
            </thead>
            <tbody>
                <?php
                $i=1; $total_qnty=0;
                $result=sql_select($sql);
                foreach($result as $row)
                {
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
					
					$from_date=date("Y-m-d",strtotime($row[csf('from_date')]));	$from_hour=$row[csf('from_hour')]; $from_minute=$row[csf('from_minute')];
					$to_date=date("Y-m-d",strtotime($row[csf('to_date')]));	$to_hour=$row[csf('to_hour')]; $to_minute=$row[csf('to_minute')];
					
					$start_time='';
					$start_time=$from_hour.':'.$from_minute;
					
					$end_time='';
					$end_time=$to_hour.':'.$to_minute;
					
					$start_date=change_date_format($from_date)." - ".$start_time;
					$end_date=change_date_format($to_date)." - ".$end_time;
                ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
                        <td width="30"><?php echo $i; ?></td>
                        <td width="120"><p><?php echo $start_date; ?>&nbsp;</p></td>
                        <td width="120"><p><?php echo $end_date; ?>&nbsp;</p></td>
                        <td width="100"><p><?php echo $cause_type[$row[csf('machine_idle_cause')]]; ?>&nbsp;</p></td>
                        <td width="140"><p><?php echo $row[csf('remarks')]; ?>&nbsp;</p></td>
                    </tr>
                <?php
                $i++;
                }
                ?>
                </tbody>
            </table>
	</fieldset>   
<?php	
	//echo $sql;
	//echo "<br />". create_list_view ( "list_view", "Order No,Order Qnty,Pub Shipment Date", "200,120,220","540","220",1, "SELECT b.id,b.po_number,b.pub_shipment_date,b.po_quantity*a.total_set_qnty as po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id='$po_break_down_id' and a.is_deleted=0 and a.status_active=1", "", "","", 1, '0,0,0', $arr, "po_number,po_quantity,pub_shipment_date","../requires/date_wise_production_report_controller", '','0,1,3');
	
	exit();
}

?>