<?php
/*-------------------------------------------- Comments
Purpose			: 	Yarn Work order entry
Functionality	        :	
JS Functions	        :
Created by		:	Bilas
Creation date 	        : 	22-04-13
Updated by 		: 	Kausar	(Creating Report)		
Update date		: 	13-02-2014	  	   
QC Performed BY	        :		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Work Order","../../", 1, 1, $unicode,'',''); 

$color_sql = execute_query("select id,color_name from lib_color order by id");
$color_name = "";
while( $result = mysql_fetch_array($color_sql))
{ 
	$color_name.= "{value:'".$result[csf('color_name')]."',id:".$result[csf('id')]."},";
}
 
?> 
<script>
	var permission='<?php echo $permission; ?>';
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";

	function fn_disable_enable(str)
	{
		if(str==3)
		{
			$("#txt_buyer_po_no").attr("disabled",false);
		}
		else
		{
			$("#txt_buyer_po_no").val('');
			$("#txt_buyer_po").val('');
			$("#txt_job_selected").val('');
			$("#txt_buyer_po_no").attr("disabled",true);
		}
	}
 
	// for buyer po
	function openmypage()
	{
		var company = $("#cbo_company_name").val();
		var garments_nature = $("#garments_nature").val(); 
		var txt_buyer_po_no = $("#txt_buyer_po_no").val(); // if value has then it will be selected
		var txt_buyer_po = $("#txt_buyer_po").val(); // if value has then it will be selected
		var txt_job_selected = $("#txt_job_selected").val();
		var page_link = 'requires/yarn_work_order_controller.php?action=order_popup&company='+company+'&garments_nature='+garments_nature+'&txt_buyer_po_no='+txt_buyer_po_no+'&txt_buyer_po='+txt_buyer_po+'&txt_job_selected='+txt_job_selected;
		var title = "Order Search"; 
		
		if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var break_down_id=this.contentDoc.getElementById("txt_selected_id").value; //break down id here
			var po_number=this.contentDoc.getElementById("txt_selected").value; // po_number
			var job_number=this.contentDoc.getElementById("txt_selected_job").value; // job_number
			$("#txt_buyer_po_no").val(po_number); 
			$("#txt_buyer_po").val(break_down_id);
			$("#txt_job_selected").val(job_number);
			
			if(break_down_id!="")
			{			
				freeze_window(5);			
				show_list_view(break_down_id+'***'+job_number,'show_dtls_listview','details_container','requires/yarn_work_order_controller','');
				release_freezing();
			}
			else
			{
				$("#details_container").html('');
			}
			
			var update_id=$("#update_id").val();
			if(update_id!="")
			{
				var delID=return_global_ajax_value( update_id, 'previous_dtls_id', '', 'requires/yarn_work_order_controller');//For Buyer Po Changed
				$("#txt_delete_row").val(delID);
			}
			
		}
	}

	function control_composition(id,td,type)
	{
		var cbocompone=(document.getElementById('cbocompone_'+id).value);
		var cbocomptwo=(document.getElementById('cbocomptwo_'+id).value);
		var percentone=(document.getElementById('percentone_'+id).value)*1;
		var percenttwo=(document.getElementById('percenttwo_'+id).value)*1;
		var row_num=$('#tbl_yarn_cost tr').length-1;
		
		if(type=='percent_one' && percentone>100)
		{
			alert("Greater Than 100 Not Allwed");
			document.getElementById('percentone_'+id).value="";
		}
		
		if(type=='percent_one' && percentone<=0)
		{
			alert("0 Or Less Than 0 Not Allwed")
			document.getElementById('percentone_'+id).value="";
			document.getElementById('percentone_'+id).disabled=true;
			document.getElementById('cbocompone_'+id).value=0;
			document.getElementById('cbocompone_'+id).disabled=true;
			document.getElementById('percenttwo_'+id).value=100;
			document.getElementById('percenttwo_'+id).disabled=false;
			document.getElementById('cbocomptwo_'+id).disabled=false;
		}
		if(type=='percent_one' && percentone==100)
		{
			document.getElementById('percenttwo_'+id).value="";
			document.getElementById('cbocomptwo_'+id).value=0;
			document.getElementById('percenttwo_'+id).disabled=true;
			document.getElementById('cbocomptwo_'+id).disabled=true;
		}
		
		if(type=='percent_one' && percentone < 100 && percentone > 0 )
		{
			document.getElementById('percenttwo_'+id).value=100-percentone;
			document.getElementById('percenttwo_'+id).disabled=false;
			document.getElementById('cbocomptwo_'+id).disabled=false;
		}
		
		if(type=='comp_one' && cbocompone==cbocomptwo  )
		{
			alert("Same Composition Not Allowed");
			document.getElementById('cbocompone_'+id).value=0;
		}
		
		if(type=='percent_two' && percenttwo>100)
		{
			alert("Greater Than 100 Not Allwed")
			document.getElementById('percenttwo_'+id).value="";
		}
		if(type=='percent_two' && percenttwo<=0)
		{
			alert("0 Or Less Than 0 Not Allwed")
			document.getElementById('percenttwo_'+id).value="";
			document.getElementById('percenttwo_'+id).disabled=true;
			document.getElementById('cbocomptwo_'+id).value=0;
			document.getElementById('cbocomptwo_'+id).disabled=true;
			document.getElementById('percentone_'+id).value=100;
			document.getElementById('percentone_'+id).disabled=false;
			document.getElementById('cbocompone_'+id).disabled=false;
		}
		if(type=='percent_two' && percenttwo==100)
		{
			document.getElementById('percentone_'+id).value="";
			document.getElementById('cbocompone_'+id).value=0;
			document.getElementById('percentone_'+id).disabled=true;
			document.getElementById('cbocompone_'+id).disabled=true;
		}
		
		if(type=='percent_two' && percenttwo<100 && percenttwo>0)
		{
			document.getElementById('percentone_'+id).value=100-percenttwo;
			document.getElementById('percentone_'+id).disabled=false;
			document.getElementById('cbocompone_'+id).disabled=false;
		}
		
		if(type=='comp_two' && cbocomptwo==cbocompone)
		{
			alert("Same Composition Not Allowed");
			document.getElementById('cbocomptwo_'+id).value=0;
		}
	}

	function calculate_yarn_consumption_ratio(i)
	{
		var cbocount=$('#txt_quantity_'+i).val();
		var cbocompone=$('#txt_rate_'+i).val();
		var amount = cbocount*1*cbocompone*1;
		$('#txt_amount_'+i).val(amount);
	 }

	function open_terms_condition_popup(page_link,title)
	{
		var txt_id_no=document.getElementById('update_id').value;
		if (txt_wo_number=="")
		{
			alert("Save The Yarn Work Order First");
			return;
		}	
		else
		{
			page_link=page_link+get_submitted_data_string('update_id','../../');
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=720px,height=370px,center=1,resize=1,scrolling=0','../')
			emailwindow.onclose=function(){};
		}
	}

	function fn_inc_decr_row(rowid,type)
	{
		if(type=="increase")
		{ 
			var row = $("#tbl_details tbody tr:last").attr('id'); 			
			var valuesLastRow = $("#tbl_details tbody tr:last").find('input[name=txt_color_'+row+']').val(); 	
			if(valuesLastRow!="")
			{
				row = row*1+1;
				var responseHtml = return_ajax_request_value(row, 'append_load_details_container', 'requires/yarn_work_order_controller');
				$("#tbl_details tbody").append(responseHtml); 
			}
		}
		else if(type=="decrease")
		{
			//alert(rowid);
			var row = $("#tbl_details tr").length-1;
			if(rowid*1!="" && row*1>1)
			{ 								 
				var vals = $("#txt_delete_row").val();
				var delID = $("#txt_row_id_"+rowid).val();
				if(vals!="")
 				$("#txt_delete_row").val(vals+','+delID);
			else
				$("#txt_delete_row").val(delID);				
			
			$("#tbl_details tr#"+rowid).remove();		
			}
			else
				return;	
		}
	}

	function colorName(rowID)
	{
		$("#hidden_colorID_"+rowID).val('');
		$(function() {
			var color_name = [<?php echo substr($color_name, 0, -1); ?>]; 
			$("#txt_color_"+rowID).autocomplete({
				source: color_name,
				select: function (event, ui) {
					$("#txt_color_"+rowID).val(ui.item.value); // display the selected text
					$("#hidden_colorID_"+rowID).val(ui.item.id); // save selected id to hidden input
					fn_copy_color(rowID);
				} 
			});
		});
	}

	function fn_copy_color(i)
	{
		var colorName = $("#txt_color_"+i).val();
		var colorID = $("#hidden_colorID_"+i).val();
		var rowCount = $('#tbl_details tr').length-1;
		for(var j=i; j<=rowCount; j++)
		{
			try
			{
				$("#txt_color_"+j).val(colorName);
				$("#hidden_colorID_"+j).val(colorID);	 	
			}
			catch(err)
			{
			//do nothing
			}
		}
	}

	function fnc_yarn_order_entry(operation)
	{
		if(operation==4)
		 {
			var report_title=$( "div.form_caption" ).html();
			print_report( $('#cbo_company_name').val()+'*'+$('#update_id').val()+'*'+report_title,"yarn_work_order_print", "requires/yarn_work_order_controller")
			return;
		 }
		else if(operation==0 || operation==1 || operation==2)
		{
			if( form_validation('cbo_company_name*cbo_item_category*cbo_supplier*txt_wo_date*cbo_currency*cbo_wo_basis*cbo_pay_mode*cbo_source*txt_delivery_date','Company Name*Item Category*Supplier Name*WO Date*Currency*WO Basis*Pay Mode*Source*Delivery Date')==false )
			{
				return;
			}
			if($("#cbo_wo_basis").val()==3 && form_validation('txt_buyer_po','Buyer PO')==false ) //buyer po basis
			{
				return;
			}
			try
			{
				var row = $("#tbl_details tbody tr:last").attr('id');
				if(row<=0) throw "Save Not Possible!!Input Item Details For Save";
			}
			catch(err)
			{
				alert("Error : "+err);
				return;
			}
			// save data here
			var detailsData="";
			for(var i=1;i<=row;i++)
			{
				try
				{ 
					if( form_validation('txt_color_'+i+'*cbocount_'+i+'*cbotype_'+i+'*cbo_uom_'+i+'*txt_quantity_'+i+'*txt_rate_'+i+'*txt_amount_'+i,'Color*Count*Yarn Type*UOM*Quantity*Rate*Amount')==false )
					{
						return;
					}

					if( $("#txt_quantity_"+i).val()*1 <= 0 || $("#txt_rate_"+i).val()*1 <= 0 )
					{
						alert("Quantity OR Rate Can not be 0 or less than 0");
						$("#txt_quantity_"+i).focus();
						return;
					}
								  
					detailsData+='*txt_po_brakdown_id_'+i+'*txt_color_'+i+'*hidden_colorID_'+i+'*cbocount_'+i+'*cbocompone_'+i+'*percentone_'+i+'*cbocomptwo_'+i+'*percenttwo_'+i+'*cbotype_'+i+'*cbo_uom_'+i+'*txt_quantity_'+i+'*txt_rate_'+i+'*txt_amount_'+i+'*txt_row_id_'+i;
				}
				catch(err){}
			}
			var is_approved=$('#is_approved').val();//approval requisition item Change not allowed
			if(is_approved==1)
			{
				alert("This Work Order is Approved. So Change Not Allowed");
				return;	
			}

			var data="action=save_update_delete&operation="+operation+'&total_row='+row+get_submitted_data_string('garments_nature*update_id*txt_wo_number*cbo_company_name*cbo_item_category*cbo_supplier*txt_wo_date*cbo_currency*cbo_wo_basis*cbo_pay_mode*cbo_source*txt_delivery_date*txt_attention*txt_buyer_po*txt_delete_row*txt_buyer_name*txt_style*txt_do_no*txt_remarks'+detailsData,"../../");
			//alert(data);return;
			
			freeze_window(operation);
			http.open("POST","requires/yarn_work_order_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_yarn_order_entry_reponse;
		}
	}

	function fnc_yarn_order_entry_reponse()
	{	
		if(http.readyState == 4) 
		{
			//alert(http.responseText);	  		
			var reponse=trim(http.responseText).split('**');
			show_msg(trim(reponse[0]));
			if(reponse[0]==0 || reponse[0]==1)
			{
				$("#txt_wo_number").val(reponse[1]);
				$("#update_id").val(reponse[2]);
				show_list_view(reponse[2],'show_dtls_listview_update','details_container','requires/yarn_work_order_controller','');
				set_button_status(1, permission, 'fnc_yarn_order_entry',1);	
			}
			release_freezing();
			//reset_form('yarnWorkOrder_1','details_container','','','','cbo_item_category*cbo_currency');
		}
	}

	function openmypage_wo()
	{
		if( form_validation('cbo_company_name*cbo_item_category','Company Name*Item Category')==false )
		{
			return;
		}
		
		var company = $("#cbo_company_name").val();
		var itemCategory = $("#cbo_item_category").val();
		var garments_nature = $("#garments_nature").val();
		var page_link = 'requires/yarn_work_order_controller.php?action=wo_popup&company='+company+'&itemCategory='+itemCategory+'&garments_nature='+garments_nature;
		var title = "Order Search"; 
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=0,scrolling=0','../')
		emailwindow.onclose=function()
		{
			freeze_window(5);
			var theform=this.contentDoc.forms[0];
			var hidden_wo_number=this.contentDoc.getElementById("hidden_wo_number").value.split("_"); 
			
			$("#txt_wo_number").val(hidden_wo_number[0]);
			$("#update_id").val(hidden_wo_number[1]);
			//reset_form('yarnWorkOrder_1','details_container','','','','cbo_item_category*cbo_currency');
			get_php_form_data(hidden_wo_number[1], "populate_data_from_search_popup", "requires/yarn_work_order_controller" );
			show_list_view(hidden_wo_number[1],'show_dtls_listview_update','details_container','requires/yarn_work_order_controller','');
			set_button_status(1, permission, 'fnc_yarn_order_entry',1,1);		
			release_freezing();
			buyer_style(document.getElementById('cbo_wo_basis').value);
		}
	}
	
	function buyer_style(val)
	{
		//alert (val)
		if (val==2)
		{
			document.getElementById('txt_buyer_name').disabled=false;
			document.getElementById('txt_style').disabled=false;
		}
		else
		{
			document.getElementById('txt_buyer_name').value='';
			document.getElementById('txt_style').value='';
			document.getElementById('txt_buyer_name').disabled=true;
			document.getElementById('txt_style').disabled=true;
		}
	}

</script>	
<body onLoad="set_hotkey()">
<div align="center">
    <div style="width:1050px;">
        <?php echo load_freeze_divs ("../../",$permission);  ?><br />
    </div>
		<fieldset style="width:1050px">
			<form name="yarnWorkOrder_1" id="yarnWorkOrder_1" method="" >
				<table cellpadding="0" cellspacing="2" width="900">
					<tr>
					  <td>&nbsp;<input type="hidden" name="is_approved" id="is_approved" value=""></td>
					  <td>&nbsp;<input type="hidden" name="update_id" id="update_id" value=""></td>
					  <td>WO Number</td>
					  <td><input type="text" name="txt_wo_number"  id="txt_wo_number" class="text_boxes" style="width:159px" placeholder="Double Click to Search" onDblClick="openmypage_wo('x','WO Number Search');" readonly />
                                          </td>
					  <td>&nbsp;</td>
					  <td>&nbsp;</td>
				  </tr>
					<tr>
						<td width="100" class="must_entry_caption">Company</td>
						<td width="170">
                        	<?php
							   	echo create_drop_down( "cbo_company_name", 170, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select --", 0, "load_drop_down( 'requires/yarn_work_order_controller', this.value, 'load_drop_down_supplier', 'supplier_td' );" );
 							?>
						</td>
                        <td width="130" class="must_entry_caption">Item Category</td>
						<td width="170"> 
                        	<?php
							   	echo create_drop_down( "cbo_item_category", 170, $item_category,"", 1, "-- Select --", 1, "",1 );
 							?> 
                        </td>
						<td width="130" class="must_entry_caption">Supplier</td>
						<td width="170" id="supplier_td"> 
						  	<?php
							   	echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET(2,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
 							?>
						</td>
					</tr>
					<tr>
						<td width="" class="must_entry_caption">WO Date</td>
						<td width="">
							<input type="text" name="txt_wo_date" id="txt_wo_date" class="datepicker" style="width:159px"/>
 						</td>
						<td width="" class="must_entry_caption">Currency</td>
						<td width="">
                         	<?php
							   	echo create_drop_down( "cbo_currency", 170, $currency,"", 1, "-- Select --", 2, "",0 );
 							?>
                        </td>
                        <td width="" class="must_entry_caption">WO Basis</td>
						<td width="">
                        	<?php
 								echo create_drop_down( "cbo_wo_basis", 170, $wo_basis,"", 1, "-- Select --", 0, "fn_disable_enable(this.value);load_drop_down( 'requires/yarn_work_order_controller', this.value, 'load_details_container', 'details_container' );buyer_style(this.value);",0,'','','','1' );
 							?>
                        </td>
					</tr>
					<tr>
                    	<td width="" >Buyer PO</td>
						<td width=""><input type="text" name="txt_buyer_po_no"  id="txt_buyer_po_no" class="text_boxes" style="width:159px" placeholder="Double Click To Search" onDblClick="openmypage()" readonly disabled />
                          <input type="hidden" name="txt_buyer_po"  id="txt_buyer_po" readonly disabled />
                          <input type="hidden" name="txt_job_selected"  id="txt_job_selected" readonly disabled />
                          <!-- when update and decrease row -->
                        <input type="hidden" name="txt_delete_row"  id="txt_delete_row"/></td>
						<td width="" class="must_entry_caption">Pay Mode</td>
						<td width=""><?php
							   	echo create_drop_down( "cbo_pay_mode", 170, $pay_mode,"", 1, "-- Select --", 0, "",0 );
 							?></td> 
						<td width="" class="must_entry_caption">Source</td>
						<td width=""><?php
							   	echo create_drop_down( "cbo_source", 170, $source,"", 1, "-- Select --", 0, "",0 );
 							?></td>
					</tr>
					<tr>
						<td width=""><span class="must_entry_caption">Delivery Date</span></td>
						<td width=""><input type="text" name="txt_delivery_date"  id="txt_delivery_date" class="datepicker"  style="width:159px" /></td>
						<td width="">Attention</td>
						<td width=""><input type="text" name="txt_attention"  id="txt_attention" style="width:159px " class="text_boxes" /></td>
						<td width="">Buyer Name</td>
						<td width=""><input type="text" name="txt_buyer_name"  id="txt_buyer_name" style="width:159px " class="text_boxes" disabled /></td>
					</tr>
                    <tr>
						<td width="">Style</td>
						<td width=""><!-- END -->
                        <input type="text" name="txt_style"  id="txt_style" style="width:159px " class="text_boxes" disabled /></td>
                        <td>D/O No.</td>
                        <td><input type="text" name="txt_do_no"  id="txt_do_no" style="width:159px " class="text_boxes" /></td>
                        <td>Remarks</td>
                        <td><input type="text" name="txt_remarks"  id="txt_remarks" style="width:159px " class="text_boxes" /></td>
                    </tr>
					<tr>
						<td align="center" height="10" colspan="6">
                        	<input type="button" id="set_button" class="image_uploader" style="width:100px; margin-left:30px; margin-top:2px;" value="Terms Condition" onClick="open_terms_condition_popup('requires/yarn_work_order_controller.php?action=terms_condition_popup','Terms Condition')" />
                        </td>
					</tr>
                </table>
                <br />
                <div style="width:1050px" id="details_container" align="left">	</div>
				<table cellpadding="0" cellspacing="2" width="100%">
                	<tr>
				  		<td align="center" colspan="6" valign="middle" class="button_container"><div id="approved" style="float:left; font-size:24px; color:#FF0000;"></div>
							<?php
								echo load_submit_buttons( $permission, "fnc_yarn_order_entry", 0,1 ,"reset_form('yarnWorkOrder_1','approved*details_container','','','','cbo_item_category*cbo_currency');$('#cbo_company_name').attr('disabled',false);$('#cbo_wo_basis').attr('disabled',false);",1); 
							?>
						</td>				
					</tr>
				</table> 
			</form>
		</fieldset>
    </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>