-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_yarn_count`
--

DROP TABLE IF EXISTS `lib_yarn_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_yarn_count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yarn_count` varchar(15) DEFAULT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'employee code will be add here',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0' COMMENT 'employee code will be add here',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `yarn_count` (`yarn_count`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_yarn_count`
--

LOCK TABLES `lib_yarn_count` WRITE;
/*!40000 ALTER TABLE `lib_yarn_count` DISABLE KEYS */;
INSERT INTO `lib_yarn_count` VALUES (1,'30/1',2,'2015-01-03 23:30:59',0,NULL,1,0),(2,'20D',2,'2015-01-04 04:14:57',0,NULL,1,0),(3,'16/1',2,'2015-01-10 04:43:23',0,NULL,1,0),(4,'20/1',2,'2015-01-10 04:43:32',0,NULL,1,0),(5,'22/1',2,'2015-01-10 04:44:28',0,NULL,1,0),(6,'24/1',2,'2015-01-10 04:44:35',0,NULL,1,0),(7,'26/1',2,'2015-01-10 04:44:42',0,NULL,1,0),(8,'10/1',2,'2015-01-10 04:44:49',0,NULL,1,0),(9,'28/1',2,'2015-01-10 04:44:59',0,NULL,1,0),(10,'32/1',2,'2015-01-10 04:45:54',0,NULL,1,0),(11,'34/1',2,'2015-01-10 04:46:12',0,NULL,1,0),(12,'36/1',2,'2015-01-10 04:46:22',0,NULL,1,0),(13,'40/1',2,'2015-01-10 04:48:05',0,NULL,1,0),(14,'70/D',2,'2015-01-10 04:48:40',0,NULL,1,0),(15,'45/1',2,'2015-01-10 04:48:47',1,'2015-01-29 04:00:12',1,0),(16,'30/D',2,'2015-01-10 04:48:54',0,NULL,1,0),(17,'40/D',2,'2015-01-10 04:49:00',0,NULL,1,0),(18,'150/D',2,'2015-02-03 00:34:43',2,'2015-08-31 15:08:11',1,0),(19,'30/1 Duble',2,'2015-02-09 15:48:35',2,'2015-02-09 16:03:30',1,0);
/*!40000 ALTER TABLE `lib_yarn_count` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
