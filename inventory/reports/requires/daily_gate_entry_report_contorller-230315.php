﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if ($action=="load_drop_down_supplier")
{	
  echo create_drop_down( "cbo_supplier", 170, "select a.id,a.supplier_name from lib_supplier a,lib_supplier_tag_company b where a.id=b.supplier_id 	 and a.tag_company=$data order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  

  
	/*echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET($data,tag_company) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 ); */ 	 
	exit();
}

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
$sample_arr=return_library_array(" select id,sample_name from lib_sample where status_active=1 order by sample_name",'id','sample_name');
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
//style search------------------------------//
if($action=="chalan_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	//echo $style_id;die;

	?>
        
<script>
	function js_set_value(str)
	{
	
		var splitData = str.split("_");	  	 
		$("#hidden_chalan_id").val(splitData[0]); 
		$("#hidden_chalan_no").val(splitData[1]); 
		$("#hidden_search_number").val($("#cbo_search_by").val()); 
	
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th id="search_by_td_up" width="200">Enter Booking No</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php 
						$search_arr=array(1=>"Chalan No",2=>"System ID"); 
						$dd="change_search_event(this.value, '0*0*0*0*0', '0*0*0*0*0', '../../') ";
                            echo create_drop_down( "cbo_search_by", 170, $search_arr,"",1, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                     <td align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>   
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $category; ?>, 'create_chalan_search_list_view', 'search_div', 'daily_gate_entry_report_contorller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_chalan_id" value="" />
                    <input type="hidden" id="hidden_chalan_no" value="" />
                    <input type="hidden" id="hidden_search_number" value="" />
                  
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_chalan_search_list_view")
{
	
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
	$item_category= $ex_data[5];
 	
	$sql_cond="";
	
	if(trim($txt_search_by)==0) { echo "please select Search By";die;}
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for challan
		{
			$sql_cond .= " and challan_no LIKE '%$txt_search_common%'";	
		}
		else if(trim($txt_search_by)==2) // for System ID
		{
			$sql_cond .= " and sys_number LIKE '%$txt_search_common%'";				
		}
 	}
	
	 if(trim($item_category)!=0) { $sql_cond .= " and item_category=$item_category"; }
 		if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and receive_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			if(trim($company)!="") $sql_cond .= " and company_name='$company'";
	
	
	if($txt_search_by==1 )
	{
 		$sql = "select id ,challan_no as chalan,supplier_name,receive_date,sample 	
				from  inv_gate_in_mst
				where status_active=1 $sql_cond";//a.supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
	}
   
   	if($txt_search_by==2)
		{
			$sql = "select id ,sys_number as chalan,supplier_name,receive_date,sample 	
					from  inv_gate_in_mst
					where  status_active=1 $sql_cond";//a.supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
		}
	//echo $sql;die;//"select id,sample_name from lib_sample where status_active=1 order by sample_name","id,sample_name"
	$result = sql_select($sql);
 
	$sample_arr=return_library_array( "select id,sample_name from lib_sample where status_active=1 order by sample_name",'id','sample_name');
	$arr=array(2=>$supplier_arr,3=>$sample_arr);
	echo  create_list_view("list_view", "Challan No/System ID, Receive Date, Supplier, Sample","150,100,120,150","600","400",0, $sql , "js_set_value", "id,chalan", "", 1, "0,0,supplier_name,sample", $arr, "chalan,receive_date,supplier_name,sample", "",'','0,0,0,0,0,0') ;		
	exit();	
	
}





//order search------------------------------//
if($action=="pi_search")
{
	
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
<script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );					
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = '';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
		}
    </script>
<?php
	extract($_REQUEST);
	
	if($company==0) $company_name=""; else $company_name=" and company_name=$company";
	if($category==0) $item_cate=""; else $item_cate="and item_category=$category";
	
	$sql ="select id,pi_wo_req_number,supplier_name,receive_date	
					from  inv_gate_in_mst
					where  status_active=1 $company_name $item_cate "; 
	$arr=array(1=>$supplier_arr);
	echo create_list_view("list_view", "PI/WO/REQ,Supplier,Receive Date","150,150,100,","450","310",0, $sql , "js_set_value", "id,pi_wo_req_number", "", 1, "0,supplier_name,0", $arr, "pi_wo_req_number,supplier_name,receive_date", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	exit();
}

//report generated here--------------------//
if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_item_cat=str_replace("'","",$cbo_item_cat);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_supplier_name=str_replace("'","",$cbo_supplier_name);
	$txt_pi_no=str_replace("'","",$txt_pi_no);
	$txt_challan=str_replace("'","",$txt_challan);
	$txt_search_item=str_replace("'","",$txt_search_item);
	$hidden_pi_id=str_replace("'","",$hidden_pi_id);
	
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	
	if($cbo_item_cat!=0) $sql_condition=" and a.item_category=$cbo_item_cat"; else $sql_condition.="";
    if($cbo_company_name!=0) $sql_condition.=" and a.company_name=$cbo_company_name"; else $sql_condition.="";
	if(str_replace("'","",$txt_search_item)==1)
	  {
		if(str_replace("'","",$txt_challan)!="")  $sql_condition.=" and a.challan_no='".str_replace("'","",$txt_challan)."'"; else $sql_condition.="";
	  }
	if(str_replace("'","",$txt_search_item)==2)
	  {
		if(str_replace("'","",$txt_challan)!="")  $sql_condition.=" and a.sys_number='".str_replace("'","",$txt_challan)."'"; else $sql_condition.="";
	  }
    if($hidden_pi_id!="") $sql_condition.=" and a.id in ($hidden_pi_id)"; else $sql_condition.="";
    if($hidden_pi_id==""  && $txt_pi_no!="") $sql_condition.=" and a.pi_wo_req_number='$txt_pi_no'"; else $sql_condition.="";
	if($cbo_supplier_name!=0) $sql_condition.=" and a.supplier_name=$cbo_supplier_name"; else $sql_condition.="";
	
	
	if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $sql_condition.=" and a.receive_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $sql_condition.=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $sql_condition="and a.receive_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $sql_condition="";
		} 
	
	
	
	
	
	//echo $sql_condition;die;
		$sql="select 
					a.id,
					a.sys_number,
					a.pi_wo_req_number,
					a.receive_date,
					a.challan_no,
					a.get_pass_id,
					a.sample,
					a.item_category,
					a.supplier_name,
					b.item_description,
					b.quantity,
					b.uom,
					b.remarks,
					a.time_hour,
					a.time_minute
				from
					 inv_gate_in_mst a,inv_gate_in_dtls b
				where
					a.id=b.mst_id   and a.status_active=1 and a.is_deleted=0 $sql_condition";
		   // echo $sql;die;	
	       $data_detail=sql_select($sql);
		  // print_r($data_detail);
			ob_start();	
			?>
			
			<div style="width:1440px"> 
        <fieldset style="width:1440px;">
				<table width="1410" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="17" align="center" style="border:none;font-size:16px; font-weight:bold" >Daily Gate Entry Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="17" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				<table width="1410" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="100" >Receive date</th>
							<th width="125" >System ID</th>
							<th width="125" >PI/WO/REQ</th>
							<th width="100">Challan No</th>
							<th width="80">Gate Pass No</th>
							<th width="100">Sample Type</th>
							<th width="100" >Item Category</th>
							<th width="120" >Supplier</th>
                            <th width="140">Description</th>
							<th width="100">Receive Qty</th>
							<th width="80">UOM</th>
							<th width="80">In time</th>
                            <th >Remarks</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1438px; overflow-y: scroll; max-height:400px;" id="scroll_body">
				<table width="1410" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
					<tbody>
					<?php
					$i=1;$total_receive=0;
					if(count($data_detail)!="")
					{
					foreach($data_detail as $value)
					{
						
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";

							?>
							<tr  bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo $value[csf("receive_date")]; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p><?php  echo $value[csf("sys_number")]; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p><?php echo $value[csf("pi_wo_req_number")]; ?>&nbsp;</p></td>
								<td width="100" align="center"><p><?php echo $value[csf("challan_no")]; ?>&nbsp;</p></td>
                                <td width="80" align="center"><p><?php echo $value[csf("get_pass_id")]; ?>&nbsp;</p></td>
                                <td width="100" align="center"><p><?php echo $sample_arr[$value[csf("sample")]]; ?>&nbsp;</p></td>
								<td width="100" align="center"><p><?php echo $item_category[$value[csf("item_category")]]; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p><?php echo $supplier_arr[$value[csf("supplier_name")]]; ?>&nbsp;</p></td>
								<td width="140" align="center"><p><?php echo $value[csf("item_description")]; ?>&nbsp;</p></td>
								<td width="100"  align="right"><p><?php $total_receive+=$value[csf("quantity")]; echo $value[csf("quantity")]; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><?php echo $unit_of_measurement[$value[csf("uom")]]; ?>&nbsp;</p></td>
								<td width="80" align="center"><p>
								<?php
								    if($value[csf("time_hour")]==24)
									   { 
										  $hour=$value[csf("time_hour")]-12;
										  $am="AM";
									   }
								  else  if($value[csf("time_hour")]==12)
									   { 
										  $hour=$value[csf("time_hour")];
										  $am="PM";
									   }
								  else  if($value[csf("time_hour")]>12 && $value[csf("time_hour")]<24)
									   { 
										  $hour=$value[csf("time_hour")]-12;
										  $am="PM";
									   } 
								   else
								       {
									    $hour=$value[csf("time_hour")];
										$am="AM";
									   }
								// echo $value[csf("time_hour")];
								 echo $hour.":".$value[csf("time_minute")]." ".$am ; ?>&nbsp;</p></td>
								
								<td ><p><?php echo $value[csf("remarks")]; ?></p></td>
							</tr>
							<?php
							$i++;
						}
					
					?>   
					</tbody>
                    <tfoot>
                    	<th colspan="10" width="890" >Total:</th>
                        <th width="100" id="value_total_receive"><?php echo number_format($total_receive,0); ?></th>
                        <th width="80" id=""></th>
                        <th width="80" id=""></th>
                        <th width="110"></th>
                    </tfoot>
                    <?php
					
					}
					
					?>
				</table>
			 </div>
			<br /><br />
            <table width="1410" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="17" align="center" style="border:none;font-size:16px; font-weight:bold" >Daily Gate Out Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="17" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				<table width="1410" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_2" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="100" >Out date</th>
							<th width="125" >Gate Out ID</th>
							<th width="120" >Challan No</th>
							<th width="100">Gate Pass No</th>
							<th width="80">Sample Type</th>
							<th width="100">Item Category</th>
							<th width="105" >Sent By</th>
							<th width="120" >Sent to</th>
                            <th width="140">Description</th>
							<th width="100">Quantity</th>
							<th width="80">UOM</th>
							<th width="80">Out time</th>
                            <th >Remarks</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1438px; overflow-y: scroll; max-height:500px;" id="scroll_body">
				<table width="1410" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body_1" align="left">
					<tbody>
					<?php
		if($cbo_item_cat!=0) $out_cond=" and a.item_category_id=$cbo_item_cat"; else $out_cond.="";			
	    if($cbo_company_name!=0) $out_cond.=" and a.company_id=$cbo_company_name"; else $out_cond.="";
	    if(str_replace("'","",$txt_search_item)==1)
	 		 {
				if(str_replace("'","",$txt_challan)!="")  $out_cond.=" and a.challan_no='".str_replace("'","",$txt_challan)."'"; else $out_cond.="";
	 		 }
		if(str_replace("'","",$txt_search_item)==2)
		 	 {
				if(str_replace("'","",$txt_challan)!="")  $out_cond.=" and a.sys_number='".str_replace("'","",$txt_challan)."'"; else $out_cond.="";
		 	 }
		
		if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $out_cond.=" and a.out_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $out_cond.=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $out_cond="and a.out_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $out_cond="";
		} 
		
		
		/*
		if($txt_date_from!="" && $txt_date_to!="") $out_cond.=" and a.out_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $out_cond.=""; */
					
					$sql_out="select 
							a.id,
							a.sys_number,
							a.sent_by,
							a.sent_to,
							a.out_date,
							a.challan_no,
							a.gate_pass_no,
							a.sample_id,
							a.item_category_id,
							b.item_description,
							b.quantity,
							b.uom,
							b.remarks,
							a.time_hour,
							a.time_minute
						from
							  inv_gate_out_mst a, inv_gate_out_dtls b
						where
							a.id=b.mst_id  and a.status_active=1 and a.is_deleted=0 $out_cond";
							//echo $sql_out;die;
					$get_out_data=sql_select($sql_out);		
					$i=1;$total_out=0;
					//count($get_out_data);
					if(count($get_out_data)!="")
					{
					foreach($get_out_data as $val)
					{
						
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";

							?>
							<tr  bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo $val[csf("out_date")]; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p><?php echo $val[csf("sys_number")]; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p><?php echo $val[csf("challan_no")]; ?>&nbsp;</p></td>
								<td width="100" align="center"><p><?php echo $val[csf("gate_pass_no")]; ?>&nbsp;</p></td>
                                <td width="80" align="center"><p><?php echo  $sample_arr[$val[csf("sample_id")]]; ?>&nbsp;</p></td>
                                <td width="100" align="center"><p><?php echo $item_category[$val[csf("item_category_id")]]; ?>&nbsp;</p></td>
								<td width="100" align="center"><p><?php echo $val[csf("sent_by")]; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p><?php echo $val[csf("sent_to")]; ?>&nbsp;</p></td>
								<td width="140" align="center"><p><?php echo $val[csf("item_description")]; ?>&nbsp;</p></td>
								<td width="100"  align="right"><p><?php $total_out+=$val[csf("quantity")]; echo $val[csf("quantity")]; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><?php echo $unit_of_measurement[$val[csf("uom")]]; ?>&nbsp;</p></td>
								<td width="80" align="center"><p>
								<?php
								    if($val[csf("time_hour")]==24)
									   { 
										  $hour=$val[csf("time_hour")]-12;
										  $am="AM";
									   }
								  else  if($val[csf("time_hour")]==12)
									   { 
										  $hour=$val[csf("time_hour")];
										  $am="PM";
									   }
								  else  if($val[csf("time_hour")]>12 && $val[csf("time_hour")]<24)
									   { 
										  $hour=$val[csf("time_hour")]-12;
										  $am="PM";
									   } 
								   else
								       {
									    $hour=$val[csf("time_hour")];
										$am="AM";
									   }
								// echo $val[csf("time_hour")];
								 echo $hour.":".$val[csf("time_minute")]." ".$am ; ?>&nbsp;</p></td>
								
								<td ><p><?php echo $val[csf("remarks")]; ?></p></td>
							</tr>
							<?php
							$i++;
						}
					
					?>   
					</tbody>
                    <tfoot>
                    	<th colspan="10" width="890" >Total:</th>
                        <th width="100" id="value_total_receive"><?php echo number_format($total_out,0); ?></th>
                        <th width="80" id=""></th>
                        <th width="80" id=""></th>
                        <th width="110"></th>
                    </tfoot>
                    <?php
					}
					?>
				</table>
			 </div>	
        </fieldset>
		</div>    
			<?php	 
	
	
	
	  foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();

          
				 
	
	
	
	/*
			
	foreach (glob("*.xls") as $filename) {
	if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc,ob_get_contents());
	echo "$html**$filename**$cbo_item_cat"; 
	exit();*/
	
}
disconnect($con);
?>

