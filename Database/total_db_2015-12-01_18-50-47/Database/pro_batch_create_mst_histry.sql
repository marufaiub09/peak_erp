-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_batch_create_mst_histry`
--

DROP TABLE IF EXISTS `pro_batch_create_mst_histry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_batch_create_mst_histry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approved_no` int(11) NOT NULL DEFAULT '0',
  `batch_id` int(11) NOT NULL DEFAULT '0',
  `batch_no` varchar(50) NOT NULL,
  `entry_form` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Batch Main Form; ',
  `batch_date` date NOT NULL,
  `batch_against` tinyint(1) NOT NULL DEFAULT '0',
  `batch_for` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `booking_no_id` int(11) NOT NULL DEFAULT '0',
  `booking_no` varchar(25) NOT NULL,
  `booking_without_order` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:yes; 0; No',
  `extention_no` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `batch_weight` double NOT NULL DEFAULT '0',
  `total_trims_weight` double NOT NULL DEFAULT '0',
  `color_range_id` int(11) NOT NULL DEFAULT '0',
  `process_id` varchar(100) NOT NULL COMMENT 'comma separated id',
  `organic` varchar(200) NOT NULL,
  `total_liquor` double NOT NULL DEFAULT '0',
  `re_dyeing_from` int(11) NOT NULL DEFAULT '0' COMMENT 'Batch Id',
  `dur_req_hr` int(11) NOT NULL DEFAULT '0',
  `dur_req_min` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(255) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `ready_to_approved` int(11) NOT NULL DEFAULT '0',
  `is_approved` int(11) NOT NULL DEFAULT '0',
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Yes 0;No',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Yes 0:No ',
  PRIMARY KEY (`id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_batch_create_mst_histry`
--

LOCK TABLES `pro_batch_create_mst_histry` WRITE;
/*!40000 ALTER TABLE `pro_batch_create_mst_histry` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_batch_create_mst_histry` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
