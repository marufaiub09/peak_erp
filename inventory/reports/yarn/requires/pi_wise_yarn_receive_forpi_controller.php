﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
$store_arr=return_library_array( "select id, store_name from lib_store_location", "id", "store_name"  );
$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );

if ($action=="load_drop_down_store")
{
	echo create_drop_down( "cbo_store_name", 150, "select a.id, a.store_name from lib_store_location a,lib_store_location_category b  where a.id=b.store_location_id and a.company_id='$data' and b.category_type in(1) and a.status_active=1 and a.is_deleted=0 order by a.store_name","id,store_name", 1, "-- Select Store --", 0, "" );
	exit();
	//select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id and a.status_active=1 and a.is_deleted=0 and a.company_id=$data[0] and b.category_type=$data[1] order by a.store_name
}

if($action=="pinumber_popup")
{
  	echo load_html_head_contents("PI Number Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
function js_set_value(str)
{
	var splitData = str.split("_");		 
	$("#pi_id").val(splitData[0]); 
	$("#pi_no").val(splitData[1]); 
	parent.emailwindow.hide();
}
	
	
</script>

</head>

<body>
<div align="center" style="width:100%; margin-top:5px" >
<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
	<table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th>Supplier</th>
                    <th id="search_by_td_up">Enter PI Number</th>
                    <th>Enter PI Date</th>
                    <th>
                    	<input type="reset" id="res" value="Reset" style="width:100px" class="formbutton" onClick="reset_form('searchlcfrm_1','search_div','','','','');" />
                        <input type="hidden" id="pi_id" value="" />
                        <input type="hidden" id="pi_no" value="" />
                    </th>           
                </tr>
            </thead>
            <tbody>
                <tr align="center">
                    <td>
                        <?php  
							echo create_drop_down( "cbo_supplier_id", 150,"select DISTINCT(c.id),c.supplier_name from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$companyID' and b.party_type=2",'id,supplier_name', 1, '-- All Supplier --',0,'',0);
                        ?>
                    </td>
                    <td align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 
                    <td align="center" id="search_by_td">				
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px;" placeholder="From Date" readonly />
                        To
                        <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px;" placeholder="To Date" readonly />
                    </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier_id').value+'_'+document.getElementById('txt_search_common').value+'_'+<?php echo $companyID; ?>+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value, 'create_pi_search_list_view', 'search_div', 'pi_wise_yarn_receive_forpi_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
           	 	</tr>
                <tr>
                	<td colspan="4" align="center"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </tbody>         
        </table>    
        <div align="center" style="margin-top:10px" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php

}

if($action=="create_pi_search_list_view")
{
	$ex_data = explode("_",$data);
	
	if($ex_data[0]==0) $cbo_supplier = "%%"; else $cbo_supplier = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$company = $ex_data[2];
	$from_date = $ex_data[3];
	$to_date = $ex_data[4];
	if( $pi_date!="" && $to_date!="")
	{
		if($db_type==0)
		{
			$pi_date_cond= " and pi_date between '".change_date_format($from_date,"yyyy-mm-dd")."' and '".change_date_format($to_date,"yyyy-mm-dd")."'";
		}
		else
		{
			$pi_date_cond= " and pi_date between '".change_date_format($from_date,'','',1)."' and '".change_date_format($to_date,'','',1)."'";	
		}
	}
	else $pi_date_cond=""; 
	//$sql= "select id, lc_number, supplier_id, importer_id, lc_date, last_shipment_date, lc_value from com_btb_lc_master_details where importer_id=$company and item_category_id=1 and supplier_id like '$cbo_supplier' and lc_number like '%".$txt_search_common."%' and is_deleted=0 and status_active=1";
	$sql= "select id, pi_number, supplier_id, importer_id, pi_date, last_shipment_date, total_amount from com_pi_master_details where importer_id=$company and item_category_id=1 and supplier_id like '$cbo_supplier' and pi_number like '%".$txt_search_common."%' and is_deleted=0 and status_active=1 $pi_date_cond";//die;
	
	$arr=array(1=>$company_arr,2=>$supplier_arr);
	echo create_list_view("list_view", "PI No, Importer, Supplier Name, PI Date, Last Shipment Date, PI Value","130,110,130,90,130","780","260",0, $sql , "js_set_value", "id,pi_number", "", 1, "0,importer_id,supplier_id,0,0,0,0", $arr, "pi_number,importer_id,supplier_id,pi_date,last_shipment_date,total_amount", "",'','0,0,0,3,3,2') ;	
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	//if(str_replace("'","",$cbo_store_name)!="") $store_cond=" and b.store_id in(".str_replace("'","",$cbo_store_name).")";
	$pi_no_cond=str_replace("'","",$txt_pi_no);
	$btbLc_id_str=str_replace("'","",$btbLc_id);
	if(str_replace("'","",$cbo_store_name)==0) $store="%%"; else $store=str_replace("'","",$cbo_store_name);
	if(str_replace("'","",$txt_pi_no)=='') $pi_cond=""; else $pi_cond="and b.pi_number='$pi_no_cond'";
	

	ob_start();
	?>
    <fieldset style="width:1120px">
    	<div style="width:100%; margin-left:10px;" align="left">
            <table width="1100" cellpadding="0" cellspacing="0" style="visibility:hidden; border:none" id="caption">
                <tr>
                   <td align="center" width="100%" colspan="11" style="font-size:16px"><strong><?php echo $report_title; ?></strong></td>
                </tr>
            </table>
           
            <br>
            <table width="1100" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">            	
                <thead>
                    <tr>
                        <th colspan="11">PI Details</th>
                    </tr>
                    <tr>
                        <th width="30">SL</th>
                        <th width="120">PI Number</th>
                        <th width="80">PI Date</th>
                        <th width="70">Count</th>
                        <th width="150">Composition</th>
                        <th width="80">Type</th>
                        <th width="90">Color</th>
                        <th width="110">Qnty</th>
                        <th width="90">Rate</th>                            
                        <th width="120">Value</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <?php 	
                    $i=1; $pi_id_all_array=array(); $pi_name_array=array(); $rate=0; $compos=''; $tot_pi_qnty=0; $tot_pi_amnt=0;
                    $sql="select b.id, b.pi_number, b.pi_date, b.remarks, c.color_id, c.count_name, c.yarn_composition_item1, c.yarn_composition_percentage1, c.yarn_composition_item2, c.yarn_composition_percentage2, c.yarn_type, sum(c.quantity) as qnty, sum(c.net_pi_amount) as amnt from com_pi_master_details b, com_pi_item_details c where b.id=c.pi_id and b.item_category_id=1 and b.importer_id=$cbo_company_name  and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $pi_cond  group by b.id, c.color_id, c.count_name, c.yarn_composition_item1, c.yarn_composition_percentage1, c.yarn_composition_item2, c.yarn_composition_percentage2, c.yarn_type,b.pi_number, b.pi_date, b.remarks";
                    $result=sql_select($sql);
                    foreach($result as $row)
                    {
                        if ($i%2==0)  
                            $bgcolor="#E9F3FF";
                        else
                            $bgcolor="#FFFFFF";
                        
                        $rate=$row[csf('amnt')]/$row[csf('qnty')];
                        
                        $compos=$composition[$row[csf('yarn_composition_item1')]]." ".$row[csf('yarn_composition_percentage1')]."%";
                        
                        if($row[csf('yarn_composition_percentage2')]>0)
                        {
                            $compos.=" ".$composition[$row[csf('yarn_composition_item2')]]." ".$row[csf('yarn_composition_percentage2')]."%";
                        }
                        
                        if(!in_array($row[csf('id')],$pi_id_all_array))
                        {
                            $pi_id_all_array[$row[csf('id')]]=$row[csf('id')];
							$pi_name_array[$row[csf('id')]]=$row[csf('pi_number')];
                        }
                        
                    ?>
                        <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                            <td width="30"><?php echo $i; ?></td>
                            <td width="120"><p><?php echo $row[csf('pi_number')]; ?></p></td>
                            <td width="80" align="center"><?php echo change_date_format($row[csf('pi_date')]); ?></td>
                            <td width="70" align="center"><p>&nbsp;<?php echo $count_arr[$row[csf('count_name')]]; ?></p></td>
                            <td width="150"><p><?php echo $compos; ?></p></td>
                            <td width="80"><p>&nbsp;<?php echo $yarn_type[$row[csf('yarn_type')]]; ?></p></td>
                            <td width="90"><p>&nbsp;<?php echo $color_arr[$row[csf('color_id')]]; ?></p></td>
                            <td width="110" align="right"><?php echo number_format($row[csf('qnty')],2,'.',''); ?>&nbsp;</td>
                            <td width="90" align="right"><?php echo number_format($rate,4,'.',''); ?>&nbsp;</td>
                            <td width="120" align="right"><?php echo number_format($row[csf('amnt')],2,'.',''); ?>&nbsp;</td>
                            <td><p><?php echo $row[csf('remarks')]; ?>&nbsp;</p></td>
                        </tr>
                    <?php
                    
                        $tot_pi_qnty+=$row[csf('qnty')]; 
                        $tot_pi_amnt+=$row[csf('amnt')];
                        
                        $i++;
                    }
                ?>
                <tfoot>
                    <th colspan="7" align="right">Total</th>
                    <th align="right"> <?php echo  number_format($tot_pi_qnty,2,'.','');?></th>
                    <th>&nbsp;</th>
                    <th align="right"> <?php echo  number_format($tot_pi_amnt,2,'.','');?></th>
                    <th>&nbsp;</th>
                </tfoot>
            </table>
            <br>
            <table width="1100" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">
                <thead>
                    <tr>
                      <th colspan="12">Yarn Receive</th>
                    </tr>
                    <tr>	
                        <th width="80">Recv. Date</th>
                        <th width="110">MRR No</th>
                        <th width="80">Challan No</th>
                        <th width="80">Lot No</th>
                        <th width="70">Count</th>            
                        <th width="130">Composition</th>
                        <th width="80">Type</th>
                        <th width="80">Color</th>
                        <th width="90">Qnty</th>
                        <th width="70">Rate</th>
                        <th width="100">Value</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                 <?php
                    //$pi_id_all=implode(",",array_flip($pi_id_all_array));
					$pi_id_all=implode(",",$pi_id_all_array); 
                    $compos=''; $tot_recv_qnty=0; $tot_recv_amnt=0; $recv_id_array=array(); $recv_pi_array=array(); $pi_data_array=array();
                    
					if ($pi_id_all!='' || $pi_id_all!=0)
					{
						$sql_recv="select a.id, a.recv_number, a.receive_date, a.challan_no, b.pi_wo_batch_no, (b.order_rate+b.order_ile_cost) as rate, b.order_qnty, b.order_amount, c.yarn_count_id, c.yarn_type, c.lot, c.color, c.yarn_comp_type1st, c.yarn_comp_percent1st, c.yarn_comp_type2nd, c.yarn_comp_percent2nd from inv_receive_master a, inv_transaction b, product_details_master c where a.item_category=1 and a.entry_form=1 and a.company_id=$cbo_company_name and a.receive_basis=1 and a.booking_id in($pi_id_all) and a.id=b.mst_id and b.item_category=1 and b.transaction_type=1 and b.prod_id=c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.store_id like '$store' order by a.id";
						
						$dataArray=sql_select($sql_recv);
					}
                    foreach($dataArray as $row_recv)
                    {
                        if ($i%2==0)  
                            $bgcolor="#E9F3FF";
                        else
                            $bgcolor="#FFFFFF";
                        
                        $compos=$composition[$row_recv[csf('yarn_comp_type1st')]]." ".$row_recv[csf('yarn_comp_percent1st')]."%";
                        
                        if($row_recv[csf('yarn_comp_percent2nd')]>0)
                        {
                            $compos.=" ".$composition[$row_recv[csf('yarn_comp_type2nd')]]." ".$row_recv[csf('yarn_comp_percent2nd')]."%";
                        }
                        
                        if(!in_array($row_recv[csf('id')],$recv_id_array))
                        {
                            $recv_id_array[$row_recv[csf('id')]]=$row_recv[csf('id')];
							$recv_pi_array[$row_recv[csf('id')]]=$row_recv[csf('pi_wo_batch_no')];
                        }
                        
                        $pi_data_array[$row_recv[csf('pi_wo_batch_no')]]['rcv']+=$row_recv[csf('order_amount')];
                    ?>
                        <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                            <td width="80" align="center"><?php echo change_date_format($row_recv[csf('receive_date')]); ?></td>
                            <td width="110"><p><?php echo $row_recv[csf('recv_number')]; ?></p></td>
                            <td width="80"><p>&nbsp;<?php echo $row_recv[csf('challan_no')]; ?></p></td>
                            <td width="80"><p>&nbsp;<?php echo $row_recv[csf('lot')]; ?></p></td>
                            <td width="70" align="center"><p>&nbsp;<?php echo $count_arr[$row_recv[csf('yarn_count_id')]]; ?></p></td>
                            <td width="130"><p><?php echo $compos; ?></p></td>
                            <td width="80"><p>&nbsp;<?php echo $yarn_type[$row_recv[csf('yarn_type')]]; ?></p></td>
                            <td width="80"><p>&nbsp;<?php echo $color_arr[$row_recv[csf('color')]]; ?></p></td>
                            <td width="90" align="right"><?php echo number_format($row_recv[csf('order_qnty')],2,'.',''); ?>&nbsp;</td>
                            <td width="70" align="right"><?php echo number_format($row_recv[csf('rate')],2,'.',''); ?>&nbsp;</td>
                            <td width="100" align="right"><?php echo number_format($row_recv[csf('order_amount')],2,'.',''); ?>&nbsp;</td>
                            <td><p>&nbsp;</p></td>
                        </tr>
                    <?php
                    
                        $tot_recv_qnty+=$row_recv[csf('order_qnty')]; 
                        $tot_recv_amnt+=$row_recv[csf('order_amount')];
                        
                        $i++;
                    }
                ?>
                <tfoot>
                    <th colspan="8" align="right">Total</th>
                    <th align="right"> <?php echo  number_format($tot_recv_qnty,2,'.','');?></th>
                    <th>&nbsp;</th>
                    <th align="right"> <?php echo  number_format($tot_recv_amnt,2,'.','');?></th>
                    <th>&nbsp;</th>
                </tfoot>
            </table>
            <br>
              <table width="1100" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">
                <thead>
                    <tr>
                      <th colspan="7">Yarn Return</th>
                    </tr>
                    <tr>	
                        <th width="100">Return Date</th>
                        <th width="130">Return No</th>
                        <th width="290">Item Description</th>
                        <th width="110">Qnty</th>
                        <th width="100">Rate</th>
                        <th width="120">Value</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                 <?php  
                    $tot_retn_qnty=0; $tot_retn_amnt=0;
                    if(count($recv_id_array)>0)
                    {
						$recv_id_all=implode(",",$recv_id_array);
						$sql_retn="select a.received_id, a.issue_number, a.issue_date, a.challan_no, (b.order_rate+b.order_ile_cost) as rate, b.cons_quantity, b.cons_amount, c.product_name_details from inv_issue_master a, inv_transaction b, product_details_master c where a.item_category=1 and a.entry_form=8 and a.company_id=$cbo_company_name and a.received_id in($recv_id_all) and a.id=b.mst_id and b.item_category=1 and b.transaction_type=3 and b.prod_id=c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0";
                        $dataRtArray=sql_select($sql_retn);
                        foreach($dataRtArray as $row_retn)
                        {
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
							
							$amnt=$row_retn[csf('cons_quantity')]*$row_retn[csf('rate')];
                        ?>
                            <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                                <td width="100" align="center"><?php echo change_date_format($row_retn[csf('issue_date')]); ?></td>
                                <td width="130"><p><?php echo $row_retn[csf('issue_number')]; ?></p></td>
                                <td width="290"><p>&nbsp;<?php echo $row_retn[csf('product_name_details')]; ?></p></td>
                                <td width="110" align="right"><?php echo number_format($row_retn[csf('cons_quantity')],2,'.',''); ?>&nbsp;</td>
                                <td width="100" align="right"><?php echo number_format($row_retn[csf('rate')],2,'.',''); ?>&nbsp;</td>
                                <td width="120" align="right"><?php echo number_format($amnt,2,'.',''); ?>&nbsp;</td>
                                <td><p>&nbsp;</p></td>
                            </tr>
                        <?php
                            $pi_id=$recv_pi_array[$row_retn[csf('received_id')]];
                            $tot_retn_qnty+=$row_retn[csf('cons_quantity')]; 
                            $tot_retn_amnt+=$amnt;
                            $pi_data_array[$pi_id]['rtn']+=$amnt;
                            
                            $i++;
                        }
                    }
					
					if ($pi_id_all!='' || $pi_id_all!=0)
					{
						$sql_retn="select a.received_id, a.pi_id, a.issue_number, a.issue_date, a.challan_no, (b.order_rate+b.order_ile_cost) as rate, b.cons_quantity, b.cons_amount, c.product_name_details from inv_issue_master a, inv_transaction b, product_details_master c where a.item_category=1 and a.entry_form=8 and a.company_id=$cbo_company_name and a.pi_id in($pi_id_all) and a.id=b.mst_id and b.item_category=1 and b.transaction_type=3 and b.prod_id=c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0";
						$dataRtArray=sql_select($sql_retn);
						foreach($dataRtArray as $row_retn)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF";
							
							$amnt=$row_retn[csf('cons_quantity')]*$row_retn[csf('rate')];
						?>
							<tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
								<td width="100" align="center"><?php echo change_date_format($row_retn[csf('issue_date')]); ?></td>
								<td width="130"><p><?php echo $row_retn[csf('issue_number')]; ?></p></td>
								<td width="290"><p>&nbsp;<?php echo $row_retn[csf('product_name_details')]; ?></p></td>
								<td width="110" align="right"><?php echo number_format($row_retn[csf('cons_quantity')],2,'.',''); ?>&nbsp;</td>
								<td width="100" align="right"><?php echo number_format($row_retn[csf('rate')],2,'.',''); ?>&nbsp;</td>
								<td width="120" align="right"><?php echo number_format($amnt,2,'.',''); ?>&nbsp;</td>
								<td><p>&nbsp;</p></td>
							</tr>
						<?php
							$pi_id=$row_retn[csf('pi_id')];
							$tot_retn_qnty+=$row_retn[csf('cons_quantity')]; 
							$tot_retn_amnt+=$amnt;
							$pi_data_array[$pi_id]['rtn']+=$amnt;
							
							$i++;
						}
					}
                    
                    $total_balance_qty = ($tot_pi_qnty+$tot_retn_qnty-$tot_recv_qnty); 
                    $total_balance_value = ($tot_pi_amnt+$tot_retn_amnt-$tot_recv_amnt);
                ?>
                <tfoot>
                    <tr>
                        <th colspan="3" align="right">Total</th>
                        <th align="right"><?php echo number_format($tot_retn_qnty,2,'.','');?></th>
                        <th>&nbsp;</th>
                        <th align="right"><?php echo number_format($tot_retn_amnt,2,'.','');?></th>
                        <th>&nbsp;</th>
                    </tr>
                    <tr>
                        <th colspan="3" align="right">Balance</th>
                        <th align="right"><?php echo number_format($total_balance_qty,2,'.','');?></th>
                        <th>&nbsp;</th>
                        <th align="right"><?php echo number_format($total_balance_value,2,'.','');?></th>
                        <th>&nbsp;</th>
                    </tr>
                </tfoot>
            </table>
            <br>
            <table width="850" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">
                <thead>
                    <th width="140">PI Number</th>
                    <th width="130">Receive Value</th>
                    <th width="130">Return Value</th>
                    <th width="130">Payable Value</th>
                    <th width="130">Acceptance Given</th>            
                    <th>Yet To Accept</th>
                </thead>
                <?php
					if ($pi_id_all!='' || $pi_id_all!=0)
					{
						$acceptance_arr=return_library_array( "select pi_id, sum(current_acceptance_value) as acceptance_value from com_import_invoice_dtls where pi_id in($pi_id_all) and is_lc=1 and status_active=1 and is_deleted=0 group by pi_id", "pi_id", "acceptance_value"  );
					}
                    
                    $payble_value=0; $tot_payble_value=0; $yet_to_accept=0; $tot_accept_value=0; $tot_yet_to_accept=0; $total_receive_value=0; $total_return_value=0;
                    foreach($pi_name_array as $key=>$value)
                    {
                        if ($i%2==0)  
                            $bgcolor="#E9F3FF";
                        else
                            $bgcolor="#FFFFFF";
                        
                        $payble_value=$pi_data_array[$key]['rcv']-$pi_data_array[$key]['rtn'];
                        $yet_to_accept=$payble_value-$acceptance_arr[$key];
                        
                        $total_receive_value+=$pi_data_array[$key]['rcv'];
                        $total_return_value+=$pi_data_array[$key]['rtn'];
                        
                        $tot_payble_value+=$payble_value;
                        $tot_accept_value+=$acceptance_arr[$key];
                        $tot_yet_to_accept+=$yet_to_accept;
                        
                    ?>
                        <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                            <td width="140"><p><?php echo $value; ?></p></td>
                            <td width="130" align="right"><?php echo number_format($pi_data_array[$key]['rcv'],2,'.',''); ?>&nbsp;</td>
                            <td width="130" align="right"><?php echo number_format($pi_data_array[$key]['rtn'],2,'.',''); ?>&nbsp;</td>
                            <td width="130" align="right"><?php echo number_format($payble_value,2,'.',''); ?>&nbsp;</td>
                            <td width="130" align="right"><?php echo number_format($acceptance_arr[$key],2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($yet_to_accept,2,'.',''); ?></td>
                        </tr>
                    <?php
                        $i++;
                    }
                ?>
                <tfoot>
                    <th align="right">Total</th>
                    <th align="right"><?php echo number_format($total_receive_value,2,'.',''); ?></th>
                    <th align="right"><?php echo number_format($total_return_value,2,'.',''); ?></th>
                    <th align="right"><?php echo number_format($tot_payble_value,2,'.',''); ?></th>
                    <th align="right"><?php echo number_format($tot_accept_value,2,'.',''); ?></th>
                    <th align="right"><?php echo number_format($tot_yet_to_accept,2,'.',''); ?></th>
                </tfoot>
            </table>
        	 <?php
				echo signature_table(3, $cbo_company_name, "1100px");
			 ?>
        </div>      
    </fieldset>      
	<?php
    foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename";
	exit();
}
?>
