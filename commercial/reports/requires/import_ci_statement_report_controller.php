<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$user_arr = return_library_array("select id,user_name from user_passwd ","id","user_name");
$issueBankrArr = return_library_array("select id,bank_name from lib_bank ","id","bank_name");
$supplierArr = return_library_array("select id,short_name from lib_supplier where status_active=1 and is_deleted=0","id","short_name");
$exp_lc_sc_id_arr = return_library_array("select import_mst_id,lc_sc_id from com_btb_export_lc_attachment","import_mst_id","lc_sc_id");
$is_lc_sc_arr = return_library_array("select import_mst_id,is_lc_sc from com_btb_export_lc_attachment","import_mst_id","is_lc_sc");
$file_reference_lc_arr = return_library_array("select f.import_mst_id,h.internal_file_no from com_btb_export_lc_attachment f,com_export_lc h where f.lc_sc_id=h.id ","import_mst_id","internal_file_no");
$file_reference_sales_arr = return_library_array("select f.import_mst_id,h.internal_file_no from com_btb_export_lc_attachment f,com_sales_contract h where f.lc_sc_id=h.id ","import_mst_id","internal_file_no");
$payment_date_arr = return_library_array("select invoice_id,max(payment_date) as payment_date from com_import_payment group by invoice_id","invoice_id","payment_date");
$paid_amount_arr = return_library_array("select invoice_id,sum(accepted_ammount) as accepted_ammount from com_import_payment where status_active=1 group by invoice_id","invoice_id","accepted_ammount");
$receive_qnty_arr = return_library_array("select pi_wo_batch_no,sum(cons_quantity) as cons_quantity from  inv_transaction where transaction_type=1 group by pi_wo_batch_no ","pi_wo_batch_no","cons_quantity");
$btb_lc_val_arr = return_library_array("select id,sum(lc_value) as lc_value from com_btb_lc_master_details where  status_active=1 and is_deleted=0 group by id","id","lc_value");
/*$receive_value_arr1 = return_library_array("select a.pi_wo_batch_no,sum(b.cons_amount) as cons_amount from inv_receive_master a, inv_transaction b where a.id=b.mst_id and a.item_category=1 group by a.pi_wo_batch_no ","pi_wo_batch_no","cons_amount");
$receive_value_arr1 = return_library_array("select a.pi_wo_batch_no,sum(b.cons_amount) as cons_amount from inv_receive_master a, inv_transaction b where a.id=b.mst_id and a.item_category=1 group by a.pi_wo_batch_no ","pi_wo_batch_no","cons_amount");
$receive_value_arr2 = return_library_array("select a.pi_wo_batch_no,sum(b.cons_amount) as cons_amount from inv_receive_master a, inv_transaction b where a.id=b.mst_id and a.item_category=2 group by a.pi_wo_batch_no ","pi_wo_batch_no","cons_amount");
$receive_value_arr4 = return_library_array("select a.pi_wo_batch_no,sum(b.cons_amount) as cons_amount from inv_receive_master a, inv_transaction b where a.id=b.mst_id and a.item_category=4 group by a.pi_wo_batch_no ","pi_wo_batch_no","cons_amount");
$receive_value_arr567 = return_library_array("select a.pi_wo_batch_no,sum(b.cons_amount) as cons_amount from inv_receive_master a, inv_transaction b where a.id=b.mst_id and a.item_category in(5,6,7) group by a.pi_wo_batch_no ","pi_wo_batch_no","cons_amount");
$receive_value_arr13 = return_library_array("select a.pi_wo_batch_no,sum(b.cons_amount) as cons_amount from inv_receive_master a, inv_transaction b where a.id=b.mst_id and a.item_category=13 group by a.pi_wo_batch_no ","pi_wo_batch_no","cons_amount");
*/


if($db_type==0)
{
	$group_concat="group_concat";
}
else
{
	$group_concat="wm_concat";
}



if ($action=="load_drop_down_supplier")
{	  
	//echo create_drop_down( "cbo_supplier_id", 100, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where FIND_IN_SET($data,a.tag_company) and a.id=b.supplier_id and a.status_active=1 and a.is_deleted=0 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select Supplier--", "", "" );  //and b.party_type in (1,6,7,8,90)	 
	
echo create_drop_down( "cbo_supplier_id", 100, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b, lib_supplier_tag_company c where a.id=b.supplier_id and a.id=c.supplier_id  and c.tag_company in($data) and a.status_active=1 and a.is_deleted=0 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	exit();
}

if ($action=="report_generate")
{
	extract($_REQUEST);
	//ob_start();
	//echo $cbo_company_id;
	?>
	<div id="" align="center" style="height:auto; width:2975px; margin:0 auto; padding:0;">
	<table width="2860px" align="center">
	<?php
	$company_library=sql_select("select id, company_name, plot_no, level_no,road_no,city from lib_company where id=".$cbo_company_id."");
	foreach( $company_library as $row)
	{
	?>
        <tr>
            <td colspan="36" align="center" style="font-size:22px"><center><strong><?php echo $row[csf('company_name')];?></strong></center></td>
        </tr>
	<?php
	}
	?>
        <tr>
            <td colspan="36" align="center" style="font-size:18px"><center><strong><u><?php echo $report_title; ?> Report</u></strong></center></td>
        </tr>
    </table>
    <div style="width:2968px;">
    <table  cellspacing="0" width="2950px"  border="1" rules="all" class="rpt_table" id="table_header_1" align="left">
       <thead>
                <th width="30">SL</th>
                <th width="100" align="center">Invoice No</th>
                <th width="75" align="center">Invoice Date</th>
                <th width="100" align="center">File Ref. No.</th>
                <th width="120" align="center">Issuing Bank</th>
                <th width="100" align="center">LC No</th>
                <th width="75" align="center">Lc Date</th>
                <th width="50" align="center"><p>Suppl.</p></th>
                <th width="50" align="center"><p>PI No & Date</p></th>
                <th width="100" align="center">Item Category</th>
                <th width="90" align="center">LCA No</th>
                <th width="50" align="center">Tenor</th>
                <th width="80" align="center">LC value</th>
                <th width="40" align="center"><p>Curr.</p></th>
                <th width="70" align="center">com. Accep. Date</th>
                <th width="70" align="center">Bank Accep. Date</th>
                <th width="70" align="center">Bank Ref</th>
                <th width="80" align="center">Bill Value</th>
                <th width="90" align="center">Paid Amount</th>
                <th width="80" align="center">Out Standing</th>
                <th width="70" align="center">Maturity Date</th>
                <th width="70" align="center">Month</th>
                <th width="70" align="center">Last Pay Date</th>
                <th width="70" align="center"><p>Shipment Date</p></th>
                <th width="70" align="center">Expiry Date</th>
                <th width="80" align="center">Lc Type</th>
                <th width="70" align="center">ETD</th>
                <th width="70" align="center">ETA</th>
                <th width="100" align="center">BL/Cargo No</th>
                <th width="70" align="center">Feeder Vassel</th>
                <th width="70" align="center">Mother Vassel</th>
                <th width="60" align="center"><p>Continer No</p></th>
                <th width="70" align="center">Pkg Qty</th>
                <th width="80" align="center">Bill Of Entry No</th>
                <th width="80" align="center">Total Qty</th>
                <th width="70" align="center">NN Doc Received Date</th>
                <th width="70" align="center">Doc Send to CNF</th>
                <th width="60" align="center">Goods in House Date</th>
                <th align="center" width="80">Received Value</th>
        </thead>
    </table>
    </div>
    <div style="width:2968px; overflow-y: scroll; max-height:300px;" id="scroll_body">
    <table cellspacing="0" width="2950px"  border="1" rules="all" class="rpt_table" id="tbl_body" >
	<?php	
		$cbo_company=str_replace("'","",$cbo_company_id);
		$cbo_issue=str_replace("'","",$cbo_issue_banking);
		$cbo_supplier=str_replace("'","",$cbo_supplier_id);
		$cbo_lc_type=str_replace("'","",$cbo_lc_type_id);
		$cbo_currency=str_replace("'","",$cbo_currency_id);
		$cbo_item_category=str_replace("'","",$cbo_item_category_id);
		$from_date=str_replace("'","",$txt_date_from);
		$to_date=str_replace("'","",$txt_date_to);
		$from_date_p=str_replace("'","",$txt_date_from_p);
		$to_date_p=str_replace("'","",$txt_date_to_p);
		$from_date_c=str_replace("'","",$txt_date_from_c);
		$to_date_c=str_replace("'","",$txt_date_to_c);
		$from_date_b=str_replace("'","",$txt_date_from_b);
		$to_date_b=str_replace("'","",$txt_date_to_b);
		$pending_type=str_replace("'","",$pending_type);
		$txt_pending_date=str_replace("'","",$txt_pending_date);
		
		//echo $pending_type;die;
		
		if ($cbo_company==0) $company_id =""; else $company_id =" and d.importer_id=$cbo_company ";
		if ($cbo_issue==0) $issue_banking =""; else $issue_banking =" and d.issuing_bank_id=$cbo_issue ";
		if ($cbo_supplier==0) $supplier_id =""; else $supplier_id =" and d.supplier_id=$cbo_supplier ";
		if ($cbo_lc_type==0) $type_id =""; else $type_id =" and d.lc_type_id=$cbo_lc_type ";
		if ($cbo_currency==0) $currency_id =""; else $currency_id =" and d.currency_id=$cbo_currency ";
		if ($cbo_item_category==0) $item_category_id =""; else $item_category_id =" and d.item_category_id=$cbo_item_category ";
		//print $company; $txt_pay_date=date("j-M-Y",strtotime($txt_pay_date));
		if($db_type==0) $txt_pending_date=change_date_format($txt_pending_date,"yyyy-mm-dd"); else if($db_type==2) $txt_pending_date=date("j-M-Y",strtotime($txt_pending_date));
		if($pending_type==0) $pending_cond="";
		if($pending_type==1) $pending_cond="";
		
		if($pending_type==2) { if($txt_pending_date!="") $pending_cond="and a.maturity_date>'$txt_pending_date'";}
		if($pending_type==3) { if($txt_pending_date!="")  $pending_cond="and a.maturity_date<='$txt_pending_date'";}
		//echo $pending_cond;die;

		if($db_type==2)
		{
			if( $from_date=="") $maturity_date=""; else $maturity_date= " and a.maturity_date between '".date("j-M-Y",strtotime($from_date))."' and '".date("j-M-Y",strtotime($to_date))."'";
			if( $from_date_c=="" ) $com_accep_date=""; else $com_accep_date= " and a.company_acc_date between '".date("j-M-Y",strtotime($from_date_c))."' and '".date("j-M-Y",strtotime($to_date_c))."'";
			if( $from_date_b=="" ) $bank_accep_date=""; else $bank_accep_date= " and a.bank_acc_date between '".date("j-M-Y",strtotime($from_date_b))."' and '".date("j-M-Y",strtotime($to_date_b))."'";
			
			if( $from_date_p=="" ) $payment_date=""; else $payment_date= " and e.payment_date between '".date("j-M-Y",strtotime($from_date_p))."' and '".date("j-M-Y",strtotime($to_date_p))."'";
			
			/*if($pending_type==0) $pending_cond="";
			if($pending_type==1) $pending_cond="";
			if($pending_type==2) { if($txt_pending_date!="") $pending_cond="and a.maturity_date<'$txt_pending_date'";}
			if($pending_type==3) { if($txt_pending_date!="")  $pending_cond="and a.maturity_date>='$txt_pending_date'";}*/
			
		}
		else if($db_type==0)
		{
			if( $from_date=="") $maturity_date=""; else $maturity_date= " and a.maturity_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'";
			if( $from_date_c=="" ) $com_accep_date=""; else $com_accep_date= " and a.company_acc_date between '".change_date_format($from_date_c,'yyyy-mm-dd')."' and '".change_date_format($to_date_c,'yyyy-mm-dd')."'";
			if( $from_date_b=="" ) $bank_accep_date=""; else $bank_accep_date= " and a.bank_acc_date between '".change_date_format($from_date_b,'yyyy-mm-dd')."' and '".change_date_format($to_date_b,'yyyy-mm-dd')."'";
			
			if( $from_date_p=="" ) $payment_date=""; else $payment_date= " and e.payment_date between '".change_date_format($from_date_p,'yyyy-mm-dd')."' and '".change_date_format($to_date_p,'yyyy-mm-dd')."'";
		}
		
		$i=1;
		if($db_type==0)
		{
			if( $payment_date=="")
			{
				$sql="Select a.id,  a.invoice_no, a.invoice_date,   a.company_acc_date ,  a.bank_acc_date,  a.bank_ref, a.shipment_date, a.eta_date, a.bill_no, a.feeder_vessel, a.mother_vessel, a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref,
			sum(b.current_acceptance_value) as current_acceptance_value, $group_concat(distinct b.import_invoice_id) as import_invoice_id, $group_concat(distinct b.pi_id) as pi_id,
			$group_concat(distinct c.pi_number) as pi_number, $group_concat(distinct c.pi_date) as pi_date,  $group_concat(distinct  c.id )as pi_mst_id,
			$group_concat(distinct d.id) as btb_lc_id, $group_concat(distinct d.lc_type_id), $group_concat(distinct d.issuing_bank_id) as issuing_bank_id, $group_concat(distinct d.lc_number) as lc_number, $group_concat(distinct d.lc_date) as lc_date, $group_concat(distinct d.supplier_id) as supplier_id, $group_concat(distinct d.lca_no) as lca_no,  $group_concat(distinct d.item_category_id) as item_category_id, $group_concat(distinct  d.tenor) as tenor, sum(d.lc_value) as lc_value, $group_concat(distinct   d.currency_id) as currency_id, $group_concat(distinct   d.lc_expiry_date) as lc_expiry_date, $group_concat(distinct   d.lc_type_id) as lc_type_id, $group_concat(distinct  d.etd_date) as etd_date
			from 
					com_import_invoice_mst a, com_import_invoice_dtls b, com_pi_master_details c, com_btb_lc_master_details d 
			where 
					a.id=b.import_invoice_id and b.btb_lc_id=d.id and b.pi_id = c.id  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $company_id $issue_banking $supplier_id $type_id $currency_id $item_category_id $maturity_date  $com_accep_date $bank_accep_date $pending_cond 
			GROUP BY 
					a.id ,  a.invoice_no, a.invoice_date,   a.company_acc_date ,  a.bank_acc_date,  a.bank_ref, a.shipment_date, a.eta_date, a.bill_no, a.feeder_vessel, a.mother_vessel, a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref
			ORDER BY a.id";
			}
			else
			{
				$sql="Select a.id,  a.invoice_no, a.invoice_date,   a.company_acc_date ,  a.bank_acc_date,  a.bank_ref, a.shipment_date, a.eta_date, a.bill_no, a.feeder_vessel, a.mother_vessel, a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref,
			sum(b.current_acceptance_value) as current_acceptance_value, $group_concat(distinct b.import_invoice_id) as import_invoice_id, $group_concat(distinct b.pi_id) as pi_id,
			$group_concat(distinct c.pi_number) as pi_number, $group_concat(distinct c.pi_date) as pi_date,  $group_concat(distinct  c.id )as pi_mst_id,
			$group_concat(distinct d.id) as btb_lc_id, $group_concat(distinct d.lc_type_id), $group_concat(distinct d.issuing_bank_id) as issuing_bank_id, $group_concat(distinct d.lc_number) as lc_number, $group_concat(distinct d.lc_date) as lc_date, $group_concat(distinct d.supplier_id) as supplier_id, $group_concat(distinct d.lca_no) as lca_no,  $group_concat(distinct d.item_category_id) as item_category_id, $group_concat(distinct  d.tenor) as tenor, sum(d.lc_value) as lc_value, $group_concat(distinct   d.currency_id) as currency_id, $group_concat(distinct   d.lc_expiry_date) as lc_expiry_date, $group_concat(distinct   d.lc_type_id) as lc_type_id, $group_concat(distinct  d.etd_date) as etd_date
			from 
					com_import_invoice_mst a, com_import_invoice_dtls b, com_pi_master_details c, com_btb_lc_master_details d,com_import_payment e 
			where 
					a.id=b.import_invoice_id and b.btb_lc_id=d.id and b.pi_id = c.id and a.id=e.invoice_id  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $company_id $issue_banking $supplier_id $type_id $currency_id $item_category_id $maturity_date  $com_accep_date $bank_accep_date $payment_date $pending_cond  
			GROUP BY 
					a.id ,  a.invoice_no, a.invoice_date,   a.company_acc_date ,  a.bank_acc_date,  a.bank_ref, a.shipment_date, a.eta_date, a.bill_no, a.feeder_vessel, a.mother_vessel, a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref
			ORDER BY a.id";
			}
		}
		else if($db_type==2)
		{
			if( $payment_date=="")
			{
				$sql="Select a.id,  a.invoice_no, a.invoice_date,   a.company_acc_date ,  a.bank_acc_date,  a.bank_ref, a.shipment_date, a.eta_date, a.bill_no, a.feeder_vessel, a.mother_vessel, a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref,
		sum(b.current_acceptance_value) as current_acceptance_value, b.import_invoice_id,
		LISTAGG(CAST( c.pi_number  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.pi_number) as pi_number, LISTAGG(CAST( c.pi_date AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.pi_date) as pi_date, LISTAGG(CAST(c.id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.id) as pi_mst_id,d.id  as btb_lc_id, d.lc_type_id ,d.issuing_bank_id,d.lc_number ,d.lc_date, d.supplier_id,d.lca_no,d.item_category_id ,d.tenor, sum(d.lc_value) as lc_value,d.currency_id, d.lc_expiry_date, d.lc_type_id, d.etd_date 
		from 
				com_import_invoice_mst a, com_import_invoice_dtls b, com_pi_master_details c, com_btb_lc_master_details d  
		where 
				a.id=b.import_invoice_id and b.btb_lc_id=d.id and b.pi_id = c.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $company_id $issue_banking $supplier_id $type_id $currency_id $item_category_id $maturity_date $com_accep_date $bank_accep_date $pending_cond
		GROUP BY 
				a.id,a.invoice_no, a.invoice_date,a.company_acc_date,a.bank_acc_date,a.bank_ref,a.shipment_date,a.eta_date,a.bill_no,a.feeder_vessel,a.mother_vessel,a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref,b.import_invoice_id, d.id, d.lc_type_id, d.issuing_bank_id, d.lc_number, d.lc_date, d.supplier_id ,d.lca_no, d.item_category_id, d.tenor, d.currency_id, d.lc_expiry_date, d.lc_type_id, d.etd_date
		ORDER BY a.id";
			}
			else
			{
				$sql="Select a.id,  a.invoice_no, a.invoice_date,   a.company_acc_date ,  a.bank_acc_date,  a.bank_ref, a.shipment_date, a.eta_date, a.bill_no, a.feeder_vessel, a.mother_vessel, a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref,
		sum(b.current_acceptance_value) as current_acceptance_value, b.import_invoice_id,
		LISTAGG(CAST( c.pi_number  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.pi_number) as pi_number, LISTAGG(CAST( c.pi_date AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.pi_date) as pi_date, LISTAGG(CAST(c.id  AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY c.id) as pi_mst_id,d.id  as btb_lc_id, d.lc_type_id ,d.issuing_bank_id,d.lc_number ,d.lc_date, d.supplier_id,d.lca_no,d.item_category_id ,d.tenor, sum(d.lc_value) as lc_value,d.currency_id, d.lc_expiry_date, d.etd_date
		from 
				com_import_invoice_mst a, com_import_invoice_dtls b, com_pi_master_details c, com_btb_lc_master_details d ,com_import_payment e
		where 
				a.id=b.import_invoice_id and b.btb_lc_id=d.id and b.pi_id = c.id and a.id=e.invoice_id  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 $company_id $issue_banking $supplier_id $type_id $currency_id $item_category_id $maturity_date  $com_accep_date $bank_accep_date  $payment_date $pending_cond
		GROUP BY 
				a.id,a.invoice_no, a.invoice_date,a.company_acc_date,a.bank_acc_date,a.bank_ref,a.shipment_date,a.eta_date,a.bill_no,a.feeder_vessel,a.mother_vessel,a.container_no, a.pkg_quantity, a.bill_of_entry_no, a.maturity_date, a.acceptance_time, a.copy_doc_receive_date, a.doc_to_cnf, a.bank_ref,b.import_invoice_id, d.id, d.lc_type_id, d.issuing_bank_id, d.lc_number, d.lc_date, d.supplier_id ,d.lca_no, d.item_category_id, d.tenor, d.currency_id, d.lc_expiry_date, d.lc_type_id, d.etd_date
		ORDER BY a.id";
			}
		}
		//echo $sql;die;
		$sql_data = sql_select($sql);
		foreach($sql_data as $row)
		{
			if($pending_type>0)
			{
				if($row[csf('current_acceptance_value')]>$paid_amount_arr[$row[csf('id')]])
				{
					$result_arr[$row[csf('id')]]['id']=$row[csf('id')];
					$result_arr[$row[csf('id')]]['invoice_no']=$row[csf('invoice_no')];
					$result_arr[$row[csf('id')]]['invoice_date']=$row[csf('invoice_date')];
					$result_arr[$row[csf('id')]]['company_acc_date']=$row[csf('company_acc_date')];
					$result_arr[$row[csf('id')]]['bank_acc_date']=$row[csf('bank_acc_date')];
					$result_arr[$row[csf('id')]]['bank_ref']=$row[csf('bank_ref')];
					$result_arr[$row[csf('id')]]['shipment_date']=$row[csf('shipment_date')];
					$result_arr[$row[csf('id')]]['eta_date']=$row[csf('eta_date')];
					$result_arr[$row[csf('id')]]['bill_no']=$row[csf('bill_no')];
					$result_arr[$row[csf('id')]]['feeder_vessel']=$row[csf('feeder_vessel')];
					$result_arr[$row[csf('id')]]['mother_vessel']=$row[csf('mother_vessel')];
					$result_arr[$row[csf('id')]]['container_no']=$row[csf('container_no')];
					$result_arr[$row[csf('id')]]['pkg_quantity']=$row[csf('pkg_quantity')];
					$result_arr[$row[csf('id')]]['bill_of_entry_no']=$row[csf('bill_of_entry_no')];
					$result_arr[$row[csf('id')]]['maturity_date']=$row[csf('maturity_date')];
					$result_arr[$row[csf('id')]]['acceptance_time']=$row[csf('acceptance_time')];
					$result_arr[$row[csf('id')]]['copy_doc_receive_date']=$row[csf('copy_doc_receive_date')];
					$result_arr[$row[csf('id')]]['doc_to_cnf']=$row[csf('doc_to_cnf')];
					$result_arr[$row[csf('id')]]['bank_ref']=$row[csf('bank_ref')];
					$result_arr[$row[csf('id')]]['current_acceptance_value']=$row[csf('current_acceptance_value')];
					$result_arr[$row[csf('id')]]['import_invoice_id']=$row[csf('import_invoice_id')];
					$result_arr[$row[csf('id')]]['pi_number']=$row[csf('pi_number')];
					$result_arr[$row[csf('id')]]['pi_date']=$row[csf('pi_date')];
					$result_arr[$row[csf('id')]]['pi_mst_id']=$row[csf('pi_mst_id')];
					$result_arr[$row[csf('id')]]['btb_lc_id']=$row[csf('btb_lc_id')];
					$result_arr[$row[csf('id')]]['lc_type_id']=$row[csf('lc_type_id')];
					$result_arr[$row[csf('id')]]['issuing_bank_id']=$row[csf('issuing_bank_id')];
					$result_arr[$row[csf('id')]]['lc_number']=$row[csf('lc_number')];
					$result_arr[$row[csf('id')]]['lc_date']=$row[csf('lc_date')];
					$result_arr[$row[csf('id')]]['supplier_id']=$row[csf('supplier_id')];
					$result_arr[$row[csf('id')]]['lca_no']=$row[csf('lca_no')];
					$result_arr[$row[csf('id')]]['item_category_id']=$row[csf('item_category_id')];
					$result_arr[$row[csf('id')]]['tenor']=$row[csf('tenor')];
					$result_arr[$row[csf('id')]]['lc_value']=$row[csf('lc_value')];
					$result_arr[$row[csf('id')]]['currency_id']=$row[csf('currency_id')];
					$result_arr[$row[csf('id')]]['lc_expiry_date']=$row[csf('lc_expiry_date')];
					$result_arr[$row[csf('id')]]['etd_date']=$row[csf('etd_date')];
				}
			}
			else
			{
				$result_arr[$row[csf('id')]]['id']=$row[csf('id')];
				$result_arr[$row[csf('id')]]['invoice_no']=$row[csf('invoice_no')];
				$result_arr[$row[csf('id')]]['invoice_date']=$row[csf('invoice_date')];
				$result_arr[$row[csf('id')]]['company_acc_date']=$row[csf('company_acc_date')];
				$result_arr[$row[csf('id')]]['bank_acc_date']=$row[csf('bank_acc_date')];
				$result_arr[$row[csf('id')]]['bank_ref']=$row[csf('bank_ref')];
				$result_arr[$row[csf('id')]]['shipment_date']=$row[csf('shipment_date')];
				$result_arr[$row[csf('id')]]['eta_date']=$row[csf('eta_date')];
				$result_arr[$row[csf('id')]]['bill_no']=$row[csf('bill_no')];
				$result_arr[$row[csf('id')]]['feeder_vessel']=$row[csf('feeder_vessel')];
				$result_arr[$row[csf('id')]]['mother_vessel']=$row[csf('mother_vessel')];
				$result_arr[$row[csf('id')]]['container_no']=$row[csf('container_no')];
				$result_arr[$row[csf('id')]]['pkg_quantity']=$row[csf('pkg_quantity')];
				$result_arr[$row[csf('id')]]['bill_of_entry_no']=$row[csf('bill_of_entry_no')];
				$result_arr[$row[csf('id')]]['maturity_date']=$row[csf('maturity_date')];
				$result_arr[$row[csf('id')]]['acceptance_time']=$row[csf('acceptance_time')];
				$result_arr[$row[csf('id')]]['copy_doc_receive_date']=$row[csf('copy_doc_receive_date')];
				$result_arr[$row[csf('id')]]['doc_to_cnf']=$row[csf('doc_to_cnf')];
				$result_arr[$row[csf('id')]]['bank_ref']=$row[csf('bank_ref')];
				$result_arr[$row[csf('id')]]['current_acceptance_value']=$row[csf('current_acceptance_value')];
				$result_arr[$row[csf('id')]]['import_invoice_id']=$row[csf('import_invoice_id')];
				$result_arr[$row[csf('id')]]['pi_number']=$row[csf('pi_number')];
				$result_arr[$row[csf('id')]]['pi_date']=$row[csf('pi_date')];
				$result_arr[$row[csf('id')]]['pi_mst_id']=$row[csf('pi_mst_id')];
				$result_arr[$row[csf('id')]]['btb_lc_id']=$row[csf('btb_lc_id')];
				$result_arr[$row[csf('id')]]['lc_type_id']=$row[csf('lc_type_id')];
				$result_arr[$row[csf('id')]]['issuing_bank_id']=$row[csf('issuing_bank_id')];
				$result_arr[$row[csf('id')]]['lc_number']=$row[csf('lc_number')];
				$result_arr[$row[csf('id')]]['lc_date']=$row[csf('lc_date')];
				$result_arr[$row[csf('id')]]['supplier_id']=$row[csf('supplier_id')];
				$result_arr[$row[csf('id')]]['lca_no']=$row[csf('lca_no')];
				$result_arr[$row[csf('id')]]['item_category_id']=$row[csf('item_category_id')];
				$result_arr[$row[csf('id')]]['tenor']=$row[csf('tenor')];
				$result_arr[$row[csf('id')]]['lc_value']=$row[csf('lc_value')];
				$result_arr[$row[csf('id')]]['currency_id']=$row[csf('currency_id')];
				$result_arr[$row[csf('id')]]['lc_expiry_date']=$row[csf('lc_expiry_date')];
				$result_arr[$row[csf('id')]]['etd_date']=$row[csf('etd_date')];
			}
		}
		
		$i=1;
		foreach( $result_arr as $row)
		{
			if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
			
			$import_invoice_id=$row[('import_invoice_id')];
			$suppl_id=$row[('supplier_id')];
			$item_id=$row[('item_category_id')];
			$curr_id=$row[('currency_id')];
			$pi_id=$row[('pi_mst_id')];
			
			?>
			<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
            	<td width="30"><p><?php echo $i; ?></p></td>
                <td width="100"><p><?php echo $row[('invoice_no')]; ?></p></td>
                <td width="75"><p><?php if($row[('invoice_date')]!="0000-00-00") echo change_date_format($row[('invoice_date')]); else echo "";?></p></td>
                <td width="100"><p><?php
						$exp_lc_sc_id = $exp_lc_sc_id_arr[$row[('btb_lc_id')]];
						$is_lc_sc = $is_lc_sc_arr[$row[('btb_lc_id')]];
						if($is_lc_sc==0)
							$file_reference_no=$file_reference_lc_arr[$row[('btb_lc_id')]];
						else 
							$file_reference_no=$file_reference_sales_arr[$row[('btb_lc_id')]];
				 	echo $file_reference_no; ?></p></td>
                <td width="120"><p><?php echo $issueBankrArr[$row[('issuing_bank_id')]]; ?></p></td>
                <td width="100"><p><?php echo $row[('lc_number')]; ?></p></td>
                
                <td width="75"><p><?php if($row[('lc_date')]!="0000-00-00") echo change_date_format($row[('lc_date')]); else echo ""; ?></p></td>
                <td width="50"><p><?php echo $supplierArr[$row[('supplier_id')]]; ?></p></td>
             	<td width="50" align="center"><p><?php echo "<a href='#report_details' style='color:#000' onclick= \"openmypage_pi_date('$import_invoice_id','$suppl_id','$item_id','$pi_id','$curr_id','pi_details','PI Details');\">"."View"."</a>";//$row[("pi_id")]; ?></p></td>
                <td width="100"><p><?php echo $item_category[$row[('item_category_id')]]; ?></p></td>
                <td width="90"><p><?php echo $row[('lca_no')]; ?></p></td>
                <td width="50"><p><?php echo $row[('tenor')]; ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($btb_lc_val_arr[$row[('btb_lc_id')]],2);//echo number_format($row[('lc_value')],2); //btb_lc_id?></p></td>
                <td width="40"><p><?php echo $currency[$row[('currency_id')]]; ?></p></td>
                <td width="70"><p><?php if($row[('company_acc_date')]!="0000-00-00") echo change_date_format($row[('company_acc_date')]); else echo ""; ?></p></td>
                <td width="70"><p><?php if($row[('bank_acc_date')]!="0000-00-00") echo change_date_format($row[('bank_acc_date')]); ?></p></td>
                <td width="70"><p><?php echo $row[('bank_ref')]; ?></p></td>
                <td width="80" align="right"><p><?php echo number_format($row[('current_acceptance_value')],2); ?></p></td>
                <td width="90" align="right"><p><a href="#report_details" onclick="paid_amount_dtls('<?php echo $row[('id')];?>','payment_details','Payment Details');"><?php echo number_format($paid_amount_arr[$row[('id')]],2); $gt_total_paid+=$paid_amount_arr[$row[('id')]]; ?></a></p></td>
                <td width="80" align="right"><p><?php $out_standing=$row[('current_acceptance_value')]-$paid_amount_arr[$row[('id')]]; echo number_format($out_standing,2); $total_out_standing +=$out_standing ?></p></td>
                <td width="70"><p><?php if($row[('maturity_date')]!="0000-00-00") echo change_date_format($row[('maturity_date')]); else echo ""; ?></p></td>
                <td width="70"><p>
				<?php
					if($row[('maturity_date')]=="00-00-0000")
					{
						$month="";
					}
					else
					{
						$month = date('F', strtotime($row[('maturity_date')]));
					}
					echo $month;
				 ?></p></td>
                <td width="70" ><p><?php if($payment_date_arr[$row[('id')]]!="") echo change_date_format($payment_date_arr[$row[('id')]]); else echo ""; ?></p></td>
                <td width="70"><p><?php if($row[('shipment_date')]!="0000-00-00") echo change_date_format($row[('shipment_date')]); else echo ""; ?></p></td>
                <td width="70"><p><?php if($row[('lc_expiry_date')]!="0000-00-00")  echo change_date_format($row[('lc_expiry_date')]); else echo ""; ?></p></td>
                <td width="80"><p><?php echo $lc_type[$row[('lc_type_id')]]; ?></p></td>
                <td width="70"><p><?php if($row[('etd_date')]!="0000-00-00") echo change_date_format($row[('etd_date')]); else echo ""; ?></p></td>
                <td width="70"><p><?php if($row[('eta_date')]!="0000-00-00") echo change_date_format($row[('eta_date')]); else echo ""; ?></p></td>
                <td width="100"><p><?php echo $row[('bill_no')]; ?></p></td>
                <td width="70"><p><?php echo $row[('feeder_vessel')]; ?></p></td>
                <td width="70"><p><?php echo $row[('mother_vessel')]; ?></p></td>
                <td width="60"><p><?php echo $row[('container_no')]; ?></p></td>
                <td width="70"><p><?php echo $row[('pkg_quantity')]; ?></p></td>
                <td width="80"><p><?php echo $row[('bill_of_entry_no')]; ?></p></td>
                <?php

               /*if($row[('item_category_id')]==1)//.................Yarn
				{
					$receive_value = $receive_value_arr1[$row[('pi_mst_id')]];
				}
				else if($row[('item_category_id')]==2)//......Knit(Finish) Fabric......
				{
					$receive_value = $receive_value_arr2[$row[('pi_mst_id')]];
				}
				else if($row[('item_category_id')]==4)//.....Accessories
				{
					//$receive_value = $receive_value_arr4[$row[('pi_mst_id')]];
				}
				else if($row[('item_category_id')]==5 || $row[('item_category_id')]==6 || $row[('item_category_id')]==7)//.....Chemicals,Dyes,Auxilary Chemicals
				{
					$receive_value =$receive_value_arr567[$row[('pi_mst_id')]];
				}
				else if($row[('item_category_id')]==13)//.....Grey Fabric
				{
					
					$receive_value = $receive_value_arr13[$row[('pi_mst_id')]];
				}*/
				
				$receive_qnty=0;
				$pi_id_all=explode(",",$row[('pi_mst_id')]);
				foreach($pi_id_all as $piid)
				{
					$receive_qnty += $receive_qnty_arr[$piid];
				}
				
					?>
                <td width="80" align="right"><p><?php echo number_format($receive_qnty,0,"",""); ?></p></td>
                <td width="70"><p><?php  if($row[('copy_doc_receive_date')]!="0000-00-00")  echo change_date_format($row[('copy_doc_receive_date')]); else echo ""; ?></p></td>
                <td width="70"><p><?php if($row[('doc_to_cnf')]!="0000-00-00") echo change_date_format($row[('doc_to_cnf')]); else echo ""; ?></p></td>
             	<td width="60" align="center"><p><?php echo "<a href='#report_details' style='color:#000' onclick= \"openmypage_inHouse_date('$pi_id','$receive_value','$receive_qnty','pi_rec_details','PI Details');\">"."View"."</a>"; //$row[("pi_id")]; ?></p></td>
					
				<td align="right" width="80"><p><?php echo number_format($receive_value,2); ?></p></td>
            </tr>
            <?php
			$tot_lc_value+=$row[('lc_value')];
			$tot_bill_value+=$row[('current_acceptance_value')];
			$total_qnty+=$receive_qnty;
			$total_receive+=$receive_value;
			$i++;
		}
		?>
        </table>
        <table cellspacing="0" width="2950px"  border="1" rules="all" class="rpt_table" id="report_table_footer" >
            <tfoot>
            	<th width="30">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="75">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="120">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="75">&nbsp;</th>
                <th width="50">&nbsp;</th>
             	<th width="50" align="center">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="90"><p>&nbsp;</th>
                <th width="50"><strong>Total : </strong></th>
                <th align="right" id="value_tot_lc_value" width="80"><strong><?php echo number_format($tot_lc_value,2); ?></strong></th> 
                <th width="40">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="80" align="right" id="value_tot_bill_value"><strong><?php echo number_format($tot_bill_value,2); ?></strong></th>
                <th width="90" align="right" id="value_gt_total_paid"><strong><?php echo number_format($gt_total_paid,2); ?></strong></th>
                <th width="80" align="right" id="value_total_out_standing"><strong><?php echo number_format($total_out_standing,2); ?></strong></th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="100">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="60">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="80">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
                <th width="70"></th>
                <th align="right" id="total_qnty" width="80"><strong><?php echo number_format($total_qnty,2); ?></strong></th>
                <th width="70">&nbsp;</th>
                <th width="70">&nbsp;</th>
             	<th width="60" align="center">&nbsp;</th>
				<th align="right" id="value_total_receive" width="80"><strong><?php echo number_format($total_receive,2,'.'); ?></strong></th>	
            </tfoot>        
	</table>
    <div align="left" style="font-weight:bold; margin-left:30px;"><?php echo "User Id : ". $user_arr[$user_id] ." , &nbsp; THIS IS SYSTEM GENERATED STATEMENT, NO SIGNATURE REQUIRED ."; ?></div>
    </div>
    </div>
<?php
}

if($action=="pi_details")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	//print_r ($import_invoice_id);

	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
?>	
<script>

	function print_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	
		d.close();
		document.getElementById('scroll_body').style.overflowY="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
	}
	
	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<div style="width:800px" align="center" id="scroll_body" >
<fieldset style="width:100%; margin-left:10px" >
<!--<input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>&nbsp;&nbsp;&nbsp;&nbsp;
--><input type="button" value="Close" onClick="window_close()" style="width:100px"  class="formbutton"/>
         <div id="report_container" align="center" style="width:100%" > 
             <div style="width:780px">
                <table cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="500" align="center">
                    <thead>
                    	<tr>
                            <th colspan="4" align="center"><?php echo $companyArr[$company_name]; ?></th>
                        </tr>
                        <tr>
                            <th width="150"><strong>Supplier</strong></th>
                            <th width="150"><strong>Item Category</strong></th>
                            <th width="150"><strong>Currency</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                        	<td><?php echo $supplierArr[$suppl_id]; ?></td>
                            <td><?php echo $item_category[$row[csf('item_category_id')]]; ?></td>
                            <td><?php echo $currency[$curr_id]; ?></td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <table class="rpt_table" border="1" rules="all" width="100%" cellpadding="0" cellspacing="0">
                	 <thead bgcolor="#dddddd">
                     	<tr>
                            <th width="30">SL</th>
                            <th width="70">PI No.</th>
                            <th width="70">PI Date</th>
                            <th width="100">Item Group</th>
                            <th width="130">Item Description</th>
                            <th width="80">Qnty</th>
                            <th width="70">Rate</th>
                            <th width="80">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
		$i=1;
		//if ($pi_id==0) $piId =""; else $piId =" and a.id in ($pi_id)";
		$sql="Select a.id, a.pi_number,a.pi_date, b.item_prod_id, b.determination_id, b.item_group, b.item_description, b.size_id, b.quantity, b.rate, b.amount from com_pi_master_details a, com_pi_item_details b where a.id=b.pi_id and a.id in ($pi_id) order by a.pi_number";
		//echo $sql;
		$result=sql_select($sql);
		
		$pi_arr=array();
		foreach( $result as $row)
		{
/*			if (!in_array($row[csf("pi_number")],$pi_arr) )
			{
				$pi_arr[]=$row[csf('pi_number')];
*/			?>
                   
<!--                        <tr>
                            <td colspan="6" align="left">PI No : <?php //echo $row[csf('pi_number')]; ?></td>
                        </tr>
-->                    	
					<?php
					$total_qnt+=$row[csf("quantity")];

					$total_amount+=$row[csf("amount")];
					
             // }
				if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
			?>
            <tr bgcolor="<?php echo $bgcolor ; ?>">
            	<td><?php echo $i; ?></td>
                <td><?php echo $row[csf("pi_number")]; ?></td>
                <td><?php echo change_date_format($row[csf("pi_date")]); ?></td>
                <td><?php echo $itemgroupArr[$row[csf("item_group")]]; ?></td>
                <td><?php echo $row[csf("item_description")]; ?></td>
                <td align="right"><?php echo $row[csf("quantity")]; ?></td>
                <td align="right"><?php echo $row[csf("rate")]; ?></td>
                <td align="right"><?php echo number_format($row[csf("amount")],2); ?></td>
            </tr>
          </tbody>
		<?php	
        $i++;
        } 
		   ?>
             <tfoot>
                <th colspan="5" align="right">Total : </th>
                <th align="right"><?php echo number_format($total_qnt,0); ?></th>
                <th>&nbsp;</th>
                <th align="right"><?php echo number_format($total_amount,2); ?></th>
            </tfoot>
        </table>
		</div>
        </div>
	</fieldset>
</div>
<?php
exit();	
}

if($action=="pi_rec_details")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	//print_r ($pi_id);

	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
?>	
<script>

	function print_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	
		d.close();
		document.getElementById('scroll_body').style.overflowY="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
	}
	
	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<div style="width:600px" align="center" id="scroll_body" >
<fieldset style="width:100%; margin-left:10px" >
<!--<input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>&nbsp;&nbsp;&nbsp;&nbsp;
--><input type="button" value="Close" onClick="window_close()" style="width:100px"  class="formbutton"/>
         <div id="report_container" align="center" style="width:100%" > 
             <div style="width:580px">
                <table class="rpt_table" border="1" rules="all" width="100%" cellpadding="0" cellspacing="0">
                	 <thead bgcolor="#dddddd">
                     	<tr>
                            <th width="30">PO No</th>
                            <th width="120">MRR No</th>
                            <th width="80">Received Date</th>
                             <th width="80">Received Qty</th>
                            <th width="80">Rate</th>
                            <th width="80">Received Value</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
		$i=1;
		//if ($pi_id==0) $piId =""; else $piId =" and a.id in ($pi_id)";
		$sql="Select a.id, a.recv_number, a.receive_date, $group_concat(distinct b.cons_rate) as cons_rate, sum(b.cons_quantity) as cons_quantity, sum(b.cons_amount) as cons_amount from inv_receive_master a,  inv_transaction b where a.id=b.mst_id  and b.pi_wo_batch_no in ($pi_id) and b.transaction_type=1 group by a.recv_number,a.id,a.receive_date order by a.id";
		//echo $sql;
		$result=sql_select($sql);
		
		$pi_arr=array();
		foreach( $result as $row)
		{
			$total_qnt+=$row[csf("cons_quantity")];
			$total_amount+=$row[csf("cons_amount")];
			
				if ($i%2==0)  
				$bgcolor="#E9F3FF";
			else
				$bgcolor="#FFFFFF";
			?>
            <tr bgcolor="<?php echo $bgcolor ; ?>">
            	<td><?php echo $pi_id; ?></td>
                <td><?php echo $row[csf("recv_number")]; ?></td>
                <td><?php echo change_date_format($row[csf("receive_date")]); ?></td>
                <td align="right"><?php echo $row[csf("cons_quantity")];//$receive_qnty; ?></td>
                <td align="right"><?php echo $row[csf("cons_rate")]; ?></td>
                <td align="right"><?php echo number_format($row[csf("cons_amount")],2);//number_format($receive_value,2); ?></td>
            </tr>
          </tbody>
		<?php	
        $i++;
        } 
		   ?>
         <tfoot>
            <th colspan="3" align="right">Total : </th>
            <th align="right"><?php echo number_format($total_qnt,0); ?></th>
            <th>&nbsp;</th>
            <th align="right"><?php echo number_format($total_amount,2); ?></th>
        </tfoot>
    </table>
    </div>
    </div>
	</fieldset>
</div>
<?php
exit();	
}

if($action=="payment_details")
{
	
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	//print_r ($pi_id);

	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
?>	
<script>

/*	function print_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	
		d.close();
		document.getElementById('scroll_body').style.overflowY="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
	}
*/	
	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<div style="width:600px" align="center" id="scroll_body" >
<fieldset style="width:100%; margin-left:10px" >
<!--<input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>&nbsp;&nbsp;&nbsp;&nbsp;
--><input type="button" value="Close" onClick="window_close()" style="width:100px"  class="formbutton"/>
    <div id="report_container" align="center" style="width:100%" > 
    <table class="rpt_table" border="1" rules="all" width="590" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th width="30">SL</th>
                <th width="120">Payment Head</th>
                <th width="120">Adj. source</th>
                 <th width="80">Pay Date</th>
                <th width="80">Conversion Rate</th>
                <th width="80">Accepted Amount</th>
                <th >Domistic Currency</th>
            </tr>
        </thead>
    <tbody>
		<?php
        $i=1;
        //if ($pi_id==0) $piId =""; else $piId =" and a.id in ($pi_id)";
        $sql="Select id, payment_head, payment_date, adj_source, conversion_rate, sum(accepted_ammount) as accepted_ammount,sum(domistic_currency) as domistic_currency from com_import_payment where invoice_id=$invoice_id and status_active=1 group by id, payment_head, adj_source, conversion_rate,payment_date";
        //echo $sql;
        $result=sql_select($sql);
        foreach( $result as $row)
        {
			$total_accept+=$row[csf("accepted_ammount")];
			$total_document+=$row[csf("domistic_currency")];
			
			if ($i%2==0)  
			$bgcolor="#E9F3FF";
			else
			$bgcolor="#FFFFFF";
			?>
			<tr bgcolor="<?php echo $bgcolor ; ?>">
				<td><?php echo $i; ?></td>
				<td><?php echo $commercial_head[$row[csf("payment_head")]]; ?></td>
				<td><?php echo $commercial_head[$row[csf("adj_source")]]; ?></td>
                <td align="center"><?php echo change_date_format($row[csf("payment_date")]); ?></td>
				<td align="right"><?php echo $row[csf("conversion_rate")];//$receive_qnty; ?></td>
				<td align="right"><?php echo number_format($row[csf("accepted_ammount")],2); ?></td>
				<td align="right"><?php echo number_format($row[csf("domistic_currency")],2);//number_format($receive_value,2); ?></td>
			</tr>
			<?php	
			$i++;
        } 
        ?>
    </tbody>
    <tfoot>
        <th colspan="5" align="right">Total : </th>
        <th align="right"><?php echo number_format($total_accept,2); ?></th>
        <th align="right"><?php echo number_format($total_document,2); ?></th>
    </tfoot>
    </table>
    </div>
	</fieldset>
</div>
<?php
exit();	
}
?>