<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_name=$_SESSION['logic_erp']['user_id'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$buyer_list=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_arr=return_library_array( "select id,short_name from lib_buyer", "id", "short_name"  );
$search_by_arr=array(0=>"--All--",1=>"Heat Setting",2=>"Slitting/Squeezing",3=>"Drying", 9=>"Stentering",4=>"Compacting",5=>"Special Finish",6=>"Wait For Slitting/Squeezing",10=>"Wait For Stentering",7=>"Wait For Drying",8=>"Wait For Compacting");//,9=>"Wait For Special Finish";
if($db_type==0) $group_concat="group_concat(c.po_number)"; 
else if($db_type==2) $group_concat="listagg(c.po_number,',' ) within group (order by c.po_number) AS po_number";

//--------------------------------------------------------------------------------------------------------------------
if($action=="check_color_id")
{	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
		?>
<script type="text/javascript">
  function js_set_value(id)
	  { //alert(id);
	document.getElementById('selected_id').value=id;
	parent.emailwindow.hide();
	  }

</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
	$sql="select id, color_name from lib_color where is_deleted=0 and status_active=1 order by id";
	$arr=array(1=>$color_library);
	echo  create_list_view("list_view", "ID,Color Name", "50,200","300","300",0, $sql, "js_set_value", "id,color_name", "", 1, "0,0", $arr , "id,color_name", "",'setFilterGrid("list_view",-1);','0') ;
exit();	
}
if($action=="batchnumbershow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php if($db_type==0) $field_grpby=" GROUP BY batch_no"; 
else if($db_type==2) $field_grpby="GROUP BY batch_no,id,batch_no,batch_for,booking_no,color_id,batch_weight";
$sql="select id,batch_no,batch_for,booking_no,color_id,batch_weight from pro_batch_create_mst where company_id=$company_name and is_deleted = 0 $field_grpby ";	
$arr=array(1=>$color_library);
	echo  create_list_view("list_view", "Batch no,Color,Booking no, Batch for,Batch weight ", "100,100,100,100,170","620","350",0, $sql, "js_set_value", "id,batch_no", "", 1, "0,color_id,0,0,0", $arr , "batch_no,color_id,booking_no,batch_for,batch_weight", "employee_info_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}//batchnumbershow;
if($action=="load_drop_down_location")
{ 
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
    echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90))  order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", 0, "" );
	exit();
}//cbo_buyer_name_td
if($action=="jobnumbershow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  {
		document.getElementById('selected_id').value=id;
		parent.emailwindow.hide();
	  }

</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
$year_job = str_replace("'","",$year);
if($db_type==0) $year_field_by="and YEAR(a.insert_date)"; 
else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
if($db_type==0) $year_field="SUBSTRING_INDEX(a.insert_date, '-', 1) as year"; 
else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
if($db_type==0) $field_grpby="GROUP BY a.job_no order by b.id desc"; 
else if($db_type==2) $field_grpby=" GROUP BY a.job_no,a.id,a.buyer_name,a.style_ref_no,a.gmts_item_id,b.po_number,a.job_no_prefix_num,a.insert_date,b.id order by b.id,a.job_no_prefix_num  desc ";
if(trim($year)!=0) $year_cond=" $year_field_by=$year_job"; else $year_cond="";
$cbo_buyer_name=($cbo_buyer_name==0)?"%%" : "%$cbo_buyer_name%";
$sql="select a.id,a.buyer_name,a.style_ref_no,a.gmts_item_id,b.po_number,a.job_no_prefix_num as job_prefix,$year_field from wo_po_details_master a,wo_po_break_down b where b.job_no_mst=a.job_no and a.company_name=$company_id and a.buyer_name like '%$cbo_buyer_name%' $year_cond and a.is_deleted = 0 $field_grpby";
$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
?>
<table width="580" border="1" rules="all" class="rpt_table">
	<thead>
    <tr>
        <th width="35">SL</th>
        <th width="100">Po number</th>
        <th width="100">Job no</th>
        <th width="50">Year</th>
        <th width="80">Buyer</th>
        <th width="100">Style</th>
        <th>Item Name</th>
    </tr>
   </thead>
</table>
<div style="max-height:390px; overflow-y:scroll; width:600px;">
<table id="table_body2" width="580" border="1" rules="all" class="rpt_table">
 <?php $rows=sql_select($sql);
	 $i=1;
 foreach($rows as $data)
 {
	  if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
  ?>
	<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="js_set_value('<?php echo $data[csf('job_prefix')]; ?>')" style="cursor:pointer;">
		<td width="35"><?php echo $i; ?></td>
		<td width="100"><p><?php echo $data[csf('po_number')]; ?></p></td>
		<td width="100"><p><?php echo $data[csf('job_prefix')]; ?></p></td>
        <td width="50"><p><?php echo $data[csf('year')]; ?></p></td>
		<td width="80"><p><?php echo $buyer[$data[csf('buyer_name')]]; ?></p></td>
		<td width="100"><p><?php echo $data[csf('style_ref_no')]; ?></p></td>
		<td><p><?php 
		$itemid=explode(",",$data[csf('gmts_item_id')]);
		foreach($itemid as $index=>$id){
		echo ($itemid[$index]==end($itemid))? $garments_item[$id] : $garments_item[$id].', ';
		}
		?></p></td>
	</tr>
    <?php $i++; } ?>
</table>
</div>
<script> setFilterGrid("table_body2",-1); </script>
<?php
	exit();
}//JobNumberShow
if($action=="order_number_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
$buyer = str_replace("'","",$buyer_name);
$year = str_replace("'","",$year);
$buyer = str_replace("'","",$buyer_name);
$year_job = str_replace("'","",$year);
if($db_type==0) $year_field_by=" and YEAR(b.insert_date)"; 
else if($db_type==2) $year_field_by=" and to_char(b.insert_date,'YYYY')";
if($db_type==0) $year_field="SUBSTRING_INDEX(b.insert_date, '-', 1) as year "; 
else if($db_type==2) $year_field="to_char(b.insert_date,'YYYY') as year";
if ($company_name==0) $company=""; else $company=" and b.company_name=$company_name";
if(trim($year)!=0) $year_cond=" $year_field_by=$year"; else $year_cond="";
//echo $buyer;die;
if ($buyer==0) $buyername=""; else $buyername=" and b.buyer_name=$buyer";//$cbo_buyer_name=($cbo_buyer_name==0)?"%%" : "%$cbo_buyer_name%";
$sql = "select distinct a.id,b.job_no,a.po_number,b.company_name,b.buyer_name,b.job_no_prefix_num as job_prefix,$year_field from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company $buyername $year_cond order by a.id asc"; 
$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
?>
<table width="490" border="1" rules="all" class="rpt_table">
	<thead>
        <tr>
        <th width="30">SL</th>
        <th width="80">Order Number</th>
        <th width="50">Job no</th>
        <th width="80">Buyer</th>
        <th width="40">Year</th>
        </tr>
   </thead>
</table>
<div style="max-height:380px; overflow:auto;">
<table id="table_body2" width="490" border="1" rules="all" class="rpt_table">
 <?php $rows=sql_select($sql);
	 $i=1;
 foreach($rows as $data)
 { if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
  ?>
	<tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value('<?php echo $data[csf('po_number')]; ?>')" style="cursor:pointer;">
		<td width="30"><?php echo $i; ?></td>
		<td width="80"><p><?php echo $data[csf('po_number')]; ?></p></td>
		<td width="50"><p><?php echo $data[csf('job_prefix')]; ?></p></td>
		<td width="80"><p><?php echo $buyer[$data[csf('buyer_name')]]; ?></p></td>
		<td width="40" align="center"><p><?php echo $data[csf('year')]; ?></p></td>
	</tr>
    <?php $i++; } ?>
</table>
</div>
<script> setFilterGrid("table_body2",-1); </script>
<?php
	exit();
}
if($action=="fabric_finishing_report")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if($db_type==0) $year_field_by="and YEAR(a.insert_date)"; 
	else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
	if($db_type==0) $field_concat="concat(machine_no,'-',brand) as machine_name"; 
	else if($db_type==2) $field_concat="machine_no || '-' || brand as machine_name";
// machine_no || '-' || brand as machine_name
	$company = str_replace("'","",$cbo_company_name);
	$buyer = str_replace("'","",$cbo_buyer_name);
	$job_number = str_replace("'","",$job_number);
	$job_number_id = str_replace("'","",$job_number_show);
	$batch_no = str_replace("'","",$batch_number_show);
	$color = str_replace("'","",$txt_color);
	//echo $batch_no;die;
	$batch_number_hidden = str_replace("'","",$batch_number);
	$ext_num = str_replace("'","",$txt_ext_no);
	$hidden_ext = str_replace("'","",$hidden_ext_no);
	$txt_order = str_replace("'","",$order_no);
	$hidden_order = str_replace("'","",$hidden_order_no);
	$cbo_type = str_replace("'","",$cbo_type);
	$year = str_replace("'","",$cbo_year);
	//echo $cbo_type;die;
	$txt_date_from = str_replace("'","",$txt_date_from);
	$txt_date_to = str_replace("'","",$txt_date_to);
	$jobdata=($job_number_id )? " and d.job_no_prefix_num='".$job_number_id ."'" : '';
	$buyerdata=($buyer)?' and d.buyer_name='.$buyer : '';
	$batch_num=($batch_no)?" and a.batch_no='".$batch_no."'" : '';
	if(trim($ext_no)!="") $ext_no_search="%".trim($ext_no)."%"; else $ext_no_search="%%";
	if ($txt_order=="") $order_no=""; else $order_no="  and c.po_number='$txt_order'";
	if ($color=="") $color_name=""; else $color_name="  and g.color_name='$color'";
	//echo $order_no;die;
	if ($ext_num=="") $ext_no=""; else $ext_no="  and a.extention_no=$ext_num ";
	if(trim($year)!=0) $year_cond=" $year_field_by=$year"; else $year_cond="";
	if($txt_date_from && $txt_date_to)
	{
		if($db_type==0)
		{
	$date_from=change_date_format($txt_date_from,'yyyy-mm-dd');
	$date_to=change_date_format($txt_date_to,'yyyy-mm-dd');
	$dates_com="and  f.process_end_date BETWEEN '$date_from' AND '$date_to'";
		}
		if($db_type==2)
		{
	$date_from=change_date_format($txt_date_from,'','',1);
	$date_to=change_date_format($txt_date_to,'','',1);
	$dates_com="and  f.process_end_date BETWEEN '$date_from' AND '$date_to'";
		}
	}
	$machine_arr=return_library_array( "select id,$field_concat from  lib_machine_name",'id','machine_name');
	$yarn_lot_arr=array();
		if($db_type==0)
		{
		$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id, group_concat(distinct(a.yarn_lot)) as yarn_lot from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot!=''  group by a.prod_id, b.po_breakdown_id");
		}
		else if($db_type==2)
		{
		$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id, listagg(a.yarn_lot,',') within group (order by a.yarn_lot) as yarn_lot from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot!='0' group by a.prod_id, b.po_breakdown_id");
		}
	foreach($yarn_lot_data as $rows)
	{
		$yarn_lot=explode(",",$rows[csf('yarn_lot')]);
		$yarn_lot_arr[$rows['prod_id']][$rows['po_breakdown_id']]=implode(",",array_unique($yarn_lot));
	}
	if($db_type==0)
	{
		if($cbo_type==1)//  For Heat Setting
		{
		$sql="
			select a.company_id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a, pro_fab_subprocess f,lib_color g	where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and g.id=a.color_id and a.entry_form=0 and a.id=b.mst_id and f.batch_id=a.id and f.entry_form=32 and  a.batch_against in(1,2) and  b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,a.company_id, a.batch_no, a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no, a.batch_against,b.item_description, b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks order by a.batch_no ";
		}
		else if($cbo_type==2) // Slitting/Squeezing
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=30 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks  order by a.batch_no ";
		}
		else if($cbo_type==3)//  Drying / Stentering 
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=31 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
			";
		}
			else if($cbo_type==4)//  Compacting
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=33 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		}	
		else if($cbo_type==5)//  Special Finish
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=34 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		}
		else if($cbo_type==9)//   Stentering 
		{
		$sql=" select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=48 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
			";
		}	
	}
	else // Oracle start here
	{
	if($cbo_type==1)//  For Heat Setting 
		{
		$sql="
			select a.company_id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a, pro_fab_subprocess f,lib_color g	where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and g.id=a.color_id and a.entry_form=0 and a.id=b.mst_id and f.batch_id=a.id and f.entry_form=32 and  a.batch_against in(1,2) and  b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,a.company_id, a.batch_no, a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no, a.batch_against,b.item_description, b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks order by a.batch_no ";
		}
		else if($cbo_type==2) // Slitting/Squeezing
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=30 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks  order by a.batch_no ";
		}
		else if($cbo_type==3)//  Drying / Stentering 
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=31 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
			";
		}
		
			else if($cbo_type==4)//  Compacting
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=33 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		}	
		else if($cbo_type==5)//  Special Finish
		{
		$sql="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=34 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		}
		else if($cbo_type==9)//   Stentering 
		{
		$sql=" select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=48 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
			";
		}
		else if($cbo_type==0)//  For All Search
		{
		$sql_heat="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.entry_form,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form in(32) and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name,f.entry_form, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		$sql_slitting="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.entry_form,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form in(30) and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name,f.entry_form, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		$sql_drying="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.entry_form,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form in(31) and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name,f.entry_form, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		$sql_stentering=" select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=48 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
			";
		$sql_compacting="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.entry_form,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form in(33) and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name,f.entry_form, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		$sql_special="
			select a.company_id,a.id,a.batch_no,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,listagg(cast(c.po_number as varchar2(4000)),',') within group (order by c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.entry_form,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form in(34) and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name,f.entry_form, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id,f.remarks  order by a.batch_no 
		";
		}
	}
	//echo $sql;
ob_start();
$batchdata=sql_select($sql);
if($cbo_type==1)
{
?>
<div>
<fieldset style="width:1320px;">
<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> <?php echo $search_by_arr[$cbo_type];?> </strong>
<br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1320" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="80">Job</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="80">Dia/Width Type</th>
    <th width="70">GSM</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="80">Ext. No</th>
    <th width="80">Batch Qty.</th>
    <th width="60">Yarn Lot</th>
    <th width="100">Process Date & Time</th>
    <th width="80">Remark</th>
    <th width="">Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:380px; width:1317px; overflow-y:scroll;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1300" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$batch_chk_arr=array();
foreach($batchdata as $batch)
{ 
if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
            { $f++;
                ?>
    <td width="30"><?php echo $f; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php //echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
    <td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
        <?php	
        $batch_chk_arr[]=$batch[csf('batch_no')];
            } 
            else
               { ?>
    <td width="30"><?php //echo $sl; ?></td>
    <td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
    <td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php //echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php //echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
            <?php }
            ?>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="80" title="<?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]];  ?></p></td>
    <td  width="70" title="<?php echo  $desc[2]; ?>"><p><?php echo $desc[2]; ?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="80" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="80" title="<?php echo $batch[csf('batch_qnty')];  ?>"><p><?php echo number_format($batch[csf('batch_qnty')],2);  ?></p></td>
    <td align="left" width="60" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
    <td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?></p></td>
     <td  align="center" width="80" title="<?php echo $batch[csf('remarks')]; ?>"><p><?php echo $batch[csf('remarks')]; ?></p></td>
    <td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]];?> </p></td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
} //batchdata froeach
 ?>
</tbody>
</table>
 <table class="rpt_table" width="1300" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="80"><?php echo number_format($btq,2); ?></th>
    <th width="60">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php
}
else if($cbo_type==2) // Slitting/Squeezing
{
?>
<div>
<fieldset style="width:1240px;">
<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong><?php echo $search_by_arr[$cbo_type];?> </strong><br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="80">Job</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="75">GSM</th>
    <th width="70">Dia/Width Type</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="40">Extn. No</th>
    <th width="70">Batch Qty.</th>
    <th width="50">Lot No</th>
    <th width="75">Process Date & Time</th>
    <th width="60">Remark</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:350px; width:1237px; overflow-y:scroll;;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$batch_chk_arr=array();
foreach($batchdata as $batch)
{ 
if ($i%2==0)  
$bgcolor="#E9F3FF";
else
$bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td width="75" title="<?php ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]];;?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')] ?>"><p><?php   echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p><?php   echo $batch[csf('remarks')]; ?></p>
     </td>
    <td align="center" title="<?php   if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?></p> </td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
} //batchdata froeach
 ?>
	</tbody>
</table>
<table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="40">&nbsp;</th>
    <th width="70"><?php echo number_format($btq,2); ?></th>
    <th width="50">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php
	}
else if($cbo_type==3) // Drying Stentering
{
?>
<div>
<fieldset style="width:1240px;">
<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong><?php echo $search_by_arr[$cbo_type];?> </strong><br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="80">Job</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="75">GSM</th>
    <th width="70">Dia/Width Type</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="40">Extn. No</th>
    <th width="70">Batch Qty.</th>
    <th width="50">Lot No</th>
    <th width="75">Process Date & Time</th>
    <th width="60">Remark</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:350px; width:1250px; overflow-y:scroll;;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$batch_chk_arr=array();
foreach($batchdata as $batch)
{ 
if ($i%2==0)  
$bgcolor="#E9F3FF";
else
$bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]];;?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')] ?>"><p><?php   echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p> <?php echo $batch[csf('remarks')]; ?>
        </p>
     </td>
    <td align="center" title="<?php   if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?></p> </td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
} //batchdata froeach
 ?>
	</tbody>
</table>
<table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="40">&nbsp;</th>
    <th width="70"><?php echo number_format($btq,2); ?></th>
    <th width="50">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php
	}
else if($cbo_type==9) // Stentering
{
?>
<div>
<fieldset style="width:1265px;">
<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong><?php echo $search_by_arr[$cbo_type];?> </strong><br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1265" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="80">Job</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="75">GSM</th>
    <th width="70">Dia/Width Type</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="40">Extn. No</th>
    <th width="70">Batch Qty.</th>
    <th width="50">Lot No</th>
    <th width="100">Process Date & Time</th>
    <th width="60">Remark</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:350px; width:1270px; overflow-y:scroll;;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1265" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$btq=0;
$batch_chk_arr=array();
foreach($batchdata as $batch)
{ 
if ($i%2==0)  
$bgcolor="#E9F3FF";
else
$bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]];;?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><p><?php echo number_format($batch[csf('batch_qnty')],2);  ?></p></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
    <td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')] ?>"><p><?php   echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p> <?php echo $batch[csf('remarks')]; ?>
        </p>
     </td>
    <td align="center" title="<?php   if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?></p> </td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
} //batchdata froeach
 ?>
	</tbody>
</table>
<table class="rpt_table" width="1265" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="40">&nbsp;</th>
    <th width="70"><?php echo number_format($btq,2); ?></th>
    <th width="50">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php
	}
else if($cbo_type==4) // Compacting
	{
?>
<div>
<fieldset style="width:1240px;">
<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong><?php echo $search_by_arr[$cbo_type];?> </strong><br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="80">Job</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="75">GSM</th>
    <th width="70">Dia/Width Type</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="40">Extn. No</th>
    <th width="70">Batch Qty.</th>
    <th width="50">Lot No</th>
    <th width="75">Process Date & Time</th>
    <th width="60">Remark</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:350px; width:1260px; overflow-y:scroll;;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$batch_chk_arr=array();
foreach($batchdata as $batch)
{ 
if ($i%2==0)  
$bgcolor="#E9F3FF";
else
$bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td width="75" title="<?php ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]];;?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')] ?>"><p><?php   echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p> <?php   echo $batch[csf('remarks')];?> </p>
     </td>
    <td align="center" title="<?php   if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?></p> </td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
} //batchdata froeach
 ?>
	</tbody>
</table>
<table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="40">&nbsp;</th>
    <th width="70"><?php echo number_format($btq,2); ?></th>
    <th width="50">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
	<?php }
else if($cbo_type==5) // Special Finishing
	{
		?>
<div>
<fieldset style="width:1240px;">
<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong><?php echo $search_by_arr[$cbo_type];?> </strong><br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="80">Job</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="75">GSM</th>
    <th width="70">Dia/Width Type</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="40">Extn. No</th>
    <th width="70">Batch Qty.</th>
    <th width="50">Lot No</th>
    <th width="75">Process Date & Time</th>
    <th width="60">Remark</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:350px; width:1260px; overflow-y:scroll;;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$batch_chk_arr=array();
foreach($batchdata as $batch)
{ 
if ($i%2==0)  
$bgcolor="#E9F3FF";
else
$bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]];?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')] ?>"><p><?php   echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p> <?php  echo $batch[csf('remarks')]; ?>
        </p>
     </td>
    <td align="center" title="<?php   if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?></p> </td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
} //batchdata froeach
 ?>
	</tbody>
</table>
<table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="40">&nbsp;</th>
    <th width="70"><?php echo number_format($btq,2); ?></th>
    <th width="50">&nbsp;</th>
    <th width="75">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
	<?php }
	else if($cbo_type==0) // All Search Fab. Finishing
	{
		?>
<div>
<fieldset style="width:1240px;">
<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong>Heat Setting </strong><br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="80">Job</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="75">GSM</th>
    <th width="70">Dia/Width Type</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="40">Extn. No</th>
    <th width="70">Batch Qty.</th>
    <th width="50">Lot No</th>
    <th width="75">Process Date & Time</th>
    <th width="60">Remark</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:350px; width:1260px; overflow-y:scroll;;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1240" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$batch_chk_arr=array();
//echo $sql_heat;
$heatsetting=sql_select($sql_heat);
foreach($heatsetting as $heat_row)
{ 
if ($i%2==0)  
$bgcolor="#E9F3FF";
else
$bgcolor="#FFFFFF";
$order_id=$heat_row[csf('po_id')];
$color_id=$heat_row[csf('color_id')];
$desc=explode(",",$heat_row[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$heat_row[csf('po_number')]))); 
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$heat_row[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$heat_row[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$heat_row[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$heat_row[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $heat_row[csf('job_no_prefix_num')]; ?>"><p><?php echo $heat_row[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$heat_row[csf('width_dia_type')]];;?></p></td>
    <td  width="80" title="<?php echo $color_library[$heat_row[csf('color_id')]]; ?>"><p><?php echo $color_library[$heat_row[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $heat_row[csf('batch_no')]; ?>"><p><?php echo $heat_row[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $heat_row[csf('extention_no')]; ?>"><p><?php echo $heat_row[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $heat_row[csf('batch_qnty')];  ?>"><?php echo number_format($heat_row[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$heat_row[csf('prod_id')]][$heat_row[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$heat_row[csf('prod_id')]][$heat_row[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($heat_row[csf('process_end_date')]).', '.$heat_row[csf('end_hours')].':'.$heat_row[csf('end_minutes')] ?>"><p><?php   echo change_date_format($heat_row[csf('process_end_date')]).', '.$heat_row[csf('end_hours')].':'.$heat_row[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p><?php   echo $batch[csf('remarks')]; ?> </p>
     </td>
    <td align="center" title="<?php   if($heat_row[csf('batch_against')]==2) echo $batch_against[$heat_row[csf('batch_against')]]; ?>"><p><?php  if($heat_row[csf('batch_against')]==2) echo $batch_against[$heat_row[csf('batch_against')]]; ?></p> </td>
</tr>
<?php 
$i++;
$btq_heat+=$heat_row[csf('batch_qnty')];
} //batchdata froeach
?>
     <tr  bgcolor="#D4D4D4" >
           <td align="left" colspan="16"><Strong> Sub Total:</Strong> <b><?php echo number_format($btq_heat,2); ?> </b></td>
     </tr>
      <tr bgcolor="#C2DCFF">
           <td colspan="16" align="center"><strong>Slitting/Squeezing</strong></td>
     </tr>
     <?php
	 $slitting_data=sql_select($sql_slitting);
     foreach($slitting_data as $slitting_row)
	 {
	if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
	$order_id=$slitting_row[csf('po_id')];
	$color_id=$slitting_row[csf('color_id')];
	$desc=explode(",",$slitting_row[csf('item_description')]); 
	$po_number=implode(",",array_unique(explode(",",$slitting_row[csf('po_number')])));
?>
     <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$slitting_row[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$slitting_row[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$slitting_row[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$slitting_row[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $slitting_row[csf('job_no_prefix_num')]; ?>"><p><?php echo $slitting_row[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$slitting_row[csf('width_dia_type')]];?></p></td>
    <td  width="80" title="<?php echo $color_library[$slitting_row[csf('color_id')]]; ?>"><p><?php echo $color_library[$slitting_row[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $slitting_row[csf('batch_no')]; ?>"><p><?php echo $slitting_row[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $slitting_row[csf('extention_no')]; ?>"><p><?php echo $slitting_row[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $slitting_row[csf('batch_qnty')];  ?>"><?php echo number_format($slitting_row[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$slitting_row[csf('prod_id')]][$slitting_row[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$slitting_row[csf('prod_id')]][$slitting_row[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($slitting_row[csf('process_end_date')]).', '.$slitting_row[csf('end_hours')].':'.$slitting_row[csf('end_minutes')] ?>"><p><?php   echo change_date_format($slitting_row[csf('process_end_date')]).', '.$slitting_row[csf('end_hours')].':'.$slitting_row[csf('end_minutes')]; ?></p></td>
    <td align="center" width="60"><p><?php   echo $slitting_row[csf('remarks')];?> </p>
     </td>
    <td align="center" title="<?php   if($slitting_row[csf('batch_against')]==2) echo $batch_against[$slitting_row[csf('batch_against')]]; ?>"><p><?php  if($slitting_row[csf('batch_against')]==2) echo $batch_against[$slitting_row[csf('batch_against')]]; ?></p> </td>
</tr>
<?php
$i++;
$btq_siltting+=$slitting_row[csf('batch_qnty')];
 }
?>
  	<tr  bgcolor="#D4D4D4">
      <td align="left" colspan="16"><Strong> Sub Total:</Strong> <b><?php echo number_format($btq_siltting,2); ?> </b></td>
     </tr>
      <tr bgcolor="#C2DCFF">
      <td colspan="16" align="center"><strong>Drying</strong></td>
     </tr>
      <?php
	 $drying_data=sql_select($sql_drying);
     foreach($drying_data as $drying_row)
	 {
	 if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
	$order_id=$drying_row[csf('po_id')];
	$color_id=$drying_row[csf('color_id')];
	$desc=explode(",",$drying_row[csf('item_description')]); 
	$po_number=implode(",",array_unique(explode(",",$drying_row[csf('po_number')]))); ?>
     <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$drying_row[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$drying_row[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$drying_row[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$drying_row[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $drying_row[csf('job_no_prefix_num')]; ?>"><p><?php echo $drying_row[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$drying_row[csf('width_dia_type')]];?></p></td>
    <td  width="80" title="<?php echo $color_library[$drying_row[csf('color_id')]]; ?>"><p><?php echo $color_library[$drying_row[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $drying_row[csf('batch_no')]; ?>"><p><?php echo $drying_row[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $drying_row[csf('extention_no')]; ?>"><p><?php echo $drying_row[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $drying_row[csf('batch_qnty')];  ?>"><?php echo number_format($drying_row[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$drying_row[csf('prod_id')]][$drying_row[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$drying_row[csf('prod_id')]][$drying_row[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($drying_row[csf('process_end_date')]).', '.$drying_row[csf('end_hours')].':'.$drying_row[csf('end_minutes')] ?>"><p><?php   echo change_date_format($drying_row[csf('process_end_date')]).', '.$drying_row[csf('end_hours')].':'.$drying_row[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p><?php  echo $drying_row[csf('remarks')]; ?> </p>
     </td>
    <td align="center" title="<?php   if($drying_row[csf('batch_against')]==2) echo $batch_against[$drying_row[csf('batch_against')]]; ?>"><p><?php  if($drying_row[csf('batch_against')]==2) echo $batch_against[$drying_row[csf('batch_against')]]; ?></p> </td>
</tr>
<?php
$i++;
$btq_drying+=$drying_row[csf('batch_qnty')];
 }
?>
  	<tr  bgcolor="#D4D4D4">
         <td align="left" colspan="16"><Strong> Sub Total:</Strong> <b><?php echo number_format($btq_drying,2); ?> </b></td>
     </tr>
      <tr bgcolor="#C2DCFF">
          <td colspan="16" align="center"><strong>Stentering</strong></td>
     </tr>
       <?php
	 $stentering_data=sql_select($sql_stentering);
     foreach($stentering_data as $sten_row)
	 {
	 if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
	$order_id=$sten_row[csf('po_id')];
	$color_id=$sten_row[csf('color_id')];
	$desc=explode(",",$sten_row[csf('item_description')]); 
	$po_number=implode(",",array_unique(explode(",",$sten_row[csf('po_number')]))); ?>
     <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$sten_row[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$sten_row[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$sten_row[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$sten_row[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $sten_row[csf('job_no_prefix_num')]; ?>"><p><?php echo $sten_row[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$sten_row[csf('width_dia_type')]];?></p></td>
    <td  width="80" title="<?php echo $color_library[$sten_row[csf('color_id')]]; ?>"><p><?php echo $color_library[$sten_row[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $sten_row[csf('batch_no')]; ?>"><p><?php echo $sten_row[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $sten_row[csf('extention_no')]; ?>"><p><?php echo $sten_row[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $sten_row[csf('batch_qnty')];  ?>"><?php echo number_format($sten_row[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$sten_row[csf('prod_id')]][$sten_row[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$sten_row[csf('prod_id')]][$sten_row[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($sten_row[csf('process_end_date')]).', '.$sten_row[csf('end_hours')].':'.$sten_row[csf('end_minutes')] ?>"><p><?php   echo change_date_format($sten_row[csf('process_end_date')]).', '.$sten_row[csf('end_hours')].':'.$sten_row[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p><?php  echo $sten_row[csf('remarks')]; ?> </p>
     </td>
    <td align="center" title="<?php   if($sten_row[csf('batch_against')]==2) echo $batch_against[$sten_row[csf('batch_against')]]; ?>"><p><?php  if($sten_row[csf('batch_against')]==2) echo $batch_against[$sten_row[csf('batch_against')]]; ?></p> </td>
</tr>
<?php
$i++;
$tot_btq_stenter+=$sten_row[csf('batch_qnty')];
 }
?>
  	<tr  bgcolor="#D4D4D4">
         <td align="left" colspan="16"><Strong> Sub Total:</Strong> <b><?php echo number_format($tot_btq_stenter,2); ?> </b></td>
     </tr>
      <tr bgcolor="#C2DCFF">
          <td colspan="16" align="center"><strong>Compacting</strong></td>
     </tr>
      <?php
	// echo $sql_compacting;
	 $compacting_data=sql_select($sql_compacting);
     foreach($compacting_data as $comp_row)
	 {
	if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
	$order_id=$comp_row[csf('po_id')];
	$color_id=$comp_row[csf('color_id')];
	$desc=explode(",",$comp_row[csf('item_description')]); 
	$po_number=implode(",",array_unique(explode(",",$comp_row[csf('po_number')])));
?>
     <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$comp_row[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$comp_row[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$comp_row[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$comp_row[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $comp_row[csf('job_no_prefix_num')]; ?>"><p><?php echo $comp_row[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$comp_row[csf('width_dia_type')]];?></p></td>
    <td  width="80" title="<?php echo $color_library[$comp_row[csf('color_id')]]; ?>"><p><?php echo $color_library[$comp_row[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $comp_row[csf('batch_no')]; ?>"><p><?php echo $comp_row[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $comp_row[csf('extention_no')]; ?>"><p><?php echo $comp_row[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $comp_row[csf('batch_qnty')];  ?>"><?php echo number_format($comp_row[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$comp_row[csf('prod_id')]][$comp_row[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$comp_row[csf('prod_id')]][$comp_row[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($comp_row[csf('process_end_date')]).', '.$comp_row[csf('end_hours')].':'.$comp_row[csf('end_minutes')] ?>"><p><?php   echo change_date_format($comp_row[csf('process_end_date')]).', '.$comp_row[csf('end_hours')].':'.$comp_row[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p>
   <?php     
echo $comp_row[csf('remarks')];
        ?>
        </p>
     </td>
    <td align="center" title="<?php   if($comp_row[csf('batch_against')]==2) echo $batch_against[$comp_row[csf('batch_against')]]; ?>"><p><?php  if($comp_row[csf('batch_against')]==2) echo $batch_against[$comp_row[csf('batch_against')]]; ?></p> </td>
</tr>
<?php
$i++;
$btq_com+=$comp_row[csf('batch_qnty')];
 }
?>
  	<tr  bgcolor="#D4D4D4">
           <td align="left" colspan="16"><Strong> Sub Total:</Strong> <b><?php echo number_format($btq_com,2); ?> </b></td>
     </tr>
      <tr bgcolor="#C2DCFF">
           <td colspan="16" align="center"><strong>Special Finish</strong></td>
     </tr>
      <?php
	// echo $sql_special;
	 $special_data=sql_select($sql_special);
     foreach($special_data as $special_row)
	 {
	if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
	$order_id=$special_row[csf('po_id')];
	$color_id=$special_row[csf('color_id')];
	$desc=explode(",",$special_row[csf('item_description')]); 
	$po_number=implode(",",array_unique(explode(",",$special_row[csf('po_number')])));
?>
    <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <td width="30"><?php echo $i; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$special_row[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$special_row[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$special_row[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$special_row[csf('buyer_name')]]; ?></p></td>
    <td  width="80" title="<?php echo  $special_row[csf('job_no_prefix_num')]; ?>"><p><?php echo $special_row[csf('job_no_prefix_num')]; ?></p></td>
    <td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo   $desc[2]; ?>"><p><?php echo  $desc[2]; ?></p></td>
    <td  width="75" title="<?php ?>"><p><?php echo $fabric_typee[$special_row[csf('width_dia_type')]];?></p></td>
    <td  width="80" title="<?php echo $color_library[$special_row[csf('color_id')]]; ?>"><p><?php echo $color_library[$special_row[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $special_row[csf('batch_no')]; ?>"><p><?php echo $special_row[csf('batch_no')]; ?></p></td>
    <td  align="center" width="40" title="<?php echo $special_row[csf('extention_no')]; ?>"><p><?php echo $special_row[csf('extention_no')]; ?></p></td>
    <td align="right" width="70" title="<?php echo $special_row[csf('batch_qnty')];  ?>"><?php echo number_format($special_row[csf('batch_qnty')],2);  ?></td>
    <td align="left" width="50" title="<?php echo $yarn_lot_arr[$special_row[csf('prod_id')]][$special_row[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$special_row[csf('prod_id')]][$special_row[csf('po_id')]];  ?></p></td>
    <td width="75" title="<?php echo change_date_format($special_row[csf('process_end_date')]).', '.$special_row[csf('end_hours')].':'.$special_row[csf('end_minutes')] ?>"><p><?php   echo change_date_format($special_row[csf('process_end_date')]).', '.$special_row[csf('end_hours')].':'.$special_row[csf('end_minutes')];
     ?></p></td>
    <td align="center" width="60"><p>
   <?php     
echo $special_row[csf('remarks')];
        ?>
        </p>
     </td>
    <td align="center" title="<?php   if($special_row[csf('batch_against')]==2) echo $batch_against[$special_row[csf('batch_against')]]; ?>"><p><?php  if($comp_row[csf('batch_against')]==2) echo $batch_against[$special_row[csf('batch_against')]]; ?></p> </td>
</tr>
<?php
$i++;
$btq_special+=$special_row[csf('batch_qnty')];
 }
 $grand_total=$btq_heat+$btq_siltting+$btq_drying+$btq_com+$btq_special;
?>
  	<tr  bgcolor="#D4D4D4">
           <td align="left" colspan="16"><Strong> Sub Total:</Strong> <b><?php echo number_format($btq_special,2); ?> </b></td>
     </tr>
      <tr  bgcolor="#C2DCFF">
           <td align="left" colspan="16"><Strong> Grand Total:</Strong> <b><?php echo number_format($grand_total,2); ?> </b></td>
     </tr>  
	</tbody>
</table>
</div>
</fieldset>
</div>
	<?php }//All Search End
else if($cbo_type==6) //Slitting
{ ?>
<div style="width:1230px;">
<fieldset style="width:1222px;">
<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> <?php echo $search_by_arr[$cbo_type];?> </strong>
<br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">M/C No</th>
    <th width="100">Buyer</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="70">GSM</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="80">Ext. No</th>
    <th width="80">Batch Qty.</th>
    <th width="100">Unloading Date</th>
    <th width="80">Unloading Time</th>
    <th width="80">Shade Position</th>
    <th width="60">Yarn Lot</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:380px; width:1240px; overflow-y:scroll;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
//if($db_type==0) $group_concat="group_concat(c.po_number)"; 
//else if($db_type==2) $group_concat="listagg(c.po_number,',' ) within group (order by c.po_number) AS po_number";
$sql_batch_h=sql_select("select batch_id from  pro_fab_subprocess where entry_form=30 and status_active=1 and is_deleted=0 and batch_id>0");
	$i=1;
	foreach($sql_batch_h as $row_h)
	{
		if($i!==1) $row_siltting.=",";
		$row_siltting.=$row_h[csf('batch_id')];
		$i++;
	}
//and a.process_id in(63)
$w_siltting=array_chunk(array_unique(explode(",",$row_siltting)),999);
//print_r($w_siltting);die;
$sql_wait=("select a.company_id,a.id,a.batch_no, a.process_id,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,$group_concat,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.result,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=35 and f.load_unload_id in(2) and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1   and f.result=1 and a.is_deleted=0  ");
			$p=1;
			foreach($w_siltting as $siltting_row)
			{
				if($p==1)  $sql_wait .=" and (a.id not in(".implode(',',$siltting_row).")"; else  $sql_wait .=" and a.id not in(".implode(',',$siltting_row).")";
				
				$p++;
			}
			$sql_wait .=")";
			 $sql_wait .=" GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no,a.process_id, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name,f.result, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks  order by a.batch_no";
			 
			// echo   $sql_wait;
			 
$batch_chk_arr=array();
$fab_data=sql_select($sql_wait);
foreach($fab_data as $batch)
{ 
if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')])));
$processid=explode(",",$batch[csf('process_id')]);
$result=$batch[csf('result')];
if (in_array(63,$processid))
{
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
            { $f++;
                ?>
    <td width="30"><?php echo $f; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
        <?php	
        $batch_chk_arr[]=$batch[csf('batch_no')];
            } 
            else
               { ?>
    <td width="30"><?php //echo $sl; ?></td>
    <td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
    <td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
            <?php }
            ?>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo  $desc[2]; ?>"><p><?php echo $desc[2]; ?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="80" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="80" title="<?php echo $batch[csf('batch_qnty')];  ?>"><p><?php echo number_format($batch[csf('batch_qnty')],2);  ?></p></td>
    <td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')])?></p></td>
    <td align="center" width="80" title="<?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];  ?>"><p><?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];   ?></p></td>
    <td align="center" width="80" title="Shade" ><p><?php if($result==1) echo 'OK';  ?></p></td>
    <td align="right" width="60" title="Lot"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?></p></td>
    <td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; else echo '';?> </p></td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
}
} //batchdata froeach
 ?>
</tbody>
</table>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="80"><?php echo number_format($btq,2); ?></th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php }
else if($cbo_type==7) // Wait For Drying 
{ ?>

<div style="width:1230px;">
<fieldset style="width:1222px;">
<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> <?php echo $search_by_arr[$cbo_type];?> </strong>
<br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">Job No</th>
    <th width="100">Buyer</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="70">GSM</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="80">Ext. No</th>
    <th width="80">Batch Qty.</th>
    <th width="100">Process Date</th>
    <th width="80">Process Time</th>
    <th width="60">Yarn Lot</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:380px; width:1240px; overflow-y:scroll;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$sql_batch_h=sql_select("select a.batch_id from  pro_fab_subprocess a,pro_fab_subprocess_dtls b where a.entry_form=31 and a.id=b.mst_id and b.width_dia_type in(2) and a.status_active=1 and a.is_deleted=0 and a.batch_id>0");
	$i=1;
	foreach($sql_batch_h as $row_h)
	{
		if($i!==1) $row_sent.=",";
		$row_sent.=$row_h[csf('batch_id')];
		$i++;
	}
//and a.process_id in(63)
$w_sent=array_chunk(array_unique(explode(",",$row_sent)),999);
$sql_wait=("select a.company_id,a.id,a.batch_no, a.process_id,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,$group_concat,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=48 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1  and a.is_deleted=0 and b.width_dia_type in(2) ");
		$p=1;
		foreach($w_sent as $dry_row)
		{
			if($p==1)  $sql_wait .="and (a.id not in(".implode(',',$dry_row).")"; else  $sql_wait .=" and a.id not in(".implode(',',$dry_row).")";
			
			$p++;
		}
		$sql_wait .=")";
		$sql_wait .=" GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no,a.process_id, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks  order by a.batch_no"; 

$batch_chk_arr=array();
$fab_wait=sql_select($sql_wait);
foreach($fab_wait as $batch)
{ 
if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')])));
$processid=explode(",",$batch[csf('process_id')]);
//echo $batch[csf('process_id')];die;
$result=$batch[csf('result')];
//$process_arr='66,91';
$process_name=array(66,91,125);
$process_count=count($processid);
$process = explode(",",str_replace("'","",$process_arr));
$process_sql=count($processid);
//print_r( $process);die;
//print_r(array_diff($process_name,$processid));die;

//if (in_array(65,$processid))
//{
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
            { $f++;
                ?>
    <td width="30"><?php echo $f; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
        <?php	
        $batch_chk_arr[]=$batch[csf('batch_no')];
            } 
            else
               { ?>
    <td width="30"><?php //echo $sl; ?></td>
    <td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
    <td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
            <?php }
            ?>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo  $desc[2]; ?>"><p><?php echo $desc[2]; ?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="80" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="80" title="<?php echo $batch[csf('batch_qnty')];  ?>"><p><?php echo number_format($batch[csf('batch_qnty')],2);  ?></p></td>
    <td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')])?></p></td>
    <td align="center" width="80" title="<?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];  ?>"><p><?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];   ?></p></td>
   
    <td align="right" width="60" title="Lot"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?></p></td>
    <td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; else echo '';?> </p></td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
//}
} //batchdata froeach
 ?>
</tbody>
</table>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="80"><?php echo number_format($btq,2); ?></th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php }
else if($cbo_type==8)//Wait for Compacting
{ ?>
<div style="width:1230px;">
<fieldset style="width:1222px;">
<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> <?php echo $search_by_arr[$cbo_type];?> </strong>
<br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">Machine No</th>
    <th width="100">Buyer</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="70">GSM</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="80">Ext. No</th>
    <th width="80">Batch Qty.</th>
    <th width="100">Process Date</th>
    <th width="80">Process Time</th>
    <th width="60">Yarn Lot</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:380px; width:1240px; overflow-y:scroll;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$sql_batch_h=sql_select("select batch_id from  pro_fab_subprocess where entry_form=33 and status_active=1 and is_deleted=0 and batch_id>0");
	$i=1;
	foreach($sql_batch_h as $row_h)
	{
		if($i!==1) $row_com.=",";
		$row_com.=$row_h[csf('batch_id')];
		$i++;
	}
	$w_com=array_chunk(array_unique(explode(",",$row_com)),999);
	
$sql_wait=("select a.company_id,a.id,a.batch_no, a.process_id,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,$group_concat,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=31 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1   and a.is_deleted=0");
		$p=1;
		foreach($w_com as $com_row)
		{
			if($p==1)  $sql_wait .="and (a.id not in(".implode(',',$com_row).")"; else  $sql_wait .=" and a.id not in(".implode(',',$com_row).")";
			$p++;
		}
		$sql_wait .=")";
		$sql_wait .="  GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no,a.process_id, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks  order by a.batch_no"; 
	
$batch_chk_arr=array();
$sql_wait_data=sql_select($sql_wait);
foreach($sql_wait_data as $batch)
{ 
if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')])));
$processid=explode(",",$batch[csf('process_id')]);
$process_name=array(66,91,125);
$process_count=count($processid);
$process = explode(",",str_replace("'","",$process_arr));
$process_sql=count($processid);
//if (in_array(66,$processid))
//{
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
            { $f++;
                ?>
    <td width="30"><?php echo $f; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]];; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
        <?php	
        $batch_chk_arr[]=$batch[csf('batch_no')];
            } 
            else
               { ?>
    <td width="30"><?php //echo $sl; ?></td>
    <td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
    <td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
            <?php }
            ?>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo  $desc[2]; ?>"><p><?php echo $desc[2]; ?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="80" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="80" title="<?php echo $batch[csf('batch_qnty')];  ?>"><p><?php echo number_format($batch[csf('batch_qnty')],2);  ?></p></td>
    <td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')])?></p></td>
    <td align="center" width="80" title="<?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];  ?>"><p><?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];   ?></p></td>
    <td align="right" width="60" title="Lot"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?></p></td>
    <td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; else echo '';?> </p></td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
//}
} //batchdata froeach
 ?>
</tbody>
</table>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="80"><?php echo number_format($btq,2); ?></th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php }
else if($cbo_type==9)// Special Finish
{ ?>
<div style="width:1230px;">
<fieldset style="width:1222px;">
<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> <?php echo $search_by_arr[$cbo_type];?> </strong>
<br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">Machine No</th>
    <th width="100">Buyer</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="70">GSM</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="80">Ext. No</th>
    <th width="80">Batch Qty.</th>
    <th width="100">Process Date</th>
    <th width="80">Process Time</th>
    <th width="60">Yarn Lot</th>
    <th width="80">Remark</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:380px; width:1240px; overflow-y:scroll;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$sql_batch_h=sql_select("select batch_id from  pro_fab_subprocess where entry_form=34 and status_active=1 and is_deleted=0 and batch_id>0");
if($db_type==0) $find_inset="and  FIND_IN_SET(67,68,69,70,73,74,75,77,83,88,92,94,127,128,a.process_id)"; 
	else if($db_type==2) $find_inset="and   ',' || a.process_id || ',' LIKE '%,67,68,69,70,73,74,75,77,83,88,92,94,127,128,%'";
	if($txt_date_from && $txt_date_to)
	{
		if($db_type==0)
		{
	$date_from=change_date_format($txt_date_from,'yyyy-mm-dd');
	$date_to=change_date_format($txt_date_to,'yyyy-mm-dd');
	$dates_batch="and  a.batch_date BETWEEN '$date_from' AND '$date_to'";
		}
		if($db_type==2)
		{
	$date_from=change_date_format($txt_date_from,'','',1);
	$date_to=change_date_format($txt_date_to,'','',1);
	$dates_batch="and  a.batch_date BETWEEN '$date_from' AND '$date_to'";
		}
	}
	$i=1;
	foreach($sql_batch_h as $row_h)
	{
		if($i!==1) $row_sp.=",";
		$row_sp.=$row_h[csf('batch_id')];
		$i++;
	}
//and a.process_id in(63)
$w_special=array_chunk(array_unique(explode(",",$row_sp)),999);
if($w_special!=0)
{
$sql_wait=("select a.company_id,a.id,a.batch_no, a.process_id,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,$group_concat,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=33 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1   and a.is_deleted=0 ");
		$p=1;
		foreach($w_special as $sp_row)
		{
			if($p==1)  $sql_wait .="and (a.id not in(".implode(',',$sp_row).")"; else  $sql_wait .=" and a.id not in(".implode(',',$sp_row).")";
			$p++;
		}
		$sql_wait .=")";
		$sql_wait .=" GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no,a.process_id, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks  order by a.batch_no"; 
}
$batch_chk_arr=array();
$sql_wait_data=sql_select($sql_wait);
foreach($sql_wait_data as $batch)
{ 
if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')])));
$processid=explode(",",$batch[csf('process_id')]);
$process_name=array(67,68,69);
$process_count=count($processid);
//echo $process_count;die;
$process = explode(",",str_replace("'","",$process_arr));
//echo $process_sql=count($process_name);
$arrdif=count(array_diff($processid,$process_name));
//echo $arrdif;
//echo $arrdif.'<br>'; 
//print_r(array_diff($processid,$process_name));echo '<br>';
//print_r( $process);die;
//print_r(array_diff($process_name,$processid));die;
//if (array_diff($processid,$process_name))
//if ($process_count!==$arrdif)
//{
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
            { $f++;
                ?>
    <td width="30"><?php echo $f; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]];; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]];; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
        <?php	
        $batch_chk_arr[]=$batch[csf('batch_no')];
            } 
            else
               { ?>
    <td width="30"><?php //echo $sl; ?></td>
    <td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
    <td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
            <?php }
            ?>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo  $desc[2]; ?>"><p><?php echo $desc[2]; ?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="80" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="80" title="<?php echo $batch[csf('batch_qnty')];  ?>"><p><?php echo number_format($batch[csf('batch_qnty')],2);  ?></p></td>
    <td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')])?></p></td>
    <td align="center" width="80" title="<?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];  ?>"><p><?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];   ?></p></td>
    <td align="right" width="60" title="Lot"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?></p></td>
     <td  width="80" title="<?php echo  $batch[csf('remarks')]; ?>"><p><?php echo $batch[csf('remarks')]; ?></p></td>
    <td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; else echo '';?> </p></td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
//}
} //batchdata froeach
 ?>
</tbody>
</table>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="80"><?php echo number_format($btq,2); ?></th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php }
else if($cbo_type==10)// Stentering
{ ?>
<div style="width:1230px;">
<fieldset style="width:1222px;">
<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> <?php echo $search_by_arr[$cbo_type];?> </strong>
<br>
<?php
	echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
?>
 </div>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<thead>
<tr>
    <th width="30">SL</th>
    <th width="80">Job No</th>
    <th width="100">Buyer</th>
    <th width="90">Order No</th>
    <th width="100">Fabrics Type</th>
    <th width="70">GSM</th>
    <th width="80">Color Name</th>
    <th width="90">Batch No</th>
    <th width="80">Ext. No</th>
    <th width="80">Batch Qty.</th>
    <th width="100">Process Date</th>
    <th width="80">Process Time</th>
    <th width="60">Yarn Lot</th>
    <th>Reprocess</th>
</tr>
</thead>
</table>
<div style=" max-height:380px; width:1240px; overflow-y:scroll;" id="scroll_body">
<table class="rpt_table" id="table_body" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tbody>
<?php 
$i=1;
$f=0;
$btq=0;
$sql_batch_h=sql_select("select a.batch_id from  pro_fab_subprocess a,pro_fab_subprocess_dtls b where a.entry_form=48 and a.id=b.mst_id and b.width_dia_type in(1,3) and a.status_active=1 and a.is_deleted=0 and a.batch_id>0");
$i=1;
foreach($sql_batch_h as $row_h)
{
	if($i!==1) $row_sent.=",";
	$row_sent.=$row_h[csf('batch_id')];
	$i++;
}
//and a.process_id in(63)
$w_sent=array_chunk(array_unique(explode(",",$row_sent)),999);
$sql_wait=("select a.company_id,a.id,a.batch_no, a.process_id,a.batch_date,a.color_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,$group_concat,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=30 and  a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1  and a.is_deleted=0 and b.width_dia_type in(1,3) ");
$p=1;
foreach($w_sent as $dry_row)
{
	if($p==1)  $sql_wait .="and (a.id not in(".implode(',',$dry_row).")"; else  $sql_wait .=" and a.id not in(".implode(',',$dry_row).")";
	$p++;
}
$sql_wait .=")";
 $sql_wait .=" GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no,a.process_id, a.batch_date,a.color_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks  order by a.batch_no"; 

$batch_chk_arr=array();
$fab_wait=sql_select($sql_wait);
foreach($fab_wait as $batch)
{ 
if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
$order_id=$batch[csf('po_id')];
$color_id=$batch[csf('color_id')];
$desc=explode(",",$batch[csf('item_description')]); 
$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')])));
$processid=explode(",",$batch[csf('process_id')]);
//echo $batch[csf('process_id')];die;
$result=$batch[csf('result')];
//$process_arr='66,91';
$process_name=array(66,91,125);
$process_count=count($processid);
$process = explode(",",str_replace("'","",$process_arr));
$process_sql=count($processid);
//print_r( $process);die;
//print_r(array_diff($process_name,$processid));die;

//if (in_array(65,$processid))
//{
?>
<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
    <?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
            { $f++;
                ?>
    <td width="30"><?php echo $f; ?></td>
    <td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
    <td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
        <?php	
        $batch_chk_arr[]=$batch[csf('batch_no')];
            } 
            else
               { ?>
    <td width="30"><?php //echo $sl; ?></td>
    <td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
    <td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
    <td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
            <?php }
            ?>
    <td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
    <td  width="70" title="<?php echo  $desc[2]; ?>"><p><?php echo $desc[2]; ?></p></td>
    <td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
    <td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
    <td  align="center" width="80" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
    <td align="right" width="80" title="<?php echo $batch[csf('batch_qnty')];  ?>"><p><?php echo number_format($batch[csf('batch_qnty')],2);  ?></p></td>
    <td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')])?></p></td>
    <td align="center" width="80" title="<?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];  ?>"><p><?php echo $batch[csf('end_hours')].':'.$batch[csf('end_minutes')];   ?></p></td>
    <td align="right" width="60" title="Lot"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?></p></td>
    <td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><p><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; else echo '';?> </p></td>
</tr>
<?php 
$i++;
$btq+=$batch[csf('batch_qnty')];
//}
} //batchdata froeach
 ?>
</tbody>
</table>
 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
<tfoot>
<tr>
    <th width="30">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="100">&nbsp;</th>
    <th width="70">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="90">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="80"><?php echo number_format($btq,2); ?></th>
    <th width="100">&nbsp;</th>
    <th width="80">&nbsp;</th>
    <th width="60">&nbsp;</th>
    <th>&nbsp;</th>
</tr>
</tfoot>
</table>
</div>
</fieldset>
</div>
<?php }
	exit();
//Fabric Finishing Report end
}
?>