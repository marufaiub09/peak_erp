<?php
session_start();
include('../../includes/common.php');


$user_id = $_SESSION['logic_erp']["user_id"];
 
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//************************************ Start *************************************************
$country_library=return_library_array( "select id,country_name from lib_country", "id", "country_name"  );
$lc_num_arr = return_library_array("select id, export_lc_no from com_export_lc where status_active=1 and is_deleted=0", "id", "export_lc_no"  );
$sc_num_arr = return_library_array("select id, contract_no from com_sales_contract where status_active=1 and is_deleted=0", "id", "contract_no");
$order_num_arr=return_library_array("select id, po_number from wo_po_break_down", "id", "po_number");


if ($action=="load_variable_settings")
{
	echo "$('#sewing_production_variable').val(0);\n";
	$sql_result = sql_select("select cutting_input,production_entry from variable_settings_production where company_name=$data and variable_list=1 and status_active=1");
 	foreach($sql_result as $result)
	{
		echo "$('#sewing_production_variable').val(".$result[csf("cutting_input")].");\n";
		echo "$('#styleOrOrderWisw').val(".$result[csf("production_entry")].");\n";
	}
 	exit();
}

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 172, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", $selected, "" );		 
}

if ($action=="load_drop_down_knit_com")
{
	$exDataArr = explode("**",$data);	
	$knit_source=$exDataArr[0];
	$company=$exDataArr[1];
	if($company=="" || $company==0) $company_cod = ""; else $company_cod = " and id=$company";
	if($knit_source==1)
		echo create_drop_down( "cbo_knitting_company", 170, "select id,company_name from lib_company where status_active=1 and is_deleted=0 $company_cod order by company_name","id,company_name", 0, "-- Select --", $company, "" );
	else if($knit_source==3)
		echo create_drop_down( "cbo_knitting_company", 170, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$company' and b.party_type =20 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else
		echo create_drop_down( "cbo_knitting_company", 170, $blank_array,"", 1, "-- Select --", $selected, "","","" );
	exit();	
}

if($action=="sys_surch_popup")
{
extract($_REQUEST);
echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
?>
	<script>
	function js_set_value(str)
	{
 		$("#hidden_delivery_id").val(str);
    	parent.emailwindow.hide();
 	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
	<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="750" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
             <thead>                	 
                <th width="200" style="display:none">Buyer Name</th>
                <th width="250">Challan No</th>
                <th width="350">Delivery Date Range</th>
                <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
            </thead>
            <tbody>
            	<tr>
                <td style="display:none">  
                <?php 
					echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",1 );
				?>
                </td>
                <td align="center" >				
                    <input type="text" style="width:150px" class="text_boxes"  name="txt_challan_no" id="txt_challan_no" />			
                </td>
                <td align="center">
                    <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:100px" readonly> To
                    <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:100px" readonly>
                </td> 
                <td align="center">
                    <input type="button" name="btn_show" id="btn_show" class="formbutton" value="Show" onClick="show_list_view (document.getElementById('txt_challan_no').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_delivery_search_list', 'search_div_delivery', 'cutting_delevar_to_input_controller','setFilterGrid(\'tbl_invoice_list\',-1)')" style="width:100px;" />
                </td>
            </tr>
            </tbody>
            <tr>
                <td align="center" height="40" colspan="5" valign="middle">
                    <?php echo load_month_buttons(1);  ?>
                    <input type="hidden" id="hidden_delivery_id" >
                </td>
            </tr>
        </table>
        <div id="search_div_delivery" style="margin-top:20px;"></div>
    </form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php

}


if($action=="create_delivery_search_list")
{
	
 	$ex_data = explode("_",$data);
	$txt_challan_no = $ex_data[0];
	$txt_date_from = $ex_data[1];
	$txt_date_to = $ex_data[2];
	$company = $ex_data[3];
	//echo $trans_com;die;
	$sql_cond="";
	if($txt_date_from!="" || $txt_date_to!="") 
	{
		if($db_type==0){$sql_cond .= " and a.delivery_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";}
		if($db_type==2 || $db_type==1){ $sql_cond .= " and a.delivery_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'";}
	}
	
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	
	if(trim($txt_challan_no)!="") $sql_cond .= " and a.challan_no='$txt_challan_no'";
	if(trim($trans_com)!=0) $sql_cond .= " and transport_supplier='$trans_com'";
	//listagg(CAST(b.po_breakdown_id as VARCHAR(4000)),',') within group (order by b.po_breakdown_id) as po_id
	if($db_type==2)
	{
		$sql = "select a.id, a.sys_number_prefix_num, a.sys_number, a.company_id, a.location_id, a.challan_no, a.delivery_date,a.deliver_basis, listagg(CAST(b.buyer_id as VARCHAR(4000)),',') within group (order by b.buyer_id) as buyer_id 
	from  pro_cut_delivery_mst a, pro_cut_delivery_order_dtls b where a.id=b.delivery_mst_id and a.status_active=1 and a.is_deleted=0 $sql_cond 
	group by a.id, a.sys_number_prefix_num, a.sys_number, a.company_id, a.location_id, a.challan_no, a.delivery_date,a.deliver_basis  order by a.id"; 
	}
	else if($db_type==0)
	{
		$sql = "select a.id, a.sys_number_prefix_num, a.sys_number, a.company_id, a.location_id, a.challan_no, a.delivery_date,a.deliver_basis, group_concat(b.buyer_id) as buyer_id 
	from  pro_cut_delivery_mst a, pro_cut_delivery_order_dtls b where a.id=b.delivery_mst_id and a.status_active=1 and a.is_deleted=0 $sql_cond 
	group by a.id, a.sys_number_prefix_num, a.sys_number, a.company_id, a.location_id, a.challan_no, a.delivery_date,a.deliver_basis  order by a.id"; 
	}
	//echo $sql;die;
	$result = sql_select($sql);
	$exfact_qty_arr=return_library_array( "select delivery_mst_id, sum(cut_delivery_qnty) as cut_delivery_qnty from  pro_cut_delivery_order_dtls where status_active=1 and delivery_mst_id>0 group by delivery_mst_id",'delivery_mst_id','cut_delivery_qnty');
 	$buyer_name_arr=return_library_array( "select id, short_name from lib_buyer where status_active=1",'id','short_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$delivery_basis_arr=array(1=>"Order No",2=>"Cut No");
   ?>
     	<table cellspacing="0" width="800" class="rpt_table" cellpadding="0" border="1" rules="all">
            <thead>
                <th width="50" >SL</th>
                <th width="90">Sys Challan No</th>
                <th width="250" >Buyer Name</th>
                <th width="90" >Challan No</th>
                <th width="100" >Delivery Date</th>
                <th width="100">Delivery Basis</th>
                <th >Delivery Qty</th>
            </thead>
     	</table>
     <div style="width:800px; max-height:220px;overflow-y:scroll;" >	 
        <table cellspacing="0" width="782" class="rpt_table" cellpadding="0" border="1" rules="all" id="tbl_invoice_list">
			<?php
			$i=1;
            foreach( $result as $row )
            {
                if ($i%2==0)  $bgcolor="#E9F3FF";
                else $bgcolor="#FFFFFF";
				?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')];?>);" > 
                    <td width="50" align="center"><?php echo $i; ?></td>
                    <td width="90" align="center"><?php echo $row[csf("sys_number_prefix_num")]; ?></td>
                    <td width="250"><p>
					<?php
					$buy_id_arr=array_unique(explode(",",$row[csf("buyer_id")]));
					$buyer_all="";
					foreach($buy_id_arr as $buy_id)
					{
						if($buyer_all!="") $buyer_all.=", ";
						$buyer_all .=$buyer_name_arr[$buy_id];
					}
					 echo $buyer_all; 
					?></p>&nbsp;</td>
                    <td width="90" align="center"><p><?php echo $row[csf("challan_no")]; ?></p>&nbsp;</td>	
                    <td width="100" align="center"><p><?php echo change_date_format($row[csf("delivery_date")]); ?></p>&nbsp;</td>
                    <td width="100"><p><?php echo $delivery_basis_arr[$row[csf("deliver_basis")]]; ?></p>&nbsp;</td>
                    <td width="" align="right" style="padding-right:3px;"><p><?php  echo number_format($exfact_qty_arr[$row[csf("id")]],0,"","");?></p></td> 	
                </tr>
				<?php 
				$i++;
             }
   		?>
			</table>
		</div> 
	  <?php  
exit();	

}

if($action=="populate_muster_from_date")
{
	$sql_mst=sql_select("select id, sys_number, company_id, location_id, challan_no, delivery_date,deliver_basis,knitting_source,knitting_company  
	from  pro_cut_delivery_mst where id=$data");
	foreach($sql_mst as $row)
	{
		echo "$('#txt_system_no').val('".$row[csf('sys_number')]."');\n";
		echo "$('#txt_system_id').val('".$row[csf('id')]."');\n";
		echo "$('#cbo_company_name').val(".$row[csf('company_id')].");\n";
		echo "$('#cbo_location_name').val(".$row[csf('location_id')].");\n";
		echo "$('#txt_challan_no').val('".$row[csf('challan_no')]."');\n";
		echo "$('#txt_ex_factory_date').val('".change_date_format($row[csf('delivery_date')])."');\n";
		echo "$('#cbo_delivery_basis').val(".$row[csf('deliver_basis')].");\n";
		echo "$('#cbo_knitting_source').val(".$row[csf('knitting_source')].");\n";
		echo "load_drop_down( 'requires/cutting_delevar_to_input_controller',".$row[csf('knitting_source')]."+'**'+".$row[csf('company_id')].", 'load_drop_down_knit_com', 'knitting_company_td' );\n";
		echo "$('#cbo_knitting_company').val(".$row[csf('knitting_company')].");\n";
		
		//echo "set_button_status(0, permission, 'fnc_cutDelivery',1,0);\n";
	}
}
 
if($action=="show_dtls_listview_mst")
{
?>	
	<div style="width:930px">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="200" >Item Name</th>
                <th width="150" >Country</th>
                <th width="150" >Order No</th>
                <th width="100" >Delivery Date</th>
                <th width="100" >Delivery Qnty</th>                    
                <th align="center">Challan No</th>
            </thead>
    	</table> 
    </div>
	<div style="width:930px;max-height:180px; overflow:y-scroll" id="sewing_production_list_view" align="left">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table" id="details_table">
		<?php  
			$i=1;
			$total_production_qnty=0;
			$sqlResult =sql_select("select a.id,a.po_break_down_id,a.item_number_id,a.country_id,a.cut_delivery_date,a.cut_delivery_qnty,a.location,b.challan_no from  pro_cut_delivery_order_dtls a,  pro_cut_delivery_mst b where a.delivery_mst_id=b.id and  a.delivery_mst_id=$data and a.status_active=1 and a.is_deleted=0 order by id");
 			foreach($sqlResult as $selectResult)
			{
 				if ($i%2==0)  
                	$bgcolor="#E9F3FF";
                else
               	 	$bgcolor="#FFFFFF";
				
				$total_production_qnty+=$selectResult[csf('cut_delivery_qnty')];	
 				?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="get_php_form_data(<?php echo $selectResult[csf('id')]; ?>,'populate_cutDelivery_form_data','requires/cutting_delevar_to_input_controller');get_php_form_data('<?php echo $selectResult[csf('po_break_down_id')];?>+**+<?php echo $selectResult[csf('item_number_id')];?>+**+<?php echo $selectResult[csf('country_id')];?>','populate_data_from_search_popup','requires/cutting_delevar_to_input_controller');" > 
                    <td width="40" align="center"><?php echo $i; ?></td>
                    <td width="200" align="center"><p><?php echo $garments_item[$selectResult[csf('item_number_id')]]; ?></p></td>
                    <td width="150" align="center"><p><?php echo $country_library[$selectResult[csf('country_id')]]; ?>&nbsp;</p></td>
                     <td width="150" align="center"><p><?php echo $order_num_arr[$selectResult[csf('po_break_down_id')]]; ?></p></td>
                    <td width="100" align="center"><p><?php echo change_date_format($selectResult[csf('cut_delivery_date')]); ?></p></td>
                    <td width="100" align="center"><p><?php echo $selectResult[csf('cut_delivery_qnty')]; ?></p></td>
                    <td align="center"><p><?php echo $selectResult[csf('challan_no')]; ?>&nbsp;</p></td>
                </tr>
			<?php
			$i++;
			}
			?>
            <!--<tfoot>
            	<tr>
                	<th colspan="3"></th>
                    <th><!? echo $total_production_qnty; ?></th>
                    <th colspan="3"></th>
                </tr>
            </tfoot>-->
		</table>
	</div>
    <!--<script> setFilterGrid("details_table",-1); </script>-->
<?php
	exit();
}
 

if($action=="order_popup")
{
extract($_REQUEST);
echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
?>
	<script>
		$(document).ready(function(e) {
            $("#txt_search_common").focus();
        });
		
		function search_populate(str)
		{
			if(str==0) 
			{		
				document.getElementById('search_by_th_up').innerHTML="Order No";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';		 
			}
			else if(str==1) 
			{
				document.getElementById('search_by_th_up').innerHTML="Style Ref. Number";
				document.getElementById('search_by_td').innerHTML='<input	type="text"	name="txt_search_common" style="width:230px " class="text_boxes" id="txt_search_common"	value=""  />';
			}
			else //if(str==2)
			{
				var buyer_name = '<option value="0">--- Select Buyer ---</option>';
				<?php 
				
					$buyer_arr=return_library_array( "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90))  order by buyer_name",'id','buyer_name');
					
								
				foreach($buyer_arr as $key=>$val)
				{
					echo "buyer_name += '<option value=\"$key\">".($val)."</option>';";
				} 
				?>
				document.getElementById('search_by_th_up').innerHTML="Select Buyer Name";
				document.getElementById('search_by_td').innerHTML='<select	name="txt_search_common" style="width:230px " class="combo_boxes" id="txt_search_common">'+ buyer_name +'</select>';
			}																																													
		}
	function js_set_value(id,item_id,po_qnty,plan_qnty,country_id)
	{
		$("#hidden_mst_id").val(id);
		$("#hidden_grmtItem_id").val(item_id); 
		$("#hidden_po_qnty").val(po_qnty);
		$("#hidden_country_id").val(country_id);
  		parent.emailwindow.hide();
 	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="780" cellspacing="0" cellpadding="0" class="rpt_table" align="center" border="1" rules="all">
    		<tr>
        		<td align="center" width="100%">
                    <table ellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                   		 <thead>                	 
                        	<th width="130">Search By</th>
                        	<th  width="180" align="center" id="search_by_th_up">Enter Order Number</th>
                        	<th width="200">Date Range</th>
                        	<th width="80"><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>
                    	</thead>
        				<tr>
                    		<td width="130">  
							<?php 
							$searchby_arr=array(0=>"Order No",1=>"Style Ref. Number",2=>"Buyer Name");
							echo create_drop_down( "txt_search_by", 130, $searchby_arr,"", 1, "-- Select Sample --", $selected, "search_populate(this.value)",0 );
  							?>
                    		</td>
                   			<td width="180" align="center" id="search_by_td">				
								<input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" onKeyDown="if (event.keyCode == 13) document.getElementById('btn_show').click()" />			
            				</td>
                    		<td align="center">
                            	<input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px"> To
					  			<input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 		</td> 
            		 		<td align="center">
                     			<input type="button" id="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $garments_nature; ?>, 'create_po_search_list_view', 'search_div', 'cutting_delevar_to_input_controller', 'setFilterGrid(\'tbl_po_list\',-1)')" style="width:100px;" />
                            </td>
        				</tr>
             		</table>
          		</td>
        	</tr>
        	<tr>
            	<td  align="center" height="40" valign="middle">
					<?php echo load_month_buttons(1);  ?>
                    <input type="hidden" id="hidden_mst_id">
                    <input type="hidden" id="hidden_grmtItem_id">
                    <input type="hidden" id="hidden_po_qnty">
                    <input type="hidden" id="hidden_country_id">
          		</td>
            </tr>
    	</table> 
        <div style="margin-top:10px" id="search_div"></div>    
    </form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_po_search_list_view")
{
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	$garments_nature = $ex_data[5];
	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==0)
			$sql_cond = " and b.po_number like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==1)
			$sql_cond = " and a.style_ref_no like '%".trim($txt_search_common)."%'";
		else if(trim($txt_search_by)==2)
			$sql_cond = " and a.buyer_name=trim('$txt_search_common')";		
 	}
	if($txt_date_from!="" || $txt_date_to!="") 
	{
		if($db_type==0){$sql_cond .= " and b.shipment_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";}
		if($db_type==2 || $db_type==1){ $sql_cond .= " and b.shipment_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."'";}
	}
	
	
	if(trim($company)!="") $sql_cond .= " and a.company_name='$company'";
		
 	$sql = "select b.id,a.order_uom,a.buyer_name,a.company_name,a.total_set_qnty,a.set_break_down, a.job_no,a.style_ref_no,a.gmts_item_id,a.location_name,b.shipment_date,b.po_number,b.po_quantity ,b.plan_cut from wo_po_details_master a, wo_po_break_down b  where a.job_no = b.job_no_mst and a.status_active=1 and  a.is_deleted=0 and b.status_active=1 and  b.is_deleted=0 and a.garments_nature=$garments_nature $sql_cond"; 
	//echo $sql;
	$result = sql_select($sql);
 	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	
	if($db_type==0)
	{
		$po_country_arr=return_library_array( "select po_break_down_id, group_concat(distinct(country_id)) as country from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id",'po_break_down_id','country');
	}
	else
	{
		$po_country_arr=return_library_array( "select po_break_down_id, listagg(CAST(country_id as VARCHAR(4000)),',') within group (order by country_id) as country from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id",'po_break_down_id','country');
	}
	
	
	$po_country_data_arr=array();
	$poCountryData=sql_select( "select po_break_down_id, item_number_id, country_id, sum(order_quantity) as qnty, sum(plan_cut_qnty) as plan_cut_qnty from wo_po_color_size_breakdown where status_active=1 and is_deleted=0 group by po_break_down_id, item_number_id, country_id");
	
	foreach($poCountryData as $row)
	{
		$po_country_data_arr[$row[csf('po_break_down_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]['po_qnty']=$row[csf('qnty')];
		$po_country_data_arr[$row[csf('po_break_down_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]['plan_cut_qnty']=$row[csf('plan_cut_qnty')];
	}
	
	
	$total_ex_fac_data_arr=array();
	$total_ex_fac_arr=sql_select( "select po_break_down_id, item_number_id, country_id, sum(cut_delivery_qnty) as cut_delivery_qnty from  pro_cut_delivery_order_dtls where status_active=1 and is_deleted=0 group by po_break_down_id, item_number_id, country_id");
	foreach($total_ex_fac_arr as $row)
	{
		$total_ex_fac_data_arr[$row[csf('po_break_down_id')]][$row[csf('item_number_id')]][$row[csf('country_id')]]=$row[csf('cut_delivery_qnty')];
	}
	?>
	<div style="width:1030px;">
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
                <th width="30">SL</th>
                <th width="70">Shipment Date</th>
                <th width="100">Order No</th>
                <th width="100">Buyer</th>
                <th width="120">Style</th>
                <th width="140">Item</th>
                <th width="100">Country</th>
                <th width="80">Order Qty</th>
                <th width="80">Total Delivery Qty</th>
                <th width="80">Balance</th>
                <th>Company Name</th>
            </thead>
     	</table>
     </div>
     <div style="width:1030px; max-height:240px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1012" class="rpt_table" id="tbl_po_list">
			<?php
			$i=1;
            foreach( $result as $row )
            {
				$exp_grmts_item = explode("__",$row[csf("set_break_down")]);
				$numOfItem = count($exp_grmts_item);
				$set_qty=""; $grmts_item="";
				
				//$country=explode(",",$po_country_arr[$row[csf("id")]]);
				$country=array_unique(explode(",",$po_country_arr[$row[csf("id")]]));
				
				$numOfCountry = count($country);
				
				for($k=0;$k<$numOfItem;$k++)								
				{
					if($row["total_set_qnty"]>1)
					{
						$grmts_item_qty = explode("_",$exp_grmts_item[$k]);
						$grmts_item = $grmts_item_qty[0];
						$set_qty = $grmts_item_qty[1];
					}else
					{
						$grmts_item_qty = explode("_",$exp_grmts_item[$k]);
						$grmts_item = $grmts_item_qty[0];
						$set_qty = $grmts_item_qty[1];
					}
					
					foreach($country as $country_id)
					{
						if ($i%2==0)  $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						
						//$po_qnty=$row[csf("po_quantity")]; $plan_cut_qnty=$row[csf("plan_cut")];
						$po_qnty=$po_country_data_arr[$row[csf('id')]][$grmts_item][$country_id]['po_qnty'];
						$plan_cut_qnty=$po_country_data_arr[$row[csf('id')]][$grmts_item][$country_id]['plan_cut_qnty'];
						?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value(<?php echo $row[csf("id")];?>,'<?php echo $grmts_item;?>','<?php echo $po_qnty;?>','<?php echo $plan_cut_qnty;?>','<?php echo $country_id;?>');" > 
								<td width="30" align="center"><?php echo $i; ?></td>
								<td width="70" align="center"><?php echo change_date_format($row[csf("shipment_date")]);?></td>		
								<td width="100"><p><?php echo $row[csf("po_number")]; ?></p></td>
								<td width="100"><p><?php echo $buyer_arr[$row[csf("buyer_name")]]; ?></p></td>	
								<td width="120"><p><?php echo $row[csf("style_ref_no")]; ?></p></td>
								<td width="140"><p><?php  echo $garments_item[$grmts_item];?></p></td>	
								<td width="100"><p><?php echo $country_library[$country_id]; ?>&nbsp;</p></td>
								<td width="80" align="right"><?php echo $po_qnty; //$po_qnty*$set_qty;?>&nbsp;</td>
                                <td width="80" align="right">
								<?php
								echo $total_cut_qty=$total_ex_fac_data_arr[$row[csf('id')]][$grmts_item][$country_id];
                                 ?> &nbsp;
                               </td>
                               <td width="80" align="right">
                                <?php
                                 $balance=$po_qnty-$total_cut_qty;
                                 echo $balance;
                                 ?>&nbsp;
                               </td>
								<td><?php  echo $company_arr[$row[csf("company_name")]];?> </td> 	
							</tr>
						<?php 
						$i++;
					}
				}
            }
   		?>
        </table>
    </div> 
	<?php	
exit();	
}
 
if($action=="populate_data_from_search_popup")
{
	$dataArr = explode("**",$data);
	$po_id = $dataArr[0];
	$item_id = $dataArr[1];
	$country_id = $dataArr[2];

	//echo "select a.id,a.po_quantity,a.plan_cut, a.po_number,a.po_quantity,b.company_name, b.buyer_name, b.style_ref_no,b.gmts_item_id, b.order_uom, b.job_no,b.location_name,a.shipment_date   from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and a.id=$po_id";
	$res = sql_select("select a.id,a.po_quantity,a.plan_cut, a.po_number,a.po_quantity,b.company_name, b.buyer_name, b.style_ref_no,b.gmts_item_id, b.order_uom, b.job_no,b.location_name,a.shipment_date   from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and a.id=$po_id"); 
	
 	foreach($res as $result)
	{
		echo "$('#txt_order_qty').val('".$result[csf('po_quantity')]."');\n";
		echo "$('#cbo_item_name').val(".$item_id.");\n";
		echo "$('#cbo_country_name').val(".$country_id.");\n";
		
		echo "$('#txt_order_no').val('".$result[csf('po_number')]."');\n";
		echo "$('#hidden_po_break_down_id').val('".$result[csf('id')]."');\n";
		echo "$('#cbo_buyer_name').val('".$result[csf('buyer_name')]."');\n";
		echo "$('#txt_style_no').val('".$result[csf('style_ref_no')]."');\n"; 		
		echo "$('#txt_shipment_date').val('".change_date_format($result[csf('shipment_date')])."');\n";
		echo "$('#txt_job_no').val('".$result[csf('job_no')]."');\n";
		
		$cutting_qty = return_field_value("sum(production_quantity) as production_quantity","pro_garments_production_mst","po_break_down_id=".$po_id." and item_number_id='$item_id' and production_type=1 and country_id='$country_id' and status_active=1 and is_deleted=0","production_quantity");
 		if($cutting_qty=="")$cutting_qty=0;
		
		$total_produced = return_field_value("sum(cut_delivery_qnty) as cut_delivery_qnty","pro_cut_delivery_order_dtls","po_break_down_id=".$po_id." and item_number_id='$item_id' and country_id='$country_id' and status_active=1 and is_deleted=0","cut_delivery_qnty");
		if($total_produced=="")$total_produced=0;
		
 		echo "$('#txt_finish_quantity').val('".$cutting_qty."');\n";
 		echo "$('#txt_cumul_quantity').attr('placeholder','".$total_produced."');\n";
		echo "$('#txt_cumul_quantity').val('".$total_produced."');\n";
		$yet_to_produced = $cutting_qty-$total_produced;
		echo "$('#txt_yet_quantity').attr('placeholder','".$yet_to_produced."');\n";
		echo "$('#txt_yet_quantity').val('".$yet_to_produced."');\n";
  	}
 	exit();	
}

if($action=="color_and_size_level")
{
	$dataArr = explode("**",$data);
	$po_id = $dataArr[0];
	$item_id = $dataArr[1];
	$variableSettings = $dataArr[2];
	$styleOrOrderWisw = $dataArr[3];
	$country_id = $dataArr[4];
	
	$color_library=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$size_library=return_library_array( "select id, size_name from lib_size",'id','size_name');
	//#############################################################################################//
	// order wise - color level, color and size level
	
	$ex_fac_value=array();
	
	//$variableSettings=2;
	
	if( $variableSettings==2 ) // color level
	{
		if($db_type==0)
		{
			
			$sql = "select a.item_number_id, a.color_number_id, sum(a.order_quantity) as order_quantity, sum(a.plan_cut_qnty) as plan_cut_qnty,
					sum(CASE WHEN b.production_type=1 then b.production_qnty ELSE 0 END) as production_qnty
					from wo_po_color_size_breakdown a 
					left join pro_garments_production_dtls b on a.id=b.color_size_break_down_id
					where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id";
					
			$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty from wo_po_color_size_breakdown a
                    left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
                    where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id");
			foreach($sql_exfac as $row_exfac)
			{
				$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("ex_production_qnty")];
				$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("bundle_qnty")];
				
			}
		}
		else
		{
			$sql = "select a.item_number_id, a.color_number_id, sum(a.order_quantity) as order_quantity, sum(a.plan_cut_qnty) as plan_cut_qnty,
					sum(CASE WHEN b.production_type=1 then b.production_qnty ELSE 0 END) as production_qnty
					from wo_po_color_size_breakdown a 
					left join pro_garments_production_dtls b on a.id=b.color_size_break_down_id
					where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id";
					
			$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty from wo_po_color_size_breakdown a
                    left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
                    where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id");
			foreach($sql_exfac as $row_exfac)
			{
				$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("ex_production_qnty")];
				$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("bundle_qnty")];
				
			}
		}
	}
	else if( $variableSettings==3 ) //color and size level
	{
			
			$prodData = sql_select("select a.color_size_break_down_id,sum(a.production_qnty) as production_qnty
										from pro_garments_production_dtls a,pro_garments_production_mst b where a.status_active=1 and a.mst_id=b.id and b.po_break_down_id='$po_id' and b.item_number_id='$item_id' and b.country_id='$country_id' and a.color_size_break_down_id!=0 and a.production_type=1 group by a.color_size_break_down_id");
			foreach($prodData as $row)
			{				  
				$color_size_pro_qnty_array[$row[csf('color_size_break_down_id')]]= $row[csf('production_qnty')];
			}
			
			$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,a.size_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty from wo_po_color_size_breakdown a
                    left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
                    where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id, a.size_number_id");
			foreach($sql_exfac as $row_exfac)
			{
				$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("ex_production_qnty")];
				$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("bundle_qnty")];
				

			}
					
			$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty 
				from wo_po_color_size_breakdown
				where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 and status_active=1 order by color_number_id";
	}
	else // by default color and size level
	{
			
			
			$prodData = sql_select("select a.color_size_break_down_id,sum(a.production_qnty) as production_qnty
										from pro_garments_production_dtls a,pro_garments_production_mst b where a.status_active=1 and a.mst_id=b.id and b.po_break_down_id='$po_id' and b.item_number_id='$item_id' and b.country_id='$country_id' and a.color_size_break_down_id!=0 and a.production_type=1 group by a.color_size_break_down_id");
			foreach($prodData as $row)
			{				  
				$color_size_pro_qnty_array[$row[csf('color_size_break_down_id')]]= $row[csf('production_qnty')];
			}
			
			$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,a.size_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty from wo_po_color_size_breakdown a
                    left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
                    where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id, a.size_number_id");
			foreach($sql_exfac as $row_exfac)
			{
				$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("ex_production_qnty")];
				$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("bundle_qnty")];
				
			}
					
			$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty 
				from wo_po_color_size_breakdown
				where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 and status_active=1 order by color_number_id";
				
	}
	
	//print_r($ex_fac_value);die;
	
	$colorResult = sql_select($sql);		
	//print_r($sql);
	$colorHTML="";
	$colorID='';
	$chkColor = array(); 
	$i=0;$totalQnty=0;
	foreach($colorResult as $color)
	{
		if( $variableSettings==2 ) // color level
		{
			
			 
			$colorHTML .='<tr><td>'.$color_library[$color[csf("color_number_id")]].'</td><td><input type="text" name="txt_color" id="colSize_'.($i+1).'" style="width:80px"  class="text_boxes_numeric" placeholder="'.($color[csf("production_qnty")]-$ex_fac_value[$color[csf('item_number_id')]][$color[csf('color_number_id')]]).'" onblur="fn_colorlevel_total('.($i+1).')"></td><td><input type="text" name="txt_bundle" id="txtbundle_'.($i+1).'" style="width:80px"  class="text_boxes_numeric" onblur="fn_colorlevel_total('.($i+1).')"></td></tr>';				
			$totalQnty += $color[csf("production_qnty")]-$ex_fac_value[$color[csf('item_number_id')]][$color[csf('color_number_id')]];
			$totalBundle += $color[csf("production_qnty")]-$ex_fac_bundle[$color[csf('item_number_id')]][$color[csf('color_number_id')]];
			$colorID .= $color[csf("color_number_id")].",";
		}
		else //color and size level
		{
			if( !in_array( $color[csf("color_number_id")], $chkColor ) )
			{
				if( $i!=0 ) $colorHTML .= "</table></div>";
				$i=0;
				$colorHTML .= '<h3 align="left" id="accordion_h'.$color[csf("color_number_id")].'" style="width:300px" class="accordion_h" onClick="accordion_menu( this.id,\'content_search_panel_'.$color[csf("color_number_id")].'\', \'\',1)"> <span id="accordion_h'.$color[csf("color_number_id")].'span">+</span>'.$color_library[$color[csf("color_number_id")]].' : <span id="total_'.$color[csf("color_number_id")].'"></span> </h3>';
				$colorHTML .= '<div id="content_search_panel_'.$color[csf("color_number_id")].'" style="display:none" class="accord_close"><table id="table_'.$color[csf("color_number_id")].'">';
				$chkColor[] = $color[csf("color_number_id")];					
			}
			//$index = $color[csf("size_number_id")].$color[csf("color_number_id")];
			$colorID .= $color[csf("size_number_id")]."*".$color[csf("color_number_id")].",";
			
			$pro_qnty=$color_size_pro_qnty_array[$color[csf('id')]];
			$exfac_qnty=$ex_fac_value[$color[csf('item_number_id')]][$color[csf('color_number_id')]][$color[csf('size_number_id')]];
			$exfac_bundle=$ex_fac_bundle[$color[csf('item_number_id')]][$color[csf('color_number_id')]][$color[csf('size_number_id')]];
			
			$colorHTML .='<tr><td>'.$size_library[$color[csf("size_number_id")]].'</td><td><input type="text" name="colorSize" id="colSize_'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes_numeric" style="width:100px" placeholder="'.($pro_qnty-$exfac_qnty).'" onblur="fn_total('.$color[csf("color_number_id")].','.($i+1).','. 1 .')"></td><td>Bundle</td><td><input type="text" name="txt_bundle" id="txtbundle_'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes_numeric" style="width:50px" onblur="fn_total('.$color[csf("color_number_id")].','.($i+1).','. 2 .')"></td></tr>';				
		}
		$i++; 
	}
	//echo $colorHTML;die; 
	if( $variableSettings==2 ){ $colorHTML = '<table id="table_color" class="rpt_table"><thead><th width="100">Color</th><th width="80">Quantity</th><th width="80">Bundle</th></thead><tbody>'.$colorHTML.'<tbody><tfoot><tr><th>Total</th><th><input type="text" id="total_color" placeholder="'.$totalQnty.'" class="text_boxes_numeric" style="width:80px" ></th><th><input type="text" id="total_bundle" placeholder="'.$totalBundle.'" class="text_boxes_numeric" style="width:80px" ></th></tr></tfoot></table>'; }
	echo "$('#breakdown_td_id').html('".addslashes($colorHTML)."');\n";
	$colorList = substr($colorID,0,-1);
	echo "$('#hidden_colorSizeID').val('".$colorList."');\n";
	//#############################################################################################//
	exit();
}

if($action=="confirm_popup")
{
extract($_REQUEST);
echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
?>
<script>
function js_set_value(str)
{
	$('#hidden_ref').val(str);
	parent.emailwindow.hide();
}
</script>
</head>
<body bgcolor="#FFFFFF">
	<input type="hidden" id="hidden_ref" >
	<table width="190">
    	<tr height="60">
        	<td colspan="2" align="center" valign="middle" style="font-size:20px; font-weight:bold;">Qnty Excceded by <?php echo ($placeholder_value-$filed_value); ?></td>
        </tr>
        <tr valign="bottom">
        	<td width="100" align="right" valign="bottom"><input type="button" value="OK" id="btn_ok" onClick="js_set_value(1)" style="width:80px;" class="formbutton"></td>
            <td width="100" valign="bottom"><input type="button" value="Cancel" id="btn_cancel" onClick="js_set_value(2)" style="width:80px;" class="formbutton"></td>
        </tr>
    </table>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="show_dtls_listview")
{
	$dataArr = explode("**",$data);
	$po_id = $dataArr[0];
	$item_id = $dataArr[1];
	$country_id = $dataArr[2];
?>	
	<div style="width:930px">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
                <th width="40">SL</th>
                <th width="150" align="center">Item Name</th>
                <th width="110" align="center">Country</th>
                <th width="110" align="center">Ex-Fact. Date</th>
                <th width="110" align="center">Ex-Fact. Qnty</th>                    
                <th width="120" align="center">Invoice No</th>
                <th width="120" align="center">LC/SC No</th>
                <th align="center">Challan No</th>
            </thead>
    	</table> 
    </div>
	<div style="width:930px;max-height:180px; overflow:y-scroll" id="sewing_production_list_view" align="left">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
		<?php  
			$i=1;
			$total_production_qnty=0;
			$sqlResult =sql_select("select id,po_break_down_id,item_number_id,country_id,cut_delivery_date,cut_delivery_qnty,location,lc_sc_no,invoice_no,challan_no from   pro_cut_delivery_order_dtls where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and status_active=1 and is_deleted=0 order by id");
 			foreach($sqlResult as $selectResult)
			{
 				if ($i%2==0)  
                	$bgcolor="#E9F3FF";
                else
               	 	$bgcolor="#FFFFFF";
					
				$total_production_qnty+=$selectResult[csf('cut_delivery_qnty')];	
  		
				$sqlEx = sql_select("select id,invoice_no,is_lc,lc_sc_id from com_export_invoice_ship_mst where id='".$selectResult[csf('invoice_no')]."'");
				foreach($sqlEx as $val)
				{
					if($val[csf("is_lc")]==1) //  lc
						$lc_sc = $lc_num_arr[$val[csf('lc_sc_id')]];
					else
						$lc_sc = $sc_num_arr[$val[csf('lc_sc_id')]];
						
					$invoiceNo = $val[csf('invoice_no')];
				}	
 			?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="get_php_form_data(<?php echo $selectResult[csf('id')]; ?>,'populate_cutDelivery_form_data','requires/cutting_delevar_to_input_controller');" > 
                    <td width="40" align="center"><?php echo $i; ?></td>
                    <td width="150" align="center"><p><?php echo $garments_item[$selectResult[csf('item_number_id')]]; ?></p></td>
                    <td width="110" align="center"><p><?php echo $country_library[$selectResult[csf('country_id')]]; ?>&nbsp;</p></td>
                    <td width="110" align="center"><p><?php echo change_date_format($selectResult[csf('cut_delivery_date')]); ?></p></td>
                    <td width="110" align="center"><p><?php echo $selectResult[csf('cut_delivery_qnty')]; ?></p></td>
                    <td width="120" align="center"><p><?php echo $invoiceNo; ?>&nbsp;</p></td>
                    <td width="120" align="center"><p><?php echo $lc_sc; ?>&nbsp;</p></td>
                    <td align="center"><p><?php echo $selectResult[csf('challan_no')]; ?>&nbsp;</p></td>
                </tr>
			<?php
			$i++;
			}
			?>
            <!--<tfoot>
            	<tr>
                	<th colspan="3"></th>
                    <th><!? echo $total_production_qnty; ?></th>
                    <th colspan="3"></th>
                </tr>
            </tfoot>-->
		</table>
	</div>
<?php
	exit();
}

if($action=="show_country_listview")
{
?>	
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="370" class="rpt_table">
        <thead>
            <th width="30">SL</th>
            <th width="110">Item Name</th>
            <th width="80">Country</th>
            <th width="75">Shipment Date</th>
            <th>Order Qty.</th>                    
        </thead>
    </table>
	<div id="scroll_body" style="width:388px; max-height:450px; overflow-x:hidden;  overflow-y:scroll;">   
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="370" class="rpt_table" id="tbl_body_1">
		<?php  
		$i=1;
		$sqlResult =sql_select("select po_break_down_id, item_number_id, country_id, max(country_ship_date) as country_ship_date, sum(order_quantity) as order_qnty, sum(plan_cut_qnty) as plan_cut_qnty from wo_po_color_size_breakdown where po_break_down_id='$data' and status_active=1 and is_deleted=0 group by po_break_down_id, item_number_id, country_id");
		foreach($sqlResult as $row)
		{
			if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="put_country_data(<?php echo $row[csf('po_break_down_id')].",".$row[csf('item_number_id')].",".$row[csf('country_id')].",".$row[csf('order_qnty')].",".$row[csf('plan_cut_qnty')]; ?>);"> 
				<td width="30"><?php echo $i; ?></td>
				<td width="110"><p><?php echo $garments_item[$row[csf('item_number_id')]]; ?></p></td>
				<td width="80"><p><?php echo $country_library[$row[csf('country_id')]]; ?>&nbsp;</p></td>
				<td width="75" align="center"><?php if($row[csf('country_ship_date')]!="0000-00-00") echo change_date_format($row[csf('country_ship_date')]); ?>&nbsp;</td>
				<td align="right"><?php  echo $row[csf('order_qnty')]; ?></td>
			</tr>
		<?php	
			$i++;
		}
		?>
	</table>
    </div>
	<?php
	exit();
}

if($action=="populate_cutDelivery_form_data")
{
	$ex_fac_value=array();
	$color_library=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$size_library=return_library_array( "select id, size_name from lib_size",'id','size_name');
	
	
	$sqlResult =sql_select("select id,garments_nature,po_break_down_id,item_number_id,country_id,location,cut_delivery_qnty,total_carton_qnty,remarks,entry_break_down_type  from  pro_cut_delivery_order_dtls where id='$data' and status_active=1 and is_deleted=0 order by id");
 	foreach($sqlResult as $result)
	{
		 
		echo "$('#txt_ex_quantity').attr('placeholder','".$result[csf('cut_delivery_qnty')]."');\n";
 		echo "$('#txt_ex_quantity').val('".$result[csf('cut_delivery_qnty')]."');\n";
		echo "$('#txt_total_carton_qnty').val('".$result[csf('total_carton_qnty')]."');\n";
		echo "$('#txt_remark').val('".$result[csf('remarks')]."');\n";
		echo "$('#txt_mst_id').val('".$result[csf('id')]."');\n";
 		echo "set_button_status(1, permission, 'fnc_cutDelivery',1,1);\n";
		
		//break down of color and size------------------------------------------
 		//#############################################################################################//
		// order wise - color level, color and size level

		$variableSettings = $result[csf('entry_break_down_type')];
		
		
		//$variableSettings=2;
		
		if( $variableSettings!=1 ) // gross level
		{ 
			$po_id = $result[csf('po_break_down_id')];
			$item_id = $result[csf('item_number_id')];
			$country_id = $result[csf('country_id')];
			//echo "select a.color_size_break_down_id,a.production_qnty,a.bundle_qnty,b.size_number_id, b.color_number_id from  pro_cut_delivery_color_dtls a,wo_po_color_size_breakdown b where a.mst_id=$data and a.status_active=1 and a.color_size_break_down_id=b.id and b.po_break_down_id='$po_id' and b.item_number_id='$item_id' and b.country_id='$country_id'";
			$sql_dtls = sql_select("select a.color_size_break_down_id,a.production_qnty,a.bundle_qnty,b.size_number_id, b.color_number_id from  pro_cut_delivery_color_dtls a,wo_po_color_size_breakdown b where a.mst_id=$data and a.status_active=1 and a.color_size_break_down_id=b.id and b.po_break_down_id='$po_id' and b.item_number_id='$item_id' and b.country_id='$country_id'");	
			foreach($sql_dtls as $row)
			{				  
				if( $variableSettings==2 ) $index = $row[csf('color_number_id')]; else $index = $row[csf('size_number_id')].$row[csf('color_number_id')];
			  	$amountArr[$index] = $row[csf('production_qnty')];
				$bundleQty[$index] = $row[csf('bundle_qnty')];
				
			}  
			
			if( $variableSettings==2 ) // color level
			{
				if($db_type==0)
				{
					
					$sql = "select a.item_number_id, a.color_number_id, sum(a.order_quantity) as order_quantity, sum(a.plan_cut_qnty) as plan_cut_qnty,
							sum(CASE WHEN b.production_type=8 then b.production_qnty ELSE 0 END) as production_qnty
							from wo_po_color_size_breakdown a 
							left join pro_garments_production_dtls b on a.id=b.color_size_break_down_id
							where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id";
							
					$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty from wo_po_color_size_breakdown a
							left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
							where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id");
					foreach($sql_exfac as $row_exfac)
					{
						$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("ex_production_qnty")];
						$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("bundle_qnty")];
						
					}
				}
				else
				{
					$sql = "select a.item_number_id, a.color_number_id, sum(a.order_quantity) as order_quantity, sum(a.plan_cut_qnty) as plan_cut_qnty,
							sum(CASE WHEN b.production_type=8 then b.production_qnty ELSE 0 END) as production_qnty
							from wo_po_color_size_breakdown a 
							left join pro_garments_production_dtls b on a.id=b.color_size_break_down_id
							where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id";
							
					$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty  from wo_po_color_size_breakdown a
							left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
							where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id");
					foreach($sql_exfac as $row_exfac)
					{
						$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("ex_production_qnty")];
						$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]]=$row_exfac[csf("bundle_qnty")];
						
					}
				}
				
			}
			else if( $variableSettings==3 ) //color and size level
			{
					
					$prodData = sql_select("select a.color_size_break_down_id,sum(a.production_qnty) as production_qnty
										from pro_garments_production_dtls a,pro_garments_production_mst b where a.status_active=1 and a.mst_id=b.id and b.po_break_down_id='$po_id' and b.item_number_id='$item_id' and b.country_id='$country_id' and a.color_size_break_down_id!=0 and a.production_type=1 group by a.color_size_break_down_id");
			foreach($prodData as $row)
			{				  
				$color_size_pro_qnty_array[$row[csf('color_size_break_down_id')]]= $row[csf('production_qnty')];
			}
			
			$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,a.size_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty from wo_po_color_size_breakdown a
                    left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
                    where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id, a.size_number_id");
			foreach($sql_exfac as $row_exfac)
			{
				$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("ex_production_qnty")];
				$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("bundle_qnty")];
				
			}
					
			$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty 
				from wo_po_color_size_breakdown
				where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 and status_active=1 order by color_number_id";
				
			}
			else // by default color and size level
			{
					$prodData = sql_select("select a.color_size_break_down_id,sum(a.production_qnty) as production_qnty
										from pro_garments_production_dtls a,pro_garments_production_mst b where a.status_active=1 and a.mst_id=b.id and b.po_break_down_id='$po_id' and b.item_number_id='$item_id' and b.country_id='$country_id' and a.color_size_break_down_id!=0 and a.production_type=1 group by a.color_size_break_down_id");
				foreach($prodData as $row)
				{				  
					$color_size_pro_qnty_array[$row[csf('color_size_break_down_id')]]= $row[csf('production_qnty')];
				}
				
				$sql_exfac=sql_select("select a.item_number_id,a.color_number_id,a.size_number_id,sum(ex.production_qnty) as ex_production_qnty,sum(ex.bundle_qnty) as bundle_qnty from wo_po_color_size_breakdown a
						left join pro_cut_delivery_color_dtls ex on ex.color_size_break_down_id=a.id
						where a.po_break_down_id='$po_id' and a.item_number_id='$item_id' and a.country_id='$country_id' and a.is_deleted=0 and a.status_active=1 group by a.item_number_id, a.color_number_id, a.size_number_id");
				foreach($sql_exfac as $row_exfac)
				{
					$ex_fac_value[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("ex_production_qnty")];
					$ex_fac_bundle[$row_exfac[csf("item_number_id")]][$row_exfac[csf("color_number_id")]][$row_exfac[csf("size_number_id")]]=$row_exfac[csf("bundle_qnty")];
					
				}
						
				$sql = "select id, item_number_id, size_number_id, color_number_id, order_quantity, plan_cut_qnty 
					from wo_po_color_size_breakdown
					where po_break_down_id='$po_id' and item_number_id='$item_id' and country_id='$country_id' and is_deleted=0 and status_active=1 order by color_number_id";
				
			}
 			$colorResult = sql_select($sql);
 			//print_r($sql);die;
			$colorHTML="";
			$colorID='';
			$chkColor = array(); 
			$i=0;$totalQnty=0;$colorWiseTotal=0;
			foreach($colorResult as $color)
			{
				if( $variableSettings==2 ) // color level
				{  
					$amount = $amountArr[$color[csf("color_number_id")]];
					$bundle = $bundleQty[$color[csf("color_number_id")]];
					$colorHTML .='<tr><td>'.$color_library[$color[csf("color_number_id")]].'</td><td><input type="text" name="txt_color" id="colSize_'.($i+1).'" style="width:80px"  class="text_boxes_numeric" placeholder="'.($color[csf("production_qnty")]-$ex_fac_value[$color[csf('item_number_id')]][$color[csf('color_number_id')]]+$amount).'" value="'.$amount.'" onblur="fn_colorlevel_total('.($i+1).')"></td><td><input type="text" name="txtbundle" id="txtbundle_'.($i+1).'" style="width:80px"  class="text_boxes_numeric" value="'.$bundle.'" onblur="fn_colorlevel_total('.($i+1).')"></td></tr>';				
					$totalQnty += $amount;
					$totalBundle += $bundle;
					$colorID .= $color[csf("color_number_id")].",";
				}
				else //color and size level
				{
					$index = $color[csf("size_number_id")].$color[csf("color_number_id")];
					$amount = $amountArr[$index];
					$bundle = $bundleQty[$index];
					if( !in_array( $color[csf("color_number_id")], $chkColor ) )
					{
						if( $i!=0 ) $colorHTML .= "</table></div>";
						$i=0;$colorWiseTotal=0;
						$colorHTML .= '<h3 align="left" id="accordion_h'.$color[csf("color_number_id")].'" style="width:300px" class="accordion_h" onClick="accordion_menu( this.id,\'content_search_panel_'.$color[csf("color_number_id")].'\', \'\',1)"> <span id="accordion_h'.$color[csf("color_number_id")].'span">+</span>'.$color_library[$color[csf("color_number_id")]].' : <span id="total_'.$color[csf("color_number_id")].'"></span></h3>';
						$colorHTML .= '<div id="content_search_panel_'.$color[csf("color_number_id")].'" style="display:none" class="accord_close"><table id="table_'.$color[csf("color_number_id")].'">';
						$chkColor[] = $color[csf("color_number_id")];
						$totalFn .= "fn_total(".$color[csf("color_number_id")].");";
					}
 					$colorID .= $color[csf("size_number_id")]."*".$color[csf("color_number_id")].",";
					
					$pro_qnty=$color_size_pro_qnty_array[$color[csf('id')]];
					$exfac_qnty=$ex_fac_value[$color[csf('item_number_id')]][$color[csf('color_number_id')]][$color[csf('size_number_id')]];
					
					$colorHTML .='<tr><td>'.$size_library[$color[csf("size_number_id")]].'</td><td><input type="text" name="colorSize" id="colSize_'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes_numeric" style="width:100px" placeholder="'.($pro_qnty-$exfac_qnty+$amount).'" onblur="fn_total('.$color[csf("color_number_id")].','.($i+1).','. 1 .')" value="'.$amount.'" ></td><td>Bundle</td><td><input type="text" name="txt_bundle" id="txtbundle_'.$color[csf("color_number_id")].($i+1).'"  class="text_boxes_numeric" style="width:50px"  onblur="fn_total('.$color[csf("color_number_id")].','.($i+1).','. 2 .')" value="'.$bundle.'" ></td></tr>';				
					$colorWiseTotal += $amount;
					$colorWiseTotalBundle += $bundle;
				}
				$i++; 
			}
			//echo $colorHTML;die; 
			if( $variableSettings==2 ){ $colorHTML = '<table id="table_color" class="rpt_table"><thead><th width="100">Color</th><th width="80">Quantity</th><th width="80">Bundle</th></thead><tbody>'.$colorHTML.'<tbody><tfoot><tr><th>Total</th><th><input type="text" id="total_color" placeholder="'.$totalQnty.'" value="'.$totalQnty.'" class="text_boxes_numeric" style="width:80px" ></th><th><input type="text" id="total_bundle" placeholder="'.$totalBundle.'" value="'.$totalBundle.'" class="text_boxes_numeric" style="width:80px" ></th></tr></tfoot></table>'; }
			echo "$('#breakdown_td_id').html('".addslashes($colorHTML)."');\n";
			if( $variableSettings==3 )echo "$totalFn;\n";
			$colorList = substr($colorID,0,-1);
			echo "$('#hidden_colorSizeID').val('".$colorList."');\n";
		}//end if condtion
	}
 	exit();		
}

// pro_cut_delivery_order_dtls
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
 		//table lock here 
		//if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		if(str_replace("'","",$txt_system_id)=="")
		{
			$delivery_mst_id=return_next_id("id", "pro_cut_delivery_mst", 1);

			if($db_type==2) $mrr_cond="and  TO_CHAR(insert_date,'YYYY')=".date('Y',time()); else if($db_type==0) $mrr_cond="and year(insert_date)=".date('Y',time());
			$new_sys_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'CDI', date("Y",time()), 5, "select sys_number_prefix,sys_number_prefix_num from pro_cut_delivery_mst where company_id=$cbo_company_name $mrr_cond order by id DESC ", "sys_number_prefix", "sys_number_prefix_num" ));
			
			$field_array_delivery="id, sys_number_prefix, sys_number_prefix_num, sys_number, company_id, location_id, challan_no, delivery_date, deliver_basis, knitting_source, knitting_company, inserted_by, insert_date";
			$data_array_delivery="(".$delivery_mst_id.",'".$new_sys_number[1]."','".$new_sys_number[2]."','".$new_sys_number[0]."', ".$cbo_company_name.",".$cbo_location_name.",".$new_sys_number[2].",".$txt_ex_factory_date.",".$cbo_delivery_basis.",".$cbo_knitting_source.",".$cbo_knitting_company.",".$user_id.",'".$pc_date_time."')";
			$mrr_no=$new_sys_number[0];
			$mrr_no_challan=$new_sys_number[2];
			
		}
		else
		{
			$delivery_mst_id=str_replace("'","",$txt_system_id);
			$mrr_no=str_replace("'","",$txt_system_no);
			$mrr_no_challan=str_replace("'","",$txt_challan_no);
			
			$field_array_delivery="company_id*location_id*challan_no*delivery_date*deliver_basis*knitting_source*knitting_company*updated_by*update_date";
			$data_array_delivery="".$cbo_company_name."*".$cbo_location_name."*".$txt_challan_no."*".$txt_ex_factory_date."*".$cbo_delivery_basis."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$user_id."*'".$pc_date_time."'";
			
		}
		$id=return_next_id("id", " pro_cut_delivery_order_dtls", 1);
		
  		$field_array1="id, delivery_mst_id, garments_nature, buyer_id, po_break_down_id, item_number_id, country_id, location, cut_delivery_date, cut_delivery_qnty, total_carton_qnty, challan_no, remarks,  entry_break_down_type, inserted_by, insert_date";
		$data_array1="(".$id.",".$delivery_mst_id.",".$garments_nature.",".$cbo_buyer_name.",".$hidden_po_break_down_id.", ".$cbo_item_name.",".$cbo_country_name.",".$cbo_location_name.",".$txt_ex_factory_date.",".$txt_ex_quantity.",".$txt_total_carton_qnty.",".$mrr_no_challan.",".$txt_remark.",".$sewing_production_variable.",".$user_id.",'".$pc_date_time."')";
		
		
		
		//echo "INSERT INTO pro_cut_delivery_mst (".$field_array_delivery.") VALUES ".$data_array_delivery;die;
		
 		//$rID=sql_insert(" pro_cut_delivery_order_dtls",$field_array1,$data_array1,1);


		
		// pro_cut_delivery_color_dtls table entry here ----------------------------------///
		$field_array="id,mst_id,color_size_break_down_id,production_qnty,bundle_qnty";
  		
		if(str_replace("'","",$sewing_production_variable)==2)//color level wise
		{		
			$color_sizeID_arr=sql_select( "select id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name and color_mst_id!=0 order by id");
			$colSizeID_arr=array(); 
			foreach($color_sizeID_arr as $val){
				$index = $val[csf("color_number_id")];
				$colSizeID_arr[$index]=$val[csf("id")];
			}	
			// $colorIDvalue concate as colorID*Value**colorID*Value -------------------------//
 			$rowEx = explode("**",$colorIDvalue); 
 			$dtls_id=return_next_id("id", "pro_cut_delivery_color_dtls", 1);
			$data_array="";$j=0;
			foreach($rowEx as $rowE=>$val)
			{
				$colorSizeNumberIDArr = explode("*",$val);
				
				if($j==0)$data_array = "(".$dtls_id.",".$id.",'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."','".$colorSizeNumberIDArr[2]."')";
				else $data_array .= ",(".$dtls_id.",".$id.",'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."','".$colorSizeNumberIDArr[2]."')";
				$dtls_id=$dtls_id+1;							
 				$j++;								
			}
 		}
		
		if(str_replace("'","",$sewing_production_variable)==3)//color and size wise
		{		
			$color_sizeID_arr=sql_select( "select id,size_number_id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name order by size_number_id,color_number_id" );
			$colSizeID_arr=array(); 
			foreach($color_sizeID_arr as $val){
				$index = $val[csf("size_number_id")].$val[csf("color_number_id")];
				$colSizeID_arr[$index]=$val[csf("id")];
			}	
			//	colorIDvalue concate as sizeID*colorID*value***sizeID*colorID*value	--------------------------// 
 			$rowEx = explode("***",$colorIDvalue); 
			$dtls_id=return_next_id("id", "pro_cut_delivery_color_dtls", 1);
			$data_array="";$j=0;
			foreach($rowEx as $rowE=>$valE)
			{
				
				$colorAndSizeAndValue_arr = explode("*",$valE);
				$sizeID = $colorAndSizeAndValue_arr[0];
				$colorID = $colorAndSizeAndValue_arr[1];				
				$colorSizeValue = $colorAndSizeAndValue_arr[2];
				$colorSizebundle = $colorAndSizeAndValue_arr[3];
				$index = $sizeID.$colorID;
				if($j==0)$data_array = "(".$dtls_id.",".$id.",'".$colSizeID_arr[$index]."','".$colorSizeValue."','".$colorSizebundle."')";
				else $data_array .= ",(".$dtls_id.",".$id.",'".$colSizeID_arr[$index]."','".$colorSizeValue."','".$colorSizebundle."')";
				$dtls_id=$dtls_id+1;
 				$j++;
			}
		}
		
		//echo "insert into pro_cut_delivery_color_dtls ($field_array) values $data_array";die;
		
		$DeliveryrID=$dtlsrID=true;
		if(str_replace("'","",$txt_system_id)=="")
		{
			$DeliveryrID=sql_insert("pro_cut_delivery_mst",$field_array_delivery,$data_array_delivery,1);
		}
		else
		{
			$DeliveryrID=sql_update("pro_cut_delivery_mst",$field_array_delivery,$data_array_delivery,"id",str_replace("'","",$txt_system_id),1);
		}
		
		$rID=sql_insert(" pro_cut_delivery_order_dtls",$field_array1,$data_array1,1);

		if(str_replace("'","",$sewing_production_variable)==2 || str_replace("'","",$sewing_production_variable)==3)
		{
 			$dtlsrID=sql_insert("pro_cut_delivery_color_dtls",$field_array,$data_array,1);
		} 
		 
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
		
			if(str_replace("'","",$sewing_production_variable)!=1)
			{
				if($DeliveryrID && $rID && $dtlsrID)
				{
					mysql_query("COMMIT");  
					echo "0**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
			else
			{
				if($DeliveryrID && $rID)
				{
					mysql_query("COMMIT");  
					echo "0**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if(str_replace("'","",$sewing_production_variable)!=1)
			{
				if($DeliveryrID && $rID && $dtlsrID)
				{
					oci_commit($con); 
					echo "0**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					oci_rollback($con);
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
			else
			{
				if($DeliveryrID && $rID)
				{
					oci_commit($con);  
					echo "0**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					oci_rollback($con);
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
		}
		disconnect($con);
		die;
	}
  	else if ($operation==1) // Update Here End------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		
		$delivery_mst_id=str_replace("'","",$txt_system_id);
		$mrr_no=str_replace("'","",$txt_system_no);
		$mrr_no_challan=str_replace("'","",$txt_challan_no);
		
		$field_array_delivery="company_id*location_id*challan_no*delivery_date*deliver_basis*knitting_source*knitting_company*updated_by*update_date";
		$data_array_delivery="".$cbo_company_name."*".$cbo_location_name."*".$txt_challan_no."*".$txt_ex_factory_date."*".$cbo_delivery_basis."*".$cbo_knitting_source."*".$cbo_knitting_company."*".$user_id."*'".$pc_date_time."'";
		
		//  pro_cut_delivery_order_dtls table data entry here
		
		$field_array1="garments_nature*buyer_id*location*cut_delivery_date*cut_delivery_qnty*total_carton_qnty*challan_no*remarks*entry_break_down_type*updated_by*update_date";
		$data_array1="".$garments_nature."*".$cbo_buyer_name."*".$cbo_location_name."*".$txt_ex_factory_date."*".$txt_ex_quantity."*".$txt_total_carton_qnty."*".$txt_challan_no."*".$txt_remark."*".$sewing_production_variable."*".$user_id."*'".$pc_date_time."'";
		

		
		
		if(str_replace("'","",$sewing_production_variable)!=1 && str_replace("'","",$txt_mst_id)!='')// check is not gross level
		{
			// pro_cut_delivery_color_dtls table entry here ----------------------------------///
			$dtlsrDelete = execute_query("delete from pro_cut_delivery_color_dtls where mst_id=$txt_mst_id",1);
			$field_array="id,mst_id,color_size_break_down_id,production_qnty,bundle_qnty";
			
			if(str_replace("'","",$sewing_production_variable)==2)//color level wise
			{		
				$color_sizeID_arr=sql_select( "select id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name and color_mst_id!=0 order by id" );
				$colSizeID_arr=array(); 
				foreach($color_sizeID_arr as $val){
					$index = $val[csf("color_number_id")];
					$colSizeID_arr[$index]=$val[csf("id")];
				}	
				// $colorIDvalue concate as colorID*Value**colorID*Value -------------------------//
				$rowEx = explode("**",$colorIDvalue); 
				$dtls_id=return_next_id("id", "pro_cut_delivery_color_dtls", 1);
				$data_array="";$j=0;
				foreach($rowEx as $rowE=>$val)
				{
					$colorSizeNumberIDArr = explode("*",$val);
					
					if($j==0)$data_array = "(".$dtls_id.",".$txt_mst_id.",'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."','".$colorSizeNumberIDArr[2]."')";
					else $data_array .= ",(".$dtls_id.",".$txt_mst_id.",'".$colSizeID_arr[$colorSizeNumberIDArr[0]]."','".$colorSizeNumberIDArr[1]."','".$colorSizeNumberIDArr[2]."')";
					$dtls_id=$dtls_id+1;							
					$j++;								
				}
			}
			
			if(str_replace("'","",$sewing_production_variable)==3)//color and size wise
			{		
				$color_sizeID_arr=sql_select( "select id,size_number_id,color_number_id from wo_po_color_size_breakdown where po_break_down_id=$hidden_po_break_down_id and item_number_id=$cbo_item_name and country_id=$cbo_country_name order by size_number_id,color_number_id" );
				$colSizeID_arr=array(); 
				foreach($color_sizeID_arr as $val){
					$index = $val[csf("size_number_id")].$val[csf("color_number_id")];
					$colSizeID_arr[$index]=$val[csf("id")];
				}	
				
				//	colorIDvalue concate as sizeID*colorID*value***sizeID*colorID*value	--------------------------// 
				$rowEx = explode("***",$colorIDvalue); 
				$dtls_id=return_next_id("id", "pro_cut_delivery_color_dtls", 1);
				$data_array="";$j=0;
				foreach($rowEx as $rowE=>$valE)
				{
					$colorAndSizeAndValue_arr = explode("*",$valE);
					$sizeID = $colorAndSizeAndValue_arr[0];
					$colorID = $colorAndSizeAndValue_arr[1];				
					$colorSizeValue = $colorAndSizeAndValue_arr[2];
					$colorSizebundle = $colorAndSizeAndValue_arr[3];
					$index = $sizeID.$colorID;
					
					if($j==0)$data_array = "(".$dtls_id.",".$txt_mst_id.",'".$colSizeID_arr[$index]."','".$colorSizeValue."','".$colorSizebundle."')";
					else $data_array .= ",(".$dtls_id.",".$txt_mst_id.",'".$colSizeID_arr[$index]."','".$colorSizeValue."','".$colorSizebundle."')";
					$dtls_id=$dtls_id+1;
					$j++;
				}
			}
			
			$deliveryrID=sql_update("pro_cut_delivery_mst",$field_array_delivery,$data_array_delivery,"id","".$delivery_mst_id."",1);
			$rID=sql_update(" pro_cut_delivery_order_dtls",$field_array1,$data_array1,"id","".$txt_mst_id."",1);
			$dtlsrID=true;
			if(str_replace("'","",$sewing_production_variable)==2 || str_replace("'","",$sewing_production_variable)==3)
			{
				$dtlsrID=sql_insert("pro_cut_delivery_color_dtls",$field_array,$data_array,1);
			} 
		}//end cond
 		
		
		if($db_type==0)
		{
			if(str_replace("'","",$sewing_production_variable)!=1)
			{
				if($deliveryrID && $rID && $dtlsrID && $dtlsrDelete)
				{
					mysql_query("COMMIT");  
					echo "1**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
			else
			{
				if($deliveryrID && $rID)
				{
					mysql_query("COMMIT");  
					echo "1**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if(str_replace("'","",$sewing_production_variable)!=1)
			{
				if($deliveryrID && $rID && $dtlsrID && $dtlsrDelete)
				{
					oci_commit($con); 
					echo "1**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					oci_rollback($con);
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
			else
			{
				if($rID && $deliveryrID)
				{
					oci_commit($con); 
					echo "1**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no."**".$mrr_no_challan;
				}
				else
				{
					oci_rollback($con);
					echo "10**".str_replace("'","",$hidden_po_break_down_id);
				}
			}
		}
		disconnect($con);
		die;
	}
 
	else if ($operation==2)  // Delete Here---------------------------------------------------------- 
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		$delivery_mst_id=str_replace("'","",$txt_system_id);
		$mrr_no=str_replace("'","",$txt_system_no);
		$mrr_no_challan=str_replace("'","",$txt_challan_no);
  		
		$rID = sql_delete(" pro_cut_delivery_order_dtls","status_active*is_deleted","0*1",'id',$txt_mst_id,1);
		$dtlsrID = sql_delete("pro_cut_delivery_color_dtls","status_active*is_deleted","0*1",'mst_id',$txt_mst_id,1);
 		
 		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$hidden_po_break_down_id); 
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID)
			{
				oci_commit($con); 
				echo "2**".str_replace("'","",$hidden_po_break_down_id)."**".str_replace("'","",$delivery_mst_id)."**".$mrr_no; 
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$hidden_po_break_down_id); 
			}
		}
		disconnect($con);
		die;
	}
}


if($action=="cut_delivery_print")
{
	extract($_REQUEST);
	$data=explode('*',$data);
	echo load_html_head_contents("Cutting Delivery Info","../", 1, 1, $unicode,'','');
	//print_r ($data);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$buyer_library=return_library_array( "select id, short_name from   lib_buyer", "id", "short_name"  );
	$country_library=return_library_array( "select id, country_name from lib_country", "id", "country_name"  );
	$order_library=return_library_array( "select id, po_number from wo_po_break_down", "id", "po_number"  );
	$color_library=return_library_array( "select id, color_name from  lib_color", "id", "color_name"  );
	$size_library=return_library_array( "select id, size_name from lib_size", "id", "size_name"  );
	
	if($db_type==0)
	{
		$delivery_mst_sql=sql_select("select id,challan_no,sys_number_prefix_num,knitting_source,knitting_company,delivery_date,company_id,DATE_FORMAT(insert_date, '%y') as insert_year from pro_cut_delivery_mst where id=$data[1]");
	}
	else if($db_type==2)
	{
		$delivery_mst_sql=sql_select("select id,challan_no,sys_number_prefix_num,knitting_source,knitting_company,delivery_date,company_id,TO_CHAR(insert_date,'YY') as insert_year from pro_cut_delivery_mst where id=$data[1]");
	}
	foreach($delivery_mst_sql as $row)
	{
		$challan_no=$row[csf("challan_no")];
		$sys_number_prefix_num=$row[csf("sys_number_prefix_num")];
		$knitting_source=$row[csf("knitting_source")];
		$knitting_company=$row[csf("knitting_company")];
		$delivery_date=$row[csf("delivery_date")];
		$com_id=$row[csf("company_id")];
		$insert_year=$row[csf("insert_year")];
	}
	
	$com_id=str_pad($com_id,2,'0',STR_PAD_LEFT);
	$cln_no=str_pad($challan_no,8,'0',STR_PAD_LEFT);
	$insert_year=str_pad($insert_year,2,'0',STR_PAD_LEFT);
	$barcode_creat_id=$com_id.$cln_no.$insert_year;

	$image_location=return_field_value("image_location","common_photo_library","file_type=1 and form_name='company_details' and master_tble_id='$data[0]'","image_location");
	
?>
<div style="width:910px;">
    <table width="900" cellspacing="0" style="margin-bottom:20px;">
        <tr>
            <td rowspan="3" align="center" width="70"><img src="../<?php echo $image_location; ?>" height="50" width="60"></td>
            <td align="center" width="600"  style="font-size:xx-large; "><strong><?php echo $company_library[$data[0]]; ?></strong></td>
            <td id="barcode_img_id" align="right" style="font-size:24px" rowspan="3"></td>
        </tr>
        <tr>
        	<td align="center" style="font-size:x-large;">Cut Panel Delivery Challan to Input</td>
        </tr>
        <tr>
        	<td align="center" style="font-size:x-large;">Cutting Section</td>
        </tr>
    </table>
    <table width="900" cellspacing="0" style="margin-bottom:20px;">
        <tr>
        	<td width="30" valign="top" align="right">To : &nbsp;</td> 
            <td width="300" valign="top">
			<?php
				if($knitting_source==1) echo $company_library[$knitting_company];  else echo $supplier_library[$knitting_company];
				
			?>
            </td>
            <td width="100" valign="top" align="right">Challan No : &nbsp;</td>
            <td width="120" valign="top"><?php echo $challan_no; ?> </td>
            <td width="100" valign="top" align="right">Date : &nbsp;</td>
            <td valign="top"><?php echo $delivery_date; ?> </td>
        </tr>
    </table>
    <br>
        <?php
		$sql_color_size=sql_select("select id, color_number_id, size_number_id from  wo_po_color_size_breakdown where status_active=1");
		foreach($sql_color_size as $row)
		{
			$color_size_arr[$row[csf("id")]]['color_number_id']=$row[csf("color_number_id")];
			$color_size_arr[$row[csf("id")]]['size_number_id']=$row[csf("size_number_id")];
		}
		//listagg(CAST(b.po_breakdown_id as VARCHAR(4000)),',') within group (order by b.po_breakdown_id) as po_id
		$sql="select a.id as mst_id,a.challan_no,b.id as dtls_order_id,b.buyer_id, b.po_break_down_id, b.item_number_id, b.country_id, b.remarks, c.id as dtls_color_id, c. 	color_size_break_down_id, c.production_qnty, c.bundle_qnty from pro_cut_delivery_mst a, pro_cut_delivery_order_dtls b,  pro_cut_delivery_color_dtls c where a.id=b.delivery_mst_id and b.id=c.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and a.id=$data[1] order by a.id,b.id,c.id";
		//echo $sql;
		$result=sql_select($sql);
			
		?> 
    <table cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" rules="all">
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="70">Buyer</th>
            <th width="100">Order</th>
            <th width="100">Item</th>
            <th width="100">Color</th>
            <th width="70">Cut No</th>
            <th width="100">Country</th>
            <th width="80">Size</th>
            <th width="80">Qty(Pcs)</th>
            <th width="50">No of bundle</th>
            <th> Remarks </th>
        </thead>
        <tbody>
		<?php
        $i=1;
        $tot_qnty=array();
        foreach($result as $row)
        {
            if ($i%2==0)  
                $bgcolor="#E9F3FF";
            else
                $bgcolor="#FFFFFF";
            $color_count=count($cid);
            ?>
            <tr bgcolor="<?php echo $bgcolor; ?>">
                <td align="center"><p><?php echo $i;  ?></p></td>
                <td><p><?php echo $buyer_library[$row[csf("buyer_id")]]; ?>&nbsp;</p></td>
                <td><p><?php echo $order_library[$row[csf("po_break_down_id")]]; ?>&nbsp;</p></td>
                <td><p><?php echo $garments_item[$row[csf("item_number_id")]]; ?>&nbsp;</p></td>
                <td><p><?php echo $color_library[$color_size_arr[$row[csf("color_size_break_down_id")]]['color_number_id']]; ?>&nbsp;</p></td>
                <td><p><?php //echo $row[csf("item_number_id")]; ?>&nbsp;</p></td>
                <td><p><?php echo $country_library[$row[csf("country_id")]]; ?>&nbsp;</p></td>
                <td><p><?php echo $size_library[$color_size_arr[$row[csf("color_size_break_down_id")]]['size_number_id']]; ?>&nbsp;</p></td>
                <td align="right" style="padding-right:3px;"><p><?php echo number_format($row[csf("production_qnty")],0,"",""); $tot_cut_qnty +=$row[csf("production_qnty")]; ?></p></td>
                <td align="right" style="padding-right:3px;"><p><?php echo number_format($row[csf("bundle_qnty")],0,"",""); $tot_bundle +=$row[csf("bundle_qnty")]; ?></p></td>
                <td><p><?php echo $row[csf("remarks")]; ?>&nbsp;</p></td>
            </tr>
            <?php
            $i++;
        }
        ?>
        </tbody>
        
        <tr>
            <td colspan="8" align="right"><strong>Grand Total :</strong></td>
            <td align="right"><?php echo number_format($tot_cut_qnty,0,"",""); ?></td>
            <td align="right"><?php echo number_format($tot_bundle,0,"",""); ?></td>
            <td align="right">&nbsp;</td>
        </tr>                           
    </table>
	</div>
		 <?php
            echo signature_table(54, $data[0], "900px");
         ?>
	</div>
     <script type="text/javascript" src="../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess )
	{
		   
			var btype = 'code39';
			var renderer ='bmp';
			 
			var settings = {
			  output:renderer,
			  bgColor: '#FFFFFF',
			  color: '#000000',
			  barWidth: 1,
			  barHeight: 30,
			  moduleSize:5,
			  posX: 10,
			  posY: 20,
			  addQuietZone: 1
			};
			 valuess = {code:valuess, rect: false};
			
			$("#barcode_img_id").show().barcode(valuess, btype, settings);
		  
	} 
  
	generateBarcode('<?php echo $barcode_creat_id; ?>');
	 
	 
	 </script>
            
<?php
exit();	
}
?>