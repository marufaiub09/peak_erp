-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_issue_master_history`
--

DROP TABLE IF EXISTS `inv_issue_master_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_issue_master_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approve_no` int(11) NOT NULL DEFAULT '0',
  `issue_id` int(11) NOT NULL DEFAULT '0',
  `issue_number_prefix` varchar(15) NOT NULL,
  `issue_number_prefix_num` int(11) NOT NULL,
  `issue_number` varchar(20) NOT NULL,
  `issue_basis` tinyint(3) NOT NULL DEFAULT '0',
  `issue_purpose` int(11) NOT NULL,
  `entry_form` tinyint(3) NOT NULL DEFAULT '0',
  `item_category` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL DEFAULT '0' COMMENT 'not yarn',
  `buyer_id` int(11) NOT NULL DEFAULT '0',
  `buyer_job_no` varchar(20) NOT NULL,
  `style_ref` varchar(30) NOT NULL,
  `booking_id` int(11) NOT NULL DEFAULT '0',
  `booking_no` varchar(20) NOT NULL,
  `req_no` varchar(50) NOT NULL COMMENT 'for general item issue',
  `batch_no` int(11) NOT NULL DEFAULT '0',
  `issue_date` date NOT NULL,
  `sample_type` int(11) NOT NULL,
  `knit_dye_source` int(11) NOT NULL DEFAULT '0',
  `knit_dye_company` int(11) NOT NULL DEFAULT '0',
  `challan_no` varchar(50) NOT NULL,
  `loan_party` int(11) NOT NULL DEFAULT '0',
  `lap_dip_no` varchar(50) NOT NULL DEFAULT '',
  `gate_pass_no` varchar(50) NOT NULL DEFAULT '',
  `item_color` int(11) NOT NULL DEFAULT '0',
  `color_range` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(450) NOT NULL,
  `received_id` int(11) NOT NULL DEFAULT '0',
  `received_mrr_no` varchar(20) NOT NULL,
  `other_party` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL COMMENT 'order id string',
  `is_approved` tinyint(4) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active 0:inactive',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Yes 0:No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_issue_master_history`
--

LOCK TABLES `inv_issue_master_history` WRITE;
/*!40000 ALTER TABLE `inv_issue_master_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `inv_issue_master_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
