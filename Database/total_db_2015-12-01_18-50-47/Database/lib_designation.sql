-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_designation`
--

DROP TABLE IF EXISTS `lib_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_des` int(11) NOT NULL DEFAULT '0',
  `system_designation` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_designation` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `custom_designation_local` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allowance_rate` float(8,2) NOT NULL,
  `allowance_treatment` tinyint(1) NOT NULL,
  `inserted_by` int(4) DEFAULT NULL COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(4) DEFAULT NULL COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `level` (`level_des`),
  KEY `custom_designation` (`custom_designation`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_designation`
--

LOCK TABLES `lib_designation` WRITE;
/*!40000 ALTER TABLE `lib_designation` DISABLE KEYS */;
INSERT INTO `lib_designation` VALUES (1,1,'Chairman','Chairman','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(2,2,'Managing Director','Managing Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(3,3,'Deputy Managing Director','Deputy Managing Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(4,4,'Director','Director','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(5,5,'Senior Deputy Director','Senior Deputy Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(6,6,'Deputy Director','Deputy Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(7,7,'Senior Assistant Director','Senior Assistant Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(8,8,'Assistant Director','Assistant Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(9,9,'Junior Director','Junior Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(10,10,'Chief Executive Officer','CEO','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(11,11,'Executive Director','Executive Director',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(12,12,'Chief Functional Officer','Adivser (finance & Accounts)','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(13,13,'Assistant Chief Functional Officer','Assistant Chief Functional Officer',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(14,14,'Senior General Manager','Senior General Manager',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(15,15,'General Manager',' GM (Corporate)','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(16,16,'Senior Deputy General Manager','Senior Deputy General Manager',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(17,17,'Deputy General Manager','Deputy General Manager',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(18,18,'Senior Assistant General Manager','Senior Assistant General Manager','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(19,19,'Assistant General Manager',' AGM(Export)','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(20,20,'Junior General Manager','Junior General Manager',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(21,21,'Senior Manager',' Sr. Manager (A&F-Group)','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(22,22,'Manager','Maintenance Manager','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',0,0,0),(23,23,'Senior Deputy Manager','Senior Deputy Manager',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(24,24,'Deputy Manager',' Deputy R & D Manager','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(25,25,'Senior Assistant Manager','Senior Assistant Manager',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(26,26,'Assistant Manager',' Asst HR Manager','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(27,27,'Junior Manager','Junior Manager','জুনিয়র অফিসার',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(28,28,'Senior Officer','Network Artichect','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(29,29,'Officer','Merchandiser','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(30,30,'Senior Deputy Officer','Senior Deputy Officer',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(31,31,'Deputy Officer','Deputy Officer',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(32,32,'Senior Assistant Officer','Senior Assistant Officer',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(33,33,'Assistant Officer','Assistant Patern Master','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(34,34,'Junior Officer','Jr. R&D Officer','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',0,0,0),(35,35,'Senior Executive','Sr. R&D Executive','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',0,0,0),(36,36,'Executive',' R&D Executive','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',0,0,0),(37,37,'Senior Deputy Executive','Senior Deputy Executive',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(38,38,'Deputy Executive','Deputy Executive',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(39,39,'Senior Assistant Executive','Senior Assistant Executive',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(40,40,'Assistant Executive','Asst IT Executive','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(41,41,'Junior Executive','Jr. R&D Executive','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',0,0,0),(42,42,'Trainee','Trainee Merchandiser','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(43,46,'Support Staff','Driver','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(44,45,'Security Staff','Security Staff','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',0,0,0),(45,43,'Staff','Counsellor','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(46,47,'Worker','Worker',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(47,1,'Chairman','Deputy Incharge','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(48,47,'Worker','Operator',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(49,47,'Worker','Helper',NULL,0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',1,0,0),(50,36,'Executive','Executive','',0.00,0,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00',0,0,0);
/*!40000 ALTER TABLE `lib_designation` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
