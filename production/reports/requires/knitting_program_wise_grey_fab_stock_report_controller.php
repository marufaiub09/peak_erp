<?php

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
//--------------------------------------------------------------------------------------------------------------------
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$yarn_count_library=return_library_array( "select id, yarn_count from lib_yarn_count", "id", "yarn_count"  );
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 120, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "- All Buyer -", $selected, "" );     	 
	exit();
}
if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<script>
	
		function js_set_value(str)
		{
			var splitData = str.split("_");
			//alert (splitData[1]);
			$("#hide_job_id").val(splitData[0]); 
			$("#hide_job_no").val(splitData[1]); 
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Job No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:100px;" onClick="reset_form('styleRef_form','search_div','','','','');"></th> 					<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                    <input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+'<?php echo $cbo_year_id; ?>'+'**'+'<?php echo $cbo_month_id; ?>', 'create_job_no_search_list_view', 'search_div', 'knitting_program_wise_grey_fab_stock_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}
if($action=="create_job_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	$year_id=$data[4];
	$month_id=$data[5];
	//echo $month_id;
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	if($data[1]==0)
	{
	if ($_SESSION['logic_erp']["data_level_secured"]==1)
	{
	if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
	}
	else
	{
	$buyer_id_cond="";
	}
	}
	else
	{
	$buyer_id_cond=" and buyer_name=$data[1]";
	}
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";
	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	//$year="year(insert_date)";
	if($db_type==0) $year_field="YEAR(insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year";
	else $year_field="";
	if($db_type==0)
	{
	if($year_id!=0) $year_cond=" and year(insert_date)=$year_id"; else $year_cond="";	
	}
	else if($db_type==2)
	{
	$year_field_con=" and to_char(insert_date,'YYYY')";
	if($year_id!=0) $year_cond="$year_field_con=$year_id"; else $year_cond="";	
	}
	//if($month_id!=0) $month_cond=" and month(insert_date)=$month_id"; else $month_cond="";
	$arr=array (0=>$company_arr,1=>$buyer_arr);
	$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no, $year_field from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $year_cond  order by job_no";
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","600","240",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0,0','') ;
	exit(); 
} // Job Search end
if($action=="order_no_search_popup")
{
	echo load_html_head_contents("Order No Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<script>
		var selected_id = new Array; var selected_name = new Array;
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			if (str!="") str=str.split("_");
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_order_id').val( id );
			$('#hide_order_no').val( name );
		}
    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:780px;">
            <table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Order No</th>
                    <th>Shipment Date</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_order_no" id="hide_order_no" value="" />
                    <input type="hidden" name="hide_order_id" id="hide_order_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Order No",2=>"Style Ref",3=>"Job No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 110, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
                            <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
                        </td>	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value, 'create_order_no_search_list_view', 'search_div', 'buyer_order_wise_knitting_status_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    	</td>
                    </tr>
                    <tr>
                        <td colspan="5" height="20" valign="middle"><?php echo load_month_buttons(1); ?></td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}
if($action=="create_order_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	echo $data[1];
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";
	}
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";
	if($search_by==1) 
		$search_field="b.po_number"; 
	else if($search_by==2) 
		$search_field="a.style_ref_no"; 	
	else 
		$search_field="a.job_no";
	$start_date =trim($data[4]);
	$end_date =trim($data[5]);	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,"yyyy-mm-dd", "-")."' and '".change_date_format($end_date,"yyyy-mm-dd", "-")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,'','',1)."' and '".change_date_format($end_date,'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	$arr=array(0=>$company_arr,1=>$buyer_arr);
	$sql= "select b.id, $year_field a.job_no, a.job_no_prefix_num, a.company_name, a.buyer_name, a.style_ref_no, b.po_number, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $date_cond order by b.id, b.pub_shipment_date";
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No, Po No, Shipment Date", "80,130,50,60,130,130","760","220",0, $sql , "js_set_value", "id,po_number", "", 1, "company_name,buyer_name,0,0,0,0,0", $arr , "company_name,buyer_name,year,job_no_prefix_num,style_ref_no,po_number,pub_shipment_date", "",'','0,0,0,0,0,0,3','',1) ;
   exit(); 
}
if($action=="report_generate")
{
		$process = array( &$_POST );
		extract(check_magic_quote_gpc( $process )); 
		$company_name= str_replace("'","",$cbo_company_name);
		$program_no= str_replace("'","",$txt_program_no);
		//echo $program_no;
		$type = str_replace("'","",$cbo_type);
		if(str_replace("'","",$cbo_buyer_name)==0)
		{
			if ($_SESSION['logic_erp']["data_level_secured"]==1)
			{
				if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
			}
			else
			{
				$buyer_id_cond="";
			}
		}
		else
		{
			$buyer_id_cond=" and a.buyer_name in (".str_replace("'","",$cbo_buyer_name).")";
		}
		if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
		{
		 if($db_type==0)
			{
				$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd","");
				$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd","");
			}
			else if($db_type==2)
			{
				$start_date=change_date_format(str_replace("'","",$txt_date_from),"","",1);
				$end_date=change_date_format(str_replace("'","",$txt_date_to),"","",1);
			}
		$date_cond=" and d.program_date between '$start_date' and '$end_date'";
		}
	
		$job_no=str_replace("'","",$txt_job_no);
		if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in ($job_no) ";
		if ($program_no=="") $program_no_cond=""; else $program_no_cond=" and d.id in ($program_no) ";
	//if(trim($program_no)!="") $program_cond="%".trim($program_no); else $program_cond="%%";
		if(str_replace("'","",trim($txt_order_no))=="")
		{
			$po_cond="";
		}
		else
		{
			if(str_replace("'","",$hide_order_id)!="")
			{
				$po_id=str_replace("'","",$hide_order_id);
				$po_cond="and b.id in(".$po_id.")";
			}
			else
			{
				$po_number=trim(str_replace("'","",$txt_order_no))."%";
				$po_cond="and b.po_number like '$po_number'";
			}
		}
	$yarn_lot_arr=array();
	$grey_qty_arr=array();
		if($db_type==0)
		{
		$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id,group_concat(distinct(a.yarn_lot)) as yarn_lot,sum(a.grey_receive_qnty) as grey_receive_qnty,a.yarn_count as yarn_count,sum(a.no_of_roll)  as no_of_roll from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot!=0 group by a.prod_id, b.po_breakdown_id,a.yarn_count");
		}
		else if($db_type==2)
		{//LISTAGG(CAST(d.responsible_dept AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY d.responsible_dept)  as responsible_dept,
		$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id,LISTAGG(CAST(a.yarn_lot AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY a.yarn_lot)  as yarn_lot,sum(a.grey_receive_qnty) as grey_receive_qnty,a.yarn_count as yarn_count,sum(a.no_of_roll)  as no_of_roll from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot is not null group by b.po_breakdown_id,a.yarn_count,a.prod_id");
		}
		foreach($yarn_lot_data as $rows)
		{
		$yarn_lot=explode(",",$rows[csf('yarn_lot')]);
		$yarn_lot_arr[$rows[csf('po_breakdown_id')]]['lot']=implode(",",array_unique($yarn_lot));
		$yarn_lot_arr[$rows[csf('po_breakdown_id')]]['ycount']=$rows[csf('yarn_count')]; 
		$yarn_lot_arr[$rows[csf('po_breakdown_id')]]['roll']=$rows[csf('no_of_roll')]; 
		$grey_qty_arr[$rows[csf('po_breakdown_id')]]['grey_receive_qnty']=$rows[csf('grey_receive_qnty')]; 
		}
		$knitDataArr=array();
		if($db_type==0)
		{
			$knitting_dataArray=sql_select("select a.booking_id, group_concat(distinct(a.id)) as knit_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and  a.entry_form=2 and a.receive_basis=2 and a.status_active=1 and a.is_deleted=0");
		}
		else
		{
			$knitting_dataArray=sql_select("select a.booking_id, LISTAGG(a.id, ',') WITHIN GROUP (ORDER BY a.id) as knit_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and item_category=13 and a.entry_form=2 and a.receive_basis=2 and b.status_active=1 and b.is_deleted=0 group by a.booking_id");	
		}
		foreach($knitting_dataArray as $row)
		{
			$knitDataArr[$row[csf('booking_id')]]['qnty']=$row[csf('knitting_qnty')];
			$knitDataArr[$row[csf('booking_id')]]['knit_id']=$row[csf('knit_id')];
		}
		$recv_array=array();
		$sql_recv=sql_select("select a.booking_id,sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and item_category=13 and a.entry_form=22 and a.receive_basis=9 and b.status_active=1 and b.is_deleted=0 group by a.booking_id");
		foreach($sql_recv as $row)
		{
			$recv_array[$row[csf('booking_id')]]=$row[csf('knitting_qnty')];
		}
		$knitting_recv_qnty_array=array(); $prod_id_arr=array();
		$sql_prod=sql_select("select a.id, a.booking_id, sum(b.grey_receive_qnty) as knitting_qnty, max(trans_id) as trans_id from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and item_category=13 and a.entry_form=2 and a.receive_basis=2 and b.status_active=1 and b.is_deleted=0 group by a.id, a.booking_id");
		foreach($sql_prod as $row)
		{
			if($row[csf('trans_id')]>0)
			{
				$knitting_recv_qnty_array[$row[csf('booking_id')]]+=$row[csf('knitting_qnty')];
			}
			else
			{ 
				$prod_id_arr[$row[csf('booking_id')]].=$row[csf('id')].",";
			}
		}
		//echo $prod_id_arr[2694];die;
		$knit_issue_arr=array();
		$sql_data=sql_select( "select b.program_no,b.remarks, sum(b.issue_qnty) as knitting_issue_qnty from  inv_issue_master a,inv_grey_fabric_issue_dtls b where a.id=b.mst_id and a.item_category=13 and a.entry_form=16 and a.issue_basis=3 and b.status_active=1 and b.is_deleted=0 group by b.program_no,b.remarks ");
	foreach($sql_data as $row)
		{
			$knit_issue_arr[$row[csf('program_no')]]['qnty']=$row[csf('knitting_issue_qnty')];
			$knit_issue_arr[$row[csf('program_no')]]['knit_id']=$row[csf('knit_id')];
			$knit_issue_arr[$row[csf('program_no')]]['remarks']=$row[csf('remarks')];
		}
		$booking_arr=array();
		$sql_book=sql_select("select  a.booking_no,sum(a.grey_fab_qnty) as grey_fab_qnty from wo_booking_dtls a,wo_booking_mst b where a.booking_no=b.booking_no and b.is_short=2 and b.booking_type=1 and  a.status_active=1 and a.is_deleted=0 group by a.booking_no");
		foreach($sql_book as $row_b)
		{
			//$yarn_lot=explode(",",$rows[csf('yarn_lot')]);
			$booking_arr[$row_b[csf('booking_no')]]['grey_fab_qnty']=$row_b[csf('grey_fab_qnty')];
		}
	ob_start();
?>
<fieldset style="width:1730px;">
 	<table width="1730" cellspacing="0" cellpadding="0" border="0" rules="all" >
            <tr class="form_caption">
                <td colspan="20" align="center" style="border:none;font-size:16px; font-weight:bold"> <?php echo $report_title; ?></td>
            </tr>
            <tr class="form_caption">
                <td colspan="20" align="center"><?php echo $company_library[$company_name]; ?></td>
            </tr>
        </table>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1730" class="rpt_table" >
        <thead>
            <th width="40">SL</th>
            <th width="80">Prog. Date</th>
            <th width="100">Buyer</th>
            <th width="70">Job No</th>
            <th width="110">Order No</th>
            <th width="80">MC/F.Dia</th>
            <th width="70">YCount</th>
            <th width="70">Lot</th>
            <th width="150">Fab. Description</th>
            <th width="100">Fab. Color</th>
            <th width="60">FGSM</th>
            <th width="70">SL</th>
            <th width="100">Fab.Booking No</th>
            <th width="80">Book Qty/KG</th>
            <th width="70">Prog. No</th>
            <th width="80">Prog Qty/kg</th>
            <th width="60">Roll</th>
            <th width="80">Receive Qty(kg)</th>
            <th width="80">Delivery Qty(kg)</th>
            <th width="80">Stock Qty(kg)</th>
            <th width="">Remarks</th>
        </thead>
    </table>
    <div style="width:1750px; overflow-y:scroll; max-height:450px;" id="scroll_body">
	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1730" class="rpt_table" id="tbl_list_search">
		 <?php
         $sql="select a.job_no_prefix_num,d.id as prog_no,d.program_date, a.job_no, a.company_name, a.buyer_name, b.id as po_id, d.machine_dia,b.po_number,c.gsm_weight,sum(c.program_qnty) as program_qnty ,d.stitch_length,d.color_id,e.booking_no,e.fabric_desc from ppl_planning_entry_plan_dtls c, ppl_planning_info_entry_dtls d ,ppl_planning_info_entry_mst e,wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and e.id=d.mst_id and c.dtls_id=d.id and c.po_id=b.id and a.company_name=$company_name  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $buyer_id_cond $po_cond $job_no_cond $program_no_cond $date_cond group by d.id,b.id,d.machine_dia,d.program_date,e.booking_no,e.fabric_desc,d.color_id,c.gsm_weight,d.stitch_length, a.job_no, a.job_no_prefix_num, a.company_name, a.buyer_name, b.po_number, a.insert_date order by a.buyer_name, a.job_no, b.id";
       //echo $sql;
        $nameArray=sql_select( $sql );
        $i=1; 
         foreach($nameArray as $row)
         {
         if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
		$po_no=$row[csf('po_id')];
        $booking_qty=$booking_arr[$row[csf('booking_no')]]['grey_fab_qnty'];
		//$grey_recv_qty=$grey_qty_arr[$po_no]['grey_receive_qnty'];
		$roll_no=$yarn_lot_arr[$po_no]['roll'];
       // echo $roll_no;
		$color_id=$row[csf('color_id')];
		$color=array_filter(explode(',',$color_id));
        $ylot=$yarn_lot_arr[$po_no]['lot'];
      	$y_count=$yarn_lot_arr[$po_no]['ycount'];
        $y_count_id=array_unique(explode(',',$y_count));
		//print_r($y_count_id);
        $yarn_count_value='';
        foreach($y_count_id as $val)
        {
            if($val>0)
            {
                if($yarn_count_value=='') $yarn_count_value=$yarn_count_library[$val]; else $yarn_count_value.=", ".$yarn_count_library[$val];
            } //print_r( $yarn_count_value);
        }
		 $color_id_value='';
        foreach($color as $cval)
        {
            if($cval>0)
            {
                if($color_id_value=='') $color_id_value=$color_library[$cval]; else $color_id_value.=", ".$color_library[$cval];
            }
        }
		$knit_issue_qty=$knit_issue_arr[$row[csf('prog_no')]]['qnty'];
		//echo $row[csf('prog_no')];
		$remark=$knit_issue_arr[$row[csf('prog_no')]]['remarks'];
		$tot_knit_grey_recv=$knit_recv_arr[$row[csf('prog_no')]]['qnty']+$knit_recv_arr[$row[csf('prog_no')]]['qnty'];
		$knitting_recv_qnty=$knitting_recv_qnty_array[$row[csf('prog_no')]];
		$knit_id=array_unique(explode(",",substr($prod_id_arr[$row[csf('prog_no')]],0,-1)));
		foreach($knit_id as $val)
		{
			$knitting_recv_qnty+=$recv_array[$val];
		}
         ?>
         <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
            <td width="40"><?php echo $i; ?></td>
            <td width="80"><p><?php echo change_date_format($row[csf('program_date')]); ?></p></td>
            <td width="100"><p><?php echo $buyer_library[$row[csf('buyer_name')]]; ?></p></td>
            <td width="70"><p><?php echo $row[csf('job_no_prefix_num')]; ?></p></td>
            <td width="110"><p><?php echo $row[csf('po_number')]; ?></p></td>
            <td width="80"><p><?php echo $row[csf('machine_dia')]; ?></p></td>
            <td width="70"><p><?php echo $yarn_count_value; ?></p></td>
            <td width="70" align="center"><p><?php echo $ylot; ?></p></td>
            <td width="150" align="center"><p><?php echo $row[csf('fabric_desc')]; ?></p></td>
            <td width="100"><p><?php echo $color_id_value; ?></p></td>
            <td width="60" align="center"><p><?php echo $row[csf('gsm_weight')]; ?></p></td>
            <td width="70" align="center"><p><?php echo $row[csf('stitch_length')];  ?></p></td>
            <td width="100"><p><?php echo $row[csf('booking_no')]; ?></p></td>
            <td width="80" align="right"><p><?php echo number_format($booking_qty,2); ?></p></td>
            <td width="70" align="center"><p><?php echo $row[csf('prog_no')]; ?></p></td>
            <td width="80" align="right"><p><?php echo number_format($row[csf('program_qnty')],2); ?></p></td>
            <td align="center" width="60"><p><?php echo $roll_no; ?></p></td>
            <td align="right" width="80"><p><a href='#report_details' onClick="openmypage_receive('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prog_no')]; ?>','<?php echo  $row[csf('booking_no')];?>','receive_grey_popup');"><?php echo number_format($knitting_recv_qnty,2,'.',''); ?> </a> </p></td>
            <td align="right" width="80"><a href='#report_details' onClick="openmypage_issue('<?php echo $row[csf('po_id')]; ?>','<?php echo $row[csf('prog_no')]; ?>','<?php echo  $row[csf('booking_no')];?>','issue_grey_popup');"><?php echo number_format($knit_issue_qty,2,'.',''); ?> </a> </td>
            <td align="right" width="80"><?php $tot_balance=$knitting_recv_qnty-$knit_issue_qty; echo number_format($tot_balance,2,'.',''); ?></td>
            <td><?php echo $remark; ?></td>
        </tr>
        <?php
		$total_booking_qty+=$booking_qty;
		$total_program_qnty+=$row[csf('program_qnty')];
		$total_grey_qty+=$knitting_recv_qnty;
		$total_knit_issue_qty+=$knit_issue_qty;
		$total_stockbalance+=$tot_balance;
       $i++;  
	 }
		?>
     </table>
     <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1730" class="rpt_table">
         <tfoot>
            <th width="40"></th>
            <th width="80"></th>
            <th width="100"></th>
            <th width="70"></th>
            <th width="110"></th>
            <th width="80"></th>
            <th width="70"></th>
            <th width="70"></th>
            <th width="150"></th>
            <th width="100"></th>
            <th width="60"></th>
            <th width="70"></th>
            <th width="100"></th>
            <th width="80"><?php echo number_format($total_booking_qty,2,'.',''); ?></th>
            <th width="70"><?php //echo number_format($total_prod_qnty,2,'.',''); ?></th>
            <th width="80"><?php echo number_format($total_program_qnty,2,'.',''); ?></th>
            <th width="60"></th>
            <th width="80"><?php echo number_format($total_grey_qty,2,'.',''); ?></th>
            <th width="80"><?php echo number_format($total_knit_issue_qty,2,'.',''); ?></th>
            <th width="80"><?php echo number_format($total_stockbalance,2,'.',''); ?></th>
            <th width=""></th>
        </tfoot>
     </table>
    </div>
</fieldset>
<?php
	exit();
}
if($action=="receive_grey_popup")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $po_id;
	?>
    <script>
	function print_window()
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
		d.close();
	}	
</script>	
<fieldset style="width:540px; margin-left:3px">
		<div align="center">
        <input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>
        <div id="report_container">
			<table border="1" class="rpt_table" rules="all" width="540" cellpadding="0" cellspacing="0" align="center">
                 <caption>
                    <b>Knit Grey Fabrics Received Info</b>
               </caption>
				<thead>
                    <th width="30">Sl</th>
                     <th width="80">Receive Date</th>
                     <th width="70">Prog. No</th>
                    <th width="100">Receive ID</th>
                     <th width="50">Receive Ch. No</th>
                     <th width="80">Receive Qty</th>
                    <th width="50">Roll</th>
                    <th width="50">Rack No</th>
				</thead>
                <tbody>
                <?php
					$i=1;
					$knitting_recv_qnty_array=array(); $prod_id_arr=array();
					$sql_prod=("select a.id, a.booking_id,b.no_of_roll as roll,b.rack,b.order_id,a.recv_number, a.receive_date,a.challan_no, sum(b.grey_receive_qnty) as knitting_qnty, max(trans_id) as trans_id from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and a.item_category=13 and a.entry_form=2 and  a.company_id='$companyID' and  a.booking_id='$prog_no'  and a.receive_basis=2 and b.status_active=1 and b.is_deleted=0 group by a.id, a.booking_id,b.rack,b.order_id,a.recv_number, a.receive_date,b.no_of_roll,a.challan_no");
					//echo $sql_prod;
					$data_prod=sql_select($sql_prod);
					$recv_id='';
					foreach($data_prod as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						//echo $row[csf('id')];
						if($row[csf('trans_id')]>0)
						{
						?>
                            <tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="80"><p><?php echo change_date_format($row[csf('receive_date')]); ?></p></td>
                            <td width="70"><p><?php echo $prog_no; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('recv_number')]; ?></p></td>
                            <td width="50"><p><?php echo $row[csf('challan_no')]; ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($row[csf('knitting_qnty')],2); ?></p></td>
                            <td width="50"><p><?php echo $row[csf('roll')]; ?></p></td>
                            <td width="50"><p><?php echo $row[csf('rack')]; ?></p></td>
                        </tr> 
						<?php
						$tot_qty+=$row[csf('knitting_qnty')];
						}
						else
						{
							if($recv_id=='') $recv_id= $row[csf('id')]; else $recv_id.=','.$row[csf('id')];
						}
						$i++;
					}
					//var_dump($knitting_recv_qnty_array[$row[csf('booking_id')]]);
					$sql_recv="select a.id, a.booking_id,b.no_of_roll as roll,b.rack,b.order_id,a.recv_number, a.receive_date,a.challan_no,a.booking_id, sum(b.grey_receive_qnty) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b where a.id=b.mst_id and a.item_category=13 and a.company_id='$companyID' and  a.booking_id in($recv_id) and a.entry_form=22 and a.receive_basis=9 and b.status_active=1 and b.is_deleted=0 group by a.booking_id,a.id, a.booking_id,b.no_of_roll,b.rack,b.order_id,a.recv_number, a.receive_date,a.challan_no";
					$data_recv=sql_select($sql_recv);
					foreach($data_recv as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
								?>
                            <tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="80"><p><?php echo change_date_format($row[csf('receive_date')]); ?></p></td>
                            <td width="70"><p><?php echo $prog_no; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('recv_number')]; ?></p></td>
                            <td width="50"><p><?php echo $row[csf('challan_no')]; ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($row[csf('knitting_qnty')],2); ?></p></td>
                            <td width="50"><p><?php echo $row[csf('roll')]; ?></p></td>
                            <td width="50"><p><?php echo $row[csf('rack')]; ?></p></td>
                        </tr> 
						<?php
						$tot_qty_gery+=$row[csf('knitting_qnty')];
						$i++;
					}
					$total_balance=$tot_qty_gery+$tot_qty;
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right">Total</td>
                        <td align="right"><?php echo number_format($total_balance,2); ?>&nbsp;</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
              </div>
        </div>
    </fieldset>
    <?php
	exit();
}
if($action=="issue_grey_popup")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	//echo $po_id;
	?>
    <script>
	function print_window()
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print"/><title></title></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
		d.close();
	}	
</script>	
<fieldset style="width:540px; margin-left:3px">
		<div align="center">
        <input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>
        <div id="report_container">
			<table border="1" class="rpt_table" rules="all" width="540" cellpadding="0" cellspacing="0" align="center">
                 <caption>
                    <b>Knit Grey Fabrics Received Info</b>
               </caption>
				<thead>
                    <th width="30">Sl</th>
                     <th width="80">Issue Date</th>
                     <th width="70">Prog. No</th>
                    <th width="100">Issue ID</th>
                     <th width="50">Issue Ch. No</th>
                     <th width="80">Delivery Qty</th>
                    <th width="50">Roll</th>
                    <th width="50">Rack No</th>
				</thead>
                <tbody>
                <?php
					$i=1;
					$mrr_sql=( "select a.issue_number,a.issue_date,a.challan_no,b.no_of_roll,b.rack,b.program_no, b.issue_qnty as knitting_issue_qnty from  inv_issue_master a,inv_grey_fabric_issue_dtls b where a.id=b.mst_id and a.item_category=13 and b.program_no='$prog_no' and a.entry_form=16 and a.issue_basis=3 and b.status_active=1 and b.is_deleted=0  group by b.program_no,b.issue_qnty,a.issue_number,a.issue_date,a.challan_no,b.no_of_roll,b.rack ");
					$dtlsArray=sql_select($mrr_sql);
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="80"><p><?php echo change_date_format($row[csf('issue_date')]); ?></p></td>
                            <td width="70"><p><?php echo $prog_no; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="50"><p><?php echo $row[csf('challan_no')]; ?></p></td>
                            <td width="80" align="right"><p><?php echo number_format($row[csf('knitting_issue_qnty')],2); ?></p></td>
                            <td width="50"><p><?php echo $row[csf('no_of_roll')]; ?></p></td>
                            <td width="50"><p><?php echo $row[csf('rack')]; ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('knitting_issue_qnty')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                        <td></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
              </div>
        </div>
    </fieldset>
    <?php
	exit();
}
?>