-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `subcon_payment_receive_dtls`
--

DROP TABLE IF EXISTS `subcon_payment_receive_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcon_payment_receive_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_id` int(11) NOT NULL DEFAULT '0',
  `bill_type` int(2) NOT NULL DEFAULT '0',
  `bill_id` int(11) NOT NULL DEFAULT '0',
  `bill_no` varchar(50) DEFAULT NULL,
  `bill_date` date DEFAULT NULL,
  `currency_id` int(11) NOT NULL DEFAULT '0',
  `bill_amount` double NOT NULL DEFAULT '0',
  `total_adjusted` double NOT NULL DEFAULT '0',
  `payment_balance` double NOT NULL DEFAULT '0',
  `exchenge_rate` double NOT NULL,
  `exchenge_amount` double NOT NULL,
  `inserted_by` tinyint(2) NOT NULL DEFAULT '0',
  `insert_date` date NOT NULL,
  `updated_by` tinyint(2) NOT NULL DEFAULT '0',
  `update_date` date NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcon_payment_receive_dtls`
--

LOCK TABLES `subcon_payment_receive_dtls` WRITE;
/*!40000 ALTER TABLE `subcon_payment_receive_dtls` DISABLE KEYS */;
INSERT INTO `subcon_payment_receive_dtls` VALUES (1,1,2,56,'PAL-KNT-15-00021','2015-04-27',1,3280,50,3230,1,50,1,'2015-05-12',1,'2015-05-12',1,0);
/*!40000 ALTER TABLE `subcon_payment_receive_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
