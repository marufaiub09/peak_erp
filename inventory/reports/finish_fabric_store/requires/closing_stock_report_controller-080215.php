<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_store")
{
	$data=explode('_',$data);
	echo create_drop_down( "cbo_store_name", 120, "select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id  and a.status_active=1 and a.is_deleted=0 and a.company_id=$data[0] and  b.category_type=$data[1] order by a.store_name","id,store_name", 1, "--Select Store--", 1, "",0 );
	exit();
	//select a.id,a.store_name from lib_store_location a,lib_store_location_category b where a.id=b.store_location_id and a.status_active=1 and a.is_deleted=0 and a.company_id=$data[0] and b.category_type=$data[1] order by a.store_name
}

if ($action=="item_account_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
	 var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#item_account_id').val( id );
		$('#item_account_val').val( ddd );
	} 
		  
	</script>
    <input type="hidden" id="item_account_id" />
    <input type="hidden" id="item_account_val" />
 <?php
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name");
	
	$sql="SELECT id,item_account,item_category_id,item_group_id,item_description,supplier_id from  product_details_master where item_category_id=$data[1] and status_active=1 and is_deleted=0"; 
	$arr=array(1=>$item_category,2=>$itemgroupArr,4=>$supplierArr);
	echo  create_list_view("list_view", "Item Account,Item Category,Item Group,Item Description,Supplier,Product ID", "70,110,150,150,100,70","780","400",0, $sql , "js_set_value", "id,item_description", "", 0, "0,item_category_id,item_group_id,0,supplier_id,0", $arr , "item_account,item_category_id,item_group_id,item_description,supplier_id,id", "",'setFilterGrid("list_view",-1);','0,0,0,0,0,0','',1) ;
	exit();
}

if ($action=="item_group_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data);  
?>	
    <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('item_name_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
        <input type="hidden" id="item_name_id" />
    <?php
	$sql="SELECT id,item_name from  lib_item_group where item_category=$data[1] and status_active=1 and is_deleted=0"; //id=$data[1] and
	
	echo  create_list_view("list_view", "Item Name", "350","500","330",0, $sql , "js_set_value", "id,item_name", "", 1, "0", $arr , "item_name", "periodical_purchase_report_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}

if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	if ($cbo_company_name==0) $company_id =""; else $company_id =" and a.company_id='$cbo_company_name'";
	if ($cbo_item_category_id==0) $item_category_id=""; else $item_category_id=" and b.item_category_id='$cbo_item_category_id'";
	if ($item_account_id==0) $item_account=""; else $item_account=" and a.prod_id in ($item_account_id)";
	if ($item_group_id==0) $group_id=""; else $group_id=" and a.item_group_id='$item_group_id'";
	if ($cbo_store_name==0){ $store_id="";}else{$store_id=" and a.store_id='$cbo_store_name'";}

	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$companyArr = return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0","id","company_name"); 
	$supplierArr = return_library_array("select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0","id","supplier_name"); 
	$storeArr = return_library_array("select id,store_name from lib_store_location where status_active=1 and is_deleted=0","id","store_name"); 
	$itemnameArr = return_library_array("select id,item_name from lib_item_creation where status_active=1 and is_deleted=0","id","item_name");
	$itemgroupArr = return_library_array("select id,item_name from  lib_item_group where status_active=1 and is_deleted=0","id","item_name");
	$determinaArr = return_library_array("select id,construction from  lib_yarn_count_determina_mst where status_active=1 and is_deleted=0","id","construction");
	$color_arr=return_library_array("select id,color_name from lib_color where status_active=1 and is_deleted=0","id","color_name");
	if($db_type==0) $select_from_date=change_date_format($from_date,'yyyy-mm-dd');
	if($db_type==2) $select_from_date=change_date_format($from_date,'','',1);
	else $select_from_date="";
	if($db_type==0) $select_from_to=change_date_format($to_date,'yyyy-mm-dd');
	if($db_type==2) $select_from_to=change_date_format($to_date,'','',1);
	else $select_from_to="";
	if($db_type==0) $select_groupby=" group by a.prod_id  order by a.prod_id,b.store_id, b.item_category_id, a.item_category ASC";
	if($db_type==2) $select_groupby=" group by a.prod_id,b.id,b.store_id,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description,b.item_size,b.color,b.detarmination_id,b.gsm,b.dia_width order by a.prod_id,b.store_id, b.item_category_id ASC";
	else $select_groupby="";
	
	$days_doh=array();
	if($db_type==2)
	{
	$returnRes="select prod_id, min(transaction_date) || ',' || max(transaction_date )  as trans_date from inv_transaction where transaction_type in (1,3) and  item_category in (2,3) and status_active=1 and is_deleted=0 group by prod_id ";
	$returnRes_result= sql_select($returnRes);
	}
	else
	{
	$returnRes="select prod_id, concat(min(transaction_date),',',max(transaction_date))  as trans_date from inv_transaction where transaction_type in (1,3) and  item_category in (2,3) and status_active=1 and is_deleted=0 group by prod_id ";
	$returnRes_result= sql_select($returnRes);
	//$returnRes=explode(",",return_field_value("concat(min(transaction_date),',',max(transaction_date))","inv_transaction","prod_id=".$row[csf("prod_id")]));
	$daysOnHand = datediff("d",$returnRes[1],date("Y-m-d"));
	
	}
	foreach($returnRes_result as $row_d)
	{
	$date_total=explode(",",$row_d[csf('trans_date')]);
	if($db_type==2)
		{
	$today= change_date_format(date("Y-m-d"),'','',1);	
	$daysOnHand = datediff("d",change_date_format($date_total[1],'','',1),$today);
		}
	else
		{
	$today= change_date_format(date("Y-m-d"));	
	$daysOnHand = datediff("d",change_date_format($date_total[1]),$today);
	
		}
	$days_doh[$row_d[csf('prod_id')]]['daysonhand']=$daysOnHand ;
	
	} 

	$prod_id_arr=array();
	$issue_sql = "select a.prod_id,
                        sum(case when a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as issue_total_opening,
						sum(case when a.transaction_type=2 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as issue,
						sum(case when a.transaction_type=3 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as receive_return,
						sum(case when a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_rate else 0 end) as rate 
						from inv_transaction a, inv_issue_master c
                        where a.mst_id=c.id and a.transaction_type in (2,3) and  a.item_category in (2,3) and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.prod_id";
						//echo $issue_sql;
	$issue_result = sql_select($issue_sql);
	foreach($issue_result as $row_arr)
	{
	$prod_id_arr[$row_arr[csf('prod_id')]]['issue_total_opening']=$row_arr[csf('issue_total_opening')];
	$prod_id_arr[$row_arr[csf('prod_id')]]['issue']=$row_arr[csf('issue')];
	$prod_id_arr[$row_arr[csf('prod_id')]]['receive_return']=$row_arr[csf('receive_return')];
	$prod_id_arr[$row_arr[csf('prod_id')]]['rate']=$row_arr[csf('rate')];
	} //var_dump($prod_id_arr);
	
	
	$sql="Select a.prod_id,b.id,b.store_id,b.item_category_id,b.item_group_id,b.sub_group_name,b.item_description,b.item_size,b.color,b.detarmination_id,b.gsm,b.dia_width,
		sum(case when a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as rcv_total_opening,
		sum(case when a.transaction_type=1 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as purchase,
		sum(case when a.transaction_type=3 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as receive_return
		from inv_transaction a, product_details_master b, inv_receive_master c where a.prod_id=b.id and a.mst_id=c.id and a.transaction_type in (1,3) and a.item_category in (2,3) and a.company_id=c.company_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $company_id $item_category_id $group_id $store_id $item_account $select_groupby";
	 	//echo  $sql;//die;
	$result = sql_select($sql);
	$i=1;
	ob_start();	
	?>
    <div> 
        <table style="width:1630px" border="1" cellpadding="2" cellspacing="0" class="" id="table_header_1" > 
            <thead>
                <tr class="form_caption" style="border:none;">
                    <td colspan="18" align="center" style="border:none;font-size:16px; font-weight:bold" ><?php echo $report_title; ?></td> 
                </tr>
                <tr style="border:none;">
                    <td colspan="18" align="center" style="border:none; font-size:14px;">
                       <b>Company Name : <?php echo $companyArr[$cbo_company_name]; ?></b>                               
                    </td>
                </tr>
                <tr style="border:none;">
                    <td colspan="18" align="center" style="border:none;font-size:12px; font-weight:bold">
                        <?php if($from_date!="" || $to_date!="")echo "From : ".change_date_format($from_date,'dd-mm-yyyy')." To : ".change_date_format($to_date,'dd-mm-yyyy')."" ;?>
                    </td>
                </tr>
            </thead>
        </table>
        <table style="width:1620px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" id="" > 
            <thead>
                <tr>
                    <th rowspan="2" width="40">SL</th>
                    <th colspan="6">Description</th>
                    <th rowspan="2" width="110">Opening Stock</th>
                    <th colspan="3">Receive</th>
                    <th colspan="3">Issue</th>
                    <th rowspan="2" width="100">Closing Stock</th>
                    <th rowspan="2" width="100">Avg. Rate</th>
                    <th rowspan="2" width="100">Stock Value</th>
                    <th rowspan="2" width="60">DOH</th>
                </tr> 
                <tr> 
                	<th width="60">Prod. ID</th>                    
                    <th width="120">Construction</th>
                    <th width="180">Composition</th>
                    <th width="70">GSM</th>
                    <th width="80">Dia/ Width</th>
                    <th width="80">Color</th>
                    <th width="80">Purchase</th>
                    <th width="80">Issue Return</th> 
                    <th width="100">Total Received</th>
                    <th width="80">Issue</th>
                    <th width="80">Receive Return</th>
                    <th width="100">Total Issue</th> 
                </tr> 
            </thead>
        </table>
        <div style="width:1640px; max-height:200px; overflow:scroll" id="scroll_body" > 
        <table style="width:1620px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_body">
        <?php
			$composition_arr=array();
			$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
			$data_array=sql_select($sql_deter);
			if(count($data_array)>0)
			{
				foreach( $data_array as $row )
				{
					if(array_key_exists($row[csf('id')],$composition_arr))
					{
						$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
					}
					else
					{
						$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
					}
				}
			}
		
			$tot_opening_bal=0;$tot_purchuse=0;$tot_issue_return=0;$tot_receive=0; $tot_issue=0;$tot_receive_return=0;$tot_total_issue=0;$total_closing_stock=0;$tot_closing_stock=0;
            foreach($result as $row)
            {
                if($i%2==0)$bgcolor="#E9F3FF";  else $bgcolor="#FFFFFF"; 
                if( $row[csf("transaction_type")]==1 || $row[csf("transaction_type")]==2 || $row[csf("transaction_type")]==3 || $row[csf("transaction_type")]==4 ) 
                    $stylecolor='style="color:#A61000"';
                else
                    $stylecolor='style="color:#000000"';

              /* $issue_sql = "select
                        sum(case when a.transaction_date<'".$select_from_date."' then a.cons_quantity else 0 end) as issue_total_opening,
						sum(case when a.transaction_type=2 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as issue,
						sum(case when a.transaction_type=3 and a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_quantity else 0 end) as receive_return,
						sum(case when a.transaction_date  between '".$select_from_date."' and '".$select_from_to."' then a.cons_rate else 0 end) as rate 
						from inv_transaction a, inv_issue_master c
                        where a.mst_id=c.id and a.transaction_type in (2,3) and a.prod_id=".$row[csf("prod_id")]." and a.item_category in (2,3) and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0";
						//echo $issue_sql;
				$issue_result = sql_select($issue_sql);*/
				//$prod_id_arr[$row[csf('prod_id')]]['issue'];
				
				//$totalIssue = $issue_result[0][csf('issue')]+$issue_result[0][csf('receive_return')];
				$totalIssue = $prod_id_arr[$row[csf('prod_id')]]['issue']+$prod_id_arr[$row[csf('prod_id')]]['receive_return'];
				//echo $totalIssue ;die;
				
				$openingBalance = $row[csf("rcv_total_opening")]-$prod_id_arr[$row[csf('prod_id')]]['issue_total_opening'];
				$totalReceive = $row[csf("purchase")]+$row[csf("issue_return")];
				$closingStock=$openingBalance+$totalReceive-$totalIssue;
				$avgRate=$prod_id_arr[$row[csf('prod_id')]]['rate'];
				$stockValue=$closingStock*$avgRate;
				 
				//$totalStockValue+=$stockValue;
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" <?php echo $stylecolor; ?> onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
						<td width="40"><?php echo $i; ?></td>
                        <td width="60"><?php echo $row[csf("prod_id")];; ?></td>								
						<td width="120"><?php echo $determinaArr[$row[csf("detarmination_id")]]; ?></td>                                 
						<td width="180"><p><?php echo $composition_arr[$row[csf('detarmination_id')]]; ?></p></td>
						<td width="70"><p><?php echo $row[csf("gsm")]; ?></p></td> 
						<td width="80"><p><?php echo $row[csf("dia_width")]; ?></p></td> 
						<td width="80"><p><?php echo $color_arr[$row[csf("color")]]; ?></p></td> 
                        
						<td width="110" align="right"><p><?php echo number_format($openingBalance,2);$tot_opening_bal+=$openingBalance; ?></p></td>
						
						<td width="80" align="right"><p><?php echo number_format($row[csf("purchase")],2); $tot_purchuse+=$row[csf("purchase")]; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($row[csf("issue_return")],2); $tot_issue_return+=$row[csf("issue_return")]; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalReceive,2); $tot_receive+=$totalReceive; ?></p></td>
                        
						<td width="80" align="right"><p><?php echo number_format($prod_id_arr[$row[csf('prod_id')]]['issue'],2); $tot_issue+=$prod_id_arr[$row[csf('prod_id')]]['issue']; ?></p></td>
						<td width="80" align="right"><p><?php echo number_format($prod_id_arr[$row[csf('prod_id')]]['receive_return'],2); $tot_receive_return+=$prod_id_arr[$row[csf('prod_id')]]['receive_return']; ?></p></td>
						<td width="100" align="right"><p><?php echo number_format($totalIssue,2); $tot_total_issue+=$totalIssue; ?></p></td>
                        
                        <td width="100" align="right"><p><?php echo number_format($closingStock,2); $total_closing_stock+=$closingStock; ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($row[csf("avg_rate")],2); ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($stockValue,2); $tot_closing_stock+=$stockValue; ?></p></td>
						<?php
							/*if($db_type==0)
							{
							$returnRes=explode(",",return_field_value("concat(min(transaction_date),',',max(transaction_date))","inv_transaction","prod_id=".$row[csf("prod_id")]));
							//echo $returnRes;die;
							$daysOnHand = datediff("d",$returnRes[1],date("Y-m-d"));
							}
							else
							
							{
									
							$returnRes=explode(",",return_field_value("(min(transaction_date)|| ',' || max(transaction_date)) as trans_date","inv_transaction","prod_id=".$row[csf("prod_id")],"trans_date")); $today= change_date_format(date("Y-m-d"),'','',1);	
							$daysOnHand = datediff("d",change_date_format($returnRes[1],'','',1),$today);
													
							}*/
						?>
						<td width="60" align="right"><?php echo $days_doh[$row[csf('prod_id')]]['daysonhand'] //$daysOnHand; ?>&nbsp;</td>
					</tr>
                <?php 												
                $i++; 				
			}
		?>
        </table>
        <table style="width:1620px" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="">
            <tr bgcolor="#CCCCCC" style="font-weight:bold">
            	<td align="right" width="40">&nbsp;</td>
                <td align="right" width="60">&nbsp;</td>
                <td align="right" width="120">&nbsp;</td>
                <td align="right" width="180">&nbsp;</td>
                <td align="right" width="70">&nbsp;</td>
                <td align="right" width="80">&nbsp;</td>
                <td align="right" width="80">Total</td>
                <td align="right" width="110" id="tot_opening_bal" ><?php echo number_format($tot_opening_bal,2); ?></td>
                <td align="right" width="80" id="tot_purchuse" ><?php echo number_format($tot_purchuse,2); ?></td>
                <td align="right" width="80" id="tot_issue_return" ><?php echo number_format($tot_issue_return,2); ?></td>
                <td align="right" width="100" id="tot_receive" ><?php echo number_format($tot_receive,2); ?></td>
                <td align="right" width="80" id="tot_issue" ><?php echo number_format($tot_issue,2); ?></td>
                <td align="right" width="80" id="tot_receive_return" ><?php echo number_format($tot_receive_return,2); ?></td>
                <td align="right" width="100" id="tot_total_issue" ><?php echo number_format($tot_total_issue,2); ?></td>
                <td align="right" width="100" id="total_closing_stock" ><?php echo number_format($total_closing_stock,2); ?></td>
                <td align="right" width="100">&nbsp;</td>
                <td align="right" width="100" id="tot_closing_stock"><?php echo number_format($tot_closing_stock,2); ?></td>
                <td align="right" width="60">&nbsp;</td>  
            </tr>
        </table>
        </div>
    </div>
    <?php
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}
?>