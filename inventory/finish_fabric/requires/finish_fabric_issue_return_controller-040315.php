﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
$store_arr=return_library_array( "select id, store_name from lib_store_location",'id','store_name');
$supplier_arr = return_library_array("select id, supplier_name from lib_supplier","id","supplier_name");
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$batch_arr = return_library_array("select id, batch_no from pro_batch_create_mst","id","batch_no");

if($action=='roll_maintained')
{
	$cbo_company_name=$data;
	$variable_setting_production=return_field_value("fabric_roll_level","variable_settings_production","company_name='$cbo_company_name' and variable_list=3 and status_active=1","fabric_roll_level");
	if($variable_setting_production==1)
	{
		echo "$('#roll_maintained').val($variable_setting_production);\n";
	}
	else
	{
		echo "$('#roll_maintained').val(0);\n";
	}
	
}

if($action=="load_drop_down_sewing_com")
{
	$data = explode("_",$data);
	$company_id=$data[1];

	if($data[0]==1)
	{
		echo create_drop_down( "cbo_sewing_company", 150, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "--Select Sewing Company--", "$company_id", "","" );
	}
	else if($data[0]==3)
	{
		echo create_drop_down( "cbo_sewing_company", 150, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type=21 and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "--Select Sewing Company--", 1, "" );
	}
	else
	{
		echo create_drop_down( "cbo_sewing_company", 150, $blank_array,"",1, "--Select Sewing Company--", 1, "" );
	}
	exit();
}



if ($action=="load_drop_down_store")
{	  
	echo create_drop_down("cbo_store_name",170, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=2 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "-- Select store --", 0,"", 0);  	 
	exit();
}

if($action=="mrr_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode); 
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="750" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th>Search By</th>
                    <th align="center" id="search_by_td_up">Enter MRR Number</th>
                    <th>Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr class="general">
                    <td>
                        <?php  
                            $search_by = array(1=>'MRR No',2=>'Challan No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 120, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $cbo_company_id; ?>, 'create_mrr_search_list_view', 'search_div', 'finish_fabric_issue_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" style="margin-top:10px" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$fromDate = $ex_data[2];
	$toDate = $ex_data[3];
	$company = $ex_data[4];
	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for mrr
		{
			$sql_cond .= " and a.issue_number LIKE '%$txt_search_common'";	
		}
		else if(trim($txt_search_by)==2) // for chllan no
		{
			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";				
 		}		 
 	} 
	
	if( $fromDate!="" && $toDate!="" ) 
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.issue_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.issue_date  between '".change_date_format($fromDate,'','',1)."' and '".change_date_format($toDate,'','',1)."'";
		}
	}
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
	else $year_field="";//defined Later
	
	$sql = "select a.id,a.issue_number_prefix_num,a.issue_number,a.issue_basis,a.issue_purpose,$year_field, a.challan_no,a.issue_date,sum(b.cons_quantity) as issue_qnty from inv_transaction b, inv_issue_master a where a.id=b.mst_id and a.entry_form in(18) and a.status_active=1 $sql_cond group by  a.id,a.issue_number_prefix_num,a.issue_number,a.issue_basis,a.issue_purpose, a.challan_no,a.issue_date,a.insert_date order by a.id";
	//echo $sql;
	?>
    	<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="720">
        	<thead>
                <tr>
                	<th width="50">SL</th>
                    <th width="100">MRR No</th>
                    <th width="100">Year</th>
                    <th width="100">Challan No</th>
                    <th width="100">Issue Date</th>
                    <th width="150">Issue Purpose</th>
                    <th>Issue Qnty</th>
                </tr>
            </thead>
        </table>
        
        <div style="width:720px; overflow-y:scroll; max-height:280px;font-size:12px; overflow-x:hidden;" id="scroll_body">
    	<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="700" id="list_view">
            <tbody>
            <?php
			$i=1;
			$sql_result=sql_select($sql);
			foreach($sql_result as $row)
			{
				if ($k%2==0)
				$bgcolor="#E9F3FF";
				else
				$bgcolor="#FFFFFF";

				?>
            	<tr bgcolor="<?php echo $bgcolor; ?>" style="cursor:pointer;" onClick="js_set_value('<?php echo $row[csf("id")]; ?>_<?php echo $row[csf("issue_number")]; ?>_<?php echo $row[csf("issue_purpose")]; ?>_<?php echo $row[csf("challan_no")]; ?>')">
                	<td width="50" align="center"><p><?php echo $i; ?>&nbsp;</p></td>
                    <td width="100" align="center"><p><?php echo $row[csf("issue_number_prefix_num")]; ?>&nbsp;</p></td>
                    <td width="100" align="center"><p><?php echo $row[csf("year")]; ?>&nbsp;</p></td>
                    <td width="100" ><p><?php echo $row[csf("challan_no")]; ?>&nbsp;</p></td>
                    <td width="100" align="center"><p><?php if($row[csf("issue_date")]!="" && $row[csf("issue_date")]!="0000-00-00") echo change_date_format($row[csf("issue_date")]); ?>&nbsp;</p></td>
                    <td width="150"><p>
					<?php
					echo $yarn_issue_purpose[$row[csf("issue_purpose")]]; 
					 
					?>&nbsp;</p></td>
                    <td align="right"><p><?php echo number_format($row[csf("issue_qnty")],2,".",""); ?>&nbsp;</p></td>
                </tr>
                <?php
				$i++;
			}
			?>
            </tbody>
        </table>
        </div>
    <?php	
	exit();
	
}

if($action=="show_fabric_desc_listview")
{ 
 	$mrr_no = $data;
	
	//echo "select issue_id, prod_id, batch_id_from_fissuertn, rack, self, sum(cons_quantity) as qnty from inv_transaction where status_active=1 and is_deleted=0 and transaction_type=4 and issue_id=$mrr_no group by issue_id, prod_id, batch_id_from_fissuertn, rack, self <br>";
	
	$issue_rtn_array=array();
	$issData=sql_select("select issue_id, prod_id, batch_id_from_fissuertn, rack, self, sum(cons_quantity) as qnty from inv_transaction where status_active=1 and is_deleted=0 and transaction_type=4 and issue_id=$mrr_no group by issue_id, prod_id, batch_id_from_fissuertn, rack, self");
	foreach($issData as $row)
	{
		$issue_rtn_array[$row[csf('issue_id')]][$row[csf('prod_id')]][$row[csf('batch_id_from_fissuertn')]][$row[csf('rack')]][$row[csf('self')]]=$row[csf('qnty')];
	}
	//var_dump($issue_rtn_array);
	
	$data_array=sql_select("select  a.id as issue_id, b.batch_id as batch_id, sum(b.issue_qnty) as qnty, b.rack_no, b.shelf_no, c.id as prod_id, c.product_name_details, c.current_stock, c.color from inv_issue_master a, inv_finish_fabric_issue_dtls b, product_details_master c  where a.id=b.mst_id and b.prod_id=c.id and a.id='$mrr_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id,b.batch_id,b.rack_no, b.shelf_no, c.id, c.product_name_details, c.current_stock, c.color");
	
	
	?>
    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="490">
        <thead>
            <th width="30">SL</th>
            <th width="50">Prod. ID</th>
            <th width="130">Fabric Description</th>
            <th width="50">Rack</th>
            <th width="50">Shelf</th>
            <th width="60">Issue Qty</th>
            <th width="60">Issue Return Qty</th>
            <th>Balance</th>
        </thead>
        <tbody>
            <?php 
            $i=1;
            foreach($data_array as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				//echo "select sum(cons_quantity) as tot_rtn from inv_transaction where issue_id='".$row[csf("issue_id")]."' and prod_id='".$row[csf("prod_id")]."' and item_category=2 and transaction_type=4";die;
				//$totalReturned = return_field_value("sum(cons_quantity) as tot_rtn","inv_transaction","issue_id='".$row[csf("issue_id")]."' and prod_id='".$row[csf("prod_id")]."' and item_category=2 and transaction_type=4","tot_rtn");
				//$totalissue = return_field_value("sum(cons_quantity) as tot_issue","inv_transaction","mst_id='".$row[csf("issue_id")]."' and prod_id='".$row[csf("prod_id")]."' and item_category=2 and transaction_type=2","tot_issue");
				if($totalReturned=="") $totalReturned=0;
				$iss_rtn_qnty=$issue_rtn_array[$row[csf('issue_id')]][$row[csf('prod_id')]][$row[csf('batch_id')]][$row[csf('rack_no')]][$row[csf('shelf_no')]];
				$balance=$row[csf('qnty')]-$iss_rtn_qnty;
             ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" onClick='set_form_data("<?php echo $row[csf('batch_id')]."**".$row[csf('issue_id')]."**".$row[csf('prod_id')]."**".$row[csf('product_name_details')]."**".$row[csf('rack_no')]."**".$row[csf('shelf_no')]."**".number_format($row[csf('qnty')],2,".","")."**".number_format($iss_rtn_qnty,2,".","")."**".number_format($row[csf('current_stock')],2,".","")."**".$color_arr[$row[csf('color')]];?>")' style="cursor:pointer" >
                    <td><?php echo $i; ?></td>
                    <td><p><?php echo $row[csf('prod_id')]; ?></p></td>
                    <td><p><?php echo $row[csf('product_name_details')]; ?></p></td>
                    <td><p><?php echo $row[csf('rack')]; ?>&nbsp;</p></td>
                    <td><p><?php echo $row[csf('self')]; ?>&nbsp;</p></td>
                    <td align="right"><?php echo number_format($row[csf('qnty')],2,'.',''); ?></td>
                    <td align="right"><?php echo number_format($iss_rtn_qnty,2,'.',''); ?></td>
                    <td align="right"><?php echo number_format($balance,2,'.',''); ?></td>
                </tr>
            <?php 
            $i++; 
            } 
            ?>
        </tbody>
    </table>
<?php
exit();
}


if($action=="populate_details_from_data")
{
	$data=explode("**",$data);
 	$dtls_data=sql_select("select batch_id, store_id from  inv_finish_fabric_issue_dtls where status_active=1 and is_deleted=0 and mst_id=$data[1] and batch_id=$data[0]");
	echo "$('#cbo_store_name').val(".$dtls_data[0][csf("store_id")].");\n";
	echo "$('#hidden_batch_id').val(".$dtls_data[0][csf("batch_id")].");\n";
	echo "$('#txt_batch_no').val('".$batch_arr[$dtls_data[0][csf("batch_id")]]."');\n";
	exit();
}

if($action=="return_po_popup")
{
	echo load_html_head_contents("Issue Return Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$txt_issue_id=str_replace("'","",$txt_issue_id);
	$txt_prod_id=str_replace("'","",$txt_prod_id);
	$update_id=str_replace("'","",$update_id);
	$txt_return_qnty=str_replace("'","",$txt_return_qnty);
	$roll_maintained=str_replace("'","",$roll_maintained);
	
	if($update_id>0 && $txt_return_qnty>0)
	{
		$order_sql=sql_select("select po_breakdown_id,quantity from order_wise_pro_details where trans_id=$update_id and trans_type=4 and entry_form=52 and status_active=1");
		foreach($order_sql as $row)
		{
			$order_wise_qnty_arr[$row[csf("po_breakdown_id")]]=$row[csf("quantity")];
		}
	}
	
	if($roll_maintained==1)
	{
		$table_width=600;
	}
	else
	{
		$table_width=500;
	}
	?>
<script>
	
	
	function js_set_value()
	{
		var table_legth=$('#pop_table tbody tr').length;
		var break_qnty=break_roll=break_id="";
		var tot_qnty=tot_roll=0;
		for(var i=1; i<=table_legth; i++)
		{
			//if(i!=1) break_qnty+="_";
			tot_qnty +=($("#issueqnty_"+i).val()*1);
			if(break_qnty!="") break_qnty +="_";
			break_qnty+=($("#poId_"+i).val()*1)+'**'+($("#issueqnty_"+i).val()*1);
			if(break_roll!="") break_roll +="_";
			break_roll+=($("#poId_"+i).val()*1)+'**'+($("#roll_"+i).val()*1)+'**'+($("#issueqnty_"+i).val()*1);
			if(break_id!="") break_id +=",";
			break_id+=($("#poId_"+i).val()*1);
			if($("#roll_"+i).val()>0) tot_roll +=($("#roll_"+i).val()*1);
			
		}
		$("#tot_qnty").val(tot_qnty);
		$("#tot_roll").val(tot_roll);
		$("#break_qnty").val(break_qnty);
		$("#break_roll").val(break_roll);
		$("#break_order_id").val(break_id);
		parent.emailwindow.hide();
	}
	
	function fn_calculate(id)
	{
		var recv_qnty=($("#recevqnty_"+id).val()*1);
		var cumu_qnty=($("#cumulativeIssue_"+id).val()*1);
		var issue_qnty=($("#issueqnty_"+id).val()*1);
		var hiddenissue_qnty=($("#hiddenissueqnty_"+id).val()*1);
		if(((cumu_qnty*1)+(issue_qnty*1))>((recv_qnty*1)+(hiddenissue_qnty*1))) 
		{
			alert("Return Quantity Can not be Greater Than Receive Quantity.");
			$("#issueqnty_"+id).val(0);
		}
	}
	
	
</script>

</head>

<body>
    <div align="center" style="width:100%;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <table width="<?php echo $table_width; ?>" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center" id="pop_table">
                <thead>
                    <tr>                	 
                        <th width="140">Order No</th>
                        <th width="120">Issue Quantity</th>
                        <th width="120">Cumulative Return</th>
                        <?php
						if($roll_maintained==1)
						{
							?>
							<th>Roll</th>
							<?php
						}
						?>           
                        <th width="120">Return Quantity</th>
                    </tr>
                </thead>
                <tbody>
                <?php
				$po_no_arr = return_library_array("select id,po_number from wo_po_break_down","id","po_number");
				

				$sql=sql_select("select a.prod_id, a.po_breakdown_id, sum(a.quantity) as receive_qnty, b.mst_id from  order_wise_pro_details a, inv_transaction b where a.trans_id=b.id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.company_id='$cbo_company_name' and b.mst_id='$txt_issue_id' and b.prod_id='$txt_prod_id' and a.entry_form in(18) and b.transaction_type in(2) group by b.mst_id, a.prod_id, a.po_breakdown_id");
				$i=1;
				foreach($sql as $row)
				{
					$cumilitive_issue=return_field_value("sum(c.quantity) as cumu_qnty","inv_transaction a,  order_wise_pro_details c","a.id=c.trans_id and c.status_active=1 and a.mst_id='$txt_issue_id' and a.prod_id=$txt_prod_id and a.transaction_type=2 and c.po_breakdown_id='".$row[csf('po_breakdown_id')]."'","cumu_qnty");
					?>
                	<tr>
                    	<td align="center"><input type="text" id="poNo_<?php echo $i; ?>" name="poNo_<?php echo $i; ?>" class="text_boxes" style="width:140px" value="<?php echo $po_no_arr[$row[csf("po_breakdown_id")]];  ?>"  readonly disabled >
                        <input type="hidden" id="poId_<?php echo $i; ?>" name="poId_<?php echo $i; ?>" class="text_boxes" style="width:140px" value="<?php echo $row[csf("po_breakdown_id")];  ?>"  readonly disabled >
                        </td>
                        <td align="center"> <input type="text" id="recevqnty_<?php echo $i; ?>" name="recevqnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:110px" value="<?php echo number_format($row[csf("receive_qnty")],2);  ?>" readonly disabled ></td>
                        <td align="center"><input type="text" id="cumulativeIssue_<?php echo $i; ?>" name="cumulativeIssue_<?php echo $i; ?>" value="<?php echo number_format($cumilitive_issue,2); ?>" class="text_boxes_numeric" style="width:110px" readonly disabled ></td>
                        <?php
						if($roll_maintained==1)
						{
							?>
							<td align="center"><input type="text" id="roll_<?php echo $i; ?>" name="roll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" ></td>
							<?php
						}
						else
						{
							?>
							<td align="center" style="display:none;"><input type="text" id="roll_<?php echo $i; ?>" name="roll_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" ></td>
							<?php
						}
						?> 
                        <td align="center">
                        <input type="text" id="issueqnty_<?php echo $i; ?>" name="issueqnty_<?php echo $i; ?>" onKeyUp="fn_calculate(<?php echo $i; ?>);" class="text_boxes_numeric" value="<?php echo $order_wise_qnty_arr[$row[csf("po_breakdown_id")]]; ?>" style="width:110px" >
                        <input type="hidden" id="hiddenissueqnty_<?php echo $i; ?>" name="hiddenissueqnty_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $order_wise_qnty_arr[$row[csf("po_breakdown_id")]]; ?>">
                        </td>
                        
                    </tr>
                    <?php
					$i++;
				}
				?>
                </tbody>
        </table>
        <table width="<?php echo $table_width; ?>" cellspacing="0" cellpadding="0" border="0" rules="all" align="center">
            <tr>
                <td align="center"> 
                <input type="button" id="btn_close" name="" value="Close" onClick="js_set_value();" style="width:150px;" class="formbutton" >
                <input type="hidden" id="tot_qnty" name="tot_qnty" >
                <input type="hidden" id="tot_roll" name="tot_roll" >
                <input type="hidden" id="break_qnty" name="break_qnty" >
                <input type="hidden" id="break_roll" name="break_roll" >
                <input type="hidden" id="break_order_id" name="break_order_id" >
                </td>
            </tr>
        </table>
    </form>
    </div>
</body>    
	<?php

}


if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$roll_maintained=str_replace("'","",$roll_maintained);
	$cbo_issue_purpose=str_replace("'","",$cbo_issue_purpose);
	//echo $issue_purpose;die;
	
	if($cbo_issue_purpose==8) $book_without_order=1; else $book_without_order=0;
	
	
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
 		
		//---------------Check Duplicate product in Same return number ------------------------//
		/*$duplicate = is_duplicate_field("b.id","inv_receive_master a, inv_transaction b","a.id=b.mst_id and a.id=$issue_mst_id and b.prod_id=$txt_prod_id and b.transaction_type=4"); 
		if($duplicate==1) 
		{
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			die;
		}*/
		//------------------------------Check Duplicate END---------------------------------------//
		
 		if(str_replace("'","",$issue_mst_id)!="")
		{
			$new_return_number[0] = str_replace("'","",$txt_system_id);
			$id=str_replace("'","",$issue_mst_id);
			//issue master table UPDATE here START----------------------//		
 			$field_array_mst="receive_purpose*booking_without_order*receive_date*issue_id*challan_no*updated_by*update_date";
			$data_array_mst=$cbo_issue_purpose."*".$book_without_order."*".$txt_issue_date."*".$txt_issue_id."*".$txt_challan_no."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
		}
		else  	
		{	 
			//issue master table entry here START---------------------------------------//		
			$id=return_next_id("id", "inv_receive_master", 1);
			
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
					
			$new_return_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'KFIR', date("Y",time()), 5, "select recv_number_prefix,recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_id and entry_form=52 and $year_cond=".date('Y',time())." order by id DESC ", "recv_number_prefix", "recv_number_prefix_num" ));
			
 			$field_array_mst="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, company_id, receive_purpose, booking_without_order, receive_date, issue_id, challan_no, inserted_by, insert_date";
			$data_array_mst="(".$id.",'".$new_return_number[1]."','".$new_return_number[2]."','".$new_return_number[0]."',52,2,".$cbo_company_id.",".$cbo_issue_purpose.",".$book_without_order.",".$txt_issue_date.",".$txt_issue_id.",".$txt_challan_no.",'".$user_id."','".$pc_date_time."')";
			//echo "20**".$field_array_mst."<br>".$data_array_mst;die;
		}
		
		//echo $pc_date;die;
		
		//transaction table insert here START--------------------------------//cbo_uom
		$transactionID = return_next_id("id", "inv_transaction", 1); 				
		$field_array_trans = "id,mst_id,company_id,prod_id,item_category,transaction_type,transaction_date,order_uom,order_qnty,cons_uom,cons_quantity,remarks,issue_id,issue_challan_no,rack,self,batch_id_from_fissuertn,no_of_roll,store_id,inserted_by,insert_date";
 		$data_array_trans = "(".$transactionID.",".$id.",".$cbo_company_id.",".$txt_prod_id.",2,4,".$txt_issue_date.",0,".$txt_return_qnty.",0,".$txt_return_qnty.",".$txt_remarks.",".$txt_issue_id.",".$txt_challan_no.",".$txt_rack.",".$txt_shelf.",".$hidden_batch_id.",".$txt_no_of_roll.",".$cbo_store_name.",'".$user_id."','".$pc_date_time."')"; 
		
		//echo "insert into inv_transaction ($field_array_trans) values $data_array_trans"; die;
		 
		//adjust product master table START-------------------------------------//
		
		$sql = sql_select("select product_name_details,last_purchased_qnty,current_stock from product_details_master where id=$txt_prod_id");
		$presentStock=$available_qnty=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$product_name_details 	=$result[csf("product_name_details")];
		}
		$nowStock 		= $presentStock+str_replace("'","",$txt_return_qnty);
		//echo $nowStock;die;
			
		$field_array_prod="last_purchased_qnty*current_stock*updated_by*update_date";
		$data_array_prod=$txt_return_qnty."*".$nowStock."*'".$user_id."'*'".$pc_date_time."'";
		
		
		//order_wise_pro_detail table insert here
		
		$txt_break_qnty=str_replace("'","",$txt_break_qnty);
		$txt_break_roll=str_replace("'","",$txt_break_roll);
		$txt_order_id_all=str_replace("'","",$txt_order_id_all);
		$ordr_wise_rtn_qnty_arr=explode("_",$txt_break_qnty);
		$ordr_wise_rtn_roll_arr=explode("_",$txt_break_roll);
		$ordr_id_arr=explode(",",$txt_order_id_all);
		$proportion_id = return_next_id("id", "order_wise_pro_details", 1);
		$roll_id = return_next_id("id", "pro_roll_details", 1);
		$field_array_proportion="id,trans_id,trans_type,entry_form,po_breakdown_id,prod_id,quantity,inserted_by,insert_date";
	
		 
		$data_array_proportion=$data_array_roll="";
		if(!empty($txt_break_qnty))
		{
			foreach($ordr_wise_rtn_qnty_arr as $val)
			{
				$order_qnty_arr=explode("**",$val);
				if($order_qnty_arr[1]>0)
				{
					if($data_array_proportion!="") $data_array_proportion.=", ";
					$data_array_proportion.="(".$proportion_id.",".$transactionID.",4,52,".$order_qnty_arr[0].",".$txt_prod_id.",".$order_qnty_arr[1].",'".$user_id."','".$pc_date_time."')";
					$proportion_id++;
				}
			}
			
			if($roll_maintained==1)
			{
				$field_array_roll="id,mst_id,dtls_id,po_breakdown_id,entry_form,roll_no,qnty,inserted_by,insert_date";
				
				foreach($ordr_wise_rtn_roll_arr as $val)
				{
					$order_roll_arr=explode("**",$val);
					
					if($order_roll_arr[1]>0)
					{
						if($data_array_roll!="") $data_array_roll.=", ";
						$data_array_roll.="(".$roll_id.",".$id.",".$transactionID.",".$order_roll_arr[0].",52,".$order_roll_arr[1].",".$order_roll_arr[2].",'".$user_id."','".$pc_date_time."')";
						$roll_id++;
					}
				}
			}
		}
		
		
		
		$rID=$transID=$prodUpdate=$propoId=$rollId=true;
		if(str_replace("'","",$txt_return_no)!="")
		{
			$rID=sql_update("inv_receive_master",$field_array_mst,$data_array_mst,"id",$id,1);
		}
		else
		{
			$rID=sql_insert("inv_receive_master",$field_array_mst,$data_array_mst,1);
		}
		$transID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,1);
		$prodUpdate = sql_update("product_details_master",$field_array_prod,$data_array_prod,"id",$txt_prod_id,1);
		if($data_array_proportion!="")
		{
			$propoId=sql_insert("order_wise_pro_details",$field_array_proportion,$data_array_proportion,1);
			if($roll_maintained==1)
			{
				$rollId=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,1);
			}
		}
		
		
		
		if($db_type==0)
		{
			if( $rID && $transID && $prodUpdate && $propoId && $rollId )
			{
				mysql_query("COMMIT");  
				echo "0**".$new_return_number[0]."**".$id."**".str_replace("'","",$roll_maintained);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_return_number[0];
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if( $rID && $transID && $prodUpdate && $propoId && $rollId)
			{
				oci_commit($con);  
				echo "0**".$new_return_number[0]."**".$id."**".str_replace("'","",$roll_maintained);
			}
			else
			{
				oci_rollback($con);
				echo "10**".$new_return_number[0];
			}
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		$issue_mst_id= str_replace("'","",$issue_mst_id);
		$roll_maintained= str_replace("'","",$roll_maintained);
		$txt_system_id = str_replace("'","",$txt_system_id);
		//check update id
		if( str_replace("'","",$update_id) == "" )
		{
			echo "10";die; 
		}
		
		
		$txt_return_qnty=str_replace("'","",$txt_return_qnty);
		
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
		//product master table information
		//before stock update
		$sql = sql_select( "select a.id,a.current_stock, b.cons_quantity from product_details_master a, inv_transaction b where a.id=b.prod_id and a.id=$before_prod_id and b.id=$update_id and a.item_category_id=2 and b.item_category=2 and b.transaction_type=4" );
		$before_prod_id=$before_issue_qnty=$before_stock_qnty=0;
		foreach($sql as $result)
		{
			$before_prod_id 	= $result[csf("id")];
 			$before_stock_qnty 	= $result[csf("current_stock")];
			//before quantity and stock value
			$before_issue_qnty	= $result[csf("cons_quantity")];
		}
		
		//current product ID
		$txt_prod_id = str_replace("'","",$txt_prod_id);
		$txt_return_qnty = str_replace("'","",$txt_return_qnty);
		$before_prod_id= str_replace("'","",$before_prod_id);
		$curr_stock_qnty=return_field_value("current_stock","product_details_master","id=$txt_prod_id and item_category_id=2");
		
 		//echo $receive_purpose;die;
		//weighted and average rate START here------------------------//
		//product master table data UPDATE START----------------------//		
		$update_array_prod= "last_purchased_qnty*current_stock*updated_by*update_date";
		if($before_prod_id==$txt_prod_id)
		{
			$adj_stock_qnty = (($curr_stock_qnty-$before_issue_qnty)+$txt_return_qnty); // CurrentStock + Before Issue Qnty - Current Issue Qnty
			 
			$data_array_prod= $txt_return_qnty."*".$adj_stock_qnty."*'".$user_id."'*'".$pc_date_time."'";
		}
		else
		{
			$updateIdprod_array = $update_dataProd = array();
			//before product adjust
			$adj_before_stock_qnty 	= $before_stock_qnty-$before_issue_qnty; // CurrentStock + Before Issue Qnty
			 
			$updateIdprod_array[]=$before_prod_id;
			$update_dataProd[$before_prod_id]=explode("*",("".$before_issue_qnty."*".$adj_before_stock_qnty."*'".$user_id."'*'".$pc_date_time."'"));
			
			//current product adjust
			$adj_curr_stock_qnty = 	$curr_stock_qnty+$txt_return_qnty; // CurrentStock + Before Issue Qnty
			
			$updateIdprod_array[]=$txt_prod_id;
			$update_dataProd[$txt_prod_id]=explode("*",("".$txt_return_qnty."*".$adj_curr_stock_qnty."*'".$user_id."'*'".$pc_date_time."'"));
		}
		
		
	
		 
  		$id=str_replace("'","",$issue_mst_id);
		//yarn master table UPDATE here START----------------------//	
		$field_array_mst="receive_purpose*booking_without_order*receive_date*issue_id*challan_no*updated_by*update_date";
		$data_array_mst=$cbo_issue_purpose."*".$book_without_order."*".$txt_issue_date."*".$txt_issue_id."*".$txt_challan_no."*'".$user_id."'*'".$pc_date_time."'";
		
		
		//,rack,self
 		$field_array_trans="company_id*prod_id*item_category*transaction_type*transaction_date*order_uom*order_qnty*cons_uom*cons_quantity*remarks*issue_id*issue_challan_no*rack*self*batch_id_from_fissuertn*no_of_roll*store_id*updated_by*update_date";
 		$data_array_trans= "".$cbo_company_id."*".$txt_prod_id."*2*4*".$txt_issue_date."*0*".$txt_return_qnty."*0*".$txt_return_qnty."*".$txt_remarks."*".$txt_issue_id."*".$txt_challan_no."*".$txt_rack."*".$txt_shelf."*".$hidden_batch_id."*".$txt_no_of_roll."*".$cbo_store_name."*'".$user_id."'*'".$pc_date_time."'"; 
		//echo $field_array."<br>".$data_array;die;
		$update_id = str_replace("'","",$update_id);
		//order_wise_pro_detail table insert here
		
		$txt_break_qnty=str_replace("'","",$txt_break_qnty);
		$txt_break_roll=str_replace("'","",$txt_break_roll);
		$txt_order_id_all=str_replace("'","",$txt_order_id_all);
		$ordr_wise_rtn_qnty_arr=explode("_",$txt_break_qnty);
		$ordr_wise_rtn_roll_arr=explode("_",$txt_break_roll);
		$ordr_id_arr=explode(",",$txt_order_id_all);
		$proportion_id = return_next_id("id", "order_wise_pro_details", 1);
		$roll_id = return_next_id("id", "pro_roll_details", 1);
		$field_array_proportion="id,trans_id,trans_type,entry_form,po_breakdown_id,prod_id,quantity,inserted_by,insert_date";
	
		 
		$data_array_proportion=$data_array_roll="";
		if(!empty($txt_break_qnty))
		{
			foreach($ordr_wise_rtn_qnty_arr as $val)
			{
				$order_qnty_arr=explode("**",$val);
				if($order_qnty_arr[1]>0)
				{
					if($data_array_proportion!="") $data_array_proportion.=", ";
					$data_array_proportion.="(".$proportion_id.",".$update_id.",4,52,".$order_qnty_arr[0].",".$txt_prod_id.",".$order_qnty_arr[1].",'".$user_id."','".$pc_date_time."')";
					$proportion_id++;
				}
			}
			
			if($roll_maintained==1)
			{
				$field_array_roll="id,mst_id,dtls_id,po_breakdown_id,entry_form,roll_no,qnty,inserted_by,insert_date";
				
				foreach($ordr_wise_rtn_roll_arr as $val)
				{
					$order_roll_arr=explode("**",$val);
					
					if($order_roll_arr[2]>0)
					{
						if($data_array_roll!="") $data_array_roll.=", ";
						$data_array_roll.="(".$roll_id.",".$id.",".$update_id.",".$order_roll_arr[0].",52,".$order_roll_arr[1].",".$order_roll_arr[2].",'".$user_id."','".$pc_date_time."')";
						$roll_id++;
					}
				}
			}
		}
		
		
 		$query1=$query4=$query5=$rID=$transID=$propoId=$rollId=true;
		
		if($before_prod_id==$txt_prod_id)
		{
			$query1= sql_update("product_details_master",$update_array_prod,$data_array_prod,"id",$before_prod_id,1);
		}
		else
		{
			$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array_prod,$update_dataProd,$updateIdprod_array));
		}
		$rID=sql_update("inv_receive_master",$field_array_mst,$data_array_mst,"id",$id,1);
		$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
		if($data_array_proportion!="")
		{
			$query4 = execute_query("DELETE FROM order_wise_pro_details WHERE trans_id=$update_id and entry_form=52");
			$propoId=sql_insert("order_wise_pro_details",$field_array_proportion,$data_array_proportion,1);
			if($roll_maintained==1)
			{
				$query5 = execute_query("DELETE FROM pro_roll_details WHERE dtls_id=$update_id and entry_form=52");
				$rollId=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,1);
			}
		} 
		
		if($db_type==0)
		{
			if($query1 && $query4 && $query5 && $rID && $transID && $propoId && $rollId)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_system_id)."**".$issue_mst_id."**".str_replace("'","",$roll_maintained);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_return_no);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($query1 && $query4 && $query5 && $rID && $transID && $propoId && $rollId)
			{
				oci_commit($con);   
				echo "1**".str_replace("'","",$txt_system_id)."**".$issue_mst_id."**".str_replace("'","",$roll_maintained);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_return_no);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		 //no operation
	}		
}

if($action=="show_dtls_list_view")
{
	
	$ex_data = explode("**",$data);
	
	$sql = "select a.recv_number,a.company_id,a.supplier_id,a.receive_date,a.item_category,a.recv_number,b.id, b.cons_quantity, b.cons_uom, b.cons_rate, b.cons_amount, c.product_name_details, c.id as prod_id   
			from  inv_receive_master a, inv_transaction b left join product_details_master c on b.prod_id=c.id
			where a.id=b.mst_id and b.item_category=2 and b.transaction_type=4  and a.id=$ex_data[0]";
	//echo $sql;
	$result = sql_select($sql);
	$i=1;
	$rettotalQnty=0;
	$rcvtotalQnty=0;
	$rejtotalQnty=0;
	$totalAmount=0;
	?> 
     	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:600px" rules="all">
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Return No</th>
                    <th>Product ID</th>
                    <th>Item Description</th>
                    <th>Return Qty</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
				foreach($result as $row){					
					if($i%2==0)
						$bgcolor="#E9F3FF";
					else 
						$bgcolor="#FFFFFF";
 					
					$rettotalQnty +=$row[csf("cons_quantity")];
 					$totalAmount +=$row[csf("cons_amount")];
 					
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")]."**".$ex_data[1];?>","child_form_input_data","requires/finish_fabric_issue_return_controller")' style="cursor:pointer" >
                        <td width="50"><?php echo $i; ?></td>
                        <td width="120"><p><?php echo $row[csf("recv_number")]; ?></p></td>
                        <td width="100" align="center"><p><?php echo $row[csf("prod_id")]; ?></p></td>
                        <td width="250"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td align="right" style="padding-right:3px;"><p><?php echo $row[csf("cons_quantity")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                        <th colspan="4">Total</th>                         
                        <th><?php echo $rettotalQnty; ?></th> 
                   </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}

if($action=="child_form_input_data")
{
	//$data // transaction id
	$ex_data = explode("**",$data);
	$roll_maintained=str_replace("'","",$ex_data[1]);
  	$sql = "select b.id as prod_id, b.product_name_details, b.color, b.current_stock, a.id as tr_id, a.store_id, a.issue_id, a.cons_quantity, a.issue_challan_no,a.remarks, a.rack, a.self,a.batch_id_from_fissuertn, a.no_of_roll
			from inv_transaction a, product_details_master b
 			where a.id=$ex_data[0] and a.status_active=1 and a.item_category=2 and transaction_type=4 and a.prod_id=b.id and b.status_active=1";
 	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		$issue_purpose=return_field_value("issue_purpose","inv_issue_master","id='".$row[csf("issue_id")]."'");
		
		echo "return_qnty_basis(".$issue_purpose.");\n";
		echo "$('#cbo_store_name').val('".$row[csf("store_id")]."');\n";
		echo "$('#txt_batch_no').val('".$batch_arr[$row[csf("batch_id_from_fissuertn")]]."');\n";
		echo "$('#hidden_batch_id').val('".$row[csf("batch_id_from_fissuertn")]."');\n";
 		echo "$('#txt_fabric_desc').val('".$row[csf("product_name_details")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#before_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#txt_return_qnty').val('".$row[csf("cons_quantity")]."');\n"; 
		echo "$('#txt_no_of_roll').val('".$row[csf("no_of_roll")]."');\n";
		echo "$('#txt_rack').val('".$row[csf("rack")]."');\n";
		echo "$('#txt_shelf').val('".$row[csf("self")]."');\n";
 		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		echo "$('#txt_issue_id').val('".$row[csf("issue_id")]."');\n";
		
		$propotion_sql=sql_select("select po_breakdown_id, quantity from order_wise_pro_details where trans_id='".$row[csf("tr_id")]."'");
		$po_wise_qnty="";$po_id_all="";
		foreach($propotion_sql as $row_order)
		{
			if($po_wise_qnty!="") $po_wise_qnty .="_";
			$po_wise_qnty .=$row_order[csf("po_breakdown_id")]."**".$row_order[csf("quantity")];
			if($po_id_all!="") $po_id_all .=",";
			$po_id_all .=$row_order[csf("po_breakdown_id")];
		}
		if($roll_maintained==1)
		{
			$roll_sql=sql_select("select po_breakdown_id, roll_no, qnty from  pro_roll_details where mst_id='$issue_id' and dtls_id='".$row[csf("tr_id")]."'");
			$roll_ref="";
			foreach($roll_sql as $row_roll)
			{
				if($roll_ref!="") $roll_ref .="_";
				$roll_ref .=$row_roll[csf("po_breakdown_id")]."**".$row_roll[csf("roll_no")]."**".$row_roll[csf("qnty")];
			}
		}
		
		echo "$('#txt_break_qnty').val('$po_wise_qnty');\n";
		echo "$('#txt_break_roll').val('$roll_ref');\n";
		echo "$('#txt_order_id_all').val('$po_id_all');\n";
		
		
		$totalIssued = return_field_value("sum(b.cons_quantity)","inv_issue_master a, inv_transaction b"," a.id=b.mst_id and a.id='".$row[csf("issue_id")]."' and b.prod_id='".$row[csf("prod_id")]."' and b.item_category=2 and b.transaction_type=2");
		
		if($totalIssued=="") $totalIssued=0;
		echo "$('#txt_tot_issue').val('".$totalIssued."');\n";
		
		
		$totalReturn = return_field_value("sum(cons_quantity)","inv_transaction","issue_id='".$row[csf("issue_id")]."' and prod_id='".$row[csf("prod_id")]."' and item_category=2 and transaction_type=4");
		echo "$('#txt_total_return_display').val('".$totalReturn."');\n";
		$netUsed = $totalIssued-$totalReturn;
		echo "$('#txt_net_used').val('".$netUsed."');\n";
		echo "$('#hide_net_used').val('".$row[csf("cons_quantity")]."');\n";
		echo "$('#txt_global_stock').val('".$row[csf("current_stock")]."');\n";
		echo "$('#update_id').val(".$row[csf("tr_id")].");\n";
		
	}
 	echo "set_button_status(1, permission, 'fnc_fabric_issue_rtn',1,1);\n";		
  	exit();
}


if($action=="return_number_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_return_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="170">Search By</th>
                    <th width="270" align="center" id="search_by_td_up">Enter Return Number</th>
                    <th width="220">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr class="general">                    
                    <td>
                        <?php  
                            $search_by = array(1=>'Return Number');
							//$dd="change_search_event(this.value, '0*0', '0*0', '../../../') ";
							echo create_drop_down( "cbo_search_by", 140, $search_by,"",0, "--Select--", "",1,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_return_search_list_view', 'search_div', 'finish_fabric_issue_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_return_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div style="margin-top:5px" align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_return_search_list_view")
{
	
	$ex_data = explode("_",$data);
	$search_by = $ex_data[0];
	$search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
	
	$sql_cond="";
	if($search_by==1)
	{
		if($search_common!="") $sql_cond .= " and recv_number like '%$search_common'";
	}	 
	if( $txt_date_from!="" && $txt_date_to!="" ) 
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.receive_date  between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
	
	if($company!="") $sql_cond .= " and a.company_id='$company'";
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql = "select a.id as mst_id,a.recv_number_prefix_num, a.recv_number, a.supplier_id, a.receive_date, a.item_category, a.issue_id, $year_field b.id, b.cons_quantity,c.product_name_details, c.id as prod_id
			from inv_receive_master a, inv_transaction b left join product_details_master c on b.prod_id=c.id
			where a.id=b.mst_id and b.item_category=2 and b.transaction_type=4 and a.entry_form=52 $sql_cond order by a.id"; 
 	//echo $sql;die;
	$arr=array();
 	echo create_list_view("list_view", "Return No, Year, Item Description, Return Date, Return Qnty","80,60,300,100","700","260",0, $sql , "js_set_value", "mst_id,issue_id", "", 1, "0,0,0,0,0", $arr, "recv_number_prefix_num,year,product_name_details,receive_date,cons_quantity","","",'0,0,0,3,2') ;	
 	exit();
}

if($action=="populate_master_from_data")
{  
	
 	$sql = "select id,recv_number,company_id,receive_purpose,receive_date,challan_no,issue_id from inv_receive_master  where id='$data'";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
 		echo "set_button_status(0, permission, 'fnc_fabric_issue_rtn',1,1);";
		echo "$('#txt_system_id').val('".$row[csf("recv_number")]."');\n";
		echo "$('#issue_mst_id').val('".$row[csf("id")]."');\n";
		echo "$('#cbo_issue_purpose').val('".$row[csf("receive_purpose")]."');\n";
		echo "$('#txt_issue_id').val('".$row[csf("issue_id")]."');\n";
		$issue_num = return_field_value("issue_number"," inv_issue_master","id='".$row[csf("issue_id")]."'");
		echo "$('#txt_issue_no').val('$issue_num');\n";
		echo "return_qnty_basis('".$row[csf("receive_basis")]."');\n";
		echo "$('#txt_issue_date').val('".change_date_format($row[csf("receive_date")])."');\n";
		
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "disable_enable_fields( 'cbo_company_name', 1, '', '' );\n"; // disable true
				
   	}	
	exit();	
}
?>
