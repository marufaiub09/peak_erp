﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 150, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "--Select Location--", $selected, "","","","","","",3 );
	exit();	 
}

if ($action=="load_drop_down_party_name")
{
	$data=explode('_',$data);
	if($data[1]==2)
	{
		echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "show_list_view(document.getElementById('cbo_party_source').value+'_'+this.value,'dyingfinishing_delivery_list_view','dyeingfinishing_info_list','requires/sub_fabric_finishing_bill_issue_controller','setFilterGrid(\'list_view_issue\',-1)');","","","","","",5 );
	}
	else
	{
		echo create_drop_down( "cbo_party_name", 150, $blank_array,"", 1, "-- Select Party --", $selected, "",0,"","","","",5);
	}
	exit();
}

if ($action=="load_drop_down_party_name_popup")
{ 
	$data=explode('_',$data);
	echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "","","","","","",5 );
	exit(); 
}

if ($action=="load_variable_settings")
{
	echo "$('#variable_check').val(0);\n";
	$sql_result = sql_select("select dyeing_fin_bill from  variable_settings_subcon where company_id='$data' and variable_list=1 order by id");
 	foreach($sql_result as $result)
	{
		echo "$('#variable_check').val(".$result[csf("dyeing_fin_bill")].");\n";
		if ($result[csf("dyeing_fin_bill")]==1)
		{
			echo "$('#bill_on').text('Bill On Grey Qty');\n"; 
		}
		else if ($result[csf("dyeing_fin_bill")]==2)
		{
			echo "$('#bill_on').text('Bill On Delivery Qty');\n"; 
		}
	}
 	exit();
}

if ($action=="bill_no_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	$exdata=explode('_',$data);
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('issue_id').value=id;
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
        <div align="center">
            <form name="dyingfinishingbill_1"  id="dyingfinishingbill_1" autocomplete="off">
                <table width="720" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="150">Company Name</th>
                        <th width="150">Party Name</th>
                        <th width="80">Issue ID</th>
                        <th width="200">Date Range</th>
                        <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>           
                    </thead>
                    <tbody>
                        <tr>
                            <td> 
                                <input type="hidden" id="issue_id">  
                                <?php   
                                    echo create_drop_down( "cbo_company_id", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $exdata[0],"load_drop_down( 'sub_fabric_finishing_bill_issue_controller', this.value, 'load_drop_down_party_name_popup', 'party_td' );",0 );
                                ?>
                            </td>
                            <td width="150" id="party_td">
                                <?php
                                    echo create_drop_down( "cbo_party_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $exdata[1], "",0,"","","","");
                                ?> 
                            </td>
                            <td>
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes_numeric" style="width:75px" />
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:65px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:65px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value, 'dyeingfinishing_bill_list_view', 'search_div', 'sub_fabric_finishing_bill_issue_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" height="40" valign="middle">
                                <?php echo load_month_buttons(1);  ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                    </tbody>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if ($action=="dyeingfinishing_bill_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company_name=" and company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $party_name=" and party_id='$data[1]'"; else $party_name="";
	
	if($db_type==0)
	{ 
		if ($data[2]!="" &&  $data[3]!="") $return_date = "and bill_date between '".change_date_format($data[2],'yyyy-mm-dd')."' and '".change_date_format($data[3],'yyyy-mm-dd')."'"; else $return_date ="";
	}
	else
	{
		if ($data[2]!="" &&  $data[3]!="") $return_date = "and bill_date between '".change_date_format($data[2], "", "",1)."' and '".change_date_format($data[3], "", "",1)."'"; else $return_date ="";
	}
	
	//if ($data[2]!="" &&  $data[3]!="") $return_date = "and bill_date between '".change_date_format($data[2], "mm-dd-yyyy", "/",1)."' and '".change_date_format($data[3], "mm-dd-yyyy", "/",1)."'"; else $return_date="";
	if ($data[4]!='') $bill_id_cond=" and prefix_no_num='$data[4]'"; else $bill_id_cond="";
	
	$company_id=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$location=return_library_array( "select id,location_name from lib_location",'id','location_name');
	$party_arr=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
	
	$arr=array (2=>$location,4=>$party_arr,5=>$knitting_source,6=>$bill_for);
	
	if($db_type==0)
	{
		$year_cond= "year(insert_date)as year";
	}
	else if($db_type==2)
	{
		$year_cond= "TO_CHAR(insert_date,'YYYY') as year";
	}
	
	$sql= "select id, bill_no, prefix_no_num, $year_cond, location_id, bill_date, party_id, party_source, bill_for from subcon_inbound_bill_mst where process_id in (3,4) and status_active=1 $company_name $party_name $return_date $bill_id_cond order by id DESC";
	
	echo  create_list_view("list_view", "Bill No,Year,Location,Bill Date,Party,Source,Bill For", "50,40,80,70,100,140,140","700","250",0, $sql , "js_set_value", "id", "", 1, "0,0,location_id,0,party_id,party_source,bill_for", $arr , "prefix_no_num,year,location_id,bill_date,party_id,party_source,bill_for", "sub_fabric_finishing_bill_issue_controller","",'0,0,0,3,0,0,0');
	exit();
}

if ($action=="load_php_data_to_form_issue")
{
	$nameArray= sql_select("select id, bill_no, company_id, location_id, bill_date, party_id, party_source, bill_for from subcon_inbound_bill_mst where id='$data'");
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_bill_no').value 					= '".$row[csf("bill_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "load_drop_down( 'requires/sub_fabric_finishing_bill_issue_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_location', 'location_td' );\n";
		echo "document.getElementById('cbo_location_name').value			= '".$row[csf("location_id")]."';\n"; 
		echo "document.getElementById('txt_bill_date').value 				= '".change_date_format($row[csf("bill_date")])."';\n";   
		echo "document.getElementById('cbo_party_source').value				= '".$row[csf("party_source")]."';\n"; 
		echo "load_drop_down( 'requires/sub_fabric_finishing_bill_issue_controller', document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_party_source').value, 'load_drop_down_party_name', 'party_td' );\n";
		echo "document.getElementById('cbo_party_name').value				= '".$row[csf("party_id")]."';\n"; 
		echo "document.getElementById('cbo_bill_for').value					= '".$row[csf("bill_for")]."';\n"; 
	    echo "document.getElementById('update_id').value            		= '".$row[csf("id")]."';\n";
		//echo "show_list_view(document.getElementById('cbo_party_source').value+'_'+document.getElementById('cbo_party_name').value+'_'+document.getElementById('update_id').value+'_'+document.getElementById('issue_id_all').value,'dyingfinishing_delivery_list_view','dyeingfinishing_info_list','requires/sub_fabric_finishing_bill_issue_controller','set_all()');\n";
		echo "set_button_status(1, permission, 'fnc_dyeing_finishing_bill_issue',1);\n";
	}
	exit();
}

if ($action=="dyingfinishing_delivery_list_view")
{
	echo load_html_head_contents("Popup Info","../", 1, 1, $unicode,1,'');
	//echo $data;
	$data=explode('_',$data);
	if($data[0]==2)
	{
		?>
            <div >
                <table cellspacing="0" cellpadding="0" border="1" rules="all"  align="center" width="950px" class="rpt_table">
                    <thead>
						<th width="30">SL</th>
						<th width="90">Process</th>
						<th width="80">Challan No</th>
						<th width="70">Delivery Date</th>
						<th width="100">Order No</th>
                        <th width="120">Sub-Process</th>                    
						<th width="160">Fabric Description</th>
                        <th width="80">Grey Qty</th>
						<th width="80">Delivery Qty</th>
						<th width="" >Currency</th>
                    </thead>
                 </table>
            </div>
            <div style="width:950px;max-height:180px; overflow-y:scroll" id="sewing_production_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="932px" class="rpt_table" id="list_view_issue">
				<?php 
				$rec_febricdesc_arr=return_library_array( "select id,material_description from sub_material_dtls",'id','material_description');
				$prod_febricdesc_arr=return_library_array( "select id,fabric_description from subcon_production_dtls",'id','fabric_description');
				$prod_process_arr=return_library_array( "select cons_comp_id, process from subcon_production_dtls",'cons_comp_id','process');
				
				$grey_qty_array=array();
				$grey_fabric_array=array();
				$grey_sql="Select a.id as batch_id, b.id, b.fabric_from, b.po_id, b.item_description, b.fin_dia, b.batch_qnty from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and b.status_active=1 and b.is_deleted=0";
				$grey_sql_result =sql_select($grey_sql);
				
				foreach($grey_sql_result as $row)
				{
					$item_name=explode(',',$row[csf('item_description')]);
					$grey_qty_array[$row[csf('batch_id')]]=$row[csf('batch_qnty')];
					$grey_fabric_array[$row[csf('id')]]=$row[csf('item_description')];
				}
				// var_dump($grey_qty_array);
				$order_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
				$currency_arr=return_library_array( "select b.id, a.currency_id from  subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst",'id','currency_id');
                $i=1;
				/*if(!$data[3])
				{
					$sql="select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id, b.sub_process_id from  subcon_delivery_mst a, subcon_delivery_dtls b where b.bill_status=0 and a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and a.is_deleted=0"; 
				}
				else
				{
					$sql="(select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id, b.sub_process_id from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and b.bill_status=0)
					 union 
					 (select b.id, a.challan_no, a.delivery_date, b.process_id, b.item_id, b.delivery_qty, b.order_id, b.sub_process_id from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and b.id in ( $data[3] ) and a.status_active=1 )";
				}*/
				$var_id=str_replace("'",$data[3]);
				if($db_type==0)
				{
					if(!$data[3])
					{
						$sql="select a.challan_no, a.delivery_date, b.sub_process_id, b.batch_id, b.order_id, group_concat(b.id SEPARATOR '_') as id, group_concat(b.process_id SEPARATOR '_') as process_id, group_concat(b.item_id SEPARATOR '_') as item_id, sum(b.delivery_qty) as delivery_qty from  subcon_delivery_mst a, subcon_delivery_dtls b where b.bill_status=0 and a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and a.is_deleted=0 group by a.challan_no, a.delivery_date, b.sub_process_id, b.batch_id, b.order_id"; 
					}
					else
					{
						$sql="(select a.challan_no, a.delivery_date, b.sub_process_id, b.batch_id, b.order_id, group_concat(b.id SEPARATOR '_') as id, group_concat(b.process_id SEPARATOR '_') as process_id, group_concat(b.item_id SEPARATOR '_') as item_id, sum(b.delivery_qty) as delivery_qty from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and b.bill_status=0 group by a.challan_no, a.delivery_date,b.sub_process_id, b.batch_id, b.order_id)
						 union 
						 (select a.challan_no, a.delivery_date,b.sub_process_id, b.batch_id, b.order_id, group_concat(b.id SEPARATOR '_') as id, group_concat(b.process_id SEPARATOR '_') as process_id, group_concat(b.item_id SEPARATOR '_') as item_id, sum(b.delivery_qty) as delivery_qty from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and b.id in ( $data[3] ) and a.status_active=1 group by a.challan_no, a.delivery_date,b.sub_process_id, b.batch_id, b.order_id)";
					}
				}
				else if ($db_type==2)
				{
					if(!$data[3])
					{
						$sql="select a.challan_no, a.delivery_date, b.sub_process_id, b.batch_id, b.order_id,
						listagg(b.id,'_') within group (order by b.id) as id,
						listagg(b.process_id,'_') within group (order by b.process_id) as process_id,
						listagg(b.item_id,'_') within group (order by b.item_id) as item_id,
						sum(b.delivery_qty) as delivery_qty
		from  subcon_delivery_mst a, subcon_delivery_dtls b where b.bill_status=0 and a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and a.is_deleted=0 group by a.challan_no, a.delivery_date, b.sub_process_id, b.batch_id, b.order_id order by a.challan_no"; 
					}
					else
					{
						$sql="(select a.challan_no, a.delivery_date,b.sub_process_id, b.batch_id, b.order_id,
						listagg(b.id,'_') within group (order by b.id) as id,
						listagg(b.process_id,'_') within group (order by b.process_id) as process_id,
						listagg(b.item_id,'_') within group (order by b.item_id) as item_id,
						sum(b.delivery_qty) as delivery_qty
						from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and a.status_active=1 and b.bill_status=0 group by a.challan_no, a.delivery_date,b.sub_process_id, b.batch_id, b.order_id)
						 union 
						 (select a.challan_no, a.delivery_date,b.sub_process_id, b.batch_id, b.order_id,
						 listagg(b.id,'_') within group (order by b.id) as id,
						listagg(b.process_id,'_') within group (order by b.process_id) as process_id,
						listagg(b.item_id,'_') within group (order by b.item_id) as item_id,
						sum(b.delivery_qty) as delivery_qty
						  from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.party_id='$data[1]' and b.process_id in (3,4) and b.id in ( $data[3] ) and a.status_active=1 group by a.challan_no, a.delivery_date,b.sub_process_id, b.batch_id, b.order_id)";
					}
				}
				echo $sql;
				$sql_result =sql_select($sql);
					
				foreach($sql_result as $row)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					//if ($row[csf('type')]==0) $row_color=""; else $row_color="Yellow";
					$sub_process_id=array_unique(explode(',',$row[csf('sub_process_id')]));
					$subprocess_val='';
					foreach ($sub_process_id as $val)
					{
						if($subprocess_val=='') $subprocess_val=$conversion_cost_head_array[$val]; else $subprocess_val.=" + ".$conversion_cost_head_array[$val];
					}
					
					$process_id=array_unique(explode(',',$row[csf('process_id')]));
					$process_val='';
					foreach ($process_id as $val)
					{
						if($process_val=='') $process_val=$production_process[$val]; else $process_val.=", ".$production_process[$val];
					}
					
					$delivery_id=array_unique(explode(',',$row[csf('id')]));
					$delivery_id_val='';
					foreach ($delivery_id as $val)
					{
						if($delivery_id_val=='') $delivery_id_val=$val; else $delivery_id_val.="_".$val;
					}
					
					$item_id=array_unique(explode(',',$row[csf('item_id')]));
					$item_name=''; $grey_qty='';
					foreach ($item_id as $val)
					{
						if($item_name=='') $item_name=$grey_fabric_array[$val]; else $item_name.=",<br>".$grey_fabric_array[$val];
					}
					?>
					<tr id="tr_<?php echo $row[csf('id')]; ?>" bgcolor="<?php echo $bgcolor; ?>"  style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]."***".$currency_arr[$row[csf('order_id')]]; ?>');" >
						<td width="30" align="center"><?php echo $i; ?></td>
						<td width="90" align="center"><?php echo $process_val; ?></td>
						<td width="80" align="center"><p><?php echo $row[csf('challan_no')]; ?></p></td>
						<td width="70" align="center"><?php echo change_date_format($row[csf('delivery_date')]); ?></td>
						<td width="100" align="center"><?php echo $order_arr[$row[csf('order_id')]]; ?></td>
                        <td width="120" align="center"><?php echo $subprocess_val; ?></td>
						<td width="160" align="center"><p><?php echo $item_name; ?></p></td>
                        <td width="80" align="right"><?php echo $grey_qty_array[$row[csf('batch_id')]]; ?>&nbsp;</td>
						<td width="80" align="right"><?php echo $row[csf('delivery_qty')]; ?>&nbsp;</td>
						<td align="center"><?php echo $currency[$currency_arr[$row[csf('order_id')]]]; ?>
						<input type="hidden" id="currid<?php echo $row[csf('id')]; ?>" style="width:40px" value="<?php echo $currency_arr[$row[csf('order_id')]]; ?>"></td>
					</tr>
					<?php
					$i++;
				}
				?>
		   </table>
		</div>
		   <table width="933px">
				<tr align="center">
					<td align="center">
						<input type="button" id="show_button" align="middle" class="formbutton" style="width:100px" value="Close" onClick="window_close()" />
					</td>
				</tr>
		   </table>
	</body>           
	<script src="../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
    <?php
	}
	exit();
}

if ($action=="load_php_dtls_form")  //new issue 
{
	//echo $data;
	$data = explode("***",$data);
	
	$sel_id_delivery=explode("_",$data[0]);
	$selected_delivery_id='';
	foreach($sel_id_delivery as $val)
	{
		if($selected_delivery_id=='') $selected_delivery_id="'".$val."'"; else $selected_delivery_id.=','."'".$val."'";
	}
	
	//echo "'".implode("'",explode("_",$data[0]))."'";
	
	$id_delivery=explode("_",$data[1]); //die;
	$bill_delivery_id='';
	foreach($id_delivery as $val)
	{
		if($bill_delivery_id=='') $bill_delivery_id="'".$val."'"; else $bill_delivery_id.=','."'".$val."'";
	}
	//echo $bill_delivery_id;
	$del_id=array_diff(explode(",",$selected_delivery_id), explode(",",$bill_delivery_id));
	$bill_id=array_intersect(explode(",",$selected_delivery_id), explode(",",$bill_delivery_id));
	$delete_id=array_diff(explode(",",$bill_delivery_id), explode(",",$selected_delivery_id));
	$del_id=implode(",",$del_id); $bill_id=implode(",",$bill_id);   $delete_id=implode(",",$delete_id);
	$new_del_id=str_replace("'","",$del_id); //echo $delete_id; //die;
	$color_arr=return_library_array( "select id,color_name from lib_color",'id','color_name');
	$rec_febricdesc_arr=return_library_array( "select id,material_description from sub_material_dtls",'id','material_description');
	$prod_febricdesc_arr=return_library_array( "select id,fabric_description from subcon_production_dtls",'id','fabric_description');
	$color_id_arr=return_library_array( "select id, color_id from subcon_delivery_dtls",'id','color_id');
	$prod_process_arr=return_library_array( "select cons_comp_id, process from subcon_production_dtls",'cons_comp_id','process');
	
	$order_array=array();
	$order_sql="Select a.currency_id, b.id, b.order_no, b.order_uom, b.cust_buyer, b.cust_style_ref, b.main_process_id, b.process_id, b.rate, b.amount, c.process_loss from subcon_ord_mst a, subcon_ord_dtls b, subcon_ord_breakdown c where a.subcon_job=b.job_no_mst and b.id=c.order_id and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0";
	$order_sql_result =sql_select($order_sql);
	foreach ($order_sql_result as $row)
	{
		$order_array[$row[csf("id")]]['order_no']=$row[csf("order_no")];
		$order_array[$row[csf("id")]]['order_uom']=$row[csf("order_uom")];
		$order_array[$row[csf("id")]]['cust_buyer']=$row[csf("cust_buyer")];
		$order_array[$row[csf("id")]]['cust_style_ref']=$row[csf("cust_style_ref")];
		$order_array[$row[csf("id")]]['rate']=$row[csf("rate")];
		$order_array[$row[csf("id")]]['amount']=$row[csf("amount")];
		$order_array[$row[csf("id")]]['currency_id']=$row[csf("currency_id")];
		$order_array[$row[csf("id")]]['main_process_id']=$row[csf("main_process_id")];
		$order_array[$row[csf("id")]]['process_id']=$row[csf("process_id")];
		$order_array[$row[csf("id")]]['process_loss']=$row[csf("process_loss")];
	}
	
	$grey_qty_array=array();
	$grey_fabric_array=array();
	$grey_sql="Select a.id as batch_id,a.color_id, a.batch_no, a.extention_no, b.id, b.fabric_from, b.po_id,
	 b.item_description, b.fin_dia, b.batch_qnty from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and b.status_active=1 and b.is_deleted=0";
	$grey_sql_result =sql_select($grey_sql);
	foreach($grey_sql_result as $row)
	{
		$item_name=explode(',',$row[csf('item_description')]);
		$grey_qty_array[$row[csf('po_id')]]=$row[csf('batch_qnty')];
		$grey_fabric_array[$row[csf('id')]]=$row[csf('item_description')];
	}	
	
	if( $data[2]!="" )
	{
		$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id, add_process as sub_process_id from subcon_inbound_bill_dtls where mst_id in ($data[2]) and status_active=1 and process_id  in (3,4)"; 
	}
	else
	{
		if($bill_id!="" && $del_id!="")
			/*$sql="(select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id, add_process as sub_process_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id in (3,4))
			 union
			 (select 0 as upd_id, b.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, 0 as rate, 0 as amount, null as remarks, b.order_id, b.sub_process_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and b.process_id in (3,4))";*/
			 $sql="(select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id, add_process as sub_process_id, null as batch_id, null as color_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id in (3,4))
			 union
			 (select 0 as upd_id,  listagg(b.id,',') within group (order by b.id) as delivery_id, a.delivery_date, a.challan_no, listagg(b.item_id,',') within group (order by b.item_id) as item_id, sum(b.carton_roll) as carton_roll, sum(b.delivery_qty) as delivery_qty, null as rate, null as amount, null as remarks, b.order_id, b.sub_process_id, b.batch_id,  listagg(b.color_id,',') within group (order by b.color_id) as color_id from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($new_del_id) and a.status_active=1 and b.process_id in (3,4) group by   a.challan_no, a.delivery_date, b.sub_process_id, b.batch_id, b.order_id)";
		else if($bill_id!="" && $del_id=="")
			$sql="select id as upd_id, delivery_id, delivery_date, challan_no, item_id, packing_qnty as carton_roll, delivery_qty, rate, amount, remarks, order_id, add_process as sub_process_id,  null as color_id from subcon_inbound_bill_dtls where delivery_id in ($bill_id) and status_active=1 and is_deleted=0 and process_id in (3,4)";
		else  if($bill_id=="" && $del_id!="")
			//$sql="select 0 as upd_id, b.id as delivery_id, a.delivery_date, a.challan_no, b.item_id, b.carton_roll, b.delivery_qty, 0 as rate, 0 as amount, 0 as remarks, b.order_id, b.sub_process_id from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($del_id) and a.status_active=1 and a.is_deleted=0 and b.process_id in (3,4)";
			$sql="select 0 as upd_id,  listagg(b.id,',') within group (order by b.id) as delivery_id, a.delivery_date, a.challan_no, listagg(b.item_id,',') within group (order by b.item_id) as item_id, sum(b.carton_roll) as carton_roll, sum(b.delivery_qty) as delivery_qty, null as rate, null as amount, null as remarks, b.order_id, b.sub_process_id, b.batch_id,  listagg(b.color_id,',') within group (order by b.color_id) as color_id
 from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.id in ($new_del_id) and a.status_active=1 and a.is_deleted=0 and b.process_id in (3,4) group by   a.challan_no, a.delivery_date, b.sub_process_id, b.batch_id, b.order_id ";
	}
	echo $sql;
	$sql_result =sql_select($sql);
	$k=0;
	$num_rowss=count($sql_result);
	foreach ($sql_result as $row)
	{
		$k++;
		//echo $data[2];
		if( $data[2]!="" )
		{
			if($id=="") $id=$row[csf("delivery_id")]; else $id.="_".$row[csf("delivery_id")];
		}
		
		$color_id=array_unique(explode(',',$row[csf('color_id')]));
		$color_id_val='';
		foreach ($color_id as $val)
		{
			if($color_id_val=='') $color_id_val=$color_arr[$val]; else $color_id_val.=", ".$color_arr[$val];
		}
	?>
        <tr align="center">				
           <td>
				<?php if ($k==$num_rowss) { ?>
                    <input type="text" name="issue_id_all" id="issue_id_all"  style="width:50px" value="<?php echo $id; ?>" />
                    <input type="text" name="delete_id" id="delete_id"  style="width:50px" value="<?php echo $delete_id; ?>" />
                <?php } ?>
                <input type="hidden" name="curanci_<?php echo $k; ?>" id="curanci_<?php echo $k; ?>"  style="width:50px" value="<?php echo $order_array[$row[csf("order_id")]]['currency_id']; ?>" />
                <input type="text" name="updateiddtls_<?php echo $k; ?>" id="updateiddtls_<?php echo $k; ?>" value="<?php echo ($row[csf("upd_id")] != 0 ? $row[csf("upd_id")] : "") ?>">
                <input type="hidden" name="deliveryid_<?php echo $k; ?>" id="deliveryid_<?php echo $k; ?>" value="<?php echo $row[csf("delivery_id")]; ?>">
                <input type="text" name="txt_deleverydate_<?php echo $k; ?>" id="txt_deleverydate_<?php echo $k; ?>"  class="datepicker" style="width:60px" value="<?php echo change_date_format($row[csf("delivery_date")]); ?>" readonly />									
            </td>
            <td>
                <input type="text" name="txt_challenno_<?php echo $k; ?>" id="txt_challenno_<?php echo $k; ?>"  class="text_boxes" style="width:45px" value="<?php echo $row[csf("challan_no")]; ?>" readonly />							 
            </td>
            <td>
                <input type="hidden" name="ordernoid_<?php echo $k; ?>" id="ordernoid_<?php echo $k; ?>" value="<?php echo $row[csf("order_id")]; ?>" style="width:40px" > 
                <input type="text" name="txt_orderno_<?php echo $k; ?>" id="txt_orderno_<?php echo $k; ?>"  class="text_boxes" style="width:65px" value="<?php echo $order_array[$row[csf("order_id")]]['order_no']; ?>" readonly />										
            </td>
            <td>
                <input type="text" name="txt_stylename_<?php echo $k; ?>" id="txt_stylename_<?php echo $k; ?>"  class="text_boxes" style="width:75px;" value="<?php echo $order_array[$row[csf("order_id")]]['cust_style_ref']; ?>"  />
            </td>
            <td>
                <input type="text" name="txt_buyername_<?php echo $k; ?>" id="txt_buyername_<?php echo $k; ?>"  class="text_boxes" style="width:65px" value="<?php echo $order_array[$row[csf("order_id")]]['cust_buyer']; ?>"  />								
            </td>
            <td>			
                <input name="txt_numberroll_<?php echo $k; ?>" id="txt_numberroll_<?php echo $k; ?>" type="text" class="text_boxes" style="width:40px" value="<?php echo $row[csf("carton_roll")]; ?>" readonly />							
            </td> 
            <td>
                <input type="hidden" name="itemid_<?php echo $k; ?>" id="itemid_<?php echo $k; ?>" value="<?php echo $row[csf("item_id")]; ?>">
                <?php
					$item_id=explode(',',$row[csf("item_id")]);
					$item_name='';
					foreach($item_id as $val)
					{
						if($item_name=='') $item_name=$grey_fabric_array[$val]; else $item_name.=",".$grey_fabric_array[$val];
					}
					//$item_name=$grey_fabric_array[$row[csf('item_id')]];
				?>
                <input type="text" name="text_febricdesc_<?php echo $k; ?>" id="text_febricdesc_<?php echo $k; ?>"  class="text_boxes" style="width:100px" value="<?php echo $item_name; ?>" readonly/>
            </td>
            <td>
                 <input type="hidden" name="color_process_<?php echo $k; ?>" id="color_process_<?php echo $k; ?>" value="<?php echo $order_array[$row[csf("order_id")]]['main_process_id']; ?>">
                 <input type="hidden" name="color_id_<?php echo $k; ?>" id="color_id_<?php echo $k; ?>" value="<?php echo $color_id_arr[$row[csf("delivery_id")]]; ?>">
                <input type="text" name="txt_color_process_<?php echo $k; ?>" id="txt_color_process_<?php echo $k; ?>"  class="text_boxes" style="width:80px" value="<?php echo $color_id_val.'/'.$production_process[$order_array[$row[csf("order_id")]]['main_process_id']];//$grey_color_array[$row[csf('order_id')]][$row[csf('item_id')]] ?>" readonly/>
            </td>
            <td>
				<?php
					$process=explode(',',$row[csf("sub_process_id")]);
					$add_process="";
					foreach($process as $inf)
					{
						if($add_process=="") $add_process=$conversion_cost_head_array[$inf]; else $add_process.=",".$conversion_cost_head_array[$inf];
					}
                ?>

                <input type="hidden" name="add_process_<?php echo $k; ?>" id="add_process_<?php echo $k; ?>" value="<?php echo $row[csf("sub_process_id")]; ?>">
                <input type="text" name="txt_add_process_<?php echo $k; ?>" id="txt_add_process_<?php echo $k; ?>" class="text_boxes" style="width:115px" value="<?php echo $add_process; ?>" readonly/>
            </td>
            <td>
            	<?php
					$bill_qty='';
					if($row[csf("upd_id")]==0)
					{
						if($data[3]==1)
						{
							$gray_qty=$row[csf("delivery_qty")]/(1-($order_array[$row[csf("order_id")]]['process_loss'] /100));
							$bill_qty=$gray_qty;
						}
						else
						{
							$bill_qty=$row[csf("delivery_qty")];
						}
					}
					else
					{
						$bill_qty=$row[csf("delivery_qty")] ;
					}
				?>
                <input type="text" name="txt_deliveryqnty_<?php echo $k; ?>" id="txt_deliveryqnty_<?php echo $k; ?>" onBlur="qnty_caluculation(<?php echo $k; ?>);" class="text_boxes_numeric" style="width:50px" value="<?php echo number_format($bill_qty, 2, '.', ''); ?>"  />
            </td>
            <td>
            	<?php
					$rate_change='';
					if($row[csf("upd_id")]!=0)
					{
						$rate_change=$row[csf("rate")] ;
					}
					else
					{
						$rate_change=$order_array[$row[csf("order_id")]]['rate'];
					}
				
				?>
                <input type="text" name="txt_rate_<?php echo $k; ?>" id="txt_rate_<?php echo $k; ?>" onBlur="qnty_caluculation(<?php echo $k; ?>);"  class="text_boxes_numeric" style="width:40px" value="<?php echo $rate_change; ?>" />
            </td>
            <td>
				<?php
					$total_amount=$bill_qty*$rate_change;
                ?>
                <input type="text" name="txt_amount_<?php echo $k; ?>" id="txt_amount_<?php echo $k; ?>" style="width:55px"  class="text_boxes_numeric"  value="<?php echo  $total_amount; ?>" readonly />
            </td>
            <td>
                <input type="text" name="txt_remarks_<?php echo $k; ?>" id="txt_remarks_<?php echo $k; ?>"  class="text_boxes" style="width:80px" value="<?php echo $row[csf("remarks")]; ?>" />
           </td>
        </tr>
	<?php	
	}
	exit();
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)   // Insert Here========================================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if (is_duplicate_field( "delivery_id", "subcon_inbound_bill_dtls", "mst_id=$update_id" )==1)
		{
		echo "11**0"; 
		die;			
		}
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)";	
		}
		else if($db_type==2)
		{
			$year_cond=" and TO_CHAR(insert_date,'YYYY')";	
		}
		
		for($i=1; $i<=$tot_row; $i++)
		{
			$color_process="color_process_".$i;  
		}
		/*if (str_replace("'",'',$$color_process)==3)
		{
			$new_bill_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'DYE', date("Y",time()), 5, "select prefix_no,prefix_no_num from  subcon_inbound_bill_mst where company_id=$cbo_company_id and process_id=".$$color_process." $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
		}
		else if (str_replace("'",'',$$color_process)==4)
		{*/
			$new_bill_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'DFB', date("Y",time()), 5, "select prefix_no, prefix_no_num from  subcon_inbound_bill_mst where company_id=$cbo_company_id and process_id=".$$color_process." $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
		//}
			$id=return_next_id( "id", "subcon_inbound_bill_mst", 1 ) ; 	
		//if(str_replace("'",'',$update_id)=="")
		//{
			$field_array="id, prefix_no, prefix_no_num, bill_no, company_id, location_id, bill_date, party_id, party_source, bill_for, process_id, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_bill_no[1]."','".$new_bill_no[2]."','".$new_bill_no[0]."',".$cbo_company_id.",".$cbo_location_name.",".$txt_bill_date.",".$cbo_party_name.",".$cbo_party_source.",".$cbo_bill_for.",".$$color_process.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
			//echo "INSERT INTO subcon_inbound_bill_mst (".$field_array.") VALUES ".$data_array; //die;
			$rID=sql_insert("subcon_inbound_bill_mst",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;
			$return_no=$new_bill_no[0];
		/*}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="bill_no*company_id*location_id*bill_date*party_id*party_source*bill_for*updated_by*update_date";
			$data_array="".$txt_bill_no."*".$cbo_company_id."*".$cbo_location_name."*".$txt_bill_date."*".$cbo_party_name."*".$cbo_party_source."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			$rID=sql_update("subcon_inbound_bill_mst",$field_array,$data_array,"id",$update_id,0);
			$return_no=str_replace("'",'',$txt_bill_no);
		}*/
		$id1=return_next_id( "id", "subcon_inbound_bill_dtls",1);
		$field_array1 ="id, mst_id, delivery_id, delivery_date, challan_no, order_id, item_id, add_process, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id, color_id, inserted_by, insert_date";
		$field_array_up ="delivery_id*delivery_date*challan_no*order_id*item_id*add_process*color_id*packing_qnty*delivery_qty*rate*amount*remarks*currency_id*updated_by*update_date";
		$field_array_delivery="bill_status";
		$field_array_order="cust_buyer*cust_style_ref";
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
		{
			$delivery_id="deliveryid_".$i;
			$delevery_date="txt_deleverydate_".$i;
			$challen_no="txt_challenno_".$i;
			$orderid="ordernoid_".$i;
			$item_id="itemid_".$i;
			$style_name="txt_stylename_".$i;
			$buyer_name="txt_buyername_".$i;
			$number_roll="txt_numberroll_".$i;
			$quantity="txt_deliveryqnty_".$i;
			$rate="txt_rate_".$i;
			$amount="txt_amount_".$i;
			$curanci="curanci_".$i;
			$remarks="txt_remarks_".$i;
			$updateid_dtls="updateiddtls_".$i;
			$color_process="color_process_".$i;
			$color_id="color_id_".$i;
			$add_process="add_process_".$i;
			  
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{ 
				if ($add_comma!=0) $data_array1.=",";
				$data_array1.="(".$id1.",".$id.",".$$delivery_id.",".$$delevery_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$add_process.",".$$number_roll.",".$$quantity.",".$$rate.",".$$amount.",".$$remarks.",'".str_replace("'",'',$$curanci)."','".str_replace("'",'',$$color_process)."','".str_replace("'",'',$$color_id)."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id1=$id1+1;
				$add_comma++;
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1")); 
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$delivery_id."*".$$delevery_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$add_process."*".$$color_id."*".$$number_roll."*".$$quantity."*".$$rate."*".$$amount."*".$$remarks."*".$$curanci."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1"));
			}
			 
			//order table insert====================================================================================================
			if(str_replace("'",'',$$style_name)=="" || str_replace("'",'',$$buyer_name)=="")  
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));
			}
			else
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));	
			}
		}
		$rID1=execute_query(bulk_update_sql_statement("subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
		//if($rID1) $flag=1; else $flag=0;
			//echo bulk_update_sql_statement( "subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr );
		$rID2=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery ));
		if($rID2) $flag=1; else $flag=0;
			//echo $data_array1;die;
		$rID4=execute_query(bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr ));
		if($data_array1!="")
		{
			//echo "insert into subcon_inbound_bill_dtls (".$field_array1.") values ".$data_array1; die; 
			$rID1=sql_insert("subcon_inbound_bill_dtls",$field_array1,$data_array1,1);
			if($rID1) $flag=1; else $flag=0;
		}
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else if($flag==1)
			{
				mysql_query("ROLLBACK");  
				echo "5**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else if($flag==1)
			{
				oci_rollback($con);
				echo "5**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here=============================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		 //$delete_id=str_replace("'","",$delete_id);
		 //echo $delete_id="'".$delete_id."'"; die;
		$id=str_replace("'",'',$update_id);
		$field_array="bill_no*company_id*location_id*bill_date*party_id*party_source*bill_for*updated_by*update_date";
		$data_array="".$txt_bill_no."*".$cbo_company_id."*".$cbo_location_name."*".$txt_bill_date."*".$cbo_party_name."*".$cbo_party_source."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		$rID=sql_update("subcon_inbound_bill_mst",$field_array,$data_array,"id",$id,0);
		if($rID) $flag=1; else $flag=0;	
		$return_no=str_replace("'",'',$txt_bill_no);
		$id1=return_next_id( "id", "subcon_inbound_bill_dtls",1);
		$field_array1 ="id, mst_id, delivery_id, delivery_date, challan_no, order_id, item_id, add_process, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id, color_id, inserted_by, insert_date";		
		$field_array_up ="delivery_id*delivery_date*challan_no*order_id*item_id*add_process*color_id*packing_qnty*delivery_qty*rate*amount*remarks*currency_id*updated_by*update_date";
		$field_array_delivery="bill_status";
		$field_array_order="cust_buyer*cust_style_ref";
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
		{
			$delivery_id="deliveryid_".$i;
			$delevery_date="txt_deleverydate_".$i;
			$challen_no="txt_challenno_".$i;
			$orderid="ordernoid_".$i;
			$item_id="itemid_".$i;
			$style_name="txt_stylename_".$i;
			$buyer_name="txt_buyername_".$i;
			$number_roll="txt_numberroll_".$i;
			$quantity="txt_deliveryqnty_".$i;
			$rate="txt_rate_".$i;
			$amount="txt_amount_".$i;
			$curanci="curanci_".$i;
			$remarks="txt_remarks_".$i;
			$updateid_dtls="updateiddtls_".$i;
			$color_process="color_process_".$i;
			$color_id="color_id_".$i;
			$add_process="add_process_".$i;
				
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{ 
				  if ($add_comma!=0) $data_array1 .=",";
				  $data_array1 .="(".$id1.",".$id.",".$$delivery_id.",".$$delevery_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$add_process.",".$$number_roll.",".$$quantity.",".$$rate.",".$$amount.",".$$remarks.",'".str_replace("'",'',$$curanci)."',".$$color_process.",".$$color_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				  $id1=$id1+1;
				  $add_comma++;
				
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1")); 
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$delivery_id."*".$$delevery_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$add_process."*".$$color_id."*".$$number_roll."*".$$quantity."*".$$rate."*".$$amount."*".$$remarks."*".$$curanci."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1"));
			}
		//order table insert====================================================================================================
			if(str_replace("'",'',$$style_name)=="" || str_replace("'",'',$$buyer_name)=="")  
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));
			}
			else
			{
				$order_id_arr[]=str_replace("'",'',$$orderid);
				$data_array_order[str_replace("'",'',$$orderid)] =explode("*",("".$$buyer_name."*".$$style_name.""));	
			}
		//order table insert====================================================================================================
		}
	 
		//echo bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery );die;
		//$rID2=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_array_delivery,$id_arr_delivery ));
		//if($rID2) $flag=1; else $flag=0;
	
		//echo bulk_update_sql_statement( "subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr );
		$rID1=execute_query(bulk_update_sql_statement("subcon_inbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
		if($rID1) $flag=1; else $flag=0;
		//echo bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr );die;
		$rID4=execute_query(bulk_update_sql_statement( "subcon_ord_dtls", "id",$field_array_order,$data_array_order,$order_id_arr ));
		if($rID4) $flag=1; else $flag=0;	
		if($data_array1!="")
		{
			//echo "insert into subcon_inbound_bill_dtls (".$field_array1.") values ".$data_array1;die;
			$rID5=sql_insert("subcon_inbound_bill_dtls",$field_array1,$data_array1,1);
			if($rID5) $flag=1; else $flag=0;
		}
		
		if(str_replace("'",'',$delete_id)!="")
		{
			$delete_id=str_replace("'",'',$delete_id);
			//echo "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id)"; die;
			$rID6=execute_query( "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id)",0);
			if($rID6) $flag=1; else $flag=0;
			$delete_id=explode(",",str_replace("'",'',$delete_id));
			for ($i=0;$i<count($delete_id);$i++)
			{
				$id_delivery[]=$delete_id[$i];
				$data_delivery[str_replace("'",'',$delete_id[$i])] =explode(",",("0"));
			}
			//$rID3=execute_query(bulk_update_sql_statement( "subcon_delivery_dtls", "id",$field_array_delivery,$data_delivery,$id_delivery ));
			//if($rID3) $flag=1; else $flag=0;	
		}
		//echo bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery );
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		disconnect($con);
		die;
	}
	else if ($operation==2)  //Delete here======================================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$id=str_replace("'",'',$update_id);
		$return_no=str_replace("'",'',$txt_bill_no);
		$field_array_delivery="bill_status";
		if(str_replace("'",'',$delete_id)!="")
		{
			$delete_id=str_replace("'",'',$delete_id);
			$rID3=execute_query( "delete from subcon_inbound_bill_dtls where delivery_id in ($delete_id)",0);
			$delete_id=explode(",",str_replace("'",'',$delete_id));
			for ($i=0;$i<count($delete_id);$i++)
			{
				$id_delivery[]=$delete_id[$i];
				$data_delivery[str_replace("'",'',$delete_id[$i])] =explode(",",("0"));
			}
		}
		//echo bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery );
		$rID4=execute_query(bulk_update_sql_statement( "subcon_delivery", "id",$field_array_delivery,$data_delivery,$id_delivery ));
		
		if($db_type==0)
		{
			if($rID3 && $rID4)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		disconnect($con);
		die;
	}
}
//======================================================================Bill Print============================================================================================
if($action=="fabric_finishing_print") 
 {
    extract($_REQUEST);
	$data=explode('*',$data);

	$imge_arr=return_library_array( "select master_tble_id,image_location from common_photo_library",'master_tble_id','image_location');
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name");
	$party_library=return_library_array( "select id,buyer_name from lib_buyer", "id","buyer_name");
	$color_library=return_library_array( "select id,color_name from  lib_color", "id","color_name");
	$color_id_arr=return_library_array( "select id, color_id from subcon_delivery_dtls",'id','color_id');
	$inv_item_arr=return_library_array( "select id,material_description from sub_material_dtls",'id','material_description');
	$prod_item_arr=return_library_array( "select id,fabric_description from subcon_production_dtls",'id','fabric_description');
	$prod_process_arr=return_library_array( "select cons_comp_id, process from subcon_production_dtls",'cons_comp_id','process');
	
	$batch_array=array();
	$grey_color_array=array();
	$grey_sql="Select a.color_id, b.fabric_from, b.po_id, b.id, b.item_description from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and b.status_active=1 and b.is_deleted=0";
	$grey_sql_result =sql_select($grey_sql);
	foreach($grey_sql_result as $row)
	{
		//$batch_array[$row[csf('id')]]=$row[csf('fabric_from')];
		$batch_array[$row[csf('id')]]['color']=$row[csf('color_id')];
		$batch_array[$row[csf('id')]]['item_description']=$row[csf('item_description')];
	}	
	
	$order_array=array();
	$order_sql="select id, order_no, order_uom, cust_buyer, cust_style_ref from subcon_ord_dtls where status_active=1 and is_deleted=0";
	$order_sql_result =sql_select($order_sql);
	foreach($order_sql_result as $row)
	{
		$order_array[$row[csf('id')]]['order_no']=$row[csf('order_no')];
		$order_array[$row[csf('id')]]['order_uom']=$row[csf('order_uom')];
		$order_array[$row[csf('id')]]['cust_buyer']=$row[csf('cust_buyer')];
		$order_array[$row[csf('id')]]['cust_style_ref']=$row[csf('cust_style_ref')];
	}
	//var_dump($order_array);
	
	$sql_mst="Select id, bill_no, bill_date, party_id, party_source, bill_for, terms_and_condition from subcon_inbound_bill_mst where company_id=$data[0] and id='$data[1]' and status_active=1 and is_deleted=0";
	$dataArray=sql_select($sql_mst);
	?>
    <div style="width:930px;">
       <table width="100%" cellpadding="0" cellspacing="0" >
       <tr>
           <td width="70" align="right"> 
               <img  src='../<?php echo $imge_arr[str_replace("'","",$data[0])]; ?>' height='70%' width='70%' />
           </td>
           <td>
        <table width="800" cellspacing="0" align="center">
        <tr>
            <td align="center" style="font-size:x-large"><strong ><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td  align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no, level_no, road_no, block_no, contact_no, country_id, province, city, zip_code, contact_no, email, website from lib_company where id=$data[0] and status_active=1 and is_deleted=0"); 
					foreach ($nameArray as $result)
					{ 
					?>
						<?php echo $result[csf('plot_no')]; ?> &nbsp; 
                        <?php echo $result[csf('level_no')]?> &nbsp; 
                        <?php echo $result[csf('road_no')]; ?> &nbsp; 
                        <?php echo $result[csf('block_no')];?> &nbsp; 
                        <?php echo $result[csf('city')];?> &nbsp; 
                        <?php echo $result[csf('zip_code')]; ?> &nbsp; 
                        <?php echo $result[csf('province')];?> &nbsp; 
                        <?php echo $country_arr[$result[csf('country_id')]]; ?>&nbsp; <br>
                        <?php echo $result[csf('contact_no')];?> &nbsp; 
                        <?php echo $result[csf('email')];?> &nbsp; 
                        <?php echo $result[csf('website')]; 
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td align="center" style="font-size:18px"><strong><?php echo $data[3]; ?></strong></td>
        </tr>
        </table>
        </td>
        </tr>
    </table> 
    <table width="930" cellspacing="0" align="" border="0">   
    	  <tr><td colspan="6" align="center"><hr></hr></td></tr>
             <tr>
			 <?php
			 	if($dataArray[0][csf('party_source')]==2)
				{
					$party_add=$dataArray[0][csf('party_id')];
					$nameArray=sql_select( "select address_1, web_site, buyer_email, country_id from  lib_buyer where id=$party_add"); 
					foreach ($nameArray as $result)
					{ 
                    	$address="";
						if($result!="") $address=$result[csf('address_1')];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
					}
				}
			 ?>
             
                <td width="300" rowspan="4" valign="top" colspan="2"><strong>Party :<?php echo $party_library[$party_add]; ?><br><?php echo $address; ?> </strong></td>
                <td width="130"><strong>Bill No :</strong></td> <td width="175"><?php echo $dataArray[0][csf('bill_no')]; ?></td>
                <td width="130"><strong>Bill Date: </strong></td><td width="175px"> <?php echo change_date_format($dataArray[0][csf('bill_date')]); ?></td>
            </tr>
             <tr>
                <td><strong>Source :</strong></td> <td><?php echo $knitting_source[$dataArray[0][csf('party_source')]]; ?></td>
                <td><strong>&nbsp;</strong></td> <td><?php //echo $knitting_source[$dataArray[0][csf('party_source')]]; ?></td>
            </tr>
        </table>
         <br>
         <?php
		 if($data[4]==1)
		 {
		 ?>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="30">SL</th>
                <th width="65" align="center">Challan & <br> Delv. Date</th>
                <th width="90" align="center">Order</th> 
                <th width="70" align="center">Buyer  & <br> Style</th>
                <th width="100" align="center">Fabric Des.</th>
                <th width="70" align="center">Color</th>
                <th width="130" align="center">Process</th>
                <th width="30" align="center">Roll</th>
                <th width="60" align="center">Bill Qty</th>
                <th width="30" align="center">UOM</th>
                <th width="30" align="center">Rate</th>
                <th width="70" align="center">Amount</th>
                <th width="" align="center">Remarks</th>
            </thead>
		 <?php
     		$i=1;
			$mst_id=$dataArray[0][csf('id')];
			//echo "select id, delivery_id, delivery_date, challan_no, order_id, item_id, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id from subcon_inbound_bill_dtls  where mst_id='$mst_id' and process_id in (3,4) and status_active=1 and is_deleted=0";
			$sql_result =sql_select("select id, delivery_id, delivery_date, challan_no, order_id, item_id, color_id, packing_qnty, delivery_qty, rate, amount, remarks, currency_id, process_id, add_process from subcon_inbound_bill_dtls  where mst_id='$mst_id' and process_id in (3,4) and status_active=1 and is_deleted=0"); 
			foreach($sql_result as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				//echo $row[csf("add_process")].'kausar';
				$process=explode(',',$row[csf("add_process")]);
				$add_process="";
				foreach($process as $inf)
				{
					if($add_process=="") $add_process=$conversion_cost_head_array[$inf]; else $add_process.=", ".$conversion_cost_head_array[$inf];
				}
                
					/*if($grey_fabric_array[$row[csf('order_id')]][$row[csf('item_id')]]==1)
					{
						$item_name= $inv_item_arr[$row[csf('item_id')]];
					}
					else if($grey_fabric_array[$row[csf('order_id')]][$row[csf('item_id')]]==2)
					{
						$item_name= $prod_item_arr[$row[csf('item_id')]];
					}
					*/
				$item_all= explode(',',$batch_array[$row[csf('item_id')]]['item_description']);
				$item_name="";
				foreach($item_all as $inf)
				{
					if($item_name=="") $item_name=$inf; else $item_name.=", ".$inf;
				}
			?>
				<tr bgcolor="<?php echo $bgcolor; ?>"> 
                    <td><?php echo $i; ?></td>
                    <td align="center"><p><?php echo $row[csf('challan_no')].'<br>'.change_date_format($row[csf('delivery_date')]);; ?></p></td>
                    <td><p><?php echo $order_array[$row[csf('order_id')]]['order_no']; ?></p></td>
                    <td align="center"><p><?php echo $order_array[$row[csf('order_id')]]['cust_buyer'].'<br>'.$order_array[$row[csf('order_id')]]['cust_style_ref']; ?></p></td>
                    <td><p><?php echo $item_name; ?></p></td>
                    <td><p><?php echo $color_library[$row[csf('color_id')]];//$color_library[$grey_color_array[$row[csf('order_id')]][$row[csf('item_id')]]]; ?></p></td>
                    <td><p><?php echo $add_process; ?></p></td>
                    <td align="right"><p><?php echo $row[csf('packing_qnty')]; $tot_packing_qty+=$row[csf('packing_qnty')]; ?>&nbsp;</p></td>
                    <td align="right"><p><?php echo number_format($row[csf('delivery_qty')],2,'.',''); $tot_delivery_qty+=$row[csf('delivery_qty')]; ?>&nbsp;</p></td>
                    <td><p><?php echo $unit_of_measurement[$order_array[$row[csf('order_id')]]['order_uom']]; ?></p></td>
                    <td align="right"><p><?php echo number_format($row[csf('rate')],2,'.',''); ?>&nbsp;</p></td>
                    <td align="right"><p><?php echo number_format($row[csf('amount')],2,'.','');  $total_amount += $row[csf('amount')]; ?>&nbsp;</p></td>
                    <td><p><?php echo $row[csf('remarks')]; ?></p></td>
                    <?php 
					$carrency_id=$row['currency_id'];
					if($carrency_id==1){$paysa_sent="Paisa";} else if($carrency_id==2){$paysa_sent="CENTS";}
				   ?>
                </tr>
                <?php
                $i++;
			}
			?>
        	<tr> 
                <td align="right" colspan="7"><strong>Total</strong></td>
                <td align="right"><?php echo $tot_packing_qty; ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_delivery_qty,2,'.',''); ?>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="right"><?php echo $format_total_amount=number_format($total_amount,2,'.',''); ?>&nbsp;</td>
                <td>&nbsp;</td>
			</tr>
           <tr>
               <td colspan="13" align="left"><b>In Word: <?php echo number_to_words($format_total_amount,$currency[$carrency_id],$paysa_sent); ?></b></td>
           </tr>
        </table>
        <?php
		 }
		 elseif($data[4]==2)
		 {
			 ?>
            <div style="width:100%;">
            <table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
                <thead bgcolor="#dddddd" align="center">
                    <th width="30">SL</th>
                    <th width="65" align="center">Challan & <br> Delv. Date</th>
                    <th width="100" align="center">Order</th> 
                    <th width="80" align="center">Buyer  & <br> Style</th>
                    <th width="90" align="center">Color</th>
                    <th width="150" align="center">Process</th>
                    <th width="30" align="center">Roll</th>
                    <th width="80" align="center">Bill Qty</th>
                    <th width="50" align="center">Rate</th>
                    <th width="80" align="center">Amount</th>
                    <th width="" align="center">Remarks</th>
                </thead>
             <?php
                $i=1;
                $mst_id=$dataArray[0][csf('id')];
                //echo "select delivery_date, challan_no, order_id, max(item_id) as item_id, sum(packing_qnty) as packing_qnty, sum(delivery_qty) as delivery_qty, rate, sum(amount) as amount, add_process, max(remarks) as remarks from subcon_inbound_bill_dtls  where mst_id='$mst_id' and process_id in (3,4) and status_active=1 and is_deleted=0 group by delivery_date, challan_no, order_id, rate, add_process order by delivery_date, challan_no, order_id, rate, add_process";
                $sql_result =sql_select("select delivery_date, challan_no, order_id, color_id, sum(packing_qnty) as packing_qnty, sum(delivery_qty) as delivery_qty, rate, sum(amount) as amount, add_process, max(remarks) as remarks from subcon_inbound_bill_dtls  where mst_id='$mst_id' and process_id in (3,4) and status_active=1 and is_deleted=0 group by delivery_date, challan_no, order_id, color_id, rate, add_process order by delivery_date, challan_no, order_id, color_id, rate, add_process"); 
                foreach($sql_result as $row)
                {
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                    
					$process=explode(',',$row[csf("add_process")]);
					$add_process="";
					foreach($process as $inf)
					{
						if($add_process=="") $add_process=$conversion_cost_head_array[$inf]; else $add_process.=", ".$conversion_cost_head_array[$inf];
					}
				
					/*if($grey_fabric_array[$row[csf('order_id')]][$row[csf('item_id')]]==1)
					{
						$item_name= $inv_item_arr[$row[csf('item_id')]];
					}
					else if($grey_fabric_array[$row[csf('order_id')]][$row[csf('item_id')]]==2)
					{
						$item_name= $prod_item_arr[$row[csf('item_id')]];
					}
					*/
					//$item_name= $batch_array[$row[csf('item_id')]]['item_description'];
                ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>"> 
                        <td><?php echo $i; ?></td>
                        <td align="center"><p><?php echo $row[csf('challan_no')].'<br>'.change_date_format($row[csf('delivery_date')]);; ?></p></td>
                        <td><p><?php echo $order_array[$row[csf('order_id')]]['order_no']; ?></p></td>
                        <td align="center"><p><?php echo $order_array[$row[csf('order_id')]]['cust_buyer'].'<br>'.$order_array[$row[csf('order_id')]]['cust_style_ref']; ?></p></td>
                        <td><p><?php echo $color_library[$row[csf('color_id')]];//$color_library[$grey_color_array[$row[csf('order_id')]][$row[csf('item_id')]]]; ?></p></td>
                        <td><p><?php echo $add_process; ?></p></td>
                        <td align="right"><p><?php echo $row[csf('packing_qnty')]; $tot_packing_qty+=$row[csf('packing_qnty')]; ?>&nbsp;</p></td>
                        <td align="right"><p><?php echo number_format($row[csf('delivery_qty')],2,'.',''); $tot_delivery_qty+=$row[csf('delivery_qty')]; ?>&nbsp;</p></td>
                        <td align="right"><p><?php echo number_format($row[csf('rate')],2,'.',''); ?>&nbsp;</p></td>
                        <td align="right"><p><?php echo number_format($row[csf('amount')],2,'.','');  $total_amount += $row[csf('amount')]; ?>&nbsp;</p></td>
                        <td><p><?php echo $row[csf('remarks')]; ?></p></td>
                        <?php 
                        $carrency_id=$row['currency_id'];
                        if($carrency_id==1){$paysa_sent="Paisa";} else if($carrency_id==2){$paysa_sent="CENTS";}
                       ?>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
                <tr> 
                    <td align="right" colspan="6"><strong>Total</strong></td>
                    <td align="right"><?php echo $tot_packing_qty; ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($tot_delivery_qty,2,'.',''); ?>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right"><?php echo $format_total_amount=number_format($total_amount,2,'.',''); ?>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
               <tr>
                   <td colspan="11" align="left"><b>In Word: <?php echo number_to_words($format_total_amount,$currency[$carrency_id],$paysa_sent); ?></b></td>
               </tr>
            </table>
        <?php			 
		 }
		?>
        <table width="930" align="left" > 
        	<tr><td colspan="2">&nbsp;</td> </tr>
            <tr><td colspan="2" align="center"><b>TERMS & CONDITION</b></td> </tr>
        <?php
			/*$terms_cond_id=$dataArray[0][csf('terms_and_condition')];
			$bill_no=$dataArray[0][csf('bill_no')];
			if ($terms_cond_id!='')
			{*/
				
			$bill_no=$dataArray[0][csf('bill_no')];
			$sql_terms="Select id,terms from subcon_terms_condition where entry_form=2 and bill_no='$bill_no' ";
			$result_sql_terms =sql_select($sql_terms);
			//}
			$i=1;
			if(count($result_sql_terms)>0)
			{
				foreach($result_sql_terms as $rows)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>"> 
						<td width="30"><?php echo $i; ?></td>
						<td><p><?php echo $rows[csf('terms')]; ?></p></td>
					</tr>
				<?php
				$i++;
				}
			}
			?>
        </table>
        <br>
		 <?php
            echo signature_table(48, $data[0], "930px");
         ?>
   </div>
   </div>
<?php
}

if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Trems & Condition Search","../../", 1, 1, $unicode);
	extract($_REQUEST);
	
	$_SESSION['page_permission']=$permission;
?>
	<script>
	var permission='<?php echo $permission; ?>';
	function add_break_down_tr(i) 
	{
		var row_num=$('#tbl_termcondi_details tr').length-1;
		if (row_num!=i)
		{
			return false;
		}
		else
		{
			i++;
			
			$("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			'name': function(_, name) { return name + i },
			'value': function(_, value) { return value }              
			});  
			}).end().appendTo("#tbl_termcondi_details");
			$('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
			$('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
			$('#termscondition_'+i).val("");
			$('#sltd_'+i).val(i);
			//$('#sl_td').i
			//alert(i)
			//document.getElementById('sltd_'+i).innerHTML=i;
		}
	}

	function fn_deletebreak_down_tr(rowNo) 
	{   
		var numRow = $('table#tbl_termcondi_details tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_termcondi_details tbody tr:last').remove();
		}
	}

	function fnc_fabric_finishing_terms_condition( operation )
	{
		var row_num=$('#tbl_termcondi_details tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			if (form_validation('termscondition_'+i,'Term Condition')==false)
			{
				return;
			}
			data_all=data_all+get_submitted_data_string('txt_bill_no*termscondition_'+i,"../../");
		}
		var data="action=save_update_delete_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
		//freeze_window(operation);
		http.open("POST","sub_fabric_finishing_bill_issue_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_finishing_terms_condition_reponse;
	}

	function fnc_fabric_finishing_terms_condition_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split('**');
			//if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				parent.emailwindow.hide();
			}
		}
	}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
<?php echo load_freeze_divs ("../../",$permission);  ?>
    <fieldset>
    <input type="hidden" id="txt_bill_no" name="txt_bill_no" value="<?php echo str_replace("'","",$txt_bill_no) ?>"/>
        <form id="termscondi_1" autocomplete="off">
        <table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
            <thead>
                <tr>
                    <th width="50">Sl</th><th width="530">Terms</th><th ></th>
                </tr>
            </thead>
            <tbody>
				<?php
                $data_array=sql_select("select id, terms from  subcon_terms_condition where bill_no=$txt_bill_no");// quotation_id='$data'
                if(count($data_array)>0)
                {
					$i=0;
					foreach( $data_array as $row )
					{
						if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$i++;
						?>
						<tr id="settr_1" align="center" bgcolor="<?php echo $bgcolor;  ?>">
                            <td >
                                <input type="text" id="sltd_<?php echo $i;?>"   name="sltd_<?php echo $i;?>" style="width:100%;background-color:<?php echo $bgcolor;  ?>"  value="<?php echo $i; ?>"   /> 
                            </td>
                            <td>
                                <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row[csf('terms')]; ?>"  /> 
                            </td>
                            <td> 
                                <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?>);" />
                            </td>
                        </tr>
                        <?php
					}
                }
                else
                {
					$data_array=sql_select("select id, terms from  lib_terms_condition where is_default=1");// quotation_id='$data'
					foreach( $data_array as $row )
					{
						if ($i%2==0) $bgcolor="#E9F3FF";  else  $bgcolor="#FFFFFF";
						$i++;
						?>
						<tr id="settr_1" align="center" bgcolor="<?php echo $bgcolor;  ?>">
                            <td >
                                <input type="text" id="sltd_<?php echo $i;?>"   name="sltd_<?php echo $i;?>" style="width:100%; background-color:<?php echo $bgcolor;  ?>"  value="<?php echo $i; ?>"   /> 
                            </td>
                            <td>
                                <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row[csf('terms')]; ?>"  /> 
                            </td>
                            <td>
                                <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?> );" />
                            </td>
						</tr>
						<?php 
					}
                } 
                ?>
            </tbody>
        </table>
        <table width="650" cellspacing="0" class="" border="0">
            <tr>
                <td align="center" height="15" width="100%"> </td>
            </tr>
            <tr>
                <td align="center" width="100%" class="button_container">
					<?php
						echo load_submit_buttons( $permission, "fnc_fabric_finishing_terms_condition", 0,0 ,"reset_form('termscondi_1','','','','')",1) ; 
                    ?>
                </td> 
            </tr>
        </table>
        </form>
    </fieldset>
	</div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if($action=="save_update_delete_terms_condition")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		//if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id=return_next_id( "id", "subcon_terms_condition", 1 ) ;
		 $field_array="id,bill_no,terms,entry_form";
		 for ($i=1;$i<=$total_row;$i++)
		 {
			$termscondition="termscondition_".$i;
			if ($i!=1) $data_array .=",";
			$data_array .="(".$id.",".$txt_bill_no.",".$$termscondition.",2)";
			$id=$id+1;
		 }
		// echo  $data_array;
		$rID_de3=execute_query( "delete from subcon_terms_condition where  bill_no =".$txt_bill_no."",0);

		$rID=sql_insert("subcon_terms_condition",$field_array,$data_array,1);
		//check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$txt_bill_no;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$txt_bill_no;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "0**".$txt_bill_no;
			}
			else{
				oci_rollback($con);  
				echo "10**".$txt_bill_no;
			}
		}
		disconnect($con);
		die;
	}		
}
?>
