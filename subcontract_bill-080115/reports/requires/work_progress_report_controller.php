<? 
include('../../../includes/common.php');
session_start();
extract($_REQUEST);
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$date=date('Y-m-d');

$buyer_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$company_short_name_arr=return_library_array( "select id,company_short_name from lib_company",'id','company_short_name');
$imge_arr=return_library_array( "select master_tble_id,image_location from common_photo_library",'master_tble_id','image_location');

if ($action=="load_drop_down_buyer")
{ 
	echo create_drop_down( "cbo_buyer_id", 125, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
	exit();   	 
} 

if($action=="report_generate")
{ 
	//'cbo_company_id*cbo_buyer_id*cbo_process_id*cbo_search_by*cbo_year*txt_job_no*txt_style_ref*txt_order_no*txt_date_from*txt_date_to'
	$job_no=str_replace("'","",$txt_job_no);
	$txt_style_ref=str_replace("'","",$txt_style_ref);
	$txt_order_no=str_replace("'","",$txt_order_no);
	$year_id=str_replace("'","",$cbo_year);
	$cbo_process=str_replace("'","",$cbo_process_id);
	if($year_id!=0) $year_cond=" and TO_CHAR(a.insert_date,'YYYY')=$year_id"; else $year_cond="";
	
	if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in ($job_no) ";
	if(trim($txt_style_ref)!="") $style_ref_cond="%".trim($txt_style_ref)."%"; else $style_ref_cond="%%";
	if(trim($txt_order_no)!="") $order_no_cond="%".trim($txt_order_no)."%"; else $order_no_cond="%%";
	if ($cbo_process==0) $process_id_cond=""; else $process_id_cond=" and b.main_process_id=$cbo_process_id";
	
	if(str_replace("'","",trim($txt_date_from))=="" && str_replace("'","",trim($txt_date_to))=="")$date_cond="";
	else $date_cond=" and b.delivery_date between $txt_date_from and $txt_date_to";
	
	$inventory_array=array();
	if($db_type==0)
	{
		$inventory_sql="select mst_id as id, order_id, material_description,
		sum(CASE WHEN item_category_id='13' THEN  quantity END) AS kniting,
		sum(CASE WHEN item_category_id='2' THEN  quantity END) AS knit_finish,
		sum(CASE WHEN item_category_id='1' THEN  quantity END) AS yarn
		from sub_material_dtls where status_active=1 and is_deleted=0 group by order_id";
	}
	elseif($db_type==2)
	{
		$inventory_sql="select b.order_id, listagg(b.material_description,',') within group (order by b.material_description) as material_description,
		log_concat(distinct(CAST(b.material_description as varchar2(500)))) as material_description,
		sum(CASE WHEN b.item_category_id='13' THEN b.quantity END) AS kniting,
		sum(CASE WHEN b.item_category_id='2' THEN  b.quantity END) AS knit_finish,
		sum(CASE WHEN b.item_category_id='1' THEN  b.quantity END) AS yarn,
		sum(b.quantity) as quantity
		from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=2 and b.is_deleted=0 group by b.order_id";
	}
	
	$inventory_sql_result=sql_select($inventory_sql);
	foreach ($inventory_sql_result as $row)
	{
		$inventory_array[$row[csf('order_id')]]['id']=$row[csf('id')];
		$inventory_array[$row[csf('order_id')]]['material_description']=$row[csf('material_description')];
		$inventory_array[$row[csf('order_id')]]['kniting']=$row[csf('kniting')];
		$inventory_array[$row[csf('order_id')]]['knit_finish']=$row[csf('knit_finish')];
		$inventory_array[$row[csf('order_id')]]['yarn']=$row[csf('yarn')];
		$inventory_array[$row[csf('order_id')]]['quantity']=$row[csf('quantity')];
	}
	//var_dump($inventory_array);
	$delivery_array=array();
	$delivery_sql="select b.order_id,
	sum(CASE WHEN b.process_id='1' THEN  b.delivery_qty END) AS cutting,
	sum(CASE WHEN b.process_id='2' THEN  b.delivery_qty END) AS kniting,
	sum(CASE WHEN b.process_id='3' THEN  b.delivery_qty END) AS dyeing,
	sum(CASE WHEN b.process_id='4' THEN  b.delivery_qty END) AS finishing,
	sum(CASE WHEN b.process_id='5' THEN  b.delivery_qty END) AS sewing,
	sum(CASE WHEN b.process_id='6' THEN  b.delivery_qty END) AS fab_print,
	sum(CASE WHEN b.process_id='7' THEN  b.delivery_qty END) AS washing,
	sum(CASE WHEN b.process_id='8' THEN  b.delivery_qty END) AS printing
	from subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 group by b.order_id";
	$delivery_sql_result=sql_select($delivery_sql);
	foreach ($delivery_sql_result as $row)
	{
		$delivery_array[$row[csf('order_id')]]['item_id']=$row[csf('item_id')];
		$delivery_array[$row[csf('order_id')]]['cutting']=$row[csf('cutting')];
		$delivery_array[$row[csf('order_id')]]['kniting']=$row[csf('kniting')];
		$delivery_array[$row[csf('order_id')]]['dyeing']=$row[csf('dyeing')];
		$delivery_array[$row[csf('order_id')]]['finishing']=$row[csf('finishing')];
		$delivery_array[$row[csf('order_id')]]['sewing']=$row[csf('sewing')];
		$delivery_array[$row[csf('order_id')]]['fab_print']=$row[csf('fab_print')];
		$delivery_array[$row[csf('order_id')]]['washing']=$row[csf('washing')];
		$delivery_array[$row[csf('order_id')]]['printing']=$row[csf('printing')];
	}
	
	$fab_production_array=array();
	$fab_production_sql="select c.order_id,
	sum(CASE WHEN c.product_type='2' THEN  c.quantity END) AS kniting,
	sum(CASE WHEN c.product_type='4' THEN  c.quantity END) AS finishing,
	sum(CASE WHEN c.product_type='8' THEN  c.quantity END) AS printing
	from  subcon_production_mst a, subcon_production_dtls b, subcon_production_qnty c where a.id=b.mst_id and b.id=c.dtls_id and a.status_active=1  and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by c.order_id";
	$fab_production_sql_result=sql_select($fab_production_sql);
	foreach ($fab_production_sql_result as $row)
	{
		$order_id=explode(',',$row[csf('order_id')]);
		foreach ($order_id as $val)
		{
			$fab_production_array[$val]['fabric_description']=$row[csf('fabric_description')];
			$fab_production_array[$val]['kniting']=$row[csf('kniting')];
			$fab_production_array[$val]['finishing']=$row[csf('finishing')];
			$fab_production_array[$val]['printing']=$row[csf('printing')];
		}
	}
	//var_dump ($fab_production_array);
	$dying_data_array=array();
	if ($db_type==0)
	{
		$dying_sql="select c.po_id, sum(c.batch_qnty) as production_qnty from pro_fab_subprocess a, pro_batch_create_mst b, pro_batch_create_dtls c where a.batch_id=b.id and b.id=c.mst_id and a.entry_form=38 and a.result=1 and a.load_unload_id=2 and b.entry_form=36 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.po_id ";
	}
	elseif($db_type==2)
	{
		$dying_sql="select c.po_id, sum(c.batch_qnty) as production_qnty from pro_fab_subprocess a, pro_batch_create_mst b, pro_batch_create_dtls c where a.batch_id=b.id and b.id=c.mst_id and a.entry_form=38 and a.result=1 and a.load_unload_id=2 and b.entry_form=36 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.po_id ";
	}
	$dying_sql_result=sql_select($dying_sql);
	foreach ($dying_sql_result as $row)
	{
		$dying_data_array[$row[csf('po_id')]]=$row[csf('production_qnty')];
	}
	//var_dump($dying_data_array);
	
	$gmt_production_array=array();
	$gmt_production_sql="select order_id,
	sum(CASE WHEN production_type='1' THEN  production_qnty END) AS cutting,
	sum(CASE WHEN production_type='2' THEN  production_qnty END) AS sewing
	from subcon_gmts_prod_dtls where status_active=1 and is_deleted=0 group by order_id";
	$gmt_production_sql_result=sql_select($gmt_production_sql);
	foreach ($gmt_production_sql_result as $row)
	{
		$gmt_production_array[$row[csf('order_id')]]['cutting']=$row[csf('cutting')];
		$gmt_production_array[$row[csf('order_id')]]['sewing']=$row[csf('sewing')];
	}
	//var_dump($gmt_production_array);
	$in_bill_qty_array=array();
	$in_bill_amnt_array=array();
	$in_bill_sql="select order_id,
	sum(CASE WHEN process_id='1' THEN  delivery_qty END) AS cutting,
	sum(CASE WHEN process_id='2' THEN  delivery_qty END) AS kniting,
	sum(CASE WHEN process_id='3' THEN  delivery_qty END) AS dyeing,
	sum(CASE WHEN process_id='4' THEN  delivery_qty END) AS finishing,
	sum(CASE WHEN process_id='5' THEN  delivery_qty END) AS sewing,
	sum(CASE WHEN process_id='6' THEN  delivery_qty END) AS fab_print,
	sum(CASE WHEN process_id='7' THEN  delivery_qty END) AS washing,
	sum(CASE WHEN process_id='8' THEN  delivery_qty END) AS printing,
	
	sum(CASE WHEN process_id='1' THEN  amount END) AS am_cutting,
	sum(CASE WHEN process_id='2' THEN  amount END) AS am_kniting,
	sum(CASE WHEN process_id='3' THEN  amount END) AS am_dyeing,
	sum(CASE WHEN process_id='4' THEN  amount END) AS am_finishing,
	sum(CASE WHEN process_id='5' THEN  amount END) AS am_sewing,
	sum(CASE WHEN process_id='6' THEN  amount END) AS am_fab_print,
	sum(CASE WHEN process_id='7' THEN  amount END) AS am_washing,
	sum(CASE WHEN process_id='8' THEN  amount END) AS am_printing
	
	from subcon_inbound_bill_dtls where status_active=1 and is_deleted=0 group by order_id";
	$in_bill_sql_result=sql_select($in_bill_sql);
	foreach ($in_bill_sql_result as $row)
	{
		$in_bill_qty_array[$row[csf('order_id')]]['cutting']=$row[csf('cutting')];
		$in_bill_qty_array[$row[csf('order_id')]]['kniting']=$row[csf('kniting')];
		$in_bill_qty_array[$row[csf('order_id')]]['dyeing']=$row[csf('dyeing')];
		$in_bill_qty_array[$row[csf('order_id')]]['finishing']=$row[csf('finishing')];
		$in_bill_qty_array[$row[csf('order_id')]]['sewing']=$row[csf('sewing')];
		$in_bill_qty_array[$row[csf('order_id')]]['fab_print']=$row[csf('fab_print')];
		$in_bill_qty_array[$row[csf('order_id')]]['washing']=$row[csf('washing')];
		$in_bill_qty_array[$row[csf('order_id')]]['printing']=$row[csf('printing')];
		
		$in_bill_amnt_array[$row[csf('order_id')]]['am_cutting']=$row[csf('am_cutting')];
		$in_bill_amnt_array[$row[csf('order_id')]]['am_kniting']=$row[csf('am_kniting')];
		$in_bill_amnt_array[$row[csf('order_id')]]['am_dyeing']=$row[csf('am_dyeing')];
		$in_bill_amnt_array[$row[csf('order_id')]]['am_finishing']=$row[csf('am_finishing')];
		$in_bill_amnt_array[$row[csf('order_id')]]['am_sewing']=$row[csf('am_sewing')];
		$in_bill_amnt_array[$row[csf('order_id')]]['am_fab_print']=$row[csf('am_fab_print')];
		$in_bill_amnt_array[$row[csf('order_id')]]['am_washing']=$row[csf('am_washing')];
		$in_bill_amnt_array[$row[csf('order_id')]]['am_printing']=$row[csf('am_printing')];
	}
	/*$pay_rec_array=array();
	$pay_rec_sql="select order_id, production_date,
	sum(CASE WHEN production_type='1' THEN  production_qnty END) AS cutting,
	sum(CASE WHEN production_type='2' THEN  production_qnty END) AS kniting,
	sum(CASE WHEN production_type='3' THEN  production_qnty END) AS dyeing,
	sum(CASE WHEN production_type='4' THEN  production_qnty END) AS finishing,
	sum(CASE WHEN production_type='5' THEN  production_qnty END) AS sewing,
	sum(CASE WHEN production_type='6' THEN  production_qnty END) AS fab_print,
	sum(CASE WHEN production_type='7' THEN  production_qnty END) AS washing,
	sum(CASE WHEN production_type='8' THEN  production_qnty END) AS printing
	from subcon_gmts_prod_dtls where status_active=1 and is_deleted=0 group by order_id, production_date";
	$pay_rec_sql_result=sql_select($pay_rec_sql);
	foreach ($pay_rec_sql_result as $row)
	{
		$pay_rec_array[$row[csf('order_id')]]['cutting']=$row[csf('cutting')];
		$pay_rec_array[$row[csf('order_id')]]['kniting']=$row[csf('kniting')];
		$pay_rec_array[$row[csf('order_id')]]['dyeing']=$row[csf('dyeing')];
		$pay_rec_array[$row[csf('order_id')]]['finishing']=$row[csf('finishing')];
		$pay_rec_array[$row[csf('order_id')]]['sewing']=$row[csf('sewing')];
		$pay_rec_array[$row[csf('order_id')]]['fab_print']=$row[csf('fab_print')];
		$pay_rec_array[$row[csf('order_id')]]['washing']=$row[csf('washing')];
		$pay_rec_array[$row[csf('order_id')]]['printing']=$row[csf('printing')];
	}*/	
	//var_dump($in_bill_array);
	
	$batch_qty_array=array();
	$sql_batch="Select b.po_id, sum(b.batch_qnty) as batch_qnty from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_id";
	$sql_batch_result=sql_select($sql_batch);
	foreach ($sql_batch_result as $row)
	{
		$batch_qty_array[$row[csf('po_id')]]=$row[csf('batch_qnty')];
	}
	
	$job_sql = "select a.job_no_prefix_num, a.subcon_job, a.company_id, a.party_id, b.order_no, b.order_quantity, b.amount, b.order_rcv_date, b.delivery_date, b.main_process_id, b.order_uom, b.id, b.cust_style_ref 
	from subcon_ord_mst a, subcon_ord_dtls b
	where a.subcon_job=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.order_no like '$order_no_cond' $date_cond $searchCond $job_no_cond $process_id_cond
		group by a.job_no_prefix_num, a.subcon_job, a.company_id, a.party_id, b.order_no, b.order_quantity, b.amount, b.order_rcv_date, b.delivery_date, b.main_process_id, b.order_uom, b.id, b.cust_style_ref  order by b.main_process_id, a.job_no_prefix_num, b.order_no, b.delivery_date ";
		
	$job_sql_result=sql_select($job_sql);
	ob_start();
	if ($cbo_process==4)
	{
		$tbl_width=1510;
		$col_span=19;
	}
	else
	{
		$tbl_width=1350;
		$col_span=17;
	}
	$buyer_short_name_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$imge_arr=return_library_array( "select master_tble_id,image_location from common_photo_library",'master_tble_id','image_location');
	?>
    <div>
        <table width="<? echo $tbl_width; ?>" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table">
            <thead>
                <th width="30">SL</th>
                <th width="70">Job No</th>
                <th width="100">Buyer</th>
                <th width="80">Order no</th>
                <th width="50">Image</th>                            
                <th width="100">Style Name</th>
                <th width="90">Order Quantity</th>
                <th width="100">Order Value</th>
                <th width="60">UOM</th>
                <th width="90">Delivery Date</th>
                <th width="60">Days in Hand</th>
                <th width="120">Material Receive</th>
                <?
					if ($cbo_process==4)
					{
						?>
                        <th width="80">Batch Qty</th>
                        <th width="80">Dyeing Qty</th>
                        <?
					}
				?>
                <th width="80">Prod. Qty</th>
                <th width="80">Delivery Qty</th>
                <th width="80">Bill Qty</th>
                <th width="80">Bill Amount</th>
                <th>Payment Rec.</th>
            </thead>
        </table>
    <div style="max-height:400px; overflow-y:scroll; width:<? echo $tbl_width+20; ?>px" id="scroll_body">
        <table width="<? echo $tbl_width; ?>" border="1" class="rpt_table" rules="all" id="table_body">
    <?
	$process_array=array();
	$i=1; $k=1;
	foreach ($job_sql_result as $row)
	{
		if (!in_array($row[csf("main_process_id")],$process_array) )
		{
			if($k!=1)
			{
			?>
				<tr class="tbl_bottom">
					<td colspan="6" align="right"><b>Process Total:</b></td>
					<td align="right"><? echo number_format($tot_order_qty,2,'.',''); ?>&nbsp;</td>
					<td align="right"><? echo number_format($tot_order_val,2,'.',''); ?>&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right">&nbsp;</td>
					<td align="right"><? echo number_format($tot_rec_qty,2,'.',''); ?>&nbsp;</td>
                    <?
						if ($cbo_process==4)
						{
							?>
                                <td><? echo number_format($tot_batch_qty,2,'.',''); ?>&nbsp;</td>
                                <td><? echo number_format($tot_dyeing_qty,2,'.',''); ?>&nbsp;</td>
							<?
						}
					?>
					<td align="right"><? echo number_format($tot_prod_qty,2,'.',''); ?>&nbsp;</td>
					<td align="right"><? echo number_format($tot_del_qty,2,'.',''); ?>&nbsp;</td>
					<td align="right"><? echo number_format($tot_bill_qty,2,'.',''); ?>&nbsp;</td>
					<td align="right"><? echo number_format($tot_bill_amnt,2,'.',''); ?>&nbsp;</td>
					<td align="right">&nbsp;</td>
				</tr>
				<tr bgcolor="#dddddd">
					<td colspan="<? echo $col_span; ?>" align="left" ><b>Process : <? echo $production_process[$row[csf("main_process_id")]]; ?></b></td>
				</tr>
			<?
				unset($tot_order_qty);
				unset($tot_order_val);
				unset($tot_rec_qty);
				if ($cbo_process==4)
				{
					unset($tot_batch_qty);
					unset($tot_dyeing_qty);
				}
				unset($tot_prod_qty);
				unset($tot_del_qty);
				unset($tot_bill_qty);
				unset($tot_bill_amnt);
			}
			else
			{
				?>
				<tr bgcolor="#dddddd">
					<td colspan="<? echo $col_span; ?>" align="left" ><b>Process : <? echo $production_process[$row[csf("main_process_id")]]; ?></b></td>
				</tr>
				<?
			}					
			$process_array[]=$row[csf('main_process_id')];            
			$k++;
		}
        if ($i%2==0)  $bgcolor="#E9F3FF";else $bgcolor="#FFFFFF";

		$prod_qty=0; $del_qty=0; $bill_qty=0; $bill_amnt=0; $pay_rec=0;
		if ($row[csf('main_process_id')]==1)
		{
			$prod_qty=$gmt_production_array[$row[csf('id')]]['cutting'];
			$del_qty=$delivery_array[$row[csf('id')]]['cutting'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['cutting'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_cutting'];
			$batch_qty=""; $dyeing_qty="";
			//$pay_rec=
		}
		else if ($row[csf('main_process_id')]==2)
		{
			$prod_qty=$fab_production_array[$row[csf('id')]]['kniting'];
			$del_qty=$delivery_array[$row[csf('id')]]['kniting'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['kniting'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_kniting'];
			$batch_qty=""; $dyeing_qty="";
			//$pay_rec=
		}
		else if ($row[csf('main_process_id')]==3)
		{
			$prod_qty=$dying_data_array[$row[csf('id')]];
			$del_qty=$delivery_array[$row[csf('id')]]['dyeing'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['dyeing'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_dyeing'];
			$batch_qty=""; $dyeing_qty="";
			//$pay_rec=
		}
		else if ($row[csf('main_process_id')]==4)
		{
			$prod_qty=$fab_production_array[$row[csf('id')]]['finishing'];
			$del_qty=$delivery_array[$row[csf('id')]]['finishing'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['finishing'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_finishing'];
			$batch_qty=$batch_qty_array[$row[csf('id')]];
			$dyeing_qty=$dying_data_array[$row[csf('id')]];
			//$pay_rec=
		}
		else if ($row[csf('main_process_id')]==5)
		{
			$prod_qty=$gmt_production_array[$row[csf('id')]]['sewing'];
			$del_qty=$delivery_array[$row[csf('id')]]['sewing'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['sewing'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_sewing'];
			$batch_qty=""; $dyeing_qty="";
			//$pay_rec=
		}
		else if ($row[csf('main_process_id')]==6)
		{
			$prod_qty=$gmt_production_array[$row[csf('id')]]['fab_print'];
			$del_qty=$delivery_array[$row[csf('id')]]['fab_print'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['fab_print'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_fab_print'];
			$batch_qty=""; $dyeing_qty="";
			//$pay_rec=
		}
		else if ($row[csf('main_process_id')]==7)
		{
			$prod_qty=$gmt_production_array[$row[csf('id')]]['washing'];
			$del_qty=$delivery_array[$row[csf('id')]]['washing'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['washing'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_washing'];
			$batch_qty=""; $dyeing_qty="";
			//$pay_rec=
		}
		else if ($row[csf('main_process_id')]==8)
		{
			$prod_qty=$gmt_production_array[$row[csf('id')]]['printing'];
			$del_qty=$delivery_array[$row[csf('id')]]['printing'];
			$bill_qty=$in_bill_qty_array[$row[csf('id')]]['printing'];
			$bill_amnt=$in_bill_amnt_array[$row[csf('id')]]['am_printing'];
			$batch_qty=""; $dyeing_qty="";
			//$pay_rec=
		}
		?>
		<tr bgcolor="<? echo $bgcolor; ?>" style="vertical-align:middle" height="25" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i; ?>">	
        	<td width="30" bgcolor="<? echo $color; ?>"> <? echo $i; ?> </td>
            <td width="70" align="center"><p><? echo $row[csf('job_no_prefix_num')]; ?></td>
            <td width="100"><p><? echo $buyer_short_name_arr[$row[csf('party_id')]]; ?></p></td>
            <td width="80"><p><? echo $row[csf('order_no')]; ?></p></td>
            <td width="50"><img onclick="openImageWindow( <? echo $row[csf('job_no_prefix_num')]; ?> )" src='../../<? echo $imge_arr[$row[csf('job_no_prefix_num')]]; ?>' height='25' width='30' /></td>
            <td width="100"><p><? echo $row[csf('cust_style_ref')]; ?></p></td>
            <td width="90" align="right"><p><? echo $row[csf('order_quantity')]; ?>&nbsp;</p></td>
            <td width="100" align="right"><p><? echo number_format($row[csf('amount')],2); ?>&nbsp;</p></td>
            <td width="60" align="center"><p><? echo $unit_of_measurement[$row[csf('order_uom')]]; ?></p></td>
            <td width="90"><p><? echo change_date_format($row[csf('delivery_date')]); ?></p></td>
            <td width="60" align="center"><? $daysOnHand = datediff("d",$row[csf('delivery_date')],date("Y-m-d")); echo $daysOnHand; ?> </td>
            <td width="120" align="right"><p><a href="##" onclick="show_progress_report_details('material_desc_popup','<? echo $row[csf("id")]; ?>','850px')"><? echo number_format($inventory_array[$row[csf('id')]]['quantity'],2,'.',''); ?></a></p></td>
			<?
                if ($cbo_process==4)
                {
                    ?>
                    <td width="80" align="right"><p><a href="##" onclick="show_progress_report_details('batch_qty_pop_up','<? echo $row[csf("id")]; ?>','850px')"><? echo number_format($batch_qty,2,'.',''); ?>&nbsp;</a></p></td>
                    <td width="80" align="right"><p><a href="##" onclick="show_progress_report_details('dyeing_qty_pop_up','<? echo $row[csf("id")]; ?>_<? echo $row[csf('main_process_id')]; ?>','850px')"><? echo number_format($dyeing_qty,2,'.',''); ?>&nbsp;</a></p></td>
                    <?
                }
            ?>
            <td width="80" align="right"><p><a href="##" onclick="show_progress_report_details('product_qty_pop_up','<? echo $row[csf("id")]; ?>_<? echo $row[csf('main_process_id')]; ?>','850px')"><? echo number_format($prod_qty,2,'.',''); ?>&nbsp;</a></p></td>
            <td width="80" align="right"><p><a href="##" onclick="show_progress_report_details('delivery_qty_pop_up','<? echo $row[csf("id")]; ?>_<? echo $row[csf('main_process_id')]; ?>','850px')"><? echo number_format($del_qty,2,'.',''); ?>&nbsp;</a></p></td>
            <td width="80" align="right"><p><a href="##" onclick="show_progress_report_details('bill_qty_pop_up','<? echo $row[csf("id")]; ?>_<? echo $row[csf('main_process_id')]; ?>','850px')"><? echo number_format($bill_qty,2,'.',''); ?>&nbsp;</a></p></td>
            <td width="80" align="right"><? echo  number_format($bill_amnt,2,'.',''); ?></td>
            <td align="right"><p><? //echo $del_qty; ?>&nbsp;</p></td>
        </tr>
        <?
		$i++;
		$tot_order_qty+=$row[csf('order_quantity')];
		$tot_order_val+=$row[csf('amount')];
		$tot_rec_qty+=$inventory_array[$row[csf('id')]]['quantity'];
		$tot_prod_qty+=$prod_qty;
		$tot_del_qty+=$del_qty;
		$tot_bill_qty+=$bill_qty;
		$tot_bill_amnt+=$bill_amnt;
		
		if ($cbo_process==4)
		{
			$tot_batch_qty+=$batch_qty;
			$tot_dyeing_qty+=$dyeing_qty;
			
			$tot_tottal_batch_qty+=$batch_qty;
			$tot_total_dyeing_qty+=$dyeing_qty;
		}
		
		$tot_total_order_qty+=$row[csf('order_quantity')];
		$tot_total_order_val+=$row[csf('amount')];
		$tot_total_rec_qty+=$inventory_array[$row[csf('id')]]['quantity'];
		$tot_total_prod_qty+=$prod_qty;
		$tot_total_del_qty+=$del_qty;
		$tot_total_bill_qty+=$bill_qty;
		$tot_total_bill_amnt+=$bill_amnt;
	}
	?>
    <tr class="tbl_bottom">
        <td colspan="6" align="right"><b>Process Total:</b></td>
        <td align="right"><? echo number_format($tot_order_qty,2,'.',''); ?>&nbsp;</td>
        <td align="right"><? echo number_format($tot_order_val,2,'.',''); ?>&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right">&nbsp;</td>
        <td align="right"><? echo number_format($tot_rec_qty,2,'.',''); ?>&nbsp;</td>
        <?
			if ($cbo_process==4)
			{
				?>
				<td><? echo number_format($tot_batch_qty,2,'.',''); ?>&nbsp;</td>
				<td><? echo number_format($tot_dyeing_qty,2,'.',''); ?>&nbsp;</td>
				<?
			}
        ?>
        <td align="right"><? echo number_format($tot_prod_qty,2,'.',''); ?>&nbsp;</td>
        <td align="right"><? echo number_format($tot_del_qty,2,'.',''); ?>&nbsp;</td>
        <td align="right"><? echo number_format($tot_bill_qty,2,'.',''); ?>&nbsp;</td>
        <td align="right"><? echo number_format($tot_bill_amnt,2,'.',''); ?>&nbsp;</td>
        <td align="right">&nbsp;</td>
    </tr>
        <tr class="tbl_bottom">
            <td colspan="6" align="right">Grand Total:</td>
            <td align="right"><? echo number_format($tot_total_order_qty,2,'.',''); ?>&nbsp;</td>                            
            <td align="right"><? echo number_format($tot_total_order_val,2,'.',''); ?>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><? echo number_format($tot_total_rec_qty,2,'.',''); ?>&nbsp;</td>
            <?
                if ($cbo_process==4)
                {
                    ?>
                    <td><? echo number_format($tot_tottal_batch_qty,2,'.',''); ?>&nbsp;</td>
                    <td><? echo number_format($tot_total_dyeing_qty,2,'.',''); ?>&nbsp;</td>
                    <? 
                }
            ?>
            <td><? echo number_format($tot_total_prod_qty,2,'.',''); ?>&nbsp;</td>
            <td><? echo number_format($tot_total_del_qty,2,'.',''); ?>&nbsp;</td>
            <td><? echo number_format($tot_total_bill_qty,2,'.',''); ?>&nbsp;</td>
            <td><? echo number_format($tot_total_bill_amnt,2,'.',''); ?>&nbsp;</td>
            <td><? //echo number_format($tot_order_qty,2,'.',''); ?>&nbsp;</td>
        </tr>
    </table>        
        
    </div>
    </div>
    <?
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}

if($action=="material_desc_popup")
{
	echo load_html_head_contents("Material Description Details", "../../../", 1, 1,$unicode,'','');
	//echo $order_id;//die;
	$expData=explode('_',$order_id);
?>
        <fieldset style="width:820px">
            <div style="width:100%;" align="center">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                    	<tr>
                            <th width="30">SL</th>
                            <th width="60">Receive ID</th>
                            <th width="70">Challan No</th>
                            <th width="70">Rec. Date</th>
                            <th width="60">Party</th>
                            <th width="80">Order No</th>
                            <th width="80">Category</th>
                            <th width="150">Description</th>
                            <th width="80">Receive Qty</th>
                            <th width="80">Bag/ Roll</th>
                            <th>Cone</th>
                    	</tr>
                    </thead>
                </table>
            </div>  
            <div style="width:100%; max-height:230px; overflow-y:scroll" align="left">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1" >
                    <?
					$party_arr=return_library_array( "select id, short_name from  lib_buyer",'id','short_name');
					$po_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
                    $i=0;
					
                    $sql= "select a.sys_no, a.prefix_no_num, a.chalan_no, a.subcon_date, a.party_id, b.order_id, b.item_category_id, b.material_description, sum(b.quantity) as quantity, sum(b.subcon_roll) as subcon_roll, sum(b.rec_cone) as rec_cone from  sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and b.order_id in ($expData[0]) and a.status_active=1 and a.is_deleted=0 and b.status_active=2 and b.is_deleted=0 and a.trans_type=1 group by a.sys_no, a.prefix_no_num, a.chalan_no, a.subcon_date, a.party_id, b.order_id, b.item_category_id, b.id, b.material_description order by a.sys_no, a.subcon_date";
                   //echo $sql;
					$material_sql= sql_select($sql);
					foreach( $material_sql as $row )
                    {
                        $i++;
                        if ($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
                   ?>
                    <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="30"><? echo $i; ?></td>
                        <td width="60"><? echo $row[csf("prefix_no_num")];?> </td>
                        <td width="70"><? echo $row[csf("chalan_no")];?> </td>
                        <td width="70"><? echo change_date_format($row[csf("subcon_date")]);?> </td> 
                        <td width="60"><p><? echo $party_arr[$row[csf("party_id")]]; ?></p></td>
                        <td align="center" width="80"><? echo $po_arr[$row[csf("order_id")]]; ?></td>
                        <td align="center" width="80"><? echo $item_category[$row[csf("item_category_id")]]; ?></td>
                        <td align="center" width="150"><? echo $row[csf("material_description")]; ?></td>
                        <td align="right" width="80"><? echo number_format($row[csf("quantity")],2); ?></td>
                        <td align="center" width="80"><? echo $row[csf("subcon_roll")];; ?></td>
                        <td><p><? echo $row[csf("rec_cone")]; ?></p></td>
                    </tr>
                    <? 
					$tot_qty+=$row[csf("quantity")];
					} ?>
                    <tr class="tbl_bottom">
                    	<td colspan="8" align="right">Total: </td>
                        <td align="right"><p><? echo number_format($tot_qty,2); ?></p></td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                </table>
            </div> 
	</fieldset>
 </div> 
<?
exit();
}

if($action=="product_qty_pop_up")
{
	echo load_html_head_contents("Production Details", "../../../", 1, 1,$unicode,'','');
	//echo $order_id;//die;
	$expData=explode('_',$order_id);
	$order_id=$expData[0];
	$process_id=$expData[1];
?>
        <fieldset style="width:820px">
            <div style="width:100%;" align="center">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                    	<tr>
                            <th width="30">SL</th>
                            <th width="60">System ID</th>
                            <th width="70">Prod. Date</th>
                            <th width="100">Party</th>
                            <th width="80">Order No</th>
                            <th width="150">Process</th>
                            <th width="150">Description</th>
                            <th width="">Prod. Qty</th>
                    	</tr>
                    </thead>
                </table>
            </div>  
            <div style="width:100%; max-height:330px; overflow-y:scroll" align="left">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1" >
                    <?
					$party_arr=return_library_array( "select id, buyer_name from  lib_buyer",'id','buyer_name');
					$kniting_item_arr=return_library_array( "select id, const_comp from lib_subcon_charge",'id','const_comp');
					$po_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
					$po_party_arr=return_library_array( "select b.id, a.party_id from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst",'id','party_id');
                    $i=0;
					if ($process_id==1)
					{
						 $sql = "select '' as sys_id, production_date, order_id, gmts_item_id as item_id, '' as process, sum(production_qnty) as production_qnty from subcon_gmts_prod_dtls where order_id='$order_id' and production_type=1 group by production_date, order_id, gmts_item_id";
					}
					else if ($process_id==5)
					{
						$sql = "select '' as sys_id, production_date, order_id, gmts_item_id as item_id, '' as process, sum(production_qnty) as production_qnty from subcon_gmts_prod_dtls where order_id='$order_id' and production_type=2 group by production_date, order_id, gmts_item_id";

					}
					else if ($process_id==2)
					{
						$sql="select a.prefix_no_num as sys_id, a.product_no, a.party_id, a.product_date as production_date, b.order_id, b.process, b.cons_comp_id as item_id, sum(b.no_of_roll) as roll_qty, sum(b.product_qnty) as production_qnty from subcon_production_mst a, subcon_production_dtls b where b.order_id='$order_id' and b.product_type=2 and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.prefix_no_num, a.product_no, a.party_id, a.product_date, b.order_id, b.process, b.cons_comp_id";
					}
					else if($process_id==3)
					{
						if($db_type==0)
						{
							$sql="select '' as sys_id, a.process_end_date as production_date, c.po_id as order_id, c.item_description as item_id, a.process_id as process, sum(c.batch_qnty) as production_qnty from pro_fab_subprocess a, pro_batch_create_mst b,  pro_batch_create_dtls c where a.batch_id=b.id and b.id=c.mst_id and a.entry_form=38 and a.result=1 and a.load_unload_id=2 and b.entry_form=36 and c.po_id='$order_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.process_end_date, a.process_id, c.po_id, c.item_description ";
						}
						elseif($db_type==2)
						{
							$sql="select '' as sys_id, a.process_end_date as production_date, c.po_id as order_id, c.item_description as item_id, a.process_id as process, sum(c.batch_qnty) as production_qnty from pro_fab_subprocess a, pro_batch_create_mst b,  pro_batch_create_dtls c where a.batch_id=b.id and b.id=c.mst_id and a.entry_form=38 and a.result=1 and a.load_unload_id=2 and b.entry_form=36 and c.po_id='$order_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by a.process_end_date, a.process_id, c.po_id, c.item_description ";
						}
					}
					else if($process_id==4)
					{
						$sql = "select a.prefix_no_num as sys_id, a.product_no, a.product_date as production_date, a.party_id, c.order_id, b.process as process, b.fabric_description as item_id, sum(c.quantity) as production_qnty from subcon_production_mst a, subcon_production_dtls b, subcon_production_qnty c where a.id=b.mst_id and b.id=c.dtls_id and c.order_id in ($order_id) and b.product_type='$process_id' group by a.prefix_no_num, a.product_no, a.party_id, a.product_date, c.order_id, b.process, b.fabric_description";
					}
                   // echo $sql;
					$production_sql= sql_select($sql);
					foreach( $production_sql as $row )
                    {
                        $i++;
                        if ($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
						
						if ($process_id==1 || $process_id==5)
						{
							$item_name=$garments_item[$row[csf('item_id')]];
							$party_name=$party_arr[$po_party_arr[$row[csf("order_id")]]];
						}
						else if ($process_id==2)
						{
							$party_name=$party_arr[$row[csf("party_id")]];
							$process_name=$conversion_cost_head_array[$row[csf("process")]];
							$item_name=$kniting_item_arr[$row[csf('item_id')]];
						}
						else if ($process_id==3)
						{
							$party_name=$party_arr[$po_party_arr[$row[csf("order_id")]]];
							$process_name="";
							$process_id=explode(',',$row[csf('process')]);
							foreach($process_id as $val)
							{
								if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=','.$conversion_cost_head_array[$val];
							}
							$item_name=$row[csf('item_id')];
						}
						else if ($process_id==4)
						{
							$party_name=$party_arr[$row[csf("party_id")]];
							$process_name="";
							$process_id=explode(',',$row[csf('process')]);
							foreach($process_id as $val)
							{
								if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=','.$conversion_cost_head_array[$val];
							}
							$item_name=$row[csf('item_id')];
						}
						else
						{
							$item_name=$row[csf('item_id')];
						}
                   ?>
                    <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="30"><? echo $i; ?></td>
                        <td width="60" align="center"><? echo $row[csf("sys_id")];?> </td>
                        <td width="70"><? echo change_date_format($row[csf("production_date")]);?> </td> 
                        <td width="100"><p><? echo $party_name; ?></p></td>
                        <td align="center" width="80"><? echo $po_arr[$row[csf("order_id")]]; ?></td>
                        <td align="center" width="150"><p><? echo $process_name; ?></p></td>
                        <td align="center" width="150"><p><? echo $item_name; ?></p></td>
                        <td align="right" width=""><? echo number_format($row[csf("production_qnty")],2); ?></td>
                    </tr>
                    <? 
					$tot_qty+=$row[csf("production_qnty")];
					} ?>
                    <tr class="tbl_bottom">
                    	<td colspan="7" align="right">Total: </td>
                        <td align="right"><p><? echo number_format($tot_qty,2); ?></p></td>
                    </tr>
                </table>
            </div> 
	</fieldset>
 </div> 
<?
exit();
}

if($action=="delivery_qty_pop_up")
{
	echo load_html_head_contents("Delivery Details", "../../../", 1, 1,$unicode,'','');
	//echo $order_id;//die;
	$expData=explode('_',$order_id);
?>
        <fieldset style="width:820px">
            <div style="width:100%;" align="center">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                    <thead>
                    	<tr>
                            <th width="30">SL</th>
                            <th width="100">Delivery ID</th>
                            <th width="70">Delivery Date</th>
                            <th width="100">Party</th>
                            <th width="80">Order No</th>
                            <th width="80">Category</th>
                            <th width="150">Description</th>
                            <th width="">Delivery Qty</th>
                    	</tr>
                    </thead>
                </table>
            </div>  
            <div style="width:100%; max-height:230px; overflow-y:scroll" align="left">
                <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1" >
                    <?
					$party_arr=return_library_array( "select id, buyer_name from  lib_buyer",'id','buyer_name');
					$po_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
					$kniting_item_arr=return_library_array( "select id, const_comp from lib_subcon_charge",'id','const_comp');
					$dye_fin_item_arr=return_library_array( "select id, item_description from pro_batch_create_dtls",'id','item_description');
                    $i=0;
                    $sql= "select a.delivery_prefix_num, a.delivery_date, a.party_id, b.order_id, b.process_id, b.item_id, sum(b.delivery_qty) as quantity from  subcon_delivery_mst a, subcon_delivery_dtls b where a.id=b.mst_id and b.order_id in ($expData[0]) and b.process_id='$expData[1]' and a.status_active=1 and a.is_deleted=0  group by a.delivery_prefix_num, a.delivery_date, a.party_id, b.order_id, b.process_id, b.item_id order by a.delivery_prefix_num, a.delivery_date";
                    //echo $sql;
					$production_sql= sql_select($sql);
					foreach( $production_sql as $row )
                    {
                        $i++;
                        if ($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
						if ($row[csf("process_id")]==1 || $row[csf("process_id")]==5)
						{
							$item_name=$garments_item[$row[csf('item_id')]];
						}
						else if ($row[csf("process_id")]==2)
						{
							$item_name=$kniting_item_arr[$row[csf('item_id')]];
						}
						else if ($row[csf("process_id")]==3 || $row[csf("process_id")]==4)
						{
							$item_name=$dye_fin_item_arr[$row[csf('item_id')]];
						}
                   ?>
                    <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                        <td width="30"><? echo $i; ?></td>
                        <td width="100"><? echo $row[csf("delivery_prefix_num")];?> </td>
                        <td width="70"><? echo change_date_format($row[csf("delivery_date")]);?> </td> 
                        <td width="100"><p><? echo $party_arr[$row[csf("party_id")]]; ?></p></td>
                        <td align="center" width="80"><? echo $po_arr[$row[csf("order_id")]]; ?></td>
                        <td align="center" width="80"><? echo $production_process[$row[csf("process_id")]]; ?></td>
                        <td align="center" width="150"><p><? echo $item_name; ?></p></td>
                        <td align="right" width=""><? echo number_format($row[csf("quantity")],2); ?></td>
                    </tr>
                    <? 
					$tot_qty+=$row[csf("quantity")];
					} ?>
                    <tr class="tbl_bottom">
                    	<td colspan="7" align="right">Total: </td>
                        <td align="right"><p><? echo number_format($tot_qty,2); ?></p></td>
                    </tr>
                </table>
            </div> 
	</fieldset>
 </div> 
<?
exit();
}

if($action=="bill_qty_pop_up")
{
	echo load_html_head_contents("Delivery Details", "../../../", 1, 1,$unicode,'','');
	echo $order_id;//die;
	$expData=explode('_',$order_id);
?>
    <fieldset style="width:820px">
        <div style="width:100%;" align="center">
            <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                <thead>
                    <tr>
                        <th width="30">SL</th>
                        <th width="100">Delivery ID</th>
                        <th width="70">Delivery Date</th>
                        <th width="100">Party</th>
                        <th width="80">Order No</th>
                        <th width="80">Category</th>
                        <th width="150">Description</th>
                        <th width="80">Delivery Qty</th>
                        <th >Amount</th>
                    </tr>
                </thead>
            </table>
        </div>  
        <div style="width:100%; max-height:230px; overflow-y:scroll" align="left">
            <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1" >
                <?
                $party_arr=return_library_array( "select id, buyer_name from  lib_buyer",'id','buyer_name');
                $po_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
                $i=0;
                $sql= "select a.bill_no, a.bill_date, a.party_id, b.order_id, b.process_id, b.item_id, sum(b.delivery_qty) as quantity, sum(b.amount) as amount from  subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.id=b.mst_id and b.order_id in ($expData[0]) and b.process_id='$expData[1]' and a.status_active=1 and a.is_deleted=0  group by a.bill_no, a.bill_date, a.party_id, b.order_id, b.process_id, b.item_id order by a.bill_no, a.bill_date";
                //echo $sql;
                $production_sql= sql_select($sql);
                foreach( $production_sql as $row )
                {
                    $i++;
                    if ($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
               ?>
                <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
                    <td width="30"><? echo $i; ?></td>
                    <td width="100"><? echo $row[csf("bill_no")];?> </td>
                    <td width="70"><? echo change_date_format($row[csf("bill_date")]);?> </td> 
                    <td width="100"><p><? echo $party_arr[$row[csf("party_id")]]; ?></p></td>
                    <td align="center" width="80"><? echo $po_arr[$row[csf("order_id")]]; ?></td>
                    <td align="center" width="80"><? echo $production_process[$row[csf("process_id")]]; ?></td>
                    <td align="center" width="150"><? echo $row[csf("item_id")]; ?></td>
                    <td align="right" width="80"><? echo number_format($row[csf("quantity")],2); ?></td>
                    <td align="right" width=""><? echo number_format($row[csf("amount")],2); ?></td>
                </tr>
                <? 
                $tot_qty+=$row[csf("quantity")];
                $tot_amount+=$row[csf("amount")];
                } ?>
                <tr class="tbl_bottom">
                    <td colspan="7" align="right">Total: </td>
                    <td align="right"><p><? echo number_format($tot_qty,2); ?></p></td>
                    <td align="right"><p><? echo number_format($tot_amount,2); ?></p></td>
                </tr>
            </table>
        </div> 
	</fieldset>
 </div> 
<?
exit();
}

if($action=="image_view_popup")
{
	extract($_REQUEST);
	echo load_html_head_contents("Work Progress Info","../../../", 1, 1, $unicode);
	//echo "select master_tble_id,image_location from common_photo_library where form_name='sub_contract_order_entry' and file_type=1 and master_tble_id='$id'";
	$imge_data=sql_select("select master_tble_id,image_location from common_photo_library where form_name='sub_contract_order_entry' and file_type=1 and master_tble_id='$id'");
	?>
	<table>
        <tr>
			<?
            foreach($imge_data as $row)
            {
				?>
                    <td><img src='../../../<? echo $row[csf('image_location')]; ?>' height='100%' width='100%' /></td>
				<?
            }
            ?>
        </tr>
	</table>
	<?
	exit();
}

if($action=="batch_qty_pop_up")
{
	echo load_html_head_contents("Batch Details", "../../../", 1, 1,$unicode,'','');
	//echo $order_id;//die;
	$expData=explode('_',$order_id);
	$order_id=$expData[0];
	//$process_id=$expData[1];
	?>
    <fieldset style="width:820px">
        <div style="width:100%;" align="center">
            <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1">
                <thead>
                    <tr>
                        <th width="30">SL</th>
                        <th width="80">Batch No</th>
                        <th width="30">Ext.</th>
                        <th width="65">Batch Date</th>
                        <th width="80">Color</th>
                        <th width="80">Order</th>
                        <th width="150">Description</th>
                        <th width="">Batch Qty</th>
                    </tr>
                </thead>
            </table>
        </div>  
        <div style="width:100%; max-height:330px; overflow-y:scroll" align="left">
            <table cellpadding="0" width="100%" class="rpt_table" rules="all" border="1" >    
			<?
	$sql_batch="Select a.batch_no, a.extention_no, a.batch_date, a.color_id, b.po_id, b.item_description, b.rec_challan, sum(b.batch_qnty) as batch_qnty from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.entry_form=36 and b. b.po_id='$order_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.batch_no, a.extention_no, a.batch_date, a.color_id, b.po_id, b.item_description, b.rec_challan";
	$sql_batch_result=sql_select($sql_batch); $i=0;
	foreach ($sql_batch_result as $row)
	{
		$i++;
		if ($i%2==0)  $bgcolor="#EFEFEF"; else $bgcolor="#FFFFFF";
		?>
        <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
            <td width="30"><? echo $i; ?></td>
            <td width="60" align="center"><? echo $row[csf("sys_id")];?> </td>
            <td width="70"><? echo change_date_format($row[csf("production_date")]);?> </td> 
            <td width="100"><p><? echo $party_name; ?></p></td>
            <td align="center" width="80"><? echo $po_arr[$row[csf("order_id")]]; ?></td>
            <td align="center" width="150"><p><? echo $process_name; ?></p></td>
            <td align="center" width="150"><p><? echo $item_name; ?></p></td>
            <td align="right" width=""><? echo number_format($row[csf("production_qnty")],2); ?></td>
        </tr>
        <?
	}
}
?>