-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_transaction_history`
--

DROP TABLE IF EXISTS `inv_transaction_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_transaction_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `approve_no` int(11) NOT NULL DEFAULT '0',
  `transaction_id` int(11) NOT NULL DEFAULT '0',
  `mst_id` int(11) NOT NULL DEFAULT '0' COMMENT 'receive,issue,return,transfer table mst ID',
  `requisition_no` int(11) NOT NULL DEFAULT '0',
  `receive_basis` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=>"PI Based",2=>"WO Based",3=>"In-Bound Subcontract",4=>"Independent",5=>"Batch Based"',
  `pi_wo_batch_no` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `prod_id` int(11) NOT NULL DEFAULT '0',
  `product_code` varchar(50) NOT NULL,
  `item_category` tinyint(3) NOT NULL DEFAULT '0',
  `transaction_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return",5=>"Item Transfer Receive",6=>"Item Transfer Issue"',
  `transaction_date` date NOT NULL DEFAULT '0000-00-00' COMMENT 'receive,issue,return,transfer date',
  `store_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `order_uom` tinyint(3) NOT NULL DEFAULT '0',
  `order_qnty` double NOT NULL DEFAULT '0',
  `order_rate` double NOT NULL DEFAULT '0',
  `order_ile` double NOT NULL DEFAULT '0',
  `order_ile_cost` double NOT NULL DEFAULT '0',
  `order_amount` double NOT NULL DEFAULT '0',
  `cons_uom` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'receive,issue,return,transfer uom',
  `cons_quantity` double NOT NULL DEFAULT '0' COMMENT 'receive,issue,return,transfer quantity',
  `return_qnty` double NOT NULL DEFAULT '0',
  `cons_reject_qnty` double NOT NULL DEFAULT '0',
  `cons_rate` double NOT NULL DEFAULT '0' COMMENT 'receive,issue,return,transfer rate,   (cons rate+cons ile cost=cons_rate)',
  `cons_ile` double NOT NULL DEFAULT '0',
  `cons_ile_cost` double NOT NULL DEFAULT '0',
  `cons_amount` double NOT NULL DEFAULT '0' COMMENT 'receive,issue,return,transfer value',
  `balance_qnty` double NOT NULL DEFAULT '0',
  `balance_amount` double NOT NULL DEFAULT '0',
  `floor_id` int(11) NOT NULL DEFAULT '0',
  `machine_id` int(11) NOT NULL DEFAULT '0',
  `machine_category` tinyint(2) NOT NULL DEFAULT '0',
  `no_of_bags` int(11) NOT NULL DEFAULT '0',
  `cone_per_bag` int(11) NOT NULL DEFAULT '0',
  `weight_per_bag` double NOT NULL DEFAULT '0',
  `weight_per_cone` double NOT NULL DEFAULT '0',
  `room` int(11) NOT NULL DEFAULT '0',
  `rack` varchar(20) NOT NULL,
  `self` int(11) NOT NULL DEFAULT '0',
  `bin_box` int(11) NOT NULL DEFAULT '0',
  `expire_date` date NOT NULL DEFAULT '0000-00-00',
  `dyeing_sub_process` int(11) NOT NULL DEFAULT '0',
  `issue_challan_no` varchar(50) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  `batch_lot` varchar(30) NOT NULL,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `section_id` int(11) NOT NULL DEFAULT '0',
  `job_no` varchar(25) NOT NULL COMMENT 'Yarn Dyeing Issue Purpose',
  `dyeing_color_id` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:active 0:inactive',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_transaction_history`
--

LOCK TABLES `inv_transaction_history` WRITE;
/*!40000 ALTER TABLE `inv_transaction_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `inv_transaction_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
