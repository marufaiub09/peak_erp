<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------
$user_name_library=return_library_array( "select id, user_name from user_passwd", "id", "user_name"  );


if ($action=="report_generate_login_history")  // Item Description wise Search
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	$user_name=str_replace("'","",$cbo_user_name);
	$cbo_search=str_replace("'","",$cbo_search);
	$search_value=str_replace("'","",$search_value);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	if($user_name==0)
		$user_cond="";
	else
		$user_cond=" and user_id=$user_name";
		if($db_type==2)
		{
			if( $date_from==0 && $date_to==0 ) $log_date=""; else $log_date= "  login_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
		}
		else
		{
			if( $date_from==0 && $date_to==0 ) $log_date=""; else $log_date= "  login_date between '".$date_from."' and '".$date_to."'";
	
		}
	
	if($cbo_search==0)
		$search_cond="";
	else if($cbo_search==1)
		$search_cond=" and lan_ip like '%$search_value%'";
	else if($cbo_search==2)
		$search_cond=" and lan_mac like '%$search_value%'";
	else if($cbo_search==3)
		$search_cond=" and wan_ip like '%$search_value%'";
	 
	
	//echo $user_name;die;
	ob_start();	
	
	?>
      <div style="width:880px;">
    
   	  <table width="880" cellspacing="0" cellpadding="0" border="0" rules="all" >
            <tr class="form_caption">
                <td colspan="8" align="center" style="border:none;font-size:16px; font-weight:bold"> <?php echo $report_title; ?></td>
            </tr>
           
     </table>
     <table cellspacing="0" cellpadding="0" border="1" rules="all" width="880" class="rpt_table" >
        <thead>
            <th width="30">SL</th>
            <th width="80">Session</th>
            <th width="80">User Name</th>
            <th width="80">Log Date</th>
            <th width="80">Login Time</th>
            <th width="80">Logout Date</th>
            <th width="80">Logout Time</th>
            <th width="80">Login Status</th>
           
           
         
        </thead>
     </table>
     <div style="width:900px; overflow-y:scroll; max-height:350px;font-size:12px; overflow-x:hidden;" id="scroll_body">
     <table cellspacing="0" cellpadding="0" border="1" rules="all" width="880" class="rpt_table"  id="table_body" >
	<?php 
		
		$act_sql="select id,session_id,user_id from activities_history ";
		$actArray=sql_select( $act_sql );
		$activity_arr=array();
		foreach($actArray as $row_data)
		{
		$activity_arr[$row_data[csf('user_id')]]['user']=$row_data[csf('session_id')];	
		}
		$user_data="select user_id,lan_ip,lan_mac,wan_ip,login_time,login_date,logout_time,logout_date,login_status from login_history where $log_date $user_cond $search_cond";
		$nameArray=sql_select( $user_data );
		$i=1; $log_status_arr=array( 0=>"success", 1=>"pc ip fail", 2=>"password" , 3=>"user", 4=>"proxy");
		foreach($nameArray as $row)
		{
	if ($i%2==0)  
                $bgcolor="#E9F3FF";
            else
                $bgcolor="#FFFFFF";
				
     ?>
        <tr bgcolor="<?php echo $bgcolor; ?>"  onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
           <td width="30"><?php echo $i;?></td>
           <td width="80" align="center"><p><?php echo $activity_arr[$row[csf('user_id')]]['user']; ?></p></td>
           <td width="80"><p><?php echo  $user_name_library[$row[csf('user_id')]];?></p></td>
           <td width="80"><p><?php echo ($row[csf('login_date')] == '0000-00-00' || $row[csf('login_date')] == '' ? '' : change_date_format($row[csf('login_date')]));?></p></td>
           <td width="80"><p><?php echo $row[csf('login_time')];?></p></td>
           <td width="80"><p><?php echo ($row[csf('logout_date')] == '0000-00-00' || $row[csf('logout_date')] == '' ? '' : change_date_format($row[csf('logout_date')]));?></p></td>
           <td width="80"><p><?php echo $row[csf('logout_time')];?></p></td>
           <td width="80" align="center"><p><?php echo  $log_status_arr[$row[csf('login_status')]];?></p></td>
          
        </tr>
        <?php
		$i++;
		}
		?>
        
        </table>
    </div>
    
<?php

	echo "$total_data**$filename";

	exit();
}

?>