<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=$_SESSION['page_permission'];

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$company_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
$yarn_count_details=return_library_array( "select id,yarn_count from lib_yarn_count", "id", "yarn_count"  );
$supllier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 0, "-- All Buyer --", $selected, "" ,0); 
	exit();
}

if ($action=="eval_multi_select")
{
 	echo "set_multiselect('cbo_buyer_name','0','0','','0');\n";
	exit();
}


if($action=="order_no_search_popup")
{
	echo load_html_head_contents("Order No Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_order_id').val( id );
			$('#hide_order_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:780px;">
            <table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Order No</th>
                    <th>Shipment Date</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_order_no" id="hide_order_no" value="" />
                    <input type="hidden" name="hide_order_id" id="hide_order_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",0,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Order No",2=>"Style Ref",3=>"Job No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 110, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
                            <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
                        </td>	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value, 'create_order_no_search_list_view', 'search_div', 'buyer_order_wise_knitting_status_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    	</td>
                    </tr>
                    <tr>
                        <td colspan="5" height="20" valign="middle"><?php echo load_month_buttons(1); ?></td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_order_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==1) 
		$search_field="b.po_number"; 
	else if($search_by==2) 
		$search_field="a.style_ref_no"; 	
	else 
		$search_field="a.job_no";
		
	$start_date =trim($data[4]);
	$end_date =trim($data[5]);	
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,"yyyy-mm-dd", "-")."' and '".change_date_format($end_date,"yyyy-mm-dd", "-")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,'','',1)."' and '".change_date_format($end_date,'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$arr=array(0=>$company_arr,1=>$buyer_arr);
		
	$sql= "select b.id, $year_field a.job_no, a.job_no_prefix_num, a.company_name, a.buyer_name, a.style_ref_no, b.po_number, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $date_cond order by b.id, b.pub_shipment_date";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No, Po No, Shipment Date", "80,130,50,60,130,130","760","220",0, $sql , "js_set_value", "id,po_number", "", 1, "company_name,buyer_name,0,0,0,0,0", $arr , "company_name,buyer_name,year,job_no_prefix_num,style_ref_no,po_number,pub_shipment_date", "",'','0,0,0,0,0,0,3','',1) ;
	
   exit(); 
}


if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$company_name= str_replace("'","",$cbo_company_name);
	$type = str_replace("'","",$cbo_type);
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name in (".str_replace("'","",$cbo_buyer_name).")";
	}
	
	$txt_job_no=str_replace("'","",$txt_job_no);
	if(trim($txt_job_no)!="") $job_no="%".trim($txt_job_no); else $job_no="%%";
	
	$cbo_year=str_replace("'","",$cbo_year);
	if(trim($cbo_year)!=0) 
	{
		if($db_type==0) 
		{
			$year_cond=" and YEAR(a.insert_date)=$cbo_year";
			$year_field=", YEAR(a.insert_date) as year ";
		}
		else if($db_type==2) 
		{
			$year_cond=" and to_char(a.insert_date,'YYYY')=$cbo_year";
			$year_field=", to_char(a.insert_date,'YYYY') as year ";
		}
		else 
		{
			$year_cond=""; $year_field="";
		}
	}
	else $year_cond="";
	
	if(str_replace("'","",trim($txt_order_no))=="")
	{
		$po_cond="";
	}
	else
	{
		if(str_replace("'","",$hide_order_id)!="")
		{
			$po_id=str_replace("'","",$hide_order_id);
			$po_cond="and b.id in(".$po_id.")";
		}
		else
		{
			$po_number=trim(str_replace("'","",$txt_order_no))."%";
			$po_cond="and b.po_number like '$po_number'";
		}
	}
	
	if($type==1)
	{
		$plan_data_array=array();
		$sql_plan=sql_select("select a.booking_no, a.po_id, a.dia, a.yarn_desc as pre_cost_id, a.fabric_desc, a.gsm_weight, a.program_qnty, b.id as prog_id, b.knitting_source, b.knitting_party, b.color_id, b.machine_dia, b.machine_gg, b.fabric_dia, b.stitch_length, b.start_date, b.end_date, b.status from ppl_planning_entry_plan_dtls a, ppl_planning_info_entry_dtls b where a.dtls_id=b.id and a.company_id=$company_name and b.program_qnty>0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 order by b.knitting_source DESC");
		foreach($sql_plan as $planRow)
		{
			$plan_data_array[$planRow[csf('po_id')]].=$planRow[csf('booking_no')]."__".$planRow[csf('dia')]."__".$planRow[csf('pre_cost_id')]."__".$planRow[csf('fabric_desc')]."__".$planRow[csf('gsm_weight')]."__".$planRow[csf('program_qnty')]."__".$planRow[csf('prog_id')]."__".$planRow[csf('knitting_source')]."__".$planRow[csf('knitting_party')]."__".$planRow[csf('color_id')]."__".$planRow[csf('machine_dia')]."__".$planRow[csf('machine_gg')]."__".$planRow[csf('fabric_dia')]."__".$planRow[csf('stitch_length')]."__".$planRow[csf('start_date')]."__".$planRow[csf('end_date')]."__".$planRow[csf('status')]."**";
		}
	
		$pre_cost_array=array();
		if($db_type==0)
		{
			$costing_sql=sql_select("select id,body_part_id,gsm_weight,concat_ws(', ',construction,composition) as fab_desc,lib_yarn_count_deter_id from wo_pre_cost_fabric_cost_dtls");
		}
		else
		{
			$costing_sql=sql_select("select id,body_part_id,gsm_weight, construction || ', ' || composition as fab_desc,lib_yarn_count_deter_id from wo_pre_cost_fabric_cost_dtls");	
		}
		foreach($costing_sql as $row)
		{
			$costing_per_id_library[$row[csf('id')]]['body_part']=$row[csf('body_part_id')];
			$costing_per_id_library[$row[csf('id')]]['gsm']=$row[csf('gsm_weight')]; 
			$costing_per_id_library[$row[csf('id')]]['desc']=$row[csf('fab_desc')]; 
			$costing_per_id_library[$row[csf('id')]]['determination_id']=$row[csf('lib_yarn_count_deter_id')]; 
		}
		
		$reqsDataArr=array();
		if($db_type==0)
		{
			$reqsDataArr=return_library_array( "select knit_id, group_concat(distinct(prod_id)) as prod_id from ppl_yarn_requisition_entry where status_active=1 and is_deleted=0 group by knit_id", "knit_id", "prod_id");
		}
		else
		{
			$reqsDataArr=return_library_array( "select knit_id, LISTAGG(prod_id, ',') WITHIN GROUP (ORDER BY prod_id) as prod_id from ppl_yarn_requisition_entry where status_active=1 and is_deleted=0 group by knit_id", "knit_id", "prod_id");	
		}
		
		$yarn_desc_array=array();
		$sql="select id, lot, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type from product_details_master where item_category_id=1";
		$result = sql_select($sql);
		foreach($result as $row)
		{
			$compostion='';
			if($row[csf('yarn_comp_percent2nd')]!=0)
			{
				$compostion=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
			}
			else
			{
				$compostion=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
			}
	
			$yarn_desc=$yarn_count_details[$row[csf('yarn_count_id')]]." ".$compostion." ".$yarn_type[$row[csf('yarn_type')]];//$row[csf('lot')]." ".
			$yarn_desc_array[$row[csf('id')]]=$yarn_desc;
		}

		$bookingDataArr=array();
		$sql_wo=sql_select("select a.booking_no, b.pre_cost_fabric_cost_dtls_id, b.po_break_down_id, b.dia_width, sum(b.grey_fab_qnty) as qnty from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$company_name and a.item_category=2 and a.fabric_source=1 and b.grey_fab_qnty>0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.booking_no, b.po_break_down_id, b.pre_cost_fabric_cost_dtls_id, b.dia_width order by b.dia_width");
		foreach($sql_wo as $woRow)
		{
			$bookingDataArr[$woRow[csf('po_break_down_id')]].=$woRow[csf('booking_no')]."**".$woRow[csf('pre_cost_fabric_cost_dtls_id')]."**".$woRow[csf('dia_width')]."**".$woRow[csf('qnty')]."__";
		}
		
		$prodDataArr=array();
		$sql_prod=sql_select("select c.po_breakdown_id, a.booking_id, sum(c.quantity) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and c.entry_form=2 and a.item_category=13 and a.entry_form=2 and a.receive_basis=2 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.po_breakdown_id, a.booking_id");
		foreach($sql_prod as $prodRow)
		{
			$prodDataArr[$prodRow[csf('po_breakdown_id')]][$prodRow[csf('booking_id')]]=$prodRow[csf('knitting_qnty')];
		}
	
		$sql="select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, b.id as po_id, b.po_number $year_field from wo_po_details_master a, wo_po_break_down b, wo_booking_dtls d where a.job_no=b.job_no_mst and b.id=d.po_break_down_id and a.company_name=$company_name and a.job_no like '$job_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and d.status_active=1 and d.is_deleted=0 and d.grey_fab_qnty>0 $buyer_id_cond $po_cond $year_cond group by b.id, a.job_no, a.job_no_prefix_num, a.company_name, a.buyer_name, b.po_number, a.insert_date order by a.buyer_name, a.job_no, b.id";
		//echo $sql;//die;
		ob_start();
	?>
		<fieldset style="width:1685px;">
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1658" class="rpt_table" >
				<thead>
					<th width="40">SL</th>
					<th width="60">Job No</th>
					<th width="50">Year</th>
					<th width="110">Order No</th>
					<th width="120">Knitting Company</th>
					<th width="70">Program No</th>
					<th width="80">Start Date</th>
					<th width="80">End Date</th>
					<th width="180">Fabric Description</th>
					<th width="200">Yarn Description</th>
					<th width="80">Color</th>
					<th width="70">Stich Length</th>
					<th width="60">M/C Dia</th>
					<th width="60">F. Dia</th>
					<th width="70">GSM</th>
					<th width="100">Req. Qty</th>
					<th width="100">Prod. Qty</th>
					<th>Balance</th>
				</thead>
			</table>
			<div style="width:1680px; overflow-y:scroll; max-height:450px;" id="scroll_body">
				<table cellspacing="0" cellpadding="0" border="1" rules="all" width="1658" class="rpt_table" id="tbl_list_search">
					<?php 
						$i=1; $buyer_array=array(); $po_array=array();
						$nameArray=sql_select( $sql );
						foreach ($nameArray as $row)
						{
							if(!in_array($row[csf('buyer_name')], $buyer_array))
							{
								if($i!=1)
								{
								?>
									<tr bgcolor="#CCCCCC">
										<td colspan="15" align="right"><b>Order Total</b></td>
										<td align="right"><b><?php echo number_format($order_req_qnty,2,'.',''); ?></b></td>
										<td align="right"><b><?php echo number_format($order_prod_qnty,2,'.',''); ?></b></td>
										<td align="right"><b><?php echo number_format($order_balance,2,'.',''); ?></b></td>
									</tr>
									<tr bgcolor="#CCCCCC">
										<td colspan="15" align="right"><b>Buyer Total</b></td>
										<td align="right"><b><?php echo number_format($buyer_req_qnty,2,'.',''); ?></b></td>
										<td align="right"><b><?php echo number_format($buyer_prod_qnty,2,'.',''); ?></b></td>
										<td align="right"><b><?php echo number_format($buyer_balance,2,'.',''); ?></b></td>
									</tr>
								<?php
									$buyer_req_qnty = 0;
									$buyer_prod_qnty = 0;
									$buyer_balance = 0;
									
									$order_req_qnty = 0;
									$order_prod_qnty = 0;
									$order_balance = 0;
								}
							?>
								<tr bgcolor="#EFEFEF">
									<td colspan="18">
										<b>Buyer Name:- <?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></b>
									</td>
								</tr>
							<?php
								$buyer_array[]=$row[csf('buyer_name')];
							}
							else
							{
								if(!in_array($row[csf('po_id')], $po_array))
								{
									if($i!=1)
									{
									?>
										<tr bgcolor="#CCCCCC">
											<td colspan="15" align="right"><b>Order Total</b></td>
											<td align="right"><b><?php echo number_format($order_req_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($order_prod_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($order_balance,2,'.',''); ?></b></td>
										</tr>
									<?php
										$order_req_qnty = 0;
										$order_prod_qnty = 0;
										$order_balance = 0;
									}
									$po_array[]=$row[csf('po_id')];
								}
									
							}
							
							$planData=array_filter(explode("**",substr($plan_data_array[$row[csf('po_id')]],0,-2)));
							$plannedArray=array(); $z=1;
							if(count($planData)>0)
							{
								foreach($planData as $planRow)
								{
									$planRow=explode("__",$planRow);
									$booking_no=$planRow[0];
									$dia=$planRow[1];
									$pre_cost_id=$planRow[2];
									$fabric_desc=$planRow[3];
									$gsm_weight=$planRow[4];
									$program_qnty=$planRow[5];
									$prog_id=$planRow[6];
									$knitting_source=$planRow[7];
									$knitting_party=$planRow[8];
									$color_id=$planRow[9];
									$machine_dia=$planRow[10];
									$machine_gg=$planRow[11];
									$fabric_dia=$planRow[12];
									$stitch_length=$planRow[13];
									$start_date=$planRow[14];
									$end_date=$planRow[15];
									$status=$planRow[16];
									
									$plannedArray[$booking_no][$row[csf('po_id')]][$pre_cost_id][$dia]=$program_qnty;
									
									if($status!=4)
									{
										if($z==1) 
										{
											$display_font_color="";
											$font_end="";
										}
										else 
										{
											$display_font_color="<font style='display:none' color='$bgcolor'>";
											$font_end="</font>";
										}
										
										if($knitting_source==1)
											$knit_party=$company_arr[$knitting_party]; 
										else if($knitting_source==3)
											$knit_party=$supllier_arr[$knitting_party];
										else
											$knit_party="Without Source";
									
										$prog_no=$prog_id;
										$prod_qnty=$prodDataArr[$row[csf('po_id')]][$prog_id];
										
										$balance_qnty=$program_qnty-$prod_qnty;
			
										$yarn_desc='';
										$prod_id=array_unique(explode(",",$reqsDataArr[$prog_id]));
										foreach($prod_id as $value)
										{
											if($yarn_desc=='') $yarn_desc=$yarn_desc_array[$value]; else $yarn_desc.=",<br>".$yarn_desc_array[$value];
										}
										
										$color_name='';
										$color_id=array_unique(explode(",",$color_id));
										foreach($color_id as $value)
										{
											if($color_name=='') $color_name=$color_library[$value]; else $color_name.=",".$color_library[$value];
										}
										
										if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
										
										?>
										<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
											<td width="40"><?php echo $display_font_color.$i.$font_end; ?>&nbsp;</td>
											<td width="60">&nbsp;&nbsp;<?php echo $display_font_color.$row[csf('job_no_prefix_num')].$font_end; ?></td>
											<td width="50" align="center"><?php echo $display_font_color.$row[csf('year')].$font_end; ?>&nbsp;</td>
											<td width="110"><p><?php echo $display_font_color.$row[csf('po_number')].$font_end; ?>&nbsp;</p></td>
											<td width="120"><p><?php echo $knit_party; ?></p></td>
											<td width="70"><p>&nbsp;&nbsp;<?php echo $prog_no; ?></p></td>
											<td width="80" align="center">&nbsp;<?php echo change_date_format($start_date); ?></td>
											<td width="80" align="center">&nbsp;<?php echo change_date_format($end_date); ?></td>
											<td width="180"><p><?php echo $fabric_desc; ?></p></td>
											<td width="200"><p><?php echo $yarn_desc; ?>&nbsp;</p></td>
											<td width="80"><p><?php echo $color_name; ?>&nbsp;</p></td>
											<td width="70"><p><?php echo $stitch_length; ?>&nbsp;</p></td>
											<td width="60"><p><?php echo $machine_dia."X".$machine_gg; ?>&nbsp;</p></td>
											<td width="60"><p><?php echo $fabric_dia; ?>&nbsp;</p></td>
											<td width="70"><p><?php echo $gsm_weight; ?>&nbsp;</p></td>
											<td align="right" width="100"><?php echo number_format($program_qnty,2,'.',''); ?></td>
											<td align="right" width="100"><?php echo number_format($prod_qnty,2,'.',''); ?></td>
											<td align="right"><?php echo number_format($balance_qnty,2,'.',''); ?></td>
										</tr>
										<?php
										
										$total_req_qnty+=$program_qnty;
										$total_prod_qnty+=$prod_qnty;
										$total_balance+=$balance_qnty;
										
										$buyer_req_qnty+=$program_qnty;
										$buyer_prod_qnty+=$prod_qnty;
										$buyer_balance+=$balance_qnty;
										
										$order_req_qnty+=$program_qnty;
										$order_prod_qnty+=$prod_qnty;
										$order_balance+=$balance_qnty;
			
										$i++;
										$z++;
									}
								}
							}
							
							$bookingData=array_filter(explode("__",substr($bookingDataArr[$row[csf('po_id')]],0,-2)));
							if(count($bookingData)>0)
							{
								foreach($bookingData as $woRow)
								{
									$woRow=explode("**",$woRow);
									$booking_no=$woRow[0];
									$pre_cost_id=$woRow[1];
									$dia_width=$woRow[2];
									$req_qnty=$woRow[3];
									$program_qnty=$plannedArray[$booking_no][$row[csf('po_id')]][$pre_cost_id][$dia_width];
									
									if($program_qnty=="")
									{
										if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
										
										if($z==1) 
										{
											$display_font_color="";
											$font_end="";
										}
										else 
										{
											$display_font_color="<font style='display:none' color='$bgcolor'>";
											$font_end="</font>";
										}
										
										$knit_party="Unplanned";
										$fabric_desc=$costing_per_id_library[$pre_cost_id]['desc'];
										$gsm_weight=$costing_per_id_library[$pre_cost_id]['gsm'];
										$prog_no="&nbsp;";
										$prod_qnty=0;
										$balance_qnty=$req_qnty;
										
										?>
										<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
											<td width="40"><?php echo $display_font_color.$i.$font_end; ?>&nbsp;</td>
											<td width="60">&nbsp;&nbsp;<?php echo $display_font_color.$row[csf('job_no_prefix_num')].$font_end; ?></td>
											<td width="50" align="center"><?php echo $display_font_color.$row[csf('year')].$font_end; ?>&nbsp;</td>
											<td width="110"><p><?php echo $display_font_color.$row[csf('po_number')].$font_end; ?>&nbsp;</p></td>
											<td width="120"><p><?php echo $knit_party; ?></p></td>
											<td width="70">&nbsp;</td>
											<td width="80" align="center">&nbsp;</td>
											<td width="80" align="center">&nbsp;</td>
											<td width="180"><p><?php echo $fabric_desc; ?></p></td>
											<td width="200">&nbsp;</td>
											<td width="80">&nbsp;</td>
											<td width="70">&nbsp;</td>
											<td width="60">&nbsp;</td>
											<td width="60"><p><?php echo $dia_width; ?>&nbsp;</p></td>
											<td width="70"><p><?php echo $gsm_weight; ?>&nbsp;</p></td>
											<td align="right" width="100"><?php echo number_format($req_qnty,2,'.',''); ?></td>
											<td align="right" width="100"><?php echo number_format($prod_qnty,2,'.',''); ?></td>
											<td align="right"><?php echo number_format($balance_qnty,2,'.',''); ?></td>
										</tr>
										<?php
										
										$total_req_qnty+=$req_qnty;
										$total_prod_qnty+=$prod_qnty;
										$total_balance+=$balance_qnty;
										
										$buyer_req_qnty+=$req_qnty;
										$buyer_prod_qnty+=$prod_qnty;
										$buyer_balance+=$balance_qnty;
										
										$order_req_qnty+=$req_qnty;
										$order_prod_qnty+=$prod_qnty;
										$order_balance+=$balance_qnty;
		
										$i++;
										$z++;
									}
								}
							}
						}
						
						if($i>1)
						{
						?>
							<tr bgcolor="#CCCCCC">
								<td colspan="15" align="right"><b>Order Total</b></td>
								<td align="right"><b><?php echo number_format($order_req_qnty,2,'.',''); ?></b></td>
								<td align="right"><b><?php echo number_format($order_prod_qnty,2,'.',''); ?></b></td>
								<td align="right"><b><?php echo number_format($order_balance,2,'.',''); ?></b></td>
							</tr>
							<tr bgcolor="#CCCCCC" id="tr_<?php echo $i; ?>">
								<td colspan="15" align="right"><b>Buyer Total</b></td>
								<td align="right"><b><?php echo number_format($buyer_req_qnty,2,'.',''); ?></b></td>
								<td align="right"><b><?php echo number_format($buyer_prod_qnty,2,'.',''); ?></b></td>
								<td align="right"><b><?php echo number_format($buyer_balance,2,'.',''); ?></b></td>
							</tr>
						<?php
						}
					?>
					<tfoot>
						<th colspan="15" align="right">Grand Total</th>
						<th align="right"><?php echo number_format($total_req_qnty,2,'.',''); ?></th>
						<th align="right"><?php echo number_format($total_prod_qnty,2,'.',''); ?></th>
						<th align="right"><?php echo number_format($total_balance,2,'.',''); ?></th>
					</tfoot>
				</table>
			</div>
		</fieldset>
<?php
	}
	else
	{
		$prodDataArr=array();
		$sql_prod=sql_select("select c.po_breakdown_id, sum(c.quantity) as knitting_qnty from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and c.entry_form=2 and a.item_category=13 and a.entry_form=2 and a.receive_basis=2 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 group by c.po_breakdown_id");
		foreach($sql_prod as $prodRow)
		{
			$prodDataArr[$prodRow[csf('po_breakdown_id')]]=$prodRow[csf('knitting_qnty')];
		}
	
		$sql="select a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, b.id as po_id, b.po_number, sum(d.grey_fab_qnty) as qnty $year_field from wo_po_details_master a, wo_po_break_down b, wo_booking_mst c, wo_booking_dtls d where a.job_no=b.job_no_mst and b.id=d.po_break_down_id and a.job_no=c.job_no and c.booking_no=d.booking_no and a.company_name=$company_name and a.job_no like '$job_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 and d.grey_fab_qnty>0 $buyer_id_cond $po_cond $year_cond group by b.id, a.job_no, a.job_no_prefix_num, a.company_name, a.buyer_name, b.po_number, a.insert_date order by a.buyer_name";
		//echo $sql;//die;
		ob_start();
		?>
		<div style="width:100%; margin-top:5px;" align="center">
			<fieldset style="width:642px;">
				<table cellspacing="0" cellpadding="0" border="1" rules="all" width="620" class="rpt_table" align="left">
					<thead>
						<th width="40">SL</th>
						<th width="60">Job No</th>
						<th width="50">Year</th>
						<th width="120">Order No</th>
						<th width="110">Req. Qty</th>
						<th width="110">Prod. Qty</th>
						<th>Balance</th>
					</thead>
				</table>
				<div style="width:642px; overflow-y:scroll; max-height:450px;" id="scroll_body" align="left">
					<table cellspacing="0" cellpadding="0" border="1" rules="all" width="620" class="rpt_table" id="tbl_list_search" align="left">
						<?php 
							$i=1; $buyer_array=array(); $po_array=array();
							$nameArray=sql_select( $sql );
							foreach ($nameArray as $row)
							{
								if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
								
								if(!in_array($row[csf('buyer_name')], $buyer_array))
								{
									if($i!=1)
									{
									?>
										<tr bgcolor="#CCCCCC">
											<td colspan="4" align="right"><b>Buyer Total</b></td>
											<td align="right"><b><?php echo number_format($buyer_req_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($buyer_prod_qnty,2,'.',''); ?></b></td>
											<td align="right"><b><?php echo number_format($buyer_balance,2,'.',''); ?></b></td>
										</tr>
									<?php
										$buyer_req_qnty = 0;
										$buyer_prod_qnty = 0;
										$buyer_balance = 0;
									}
								?>
									<tr bgcolor="#EFEFEF">
										<td colspan="18">
											<b>Buyer Name:- <?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></b>
										</td>
									</tr>
								<?php
									$buyer_array[]=$row[csf('buyer_name')];
								}
								
								$req_qnty=$row[csf('qnty')];
								$prod_qnty=$prodDataArr[$row[csf('po_id')]];
								$balance_qnty=$req_qnty-$prod_qnty;
								
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>"> 
									<td width="40"><?php echo $i; ?></td>
									<td width="60">&nbsp;&nbsp;<?php echo $row[csf('job_no_prefix_num')]; ?></td>
									<td width="50" align="center"><?php echo $row[csf('year')]; ?>&nbsp;</td>
									<td width="120"><p><?php echo $row[csf('po_number')]; ?></p></td>
									<td align="right" width="110"><?php echo number_format($req_qnty,2,'.',''); ?></td>
									<td align="right" width="110"><?php echo number_format($prod_qnty,2,'.',''); ?></td>
									<td align="right"><?php echo number_format($balance_qnty,2,'.',''); ?></td>
								</tr>
								<?php
								
								$total_req_qnty+=$req_qnty;
								$total_prod_qnty+=$prod_qnty;
								$total_balance+=$balance_qnty;
								
								$buyer_req_qnty+=$req_qnty;
								$buyer_prod_qnty+=$prod_qnty;
								$buyer_balance+=$balance_qnty;
	
								$i++;
							}
							
							if($i>1)
							{
							?>
								<tr bgcolor="#CCCCCC" id="tr_<?php echo $i; ?>">
									<td colspan="4" align="right"><b>Buyer Total</b></td>
									<td align="right"><b><?php echo number_format($buyer_req_qnty,2,'.',''); ?></b></td>
									<td align="right"><b><?php echo number_format($buyer_prod_qnty,2,'.',''); ?></b></td>
									<td align="right"><b><?php echo number_format($buyer_balance,2,'.',''); ?></b></td>
	
								</tr>
							<?php
							}
						?>
						<tfoot>
							<th colspan="4" align="right">Grand Total</th>
							<th align="right"><?php echo number_format($total_req_qnty,2,'.',''); ?></th>
							<th align="right"><?php echo number_format($total_prod_qnty,2,'.',''); ?></th>
							<th align="right"><?php echo number_format($total_balance,2,'.',''); ?></th>
						</tfoot>
					</table>
				</div>
			</fieldset>
		</div>
	<?php	
	}
	foreach (glob("../../../ext_resource/tmp_report/$user_name*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename="../../../ext_resource/tmp_report/".$user_name."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,$html);
	$filename="../../ext_resource/tmp_report/".$user_name."_".$name.".xls";
	echo "$total_data####$filename";
	exit();

}

?>