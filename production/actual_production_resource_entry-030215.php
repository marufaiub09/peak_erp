<?php
/*-------------------------------------------- Comments
Purpose			: 	This form for Next to Ex-Factory Entry
				
Functionality	:	
JS Functions	:
Created by		:	Saidul Reza 
Creation date 	: 	28-12-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Sewing Out Info","../", 1, 1, $unicode,1,'');

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";
 

<!--System ID-->
function open_popup(id)
{ 
/*	var row_num=$('#evaluation_tbl tbody tr').length;	
	//alert (id);
	if( form_validation('cbo_Report_id','Page/Report Name')==false )
	{
		return;
	}
	
*/	
	var page_link="requires/actual_production_resource_entry_controller.php?action=StyleRef_popup"; 
	var title="Style Ref";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=420px,center=1,resize=0,scrolling=0','')

	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("selected_id").value;
		if (theemail!="")
		{
			theemail=theemail.split("_");
			document.getElementById('txt_style_ref').value=theemail[0];
			document.getElementById('txt_style_ref_show').value=theemail[1];
			release_freezing();
		}
		
	}
}



<!--System ID-->
function open_line_popup(id)
{ 
/*	var row_num=$('#evaluation_tbl tbody tr').length;	
	//alert (id);
	if( form_validation('cbo_Report_id','Page/Report Name')==false )
	{
		return;
	}
	
*/	
	var page_link="requires/actual_production_resource_entry_controller.php?action=Line_popup"+"**"+document.getElementById('cbo_line_no').value+"**"+document.getElementById('cbo_line_merge').value+"**"+document.getElementById('cbo_company_name').value+"**"+document.getElementById('cbo_location').value+"**"+document.getElementById('cbo_floor').value; 
	var title="Style Ref";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=350px,height=420px,center=1,resize=0,scrolling=0','')

	emailwindow.onclose=function()
	{
		var lineid=this.contentDoc.getElementById("selecteds").value; 
		var linetext=this.contentDoc.getElementById("linename").value; 
		if (lineid!="")
		{
			document.getElementById('cbo_line_no').value=lineid;
			document.getElementById('cbo_line_no_sow').value=linetext;
			release_freezing();
		}
		
	}
}

function openmypage_time()
{
	var cbo_company_name = $('#cbo_company_name').val();
	var save_string = $('#save_string').val();
	if (form_validation('cbo_company_name','Company')==false)
	{
		return;
	}
	
	var title="Production Info";
	var page_link = 'requires/actual_production_resource_entry_controller.php?cbo_company_name='+cbo_company_name+'&save_string='+save_string+'&action=time_popup';
	  
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=250px,center=1,resize=1,scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
		var save_string=this.contentDoc.getElementById("hide_save_string").value;	
	
		$('#save_string').val(save_string);
	}
}

<!--System ID-->
function system_id_popup(id)
{ 
	var page_link="requires/actual_production_resource_entry_controller.php?action=SystemIdPopup"; 
	var title="Style Ref";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=625px,height=420px,center=1,resize=0,scrolling=0','')

	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("selected_id").value; 
			
		if (theemail!="")
		{
			theemail=theemail.split("_");
			document.getElementById('system_id').value=theemail[0];
			document.getElementById('system_id_show').value=theemail[1]; 
			document.getElementById('cbo_company_name').value=theemail[2];
			
			load_drop_down( 'requires/actual_production_resource_entry_controller', theemail[2], 'load_drop_down_location', 'location_td' );
			document.getElementById('cbo_location').value=theemail[3];
			load_drop_down( 'requires/actual_production_resource_entry_controller',theemail[3]+'_'+document.getElementById('cbo_company_name').value, 'load_drop_down_floor', 'floor_td' );			
			
			document.getElementById('cbo_floor').value=theemail[4];
			document.getElementById('cbo_line_merge').value=theemail[5];
			document.getElementById('cbo_line_no').value=theemail[6];
			document.getElementById('cbo_line_no_sow').value=theemail[7];
			jQuery('#cbo_company_name').attr('disabled',true);
			jQuery('#cbo_floor').attr('disabled',true);
			jQuery('#cbo_line_no').attr('disabled',true);
			jQuery('#cbo_location').attr('disabled',true);
			jQuery('#cbo_line_merge').attr('disabled',true);
			jQuery('#cbo_line_no_sow').attr('disabled',true);
			
			show_list_view(theemail[0],'create_acl_pdc_rec_entry_list_view','list_container','requires/actual_production_resource_entry_controller','setFilterGrid("list_view",-1)');
			
			release_freezing();
			
		}
		
	}
}

function fnc_Ac_Production_Resource_Entry(operation)
{ 

	if( form_validation('cbo_company_name*cbo_location*cbo_floor*cbo_line_no*txt_date_from*txt_date_to*txt_target_hour*txt_working_hour*txt_man_power','Company Name*Location*Floor*Line No*Date From*Date To*Target/Hour*Working Hour*Man Power/line')==false )
	{
		return;
	}
	
	var dataString = "cbo_company_name*cbo_location*cbo_floor*cbo_line_merge*cbo_line_no*txt_date_from*txt_target_hour*txt_date_to*txt_working_hour*txt_man_power*txt_style_ref*txt_active_machine*system_id*txt_operator*txt_helper*txt_Line_chief*h_dtl_mst_id*txt_capacity*save_string*txt_smv_adjustment*cbo_smv_adjust_by";
 	var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
	//alert(data)
	freeze_window(operation);
	http.open("POST","requires/actual_production_resource_entry_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_emp_info_reponse;
}

function fnc_emp_info_reponse()
{
	if(http.readyState == 4) 
	{ 
 //alert(http.responseText)
		var response=trim(http.responseText).split('**');

		if(response[0]==0 || response[0]==1|| response[0]==2)
		{
			show_msg(trim(response[0]));
			if(response[2]){document.getElementById("system_id_show").value=response[2];}
			document.getElementById("system_id").value=response[1];
			reset_form('','','txt_date_from*txt_target_hour*txt_date_to*txt_working_hour*txt_man_power*txt_style_ref*txt_active_machine*txt_operator*txt_helper*txt_Line_chief*txt_style_ref_show*txt_capacity*txt_smv_adjustment*cbo_smv_adjust_by','','','');
			show_list_view(response[1],'create_acl_pdc_rec_entry_list_view','list_container','requires/actual_production_resource_entry_controller','setFilterGrid("list_view",-1)');
			set_button_status(0, permission, 'fnc_Ac_Production_Resource_Entry',1);
			document.getElementById("h_dtl_mst_id").value='';
			//release_freezing();
		}
		if(response[0]==10)
		{
			show_msg(trim(response[0]));

		}
		if(response[0]==11)
		{
			alert("Id Card Number Should not be Duplicate");
			//set_button_status(0, permission, 'fnc_emp_info',1,1);
			release_freezing();
		}
		
		
		if(response[0]==45)
		{
			alert("Make Variable setting yes (Production Resource Allocation) for data entry in this page");
			//set_button_status(0, permission, 'fnc_emp_info',1,1);
			release_freezing();
		}
		/*if(response[0]==5)
		{
			alert('Duplicate system id not allowed for same line number. Check existing ID : '+response[1]+' for this line.');
			release_freezing();
		}*/
		if(response[0]==6)
			{
				alert('Duplicate Date Range not allowed for same line number. Check existing ID : '+response[1]+' for this line.');
				release_freezing();
			}
			release_freezing();
	
	}
	
	
}


function fnResetForm()
{
	/*$("#tbl_master").find('input,select').attr("disabled", false);	
	disable_enable_fields( 'cbo_uom', 0, "", "" );
	set_button_status(0, permission, 'fnc_getout_entry',1);*/
	jQuery('#cbo_company_name').attr('disabled',false);
	jQuery('#cbo_floor').attr('disabled',false);
	jQuery('#cbo_line_no').attr('disabled',false);
	jQuery('#cbo_location').attr('disabled',false);
	jQuery('#cbo_line_merge').attr('disabled',false);
	jQuery('#cbo_line_no_sow').attr('disabled',false);
	reset_form('actual_production_resource_entry_1','list_container','','','','');
	set_button_status(0, permission, 'fnc_Ac_Production_Resource_Entry',1);
	
}

</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">

<?php echo load_freeze_divs ("../",$permission);  ?>

<fieldset style="width:900px;">
<legend>Production Module</legend>  
    <form name="actual_production_resource_entry_1" id="actual_production_resource_entry_1" autocomplete="off" >
            <table width="100%" border="0">
               <tr>
                    <td width="60" colspan="3" align="right">System Id</td>
                    <td colspan="3" class="must_entry_caption">
                         <input type="text" name="system_id_show" id="system_id_show" class="text_boxes" style="width:150px;" tabindex="1" placeholder="Double Click to Search" onDblClick="system_id_popup();">
                         <input type="hidden" name="system_id" id="system_id">
                         <input type="hidden" name="h_dtl_mst_id" id="h_dtl_mst_id">
                    </td>
               </tr>
                <tr>
                    <td width="60" class="must_entry_caption">Company</td>
                    <td width="220">                                
                        <?php 
                         echo create_drop_down( "cbo_company_name", 220, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 order by company_name","id,company_name", 1, "-- Select --", $selected, "load_drop_down( 'requires/actual_production_resource_entry_controller', this.value, 'load_drop_down_location', 'location_td' );get_php_form_data(this.value,'sweing_production_start','requires/actual_production_resource_entry_controller' );" ,"","","","","",2 );
                        ?>
                    </td>
                    <td width="60" class="must_entry_caption">Location</td>
                    <td width="220" id="location_td">
                        <?php
                           
						   echo create_drop_down( "cbo_location", 220, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 order by floor_name limit 0 ,0","id,floor_name", 1, "-- Select Floor --", $selected, "",0,"","","","",3 );
							// echo create_drop_down( "cbo_location", 220, "select id,location_name from lib_location where status_active =1 and is_deleted=0 order by location_name limit 0 ,0","id,location_name", 1, "-- Select Floor --", $selected, "",0,"","","","",3 );
                        ?> 
                    </td>
                    <td width="50" class="must_entry_caption">Floor</td>
                    <td width="220" id="floor_td">
                        <?php
                            echo create_drop_down( "cbo_floor", 220, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 order by floor_name limit 0 ,0","id,floor_name", 1, "-- Select Floor --", $selected, "",0,"","","","",4 );
                         ?> 
                    </td>
                </tr>
                <tr>    
                    <td>Line Merge</td>
                    <td>
                        <?php
                            echo create_drop_down( "cbo_line_merge", 220, $yes_no,"", 1, "-- Select --", 2, "",0,"","","","",5);
                        ?>
                    </td>
                     <td class="must_entry_caption">Line No</td>
                    <td id="line_no_td" colspan="3">
                         <input type="text" name="cbo_line_no_sow" id="cbo_line_no_sow" class="text_boxes" tabindex="6" placeholder="Brows" onClick="open_line_popup();"  style="width:208px ">
                         <input type="hidden" name="cbo_line_no" id="cbo_line_no">
                    </td>
                </tr>
            </table>
           
            <table width="80%" border="0" id="acprsreenTable">
                <tr>
                    <td width="110" class="must_entry_caption">From date</td>
                    <td>
                        <input type="date" name="txt_date_from" id="txt_date_from" class="datepicker" tabindex="7"  style="width:208px " readonly>
                    </td>
                   
                    <td width="110">Line Chief</td>
                    <td>
                        <input type="text" name="txt_Line_chief" id="txt_Line_chief" class="text_boxes" tabindex="13"  style="width:208px ">
                    </td>
                </tr>
                <tr>
                     <td class="must_entry_caption"> To Date </td>
                     <td>
                         <input type="date" name="txt_date_to" id="txt_date_to" class="datepicker"  tabindex="8" style="width:208px " readonly>
                     </td>  
                     <td>Active Machine/line</td>
                     <td>
                         <input type="text" name="txt_active_machine" id="txt_active_machine" class="text_boxes_numeric" tabindex="14" style="width:208px ">
                     </td>  
                    
                </tr>
                <tr>
                     <td class="must_entry_caption">Man Power/line</td>
                     <td>
                         <input type="text" name="txt_man_power" id="txt_man_power" class="text_boxes_numeric" tabindex="9" style="width:208px " >
                     </td>  
                    <td class="must_entry_caption">Target/Hour/line</td>
                    <td>
                        <input type="text" name="txt_target_hour" id="txt_target_hour" class="text_boxes_numeric" tabindex="15"  style="width:208px ">
                    </td>
                </tr>
                <tr>
                     <td>Operator/line</td>
                     <td>
                         <input type="text" name="txt_operator" id="txt_operator" class="text_boxes_numeric" tabindex="10" style="width:208px ">
                     </td> 
                    <td class="must_entry_caption">Working Hour</td>
                    <td>
                        <input type="text" name="txt_working_hour" id="txt_working_hour" class="text_boxes_numeric"  tabindex="16" style="width:208px ">
                    </td>
                </tr>
                <tr>
                     <td>Helper/line</td>
                     <td>
                         <input type="text" name="txt_helper" id="txt_helper" class="text_boxes_numeric" tabindex="11" style="width:208px ">
                     </td> 
                        <td>Capacity</td>
                     <td>
                         <input type="text" name="txt_capacity" id="txt_capacity" class="text_boxes_numeric" tabindex="17" style="width:208px ">
                     </td>
                </tr>
                <tr>
                     <td>Style Ref.</td>
                     <td>
                         <input type="text" name="txt_style_ref_show" id="txt_style_ref_show" class="text_boxes" style="width:208px;" tabindex="12" placeholder="Browse" onClick="open_popup();">
                         <input type="hidden" name="txt_style_ref" id="txt_style_ref" class="text_boxes">
                     </td>
                      <td>Extra Hour SMV Ad. </td>
                     <td>
                         <input type="text" name="txt_smv_adjustment" id="txt_smv_adjustment" class="text_boxes_numeric" tabindex="17" style="width:100px ">
                           	<?php echo create_drop_down( "cbo_smv_adjust_by", 105, $increase_decrease,"", 1, "--- Select ---", 2, "" ); ?>
                     </td>
                </tr>
                <tr>
                   <td> </td>
                    <td>
                         <input type="button" name="btn" id="btn" value="Sweing Production Start" class="formbuttonplasminus" onClick="openmypage_time();" style="width:180px">
                         <input type="hidden" name="save_string" id="save_string">
                     </td>
                
                </tr>
            </table>
           
           
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td align="center" colspan="6" valign="middle" class="button_container">
                        <?php 
                            echo load_submit_buttons( $permission, "fnc_Ac_Production_Resource_Entry", 0,0,"fnResetForm()",1);
                        ?>
                    </td>
                </tr> 
            </table>
 	</form>
     
</fieldset>
<br>
<fieldset style="width:920px;">
<div id="list_container">
</div> 
</fieldset>
</div>
</body>

<script src="../includes/functions_bottom.js" type="text/javascript"></script>
<script>
$('#cbo_location').val(0);

</script>
</html>