﻿<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../includes/common.php');

// if tna_integrated then upadte related apporval table from tna some date fields

extract ($_REQUEST);

// Variable Settings List 
	$sql = "SELECT company_name,tna_integrated FROM  variable_order_tracking WHERE status_active =1 and is_deleted = 0 and variable_list=14";
	$result = sql_select( $sql );
	$variable_settings = array();
	foreach( $result as $row ) 
	{		
		$variable_settings[$row[csf('company_name')]] = $row[csf('tna_integrated')];
	}

//Task Details
	$sql = "SELECT id,task_catagory,task_name,task_short_name,task_type,module_name,link_page,penalty,completion_percent FROM lib_tna_task WHERE is_deleted = 0 and status_active=1";
	$result = sql_select( $sql ) ;
	$tna_task_details = array();
	$tna_task_name=array();
	$tna_task_name_tmp=array();
	foreach( $result as $row ) 
	{
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_catagory']=  $row[csf('task_catagory')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['id']=  $row[csf('id')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_name']=  $row[csf('task_name')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_short_name']=  $row[csf('task_short_name')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['task_type']=  $row[csf('task_type')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['module_name']=  $row[csf('module_name')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['link_page']=  $row[csf('link_page')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['penalty']=  $row[csf('penalty')];
		$tna_task_details[$row[csf('task_catagory')]][$row[csf('task_name')]]['completion_percent']=  $row[csf('completion_percent')];
		
		$tna_task_name[$row[csf('id')]]['task_catagory']=  $row[csf('task_catagory')];
		$tna_task_name[$row[csf('id')]]['task_name']=  $row[csf('task_name')];
		$tna_task_name[$row[csf('id')]]['task_short_name']=  $row[csf('task_short_name')];
		$tna_task_name[$row[csf('id')]]['task_type']=  $row[csf('task_type')];
		$tna_task_name[$row[csf('id')]]['module_name']=  $row[csf('module_name')];
		$tna_task_name[$row[csf('id')]]['link_page']=  $row[csf('link_page')];
		$tna_task_name[$row[csf('id')]]['penalty']=  $row[csf('penalty')];
		$tna_task_name[$row[csf('id')]]['completion_percent']=  $row[csf('completion_percent')]; 
		$tna_task_name_tmp[$row[csf('task_catagory')]][]=$row[csf('id')];
	}
	
// Sample List 
	/*$sql = "SELECT id,sample_type FROM  lib_sample WHERE sample_type in ( 2,3,4 ) and status_active =1 and is_deleted = 0";
	$result = sql_select( $sql );
	$sample_list = array();
	foreach( $result as $row ) 
	{		
		$sample_list[] = $row[csf('id')];
	}*/

// Item List 
	/*$sql = "SELECT id,item_name FROM  lib_item_group WHERE trim_type in ( 1,2 ) and status_active =1 and is_deleted = 0";
	$result = sql_select( $sql );
	$accessories_list = array();
	foreach( $result as $row ) 
	{		
		$accessories_list[] = $row[csf('id')];
	}*/

//Template Details
	$sql_task = "SELECT a.id,task_template_id,lead_time,material_source,total_task,tna_task_id,deadline,execution_days,notice_before,sequence_no,for_specific,b.task_catagory,b.task_name,b.task_type FROM  tna_task_template_details a, lib_tna_task b WHERE  a.is_deleted = 0 and a.status_active=1 and a.tna_task_id=b.id order by for_specific,lead_time";
	$result = sql_select( $sql_task ) ;
	$tna_task_template = array();
	$tna_task_template_task=array();
	$tna_template = array();
	$tna_template_buyer = array(); 
	$i=0;
	$k=0;
	$j=0;
	$template_information=array();
	foreach( $result as $row ) 
	{
		if (!in_array($row[csf("task_template_id")],$template))
		{
			$template[]=$row[csf("task_template_id")];
			if($row[csf("for_specific")]==3) $row[csf("for_specific")]=0;
			if ( $row[csf("for_specific")]==0 )
			{
				$tna_template[]['lead']=$row[csf("lead_time")];
				$tna_template[]['id']=$row[csf("task_template_id")];
				$i++;
			}
			else
			{
				if(!in_array($row[csf('for_specific')],$tna_template_spc)) { $j=0; $tna_template_spc[]=$row[csf("for_specific")]; }
				$tna_template_buyer[$row[csf('for_specific')]][$j]['lead']=$row[csf('lead_time')];
				$tna_template_buyer[$row[csf('for_specific')]][$j]['id']=$row[csf('task_template_id')];
				$j++;
			}
			$k++;
		}
		$template_information[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['lead_time']=$row[csf("lead_time")];
		
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['lead_time']=$row[csf("lead_time")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['material_source']=$row[csf("material_source")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['total_task']=$row[csf("total_task")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['tna_task_id']=$row[csf("tna_task_id")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['deadline']=$row[csf("deadline")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['execution_days']=$row[csf("execution_days")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['notice_before']=$row[csf("notice_before")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['sequence_no']=$row[csf("sequence_no")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['for_specific']=$row[csf("for_specific")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['id']=$row[csf("id")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['task_template_id']=$row[csf("task_template_id")];
		
		
		/*$template_information[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$row[csf("tna_task_id")]]['lead_time']=$row[csf("lead_time")];
		
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['lead_time']=$row[csf("lead_time")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['material_source']=$row[csf("material_source")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['total_task']=$row[csf("total_task")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['tna_task_id']=$row[csf("tna_task_id")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['deadline']=$row[csf("deadline")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['execution_days']=$row[csf("execution_days")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['notice_before']=$row[csf("notice_before")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['sequence_no']=$row[csf("sequence_no")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['for_specific']=$row[csf("for_specific")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['id']=$row[csf("id")];
		 $tna_task_template[$row[csf("task_template_id")]][$tna_task_name[$row[csf("tna_task_id")]]['task_catagory']][$tna_task_name[$row[csf("tna_task_id")]]['task_name']]['task_template_id']=$row[csf("task_template_id")];
		 */
		 if (!in_array($row[csf("task_template_id")],$templatesss))
		 	{ $templatesss[]=$row[csf("task_template_id")]; $g=0; }
			 
		 $tna_task_template_task[$row[csf("task_template_id")]][$row[csf("task_catagory")]][$row[csf("tna_task_id")]]=$row[csf("task_name")];
		 
		// $tna_task_template_task_new[$row[csf("task_template_id")]][$row[csf("task_catagory")]][$row[csf("task_name")]][$row[csf("task_type")]]=$row[csf("task_name")];
		 $tna_task_template_task[$row[csf("task_template_id")]][$row[csf("task_catagory")]]['type']=$row[csf("task_type")];
		 $g++;
		 $i++;
	}
	 
if ( $action=="tna_process" )
{
	// Reprocess Check
	/*if (trim($job_no)=="") $strcond=""; 
	else 
	{
		$job_no=explode("*",$job_no);
		
		for ($i=0; $i<count($job_no);$i++)
		{
			if($job_nos=="")$job_nos="'".$job_no[$i]."'"; else $job_nos=$job_nos.","."'".$job_no[$i]."'";
		}
		mysql_query("delete a.*,b.* FROM tna_process_mst a, tna_process_dtls_mail b  WHERE a.id=b.mst_id and a.job_no in ( $job_nos )") or die (mysql_error()); 
		$strcond=" and job_no in ( $job_nos )";
		$strcond_break=" and job_no_mst in ( $job_nos )";
	}*/
	//$job_nos ="'ASL-13-00191'";
	///mysql_query("delete a.* FROM tna_process_mst a  WHERE   a.job_no in ( $job_nos )") or die (mysql_error()); 
	//$strcond="sdas";

// PO Details List with JOB NO

	if ($strcond=="")
	{
		$sql = "SELECT (pub_shipment_date) as shipment_date,job_no_mst,po_received_date,b.id,po_quantity,a.buyer_name,a.garments_nature FROM wo_po_break_down b, wo_po_details_master a WHERE b.is_deleted = 0 and b.status_active=1 and a.job_no=b.job_no_mst and b.shipment_date!='0000-00-00' and b.po_received_date!='0000-00-00'   ORDER BY b.shipment_date asc";
	}
	else
	{
		$sql = "SELECT (pub_shipment_date) as shipment_date,job_no_mst,po_received_date,b.id,po_quantity,a.buyer_name,a.garments_nature FROM wo_po_break_down b, wo_po_details_master a WHERE b.is_deleted = 0 and b.status_active=1 and a.job_no=b.job_no_mst and b.shipment_date!='0000-00-00' and b.po_received_date!='0000-00-00' and job_no_mst  in ( $job_nos )   ORDER BY b.shipment_date asc";
	}
	 
	$data_array=sql_select($sql);
	$job_no_array=array();
	$order_id_array=array();
	$po_order_template=array();
	$po_order_details=array();
	$job_nature = array();
	$i=0;
	foreach($data_array as $row)
	{
		$remain_days=datediff( "d", $row[csf("po_received_date")], $row[csf("shipment_date")] );
		$template_id=get_tna_template($remain_days,$tna_template,$row[csf("buyer_name")]);
		
		if ( $template_id=="" || $template_id==0 )
		{
			if(count($tna_template_buyer[$row[csf("buyer_name")]])>0) $template_id=
			$tna_template_buyer[$row[csf("buyer_name")]][count($tna_template_buyer[$row[csf("buyer_name")]])-1]['id'];
			else $template_id= $tna_template[count($tna_template)-1]['id'];
		} 
		 //print_r( $template_id ); die;
		if (!in_array( $row[csf("job_no_mst")],$job_no_array)) $job_no_array[]= $row[csf("job_no_mst")] ;
		$order_id_array[$i]=$row[csf("id")];
		$po_order_template[$row[csf("id")]]=  $template_id; 
		$po_order_details[$row[csf("id")]]['po_received_date']=$row[csf("po_received_date")];
		$po_order_details[$row[csf("id")]]['shipment_date']=$row[csf("shipment_date")];
		$po_order_details[$row[csf("id")]]['job_no_mst']=$row[csf("job_no_mst")];
		$po_order_details[$row[csf("id")]]['po_quantity']=$row[csf("po_quantity")];
		$job_nature[$row[csf('job_no')]] = $row[csf('garments_nature')];
		$i++;
	}
	$po_ids=implode(",",$order_id_array);
	$job_no_list="'".implode("','",$job_no_array)."'";

// TNA Processs Data List for Update      TNA UPDATE
	$sql = "SELECT id,job_no,po_number_id,task_category,task_number,actual_start_date,actual_finish_date FROM   tna_process_mst WHERE job_no in ( $job_no_list ) and status_active =1 and is_deleted = 0";
	$result = sql_select( $sql );
	$tna_process_list = array();
	$tna_process_details = array();
	foreach( $result as $row ) 
	{
		$tna_process_list[$row[csf('task_category')]][$row[csf('po_number_id')]][$row[csf('task_number')]]= $row[csf('id')];
		$tna_process_details[$row[csf('id')]]['start']=$row[csf('actual_start_date')];
		$tna_process_details[$row[csf('id')]]['finish']=$row[csf('actual_finish_date')];
	}

// Sample Approval Update Data
	$sql_task = "SELECT job_no_mst,po_break_down_id,sample_type_id,max(submitted_to_buyer) as max_submitted_to_buyer,min(submitted_to_buyer) as min_submitted_to_buyer,max(approval_status_date) as max_approval_status_date,min(approval_status_date) as min_approval_status_date
	 FROM wo_po_sample_approval_info WHERE job_no_mst in ( $job_no_list ) and is_deleted = 0 and status_active=1 group by sample_type_id,po_break_down_id order by job_no_mst,po_break_down_id asc,  	approval_status desc";
	$result = sql_select( $sql_task );
	$sample_approval_update = array(); 
	foreach( $result as $row ) 
	{
		$sample_approval_update[$row[csf("po_break_down_id")]][$row[csf("sample_type_id")]]['max_start_date']=$row[csf("max_submitted_to_buyer")];
		$sample_approval_update[$row[csf("po_break_down_id")]][$row[csf("sample_type_id")]]['min_start_date']=$row[csf("min_submitted_to_buyer")];
		
		$sample_approval_update[$row[csf("po_break_down_id")]][$row[csf("sample_type_id")]]['min_approval_status_date']=$row[csf("min_approval_status_date")];
		$sample_approval_update[$row[csf("po_break_down_id")]][$row[csf("sample_type_id")]]['max_approval_status_date']=$row[csf("max_approval_status_date")];
	}

// LABDIP Approval Data for Update
	$sql_task = "SELECT job_no_mst,po_break_down_id,max(submitted_to_buyer) as max_submitted_to_buyer,min(submitted_to_buyer) as min_submitted_to_buyer,max(approval_status_date) as max_approval_status_date,min(approval_status_date) as min_approval_status_date
	 FROM wo_po_lapdip_approval_info WHERE job_no_mst in ( $job_no_list ) and is_deleted = 0 and status_active=1 group by po_break_down_id order by job_no_mst,po_break_down_id asc";
	$result = sql_select( $sql_task );
	$labdip_update_task = array(); 
	foreach( $result as $row ) 
	{
		$labdip_update_task[$row[csf("po_break_down_id")]]['max_start_date']=$row[csf("max_submitted_to_buyer")];
		$labdip_update_task[$row[csf("po_break_down_id")]]['min_start_date']=$row[csf("min_submitted_to_buyer")];
		
		$labdip_update_task[$row[csf("po_break_down_id")]]['min_approval_status_date']=$row[csf("min_approval_status_date")];
		$labdip_update_task[$row[csf("po_break_down_id")]]['max_approval_status_date']=$row[csf("max_approval_status_date")];
	}

// Trims Approval Data for Update
	$sql_task = "SELECT job_no_mst,po_break_down_id,max(submitted_to_buyer) as max_submitted_to_buyer,min(submitted_to_buyer) as min_submitted_to_buyer,max(approval_status_date) as max_approval_status_date,min(approval_status_date) as min_approval_status_date,trim_type
FROM lib_item_group b, wo_po_trims_approval_info a
WHERE b.id=a.accessories_type_id and job_no_mst in ( $job_no_list ) and a.is_deleted = 0 and a.status_active=1 group by po_break_down_id,trim_type order by job_no_mst,po_break_down_id asc";
	$result = sql_select( $sql_task );
	$trims_update_task = array(); 
	foreach( $result as $row ) 
	{
		$trims_update_task[$row[csf("po_break_down_id")]][$row[csf("trim_type")]]['max_start_date']=$row[csf("max_submitted_to_buyer")];
		$trims_update_task[$row[csf("po_break_down_id")]][$row[csf("trim_type")]]['min_start_date']=$row[csf("min_submitted_to_buyer")];
		
		$trims_update_task[$row[csf("po_break_down_id")]][$row[csf("trim_type")]]['min_approval_status_date']=$row[csf("min_approval_status_date")];
		$trims_update_task[$row[csf("po_break_down_id")]][$row[csf("trim_type")]]['max_approval_status_date']=$row[csf("max_approval_status_date")];
	}
	
// Embelishment Approval Data for Update
	$sql_task = "SELECT job_no_mst,po_break_down_id,max(submitted_to_buyer) as max_submitted_to_buyer,min(submitted_to_buyer) as min_submitted_to_buyer,max(approval_status_date) as max_approval_status_date,min(approval_status_date) as min_approval_status_date,embellishment_id
	 FROM wo_po_embell_approval WHERE job_no_mst in ( $job_no_list ) and is_deleted = 0 and status_active=1 group by po_break_down_id,embellishment_id order by job_no_mst,po_break_down_id asc";
	 
	$result = sql_select( $sql_task );
	$embelishment_update_task = array(); 
	foreach( $result as $row ) 
	{
		$embelishment_update_task[$row[csf("po_break_down_id")]][$row[csf("embellishment_id")]]['max_start_date']=$row[csf("max_submitted_to_buyer")];
		$embelishment_update_task[$row[csf("po_break_down_id")]][$row[csf("embellishment_id")]]['min_start_date']=$row[csf("min_submitted_to_buyer")];
		
		$embelishment_update_task[$row[csf("po_break_down_id")]][$row[csf("embellishment_id")]]['min_approval_status_date']=$row[csf("min_approval_status_date")];
		$embelishment_update_task[$row[csf("po_break_down_id")]][$row[csf("embellishment_id")]]['max_approval_status_date']=$row[csf("max_approval_status_date")];
	}	

// Purchase Approval Data for Update 2- FB, 4 -Trims, 12- Service
	$sql_task = "SELECT b.po_break_down_id, a.booking_date, a.item_category
	 FROM  wo_booking_mst a, wo_booking_dtls b WHERE b.po_break_down_id in ( $po_ids ) and a.booking_no=b.booking_no and a.item_category in (2,4,12 ) group by b.po_break_down_id,item_category order by b.po_break_down_id asc";
	$result = sql_select( $sql_task );
	$purchase_update_task = array(); 
	foreach( $result as $row ) 
	{
		$purchase_update_task[$row[csf("po_break_down_id")]][$row[csf("item_category")]]['booking_date']=$row[csf("booking_date")];
	}
	$purchase_array_mapping=array(1=>2,2=>4,3=>12);
  
// Inventory Update Data  
	$sql = "SELECT b.po_breakdown_id,b.entry_form, min(a.transaction_date) mindate, max(a.transaction_date) maxdate, sum(quantity) as prod_qntry 
FROM inv_transaction a,  order_wise_pro_details b where  b.trans_id=a.id and b.entry_form in ( 2,7 ) and b.po_breakdown_id in ( $po_ids ) group by b.po_breakdown_id,b.entry_form order by b.po_breakdown_id";
	$inventory_transaction_update=array();
	$inv_array_mapping=array(1=>"2",2=>"7",3=>"24",4=>"244");
	$data_array=sql_select($sql);
	foreach($data_array as $row)
	{
		$inventory_transaction_update[$row[csf("po_breakdown_id")]][$row[csf("entry_form")]]['start_date']=$row[csf("mindate")];
		$inventory_transaction_update[$row[csf("po_breakdown_id")]][$row[csf("entry_form")]]['end_date']=$row[csf("maxdate")];
		$inventory_transaction_update[$row[csf("po_breakdown_id")]][$row[csf("entry_form")]]['quantity']=$row[csf("prod_qntry")];
	}
	$sql = "SELECT b.po_breakdown_id,b.entry_form, min(a.transaction_date) mindate, max(a.transaction_date) maxdate, sum(quantity) as prod_qntry,d.trim_type 
FROM inv_transaction a,  order_wise_pro_details b, product_details_master c , lib_item_group d
where a.prod_id=b.id and b.trans_id=a.id and c.item_group_id=d.id and d.trim_type in (1,2) and b.entry_form in ( 24 ) and b.po_breakdown_id in ( $po_ids ) group by b.po_breakdown_id,d.trim_type order by b.po_breakdown_id";
	$data_array=sql_select($sql);
	foreach($data_array as $row)
	{
		$entry=($user['trim_type'] == 1 ? 24 : 244);
		$inventory_transaction_update[$row[csf("po_breakdown_id")]][$entry]['start_date']=$row[csf("mindate")];
		$inventory_transaction_update[$row[csf("po_breakdown_id")]][$entry]['end_date']=$row[csf("maxdate")];
		$inventory_transaction_update[$row[csf("po_breakdown_id")]][$entry]['quantity']=$row[csf("prod_qntry")];
	}
 
 
 //print_r($inventory_transaction_update);  die;
// Inspection Data for Update
	$sql = "SELECT job_no,po_break_down_id,min(inspection_date) as mind,max(inspection_date) as maxd,sum(inspection_qnty) as sumtot FROM pro_buyer_inspection WHERE job_no in ( $job_no_list ) and status_active =1 and is_deleted = 0 group by po_break_down_id ";
	$result = sql_select( $sql );
	$inspection_status_array = array();
	foreach( $result as $row ) 
	{
		$inspection_status_array[$row[csf('po_break_down_id')]]['min'] = $row[csf('mind')];
		$inspection_status_array[$row[csf('po_break_down_id')]]['max'] = $row[csf('maxd')];
		$inspection_status_array[$row[csf('po_break_down_id')]]['qnty'] = $row[csf('sumtot')];
	}
	
// Ex-factory Data for Update  	
	$sql = "SELECT po_break_down_id,min(ex_factory_date) as mind,max(ex_factory_date) as maxd,sum(ex_factory_qnty) as sumtot FROM  pro_ex_factory_mst WHERE po_break_down_id in ( $po_ids ) and status_active =1 and is_deleted = 0 group by po_break_down_id ";
	$result = sql_select( $sql );
	$exfactory_status_array = array();
	//$exfactory_array_mapping=array(1=>"2",2=>"7",3=>"24",4=>"244");
	foreach( $result as $row ) 
	{
		$exfactory_status_array[$row[csf('po_break_down_id')]][1]['min'] = $row[csf('mind')];
		$exfactory_status_array[$row[csf('po_break_down_id')]][1]['max'] = $row[csf('maxd')];
		$exfactory_status_array[$row[csf('po_break_down_id')]][1]['qnty'] = $row[csf('sumtot')];
	}
	
	 
	 
	 
	 // print_r($labdip_update_task); die;
		 

//array(1=>"Ex-Factory to be done",2=>"Document to be submited",3=>"Proceeds to be realized");
	
	$field_array_tna_process="id,template_id,job_no,po_number_id,po_receive_date,shipment_date,task_category,task_number,target_date,task_start_date,task_finish_date,notice_date_start,notice_date_end,process_date,sequence_no,status_active,is_deleted";
	$field_array_tna_process_up="actual_start_date*actual_finish_date";
	 
	// Test Task Start Here
	 
	 //  General Task Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[1] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][1][$value]!=""  )
			{ 
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,-$tna_task_template[$po_order_template[$poid]][1][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][1][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][1][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[1][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',1,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][1][$value]['sequence_no']."',1,0)";
				}
			}
		}
	} 
		echo "umpon".$up; die;
	// Sample Approval Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[5] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][5][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][5][$value]['deadline']);
				$to_add_days=$tna_task_template[$po_order_template[$poid]][5][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][5][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[5][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',5,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][5][$value]['sequence_no']."',1,0)";
				}
				else // Update  submitted_to_buyer   approval_status_date
				{
					// Need to be updated by Task wise
					$process_id_up=$tna_process_list[5][$poid][$value];
					if( $tna_task_name[$value]['task_type']==1 )  // submission
					{
						if ( $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="0000-00-00" || $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="" ) $start_date=$sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_start_date']; else $start_date="0000-00-00";
						if ( $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_start_date']!="0000-00-00" || $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_start_date']!="" ) $finish_date=$sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_start_date']; else $finish_date="0000-00-00";
						$process_id_up_sample[]=$process_id_up;
						$data_array_tna_process_sample[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else if( $tna_task_name[$value]['task_type']==2 )
					{
						if ( $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']!="0000-00-00" || $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']!="" ) $start_date=$sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']; else $start_date="0000-00-00";
						if ( $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="0000-00-00" || $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="" ) $finish_date=$sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_sample[]=$process_id_up;
						$data_array_tna_process_sample[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else
					{
						if ( $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="0000-00-00" || $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="" ) $start_date=$sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['min_start_date']; else $start_date="0000-00-00";
						if ( $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="0000-00-00" || $sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="" ) $finish_date=$sample_approval_update[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_sample[]=$process_id_up;
						$data_array_tna_process_sample[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
				}
			}
		}
	} 
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_sample, $process_id_up_sample ));
	mysql_query("COMMIT");
	
	 // Lapdip Approval Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[6] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][6][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][6][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][6][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][6][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				
				if ($tna_process_list[6][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',6,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][6][$value]['sequence_no']."',1,0)";
				}
				else // Update  submitted_to_buyer   approval_status_date
				{
					$process_id_up=$tna_process_list[6][$poid][$value];
					if( $tna_task_name[$value]['task_type']==1 )  // submission
					{
						if ( $labdip_update_task[$poid]['min_start_date']!="0000-00-00" || $labdip_update_task[$poid]['min_start_date']!="" ) $start_date=$labdip_update_task[$poid]['min_start_date']; else $start_date="0000-00-00";
						if ( $labdip_update_task[$poid]['max_start_date']!="0000-00-00" || $labdip_update_task[$poid]['max_start_date']!="" ) $finish_date=$labdip_update_task[$poid]['max_start_date']; else $finish_date="0000-00-00";
						$process_id_up_lap[]=$process_id_up;
						$data_array_tna_process_lap[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else if( $tna_task_name[$value]['task_type']==2 )
					{
						if ( $labdip_update_task[$poid]['min_approval_status_date']!="0000-00-00" || $labdip_update_task[$poid]['min_approval_status_date']!="" ) $start_date=$labdip_update_task[$poid]['min_approval_status_date']; else $start_date="0000-00-00";
						if ( $labdip_update_task[$poid]['max_approval_status_date']!="0000-00-00" || $labdip_update_task[$poid]['max_approval_status_date']!="" ) $finish_date=$labdip_update_task[$poid]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_lap[]=$process_id_up;
						$data_array_tna_process_lap[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else
					{
						if ( $labdip_update_task[$poid]['min_start_date']!="0000-00-00" || $labdip_update_task[$poid]['min_start_date']!="" ) $start_date=$labdip_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']; else $start_date="0000-00-00";
						if ( $labdip_update_task[$poid]['max_approval_status_date']!="0000-00-00" || $labdip_update_task[$poid]['max_approval_status_date']!="" ) $finish_date=$labdip_update_task[$poid]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_lap[]=$process_id_up;
						$data_array_tna_process_lap[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
				}
			}
		}
	} 
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_lap, $process_id_up_lap ));
	mysql_query("COMMIT");
	 
	 // Accessories Approval Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[7] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][7][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][7][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][7][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][7][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[7][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',7,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][7][$value]['sequence_no']."',1,0)";
				}
				else // Update  min submitted_to_buyer ,max submitted_to_buyer,   same approval_status_date
				{
					// Need to be updated by Task wise
					$process_id_up=$tna_process_list[7][$poid][$value];
					if( $tna_task_name[$value]['task_type']==1 )  // submission
					{
						if ( $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="0000-00-00" || $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="" ) $start_date=$trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']; else $start_date="0000-00-00";
						if ( $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_start_date']!="0000-00-00" || $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_start_date']!="" ) $finish_date=$trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_start_date']; else $finish_date="0000-00-00";
						$process_id_up_trims[]=$process_id_up;
						$data_array_tna_process_trims[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else if( $tna_task_name[$value]['task_type']==2 )
					{
						if ( $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']!="0000-00-00" || $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']!="" ) $start_date=$trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']; else $start_date="0000-00-00";
						if ( $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="0000-00-00" || $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="" ) $finish_date=$trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_trims[]=$process_id_up;
						$data_array_tna_process_trims[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else
					{
						if ( $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="0000-00-00" || $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="" ) $start_date=$trims_update_task[$poid][$tna_task_name[$value]['task_name']][$tna_task_name[$value]['task_name']]['min_start_date']; else $start_date="0000-00-00";
						if ( $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="0000-00-00" || $trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="" ) $finish_date=$trims_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_trims[]=$process_id_up;
						$data_array_tna_process_trims[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
				}
			}
		}
	} 
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_trims, $process_id_up_trims ));
	mysql_query("COMMIT");
	
	 // Embellishment Approval Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[8] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][8][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][8][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][8][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][8][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[8][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',8,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][8][$value]['sequence_no']."',1,0)";
				}
				else // Update  submitted_to_buyer   approval_status_date
				{
					// Need to be updated by Task wise
					$process_id_up=$tna_process_list[8][$poid][$value];
					if( $tna_task_name[$value]['task_type']==1 )  // submission
					{
						if ( $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="0000-00-00" || $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="" ) $start_date=$embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']; else $start_date="0000-00-00";
						if ( $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_start_date']!="0000-00-00" || $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_start_date']!="" ) $finish_date=$embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_start_date']; else $finish_date="0000-00-00";
						$process_id_up_emb[]=$process_id_up;
						$data_array_tna_process_emb[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else if( $tna_task_name[$value]['task_type']==2 )
					{
						if ( $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']!="0000-00-00" || $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']!="" ) $start_date=$embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_approval_status_date']; else $start_date="0000-00-00";
						if ( $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="0000-00-00" || $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="" ) $finish_date=$embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_emb[]=$process_id_up;
						$data_array_tna_process_emb[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
					else
					{
						if ( $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="0000-00-00" || $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['min_start_date']!="" ) $start_date=$embelishment_update_task[$poid][$tna_task_name[$value]['task_name']][$tna_task_name[$value]['task_name']]['min_start_date']; else $start_date="0000-00-00";
						if ( $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="0000-00-00" || $embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']!="" ) $finish_date=$embelishment_update_task[$poid][$tna_task_name[$value]['task_name']]['max_approval_status_date']; else $finish_date="0000-00-00";
						$process_id_up_emb[]=$process_id_up;
						$data_array_tna_process_emb[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
					}
				}
			}
		}
	} 
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_emb, $process_id_up_emb ));
	mysql_query("COMMIT");
	
	 // Test  Approval Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[9] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][9][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][9][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][9][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][9][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[9][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',9,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][9][$value]['sequence_no']."',1,0)";
				}
			}
		}
	}  
	
	 // Purchase  Task Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[15] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][15][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][15][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][15][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][15][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[15][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',15,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][15][$value]['sequence_no']."',1,0)";
				}
				else // Update  3 booking from booking table ---if booking found then booking date is start and end date (Same date)
				{
					// Need to be updated by Task wise
					$process_id_up=$tna_process_list[15][$poid][$value];
					if ( $purchase_update_task[$poid][$purchase_array_mapping[$tna_task_name[$value]['task_name']]]['booking_date']!="0000-00-00" || $purchase_update_task[$poid][$purchase_array_mapping[$tna_task_name[$value]['task_name']]]['booking_date']!="" ) $start_date=$purchase_update_task[$poid][$purchase_array_mapping[$tna_task_name[$value]['task_name']]]['booking_date']; else $start_date="0000-00-00";
					if ( $purchase_update_task[$poid][$purchase_array_mapping[$tna_task_name[$value]['task_name']]]['booking_date']!="0000-00-00" || $purchase_update_task[$poid][$purchase_array_mapping[$tna_task_name[$value]['task_name']]]['booking_date']!="" ) $finish_date=$purchase_update_task[$poid][$purchase_array_mapping[$tna_task_name[$value]['task_name']]]['booking_date']; else $finish_date="0000-00-00";
					$process_id_up_book[]=$process_id_up;
					$data_array_tna_process_book[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
				}
			}
		}
	}
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_book, $process_id_up_book ));
	mysql_query("COMMIT");
	
	 // Material Receive Task Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[20] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][20][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][20][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][20][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][20][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[20][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',20,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][20][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					// Need to be updated by Task wise
					$process_id_up=$tna_process_list[20][$poid][$value];
					if ( $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['start_date']!="0000-00-00" || $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['start_date']!="" ) $start_date=$inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['start_date']; else $start_date="0000-00-00";
					if ( $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['end_date']!="0000-00-00" || $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['end_date']!="" ) $finish_date=$inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['end_date']; else $finish_date="0000-00-00";
					$process_id_up_mat[]=$process_id_up;
					$data_array_tna_process_mat[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'")); 
				}
			}
		}
	}
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_mat, $process_id_up_mat ));
	mysql_query("COMMIT");
	
	// Fabric Production Task Start Here  // ------------------------ Type 6 is Left for Dyeing
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[25] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][25][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][25][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][25][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][25][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[25][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',25,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][25][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					// Need to be updated by Task wise
					$process_id_up=$tna_process_list[25][$poid][$value];
					if ( $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['start_date']!="0000-00-00" || $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['start_date']!="" ) $start_date=$inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['start_date']; else $start_date="0000-00-00";
					if ( $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['end_date']!="0000-00-00" || $inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['end_date']!="" ) $finish_date=$inventory_transaction_update[$poid][$inv_array_mapping[$tna_task_name[$value]['task_name']]]['end_date']; else $finish_date="0000-00-00";
					$process_id_up_fp[]=$process_id_up;
					$data_array_tna_process_fp[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
				}
			}
		}
	}
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_fp, $process_id_up_fp ));
	mysql_query("COMMIT");
	
	 // Garments Production Task Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {  // Need Cunsultancy abt task detais
		foreach($tna_task_name_tmp[26] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][26][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][26][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][26][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][26][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[26][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',26,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][26][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					// Need to be updated by Task wise
					//tna_task_template_type[$row[csf("task_template_id")]][$row[csf("task_catagory")]]['type']
					$process_id_up=$tna_process_list[26][$poid][$value];
					if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']!="" ) $start_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['start_date']; else $start_date="0000-00-00";
					if ( $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="0000-00-00" || $knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']!="" ) $finish_date=$knit_fabric_transaction_update[$poid][$knit_tmp[$value]]['end_date']; else $finish_date="0000-00-00";
					$process_id_up_array[]=$process_id_up;
					$data_array_tna_process_up[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
				}
			}
		}
	 } 
	
	 // Inspection Task Start Here  Update Completed
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[30] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][30][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][30][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][30][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][30][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[30][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',30,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][30][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					
					//tna_task_template_type[$row[csf("task_template_id")]][$row[csf("task_catagory")]]['type']
					//Update from Inspection table  total pass qnty to be compared with task completion %
					$process_id_up=$tna_process_list[30][$poid][$value];
					
					if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; 
					else $start_date= ($inspection_status_array[$poid]['min'] == "" ? "0000-00-00" : $inspection_status_array[$poid]['min']); 
					if (get_percent($inspection_status_array[$poid]['qnty'], $po_order_details[$poid]['po_quantity'])>=$tna_task_details[30][$value]['completion_percent'])
					{
						if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; 
						else $finish_date=($inspection_status_array[$poid]['maxd'] == "" ? "0000-00-00" : $inspection_status_array[$poid]['maxd']); 
					}
					else $finish_date="0000-00-00";
					$process_id_up_ins[]=$process_id_up;
					$data_array_tna_process_ins[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
				}
			}
		}
	}
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_ins, $process_id_up_ins ));
	mysql_query("COMMIT");
	
	 // Export Task Start Here
	 foreach($order_id_array as $poid)   // treat fabric nature knit or woven
	 {
		foreach($tna_task_name_tmp[35] as $fid=>$value)
		{
			if( $tna_task_template_task[$po_order_template[$poid]][35][$value]!=""  )
			{
				$target_date=add_date($po_order_details[$poid]['shipment_date'] ,- $tna_task_template[$po_order_template[$poid]][35][$value]['deadline']);
				
				$to_add_days=$tna_task_template[$po_order_template[$poid]][35][$value]['execution_days']-1;
				$start_date=add_date($target_date ,-$to_add_days);
				$finish_date=$target_date;
				$to_add_days=$tna_task_template[$po_order_template[$poid]][35][$value]['notice_before'];
				$notice_date_start=add_date($start_date ,-$to_add_days);
				$notice_date_end=add_date($finish_date ,-$to_add_days);
				if ($tna_process_list[35][$poid][$value]=="")
				{
					if ($id_tna_process=="")$id_tna_process=return_next_id( "id", " tna_process_mst",1); else $id_tna_process+=1;
					if ($data_array_tna_process!="") $data_array_tna_process .=",";
					$data_array_tna_process .="(".$id_tna_process.",".$po_order_template[$poid].",'".$po_order_details[$poid]['job_no_mst']."','".$poid."','".$po_order_details[$poid]['po_received_date']."','".$po_order_details[$poid]['shipment_date']."',35,".$value.",'".$target_date."','".$start_date."','".$finish_date."','".$notice_date_start."','".$notice_date_end."','".$pc_date_time."','".$tna_task_template[$po_order_template[$poid]][35][$value]['sequence_no']."',1,0)";
				}
				else // Update
				{
					// Need to be updated by Task wise
					$process_id_up=$tna_process_list[35][$poid][$value];
					if ( $tna_process_details[$process_id_up]['start']!="0000-00-00" ) $start_date=$tna_process_details[$process_id_up]['start']; 
					else $start_date= ($exfactory_status_array[$poid][$tna_task_name[$value]['task_name']]['min'] == "" ? "0000-00-00" : $exfactory_status_array[$poid][$tna_task_name[$value]['task_name']]['min']); 
					if (get_percent($exfactory_status_array[$poid][$tna_task_name[$value]['task_name']]['qnty'], $po_order_details[$poid]['po_quantity'])>=$tna_task_details[35][$value]['completion_percent'])
					{
						if( $tna_process_details[$process_id_up]['finish']!="0000-00-00" ) $finish_date=$tna_process_details[$process_id_up]['finish']; 
						else $finish_date=($exfactory_status_array[$poid][$tna_task_name[$value]['task_name']]['maxd'] == "" ? "0000-00-00" : $exfactory_status_array[$poid][$tna_task_name[$value]['task_name']]['maxd']); 
					}
					else $finish_date="0000-00-00";
					$process_id_up_exp[]=$process_id_up;
					$data_array_tna_process_exp[$process_id_up] =explode(",",("'".$start_date."','".$finish_date."'"));
				}
			}
		}
	} 
	$up=sql_execute(bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_exp, $process_id_up_exp ));
	mysql_query("COMMIT");
	
	//echo bulk_update_sql_statement( "tna_process_mst", "id", $field_array_tna_process_up, $data_array_tna_process_up, $process_id_up_array ); die;
	$con = connect();
	if($db_type==0)
	{
		mysql_query("BEGIN");
	}
	if($data_array_tna_process!="") 
		$rID=sql_insert("tna_process_mst",$field_array_tna_process,$data_array_tna_process,1);
	 
		 
	mysql_query("COMMIT"); echo "0**".$rID; die;
	if($db_type==0)
	{
		if($rID ){
			mysql_query("COMMIT");  
			
		}
		else{
			mysql_query("ROLLBACK"); 
			echo "10**".$rID;
		}
	}
	
	if($db_type==2 || $db_type==1 )
	{
		echo '0**0';
	}
	disconnect($con);
	
	
	
	echo "222"; die;
	
	
}


function get_tna_template( $remain_days, $tna_template, $buyer ) // Always treat the lowest template ... if not no process on that
{
	//return 5;   
	global $tna_template_buyer;
	if(count($tna_template_buyer[$buyer])>0)
	{ 
		$n=count($tna_template_buyer[$buyer]); 
		for($i=0;$i<$n;$i++)
		{ 
			if($remain_days<=$tna_template_buyer[$buyer][$i]['lead']) 
			{
				if ($i!=0)
				{
					$up_day=$tna_template_buyer[$buyer][$i]['lead']-$remain_days;
					$low_day=$remain_days-$tna_template_buyer[$buyer][$i-1]['lead'];
					if ($up_day>=$low_day)
						return $tna_template_buyer[$buyer][$i-1]['id'];
					else
						return $tna_template_buyer[$buyer][$i]['id'];
				}
				else
				{
					return $tna_template_buyer[$buyer][$i]['id'];
				}
			}
		}
	}
	else
	{
		$n=count($tna_template); 
		for($i=0;$i<$n;$i++)
		{
			if($remain_days<=$tna_template[$i]['lead']) 
			{
				if ($i!=0)
				{
					$up_day=$tna_template[$i]['lead']-$remain_days;
					$low_day=$remain_days-$tna_template[$i-1]['lead'];
					if ($up_day>=$low_day)
						return $tna_template[$i-1]['id'];
					else
						return $tna_template[$i]['id'];
				}
				else
				{
					return $tna_template[$i]['id'];
				}
			}
		}
	}
}

function get_percent($completed, $actual)
{
	return number_format((($completed*100)/$actual),0);
}

?>