<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../includes/common.php');
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 135, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  
	exit();
}

if ($action=="style_ref_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
?>
	  <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('style_ref_id').value=id;
			  parent.emailwindow.hide();
		  }
	  </script>
  </head>
  <body>
	<div align="center" style="width:100%;" >
		<form name="gsdentry_1"  id="gsdentry_1" autocomplete="off">
            <table width="880" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                <thead>                	 
                      <th>Company Name</th>
                      <th>Buyer Name</th>
                      <th>Date Range</th>
                      <th>Search By</th>
                      <th id="search_by_td_up" width="165">Please Enter Order Number</th>
                      <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>           
                  </thead>
                  <tr class="general">
                      <td> <input type="hidden" id="style_ref_id" style="width:100px;" >  
                      <?php   
                          echo create_drop_down( "cbo_company_id", 135, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "-- Select Company --",$data,"load_drop_down( 'gsd_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );",0 );
                      ?>
                      </td>
                      <td id="buyer_td">
                        <?php
                        	echo create_drop_down( "cbo_buyer_name", 135, $blank_array,"", 1, "-- Select Buyer --", $selected, "",0,"","","","");
                        ?> 
                      </td>
                      <td>
                      <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">To
                      <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
                      </td>
                      <td>
                        <?php
                            $search_by_arr=array(1=>"Order Number",2=>"Job Number",3=>"Style Ref");
                            $dd="change_search_event(this.value, '0*0*0*0*0', '0*0*0*2*0', '../../') ";							
                            echo create_drop_down( "cbo_search_by", 115, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?> 
                      </td>
                      <td id="search_by_td">
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                      </td> 
                      <td align="center">
                          <input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value, 'style_ref_list_view', 'search_div', 'gsd_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                      </td>
                  </tr>
                  <tr>
                      <td colspan="6" align="center" height="40" valign="middle">
                          <?php echo load_month_buttons(1);  ?>
                      </td>
                  </tr>
              </table>
              <div id="search_div" style="margin-top:5px"></div>
		</form>
	</div>
    </body>
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    <script> document.getElementById('cbo_buyer_name').value=<?php echo $buyer_id; ?>; </script> 
    </html>
<?php
exit();
}

if ($action=="style_ref_list_view")
{
	$data=explode('_',$data);
	
	//$company_id=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$buyer_name_arr=return_library_array( "select id,short_name from lib_buyer", "id","short_name"  );
	
	if ($data[0]!=0) $company_name=" and b.company_name='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $buyer_name=" and b.buyer_name='$data[1]'"; else { echo "Please Select Buyer First."; die; }
	
	$start_date =trim($data[2]);
	$end_date =trim($data[3]);	
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,"yyyy-mm-dd", "-")."' and '".change_date_format($end_date,"yyyy-mm-dd", "-")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,'','',1)."' and '".change_date_format($end_date,'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	
	$search_by=$data[4];
	$search_string=trim($data[5]);

	if($search_by==1) $search_field_cond=" and b.po_number like '%".$search_string."%'";
	else if($search_by==2) $search_field_cond=" and  a.job_no like '%".$search_string."'";
	else $search_field_cond=" and a.style_ref_no like '%".$search_string."%'"; 

	if($db_type==0) $year_field="YEAR(a.insert_date)"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY')";
	else $year_field="";//defined Later
	
	$arr=array (1=>$buyer_name_arr,3=>$garments_item);
		
	//$sql ="SELECT DISTINCT  id, up_id,po_job_no,company_name,buyer_name,style_ref_no,gmts_item_id,po_number,shipment_date FROM (SELECT 0 AS id, a.id AS up_id,a.po_job_no,b.company_name,b.buyer_name,b.style_ref_no,b.gmts_item_id,c.po_number,c.pub_shipment_date as shipment_date FROM ppl_gsd_entry_mst a,wo_po_details_master b, wo_po_break_down c WHERE a.po_job_no=b.job_no AND b.job_no=c.job_no_mst $company_name $buyer_name GROUP BY job_no UNION SELECT a.id AS id,0 AS up_id,a.job_no AS po_job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id,GROUP_CONCAT(b.po_number) AS po_number,min(b.pub_shipment_date) as shipment_date FROM wo_po_details_master a, wo_po_break_down b WHERE a.job_no= b.job_no_mst AND a.company_name=$data[0] AND a.buyer_name=$data[1] GROUP BY job_no) AS t1 GROUP BY po_job_no";
	
	/*$sql ="SELECT DISTINCT po_job_no,company_name,buyer_name,style_ref_no,gmts_item_id,po_number,shipment_date FROM (SELECT a.po_job_no,b.company_name,b.buyer_name,b.style_ref_no,b.gmts_item_id,wm_concat(CAST(c.po_number  AS VARCHAR(4000))) AS po_number,min(c.pub_shipment_date) as shipment_date FROM ppl_gsd_entry_mst a,wo_po_details_master b, wo_po_break_down c WHERE a.po_job_no=b.job_no AND b.job_no=c.job_no_mst $company_name $buyer_name GROUP BY a.po_job_no,b.company_name,b.buyer_name,b.style_ref_no,b.gmts_item_id UNION SELECT a.job_no AS po_job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id,wm_concat(CAST(po_number  AS VARCHAR(4000))) AS po_number,min(b.pub_shipment_date) as shipment_date FROM wo_po_details_master a, wo_po_break_down b WHERE a.job_no= b.job_no_mst AND a.company_name=$data[0] AND a.buyer_name=$data[1] GROUP BY a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id) t group by po_job_no,company_name,buyer_name,style_ref_no,gmts_item_id,po_number,shipment_date";*/
	
	if($db_type==0)
	{
		//$sql ="SELECT c.id as up_id,a.id as id,a.job_no as po_job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id,GROUP_CONCAT(b.po_number) AS po_number, min(b.pub_shipment_date) as shipment_date, $year_field as year FROM wo_po_break_down b, wo_po_details_master a left join ppl_gsd_entry_mst c on a.job_no=c.po_job_no and c.is_deleted=0 where a.job_no= b.job_no_mst AND a.company_name=$data[0] AND a.buyer_name=$data[1] $date_cond $search_field_cond group by c.id,a.id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.insert_date, a.gmts_item_id";
		$sql ="SELECT c.id as up_id,a.id as id,a.job_no as po_job_no,a.company_name,a.job_no_prefix_num,a.buyer_name,a.style_ref_no,s.gmts_item_id,GROUP_CONCAT(b.po_number) AS po_number, min(b.pub_shipment_date) as shipment_date, $year_field as year FROM wo_po_break_down b, wo_po_details_master a, wo_po_details_mas_set_details s left join ppl_gsd_entry_mst c on s.job_no=c.po_job_no and s.gmts_item_id=c.gmts_item_id and c.is_deleted=0 where a.job_no= b.job_no_mst and a.job_no=s.job_no AND a.company_name=$data[0] AND a.buyer_name=$data[1] $date_cond $search_field_cond group by c.id, a.id, a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.style_ref_no,a.insert_date,s.gmts_item_id";
	
	}
	else
	{
		//$sql ="SELECT c.id as up_id,a.id as id,a.job_no as po_job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id,listagg(CAST(b.po_number as VARCHAR(4000)),',') within group (order by b.po_number) as po_number, min(b.pub_shipment_date) as shipment_date, $year_field as year FROM wo_po_break_down b, wo_po_details_master a left join ppl_gsd_entry_mst c on a.job_no=c.po_job_no and c.is_deleted=0 where a.job_no= b.job_no_mst AND a.company_name=$data[0] AND a.buyer_name=$data[1] $date_cond $search_field_cond group by c.id, a.id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.insert_date, a.gmts_item_id";
		$sql ="SELECT c.id as up_id,a.id as id,a.job_no as po_job_no,a.job_no_prefix_num,a.company_name,a.buyer_name,a.style_ref_no,s.gmts_item_id,listagg(CAST(b.po_number as VARCHAR(4000)),',') within group (order by b.po_number) as po_number, min(b.pub_shipment_date) as shipment_date, $year_field as year FROM wo_po_break_down b, wo_po_details_master a, wo_po_details_mas_set_details s left join ppl_gsd_entry_mst c on s.job_no=c.po_job_no and s.gmts_item_id=c.gmts_item_id and c.is_deleted=0 where a.job_no= b.job_no_mst and a.job_no=s.job_no and a.company_name=$data[0] AND a.buyer_name=$data[1] $date_cond $search_field_cond group by c.id, a.id, a.job_no_prefix_num, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, a.insert_date, s.gmts_item_id";
	}
	//echo $sql;die;
	echo create_list_view("list_view", "GSD ID, Buyer,Style Ref.,Gmt. Item,Year, Job No.,Shipment Date,PO Numbers
", "70,70,120,140,60,70,90","890","230",0, $sql , "js_set_value", "up_id,id,po_job_no,buyer_name,style_ref_no,gmts_item_id,po_number", "", 1, "0,buyer_name,0,gmts_item_id,0,0,0,0,", $arr , "up_id,buyer_name,style_ref_no,gmts_item_id,year,job_no_prefix_num,shipment_date,po_number", "gsd_entry_controller","",'0,0,0,0,0,0,3,0') ;

exit();
}

if ($action=="load_php_data_to_form_style")
{
	/*$data=explode(',',$data);
	
	//echo $data[2];

	$buyer_name_arr=return_library_array( "select id,buyer_name from lib_buyer", "id","buyer_name"  );
	
	if($db_type==0)
	{
		$nameArray =sql_select("SELECT a.id,a.job_no as po_job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id,group_concat(b.po_number) as po_number from  wo_po_details_master a, wo_po_break_down b where a.job_no= b.job_no_mst and a.job_no ='$data[2]' group by a.id,a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id");
		
	}
	else
	{
		$nameArray =sql_select("SELECT a.id,a.job_no as po_job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id,listagg(CAST(b.po_number AS VARCHAR(4000)),',') within group (order by b.po_number) as po_number from  wo_po_details_master a, wo_po_break_down b where a.job_no= b.job_no_mst and a.job_no ='$data[2]' group by a.id,a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.gmts_item_id");
		
	}
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_name")]."';\n";
		echo "document.getElementById('txt_style_ref').value 				= '".$row[csf("style_ref_no")]."';\n";
		echo "document.getElementById('txt_job_no').value					= '".$row[csf("po_job_no")]."';\n"; 
		echo "document.getElementById('txt_order_no').value					= '".$row[csf("po_number")]."';\n"; 
		echo "document.getElementById('cbo_gmt_item').value					= '".$garments_item[$row[csf("gmts_item_id")]]."';\n"; 
		
		//$gsd_id=return_field_value("id", "ppl_gsd_entry_mst", "company_id='".$row[csf("company_name")]."' and po_job_no='$data[2]' and status_active=1 and is_deleted=0");
		
		if( $data[0]!="" && $data[0]!=0 )
		{
			$nameArraysub =sql_select("SELECT id,working_hour,allowance,total_smv,sam_style,operation_count,pitch_time,man_power_1,man_power_2 from ppl_gsd_entry_mst where id='$data[0]'");
			foreach ($nameArraysub as $rowdata)
			{
				echo "document.getElementById('update_id').value					= '".$rowdata[csf("id")]."';\n"; //wo_po_id
				echo "document.getElementById('txt_working_hour').value				= '".$rowdata[csf("working_hour")]."';\n";
				echo "document.getElementById('txt_allowance').value				= '".$rowdata[csf("allowance")]."';\n";
				echo "document.getElementById('txt_sam_for_style').value			= '".$rowdata[csf("sam_style")]."';\n";
				echo "document.getElementById('txt_operation_count').value			= '".$rowdata[csf("operation_count")]."';\n";
				echo "document.getElementById('txt_pitch_time').value				= '".$rowdata[csf("pitch_time")]."';\n";
				echo "document.getElementById('txt_where_man_power').value			= '".$rowdata[csf("man_power_1")]."';\n";
				echo "document.getElementById('txt_where_man_power1').value			= '".$rowdata[csf("man_power_2")]."';\n";
			}
		}
	}*/
	
	if( $data!="" && $data!=0 )
	{
		$nameArraysub=sql_select("SELECT id,working_hour,allowance,total_smv,sam_style,operation_count,pitch_time,man_power_1,man_power_2 from ppl_gsd_entry_mst where id='$data'");
		foreach ($nameArraysub as $rowdata)
		{
			echo "document.getElementById('update_id').value					= '".$rowdata[csf("id")]."';\n"; //wo_po_id
			echo "document.getElementById('txt_working_hour').value				= '".$rowdata[csf("working_hour")]."';\n";
			echo "document.getElementById('txt_allowance').value				= '".$rowdata[csf("allowance")]."';\n";
			echo "document.getElementById('txt_sam_for_style').value			= '".$rowdata[csf("sam_style")]."';\n";
			echo "document.getElementById('txt_operation_count').value			= '".$rowdata[csf("operation_count")]."';\n";
			echo "document.getElementById('txt_pitch_time').value				= '".$rowdata[csf("pitch_time")]."';\n";
			echo "document.getElementById('txt_where_man_power').value			= '".$rowdata[csf("man_power_1")]."';\n";
			echo "document.getElementById('txt_where_man_power1').value			= '".$rowdata[csf("man_power_2")]."';\n";
		}
	}
	exit();	
}

if ($action=="operation_popup")
{
	echo load_html_head_contents("Popup Info", "../../", 1, 1,'',1,'');
	$data=explode('_',$data);
?>	
    <script>
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
	
		  function js_set_value(id)
		  { 
			  document.getElementById('operation_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
</head>
<body>
    <div style="width:100%" align="center">
        <input type="hidden" id="operation_id" />
    <div style="width:100%;">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="768" class="rpt_table">
            <thead>
                <th width="50">SL</th>
                <th width="220">Operation Name</th>
                <th width="150">Resource</th>
                <th width="100">Operator SMV</th>
                <th width="100">Helper SMV</th>
                <th>Total SMV</th>
            </thead>
        </table>
    </div>
    <div style="width:768px;max-height:300px; overflow-y:scroll" id="gsd_operator_list_view" align="left">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="tbl_list_search">
            <?php  
            $supplier_library_arr=return_library_array( "select id,supplier_name from lib_supplier", "id","supplier_name"  );
            $color_library_arr=return_library_array( "select id,color_name from lib_color", "id","color_name"  );
            $i=1;
            $sql_result=sql_select("select id,operation_name,resource_sewing,operator_smv,helper_smv,total_smv from lib_sewing_operation_entry where status_active=1 and is_deleted=0");
            foreach($sql_result as $row)
            {
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";
                ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]."_".$row[csf('operation_name')]."_".$row[csf('resource_sewing')]."_".$row[csf('operator_smv')]."_".$row[csf('helper_smv')]."_".$row[csf('total_smv')]; ?>');" > 
                    <td width="50" align="center"><?php echo $i; ?></td>
                    <td width="220" ><p><?php echo $row[csf('operation_name')]; ?></p></td>
                    <td width="150"><?php echo $production_resource[$row[csf('resource_sewing')]]; ?></td>
                    <td width="100" align="right"><?php echo $row[csf('operator_smv')]; ?></td>
                    <td width="100" align="right"><?php echo $row[csf('helper_smv')]; ?></td>
                    <td align="right"><?php echo $row[csf('total_smv')]; ?></td>
                </tr>
            <?php
                $i++;
            }
            ?>
        </table>
    </div>
    </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
	<?php	
	die;
}

if ($action=="load_php_dtls_form")
{
	
	$attach_id=return_library_array( "select id,attachment_name from lib_attachment",'id','attachment_name');
	//$gsdArray1=sql_select( "select listagg(gsd_dtls,',') within group (order by gsd_dtls) as gsd_dtls_id from pro_operation_bar_code");
	
	if($db_type==0)
	{
		$gsdArray=sql_select( "select group_concat(distinct(gsd_dtls)) as gsd_dtls_id from pro_operation_bar_code");
	}
	else
	{
		$gsdArray=sql_select( "select listagg(CAST(gsd_dtls as VARCHAR(4000)),',') within group (order by gsd_dtls) as gsd_dtls_id from pro_operation_bar_code");
	}
	//$gsdArray=implode(",",array_unique(explode(",",$gsdArray1[0][csf('gsd_dtls_id')])));
	//$gsdArray=explode(",",$gsdArray1[0][csf('gsd_dtls_id')]);
	
	
	//echo $gsdArray;die;
	
	
	$attach_gsd_dtls_id_array=explode(",",$gsdArray[0][csf('gsd_dtls_id')]);

	$sql_result =sql_select("SELECT id,mst_id,row_sequence_no,body_part_id,lib_sewing_id,resource_gsd,attachment_id,oparetion_type_id,operator_smv,helper_smv from ppl_gsd_entry_dtls where mst_id=$data");
				
	$k=1;
	$num_rows=count($sql_result);
	$operator_total=0;
	$helper_total=0;
	$total_total=0;
	?>
    <table id="gsd_tbl" width="100%" cellspacing="0" border="1" rules="all" class="rpt_table">
    <?php
	foreach ($sql_result as $row)
	{
		if(in_array($row[csf("id")],$attach_gsd_dtls_id_array))
		{
			$disable="disabled='disabled'";
			$not_delete_row=1;// 1 means can not remove row
		}
		else
		{
			$disable="";
			$not_delete_row=0;
		}
	 ?>
        <tr id="gsd_<?php echo $k; ?>">
            <td align="center">
                <input type="text" name="txt_seq_[]" id="txt_seq_<?php echo $k; ?>"  class="text_boxes" style="width:40px" value="<?php echo $row[csf("row_sequence_no")]; ?>" onBlur="duplication_check(<?php echo $k; ?>);" <?php echo $disable; ?>/>							 
            </td>
            <td align="center">
            	<input type="hidden" name="cbo_body_part_id_[]" class="text_boxes" id="cbo_body_part_id_<?php echo $k; ?>" value="<?php echo $row[csf("body_part_id")]; ?>" style="width:80px " />
                <input type="text" name="cbo_body_part_[]" id="cbo_body_part_<?php echo $k; ?>"  class="text_boxes" style="width:83px" value="<?php echo $body_part[$row[csf("body_part_id")]]; ?>" readonly <?php echo $disable; ?> />
                <input type="hidden" name="not_delete_row_[]" class="text_boxes" id="not_delete_row_<?php echo $k; ?>" value="<?php echo $not_delete_row; ?>" style="width:80px " />							
            </td>
            <td> 
				<?php
            		$operation_arr=return_library_array( "select id,operation_name from lib_sewing_operation_entry", "id","operation_name"  );
				?>
                <input type="hidden" name="sewing_id_[]" class="text_boxes" id="sewing_id_<?php echo $k; ?>" value="<?php echo $row[csf("lib_sewing_id")]; ?>" style="width:40px;" />
                <input type="hidden" name="update_id_dtls_[]" class="text_boxes" id="update_id_dtls_<?php echo $k; ?>" value="<?php echo $row[csf("id")]; ?>"  style="width:40px;" />
                <input type="text" name="txt_operation_[]" id="txt_operation_<?php echo $k; ?>"  class="text_boxes" style="width:100px" value="<?php echo $operation_arr[$row[csf("lib_sewing_id")]]; ?>" readonly <?php echo $disable; ?> />
                <input type="hidden" name="operation_id_[]" class="text_boxes" id="operation_id_<?php echo $k; ?>" value="<?php echo $row[csf("lib_sewing_id")]; ?>" style="width:70px;" />						
            </td>
            <td> 
                <input type="text" name="txt_resource_[]" id="txt_resource_<?php echo $k; ?>"  class="text_boxes" style="width:78px" value="<?php echo $production_resource[$row[csf("resource_gsd")]]; ?>" readonly <?php echo $disable; ?> />	
                <input type="hidden" name="txt_resource_id_[]" class="text_boxes" id="txt_resource_id_<?php echo $k; ?>" value="<?php echo $row[csf("resource_gsd")]; ?>" style="width:70px;" readonly/>						 
            </td>
            <td> 
                <input type="text" name="txt_attachment_[]" id="txt_attachment_<?php echo $k; ?>"  class="text_boxes" style="width:73px" value="<?php echo $attach_id[$row[csf("attachment_id")]]; ?>" readonly <?php echo $disable; ?> />
                 <input type="hidden" name="txt_attachment_id_[]" id="txt_attachment_id_<?php echo $k; ?>"  value="<?php echo $row[csf("attachment_id")]; ?>"/>						 			</td>
            <td> 
			<?php
            	$operator_smv_arr=return_library_array( "select id,operator_smv from lib_sewing_operation_entry", "id","operator_smv"  );
			?>
                <input type="text" name="txt_operator_[]" id="txt_operator_<?php echo $k; ?>"  class="text_boxes_numeric" style="width:65px" value="<?php echo $row[csf("operator_smv")]; ?>" readonly <?php echo $disable; ?> />							 
            </td>
            <td> 
			<?php  	 
            	$helper_smv_arr=return_library_array( "select id,helper_smv from lib_sewing_operation_entry", "id","helper_smv"  );
			?>
                <input type="text" name="txt_helper_[]" id="txt_helper_<?php echo $k; ?>"  class="text_boxes_numeric" style="width:65px" value="<?php echo $row[csf("helper_smv")]; ?>" readonly <?php echo $disable; ?> />							 
            </td>
            <td> 
			<?php
            	$total_smv_arr=return_library_array( "select id,total_smv from lib_sewing_operation_entry", "id","total_smv"  );
			?>
                <input type="text" name="txt_total_[]" id="txt_total_<?php echo $k; ?>"  class="text_boxes_numeric" style="width:70px" value="<?php echo $row[csf("helper_smv")]+$row[csf("operator_smv")]; ?>" readonly <?php echo $disable; ?> />							 
            </td>
            <td> 
            <?php
				$operation_type=array(1=>"Body Part Starting",2=>"Body Part Ending",3=>"Gmt Last Operation");
			?>
            	<input type="hidden"  name="cbo_operation_type_id_[]" class="text_boxes" id="cbo_operation_type_id_<?php echo $k; ?>" value="<?php echo $row[csf("oparetion_type_id")]; ?>" style="width:80px " />
                <input type="text" name="cbo_operation_type_[]" id="cbo_operation_type_<?php echo $k; ?>"  class="text_boxes" style="width:90px" value="<?php echo $operation_type[$row[csf("oparetion_type_id")]]; ?>" readonly <?php echo $disable; ?> />							 
            </td>
            <td> 
                <input type="text" name="txt_remove[]" id="txt_remove<?php echo $k; ?>"  class="formbutton" onClick="remove_row( <?php echo $k; ?>)" style="width:50px" value="Remove" readonly />							 
            </td>
         </tr>
    <?php 
	$k++;
	}
	?>
    </table>
    <?php
	die;
}

if ($action=="attachment_popup")
{
	echo load_html_head_contents("Popup Info", "../../", 1, 1,'',1,'');
?>	
    <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('attachment_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
    <input type="hidden" id="attachment_id" />
    <?php
		$sql="SELECT id,attachment_name from  lib_attachment"; 
		
		echo  create_list_view("list_view", "Attachment Name", "350","390","350",0, $sql , "js_set_value", "id,attachment_name", "", 1, "", 0 , "attachment_name", "gsd_entry_controller",'setFilterGrid("list_view",-1);','0') ;
		 die; 
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$fraction_count=25;   // Comes from Variable Settings..
	
	if ( $operation==0 )   // Insert Here========================================================================================delivery_id
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if(str_replace("'",'',$update_id)==0)
		{		
			$id=return_next_id( "id", "ppl_gsd_entry_mst", 1 ) ; 	
		}
		
		/*if(str_replace("'",'',$update_id)==0)
		{		
			$id=return_next_id( "id", "ppl_gsd_entry_mst", 1 ) ; 	
			$field_array="id,company_id,po_dtls_id,po_job_no,po_break_down_id,working_hour,total_smv,allowance,sam_style,operation_count,pitch_time,man_power_1,man_power_2,per_hour_gmt_target,inserted_by,insert_date,status_active,is_deleted";
			$data_array="(".$id.",".$cbo_company_id.",".$wo_po_id.",".$job_no.",".$ord_id.",".$txt_working_hour.",".$txt_total_tot.",".$txt_allowance.",".$txt_sam_for_style.",".$txt_operation_count.",".$txt_pitch_time.",".$txt_where_man_power.",".$txt_where_man_power1.",1,".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1,0)"; 
			$rID=sql_insert("ppl_gsd_entry_mst",$field_array,$data_array,0);
		}
		else
		{
			$field_array="company_id*po_dtls_id*po_break_down_id*working_hour*total_smv*allowance*sam_style*operation_count*pitch_time*man_power_1*man_power_2*per_hour_gmt_target*updated_by*update_date";
			$data_array="".$cbo_company_id."*".$wo_po_id."*".$ord_id."*".$txt_working_hour."*".$txt_total_tot."*".$txt_allowance."*".$txt_sam_for_style."*".$txt_operation_count."*".$txt_pitch_time."*".$txt_where_man_power."*".$txt_where_man_power1."*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			$rID=sql_update("ppl_gsd_entry_mst",$field_array,$data_array,"id",$update_id,0);
			$id=str_replace("'",'',$update_id);
			if( $id!="" ) $d=execute_query("delete from ppl_gsd_entry_dtls where mst_id=$id",1);
			
		}*/
		
		$id1=return_next_id( "id","ppl_gsd_entry_dtls",1);
		$field_array1 ="id,mst_id,row_sequence_no,	resource_gsd,body_part_id,lib_sewing_id,attachment_id,oparetion_type_id,total_smv,no_of_worker_calculative,no_of_worker_rounding,target_per_hour_operation,target_per_day_operation,operation_id,operator_smv,helper_smv";
		
		$add_comma=0;
		   
		$data_array1="";
		for($i=1; $i<=$num_row; $i++)
		{
			$seq_no="txt_seq_".$i;
			$body_part="cbo_body_part_id_".$i;
			$sewing_id="sewing_id_".$i;
			$resource_id="txt_resource_id_".$i;
			$attachment_id="txt_attachment_id_".$i;
			$operation_type_id="cbo_operation_type_id_".$i;
			
			$operation_id="operation_id_".$i;
			$operator_smv="txt_operator_".$i;
			$helper_smv="txt_helper_".$i;
			$total_smv="txt_total_".$i;
			 
			$no_of_worker_calculative=str_replace("'","",$$total_smv)/str_replace("'","",$txt_pitch_time);
			$no_of_worker_rounding=get_total_worker($fraction_count,$no_of_worker_calculative);
			$target_per_hr_operation=round(60/str_replace("'","",$$total_smv));
			$target_per_day_operation=str_replace("'","",$txt_working_hour)*$target_per_hr_operation;
			
			$updateid_dtls="update_id_dtls_".$i;
			
			if ($add_comma!=0) $data_array1 .=",";
			$data_array1 .="(".$id1.",".$id.",".$$seq_no.",".$$resource_id.",".$$body_part.",".$$sewing_id.",".$$attachment_id.",".$$operation_type_id.",".$$total_smv.",'".$no_of_worker_calculative."',".$no_of_worker_rounding.",".$target_per_hr_operation.",".$target_per_day_operation.",".$$operation_id.",".$$operator_smv.",".$$helper_smv.")";
			$id1=$id1+1;
			$add_comma++;
		}
		
		if(str_replace("'",'',$update_id)==0)
		{		
			//$id=return_next_id( "id", "ppl_gsd_entry_mst", 1 ) ; 	
			$field_array="id,company_id,po_dtls_id,po_job_no,po_break_down_id,gmts_item_id,working_hour,total_smv,allowance,sam_style,operation_count,pitch_time,man_power_1,man_power_2,per_hour_gmt_target,inserted_by,insert_date,status_active,is_deleted";
			$data_array="(".$id.",".$cbo_company_id.",".$wo_po_id.",".$job_no.",".$ord_id.",".$cbo_gmt_item.",".$txt_working_hour.",".$txt_total_tot.",".$txt_allowance.",".$txt_sam_for_style.",".$txt_operation_count.",".$txt_pitch_time.",".$txt_where_man_power.",".$txt_where_man_power1.",1,".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1,0)"; 
			
			$rID=sql_insert("ppl_gsd_entry_mst",$field_array,$data_array,1); 
		}
		else
		{
			$field_array="company_id*po_dtls_id*po_break_down_id*gmts_item_id*working_hour*total_smv*allowance*sam_style*operation_count*pitch_time*man_power_1*man_power_2*per_hour_gmt_target*updated_by*update_date";
			$data_array="".$cbo_company_id."*".$wo_po_id."*".$ord_id."*".$cbo_gmt_item."*".$txt_working_hour."*".$txt_total_tot."*".$txt_allowance."*".$txt_sam_for_style."*".$txt_operation_count."*".$txt_pitch_time."*".$txt_where_man_power."*".$txt_where_man_power1."*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			$rID=sql_update("ppl_gsd_entry_mst",$field_array,$data_array,"id",$update_id,1);
			$id=str_replace("'",'',$update_id);
			if( $id!="" ) $d=execute_query("delete from ppl_gsd_entry_dtls where mst_id=$id",1);
			
		}
			
		if($data_array1!="")
		{
			//echo "INSERT INTO ppl_gsd_entry_dtls (".$field_array1.") VALUES ".$data_array1; die;
			
			$rID1=sql_insert("ppl_gsd_entry_dtls",$field_array1,$data_array1,1);
		}

		if($db_type==0)
		{
			if( $rID && $rID1)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID || $rID1)
			{
				oci_commit($con);    
				echo "0**".str_replace("'",'',$id);
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here=============================================================================
	{
		
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
			//$id=str_replace("'",'',$update_id);
		$field_array="company_id*po_dtls_id*po_break_down_id*gmts_item_id*working_hour*total_smv*allowance*sam_style*operation_count*pitch_time*man_power_1*man_power_2*per_hour_gmt_target*updated_by*update_date";
		$data_array="".$cbo_company_id."*".$wo_po_id."*".$ord_id."*".$cbo_gmt_item."*".$txt_working_hour."*".$txt_total_tot."*".$txt_allowance."*".$txt_sam_for_style."*".$txt_operation_count."*".$txt_pitch_time."*".$txt_where_man_power."*".$txt_where_man_power1."*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		/*$rID=sql_update("ppl_gsd_entry_mst",$field_array,$data_array,"id",$update_id,0);
		//$d=execute_query("delete from ppl_gsd_entry_dtls where mst_id=$update_id");
		if($rID) $flag=1; else $flag=0; 
		
		$deleted_id=str_replace("'",'',$deleted_id);
		if($deleted_id!="")
		{
			$delete=execute_query("delete from ppl_gsd_entry_dtls where mst_id=$update_id and id in($deleted_id)",1);
			if($flag==1) 
			{
				if($delete) $flag=1; else $flag=0; 
			} 
		}*/
		
		$id1=return_next_id( "id","ppl_gsd_entry_dtls",1);
		$field_array1 ="id,mst_id,row_sequence_no,resource_gsd,body_part_id,lib_sewing_id,attachment_id,oparetion_type_id,total_smv,no_of_worker_calculative,no_of_worker_rounding,target_per_hour_operation,target_per_day_operation,operation_id,operator_smv,helper_smv";
		
		$field_array_update ="row_sequence_no*resource_gsd*body_part_id*lib_sewing_id*attachment_id*oparetion_type_id*total_smv*no_of_worker_calculative*no_of_worker_rounding*target_per_hour_operation*target_per_day_operation*operation_id*operator_smv*helper_smv";
		
		$data_array1="";
		$add_comma=0;
		for($i=1; $i<=$num_row; $i++)
		{
			$seq_no="txt_seq_".$i;
			$body_part="cbo_body_part_id_".$i;
			$sewing_id="sewing_id_".$i;
			$resource_id="txt_resource_id_".$i;
			$attachment_id="txt_attachment_id_".$i;
			$operation_type_id="cbo_operation_type_id_".$i;
			
			$operation_id="operation_id_".$i;
			$operator_smv="txt_operator_".$i;
			$helper_smv="txt_helper_".$i;
			$total_smv="txt_total_".$i;
			 
			$no_of_worker_calculative=str_replace("'","",$$total_smv)/str_replace("'","",$txt_pitch_time);
			$no_of_worker_rounding=get_total_worker($fraction_count,$no_of_worker_calculative);
			$target_per_hr_operation=round(60/str_replace("'","",$$total_smv));
			$target_per_day_operation=str_replace("'","",$txt_working_hour)*$target_per_hr_operation;
			
			$updateid_dtls="update_id_dtls_".$i;
			if(str_replace("'",'',$$updateid_dtls)!="")
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_update[str_replace("'",'',$$updateid_dtls)] = explode(",",($$seq_no.",".$$resource_id.",".$$body_part.",".$$sewing_id.",".$$attachment_id.",".$$operation_type_id.",".$$total_smv.",'".$no_of_worker_calculative."',".$no_of_worker_rounding.",".$target_per_hr_operation.",".$target_per_day_operation.",".$$operation_id.",".$$operator_smv.",".$$helper_smv));
			}
			else
			{
				if ($add_comma!=0) $data_array1 .=",";
				$data_array1 .="(".$id1.",".$update_id.",".$$seq_no.",".$$resource_id.",".$$body_part.",".$$sewing_id.",".$$attachment_id.",".$$operation_type_id.",".$$total_smv.",'".$no_of_worker_calculative."',".$no_of_worker_rounding.",".$target_per_hr_operation.",".$target_per_day_operation.",".$$operation_id.",".$$operator_smv.",".$$helper_smv.")";
				$id1=$id1+1;
				$add_comma++;
			}
		}
		
		$rID=sql_update("ppl_gsd_entry_mst",$field_array,$data_array,"id",$update_id,1);
		
		if($rID) $flag=1; else $flag=0; 
		
		$deleted_id=str_replace("'",'',$deleted_id);
		
		
		if($deleted_id!="")
		{
			$delete=execute_query("delete from ppl_gsd_entry_dtls where mst_id=$update_id and id in($deleted_id)",1);
			if($flag==1) 
			{
				if($delete) $flag=1; else $flag=0; 
			} 
		}
		
		//echo "INSERT INTO ppl_gsd_entry_dtls (".$field_array1.") VALUES ".$data_array1; die;
		
		//echo "sajjad23_".$field_array_update.'*****';
		//print_r($data_array_update);die;
		
		
		if($data_array1!="")
		{
			$rID2=sql_insert("ppl_gsd_entry_dtls",$field_array1,$data_array1,1);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		//echo "INSERT INTO ppl_gsd_entry_dtls (".$field_array_update.") VALUES ".$data_array_update; die;
	//print_r($id_arr);die;
		
		if($data_array_update!="")
		{
			$rID3=execute_query(bulk_update_sql_statement( "ppl_gsd_entry_dtls", "id", $field_array_update, $data_array_update, $id_arr ));//echo "sajjad2_".$rID3;die;
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			}  
		}

		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$update_id);
			}
			
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$update_id);
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "1**".str_replace("'",'',$update_id);
			}
			
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$update_id);
			}
		}
		
		disconnect($con);
		die;
	}
}
function get_total_worker($fraction_count,$no_of_worker)
{
	if ($no_of_worker<1) return 1;
	else
	{
		$no_of_worker2 = number_format($no_of_worker,2,'.','');
		
		$no_of_worker=explode(".",$no_of_worker2);
		
		if ($no_of_worker[1]<$fraction_count) return $no_of_worker[0];
		else return $no_of_worker[0]+1;
	}
	die;
}

if ($action=="print_gsd_report")
{
	$data=explode("*",$data);
	
	mysql_query("SET CHARACTER SET utf8");
	mysql_query("SET SESSION collation_connection ='utf8_general_ci'"); 	
 	$lib_country=return_library_array( "select id,country_name from lib_country","id", "country_name"  );
	$lib_buyer=return_library_array( "select id,buyer_name from lib_buyer","id", "buyer_name"  );
	$lib_attachment=return_library_array( "select id,attachment_name from lib_attachment","id", "attachment_name"  );
	
	$lib_sewing_operation=return_library_array( "select id,operation_name from lib_sewing_operation_entry","id", "operation_name"  );

	ob_start();
?>		
	<div style="width:1250px" align="center">
<?php 
		$row_data=sql_select("select id,country_id,company_name,plot_no,level_no,road_no,block_no,city,zip_code,province,email,website from lib_company where id='$data[0]' order by id");
			foreach($row_data as $row_com)
			{
	?>
    		<table width="100%" cellpadding="0" cellspacing="0" style="border:1px solid black">
            <tr>
                <td align="center" style="font-size:20px">
					<?php echo $row_com[csf("company_name")];	?>
                </td>
            </tr>
            <tr>
                <td align="center" style="font-size:10px">
                 	Plot No: <?php echo $row_com[csf("plot_no")]; ?> Level No: <?php echo $row_com[csf("level_no")]?> Road No: <?php echo $row_com[csf("road_no")]; ?> Block No: <?php echo $row_com[csf("block_no")];?> City No: <?php echo $row_com[csf("city")];?> Zip Code: <?php echo $row_com[csf("zip_code")]; ?> Province No: <?php echo $row_com[csf("province")];?> Country: <?php echo $lib_country[$row_com[csf("country_id")]]; ?><br> Email Address: <?php echo $row_com[csf("email")];?> Website No: <?php echo $row_com[csf("website")];?>
                </td>  
            </tr>
            <tr><td align="center"><b>Operation Bulletin</b></td></tr>
        </table>
        <?php  }  ?>
        <style type="text/css">
            table.display { border-collapse: collapse; }
            table.display td { padding: .3em; border: 1px black solid; }                	                   	
        </style>
		<?php
			
			//"select a.buyer_name,a.style_ref_no,a.style_description,b.image_location, group_concat(distinct(po_number)) as order_number from  wo_po_details_master a, common_photo_library b, wo_po_break_down c where a.job_no=b.master_tble_id and a.job_no ='$data[1]' and b.pic_size=0 and a.job_no=c.job_no_mst group by a.job_no"
			$image_name_array=return_library_array( "select master_tble_id,image_location from  common_photo_library", "master_tble_id", "image_location"  );
			
			if($db_type==0)
			{
				$row_data=sql_select("select a.buyer_name,a.style_ref_no,a.style_description, group_concat(distinct(po_number)) as order_number,a.job_no from  wo_po_details_master a,  wo_po_break_down c where a.job_no ='$data[1]' and a.job_no=c.job_no_mst group by a.job_no");
			}
			else
			{
				$row_data=sql_select("select a.buyer_name,a.style_ref_no,a.style_description, listagg(CAST(po_number as VARCHAR(4000)),',') within group (order by po_number) as order_number,a.job_no from  wo_po_details_master a,  wo_po_break_down c where a.job_no ='$data[1]' and a.job_no=c.job_no_mst group by a.job_no,a.buyer_name,a.style_ref_no,a.style_description");
			}
			
			foreach($row_data as $row_wo)
			{
				$order_number=implode(",",array_unique(explode(",",$row_wo[csf("order_number")])));
			?>
			<table width="100%" style="border:1px solid black" class="display">
				<tr>
					<td width="100" style="font-size:12px"><b>Buyer Name</b></td>
					<td width="200"><?php echo $lib_buyer[$row_wo[csf("buyer_name")]];?></td>
					<td width="100" style="font-size:12px"><b>Style</b></td>
					<td width="200"><?php echo $row_wo[csf('style_ref_no')];?></td>
					<td rowspan="4" align="center"><img src="../<?php echo $image_name_array[$row_wo[csf('job_no')]]; ?>" width="150" height="70"; border="2" /></td>
				</tr>
				<tr>
					<td  style="font-size:12px"><b>Style Details</b></td>
					<td >
						<?php
						echo $row_wo[csf('style_description')];
						?>
					</td>
					<td style="font-size:12px"><b>Style Details</b></td>
					<td >
						<?php
						echo $row_wo[csf('style_description')];
						?>
					</td>
				</tr>
				<tr>
					<td  style="font-size:12px"><b>Order</b></td>
					<td colspan="3" ><?php echo split_string($order_number,50); ?></td>
				</tr>
				<tr>
					<td  style="font-size:12px"><b>Prepared By</b></td>
					<td style="font-size:12px" align="center"></td>
					<td style="font-size:12px"><b>IE</b></td>
					<td style="font-size:12px" align="center"></td>
				</tr>
			</table>
			<?php
        	} // End of Work Order Mast
        // Start of Item Description
       ?>
	   <style type="text/css">
            table.display { border-collapse: collapse; }
            table.display td { padding: .3em; border: 1px black solid; }                	                   	
        </style>
        <?php
				$sam2="";
				$total_no_of_worker_real2="";
				$working_hour2="";
				$tar_per="";
				$tar_per2="";
				$tar_per3="";
				
				if($db_type==0)
				{
					$sql_data=sql_select("select sum(no_of_worker_rounding) as no_of_worker_rounding, a.sam_style, a.working_hour from ppl_gsd_entry_mst a, ppl_gsd_entry_dtls b where a.id=b.mst_id and a.po_job_no ='$data[1]' and a.is_deleted=0 group by a.po_job_no ");
				}
				else
				{
					$sql_data=sql_select("select sum(no_of_worker_rounding) as no_of_worker_rounding, a.sam_style, a.working_hour from ppl_gsd_entry_mst a, ppl_gsd_entry_dtls b where a.id=b.mst_id and a.po_job_no ='$data[1]' and a.is_deleted=0 group by a.po_job_no,a.sam_style, a.working_hour");
				
				}
				
                foreach($sql_data as $tar_day )
                {
					$sam2=$tar_day[csf("sam_style")]; 
					$total_no_of_worker_real2=$tar_day[csf("no_of_worker_rounding")];
					$working_hour2=$tar_day[csf("working_hour")]; 
				}
				
				$tar_per=(60/$sam2)*$working_hour2*$total_no_of_worker_real2;
				$tar_per2=(60/$sam2)*$working_hour2*$txt_man_power2;
				$tar_per3=(60/$sam2)*$working_hour2*$txt_man_power3;
		?>
        <table width="100%" class="display">
            <tr>
                <td rowspan="2" width="40" align="center">Seq No</td>
                <td rowspan="2" width="" align="center">Process Name</td>
                <td rowspan="2" width="100" align="center">Resource/ MC Type</td>
                <td rowspan="2" width="100" align="center">Attachment</td>
                
                <td rowspan="2" width="60" align="center">Operator's SMV</td>
                <td rowspan="2" width="60" align="center">Helper'S SMV</td>
                <td rowspan="2" width="30" align="center">Total SMV</td>
                 <td rowspan="2" width="60" align="center">Target/Hr</td>
                <td rowspan="2" width="60" align="center">No of Worker</td>
                <td rowspan="2" width="60" align="center">No of Worker(Real)</td>
               
                <td colspan="3" width="180" align="center">Target/Day</td>
            </tr>
            <tr>
                <td width="60" align="center"><?php echo $total_no_of_worker_real2;?></td>
                <td width="60" align="center"><?php echo $txt_man_power2;?></td>
                <td width="60" align="center"><?php echo $txt_man_power3;?></td>
            </tr>
			<?php
			 
				$i=0;
				$counter="";
				$total_smv="";
				$total_operator_smv="";
				$total_helper_smv="";
				$total_no_of_worker_cal="";
				$total_no_of_worker_real="";
				$allowance="";
				$sam="";
				$working_hour="";
				$pitch_time="";
				$all_machine="";
				$new_category=array();
				  
				 $sql_ord=sql_select("select mst_id,row_sequence_no,body_part_id,lib_sewing_id,resource_gsd,attachment_id,oparetion_type_id,b.total_smv as b_total_smv,no_of_worker_calculative,no_of_worker_rounding,target_per_hour_operation,target_per_day_operation,operation_id,operator_smv,helper_smv,a.allowance,a.sam_style,a.working_hour,a.pitch_time,man_power_1,man_power_2 from ppl_gsd_entry_mst a, ppl_gsd_entry_dtls b where a.id=b.mst_id and a.po_job_no ='$data[1]' and a.is_deleted=0 order by b.row_sequence_no, body_part_id asc");
				$counter=count($sql_ord); 
                foreach($sql_ord as $row_ord2 )
                {
                 	if(!in_array($row_ord2[csf("body_part_id")],$new_category))
					{
						 
						$new_category[]=$row_ord2[csf("body_part_id")];
						?>
                        <tr>
                        	<td colspan="9" height="10" style="padding-left:30px" bgcolor="#CCCCCC"><strong><?php echo $body_part[$row_ord2[csf("body_part_id")]]; ?></strong></td>
                        </tr>
                        <?php
					}
					$i++;
            ?>
            <tr bgcolor="<?php echo $bgcolor; ?>">
                <td width="20"><?php echo $row_ord2[csf("row_sequence_no")]; ?></td>
                <td width="">
					<?php echo $lib_sewing_operation[$row_ord2[csf("operation_id")]]; ?>
                </td>
                <td width="100"><?php 
				 
					echo $production_resource[$row_ord2[csf("resource_gsd")]];
					$machine_count[$row_ord2[csf("resource_gsd")]]=$machine_count[$row_ord2[csf("resource_gsd")]]+$row_ord2[csf("no_of_worker_rounding")];
				?></td> 
                <td width="30" align="center"><?php echo $lib_attachment[$row_ord2[csf("attachment_id")]];?></td>	
                
                <td width="60" align="right"><?php echo $row_ord2[csf("operator_smv")];?></td>
                <td width="60" align="right"><?php echo $row_ord2[csf("helper_smv")];?></td>
                <td width="30" align="right"><?php echo $row_ord2[csf("b_total_smv")];?></td>
                 <td width="60" align="right"><?php echo $row_ord2[csf("target_per_hour_operation")];?></td>
                <td width="60" align="right"><?php echo $row_ord2[csf("no_of_worker_calculative")];?></td>
                <td width="60" align="right"><?php echo $row_ord2[csf("no_of_worker_rounding")];?></td>
               
					<?php
                    if($i==1)
                    {
						$count_header=return_field_value("count(distinct(body_part_id)) as body_part_id","ppl_gsd_entry_dtls"," mst_id=".$row_ord2[csf("mst_id")]." group by mst_id ","body_part_id");
						
						$counter=$count_header+$counter;
                    ?>
                        <td rowspan="<?php echo $counter;?>" width="60" align="right" bgcolor="#FFFFFF"><?php echo round($tar_per); ?></td>
                        <td rowspan="<?php echo $counter;?>" width="60" align="right" bgcolor="#FFFFFF"><?php echo $row_ord2[csf("man_power_1")]; ?></td>
                        <td rowspan="<?php echo $counter;?>" width="60" align="right" bgcolor="#FFFFFF"><?php echo $row_ord2[csf("man_power_2")]; ?></td>
                    <?php
                    }
					//echo $counter;
					?>
               </tr>
               	<?php
               		$total_smv=$total_smv+$row_ord2[csf("b_total_smv")];
					$total_operator_smv=$total_operator_smv+$row_ord2[csf("operator_smv")];
					$total_helper_smv=$total_helper_smv+$row_ord2[csf("helper_smv")];
					$total_no_of_worker_cal=$total_no_of_worker_cal+$row_ord2[csf("no_of_worker_calculative")];
					$total_no_of_worker_real=$total_no_of_worker_real+$row_ord2[csf("no_of_worker_rounding")];
					$allowance=$row_ord2[csf("allowance")]; 
					$sam=$row_ord2[csf("sam_style")];
					$working_hour=$row_ord2[csf("working_hour")];
					$pitch_time=$row_ord2[csf("pitch_time")];
					//$all_machine=$machine_count[1]+$machine_count[5]+$machine_count[9]+$machine_count[13];
					$all_man_machine=$machine_count[$row_ord2[csf("resource_gsd")]];
                 } 
                ?>
               <tr>
                    <td colspan="4" align="right">Total</td>
                    <td width="30" align="right"><?php echo $total_smv;?></td>
                    <td width="60" align="right"><?php echo $total_operator_smv;?></td>
                    <td width="60" align="right"><?php echo $total_helper_smv;?></td>
                    <td width="60" align="right"><?php echo $total_no_of_worker_cal;?></td>
                    <td width="60" align="right"><?php echo $total_no_of_worker_real;?></td>
                </tr>
               <tr>
                    <td colspan="4" align="right">Allowance(%)</td>
                    <td width="30" align="right"><?php echo $allowance;?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
               <tr>
                    <td height="25" colspan="4" align="right">SAM</td>
                    <td width="30" align="right"><?php echo $sam;?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
             </table>
             
             <table>
               	<tr style="border-left:hidden; border-right:hidden"><td colspan="12">&nbsp;</td></tr>
               <?php
			   	$sum="";
					foreach (array_keys($machine_count) as $key=>$values)
						{
							if ($values<40)
							{
								$sum=$sum+$machine_count[$values];
							}
						}
				?>
          		<tr style="border:hidden">
                    <td colspan="4">
                        <table class="display">
                            <tr><td colspan="2" width="210" align="center">WORKERS SUMMARY</td><td width="120" align="center">PITCH TIME</td></tr>
                            <tr ><td width="160">Total Machine Operators</td><td width="50" align="center"><?php echo $sum;?></td><td width="90" rowspan="4" align="center" bgcolor="#FFFFFF"><?php echo $pitch_time;?></td></tr>
                            <tr ><td width="160">Total Helpers</td><td width="50" align="center"><?php echo $machine_count[40]; ?></td></tr>
                            <tr ><td width="160">Total QI</td><td width="50" align="center"><?php echo $machine_count[41]; ?></td></tr>
                            <tr><td width="160">Total Man Power</td><td width="50" align="center"><?php echo $total_no_of_worker_real;?></td></tr>
                        </table>
                    </td>
                    <td style="border-left:hidden; border-right:hidden">&nbsp;</td>
                    <td colspan="3">
                        <table class="display">
                            <tr><td colspan="2" width="220" align="center">TARGET SUMMARY</td></tr>
                            <tr><td width="170"><font size="-1">SAM</font></td><td width="50" align="right"><?php echo $sam;?></td></tr>
                            <tr><td width="170">Total Working Hour</td><td width="50" align="right"><?php echo $working_hour; ?></td></tr>
                            <tr><td width="170">Target Per Hour</td><td align="right"><?php $target_per_hr=(60/$sam)*$total_no_of_worker_real; echo round($target_per_hr); ?></td></tr>
                            <tr><td width="170">Target Per Day</td><td align="right"><?php $target_per_day=($working_hour*$target_per_hr); echo round($target_per_day); ?></td></tr>
                        </table>
                    </td>
                    <td style="border-left:hidden; border-right:hidden">&nbsp;</td>
                    <td colspan="3">
                        <table class="display">
                            <tr><td colspan="3" width="240" align="center">MACHINE SUMMARY</td></tr>
                        <?php  $i=0;
                            $bb="";
                            $cc="";
                            $aa="";
                             $machine_total=0;
                            foreach (array_keys($machine_count) as $key=>$values)
                            { 
                                if ($values<40 && $production_resource[$values]!="")
                                {	
                                    $machine_total=$machine_total+$machine_count[$values];
                                    if ($aa=="") $aa = $production_resource[$values]; else $aa=$aa.",".$production_resource[$values];
                                }
                            }
                            $bb=explode(',',$aa);
                            $cc=count($bb);
                            
                            
                            foreach (array_keys($machine_count) as $key=>$values)
                            {
                                if ($values<40 && $production_resource[$values]!="")
                                {
                                    $sum_td='';
                                    if ($i==0) $sum_td='<td rowspan="'.$cc.'">'.$machine_total.'</td>';
                                    $i++;
                                    echo '<tr><td width="120">'.$production_resource[$values].'</td><td width="50" align="center">'.$machine_count[$values].'</td>'.$sum_td.'</tr>';
                                }
                            }
                            ?> 
                      </table>
                    </td>
            	</tr>
             </table>
    </div>
<?php	   
   $html = ob_get_contents();
   ob_clean();
   //previous file delete code-----------------------------//
	foreach (glob(""."*.pdf") as $filename) 
	{			
		@unlink($filename);
	}
	echo "$html"."####".$name;
	exit();
}



?>