﻿<?
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
 
if($db_type==0) $select_field="group"; 
else if($db_type==2) $select_field="wm";
else $select_field="";//defined Later


$lcscRes=sql_select("select id, export_lc_no, currency_name, 0 as pay_term from com_export_lc");
 
foreach($lcscRes as $row)
{
	$lcscRes_arr[$row[csf('id')]]['export_lc_no']=$row[csf('export_lc_no')];
	$lcscRes_arr[$row[csf('id')]]['currency_name']=$row[csf('currency_name')];
	$lcscRes_arr[$row[csf('id')]]['pay_term']=$row[csf('pay_term')];
}

 $ScRes=sql_select("select id, contract_no, currency_name,pay_term from com_sales_contract");
 
 foreach($ScRes as $row)
 {
	 $ScRes_arr[$row[csf('id')]]['contract_no']=$row[csf('contract_no')];
	 $ScRes_arr[$row[csf('id')]]['currency_name']=$row[csf('currency_name')];
	 $ScRes_arr[$row[csf('id')]]['pay_term']=$row[csf('pay_term')];
 }
 
//--------------------------- Start-------------------------------------//

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  
	
}


if($action=="populate_acc_loan_no_data")
{
	$data=explode("**",$data);
	$acc_type=$data[0];
	$rowID=$data[1];
	
	$sql="select account_no from lib_bank_account where account_type=$acc_type";
	$nameArray=sql_select($sql);
	echo "$('#txt_ac_loan_no_".$rowID."').removeAttr('readonly');\n";
 	echo "$('#txt_ac_loan_no_".$rowID."').val('');\n";
	foreach($nameArray as $row)
	{
		echo "$('#txt_ac_loan_no_".$rowID."').attr('readonly','readonly');\n";
		echo "$('#txt_ac_loan_no_".$rowID."').val('".$row[csf("account_no")]."');\n";
	}
	exit();
}


if($action=="transaction_add_row")
{
	$rowNo = $data+1;
	echo '<tr id="tr'.$rowNo.'">
			<td>'.create_drop_down( "cbo_account_head_".$rowNo, 200, $commercial_head,"", 1, "-- Select --", $selected, "get_php_form_data(this.value+'**'+".$rowNo.", 'populate_acc_loan_no_data', 'requires/export_doc_sub_entry_controller' );",0,"" ).'</td>							
			<td><input type="text" id="txt_ac_loan_no_'.$rowNo.'" name="txt_ac_loan_no[]" class="text_boxes" style="width:100px" /></td>
			<td><input type="text" id="txt_domestic_curr_'.$rowNo.'" name="txt_domestic_curr[]" class="text_boxes_numeric" style="width:100px" onkeyup="fn_calculate(this.id,'.$rowNo.')" /></td>
			<td><input type="text" id="txt_conversion_rate_'.$rowNo.'" name="txt_conversion_rate[]" class="text_boxes_numeric" style="width:100px" onkeyup="fn_calculate(this.id,'.$rowNo.')" /></td>
			<td><input type="text" id="txt_lcsc_currency_'.$rowNo.'" name="txt_lcsc_currency[]" class="text_boxes_numeric" style="width:100px" onkeyup="fn_calculate(this.id,'.$rowNo.')" /></td>
			<td>
			<input type="button" id="increaserow_'.$rowNo.'" style="width:30px" class="formbuttonplasminus" value="+" onClick="javascript:fn_inc_decr_row('.$rowNo.',\'increase\');" /><input type="button" id="decreaserow_'.$rowNo.'" style="width:30px" class="formbuttonplasminus" value="-" onClick="javascript:fn_inc_decr_row('.$rowNo.',\'decrease\');" />
			</td> 
		</tr>'; 
	 
	exit();
}




if($action=="lcSc_popup_search")
{
	echo load_html_head_contents("Export Information Entry Form", "../../../", 1, 1,'','1','');
	extract($_REQUEST);
	//echo $dtls_sub_ref_id;die;
	?>
     
	<script>
	
	
	function onClosed()
	{
		
	}
	
	function set_all()
	{
		var old=document.getElementById('old_data_row_color').value;
		if(old!="")
		{   
			old=old.split(",");
			for(var i=0; i<old.length; i++)
			{  
				js_set_value( old[i] ) 
			}
		}
	}
	
	
	var selected_id = new Array;
	var selected_id_dtls = new Array;
	var selected_name = new Array;
	var currencyArr = new Array; //for check currency mix
	var buyerArr = new Array; //for check buyer mix
	
	function check_all_data() {
		var tbl_row_count = document.getElementById( 'table_body' ).rows.length;
		tbl_row_count = tbl_row_count - 0;
		for( var i = 1; i <= tbl_row_count; i++ ) {
			js_set_value( i );
		}
	}
	
	function toggle( x, origColor ) {
		var newColor = 'yellow';
		if ( x.style ) {
			x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
		}
	}
	
	function js_set_value(str) 
	{ 
			
		var invoiceID = $('#hidden_invoice_id'+str).val();
		var subdtlsid = $('#hidden_sub_dtls_id'+str).val();
		var currency = $('#hidden_currency'+str).val();
		var buyer = $('#hidden_buyer'+str).val();
		 
		//currency mix check-------------------------------//
		if(currencyArr.length==0)
		{
			currencyArr.push( currency );
		}
		else if( jQuery.inArray( currency, currencyArr )==-1 &&  currencyArr.length>0)
		{
			alert("Currency Mixed is Not Allow");
			return;
		}
		
		//buyer mix check--------------------------------//
		if(buyerArr.length==0)
		{
			buyerArr.push( buyer );
			//alert(buyer);
		}
		else if( jQuery.inArray( buyer, buyerArr )==-1 &&  buyerArr.length>0)
		{
			alert("Buyer Mixed is Not Allow");
			return;
		}
					
		toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
		
		if( jQuery.inArray( invoiceID, selected_id ) == -1 ) {
			selected_id.push( invoiceID );
			selected_id_dtls.push( subdtlsid );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == invoiceID ) break;
			}
			selected_id.splice( i, 1 );
			selected_id_dtls.splice( i, 1 );
		}
		var id =''; var id_dtls = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			if(selected_id_dtls[i]>0)
			{
				id_dtls += selected_id_dtls[i] + ',';
			}
		}
		id 		= id.substr( 0, id.length - 1 );
		if(id_dtls!=""&& id_dtls !=null) id_dtls= id_dtls.substr( 0, id_dtls.length - 1 );
		 
		$('#all_invoice_id').val( id );	
		$('#all_sub_dtls_id').val( id_dtls );			
 	}
	
    </script>

</head>

<body>
<div align="center" style="width:740px;">
	<form name="searchexportinformationfrm"  id="searchexportinformationfrm">
		<fieldset style="width:730px;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" border="1" rules="all" width="720" class="rpt_table">
                <thead>
                    <th>Company</th>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th>Lc Number</th>
                    <th>Invoice No.</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:80px" class="formbutton" />
                        <input type="hidden" name="hidden_lcSc_id" id="hidden_lcSc_id" value="" />
                        <input type="hidden" name="is_lcSc" id="is_lcSc" value="" />
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <?
                            echo create_drop_down( "cbo_company_name", 140, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--- Select Company ---", $companyID, "",1 );
                        ?>                        
                    </td>
                    <td>
                    	<?
                    		echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$companyID' $buyer_cond group by buy.id, buy.buyer_name order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
						?>
                    </td>
                    <td> 
                        <?
						//function create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index, $new_conn, $field_name )
                            $arr=array(1=>'From Invoice',2=>'From Submission To Buyer');
                            //echo create_drop_down( "cbo_buyer_sub", 180, $arr,"", 0, "--- Select ---", 0, "" );
							if($dtls_sub_ref_id=="")
							{
								echo create_drop_down( "cbo_buyer_sub", 160,$arr,"", 0, "", 1, "",0 );
							}
							else if($dtls_sub_ref_id==0)
							{
								echo create_drop_down( "cbo_buyer_sub", 160,$arr,"", 0, "", 1, "",1 );
							}
							else
							{
								echo create_drop_down( "cbo_buyer_sub", 160,$arr,"", 0, "", 2, "",1 );
							}
                        ?> 
                    </td>
                    <td>
                        <input type="text" style="width:80px" class="text_boxes"  name="txt_lc_no" id="txt_lc_no" />
                    </td>						
                    <td>
                        <input type="text" style="width:80px" class="text_boxes"  name="txt_invoice_no" id="txt_invoice_no" />
                    </td>                       
                     <td>
                        <input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_buyer_sub').value+'**'+document.getElementById('txt_lc_no').value+'**'+document.getElementById('txt_invoice_no').value+'**'+'<? echo $invoice_id_string; ?>'+'**'+'<? echo $mst_tbl_id; ?>', 'lcSc_search_list_view', 'search_div', 'export_doc_sub_entry_controller', 'setFilterGrid(\'table_body\',-1)'); set_all();" style="width:80px;" />
                     </td>
                </tr>
           </table>
            <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
	exit(); 
 
}

if($action=="lcSc_search_list_view")
{
	$data=explode('**',$data); 	
	$company_id=$data[0];
	$buyer_id=$data[1];
	$search_by_buy_sub=$data[2];
	$search_lc_sc=trim($data[3]);
	$search_invoice=trim($data[4]);
	//echo $data[5];die;
	$invoiceAllid =$data[5];
	$invoiceArr = explode(",",$data[5]);
	$mst_tbl_id = $data[6];
	if(empty($invoiceAllid)) $invoiceAllid=0;
	
	if($mst_tbl_id=="") $mst_tbl_id=0;
	
	if($buyer_id!=0) $buyer_con="and m.buyer_id=$buyer_id"; else $buyer_con;
	if($company_id==0){ echo "Please Select Company first"; die; }
	
	
	if($search_by_buy_sub==2)
	{
		if($search_invoice != "")
		{
			//$sql = "SELECT m.is_lc,m.id,invoice_no,lc_sc_id,net_invo_value,m.buyer_id,short_name FROM com_export_invoice_ship_mst m, lib_buyer b, com_export_doc_submission_invo c  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con and benificiary_id=$company_id and invoice_no like '%$search_invoice%' and b.id=m.buyer_id  and m.id=c.invoice_id and c.is_converted=0 and m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id != '$mst_tbl_id' and status_active=1 and is_deleted=0)";
			
				$sql = "SELECT m.is_lc,m.id,m.invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name,c.id as sub_dtls_id,c.doc_submission_mst_id as sub_mst_id FROM com_export_invoice_ship_mst m, lib_buyer b, com_export_doc_submission_invo c, com_export_doc_submission_mst d  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con and benificiary_id=$company_id and invoice_no like '%$search_invoice%' and b.id=m.buyer_id  and m.id=c.invoice_id and d.id=c.doc_submission_mst_id and d.entry_form=39 and c.is_converted=0
				union all
			SELECT m.is_lc,m.id,invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name,c.id as sub_dtls_id,c.doc_submission_mst_id as sub_mst_id FROM com_export_invoice_ship_mst m, lib_buyer b , com_export_doc_submission_invo c , com_export_doc_submission_mst d  WHERE m.status_active=1  and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and m.id=c.invoice_id and d.id=c.doc_submission_mst_id and d.entry_form=39  and m.id in(select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id ='$mst_tbl_id' and status_active=1 and is_deleted=0)";
			
		}
		else if ($search_lc_sc != "")
		{ 
				$sql = "SELECT m.is_lc,m.id,m.invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name,c.id as sub_dtls_id,c.doc_submission_mst_id as sub_mst_id FROM com_export_invoice_ship_mst m, lib_buyer b , com_export_doc_submission_invo c , com_export_doc_submission_mst d  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and ( lc_sc_id in (select id from com_sales_contract where contract_no like '%$search_lc_sc%' and status_active=1 and is_deleted=0) or lc_sc_id in(select id from com_export_lc where export_lc_no like '%$search_lc_sc%' and status_active=1 and is_deleted=0) ) and b.id=m.buyer_id and m.id=c.invoice_id and d.id=c.doc_submission_mst_id and d.entry_form=39 and c.is_converted=0
				union all
			SELECT m.is_lc,m.id,invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name,c.id as sub_dtls_id,c.doc_submission_mst_id as sub_mst_id FROM com_export_invoice_ship_mst m, lib_buyer b , com_export_doc_submission_invo c , com_export_doc_submission_mst d  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and m.id=c.invoice_id and d.id=c.doc_submission_mst_id and d.entry_form=39  and m.id in(select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id ='$mst_tbl_id' and status_active=1 and is_deleted=0)";
			
		}
		else
		{
			$sql = "SELECT m.is_lc,m.id,invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name,c.id as sub_dtls_id,c.doc_submission_mst_id as sub_mst_id FROM com_export_invoice_ship_mst m, lib_buyer b , com_export_doc_submission_invo c , com_export_doc_submission_mst d  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and m.id=c.invoice_id and d.id=c.doc_submission_mst_id and d.entry_form=39  and c.is_converted=0
			union all
			SELECT m.is_lc,m.id,invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name,c.id as sub_dtls_id,c.doc_submission_mst_id as sub_mst_id FROM com_export_invoice_ship_mst m, lib_buyer b , com_export_doc_submission_invo c , com_export_doc_submission_mst d  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and m.id=c.invoice_id and d.id=c.doc_submission_mst_id and d.entry_form=39 and m.id in(select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id ='$mst_tbl_id' and status_active=1 and is_deleted=0)";
		}
		
	}
	else
	{
		if($search_invoice != "")
		{
				$sql = "SELECT m.is_lc,m.id,m.invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con and benificiary_id=$company_id and invoice_no like '%$search_invoice%' and b.id=m.buyer_id and m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id !='$mst_tbl_id' and status_active=1 and is_deleted=0)";
			
		}
		else if ($search_lc_sc != "")
		{ 
				$sql = "SELECT m.is_lc,m.id,m.invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and ( lc_sc_id in (select id from com_sales_contract where contract_no like '%$search_lc_sc%' and status_active=1 and is_deleted=0) or lc_sc_id in(select id from com_export_lc where export_lc_no like '%$search_lc_sc%' and status_active=1 and is_deleted=0) ) and b.id=m.buyer_id and  m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id !='$mst_tbl_id' and status_active=1 and is_deleted=0)";
			
		}
		else
		{
			$sql = "SELECT m.is_lc,m.id,m.invoice_no,m.lc_sc_id,m.net_invo_value,m.buyer_id,b.short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and  m.id NOT IN (select invoice_id from com_export_doc_submission_invo where doc_submission_mst_id !='$mst_tbl_id' and status_active=1 and is_deleted=0)";
			//$sql = "SELECT m.is_lc,m.id,m.invoice_no,m.lc_sc_id,m.net_invo_value,b.short_name FROM com_export_invoice_ship_mst m, lib_buyer b  WHERE m.status_active=1 and m.is_deleted=0 $buyer_con  and benificiary_id=$company_id and b.id=m.buyer_id and  m.id NOT IN (select invoice_id from com_export_doc_submission_invo where invoice_id not in($invoiceAllid) and status_active=1 and is_deleted=0)";
		}
	}
	//echo $sql; die;		
		 
	?>
		<div style=" width:730px;">
                <table  width="710" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table">                    
                    <thead>
                        <th width="40">SL</th>
                        <th width="180">Invoice No</th>
                        <th width="180">LC/SC No</th>
                        <th width="100">Buyer</th>
                        <th width="100">Currency</th>
                        <th >Net Invoice Value</th>
                    </thead> 
               </table>
       </div>             
       <div style="width:730px; overflow-y:scroll; max-height:200px">                
       		<table  width="710" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="table_body">
			<?
                $i=1; $oldDataRow=$pay_term_cond="";
                $nameArray=sql_select($sql);
                foreach($nameArray as $row)
                {  
                    if ($i%2==0)  $bgcolor="#E9F3FF";
                    else $bgcolor="#FFFFFF";   
                    
                    if($row[csf("is_lc")]==1)
					{
                        //$lcscRes=sql_select("select export_lc_no, currency_name, 0 as pay_term from com_export_lc where id=".$row[csf("lc_sc_id")].""); 
						$lc_sc_no = $lcscRes_arr[$row[csf('lc_sc_id')]]['export_lc_no']; 
                    	$currency_name = $lcscRes_arr[$row[csf('lc_sc_id')]]['currency_name'];
						$pay_term_cond = $lcscRes_arr[$row[csf('lc_sc_id')]]['pay_term'];

					}
					else 
					{
                        //$lcscRes=sql_select("select contract_no, currency_name,pay_term from com_sales_contract where id=".$row[csf("lc_sc_id")].""); 
						$lc_sc_no = $ScRes_arr[$row[csf('lc_sc_id')]]['contract_no'];  
                    	$currency_name = $ScRes_arr[$row[csf('lc_sc_id')]]['currency_name'];		
						$pay_term_cond =  $ScRes_arr[$row[csf('lc_sc_id')]]['pay_term'];
						
 					}
					 
					//var_dump($lcscRes);
					if($pay_term_cond!=3)	// pay term 3 means cash in advanced sales contact not come in document submission form		
					{
						//old data row arrange here------
						if( in_array($row[csf("id")], $invoiceArr) )
						{
							if($oldDataRow=="") $oldDataRow = $i; else $oldDataRow .= ",".$i;
						}
						?>   	
								<tr bgcolor="<? echo $bgcolor; ?>" id="search<? echo $i; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<? echo $i;?>)" > 
									<td width="40"><? echo $i;?> 
										<input type="hidden" id="hidden_invoice_id<? echo $i; ?>" value="<? echo $row[csf("id")];?>" />
										<input type="hidden" id="hidden_invoice_no<? echo $i; ?>" value="<? echo $row[csf("invoice_no")];?>" />
                                        <input type="hidden" id="hidden_sub_dtls_id<? echo $i; ?>" value="<? echo $row[csf("sub_dtls_id")];?>" />                                     
									</td>		                   
									<td width="180"><p><? echo $row[csf("invoice_no")];?></p></td>
									<td width="180"><p><? echo $lc_sc_no;?>&nbsp;</p></td> 
									<td width="100"><p><? echo $row[csf("short_name")]; ?></p><input type="hidden" id="hidden_buyer<? echo $i; ?>" value="<? echo $row[csf("buyer_id")]; ?>" /></td>
									<td width="100">
										<p><? echo $currency[$currency_name]; ?>&nbsp;</p>
										<input type="hidden" id="hidden_currency<? echo $i; ?>" value="<? echo $currency_name; ?>" />
									</td>
									<td align="right"><p><? echo $row[csf("net_invo_value")];?>&nbsp;</p></td>
								</tr>         
						<?           
						$i++; 
					}//pay term if cond end
					
                } //foeach end
             ?>		
             		<input type="hidden" name="old_data_row_color" id="old_data_row_color" value="<?php echo $oldDataRow; ?>"/>
			</table>
                </div>
                <div style="width:50%; float:left" align="left">
                            <input type="hidden"  id="all_invoice_id" value="" />
                            <input type="hidden"  id="all_sub_dtls_id" value="" />
                            <input type="hidden"  id="all_invoice_no" value="" /> 
                            <input type="checkbox" name="check_all_lc" id="check_all_lc" onClick="check_all_data()" value="0" />&nbsp;&nbsp;Check All
                </div>
                <div style="width:40%; float:left" align="left">
                            <input type="submit" class="formbutton" id="close" style="width:80px" onClick="parent.emailwindow.hide();" value="Close" />
                </div>       
            <?        
       
	   exit();    
 	
}




//invoice list view create
if ($action=="show_invoice_list_view")
{
	//echo $data;die;
	$data=explode('**',$data);
	//echo( $data[0]);die;
	$sql="select id,is_lc,lc_sc_id from com_export_invoice_ship_mst where id in($data[0])";
	
	$sub_dtls_id_arr = return_library_array( "select invoice_id,id from com_export_doc_submission_invo where id in($data[1])",'invoice_id','id');
	
	$nameArray=sql_select($sql);
	$lcscNoString="";$lcscNoID="";$lienBank="";$currencyID="";$invoice_tr="";$tot_invoice_val=0; 
	$i=1;
	foreach($nameArray as $row)
	{
		
			if($row[csf("is_lc")] == "1") // if LC
			{
				if($db_type==0)
				{
					$sqlDtls="SELECT invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc,invo.bl_no,invo.bl_date, invo.net_invo_value, lc.lien_bank, lc.currency_name, lc.export_lc_no as lcsc_no, group_concat(dtl.po_breakdown_id) as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_export_lc lc, com_export_invoice_ship_dtls dtl WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = lc.id and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc, invo.bl_no, invo.bl_date, invo.net_invo_value, lc.lien_bank, lc.currency_name, lc.export_lc_no";
				}
				else
				{
					$sqlDtls="SELECT invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc,invo.bl_no,invo.bl_date, invo.net_invo_value, lc.lien_bank, lc.currency_name, lc.export_lc_no as lcsc_no, LISTAGG(dtl.po_breakdown_id, ',') WITHIN GROUP (ORDER BY dtl.po_breakdown_id) as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_export_lc lc, com_export_invoice_ship_dtls dtl WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = lc.id  and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc, invo.bl_no, invo.bl_date, invo.net_invo_value, lc.lien_bank, lc.currency_name, lc.export_lc_no";
				}
			}
			else
			{
				if($db_type==0)
				{
					$sqlDtls="SELECT invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc,invo.bl_no,invo.bl_date, invo.net_invo_value, sc.lien_bank,sc.currency_name,sc.contract_no as lcsc_no, group_concat(dtl.po_breakdown_id) as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_sales_contract sc, com_export_invoice_ship_dtls dtl WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = sc.id  and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc, invo.bl_no, invo.bl_date, invo.net_invo_value, sc.lien_bank, sc.currency_name, sc.contract_no";
				}
				else
				{
					$sqlDtls="SELECT invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc,invo.bl_no,invo.bl_date, invo.net_invo_value, sc.lien_bank,sc.currency_name,sc.contract_no as lcsc_no, LISTAGG(dtl.po_breakdown_id, ',') WITHIN GROUP (ORDER BY dtl.po_breakdown_id) as po_breakdown_id FROM  com_export_invoice_ship_mst invo, com_sales_contract sc, com_export_invoice_ship_dtls dtl  WHERE invo.id=".$row[csf("id")]." and invo.lc_sc_id = sc.id and invo.id=dtl.mst_id and dtl.current_invoice_qnty>0 group by invo.id, invo.invoice_no, invo.buyer_id, invo.invoice_date, invo.lc_sc_id, invo.is_lc, invo.bl_no, invo.bl_date, invo.net_invo_value, sc.lien_bank, sc.currency_name, sc.contract_no";
				}
				
			}
		
	
		//echo $sqlDtls;die;
		$resArray=sql_select($sqlDtls);
		foreach($resArray as $res)
		{
			if($lcscNoString=="") $lcscNoString .= $res[csf("lcsc_no")]; else $lcscNoString .=",".$res[csf("lcsc_no")];
			if($lcscNoID=="") $lcscNoID .= $res[csf("lc_sc_id")]; else $lcscNoID .=",".$res[csf("lc_sc_id")];
			$lienBank=$res[csf("lien_bank")];
			$currencyID=$res[csf("currency_name")];
			$buyerID=$res[csf("buyer_id")];
			
			//list view data arrange-----------------// 
			$poSQL=sql_select("select po_number from wo_po_break_down where id in (".$res[csf("po_breakdown_id")].")");
			$po_numbers="";
			foreach($poSQL as $poR)
			{
				if($po_numbers=="") $po_numbers=$poR[csf("po_number")]; else $po_numbers.=",".$poR[csf("po_number")];
			}
			$invoice_tr .= "<tr>".
								"<td><input type=\"text\" id=\"txt_invoice_no$i\" name=\"txt_invoice_no[]\" class=\"text_boxes\" style=\"width:150px\" value=\"".$res[csf("invoice_no")]."\" /><input type=\"hidden\" id=\"hidden_invoice_id$i\" value=\"".$res[csf("id")]."\" /><input type=\"hidden\" id=\"hidden_sub_dtls_id$i\" value=\"".$sub_dtls_id_arr[$res[csf("id")]]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_lcsc_no$i\" name=\"txt_lcsc_no[]\" class=\"text_boxes\" style=\"width:150px\" value=\"".$res[csf("lcsc_no")]."\" /><input type=\"hidden\" id=\"txt_lcsc_id$i\" value=\"".$res[csf("lc_sc_id")]."\" /><input type=\"hidden\" id=\"hidden_is_lc$i\" value=\"".$res[csf("is_lc")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_bl_no$i\" name=\"txt_bl_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$res[csf("bl_no")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_invoice_date$i\" name=\"txt_invoice_date[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".change_date_format($res[csf("invoice_date")])."\" /></td>".
								"<td><input type=\"text\" id=\"txt_net_invo_value$i\" name=\"txt_net_invo_value[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$res[csf("net_invo_value")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_po_numbers$i\" name=\"txt_po_numbers[]\" class=\"text_boxes\" style=\"width:200px\" value=\"".$po_numbers."\" /><input type=\"hidden\" id=\"hidden_po_numbers_id$i\" value=\"".$res[csf("po_breakdown_id")]."\" /></td>".
							"</tr>"; 
			$tot_invoice_val+=$res[csf("net_invo_value")];				
			$i++; 
		}
	}
	
	$invoice_tr .= "<tr class=\"tbl_bottom\">".
						"<td colspan=\"4\" align=\"right\">Total</td>".
						"<td><input type=\"text\" name=\"txt_total\" id=\"txt_total\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$tot_invoice_val."\"/></td>".
						"<td>&nbsp;</td>".
					"</tr>";
	echo "$('#lcsc_no').val('".$lcscNoString."');\n";
	echo "$('#lc_sc_id').val('".$lcscNoID."');\n";	
	echo "$('#cbo_buyer_name').val('".$buyerID."');\n";		
	echo "$('#cbo_lien_bank').val('".$lienBank."');\n";
	echo "$('#cbo_currency').val('".$currencyID."');\n";
	
	//list view for invoice area----------------------------------------//
	echo "$('#invo_table').find('tr:gt(0)').remove()".";\n";
	echo "$('#invoice_container').html('".$invoice_tr."')".";\n";
	echo "$('#invoice_container').find('input').attr('Disabled','Disabled');\n";
		
	exit();
}



if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
 		
 	 	//---------------Check Duplicate Bank Ref/ Bill No  ------------------------//
		$duplicate = is_duplicate_field("id"," com_export_doc_submission_mst","bank_ref_no=$txt_bank_ref and company_id=$cbo_company_name  and buyer_id=$cbo_buyer_name"); 
		if($duplicate==1) 
		{			 
			echo "20**Duplicate Bank Ref. Number";
			die;
		}
		//------------------------------Check Duplicate END---------------------------------------//
		
		
		//master table entry here START---------------------------------------//		
		$id=return_next_id("id", "com_export_doc_submission_mst", 1);		
 		$field_array_mst="id,company_id,buyer_id,submit_date,entry_form,submit_to,bank_ref_no,bank_ref_date,days_to_realize,possible_reali_date,courier_receipt_no,courier_company,courier_date,bnk_to_bnk_cour_no,bnk_to_bnk_cour_dt,lien_bank,lc_currency,submit_type,negotiation_date,total_negotiated_amount,total_lcsc_currency,remarks,inserted_by,insert_date";
		$data_array_mst="(".$id.",".$cbo_company_name.",".$cbo_buyer_name.",".$txt_submit_date.",40,".$cbo_submit_to.",".$txt_bank_ref.",".$txt_bank_ref_date.",".$txt_day_to_realize.",".$txt_possible_reali_date.",".$courier_receipt_no.",".$txt_courier_company.",".$txt_courier_date.",".$txt_bnk_to_bnk_cour_no.",".$txt_bnk_to_bnk_cour_date.",".$cbo_lien_bank.",".$cbo_currency.",".$cbo_submission_type.",".$txt_negotiation_date.",".$total_dom_curr_hid.",".$total_foreign_curr_hid.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
		//echo "20**".$field_array."<br>".$data_array;die;
		//master table entry here END---------------------------------------// 
 		 
 		 
 		//dtls table entry here START---------------------------------------//		
		$dtlsid=return_next_id("id", "com_export_doc_submission_invo", 1);		
		$field_array="id,doc_submission_mst_id,invoice_id,is_lc,lc_sc_id,submission_dtls_id,bl_no,invoice_date,net_invo_value,all_order_no,inserted_by,insert_date";
		$update_sub_dtls_arr="is_converted";
		$data_array="";
		for($i=1;$i<=$invoiceRow;$i++)
		{
			if($i>1) $data_array .= ",";
			$hidden_invoice_id 		= 'hidden_invoice_id'.$i;
			$hidden_sub_dtls_id 	= 'hidden_sub_dtls_id'.$i;
			$txt_lcsc_id 			= 'txt_lcsc_id'.$i;
			$hidden_is_lc 			= 'hidden_is_lc'.$i;
			$txt_lcsc_no 			= 'txt_lcsc_no'.$i;
			$txt_bl_no 				= 'txt_bl_no'.$i;
			$txt_invoice_date 		= 'txt_invoice_date'.$i;
			$txt_net_invo_value 	= 'txt_net_invo_value'.$i;
			$hidden_po_numbers_id 	= 'hidden_po_numbers_id'.$i;
			
			if($$hidden_sub_dtls_id=="") $$hidden_sub_dtls_id=0;
			
			if($db_type==0) $invoice_date=$$txt_invoice_date;
			else $invoice_date=change_date_format($$txt_invoice_date,'','',1);
			
  			$data_array .= "(".$dtlsid.",".$id.",'".$$hidden_invoice_id."','".$$hidden_is_lc."','".$$txt_lcsc_id."','".$$hidden_sub_dtls_id."','".$$txt_bl_no."','".$invoice_date."','".$$txt_net_invo_value."','".$$hidden_po_numbers_id."','".$user_id."','".$pc_date_time."')";	
			$dtlsid=$dtlsid+1;
			
			$update_sub_dtlsID_array[]=$$hidden_sub_dtls_id; 
			$update_sub_dtls_data[$$hidden_sub_dtls_id]=explode("*",("1"));
		}
		//echo $$hidden_sub_dtls_id;die;
		//echo "20**insert into com_export_doc_submission_invo (".$field_array.") values ".$data_array;die;
		/*if($data_array!="")
		{
			$dtlsrID=sql_insert("com_export_doc_submission_invo",$field_array,$data_array,0);
		}*/
		//dtls table entry here END---------------------------------------// 
		
		
		//transaction table entry here START---------------------------------------//		
		$trid=return_next_id("id", "com_export_doc_sub_trans", 1);		
		$field_array_trans="id,doc_submission_mst_id,acc_head,acc_loan,dom_curr,conver_rate,lc_sc_curr,inserted_by,insert_date";
		$data_array_trans=""; $tsrID=true; 
		for($i=1;$i<=$transRow;$i++)
		{
			if($i>1) $data_array_trans .= ",";
			$cbo_account_head 		= 'cbo_account_head_'.$i;
			$txt_ac_loan_no 		= 'txt_ac_loan_no_'.$i;
			$txt_domestic_curr 		= 'txt_domestic_curr_'.$i;
			$txt_conversion_rate 	= 'txt_conversion_rate_'.$i;
			$txt_lcsc_currency 		= 'txt_lcsc_currency_'.$i;
			
  			$data_array_trans .= "(".$trid.",".$id.",'".$$cbo_account_head."','".$$txt_ac_loan_no."','".$$txt_domestic_curr."','".$$txt_conversion_rate."','".$$txt_lcsc_currency."','".$user_id."','".$pc_date_time."')";	
			$trid=$trid+1;
		}
 		//echo "20**".$field_array."<br>".$data_array;die;
		if($data_array_trans!="")
		{
			$tsrID=sql_insert("com_export_doc_sub_trans",$field_array_trans,$data_array_trans,0);
		}
		
		
		//echo "20**".$field_array."<br>".$data_array;die;
		$rID=sql_insert("com_export_doc_submission_mst",$field_array_mst,$data_array_mst,1);
		//echo "20**".$rID." && ".$dtlsrID." && ".$tsrID;mysql_query("ROLLBACK");die;
		//echo "20**insert into com_export_doc_submission_invo (".$field_array.") values ".$data_array;die;
		$dtlsrID=true;
		if($data_array!="")
		{
			$dtlsrID=sql_insert("com_export_doc_submission_invo",$field_array,$data_array,0);
		}

		$upsubDtlsID=true;
		if($$hidden_sub_dtls_id!="")
		{
			$upsubDtlsID=execute_query(bulk_update_sql_statement("com_export_doc_submission_invo","id",$update_sub_dtls_arr,$update_sub_dtls_data,$update_sub_dtlsID_array),1);
		}

		if($db_type==0)
		{
			if($rID && $dtlsrID && $tsrID && $upsubDtlsID)
			{
				mysql_query("COMMIT");  
				echo "0**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10".$id;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID && $tsrID && $upsubDtlsID)
			{
				oci_commit($con);    
				echo "0**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10".$id;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if(str_replace("'","",$mst_tbl_id)=="" || str_replace("'","",$invoice_tbl_id)=="") { echo "10**";die; }
		
 		 
		//---------------Check Duplicate Bank Ref/ Bill No  ------------------------//
		$duplicate = is_duplicate_field("id"," com_export_doc_submission_mst","bank_ref_no=$txt_bank_ref and id!=$mst_tbl_id and company_id=$cbo_company_name and buyer_id=$cbo_buyer_name"); 
		if($duplicate==1 && str_replace("'","",$txt_bank_ref)!="") 
		{			 
			echo "20**Duplicate Bank Ref. Number";
			die;
		}
		//------------------------------Check Duplicate END---------------------------------------//
		
		 
		//master table entry here START---------------------------------------//		
 		$id=str_replace("'","",$mst_tbl_id);
		$field_array_mst="company_id*buyer_id*submit_date*submit_to*bank_ref_no*bank_ref_date*days_to_realize*possible_reali_date*courier_receipt_no*courier_company*courier_date*bnk_to_bnk_cour_no*bnk_to_bnk_cour_dt*lien_bank*lc_currency*submit_type*negotiation_date*total_negotiated_amount*total_lcsc_currency*remarks*updated_by*update_date";
		$data_array_mst="".$cbo_company_name."*".$cbo_buyer_name."*".$txt_submit_date."*".$cbo_submit_to."*".$txt_bank_ref."*".$txt_bank_ref_date."*".$txt_day_to_realize."*".$txt_possible_reali_date."*".$courier_receipt_no."*".$txt_courier_company."*".$txt_courier_date."*".$txt_bnk_to_bnk_cour_no."*".$txt_bnk_to_bnk_cour_date."*".$cbo_lien_bank."*".$cbo_currency."*".$cbo_submission_type."*".$txt_negotiation_date."*".$total_dom_curr_hid."*".$total_foreign_curr_hid."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
		
		
		//echo "20**".$field_array."<br>".$data_array;die;
 		
		//master table entry here END---------------------------------------// 
 		 
 		 
 		//dtls table entry here START---------------------------------------//	
 		
		$dtlsid=return_next_id("id", "com_export_doc_submission_invo", 1);
		$field_array="id,doc_submission_mst_id,invoice_id,is_lc,lc_sc_id,submission_dtls_id,bl_no,invoice_date,net_invo_value,all_order_no,inserted_by,insert_date";
		$update_sub_dtls_arr="is_converted";
		$data_array="";
		for($i=1;$i<=$invoiceRow;$i++)
		{
			if($i>1) $data_array .= ",";
			$hidden_invoice_id 		= 'hidden_invoice_id'.$i;
			$hidden_sub_dtls_id 	= 'hidden_sub_dtls_id'.$i;
			$txt_lcsc_id 			= 'txt_lcsc_id'.$i;
			$hidden_is_lc 			= 'hidden_is_lc'.$i;
			$txt_lcsc_no 			= 'txt_lcsc_no'.$i;
			$txt_bl_no 				= 'txt_bl_no'.$i;
			$txt_invoice_date 		= 'txt_invoice_date'.$i;
			$txt_net_invo_value 	= 'txt_net_invo_value'.$i;
			$hidden_po_numbers_id 	= 'hidden_po_numbers_id'.$i;
			
			if($db_type==0) $invoice_date=$$txt_invoice_date;
			else $invoice_date=change_date_format($$txt_invoice_date,'','',1);
			
			if($$hidden_sub_dtls_id=="") $$hidden_sub_dtls_id=0;
			
  			$data_array .= "(".$dtlsid.",".$id.",'".$$hidden_invoice_id."','".$$hidden_is_lc."','".$$txt_lcsc_id."','".$$hidden_sub_dtls_id."','".$$txt_bl_no."','".$invoice_date."','".$$txt_net_invo_value."','".$$hidden_po_numbers_id."','".$user_id."','".$pc_date_time."')";	
			$dtlsid=$dtlsid+1;
			$update_sub_dtlsID_array[]=$$hidden_sub_dtls_id; 
			$update_sub_dtls_data[$$hidden_sub_dtls_id]=explode("*",("1"));
		}
 		//echo "insert into com_export_doc_submission_invo ($field_array) values $data_array";die;
		/*if($data_array!="")
		{
			$dtlsrID=sql_insert("com_export_doc_submission_invo",$field_array,$data_array,0);
		}*/
				
		$trid=return_next_id("id", "com_export_doc_sub_trans", 1);		
		$field_array_trans="id,doc_submission_mst_id,acc_head,acc_loan,dom_curr,conver_rate,lc_sc_curr,inserted_by,insert_date";
		$data_array_trans="";$tsrID=true;
		for($i=1;$i<=$transRow;$i++)
		{
			if($i>1) $data_array_trans .= ",";
			$cbo_account_head 		= 'cbo_account_head_'.$i;
			$txt_ac_loan_no 		= 'txt_ac_loan_no_'.$i;
			$txt_domestic_curr 		= 'txt_domestic_curr_'.$i;
			$txt_conversion_rate 	= 'txt_conversion_rate_'.$i;
			$txt_lcsc_currency 		= 'txt_lcsc_currency_'.$i;
			
  			$data_array_trans .= "(".$trid.",".$id.",'".$$cbo_account_head."','".$$txt_ac_loan_no."','".$$txt_domestic_curr."','".$$txt_conversion_rate."','".$$txt_lcsc_currency."','".$user_id."','".$pc_date_time."')";	
			$trid=$trid+1;
		}
		
		$deletetrans = execute_query("DELETE FROM com_export_doc_sub_trans WHERE doc_submission_mst_id=$mst_tbl_id");
		$deleteDtls = execute_query("DELETE FROM com_export_doc_submission_invo WHERE doc_submission_mst_id=$mst_tbl_id");
 		//echo "20**".$field_array."<br>".$data_array;die;
		if($data_array_trans!="")
		{
			$tsrID=sql_insert("com_export_doc_sub_trans",$field_array_trans,$data_array_trans,0);
		}
		//transaction table entry here END---------------------------------------// 
		$dtlsrID=true;
		if($data_array!="")
		{
			$dtlsrID=sql_insert("com_export_doc_submission_invo",$field_array,$data_array,0);
		}

		//echo "20**".$rID." && ".$dtlsrID." && ".$tsrID;mysql_query("ROLLBACK");die;
		$rID=sql_update("com_export_doc_submission_mst",$field_array_mst,$data_array_mst,"id",$mst_tbl_id,1);
		
		$upsubDtlsID=true;
		if($$hidden_sub_dtls_id!="")
		{
			$upsubDtlsID=execute_query(bulk_update_sql_statement("com_export_doc_submission_invo","id",$update_sub_dtls_arr,$update_sub_dtls_data,$update_sub_dtlsID_array),1);
		}
		
		
		if($db_type==0)
		{
			if($rID && $dtlsrID && $tsrID && $deleteDtls && $deletetrans && $upsubDtlsID)
			{
				mysql_query("COMMIT");  
				echo "1**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID && $tsrID && $deleteDtls && $deletetrans && $upsubDtlsID)
			{
				oci_commit($con);  
				echo "1**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		disconnect($con);
		die;
	}
	
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if(str_replace("'","",$mst_tbl_id)=="" || str_replace("'","",$invoice_tbl_id)=="") { echo "10**";die; }
 		$id=str_replace("'","",$mst_tbl_id);
		
		$update_field_arr="status_active*is_deleted";
		$update_data_arr="0*1";
		$upsubDtlsID=$upsubTransid=true;
		if($id>0)
		{
			$upsubDtlsID=sql_update("com_export_doc_submission_invo",$update_field_arr,$update_data_arr,"doc_submission_mst_id",$id,1);
			$upsubTransid=sql_update("com_export_doc_sub_trans",$update_field_arr,$update_data_arr,"doc_submission_mst_id",$id,1);
		}
		
		if($db_type==0)
		{
			if($upsubDtlsID && $upsubTransid)
			{
				mysql_query("COMMIT");  
				echo "2**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($upsubDtlsID && $upsubTransid)
			{
				oci_commit($con);  
				echo "2**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		disconnect($con);
		die;
	}
	
	
}

if($action=="doc_sub_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_system_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchdocfrm_1"  id="searchdocfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr align="center">                    
                    <td>
                        <?  
                            $search_by = array(1=>'Bank Ref/Bill No',2=>'Submitted To',3=>'Submission Type');
							$dd="change_search_event(this.value, '0*2*2', '0*submited_to*submission_type', '../../../')";
							echo create_drop_down( "cbo_search_by", 140, $search_by, "", 0, "--Select--", 0,$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<? echo $company_name; ?>+'_'+<? echo $buyer_name; ?>, 'create_system_id_search_list_view', 'search_div', 'export_doc_sub_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<? echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_system_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div" style="margin-top:5px"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?
}


if($action=="create_system_id_search_list_view")
{
	 
	$ex_data = explode("_",$data);
	$search_by = $ex_data[0];
	$search_common = $ex_data[1];
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company_name = $ex_data[4];
	$buyer_name = $ex_data[5];
	
	$sql_cond="";
	if($search_by==1 && $search_common!="")
	{
		$sql_cond .= " and a.bank_ref_no like '%$search_common%'";
	}	
	else if($search_by==2 && $search_common!="")
	{
		$sql_cond .= " and a.submit_to='$search_common'";
	}
	else if($search_by==3 && $search_common!="")
	{
		$sql_cond .= " and a.submit_type='$search_common'";
	}
	
	if($db_type==0)
	{
		if( $txt_date_from!="" && $txt_date_to!="" )
		{
			$sql_cond .= " and a.submit_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
		}
	}
	else if($db_type==2)
	{
		if($txt_date_from!="" && $txt_date_to!="") 
		{
			$sql_cond .= " and a.submit_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
		}
	}
 	
	
	if(trim($company_name)!="") $sql_cond .= " and company_id ='$company_name'";
	if(trim($buyer_name)!=0) $sql_cond .= " and buyer_id ='$buyer_name'";
	
	$bank_arr = return_library_array( "select id, bank_name from lib_bank",'id','bank_name');
	$lc_no_arr = return_library_array( "select id, export_lc_no from com_export_lc",'id','export_lc_no');
	$sc_no_arr = return_library_array( "select id, contract_no from com_sales_contract",'id','contract_no');
	$invoice_no_arr= return_library_array( "select id, invoice_no from com_export_invoice_ship_mst",'id','invoice_no');
	
	//$invoiceSQL=sql_select("select invoice_no from com_export_invoice_ship_mst where id in (".$row[csf("invoice_id_string")].")");
	//$poSQL=sql_select("select export_lc_no from com_export_lc where id in (".$lcID.")");
	//$scSQL=sql_select("select contract_no from com_sales_contract where id in (".$scID.")");
	if($db_type==0)
	{
		$sql ="select a.id,a.bank_ref_no,a.submit_date,a.submit_to,a.lien_bank,a.submit_type, group_concat(b.is_lc) as is_lc_string, group_concat(b.invoice_id) as invoice_id_string, group_concat(b.lc_sc_id) as lc_sc_id_string , group_concat(b.submission_dtls_id) as submission_dtls_id
			from com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id and a.status_active=1 and b.status_active=1  and a.entry_form=40 $sql_cond group by a.id,a.bank_ref_no,a.submit_date, a.submit_to, a.lien_bank, a.submit_type";
	}
	else
	{
		$sql ="select a.id,a.bank_ref_no,a.submit_date,a.submit_to,a.lien_bank,a.submit_type, LISTAGG(b.is_lc, ',') WITHIN GROUP (ORDER BY b.is_lc) as is_lc_string, LISTAGG(b.invoice_id, ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id_string, LISTAGG(b.lc_sc_id, ',') WITHIN GROUP (ORDER BY b.lc_sc_id) as lc_sc_id_string, LISTAGG(b.submission_dtls_id, ',') WITHIN GROUP (ORDER BY b.submission_dtls_id) as submission_dtls_id 
			from com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id and a.status_active=1 and b.status_active=1 and a.entry_form=40  $sql_cond group by a.id,a.bank_ref_no,a.submit_date, a.submit_to, a.lien_bank, a.submit_type";	
	}
	//echo $sql;
	$res = sql_select($sql);
    ?>
    <div style="width:918px;">
        <table border="1" width="100%" cellpadding="0" cellspacing="0" class="rpt_table" rules="all">
            <thead>
                <th width="30">SL</th>
                <th width="50">System Id</th>
                <th width="100">Bank Ref /Bill No</th>
                <th width="150">LC/SC No</th>
                <th width="200">Invoice List</th>
                <th width="70">Submit Date</th>
                <th width="70">Submitted To</th>
                <th width="130">Lien Bank</th>
                <th width="">Submit Type</th>
            </thead>
        </table>
    </div>            
    <div style="width:918px; overflow-y:scroll; max-height:230px">
        <table border="1" width="900" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="list_view">             
   		<?                     
        	$i=1;
			foreach($res as $row)
			{  
				
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
				
				$submission_dtls_id = implode(",",array_unique(explode(",",$row[csf("submission_dtls_id")])));
				
				$expisLC = explode(",",$row[csf("is_lc_string")]);
				$expisINV = explode(",",$row[csf("lc_sc_id_string")]);
				$j=0;$lcID="";$scID="";
				foreach($expisLC as $key=>$val)
				{
					if($val==1 && $expisINV[$j]!=0)
					{
 						// export LC id
						if($lcID=="") $lcID .= $expisINV[$j]; else $lcID .=",".$expisINV[$j];
					}
					else if($expisINV[$j]!=0)
					{  
 						//Sales Contact id
						if($scID=="") $scID .= $expisINV[$j]; else $scID .=",".$expisINV[$j];
					}
					$j++;	
 				}
				
				$lc_sc_no="";
				if($lcID!="")
				{
 					// export LC number
					$poSQL=array_unique(explode(",",$lcID));
 					foreach($poSQL as $poR)
					{
						if($lc_sc_no=="") $lc_sc_no=$lc_no_arr[$poR]; else $lc_sc_no.=",".$lc_no_arr[$poR];
					} 
				}
				if($scID!="")
				{
					//Sales Contact Number
					$scSQL=array_unique(explode(",",$scID));
 					foreach($scSQL as $poR)
					{
						if($lc_sc_no=="") $lc_sc_no=$sc_no_arr[$poR]; else $lc_sc_no.=",".$sc_no_arr[$poR];
					}  		 
				}
				
				//invoice list 
				$invoiceNo="";
				if($row[csf("invoice_id_string")]!="")
				{
					$all_invoice_id=array_unique(explode(",",$row[csf("invoice_id_string")]));
					//$invoice_no_arr
					//$invoiceSQL=sql_select("select invoice_no from com_export_invoice_ship_mst where id in (".$row[csf("invoice_id_string")].")");
					foreach($all_invoice_id as $poR)
					{
						if($invoiceNo=="") $invoiceNo=$invoice_no_arr[$poR]; else $invoiceNo.=",".$invoice_no_arr[$poR];
					} 
				}
					
          		?>     
			   		<tr bgcolor="<? echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<? echo $row[csf("id")];?>**<? echo $submission_dtls_id;?>')" > 
                        <td width="30" align="center"><? echo $i;?></td>
                        <td width="50" align="center"><? echo $row[csf("id")];?></td>
                        <td width="100"><p><? echo $row[csf("bank_ref_no")]; ?></p></td>                    
                        <td width="150"><p><? echo $lc_sc_no; ?></p></td>
                        <td width="200"><p><? echo $invoiceNo; ?></p></td>
                        <td width="70" align="center"><? echo change_date_format($row[csf("submit_date")]); ?></td>
                        <td width="70"><p><? echo $submited_to[$row[csf("submit_to")]]; ?></p></td>
                        <td width="130"><p><? echo $bank_arr[$row[csf("lien_bank")]]; ?></p></td>
                        <td><p><? echo $submission_type[$row[csf("submit_type")]]; ?></p></td>
                    </tr>
          		<?
			                 
            	$i++; 
			}      
		?>	
        </table>
	</div>
	<?	
	exit();
}




if($action=="populate_master_from_data")
{  
	if($db_type==0)
	{
		$sql = "select a.id,a.company_id, a.buyer_id,a.submit_date, a.submit_to,a.bank_ref_no, a.bank_ref_date, a.days_to_realize,a.possible_reali_date, a.courier_receipt_no,a.courier_company,a.courier_date, a.bnk_to_bnk_cour_no, a.bnk_to_bnk_cour_dt, a.lien_bank,a.lc_currency,a.submit_type,a.negotiation_date,a.remarks,group_concat(b.id) as inv_id, group_concat(b.is_lc) as is_lc_string, group_concat(b.invoice_id) as invoice_id_string, group_concat(b.lc_sc_id) as lc_sc_id_string 
			from com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id and a.id=$data 
			group by a.id,a.company_id, a.buyer_id,a.submit_date, a.submit_to,a.bank_ref_no, a.bank_ref_date, a.days_to_realize,a.possible_reali_date, a.courier_receipt_no,a.courier_company,a.courier_date, a.bnk_to_bnk_cour_no, a.bnk_to_bnk_cour_dt, a.lien_bank,a.lc_currency,a.submit_type,a.negotiation_date,a.remarks";
	}
	else
	{
		$sql = "select a.id,a.company_id, a.buyer_id,a.submit_date, a.submit_to,a.bank_ref_no, a.bank_ref_date, a.days_to_realize,a.possible_reali_date, a.courier_receipt_no,a.courier_company,a.courier_date, a.bnk_to_bnk_cour_no, a.bnk_to_bnk_cour_dt, a.lien_bank,a.lc_currency,a.submit_type,a.negotiation_date,a.remarks, LISTAGG(b.id, ',') WITHIN GROUP (ORDER BY b.id) as inv_id, LISTAGG(b.is_lc, ',') WITHIN GROUP (ORDER BY b.is_lc) as is_lc_string, LISTAGG(b.invoice_id, ',') WITHIN GROUP (ORDER BY b.invoice_id) as invoice_id_string, LISTAGG(b.lc_sc_id, ',') WITHIN GROUP (ORDER BY b.lc_sc_id) as lc_sc_id_string 
			from com_export_doc_submission_mst a, com_export_doc_submission_invo b
			where a.id=b.doc_submission_mst_id and a.id=$data 
			group by a.id,a.company_id, a.buyer_id,a.submit_date, a.submit_to,a.bank_ref_no, a.bank_ref_date, a.days_to_realize,a.possible_reali_date, a.courier_receipt_no,a.courier_company,a.courier_date, a.bnk_to_bnk_cour_no, a.bnk_to_bnk_cour_dt, a.lien_bank,a.lc_currency,a.submit_type,a.negotiation_date,a.remarks";	
	}
	//echo $sql;
	$res = sql_select($sql);
	$i=1;
	foreach($res as $row)
	{
		
  		echo "$('#cbo_company_name').val('".$row[csf("company_id")]."');\n";
		echo "$('#cbo_buyer_name').val('".$row[csf("buyer_id")]."');\n";
		
 		
		$expisLC = explode(",",$row[csf("is_lc_string")]);
		$expisINV = explode(",",$row[csf("lc_sc_id_string")]);
		$j=0;$lcID="";$scID="";
		foreach($expisLC as $key=>$val)
		{
			if($val==1 && $expisINV[$j]!=0)
			{
				// export LC id
				if($lcID=="") $lcID .= $expisINV[$j]; else $lcID .=",".$expisINV[$j];
			}
			else if($expisINV[$j]!=0)
			{  
				//Sales Contact id
				if($scID=="") $scID .= $expisINV[$j]; else $scID .=",".$expisINV[$j];
			}
			$j++;	
		}
		 
		$lc_sc_no="";
		if($lcID!="")
		{
			// export LC number
			$poSQL=sql_select("select export_lc_no from com_export_lc where id in (".$lcID.")");
			foreach($poSQL as $poR)
			{
				if($lc_sc_no=="") $lc_sc_no=$poR[csf("export_lc_no")]; else $lc_sc_no.=",".$poR[csf("export_lc_no")];
			} 
		}
		if($scID!="")
		{
			//Sales Contact Number
			$scSQL=sql_select("select contract_no from com_sales_contract where id in (".$scID.")");
			foreach($scSQL as $poR)
			{
				if($lc_sc_no=="") $lc_sc_no=$poR[csf("contract_no")]; else $lc_sc_no.=",".$poR[csf("contract_no")];
			}  		 
		}
		echo "$('#lcsc_no').val('".$lc_sc_no."');\n";  
		echo "$('#lc_sc_id').val('".$row[csf("lc_sc_id_string")]."');\n"; 
		echo "$('#invoice_id_string').val('".$row[csf("invoice_id_string")]."');\n"; 
		if($row[csf("submit_date")]!='0000-00-00' || $row[csf("submit_date")]!='')
		{		 
 			echo "$('#txt_submit_date').val('".change_date_format($row[csf("submit_date")])."');\n";
		}
		else
		{
			echo "$('#txt_submit_date').val('');\n";
		}
		echo "$('#cbo_submit_to').val('".$row[csf("submit_to")]."');\n";
		echo "$('#txt_bank_ref').val('".$row[csf("bank_ref_no")]."');\n";
		if($row[csf("bank_ref_date")]=='0000-00-00' || $row[csf("bank_ref_date")]=='')
		{
			echo "$('#txt_bank_ref_date').val('');\n";
		}
		else
		{
			echo "$('#txt_bank_ref_date').val('".change_date_format($row[csf("bank_ref_date")])."');\n";
		}
		echo "$('#cbo_submission_type').val('".$row[csf("submit_type")]."');\n";
		if($row[csf("submit_type")]==1)
		{
		 echo "$('#txt_negotiation_date').attr('disabled',true);\n";
		}
		else
		{
			if($row[csf("negotiation_date")]!='0000-00-00' || $row[csf("negotiation_date")]!='')
			{
				echo "$('#txt_negotiation_date').attr('disabled',false);\n";
				echo "$('#txt_negotiation_date').val('".change_date_format($row[csf("negotiation_date")])."');\n";
			}
			else
			{
				echo "$('#txt_negotiation_date').val('');\n";
			}
		}
		echo "$('#txt_day_to_realize').val('".$row[csf("days_to_realize")]."');\n";
		if($row[csf("possible_reali_date")]=='0000-00-00' || $row[csf("possible_reali_date")]=='')
		{
			echo "$('#txt_possible_reali_date').val('');\n";
		}
		else
		{
			echo "$('#txt_possible_reali_date').val('".change_date_format($row[csf("possible_reali_date")])."');\n";
		}
		echo "$('#courier_receipt_no').val('".$row[csf("courier_receipt_no")]."');\n";
		echo "$('#txt_courier_company').val('".$row[csf("courier_company")]."');\n";
		if($row[csf("courier_date")]=='0000-00-00' || $row[csf("courier_date")]=='')
		{
			echo "$('#txt_courier_date').val('');\n";
		}
		else
		{
			echo "$('#txt_courier_date').val('".change_date_format($row[csf("courier_date")])."');\n";
		}
		echo "$('#txt_bnk_to_bnk_cour_no').val('".$row[csf("bnk_to_bnk_cour_no")]."');\n";
		if($row[csf("bnk_to_bnk_cour_dt")]=='0000-00-00' || $row[csf("bnk_to_bnk_cour_dt")]=='')
		{
			echo "$('#txt_bnk_to_bnk_cour_date').val('');\n";
		}
		else
		{
			echo "$('#txt_bnk_to_bnk_cour_date').val('".change_date_format($row[csf("bnk_to_bnk_cour_dt")])."');\n";
		}
		echo "$('#cbo_lien_bank').val('".$row[csf("lien_bank")]."');\n";
		echo "$('#cbo_currency').val('".$row[csf("lc_currency")]."');\n";
		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		
		echo "$('#mst_tbl_id').val('".$row[csf("id")]."');\n";
		echo "$('#invoice_tbl_id').val('".$row[csf("inv_id")]."');\n";
		
		 
		
		//invoice list view -----------------------------------------------------------------------//
		//start -----------------------------------------------------------------------//
		$invSQL="select a.id,a.doc_submission_mst_id,a.invoice_id,a.is_lc,a.lc_sc_id,a.bl_no,a.invoice_date,a.net_invo_value,a.all_order_no,b.invoice_no,b.id as dtls_id from com_export_doc_submission_invo a, com_export_invoice_ship_mst b where a.invoice_id=b.id and a.doc_submission_mst_id=".$row[csf("id")]."";
 		//echo $invSQL;
		$resArray=sql_select($invSQL);
		$tot_invoice_val=0;
 		foreach($resArray as $invRow)
		{ 
 			//ls or sc number
			if($invRow[csf("is_lc")]==1)
				$lc_sc_nos = return_field_value("export_lc_no","com_export_lc","id=".$invRow[csf("lc_sc_id")].""); 
			else
				$lc_sc_nos = return_field_value("contract_no","com_sales_contract","id=".$invRow[csf("lc_sc_id")].""); 
				
			//list view data arrange-----------------// 
			$poSQL=sql_select("select po_number from wo_po_break_down where id in (".$invRow[csf("all_order_no")].")");
			$po_numbers="";
			foreach($poSQL as $poR)
			{
				if($po_numbers=="") $po_numbers=$poR[csf("po_number")]; else $po_numbers.=",".$poR[csf("po_number")];
			}
			$invoice_tr .= "<tr>".
								"<td><input type=\"text\" id=\"txt_invoice_no$i\" name=\"txt_invoice_no[]\" class=\"text_boxes\" style=\"width:150px\" value=\"".$invRow[csf("invoice_no")]."\" /><input type=\"hidden\" id=\"hidden_invoice_id$i\" value=\"".$invRow[csf("invoice_id")]."\" /><input type=\"hidden\" id=\"hidden_sub_dtls_id$i\" value=\"".$invRow[csf("id")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_lcsc_no$i\" name=\"txt_lcsc_no[]\" class=\"text_boxes\" style=\"width:150px\" value=\"".$lc_sc_nos."\" /><input type=\"hidden\" id=\"txt_lcsc_id$i\" value=\"".$invRow[csf("lc_sc_id")]."\" /><input type=\"hidden\" id=\"hidden_is_lc$i\" value=\"".$invRow[csf("is_lc")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_bl_no$i\" name=\"txt_bl_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$invRow[csf("bl_no")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_invoice_date$i\" name=\"txt_invoice_date[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".change_date_format($invRow[csf("invoice_date")])."\" /></td>".
								"<td><input type=\"text\" id=\"txt_net_invo_value$i\" name=\"txt_net_invo_value[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("net_invo_value")]."\" /></td>".
								"<td><input type=\"text\" id=\"txt_po_numbers$i\" name=\"txt_po_numbers[]\" class=\"text_boxes\" style=\"width:200px\" value=\"".$po_numbers."\" /><input type=\"hidden\" id=\"hidden_po_numbers_id$i\" value=\"".$invRow[csf("all_order_no")]."\" /></td>".
							"</tr>"; 
			
			$tot_invoice_val+=$invRow[csf("net_invo_value")];
			$i++; 
		}
		
		$invoice_tr.= "<tr class=\"tbl_bottom\">".
							"<td colspan=\"4\" align=\"right\">Total</td>".
							"<td><input type=\"text\" name=\"txt_total\" id=\"txt_total\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$tot_invoice_val."\"/></td>".
							"<td>&nbsp;</td>".
						"</tr>";
		//list view for invoice area----------------------------------------//
 		echo "$('#invo_table').find('tr:gt(0)').remove()".";\n";         
		echo "$('#invoice_container').html( '".$invoice_tr."')".";\n";
		echo "$('#invoice_container').find('input').attr('Disabled','Disabled');\n";
 		//invoice list view -----------------------------------------------------------------------//
		//END -----------------------------------------------------------------------//
		
		 
		
		//transaction list generate here Start------------------------------------//
		$invSQL =  "select a.id,a.doc_submission_mst_id,a.acc_head,a.acc_loan,a.dom_curr,a.conver_rate,a.lc_sc_curr 
					from com_export_doc_sub_trans a, com_export_invoice_ship_mst b 
					where a.doc_submission_mst_id=b.id and a.doc_submission_mst_id=".$row[csf("id")]."";
 		//echo $invSQL;
		$resArray=sql_select($invSQL);
		$rowNo=1; $transaction_tr="";
 		foreach($resArray as $invRow)
		{
			$transaction_tr .= "<tr id=\"tr".$rowNo."\">".
					"<td>".create_drop_down( "cbo_account_head_".$rowNo, 200, $commercial_head,"", 1, "-- Select --", $invRow[csf("acc_head")], "get_php_form_data(this.value+\'**\'+".$rowNo.", \'populate_acc_loan_no_data\', \'requires/export_doc_sub_entry_controller\' )",0, "" )."</td>".
					"<td><input type=\"text\" id=\"txt_ac_loan_no_".$rowNo."\" name=\"txt_ac_loan_no[]\" class=\"text_boxes\" style=\"width:100px\" value=\"".$invRow[csf("acc_loan")]."\" /></td>".
					"<td><input type=\"text\" id=\"txt_domestic_curr_".$rowNo."\" name=\"txt_domestic_curr[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("dom_curr")]."\" onkeyup=\"fn_calculate(this.id,".$rowNo.")\" /></td>".
					"<td><input type=\"text\" id=\"txt_conversion_rate_".$rowNo."\" name=\"txt_conversion_rate[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("conver_rate")]."\" onkeyup=\"fn_calculate(this.id,".$rowNo.")\" /></td>".
					"<td><input type=\"text\" id=\"txt_lcsc_currency_".$rowNo."\" name=\"txt_lcsc_currency[]\" class=\"text_boxes_numeric\" style=\"width:100px\" value=\"".$invRow[csf("lc_sc_curr")]."\" onkeyup=\"fn_calculate(this.id,".$rowNo.")\" /></td>".
					"<td>".
					"<input type=\"button\" id=\"increaserow_".$rowNo."\" style=\"width:30px\" class=\"formbuttonplasminus\" value=\"+\" onClick=\"javascript:fn_inc_decr_row(".$rowNo.",\'increase\');\" />".
					"<input type=\"button\" id=\"decreaserow_".$rowNo."\" style=\"width:30px\" class=\"formbuttonplasminus\" value=\"-\" onClick=\"javascript:fn_inc_decr_row(".$rowNo.",\'decrease\');\" />".
					"</td>".
			"</tr>";	
			$rowNo = $rowNo+1;
		}
		
		//list view for transaction area----------------------------------------//
 		echo "$('#transaction_container').find('tr').remove();\n";         
		echo "$('#transaction_container').html( '".$transaction_tr."')".";\n";	
		echo "sum_of_currency();\n";	
		//transaction list generate here End------------------------------------//
		
				 
   	}//main foreach end
		
	exit();	
}





?>


 