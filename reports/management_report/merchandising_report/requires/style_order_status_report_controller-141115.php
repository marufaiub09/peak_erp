<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../../includes/common.php');
require_once('../../../../includes/class.reports.php');
require_once('../../../../includes/class.yarns.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id = $_SESSION['logic_erp']["user_id"];

$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );

if($db_type==2) $select_date=" to_char(a.insert_date,'YYYY')";
else if ($db_type==0) $select_date=" year(a.insert_date)";

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 130, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}

if ($action=="job_popup")
{
  	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
	<script>
        function js_set_value(str)
        {
            $("#hide_job_no").val(str);
            parent.emailwindow.hide(); 
        }
    </script>
<?
	if($buyer_id==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_cond="";
		}
		else
		{
			$buyer_cond="";
		}
	}
	else
	{
		$buyer_cond=" and a.buyer_name=$buyer_id";
	}
	if(trim($cbo_year)!=0) 
	{
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)=$cbo_year";
			$year_field="YEAR(insert_date)";
		}
		else
		{
			$year_cond=" and to_char(insert_date,'YYYY')=$cbo_year";	
			$year_field="to_char(insert_date,'YYYY')";
		}
	}
	else $year_cond="";

	$arr=array (2=>$company_library,3=>$buyer_arr);
	$sql= "select a.job_no_prefix_num, a.job_no, a.company_name,a.buyer_name,a.style_ref_no,$year_field as year from wo_po_details_master a where a.company_name=$company_id $buyer_cond $year_cond order by a.id";
	//echo $sql;
	echo  create_list_view("list_view", "Job No,Year,Company,Buyer Name,Style Ref. No", "70,70,120,100,100","570","320",0, $sql , "js_set_value", "year,job_no", "", 1, "0,0,company_name,buyer_name,0", $arr , "job_no_prefix_num,year,company_name,buyer_name,style_ref_no", "","setFilterGrid('list_view',-1)",'0,0,0,0,0');
	echo "<input type='hidden' id='hide_job_no' />";
	
	exit();
}

//style search------------------------------//
if($action=="style_refarence_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	//echo $style_id;die;
	?>
    <script>
		var selected_id = new Array;
		var selected_name = new Array;
		var selected_no = new Array;
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
			var splitSTR = strCon.split("_");
			var str = splitSTR[0];
			var selectID = splitSTR[1];
			var selectDESC = splitSTR[2];
			//$('#txt_individual_id' + str).val(splitSTR[1]);
			//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
			
			toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( selectID, selected_id ) == -1 ) {
				selected_id.push( selectID );
				selected_name.push( selectDESC );
				selected_no.push( str );				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == selectID ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
				selected_no.splice( i, 1 ); 
			}
			var id = ''; var name = ''; var job = ''; var num='';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
				num += selected_no[i] + ','; 
			}
			id 		= id.substr( 0, id.length - 1 );
			name 	= name.substr( 0, name.length - 1 ); 
			num 	= num.substr( 0, num.length - 1 );
			//alert(num);
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name ); 
			$('#txt_selected_no').val( num );
		}
    </script>
    <?
	$buyer=str_replace("'","",$buyer);
	$company=str_replace("'","",$company);
	$cbo_year=str_replace("'","",$cbo_year);
	if($db_type==0) if($cbo_year!=0) $job_cond=" and year(a.insert_date)='$cbo_year'";
	else if($cbo_year!=0) $job_cond=" and to_char(a.insert_date,'YYYY')='$cbo_year'";
	
	if($buyer!=0) $buyer_cond="and a.buyer_name=$buyer"; else $buyer_cond="";
	$sql = "select a.id,a.style_ref_no,a.job_no,a.job_no_prefix_num,$select_date as job_year from wo_po_details_master a where a.company_name=$company $buyer_cond $job_cond  and is_deleted=0 order by job_no_prefix_num"; 
	//echo $sql; die;
	echo create_list_view("list_view", "Style Ref No,Job No,Year","160,90,100","400","310",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "0", $arr, "style_ref_no,job_no_prefix_num,job_year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	echo "<input type='hidden' id='txt_selected_no' />";
	?>
    <script language="javascript" type="text/javascript">
	var style_no='<? echo $txt_style_ref_no;?>';
	var style_id='<? echo $txt_style_ref_id;?>';
	var style_des='<? echo $txt_style_ref;?>';
	//alert(style_id);
	if(style_no!="")
	{
		style_no_arr=style_no.split(",");
		style_id_arr=style_id.split(",");
		style_des_arr=style_des.split(",");
		var str_ref="";
		for(var k=0;k<style_no_arr.length; k++)
		{
			str_ref=style_no_arr[k]+'_'+style_id_arr[k]+'_'+style_des_arr[k];
			js_set_value(str_ref);
		}
	}
	</script>
    <?
	exit();
}

if($action=="order_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	?>
    <script>
		var selected_id = new Array;
		var selected_name = new Array;
		var selected_no = new Array;
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
			var splitSTR = strCon.split("_");
			var str_or = splitSTR[0];
			var selectID = splitSTR[1];
			var selectDESC = splitSTR[2];
			//$('#txt_individual_id' + str).val(splitSTR[1]);
			//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
			
			toggle( document.getElementById( 'tr_' + str_or ), '#FFFFCC' );
			
			if( jQuery.inArray( selectID, selected_id ) == -1 ) {
				selected_id.push( selectID );
				selected_name.push( selectDESC );
				selected_no.push( str_or );				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == selectID ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
				selected_no.splice( i, 1 ); 
			}
			var id = ''; var name = ''; var job = ''; var num='';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
				num += selected_no[i] + ','; 
			}
			id 		= id.substr( 0, id.length - 1 );
			name 	= name.substr( 0, name.length - 1 ); 
			num 	= num.substr( 0, num.length - 1 );
			//alert(num);
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name ); 
			$('#txt_selected_no').val( num );
		}
    </script>
    <?
	$buyer=str_replace("'","",$buyer);
	$company=str_replace("'","",$company);
	$txt_style_ref=str_replace("'","",$txt_style_ref);
	$cbo_year=str_replace("'","",$cbo_year);
	
	if($buyer!=0) $buyer_cond="and b.buyer_name=$buyer"; else $buyer_cond="";
	if($txt_style_ref!="")
	{
		if($db_type==0) $style_cond="and b.job_no_prefix_num in($txt_style_ref) and year(b.insert_date)= '$cbo_year' "; 
		else $style_cond="and b.job_no_prefix_num in($txt_style_ref) and to_char(b.insert_date,'YYYY')= '$cbo_year' ";
	}
	else $style_cond="";
	
	//echo $style_cond."jahid";die;
	$sql = "select a.id,a.po_number,a.job_no_mst,b.style_ref_no,b.job_no_prefix_num,$select_date as job_year from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and b.company_name=$company $buyer_cond  $style_cond and a.status_active=1"; 
	//echo $sql;
	echo create_list_view("list_view", "Order NO,Job No,Year,Style Ref No","150,80,70,150","500","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "po_number,job_no_prefix_num,job_year,style_ref_no", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	echo "<input type='hidden' id='txt_selected_no' />";
	?>
    <script language="javascript" type="text/javascript">
	var style_no='<? echo $txt_order_id_no;?>';
	var style_id='<? echo $txt_order_id;?>';
	var style_des='<? echo $txt_order;?>';
	//alert(style_id);
	if(style_no!="")
	{
		style_no_arr=style_no.split(",");
		style_id_arr=style_id.split(",");
		style_des_arr=style_des.split(",");
		var str_ref="";
		for(var k=0;k<style_no_arr.length; k++)
		{
			str_ref=style_no_arr[k]+'_'+style_id_arr[k]+'_'+style_des_arr[k];
			js_set_value(str_ref);
		}
	}
	</script>
    <?
	exit();
}

if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	$cbo_search_type=str_replace("'","",$cbo_search_type);
	$txt_style_ref=str_replace("'","",$txt_style_ref);
	$txt_order=str_replace("'","",$txt_order);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	$txt_ex_date_form=str_replace("'","",$txt_ex_date_form);
	$txt_ex_date_to=str_replace("'","",$txt_ex_date_to);
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";
	}
	
	$cbo_year=str_replace("'","",$cbo_year);
	if(trim($cbo_year)!=0) 
	{
		if($db_type==0) $year_cond=" and YEAR(a.insert_date)=$cbo_year";
		else $year_cond=" and to_char(a.insert_date,'YYYY')=$cbo_year";	
	}
	$ship_date_cond="";
	if($txt_date_from!="" && $txt_date_to!="")
	{
		$ship_date_cond="and b.shipment_date between '$txt_date_from' and '$txt_date_to' ";
	}
	$ex_fact_date_cond="";
	if($txt_ex_date_form!="" && $txt_ex_date_to!="")
	{
		$ex_fact_date_cond="and c.ex_factory_date between '$txt_ex_date_form' and '$txt_ex_date_to' ";
	}
	
	$job_no_cond="";
	if(trim($txt_style_ref)!="") $job_no_cond="and a.job_no_prefix_num  in($txt_style_ref)";
	$order_cond="";
	if(trim($txt_order)!="") $order_cond="and b.po_number='$txt_order'";
	if($db_type==0) $select_job_year="year(a.insert_date) as job_year"; else $select_job_year="to_char(a.insert_date,'YYYY') as job_year";
	if($txt_ex_date_form!="" && $txt_ex_date_to!="")
	{
		$sql_po="select a.buyer_name, a.job_no, a.job_no_prefix_num, $select_job_year, a.style_ref_no, a.total_set_qnty as ratio, b.id as po_id, b.po_number, b.po_quantity as po_qnty, b.plan_cut, b.pub_shipment_date, c.ex_factory_date, c.ex_factory_qnty
		from wo_po_details_master a, wo_po_break_down b, pro_ex_factory_mst c 
		where a.job_no=b.job_no_mst and  b.id=c.po_break_down_id and a.company_name=$cbo_company_name and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 $buyer_id_cond $year_cond $job_no_cond $order_cond $ex_fact_date_cond order by b.pub_shipment_date, b.id";
	}
	else
	{
		$sql_po="select a.buyer_name, a.job_no, a.job_no_prefix_num, $select_job_year, a.style_ref_no, a.total_set_qnty as ratio, b.id as po_id, b.po_number, b.po_quantity as po_qnty, b.plan_cut, b.pub_shipment_date, c.ex_factory_date, c.ex_factory_qnty 
		from wo_po_details_master a, wo_po_break_down b left join pro_ex_factory_mst c on b.id=c.po_break_down_id
		where a.job_no=b.job_no_mst and a.company_name=$cbo_company_name and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 $buyer_id_cond $year_cond $job_no_cond $order_cond $ship_date_cond order by b.pub_shipment_date, b.id";
	}
	
	$sql_po_result=sql_select($sql_po);
	$result_data_arr=$result_job_wise=array();$all_po_id=""; $JobArr=array();
	foreach($sql_po_result as $row)
	{
		if($all_po_id=="") $all_po_id=$row[csf("po_id")]; else $all_po_id.=",".$row[csf("po_id")];
		$result_data_arr[$row[csf("po_id")]]["po_id"]=$row[csf("po_id")];
		$result_data_arr[$row[csf("po_id")]]["buyer_name"]=$row[csf("buyer_name")];
		$result_data_arr[$row[csf("po_id")]]["job_no"]=$row[csf("job_no")];
		$result_data_arr[$row[csf("po_id")]]["job_no_prefix_num"]=$row[csf("job_no_prefix_num")];
		$result_data_arr[$row[csf("po_id")]]["job_year"]=$row[csf("job_year")];
		$result_data_arr[$row[csf("po_id")]]["style_ref_no"]=$row[csf("style_ref_no")];
		$result_data_arr[$row[csf("po_id")]]["ratio"]=$row[csf("ratio")];
		$result_data_arr[$row[csf("po_id")]]["po_number"]=$row[csf("po_number")];
		$result_data_arr[$row[csf("po_id")]]["po_qnty"]=$row[csf("po_qnty")]*$row[csf("ratio")];
		$result_data_arr[$row[csf("po_id")]]["plan_cut"]=$row[csf("plan_cut")];
		$result_data_arr[$row[csf("po_id")]]["pub_shipment_date"]=$row[csf("pub_shipment_date")];
		$result_data_arr[$row[csf("po_id")]]["ex_factory_date"]=$row[csf("ex_factory_date")];
		$result_data_arr[$row[csf("po_id")]]["ex_factory_qnty"]+=$row[csf("ex_factory_qnty")];
		$result_job_wise[$row[csf("job_no")]].=$row[csf("po_id")].",";
		$JobArr[]="'".$row[csf('job_no')]."'";
	}
	$yarn= new yarn($JobArr,'job');
	$yarn_qty_arr=$yarn->getOrderWiseYarnQtyArray();
	//print_r($yarn_qty_arr);
	
	$booking_req_arr=array();
	$sql_wo=sql_select("select b.po_break_down_id, 
	sum(CASE WHEN a.fabric_source=1 THEN b.grey_fab_qnty ELSE 0 END) AS grey_req_qnty,
	sum(b.fin_fab_qnty) as fin_fab_qnty  from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.po_break_down_id in ($all_po_id) group by b.po_break_down_id");
	foreach ($sql_wo as $brow)
	{
		$booking_req_arr[$brow[csf("po_break_down_id")]]['gray']=$brow[csf("grey_req_qnty")];
		$booking_req_arr[$brow[csf("po_break_down_id")]]['fin']=$brow[csf("fin_fab_qnty")];
	}
	
	
	$all_po_id=implode(",",array_unique(explode(",",$all_po_id)));
	$dataArrayYarnReq=array();
	$yarn_sql="select job_no, sum(avg_cons_qnty) as qnty from wo_pre_cost_fab_yarn_cost_dtls where status_active=1 and is_deleted=0 group by job_no";
	$resultYarn=sql_select($yarn_sql);
	foreach($resultYarn as $yarnRow)
	{
		$dataArrayYarnReq[$yarnRow[csf('job_no')]]=$yarnRow[csf('qnty')];
	}	
	
	$reqDataArray=sql_select("select  a.po_break_down_id, sum((b.requirment/b.pcs)*a.plan_cut_qnty) as grey_req, sum((b.cons/b.pcs)*a.plan_cut_qnty) as finish_req from wo_po_color_size_breakdown a, wo_pre_cos_fab_co_avg_con_dtls b where a.po_break_down_id=b.po_break_down_id and a.color_number_id=b.color_number_id and a.size_number_id=b.gmts_sizes and a.is_deleted=0 and a.status_active=1 and a.po_break_down_id in ($all_po_id) group by a.po_break_down_id");
	$grey_finish_require_arr=array();
	foreach($reqDataArray as $row)
	{
		$grey_finish_require_arr[$row[csf("po_break_down_id")]]["grey_req"]=$row[csf("grey_req")];
		$grey_finish_require_arr[$row[csf("po_break_down_id")]]["finish_req"]=$row[csf("finish_req")];
	}
	
	$yarnDataArr=sql_select("select a.po_breakdown_id, 
						sum(CASE WHEN a.entry_form=3 and c.entry_form=3 and c.knit_dye_source!=3 THEN a.quantity ELSE 0 END) AS issue_qnty_in,
						sum(CASE WHEN a.entry_form=3 and c.entry_form=3 and c.knit_dye_source=3 THEN a.quantity ELSE 0 END) AS issue_qnty_out
						from order_wise_pro_details a, inv_transaction b, inv_issue_master c 
						where a.trans_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and b.item_category=1 and c.issue_purpose!=2 and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 
						group by a.po_breakdown_id");
	$yarn_issue_arr=array();
	foreach($yarnDataArr as $row)
	{
		$yarn_issue_arr[$row[csf("po_breakdown_id")]]["issue_qnty_in"]=$row[csf("issue_qnty_in")];
		$yarn_issue_arr[$row[csf("po_breakdown_id")]]["issue_qnty_out"]=$row[csf("issue_qnty_out")];
	}
						
	$yarnReturnDataArr=sql_select("select a.po_breakdown_id,  
				sum(CASE WHEN a.entry_form=9 and c.entry_form=9 and c.knitting_source!=3 THEN a.quantity ELSE 0 END) AS return_qnty_in,
				sum(CASE WHEN a.entry_form=9 and c.entry_form=9 and c.knitting_source=3 THEN a.quantity ELSE 0 END) AS return_qnty_out
				from order_wise_pro_details a, inv_transaction b, inv_receive_master c 
				where a.trans_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and b.item_category=1 and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 
				group by a.po_breakdown_id");
	
	
	$yarn_issue_rtn_arr=array();
	foreach($yarnReturnDataArr as $row)
	{
		$yarn_issue_rtn_arr[$row[csf("po_breakdown_id")]]["return_qnty_in"]=$row[csf("return_qnty_in")];
		$yarn_issue_rtn_arr[$row[csf("po_breakdown_id")]]["return_qnty_out"]=$row[csf("return_qnty_out")];
	}
	
	
	$dataArrayTrans=sql_select("select po_breakdown_id,
								sum(CASE WHEN entry_form ='11' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_yarn,
								sum(CASE WHEN entry_form ='11' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_yarn,
								sum(CASE WHEN entry_form ='13' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_knit,
								sum(CASE WHEN entry_form ='13' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_knit,
								sum(CASE WHEN entry_form ='15' and trans_type=5 THEN quantity ELSE 0 END) AS transfer_in_qnty_finish,
								sum(CASE WHEN entry_form ='15' and trans_type=6 THEN quantity ELSE 0 END) AS transfer_out_qnty_finish
							from order_wise_pro_details where status_active=1 and is_deleted=0 and entry_form in(11,13,15) and po_breakdown_id in($all_po_id) 
							group by po_breakdown_id");
							
	$transfer_data_arr=array();
	foreach($dataArrayTrans as $row)
	{
		$transfer_data_arr[$row[csf("po_breakdown_id")]]["transfer_in_qnty_yarn"]=$row[csf("transfer_in_qnty_yarn")];
		$transfer_data_arr[$row[csf("po_breakdown_id")]]["transfer_out_qnty_yarn"]=$row[csf("transfer_out_qnty_yarn")];
		$transfer_data_arr[$row[csf("po_breakdown_id")]]["transfer_in_qnty_knit"]=$row[csf("transfer_in_qnty_knit")];
		$transfer_data_arr[$row[csf("po_breakdown_id")]]["transfer_out_qnty_knit"]=$row[csf("transfer_out_qnty_knit")];
		$transfer_data_arr[$row[csf("po_breakdown_id")]]["transfer_in_qnty_finish"]=$row[csf("transfer_in_qnty_finish")];
		$transfer_data_arr[$row[csf("po_breakdown_id")]]["transfer_out_qnty_finish"]=$row[csf("transfer_out_qnty_finish")];
	}
	
	// decision pending	dyed yearn receive	
	//$greyYarnIssueQnty=return_library_array("select c.po_breakdown_id, sum(c.quantity) as issue_qnty from inv_transaction a, inv_issue_master b,  order_wise_pro_details c where a.mst_id=b.id and a.id=c.trans_id and b.entry_form=3 and b.issue_basis=1 and b.issue_purpose=2 and c.po_breakdown_id in($all_po_id) and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=2 group by c.po_breakdown_id","po_breakdown_id","issue_qnty");
	
	
	//$dyedYarnRecvQnty=return_field_value("sum(a.cons_quantity) as recv_qnty","inv_transaction a, inv_receive_master b","a.mst_id=b.id and b.entry_form=1 and b.receive_basis=2 and b.receive_purpose=2 and a.job_no='$job_no' and a.status_active=1 and a.is_deleted=0 and a.item_category=1 and a.transaction_type=1","recv_qnty");
	
	$prodKnitDataArr=sql_select("select a.po_breakdown_id,  
				sum(CASE WHEN c.knitting_source!=3 THEN a.quantity ELSE 0 END) AS knit_qnty_in,
				sum(CASE WHEN c.knitting_source=3 THEN a.quantity ELSE 0 END) AS knit_qnty_out
				from order_wise_pro_details a, pro_grey_prod_entry_dtls b, inv_receive_master c where a.dtls_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and c.item_category=13 and a.entry_form in(2,22) and c.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 and c.receive_basis<>9 group by a.po_breakdown_id");
	$kniting_prod_arr=array();
	foreach($prodKnitDataArr as $row)
	{
		$kniting_prod_arr[$row[csf("po_breakdown_id")]]["knit_qnty_in"]=$row[csf("knit_qnty_in")];
		$kniting_prod_arr[$row[csf("po_breakdown_id")]]["knit_qnty_out"]=$row[csf("knit_qnty_out")];
	}
	
	$prodFinDataArr=sql_select("select a.po_breakdown_id,  
				sum(CASE WHEN c.knitting_source!=3 THEN a.quantity ELSE 0 END) AS finish_qnty_in,
				sum(CASE WHEN c.knitting_source=3 THEN a.quantity ELSE 0 END) AS finish_qnty_out
				from order_wise_pro_details a, pro_finish_fabric_rcv_dtls b, inv_receive_master c where a.dtls_id=b.id and b.mst_id=c.id and a.po_breakdown_id in($all_po_id) and c.item_category=2 and a.entry_form in(7,37,66) and c.entry_form in(7,37,66) and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 and c.receive_basis<>9 group by a.po_breakdown_id");
	
	$finish_prod_arr=array();
	foreach($prodFinDataArr as $row)
	{
		$finish_prod_arr[$row[csf("po_breakdown_id")]]["finish_qnty_in"]=$row[csf("finish_qnty_in")];
		$finish_prod_arr[$row[csf("po_breakdown_id")]]["finish_qnty_out"]=$row[csf("finish_qnty_out")];
	}
	$issueData=sql_select("select po_breakdown_id, 
							sum(CASE WHEN entry_form=16 THEN quantity ELSE 0 END) AS grey_issue_qnty,
							sum(CASE WHEN entry_form=61 THEN quantity ELSE 0 END) AS grey_issue_qnty_roll_wise,
							sum(CASE WHEN entry_form=18 THEN quantity ELSE 0 END) AS issue_to_cut_qnty,
							sum(CASE WHEN entry_form=71 THEN quantity ELSE 0 END) AS issue_to_cut_qnty_roll_wise
							from order_wise_pro_details where po_breakdown_id in($all_po_id) and status_active=1 and is_deleted=0 group by po_breakdown_id");
	
	
	$grey_cut_issue_arr=array();
	foreach($issueData as $row)
	{
		$grey_cut_issue_arr[$row[csf("po_breakdown_id")]]["grey_issue_qnty"]=$row[csf("grey_issue_qnty")]+$row[csf("grey_issue_qnty_roll_wise")];
		$grey_cut_issue_arr[$row[csf("po_breakdown_id")]]["issue_to_cut_qnty"]=$row[csf("issue_to_cut_qnty")]+$row[csf("issue_to_cut_qnty_roll_wise")];
	}
	$trimsDataArr=sql_select("select a.po_breakdown_id,  
							sum(CASE WHEN a.entry_form=24 THEN a.quantity ELSE 0 END) AS recv_qnty,
							sum(CASE WHEN a.entry_form=25 THEN a.quantity ELSE 0 END) AS issue_qnty
							from order_wise_pro_details a, product_details_master b where a.prod_id=b.id and a.po_breakdown_id in($all_po_id) and b.item_category_id=4 and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 group by a.po_breakdown_id");
	foreach($trimsDataArr as $row)	
	{
		$trims_array[$row[csf('po_breakdown_id')]]['recv']=$row[csf('recv_qnty')];
		$trims_array[$row[csf('po_breakdown_id')]]['iss']=$row[csf('issue_qnty')];
	}
	
	$gmtsProdDataArr=sql_select("select  po_break_down_id, 
					sum(CASE WHEN production_type=1 THEN production_quantity ELSE 0 END) AS cutting_qnty,
					sum(CASE WHEN production_type=2 and embel_name=1 and production_source=1 THEN production_quantity ELSE 0 END) AS print_issue_qnty_in,
					sum(CASE WHEN production_type=2 and embel_name=1 and production_source=3 THEN production_quantity ELSE 0 END) AS print_issue_qnty_out,
					sum(CASE WHEN production_type=3 and embel_name=1 and production_source=1 THEN production_quantity ELSE 0 END) AS print_recv_qnty_in,
					sum(CASE WHEN production_type=3 and embel_name=1 and production_source=3 THEN production_quantity ELSE 0 END) AS print_recv_qnty_out,
					sum(CASE WHEN production_type=2 and embel_name=2 and production_source=1 THEN production_quantity ELSE 0 END) AS emb_issue_qnty_in,
					sum(CASE WHEN production_type=2 and embel_name=2 and production_source=3 THEN production_quantity ELSE 0 END) AS emb_issue_qnty_out,
					sum(CASE WHEN production_type=3 and embel_name=2 and production_source=1 THEN production_quantity ELSE 0 END) AS emb_recv_qnty_in,
					sum(CASE WHEN production_type=3 and embel_name=2 and production_source=3 THEN production_quantity ELSE 0 END) AS emb_recv_qnty_out,
					sum(CASE WHEN production_type=4 and production_source=1 THEN production_quantity ELSE 0 END) AS sew_input_qnty_in,
					sum(CASE WHEN production_type=4 and production_source=3 THEN production_quantity ELSE 0 END) AS sew_input_qnty_out,
					sum(CASE WHEN production_type=5 and production_source=1 THEN production_quantity ELSE 0 END) AS sew_recv_qnty_in,
					sum(CASE WHEN production_type=5 and production_source=3 THEN production_quantity ELSE 0 END) AS sew_recv_qnty_out,
					sum(CASE WHEN production_type=8 and production_source=1 THEN production_quantity ELSE 0 END) AS finish_qnty_in,
					sum(CASE WHEN production_type=8 and production_source=3 THEN production_quantity ELSE 0 END) AS finish_qnty_out,
					sum(CASE WHEN production_type=3 and embel_name=3 and production_source=1 THEN production_quantity ELSE 0 END) AS wash_recv_qnty_in,
					sum(CASE WHEN production_type=3 and embel_name=3 and production_source=3 THEN production_quantity ELSE 0 END) AS wash_recv_qnty_out,
					sum(CASE WHEN production_type=3 and embel_name=1 THEN reject_qnty ELSE 0 END) AS print_reject_qnty,
					sum(CASE WHEN production_type=3 and embel_name=2 THEN reject_qnty ELSE 0 END) AS emb_reject_qnty,
					sum(CASE WHEN production_type=5 THEN reject_qnty ELSE 0 END) AS sew_reject_qnty,
					sum(CASE WHEN production_type=8 THEN reject_qnty ELSE 0 END) AS finish_reject_qnty
					from pro_garments_production_mst where po_break_down_id in($all_po_id) and is_deleted=0 and status_active=1 group by po_break_down_id");
					
	$garment_prod_data_arr=array();
	foreach($gmtsProdDataArr as $row)
	{
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['cutting_qnty']=$row[csf("cutting_qnty")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['print_issue_qnty_in']=$row[csf("print_issue_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['print_issue_qnty_out']=$row[csf("print_issue_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['print_recv_qnty_in']=$row[csf("print_recv_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['print_recv_qnty_out']=$row[csf("print_recv_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['emb_issue_qnty_in']=$row[csf("emb_issue_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['emb_issue_qnty_out']=$row[csf("emb_issue_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['emb_recv_qnty_in']=$row[csf("emb_recv_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['emb_recv_qnty_out']=$row[csf("emb_recv_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['sew_input_qnty_in']=$row[csf("sew_input_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['sew_input_qnty_out']=$row[csf("sew_input_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['sew_recv_qnty_in']=$row[csf("sew_recv_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['sew_recv_qnty_out']=$row[csf("sew_recv_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['finish_qnty_in']=$row[csf("finish_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['finish_qnty_out']=$row[csf("finish_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['wash_recv_qnty_in']=$row[csf("wash_recv_qnty_in")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['wash_recv_qnty_out']=$row[csf("wash_recv_qnty_out")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['print_reject_qnty']=$row[csf("print_reject_qnty")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['emb_reject_qnty']=$row[csf("emb_reject_qnty")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['sew_reject_qnty']=$row[csf("sew_reject_qnty")];
		$garment_prod_data_arr[$row[csf("po_break_down_id")]]['finish_reject_qnty']=$row[csf("finish_reject_qnty")];
	}
	if(empty($all_po_id))
	{
		echo '<div align="left" style="width:1000px;"><h1 align="center" style="color:#f00;">Order not found</h></div>'; die;
	}
	if($cbo_search_type==1)
	{
		$tbl_width=6200;
		
		$ship_date_html="Shipment Date";
		$ex_fact_date_html="Ex-Fact. Date";
	}
	else
	{
		$tbl_width=5960;
		$ship_date_html="Last Shipment Date";
		$ex_fact_date_html="Last Ex-Fact. Date";
	}
	?>
        <div style="width:100%">
             <table width="<? echo $tbl_width;?>">
                <tr>
                    <td align="center" width="100%" colspan="70" class="form_caption"><? echo $company_library[str_replace("'","",$cbo_company_name)]; ?></td>
                </tr>
            </table>
            <table width="<? echo $tbl_width;?>" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_header_1">
                <thead>
                	<tr style="font-size:13px">
                        <th width="40" >SL</th>
                        <th width="110" >Buyer</th>
                        <th width="50" >Job Year</th>
                        <th width="50" >Job No</th>
                        <th width="100" >Style No</th>
                        <?
						if($cbo_search_type==1)
						{
							?>
                        	<th width="100" >Order No</th>
							<?
						}
						?>
                        <th width="80" >Order Qty. (Pcs)</th>
                        <?
						if($cbo_search_type==1)
						{
							?>
							<th width="70" ><? echo $ship_date_html; ?></th>
							<th width="70"><? echo $ex_fact_date_html; ?></th>
							<?
						}
						?>
                        <th width="80">Yarn Req.<br/><font style="font-size:9px; font-weight:100">(As Per Pre-Cost)</font></th>
                        <th width="80">Yarn Issued In</th>
                        <th width="80">Yarn Issued Out</th>
                        <th width="80">Yarn Trans In</th>
                        <th width="80">Yarn Trans Out</th>
                        <th width="80">Yarn Total Issued</th>
                        <th width="80">Yarn Under or Over Issued</th>
                        
                        <th width="80">Knit. Gray Fab Req. <br/><font style="font-size:9px; font-weight:100">(As Per Booking)</font></th>
                        <th width="80">Knit. Prod Inside</th>
                        <th width="80">Knit. Prod SubCon</th>
                        <th width="80">Knit. Trans. In</th>
                        <th width="80">Knit. Trans. Out</th>
                        <th width="80">Knit. Total Prod.</th>
                        <th width="80">Knit. Process Loss</th>
                        <th width="80">Knit. Under or Over Prod.</th>
                        <th width="80">Knit. Issued To Dyeing</th>
                        <th width="80">Knit. Left Over</th>
                        
                        <th width="80">Fin Fab Req. <br/><font style="font-size:9px; font-weight:100">(As Per Booking)</font></th>
                        <th width="80">Fin Prod. Inside</th>
                        <th width="80">Fin Prod. SubCon</th>
                        <th width="80">Fin Trans. In</th>
                        <th width="80">Fin Trans. Out</th>
                        <th width="80">Fin Prod. Total</th>
                        <th width="80">Fin Process Loss</th>
                        <th width="80">Fin Under or Over</th>
                        <th width="80">Fin Issue To Cut</th>
                        <th width="80">Fin Left Over</th>
                        
                        <th width="80">Gmts. Req (Po Qty)</th>
                        <th width="80">Gmts. Print Issued Inside</th>
                        <th width="80">Gmts. Print Issued SubCon</th>
                        <th width="80">Gmts. Total Print Issued</th>
                        <th width="80">Gmts. Print Rec. Inside</th>
                        <th width="80">Gmts. Print Rec. SubCon</th>
                        <th width="80">Gmts. Total Rec. Print</th>
                        <th width="80">Gmts. Reject</th>
                        
                        <th width="80">Sew. Input Inside</th>
                        <th width="80">Sew. Input SubCon</th>
                        <th width="80">Total Sew. Input</th>
                        <th width="80">Sew. Input Balance</th>
                        
                        <th width="100">Accessories Status</th>
                        
                        <th width="80">Sew. Out Inside</th>
                        <th width="80">Sew Out SubCon</th>
                        <th width="80">Total Out Sew</th>
                        <th width="80">Sew Out Balance</th>
                        <th width="80">Sew Out Reject</th>
                        
                        <th width="80">Wash Inside</th>
                        <th width="80">Wash SubCon</th>
                        <th width="80">Total Wash</th>
                        <th width="80">Wash Balance</th>
                        
                        <th width="80">Finish Inside</th>
                        <th width="80">Finish SubCon</th>
                        <th width="80">Total Finish</th>
                        <th width="80">Finish Balance</th>
                        <th width="80">Finish Reject</th>
                        <th width="80">Ex-Factory</th>
                        <th width="80">Left Over</th>
                        
                        <th width="80">Gray Fab.</th>
                        <th width="80">Finish Fab.</th>
                        <th width="80">Garment</th>
                        <th width="80">Trims</th>
                        
                        <th width="80">Print Gmts.</th>
                        <th width="80">Emb Gmts.</th>
                        <th width="80">Sewing Gmts.</th>
                        <th width="80">Finishing Gmts.</th>
                        
                        <th width="80">Process Loss Yarn</th>
                        <th width="80">Process Loss Dyeing</th>
                        <th>Process Loss Cutting</th>
                    </tr>
                </thead>
           </table>
            <div style="width:<? echo $tbl_width+17;?>px; overflow-y:scroll; max-height:300px;font-size:12px; overflow-x:hidden;" id="scroll_body">
            <table width="<? echo $tbl_width;?>" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
				
                <?
					$i=1;
					if($cbo_search_type==1)
					{
						foreach($result_data_arr as $po_id=>$val)
						{
							$ratio=$val["ratio"];
							$tot_po_qnty=$val["po_qnty"];
							$exfactory_qnty=$val["ex_factory_qnty"];
							$plan_cut_qty=$val["plan_cut"];
							$job_no=$val["job_no"];
							$dzn_qnty=0; $balance=0; $job_mkt_required=0; $yarn_issued=0;
							if($costing_per_id_library[$job_no]==1) $dzn_qnty=12;
							else if($costing_per_id_library[$job_no]==3) $dzn_qnty=12*2;
							else if($costing_per_id_library[$job_no]==4) $dzn_qnty=12*3;
							else if($costing_per_id_library[$job_no]==5) $dzn_qnty=12*4;
							else $dzn_qnty=1;
							$dzn_qnty=$dzn_qnty*$ratio;
							
							$yarn_req_job=$yarn_qty_arr[$po_id];//$dataArrayYarnReq[$job_no];
							$yarn_required=$yarn_qty_arr[$po_id];//$plan_cut_qty*($yarn_req_job/$dzn_qnty);
							$yarn_issue_inside=$yarn_issue_arr[$po_id]["issue_qnty_in"]-$yarn_issue_rtn_arr[$po_id]["return_qnty_in"]; 
							$yarn_issue_outside=$yarn_issue_arr[$po_id]["issue_qnty_out"]-$yarn_issue_rtn_arr[$po_id]["return_qnty_out"]; 
							$transfer_in_qnty_yarn=$transfer_data_arr[$po_id]["transfer_in_qnty_yarn"];
							$transfer_out_qnty_yarn=$transfer_data_arr[$po_id]["transfer_out_qnty_yarn"];
							$total_issued=$yarn_issue_inside+$yarn_issue_outside+$transfer_in_qnty_yarn-$transfer_out_qnty_yarn;
							$under_over_issued=$yarn_required-$total_issued;
							
							$grey_fabric_req_qnty=$booking_req_arr[$po_id]['gray'];//$grey_finish_require_arr[$po_id]["grey_req"];
							$knit_qnty_in=$kniting_prod_arr[$po_id]["knit_qnty_in"];
							$knit_qnty_out=$kniting_prod_arr[$po_id]["knit_qnty_out"];
							$transfer_in_qnty_knit=$transfer_data_arr[$po_id]["transfer_in_qnty_knit"];
							$transfer_out_qnty_knit=$transfer_data_arr[$po_id]["transfer_out_qnty_knit"];
							$total_knitting=$knit_qnty_in+$knit_qnty_out+$transfer_in_qnty_knit-$transfer_out_qnty_knit;
							$process_loss=($knit_qnty_in+$knit_qnty_out)-$total_knitting;
							$under_over_prod=$grey_fabric_req_qnty-$total_knitting;
							$issuedToDyeQnty=$grey_cut_issue_arr[$po_id]["grey_issue_qnty"];
							$left_over=$total_knitting-$issuedToDyeQnty;
							
							$finish_fabric_req_qnty=$booking_req_arr[$po_id]['fin'];//$grey_finish_require_arr[$po_id]["finish_req"];
							$finish_qnty_in=$finish_prod_arr[$po_id]["finish_qnty_in"];
							$finish_qnty_out=$finish_prod_arr[$po_id]["finish_qnty_out"];
							$transfer_in_qnty_finish=$transfer_data_arr[$po_id]["transfer_in_qnty_finish"];
							$transfer_out_qnty_finish=$transfer_data_arr[$po_id]["transfer_out_qnty_finish"];
							$total_finishing=$finish_qnty_in+$finish_qnty_out+$transfer_in_qnty_finish-$transfer_out_qnty_finish;
							//$process_loss_finishing=($finish_qnty_in+$finish_qnty_out)-$total_finishing;

$process_loss_finishing=((($total_knitting-$finish_fabric_req_qnty)/$total_knitting)*100);

							$under_over_finish_prod=$finish_fabric_req_qnty-$total_finishing;
							$issuedToCutQnty=$grey_cut_issue_arr[$po_id]["issue_to_cut_qnty"];
							$finish_left_over=$total_finishing-$issuedToCutQnty;
							
							$print_issue_qnty_in=$garment_prod_data_arr[$po_id]['print_issue_qnty_in'];
							$print_issue_qnty_out=$garment_prod_data_arr[$po_id]['print_issue_qnty_out'];
							$total_print_issued=$print_issue_qnty_in+$print_issue_qnty_out;
							$print_recv_qnty_in=$garment_prod_data_arr[$po_id]['print_recv_qnty_in'];
							$print_recv_qnty_out=$garment_prod_data_arr[$po_id]['print_recv_qnty_out'];
							$total_print_recv=$print_recv_qnty_in+$print_recv_qnty_out;
							$print_reject_qnty=$garment_prod_data_arr[$po_id]['print_reject_qnty'];
							
							$sew_input_qnty_in=$garment_prod_data_arr[$po_id]['sew_input_qnty_in'];
							$sew_input_qnty_out=$garment_prod_data_arr[$po_id]['sew_input_qnty_out'];
							$total_sew_input=$sew_input_qnty_in+$sew_input_qnty_out;
							$sew_input_balance_qnty=$tot_po_qnty-$total_sew_input;
							
							$sew_recv_qnty_in=$garment_prod_data_arr[$po_id]['sew_recv_qnty_in'];
							$sew_recv_qnty_out=$garment_prod_data_arr[$po_id]['sew_recv_qnty_out'];
							$total_sew_recv=$sew_recv_qnty_in+$sew_recv_qnty_out;
							$sew_balance_recv_qnty=$tot_po_qnty-$total_sew_recv;
							$sew_reject_qnty=$garment_prod_data_arr[$po_id]['sew_reject_qnty'];
							
							$wash_recv_qnty_in=$garment_prod_data_arr[$po_id]['wash_recv_qnty_in'];
							$wash_recv_qnty_out=$garment_prod_data_arr[$po_id]['wash_recv_qnty_out'];
							$total_wash_recv=$wash_recv_qnty_in+$wash_recv_qnty_out;
							$wash_balance_qnty=$tot_po_qnty-$total_wash_recv;
							
							$gmt_finish_in=$garment_prod_data_arr[$po_id]['finish_qnty_in'];
							$gmt_finish_out=$garment_prod_data_arr[$po_id]['finish_qnty_out'];
							$total_gmts_finish_qnty=$gmt_finish_in+$gmt_finish_out;
							$finish_balance_qnty=$tot_po_qnty-$total_gmts_finish_qnty;
							$finish_reject_qnty=$garment_prod_data_arr[$po_id]['finish_reject_qnty'];
							$left_over_finish_gmts=$total_gmts_finish_qnty-$exfactory_qnty;
							
							
						
							$trims_recv=$trims_array[$po_id]['recv'];
							$trims_issue=$trims_array[$po_id]['iss'];
							$tot_trims_left_over_qnty=$trims_recv+$trims_issue;
							
							$emb_reject_qnty=$garment_prod_data_arr[$po_id]['emb_reject_qnty'];
							?>
							<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>" style="font-size:13px">
								<td width="40"><? echo $i; ?></td>
								<td width="110"><div style="word-wrap:break-word; width:110px"><? echo $buyer_arr[$val["buyer_name"]]; ?></div></td>
								<td width="50" align="center"><p><? echo $val["job_year"]; ?>&nbsp;</p></td>
								<td width="50" align="center"><p><? echo $val["job_no_prefix_num"]; ?>&nbsp;</p></td>
								<td width="100"><div style="word-wrap:break-word; width:100px"><? echo $val["style_ref_no"]; ?></div></td>
								<td width="100"><div style="word-wrap:break-word; width:100px"><? echo $val["po_number"]; ?></div></td>
								<td width="80" align="right" bgcolor="#FFFFCC"><p><? echo number_format($tot_po_qnty); ?></p></td>
								<td width="70"><p><? if(trim($val["pub_shipment_date"])!="" && trim($val["pub_shipment_date"])!='0000-00-00') echo change_date_format($val["pub_shipment_date"]); ?>&nbsp;</p></td>
								<td width="70"><p><? if(trim($val["ex_factory_date"])!="" && trim($val["ex_factory_date"])!='0000-00-00') echo change_date_format($val["ex_factory_date"]); ?>&nbsp;</p></td>
								
								<td align="right" width="80"><? echo number_format($yarn_required,2); ?></td>
								<td align="right" width="80"><? echo number_format($yarn_issue_inside,2); ?></td>
								<td align="right" width="80"><? echo number_format($yarn_issue_outside,2);?></td>
								<td align="right" width="80"><? echo number_format($transfer_in_qnty_yarn,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_out_qnty_yarn,2); ?></td>
								<td align="right" width="80"><? echo number_format($total_issued,2); ?></td>
								<td align="right" width="80"><? echo number_format($under_over_issued,2); ?></td>
								
								<td align="right" width="80"><? echo number_format($grey_fabric_req_qnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($knit_qnty_in,2); ?></td>
								<td align="right" width="80"><? echo number_format($knit_qnty_out,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_in_qnty_knit,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_out_qnty_knit,2); ?></td>
								<td align="right" width="80"><? echo number_format($total_knitting,2); ?></td>
								<td align="right" width="80"><? echo number_format($process_loss,2); ?></td>
								<td align="right" width="80"><? echo number_format($under_over_prod,2); ?></td>
								<td align="right" width="80"><? echo number_format($issuedToDyeQnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($left_over,2); ?></td>
								
								<td align="right" width="80"><? echo number_format($finish_fabric_req_qnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($finish_qnty_in,2); ?></td>
								<td align="right" width="80"><? echo number_format($finish_qnty_out,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_in_qnty_finish,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_out_qnty_finish,2); ?></td>
								<td align="right" width="80"><? echo number_format($total_finishing,2); ?></td>
								<td align="right" width="80"><? echo number_format($process_loss_finishing,2); ?></td>
								<td align="right" width="80"><? echo number_format($under_over_finish_prod,2); ?></td>
								<td align="right" width="80"><? echo number_format($issuedToCutQnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($finish_left_over,2); ?></td>
								
								<td align="right" width="80" bgcolor="#FFFFCC"><? echo number_format($tot_po_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($print_issue_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($print_issue_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_print_issued); ?></td>
								<td align="right" width="80"><? echo number_format($print_recv_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($print_recv_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_print_recv); ?></td>
								<td align="right" width="80"><? echo number_format($print_reject_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($sew_input_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($sew_input_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_sew_input); ?></td>
								<td align="right" width="80"><? echo number_format($sew_input_balance_qnty); ?></td>
								
								<td align="center" width="100"><a href="javascript:open_trims_dtls('<? echo $po_id;?>','<? echo $tot_po_qnty; ?>','<? echo $ratio; ?>','Trims Info','trims_popup')">View</a></td>
								
								<td align="right" width="80"><? echo number_format($sew_recv_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($sew_recv_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_sew_recv); ?></td>
								<td align="right" width="80"><? echo number_format($sew_balance_recv_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($sew_reject_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($wash_recv_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($wash_recv_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_wash_recv); ?></td>
								<td align="right" width="80"><? echo number_format($wash_balance_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($gmt_finish_in); ?></td>
								<td align="right" width="80"><? echo number_format($gmt_finish_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_gmts_finish_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($finish_balance_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($finish_reject_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($exfactory_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($left_over_finish_gmts); ?></td>
								
								<td align="right" width="80"><? echo number_format($left_over); ?></td>
								<td align="right" width="80"><? echo number_format($finish_left_over); ?></td>
								<td align="right" width="80"><? echo number_format($left_over_finish_gmts); ?></td>
								<td align="right" width="80"><? echo number_format($tot_trims_left_over_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($print_reject_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($emb_reject_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($sew_reject_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($finish_reject_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($process_loss); ?></td>
								<td align="right" width="80"><? echo number_format($process_loss_finishing); ?></td>
								<td align="right"><? echo number_format($cutting_process_loss); ?></td>
							</tr>
							<?
							$tot_order_qty+=$tot_po_qnty;
							$tot_yarn_req_qty+=$yarn_required;
							$tot_yarn_issIn_qty+=$yarn_issue_inside;
							$tot_yarn_issOut_qty+=$yarn_issue_outside;
							$tot_yarn_trnsInq_qty+=$transfer_in_qnty_yarn;
							$tot_yarn_trnsOut_qty+=$transfer_out_qnty_yarn;
							$tot_yarn_issue_qty+=$total_issued;
							$tot_yarn_undOvr_qty+=$under_over_issued;
							
							$tot_grey_req_qty+=$grey_fabric_req_qnty;
							$tot_grey_in_qty+=$knit_qnty_in;
							$tot_grey_out_qty+=$knit_qnty_out;
							$tot_grey_trnsIn_qty+=$transfer_in_qnty_knit;
							$tot_grey_transOut_qty+=$transfer_out_qnty_knit;
							$tot_grey_qty+=$total_knitting;
							$tot_grey_prLoss_qty+=$process_loss;
							$tot_grey_undOver_qty+=$under_over_prod;
							$tot_grey_issDye_qty+=$issuedToDyeQnty;
							$tot_grey_lftOver_qty+=$left_over;
							
							$tot_fin_req_qty+=$finish_fabric_req_qnty;
							$tot_fin_in_qty+=$finish_qnty_in;
							$tot_fin_out_qty+=$finish_qnty_out;
							$tot_fin_transIn_qty+=$transfer_in_qnty_finish;
							$tot_fin_transOut_qty+=$transfer_out_qnty_finish;
							$tot_fin_qty+=$total_finishing;
							$tot_fin_prLoss_qty+=$process_loss_finishing;
							$tot_fin_undOver_qty+=$under_over_finish_prod;
							$tot_fin_issCut_qty+=$issuedToCutQnty;
							$tot_fin_lftOver_qty+=$finish_left_over;
							
							$tot_gmt_qty+=$tot_po_qnty;
							$tot_printIssIn_qty+=$print_issue_qnty_in;
							$tot_printIssOut_qty+=$print_issue_qnty_out;
							$tot_printIssue_qty+=$total_print_issued;
							$tot_printRcvIn_qty+=$print_recv_qnty_in;
							$tot_printRcvOut_qty+=$print_recv_qnty_out;
							$tot_printRcv_qty+=$total_print_recv;
							$tot_printRjt_qty+=$print_reject_qnty;
							
							$tot_sewInInput_qty+=$sew_input_qnty_in;
							$tot_sewInOutput_qty+=$sew_input_qnty_out;
							$tot_sewIn_qty+=$total_sew_input;
							$tot_sewInBal_qty+=$sew_input_balance_qnty;
							
							$tot_sewRcvIn_qty+=$sew_recv_qnty_in;
							$tot_sewRcvOut_qty+=$sew_recv_qnty_out;
							$tot_sewRcv_qty+=$total_sew_recv;
							$tot_sewRcvBal_qty+=$sew_balance_recv_qnty;
							$tot_sewRcvRjt_qty+=$sew_reject_qnty;
							
							$tot_washRcvIn_qty+=$wash_recv_qnty_in;
							$tot_washRcvOut_qty+=$wash_recv_qnty_out;
							$tot_washRcv_qty+=$total_wash_recv;
							$tot_washRcvBal_qty+=$wash_balance_qnty;
							
							$tot_gmtFinIn_qty+=$gmt_finish_in;
							$tot_gmtFinOut_qty+=$gmt_finish_out;
							$tot_gmtFin_qty+=$total_gmts_finish_qnty;
							$tot_gmtFinBal_qty+=$finish_balance_qnty;
							$tot_gmtFinRjt_qty+=$finish_reject_qnty;
							$tot_gmtEx_qty+=$exfactory_qnty;
							$tot_gmtFinLeftOver_qty+=$left_over_finish_gmts;
							
							$tot_leftOver_qty+=$left_over;
							$tot_leftOverFin_qty+=$finish_left_over;
							$tot_leftOverGmtFin_qty+=$left_over_finish_gmts;
							$tot_leftOverTrm_qty+=$tot_trims_left_over_qnty;
							
							$tot_rjtPrint_qty+=$print_reject_qnty;
							$tot_rjtEmb_qty+=$emb_reject_qnty;
							$tot_rjtSew_qty+=$sew_reject_qnty;
							$tot_rjtFin_qty+=$finish_reject_qnty;
							
							$tot_prLoss_qty+=$process_loss;
							$tot_prLossFin_qty+=$process_loss_finishing;
							$tot_prLossCut_qty+=$cutting_process_loss;
							$i++;
						}
					}
					else
					{
						foreach($result_job_wise as $po_id_job)
						{
							$po_id_arr=array_unique(explode(",",substr($po_id_job,0,-1)));
							$tot_po_qnty=$yarn_required=$tot_exfactory_qnty=$grey_fabric_req_qnty=$finish_fabric_req_qnty=$yarn_issue_inside=$yarn_issue_outside=$transfer_in_qnty_yarn=$transfer_in_qnty_yarn=$transfer_out_qnty_yarn=$transfer_in_qnty_knit=$transfer_in_qnty_finish=$transfer_out_qnty_finish=$knit_qnty_in=$issuedToDyeQnty=$issuedToCutQnty=$finish_qnty_in=$finish_qnty_out=$finish_reject_qnty=$print_issue_qnty_in=$print_issue_qnty_out=$print_recv_qnty_in=$print_recv_qnty_out=$print_reject_qnty=$sew_input_qnty_in=$sew_input_qnty_out=$sew_recv_qnty_in=$sew_recv_qnty_out=$sew_reject_qnty=$wash_recv_qnty_in=$wash_recv_qnty_out=$trims_recv=$trims_issue=$emb_reject_qnty=$gmt_finish_in=$gmt_finish_out=$gmt_finish_reject_qnty=0;
							foreach($po_id_arr as $po_id)
							{
								$tot_po_qnty +=$result_data_arr[$po_id]["po_qnty"];
								$tot_exfactory_qnty +=$result_data_arr[$po_id]["ex_factory_qnty"];
								$yarn_required+=$yarn_qty_arr[$po_id];
								$grey_fabric_req_qnty +=$booking_req_arr[$po_id]['gray'];;
								$finish_fabric_req_qnty +=$booking_req_arr[$po_id]['fin'];
								
								$yarn_issue_inside +=$yarn_issue_arr[$po_id]["issue_qnty_in"]-$yarn_issue_rtn_arr[$po_id]["return_qnty_in"]; 
								$yarn_issue_outside +=$yarn_issue_arr[$po_id]["issue_qnty_out"]-$yarn_issue_rtn_arr[$po_id]["return_qnty_out"]; 
								$transfer_in_qnty_yarn +=$transfer_data_arr[$po_id]["transfer_in_qnty_yarn"];
								$transfer_out_qnty_yarn +=$transfer_data_arr[$po_id]["transfer_out_qnty_yarn"];
								$transfer_in_qnty_knit +=$transfer_data_arr[$po_id]["transfer_in_qnty_knit"];
								$transfer_out_qnty_knit +=$transfer_data_arr[$po_id]["transfer_out_qnty_knit"];
								$transfer_in_qnty_finish +=$transfer_data_arr[$po_id]["transfer_in_qnty_finish"];
								$transfer_out_qnty_finish +=$transfer_data_arr[$po_id]["transfer_out_qnty_finish"];
								
								$total_issued =$yarn_issue_inside+$yarn_issue_outside+$transfer_in_qnty_yarn-$transfer_out_qnty_yarn;
								$under_over_issued =$grey_fabric_req_qnty-$total_issued;
								
								$knit_qnty_in +=$kniting_prod_arr[$po_id]["knit_qnty_in"];
								$knit_qnty_out +=$kniting_prod_arr[$po_id]["knit_qnty_out"];
								$total_knitting=$knit_qnty_in+$knit_qnty_out+$transfer_in_qnty_knit-$transfer_out_qnty_knit;
								$process_loss=($knit_qnty_in+$knit_qnty_out)-$total_issued;
								$under_over_prod=$grey_fabric_req_qnty-$total_knitting;
								$issuedToDyeQnty +=$grey_cut_issue_arr[$po_id]["grey_issue_qnty"];	
								$left_over=$total_knitting-$issuedToDyeQnty;
								
								
								$issuedToCutQnty +=$grey_cut_issue_arr[$po_id]["issue_to_cut_qnty"];
								
								$finish_qnty_in +=$finish_prod_arr[$po_id]["finish_qnty_in"];
								$finish_qnty_out +=$finish_prod_arr[$po_id]["finish_qnty_out"];
								$total_finish_qnty=$finish_qnty_in+$finish_qnty_out;
								$finish_balance_qnty=$tot_po_qnty-$total_finish_qnty;
								$finish_reject_qnty +=$garment_prod_data_arr[$po_id]['finish_reject_qnty'];
								$left_over_finish_gmts=$total_finish_qnty-$tot_exfactory_qnty;
								
								$total_finishing=$finish_qnty_in+$finish_qnty_out+$transfer_in_qnty_finish-$transfer_out_qnty_finish;
								$process_loss_finishing=($finish_qnty_in+$finish_qnty_out)-$total_finishing;
								$under_over_finish_prod=$finish_fabric_req_qnty-$total_finishing;
								$finish_left_over=$total_finishing-$issuedToCutQnty;
								
								$print_issue_qnty_in +=$garment_prod_data_arr[$po_id]['print_issue_qnty_in'];
								$print_issue_qnty_out +=$garment_prod_data_arr[$po_id]['print_issue_qnty_out'];
								$print_recv_qnty_in +=$garment_prod_data_arr[$po_id]['print_recv_qnty_in'];
								$print_recv_qnty_out +=$garment_prod_data_arr[$po_id]['print_recv_qnty_out'];
								$print_reject_qnty +=$garment_prod_data_arr[$po_id]['print_reject_qnty'];	
								
								$total_print_issued=$print_issue_qnty_in+$print_issue_qnty_out;
								$total_print_recv=$print_recv_qnty_in+$print_recv_qnty_out;
								
								$sew_input_qnty_in +=$garment_prod_data_arr[$po_id]['sew_input_qnty_in'];
								$sew_input_qnty_out +=$garment_prod_data_arr[$po_id]['sew_input_qnty_out'];
								$total_sew_issued=$sew_input_qnty_in+$sew_input_qnty_out;
								$sew_balance_qnty=$tot_po_qnty-$total_sew_issued;
								
								
								$sew_recv_qnty_in +=$garment_prod_data_arr[$po_id]['sew_recv_qnty_in'];
								$sew_recv_qnty_out +=$garment_prod_data_arr[$po_id]['sew_recv_qnty_out'];
								$total_sew_recv=$sew_recv_qnty_in+$sew_recv_qnty_out;
								
								
								$sew_balance_recv_qnty=$tot_po_qnty-$total_sew_recv;
								
								$sew_reject_qnty +=$garment_prod_data_arr[$po_id]['sew_reject_qnty'];
								
								$wash_recv_qnty_in +=$garment_prod_data_arr[$po_id]['wash_recv_qnty_in'];
								$wash_recv_qnty_out +=$garment_prod_data_arr[$po_id]['wash_recv_qnty_out'];
								$total_wash_recv=$wash_recv_qnty_in+$wash_recv_qnty_out;
								$wash_balance_qnty=$tot_po_qnty-$total_wash_recv;
								
								$gmt_finish_in+=$garment_prod_data_arr[$po_id]['finish_qnty_in'];
								$gmt_finish_out+=$garment_prod_data_arr[$po_id]['finish_qnty_out'];
								$total_gmts_finish_qnty=$gmt_finish_in+$gmt_finish_out;
								$finish_balance_qnty=$tot_po_qnty-$total_gmts_finish_qnty;
								$gmt_finish_reject_qnty+=$garment_prod_data_arr[$po_id]['finish_reject_qnty'];
								$left_over_finish_gmts=$total_gmts_finish_qnty-$tot_exfactory_qnty;
							
								$trims_recv+=$trims_array[$po_id]['recv'];
								$trims_issue+=$trims_array[$po_id]['iss'];
								$tot_trims_left_over_qnty=$trims_recv+$trims_issue;
								
								$emb_reject_qnty +=$garment_prod_data_arr[$po_id]['emb_reject_qnty'];
							}
							?>
                            <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>" style="font-size:13px">
								<td width="40"><? echo $i; ?></td>
								<td width="110"><div style="word-wrap:break-word; width:110px"><? echo $buyer_arr[$result_data_arr[$po_id]["buyer_name"]]; ?></div></td>
								<td width="50" align="center"><? echo $result_data_arr[$po_id]["job_year"]; ?></td>
								<td width="50" align="center"><? echo $result_data_arr[$po_id]["job_no_prefix_num"]; ?></td>
								<td width="100"><div style="word-wrap:break-word; width:100px"><? echo $result_data_arr[$po_id]["style_ref_no"]; ?></div></td>
								<td width="80" align="right" bgcolor="#FFFFCC"><? echo number_format($tot_po_qnty,0); ?></td>
								
								<td align="right" width="80"><? echo number_format($yarn_required,2); ?></td>
								<td align="right" width="80"><? echo number_format($yarn_issue_inside,2); ?></td>
								<td align="right" width="80"><? echo number_format($yarn_issue_outside,2);?></td>
								<td align="right" width="80"><? echo number_format($transfer_in_qnty_yarn,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_out_qnty_yarn,2); ?></td>
								<td align="right" width="80"><? echo number_format($total_issued,2); ?></td>
								<td align="right" width="80"><? echo number_format($under_over_issued,2); ?></td>
								
								<td align="right" width="80"><? echo number_format($grey_fabric_req_qnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($knit_qnty_in,2); ?></td>
								<td align="right" width="80"><? echo number_format($knit_qnty_out,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_in_qnty_knit,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_out_qnty_knit,2); ?></td>
								<td align="right" width="80"><? echo number_format($total_knitting,2); ?></td>
								<td align="right" width="80"><? echo number_format($process_loss,2); ?></td>
								<td align="right" width="80"><? echo number_format($under_over_prod,2); ?></td>
								<td align="right" width="80"><? echo number_format($issuedToDyeQnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($left_over,2); ?></td>
								
								<td align="right" width="80"><? echo number_format($finish_fabric_req_qnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($finish_qnty_in,2); ?></td>
								<td align="right" width="80"><? echo number_format($finish_qnty_out,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_in_qnty_finish,2); ?></td>
								<td align="right" width="80"><? echo number_format($transfer_out_qnty_finish,2); ?></td>
								<td align="right" width="80"><? echo number_format($total_finishing,2); ?></td>
								<td align="right" width="80"><? echo number_format($process_loss_finishing,2); ?></td>
								<td align="right" width="80"><? echo number_format($under_over_finish_prod,2); ?></td>
								<td align="right" width="80"><? echo number_format($issuedToCutQnty,2); ?></td>
								<td align="right" width="80"><? echo number_format($finish_left_over,2); ?></td>
								
								<td align="right" width="80" bgcolor="#FFFFCC"><? echo number_format($tot_po_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($print_issue_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($print_issue_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_print_issued); ?></td>
								<td align="right" width="80"><? echo number_format($print_recv_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($print_recv_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_print_recv); ?></td>
								<td align="right" width="80"><? echo number_format($print_reject_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($sew_input_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($sew_input_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_sew_issued); ?></td>
								<td align="right" width="80"><? echo number_format($sew_balance_qnty); ?></td>
								
								<td width="100" align="center">View</td>
								
								<td align="right" width="80"><? echo number_format($sew_recv_qnty_in); ?></td>
								<td align="right" width="80"><? echo number_format($sew_recv_qnty_out); ?></td>
								<td align="right" width="80"><? echo number_format($total_sew_recv); ?></td>
								<td align="right" width="80"><? echo number_format($sew_balance_recv_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($sew_reject_qnty); ?></td>
								
								<td width="80" align="right"><? echo number_format($wash_recv_qnty_in); ?></td>
								<td width="80" align="right"><? echo number_format($wash_recv_qnty_out); ?></td>
								<td width="80" align="right"><? echo number_format($total_wash_recv); ?></td>
								<td width="80" align="right"><? echo number_format($wash_balance_qnty); ?></td>
								
								<td width="80" align="right"><? echo number_format($gmt_finish_in); ?></td>
								<td width="80" align="right"><? echo number_format($gmt_finish_out); ?></td>
								<td width="80" align="right"><? echo number_format($total_gmts_finish_qnty); ?></td>
								<td width="80" align="right"><? echo number_format($finish_balance_qnty); ?></td>
								<td width="80" align="right"><? echo number_format($gmt_finish_reject_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($tot_exfactory_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($left_over_finish_gmts); ?></td>
								
								<td align="right" width="80"><? echo number_format($left_over); ?></td>
								<td align="right" width="80"><? echo number_format($finish_left_over); ?></td>
								<td align="right" width="80"><? echo number_format($left_over_finish_gmts); ?></td>
								<td align="right" width="80"><? echo number_format($tot_trims_left_over_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($print_reject_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($emb_reject_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($sew_reject_qnty); ?></td>
								<td align="right" width="80"><? echo number_format($finish_reject_qnty); ?></td>
								
								<td align="right" width="80"><? echo number_format($process_loss); ?></td>
								<td align="right" width="80"><? echo number_format($process_loss_finishing); ?></td>
								<td align="right"><? echo number_format($cutting_process_loss); ?></td>
							</tr>
                            <?
							$tot_order_qty+=$tot_po_qnty;
							$tot_yarn_req_qty+=$yarn_required;
							$tot_yarn_issIn_qty+=$yarn_issue_inside;
							$tot_yarn_issOut_qty+=$yarn_issue_outside;
							$tot_yarn_trnsInq_qty+=$transfer_in_qnty_yarn;
							$tot_yarn_trnsOut_qty+=$transfer_out_qnty_yarn;
							$tot_yarn_issue_qty+=$total_issued;
							$tot_yarn_undOvr_qty+=$under_over_issued;
							
							$tot_grey_req_qty+=$grey_fabric_req_qnty;
							$tot_grey_in_qty+=$knit_qnty_in;
							$tot_grey_out_qty+=$knit_qnty_out;
							$tot_grey_trnsIn_qty+=$transfer_in_qnty_knit;
							$tot_grey_transOut_qty+=$transfer_out_qnty_knit;
							$tot_grey_qty+=$total_knitting;
							$tot_grey_prLoss_qty+=$process_loss;
							$tot_grey_undOver_qty+=$under_over_prod;
							$tot_grey_issDye_qty+=$issuedToDyeQnty;
							$tot_grey_lftOver_qty+=$left_over;
							
							$tot_fin_req_qty+=$finish_fabric_req_qnty;
							$tot_fin_in_qty+=$finish_qnty_in;
							$tot_fin_out_qty+=$finish_qnty_out;
							$tot_fin_transIn_qty+=$transfer_in_qnty_finish;
							$tot_fin_transOut_qty+=$transfer_out_qnty_finish;
							$tot_fin_qty+=$total_finishing;
							$tot_fin_prLoss_qty+=$process_loss_finishing;
							$tot_fin_undOver_qty+=$under_over_finish_prod;
							$tot_fin_issCut_qty+=$issuedToCutQnty;
							$tot_fin_lftOver_qty+=$finish_left_over;
							
							$tot_gmt_qty+=$tot_po_qnty;
							$tot_printIssIn_qty+=$print_issue_qnty_in;
							$tot_printIssOut_qty+=$print_issue_qnty_out;
							$tot_printIssue_qty+=$total_print_issued;
							$tot_printRcvIn_qty+=$print_recv_qnty_in;
							$tot_printRcvOut_qty+=$print_recv_qnty_out;
							$tot_printRcv_qty+=$total_print_recv;
							$tot_printRjt_qty+=$print_reject_qnty;
							
							$tot_sewInInput_qty+=$sew_input_qnty_in;
							$tot_sewInOutput_qty+=$sew_input_qnty_out;
							$tot_sewIn_qty+=$total_sew_issued;
							$tot_sewInBal_qty+=$sew_balance_qnty;
							
							$tot_sewRcvIn_qty+=$sew_recv_qnty_in;
							$tot_sewRcvOut_qty+=$sew_recv_qnty_out;
							$tot_sewRcv_qty+=$total_sew_recv;
							$tot_sewRcvBal_qty+=$sew_balance_recv_qnty;
							$tot_sewRcvRjt_qty+=$sew_reject_qnty;
							
							$tot_washRcvIn_qty+=$wash_recv_qnty_in;
							$tot_washRcvOut_qty+=$wash_recv_qnty_out;
							$tot_washRcv_qty+=$total_wash_recv;
							$tot_washRcvBal_qty+=$wash_balance_qnty;
							
							$tot_gmtFinIn_qty+=$gmt_finish_in;
							$tot_gmtFinOut_qty+=$gmt_finish_out;
							$tot_gmtFin_qty+=$total_gmts_finish_qnty;
							$tot_gmtFinBal_qty+=$finish_balance_qnty;
							$tot_gmtFinRjt_qty+=$gmt_finish_reject_qnty;
							$tot_gmtEx_qty+=$tot_exfactory_qnty;
							$tot_gmtFinLeftOver_qty+=$left_over_finish_gmts;
							
							$tot_leftOver_qty+=$left_over;
							$tot_leftOverFin_qty+=$finish_left_over;
							$tot_leftOverGmtFin_qty+=$left_over_finish_gmts;
							$tot_leftOverTrm_qty+=$tot_trims_left_over_qnty;
							
							$tot_rjtPrint_qty+=$print_reject_qnty;
							$tot_rjtEmb_qty+=$emb_reject_qnty;
							$tot_rjtSew_qty+=$sew_reject_qnty;
							$tot_rjtFin_qty+=$finish_reject_qnty;
							
							$tot_prLoss_qty+=$process_loss;
							$tot_prLossFin_qty+=$process_loss_finishing;
							$tot_prLossCut_qty+=$cutting_process_loss;
							$i++;
						}
					}
					?>
                </table> 
            </div>
            <table width="<? echo $tbl_width;?>" class="tbl_bottom" cellpadding="0" cellspacing="0" border="1" rules="all">
                <tr style="font-size:13px">
                    <td width="40">&nbsp;</td>
                    <td width="110">&nbsp;</td>
                    <td width="50">&nbsp;</td>
                    <td width="50">&nbsp;</td>
                    <td width="100">Total :</td>
                    <?
                    if($cbo_search_type==1)
                    {
                        ?>
                        <td width="100">&nbsp;</td>
                        <?
                    }
                    ?>
                    <td width="80" align="right" id="td_order_qty" bgcolor="#FFFFCC"><? echo number_format($tot_order_qty); ?></td>
                    <?
                    if($cbo_search_type==1)
                    {
                        ?>
                        <td width="70">&nbsp;</td>
                        <td width="70">&nbsp;</td>
                        <?
                    }
                    ?>
                    <td width="80" align="right" id="td_yarn_req_qty"><? echo number_format($tot_yarn_req_qty,2); ?></td>
                    <td width="80" align="right" id="td_yarn_issIn_qty"><? echo number_format($tot_yarn_issIn_qty,2); ?></td>
                    <td width="80" align="right" id="td_yarn_issOut_qty"><? echo number_format($tot_yarn_issOut_qty,2); ?></td>
                    <td width="80" align="right" id="td_yarn_trnsInq_qty"><? echo number_format($tot_yarn_trnsInq_qty,2); ?></td>
                    <td width="80" align="right" id="td_yarn_trnsOut_qty"><? echo number_format($tot_yarn_trnsOut_qty,2); ?></td>
                    <td width="80" align="right" id="td_yarn_issue_qty"><? echo number_format($tot_yarn_issue_qty,2); ?></td>
                    <td width="80" align="right" id="td_yarn_undOvr_qty"><? echo number_format($tot_yarn_undOvr_qty,2); ?></td>
                    
                    <td width="80" align="right" id="td_grey_req_qty"><? echo number_format($tot_grey_req_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_in_qty"><? echo number_format($tot_grey_in_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_out_qty"><? echo number_format($tot_grey_out_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_trnsIn_qty"><? echo number_format($tot_grey_trnsIn_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_transOut_qty"><? echo number_format($tot_grey_transOut_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_qty"><? echo number_format($tot_grey_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_prLoss_qty"><? echo number_format($tot_grey_prLoss_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_undOver_qty"><? echo number_format($tot_grey_undOver_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_issDye_qty"><? echo number_format($tot_grey_issDye_qty,2); ?></td>
                    <td width="80" align="right" id="td_grey_lftOver_qty"><? echo number_format($tot_grey_lftOver_qty,2); ?></td>
                    
                    <td width="80" align="right" id="td_fin_req_qty"><? echo number_format($tot_fin_req_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_in_qty"><? echo number_format($tot_fin_in_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_out_qty"><? echo number_format($tot_fin_out_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_transIn_qty"><? echo number_format($tot_fin_transIn_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_transOut_qty"><? echo number_format($tot_fin_transOut_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_qty"><? echo number_format($tot_fin_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_prLoss_qty"><? echo number_format($tot_fin_prLoss_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_undOver_qty"><? echo number_format($tot_fin_undOver_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_issCut_qty"><? echo number_format($tot_fin_issCut_qty,2); ?></td>
                    <td width="80" align="right" id="td_fin_lftOver_qty"><? echo number_format($tot_fin_lftOver_qty,2); ?></td>
                    
                    <td width="80" align="right" id="td_gmt_qty" bgcolor="#FFFFCC"><? echo number_format($tot_gmt_qty); ?></td>
                    <td width="80" align="right" id="td_printIssIn_qty"><? echo number_format($tot_printIssIn_qty); ?></td>
                    <td width="80" align="right" id="td_printIssOut_qty"><? echo number_format($tot_printIssOut_qty); ?></td>
                    <td width="80" align="right" id="td_printIssue_qty"><? echo number_format($tot_printIssue_qty); ?></td>
                    <td width="80" align="right" id="td_printRcvIn_qty"><? echo number_format($tot_printRcvIn_qty); ?></td>
                    <td width="80" align="right" id="td_printRcvOut_qty"><? echo number_format($tot_printRcvOut_qty); ?></td>
                    <td width="80" align="right" id="td_printRcv_qty"><? echo number_format($tot_printRcv_qty); ?></td>
                    <td width="80" align="right" id="td_printRjt_qty"><? echo number_format($tot_printRjt_qty); ?></td>
                    
                    <td width="80" align="right" id="td_sewInInput_qty"><? echo number_format($tot_sewInInput_qty); ?></td>
                    <td width="80" align="right" id="td_sewInOutput_qty"><? echo number_format($tot_sewInOutput_qty); ?></td>
                    <td width="80" align="right" id="td_sewIn_qty"><? echo number_format($tot_sewIn_qty); ?></td>
                    <td width="80" align="right" id="td_sewInBal_qty"><? echo number_format($tot_sewInBal_qty); ?></td>
                    
                    <td width="100">&nbsp;</td>
                    
                    <td width="80" align="right" id="td_sewRcvIn_qty"><? echo number_format($tot_sewRcvIn_qty); ?></td>
                    <td width="80" align="right" id="td_sewRcvOut_qty"><? echo number_format($tot_sewRcvOut_qty); ?></td>
                    <td width="80" align="right" id="td_sewRcv_qty"><? echo number_format($tot_sewRcv_qty); ?></td>
                    <td width="80" align="right" id="td_sewRcvBal_qty"><? echo number_format($tot_sewRcvBal_qty); ?></td>
                    <td width="80" align="right" id="td_sewRcvRjt_qty"><? echo number_format($tot_sewRcvRjt_qty); ?></td>
                    
                    <td width="80" align="right" id="td_washRcvIn_qty"><? echo number_format($tot_washRcvIn_qty); ?></td>
                    <td width="80" align="right" id="td_washRcvOut_qty"><? echo number_format($tot_washRcvOut_qty); ?></td>
                    <td width="80" align="right" id="td_washRcv_qty"><? echo number_format($tot_washRcv_qty); ?></td>
                    <td width="80" align="right" id="td_washRcvBal_qty"><? echo number_format($tot_washRcvBal_qty); ?></td>
                    
                    <td width="80" align="right" id="td_gmtFinIn_qty"><? echo number_format($tot_gmtFinIn_qty); ?></td>
                    <td width="80" align="right" id="td_gmtFinOut_qty"><? echo number_format($tot_gmtFinOut_qty); ?></td>
                    <td width="80" align="right" id="td_gmtFin_qty"><? echo number_format($tot_gmtFin_qty); ?></td>
                    <td width="80" align="right" id="td_gmtFinBal_qty"><? echo number_format($tot_gmtFinBal_qty); ?></td>
                    <td width="80" align="right" id="td_gmtFinRjt_qty"><? echo number_format($tot_gmtFinRjt_qty); ?></td>
                    <td width="80" align="right" id="td_gmtEx_qty"><? echo number_format($tot_gmtEx_qty); ?></td>
                    <td width="80" align="right" id="td_gmtFinLeftOver_qty"><? echo number_format($tot_gmtFinLeftOver_qty); ?></td>
                    
                    <td width="80" align="right" id="td_rjtPrint_qty"><? echo number_format($tot_rjtPrint_qty); ?></td>
                    <td width="80" align="right" id="td_leftOverFin_qty"><? echo number_format($tot_leftOverFin_qty); ?></td>
                    <td width="80" align="right" id="td_leftOverGmtFin_qty"><? echo number_format($tot_leftOverGmtFin_qty); ?></td>
                    <td width="80" align="right" id="td_leftOverTrm_qty"><? echo number_format($tot_leftOverTrm_qty); ?></td>
                    
                    <td width="80" align="right" id="td_rjtPrint_qty"><? echo number_format($tot_rjtPrint_qty); ?></td>
                    <td width="80" align="right" id="td_rjtEmb_qty"><? echo number_format($tot_rjtEmb_qty); ?></td>
                    <td width="80" align="right" id="td_rjtSew_qty"><? echo number_format($tot_rjtSew_qty); ?></td>
                    <td width="80" align="right" id="td_rjtFin_qty"><? echo number_format($tot_rjtFin_qty); ?></td>
                    
                    <td width="80" align="right" id="td_prLoss_qty"><? echo number_format($tot_prLoss_qty); ?></td>
                    <td width="80" align="right" id="td_prLossFin_qty"><? echo number_format($tot_prLossFin_qty); ?></td>
                    <td align="right" id="td_prLossCut_qty"><? echo number_format($tot_prLossCut_qty); ?></td>
                </tr>
           </table>
        </div>
<?
	$html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename**$cbo_search_type"; 
    exit();
}

if($action=='trims_popup')
{
	echo load_html_head_contents("Trims Details info", "../../../../", 1, 1,$unicode,'','');
 	extract($_REQUEST);
	//echo $po_break_down_id."*".$tot_po_qnty;die;
	
	//echo $ratio;die;

?>	
<script>

	function window_close()
	{
		parent.emailwindow.hide();
	}
	
</script>	
<fieldset style="width:650px;" >
<legend>Accessories Status pop up</legend>
    <div style="100%" id="report_container">
       <table width="650" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <tr>
                    <th colspan="7">Accessories Status</th>
                </tr>
                <tr>
                    <th width="110">Item</th>
                    <th width="70">UOM</th>
                    <th width="90">Req. Qty.</th>
                    <th width="90">Received</th>
                    <th width="90">Recv. Balance</th>
                    <th width="90">Issued</th>
                    <th>Left Over</th>
                </tr>
            </thead>
            <?
			$item_library=return_library_array( "select id, item_name from lib_item_group", "id", "item_name"  );
			$trims_array=array();
			$trimsDataArr=sql_select("select b.item_group_id,  
									sum(CASE WHEN a.entry_form=24 THEN a.quantity ELSE 0 END) AS recv_qnty,
									sum(CASE WHEN a.entry_form=25 THEN a.quantity ELSE 0 END) AS issue_qnty
									from order_wise_pro_details a, product_details_master b where a.prod_id=b.id and a.po_breakdown_id in($po_break_down_id) and b.item_category_id=4 and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 group by b.item_group_id");
			foreach($trimsDataArr as $row)	
			{
				$trims_array[$row[csf('item_group_id')]]['recv']=$row[csf('recv_qnty')];
				$trims_array[$row[csf('item_group_id')]]['iss']=$row[csf('issue_qnty')];
			}
			
			
			//$costing_per_arr=return_library_array( "select job_no, costing_per from wo_pre_cost_mst", "job_no", "costing_per"  );
			$trimsDataArr=sql_select("select c.po_break_down_id, max(a.costing_per) as costing_per, b.trim_group, max(b.cons_uom) as cons_uom, sum(b.cons_dzn_gmts) cons_dzn_gmts from wo_pre_cost_mst a, wo_pre_cost_trim_cost_dtls b , wo_pre_cost_trim_co_cons_dtls c where a.job_no=b.job_no and b.id=c.wo_pre_cost_trim_cost_dtls_id and b.status_active=1 and b.is_deleted=0 and c.po_break_down_id=$po_break_down_id group by b.trim_group, c.po_break_down_id");
			$i=1; $tot_accss_req_qnty=0; $tot_recv_qnty=0; $tot_iss_qnty=0; $tot_recv_bl_qnty=0; $tot_trims_left_over_qnty=0;
			foreach($trimsDataArr as $row)
			{
				if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$dzn_qnty='';
				if($row[csf('costing_per')]==1) $dzn_qnty=12;
				else if($row[csf('costing_per')]==3) $dzn_qnty=12*2;
				else if($row[csf('costing_per')]==4) $dzn_qnty=12*3;
				else if($row[csf('costing_per')]==5) $dzn_qnty=12*4;
				else $dzn_qnty=1;
				
				$dzn_qnty=$dzn_qnty*$ratio;
        		$accss_req_qnty=($row[csf('cons_dzn_gmts')]/$dzn_qnty)*$tot_po_qnty;
				
				$trims_recv=$trims_array[$row[csf('trim_group')]]['recv'];
				$trims_issue=$trims_array[$row[csf('trim_group')]]['iss'];
				$recv_bl=$accss_req_qnty-$trims_recv;
				$trims_left_over=$trims_recv-$trims_issue;
			?>
            	<tr bgcolor="<? echo $bgcolor; ?>">
                    <td><p><? echo $item_library[$row[csf('trim_group')]]; ?>&nbsp;</p></td>
                    <td align="center"><? echo $unit_of_measurement[$row[csf('cons_uom')]]; ?>&nbsp;</td>
                    <td align="right"><? echo number_format($accss_req_qnty,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><? echo number_format($trims_recv,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><? echo number_format($recv_bl,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><? echo number_format($trims_issue,2,'.',''); ?>&nbsp;</td>
                    <td align="right"><? echo number_format($trims_left_over,2,'.',''); ?>&nbsp;</td>
                </tr>
            <?
				$tot_accss_req_qnty+=$accss_req_qnty;
				$tot_recv_qnty+=$trims_recv;
				$tot_recv_bl_qnty+=$recv_bl;
				$tot_iss_qnty+=$trims_issue;
				$tot_trims_left_over_qnty+=$trims_left_over;
				$i++;
			}
			$tot_trims_left_over_qnty_perc=($tot_trims_left_over_qnty/$tot_recv_qnty)*100;
			?>
            <tfoot>
                <tr>
                    <th align="right">&nbsp;</th>
                    <th align="right">Total</th>
                    <th align="right"><? echo number_format($tot_accss_req_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><? echo number_format($tot_recv_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><? echo number_format($tot_recv_bl_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><? echo number_format($tot_iss_qnty,0,'.',''); ?>&nbsp;</th>
                    <th align="right"><? echo number_format($tot_trims_left_over_qnty,0,'.',''); ?>&nbsp;</th>
                </tr>
             </tfoot>
        </table>  
        </div>
    </fieldset>
<?	

	
}

disconnect($con);
?>
