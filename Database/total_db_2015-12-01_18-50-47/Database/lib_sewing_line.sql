-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_sewing_line`
--

DROP TABLE IF EXISTS `lib_sewing_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_sewing_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` int(11) NOT NULL DEFAULT '0',
  `location_name` int(11) NOT NULL DEFAULT '0',
  `floor_name` int(11) NOT NULL DEFAULT '0',
  `sewing_line_serial` int(20) NOT NULL DEFAULT '0',
  `line_name` varchar(30) DEFAULT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `company_name` (`company_name`),
  KEY `location_name` (`location_name`),
  KEY `floor_name` (`floor_name`),
  KEY `line_name` (`line_name`),
  KEY `is_deleted` (`is_deleted`),
  KEY `status_active` (`status_active`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_sewing_line`
--

LOCK TABLES `lib_sewing_line` WRITE;
/*!40000 ALTER TABLE `lib_sewing_line` DISABLE KEYS */;
INSERT INTO `lib_sewing_line` VALUES (1,1,1,2,1,'Line-1',1,'2015-01-08 01:13:40',1,'2015-01-29 02:39:21',1,0,0),(2,1,1,2,2,'Line-2',1,'2015-01-08 01:13:55',1,'2015-01-29 02:39:34',1,0,0),(3,1,1,2,3,'Line-3',2,'2015-01-13 21:25:28',1,'2015-01-29 02:39:49',1,0,0),(4,1,1,2,4,'Line-4',2,'2015-01-13 21:25:42',1,'2015-01-29 02:40:00',1,0,0),(5,1,1,2,5,'Line-5',2,'2015-01-13 21:26:15',1,'2015-01-29 02:40:09',1,0,0),(6,1,1,2,6,'Line-6',2,'2015-01-13 21:26:31',1,'2015-01-29 02:40:18',1,0,0),(7,1,1,2,7,'Line-7',2,'2015-01-16 21:37:07',1,'2015-01-29 02:40:27',1,0,0),(8,1,1,5,10,'Tar-1',1,'2015-01-29 02:40:42',2,'2015-10-20 10:23:26',1,0,0),(9,1,1,5,11,'Tar-2',1,'2015-01-29 02:41:02',3,'2015-07-07 09:49:30',1,0,0),(10,1,1,2,8,'Line-8',2,'2015-06-11 16:38:59',2,'2015-06-11 16:39:38',1,0,0),(11,1,1,2,9,'Line-9',3,'2015-07-07 09:49:08',NULL,NULL,1,0,0),(12,1,1,5,12,'Tar-3',2,'2015-08-26 10:49:35',2,'2015-08-26 10:49:50',1,0,0),(13,1,1,2,13,'Line-1-Lay-1',2,'2015-10-19 11:37:09',2,'2015-10-19 11:57:49',1,0,0),(14,1,1,2,14,'Line-2-Lay-1',2,'2015-10-19 11:37:40',2,'2015-10-19 11:58:04',1,0,0),(15,1,1,2,15,'Line-3-Lay-1',2,'2015-10-19 11:58:26',NULL,NULL,1,0,0),(16,1,1,2,16,'Line-4-Lay-1',2,'2015-10-19 11:58:44',NULL,NULL,1,0,0),(17,1,1,2,17,'Line-5-Lay-1',2,'2015-10-19 11:59:03',NULL,NULL,1,0,0),(18,1,1,2,18,'Line-6-Lay-1',2,'2015-10-19 11:59:26',NULL,NULL,1,0,0),(19,1,1,2,19,'Line-7-Lay-1',2,'2015-10-19 11:59:50',NULL,NULL,1,0,0),(20,1,1,2,20,'Line-8-Lay-1',2,'2015-10-19 12:00:08',NULL,NULL,1,0,0),(21,1,1,2,9,'Line-9-Lay-1',2,'2015-10-19 12:00:23',2,'2015-11-21 17:43:16',1,0,0),(22,1,1,5,22,'Tar-1-Lay-1',2,'2015-10-19 12:01:02',NULL,NULL,1,0,0),(23,1,1,5,23,'Tar-2-Lay-1',2,'2015-10-19 12:01:19',NULL,NULL,1,0,0),(24,1,1,5,24,'Tar-3-Lay-1',2,'2015-10-19 12:01:34',NULL,NULL,1,0,0),(25,1,1,2,10,'Line-11',2,'2015-10-20 10:24:17',NULL,NULL,1,0,0),(26,1,1,2,6,'Line-6-Lay-2',2,'2015-11-03 11:31:18',NULL,NULL,1,0,0),(27,1,1,2,6,'Line-6-Lay-3',2,'2015-11-03 11:31:32',NULL,NULL,1,0,0),(28,1,1,2,6,'Line-6-Lay-4',2,'2015-11-03 11:31:48',NULL,NULL,1,0,0),(29,1,1,2,6,'Line-6-Lay-5',2,'2015-11-03 11:32:02',NULL,NULL,1,0,0),(30,1,1,2,3,'Line-3-lay-2',2,'2015-11-03 17:26:20',NULL,NULL,1,0,0),(31,1,1,2,15,'Line-3, Lay-2',2,'2015-11-28 16:58:02',NULL,NULL,1,0,0);
/*!40000 ALTER TABLE `lib_sewing_line` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
