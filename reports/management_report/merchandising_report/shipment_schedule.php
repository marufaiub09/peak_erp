﻿<?php
/*-------------------------------------------- Comments
Version                  :   V1
Purpose			         : 	This form will create  Shipment Schedule Report
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 	Jahid	
Update date		         : 	07-06-15	   
QC Performed BY	         :		
QC Date			         :	
Comments		         : From this version oracle conversion is start
							Update description(Create New Button Short)
*/
//error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Info","../../../", 1, 1, $unicode);
?>	
<script>
	var permission='<?php echo $permission; ?>';
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
	
	var tableFilters = 
	{
		col_34: "none",
		col_operation: {
		id: ["total_order_qnty_pcs","total_order_qnty","value_total_order_value","total_ex_factory_qnty","total_short_access_qnty","value_total_short_access_value","value_yarn_req_tot"],
		col: [16,17,20,22,24,25,26],
		operation: ["sum","sum","sum","sum","sum","sum","sum"],
		write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		}
	} 
	
	function generate_report_main(rpt_type)
	{
		/*if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}*/
		var report_title=$( "div.form_caption" ).html();
		var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*cbo_year*cbo_team_name*cbo_team_member*cbo_search_by*txt_search_string*txt_date_from*txt_date_to*cbo_category_by',"../../../")+'&report_title='+report_title+'&rpt_type='+rpt_type;
		//alert (data); return;
		freeze_window(3);
		http.open("POST","requires/shipment_schedule_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = generate_report_reponse;  
	}

	function generate_report_reponse()
	{	
		if(http.readyState == 4) 
		{	 
			var reponse=trim(http.responseText).split("####");
			$("#report_container2").html(reponse[0]); 
			document.getElementById('report_container').innerHTML=report_convert_button('../../../');  
			
			append_report_checkbox('table_header_1',1);
			setFilterGrid("table_body",-1,tableFilters);
			show_msg('3');
			release_freezing();
		}
	} 
	
	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		$('#scroll_body tr:first').hide();
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');
		d.close(); 
	
		document.getElementById('scroll_body').style.overflowY="scroll";
		document.getElementById('scroll_body').style.maxHeight="380px";
		$('#scroll_body tr:first').show();
	}

	function percent_set()
	{
		var tot_row=document.getElementById('tot_row').value;
		var tot_value_js=document.getElementById('total_value').value;
		for(var i=1;i<tot_row;i++)
		{
			var value_js=document.getElementById('value_'+i).value;
			var percent_value_js=((value_js*1)/(tot_value_js*1))*100
			document.getElementById('value_percent_'+i).innerHTML=percent_value_js.toFixed(2);
		}
	}

	function openmypage_image(page_link,title)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=1,scrolling=0','../../')
		emailwindow.onclose=function()
		{
			
		}
	}

	function last_ex_factory_popup(action,job_no,id,width)
	{ 
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/shipment_schedule_controller.php?action='+action+'&job_no='+job_no+'&id='+id, 'Last Ex-Factory Details', 'width='+width+',height=400px,center=1,resize=0,scrolling=0','../../');
	} 

	function search_by(val)
	{
		$('#txt_search_string').val('');
		if(val==1)
		{
			$('#search_by_td_up').html('Enter Order No');
		}
		else
		{
			$('#search_by_td_up').html('Enter Job No');
		}
	}
	
</script>
</head>
<body onLoad="set_hotkey()">
    <div style="width:100%;" align="center">
    <?php echo load_freeze_divs ("../../../");  ?>
    <form name="shipmentschedule_1" id="shipmentschedule_1" autocomplete="off" > 
        <h3 style="width:1150px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
        <div id="content_search_panel" > 
            <fieldset style="width:1150px;">
            <table class="rpt_table" width="1150" cellpadding="0" cellspacing="0" border="1" rules="all">
                <thead>
                    <tr>
                        <th>Company</th>
                        <th>Buyer</th>
                        <th>Year</th>
                        <th>Team</th>
                        <th>Team Member</th>
                        <th>Search By</th>
                        <th id="search_by_td_up">Order Wise</th>
                        <th colspan="2">Date</th>
                        <th>Date Category</th>
                        <th><input type="reset" name="reset" id="reset" value="Reset" style="width:70px" class="formbutton" /></th>
                    </tr>
                </thead>
                <tr>
                    <td>
						<?php
                        	echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "--Select Company--", $selected, " load_drop_down( 'requires/shipment_schedule_controller', this.value, 'load_drop_down_buyer', 'buyer_td' )" );
                        ?> 
                    </td>
                    <td id="buyer_td">
						<?php 
                        	echo create_drop_down( "cbo_buyer_name", 130, $blank_array,"", 1, "-- Select Buyer --", $selected, "" );
                        ?>	
                    </td>
                    <td>
						<?php 
                                echo create_drop_down( "cbo_year", 150,$year,"id,year", 1, "-- Select Year --", $selected,"" );
                                ?>
                    </td>
                    <td>                
						<?php
                        	echo create_drop_down( "cbo_team_name", 100, "select id,team_name from lib_marketing_team  where status_active =1 and is_deleted=0  order by team_name","id,team_name", 1, "-Team Name-", $selected, " load_drop_down( 'requires/shipment_schedule_controller', this.value, 'load_drop_down_team_member', 'team_td' )" );
                        ?>
                    </td>
                    <td id="team_td">
                        <?php 
                        	echo create_drop_down( "cbo_team_member", 120, $blank_array,"", 1, "-Team Member-", $selected, "" );
                        ?>	
                    </td>
                    <td align="center">
						<?php 
							$search_by_arr = array(1=>"Order Wise");//,2=>"Job Wise"
							echo create_drop_down( "cbo_search_by", 100, $search_by_arr,"",0, "", "",'search_by(this.value)',0 );
                        ?>
                    </td>
                    <td id="search_by_td">
                    	<input type="text" name="txt_search_string" id="txt_search_string" class="text_boxes" style="width:80px" placeholder="Write" />
                    </td>
                    <td>
                    	<input name="txt_date_from" id="txt_date_from"  class="datepicker" style="width:60px" placeholder="From Date">
                    </td>
                    <td>
                    	<input name="txt_date_to" id="txt_date_to"  class="datepicker" style="width:60px" placeholder="To Date">
                    </td>
                    <td>
                        <select name="cbo_category_by" id="cbo_category_by"  style="width:100px" class="combo_boxes">
                            <option value="1">Ship Date Wise </option>
                            <option value="2">PO Rec. Date Wise </option>
                        </select>
                    </td>
                    <td>
                        <input type="button" name="search" id="search" value="Show" onClick="generate_report_main(1)" style="width:70px" class="formbutton" /> &nbsp;
                        <input type="button" name="search" id="search" value="Short" onClick="generate_report_main(2)" style="width:70px" class="formbutton" />
                    </td>
                </tr>
                <tr>
                    <td colspan="10" align="center">
						<?php echo load_month_buttons(1); ?>
                    </td>
                </tr>
            </table>
        </fieldset>
        </div>
        </form>
           <div id="report_container" align="center"></div>
           <div id="report_container2"> 
       </div>
    </div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>