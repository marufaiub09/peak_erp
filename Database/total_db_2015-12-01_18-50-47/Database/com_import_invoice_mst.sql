-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_import_invoice_mst`
--

DROP TABLE IF EXISTS `com_import_invoice_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_import_invoice_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `btb_lc_id` varchar(50) NOT NULL,
  `temp_btb_lc_id` int(11) NOT NULL DEFAULT '0',
  `is_lc` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=yes,2=no',
  `invoice_no` varchar(54) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `document_value` double NOT NULL DEFAULT '0',
  `shipment_date` date NOT NULL,
  `company_acc_date` date NOT NULL,
  `bank_acc_date` date NOT NULL,
  `nagotiate_date` date NOT NULL,
  `bank_ref` varchar(54) NOT NULL,
  `acceptance_time` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=After Goods Receive; 2=Before Goods Receive',
  `retire_source` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(500) NOT NULL,
  `edf_tenor` tinyint(4) NOT NULL DEFAULT '0',
  `bill_no` varchar(64) DEFAULT NULL,
  `bill_date` date DEFAULT NULL,
  `shipment_mode` int(11) DEFAULT NULL,
  `document_status` tinyint(1) NOT NULL DEFAULT '0',
  `copy_doc_receive_date` date DEFAULT NULL,
  `original_doc_receive_date` date DEFAULT NULL,
  `doc_to_cnf` date DEFAULT NULL,
  `feeder_vessel` varchar(50) DEFAULT NULL,
  `mother_vessel` varchar(64) DEFAULT NULL,
  `eta_date` date DEFAULT NULL,
  `ic_receive_date` date DEFAULT NULL,
  `shipping_bill_no` varchar(64) DEFAULT NULL,
  `inco_term` int(11) NOT NULL DEFAULT '0',
  `inco_term_place` varchar(64) DEFAULT NULL,
  `port_of_loading` varchar(64) DEFAULT NULL,
  `port_of_discharge` varchar(64) DEFAULT NULL,
  `bill_of_entry_no` varchar(64) DEFAULT NULL,
  `psi_reference_no` varchar(54) DEFAULT NULL,
  `maturity_date` date NOT NULL,
  `container_no` varchar(100) NOT NULL,
  `pkg_quantity` double NOT NULL,
  `is_posted_account` int(5) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_import_invoice_mst`
--

LOCK TABLES `com_import_invoice_mst` WRITE;
/*!40000 ALTER TABLE `com_import_invoice_mst` DISABLE KEYS */;
/*!40000 ALTER TABLE `com_import_invoice_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
