﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------

//load drop down supplier
if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier", 170, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data' and b.party_type =2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

if ($action=="load_drop_down_supplier_from_issue")
{	  
	echo create_drop_down( "cbo_supplier", 170, "select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c, inv_issue_master d where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.id=d.knit_dye_company and d.knit_dye_source=3 and d.issue_purpose=15 and d.entry_form=3 and a.tag_company='$data' and b.party_type =2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

//load drop down store
if ($action=="load_drop_down_store")
{	  
	echo create_drop_down( "cbo_store_name", 170, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=1 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "-- Select --",0, "",0); 
	 	 
	exit();
}

//load drop down color
if($action=="load_drop_down_color")
{
	if($data=="")
	{
		if($db_type==0) $color_cond=" and color_name!=''"; else $color_cond=" and color_name IS NOT NULL";
		
		$sql="select id,color_name from lib_color where status_active=1 $color_cond order by color_name";
	}
	else
	{
		$sql="select a.id, a.color_name from lib_color a, wo_yarn_dyeing_dtls b where a.id=b.yarn_color and b.status_active =1 and b.is_deleted=0 and b.mst_id='$data' group by a.id, a.color_name order by a.color_name";
	}
	
	echo create_drop_down( "cbo_color", 110,$sql,"id,color_name", 1, "--Select--", 0, "",0 );

	echo '<input type="button" name="btn_color" id="btn_color" class="formbuttonplasminus"  style="width:20px" onClick="fn_color_new(this.id)" value="N" />';
	exit();
}


//load drop down composition
if($action=="load_drop_down_composition") // not used
{
	
	if($data==1)//new if
	{
		echo create_drop_down( "cbocomposition1", 80, $composition,"", 1, "-- Select --", "", "",$disabled,"" );
		echo '<input type="text" id="percentage1" name="percentage1" class="text_boxes_numeric" style="width:40px" maxlength="3" placeholder="%" />';
		echo create_drop_down( "cbocomposition2", 80, $composition,"", 1, "-- Select --", "", "",$disabled,"" );
		echo '<input type="text" id="percentage2" name="percentage2" class="text_boxes_numeric" style="width:40px" maxlength="3" placeholder="%" />';
		echo '<input type="button" class="formbutton" name="btn_composition" id="btn_composition" width="15" onClick="fn_comp_new(this.id)" value="F" />';
	}
	else
	{
		$sql =sql_select("select id,composition1,percentage1,composition2,percentage2 from lib_composition where is_deleted=0 AND status_active=1");
		$arr=array();
		foreach($sql as $row)
		{
			if($row[csf("composition1")]==0){ $row[csf("composition1")]=""; $row[csf("percentage1")]=""; }
			if($row[csf("composition2")]==0){ $row[csf("composition2")]=""; $row[csf("percentage2")]=""; }
			$arr[$row[csf("id")]] = $composition[$row[csf("composition1")]]." ".$row[csf("percentage1")]." ".$composition[$row[csf("composition2")]]." ".$row[csf("percentage2")];
		}
		
		echo create_drop_down( "cbo_composition", 110, $arr, "", 1, "--Select--", 0, "",0 );
		echo '<input type="button" class="formbutton" name="btn_composition" id="btn_composition" width="15" onClick="fn_comp_new(this.id)" value="N" />';
	}
	exit();
}


// wo/pi popup here----------------------// 
if ($action=="wopi_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
		$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_th_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            echo create_drop_down( "cbo_search_by", 170, $receive_basis_arr,"",1, "--Select--", $receive_basis,"",1 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $receive_purpose; ?>, 'create_wopi_search_list_view', 'search_div', 'yarn_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_tbl_id" value="" />
                    <input type="hidden" id="hidden_wopi_number" value="hidden_wopi_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" style="margin-top:5px" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_wopi_search_list_view")
{
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
	$receive_purpose = $ex_data[5];
 	
	$sql_cond="";
	if(trim($txt_search_by)==1) // for pi
	{
		$sql_cond .= " and a.pi_number LIKE '%$txt_search_common%'";
		if(trim($company)!=0) $sql_cond .= " and a.importer_id='$company'";
		
		if($txt_date_from!="" && $txt_date_to!="" )
		{
			if($db_type==0)
			{
				$sql_cond .= " and a.pi_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			}
			else
			{
				$sql_cond .= " and a.pi_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
			}
		}
	}
	else if(trim($txt_search_by)==2) // for wo
	{
		if(trim($txt_search_common)!="") $sql_cond .= " and wo_number_prefix_num LIKE '$txt_search_common'";
		if(trim($company)!=0) $sql_cond .= " and company_name='$company'";
		
		if($txt_date_from!="" && $txt_date_to!="" )
		{
			if($db_type==0)
			{
				$sql_cond .= " and wo_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			}
			else
			{
				$sql_cond .= " and wo_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
			}
		}
	}
 	
	if($txt_search_by==1 )
	{
 		$sql = "select a.id as id, a.pi_number as wopi_number, a.pi_number as wopi_prefix, a.pi_date as wopi_date, a.supplier_id as supplier_id, a.currency_id as currency_id, a.source as source, c.lc_number as lc_number
				from com_pi_master_details a 
				left join com_btb_lc_pi b on a.id=b.pi_id 
				left join com_btb_lc_master_details c on b.com_btb_lc_master_details_id=c.id
				where 
				a.item_category_id = 1 and
				a.status_active=1 and a.is_deleted=0
				$sql_cond";
	}
	else if($txt_search_by==2)
	{
		if($receive_purpose==2)
		{
			$sql_cond= " and ydw_no LIKE '%$txt_search_common%'";	
			if(trim($company)!="") $sql_cond .= " and company_id='$company'";
			
			if($txt_date_from!="" && $txt_date_to!="" )
			{
				if($db_type==0)
				{
					$sql_cond .= " and booking_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
				}
				else
				{
					$sql_cond .= " and booking_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
				}
			}
			
			$sql = "select id, yarn_dyeing_prefix_num, ydw_no, booking_date, delivery_date, supplier_id, currency, source, pay_mode
			from wo_yarn_dyeing_mst
			where 
 				status_active=1 and is_deleted=0
				and entry_form=41
				$sql_cond"; 
		}
		else
		{
			$sql = "select id,wo_number as wopi_number,wo_number_prefix_num as wopi_prefix,' ' as lc_number,wo_date as wopi_date,supplier_id as supplier_id,currency_id as currency_id,source as source 
					from wo_non_order_info_mst
					where 
					status_active=1 and is_deleted=0 and
					item_category=1 and pay_mode!=2
					$sql_cond order by id";
		}
	}
	//echo $sql;
	$result = sql_select($sql);
 	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	if($receive_purpose==2 && $txt_search_by==2)
	{
		$arr=array(3=>$supplier_arr,4=>$currency,5=>$source,6=>$pay_mode);
		echo  create_list_view("list_view", "WO No, Booking Date, Delivery Date, Supplier, Currency, Source, Pay Mode","100,100,100,180,100,120","900","260",0, $sql , "js_set_value", "id,ydw_no", "", 1, "0,0,0,supplier_id,currency,source,pay_mode", $arr, "yarn_dyeing_prefix_num,booking_date,delivery_date,supplier_id,currency,source,pay_mode", "",'','0,3,3,0,0,0,0') ;	
	}
	else
	{
		$arr=array(3=>$supplier_arr,4=>$currency,5=>$source);
		echo  create_list_view("list_view", "WO/PI No, LC ,Date, Supplier, Currency, Source","120,120,120,200,120,120,120","900","260",0, $sql , "js_set_value", "id,wopi_number", "", 1, "0,0,0,supplier_id,currency_id,source", $arr, "wopi_prefix,lc_number,wopi_date,supplier_id,currency_id,source", "",'','0,0,3,0,0,0') ;	
	}
	exit();	
	
}


//after select wo/pi number get form data here---------------------------//
if($action=="populate_data_from_wopi_popup")
{
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$receive_purpose = $ex_data[2];
	
	if($receive_basis==1 )
	{
 		$sql = "select c.id as id, c.lc_number as lc_number,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a 
				left join com_btb_lc_pi b on a.id=b.pi_id 
				left join com_btb_lc_master_details c on b.com_btb_lc_master_details_id=c.id
				where  
				a.item_category_id = 1 and
				a.status_active=1 and a.is_deleted=0 and
				a.id=$wo_pi_ID";
	}
	else if($receive_basis==2)
	{
		if($receive_purpose==2)
		{//currency as currency_id,
			$sql = "select id, '' as lc_number, supplier_id, 1 as currency_id, source
			from wo_yarn_dyeing_mst
			where 
 				status_active=1 and 
				is_deleted=0 and
				id=$wo_pi_ID"; 
		}
		else
		{
 			$sql = "select id,'' as lc_number,supplier_id as supplier_id,currency_id as currency_id,source as source 
				from wo_non_order_info_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category = 1 and 
				id=$wo_pi_ID";
		}
	}
	//echo $sql;die;
	$result = sql_select($sql);
	foreach($result as $row)
	{
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		echo "$('#txt_lc_no').val('".$row[csf("lc_number")]."');\n";
		if($row[csf("lc_number")]!="")
		{
			echo "$('#hidden_lc_id').val(".$row[csf("id")].");\n";
		}
		
		if($row[csf("currency_id")]==1)
		{
			echo "$('#txt_exchange_rate').val(1);\n";
		}
	}
	exit();	
}



//right side product list create here--------------------//
if($action=="show_product_listview")
{ 
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$receive_purpose = $ex_data[2];
	
	if($receive_basis==2 && $receive_purpose==2)
	{
		$sql = "select a.job_no, sum(a.cons_quantity) as quantity, c.id as prod_id, c.product_name_details
		from inv_transaction a, inv_issue_master b, product_details_master c 
		where a.mst_id=b.id and a.prod_id=c.id and b.booking_id=$wo_pi_ID and b.entry_form=3 and b.issue_basis=1 and b.issue_purpose=2 and a.item_category=1 and a.transaction_type=2 and a.status_active=1 and a.is_deleted=0 group by c.id, c.product_name_details, a.job_no";
		$result = sql_select($sql);
		$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
		$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
		$i=1; 
		?>
     
    	<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" id="tbl_product">
        	<thead><tr><th>SL</th><th>Job No</th><th>Product Name</th><th>Qnty</th></tr></thead>
            <tbody>
            	<?php 
				foreach($result as $row)
				{  
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("prod_id")]."**".$row[csf("job_no")]."**".$wo_pi_ID; ?>","wo_product_form_yarn_dyeing","requires/yarn_receive_controller");change_color_tr("<?php echo $i; ?>","<?php echo $bgcolor; ?>")' style="cursor:pointer" id="tr_<?php echo $i; ?>">
                		<td><?php echo $i; ?></td>
                        <td><?php echo $row[csf("job_no")]; ?></td>
                    	<td><?php echo $row[csf("product_name_details")]; ?></td>
                        <td align="right"><?php echo $row[csf("quantity")]; ?></td>
                    </tr>
                <?php 
					$i++; 
				} 
				?>
            </tbody>
        </table>
	<?php	 
	}
	else
	{
		if($receive_basis==1) // pi basis
		{	
			$sql = "select a.importer_id as company_id, a.supplier_id,b.id,b.count_name as yarn_count,b.yarn_composition_item1 as yarn_comp_type1st, b.yarn_composition_percentage1 as yarn_comp_percent1st, b.yarn_composition_item2 as yarn_comp_type2nd, b.yarn_composition_percentage2 as yarn_comp_percent2nd, b.yarn_type, b.color_id as color_name, sum(quantity) as quantity  
			from com_pi_master_details a, com_pi_item_details b
			where a.id=b.pi_id and a.id=$wo_pi_ID group by a.importer_id, a.supplier_id, b.id, b.count_name, b.yarn_composition_item1, b.yarn_composition_percentage1, b.yarn_composition_item2, b.yarn_composition_percentage2, b.yarn_type,b.color_id"; 
		}  
		else if($receive_basis==2) // wo basis
		{
			$sql = "select a.company_name as company_id,a.supplier_id, b.id, b.yarn_count, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color_name, sum(supplier_order_quantity) as quantity
			from wo_non_order_info_mst a, wo_non_order_info_dtls b 
			where a.id=b.mst_id and a.id=$wo_pi_ID group by a.company_name, a.supplier_id, b.id, b.yarn_count, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type, b.color_name";
			
		}	
		//echo $sql;die;
		$result = sql_select($sql);
		$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
		$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
		$i=1; 
		?>
     
    	<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" id="tbl_product">
        	<thead><tr><th>SL</th><th>Product Name</th></tr></thead>
            <tbody>
            	<?php foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					
					$compositionPart = $composition[$row[csf("yarn_comp_type1st")]]." ".$row[csf("yarn_comp_percent1st")];
					if( $row[csf("yarn_comp_type2nd")] != 0 )
					{
						$compositionPart .=" ".$composition[$row[csf("yarn_comp_type2nd")]]." ".$row[csf("yarn_comp_percent2nd")];
					}
					//$yarn_count.','.$composition.','.$ytype.','.$color;
				   $productName = $yarn_count_arr[$row[csf("yarn_count")]]." ".$compositionPart." ".$yarn_type[$row[csf("yarn_type")]]." ".$color_name_arr[$row[csf("color_name")]];
				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $receive_basis."**".$row[csf("id")]."**".$row[csf("company_id")]."**".$row[csf("supplier_id")]."**".$row[csf("quantity")];?>","wo_pi_product_form_input","requires/yarn_receive_controller");change_color_tr("<?php echo $i; ?>","<?php echo $bgcolor; ?>")' style="cursor:pointer" id="tr_<?php echo $i; ?>">
                		<td><?php echo $i; ?></td>
                    	<td><?php echo $productName; ?></td>
                    </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
	<?php	 
	}
	exit();
}


// get form data from product click in right side
if($action=="wo_pi_product_form_input")
{
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$company_id = $ex_data[2];
	$supplier_id = $ex_data[3];
	$wo_po_qnty = $ex_data[4];
	
	if($receive_basis==1) // pi basis
	{	
  		$sql = "select a.id as mst_id, b.count_name as yarn_count,b.yarn_composition_item1 as yarn_comp_type1st, b.yarn_composition_percentage1 as yarn_comp_percent1st, b.yarn_composition_item2 as yarn_comp_type2nd, b.yarn_composition_percentage2 as yarn_comp_percent2nd, b.yarn_type, b.color_id as color_name,b.quantity, b.net_pi_rate as rate, b.uom  
		from com_pi_master_details a, com_pi_item_details b
		where a.id=b.pi_id and b.id=$wo_pi_ID";		
	}
	else if($receive_basis==2) // wo basis
	{
 		$sql = "select a.id as mst_id, b.yarn_count, b.yarn_comp_type1st, b.yarn_comp_percent1st, b.yarn_comp_type2nd, b.yarn_comp_percent2nd, b.yarn_type,b.color_name, b.supplier_order_quantity as quantity, rate, uom  
		from wo_non_order_info_mst a, wo_non_order_info_dtls b 
		where a.id=b.mst_id and b.id=$wo_pi_ID";		
	}
	//echo $sql;	die;
	$result = sql_select($sql); 
	foreach($result as $row)
	{  
	 	echo "$('#cbo_yarn_count').val(".$row[csf("yarn_count")].");\n";		
		echo "$('#cbocomposition1').val(".$row[csf("yarn_comp_type1st")].");\n";
		echo "$('#percentage1').val(".$row[csf("yarn_comp_percent1st")].");\n";
		if($row[csf("yarn_comp_percent2nd")]>0)
		{
			echo "$('#cbocomposition2').val(".$row[csf("yarn_comp_type2nd")].");\n";
			echo "$('#percentage2').val(".$row[csf("yarn_comp_percent2nd")].");\n";
		}else{
			echo "$('#cbocomposition2').val(0);\n";
			echo "$('#percentage2').val('');\n";
		}
		echo "$('#cbo_yarn_type').val(".$row[csf("yarn_type")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("uom")].");\n";
		echo "$('#cbo_color').val(".$row[csf("color_name")].");\n";
		echo "$('#txt_rate').val(".$row[csf("rate")].");\n";
		
		if($row[csf("yarn_comp_percent2nd")] <= 0 || $row[csf("yarn_comp_percent2nd")]==""){ $yarn_comp_percent2nd=0;} else{ $yarn_comp_percent2nd=$row[csf("yarn_comp_percent2nd")];}
		
		$whereCondition = "a.id=b.prod_id and a.yarn_count_id=".$row[csf("yarn_count")]." and a.yarn_comp_type1st=".$row[csf("yarn_comp_type1st")]." and a.yarn_comp_percent1st=".$row[csf("yarn_comp_percent1st")]." and a.yarn_comp_type2nd=".$row[csf("yarn_comp_type2nd")]." and a.yarn_comp_percent2nd=".$yarn_comp_percent2nd." and a.yarn_type=".$row[csf("yarn_type")]." and a.color=".$row[csf("color_name")]." and b.company_id=$company_id and a.supplier_id=$supplier_id and a.item_category_id=1 and b.transaction_type=1 and b.item_category=1 and b.pi_wo_batch_no=".$row[csf("mst_id")]." and b.receive_basis=$receive_basis";
		if($whereCondition=="") $totalRcvQnty=0;
		
		//echo "select sum(b.cons_quantity) as recv_qnty from product_details_master a, inv_transaction b where $whereCondition";
		$totalRcvQnty = return_field_value("sum(b.cons_quantity) as recv_qnty","product_details_master a, inv_transaction b","$whereCondition","recv_qnty");

		$orderQnty = round($wo_po_qnty)-round($totalRcvQnty);
		echo "$('#txt_order_qty').val(".$orderQnty.");\n";
		echo "control_composition('percent_one');\n";
	} 
	exit();	 
}

if($action=="wo_product_form_yarn_dyeing")
{
	$ex_data = explode("**",$data);
	$prod_id = $ex_data[0];
	$job_no = $ex_data[1];
	$wo_pi_ID = $ex_data[2];
	
	$avg_rate=return_field_value("(sum(cons_amount)/ sum(cons_quantity)) as order_rate","inv_transaction","status_active=1 and is_deleted=0 and prod_id=$prod_id and item_category=1 and transaction_type=1","order_rate");

	$dyeing_charge=return_field_value("a.ecchange_rate*b.dyeing_charge as dyeing_charge","wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b","a.id=b.mst_id and b.status_active=1 and b.is_deleted=0 and b.product_id=$prod_id and a.id=$wo_pi_ID","dyeing_charge");
	
	$dyed_yarn_rate=$avg_rate+$dyeing_charge;
	
	$sql = "select yarn_count_id, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_type, unit_of_measure from product_details_master where id=$prod_id";
	///echo $sql;//die;
	$result = sql_select($sql); 
	foreach($result as $row)
	{  
	 	echo "$('#cbo_yarn_count').val(".$row[csf("yarn_count_id")].");\n";		
		echo "$('#cbocomposition1').val(".$row[csf("yarn_comp_type1st")].");\n";
		echo "$('#percentage1').val(".$row[csf("yarn_comp_percent1st")].");\n";
		if($row[csf("yarn_comp_type2nd")]>0)
		{
			echo "$('#cbocomposition2').val(".$row[csf("yarn_comp_type2nd")].");\n";
			echo "$('#percentage2').val(".$row[csf("yarn_comp_percent2nd")].");\n";
		}else{
			echo "$('#cbocomposition2').val(0);\n";
			echo "$('#percentage2').val('');\n";
		}
		echo "$('#cbo_yarn_type').val(".$row[csf("yarn_type")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("unit_of_measure")].");\n";
		echo "$('#txt_rate').val('".number_format($dyed_yarn_rate,4,'.','')."');\n";
		echo "$('#job_no').val('".$job_no."');\n";
		echo "$('#txt_order_qty').val('');\n";
	} 
	exit();	 
}

// LC popup here----------------------Not Used// 
if ($action=="lc_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
function js_set_value(str)
{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
		$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
		parent.emailwindow.hide();
}
	
	
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th>
                    	<input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  />
                   		<!-- Hidden field here-------->
                        <input type="hidden" id="hidden_tbl_id" value="" />
                        <input type="hidden" id="hidden_wopi_number" value="hidden_wopi_number" />
                        <!-- ---------END------------->
                    </th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            $search_by_arr=array(0=>'LC Number',1=>'Supplier Name');
							$dd="change_search_event(this.value, '0*1', '0*select id, supplier_name from lib_supplier', '../../') ";							
							echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+<?php echo $company; ?>, 'create_lc_search_list_view', 'search_div', 'yarn_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
           	 	</tr> 
            </tbody>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit();
}

//Not Used
if($action=="create_lc_search_list_view")
{
	$ex_data = explode("_",$data);
	$cbo_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$company = $ex_data[2];
	
	if($cbo_search_by==1 && $txt_search_common!="") // lc number
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where lc_number LIKE '%$search_string%' and importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	} 
	else if($cbo_search_by==1 && $txt_search_common!="") //supplier
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where supplier_id='$search_string' and importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	} 
	else
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where importer_id=$company and item_category_id=1 and is_deleted=0 and status_active=1";
	}
	
	$company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$company_arr,2=>$supplier_arr,3=>$item_category);
	echo  create_list_view("list_view", "LC No,Importer,Supplier Name,Item Category,Value","120,150,150,120,120","750","260",0, $sql , "js_set_value", "id,lc_number", "", 1, "0,importer_id,supplier_id,item_category_id,0", $arr, "lc_number,importer_id,supplier_id,item_category_id,lc_value", "",'','0,0,0,0,0,1') ;	
	exit();
	
}


if($action=="show_ile")
{
	$ex_data = explode("**",$data);
	$company = $ex_data[0];
	$source = $ex_data[1];
	$rate = $ex_data[2];
	
	$sql="select standard from variable_inv_ile_standard where source='$source' and company_name='$company' and category=1 and status_active=1 and is_deleted=0";
	//echo $sql;
	$result=sql_select($sql,1);
	foreach($result as $row)
	{
		// NOTE :- ILE=standard, ILE% = standard/100*rate
		$ile = $row[csf("standard")];
		$ile_percentage = ( $row[csf("standard")]/100 )*$rate;
		echo $ile."**".number_format($ile_percentage,$dec_place[3],".","");
		exit();
	}
	exit();
}



//data save update delete here------------------------------//
if($action=="save_update_delete")
{	  
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		
		$flag=1;
		//table lock here   
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0**0"; die;}
		 
		//---------------Check Color---------------------------//
		if( str_replace("'","",$btn_color)=='F' )
		{
			$color_library=return_library_array( "select id,color_name from lib_color",'id','color_name');
			$cbo_color = return_id( str_replace("'","",$cbo_color), $color_library, "lib_color", "id,color_name");
		}
		//----------------Check Color END---------------------//
		
		
		//---------------Check Brand---------------------------//
		if( str_replace("'","",$txt_brand)!="" )
		{
			$brand_library = return_library_array( "select id,brand_name from lib_brand",'id','brand_name');
			$txt_brand = return_id( str_replace("'","",$txt_brand), $brand_library, "lib_brand", "id,brand_name");
		}
		//----------------Check Brand END---------------------//
		
		
		//---------------Check Product ID --------------------------//
		$insertR=true;
 		$rtnString =  return_product_id($cbo_yarn_count,$cbocomposition1,$cbocomposition2,$percentage1,$percentage2,$cbo_yarn_type,$cbo_color,$txt_yarn_lot,$txt_prod_code,$cbo_company_name,$cbo_supplier,$cbo_store_name,$cbo_uom,$yarn_type,$composition);
 		$expString = explode("***",$rtnString); 
		if($expString[0]==true && $expString[0]!="")
		{
			$prodMSTID = $expString[1];
		}
		else
		{
			$field_array = $expString[1];
			$data_array = $expString[2];
			//echo "20**"."insert into product_details_master (".$field_array.") values ".$data_array;die;		
			$insertR = sql_insert("product_details_master",$field_array,$data_array,0);
			$prodMSTID = $expString[3];
		}	
		//oci_rollback($con);	 
		//echo  "20**bipul".$flag;die;
		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_receive_master a, inv_transaction b","a.id=b.mst_id and a.recv_number=$txt_mrr_no and b.prod_id=$prodMSTID and b.transaction_type=1 and a.item_category=1"); 
		if($duplicate==1 && str_replace("'","",$txt_mrr_no)=="") 
		{
			//check_table_status( $_SESSION['menu_id'],0);
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			die;
		}
		//------------------------------Check Brand END---------------------------------------//
		 
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value,available_qnty,allocated_qnty from product_details_master where id=$prodMSTID");
		$presentStock=$presentStockValue=$presentAvgRate=$allocated_qnty=$available_qnty=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$product_name_details 	=$result[csf("product_name_details")];	
			$available_qnty			=$result[csf("available_qnty")];	
			$allocated_qnty 		=$result[csf("allocated_qnty")];	
		}		 
		//----------------Check Product ID END---------------------//
	 		
		
		if(str_replace("'","",$txt_mrr_no)!="")
		{
			$new_recv_number[0] = str_replace("'","",$txt_mrr_no);
			$id=return_field_value("id","inv_receive_master","recv_number=$txt_mrr_no");
			//yarn master table UPDATE here START----------------------//		
			$field_array="item_category*receive_basis*receive_purpose*receive_date*booking_id*challan_no*store_id*exchange_rate*currency_id*supplier_id*yarn_issue_challan_no*issue_id*lc_no*source*updated_by*update_date";
			$data_array="1*".$cbo_receive_basis."*".$cbo_receive_purpose."*".$txt_receive_date."*".$txt_wo_pi_id."*".$txt_challan_no."*".$cbo_store_name."*".$txt_exchange_rate."*".$cbo_currency."*".$cbo_supplier."*".$txt_issue_challan_no."*".$txt_issue_id."*".$hidden_lc_id."*".$cbo_source."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
					
			//$rID=sql_update("inv_receive_master",$field_array,$data_array,"id",$id,0);
			//yarn master table UPDATE here END---------------------------------------// 
		}
		else  	
		{	
			// yarn master table entry here START---------------------------------------//		
			$id=return_next_id("id", "inv_receive_master", 1);	
				
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$new_recv_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'YRV', date("Y",time()), 5, "select recv_number_prefix,recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='1' and $year_cond=".date('Y',time())." order by id DESC ", "recv_number_prefix", "recv_number_prefix_num" )); 
			
 			$field_array="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category, company_id, receive_basis, receive_purpose, receive_date, booking_id, challan_no, store_id, exchange_rate, currency_id, supplier_id, yarn_issue_challan_no, issue_id, lc_no, source, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_recv_number[1]."','".$new_recv_number[2]."','".$new_recv_number[0]."',1,1,".$cbo_company_name.",".$cbo_receive_basis.",".$cbo_receive_purpose.",".$txt_receive_date.",".$txt_wo_pi_id.",".$txt_challan_no.",".$cbo_store_name.",".$txt_exchange_rate.",".$cbo_currency.",".$cbo_supplier.",".$txt_issue_challan_no.",".$txt_issue_id.",".$hidden_lc_id.",".$cbo_source.",'".$user_id."','".$pc_date_time."')";
			//echo $field_array."<br>".$data_array;die;
			//echo "20**"."insert into inv_receive_master (".$field_array.") values ".$data_array;die;
			//$rID=sql_insert("inv_receive_master",$field_array,$data_array,0);
			// yarn master table entry here END---------------------------------------// 
		}
		
		// yarn details table entry here START-----------------------------------//		
		$rate = str_replace("'","",$txt_rate);
		$txt_ile = str_replace("'","",$txt_ile);
		if($txt_ile=='') $txt_ile=0;
		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$ile = ($txt_ile/$rate)*100; // ile cost to ile
		$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
		$exchange_rate = str_replace("'","",$txt_exchange_rate);
		$conversion_factor = 1; // yarn always KG	
		$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
 		$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
		$con_amount = $cons_rate*$txt_receive_qty;
		$con_ile = $ile;//($ile/$domestic_rate)*100;
		$con_ile_cost = ($ile/100)*($rate*$exchange_rate);
		
		$dtlsid = return_next_id("id", "inv_transaction", 1);		 
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$field_array_trans= "id,mst_id,receive_basis,pi_wo_batch_no,company_id,supplier_id,prod_id,product_code,item_category,transaction_type,transaction_date,store_id,brand_id,order_uom,order_qnty,order_rate,order_ile,order_ile_cost,order_amount,cons_uom,cons_quantity,cons_rate,cons_ile,cons_ile_cost,cons_amount,balance_qnty,balance_amount,no_of_bags,cone_per_bag,weight_per_bag,weight_per_cone,room,rack,self,bin_box,remarks,job_no,inserted_by,insert_date";
		
 		$data_array_trans= "(".$dtlsid.",".$id.",".$cbo_receive_basis.",".$txt_wo_pi_id.",".$cbo_company_name.",".$cbo_supplier.",".$prodMSTID.",".$txt_prod_code.",1,1,".$txt_receive_date.",".$cbo_store_name.",".$txt_brand.",".$cbo_uom.",".$txt_receive_qty.",".$txt_rate.",".$ile.",".$ile_cost.",".$txt_amount.",".$cbo_uom.",".$txt_receive_qty.",".$cons_rate.",".$con_ile.",".$con_ile_cost.",".$con_amount.",".$txt_receive_qty.",".$con_amount.",".$txt_no_bag.",".$txt_cone_per_bag.",".$txt_weight_per_bag.",".$txt_weight_per_cone.",".$txt_room.",".$txt_rack.",".$txt_self.",".$txt_binbox.",".$txt_remarks.",".$job_no.",'".$user_id."','".$pc_date_time."')";
		//echo "INSERT INTO inv_transaction (".$field_array.") VALUES ".$data_array.""; die;
		//echo $field_array."<br>".$data_array;die;
		//$dtlsrID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
		//yarn details table entry here END-----------------------------------//
		
		
		//product master table data UPDATE START----------------------------------------------------------//	
		$stock_value 	= $domestic_rate*$txt_receive_qty;
  		$currentStock   = $presentStock+$txt_receive_qty;
		//$available_qnty = $available_qnty+$txt_receive_qty;
		$StockValue	 = $presentStockValue+$stock_value;
		$avgRate		= $StockValue/$currentStock;
		
		if(str_replace("'","",$cbo_receive_purpose)==2)
		{
			$allocated_qnty=$allocated_qnty+$txt_receive_qty;
			$available_qnty = $available_qnty;
		}
		else
		{
			$allocated_qnty=$allocated_qnty;
			$available_qnty = $available_qnty+$txt_receive_qty;
		}
		
		if(str_replace("'","",$txt_brand)=="") $txt_brand=0;
 		$field_array_prod_update="brand*avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";
 		$data_array_prod_update="".$txt_brand."*".number_format($avgRate,$dec_place[3],".","")."*".$txt_receive_qty."*".$currentStock."*".number_format($StockValue,$dec_place[4],".","")."*".$allocated_qnty."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'";
		//echo "20**".$field_array."**".$data_array;die;
		
		if(str_replace("'","",$txt_mrr_no)!="")
		{
			$rID=sql_update("inv_receive_master",$field_array,$data_array,"id",$id,0);
		}
		else
		{
			$rID=sql_insert("inv_receive_master",$field_array,$data_array,0);
		}
		
		$dtlsrID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,0);
		$prodUpdate = sql_update("product_details_master",$field_array_prod_update,$data_array_prod_update,"id",$prodMSTID,1);
		//------------------ product_details_master END---------------------------------------------------//
		//echo "20**".$new_recv_number[0];die;
		//oci_rollback($con);
		//echo "10**".$rID." && ".$dtlsrID." && ".$prodUpdate."&&".$rtnString; die;
 		
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if( $rID && $dtlsrID && $prodUpdate && $insertR)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_recv_number[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_recv_number[0];
			}
		}
		else if($db_type==2 || $db_type==1)
		{
			if( $rID && $dtlsrID && $prodUpdate && $insertR)
			{
				oci_commit($con);  
				echo "0**".$new_recv_number[0];
			}
			else
			{
				oci_rollback($con);
				echo "10**".$new_recv_number[0];
			}
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		//check_table_status( $_SESSION['menu_id'],0);
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0**1"; die;}
		
		//check update id
		/*if( str_replace("'","",$update_id) == "" )
		{
			check_table_status( $_SESSION['menu_id'],0);
			echo "15**Duplicate Product is Not Allow in Same Return Number."; exit(); 
		}
		*/
				
		//previous product stock adjust here--------------------------//
		//product master table UPDATE here START ---------------------//
		$sql = sql_select("select a.prod_id,a.cons_quantity,a.cons_rate,a.cons_amount,a.balance_qnty,a.balance_amount,b.avg_rate_per_unit,b.current_stock,b.stock_value,b.allocated_qnty,b.available_qnty,c.receive_purpose from inv_transaction a, product_details_master b, inv_receive_master c where a.id=$update_id and a.prod_id=b.id and a.mst_id=c.id and c.entry_form=1");
		$before_prod_id=$before_receive_qnty=$before_rate=$beforeAmount=$beforeBalanceQnty=$beforeBalanceAmount=0;$before_brand="";
		$beforeStock=$beforeStockValue=$beforeAvgRate=$before_allocated_qnty=$before_available_qnty=$adj_allocated_qnty=$adj_beforeAvailableQnty=0;
		foreach( $sql as $row)
		{
			$before_prod_id 		= $row[csf("prod_id")]; 
			$before_receive_qnty 	= $row[csf("cons_quantity")]; //stock qnty
			$before_rate 			= $row[csf("cons_rate")]; 
			$beforeAmount			= $row[csf("cons_amount")]; //stock value
			$beforeBalanceQnty		= $row[csf("balance_qnty")]; 
			$beforeBalanceAmount	= $row[csf("balance_amount")]; 
			
			$before_brand 			=$row[csf("brand")];
			$beforeStock			=$row[csf("current_stock")];
			$beforeStockValue		=$row[csf("stock_value")];
			$beforeAvgRate			=$row[csf("avg_rate_per_unit")];	
			$before_available_qnty	=$row[csf("available_qnty")];	
			$before_allocated_qnty	=$row[csf("allocated_qnty")];	
			$before_receive_purpose	=$row[csf("receive_purpose")];	
		}
		
		//stock value minus here---------------------------//
		$adj_beforeStock			=$beforeStock-$before_receive_qnty;
		$adj_beforeStockValue		=$beforeStockValue-$beforeAmount;
		$adj_beforeAvgRate			=number_format(($adj_beforeStockValue/$adj_beforeStock),$dec_place[3],'.','');
		
		if($before_receive_purpose==2)
		{
			$adj_allocated_qnty=$before_allocated_qnty-$before_receive_qnty;
			$adj_beforeAvailableQnty=$before_available_qnty;
		}
		else
		{
			$adj_allocated_qnty=$before_allocated_qnty;
			$adj_beforeAvailableQnty=$before_available_qnty-$before_receive_qnty;
		}
		

		//$beforeStockAdjSQL = sql_update("product_details_master","avg_rate_per_unit*current_stock*stock_value","$adj_beforeAvgRate*$adj_beforeStock*$adj_beforeStockValue","id",$before_prod_id,1);			
		//product master table UPDATE here END   ---------------------//
		//----------------- END PREVIOUS STOCK ADJUST-----------------//
				
		 	
		
		//---------------Check Color---------------------------//
		if( str_replace("'","",$btn_color)=='F' )
		{
			$color_library=return_library_array( "select id,color_name from lib_color",'id','color_name');
			$cbo_color = return_id( str_replace("'","",$cbo_color), $color_library, "lib_color", "id,color_name");
		}
		//----------------Check Color END---------------------//
		
		
		//---------------Check Brand---------------------------//
		if( str_replace("'","",$txt_brand)!="" )
		{
			$brand_library = return_library_array( "select id,brand_name from lib_brand",'id','brand_name');
			$txt_brand = return_id( str_replace("'","",$txt_brand), $brand_library, "lib_brand", "id,brand_name");
		}
		//----------------Check Brand END---------------------//
		
		 
		//---------------Check Product ID --------------------------//
		$insertR=true;
 		$rtnString = return_product_id($cbo_yarn_count,$cbocomposition1,$cbocomposition2,$percentage1,$percentage2,$cbo_yarn_type,$cbo_color,$txt_yarn_lot,$txt_prod_code,$cbo_company_name,$cbo_supplier,$cbo_store_name,$cbo_uom,$yarn_type,$composition);
 		$expString = explode("***",$rtnString);
		if($expString[0]==true)
		{
			$prodMSTID = $expString[1];
		}
		else
		{
			$field_array = $expString[1];
			$data_array = $expString[2];			
			$insertR = sql_insert("product_details_master",$field_array,$data_array,0); 
			/*if($db_type==0)	{ mysql_query("COMMIT");} 	
			if($db_type==0)	{ mysql_query("BEGIN"); }*/
			$prodMSTID = $expString[3];
		}		 
		//current product stock-------------------------//
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value,available_qnty,allocated_qnty from product_details_master where id=$prodMSTID");
		$presentStock=$presentStockValue=$presentAvgRate=$allocated_qnty=$available_qnty=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock		  	=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$product_name_details   =$result[csf("product_name_details")];
			$available_qnty			=$result[csf("available_qnty")];
			$allocated_qnty 		=$result[csf("allocated_qnty")];
		}	 
		//----------------Check Product ID END---------------------//
			
		//yarn master table UPDATE here START----------------------//		
		$field_array_update="item_category*receive_basis*receive_purpose*receive_date*challan_no*store_id*exchange_rate*currency_id*supplier_id*yarn_issue_challan_no*issue_id*lc_no*source*updated_by*update_date";
		$data_array_update="1*".$cbo_receive_basis."*".$cbo_receive_purpose."*".$txt_receive_date."*".$txt_challan_no."*".$cbo_store_name."*".$txt_exchange_rate."*".$cbo_currency."*".$cbo_supplier."*".$txt_issue_challan_no."*".$txt_issue_id."*".$hidden_lc_id."*".$cbo_source."*'".$user_id."'*'".$pc_date_time."'";
 		//$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"recv_number",$txt_mrr_no,0);	
		
		// yarn details table UPDATE here START-----------------------------------//		
		$rate = str_replace("'","",$txt_rate);
		$txt_ile = str_replace("'","",$txt_ile);
		if($txt_ile=='') $txt_ile=0;
		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$ile = ($txt_ile/$rate)*100; // ile cost to ile
		$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
		$exchange_rate = str_replace("'","",$txt_exchange_rate);
		$conversion_factor = 1; // yarn always KG	
		$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
 		$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
		
		$con_amount = $cons_rate*$txt_receive_qty;
		$con_ile = $ile;  
		$con_ile_cost = ($ile/100)*($rate*$exchange_rate);
		//echo "20**".$con_ile_cost; mysql_query("ROLLBACK"); die;
		
		$adjBalanceQnty		=$beforeBalanceQnty-$before_receive_qnty+$txt_receive_qty;
		$adjBalanceAmount	=$beforeBalanceAmount-$beforeAmount+$con_amount;
 		 
		$field_array_trans= "receive_basis*pi_wo_batch_no*company_id*supplier_id*prod_id*product_code*item_category*transaction_type*transaction_date*store_id*brand_id*order_uom*order_qnty*order_rate*order_ile*order_ile_cost*order_amount*cons_uom*cons_quantity*cons_rate*cons_ile*cons_ile_cost*cons_amount*balance_qnty*balance_amount*no_of_bags*cone_per_bag*weight_per_bag*weight_per_cone*room*rack*self*bin_box*remarks*job_no*updated_by*update_date";
 		$data_array_trans= "".$cbo_receive_basis."*".$txt_wo_pi_id."*".$cbo_company_name."*".$cbo_supplier."*".$prodMSTID."*".$txt_prod_code."*1*1*".$txt_receive_date."*".$cbo_store_name."*".$txt_brand."*".$cbo_uom."*".$txt_receive_qty."*".$txt_rate."*".$ile."*".$ile_cost."*".$txt_amount."*".$cbo_uom."*".$txt_receive_qty."*".$cons_rate."*".$con_ile."*".$con_ile_cost."*".$con_amount."*".$adjBalanceQnty."*".$adjBalanceAmount."*".$txt_no_bag."*".$txt_cone_per_bag."*".$txt_weight_per_bag."*".$txt_weight_per_cone."*".$txt_room."*".$txt_rack."*".$txt_self."*".$txt_binbox."*".$txt_remarks."*".$job_no."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
 		//$dtlsrID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);  
		//yarn details table UPDATE here END-----------------------------------//
		
    		
		//product master table data UPDATE START----------------------------------------------------------// 
		$field_array="brand*avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*allocated_qnty*available_qnty*updated_by*update_date";
		if($before_prod_id==$prodMSTID)
		{
			$currentStock	= $adj_beforeStock+$txt_receive_qty;
			//$available_qnty  = $adj_beforeAvailableQnty+$txt_receive_qty;
			$StockValue	  = $adj_beforeStockValue+($domestic_rate*$txt_receive_qty);
			$avgRate		 = number_format($StockValue/$currentStock,$dec_place[3],'.','');
			
			if(str_replace("'","",$cbo_receive_purpose)==2)
			{
				$allocated_qnty=$adj_allocated_qnty+$txt_receive_qty;
				$available_qnty = $adj_beforeAvailableQnty;
			}
			else
			{
				$allocated_qnty=$adj_allocated_qnty;
				$available_qnty = $adj_beforeAvailableQnty+$txt_receive_qty;
			}
			
			if(str_replace("'","",$txt_brand)=="") $txt_brand=0;
 			$data_array = "".$txt_brand."*".$avgRate."*".$txt_receive_qty."*".$currentStock."*".number_format($StockValue,$dec_place[4],'.','')."*".$allocated_qnty."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'";
			//$prodUpdate = sql_update("product_details_master",$field_array,$data_array,"id",$prodMSTID,0);
		}
		else
		{ 
			//before
			$updateID_array=$update_data=array();
			$updateID_array[]=$before_prod_id;
			
			if(str_replace("'","",$before_brand)=="") $before_brand=0;
			$update_data[$before_prod_id]=explode("*",("".$before_brand."*".$adj_beforeAvgRate."*".$before_receive_qnty."*".$adj_beforeStock."*".number_format($adj_beforeStockValue,$dec_place[4],'.','')."*".$adj_allocated_qnty."*".$adj_beforeAvailableQnty."*'".$user_id."'*'".$pc_date_time."'"));
			
			//current			 
 			$presentStock 			= $presentStock+$txt_receive_qty;
 			//$available_qnty  		= $available_qnty+$txt_receive_qty;
			$presentStockValue	    = $presentStockValue+($domestic_rate*$txt_receive_qty);
			$presentAvgRate		    = number_format($presentStockValue/$presentStock,$dec_place[3],'.','');
			
			if(str_replace("'","",$cbo_receive_purpose)==2)
			{
				$allocated_qnty=$allocated_qnty+$txt_receive_qty;
				$available_qnty = $available_qnty;
			}
			else
			{
				$allocated_qnty=$allocated_qnty;
				$available_qnty = $available_qnty+$txt_receive_qty;
			}
			
			if(str_replace("'","",$txt_brand)=="") $txt_brand=0;
			
			$updateID_array[]=$prodMSTID;
			$update_data[$prodMSTID]=explode("*",("".$txt_brand."*".$presentAvgRate."*".$txt_receive_qty."*".$presentStock."*".number_format($presentStockValue,$dec_place[4],'.','')."*".$allocated_qnty."*".$available_qnty."*'".$user_id."'*'".$pc_date_time."'"));
			//$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$field_array,$update_data,$updateID_array));
		}
		//------------------ product_details_master END---------------------------------------------------//
		
		//echo "20**".$beforeAmount."==".$adj_beforeAvailableQnty."==".$avgRate;mysql_query("ROLLBACK");die; 
  		
		$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"recv_number",$txt_mrr_no,0);
		$dtlsrID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1); 
		if($before_prod_id==$prodMSTID)
		{
			$prodUpdate = sql_update("product_details_master",$field_array,$data_array,"id",$prodMSTID,0);
		}
		else
		{
			$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$field_array,$update_data,$updateID_array));
		}
 		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($rID && $dtlsrID && $prodUpdate && $insertR)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_mrr_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_mrr_no);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if( $rID && $dtlsrID && $prodUpdate && $insertR)
			{
				oci_commit($con);  
				echo "1**".str_replace("'","",$txt_mrr_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_mrr_no);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) //Not Used Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		// master table delete here---------------------------------------
		$mst_id = return_field_value("id","inv_receive_master","recv_number like $txt_recv_number");	
		if($mst_id=="" || $mst_id==0){ echo "10**0"; die;}
 		$rID = sql_update("inv_receive_master",'status_active*is_deleted','0*1',"id*item_category","$mst_id*1",1);
		$dtlsrID = sql_update("inv_transaction",'status_active*is_deleted','0*1',"mst_id*item_category","$mst_id*1",1);
		
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_mrr_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_mrr_no);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID)
			{
				oci_commit($con);  
				echo "2**".str_replace("'","",$txt_mrr_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_mrr_no);
			}
		}
		disconnect($con);
		die;
	}		
}


if($action=="mrr_popup_info")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrrID)
	{
 		var splitArr = mrrID.split("_");
 		$("#hidden_recv_id").val(splitArr[0]); 		// id number
		$("#hidden_recv_number").val(splitArr[1]); 	// mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter MRR Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php
 							 echo create_drop_down( "cbo_supplier", 150, "select c.supplier_name,c.id from lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and b.party_type=2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td>
                        <?php  
                            $search_by = array(1=>'MRR No',2=>'Challan No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 120, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'yarn_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="" />
                     <input type="hidden" id="hidden_recv_id" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php

exit;
}


if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for mrr
		{
			$sql_cond .= " and a.recv_number LIKE '%$txt_search_common'";	
			
		}
		else if(trim($txt_search_by)==2) // for chllan no
		{
			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";				
 		}		 
		 
 	} 
	
	if($fromDate!="" && $toDate!="" ) 
	{
		if($db_type==0)
		{
			$sql_cond .= " and a.receive_date between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and a.receive_date between '".change_date_format($fromDate,'','',1)."' and '".change_date_format($toDate,'','',1)."'";
		}
	}
	
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	if(trim($supplier)!=0) $sql_cond .= " and a.supplier_id='$supplier'";
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql = "select a.id, $year_field a.recv_number_prefix_num, a.recv_number, a.supplier_id, a.challan_no, c.lc_number, a.receive_date, a.receive_basis, sum(b.cons_quantity) as receive_qnty from inv_transaction b, inv_receive_master a left join com_btb_lc_master_details c on a.lc_no=c.id where a.id=b.mst_id and a.entry_form=1 and b.item_category=1 and b.transaction_type=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $sql_cond group by a.id, a.recv_number_prefix_num, a.recv_number, a.supplier_id, a.challan_no, a.receive_date, a.receive_basis, a.insert_date, c.lc_number order by a.id";
	//echo $sql;
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(2=>$supplier_arr,6=>$receive_basis_arr);
	echo create_list_view("list_view", "Year, MRR No, Supplier Name, Challan No, LC No, Receive Date, Receive Basis, Receive Qnty","60,70,130,120,120,120,100,100","900","260",0, $sql , "js_set_value", "id,recv_number", "", 1, "0,0,supplier_id,0,0,0,receive_basis,0", $arr, "year,recv_number_prefix_num,supplier_id,challan_no,lc_number,receive_date,receive_basis,receive_qnty", "",'','0,0,0,0,0,3,0,1') ;	
	exit();
	
}

if($action=="populate_data_from_data")
{
	
	$ex_data = explode("_",$data);
	$mrrNo = $ex_data[0];
	$rcvID = $ex_data[1];
	
	$sql = "select id,recv_number,company_id,receive_basis,receive_purpose,receive_date,booking_id,challan_no,store_id,lc_no,supplier_id,exchange_rate,currency_id,lc_no,source, yarn_issue_challan_no, issue_id  
			from inv_receive_master 
			where id=$rcvID and recv_number='$mrrNo' and entry_form=1";
	//echo $sql;die;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		echo"load_drop_down( 'requires/yarn_receive_controller', ".$row[csf("company_id")].", 'load_drop_down_supplier', 'supplier' );\n";
		echo "$('#cbo_receive_basis').val(".$row[csf("receive_basis")].");\n";
		echo "$('#cbo_receive_purpose').val(".$row[csf("receive_purpose")].");\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("receive_date")])."');\n";
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		echo "$('#txt_exchange_rate').val(".$row[csf("exchange_rate")].");\n";
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		
		if($row[csf("receive_basis")]==1)
			$wopi=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("booking_id")]."");	
		else if($row[csf("receive_basis")]==2 && $row[csf("receive_purpose")]==2)
			$wopi=return_field_value("ydw_no","wo_yarn_dyeing_mst","id=".$row[csf("booking_id")]."");
		else
			$wopi=return_field_value("wo_number","wo_non_order_info_mst","id=".$row[csf("booking_id")]."");
			
		echo "$('#txt_wo_pi').val('".$wopi."');\n";
		echo "$('#txt_wo_pi_id').val(".$row[csf("booking_id")].");\n";
		
		if($row[csf("receive_basis")]==1 || $row[csf("receive_basis")]==2)
		{
			echo "show_list_view('".$row[csf("receive_basis")]."**".$row[csf("booking_id")]."**".$row[csf("receive_purpose")]."','show_product_listview','list_product_container','requires/yarn_receive_controller','');\n";
			
			if($row[csf("receive_basis")]==2 && $row[csf("receive_purpose")]==2)
			{
				echo "load_drop_down( 'requires/yarn_receive_controller','".$row[csf("booking_id")]."','load_drop_down_color', 'color_td_id' );\n";
			}
			else if($row[csf("receive_basis")]==2 && $row[csf("receive_purpose")]==15)
			{
				echo "load_supplier();\n";
			}
		}
		
		echo "$('#txt_issue_challan_no').val(".$row[csf("yarn_issue_challan_no")].");\n";
		echo "$('#txt_issue_id').val(".$row[csf("issue_id")].");\n";
		echo "$('#hidden_lc_id').val(".$row[csf("lc_no")].");\n";
		$lcNumber = return_field_value("lc_number","com_btb_lc_master_details","id=".$row[csf("lc_no")]."");
		echo "$('#txt_lc_no').val('".$lcNumber."');\n";
		
		//right side list view
		echo "show_list_view('".$row[csf("recv_number")]."**".$row[csf("id")]."','show_dtls_list_view','list_container_yarn','requires/yarn_receive_controller','');\n";
		
 	}
	
	exit();	
}




if($action=="show_dtls_list_view")
{
	
	$ex_data = explode("**",$data);
	$recv_number = $ex_data[0];
	$rcv_mst_id = $ex_data[1];
	
	$cond="";
	if($recv_number!="") $cond .= " and a.recv_number='$recv_number'";
	if($rcv_mst_id!="") $cond .= " and a.id='$rcv_mst_id'";
	
	$sql = "select a.recv_number, a.receive_purpose, b.id, a.booking_id, a.receive_basis,b.pi_wo_batch_no,c.product_name_details,c.lot,b.order_uom,b.order_qnty,b.order_rate,b.order_ile_cost,b.order_amount,b.cons_amount 
			from inv_receive_master a, inv_transaction b,  product_details_master c 
			where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category=1 and a.entry_form=1 $cond";
	//echo $sql;		
	$result = sql_select($sql);
	$i=1;
	$totalQnty=0;
	$totalAmount=0;
	$totalbookCurr=0;
	?>
    	<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" style="990" rules="all">
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>WO/PI No</th>
                    <th>MRR No</th>
                    <th>Product Details</th>
                    <th>Yarn Lot</th>
                    <th>UOM</th>
                    <th>Receive Qty</th>
                    <th>Rate</th>
                    <th>ILE Cost</th>
                    <th>Amount</th>
                    <th>Book Currency</th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					
					$wopi="";
					if($row[csf("receive_basis")]==1)
						$wopi=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("booking_id")]."");	
					else if($row[csf("receive_basis")]==2 && $row[csf("receive_purpose")]==2)
						$wopi=return_field_value("ydw_no","wo_yarn_dyeing_mst","id=".$row[csf("booking_id")]."");	
					else
						$wopi=return_field_value("wo_number","wo_non_order_info_mst","id=".$row[csf("booking_id")]."");	
					
					$totalQnty +=$row[csf("order_qnty")];
					$totalAmount +=$row[csf("order_amount")];
					$totalbookCurr +=$row[csf("cons_amount")];	
					
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>","child_form_input_data","requires/yarn_receive_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $wopi; ?></p></td>
                        <td width="130"><p><?php echo $row[csf("recv_number")]; ?></p></td>
                        <td width="200"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td width="80"><p><?php echo $row[csf("lot")]; ?></p></td>
                        <td width="60"><p><?php echo $unit_of_measurement[$row[csf("order_uom")]]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("order_qnty")]; ?></p></td>
                         <td width="60" align="right"><p><?php echo $row[csf("order_rate")]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("order_ile_cost")]; ?></p></td>                       
                        <td width="70" align="right"><p><?php echo $row[csf("order_amount")]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("cons_amount")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                        <th colspan="6">Total</th>                         
                        <th><?php echo $totalQnty; ?></th>
                        <th colspan="2"></th>
                        <th><?php echo $totalAmount; ?></th>
                        <th><?php echo $totalbookCurr; ?></th>
                        <th></th>
                  </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}


if($action=="child_form_input_data")
{
	
	$rcv_dtls_id = $data;	
 	
	$sql = "select a.currency_id,a.exchange_rate,b.id,b.receive_basis, b.job_no, a.receive_purpose,b.pi_wo_batch_no,b.prod_id,b.brand_id,c.lot,b.order_uom,b.order_qnty,b.order_rate,b.order_ile_cost,b.order_amount,b.cons_amount,b.no_of_bags,b.product_code,b.room,b.rack,b.self,b.bin_box,b.cone_per_bag, b.weight_per_bag, b.weight_per_cone, b.remarks
			from inv_receive_master a, inv_transaction b, product_details_master c  
			where a.id=b.mst_id and b.prod_id=c.id and b.id='$rcv_dtls_id'";
			//echo $sql;
	$result = sql_select($sql);
    
	foreach($result as $row)
	{
		$sql = sql_select("select yarn_count_id,yarn_comp_type1st,yarn_comp_percent1st,yarn_comp_type2nd,yarn_comp_percent2nd,yarn_type,color from product_details_master where id=".$row[csf("prod_id")]."");
		
   		echo "$('#cbo_yarn_count').val(".$sql[0][csf("yarn_count_id")].");\n";
		echo "$('#cbocomposition1').val(".$sql[0][csf("yarn_comp_type1st")].");\n";
		echo "$('#percentage1').val(".$sql[0][csf("yarn_comp_percent1st")].");\n";
		echo "$('#cbocomposition2').val(".$sql[0][csf("yarn_comp_type2nd")].");\n";		
		if($sql[0][csf("yarn_comp_percent2nd")]==0) $sql[0][csf("yarn_comp_percent2nd")]="";
		echo "$('#percentage2').val('".$sql[0][csf("yarn_comp_percent2nd")]."');\n";		
		echo "control_composition('percent_one');\n";
		echo "$('#cbo_yarn_type').val(".$sql[0][csf("yarn_type")].");\n";
		
		
		//echo "$('#btn_color').val('N');\n";
		if($row[csf("receive_basis")]!=2 && $row[csf("receive_basis")]!=2)
		{
			echo "load_drop_down('requires/yarn_receive_controller', '', 'load_drop_down_color', 'color_td_id' );\n";
		}
		echo "$('#cbo_color').val(".$sql[0][csf("color")].");\n";
		
		echo "$('#txt_yarn_lot').val('".$row[csf("lot")]."');\n";
		$brand_name = return_field_value("brand_name","lib_brand","id=".$row[csf("brand_id")]."");
		echo "$('#txt_brand').val('".$brand_name."');\n";
		echo "$('#txt_receive_qty').val(".$row[csf("order_qnty")].");\n";
		echo "$('#txt_rate').val(".$row[csf("order_rate")].");\n";
		echo "$('#txt_ile').val(".$row[csf("order_ile_cost")].");\n";
		echo "$('#cbo_uom').val(".$row[csf("order_uom")].");\n";
		echo "$('#txt_amount').val(".$row[csf("order_amount")].");\n";
		echo "$('#txt_book_currency').val(".$row[csf("cons_amount")].");\n";
		echo "$('#txt_order_qty').val(0);\n";
		echo "$('#txt_no_bag').val(".$row[csf("no_of_bags")].");\n";
		echo "$('#txt_cone_per_bag').val(".$row[csf("cone_per_bag")].");\n";
		echo "$('#txt_weight_per_bag').val(".$row[csf("weight_per_bag")].");\n";
		echo "$('#txt_weight_per_cone').val(".$row[csf("weight_per_cone")].");\n";
		
		echo "$('#txt_prod_code').val(".$row[csf("prod_id")].");\n";
		echo "$('#job_no').val('".$row[csf("job_no")]."');\n";
		
		echo "$('#txt_room').val(".$row[csf("room")].");\n";
		echo "$('#txt_rack').val(".$row[csf("rack")].");\n";
		echo "$('#txt_self').val(".$row[csf("self")].");\n";
		echo "$('#txt_binbox').val(".$row[csf("bin_box")].");\n";
		//echo "$('#txt_remarks').val(".$row[csf("remarks")].");\n";
		echo "$('#txt_remarks').val('".$row[csf("remarks")]."');\n";
		//update id here
		echo "$('#update_id').val(".$row[csf("id")].");\n";
		
		/*if($row[csf("receive_basis")]==1 || $row[csf("receive_basis")] ==2) // call for only PI BASIS / WO BASIS
		{
			echo "show_list_view(".$row[csf("receive_basis")]."+'**'+".$row[csf("pi_wo_batch_no")].",'show_product_listview','list_product_container','requires/yarn_receive_controller','');\n";
		}*/
		
		echo "set_button_status(1, permission, 'fnc_yarn_receive_entry',1,1);\n";
		echo "disable_enable_fields( 'cbo_receive_basis*cbo_receive_purpose*txt_receive_date*txt_challan_no*cbo_store_name*txt_exchange_rate', 0, '', '');\n";
		echo "fn_calile();\n";
	}
	
	exit();
}


if($action=="issue_challan_popup_info")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST); 
	
	$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count'); 
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
?>
     
<script>
	function js_set_value(data)
	{
 		var splitArr = data.split("_");
 		$("#hidden_issue_id").val(splitArr[0]); 		// id number
		$("#hidden_challan_number").val(splitArr[1]); 	// mrr number
		parent.emailwindow.hide();
	}
	
	$(document).ready(function(e) {
		setFilterGrid('tbl_list_search',-1);
	});
</script>

</head>

<body>
<div align="center" style="width:100%; margin-top:10px" >
	<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
    	<input type="hidden" name="hidden_issue_id" id="hidden_issue_id">
        <input type="hidden" name="hidden_challan_number" id="hidden_challan_number">
		<table width="630" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all">
			<thead>
            	<th width="40">SL</th>
                <th width="130">Issue System Id</th>
                <th width="130">Challan No</th>
                <th width="140">Issue To</th>
                <th>Yarn Count</th>
            </thead>
		</table>
        <div style="width:630px; overflow-y: scroll; max-height:320px;" id="scroll_body">
			<table width="612" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" id="tbl_list_search">
            <?php
				$i=1;
				if($db_type==0)
				{
					$sql="select a.id, a.issue_number, a.challan_no, a.knit_dye_company, group_concat(distinct(c.yarn_count_id)) as yarn_count_id from inv_issue_master a, inv_transaction b, product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.item_category=1 and b.transaction_type=2 and a.knit_dye_source=3 and a.knit_dye_company='$supplier' and a.issue_purpose=15 and a.entry_form=3 and a.status_active=1 and a.is_deleted=0 group by a.id";
				}
				else
				{
					$sql="select a.id, a.issue_number, a.challan_no, a.knit_dye_company, LISTAGG(c.yarn_count_id, ',') WITHIN GROUP (ORDER BY c.yarn_count_id) as yarn_count_id from inv_issue_master a, inv_transaction b, product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.item_category=1 and b.transaction_type=2 and a.knit_dye_source=3 and a.knit_dye_company='$supplier' and a.issue_purpose=15 and a.entry_form=3 and a.status_active=1 and a.is_deleted=0 group by a.id, a.issue_number, a.challan_no, a.knit_dye_company";
				}
				$result=sql_select($sql);
				foreach($result as $row)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$data=$row[csf('id')]."_".$row[csf('challan_no')];
					
					$issue_to=$supplier_library[$row[csf('knit_dye_company')]];
					
					$yarn_count_id=array_unique(explode(",",$row[csf('yarn_count_id')]));
					$yarn_count='';
					foreach($yarn_count_id as $count_id)
					{
						if($yarn_count=="") $yarn_count=$yarn_count_arr[$count_id]; else $yarn_count.=",".$yarn_count_arr[$count_id];
					}
				?>
					<tr bgcolor="<?php echo $bgcolor;?>" style="cursor:pointer" onClick="js_set_value('<?php echo $data; ?>');">
						<td width="40"><?php echo $i; ?></td>
						<td width="130"><p>&nbsp;<?php echo $row[csf('issue_number')]; ?></p></td>
						<td width="130"><p><?php echo $row[csf('challan_no')]; ?>&nbsp;</p></td>
						<td width="140"><p><?php echo $issue_to; ?>&nbsp;</p></td>
						<td><p><?php echo $yarn_count; ?>&nbsp;</p></td>
					</tr>
                <?php
					$i++;
				}
				?>
            </table>
 		</div>   
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php

exit;
}

if($action=="is_allocation_maintained")
{
	$allocation_maintained=return_field_value("allocation","variable_settings_inventory","company_name ='$data' and variable_list=18 and item_category_id=1 and is_deleted=0 and status_active=1");
	if($allocation_maintained!=1) $allocation_maintained=0;
	echo "document.getElementById('allocation_maintained').value 	= '".$allocation_maintained."';\n";
	exit();	
}


//################################################# function Here #########################################//


//function for domestic rate find--------------//
//parameters rate,ile cost,exchange rate,conversion factor
function return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor){
	$rate_ile=$rate+$ile_cost;
	$rate_ile_exchange=$rate_ile*$exchange_rate;
	$doemstic_rate=$rate_ile_exchange/$conversion_factor;
	return $doemstic_rate;	
}


//return product master table id ----------------------------------------//
function return_product_id($yarncount,$composition_one,$composition_two,$percentage_one,$percentage_two,$yarntype,$color,$yarnlot,$prodCode,$company,$supplier,$store,$uom,$yarn_type,$composition)
{	
	 
	$composition_one = str_replace("'","",$composition_one);
	$composition_two = str_replace("'","",$composition_two);
	$percentage_one = str_replace("'","",$percentage_one);
	$percentage_two = str_replace("'","",$percentage_two);
	$yarntype = str_replace("'","",$yarntype);
	$color = str_replace("'","",$color);
	$yarncount = str_replace("'","",$yarncount);
	if($percentage_one=="" ) $percentage_one=0;
	if($percentage_two=="" ) $percentage_two=0;
	
	//NOTE :- Yarn category array ID=1
	$whereCondition = "yarn_count_id=$yarncount and yarn_comp_type1st=$composition_one and yarn_comp_percent1st=$percentage_one
						and yarn_comp_type2nd=$composition_two and yarn_comp_percent2nd=$percentage_two and yarn_type=$yarntype and color=$color
						and company_id=$company and supplier_id=$supplier and item_category_id=1 and lot=$yarnlot"; //and store_id=$store
  	$prodMSTID = return_field_value("id","product_details_master","$whereCondition");
	$insertResult = true;
	if($prodMSTID==false || $prodMSTID=="")
	{
		// new product create here--------------------------//
		$yarn_count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
		$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
		
 		$compositionPart = $composition[$composition_one]." ".$percentage_one;
		if( $percentage_two != 0 )
		{
			$compositionPart .=" ".$composition[$composition_two]." ".$percentage_two;
		}
		 
		//$yarn_count.','.$composition.','.$ytype.','.$color;
		$product_name_details = $yarn_count_arr[$yarncount]." ".$compositionPart." ".$yarn_type[$yarntype]." ".$color_name_arr[$color];
 		$prodMSTID=return_next_id("id", "product_details_master", 1);
		$field_array = "id,company_id,supplier_id,item_category_id,product_name_details,lot,item_code,unit_of_measure,yarn_count_id,yarn_comp_type1st,yarn_comp_percent1st,yarn_comp_type2nd,yarn_comp_percent2nd,yarn_type,color";
 		$data_array = "(".$prodMSTID.",".$company.",".$supplier.",1,'".$product_name_details."',".$yarnlot.",".$prodCode.",".$uom.",".$yarncount.",".$composition_one.",".$percentage_one.",".$composition_two.",".$percentage_two.",".$yarntype.",".$color.")";
		//echo $field_array."<br>".$data_array."--".$product_name_details;die;
		$insertResult = false;
		//$insertResult = sql_insert("product_details_master",$field_array,$data_array,1);		
	}
	if($insertResult == true)
	{
		return $insertResult."***".$prodMSTID;
	}else{
		return $insertResult."***".$field_array."***".$data_array."***".$prodMSTID;
	}
}

if ($action=="yarn_receive_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
?>
	<script>
	
	</script>
    <?php
	$sql=" select id, recv_number,supplier_id,currency_id,challan_no, receive_date, exchange_rate, store_id, receive_basis,lc_no from inv_receive_master where recv_number='$data[1]'";
	$dataArray=sql_select($sql);
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$location=return_field_value("location_name","lib_location","company_id=$data[0]");
	$address=return_field_value("address","lib_location","company_id=$data[0]");
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$yarn_desc_arr=return_library_array( "select id,yarn_description from lib_subcon_charge",'id','yarn_description');
	$const_comp_arr=return_library_array( "select id,const_comp from lib_subcon_charge",'id','const_comp');
	$lcNum=return_library_array( "select id,lc_number from com_btb_lc_master_details",'id','lc_number');

?>
	<div id="table_row" style="width:930px;">
        <table width="900" align="right">
            <tr class="form_caption">
                <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
            </tr>
            <tr class="form_caption">
                <td colspan="6" align="center">
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center" style="font-size:x-large"><strong><u>Material Receive Report</u></strong></center></td>
            </tr>
            <tr>
                <td width="130"><strong>Supplier Name:</strong></td> <td width="175px"><?php echo $supplier_library[$dataArray[0][csf('supplier_id')]]; ?></td>
                <td width="120"><strong>MRR No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('recv_number')]; ?></td>
                <td width="125"><strong>Currency:</strong></td><td width="175px"><?php echo $currency[$dataArray[0][csf('currency_id')]]; ?></td>
            </tr>
            <tr>
                <td><strong>Challan No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
                <td><strong>Receive Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('receive_date')]); ?></td>
                <td><strong>Exchange Rate:</strong></td><td width="175px"><?php echo $dataArray[0][csf('exchange_rate')]; ?></td>
            </tr>
            <tr>
                <td><strong>Store Name:</strong></td> <td width="175px"><?php echo $store_library[$dataArray[0][csf('store_id')]]; ?></td>
                <td><strong>Receive Basis:</strong></td><td width="175px"><?php echo $receive_basis_arr[$dataArray[0][csf('receive_basis')]]; ?></td>
                <?php
				if ($dataArray[0][csf('receive_basis')]==1)
				{
				?>
                    <td><strong>LC NO:</strong></td><td width="175px"><?php echo $lcNum[$dataArray[0][csf('lc_no')]]; ?></td>
                <?php
				}
				?>
                
            </tr>
        </table>
             <br>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="30">SL</th>
                <th width="80" align="center">WO/PI No</th>
                <th width="160" align="center">Item Details</th>
                <th width="60" align="center">Yarn Lot</th> 
                <th width="40" align="center">UOM</th>
                <th width="60" align="center">Receive Qty</th>
                <th width="60" align="center">Rate</th>                   
                <th width="60" align="center">ILE Cost</th>
                <th width="85" align="center">Amount</th>
                <th width="60" align="center">No. Of Bag</th>
                <th width="60" align="center">Cons Per Bag</th>
                <th width="145" align="center">Remarks</th>
            </thead>
     <?php
	$pi_library=return_library_array( "select id,pi_number from  com_pi_master_details", "id","pi_number");
	$wo_library=return_library_array( "select id,wo_number from  wo_non_order_info_mst", "id","wo_number");
	$wo_yrn_library=return_library_array( "select id, ydw_no from  wo_yarn_dyeing_mst", "id","ydw_no");
	$cond="";
	if($data[1]!="") $cond .= " and a.recv_number='$data[1]'";
	
     $i=1;
	$sql_result= sql_select("select a.recv_number, a.receive_basis, a.receive_purpose, b.id, b.receive_basis, b.pi_wo_batch_no, b.cone_per_bag, c.product_name_details, c.lot, b.order_uom, b.order_qnty, b.order_rate, b.order_ile_cost, b.order_amount, b.cons_amount, b.no_of_bags, b.remarks 
			from inv_receive_master a, inv_transaction b,  product_details_master c 
			where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category=1 and a.entry_form=1 $cond");
			//echo $sql_result;
			foreach($sql_result as $row)
			{
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";
					$order_qnty_val=$row[csf('order_qnty')];
					$order_qnty_val_sum += $order_qnty_val;
					
					$order_amount_val=$row[csf('order_amount')];
					$order_amount_val_sum += $order_amount_val;
					
					$no_of_bags_val=$row[csf('no_of_bags')];
					$no_of_bags_val_sum += $no_of_bags_val;
					
					$con_per_bags_val=$row[csf('cone_per_bag')];
					$con_per_bags_sum += $con_per_bags_val;

		?>
			<tr bgcolor="<?php echo $bgcolor; ?>"> 
                <td><?php echo $i; ?></td>
                <?php
					if ($row[csf("receive_basis")]==1)
					{
				?>
                        <td><?php echo $pi_library[$row[csf('pi_wo_batch_no')]]; ?></td>
                <?php
					}
					else if ($row[csf("receive_basis")]==2 && $row[csf("receive_purpose")]==2)
					{
				?>
                        <td><?php echo $wo_yrn_library[$row[csf('pi_wo_batch_no')]]; ?></td>
                <?php
					}
					else 
					{
				?>
                		<td><?php echo $wo_library[$row[csf('pi_wo_batch_no')]]; ?></td>
				<?php		
					}
				?>
                <td><?php echo $row[csf('product_name_details')]; ?></td>
                <td><?php echo $row[csf('lot')]; ?></td>
                <td><?php echo $unit_of_measurement[$row[csf('order_uom')]]; ?></td>
                <td align="right"><?php echo number_format($order_qnty_val,2,'.',',')//$row[csf('order_qnty')]; ?></td>
                <td align="right"><?php echo number_format($row[csf('order_rate')],4,'.',','); ?></td>
                <td align="right"><?php echo $row[csf('order_ile_cost')]; ?></td>
                <td align="right"><?php echo number_format($order_amount_val,2,'.',','); ?></td>
                <td align="right"><?php echo $no_of_bags_val; ?></td>
                <td align="right"><?php echo $con_per_bags_val; ?></td>
                <td><?php echo $row[csf('remarks')]; ?></td>
			</tr>
			<?php
			$i++;
			}
		?>
        	<tr> 
                <td align="right" colspan="5" >Total : </td>
                <td align="right"><?php echo number_format($order_qnty_val_sum,2,'.',',')  //$total_order_qnty; ?></td>
                <td align="right" colspan="3"><?php echo number_format($order_amount_val_sum,2,'.',',') ?></td>
                <td align="right"><?php echo $no_of_bags_val_sum; ?></td>
                <td align="right"><?php echo $con_per_bags_sum; ?></td>
                <td align="right">&nbsp;</td>
			</tr>
        </table>
        <br>
		 <?php
            echo signature_table(65, $data[0], "930px");
         ?>
</div>
</div>
<?php
exit();
}


?>

