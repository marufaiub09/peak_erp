﻿<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="save_update_delete")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	 
	if ($operation==0)  // Insert Here
	{
		$txt_lc_number=str_replace("'","",$txt_bank_code).str_replace("'","",$txt_lc_year).str_replace("'","",$txt_category).str_replace("'","",$txt_lc_serial);
		if($txt_lc_number!="")
		{
			if (is_duplicate_field( "lc_number", "com_btb_lc_master_details", "lc_number='$txt_lc_number' and importer_id=$cbo_importer_id and supplier_id=$cbo_supplier_id" ) == 1)
			{
				echo "11**0"; die;
			}
		}
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if(str_replace("'", '',$cbo_lc_currency_id)==1)
		{
			$txt_lc_value=number_format(str_replace("'", '',$txt_lc_value),$dec_place[4],'.','');
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[4],'.','');
		}
		else
		{
			$txt_lc_value=number_format(str_replace("'", '',$txt_lc_value),$dec_place[5],'.','');
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[5],'.','');
		}
		 
		$id=return_next_id( "id", "com_btb_lc_master_details", 1 ); 
		
		$tolarence_value = (($txt_tolerance*$txt_lc_value)/100);
		$txt_max_lc_value = $txt_lc_value + $tolarence_value;
		$txt_min_lc_value = $txt_lc_value - $tolarence_value;
		
		if(str_replace("'","",$cbo_lc_type_id)==1)
			$prefix="BTB";
		else if(str_replace("'","",$cbo_lc_type_id)==2)
			$prefix="MRGN";
		else if(str_replace("'","",$cbo_lc_type_id)==3)
			$prefix="FUND";	
		
		if($db_type==0) $year_cond="YEAR(insert_date)"; 
		else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
		else $year_cond="";//defined Later
		
		$new_contact_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_importer_id), '', $prefix, date("Y",time()), 5, "select btb_prefix,btb_prefix_number from com_btb_lc_master_details where importer_id=$cbo_importer_id and $year_cond=".date('Y',time())." order by id desc ", "btb_prefix", "btb_prefix_number" ));
		
		$field_array="id,btb_prefix,btb_prefix_number,btb_system_id,lc_number,bank_code,lc_year,lc_category,lc_serial,supplier_id,importer_id,application_date,lc_date,last_shipment_date,lc_expiry_date,lc_type_id,lc_value,max_lc_value,min_lc_value,currency_id,issuing_bank_id,item_category_id,tenor,tolerance,inco_term_id,inco_term_place,delivery_mode_id,etd_date,insurance_company_name,lca_no,lcaf_no,imp_form_no,psi_company,cover_note_no,cover_note_date,maturity_from_id,margin,origin,bonded_warehouse,item_basis_id,garments_qty,credit_to_be_advised,partial_shipment,transhipment,shipping_mark,doc_presentation_days,port_of_loading,port_of_discharge,remarks,pi_id,pi_value,payterm_id,uom_id,ud_no,ud_date,credit_advice_id,confirming_bank,inserted_by,insert_date,status_active,is_deleted";
	
		$data_array="(".$id.",'".$new_contact_system_id[1]."',".$new_contact_system_id[2].",'".$new_contact_system_id[0]."','".$txt_lc_number."',".$txt_bank_code.",".$txt_lc_year.",".$txt_category.",".$txt_lc_serial.",".$cbo_supplier_id.",".$cbo_importer_id.",".$application_date.",".$txt_lc_date.",".$txt_last_shipment_date.",".$txt_lc_expiry_date.",".$cbo_lc_type_id.",".$txt_lc_value.",".$txt_max_lc_value.",".$txt_min_lc_value.",".$cbo_lc_currency_id.",".$cbo_issuing_bank.",".$cbo_item_category_id.",".$txt_tenor.",".$txt_tolerance.",".$cbo_inco_term_id.",".$txt_inco_term_place.",".$cbo_delevery_mode.",".$txt_etd_date.",".$txt_insurance_company.",".$txt_lca_no.",".$txt_lcaf_no.",".$txt_imp_form_no.",".$txt_psi_company.",".$txt_cover_note_no.",".$txt_cover_note_date.",".$cbo_maturit_from_id.",".$txt_margin_deposit.",".$cbo_origin_id.",".$cbo_bond_warehouse_id.",".$cbo_lc_basis_id.",".$txt_gmt_qnty.",".$cbo_credit_advice_id.",".$cbo_partial_ship_id.",".$cbo_transhipment_id.",".$txt_shiping_mark.",".$txt_doc_perc_days.",".$txt_port_loading.",".$txt_port_discharge.",".$txt_remarks.",".$txt_hidden_pi_id.",'".$txt_pi_value."',".$cbo_payterm_id.",".$cbo_gmt_uom_id.",".$txt_ud_no.",".$txt_ud_date.",".$cbo_credit_advice_id.",".$txt_conf_bank.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',".$cbo_status.",0)";                                  
		
		$flag=1;
		//----------Insert Data in  com_btb_lc_pi Table----------------------------------------
		if(str_replace("'","",$txt_hidden_pi_id)!="")
		{
			$data_array2="";
			$field_array2="id, com_btb_lc_master_details_id, pi_id, inserted_by, insert_date";
			$tag_pi=explode(',',str_replace("'","",$txt_hidden_pi_id));
			$id_lbtb_lc_pi=return_next_id( "id","com_btb_lc_pi", 1 );
			for($i=0; $i<count($tag_pi); $i++)
			{  
				if($i==0) $add_comma=""; else $add_comma=",";
				$data_array2.="$add_comma(".$id_lbtb_lc_pi.",".$id.",".$tag_pi[$i].",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id_lbtb_lc_pi++;
			}

			$rID2=sql_insert("com_btb_lc_pi",$field_array2,$data_array2,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		//echo "0**"."insert into com_btb_lc_master_details (".$field_array.") values ".$data_array;die;
		$rID=sql_insert("com_btb_lc_master_details",$field_array,$data_array,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0;
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'", '', $id)."**".$new_contact_system_id[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**"."0"."**".$new_contact_system_id[0];
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "0**".str_replace("'", '', $id)."**".$new_contact_system_id[0];
			}
			else
			{
				oci_rollback($con);
				echo "5**"."0"."**".$new_contact_system_id[0];
			}
		}
		disconnect($con);
		die;
		
	}
	else if ($operation==1)   // Update Here
	{
		$txt_lc_number=str_replace("'","",$txt_bank_code).str_replace("'","",$txt_lc_year).str_replace("'","",$txt_category).str_replace("'","",$txt_lc_serial);
		
		if($txt_lc_number!="")
		{
			if (is_duplicate_field( "lc_number", "com_btb_lc_master_details", "lc_number='$txt_lc_number' and importer_id=$cbo_importer_id and supplier_id=$cbo_supplier_id and id!=$update_id" ) == 1)
			{
				echo "11**0"; die;
			}
		}
		
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if(str_replace("'", '',$cbo_lc_currency_id)==1)
		{
			$txt_lc_value=number_format(str_replace("'", '',$txt_lc_value),$dec_place[4],'.','');
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[4],'.','');
		}
		else
		{
			$txt_lc_value=number_format(str_replace("'", '',$txt_lc_value),$dec_place[5],'.','');
			$txt_pi_value=number_format(str_replace("'", '',$txt_pi_value),$dec_place[5],'.','');
		}
		
		$tolarence_value = (($txt_tolerance*$txt_lc_value)/100);
		$txt_max_lc_value = $txt_lc_value + $tolarence_value;
		$txt_min_lc_value = $txt_lc_value - $tolarence_value;
		
		$field_array="lc_number*bank_code*lc_year*lc_category*lc_serial*supplier_id*importer_id*application_date*lc_date*last_shipment_date*lc_expiry_date*lc_type_id*lc_value*max_lc_value*min_lc_value*currency_id*issuing_bank_id*item_category_id*tenor*tolerance*inco_term_id*inco_term_place*delivery_mode_id*etd_date*insurance_company_name*lca_no*lcaf_no*imp_form_no*psi_company*cover_note_no*cover_note_date*maturity_from_id*margin*origin*bonded_warehouse*item_basis_id*garments_qty*credit_to_be_advised*partial_shipment*transhipment*shipping_mark*doc_presentation_days*port_of_loading*port_of_discharge*remarks*pi_id*pi_value*payterm_id*uom_id*ud_no*ud_date*credit_advice_id*confirming_bank*updated_by*update_date*status_active*is_deleted";
		$data_array="'".$txt_lc_number."'*".$txt_bank_code."*".$txt_lc_year."*".$txt_category."*".$txt_lc_serial."*".$cbo_supplier_id."*".$cbo_importer_id."*".$application_date."*".$txt_lc_date."*".$txt_last_shipment_date."*".$txt_lc_expiry_date."*".$cbo_lc_type_id."*".$txt_lc_value."*".$txt_max_lc_value."*".$txt_min_lc_value."*".$cbo_lc_currency_id."*".$cbo_issuing_bank."*".$cbo_item_category_id."*".$txt_tenor."*".$txt_tolerance."*".$cbo_inco_term_id."*".$txt_inco_term_place."*".$cbo_delevery_mode."*".$txt_etd_date."*".$txt_insurance_company."*".$txt_lca_no."*".$txt_lcaf_no."*".$txt_imp_form_no."*".$txt_psi_company."*".$txt_cover_note_no."*".$txt_cover_note_date."*".$cbo_maturit_from_id."*".$txt_margin_deposit."*".$cbo_origin_id."*".$cbo_bond_warehouse_id."*".$cbo_lc_basis_id."*".$txt_gmt_qnty."*".$cbo_credit_advice_id."*".$cbo_partial_ship_id."*".$cbo_transhipment_id."*".$txt_shiping_mark."*".$txt_doc_perc_days."*".$txt_port_loading."*".$txt_port_discharge."*".$txt_remarks."*".$txt_hidden_pi_id."*'".$txt_pi_value."'*".$cbo_payterm_id."*".$cbo_gmt_uom_id."*".$txt_ud_no."*".$txt_ud_date."*".$cbo_credit_advice_id."*".$txt_conf_bank."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*".$cbo_status."*0";
		 
		$flag=1;
		//----------Insert Data in  com_btb_lc_pi Table----------------------------------------
		$delete=execute_query( "delete from com_btb_lc_pi where com_btb_lc_master_details_id = $update_id",0);
		if($flag==1) 
		{
			if($delete) $flag=1; else $flag=0; 
		} 
		
		if(str_replace("'","",$txt_hidden_pi_id)!="")
		{
			$data_array2="";
			$tag_pi=explode(',',str_replace("'","",$txt_hidden_pi_id));
			
			$field_array2="id, com_btb_lc_master_details_id, pi_id, inserted_by, insert_date";
			for($i=0; $i<count($tag_pi); $i++)
			{  
				if($id_lbtb_lc_pi=="") {$id_lbtb_lc_pi=return_next_id( "id","com_btb_lc_pi", 1 ); }
				if($i==0) $add_comma=""; else $add_comma=",";
				$data_array2.="$add_comma(".$id_lbtb_lc_pi.",".$update_id.",".$tag_pi[$i].",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id_lbtb_lc_pi++;
			}
		
			$rID2=sql_insert("com_btb_lc_pi",$field_array2,$data_array2,0);
			if($flag==1) 
			{
				if($rID2) $flag=1; else $flag=0; 
			} 
		}
		
		$rID=sql_update("com_btb_lc_master_details",$field_array,$data_array,"id",$update_id,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0;
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_system_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_system_id);
			}
			else
			{
				oci_rollback($con);
				echo "6**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_system_id);
			}
		}
		disconnect($con);
		die;
		
	}
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
								
		$field_array="updated_by*update_date*status_active*is_deleted";
	    $data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
		
		$rID=sql_delete("com_btb_lc_master_details",$field_array,$data_array,"id","".$update_id."",1);
		$delete=execute_query( "delete from com_btb_lc_pi where com_btb_lc_master_details_id = $update_id",0); 
		
		if($db_type==0)
		{
			if($rID && $delete)
			{
				mysql_query("COMMIT");  
				echo "2**".$rID;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "7**".$rID;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $delete)
			{
				oci_commit($con);
				echo "2**".$rID;
			}
			else
			{
				oci_rollback($con); 
				echo "7**".$rID;
			}
		}
		
		disconnect($con);
	} 
}

///Save Item Details Table

if ($action=="save_update_delete_dtls")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	 
	if($operation==0 || $operation==1) 
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
	 	
		 $delete=execute_query( "delete from com_btb_export_lc_attachment where import_mst_id = $update_id",0);
		 
		 $id=return_next_id( "id","com_btb_export_lc_attachment", 1 ) ;

		 $field_array="id,import_mst_id,lc_sc_id,is_lc_sc,current_distribution,is_deleted,status_active"; 
		
		 for($i=1;$i<=$total_row;$i++)
		 {
			$txtlcscid="txtLcScid_".$i;
			$txtlcscflag="txtlcscflagId_".$i;
			$txtcurdistribution="txtcurdistribution_".$i;
			$cbostatus="cbostatus_".$i; 
			
			if(str_replace("'","",$$txtlcscid)!='')
			{ 
				if($data_array!="") $data_array.=",";

				$data_array .="(".$id.",".$update_id.",'".str_replace("'","",$$txtlcscid)."','".str_replace("'","",$$txtlcscflag)."','".str_replace("'","",$$txtcurdistribution)."',0,'".str_replace("'","",$$cbostatus)."')";
				
				$id=$id+1;
			}
		 }
		
		$rID=sql_insert("com_btb_export_lc_attachment",$field_array,$data_array,1);
		 
		if($operation==0) $msg=5; else $msg=6;
		
		if($db_type==0)
		{
			if($rID && $delete)
			{
				mysql_query("COMMIT");  
				echo $operation."**".$rID;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo $msg."**".$rID;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $delete)
			{
				oci_commit($con);  
				echo $operation."**".$rID;
			}
			else
			{
				oci_rollback($con); 
				echo $msg."**".$rID;
			}
		}
		disconnect($con);
		die;
	}
}


//--------------------------------------------Start Pi Details List----------------------------------------------------------------//
if( $action == 'show_pi_details_list' ) 
 {	
	$data = explode('_',$data);
	 
	$pi_mst_id = $data[0];
	$cbo_item_category_id = $data[1];
	//$pi_mst_id = explode('*',$pi_mst_id);
	$size_library = return_library_array('SELECT id,size_name FROM lib_size','id','size_name');
	$color_library = return_library_array('SELECT id,color_name FROM lib_color','id','color_name');
	?>
    <table class="rpt_table" width="100%" cellspacing="1" rules="all">
    <?php
	switch($cbo_item_category_id ) 
		{
			case 1:			//Yarn
			$sql = "SELECT b.pi_number, a.id, a.color_id, a.count_name, a.yarn_composition_item1, a.yarn_composition_percentage1, a.yarn_composition_item2,a.yarn_composition_percentage2, a.yarn_type,a.uom,a.quantity,a.net_pi_rate as rate, a.net_pi_amount as amount FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id)  AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC";
			$data_array=sql_select($sql);	
			 
			$yarn_count = return_library_array('SELECT id,yarn_count FROM lib_yarn_count','id','yarn_count');
		   
			?>
				<thead>
					<tr> 
                    	<th>PI No</th>
						<th>Color</th>
						<th>Count</th>
						<th colspan="4">Composition</th>
						<th>Yarn Type</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Rate</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
				<?php
                $i = 0;
                foreach($data_array as $row) 
                {
                    $i++;
                    if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
                    else $bgcolor = "#FFFFFF";//get_php_form_data( theemail.value, "populate_data_from_search_popup", "requires/pi_controller" );
                ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>">
                        <td><?php echo $row[csf('pi_number')]; ?></td>
                        <td width="110"><?php echo $color_library[$row[csf('color_id')]]; ?></td>
                        <td width="85"><?php echo $yarn_count[$row[csf('count_name')]]; ?></td>
                        <td width="90"><?php echo $composition[$row[csf('yarn_composition_item1')]]; ?></td>
                        <td width="40" align="right"><?php echo $row[csf('yarn_composition_percentage1')]; ?>%</td>
                        <td width="90"><?php echo $composition[$row[csf('yarn_composition_item2')]]; ?>&nbsp;</td>
                        <td width="40" align="right"><?php if($row[csf('yarn_composition_percentage2')]!=0) echo $row[csf('yarn_composition_percentage2')]."%"; ?>&nbsp;</td>
                        <td width="120">
                        	<?php if( $row[csf('yarn_type')] != 0 ) echo $yarn_type[$row[csf('yarn_type')]]; ?>
                        </td>
                        <td width="60">
                        	<?php if( $row[csf('uom')] != 0 ) echo $unit_of_measurement[$row[csf('uom')]]; ?>
                        </td>
                        <td width="100"  align="right"><?php echo number_format($row[csf('quantity')],2);  $total_quantity += $row[csf('quantity')];?></td>
                        <td width="60" align="right"><?php echo $row[csf('rate')]; ?></td>
                        <td width="100" align="right"><?php echo number_format($row[csf('amount')],2);  $total_ammount += $row[csf('amount')];?></td>
                    </tr>
                <?php 
                } 
                ?>
                <tr class="tbl_bottom"> 
                    <td colspan="9">Sum</td> 
                    <td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
                    <td></td>
                    <td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
                </tr>
             </tbody>
            <?php 				
			 
			break;
			case 2:			//Knit Fabric
			case 13:		//grey fabric Knit Fabric
				?>
				<thead>
					<tr> 
                    	<th>PI No</th>
                        <th>Construction</th>
                        <th>Composition</th>
                        <th>Color</th>					
                        <th>GSM</th>
                        <th>Dia/Width</th>
                        <th>UOM</th>
                        <th>Quantity</th>
                        <th>Rate</th>
                        <th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$data_array=sql_select("SELECT b.pi_number, a.fabric_composition, a.fabric_construction, a.color_id, a.gsm,a.dia_width, a.dia_width, a.uom, a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id) and a.quantity>0 AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");
					$color_library = return_library_array('SELECT id,color_name FROM lib_color','id','color_name');
					 
					$i = 0;
                    foreach($data_array as $row) 
					{
						$i++;
						if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
						else $bgcolor = "#FFFFFF";//get_php_form_data( theemail.value, "populate_data_from_search_popup", "requires/pi_controller" );
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" height="25">
                            <td><?php echo $row[csf('pi_number')]; ?></td> 
                            <td><?php echo $row[csf('fabric_construction')]; ?></td>
                            <td><?php echo $row[csf('fabric_composition')]; ?></td>
                            <td><?php echo $color_library[$row[csf('color_id')]]; ?></td>
                            <td><?php echo $row[csf('gsm')]; ?></td>
                            <td><?php echo $row[csf('dia_width')]; ?></td>
                            <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                            <td align="right"><?php echo number_format($row[csf('quantity')],2); $total_quantity += $row[csf('quantity')]; ?></td>
                            <td align="right"><?php echo $row[csf('rate')]; ?></td>
                            <td align="right"><?php echo number_format($row[csf('amount')],2); $total_ammount += $row[csf('amount')]; ?></td>
                        </tr>
					<?php 
					}
					?>
                    <tr class="tbl_bottom" height="25"> 
                        <td colspan="8">Sum</td> 
                        <td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
                        <td></td>
                        <td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
                    </tr>
                </tbody>  
				 <?php
				break;
				case 3:			//Woven Fabric
				case 14:		//Grey Fabric Woven
			?>
			<thead>
				<tr>
                	<th>PI No</th>
					<th>Construction</th>
					<th>Composition</th> 
                    <th>Color</th>
                    <th>Weight</th>
					<th>Width</th>
					<th>UOM</th>
					<th>Quantity</th>
					<th>Rate</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tbody>
            <?php
				$data_array=sql_select("SELECT b.pi_number, a.color_id, a.fabric_composition, a.fabric_construction, a.dia_width, a.weight, a.uom, a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id) and a.quantity>0 and a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");	 
				
				$color_library = return_library_array('SELECT id,color_name FROM lib_color','id','color_name');
				
				$i = 0;
				foreach($data_array as $row) 
				{
					$i++;
					if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
					else $bgcolor = "#FFFFFF";
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>"  height="25">
						<td><?php echo $row[csf('pi_number')]; ?></td>
						<td><?php echo $row[csf('fabric_construction')]; ?></td>
						<td><?php echo $row[csf('fabric_composition')]; ?></td>
						<td><?php echo $color_library[$row[csf('color_id')]]; ?></td>
						<td><?php echo $row[csf('weight')]; ?></td>
						<td><?php echo $row[csf('dia_width')]; ?></td>
						<td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
						<td align="right"><?php echo number_format($row[csf('quantity')],2); $total_quantity += $row[csf('quantity')];?></td>
						<td align="right"><?php echo number_format($row[csf('rate')],4); ?></td>
						<td align="right"><?php echo number_format($row[csf('amount')],2); $total_ammount += $row[csf('amount')]; ?></td>
					</tr>
				<?php 
				} 
				?>
				<tr class="tbl_bottom" height="25">
					<td colspan = "7" align="right">Sum</td> 
					<td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
					<td>&nbsp;</td>
					<td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
				</tr>
			</tbody>
			<?php
			break;
			case 8:			//Spare Parts
			case 9:			//Machinaries
			case 10:		//Other Capital Items
			case 11:		//Stationaries
			?>
            <thead>
                <tr>
                    <th>PI No</th>
                    <th>Item Group</th>
                    <th>Item Description</th>
                    <th>UOM</th>
                    <th>Quantity</th>
                    <th>Rate</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
				<?php
                 $data_array=sql_select("SELECT b.pi_number, a.item_group, a.item_description, a.uom, a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id)  AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");	 
                 
                $item_group_library = return_library_array('SELECT id, item_name FROM lib_item_group','id','item_name'); 
                $i = 0;
                foreach($data_array as $row) 
                {
					$i++;
					if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
					else $bgcolor = "#FFFFFF";
                ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" height="25" >
                        <td><?php echo $row[csf('pi_number')]; ?></td>
                        <td><?php echo $item_group_library[$row[csf('item_group')]]; ?></td>
                        <td><?php echo $row[csf('item_description')]; ?></td>
                        <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                        <td  align="right"><?php echo number_format($row[csf('quantity')],2); $total_quantity += $row[csf('quantity')];?></td>
                        <td  align="right"><?php echo number_format($row[csf('rate')],4); ?></td>
                        <td  align="right"><?php echo number_format($row[csf('amount')],2); $total_ammount += $row[csf('amount')]; ?></td>
                    </tr>
                <?php 
                } 
                ?>
                <tr class="tbl_bottom" height="25">
                    <td  colspan = "5" align="right">Sum</td> 
                    <td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
                    <td></td>
                    <td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
                </tr>
             </tbody>
			<?php
			break;
			case 4:			//Accessories  
				?>
				<thead>
					<tr>
                    	<th>PI No</th>
						<th>Item Group</th>
						<th>Item Description</th>
                        <th>Gmts Color</th>
                        <th>Gmts Size</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Rate</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<?php
					 $data_array=sql_select("SELECT b.pi_number, a.item_group, a.item_description, a.color_id, a.size_id, a.uom, a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id)  AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");	 
					
					$item_group_library = return_library_array('SELECT id, item_name FROM lib_item_group','id','item_name'); 
					$i = 0;
                    foreach($data_array as $row) 
					{
						$i++;
						if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
						else $bgcolor = "#FFFFFF";
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>"  height="25" >
                            <td><?php echo $row[csf('pi_number')]; ?></td>
                            <td><?php echo $item_group_library[$row[csf('item_group')]]; ?></td>
                            <td><?php echo $row[csf('item_description')]; ?></td>
                            <td><?php echo $color_library[$row[csf('color_id')]]; ?></td>
                            <td><?php echo $size_library[$row[csf('size_id')]]; ?></td>
                            <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                            <td align="right"><?php echo number_format($row[csf('quantity')],2); $total_quantity += $row[csf('quantity')];?></td>
                            <td align="right"><?php echo number_format($row[csf('rate')],4); ?></td>
                            <td align="right"><?php echo number_format($row[csf('amount')],2); $total_ammount += $row[csf('amount')]; ?></td>
                        </tr>
					<?php 
				    } 
				    ?>
                    <tr class="tbl_bottom" height="25">
                        <td  colspan = "6" align="right">Sum</td> 
                        <td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
                        <td></td>
                        <td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
					</tr>
             </tbody>	
			<?php 
				break;
			case 5:			//Chemicals
			case 6:			//Dyes
			case 7:			//Auxilary Chemicals
			case 15:		
			case 16:		
			case 17:			
			case 18:			
			case 19:			
			case 20:		
			case 21:		
			case 22:		
			case 23:		
				?>
				<thead>
					<tr>
                    	<th>PI No</th>
						<th>Item Group</th>
						<th>Item Description</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Rate</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$data_array=sql_select("SELECT b.pi_number ,a.item_group, a.item_description, a.uom, a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id) AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");	

					$item_group_library = return_library_array('SELECT id, item_name FROM lib_item_group','id','item_name'); 
					 
					$i = 0;
                    foreach($data_array as $row) 
					{
						$i++;
					 	if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
						else $bgcolor = "#FFFFFF";
					?>
                        <tr bgcolor="<?php echo $bgcolor; ?>">
                            <td><?php echo $row[csf('pi_number')]; ?></td>
                            <td><?php echo $item_group_library[$row[csf('item_group')]]; ?>&nbsp;</td>
                            <td><?php echo $row[csf('item_description')]; ?>&nbsp;</td>
                            <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                            <td align="right"><?php echo number_format($row[csf('quantity')],2);  $total_quantity += $row[csf('quantity')];?></td>
                            <td align="right"><?php echo $row[csf('rate')]; ?></td>
                            <td align="right"><?php echo number_format($row[csf('amount')],2);  $total_ammount += $row[csf('amount')]; ?></td>
                        </tr>
					<?php 
					} 
					?>
                    <tr class="tbl_bottom" height="25"> 
                        <td  colspan = "4" align="right">Sum</td> 
                        <td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
                        <td></td>
                        <td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
                    </tr>
               </tbody>
                    
				
				<?php  
				break;
			case 12:		//Services
				?>
				<thead>
					<tr>
                    	<th>PI No</th>
                    	<th>Service Type</th>
						<th>Item Description</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Rate</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
                <?php
				 $data_array=sql_select("SELECT b.pi_number,a.id,a.pi_id,a.item_description,a.uom,a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount, a.service_type,a.status_active FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id) AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");	 
				 
				$i = 0;
				foreach($data_array as $row) 
				{
					$i++;
				 	if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
					else $bgcolor = "#FFFFFF";
				?>
                    <tr bgcolor="<?php echo $bgcolor; ?>"  height="25" >
                        <td><?php echo $row[csf('pi_number')]; ?></td>
                        <td><?php echo $service_type[$row[csf('service_type')]]; ?></td>
                        <td><?php echo ($row[csf('item_description')]); ?></td>
                        <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                        <td align="right"><?php echo number_format($row[csf('quantity')],2);  $total_quantity += $row[csf('quantity')];?></td>
                        <td align="right"><?php echo $row[csf('rate')]; ?></td>
                        <td align="right"><?php echo number_format($row[csf('amount')],2);  $total_ammount += $row[csf('amount')];  ?></td>
                    </tr>
				<?php 
				} 
				?>
				
				<tr class="tbl_bottom">
					<td  colspan = "4" align="right">Sum</td> 
					<td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
					<td>&nbsp;</td>
					<td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
				</tr>
			</tbody>
			<?php  
			break;
			case 24:		
				?>
				<thead>
					<tr>
                    	<th>PI No</th>
                    	<th>Lot No</th>
                        <th>Count</th>
						<th>Yarn Description</th>
                        <th>Color</th>
                        <th>Color Range</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Rate</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
                <?php
				 $data_array=sql_select("SELECT b.pi_number,a.id,a.pi_id,a.item_description,a.uom,a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount, a.service_type,a.status_active, a.lot_no,a.yarn_color,a.color_range, a.count_name FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id) AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");	 
				 $color_library = return_library_array('SELECT id,color_name FROM lib_color','id','color_name');
				 $count_library = return_library_array('SELECT id,yarn_count FROM lib_yarn_count','id','yarn_count');
				 $i = 0;
				 foreach($data_array as $row) 
				 {
					$i++;
				 	if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
					else $bgcolor = "#FFFFFF";
				 ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>">
                        <td><?php echo $row[csf('pi_number')]; ?></td>
                        <td><?php echo $row[csf('lot_no')]; ?>&nbsp;</td>
                        <td><?php echo $count_library[$row[csf('count_name')]]; ?>&nbsp;</td>
                        <td><?php echo $row[csf('item_description')]; ?></td>
                        <td><?php echo $color_library[$row[csf('yarn_color')]]; ?>&nbsp;</td>
                        <td><?php echo $color_range[$row[csf('color_range')]]; ?>&nbsp;</td>
                        <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                        <td align="right"><?php echo number_format($row[csf('quantity')],2);  $total_quantity += $row[csf('quantity')];?></td>
                        <td align="right"><?php echo $row[csf('rate')]; ?></td>
                        <td align="right"><?php echo number_format($row[csf('amount')],2);  $total_ammount += $row[csf('amount')];  ?></td>
                    </tr>
				 <?php 
				 } 
				 ?>
				
				<tr class="tbl_bottom">
					<td colspan = "7" align="right">Sum</td> 
					<td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
					<td>&nbsp;</td>
					<td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
				</tr>
			</tbody>
			<?php  
			break;
			case 25:		
				?>
				<thead>
					<tr>
                    	<th>PI No</th>
                    	<th>Gmts Item</th>
                        <th>Embellishment Name</th>
						<th>Embellishment Type</th>
                        <th>Gmts Color</th>
						<th>UOM</th>
						<th>Quantity</th>
						<th>Rate</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
                <?php
				 $data_array=sql_select("SELECT b.pi_number,a.id,a.pi_id,a.item_description,a.uom,a.quantity, a.net_pi_rate as rate, a.net_pi_amount as amount, a.service_type,a.status_active, a.embell_name,a.embell_type,a.color_id, a.gmts_item_id FROM com_pi_item_details a, com_pi_master_details b WHERE b.id = a.pi_id and a.pi_id in($pi_mst_id) AND a.status_active = 1 AND a.is_deleted = 0 ORDER BY a.id ASC");	 
				 $color_library = return_library_array('SELECT id,color_name FROM lib_color','id','color_name');
				 $i = 0;
				 foreach($data_array as $row) 
				 {
					$i++;
				 	if( $i % 2 == 0 ) $bgcolor="#E9F3FF";
					else $bgcolor = "#FFFFFF";
					
					$emb_arr=array();
					if($row[csf('embell_name')]==1) $emb_arr=$emblishment_print_type;
					else if($row[csf('embell_name')]==2) $emb_arr=$emblishment_embroy_type;
					else if($row[csf('embell_name')]==3) $emb_arr=$emblishment_wash_type;
					else if($row[csf('embell_name')]==4) $emb_arr=$emblishment_spwork_type;
					else $emb_arr=$blank_array;
				 ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" >
                        <td><?php echo $row[csf('pi_number')]; ?></td>
                        <td><?php echo $garments_item[$row[csf('gmts_item_id')]]; ?>&nbsp;</td>
                        <td><?php echo $emblishment_name_array[$row[csf('embell_name')]]; ?>&nbsp;</td>
                        <td><?php echo $emb_arr[$row[csf('embell_type')]]; ?>&nbsp;</td>
                        <td><?php echo $color_library[$row[csf('color_id')]]; ?>&nbsp;</td>
                        <td><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
                        <td align="right"><?php echo number_format($row[csf('quantity')],2);  $total_quantity += $row[csf('quantity')];?></td>
                        <td align="right"><?php echo $row[csf('rate')]; ?></td>
                        <td align="right"><?php echo number_format($row[csf('amount')],2);  $total_ammount += $row[csf('amount')];  ?></td>
                    </tr>
				 <?php 
				 } 
				 ?>
				
				<tr class="tbl_bottom">
					<td colspan = "6" align="right">Sum</td> 
					<td><?php echo number_format($total_quantity,2); $total_quantity = 0;?></td>
					<td>&nbsp;</td>
					<td><?php echo number_format($total_ammount,2); $total_ammount = 0;?></td>
				</tr>
			</tbody>
			<?php  
			break;
		} 
		?>
	 </table>
	<?php
	exit();
}

//-------------------------------------------End Pi Details List---------------------------------------------------------------------//


//------------------------------------------Load Drop Down on Change---------------------------------------------//
if ($action=="load_supplier_dropdown")
{
	//echo $data;
	$data = explode('_',$data);
	
	if ($data[1]==0) 
	{
		echo create_drop_down( "cbo_supplier_id", 165, $blank_array,'', 1, '----Select----',0,0,0);
	}
	else if($data[1]==1)
	{
		echo create_drop_down( "cbo_supplier_id",165,"select c.supplier_name,c.id from lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =2 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);		
	}
	else if($data[1]==2 || $data[1]==3 || $data[1]==13 || $data[1]==14)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select c.supplier_name, c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type =9 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	else if($data[1]==4)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select c.supplier_name,c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(4,5) and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);
		
	}
	else if($data[1]==5 || $data[1]==6 || $data[1]==7)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select c.supplier_name,c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type=3 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	else if($data[1]==8)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select c.supplier_name,c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 7 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	else if($data[1]==9 || $data[1]==10)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select c.supplier_name,c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 6 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);
	} 
	else if($data[1]==11)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select c.supplier_name,c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 8 and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	else if($data[1]==12 || $data[1]==24 || $data[1]==25)
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select c.supplier_name,c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type in(20,21,22,23,24,30,31,32,35,36,37,38,39) and c.status_active=1 and c.is_deleted=0 group by c.id, c.supplier_name",'id,supplier_name', 1, '----Select----',0,0,0);
	}
	else
	{
		echo create_drop_down( "cbo_supplier_id", 165,"select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company='$data[0]' and b.party_type = 7 and c.status_active=1 and c.is_deleted=0",'id,supplier_name', 1, '-- Select Supplier --',0,'',0);
	} 
	
	exit(); 
}


if ($action=="set_value_pi_select")
{
	$pi_value = return_field_value("sum(net_pi_amount)","com_pi_item_details","pi_id in($data) and status_active=1 and is_deleted=0");
	$nameArray=sql_select( "select id,pi_number,supplier_id,last_shipment_date,currency_id from com_pi_master_details where id in($data)" );
	 
	foreach ($nameArray as $inf)
	{   
		
		if($inf[csf("currency_id")]==1)
			$txt_pi_value=number_format($pi_value,$dec_place[4],'.','');
		else
			$txt_pi_value=number_format($pi_value,$dec_place[5],'.','');
			
		echo "document.getElementById('cbo_supplier_id').value = '".$inf[csf("supplier_id")]."';\n";    
		echo "document.getElementById('txt_last_shipment_date').value = '".change_date_format($inf[csf("last_shipment_date")])."';\n"; 
		echo "document.getElementById('txt_pi_value').value = '".$txt_pi_value."';\n";
		echo "document.getElementById('txt_lc_value').value = '".$txt_pi_value."';\n";     
		echo "document.getElementById('cbo_pi_currency_id').value = '".$inf[csf("currency_id")]."';\n";  
		echo "document.getElementById('cbo_lc_currency_id').value = '".$inf[csf("currency_id")]."';\n";
		echo "document.getElementById('cbo_supplier_id').disabled=true\n";
		echo "document.getElementById('txt_pi_value').disabled=true\n";
		echo "document.getElementById('cbo_pi_currency_id').disabled=true\n";
		echo "document.getElementById('cbo_lc_currency_id').disabled=true\n";
	}
	exit();
}

if ($action=="pi_popup")
{
	echo load_html_head_contents("PI Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>

    <script>
	
	 var btb_id='<?php echo $btb_id; ?>';
	
	 var selected_id = new Array, selected_name = new Array();
		
		function check_all_data(is_checked)
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) 
		{
			var newColor = 'yellow';
			if ( x.style ) 
			{
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_pi_row_id').value;
			if(old!="")
			{   
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{  
					js_set_value( old[i] ) 
				}
			}
		}

		function js_set_value( str) 
		{
			if(btb_id!="")
			{
				var data=$('#txt_individual_id' + str).val()+"**"+btb_id;
				if(document.getElementById('search' + str).style.backgroundColor=='yellow')
				{
					var pi_no=$('#search' + str).find("td:eq(1)").text();
					var response = return_global_ajax_value( data, 'check_used_or_not', '', 'btb_margin_lc_controller');
					response=response.split("**");
					if(response[0]==1)
					{
						alert("Bellow Invoice Found Against PI- "+pi_no+". So You can't Detach it.\n Invoice No: "+response[1]);
						return false;
					}
				}
			}
			
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			 
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
				
			}
			else
			{
				for( var i = 0; i < selected_id.length; i++ )
				{
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id =''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}
		
		function reset_hide_field(type)
		{
			$('#txt_selected_id').val( '' );
			$('#txt_selected').val( '' );
			if(type==1)
			{
				$('#search_div').html( '' );
			}
		}
    </script>

</head>

<body>
<div align="center" style="width:900px;" >
    <form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
        <fieldset style="width:890px;margin-left:10px">
            <table style="margin-top:10px" width="780" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
                <thead>  
                    <th>Importer</th> 
                    <th>Supplier</th>              	 
                    <th>PI Number</th>
                    <th>Date Range</th>  
                    <th><input type="reset" name="button" class="formbutton" value="Reset" onClick="reset_hide_field(1)" style="width:100px;"></th>
                    <input type="hidden" name="txt_item_category" id="txt_item_category" class="text_boxes" style="width:70px" value="<?php echo $item_category_id; ?>">  
                    <input type="hidden" name="txt_selected_id" id="txt_selected_id" value="" />
                    <input type="hidden" name="txt_selected"  id="txt_selected" value="" />  
                </thead>
                <tr>
                    <td align="center">
                        <?php 
                            echo create_drop_down( "cbo_company_id", 165,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, '----Select----',0,"load_drop_down( 'btb_margin_lc_controller',this.value+'_'+document.getElementById('txt_item_category').value, 'load_supplier_dropdown', 'supplier_td' );",0); 
                        ?>  
                     
                     </td>
                     <td align="center" id="supplier_td">
                      <?php echo create_drop_down( "cbo_supplier_id", 165,$blank_array,'', 1, '----Select----',0,0,0); ?>       
                      
                     </td>
                    <td align="center">
                     <input type="text" name="txt_pi_no" id="txt_pi_no" class="text_boxes" style="width:120px">
                     </td>
                    <td align="center">
                      <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">To
                      <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
                     </td> 
                     <td align="center">
                     <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_pi_no').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_item_category').value+'_'+document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_supplier_id').value+'_'+'<?php echo $txt_hidden_pi_id; ?>', 'create_pi_search_list_view', 'search_div', 'btb_margin_lc_controller', 'setFilterGrid(\'tbl_list_search\',-1)');reset_hide_field(0);set_all();" style="width:100px;" />
                     </td>
                </tr>
                <tr>
                    <td colspan="5" align="center" height="40" valign="middle"><?php echo load_month_buttons(1);  ?></td>
                </tr>
            </table>
            <div style="margin-top:10px" id="search_div"></div>
        </fieldset>   
	</form>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_pi_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!="") $pi_number="%".$data[0]."%"; else $pi_number = '%%';
	
	if($db_type==0)
	{
		if ($data[1]!="" && $data[2]!="") $pi_date = "and pi_date between '".change_date_format($data[1], "yyyy-mm-dd", "-")."' and '".change_date_format($data[2], "yyyy-mm-dd", "-")."'"; else $pi_date ="";
	}
	else if($db_type==2)
	{
		if($data[1]!="" && $data[2]!="") $pi_date ="and pi_date between '".change_date_format($data[1],'','',1)."' and '".change_date_format($data[2],'','',1)."'"; 
		else $pi_date="";
	}
	
	$item_category_id =$data[3];
	if($data[4]!=0) $importer_id =$data[4]; else $importer_id='%%';
	if($data[5]!=0) $supplier_id =$data[5]; else $supplier_id='%%';
	 
	/*if($supplier_id==0)
	{
		echo 'Select Supplier'; 
		echo '<input type="hidden" name="txt_pi_row_id" id="txt_pi_row_id" value=""/>';
		die;
	}*/
	
	$all_pi_id=$data[6];
	$hidden_pi_id=explode(",",$all_pi_id);
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$supplier=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name'); 
	
	if($all_pi_id=="")
	{
		$sql= "select id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id, net_total_amount from com_pi_master_details a where supplier_id like '".$supplier_id."' and importer_id like '".$importer_id."' and item_category_id = $item_category_id and pi_number like '$pi_number' $pi_date and status_active = 1 and is_deleted =0 and id not in(select pi_id from com_btb_lc_pi where status_active=1 and is_deleted=0) group by id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id, net_total_amount order by pi_number";  
	}
	else
	{
		$sql= "select id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id, net_total_amount from com_pi_master_details a where supplier_id like '".$supplier_id."' and importer_id like '".$importer_id."' and item_category_id = $item_category_id and pi_number like '$pi_number' $pi_date and status_active = 1 and is_deleted =0 and id not in(select pi_id from com_btb_lc_pi where status_active=1 and is_deleted=0) or id in($all_pi_id) group by id, pi_number, pi_date, item_category_id, importer_id, supplier_id, last_shipment_date, hs_code, pi_basis_id, net_total_amount order by pi_number";   
	}
	//echo $sql;
	?>
    <div>	
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="880" class="rpt_table">
            <thead>
                <th width="30">SL</th>
                <th width="110">PI No</th>
                <th width="75">PI Date</th>
                <th width="100">Item Category</th>
                <th width="70">Importer</th>
                <th width="130">Supplier</th>
                <th width="75">Last Ship Date</th>
                <th width="70">HS Code</th>
                <th width="100">PI Basis</th>
                <th>PI Value</th>
            </thead>
		</table>
		<div style="width:880px; max-height:280px; overflow-y:scroll">
        	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="862" class="rpt_table" id="tbl_list_search">
                <?php 
                 $i=1; $pi_row_id="";
                 $nameArray=sql_select( $sql );
                 foreach ($nameArray as $selectResult)
                 {
                    if ($i%2==0)  
                        $bgcolor="#E9F3FF";
                    else
                        $bgcolor="#FFFFFF";	

					if(in_array($selectResult[csf('id')],$hidden_pi_id)) 
					{
						if($pi_row_id=="") $pi_row_id=$i; else $pi_row_id.=",".$i;
					}
            ?>
                    <tr height="20" bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
                        <td width="30" align="center"><?php echo "$i"; ?>
                         <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i; ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>	
                         <input type="hidden" name="txt_individual" id="txt_individual<?php echo $i; ?>" value="<?php echo $selectResult[csf('pi_number')]; ?>"/>
                        </td>	
                        <td width="110"><p><?php echo $selectResult[csf('pi_number')];?></p></td>
                        <td width="75"><?php echo change_date_format($selectResult[csf('pi_date')]);?></td> 
                        <td width="100"><p><?php echo $item_category[$selectResult[csf('item_category_id')]]; ?></p></td>
                        <td width="70"><p><?php echo $comp[$selectResult[csf('importer_id')]]; ?></p></td>
                        <td width="130"><p><?php echo $supplier[$selectResult[csf('supplier_id')]]; ?></p></td>
                        <td width="75"><?php echo change_date_format($selectResult[csf('last_shipment_date')]); ?></td>	
                        <td width="70"><p><?php echo $selectResult[csf('hs_code')]; ?></p></td>
                        <td width="100"><p><?php echo $pi_basis[$selectResult[csf('pi_basis_id')]]; ?></p></td>
                        <td align="right"><?php echo number_format($selectResult[csf('net_total_amount')],2,'.',''); ?>&nbsp;</td>
                    </tr>
                <?php
                $i++;
                }
                ?>
               	<input type="hidden" name="txt_pi_row_id" id="txt_pi_row_id" value="<?php echo $pi_row_id; ?>"/>	
            </table>
        </div>
        <table width="860" cellspacing="0" cellpadding="0" border="1" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:45%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data(this.checked)" /> Check / Uncheck All
                        </div>
                        <div style="width:55%; float:left" align="left">
                            <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>	
<?php	 
} 
 
if($action=="check_used_or_not")
{
	$data=explode("**",$data);
	$pi_id=$data[0];
	$btb_id=$data[1];
	
	$sql="select a.invoice_no from com_import_invoice_mst a, com_import_invoice_dtls b where a.id=b.import_invoice_id and b.btb_lc_id=$btb_id and b.pi_id=$pi_id and b.status_active=1 and b.is_deleted=0 group by a.id, a.invoice_no";
	$data_array=sql_select($sql);
	$invoice_no='';
	if(count($data_array)>0)
	{
		foreach($data_array as $row)
		{
			if($invoice_no=="") $invoice_no=$row[csf('invoice_no')]; else $invoice_no.=", ".$row[csf('invoice_no')];
		}
		echo "1**".$invoice_no;
	}
	else
	{
		echo "0**";	
	}
	exit();
}

if($action=="btb_lc_search")
{
	echo load_html_head_contents("BTB L/C Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		function js_set_value(id)
		{
			$('#hidden_btb_id').val(id);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:1000px;">
	<form name="searchscfrm"  id="searchscfrm">
		<fieldset style="width:100%; margin-left:15px">
            <legend>Enter search words</legend>           
            	<table cellpadding="0" cellspacing="0" border="1" rules="all" width="980" class="rpt_table">
                	<thead>
                    	<th>Item Category</th>
                    	<th>Company</th>
                        <th>Supplier</th>
                        <th>L/C Date</th>
                        <th>System Id</th>
                        <th>LC No</th>
                        <th>
                        	<input type="reset" name="reset" id="reset" value="Reset" style="width:80px" class="formbutton" />
                        	<input type="hidden" name="id_field" id="id_field" value="" />
                        </th>
                    </thead>
                    <tr class="general">
                        <td> 
                             <?php echo create_drop_down( "cbo_item_category_id", 140, $item_category,'', 1, '--Select--',0,"load_drop_down( 'btb_margin_lc_controller',document.getElementById('txt_company_id').value+'_'+this.value,'load_supplier_dropdown','supplier_td' );",0); ?>  
                        </td>
                        <td>
                           <?php 
								echo create_drop_down( "txt_company_id",150,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name",'id,company_name', 1, 'Select',0,"load_drop_down( 'btb_margin_lc_controller',this.value+'_'+document.getElementById('cbo_item_category_id').value,'load_supplier_dropdown','supplier_td' );",0); 
							?>  
                         </td>
                         <td align="center" id="supplier_td">
                          <?php echo create_drop_down( "cbo_supplier_id", 165,$blank_array,'', 1, '----Select----',0,0,0); ?>       
                         </td>            
						<td> 
                        	 <input type="text" name="btb_start_date" id="btb_start_date" class="datepicker" style="width:70px;" />To
                             <input type="text" name="btb_end_date" id="btb_end_date" class="datepicker" style="width:70px;" />
                        </td>						
						<td id="search_by_td">
							<input type="text" style="width:90px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                            <input type="hidden" id="hidden_btb_id" />
            			</td>
                        <td >
							<input type="text" style="width:90px" class="text_boxes"  name="txt_lc_no" id="txt_lc_no" />
            			</td>                       
                         <td>
                 		  	<input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_company_id').value+'**'+document.getElementById('cbo_item_category_id').value+'**'+document.getElementById('cbo_supplier_id').value+'**'+document.getElementById('btb_start_date').value+'**'+document.getElementById('btb_end_date').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_lc_no').value, 'create_btb_search_list_view', 'search_div', 'btb_margin_lc_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:80px;" />
                         </td>
					</tr>
               </table>
               <table width="100%" style="margin-top:5px" align="center">
					<tr>
                    	<td colspan="5" id="search_div" align="center"></td>
                    </tr>
                </table> 
            </fieldset>
		</form>
	</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit(); 
}

if($action=="create_btb_search_list_view")
{
	$data=explode('**',$data);
	$company_id = $data[0];
	$item_category_id = $data[1];
	$supplier_id = $data[2];
	$lc_start_date = $data[3];
	$lc_end_date = $data[4];
	$system_id = $data[5];
	$lc_num = $data[6];
		
	if($company_id==0)
	{
		echo 'Select Importer';die;
	}
	
	if ($company_id!=0) $company=$company_id;
	if ($item_category_id==0) $item_category_cond="%%"; else $item_category_cond=$item_category_id;
	if ($supplier_id!=0) $supplier=$supplier_id; else $supplier='%%';
	if ($system_id!='') $system_number=$system_id; else $system_number='%';
	if ($lc_num!='') $lc_number_cond=" and lc_number like '%".$lc_num."'"; else $lc_number_cond='';

	if($lc_start_date!='' && $lc_end_date!='')
	{
		if($db_type==0)
		{
			$date = "and application_date between '".change_date_format($lc_start_date,'yyyy-mm-dd')."' and '".change_date_format($lc_end_date,'yyyy-mm-dd')."'";
		}
		else if($db_type==2)
		{
			$date = "and application_date between '".change_date_format($lc_start_date,'','',1)."' and '".change_date_format($lc_end_date,'','',1)."'";
		}
	}
	else
	{
		$date = "";
	}
	
	if($db_type==0) $year_field="YEAR(insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$sql = "SELECT id, $year_field btb_prefix_number, btb_system_id, lc_number, supplier_id, application_date, last_shipment_date, lc_date, lc_value, item_category_id, importer_id FROM com_btb_lc_master_details WHERE btb_system_id like '%".$system_number."' and importer_id = '".$company."' and supplier_id like '".$supplier."' and item_category_id like '".$item_category_cond."' $date $lc_number_cond and status_active = 1 and is_deleted = 0 order by item_category_id, id";
	//echo $sql;
	
	$supplier_lib=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name'); 
	$arr=array(0=>$item_category,3=>$supplier_lib);
			
	echo create_list_view("list_view", "Item Category,Year,System Id,Supplier,L/C Number,L/C Date,L/C Value,Application Date,Last Ship Date", "110,55,65,150,150,80,100,100,100","980","320",0, $sql , "js_set_value", "id", "",1,"item_category_id,0,0,supplier_id,0,0,0,0,0", $arr , "item_category_id,year,btb_prefix_number,supplier_id,lc_number,lc_date,lc_value,application_date,last_shipment_date","",'','0,0,0,0,0,3,2,3,3') ;
	
	exit();	 
}


if($action=='populate_data_from_btb_lc')
{
	$data_array=sql_select("select id,btb_prefix,btb_prefix_number,btb_system_id,bank_code,lc_year, 	lc_category,lc_serial,supplier_id,importer_id,application_date,lc_date,last_shipment_date,lc_expiry_date,lc_type_id,lc_value,max_lc_value,min_lc_value,currency_id,issuing_bank_id,item_category_id,tenor,tolerance,inco_term_id,inco_term_place,delivery_mode_id,etd_date,insurance_company_name,lca_no,lcaf_no,imp_form_no,psi_company,cover_note_no,cover_note_date,maturity_from_id,margin,origin,bonded_warehouse,item_basis_id,garments_qty,credit_to_be_advised,partial_shipment,transhipment,shipping_mark,doc_presentation_days,port_of_loading,port_of_discharge,remarks,pi_id,pi_value,payterm_id,uom_id,ud_no,ud_date,credit_advice_id,confirming_bank,inserted_by,insert_date,status_active from com_btb_lc_master_details where id='$data'"); 
	foreach ($data_array as $row)
	{  
		echo "document.getElementById('txt_system_id').value 			= '".$row[csf("btb_system_id")]."';\n";
		echo "document.getElementById('txt_bank_code').value 			= '".$row[csf("bank_code")]."';\n";
		echo "document.getElementById('txt_lc_year').value 				= '".$row[csf("lc_year")]."';\n";
		echo "document.getElementById('txt_category').value 			= '".$row[csf("lc_category")]."';\n";
		echo "document.getElementById('txt_lc_serial').value 			= '".$row[csf("lc_serial")]."';\n";
		echo "document.getElementById('update_id').value 				= '".$row[csf("id")]."';\n";
		echo "document.getElementById('cbo_importer_id').value			= '".$row[csf("importer_id")]."';\n";
		echo "$('#cbo_importer_id').attr('disabled','true')".";\n";
		echo "document.getElementById('cbo_item_category_id').value 	= '".$row[csf("item_category_id")]."';\n";
		
		echo "load_drop_down( 'requires/btb_margin_lc_controller', document.getElementById('cbo_importer_id').value+'_'+document.getElementById('cbo_item_category_id').value, 'load_supplier_dropdown', 'supplier_td' );\n";
		
		echo "document.getElementById('cbo_supplier_id').value 			= '".$row[csf("supplier_id")]."';\n";
		echo "document.getElementById('application_date').value 		= '".change_date_format($row[csf("application_date")])."';\n";
		echo "document.getElementById('txt_lc_date').value 				= '".change_date_format($row[csf("lc_date")])."';\n";
		echo "document.getElementById('txt_last_shipment_date').value 	= '".change_date_format($row[csf("last_shipment_date")])."';\n";
		echo "document.getElementById('txt_lc_expiry_date').value 		= '".change_date_format($row[csf("lc_expiry_date")])."';\n";
		echo "document.getElementById('cbo_lc_type_id').value 			= '".$row[csf("lc_type_id")]."';\n";
		echo "document.getElementById('cbo_lc_basis_id').value 			= '".$row[csf("item_basis_id")]."';\n";
		
		echo "active_inactive(".$row[csf("item_basis_id")].");\n";
		
		if($row[csf("etd_date")]=="0000-00-00" || $row[csf("etd_date")]=="") $etd_date=""; else $etd_date=change_date_format($row[csf("etd_date")]);
		if($row[csf("cover_note_date")]=="0000-00-00" || $row[csf("cover_note_date")]=="") $cover_note_date=""; else $cover_note_date=change_date_format($row[csf("cover_note_date")]);
		if($row[csf("ud_date")]=="0000-00-00" || $row[csf("ud_date")]=="") $ud_date=""; else $ud_date=change_date_format($row[csf("ud_date")]);
		
		echo "document.getElementById('txt_lc_value').value 			= '".$row[csf("lc_value")]."';\n";
		echo "document.getElementById('cbo_lc_currency_id').value 		= '".$row[csf("currency_id")]."';\n"; 
		echo "document.getElementById('cbo_issuing_bank').value			= '".$row[csf("issuing_bank_id")]."';\n";
		echo "document.getElementById('cbo_item_category_id').value		= '".$row[csf("item_category_id")]."';\n";
		echo "document.getElementById('txt_tenor').value				= '".$row[csf("tenor")]."';\n";
		echo "document.getElementById('txt_tolerance').value			= '".$row[csf("tolerance")]."';\n";
		echo "document.getElementById('cbo_inco_term_id').value 		= '".$row[csf("inco_term_id")]."';\n";
		echo "document.getElementById('txt_inco_term_place').value 		= '".$row[csf("inco_term_place")]."';\n";
		echo "document.getElementById('cbo_delevery_mode').value 		= '".$row[csf("delivery_mode_id")]."';\n";
		echo "document.getElementById('txt_etd_date').value 			= '".$etd_date."';\n";
		echo "document.getElementById('txt_insurance_company').value 	= '".$row[csf("insurance_company_name")]."';\n";
		echo "document.getElementById('txt_lca_no').value 				= '".$row[csf("lca_no")]."';\n";
		echo "document.getElementById('txt_lcaf_no').value 				= '".$row[csf("lcaf_no")]."';\n";
		echo "document.getElementById('txt_imp_form_no').value 			= '".$row[csf("imp_form_no")]."';\n";
		echo "document.getElementById('txt_psi_company').value 			= '".$row[csf("psi_company")]."';\n";
		echo "document.getElementById('txt_cover_note_no').value 		= '".$row[csf("cover_note_no")]."';\n";
		echo "document.getElementById('txt_cover_note_date').value 		= '".$cover_note_date."';\n";
		echo "document.getElementById('cbo_maturit_from_id').value 		= '".$row[csf("maturity_from_id")]."';\n";
		echo "document.getElementById('txt_margin_deposit').value 		= '".$row[csf("margin")]."';\n";
		echo "document.getElementById('cbo_origin_id').value 			= '".$row[csf("origin")]."';\n";
		echo "document.getElementById('cbo_bond_warehouse_id').value 	= '".$row[csf("bonded_warehouse")]."';\n";
		echo "document.getElementById('txt_gmt_qnty').value 			= '".$row[csf("garments_qty")]."';\n";
		echo "document.getElementById('cbo_credit_advice_id').value 	= '".$row[csf("credit_to_be_advised")]."';\n";
		echo "document.getElementById('cbo_partial_ship_id').value 		= '".$row[csf("partial_shipment")]."';\n";
		echo "document.getElementById('cbo_transhipment_id').value 		= '".$row[csf("transhipment")]."';\n";
		echo "document.getElementById('txt_shiping_mark').value 		= '".$row[csf("shipping_mark")]."';\n";
		echo "document.getElementById('txt_doc_perc_days').value 		= '".$row[csf("doc_presentation_days")]."';\n";
		echo "document.getElementById('txt_port_loading').value 		= '".$row[csf("port_of_loading")]."';\n";
		echo "document.getElementById('txt_port_discharge').value 		= '".$row[csf("port_of_discharge")]."';\n";
		echo "document.getElementById('txt_remarks').value 				= '".$row[csf("remarks")]."';\n";
		echo "document.getElementById('txt_hidden_pi_id').value 		= '".$row[csf("pi_id")]."';\n";
		echo "document.getElementById('txt_pi_value').value 			= '".$row[csf("pi_value")]."';\n";
		echo "document.getElementById('cbo_payterm_id').value 			= '".$row[csf("payterm_id")]."';\n";
		echo "document.getElementById('cbo_gmt_uom_id').value 			= '".$row[csf("uom_id")]."';\n";
		echo "document.getElementById('txt_ud_no').value 				= '".$row[csf("ud_no")]."';\n";
		echo "document.getElementById('txt_ud_date').value 				= '".$ud_date."';\n";
		echo "document.getElementById('cbo_credit_advice_id').value 	= '".$row[csf("credit_advice_id")]."';\n";
		echo "document.getElementById('txt_conf_bank').value 			= '".$row[csf("confirming_bank")]."';\n";
		echo "document.getElementById('cbo_status').value 				= '".$row[csf("status_active")]."';\n";
		
		if($row[csf("pi_id")]!="")
		{
			if($db_type==0)
			{
				$pi_no=return_field_value("group_concat(pi_number)","com_pi_master_details","id in(".$row[csf("pi_id")].") and status_active=1 and is_deleted=0");
			}
			else
			{
				$pi_no=return_field_value("LISTAGG(cast(pi_number as varchar2(4000)), ',') WITHIN GROUP (ORDER BY id) as pi_number","com_pi_master_details","id in(".$row[csf("pi_id")].") and status_active=1 and is_deleted=0","pi_number");
			}
		}
		else
		{
			$pi_no="";
		}

		echo "document.getElementById('txt_pi').value 				= '".$pi_no."';\n";
		
		$lc_amnd=return_field_value("count(id )","com_btb_lc_amendment","btb_id=$data and is_original=0 and status_active=1 and is_deleted=0");
		if($lc_amnd>0)
		{
			echo "disable_enable_fields('txt_last_shipment_date*txt_lc_expiry_date*cbo_delevery_mode*cbo_inco_term_id*txt_inco_term_place*cbo_delevery_mode*txt_port_of_loading*txt_port_of_discharge*cbo_payterm_id*txt_tenor*txt_pi*txt_remarks',1);\n";
		}
		else
		{
			echo "disable_enable_fields('txt_last_shipment_date*txt_lc_expiry_date*cbo_delevery_mode*cbo_inco_term_id*txt_inco_term_place*cbo_delevery_mode*txt_port_of_loading*txt_port_of_discharge*cbo_payterm_id*txt_tenor*txt_pi*txt_remarks',0);\n";
		}
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_btb_mst',1);\n";  
		
		exit();
	}
}


if($action=="lc_popup")
{
	
	echo load_html_head_contents("LC/SC Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	 <script>
	
	 var selected_id = new Array, selected_name = new Array();
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		

		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			var id_type=str[1]+"_"+str[2];
			 
			if( jQuery.inArray( id_type, selected_id ) == -1 ) {
				selected_id.push( id_type );
				selected_name.push( str[3] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == id_type ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id =''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}
		
		function reset_hide_field(type)
		{
			$('#txt_selected_id').val('');
			$('#txt_selected').val('');
			if(type==1)
			{
				$('#search_div').html( '' );
			}
		}
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
	<form name="searchpofrm"  id="searchpofrm">
		<fieldset style="width:820px">
			<table style="margin-top:5px;" width="650" cellspacing="0" cellpadding="0" class="rpt_table">
                <thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th>Search</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" onClick="reset_hide_field(1)" style="width:100px;"></th>                    
                </thead>
                <tr class="general">
                    <td align="center">
                        <?php 
							echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy where buy.status_active=1 and buy.is_deleted=0 $buyer_cond order by buy.buyer_name","id,buyer_name",1, "-- Select--",0,"",0 );
						?>
                    </td>
                    <td align="center">
                        <?php
                        	$arr=array(1=>'L/C',2=>'S/C');
							echo create_drop_down( "cbo_search_by", 150, $arr,"",1, "--- Select ---", '0',"",0 );
						?>
                       
                     </td>
                     <td align="center">
                        <input type="text" name="txt_search_text" id="txt_search_text" class="text_boxes" style="width:150px" />
                        <input type="hidden" name="txt_selected" id="txt_selected" class="text_boxes" readonly />
                        <input type="hidden" name="txt_selected_id" id="txt_selected_id" class="text_boxes" readonly />
                    </td>
                    <td align="center">
                        <input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $company_id; ?>'+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_text').value+'**'+document.getElementById('cbo_buyer_name').value+'**'+'<?php echo $lc_sc; ?>', 'create_lc_search_list_view', 'search_div', 'btb_margin_lc_controller', 'setFilterGrid(\'tbl_list_search\',-1)');reset_hide_field(0)" style="width:100px;" />
                    </td>
            </tr>
        </table>
        <table width="100%" style="margin-top:5px">  
            <tr>
                <td id="search_div"></td>
            </tr>
            <tr>
                <td align="center"><input type="hidden" name="close" onClick="parent.emailwindow.hide();"  class="formbutton" value="Close" /></td>
            </tr>
        </table>
	</fieldset>
	</form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
  exit();
}
 

if($action=="create_lc_search_list_view")
{
	$data=explode('**',$data);
	$company=$data[0];
	$search_by = $data[1]; 
	$search_string =trim($data[2]);
	if($data[3]==0) $buyer_name="%%"; else $buyer_name=$data[3];
	
	$company_details=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$buyer_details=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name'); 
	
	$lc_sc=$data[4];
	$lc_id='';
	$sc_id='';
	if($lc_sc!="")
	{
		$lc_sc_id=explode(",",$lc_sc);
		for($s=0;$s<count($lc_sc_id); $s++)
		{
			$sc_lc_all=explode("__",$lc_sc_id[$s]);
			$id=$sc_lc_all[0];
			$type=$sc_lc_all[1];
			if($type==0)
			{
				if($lc_id=="") $lc_id=$id; else $lc_id.=",".$id; 
			}
			else
			{
				if($sc_id=="") $sc_id=$id; else $sc_id.=",".$id;
			}
		}
	}
	
	if($lc_id=="")
	{
		$lc_id_cond='';
	}
	else
	{
		$lc_id_cond="and a.id not in($lc_id)";
	}
	
	if($sc_id=="")
	{
		$sc_id_cond='';
	}
	else
	{
		$sc_id_cond="and b.id not in($sc_id)";
	}
	
	if($search_by==0)
	{
		$sql="select a.id as sc_lc_id, a.export_lc_no as sc_lc_no, a.lc_date as lc_sc_date, a.beneficiary_name as company_name, a.buyer_name as buyer_name, a.lc_value as sc_lc_value, '0' as type from com_export_lc a where a.beneficiary_name='$company' and a.buyer_name like '$buyer_name' and a.export_lc_no like '%$search_string%' and a.status_active=1 and a.is_deleted=0 $lc_id_cond group by a.id, a.export_lc_no, a.lc_date, a.beneficiary_name, a.buyer_name, a.lc_value
				union all
				select b.id as sc_lc_id, b.contract_no as sc_lc_no, b.contract_date as lc_sc_date, b.beneficiary_name as company_name, b.buyer_name as buyer_name, b.contract_value as sc_lc_value, '1' as type from com_sales_contract b where b.beneficiary_name='$company' and b.buyer_name like '$buyer_name' and b.contract_no like '%$search_string%' and b.status_active=1 and b.is_deleted=0 $sc_id_cond group by b.id, b.contract_no, b.contract_date, b.beneficiary_name, b.buyer_name, b.contract_value";
	}   
	else if($search_by==1)
	{
		$sql="select a.id as sc_lc_id, a.export_lc_no as sc_lc_no, a.lc_date as lc_sc_date, a.beneficiary_name as company_name, a.buyer_name as buyer_name, a.lc_value as sc_lc_value, '0' as type from com_export_lc a where a.beneficiary_name='$company' and a.buyer_name like '$buyer_name' and a.export_lc_no like '%$search_string%' and a.status_active=1 and a.is_deleted=0 $lc_id_cond group by a.id, a.export_lc_no, a.lc_date, a.beneficiary_name, a.buyer_name, a.lc_value";
	} 
	else if($search_by==2)
	{
		$sql="select b.id as sc_lc_id, b.contract_no as sc_lc_no, b.contract_date as lc_sc_date, b.beneficiary_name as company_name, b.buyer_name as buyer_name, b.contract_value as sc_lc_value, '1' as type from com_sales_contract b where b.beneficiary_name='$company' and b.buyer_name like '$buyer_name' and b.contract_no like '%$search_string%' and b.status_active=1 and b.is_deleted=0 $sc_id_cond group by b.id, b.contract_no, b.contract_date, b.beneficiary_name, b.buyer_name, b.contract_value";
	} 
	//echo $sql;
	$lc_sc_type_array=array (0=>"LC",1=>"SC");
	$arr=array (2=>$lc_sc_type_array,3=>$company_details,4=>$buyer_details);
		
	echo create_list_view("tbl_list_search", "LC/SC No,LC/SC Date,Type,Beneficiary,Buyer,LC/SC Value", "150,80,80,150,150,150","820","200",0, $sql , "js_set_value", "sc_lc_id,type,sc_lc_no", "", 1, "0,0,type,company_name,buyer_name,0", $arr , "sc_lc_no,lc_sc_date,type,company_name,buyer_name,sc_lc_value", "","",'0,3,0,0,0,2','',1) ;
	
   exit(); 
} 


if($action=="lc_list_for_attach")
{
	$data=explode("**",$data);
	$explode_data=explode(",",$data[1]);
	$lc_id=""; $sc_id="";
	for($s=0;$s<count($explode_data);$s++)
	{
		$ls_sc=explode("_",$explode_data[$s]);
		$ls_sc_type=$ls_sc[1];
		if($ls_sc_type==0)
		{
			if($lc_id=="") $lc_id=$ls_sc[0]; else $lc_id.=",".$ls_sc[0];
		}
		else
		{
			if($sc_id=="") $sc_id=$ls_sc[0]; else $sc_id.=",".$ls_sc[0];
		}
	}
	
	if($lc_id!='') $lc_query = "SELECT id as sc_lc_id, export_lc_no as lc_sc_no, buyer_name, lc_value as value, '0' as type FROM com_export_lc WHERE id in($lc_id)";
	if($sc_id!='') $sc_query = "SELECT id as sc_lc_id, contract_no as lc_sc_no, buyer_name, contract_value as value, '1' as type FROM com_sales_contract WHERE id in($sc_id)";
	
	if($lc_id!='' && $sc_id!='') $union="union all"; else $union=""; 
	
	$sql="$lc_query $union $sc_query";
	
	$lc_sc_sql=sql_select($sql);
	$count_row=count($lc_sc_sql);
	$table_row=$data[0];
	
	$buyer_library = return_library_array('SELECT id, buyer_name FROM lib_buyer','id','buyer_name'); 
	if($count_row>0)
	{
		foreach($lc_sc_sql as $row_lc)
		{ 	 	
			if($row_lc[csf("type")]=='0')
			{
				$fag = 'L/C';
				$cumulative_value = return_field_value("sum(current_distribution)","com_btb_export_lc_attachment","lc_sc_id='".$row_lc[csf("sc_lc_id")]."' and is_lc_sc=0 and status_active=1 and is_deleted=0");
			}
			else 
			{
				$fag = 'S/C';
				$cumulative_value = return_field_value("sum(current_distribution)","com_btb_export_lc_attachment","lc_sc_id='".$row_lc[csf("sc_lc_id")]."' and is_lc_sc=1 and status_active=1 and is_deleted=0");
			}
			
			$table_row++;
			?>	
			<tr class="general" id="tr_<?php echo $table_row; ?>">
				<td>
				<input type="text" name="txtlcsc_<?php echo $table_row; ?>" id="txtlcsc_<?php echo $table_row; ?>" class="text_boxes" style="width:100px"  onDblClick= "openmypage(3,<?php echo $table_row; ?>)" value="<?php echo $row_lc[csf("lc_sc_no")]; ?>" readonly= "readonly" placeholder="Double For Search" />
				 <input type="hidden" name="txtLcScid_<?php echo $table_row; ?>" id="txtLcScid_<?php echo $table_row; ?>" class="text_boxes" style="width:100px" value="<?php echo $row_lc[csf("sc_lc_id")]; ?>" readonly= "readonly" />
				</td>
				<td><input type="text" name="txtbuyer_<?php echo $table_row; ?>" id="txtbuyer_<?php echo $table_row; ?>" class="text_boxes" style="width:120px;" value="<?php echo $buyer_library[$row_lc[csf("buyer_name")]]; ?>" readonly= "readonly" /></td>
				<td><input type="text" name="txtlcscflag_<?php echo $table_row; ?>" id="txtlcscflag_<?php echo $table_row; ?>" class="text_boxes" style="width:70px;" value="<?php echo $fag; ?>" readonly= "readonly" />
                	<input type="hidden" name="txtlcscflagId_<?php echo $table_row; ?>" id="txtlcscflagId_<?php echo $table_row; ?>" class="text_boxes" style="width:100px" value="<?php echo $row_lc[csf("type")]; ?>" readonly= "readonly" />
                </td>
				<td><input type="text" name="txtlcscvalue_<?php echo $table_row; ?>" id="txtlcscvalue_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px;" value="<?php echo $row_lc[csf("value")]; ?>" readonly/></td>
				<td>
                <input type="text" name="txtcurdistribution[]" id="txtcurdistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="" onKeyUp="distribution_value(0,<?php echo $table_row; ?>)" disabled="disabled" />
                <input type="hidden" name="hidecurdistribution_<?php echo $table_row; ?>" id="hidecurdistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="" readonly />
                </td>
				<td>
				<input type="text" name="txtcumudistribution_<?php echo $table_row; ?>" id="txtcumudistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="<?php echo number_format($cumulative_value,2,'.','');?>"  readonly="readonly" />
				<input type="hidden" name="hiddencumudistribution_<?php echo $table_row; ?>" id="hiddencumudistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="<?php echo $cumulative_value;?>" readonly />
				</td>
				<td><input type="text" name="txtoccupied_<?php echo $table_row; ?>" id="txtoccupied_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:100px" value="" readonly= "readonly"/></td> 
				<td>                             
					<?php 
						 echo create_drop_down( "cbostatus_".$table_row, 100, $row_status,"", 0, "", 1, "" );
					?>
				</td> 
				<td width="65">
					<input type="button" id="increase_<?php echo $table_row; ?>" name="increase_<?php echo $table_row; ?>" style="width:25px" class="formbuttonplasminus" value="+" onClick="fn_add_row_lc(<?php echo $table_row; ?>)" />
					<input type="button" id="decrease_<?php echo $table_row; ?>" name="decrease_<?php echo $table_row; ?>" style="width:25px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $table_row; ?>);" />
                    <input type="hidden" name="txtcaltype_<?php echo $table_row; ?>" id="txtcaltype_<?php echo $table_row; ?>" class="text_boxes" style="width:100px" value="2" readonly= "readonly" />
			   </td>                       
			</tr>
			<?php
		
		}//end foreach
	}
	else
	{
		if($table_row==0)
		{
		?>
            <tr class="general" id="tr_1">
                <td>
                <input type="text" name="txtlcsc_1" id="txtlcsc_1" class="text_boxes" style="width:100px"  onDblClick= "openmypage(3,1)" readonly= "readonly" placeholder="Double For Search" />
                <input type="hidden" name="txtLcScid_1" id="txtLcScid_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
                <input type="hidden" name="txtLcScidType_1" id="txtLcScidType_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
                </td>
                <td><input type="text" name="txtbuyer_1" id="txtbuyer_1" class="text_boxes" style="width:120px;" readonly= "readonly" /></td>
                <td><input type="text" name="txtlcscflag_1" id="txtlcscflag_1" class="text_boxes" style="width:70px;" readonly= "readonly" /></td>
                <td>
                <input type="text" name="txtlcscvalue_1" id="txtlcscvalue_1" class="text_boxes_numeric" style="width:120px;" readonly= "readonly"/>
                <input type="hidden" name="txtlcscflagId_1" id="txtlcscflagId_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
                </td>
                <td>
                <input type="text" name="txtcurdistribution[]" id="txtcurdistribution_1" class="text_boxes_numeric" style="width:120px" onKeyUp="distribution_value(0,1)" disabled="disabled" />
                <input type="hidden" name="hidecurdistribution_1" id="hidecurdistribution_1" class="text_boxes_numeric" style="width:120px" readonly/>
                </td>
                <td>
                <input type="text" name="txtcumudistribution_1" id="txtcumudistribution_1" class="text_boxes_numeric" style="width:120px"   readonly="readonly" />
                <input type="hidden" name="hiddencumudistribution_1" id="hiddencumudistribution_1" class="text_boxes_numeric" style="width:120px" readonly />
                </td>
                <td><input type="text" name="txtoccupied_1" id="txtoccupied_1"  style="width:100px" class="text_boxes_numeric" readonly= "readonly"/></td>  
                     
                <td>                             
                    <?php 
                         echo create_drop_down( "cbostatus_1", 100, $row_status,"", 0, "", 1, "" );
                    ?>
                </td> 
                <td width="65">
                    <input type="button" id="increase_1" name="increase_1" style="width:25px" class="formbuttonplasminus" value="+" onClick="fn_add_row_lc(0)" />
                    <input type="button" id="decrease_1" name="decrease_1" style="width:25px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(0);" />
                    <input type="hidden" name="txtcaltype_1" id="txtcaltype_1" class="text_boxes" style="width:100px" value="0" readonly= "readonly" />
               </td>                       
            </tr>
		<?php
		}
	}
	
	exit();
}	 


if($action=="show_lc_listview")
{ 
	$update_id = $data; 
	$sql="select lc_sc_id, is_lc_sc, current_distribution, status_active from com_btb_export_lc_attachment where import_mst_id=$update_id and is_deleted=0 and status_active=1";
	$lc_sc_sql=sql_select($sql);
	$count_row=count($lc_sc_sql);
	
	$table_row=0;
	$buyer_library = return_library_array('SELECT id, buyer_name FROM lib_buyer','id','buyer_name'); 
	
	if($count_row>0)
	{
		foreach($lc_sc_sql as $row_lc)
		{ 	
			if($row_lc[csf("is_lc_sc")]==0) 
			{	
				$cumulative_value = return_field_value("sum(current_distribution)","com_btb_export_lc_attachment","lc_sc_id='".$row_lc[csf("lc_sc_id")]."' and status_active=1 and is_deleted=0 and is_lc_sc=0");
				$hide_cumulative_value= return_field_value("sum(current_distribution)","com_btb_export_lc_attachment","lc_sc_id='".$row_lc[csf("lc_sc_id")]."' and import_mst_id<>$update_id and status_active=1 and is_deleted=0 and is_lc_sc=0");
				$sc_lc = 'L/C';
				
				$sql_sc_lc="select export_lc_no as lc_sc_no, buyer_name, lc_value as value from com_export_lc where id='".$row_lc[csf("lc_sc_id")]."'";
			}
			else
			{
				$cumulative_value = return_field_value("sum(current_distribution)","com_btb_export_lc_attachment","lc_sc_id='".$row_lc[csf("lc_sc_id")]."' and status_active=1 and is_deleted=0 and is_lc_sc=1");
				$hide_cumulative_value = return_field_value("sum(current_distribution)","com_btb_export_lc_attachment","lc_sc_id='".$row_lc[csf("lc_sc_id")]."' and import_mst_id<>$update_id and status_active=1 and is_deleted=0 and is_lc_sc=1");
				$sc_lc = 'S/C'; 
				
				$sql_sc_lc="select contract_no as lc_sc_no, buyer_name, contract_value as value from com_sales_contract where id='".$row_lc[csf("lc_sc_id")]."'";
			}
			
			$query_res=sql_select($sql_sc_lc);
			
			$table_row++;
			?>	
			<tr class="general" id="tr_<?php echo $table_row; ?>">
				<td>
				<input type="text" name="txtlcsc_<?php echo $table_row; ?>" id="txtlcsc_<?php echo $table_row; ?>" class="text_boxes" style="width:100px"  onDblClick= "openmypage(3,<?php echo $table_row; ?>)" value="<?php echo $query_res[0][csf("lc_sc_no")]; ?>" readonly= "readonly" placeholder="Double For Search" />
				 <input type="hidden" name="txtLcScid_<?php echo $table_row; ?>" id="txtLcScid_<?php echo $table_row; ?>" class="text_boxes" style="width:100px" value="<?php echo $row_lc[csf("lc_sc_id")]; ?>" readonly= "readonly" />
				</td>
				<td><input type="text" name="txtbuyer_<?php echo $table_row; ?>" id="txtbuyer_<?php echo $table_row; ?>" class="text_boxes" style="width:120px;" value="<?php echo $buyer_library[$query_res[0][csf("buyer_name")]]; ?>" readonly= "readonly" /></td>
				<td><input type="text" name="txtlcscflag_<?php echo $table_row; ?>" id="txtlcscflag_<?php echo $table_row; ?>" class="text_boxes" style="width:70px;" value="<?php echo $sc_lc; ?>" readonly= "readonly" />
					<input type="hidden" name="txtlcscflagId_<?php echo $table_row; ?>" id="txtlcscflagId_<?php echo $table_row; ?>" class="text_boxes" style="width:100px" value="<?php echo $row_lc[csf("is_lc_sc")]; ?>" readonly= "readonly" />
				</td>
				<td><input type="text" name="txtlcscvalue_<?php echo $table_row; ?>" id="txtlcscvalue_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px;" value="<?php echo $query_res[0][csf("value")]; ?>" readonly= "readonly"/></td>
				<td>
                <input type="text" name="txtcurdistribution[]" id="txtcurdistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="<?php echo $row_lc[csf("current_distribution")]; ?>" disabled="disabled" onKeyUp="distribution_value(0,<?php echo $table_row; ?>)"/>
                <input type="hidden" name="hidecurdistribution_<?php echo $table_row; ?>" id="hidecurdistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="<?php echo $row_lc[csf("current_distribution")]; ?>" readonly/>
                </td>
				<td>
				<input type="text" name="txtcumudistribution_<?php echo $table_row; ?>" id="txtcumudistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="<?php echo number_format($cumulative_value,2,'.','');?>" readonly/>
				<input type="hidden" name="hiddencumudistribution_<?php echo $table_row; ?>" id="hiddencumudistribution_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:120px" value="<?php echo $hide_cumulative_value;?>" />
				</td>
				<td>
                <input type="text" name="txtoccupied_<?php echo $table_row; ?>" id="txtoccupied_<?php echo $table_row; ?>" class="text_boxes_numeric" style="width:100px" value="" readonly= "readonly"/>
                </td> 
				<td>                             
					<?php 
						 echo create_drop_down( "cbostatus_".$table_row, 100, $row_status,"", 0, "", $row_lc[csf("status_active")], "" );
					?>
				</td> 
				<td width="65">
					<input type="button" id="increase_<?php echo $table_row; ?>" name="increase_<?php echo $table_row; ?>" style="width:25px" class="formbuttonplasminus" value="+" onClick="fn_add_row_lc(<?php echo $table_row; ?>)" />
					<input type="button" id="decrease_<?php echo $table_row; ?>" name="decrease_<?php echo $table_row; ?>" style="width:25px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(<?php echo $table_row; ?>);" />
                    <input type="hidden" name="txtcaltype_<?php echo $table_row; ?>" id="txtcaltype_<?php echo $table_row; ?>" class="text_boxes" style="width:100px" value="1" readonly= "readonly" />
			   </td>                       
			</tr>
			<?php
		
		}//end foreach
	}
	else
	{
	?>
		<tr class="general" id="tr_1">
			<td>
			<input type="text" name="txtlcsc_1" id="txtlcsc_1" class="text_boxes" style="width:100px"  onDblClick= "openmypage(3,1)" readonly= "readonly" placeholder="Double For Search" />
			<input type="hidden" name="txtLcScid_1" id="txtLcScid_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
			<input type="hidden" name="txtLcScidType_1" id="txtLcScidType_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
			</td>
			<td><input type="text" name="txtbuyer_1" id="txtbuyer_1" class="text_boxes" style="width:120px;" readonly= "readonly" /></td>
			<td>
            <input type="text" name="txtlcscflag_1" id="txtlcscflag_1" class="text_boxes" style="width:70px;" readonly= "readonly" />
            <input type="hidden" name="txtlcscflagId_1" id="txtlcscflagId_1" class="text_boxes" style="width:100px" value="" readonly= "readonly" />
            </td>
			<td><input type="text" name="txtlcscvalue_1" id="txtlcscvalue_1" class="text_boxes_numeric" style="width:120px;" readonly= "readonly"/></td>
			<td>
            <input type="text" name="txtcurdistribution[]" id="txtcurdistribution_1" class="text_boxes_numeric" style="width:120px" disabled="disabled" onKeyUp="distribution_value(0,1)"/>
            <input type="hidden" name="hidecurdistribution_1" id="hidecurdistribution_1" class="text_boxes_numeric" style="width:120px" value="" readonly/>
            </td>
			<td>
			<input type="text" name="txtcumudistribution_1" id="txtcumudistribution_1" class="text_boxes_numeric" style="width:120px" readonly/>
			<input type="hidden" name="hiddencumudistribution_1" id="hiddencumudistribution_1" class="text_boxes_numeric" style="width:120px" readonly/>
			</td>
			<td><input type="text" name="txtoccupied_1" id="txtoccupied_1"  style="width:100px" class="text_boxes_numeric" readonly/></td>  
				 
			<td>                             
				<?php 
					 echo create_drop_down( "cbostatus_1", 100, $row_status,"", 0, "", 1, "" );
				?>
			</td> 
			<td width="65">
				<input type="button" id="increase_1" name="increase_1" style="width:25px" class="formbuttonplasminus" value="+" onClick="fn_add_row_lc()" />
				<input type="button" id="decrease_1" name="decrease_1" style="width:25px" class="formbuttonplasminus" value="-" onClick="fn_deleteRow(1);" />
                <input type="hidden" name="txtcaltype_1" id="txtcaltype_1" class="text_boxes" style="width:100px" value="0" readonly= "readonly" />
		   </td>                       
		</tr>
	<?php
	}
	
	exit();
}


if($action=="btb_import_lc_letter")
{
	//echo load_html_head_contents("BTB Import Lc Letter","../../", 1, 1, $unicode,'','');
	$sql_bank_info=sql_select("select id, contact_person, bank_name, branch_name, address from lib_bank ");
	foreach($sql_bank_info as $row)
	{
		$bank_dtls_arr[$row[csf("id")]]["contact_person"]=$row[csf("contact_person")];
		$bank_dtls_arr[$row[csf("id")]]["bank_name"]=$row[csf("bank_name")];
		$bank_dtls_arr[$row[csf("id")]]["branch_name"]=$row[csf("branch_name")];
		$bank_dtls_arr[$row[csf("id")]]["address"]=$row[csf("address")];
	}
	$sql_lc=sql_select("SELECT id,export_lc_no,tenor FROM com_export_lc ");
	foreach($sql_lc as $row)
	{
		$export_lc_no_arr[$row[csf("id")]]["export_lc_no"]=$row[csf("export_lc_no")];
		$export_lc_no_arr[$row[csf("id")]]["tenor"]=$row[csf("tenor")];
	}
	$sql_sc=sql_select("SELECT id,contract_no,tenor FROM com_sales_contract ");
	foreach($sql_sc as $row)
	{
		$export_sc_no_arr[$row[csf("id")]]["contract_no"]=$row[csf("contract_no")];
		$export_sc_no_arr[$row[csf("id")]]["tenor"]=$row[csf("tenor")];
	}
	 
	//echo $data."jahid";die;
	if($db_type==2)
	{
		$sql_com="select a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.origin, a.issuing_bank_id, a.currency_id, a.supplier_id, a.lc_value, a.margin, listagg(cast(b.is_lc_sc || '__' || b.lc_sc_id as varchar(4000)),',') within group (order by b.is_lc_sc,b.lc_sc_id) as lc_sc  from com_btb_lc_master_details a, com_btb_export_lc_attachment b where a.id=b.import_mst_id and  a.id=$data and a.is_deleted = 0 AND a.status_active = 1 group by a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.origin, a.issuing_bank_id, a.currency_id, a.supplier_id, a.lc_value, a.margin";
	}
	elseif($db_type==0)
	{
		$sql_com="select a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.origin, a.issuing_bank_id, a.currency_id, a.supplier_id, a.lc_value, a.margin, group_concat(concat(b.is_lc_sc, '__', b.lc_sc_id)) as lc_sc  from com_btb_lc_master_details a, com_btb_export_lc_attachment b where a.id=b.import_mst_id and  a.id=$data and a.is_deleted = 0 AND a.status_active = 1 group by a.btb_system_id, a.application_date, a.importer_id, a.item_category_id, a.supplier_id, a.origin, a.issuing_bank_id, a.currency_id, a.lc_value, a.margin";
	}
	
	$result=sql_select($sql_com);
	
	$company_name = return_field_value("company_name","lib_company","id=".$result[0][csf("importer_id")],"company_name");
	$supplier_name = return_field_value("supplier_name","lib_supplier","id=".$result[0][csf("supplier_id")],"supplier_name");
	$country_name = return_field_value("country_name"," lib_country","id=".$result[0][csf("origin")],"country_name");
	
	//echo $sql_com;
	
	?>
    
    <table width="700" cellpadding="0" align="left" cellspacing="0" border="0">
        <tr>
        	<td colspan="3" height="110"></td>
        </tr>
        <tr>
            <td width="25"></td>
            <td width="650" align="left">Ref : <?php echo $result[0][csf("btb_system_id")]; ?></td>
            <td width="25" ></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">Dated : <?php echo change_date_format($result[0][csf("application_date")]); ?> </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="20"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            <?php
				echo $bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["contact_person"]."<br>".$bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["bank_name"]."<br>".$bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["branch_name"]."<br>".$bank_dtls_arr[$result[0][csf("issuing_bank_id")]]["address"];
			?>
           
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="30"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            Subject: Application for opening letter of credit under back to back facility for <?php echo $currency[$result[0][csf("currency_id")]]."&nbsp;".number_format($result[0][csf("lc_value")],2,'.','')." @ ".number_format($result[0][csf("margin")]); ?>% for importing <?php echo $item_category[$result[0][csf("item_category_id")]];?> from <?php echo $country_name; ?> against Export LC/Sales Contract NO-
			<?php 
			$lc_sc_ref=explode(",",$result[0][csf("lc_sc")]);  
			$lc_sc_no="";
			$tenor_first="";
			foreach($lc_sc_ref as $row)
			{
				$lc_sc_arr=explode("__",$row);
				if($lc_sc_arr[0]==0)
				{
					$lc_sc_no.=$export_lc_no_arr[$lc_sc_arr[1]]["export_lc_no"].", ";
					if($tenor_first=="") $tenor_first=$export_lc_no_arr[$lc_sc_arr[1]]["tenor"];
				}
				else
				{
					$lc_sc_no.=$export_sc_no_arr[$lc_sc_arr[1]]["contract_no"].", ";
					if($tenor_first=="") $tenor_first=$export_sc_no_arr[$lc_sc_arr[1]]["tenor"];
				}
			}
			$lc_sc_no=chop($lc_sc_no," , ");
			echo $lc_sc_no;
			?> .
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="20"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left"> Dear Sir, </td>
            <td width="25" ></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            Please issue a letter of credit as mentioned in the subject line by the amount of <?php echo $currency[$result[0][csf("currency_id")]]."&nbsp;".number_format($result[0][csf("lc_value")],2,'.','')." @ ".number_format($result[0][csf("margin")]); ?>% favoring to <?php echo $supplier_name;?> for <?php echo $item_category[$result[0][csf("item_category_id")]];?>. We have provided necessary documents/papers herewith as required.
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            We are hereby giving undertaking that we shall make payment all the liabilities of this LC if we fail to export the goods within <?php echo $tenor_first; ?> days against above  Export LC.
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
    
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            You are authorized to debit our CD account for agreed margin and charges.
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            We appreciate your prompt action on the matter.
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="15"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
            Thanking You,
            </td>
            <td width="25" ></td>
        </tr>
        <tr>
        	<td colspan="3" height="80"></td>
        </tr>
        <tr>
            <td width="25" ></td>
            <td width="650" align="left">
			<?php 
			
			echo $company_name;
			 
			?></td>
            <td width="25" ></td>
        </tr>
       <tr>
        	<td colspan="3" height="50"></td>
       </tr>
    </table>
    <?php
	exit();

}

?>


 