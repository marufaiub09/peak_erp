<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Order wise Production Report.
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	1-04-2013
Updated by 		: 		
Update date		: 	Maruf		   
QC Performed BY	:	08-12-2015	
QC Date			:	
Comments		:
*/
error_reporting('0');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Wise Production Report", "../../../", 1, 1,$unicode,1,1);

?>	

<script>

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../../logout.php";  
var permission = '<?php echo $permission; ?>';

var tableFilters = 
	{
		//col_0: "none",col_5: "none",col_5: "none",col_21: "none",col_29: "none",
		col_operation: {
			id: ["total_order_qnty","value_total_order_value","total_ship_qnty","value_total_ship_value","total_balance_ship_qnty"],
			col: [9,10,33,34,35],
			operation: ["sum","sum","sum","sum","sum","sum","sum","sum"],
			write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
		} 
 	} 	 

			
function fn_report_generated()
{
	if (form_validation('cbo_company_name','Comapny Name')==false)//*txt_date_from*txt_date_to----*From Date*To Date
	{
		return;
	}
	else
	{
		var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_buyer_name*txt_date_from*txt_date_to*txt_job_no*txt_style_ref*txt_order_no*cbo_search_by*cbo_year*cbo_team_name*cbo_team_member',"../../../");
		freeze_window(3);
		http.open("POST","requires/shipment_date_wise_wp_report_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fn_report_generated_reponse;
	}
}
	

function fn_report_generated_reponse()
{
 	if(http.readyState == 4) 
	{
  		var reponse=trim(http.responseText).split("**"); 
		$('#report_container2').html(reponse[0]);
		document.getElementById('report_container').innerHTML=report_convert_button('../../../'); 
		append_report_checkbox('table_header_1',1);
		setFilterGrid("table_body",-1,tableFilters);
		
 		show_msg('3');
		release_freezing();
 	}
	
}
	
function show_progress_report_details(action,job_number,width)
{ 
	 emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/shipment_date_wise_wp_report_controller.php?action='+action+'&job_number='+job_number, 'Work Progress Report Details', 'width='+width+',height=400px,center=1,resize=0,scrolling=0','../../');
	
} 

function show_trims_rec(action,po_number,po_id,width)
{ 
//alert (po_id);
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/shipment_date_wise_wp_report_controller.php?action='+action+'&po_number='+po_number+'&po_id='+po_id, 'Trims Receive Details', 'width='+width+',height=400px,center=1,resize=0,scrolling=0','../../');
} 

function progress_comment_popup(job_no,po_id,template_id,tna_process_type)
{
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/shipment_date_wise_wp_report_controller.php?job_no='+job_no+'&po_id='+po_id+'&template_id='+template_id+'&tna_process_type='+tna_process_type+'&action=update_tna_progress_comment'+'&permission='+permission, "TNA Progress Comment", 'width=1030px,height=390px,center=1,resize=1,scrolling=0','../../');
}


</script>
</head>
<body onLoad="set_hotkey();">
<form id="dateWiseProductionReport_1">
    <div style="width:100%;" align="center">    
        <?php echo load_freeze_divs ("../../../",'');  ?>
         <h3 style="width:1100px; margin-top:20px;" align="left" id="accordion_h1" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3> 
         <div id="content_search_panel" >      
         <fieldset style="width:1100px;">
             <table class="rpt_table" width="100%" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
               <thead>                    
                        <th class="must_entry_caption">Company Name</th>
                        <th>Buyer Name</th>
                        <th>Team</th>
                        <th>Team Member</th>
                        <th>Type</th>
                        <th>Year</th>                     
                        <th>Job No</th>
                        <th>Style Ref.</th>
                        <th>Order No</th>
                        <th>Shipment Date</th>
                        <th><input type="reset" id="reset_btn" class="formbutton" style="width:70px" value="Reset" /></th>
                 </thead>
                <tbody>
                    <tr class="general">
                        <td align="center"> 
                            <?php
                                echo create_drop_down( "cbo_company_name", 110, "select comp.id, comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/shipment_date_wise_wp_report_controller',this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                            ?>
                        </td>
                        <td id="buyer_td"  align="center">
                            <?php 
                                echo create_drop_down( "cbo_buyer_name", 110, $blank_array,"", 1, "-- Select Buyer --", $selected, "",1,"" );
                            ?>
                        </td>
                        <td>
                        	<?php
                        		echo create_drop_down( "cbo_team_name", 110, "select id,team_name from lib_marketing_team where status_active=1 and is_deleted=0 order by team_name","id,team_name", 1, "-- Select Team --", $selected, " load_drop_down( 'requires/shipment_date_wise_wp_report_controller', this.value, 'load_drop_down_team_member', 'team_td' )" );
                        	?>
                        </td>
                        <td id="team_td">
                             <?php 
                                echo create_drop_down( "cbo_team_member", 120, $blank_array,"", 1, "- Team Member- ", $selected, "" );
                             ?>	
                        </td>
                        <td align="center">
                            <?php 
                                $search_by_arr = array(1=>"Order Wise",2=>"Style Wise");
                                echo create_drop_down( "cbo_search_by", 90, $search_by_arr,"",0, "", "",'',0 );//search_by(this.value)
                             ?>
                        </td>
                        <td align="center">
                            <?php
                                $selected_year=date("Y");
                                echo create_drop_down( "cbo_year", 60, $year,"", 1, "--Select Year--", $selected_year, "",0 );
                            ?>
                        </td>
                        <td align="center">
                            <input name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:60px" placeholder="Job No" >
                        </td>
                        <td align="center">
                            <input name="txt_style_ref" id="txt_style_ref" class="text_boxes" style="width:70px" placeholder="Styel Ref" >
                        </td>
                        <td align="center">
                            <input name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:70px" placeholder="Order No" >
                        </td>
                        <td align="center">
                            <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px" placeholder="From Date">&nbsp;
                            <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px"  placeholder="To Date">
                        </td>
                        <td>
                            <input type="button" id="show_button" class="formbutton" style="width:70px" value="Show" onClick="fn_report_generated()" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <table>
            	<tr>
                	<td>
 						<?php echo load_month_buttons(1); ?>
                   	</td>
              </tr>
           </table> 
            <br />
        </fieldset>
    </div>
    </div>
        
    <div id="report_container" align="center"></div>
    <div id="report_container2" align="left"></div>
 </form>    
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
