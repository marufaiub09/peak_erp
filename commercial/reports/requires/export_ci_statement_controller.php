<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id = $_SESSION['logic_erp']["user_id"];


// $ref_field=return_field_value("a.style_ref_no","wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c","a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.mst_id=".$row_result[csf('id')],"style_ref_no");
if($db_type==0)
{
	$style_ref_arr=return_library_array("select c.mst_id,group_concat(distinct a.style_ref_no) as style_ref_no from  wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.current_invoice_qnty>0  group by  c.mst_id","mst_id","style_ref_no");
}
else
{
	//$style_ref_arr=return_library_array("select c.mst_id,LISTAGG(CAST(a.style_ref_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.style_ref_no) as style_ref_no from  wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id  group by  c.mst_id","mst_id","style_ref_no");
	$sql=sql_select("select c.mst_id,LISTAGG(CAST(a.style_ref_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.style_ref_no) as style_ref_no from  wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.current_invoice_qnty>0  group by  c.mst_id");
	foreach($sql as $row)
	{
		$style=implode(",",array_unique(explode(",",$row[csf('style_ref_no')])));
		$style_ref_arr[$row[csf('mst_id')]]=$style;
	}
}
//var_dump($style_ref_arr);die;
//$curier_receipt_date=return_field_value("a.courier_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf("id")]."","courier_date");


if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}
//Company Details

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_based_on=str_replace("'","",$cbo_based_on);
	//echo $RptType;die;
	//echo $cbo_based_on;die;
	if($RptType==1)
	{
		$user_arr = return_library_array("select id,user_name from user_passwd ","id","user_name");
		$bank_arr=return_library_array("select id,bank_name from   lib_bank","id","bank_name");
		$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
		$location_arr=return_library_array("select id,location_name from  lib_location","id","location_name");
		$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
		$country_arr=return_library_array("select id,country_name from  lib_country","id","country_name");
		$currier_date_arr=return_library_array("select b.invoice_id,a.courier_date from  com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id ","invoice_id","courier_date");
		
		$curier_submit_date_arr=return_library_array(" select b.invoice_id,a.submit_date from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","submit_date");
		$bnk_to_bnk_cour_no_arr=return_library_array(" select b.invoice_id,a.bnk_to_bnk_cour_no from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","bnk_to_bnk_cour_no");
		$bank_ref_no_arr=return_library_array(" select b.invoice_id,a.bank_ref_no from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","bank_ref_no");
		$bank_date_no_arr=return_library_array(" select b.invoice_id,a.bank_ref_date from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","bank_ref_date");
		$possible_rlz_date_arr=return_library_array(" select b.invoice_id,a.possible_reali_date from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","possible_reali_date");
		$rlz_date_arr=return_library_array(" select a.invoice_id,b.received_date from com_export_doc_submission_invo a, com_export_proceed_realization b where a.doc_submission_mst_id=b.invoice_bill_id","invoice_id","received_date");
		?>
		<div style="width:4700px">
            <table width="3500" cellpadding="0" cellspacing="0" id="caption">
            <tr>
            <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:18px">Company Name:<?php echo " ". $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
            </tr> 
            <tr>  
            <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:18px"><?php echo $report_title; ?>(
            <?php
			if($cbo_based_on!=0)
			{
			?>
            <strong style="font-size:18px">Based On:<?php $based_on_arr=array(1=>"Invoice Date",2=>"Exfactory Date",3=>"Actual Date"); echo " ". $based_on_arr[$cbo_based_on]; ?></strong>
            
            <?php
			}
			?>)</strong></td>
            </tr>
            </table>
    		<br />
            <table width="4680" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_header_1" align="left">
                <thead>
                    <tr>
                        <th width="50">Sl</th>
                        <th width="100">Company Name</th>
                        <th width="100">Location</th>
                        <th width="100">Country Name</th>
                        <th width="100">Invoice No.</th>
                        <th width="80">Ship Mode</th>
                        <th width="70">Invoice Date</th>
                        <th width="70">Insert Date</th>
                        <th width="70">SC/LC</th>
                        <th width="100">SC/LC No.</th>
                        <th width="70">Buyer Name</th>
                        <th width="100">Forwarder Name</th>
                        <th width="150">Lien Bank</th>
                        <th width="90">EXP Form No</th>
                        <th width="80">EXP Form Date</th>
                        <th width="100">Grs. Invoice Amount</th>
                        <th width="80">Discount</th>
                        <th width="70">Bonous</th>
                        <th width="70">Claim</th>
                        <th width="80">Commission</th>
                        <th width="100">Net Invoice Amount</th>
                        <th width="80">Currency</th>
                        <th width="80">PO NO</th>
                        <th width="110">Style Ref.</th>
                        <th width="110">Invoice Qnty.</th>
                        <th width="110">Ctn Qnty.</th>
                        <th width="80">Actual Ship Date</th>
                        <th width="100">B/L No</th>
                        <th width="80">B/L Date</th>
                        <th width="50">B/L Days</th>
                        <th width="90">Ship Bl No</th>
                        <th width="80">Ship Bl Date</th>
                        <th width="110">ETD</th>
                        <th width="100">Feeder Vessle</th>
                        <th width="100">Mother Vessle</th>
                        <th width="80">ETA Dest.</th>
                        <th width="100">Courier No(NN Docs)</th>
                        <th width="100">GSP/CO No.</th>
                        <th width="80">GSP/CO Date</th>
                        <th width="80">GSP Cour. Date</th>
                        <th width="80">Org B/L Rcv</th>
                        <th width="80">I/C Rcv Date</th>
                        <th width="80">Ex-Factory Date</th>
                        <th width="70">Document In Hand</th>
                        <th width="80">Doc Sub Date</th>
                        <th width="50">Sub. Days</th>
                        <th width="100">B TO B Courier No</th>
                        <th width="100">Bank Bill No.</th>
                        <th width="80">Bank Bill Date</th>
                        <th width="80">Pay Term</th>
                        <th width="80">Possible Rlz. Date</th>
                        <th width="80">Realized Date</th>
                        <th width="100">Remarks</th>
                    </tr>
                </thead>
            </table>
            <div style="width:4700px; overflow-y:scroll; max-height:290px;font-size:12px; overflow-x:hidden;" id="scroll_body">
            <table width="4680" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
                <tbody>
                <?php
                $cbo_company_name=str_replace("'","",$cbo_company_name);
                if($cbo_company_name!=0) $cbo_company_name = $cbo_company_name; else $cbo_company_name="%%";
				
                $cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
                if($cbo_buyer_name == 0) $cbo_buyer_name="%%"; else $cbo_buyer_name = $cbo_buyer_name;
				
                $cbo_lien_bank=str_replace("'","",$cbo_lien_bank);
                if($cbo_lien_bank == 0) $cbo_lien_bank="%%"; else $cbo_lien_bank = $cbo_lien_bank;
				//echo  $cbo_lien_bank;die;
                $cbo_location=str_replace("'","",$cbo_location);
                if($cbo_location == 0) $cbo_location="%%"; else $cbo_location = $cbo_location;
				
                if($cbo_based_on == 0) $cbo_based_on="%%"; else $cbo_based_on = $cbo_based_on;
				
                $txt_date_from=str_replace("'","",$txt_date_from);
                if(trim($txt_date_from)!= "") $txt_date_from  =$txt_date_from;
				
                $txt_date_to=str_replace("'","",$txt_date_to);
                if(trim($txt_date_to)!= "") $txt_date_to = $txt_date_to;
				
				$forwarder_name=str_replace("'","",$forwarder_name);
				if($forwarder_name!=0) $forwarder_cond=" and a.forwarder_name='$forwarder_name'"; else $forwarder_cond="";
				
                //if(trim($data[7])!="") $cbo_year2=$data[7];
                if ($txt_date_from!="" && $txt_date_to!="")
                {
					if($cbo_based_on ==0)
					{
						$str_cond=" and a.invoice_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else if($cbo_based_on ==1)
					{
						$str_cond=" and a.invoice_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else if($cbo_based_on ==2)
					{
						$str_cond=" and a.ex_factory_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else if($cbo_based_on ==3)
					{
						$str_cond=" and a.actual_shipment_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else if($cbo_based_on ==4)
					{
						$str_cond=" and a.bl_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else if($cbo_based_on ==5)
					{
						$str_cond=" and a.ship_bl_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else if($cbo_based_on ==6)
					{
						$str_cond=" and e.received_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else if($cbo_based_on ==7)
					{
						
						if($db_type==0) $str_cond=" and a.insert_date between '$txt_date_from 00:00:01' and  '$txt_date_to 23:59:59'";
						else if($db_type==2) $str_cond=" and a.insert_date between '$txt_date_from 12:00:01 AM' and  '$txt_date_to 11:59:59 PM'";
					}
					else if($cbo_based_on ==8)
					{
						$str_cond=" and a.invoice_date between '$txt_date_from' and  '$txt_date_to'";
					}
                }
                else
                {
                $str_cond="";
                }
				$shipping_mode=str_replace("'","",$shipping_mode);
				$ship_cond="";
				if($shipping_mode!=0) $ship_cond=" and a.shipping_mode=$shipping_mode";
				//echo  $str_cond;die;
				if($cbo_based_on==6)
				{
					if($db_type==0)
					{
						$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, DATE_FORMAT(a.insert_date,'%d-%m-%Y') as  insert_date, 1 as type
						FROM com_export_invoice_ship_mst a, com_export_lc b, com_export_doc_submission_invo d, com_export_proceed_realization e
						WHERE a.lc_sc_id=b.id and a.id=d.invoice_id and d.doc_submission_mst_id=e.invoice_bill_id  and a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond $forwarder_cond $ship_cond
						 
						UNION ALL
						
						SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, DATE_FORMAT(a.insert_date,'%d-%m-%Y') as  insert_date, 2 as type 
						FROM com_export_invoice_ship_mst a, com_sales_contract c , com_export_doc_submission_invo d, com_export_proceed_realization e
						WHERE a.lc_sc_id=c.id and a.id=d.invoice_id and d.doc_submission_mst_id=e.invoice_bill_id and a.is_lc=2 and a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and  a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond $forwarder_cond $ship_cond  ORDER BY id";
						
					}
					else
					{
						$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode,b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, to_char(a.insert_date,'DD-MM-YYYY') as  insert_date, 1 as type
					FROM com_export_invoice_ship_mst a, com_export_lc b , com_export_doc_submission_invo d, com_export_proceed_realization e
					WHERE a.lc_sc_id=b.id and a.id=d.invoice_id and d.doc_submission_mst_id=e.invoice_bill_id  and a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond $forwarder_cond $ship_cond
					 
					UNION ALL
					
					SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, to_char(a.insert_date,'DD-MM-YYYY') as  insert_date, 2 as type 
					FROM com_export_invoice_ship_mst a, com_sales_contract c, com_export_doc_submission_invo d, com_export_proceed_realization e
					WHERE a.lc_sc_id=c.id and a.id=d.invoice_id and d.doc_submission_mst_id=e.invoice_bill_id  and a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=2 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond $forwarder_cond $ship_cond  ORDER BY id";
					}
				}
				else if($cbo_based_on==8)
				{
					if($db_type==0)
					{
						$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, DATE_FORMAT(a.insert_date,'%d-%m-%Y') as  insert_date, 1 as type
						FROM com_export_invoice_ship_mst a, com_export_lc b 
						WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.lc_sc_id=b.id and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond $forwarder_cond $ship_cond and a.id not in(select a.invoice_id from com_export_doc_submission_invo a, com_export_proceed_realization b where a.doc_submission_mst_id=b.invoice_bill_id and a.is_lc=1)
						 
						UNION ALL
						
						SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, DATE_FORMAT(a.insert_date,'%d-%m-%Y') as  insert_date, 2 as type 
						FROM com_export_invoice_ship_mst a, com_sales_contract c 
						WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=2 and a.lc_sc_id=c.id and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond $forwarder_cond $ship_cond and a.id not in(select a.invoice_id from com_export_doc_submission_invo a, com_export_proceed_realization b where a.doc_submission_mst_id=b.invoice_bill_id and a.is_lc=2)  ORDER BY id";
						
					}
					else
					{
						$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode,b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, to_char(a.insert_date,'DD-MM-YYYY') as  insert_date, 1 as type
					FROM com_export_invoice_ship_mst a, com_export_lc b 
					WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.lc_sc_id=b.id and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond $forwarder_cond $ship_cond and a.id not in(select a.invoice_id from com_export_doc_submission_invo a, com_export_proceed_realization b where a.doc_submission_mst_id=b.invoice_bill_id and a.is_lc=1)
					 
					UNION ALL
					
					SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, to_char(a.insert_date,'DD-MM-YYYY') as  insert_date, 2 as type 
					FROM com_export_invoice_ship_mst a, com_sales_contract c 
					WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=2 and a.lc_sc_id=c.id and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond $forwarder_cond $ship_cond and a.id not in(select a.invoice_id from com_export_doc_submission_invo a, com_export_proceed_realization b where a.doc_submission_mst_id=b.invoice_bill_id and a.is_lc=2)  ORDER BY id";
					}
				}
				else
				{
					if($db_type==0)
					{
						$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, DATE_FORMAT(a.insert_date,'%d-%m-%Y') as  insert_date, 1 as type
						FROM com_export_invoice_ship_mst a, com_export_lc b 
						WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.lc_sc_id=b.id and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond $forwarder_cond $ship_cond
						 
						UNION ALL
						
						SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, DATE_FORMAT(a.insert_date,'%d-%m-%Y') as  insert_date, 2 as type 
						FROM com_export_invoice_ship_mst a, com_sales_contract c 
						WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=2 and a.lc_sc_id=c.id and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond $forwarder_cond $ship_cond  ORDER BY id";
						
					}
					else
					{
						$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode,b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, to_char(a.insert_date,'DD-MM-YYYY') as  insert_date, 1 as type
					FROM com_export_invoice_ship_mst a, com_export_lc b 
					WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.lc_sc_id=b.id and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond $forwarder_cond $ship_cond
					 
					UNION ALL
					
					SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, a.forwarder_name, a.country_id, a.shipping_mode, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, to_char(a.insert_date,'DD-MM-YYYY') as  insert_date, 2 as type 
					FROM com_export_invoice_ship_mst a, com_sales_contract c 
					WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=2 and a.lc_sc_id=c.id and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond $forwarder_cond $ship_cond  ORDER BY id";
					}	
				}
				
				//echo $sql;die;
                $sql_re=sql_select($sql);$k=1;
                foreach($sql_re as $row_result)
                {
					if ($k%2==0)
					$bgcolor="#E9F3FF";
					else
					$bgcolor="#FFFFFF";
					$id=$row_result[csf('id')];
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $k; ?>">
                        <td  width="50"><p><?php echo $k;//$row_result[csf('id')];?></p></td>
                        <td  width="100"><p><?php echo  $company_arr[$row_result[csf('benificiary_id')]]; ?></p></td>
                        <td  width="100"><p><?php echo $location_arr[$row_result[csf('location_id')]];?></p></td>
                        <td align="center" width="100"><p><?php echo $country_arr[$row_result[csf('country_id')]];?></p></td>
                        <td align="center" width="100"><p><?php echo $row_result[csf('invoice_no')];?></p></td>
                        <td align="center" width="80"><p><?php echo $shipment_mode[$row_result[csf('shipping_mode')]];?></p></td>
                        <td align="center" width="70"><p>
                        <?php if($row_result[csf('invoice_date')]!="0000-00-00" && $row_result[csf('invoice_date')]!="") {echo change_date_format($row_result[csf('invoice_date')]);} else {echo "&nbsp;";}?>
                        </p></td>
                        <td align="center" width="70"><p>
                        <?php if($row_result[csf('insert_date')]!="0000-00-00" && $row_result[csf('insert_date')]!="") {echo change_date_format($row_result[csf('insert_date')]);} else {echo "&nbsp;";}?>
                        </p></td> 
                        <td align="center" width="70"><p><?php if($row_result[csf('type')] == 1) echo "LC"; else echo "SC"; ?></p></td>
                        <td width="100"><p><?php echo $row_result[csf('lc_sc_no')];?></p></td>
                        <td width="70"><p><?php echo  $buyer_arr[$row_result[csf('buyer_id')]];?></p></td>
                        <td width="100"><p><?php echo  $buyer_arr[$row_result[csf('forwarder_name')]];?></p></td>
                        <td width="150"><p><?php echo $bank_arr[$row_result[csf('lien_bank')]];?></p></td>
                        <td width="90"><p><?php echo $row_result[csf('exp_form_no')];?></p></td>
                        <td align="center" width="80"><p><?php  if($row_result[csf('exp_form_date')]!="0000-00-00" && $row_result[csf('exp_form_date')]!="") {echo change_date_format($row_result[csf('exp_form_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($row_result[csf('invoice_value')],2,'.',''); $total_grs_value +=$row_result[csf('invoice_value')];?></p></td>
                        <td width="80" align="right"><p><?php echo number_format($row_result[csf('discount_ammount')],2,'.',''); $total_discount_value +=$row_result[csf('discount_ammount')]; ?></p></td>
                        <td width="70" align="right"><p><?php echo number_format($row_result[csf('bonus_ammount')],2,'.',''); $total_bonous_value +=$row_result[csf('bonus_ammount')];  ?></p></td>
                        <td width="70" align="right"><p><?php echo number_format($row_result[csf('claim_ammount')],2,'.',''); $total_claim_value +=$row_result[csf('claim_ammount')];  ?></p></td>
                        <td width="80" align="right"><p><?php  echo number_format($row_result[csf('commission')],2,'.',''); $total_commission_value +=$row_result[csf('commission')]; ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($row_result[csf('net_invo_value')],2,'.',''); $total_order_qnty +=$row_result[csf('net_invo_value')];?></p></td>
                        <td align="center" width="80"><p><?php echo $currency[$row_result[csf('currency_name')]];?></p></td>
                        <td align="center" width="80">
                        <p><?php //echo $row[import_invoice_id]; 
                        echo "<a href='#report_detals'  onclick= \"openmypage('$id','$k');\"> View</a>";
                        ?></p>
                        </td>
                        <td width="110">
                        <p><?php
                        //$ref_field=return_field_value("a.style_ref_no","wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c","a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.mst_id=".$row_result[csf('id')],"style_ref_no");
                        echo $style_ref_arr[$row_result[csf('id')]];
                       //echo "select a.style_ref_no from wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.mst_id=".$row_result[csf('id')]."";
                        ?></p>
                        </td>
                        <td align="right" width="110"><p><?php echo number_format($row_result[csf('invoice_quantity')],2); $total_invoice_qty +=$row_result[csf('invoice_quantity')];?></p></td>
                        <td align="right" width="110"><p><?php echo number_format($row_result[csf('total_carton_qnty')],2); $total_carton_qty +=$row_result[csf('total_carton_qnty')];?></p></td>
                        <td align="center" width="80"><p><?php  if($row_result[csf('actual_shipment_date')]!="0000-00-00" && $row_result[csf('actual_shipment_date')]!="") {echo change_date_format($row_result[csf('actual_shipment_date')]);} else {echo "&nbsp;";}  ?></p></td>
                        <td width="100"><p><?php echo $row_result[csf('bl_no')];?></p></td>
                        <td align="center" width="80"><p><?php  if($row_result[csf('bl_date')]!="0000-00-00" && $row_result[csf('bl_date')]!="") {echo change_date_format($row_result[csf('bl_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td align="center" width="50"><p><?php  $diff_bl=datediff("d",$row_result[csf('ex_factory_date')], $row_result[csf('bl_date')]); if($diff_bl>0) echo $diff_bl."days";  ?></p></td>
                        <td width="90"><p><?php echo $row_result[csf('shipping_bill_n')]; //a.shipping_bill_n,a.ship_bl_date, ?></p></td>
                        <td align="center" width="80"><p><?php  if($row_result[csf('ship_bl_date')]!="0000-00-00" && $row_result[csf('ship_bl_date')]!="") {echo change_date_format($row_result[csf('ship_bl_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td align="center" width="110"><p><?php  if($row_result[csf('etd')]!="0000-00-00" && $row_result[csf('etd')]!="") {echo change_date_format($row_result[csf('etd')]);} else {echo "&nbsp;";}?></p></td>
                        <td width="100"><p><?php echo $row_result[csf('feeder_vessel')];?></p></td>
                        <td width="100"><p><?php echo $row_result[csf('mother_vessel')];?></p></td>
                        <td align="center" width="80"><p><?php  if($row_result[csf('etd_destination')]!="0000-00-00" && $row_result[csf('etd_destination')]!="") {echo change_date_format($row_result[csf('etd_destination')]);} else { echo "&nbsp;";} ?></p></td>
                        <td width="100">
                        <p></p>
                        </td>
                        <td width="100" align="center"><p><?php echo $row_result[csf('gsp_co_no')];?></p></td>
                        <td width="80" align="center"><p>
                        <?php if(trim($row_result[csf('gsp_co_no_date')])!="0000-00-00" && trim($row_result[csf('gsp_co_no_date')])!="") {echo change_date_format($row_result[csf('gsp_co_no_date')]);}else {echo "&nbsp;";}?>
                        </p></td>
                        <td align="center" width="80">
                        <p><?php
                        //$curier_receipt_date=return_field_value("a.courier_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf("id")]."","courier_date");
                        
                        //echo $row_result[csf("id")];
						$curier_receipt_date=$currier_date_arr[$row_result[csf("id")]];
                        if(!(trim($curier_receipt_date)=="0000-00-00" || trim($curier_receipt_date)=="")) 
                        {
                            echo change_date_format($curier_receipt_date);
                        }
                        else
                        {
                            echo "&nbsp;";	
                        }
                        ?></p>
                        </td>
                        <td align="center" width="80"><p><?php if($row_result[csf('bl_rev_date')]!="0000-00-00" && $row_result[csf('bl_rev_date')]!="") {echo change_date_format($row_result[csf('bl_rev_date')]);} else { echo "&nbsp;";}?></p></td>
                        <td align="center" width="80"><p><?php if($row_result[csf('etd')]!="0000-00-00" && $row_result[csf('etd')]!="") echo change_date_format( $row_result[csf('ic_recieved_date')]); else echo "";?></p></td>
                        <td align="center" width="80"><p><?php if($row_result[csf('ex_factory_date')]!="0000-00-00" && $row_result[csf('ex_factory_date')]!="") {echo change_date_format($row_result[csf('ex_factory_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td align="center" width="70"><p>
                        <?php
                        //$curier_submit_date=return_field_value("a.submit_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')]."","submit_date");
                        //var_dump($curier_submit_date);die;
                        if(($row_result[csf('ex_factory_date')]!='0000-00-00') )
                        {
                            $current_date=date("Y-m-d");
                            if($curier_submit_date_arr[$row_result[csf('id')]]=='0000-00-00' || $curier_submit_date_arr[$row_result[csf('id')]]==null)
                            {
                            $diff=datediff("d",$row_result[csf('ex_factory_date')], $current_date);
                            }
                            else
                            {
                            $diff=datediff("d",$row_result[csf('ex_factory_date')], $curier_submit_date_arr[$row_result[csf('id')]]);
                            }
                            
                        }
                        else 
                        {
                            $diff="";
                        }
                        
                        echo $diff;
                        ?>
                        </p></td>
                        <td align="center" width="80"><p>
                        <?php
                            if(trim($curier_submit_date_arr[$row_result[csf('id')]])!='0000-00-00' && trim($curier_submit_date_arr[$row_result[csf('id')]])!='') echo change_date_format(trim($curier_submit_date_arr[$row_result[csf('id')]])); else echo "&nbsp;";
                        ?>
                        </p></td>
                        <td align="center" width="50"><p><?php  $diff_sub=datediff("d",$row_result[csf('bl_date')], $curier_submit_date_arr[$row_result[csf('id')]]); if($diff_sub>0) echo $diff_sub."days";   ?></p></td>
                        <td width="100" align="center">
                        <p><?php
                        //$bnk_to_bnk_cour_no=return_field_value("a.bnk_to_bnk_cour_no","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bnk_to_bnk_cour_no");
                        echo $bnk_to_bnk_cour_no_arr[$row_result[csf('id')]];
                        ?></p>
                        </td>
                        <td width="100">
                        <p><?php
                        //$bank_ref_no=return_field_value("a.bank_ref_no","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bank_ref_no");
                        echo $bank_ref_no_arr[$row_result[csf('id')]];
                        ?></p>
                        </td>
                        <td align="center" width="80"> 
                        <p><?php
                       // $bank_date_no=return_field_value("a.bank_ref_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bank_ref_date");
                        if(!(trim($bank_date_no_arr[$row_result[csf('id')]])=="0000-00-00" || trim($bank_date_no_arr[$row_result[csf('id')]])=="")) 
                        {
                            echo change_date_format($bank_date_no_arr[$row_result[csf('id')]]);
                        }
                        else
                        {
                            echo "&nbsp;";	
                        }
                        ?></p>
                        </td>
                        <td width="80" align="center"><p><?php echo $pay_term[$row_result[csf('pay_term')]];?></p></td>
                        <td width="80" align="center"><p>
                        <?php
						if(!(trim($possible_rlz_date_arr[$row_result[csf('id')]])=="0000-00-00" || trim($possible_rlz_date_arr[$row_result[csf('id')]])=="")) 
						{
							echo change_date_format($possible_rlz_date_arr[$row_result[csf('id')]]);
						}
						else
						{
							echo "&nbsp;";	
						}
                         ?>
                        </p></td>
                        <td width="80" align="center"><p>
                        <?php
						if(!(trim($rlz_date_arr[$row_result[csf('id')]])=="0000-00-00" || trim($possible_rlz_date_arr[$row_result[csf('id')]])=="")) 
						{
							echo change_date_format($rlz_date_arr[$row_result[csf('id')]]);
						}
						else
						{
							echo "&nbsp;";	
						}
                         ?>
                        </p></td>
                        <td  width="100"><p><?php echo $row_result[csf('remarks')];?></p></td>
					</tr>
					<?php
					$k++;
                }
                //print_r($sc_value_1_3);
                
                ?>
                </tbody>
            </table>

            <table width="4680" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="report_table_footer">
            	<tfoot>
                <tr>
                	<th width="50" >&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="150">&nbsp;</th>
                    <th width="90">&nbsp;</th>
                    <th width="80" align="right">Grand Total:</th>
                    <th width="100"id="value_total_grs_value"><?php echo number_format($total_grs_value,2);  ?></th>
                    <th width="80"id="value_total_discount_value"><?php echo number_format($total_discount_value,2);  ?></th>
                    <th width="70"id="value_total_bonous_value"><?php echo number_format($total_bonous_value,2);  ?></th>
                    <th width="70"id="value_total_claim_value"><?php echo number_format($total_claim_value,2);  ?></th>
                    <th width="80"id="value_total_commission_value"><?php echo number_format($total_commission_value,2);  ?></th>
                    <th width="100" id="value_total_net_invo_value"><?php echo number_format($total_order_qnty,2);  ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="110" ></th>
                    <th width="110" id="total_invoice_qty"><?php echo number_format($total_invoice_qty,2); ?></th>
                    <th width="110"id="total_carton_qty"><?php echo number_format($total_carton_qty,2); ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="50">&nbsp;</th>
                    <th width="90">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="110">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="50">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                </tr>
                </tfoot>
            </table>
               <div align="left" style="font-weight:bold; margin-left:30px;"><?php echo "User Id : ". $user_arr[$user_id] ." , &nbsp; THIS IS SYSTEM GENERATED STATEMENT, NO SIGNATURE REQUIRED ."; ?></div>
     </div>
    </div>
	<?php
	}
	else
	{
		$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
		$buyer_arr=return_library_array("select id,buyer_name from lib_buyer","id","buyer_name");
		$cbo_company_name=str_replace("'","",$cbo_company_name);
		$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
		$txt_date_from=str_replace("'","",$txt_date_from);
		$txt_date_to=str_replace("'","",$txt_date_to);
		$shipping_mode=str_replace("'","",$shipping_mode);
		$ship_cond="";
		if($cbo_company_name!=0) $cbo_company_name = $cbo_company_name; else $cbo_company_name="%%";
		if($cbo_buyer_name != 0) $cbo_buyer_name = $cbo_buyer_name;  else $cbo_buyer_name="%%";
		if($shipping_mode!=0) $ship_cond=" and a.shipping_mode=$shipping_mode";
		
		$date_cond="";
		if($txt_date_from!="" && $txt_date_to!="")  $date_cond=" and a.invoice_date between '$txt_date_from' and '$txt_date_to'";
		$sql="select a.id, a.invoice_no, a.invoice_date, a.buyer_id, a.benificiary_id, a.commission, a.shipping_mode, b.id as dtls_id, b.po_breakdown_id, b.current_invoice_rate, b.current_invoice_qnty, b.current_invoice_value from com_export_invoice_ship_mst a, com_export_invoice_ship_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.benificiary_id like '$cbo_company_name' and a.buyer_id like '$cbo_buyer_name' $date_cond $ship_cond order by a.id";
        $sql_re=sql_select($sql);
		$invoice_data=array();
		foreach($sql_re as $row)
		{
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['dtls_id']=$row[csf("dtls_id")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['invoice_no']=$row[csf("invoice_no")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['invoice_date']=$row[csf("invoice_date")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['buyer_id']=$row[csf("buyer_id")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['benificiary_id']=$row[csf("benificiary_id")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['commission']=$row[csf("commission")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['po_breakdown_id']=$row[csf("po_breakdown_id")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['current_invoice_rate']=$row[csf("current_invoice_rate")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['current_invoice_value']=$row[csf("current_invoice_value")];
			$invoice_data[$row[csf("id")]][$row[csf("dtls_id")]]['shipping_mode']=$row[csf("shipping_mode")];
		}
		$order_sql=sql_select("select id, po_number, po_quantity from wo_po_break_down");
		$order_data_arr=array();
		foreach($order_sql as $row)
		{
			$order_data_arr[$row[csf("id")]]["po_number"]=$row[csf("po_number")];
			$order_data_arr[$row[csf("id")]]["po_quantity"]=$row[csf("po_quantity")];
		}
		
		?>
        <div style="width:1450px">
            <table width="1430" cellpadding="0" cellspacing="0" id="caption">
                <tr>
                	<td align="center" width="100%" colspan="13" class="form_caption" ><strong style="font-size:18px">Company Name:<?php echo " ". $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
                </tr> 
                <tr>  
                	<td align="center" width="100%" colspan="13" class="form_caption" ><strong style="font-size:18px"><?php echo $report_title; ?></strong></td>
                </tr>
            </table>
            <table width="1430" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_header_short" align="left">
                <thead>
                    <tr>
                        <th width="40">Sl</th>
                        <th width="130">Invoice No</th>
                        <th width="80">Invoice Date</th>
                        <th width="130">Order No</th>
                        <th width="100">Order Qnty</th>
                        <th width="100">Ship Qnty</th>
                        <th width="80">Short Qnty</th>
                        <th width="80">Extra Qnty</th>
                        <th width="70">Unite Price</th>
                        <th width="100">Invoice Value</th>
                        <th width="100">Short Qnty Value</th>
                        <th width="100">Extra Qnty Value</th>
                        <th width="100">Total Commission</th>
                        <th width="100">Ship Mode</th>
                        <th >Buyer Name</th>
                    </tr>
                </thead>
            </table>
            <div style="width:1450px; overflow-y:scroll; max-height:310px;font-size:12px; overflow-x:hidden;" id="scroll_body_short">
            <table width="1430" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body_short">
                <tbody>
                <?php
				$k=1;$temp_arr=$temp_arr2=array();$i=1;$j=1;
                foreach($invoice_data as $inv_id=>$row_val)
                {
					if($j!=1)
					{
						?>
						<tr bgcolor="#CCCCCC">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Sub Total:</td>
							<td align="right"><?php echo number_format($sub_order_qnty,0); ?></td>
							<td align="right"><?php echo number_format($sub_ship_qnty,0); ?></td>
							<td align="right"><?php echo number_format($sub_short_qnty,0); ?></td>
							<td align="right"><?php echo number_format($sub_extra_qnty,0); ?></td>
							<td>&nbsp;</td>
							<td align="right"><?php echo number_format($sub_ship_value,2); ?></td>
							<td align="right"><?php echo number_format($sub_short_value,2); ?></td>
							<td align="right"><?php echo number_format($sub_extra_value,2); ?></td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<?php
						$sub_order_qnty=$sub_ship_qnty=$sub_short_qnty=$sub_extra_qnty=$sub_ship_value=$sub_short_value=$sub_extra_value=0;
					}
					$j++;
					$row_count=count($row_val);
					$flag=$flag2=1;
					foreach($row_val as $row_result)
					{
						if ($k%2==0)
						$bgcolor="#E9F3FF";
						else
						$bgcolor="#FFFFFF";
						$extra_qnty=$short_qnty=$short_value=$extra_value=0;
						if($order_data_arr[$row_result[('po_breakdown_id')]]["po_quantity"]>$row_result[('current_invoice_qnty')]) $short_qnty=$order_data_arr[$row_result[('po_breakdown_id')]]["po_quantity"]-$row_result[('current_invoice_qnty')];
						if($order_data_arr[$row_result[('po_breakdown_id')]]["po_quantity"]<$row_result[('current_invoice_qnty')]) $extra_qnty=$row_result[('current_invoice_qnty')]-$order_data_arr[$row_result[('po_breakdown_id')]]["po_quantity"];
						$short_value=$short_qnty*$row_result[('current_invoice_rate')];
						$extra_value=$extra_qnty*$row_result[('current_invoice_rate')];
						//$short_qnty=0;
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $k; ?>">
							<?php
							if($flag==1)
							{
								?>
                                <td  width="40" rowspan="<?php echo $row_count; ?>" align="center" valign="top"><p><?php echo $i;?>&nbsp;</p></td>
                                <td  width="130" rowspan="<?php echo $row_count; ?>" valign="top"><p><?php echo  $row_result[('invoice_no')]; ?>&nbsp;</p></td>
                                <td  width="80" align="center" rowspan="<?php echo $row_count; ?>" valign="top"><p><?php if($row_result[('invoice_date')]!="" && $row_result[('invoice_date')]!="0000-00-00") echo change_date_format($row_result[('invoice_date')]); ?>&nbsp;</p></td>
                                <?php
								$flag=0;$i++;
							}
							?>
							<td  width="130"><p><?php echo $order_data_arr[$row_result[('po_breakdown_id')]]["po_number"];?>&nbsp;</p></td>
							<td align="right" width="100"><p><?php echo number_format($order_data_arr[$row_result[('po_breakdown_id')]]["po_quantity"],0); $sub_order_qnty+=$order_data_arr[$row_result[('po_breakdown_id')]]["po_quantity"]; $grand_order_qnty+=$order_data_arr[$row_result[('po_breakdown_id')]]["po_quantity"];?></p></td>
							<td align="right" width="100"><p><?php echo number_format($row_result[('current_invoice_qnty')],0); $sub_ship_qnty+=$row_result[('current_invoice_qnty')]; $grand_ship_qnty+=$row_result[('current_invoice_qnty')];?></p></td>
							<td align="right" width="80"><p><?php echo number_format($short_qnty,0); $sub_short_qnty+=$short_qnty; $grand_short_qnty+=$short_qnty;?></p></td>
							<td align="right" width="80"><p><?php echo number_format($extra_qnty,0);  $sub_extra_qnty+=$extra_qnty; $grand_extra_qnty+=$extra_qnty; ?></p></td>
							<td align="right" width="70"><p><?php echo number_format($row_result[('current_invoice_rate')],2); ?></p></td> 
							<td align="right" width="100"><p><?php echo number_format($row_result[('current_invoice_value')],2);  $sub_ship_value+=$row_result[('current_invoice_value')]; $grand_ship_value+=$row_result[('current_invoice_value')];  ?></p></td>
							<td align="right" width="100"><p><?php echo number_format($short_value,2); $sub_short_value+=$short_value; $grand_short_value+=$short_value; ?></p></td>
							<td align="right" width="100"><p><?php echo number_format($extra_value,2); $sub_extra_value+=$extra_value; $grand_extra_value+=$extra_value; ?></p></td>
							<?php
							if($flag2==1)
							{
								?>
								<td align="right" width="100" rowspan="<?php echo $row_count; ?>" valign="top"><p><?php echo number_format($row_result[('commission')],2); ?></p></td>
                                <td width="100" align="center" rowspan="<?php echo $row_count; ?>" valign="top"><p><?php echo $shipment_mode[$row_result[('shipping_mode')]]; ?></p></td>
								<td style="padding-left:3px;" rowspan="<?php echo $row_count; ?>" valign="top"><p><?php echo $buyer_arr[$row_result[('buyer_id')]];?></p></td>
								<?php
								$flag2=0;
							}
							?>
						</tr>
						<?php
						$k++;
					}
                }
                //print_r($sc_value_1_3);
                ?>
                <tr bgcolor="#CCCCCC">
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>Sub Total:</td>
                    <td align="right"><?php echo number_format($sub_order_qnty,0); ?></td>
                    <td align="right"><?php echo number_format($sub_ship_qnty,0); ?></td>
                    <td align="right"><?php echo number_format($sub_short_qnty,0); ?></td>
                    <td align="right"><?php echo number_format($sub_extra_qnty,0); ?></td>
                    <td>&nbsp;</td>
                    <td align="right"><?php echo number_format($sub_ship_value,2); ?></td>
                    <td align="right"><?php echo number_format($sub_short_value,2); ?></td>
                    <td align="right"><?php echo number_format($sub_extra_value,2); ?></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
     		</div>
            <table width="1430" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="report_table_footer_short" align="left">
            	<tfoot>
                <tr>
                	<th width="40">&nbsp;</th>
                    <th width="130">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="130">Grand Total:</th>
                    <th width="100"><?php echo number_format($grand_order_qnty,0); ?></th>
                    <th width="100"><?php echo number_format($grand_ship_qnty,0); ?></th>
                    <th width="80"><?php echo number_format($grand_short_qnty,0); ?></th>
                    <th width="80"><?php echo number_format($grand_extra_qnty,0); ?></th>
                    <th width="70">&nbsp;</th>
                    <th width="100"><?php echo number_format($grand_ship_value,2); ?></th>
                    <th width="100"><?php echo number_format($grand_short_value,2); ?></th>
                    <th width="100"><?php echo number_format($grand_extra_value,2); ?></th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th >&nbsp;</th>
                </tr>
                </tfoot>
            </table>
    </div>
    <?php
	}
	exit();
}
if($action=="po_id_details")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	$po_id=str_replace("'","",$po_id);
	//print_r($po_id);die;
?> 

<div style="width:460px">
<fieldset style="width:100%"  >
    <table border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="440">
        <thead>
            <th width="120">PO NO</th>
            <th width="110">PO Qnty</th>
            <th width="110">PO Attach Qnty</th>
            <th width="100">Invoice Qnty</th>
        </thead>
    </table>
	<div style="width:460px; max-height:280px; overflow-y:scroll">   
    <table border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="440">
		<?php
		if($db_type==0)
		{
			$company_sql="select c.invoice_no, b.po_number, b.po_quantity, a.po_breakdown_id, a.current_invoice_qnty from com_export_invoice_ship_dtls a, wo_po_break_down b, com_export_invoice_ship_mst c where a.po_breakdown_id=b.id and a.mst_id=c.id and c.id='$po_id' and a.current_invoice_qnty not in(0) and a.status_active=1 and a.is_deleted=0";
		}
		else if($db_type==2)
		{
			$company_sql="select c.invoice_no, b.po_number, b.po_quantity, a.po_breakdown_id, a.current_invoice_qnty from com_export_invoice_ship_dtls a, wo_po_break_down b, com_export_invoice_ship_mst c where a.po_breakdown_id=b.id and a.mst_id=c.id and c.id='$po_id' and a.current_invoice_qnty not in(0) and a.status_active=1 and a.is_deleted=0";
		}
        
		//echo $company_sql;die;
        //$sql_re=sql_select($sql);
		$i=$k+1;
        $sql_re=sql_select($company_sql);
        $total_invoice_qty=0;  $total_order_qty=0;  $total_attach_qty=0;
        $result=0;
        foreach($sql_re as $row)
        {  
        ?>
    
        <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
        	<td width="120"><p><?php echo $row[csf('po_number')]; ?></p></td>
            <td width="110" align="right">
            <p><?php 
            echo number_format($row[csf('po_quantity')],0); 
            $total_order_qty +=$row[csf('po_quantity')];
            ?></p>
            </td>
            <td width="110" align="right">
            <p><?php 
			$borken_id=number_format($row[csf('po_breakdown_id')],0);
            $att_qnty_lc=return_field_value("sum(attached_qnty)","com_export_lc_order_info","wo_po_break_down_id='$borken_id' and status_active=1 and is_deleted=0 ");
			//echo $att_qnty_lc;
            $att_qnty_sc=return_field_value("sum(attached_qnty)","com_sales_contract_order_info","wo_po_break_down_id='$borken_id' and status_active=1 and is_deleted=0");
            $attached_qnty=$att_qnty_lc+$att_qnty_sc;
            echo number_format($attached_qnty,0); 
            $total_attach_qty+=$attached_qnty;
            ?></p>
            </td>
            <td align="right" width="100">
            <p><?php 
            $result=$row[csf('current_invoice_qnty')]; 
            echo number_format( $result,0);
            $total_invoice_qty+=$result;
            ?></p>
            </td>
        
        </tr>
		<?php
		$i++;
        }
        ?>
        
        <tr >
            <td align="right" width="120">Total</td>
            <td align="right" width="110"><p><?php echo number_format($total_order_qty,0); ?></p></td>
            <td align="right" width="110"><p><?php echo number_format($total_attach_qty,0); ?></p></td>
            <td align="right" width="100"><p><?php echo number_format($total_invoice_qty,0); ?></p></td>
        </tr>
    </table>
    </div>
</fieldset>
</div>
<?php
exit();
}

disconnect($con);
?>


