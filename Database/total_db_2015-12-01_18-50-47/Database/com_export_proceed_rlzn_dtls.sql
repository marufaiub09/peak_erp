-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_export_proceed_rlzn_dtls`
--

DROP TABLE IF EXISTS `com_export_proceed_rlzn_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_export_proceed_rlzn_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=deducation, 1=distribution',
  `account_head` int(11) NOT NULL DEFAULT '0',
  `ac_loan_no` varchar(64) NOT NULL,
  `document_currency` double NOT NULL DEFAULT '0',
  `conversion_rate` double NOT NULL DEFAULT '0',
  `domestic_currency` double NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_export_proceed_rlzn_dtls`
--

LOCK TABLES `com_export_proceed_rlzn_dtls` WRITE;
/*!40000 ALTER TABLE `com_export_proceed_rlzn_dtls` DISABLE KEYS */;
INSERT INTO `com_export_proceed_rlzn_dtls` VALUES (1,1,0,5,'',5,78,390,1,'2015-10-25 13:46:44',0,'0000-00-00 00:00:00',1,0),(2,1,1,10,'11100005680',3,0,0,1,'2015-10-25 13:46:44',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `com_export_proceed_rlzn_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
