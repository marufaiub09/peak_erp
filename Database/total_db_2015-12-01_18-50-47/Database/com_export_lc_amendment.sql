-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_export_lc_amendment`
--

DROP TABLE IF EXISTS `com_export_lc_amendment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_export_lc_amendment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amendment_no` int(11) NOT NULL DEFAULT '0',
  `amendment_date` date NOT NULL,
  `export_lc_id` int(11) NOT NULL DEFAULT '0',
  `export_lc_no` varchar(50) NOT NULL,
  `lc_value` double NOT NULL DEFAULT '0',
  `amendment_value` double NOT NULL DEFAULT '0',
  `value_change_by` tinyint(2) NOT NULL DEFAULT '0',
  `last_shipment_date` date NOT NULL,
  `expiry_date` date NOT NULL,
  `shipping_mode` int(11) NOT NULL DEFAULT '0',
  `pay_term` int(11) NOT NULL DEFAULT '0',
  `inco_term` int(11) NOT NULL DEFAULT '0',
  `inco_term_place` varchar(100) NOT NULL,
  `port_of_entry` varchar(50) NOT NULL,
  `port_of_loading` varchar(50) NOT NULL,
  `port_of_discharge` varchar(50) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `tenor` int(11) NOT NULL DEFAULT '0',
  `discount_clauses` tinytext NOT NULL,
  `claim_adjustment` double NOT NULL DEFAULT '0',
  `claim_adjust_by` int(2) NOT NULL DEFAULT '0',
  `po_id` varchar(2000) NOT NULL,
  `is_original` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=No;1=Yes',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `amendment_no` (`amendment_no`),
  KEY `amendment_date` (`amendment_date`),
  KEY `contact_system_id` (`export_lc_id`),
  KEY `contract_no` (`export_lc_no`),
  KEY `last_shipment_date` (`last_shipment_date`),
  KEY `expiry_date` (`expiry_date`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_export_lc_amendment`
--

LOCK TABLES `com_export_lc_amendment` WRITE;
/*!40000 ALTER TABLE `com_export_lc_amendment` DISABLE KEYS */;
INSERT INTO `com_export_lc_amendment` VALUES (1,0,'0000-00-00',16,'DC TST521630',29422.36,0,0,'2015-09-30','2015-10-15',1,1,1,'FOB','Ctg','CTG','ANY PORT OF U.K','',0,'',0,0,'388,389,384,385,386,387',1,13,'2015-10-24 16:56:10',0,'0000-00-00 00:00:00',1,0),(2,1,'2015-10-20',16,'DC TST521630',104191.12,74768.76,1,'2015-12-15','2015-12-22',0,0,0,'','','','','',0,'',0,1,'388,389,384,385,386,387,503,502,501,509,516,500,508,510,524,525,521,519,497,498,507,506,505,523,522',0,13,'2015-10-25 19:07:38',13,'2015-10-25 19:09:53',1,0);
/*!40000 ALTER TABLE `com_export_lc_amendment` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
