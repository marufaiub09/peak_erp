<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$supplier_arr=return_library_array( "select id,supplier_name from lib_supplier", "id", "supplier_name"  );
$bank_arr=return_library_array( "select id,bank_name from  lib_bank", "id", "bank_name"  );
$lc_charge_arr=array(1=>"Lc Opening",2=>"Lc Amendments");

/*if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier_id", 130, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where FIND_IN_SET($data,a.tag_company) and a.id=b.supplier_id and a.status_active=1 and a.is_deleted=0 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select Supplier--", "", "" );  //and b.party_type in (1,6,7,8,90)	 
	exit();
}*/
if($db_type==2 || $db_type==1 )
{
	$mrr_date_check="and to_char(insert_date,'YYYY')=".date('Y',time())."";
	$concat="";
	$concat_coma="||";
	$groupconcat="wm_concat";
}
else if ($db_type==0)
{
	$mrr_date_check="and year(insert_date)=".date('Y',time())."";
	$concat="concat";
	$concat_coma=",";
	$groupconcat="group_concat";

}


if($action=="supplier_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$company=str_replace("'","",$company);
	//echo $company;
	
	
?>
<script>
	function js_set_value(str)
	{
		document.getElementById("hidden_suplier_id").value=str;
		parent.emailwindow.hide(); 
	}
</script>
<?php
	$sql="select a.id,a.supplier_name,a.designation from  lib_supplier a,lib_supplier_tag_company b where a.id=b.supplier_id and  a.status_active=1 and b.tag_company=$company order by supplier_name";
	//echo $sql;die; 
	$arr=array();
	echo  create_list_view("list_view1", "Supplier Name,Designation", "200,150","400","330",0, $sql , "js_set_value", "id,supplier_name", "", 1, "0,0", $arr , "supplier_name,designation", "","setFilterGrid('list_view1',-1)");
	echo "<input type='hidden' id='hidden_suplier_id' />";
	exit();

}




if($action=="lc_no_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	//echo $company."----";echo $bank."----";echo $supplier."----";echo $date_from."----";echo $date_to."----";
	$company=str_replace("'","",$company);
	$bank=str_replace("'","",$bank);
	$supplier=str_replace("'","",$supplier);
	$date_from=str_replace("'","",$date_from);
	$date_to=str_replace("'","",$date_to);
	if($bank!=0) $bank="and issuing_bank_id=$bank"; else $bank="";
	if($supplier!="") $supplier="and supplier_id=$supplier"; else $supplier="";
	$date_from=change_date_format($date_from,"yyyy-mm-dd");
	$date_to=change_date_format($date_to,"yyyy-mm-dd");
	if($date_from!="") $lc_date="and lc_date between '$date_from' and '$date_to' "; else $lc_date="";
	//echo $lc_date;
?>
<script>
	function js_set_value(str)
	{
		//$("#hidden_lc").val(str);
		document.getElementById("hidden_lc").value=str;
		parent.emailwindow.hide();
		
	}
		
</script>
<?php
	$sql="select id,lc_number,lc_year,importer_id from com_btb_lc_master_details where importer_id=$company and status_active=1 $bank $lc_date  $supplier";
	//echo $sql;die; 
	$arr=array(2=>$company_library);
	echo  create_list_view("list_view2", "Lc Number,Year,Company", "150,60,135","395","320",0, $sql , "js_set_value", "id,lc_number", "", 1, "0,0,importer_id,", $arr , "lc_number,lc_year,importer_id", "","setFilterGrid('list_view2',-1)");
	echo "<input type='hidden' id='hidden_lc' />";
	exit();

}

if($action=="list_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$hide_lc_id=str_replace("'","",$hide_lc_id);
	$cbo_company_id=str_replace("'","",$cbo_company_id);
	$cbo_issue_banking=str_replace("'","",$cbo_issue_banking);
	$txt_supplier_name=str_replace("'","",$txt_supplier_name);
	$txt_supplier=str_replace("'","",$txt_supplier);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	$txt_lc_no=str_replace("'","",$txt_lc_no);
	//echo $txt_supplier_name;die;
	if($cbo_issue_banking!=0) $cbo_issue_banking="and issuing_bank_id=$cbo_issue_banking"; else $cbo_issue_banking="";
	if($txt_supplier!="") $txt_supplier="and supplier_id=$txt_supplier"; else $txt_supplier="";
	if($db_type==0)$txt_date_from=change_date_format($txt_date_from,"yyyy-mm-dd");
	if($db_type==0)$txt_date_to=change_date_format($txt_date_to,"yyyy-mm-dd");
	
	if($txt_date_from!="") $la_date="and lc_date between '$txt_date_from' and '$txt_date_to'"; else $la_date="";
	if($txt_lc_no!="") $lc_id="and lc_number='$txt_lc_no'"; else $lc_id="";
	//echo $la_date;die;
	
	$sql=("select id,lc_number,lc_year,supplier_id,importer_id,item_category_id,lc_value,issuing_bank_id,lc_type_id from com_btb_lc_master_details where importer_id=$cbo_company_id $cbo_issue_banking $txt_supplier $la_date $lc_id and status_active=1 and is_deleted=0");
	//echo $sql;die;
	$sql_lc=sql_select($sql);
	?>
<div style="width:900px">
    <fieldset style="width:100%;">
    	<table width="900" cellpadding="0" cellspacing="0" id="" rules="all" class="rpt_table">
        	<thead>
            	<tr>
                	<th width="40">Sl</th>
                    <th width="120">Lc No</th>
                    <th width="170">Supplier</th>
                    <th width="170">Bank</th>
                    <th width="120">Item Catg.</th>
                    <th width="90">LC Type</th>
                    <th width="90">LC Value</th>
                    <th>Charge/ Payment</th>
                </tr>
            </thead>
    	</table>
        <table width="900" cellpadding="0" cellspacing="0" id="tbl_lc_list" rules="all" class="rpt_table">
            <tbody>
            <?php
			$i=1;
			foreach($sql_lc as $row)
			{
				if ($i%2==0)
				$bgcolor="#E9F3FF";
				else
				$bgcolor="#FFFFFF";
			//echo "select $groupconcat(id) as id,entry_id,btb_lc_id,sum(amount) as total_amt from com_lc_charge where btb_lc_id=".$row[csf('id')]." group by entry_id";
					if($db_type==0)
					{
						$sql_pay=sql_select("select $groupconcat(id) as id,entry_id,btb_lc_id,sum(amount) as total_amt from com_lc_charge where btb_lc_id=".$row[csf('id')]." group by entry_id,btb_lc_id");
					}
					else
					{
						$sql_pay=sql_select("select LISTAGG(CAST(id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY id) as id,entry_id,btb_lc_id,sum(amount) as total_amt from com_lc_charge where btb_lc_id=".$row[csf('id')]." group by entry_id,btb_lc_id");
					}
					
				//var_dump($sql_pay);
				if(!empty($sql_pay))
				{
					foreach($sql_pay as $result)
					{
					
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>">
						<td width="40" align="center"><?php echo $i;  ?></td>
						<td width="120" id="lc_num_<?php echo $i;?>"><?php echo $row[csf("lc_number")]; ?></td>
						<td width="170" id="supplier_id_<?php echo $i;?>"><?php echo $supplier_arr[$row[csf("supplier_id")]]; ?></td>
						<td width="170" id="issue_bank_<?php echo $i;?>"><?php echo $bank_arr[$row[csf("issuing_bank_id")]]; ?></td>
						<td width="120" id="item_cat_<?php echo $i;?>"><?php echo $item_category[$row[csf("item_category_id")]]; ?></td>
						<td width="90" id="lc_type_<?php echo $i;?>"><?php echo $lc_type[$row[csf("lc_type_id")]]; ?><input type="hidden" id="lc_id_<?php echo $i; ?>" name="lc_id_<?php echo $i; ?>" value="<?php echo $row[csf("id")]; ?>" >
	</td>
						<td width="90" align="right" id="lc_val_<?php echo $i;?>"><?php echo number_format($row[csf("lc_value")],2); ?></td>
						<td align="center">
						<input type="text" id="txt_charge_<?php echo $i;?>" name="txt_charge_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px; font-weight:bold; font-size:10px;" placeholder="Browse" onClick="open_payment(<?php echo $i;?>)" value="<?php echo number_format($result[csf("total_amt")],2);   ?>" readonly />
						<input type="hidden" id="hidden_entry_id_<?php echo $i;?>" name="hidden_entry_id_<?php echo $i;?>" value="<?php  echo $result[csf("entry_id")]; ?>" >
						</td>
					</tr>
					<?php
					}
				}
				else
				{
				
					?>
                    <tr bgcolor="<?php echo $bgcolor; ?>">
                	<td width="40" align="center"><?php echo $i;  ?></td>
                    <td width="120" id="lc_num_<?php echo $i;?>"><?php echo $row[csf("lc_number")]; ?></td>
                    <td width="170" id="supplier_id_<?php echo $i;?>"><?php echo $supplier_arr[$row[csf("supplier_id")]]; ?></td>
                    <td width="170" id="issue_bank_<?php echo $i;?>"><?php echo $bank_arr[$row[csf("issuing_bank_id")]]; ?></td>
                    <td width="120" id="item_cat_<?php echo $i;?>"><?php echo $item_category[$row[csf("item_category_id")]]; ?></td>
                    <td width="90" id="lc_type_<?php echo $i;?>"><?php echo $lc_type[$row[csf("lc_type_id")]]; ?><input type="hidden" id="lc_id_<?php echo $i; ?>" name="lc_id_<?php echo $i; ?>" value="<?php echo $row[csf("id")]; ?>" >
</td>
                    <td width="90" align="right" id="lc_val_<?php echo $i;?>"><?php echo number_format($row[csf("lc_value")],2); ?></td>
                    <td align="center">
                    <input type="text" id="txt_charge_<?php echo $i;?>" name="txt_charge_<?php echo $i;?>" class="text_boxes_numeric" style="width:80px;" placeholder="Browse" onClick="open_payment(<?php echo $i;?>)" readonly />
                    <input type="hidden" id="hidden_entry_id_<?php echo $i;?>" name="hidden_entry_id_<?php echo $i;?>" >
                    </td>
                </tr>
                <?php
				}
				
				$i++;
			}
            ?>
            </tbody>
        </table>
    </fieldset>	
</div>
<!--<script type="text/javascript"></script>-->
    <?php
}

if($action=="charge_popup")
{
	$permission=$_SESSION['page_permission'];
	//echo $permission;die;
?>
<script>var permission='<?php echo $permission; ?>';</script>
    
    <?php
	//echo "yes";
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$hidden_entry_id=str_replace("'","",$hidden_entry_id);
	//echo $hidden_entry_id;die;
	//echo $lc_num."****";echo $supplier."****";echo $issue_bank."****";echo $item_cat."****";echo $lc_val."****";
	?>
<script>
function js_set_value(str)
{
	//alert(str);
	$('#hedden_value').val(str);
	
	parent.emailwindow.hide(); 
}

function add_factor_row( i) 
{	
	var chargefor=0;
	var row_num=$('#tbl_pay_head tbody tr').length;
	//alert(row_num);
	if (row_num!=i)
	{
		return false;
	}
		i++;
	
	if(i!=2)
	{
	 chargefor=$('#cbochargefor_'+(i-1)).val();
	}
	else
	{
		chargefor=$('#cbochargefor_1').val();
	}
	
	//alert(chargefor);

 
 	$("#tbl_pay_head tbody tr:last").clone().find("input,select").each(function() {
		$(this).attr({
		'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i; },
		'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i; },
		'value': function(_, value) { if(value=='+' || value=="-"|| value=="chargefor"){return value}else{return ''} }              
		});
		
	}).end().appendTo("#tbl_pay_head");
	
	
		var k=i-1;
		$('#incrementfactor_'+k).hide();
	  	$('#decrementfactor_'+k).hide();
		//$('#updateiddtls_'+i).val('');

	  
	  $('#incrementfactor_'+i).removeAttr("onClick").attr("onClick","add_factor_row("+i+");");
	  $('#decrementfactor_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+");");
	  $('#txtamount_'+i).removeAttr("onChange").attr("onChange","total_val("+i+");");
	  $('#cbochargefor_'+i).val(chargefor);
	
	  
	  
}

function fn_deletebreak_down_tr(rowNo ) 
{
	
	document.getElementById('total_pay').value =(document.getElementById('total_pay').value*1)-($('#txtamount_'+rowNo).val()*1);
	var numRow = $('#tbl_pay_head tbody tr').length;
	if(numRow==rowNo && rowNo!=1)
	{
		var k=rowNo-1;
		$('#incrementfactor_'+k).show();
		$('#decrementfactor_'+k).show();
		
		$('#tbl_pay_head tbody tr:last').remove();
	}
	else
		return false;
	
}


function total_val(j)
{
	var last_val=$('#txtamount_'+j).val();
	//alert(last_val);
	var last_total_val=0;
	last_total_val=document.getElementById('total_pay').value;
	document.getElementById('total_pay').value =(last_total_val*1)+(last_val*1);
}


function fnc_charge_payment( operation )
{
		if( form_validation('txt_pay_date','Pay Date')==false )
		{
			return;
		}
	    var row_num=$('#tbl_pay_head tbody tr').length;
		var btb_lc_id=$('#btb_lc_id').val();
		//var update_id=$('#update_id').val();
		var txt_pay_date=$('#txt_pay_date').val();
		var txt_entry_id=$('#txt_entry_id').val();
		var update_dts_id=$('#update_dts_id').val();
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			data_all=data_all+get_submitted_data_string('cboissuebanking_'+i+'*txtamount_'+i+'*cbochargefor_'+i,"../../../");
			
		}
		//alert(data_all);return;
		var data="action=save_update_delete_charge&operation="+operation+'&total_row='+row_num+data_all+'&btb_lc_id='+btb_lc_id+'&txt_pay_date='+txt_pay_date+'&txt_entry_id='+txt_entry_id+'&update_dts_id='+update_dts_id;//+'&update_id='+update_id
		freeze_window(operation);
		http.open("POST","lc_opening_payment_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_charge_payment_response;
}

function fnc_charge_payment_response()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
		alert(http.responseText);
		//release_freezing();
		if(reponse[0]==0)
		{
			show_msg(trim(reponse[0]));
			release_freezing();
		}
		else if(reponse[0]==1)
		{
			show_msg(trim(reponse[0]));
			$('#txt_entry_id').val(reponse[1]);
			release_freezing();

		}
		else if(reponse[0]==10 || reponse[0]==15)
		{
			show_msg(trim(reponse[0]));
			release_freezing();
			return;
		}
		show_list_view(reponse[1],'show_dtls_list_view','pay_list_view','lc_opening_payment_entry_controller','');
		set_button_status(0, permission, 'fnc_charge_payment',1,0);
		reset_form('lc_pay_1','','','','');
		$('#txt_entry_id').val(reponse[1]);
		release_freezing();
		//show_list_view(reponse[1],'show_dtls_list_view','pay_list_view','lc_opening_payment_entry_controller','');
			/*if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				parent.emailwindow.hide();
			}*/
	}
}

</script>
</head>
<body>
<div style="width:680px;">
	<?php echo load_freeze_divs ("../../../",$permission);  ?>
	<fieldset style="width:100%">
	<form id="lc_pay_1" autocomplete="off">
    <div id="top_part" style="margin-bottom:20px; margin-top:5px;">
    	<table width="650" cellpadding="0" cellspacing="0" id="" border="1" rules="all" class="rpt_table">
        	<thead>
            	<tr>
                	<th width="120">LC No</th>
                    <th width="140">Supplier</th>
                    <th width="180">Bank</th>
                    <th width="100">Item Catg.</th>
                    <th>LC Value</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td><?php echo $lc_num; ?></td>
                    <td><?php echo $supplier; ?></td>
                    <td><?php echo $issue_bank; ?></td>
                    <td><?php echo $item_cat; ?></td>
                    <td align="right"><?php echo $lc_val; ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div id="bottom_part">
    	<table width="450" cellpadding="0" cellspacing="0" id="" rules="all" border="0" class="" style="margin-bottom:20px;">
            <tr>
            	<td width="100">Pay Date:</td>
                <td width="130"><input type="date" id="txt_pay_date" name="txt_pay_date" class="datepicker" style="width:100px;" ></td>
                <td width="200">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="620" cellpadding="0" cellspacing="0" id="tbl_pay_head" rules="all" border="1" class="rpt_table" style="margin-bottom:20px;">
            <thead>
                <tr>
                    <th width="200">Pay Head</th>
                    <th width="200">Charge For</th>
                    <th width="150">Amount</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td align="center">
					<?php 
                    	echo create_drop_down( "cboissuebanking_1", 190, $commercial_head,"", 1, "--Select --", $selected, "","","88,89,90,91");
                    ?> 
                    </td>
                    <td align="center">
					<?php 
						//function create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index, $new_conn, $field_name )
                    	echo create_drop_down( "cbochargefor_1", 190, $lc_charge_arr,"", 1, "--Select --", $selected, "","");
                    ?> 
                    </td>
                    <td align="center">
                    <input type="text" id="txtamount_1" name="txtamount_1" style="width:140px;" class="text_boxes_numeric"  onChange="total_val(1)">
                    </td>
                    <td>
                    <input style="width:30px;" type="button" id="incrementfactor_1" name="incrementfactor_1"  class="formbutton" value="+" onClick="add_factor_row(1)"/>
                     <input style="width:30px;" type="button" id="decrementfactor_1" name="decrementfactor_1"  class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(1)"/>
                    </td>
                </tr>
            </tbody>
            <tfoot>
            	<tr>
                	<td>&nbsp;</td>
                    <td align="right">Total:</td>
                    <td align="center"><input type="text" id="total_pay" name="total_pay" style="width:140" class="text_boxes_numeric" value="" readonly></td>
                    <td>&nbsp;</td>
                </tr>
            </tfoot>
        </table>
        <table width="680" cellpadding="0" cellspacing="0" id="" rules="all" border="0" class="">
            <tr>
                <td align="center" colspan="3" valign="middle" class="button_container">
                <?php
				//load_submit_buttons( $permission, $sub_func, $is_update, $is_show_print, $refresh_function, $btn_id, $is_show_approve )
				 //echo load_submit_buttons( $permission, "fnc_charge_payment", 0,0 ,"",1) ;
					echo load_submit_buttons( $permission, "fnc_charge_payment", 0,0,"",0);
				 ?>
                <input type="hidden" id="txt_entry_id" name="txt_entry_id" value="<?php echo $hidden_entry_id;?>">
                
                <input type="hidden" id="update_dts_id" name="update_dts_id" value="">
                
                <input type="hidden" id="btb_lc_id" name="btb_lc_id" value="<?php echo $btb_lc_id; ?>">
                </td>
            </tr>
            <?php
			
			
			?>
            

        </table>
    </div>
    </form>
    <br>
    <div id="pay_list_view">
    <?php
	if($hidden_entry_id!="")
	{
		$sql = "select id,entry_id,pay_head_id,pay_date,change_for,amount from  com_lc_charge  where entry_id='$hidden_entry_id'"; 
		
		
		$sql_result=sql_select($sql);
		foreach($sql_result as $row)
		{
			$hidden_to_val+=$row[csf("amount")];
		}
		?>
		<table width="680" cellpadding="0" cellspacing="0" id="" rules="all" border="0" class="">
			<tr >
				<td>&nbsp;</td>
				<td align="center"  valign="middle" class="" onClick="js_set_value(document.getElementById('hidden_to_val').value+'_'+document.getElementById('txt_entry_id').value)">
				<input type="hidden" id="hedden_value">
				<input type="hidden" id="hidden_to_val" value="<?php  echo $hidden_to_val; ?>">
				<input type="button" id="btn_close" value="Close" class="formbutton" style="width:100px">
				</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<?php
		
		$arr=array(0=>$commercial_head,2=>$lc_charge_arr);
		//function create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path, $filter_grid_fnc, $fld_type_arr, $summary_flds, $check_box_all )
		echo create_list_view("list_view3", "Pay Head,Pay Date,Charge For,Amount","200,100,150,100","600","200",0, $sql, "get_php_form_data", "id", "'child_form_input_data','lc_opening_payment_entry_controller'", 1, "pay_head_id,0,change_for,0", $arr, "pay_head_id,pay_date,change_for,amount", "","setFilterGrid('list_view3',-1)",'0,0,0,2',"4,amount");	
	}
	?>
     </div>
    </fieldset>
</div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>   
    <?php
	
}

if($action=="save_update_delete_charge")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$total_row=str_replace("'","",$total_row);
	$btb_lc_id=str_replace("'","",$btb_lc_id);
	$txt_pay_date=str_replace("'","",$txt_pay_date);
	if($db_type==0)$txt_pay_date= change_date_format($txt_pay_date,"yyyy-mm-dd"); else if($db_type==1 ||$db_type==2) $txt_pay_date=date("j-M-Y",strtotime($txt_pay_date));
	$txt_entry_id=str_replace("'","",$txt_entry_id);
	$update_dts_id=str_replace("'","",$update_dts_id);
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		//echo $btb_lc_id."xx";die;

		if($txt_entry_id!="")
		{
			$id=return_next_id( "id", "com_lc_charge", 1 ) ;
			$entry_id=$txt_entry_id ;
			if($update_id=="")
			{
				$field_array="id,entry_id,btb_lc_id,pay_date,pay_head_id,change_for,amount,inserted_by,insert_date";
				
				$data_array="";
				for($i=1;$i<=$total_row;$i++)
				{
					if($i!=1)$id=$id+1;
					$pay_head="cboissuebanking_".$i;
					$txtamount="txtamount_".$i;
					$change_for="cbochargefor_".$i;
					if ($i!=1) $data_array .=",";
					$data_array	.="(".$id.",".$entry_id.",'".$btb_lc_id."','".$txt_pay_date."',".str_replace("'","",$$pay_head).",".str_replace("'","",$$change_for).",".str_replace("'","",$$txtamount).",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				}
				
				$rID=sql_insert("com_lc_charge",$field_array,$data_array,1);
			
			}
		}
		else
		{
		
			//if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}	
			$id=return_next_id( "id", "com_lc_charge", 1 ) ;
			$entry_id=return_next_id( "entry_id", "com_lc_charge", 1 ) ;
			if($update_id=="")
			{
				$field_array="id,entry_id,btb_lc_id,pay_date,pay_head_id,change_for,amount,inserted_by,insert_date";
				
				$data_array="";
				for($i=1;$i<=$total_row;$i++)
				{
					if($i!=1)$id=$id+1;
					$pay_head="cboissuebanking_".$i;
					$txtamount="txtamount_".$i;
					$change_for="cbochargefor_".$i;
					if ($i!=1) $data_array .=",";
					$data_array	.="(".$id.",".$entry_id.",'".$btb_lc_id."','".$txt_pay_date."',".str_replace("'","",$$pay_head).",".str_replace("'","",$$change_for).",".str_replace("'","",$$txtamount).",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				}
			//echo $data_array;
			$rID=sql_insert("com_lc_charge",$field_array,$data_array,1);
			
			}
		}
		//echo $rID;die;
		
		//echo "insert into com_lc_charge($field_array) values $data_array";die;
		
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");
				echo "0**".$entry_id."**".str_replace("'",'',$id);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$entry_id."**".str_replace("'",'',$id);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);
				echo "0**".$entry_id."**".str_replace("'",'',$id);
			}
			else{
				oci_rollback($con); 
				echo "10**".$entry_id."**".str_replace("'",'',$id);
			}
		}
		disconnect($con);
		die;

	}
	
	if ($operation==1)  // Update Here
	{
		//echo $txt_entry_id;die;
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if($update_dts_id!="")
		{
			/*$update_id_array=sql_select("select id from com_lc_charge where entry_id=$txt_entry_id"); 
			$i=1;
			$field_up_array="pay_date*pay_head_id*change_for*amount*updated_by*update_date";
			foreach($update_id_array as $row)
			{
				$pay_head="cboissuebanking_".$i;
				$txtamount="txtamount_".$i;
				$change_for="cbochargefor_".$i;
				$id_arr[]=$row[csf("id")];
				$data_up_array[str_replace("'","",$row[csf("id")])]=explode("*",("'".$txt_pay_date."'*".$$pay_head."*".$$change_for."*".$$txtamount."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));
				$i++;
			}
		 $rID=execute_query(bulk_update_sql_statement( "com_lc_charge", "id", $field_up_array, $data_up_array, $id_arr ));
			
		}
		else if($update_dts_id!="")
		{*/
 		$field_array = "pay_date*pay_head_id*change_for*amount*updated_by*update_date";
 		$data_array = "'".$txt_pay_date."'*".$cboissuebanking_1."*".$cbochargefor_1."*".$txtamount_1."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
 		$rID= sql_update("com_lc_charge",$field_array,$data_array,"id",$update_dts_id,1); 
		}
		if($db_type==0)
		{
			if($rID )
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_entry_id)."**".str_replace("'",'',$update_dts_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_entry_id)."**".str_replace("'",'',$update_dts_id);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID )
			{
				oci_commit($con);  
				echo "1**".str_replace("'","",$txt_entry_id)."**".str_replace("'",'',$update_dts_id);
			}
			else
			{
				oci_rollback($con); 
				echo "10**".str_replace("'","",$txt_entry_id)."**".str_replace("'",'',$update_dts_id);
			}
		}
		disconnect($con);
		die;
		
	}

}

if($action=="show_dtls_list_view")
{
 	$sql = "select id,entry_id,pay_head_id,pay_date,change_for,amount from  com_lc_charge  where entry_id='$data'"; 
	//echo $sql;die;
	$sql_result=sql_select($sql);
	foreach($sql_result as $row)
	{
		$hidden_to_val+=$row[csf("amount")];
	}
	?>
    <table width="680" cellpadding="0" cellspacing="0" id="" rules="all" border="0" class="">
        <tr >
            <td>&nbsp;</td>
            <td align="center"  valign="middle" class="" onClick="js_set_value(document.getElementById('hidden_to_val').value+'_'+document.getElementById('txt_entry_id').value)">
            <input type="hidden" id="hedden_value">
            <input type="hidden" id="hidden_to_val" value="<?php  echo $hidden_to_val; ?>">
            <input type="button" id="btn_close" value="Close" class="formbutton" style="width:100px">
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <?php
	//echo $data;
	$arr=array(0=>$commercial_head,2=>$lc_charge_arr);
 	/*echo create_list_view("list_view", "Pay Head,Pay Date,Charge For,Amount","200,100,150,100","600","200",0, $sql, "get_php_form_data", "id", "'child_form_input_data','lc_opening_payment_entry_controller'", 1, "pay_head_id,0,change_for,0", $arr, "pay_head_id,pay_date,change_for,amount", "","setFilterGrid('list_view',-1)",'0,0,0,2',"4,amount");	
	exit();*/
	
	echo create_list_view("list_view4", "Pay Head,Pay Date,Charge For,Amount","200,100,150,100","600","200",0, $sql, "get_php_form_data", "id", "'child_form_input_data','lc_opening_payment_entry_controller'", 1, "pay_head_id,0,change_for,0", $arr, "pay_head_id,pay_date,change_for,amount", "","setFilterGrid('list_view4',-1)",'0,0,0,2',"4,amount");	
		
} 

if($action=="child_form_input_data")
{
	//echo $data;die;
	//$data = details table ID 
	
	$sql="select id,entry_id,pay_date,pay_head_id,change_for,amount from  com_lc_charge where id=$data"; 
	$result = sql_select($sql);
	
	foreach($result as $row)
	{
		//$pa_date=change_date_format($row[csf("pay_date")]);
		echo "$('#txt_pay_date').val('".$row[csf("pay_date")]."');\n";
		echo "$('#cboissuebanking_1').val(".$row[csf("pay_head_id")].");\n";
		echo "$('#cbochargefor_1').val(".$row[csf("change_for")].");\n";
		echo "$('#txtamount_1').val(".$row[csf("amount")].");\n";
		//update id here
		echo "$('#update_dts_id').val(".$row[csf("id")].");\n";	
		echo "$('#txt_entry_id').val(".$row[csf("entry_id")].");\n";		
		//echo "show_list_view(".$row[csf("wo_po_type")]."+'**'+".$row[csf("wo_pi_no")].",'show_product_listview','list_product_container','requires/yarn_receive_controller','');\n";
		echo "set_button_status(1, permission, 'fnc_charge_payment',1,1);\n";
	}
	exit();
}

?>