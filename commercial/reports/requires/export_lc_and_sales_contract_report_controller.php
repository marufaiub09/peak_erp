<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');


$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id=$_SESSION['logic_erp']['user_id'];
$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
$bank_arr=return_library_array("select id,bank_name from   lib_bank","id","bank_name");

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 110, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}

if ($action=="load_drop_down_applicant")
{
	$sql = "select a.id,a.buyer_name from lib_buyer a, lib_buyer_tag_company b where a.status_active =1 and a.is_deleted=0 and b.buyer_id=a.id and b.tag_company='$data' and a.id in (select  buyer_id from  lib_buyer_party_type where party_type in (22,23)) order by buyer_name";  
 	echo create_drop_down( "cbo_applicant", 110, $sql,"id,buyer_name", 1, "-- Select --", 0, "" );
	exit();
	
}


//Company Details
$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');


if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$txt_year=str_replace("'","",$txt_year);
	if($txt_year!="")
	{
		$year_lc="and lc_year='$txt_year'";
		$year_sc="and sc_year='$txt_year'";
	}
	else
	{
		$year_lc="";
		$year_sc="";
	}
	
	?>
                <div style="width:2850px;" align="left">
                <fieldset>
                <table width="2800" cellpadding="0" cellspacing="0" id="caption">
                    <tr>
                       <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:18px">Company Name:<?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
                    </tr> 
                    <tr>  
                       <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:18px"><?php echo $report_title; ?></strong></td>
                    </tr>  
                </table>
                <table width="2802" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="">
                    <thead>
                        <tr>
                            <th width="50">Sl</th>
                            <th width="100">Internal File No.</th>
                            <th width="100">Bank File No.</th>
                            <th width="80">Year</th>
                            <th width="80">Beneficiary</th>
                            <th width="100">Buyer</th>
                            <th width="100">Applicant</th>
                            <th width="80">SC/LC</th>
                            <th width="110">SC/LC No.</th>
                            <th width="75">Convertible Type</th>
                            <th width="100">SC/LC Date</th>
                            <th width="100">Last Ship Date</th>
                            <th width="120">SC Value(LC/SC, Finance)</th>
                            <th width="110">Rep. LC/SC</th>
                            <th width="110">Balance</th>
                            <th width="110">SC Value(Direct)</th>
                            <th width="110">LC Value(Direct)</th>
                            <th width="110">File Value</th>
                            <th width="100">Expiry Date</th>
                            <th width="110">Lien Bank</th>
                            <th width="110">Issuing Bank</th>
                            <th width="100">Pay Term</th>
                            <th width="100">Tenor</th>
                            <th width="100">Inco Term</th>
                            <th width="110">Transfering Bank</th>
                            <th width="110">Negotiating Bank</th>
                            <th width="110">Nominated Ship. Line</th>
                            <th>Re- Imbursing Bank</th>
                        </tr>
                    </thead>
                </table>
                
                <div style="width:2820px; overflow-y: scroll; overflow-x:hidden; max-height:250px;font-size:12px;" id="scroll_body">
                <table width="2802" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
                    <tbody>
					
                    <?php
					$cbo_company_name=str_replace("'","",$cbo_company_name);
					if($cbo_company_name!=0) $cbo_company_name = $cbo_company_name; else $cbo_company_name="%%";
					
					$cbo_lien_bank=str_replace("'","",$cbo_lien_bank);
					if($cbo_lien_bank == 0) $cbo_lien_bank="%%"; else $cbo_lien_bank = $cbo_lien_bank;
					
					$cbo_currency_name=str_replace("'","",$cbo_currency_name);
					if($cbo_currency_name == 0) $cbo_currency_name="%%"; else $cbo_currency_name = $cbo_currency_name;
					
					$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
					if($cbo_buyer_name == 0) $cbo_buyer_name="%%"; else $cbo_buyer_name = $cbo_buyer_name;
					
					$cbo_applicant=str_replace("'","",$cbo_applicant);
					if($cbo_applicant == 0) $cbo_applicant="%%"; else $cbo_applicant = $cbo_applicant;
					
					$txt_date_from=str_replace("'","",$txt_date_from);
					if(trim($txt_date_from)!= "") $txt_date_from  =$txt_date_from;
					
					$txt_date_to=str_replace("'","",$txt_date_to);
					if(trim($txt_date_to)!= "") $txt_date_to = $txt_date_to;
					//if(trim($data[7])!="") $cbo_year2=$data[7];,lc_date
					if ($txt_date_from!="" && $txt_date_to!="")
					{
						$str_cond=" and lc_date between '$txt_date_from' and  '$txt_date_to'";
						
						$str_cond_con=" and contract_date between '$txt_date_from' and  '$txt_date_to'";
					}
					else
					{
						$str_cond="";
						$str_cond_con="";
					}
					
					//com_export_lc = ,' ' as convertible_to_lc,issuing_bank_name,transfer_bank,negotiating_bank,re_imbursing_bank,replacement_lc
					//com_sales_contract= ,convertible_to_lc,'' as issuing_bank_name,'' as transfer_bank ,'' as negotiating_bank,'' as re_imbursing_bank,'' as replacement_lc
					
					/*$lc_sql=sql_select("select id, issuing_bank_name,transfer_bank,negotiating_bank,re_imbursing_bank,replacement_lc
					from  com_export_lc
					where  beneficiary_name like '$cbo_company_name' and lien_bank like '$cbo_lien_bank' and currency_name  like '$cbo_currency_name'and buyer_name like '$cbo_buyer_name' and applicant_name like '$cbo_applicant' and status_active=1 and is_deleted=0 $str_cond $year_lc");
					foreach($lc_sql as $row)
					{
						$lc_data_array[$row[csf("id")]]["id"]=$row[csf("id")];
						$lc_data_array[$row[csf("id")]]["issuing_bank_name"]=$row[csf("issuing_bank_name")];
						$lc_data_array[$row[csf("id")]]["transfer_bank"]=$row[csf("transfer_bank")];
						$lc_data_array[$row[csf("id")]]["negotiating_bank"]=$row[csf("negotiating_bank")];
						$lc_data_array[$row[csf("id")]]["re_imbursing_bank"]=$row[csf("re_imbursing_bank")];
						$lc_data_array[$row[csf("id")]]["replacement_lc"]=$row[csf("replacement_lc")];
					}
					
					$sc_sql=sql_select("select id,convertible_to_lc 
					from com_sales_contract
					where  beneficiary_name like '$cbo_company_name' and lien_bank like '$cbo_lien_bank' and currency_name  like '$cbo_currency_name' and buyer_name like '$cbo_buyer_name' and applicant_name like '$cbo_applicant' and status_active=1 and is_deleted=0 $str_cond_con $year_sc");
					foreach($sc_sql as $row)
					{
						$sc_data_array[$row[csf("id")]]["id"]=$row[csf("id")];
						$sc_data_array[$row[csf("id")]]["convertible_to_lc"]=$row[csf("convertible_to_lc")];
					}*/
					
					//echo $sc_sql;die;
					
					$sql="select id, internal_file_no,bank_file_no,lc_year as lc_sc_year,beneficiary_name,buyer_name,applicant_name,export_lc_no as lc_sc ,lc_date as lc_sc_date,last_shipment_date,lc_value as lc_sc_value, currency_name,expiry_date,lien_bank,pay_term,tenor,inco_term_place,nominated_shipp_line as ship_line, null as convertible_to_lc,issuing_bank_name,transfer_bank,negotiating_bank,re_imbursing_bank,replacement_lc,1 as type
					from  com_export_lc
					where  beneficiary_name like '$cbo_company_name' and lien_bank like '$cbo_lien_bank' and currency_name  like '$cbo_currency_name'and buyer_name like '$cbo_buyer_name' and applicant_name like '$cbo_applicant' and status_active=1 and is_deleted=0 $str_cond $year_lc 
					
					UNION ALL
					
					select id,internal_file_no,bank_file_no,sc_year as lc_sc_year,beneficiary_name,buyer_name,applicant_name,contract_no as lc_sc,contract_date as lc_sc_date,last_shipment_date,contract_value as lc_sc_value,currency_name,expiry_date,lien_bank,pay_term,tenor,inco_term_place,shipping_line  as ship_line,convertible_to_lc, null as issuing_bank_name, null as transfer_bank , null as negotiating_bank, null as re_imbursing_bank, null as replacement_lc,2 as type 
					from com_sales_contract
					where  beneficiary_name like '$cbo_company_name' and lien_bank like '$cbo_lien_bank' and currency_name  like '$cbo_currency_name' and buyer_name like '$cbo_buyer_name' and applicant_name like '$cbo_applicant' and status_active=1 and is_deleted=0 $str_cond_con $year_sc order by internal_file_no";
					
					//echo $sql;
					$i= 1; $k=1; $item_group_array=array();$year=array();
					$sql_re=sql_select($sql);
					$grand_total_file_value=0;
					$grand_total_balance=0;
					$grand_sc_value_1_3=0;
					$grand_lc_value_1=0;
					$grand_sc_value_2=0;
					$grand_lc_value_0_1=0;
					//echo $to_ro=count($sql_re);
					foreach($sql_re as $row_result)
					{
						if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
						if(!in_array($row_result[csf('lc_sc_year')],$item_group_array[$row_result[csf('internal_file_no')]]))
						{
							if ($k!=1)
							{
								//$k1=$k-2;
								?>	
								<tr bgcolor="#CCCCCC">
									<td>&nbsp;</td><td><font color="#CCCCCC"><?php //echo $last_file22;  ?></font></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
									<td align="right"><b>Total</b></td>
									<td align="right"><p> <?php echo number_format($sc_value_1_3,2,'.',''); $grand_sc_value_1_3 +=$sc_value_1_3 ;?></p></td>
									<td align="right"><p> <?php echo number_format($lc_value_1,2,'.',''); $grand_lc_value_1 +=$lc_value_1 ;?></p></td>
									<td align="right"><p><?php echo number_format($balance_1_3_1,2,'.','');$grand_total_balance +=$balance_1_3_1 ; ?></p></td>
									<td align="right"><p><?php echo number_format($sc_value_2,2,'.',''); $grand_sc_value_2 +=$sc_value_2 ; ?></p></td>
								   <td align="right"> <p><?php echo number_format($lc_value_0_1,2,'.',''); $grand_lc_value_0_1 +=$lc_value_0_1 ; ?></p></td>
									<td align="right"><p><?php echo number_format($file_value,2,'.',''); $grand_total_file_value +=$file_value ; ?></p></td>
									<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
								</tr>
								<?php
								$sc_value_1_3 = 0;
								$lc_value_1 = 0;
								$balance_1_3_1 = 0;
								$sc_value_2 = 0;
								$lc_value_0_1 = 0;
								$file_value = 0;
							}
							$k++;
							$print_cond=0;
							$item_group_array[$row_result[csf('internal_file_no')]]['year']=$row_result[csf('lc_sc_year')];
							//$item_group_array['year']=(string)$row['lc_year'];
							//$year[$i]=(string)$row['lc_year'];
						}
						
						
						?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                            <td width="50" ><?php echo $i;?></td>
                            <td width="100"><p><?php echo  $row_result[csf('internal_file_no')];?></p></td>
                            <td  width="100"><p><?php echo $row_result[csf('bank_file_no')];?></p></td>
                            <td  width="80"><p><?php echo $row_result[csf('lc_sc_year')];?></p></td>
                            <td width="80"><p><?php echo $company_arr[$row_result[csf('beneficiary_name')]]; ?></p></td>
                            <td width="100">
                             <p><?php
							echo $buyer_arr[$row_result[csf('buyer_name')]];
							?></p>
                            </td>
                            <td width="100"><p><?php echo $buyer_arr[$row_result[csf('applicant_name')]];?><p></td>
                            
                            <td align="center" width="80"><p><?php if($row_result[csf('type')] == 1) echo "LC"; else echo "SC"; ?></p></td>
                            
                            <td width="110"><p><?php echo $row_result[csf('lc_sc')];?></p></td>
                            <td align="center" width="75"><p><?php echo $convertible_to_lc[$row_result[csf('convertible_to_lc')]];?></p></td>
                            <td align="center" width="100"><p><?php  if($row_result[csf('lc_sc_date')]!="0000-00-00") echo change_date_format( $row_result[csf('lc_sc_date')]);?></p></td>
                            <td align="center" width="100"><p><?php if($row_result[csf('last_shipment_date')]!="0000-00-00") echo change_date_format( $row_result[csf('last_shipment_date')]);?></p></td>
                            
                            <td align="right" width="120"><p><?php if($row_result[csf('type')] == 2 && ($row_result[csf('convertible_to_lc')] ==1 || $row_result[csf('convertible_to_lc')] ==3) ) {$sc_1_3=$row_result[csf('lc_sc_value')]; echo number_format($sc_1_3 ,2);}?><p></td>
                            
                            <td align="right" width="110"><p><?php if($row_result[csf('replacement_lc')] == 1 && $row_result[csf('type')] == 1 ){ $lc_1=$row_result[csf('lc_sc_value')]; echo number_format($lc_1 ,2);} ?></p></td>
                            <td align="right" width="110"><p></p></td>
                            <td align="right" width="110"><p><?php if($row_result[csf('type')] == 2 && $row_result[csf('convertible_to_lc')] ==2){$sc_2=$row_result[csf('lc_sc_value')]; echo number_format($sc_2 ,2); }?></p></td>
                            <td align="right" width="110"><p><?php if($row_result[csf('replacement_lc')] == 2 && $row_result[csf('type')] == 1 ){ $lc_0_1=$row_result[csf('lc_sc_value')] ; echo number_format($lc_0_1,2);} ?></p></td>
                            <td align="right" width="110"><p></p></td>
                            <td align="center" width="100"><p><?php if($row_result[csf('ex_factory_date')]!="0000-00-00") echo change_date_format( $row_result[csf('expiry_date')]);?></p></td>
                            <td width="110"><p>
							<?php
							 
							 echo $bank_arr[$row_result[csf('lien_bank')]];
							 ?></p>
                             </td>
                            <td width="110"><p><?php if($row_result[csf('type')]==1) echo $row_result[csf("issuing_bank_name")];?></p></td>
                            <td width="100"><p><?php echo $pay_term[$row_result[csf('pay_term')]];?></p></td>
                            <td width="100"><p><?php echo $row_result[csf('tenor')];?></p></td>
                            <td width="100"><p><?php echo $row_result[csf('inco_term_place')];?></p></td>
                            <td width="110"><p><?php echo $row_result[csf('transfer_bank')];?></p></td>
                            <td width="110"><p><?php echo $row_result[csf('negotiating_bank')];?></p></td>
                            <td width="110"><P><?php echo $row_result[csf('nominated_shipp_line')];?></P></td>
                            <td><p><?php echo $row_result[csf('re_imbursing_bank')];?></p></td>
                        </tr>
						<?php
                        $i++;
                        if($row_result[csf('type')] == 2 && $row_result[csf('convertible_to_lc')] ==1 || $row_result[csf('convertible_to_lc')] ==3 ) $sc_value_1_3 += $row_result[csf('lc_sc_value')];
                         if($row_result[csf('replacement_lc')] == 1 && $row_result[csf('type')] == 1 )$lc_value_1 += $row_result[csf('lc_sc_value')]; 
                         
                         $balance_1_3_1=$sc_value_1_3-$lc_value_1;
                         //$grand_total_balance +=$balance_1_3_1;
                         
                         if($row_result[csf('type')] == 2 && $row_result[csf('convertible_to_lc')] ==2) $sc_value_2 += $row_result[csf('lc_sc_value')];
                         if($row_result[csf('replacement_lc')] == 2 && $row_result[csf('type')] == 1 )$lc_value_0_1 += $row_result[csf('lc_sc_value')];
                        $file_value=($lc_value_1+ $balance_1_3_1+$sc_value_2+$lc_value_0_1);
                    }
                    ?>
                    <tr bgcolor="#CCCCCC">
                        <td>&nbsp;</td><td><font color="#CCCCCC"><?php //echo $last_file22;  ?></font></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                        <td align="right"><b>Total</b></td>
                        <td align="right"><p> <?php echo number_format($sc_value_1_3,2,'.',''); $grand_sc_value_1_3 +=$sc_value_1_3 ;?></p></td>
                        <td align="right"><p> <?php echo number_format($lc_value_1,2,'.',''); $grand_lc_value_1 +=$lc_value_1 ;?></p></td>
                        <td align="right"><p><?php echo number_format($balance_1_3_1,2,'.','');$grand_total_balance +=$balance_1_3_1 ; ?></p></td>
                        <td align="right"><p><?php echo number_format($sc_value_2,2,'.',''); $grand_sc_value_2 +=$sc_value_2 ; ?></p></td>
                        <td align="right"> <p><?php echo number_format($lc_value_0_1,2,'.',''); $grand_lc_value_0_1 +=$lc_value_0_1 ; ?></p></td>
                        <td align="right"><p><?php echo number_format($file_value,2,'.',''); $grand_total_file_value +=$file_value ; ?></p></td>
                        <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                </div>
                    <table width="2802" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="report_table_footer">
                    <tfoot>
                    <tr>
                    <th colspan="12" align="right"><b>Grand Total:</b></th>
                    <th width="120" align="right" id="grand_sc_value_1_3"><?php echo number_format($grand_sc_value_1_3,2,'.',',');  ?></th>
                    <th width="110" align="right" id="grand_lc_value_1"><?php echo number_format($grand_lc_value_1,2,'.',',');  ?></th>
                    <th width="110" align="right" id="grand_total_balance"><?php echo number_format($grand_total_balance,2,'.',',');  ?></th>
                    <th width="110"  align="right" id="grand_sc_value_2"><?php echo number_format($grand_sc_value_2,2,'.',',');  ?></th>
                    <th width="110" align="right" id="grand_lc_value_0_1"><?php echo number_format($grand_lc_value_0_1,2,'.',',');  ?></th>
                    <th width="110" align="right" id="grand_total_file_value"><?php echo  number_format($grand_total_file_value,2,'.',','); ?></th>
                    <th width="100">&nbsp;</th>
                    <th width="110">&nbsp;</th>
                    <th width="110">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="110">&nbsp;</th>
                    <th width="110">&nbsp;</th>
                    <th width="110">&nbsp;</th>
                    <th width="78">&nbsp;</th>
                    </tr>
                    
                    </tfoot>
                    </table>
                </fieldset>
                </div>
<?php

	exit();
}
disconnect($con);
?>