-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_employee_aman`
--

DROP TABLE IF EXISTS `lib_employee_aman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_employee_aman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `middle_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `full_name_bangla` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `id_card_no` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `punch_card_no` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `religion` int(11) DEFAULT '0',
  `blood_group` int(11) DEFAULT '0',
  `marital_status` int(11) DEFAULT '0' COMMENT '0=single; 1=married; 2=separated; 3=widow',
  `sex` int(11) DEFAULT '0' COMMENT '0=male; 1=female',
  `nationality` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `national_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation_id` int(11) DEFAULT '0',
  `designation_level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `joining_date` date DEFAULT NULL,
  `category` int(11) DEFAULT '2' COMMENT '0=top; 1=mid; 2=non',
  `remark` text COLLATE utf8_unicode_ci,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `company_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `division_id` int(11) NOT NULL DEFAULT '0',
  `department_id` int(11) NOT NULL DEFAULT '0',
  `section_id` int(11) NOT NULL DEFAULT '0',
  `subsection_id` int(11) NOT NULL DEFAULT '0',
  `line_no` int(11) NOT NULL DEFAULT '0',
  `Contract_start_date` date NOT NULL,
  `Contract_end_date` date NOT NULL,
  `inserted_by` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insert_date` int(11) DEFAULT '0',
  `updated_by` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_date` int(11) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `emp_code` (`emp_code`),
  KEY `id_card_no` (`id_card_no`),
  KEY `punch_card_no` (`punch_card_no`),
  KEY `designation_id` (`designation_id`),
  KEY `designation_level` (`designation_level`),
  KEY `joining_date` (`joining_date`),
  KEY `category` (`category`),
  KEY `company_id` (`company_id`),
  KEY `location_id` (`location_id`),
  KEY `division_id` (`division_id`),
  KEY `department_id` (`department_id`),
  KEY `section_id` (`section_id`),
  KEY `subsection_id` (`subsection_id`),
  KEY `status_active` (`status_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_employee_aman`
--

LOCK TABLES `lib_employee_aman` WRITE;
/*!40000 ALTER TABLE `lib_employee_aman` DISABLE KEYS */;
/*!40000 ALTER TABLE `lib_employee_aman` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:41
