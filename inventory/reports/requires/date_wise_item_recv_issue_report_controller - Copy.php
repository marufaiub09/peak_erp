﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if($db_type==0)
{
	$group_concat="group_concat";
	$select_year="year";
	$year_con="";
}
else
{
	$group_concat="wm_concat";
	$select_year="to_char";
	$year_con=",'YYYY'";
}


//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------

$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
$buyer_name_arr=return_library_array( "select id, buyer_name from  lib_buyer",'id','buyer_name');
$buyer_short_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$color_arr=return_library_array( "select id, color_name from  lib_color",'id','color_name');
$size_arr=return_library_array( "select id, size_name from  lib_size",'id','size_name');
$group_arr=return_library_array( "select id, item_name from  lib_item_group",'id','item_name');
$batch_arr=return_library_array( "select id, batch_no from pro_batch_create_mst",'id','batch_no');
$yarn_count_arr=return_library_array( "select id, yarn_count from  lib_yarn_count",'id','yarn_count');
$composition_arr=array();
$construction_arr=array();
$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
$data_array=sql_select($sql_deter);
if(count($data_array)>0)
{
	
/*foreach( $data_array as $row )
{
	if(array_key_exists($row[csf('id')],$composition_arr))
	{
		$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]].$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
	}
	else
	{
		$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
	}
}*/

	foreach( $data_array as $row )
	{
		if(array_key_exists($row[csf('id')],$construction_arr))
		{
			$construction_arr[$row[csf('id')]]=$construction_arr[$row[csf('id')]];
		}
		else
		{
			$construction_arr[$row[csf('id')]]=$row[csf('construction')];
		}
		if(array_key_exists($row[csf('id')],$composition_arr))
		{
			$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]];
		}
		else
		{
			$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
		}
	}
}

//load drop down Buyer
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 132, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}




//style search------------------------------//
if($action=="style_refarence_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	//echo $style_id;die;

	?>
    <script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		var selected_no = new Array;
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
			//alert(strCon);
				var splitSTR = strCon.split("_");
				var str = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );
					selected_no.push( str );				
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 );
					selected_no.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = ''; var num='';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ',';
					num += selected_no[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				num 	= num.substr( 0, num.length - 1 );
				//alert(num);
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
				$('#txt_selected_no').val( num );
		}
		
		
		
    </script>
    <?php
	$buyer=str_replace("'","",$buyer);
	$company=str_replace("'","",$company);
	if($buyer!=0) $buyer_cond="and buyer_name=$buyer"; else $buyer_cond="";
	$sql = "select id,style_ref_no,job_no,job_no_prefix_num,$select_year(insert_date $year_con) as year from wo_po_details_master where company_name=$company $buyer_cond  and is_deleted=0 order by job_no_prefix_num"; 
	//echo $sql; die;
	echo create_list_view("list_view", "Style Ref No,Job No,Year","160,90,100","400","310",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "0", $arr, "style_ref_no,job_no_prefix_num,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	echo "<input type='hidden' id='txt_selected_no' />";
	
	?>
    <script language="javascript" type="text/javascript">
	var style_no='<?php echo $txt_style_ref_no;?>';
	var style_id='<?php echo $txt_style_ref_id;?>';
	var style_des='<?php echo $txt_style_ref;?>';
	//alert(style_id);
	if(style_no!="")
	{
		style_no_arr=style_no.split(",");
		style_id_arr=style_id.split(",");
		style_des_arr=style_des.split(",");
		var str_ref="";
		for(var k=0;k<style_no_arr.length; k++)
		{
			str_ref=style_no_arr[k]+'_'+style_id_arr[k]+'_'+style_des_arr[k];
			js_set_value(str_ref);
		}
	}
	</script>
    
    <?php
	
	exit();
}


//order search------------------------------//
if($action=="order_surch")
{
	extract($_REQUEST);
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	?>
    <script>
		
		var selected_id = new Array;
		var selected_name = new Array;
		var selected_no = new Array;
    	function check_all_data() {
			var tbl_row_count = document.getElementById( 'list_view' ).rows.length;
			tbl_row_count = tbl_row_count - 0;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				var onclickString = $('#tr_' + i).attr('onclick');
				var paramArr = onclickString.split("'");
				var functionParam = paramArr[1];
				js_set_value( functionParam );
				
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) { 
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( strCon ) 
		{
			//alert(strCon);
				var splitSTR = strCon.split("_");
				var str_or = splitSTR[0];
				var selectID = splitSTR[1];
				var selectDESC = splitSTR[2];
				//$('#txt_individual_id' + str).val(splitSTR[1]);
				//$('#txt_individual' + str).val('"'+splitSTR[2]+'"');
				
				toggle( document.getElementById( 'tr_' + str_or ), '#FFFFCC' );
				
				if( jQuery.inArray( selectID, selected_id ) == -1 ) {
					selected_id.push( selectID );
					selected_name.push( selectDESC );
					selected_no.push( str_or );				
				}
				else {
					for( var i = 0; i < selected_id.length; i++ ) {
						if( selected_id[i] == selectID ) break;
					}
					selected_id.splice( i, 1 );
					selected_name.splice( i, 1 );
					selected_no.splice( i, 1 ); 
				}
				var id = ''; var name = ''; var job = ''; var num='';
				for( var i = 0; i < selected_id.length; i++ ) {
					id += selected_id[i] + ',';
					name += selected_name[i] + ',';
					num += selected_no[i] + ','; 
				}
				id 		= id.substr( 0, id.length - 1 );
				name 	= name.substr( 0, name.length - 1 ); 
				num 	= num.substr( 0, num.length - 1 );
				//alert(num);
				$('#txt_selected_id').val( id );
				$('#txt_selected').val( name ); 
				$('#txt_selected_no').val( num );
		}
    </script>
    <?php
	$buyer=str_replace("'","",$buyer);
	$company=str_replace("'","",$company);
	$style_all=str_replace("'","",$style_all);
	
	if($buyer!=0) $buyer_cond="and b.buyer_name=$buyer"; else $buyer_cond="";
	if($style_all!="") $style_cond="and b.id in($style_all)"; else $style_cond="";
	$sql = "select a.id,a.po_number,a.job_no_mst,b.style_ref_no,b.job_no_prefix_num,$select_year(b.insert_date $year_con) as year from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and b.company_name=$company $buyer_cond  $style_cond and a.status_active=1"; 
	//echo $sql; die;
	echo create_list_view("list_view", "Order NO,Job No,Year,Style Ref No","150,80,70,150","500","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "po_number,job_no_prefix_num,year,style_ref_no", "","setFilterGrid('list_view',-1)","0","",1) ;	
	echo "<input type='hidden' id='txt_selected_id' />";
	echo "<input type='hidden' id='txt_selected' />";
	echo "<input type='hidden' id='txt_selected_no' />";
	
	?>
    <script language="javascript" type="text/javascript">
	var order_no='<?php echo $txt_order_id_no; ?>';
	var order_id='<?php echo $txt_order_id;?>';
	var order_des='<?php echo $txt_order;?>';
	//alert(style_id);
	if(order_no!="")
	{
		order_no_arr=order_no.split(",");
		order_id_arr=order_id.split(",");
		order_des_arr=order_des.split(",");
		var order_ref="";
		for(var k=0;k<order_no_arr.length; k++)
		{
			order_ref=order_no_arr[k]+'_'+order_id_arr[k]+'_'+order_des_arr[k];
			js_set_value(order_ref);
		}
	}
	</script>
    
    <?php
	
	exit();
}



//report generated here--------------------//
if($action=="generate_report")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$cbo_item_cat=str_replace("'","",$cbo_item_cat);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	$txt_style_ref=str_replace("'","",$txt_style_ref);
	$txt_style_ref_id=str_replace("'","",$txt_style_ref_id);
	$txt_order=str_replace("'","",$txt_order);
	$txt_order_id=str_replace("'","",$txt_order_id);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	$receive_num_arr=return_library_array( "select id, recv_number from  inv_receive_master",'id','recv_number');
	$issue_num_arr=return_library_array( "select id, issue_number from  inv_issue_master",'id','issue_number');
	
	//echo $cbo_item_cat;die;
	if($cbo_item_cat==4)
	{
		if($txt_style_ref!="")
		{
			if($txt_style_ref_id!="")
			{
				$txt_style_ref_id="and d.id in($txt_style_ref_id)";
			}
			else
			{
				$txt_style_ref_id="and d.job_no_prefix_num like'%$txt_style_ref'"; 
	
			}
		}
		else
		{
			 $txt_style_ref_id="";
		}
		if($txt_order!="") 
		{
			if($txt_order_id!="")
			{
				$txt_order_id="and c.id in($txt_order_id)";
			}
			else
			{
				$txt_order_id="and c.po_number='$txt_order'";
			}
		}
		else
		{
			$txt_order_id="";
		}
		//print $company; $txt_pay_date=date("j-M-Y",strtotime($txt_pay_date));
		if($cbo_buyer_name!=0) $cbo_buyer_name="and d.buyer_name=$cbo_buyer_name"; else $cbo_buyer_name="";
		if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $date_cond=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $date_cond="";
		}
		
		//echo $date_cond;die;
		
		
					
		$sql="select 
					a.id as trans_id,
					b.po_breakdown_id as order_id,
					a.transaction_date,
					case when b.trans_type in(1,4) then b.quantity else 0 end as receive_qty,
					case when b.trans_type in(2,3) then b.quantity else 0 end as issue_qty,
					a.cons_uom,
					d.job_no,
					d.job_no_prefix_num,
					$select_year(d.insert_date $year_con) as job_year,
					d.buyer_name,
					c.po_number,
					c.shipment_date,
					b.prod_id,
					a.transaction_type,
					a.mst_id
				from
					inv_transaction a, order_wise_pro_details b, wo_po_break_down c,wo_po_details_master d
				where
					a.id=b.trans_id and b.po_breakdown_id=c.id and c.job_no_mst=d.job_no and a.item_category=4 and d.company_name=$cbo_company_name $cbo_buyer_name $txt_style_ref_id $txt_order_id $date_cond  and a.status_active=1 and a.is_deleted=0  order by a.transaction_date, a.id";
		//echo $sql;die;	
		$sql_result=sql_select($sql);
		$trms_rasult_arr=array();
		$order_id="";
		foreach($sql_result as $row)
		{
			if($order_id=="") $order_id=$row[csf("order_id")]; else $order_id=$order_id.",".$row[csf("order_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["trans_id"]=$row[csf("trans_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["order_id"]=$row[csf("order_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["prod_id"]=$row[csf("prod_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["transaction_type"]=$row[csf("transaction_type")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["rec_issue_id"]=$row[csf("mst_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["transaction_date"]=$row[csf("transaction_date")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_no"]=$row[csf("job_no")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_no_prefix_num"]=$row[csf("job_no_prefix_num")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_year"]=$row[csf("job_year")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["buyer_name"]=$row[csf("buyer_name")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["po_number"]=$row[csf("po_number")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["shipment_date"]=$row[csf("shipment_date")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["receive_qty"]+=$row[csf("receive_qty")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["issue_qty"]+=$row[csf("issue_qty")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["cons_uom"]=$row[csf("cons_uom")];
		}
		
		
		//var_dump($order_id);die;
		if(empty($order_id))  $order_id=0;
		$sql_prod="select
							a.id as product_id,
							b.trans_id,
							b.po_breakdown_id,
							a.item_description,
							a.product_name_details,
							a.color,
							a.item_color,
							a.gmts_size,
							a.item_size,
							a.item_group_id
							
					from
							 product_details_master a, order_wise_pro_details b
					where
							a.id=b.prod_id and b.po_breakdown_id in($order_id) order by b.trans_id";
		
		//echo $sql_prod;die;
		$product_result_arr=array();
		$result=sql_select($sql_prod);
		foreach($result as $row)
		{
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_id"]=$row[csf("product_id")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["item_group_id"]=$row[csf("item_group_id")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["grope_name"]=$row[csf("item_description")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_name_details"]=$row[csf("product_name_details")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["color"]=$row[csf("color")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["gmts_size"]=$row[csf("gmts_size")];
		}
		
		//var_dump($product_result_arr);die;
			ob_start();	
			?>
			
			<div style="width:1420px"> 
        <fieldset style="width:1410px;">
				<table width="1400" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="14" align="center" style="border:none;font-size:16px; font-weight:bold" >Date Wise Item Receive Issue Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="14" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				<table width="1400" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="50" >Prod. Id</th>
							<th width="80" >Trans. Date</th>
							<th width="120" >Trans. Ref.</th>
							<th width="60" >Year</th>
							<th width="50" >Job No</th>
							<th width="90">Buyer</th>
							<th width="120">Order No</th>
							<th width="80">Ship Date</th>
							<th width="100">Group Name</th>
							<th width="220">Description</th>
							<th width="80">RMG Color</th>
							<th width="80">RMG Size</th>
							<th width="80">Receive Qty</th>
							<th width="80">Issue Qty</th>
							<th>UOM</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1418px; overflow-y: scroll; max-height:250px;" id="scroll_body">
				<table width="1400" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
					<tbody>
					<?php
					$i=1;
					foreach($trms_rasult_arr as $trans_key=>$value)
					{
						foreach($value as $order_key=>$val)
						{
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";

							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
								<td width="50"><p><?php echo $val["prod_id"]; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><?php if($val["transaction_date"]!="0000-00-00") echo change_date_format($val["transaction_date"]); else echo ""; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p>
								<?php
									if($val["transaction_type"]==1 || $val["transaction_type"]==4) 
									{
										echo $receive_num_arr[$val['rec_issue_id']];
									}
									else if($val["transaction_type"]==2 || $val["transaction_type"]==3)
									{
										echo $issue_num_arr[$val['rec_issue_id']];
									}
	
								?>&nbsp;
								</p></td>
								<td width="60"  align="center"><p><?php echo $val["job_year"]; ?>&nbsp;</p></td>
								<td width="50"  align="center"><p><?php echo $val["job_no_prefix_num"]; ?>&nbsp;</p></td>
								<td width="90"><p><?php echo $buyer_short_arr[$val["buyer_name"]]; ?>&nbsp;</p></td>
								<td width="120" align="center"><p><?php echo $val["po_number"]; ?>&nbsp;</p></td>
								<td width="80"  align="center"><p><?php if($val["shipment_date"]!="0000-00-00") echo change_date_format($val["shipment_date"]); else echo ""; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo $group_arr[$product_result_arr[$trans_key][$order_key]["item_group_id"]]; ?>&nbsp;</p></td>
								<td width="220"><p><?php echo $product_result_arr[$trans_key][$order_key]["product_name_details"]; ?>&nbsp;</p></td>
								<td width="80"><p><?php echo $color_arr[$product_result_arr[$trans_key][$order_key]["color"]]; ?>&nbsp;</p></td>
								<td width="80"><p><?php echo $size_arr[$product_result_arr[$trans_key][$order_key]["gmts_size"]]; ?>&nbsp;</p></td>
								<td width="80" align="right"><p><?php echo number_format($val["receive_qty"],2); ?>&nbsp;</p></td>
								<td width="80" align="right"><p><?php echo number_format($val["issue_qty"],2); ?>&nbsp;</p></td>
								<td ><p><?php echo $unit_of_measurement[$val["cons_uom"]]; ?>&nbsp;</p></td>
							</tr>
							<?php
							$i++;
						}
					}
					?>   
					</tbody>
				</table>
				<!--<script language="javascript"> setFilterGrid('table_body',-1)</script> --> 
			 </div>
        </fieldset>
		</div>    
			<?php
	}
	
	
	else if($cbo_item_cat==2)
	{
		
		if($txt_style_ref!="")
		{
			if($txt_style_ref_id!="")
			{
				$txt_style_ref_id="and d.id in($txt_style_ref_id)";
			}
			else
			{
				$txt_style_ref_id="and d.job_no_prefix_num ='$txt_style_ref'"; 
	
			}
		}
		else
		{
			 $txt_style_ref_id="";
		}
		if($txt_order!="") 
		{
			if($txt_order_id!="")
			{
				$txt_order_id="and c.id in($txt_order_id)";
			}
			else
			{
				$txt_order_id="and c.po_number='$txt_order'";
			}
		}
		else
		{
			$txt_order_id="";
		}
	
		if($cbo_buyer_name!=0) $cbo_buyer_name="and d.buyer_name=$cbo_buyer_name"; else $cbo_buyer_name="";
		
		if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $date_cond=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $date_cond="";
		} 
		
		//echo $date_cond;die;
		
		
					
		$sql="select 
					a.id as trans_id,
					a.transaction_type,
					b.po_breakdown_id as order_id,
					a.transaction_date,
					case when b.trans_type in(1,4) then b.quantity else 0 end as receive_qty,
					case when b.trans_type in(2,3) then b.quantity else 0 end as issue_qty,
					a.cons_uom,
					d.job_no,
					d.job_no_prefix_num,
					$select_year(d.insert_date $year_con) as job_year,
					d.buyer_name,
					c.po_number,
					c.shipment_date,
					b.prod_id,
					a.mst_id
				from
					inv_transaction a, order_wise_pro_details b, wo_po_break_down c,wo_po_details_master d
				where
					a.id=b.trans_id and b.po_breakdown_id=c.id and c.job_no_mst=d.job_no and a.item_category=2 and d.company_name=$cbo_company_name $cbo_buyer_name $txt_style_ref_id $txt_order_id $date_cond and a.transaction_type in(1,2,3,4)  and a.status_active=1 and a.is_deleted=0  order by a.transaction_date, a.id";
		//echo $sql;die;	
		$sql_result=sql_select($sql);
		$trms_rasult_arr=array();
		$order_id="";$trans_rec_id="";$trans_issue_id="";
		foreach($sql_result as $row)
		{
			if($row[csf("transaction_type")]==1){ if($trans_rec_id=="") $trans_rec_id=$row[csf("trans_id")]; else   $trans_rec_id=$trans_rec_id.",".$row[csf("trans_id")];}
			if($row[csf("transaction_type")]==2){ if($trans_issue_id=="") $trans_issue_id=$row[csf("trans_id")]; else   $trans_issue_id=$trans_issue_id.",".$row[csf("trans_id")];}
			if($order_id=="") $order_id=$row[csf("order_id")]; else $order_id=$order_id.",".$row[csf("order_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["trans_id"]=$row[csf("trans_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["order_id"]=$row[csf("order_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["prod_id"]=$row[csf("prod_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["transaction_type"]=$row[csf("transaction_type")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["rec_issue_id"]=$row[csf("mst_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["transaction_date"]=$row[csf("transaction_date")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_no"]=$row[csf("job_no")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_no_prefix_num"]=$row[csf("job_no_prefix_num")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_year"]=$row[csf("job_year")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["buyer_name"]=$row[csf("buyer_name")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["po_number"]=$row[csf("po_number")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["shipment_date"]=$row[csf("shipment_date")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["receive_qty"]+=$row[csf("receive_qty")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["issue_qty"]+=$row[csf("issue_qty")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["cons_uom"]=$row[csf("cons_uom")];
		}
		
		
		//var_dump($order_id);die;
		if(empty($order_id))  $order_id=0;
		$sql_prod="select
							a.id as product_id,
							b.trans_id,
							b.po_breakdown_id,
							a.product_name_details,
							a.color,
							a.item_color,
							a.detarmination_id,
							a.gsm,
							a.dia_width
							
					from
							 product_details_master a, order_wise_pro_details b
					where
							a.id=b.prod_id and b.po_breakdown_id in($order_id) order by b.trans_id";
		
		//echo $sql_prod;die;
		$product_result_arr=array();
		$result=sql_select($sql_prod);
		foreach($result as $row)
		{
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_id"]=$row[csf("product_id")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_name_details"]=$row[csf("product_name_details")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["color"]=$row[csf("color")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["detarmination_id"]=$row[csf("detarmination_id")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["gsm"]=$row[csf("gsm")];
			$product_result_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["width"]=$row[csf("dia_width")];
		}
		
		
		if($trans_rec_id=="")$trans_rec_id=0;
		if($trans_issue_id=="")$trans_issue_id=0;
		$sql_batch_rec=sql_select("select a.trans_id,a.po_breakdown_id,b.batch_id from order_wise_pro_details a,pro_finish_fabric_rcv_dtls b  where a.trans_id=b.trans_id and  a.trans_id in($trans_rec_id)");
		
		foreach($sql_batch_rec as $row)
		{
			$batch_rec_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]=$row[csf("batch_id")];
		}
		$sql_batch_issue=sql_select("select a.trans_id,a.po_breakdown_id,b.batch_id from order_wise_pro_details a,inv_finish_fabric_issue_dtls b  where a.trans_id=b.trans_id and a.trans_id in($trans_issue_id)");
		foreach($sql_batch_issue as $row)
		{
			$batch_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]=$row[csf("batch_id")];
		}
		
		
		//var_dump($product_result_arr);die;
		
		
			ob_start();	
			?>
			
			<div style="width:1440px"> 
        <fieldset style="width:1440px;">
				<table width="1410" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="17" align="center" style="border:none;font-size:16px; font-weight:bold" >Date Wise Item Receive Issue Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="17" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				<table width="1410" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="50" >Prod. Id</th>
							<th width="60" >Year</th>
							<th width="50" >Job No</th>
							<th width="70">Buyer</th>
							<th width="120">Order No</th>
							<th width="80">Ship Date</th>
							<th width="80" >Trans. Date</th>
							<th width="120" >Trans. Ref.</th>
                            <th width="110">Construction</th>
							<th width="110">Composition</th>
							<th width="80">Color</th>
							<th width="80">GSM</th>
                            <th width="80">Dia</th>
							<th width="80">Receive Qty</th>
							<th width="80">Issue Qty</th>
							<th>Batch No</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1438px; overflow-y: scroll; max-height:250px;" id="scroll_body">
				<table width="1410" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
					<tbody>
					<?php
					$i=1;$total_receive="";$total_issue="";
					foreach($trms_rasult_arr as $trans_key=>$value)
					{
						foreach($value as $order_key=>$val)
						{
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";

							?>
							<tr  bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
								<td width="50"><p><?php echo $val["prod_id"]; ?>&nbsp;</p></td>
								<td width="60"  align="center"><p><?php echo $val["job_year"]; ?>&nbsp;</p></td>
								<td width="50"  align="center"><p><?php echo $val["job_no_prefix_num"]; ?>&nbsp;</p></td>
								<td width="70"><p><?php echo $buyer_short_arr[$val["buyer_name"]]; ?>&nbsp;</p></td>
								<td width="120" align="center"><p><?php echo $val["po_number"]; ?>&nbsp;</p></td>
								<td width="80"  align="center"><p><?php if($val["shipment_date"]!="0000-00-00") echo change_date_format($val["shipment_date"]); else echo ""; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><?php if($val["transaction_date"]!="0000-00-00") echo change_date_format($val["transaction_date"]); else echo ""; ?>&nbsp;</p></td>
								<td width="120"  align="center"><p>
								<?php
									if($val["transaction_type"]==1  || $val["transaction_type"]==4) 
									{
										echo $receive_num_arr[$val['rec_issue_id']];
									}
									else if($val["transaction_type"]==2  || $val["transaction_type"]==3)
									{
										echo $issue_num_arr[$val['rec_issue_id']];
									}
	
								?>&nbsp;
								</p></td>
								<td width="110"><p><?php echo $construction_arr[$product_result_arr[$trans_key][$order_key]["detarmination_id"]]; ?>&nbsp;</p></td>
								<td width="110"><p><?php echo $composition_arr[$product_result_arr[$trans_key][$order_key]["detarmination_id"]]; ?>&nbsp;</p></td>
								<td width="80"><p><?php echo $color_arr[$product_result_arr[$trans_key][$order_key]["color"]]; ?>&nbsp;</p></td>
								<td width="80"><p><?php echo $product_result_arr[$trans_key][$order_key]["gsm"]; ?>&nbsp;</p></td>
                                <td width="80"><p><?php echo $product_result_arr[$trans_key][$order_key]["width"]; ?>&nbsp;</p></td>
								<td width="80" align="right"><p><?php echo number_format($val["receive_qty"],2); $total_receive +=$val["receive_qty"]; ?>&nbsp;</p></td>
								<td width="80" align="right"><p><?php  echo number_format($val["issue_qty"],2); $total_issue +=$val["issue_qty"]; ?>&nbsp;</p></td>
								<td ><p>
								<?php 
									if($val["transaction_type"]==1) echo $batch_arr[$batch_rec_arr[$trans_key][$order_key]];
									if($val["transaction_type"]==2) echo $batch_arr[$batch_issue_arr[$trans_key][$order_key]];
								?>
                                &nbsp;</p></td>
							</tr>
							<?php
							$i++;
						}
					}
					?>   
					</tbody>
				</table>
			 </div>
				<table width="1410" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_footer" align="left">
                	<tfoot>
                    	<th colspan="14">Total:</th>
                        <th width="80" id="value_total_receive"><?php echo number_format($total_receive,2); ?></th>
                        <th width="80" id="value_total_issue"><?php echo number_format($total_issue,2); ?></th>
                        <th width="110"></th>
                    </tfoot> 
                </table>
        </fieldset>
		</div>    
			<?php	 
	
	
	}
	
	
	else if($cbo_item_cat==13)
	{
		
		
		if($txt_style_ref!="")
		{
			if($txt_style_ref_id!="")
			{
				$txt_style_ref_id="and d.id in($txt_style_ref_id)";
			}
			else
			{
				$txt_style_ref_id="and d.job_no_prefix_num ='$txt_style_ref'"; 
	
			}
		}
		else
		{
			 $txt_style_ref_id="";
		}
		if($txt_order!="") 
		{
			if($txt_order_id!="")
			{
				$txt_order_id="and c.id in($txt_order_id)";
			}
			else
			{
				$txt_order_id="and c.po_number='$txt_order'";
			}
		}
		else
		{
			$txt_order_id="";
		}
	
		if($cbo_buyer_name!=0) $cbo_buyer_name="and d.buyer_name=$cbo_buyer_name"; else $cbo_buyer_name="";
		
		if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $date_cond=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $date_cond="";
		}  
		
		//echo $date_cond;die;
		
		
					
		$sql="select 
					a.id as trans_id,
					a.transaction_type,
					a.mst_id,
					b.po_breakdown_id as order_id,
					a.transaction_date,
					case when b.trans_type in(1,4) then b.quantity else 0 end as receive_qty,
					case when b.trans_type in(2,3) then b.quantity else 0 end as issue_qty,
					a.cons_uom,
					d.job_no,
					d.job_no_prefix_num,
					$select_year(d.insert_date $year_con) as job_year,
					d.buyer_name,
					c.po_number,
					c.shipment_date,
					b.prod_id
				from
					inv_transaction a, order_wise_pro_details b, wo_po_break_down c,wo_po_details_master d
				where
					a.id=b.trans_id and b.po_breakdown_id=c.id and c.job_no_mst=d.job_no and a.item_category=13 and d.company_name=$cbo_company_name $cbo_buyer_name $txt_style_ref_id $txt_order_id $date_cond and a.transaction_type in(1,2,3,4)  and a.status_active=1 and a.is_deleted=0  order by a.transaction_date, a.id";
		//echo $sql;die;	
		$sql_result=sql_select($sql);
		$trms_rasult_arr=array();
		$order_id="";
		foreach($sql_result as $row)
		{
			if($order_id=="") $order_id=$row[csf("order_id")]; else $order_id=$order_id.",".$row[csf("order_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["trans_id"]=$row[csf("trans_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["order_id"]=$row[csf("order_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["prod_id"]=$row[csf("prod_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["transaction_type"]=$row[csf("transaction_type")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["rec_issue_id"]=$row[csf("mst_id")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["transaction_date"]=$row[csf("transaction_date")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_no"]=$row[csf("job_no")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_no_prefix_num"]=$row[csf("job_no_prefix_num")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["job_year"]=$row[csf("job_year")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["buyer_name"]=$row[csf("buyer_name")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["po_number"]=$row[csf("po_number")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["shipment_date"]=$row[csf("shipment_date")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["receive_qty"]+=$row[csf("receive_qty")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["issue_qty"]+=$row[csf("issue_qty")];
			$trms_rasult_arr[$row[csf("trans_id")]][$row[csf("order_id")]]["cons_uom"]=$row[csf("cons_uom")];
		}
		
		
		//var_dump($trms_rasult_arr);die;
		if(empty($order_id))  $order_id=0;
		$sql_prod="select
							a.id as product_id,
							b.trans_id,
							b.po_breakdown_id,
							a.product_name_details,
							a.color,
							a.item_color,
							a.detarmination_id,
							a.gsm,
							a.dia_width,
							c.color_id as dtls_color,
							c.stitch_length,
							c.yarn_lot,
							c.yarn_count
							
					from
							 product_details_master a, order_wise_pro_details b, pro_grey_prod_entry_dtls c
					where
							a.id=b.prod_id and b.trans_id=c.trans_id and b.trans_type in(1,4) and b.po_breakdown_id in($order_id) order by b.trans_id";
		
		//echo $sql_prod;die;
		$product_result_arr=array();
		$result=sql_select($sql_prod);
		foreach($result as $row)
		{
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_id"]=$row[csf("product_id")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_name_details"]=$row[csf("product_name_details")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["color"]=$row[csf("color")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["detarmination_id"]=$row[csf("detarmination_id")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["gsm"]=$row[csf("gsm")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["width"]=$row[csf("dia_width")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["dtls_color"]=$row[csf("dtls_color")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["stitch_length"]=$row[csf("stitch_length")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["yarn_lot"]=$row[csf("yarn_lot")];
			$product_receive_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["yarn_count"]=$row[csf("yarn_count")];
		}
		
		$stitch_sql=sql_select("select id,prod_id,rack,self,stitch_length from pro_grey_prod_entry_dtls where status_active=1 and is_deleted=0");
		$stitch_length=array();
		foreach($stitch_sql as $row)
		{
			$stitch_length[$row[csf("prod_id")]][$row[csf("rack")]][$row[csf("self")]]=$row[csf("stitch_length")];
		}
		$sql_prod="select
							a.id as product_id,
							b.trans_id,
							b.po_breakdown_id,
							a.product_name_details,
							a.color,
							a.item_color,
							a.detarmination_id,
							a.gsm,
							a.dia_width,
							c.color_id as dtls_color,
							c.yarn_lot,
							c.yarn_count,
							c.rack,
							c.self
							
							
					from
							 product_details_master a, order_wise_pro_details b, inv_grey_fabric_issue_dtls c
					where
							a.id=b.prod_id and b.trans_id=c.trans_id and b.trans_type in(2,3) and b.po_breakdown_id in($order_id) order by b.trans_id";
		
		//echo $sql_prod;die;
		$product_result_arr=array();
		$result=sql_select($sql_prod);
		foreach($result as $row)
		{
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_id"]=$row[csf("product_id")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["product_name_details"]=$row[csf("product_name_details")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["color"]=$row[csf("color")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["body_part_id"]=$row[csf("body_part_id")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["detarmination_id"]=$row[csf("detarmination_id")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["determin_id"]=$row[csf("determin_id")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["gsm"]=$row[csf("gsm")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["width"]=$row[csf("width")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["dtls_color"]=$row[csf("dtls_color")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["stitch_length"]=$stitch_length[$row[csf("product_id")]][$row[csf("rack")]][$row[csf("self")]];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["yarn_lot"]=$row[csf("yarn_lot")];
			$product_issue_arr[$row[csf("trans_id")]][$row[csf("po_breakdown_id")]]["yarn_count"]=$row[csf("yarn_count")];
		}
		
		//var_dump($product_issue_arr);die;
		
			ob_start();	
			?>
			
			<div style="width:1550px"> 
        <fieldset style="width:1550px;">
				<table width="1530" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="19" align="center" style="border:none;font-size:16px; font-weight:bold" >Date Wise Item Receive Issue Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="19" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				<table width="1530" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="50" >Prod. Id</th>
							<th width="60" >Year</th>
							<th width="50" >Job No</th>
							<th width="70">Buyer</th>
							<th width="120">Order No</th>
							<th width="80">Ship Date</th>
							<th width="80" >Trans. Date</th>
							<th width="130" >Trans. Ref.</th>
                            <th width="80" >Yarn Lot</th>
                            <th width="80" >Yarn Count</th>
                            <th width="100">Construction</th>
							<th width="110">Composition</th>
							<th width="80">Color</th>
							<th width="80">GSM</th>
                            <th width="80">Dia</th>
                            <th width="60">Stitch Lenth</th>
							<th width="80">Receive Qty</th>
							<th >Issue Qty</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1548px; overflow-y: scroll; max-height:250px;" id="scroll_body">
				<table width="1530" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
					<tbody>
					<?php
					$i=1;$total_receive="";$total_issue="";
					foreach($trms_rasult_arr as $trans_key=>$value)
					{
						foreach($value as $order_key=>$val)
						{
							if ($i%2==0)
							$bgcolor="#E9F3FF";
							else
							$bgcolor="#FFFFFF";
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
								<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
								<td width="50"><p><?php echo $val["prod_id"]; ?>&nbsp;</p></td>
								<td width="60"  align="center"><p><?php echo $val["job_year"]; ?>&nbsp;</p></td>
								<td width="50"  align="center"><p><?php echo $val["job_no_prefix_num"]; ?>&nbsp;</p></td>
								<td width="70"><p><?php echo $buyer_short_arr[$val["buyer_name"]]; ?>&nbsp;</p></td>
								<td width="120" align="center"><p><?php echo $val["po_number"]; ?>&nbsp;</p></td>
								<td width="80"  align="center"><p><?php if($val["shipment_date"]!="0000-00-00") echo change_date_format($val["shipment_date"]); else echo ""; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><?php if($val["transaction_date"]!="0000-00-00") echo change_date_format($val["transaction_date"]); else echo ""; ?>&nbsp;</p></td>
								<td width="130"  align="center"><p>
								<?php
									if($val["transaction_type"]==1  || $val["transaction_type"]==4) 
									{
										echo $receive_num_arr[$val['rec_issue_id']];
									}
									else if($val["transaction_type"]==2  || $val["transaction_type"]==3)
									{
										echo $issue_num_arr[$val['rec_issue_id']];
									}
	
								?>&nbsp;
								</p></td>
                                <?php
                                if($val["transaction_type"]==1  || $val["transaction_type"]==4) 
								{
									?>
									<td width="80" align="center"><p><?php echo $product_receive_arr[$trans_key][$order_key]["yarn_lot"]; ?>&nbsp;</p></td>
									<td width="80" align="center"><p><?php echo $yarn_count_arr[$product_receive_arr[$trans_key][$order_key]["yarn_count"]]; ?>&nbsp;</p></td>
									<td width="100"><p><?php echo $construction_arr[$product_receive_arr[$trans_key][$order_key]["detarmination_id"]]; ?>&nbsp;</p></td>
									<td width="110"><p><?php echo $composition_arr[$product_receive_arr[$trans_key][$order_key]["detarmination_id"]]; ?>&nbsp;</p></td>
									<td width="80"><p><?php echo $color_arr[$product_receive_arr[$trans_key][$order_key]["dtls_color"]]; ?>&nbsp;</p></td>
									<td width="80"><p><?php echo $product_receive_arr[$trans_key][$order_key]["gsm"]; ?>&nbsp;</p></td>
									<td width="80"><p><?php echo $product_receive_arr[$trans_key][$order_key]["width"]; ?>&nbsp;</p></td>
									<td width="60" ><p><?php echo $product_receive_arr[$trans_key][$order_key]["stitch_length"]; ?>&nbsp;</p></td>
									<?php
                                }
                                else if($val["transaction_type"]==2  || $val["transaction_type"]==3) 
								{
									?>
									<td width="80" align="center"><p><?php echo $product_issue_arr[$trans_key][$order_key]["yarn_lot"]; ?>&nbsp;</p></td>
									<td width="80" align="center"><p><?php echo $yarn_count_arr[$product_issue_arr[$trans_key][$order_key]["yarn_count"]]; ?>&nbsp;</p></td>
									<td width="100"><p><?php echo $construction_arr[$product_issue_arr[$trans_key][$order_key]["detarmination_id"]]; ?>&nbsp;</p></td>
									<td width="110"><p><?php echo $composition_arr[$product_issue_arr[$trans_key][$order_key]["detarmination_id"]]; ?>&nbsp;</p></td>
									<td width="80"><p><?php echo $color_arr[$product_issue_arr[$trans_key][$order_key]["dtls_color"]]; ?>&nbsp;</p></td>
									<td width="80"><p><?php echo $product_issue_arr[$trans_key][$order_key]["gsm"]; ?>&nbsp;</p></td>
									<td width="80"><p><?php echo $product_issue_arr[$trans_key][$order_key]["width"]; ?>&nbsp;</p></td>
									<td width="60" ><p><?php echo $product_issue_arr[$trans_key][$order_key]["stitch_length"]; ?>&nbsp;</p></td>
									<?php
                                }
                                ?>
                                
								<td width="80" align="right"><p><?php echo number_format($val["receive_qty"],2); $total_receive +=$val["receive_qty"]; ?>&nbsp;</p></td>
								<td align="right"><p><?php echo number_format($val["issue_qty"],2); $total_issue +=$val["issue_qty"]; ?>&nbsp;</p></td>
							</tr>
							<?php
							$i++;
						}
					}
					?>   
					</tbody>
				</table>
			 </div>
                <table width="1530" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_footer" align="left">
                    <tfoot>
                        <th colspan="17">Total:</th>
                        <th width="80" id="value_total_receive"><?php echo number_format($total_receive,2); ?></th>
                        <th width="88" id="value_total_issue"><?php echo number_format($total_issue,2); ?></th>
                    </tfoot> 
                </table>
        </fieldset>
		</div>    
			<?php	 
	
	}
	
	else if($cbo_item_cat==8 || $cbo_item_cat==9 || $cbo_item_cat==10 || $cbo_item_cat==11 || $cbo_item_cat==15 || $cbo_item_cat==16 || $cbo_item_cat==17 || $cbo_item_cat==18 || $cbo_item_cat==19 || $cbo_item_cat==20 || $cbo_item_cat==21 || $cbo_item_cat==22)
	{
		
		if($cbo_item_cat==8) $item_cond="and a.item_category=8";
		else if($cbo_item_cat==9) $item_cond="and a.item_category=9";
		else if($cbo_item_cat==10) $item_cond="and a.item_category=10";
		else if($cbo_item_cat==11) $item_cond="and a.item_category=11";
		else if($cbo_item_cat==15) $item_cond="and a.item_category=15";
		else if($cbo_item_cat==16) $item_cond="and a.item_category=16";
		else if($cbo_item_cat==17) $item_cond="and a.item_category=17";
		else if($cbo_item_cat==18) $item_cond="and a.item_category=18";
		else if($cbo_item_cat==19) $item_cond="and a.item_category=19";
		else if($cbo_item_cat==20) $item_cond="and a.item_category=20";
		else if($cbo_item_cat==21) $item_cond="and a.item_category=21";
		else if($cbo_item_cat==22) $item_cond="and a.item_category=22";
		else echo  $item_cond="";
		//echo $item_cond;die;
		
		if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $date_cond=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $date_cond="";
		}  
		
		//echo $date_cond;die;
		
		
					
		$sql="select 
					a.id as trans_id,
					a.transaction_type,
					a.mst_id as rec_issue_id,
					a.transaction_date,
					case when a.transaction_type in(1,4) then a.cons_quantity else 0 end as receive_qty,
					case when a.transaction_type in(2,3) then a.cons_quantity else 0 end as issue_qty,
					a.cons_uom,
					b.id as prod_id,
					b.item_group_id,
					b.sub_group_name,
					b.item_description,
					b.item_size
				from
					inv_transaction a, product_details_master b
				where
					a.prod_id=b.id and a.company_id=$cbo_company_name and a.transaction_type in(1,2,3,4)  $item_cond $date_cond
				order by a.transaction_date, a.id";
		//echo $sql;		
		$sql_result=sql_select($sql);
		ob_start();	
		?>
			
		<div style="width:950px"> 
        <fieldset style="width:950px;">
				<table width="930" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="10" align="center" style="border:none;font-size:16px; font-weight:bold" >Date Wise Item Receive Issue Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="10" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				<table width="930" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="70" >Prod. Id</th>
							<th width="100" >Trans. Date</th>
							<th width="130" >Trans. Ref.</th>
							<th width="100">Item Group</th>
							<th width="100">Item Sub-Group</th>
                            <th width="120">Item Description</th>
                            <th width="80">Size</th>
							<th width="100">Receive Qty</th>
							<th >Issue Qty</th>
						</tr>
					</thead>
			   </table> 
               <br />
			  <div style="width:948px; overflow-y: scroll; max-height:250px;" id="scroll_body">
				<table width="930" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
					<tbody>
					<?php
					$i=1;$total_receive="";$total_issue="";
					foreach($sql_result as $row)
					{
						if ($i%2==0)
						$bgcolor="#E9F3FF";
						else
						$bgcolor="#FFFFFF";
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
							<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
							<td width="70" align="center"><p><?php echo $row[csf("prod_id")]; ?>&nbsp;</p></td>
							<td width="100" align="center"><p><?php if($row[csf("transaction_date")]!="0000-00-00") echo change_date_format($row[csf("transaction_date")]); else echo ""; ?>&nbsp;</p></td>
							<td width="130"  align="center"><p>
							<?php
								if($row[csf("transaction_type")]==1 || $row[csf("transaction_type")]==4) 
								{
									echo $receive_num_arr[$row[csf('rec_issue_id')]];
								}
								else if($row[csf("transaction_type")]==2  || $row[csf("transaction_type")]==3)
								{
									echo $issue_num_arr[$row[csf('rec_issue_id')]];
								}

							?>&nbsp;
							</p>
                            </td>
                            <td width="100" ><p><?php echo $group_arr[$row[csf('item_group_id')]]; ?>&nbsp;</p></td>
                            <td width="100" ><p><?php echo $row[csf('sub_group_name')]; ?>&nbsp;</p></td>
                            <td width="120"><p><?php echo $row[csf('item_description')]; ?>&nbsp;</p></td>
                            <td width="80"><p><?php echo $row[csf('item_size')]; ?>&nbsp;</p></td>
							<td width="100" align="right"><p><?php echo number_format($row[csf("receive_qty")],2); $total_receive +=$row[csf("receive_qty")]; ?>&nbsp;</p></td>
							<td align="right"><p><?php echo number_format($row[csf("issue_qty")],2); $total_issue +=$row[csf("issue_qty")]; ?>&nbsp;</p></td>
						</tr>
						<?php
						$i++;
					}
					?>   
					</tbody>
				</table>
			 </div>
                <table width="930" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_footer" align="left">
                    <tfoot>
                        <th colspan="8">Total:</th>
                        <th width="100" id="value_total_receive"><?php echo number_format($total_receive,2); ?></th>
                        <th width="100" id="value_total_issue"><?php echo number_format($total_issue,2); ?></th>
                    </tfoot> 
                </table>
        </fieldset>
		</div>    
			<?php	 
	}
	else if($cbo_item_cat==1)
	{
		
		
		if($db_type==0)
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".change_date_format($txt_date_from,"yyyy-mm-dd")."' and '".change_date_format($txt_date_to,"yyyy-mm-dd")."' "; else $date_cond=""; 
		}
		else
		{
			if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.transaction_date between '".date("j-M-Y",strtotime($txt_date_from))."' and '".date("j-M-Y",strtotime($txt_date_to))."' "; else $date_cond="";
		}  
		
		//echo $date_cond;die;
		$sql="select
					b.id as prod_id, 
					a.id as trans_id,
					a.transaction_type,
					a.mst_id as rec_issue_id,
					a.transaction_date,
					case when a.transaction_type in(1,4) then a.cons_quantity else 0 end as receive_qty,
					case when a.transaction_type in(2,3) then a.cons_quantity else 0 end as issue_qty,
					a.cons_uom,
					((case when b.yarn_comp_type1st!=0 then b.yarn_comp_type1st end) || (case when b.yarn_comp_percent1st!=0 then b.yarn_comp_percent1st end)  || (case when b.yarn_comp_type2nd!=0 then b.yarn_comp_type2nd end) || (case when b.yarn_comp_percent2nd!=0 then b.yarn_comp_percent2nd end) ) as composition,
					b.lot,
					b.yarn_count_id,
					b.yarn_type,
					b.color
				from
					inv_transaction a, product_details_master b
				where
					a.prod_id=b.id and a.item_category=1 and a.company_id=$cbo_company_name and a.transaction_type in(1,2,3,4)  $date_cond   and a.status_active=1 and a.is_deleted=0  order by a.transaction_date, a.id";
		//echo $sql;	
		$sql_result=sql_select($sql);
		
		
		$sql_receive=sql_select("select id,receive_basis,booking_no,receive_purpose from inv_receive_master where company_id=$cbo_company_name");
		$receive_data_arr=array();
		foreach($sql_receive as $row)
		{
			$receive_data_arr[$row[csf("id")]]['rec_id']=$row[csf("id")];
			$receive_data_arr[$row[csf("id")]]['receive_basis']=$row[csf("receive_basis")];
			$receive_data_arr[$row[csf("id")]]['booking_no']=$row[csf("booking_no")];
			$receive_data_arr[$row[csf("id")]]['receive_purpose']=$row[csf("receive_purpose")];
		}
		
		$sql_issue=sql_select("select id,issue_basis,booking_no,issue_purpose from inv_issue_master where company_id=$cbo_company_name");
		$issue_data_arr=array();
		foreach($sql_issue as $row)
		{
			$issue_data_arr[$row[csf("id")]]['issue_id']=$row[csf("id")];
			$issue_data_arr[$row[csf("id")]]['issue_basis']=$row[csf("issue_basis")];
			$issue_data_arr[$row[csf("id")]]['booking_no']=$row[csf("booking_no")];
			$issue_data_arr[$row[csf("id")]]['issue_purpose']=$row[csf("issue_purpose")];
		}
		//var_dump($receive_data_arr);die;
		ob_start();	
		?>
		
		<div style="width:1350px"> 
        <fieldset style="width:1350px;">
				<table width="1330" border="1" cellpadding="2" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
						<tr class="form_caption" style="border:none;">
							<td colspan="14" align="center" style="border:none;font-size:16px; font-weight:bold" >Date Wise Item Receive Issue Report </td> 
						</tr>
						<tr style="border:none;">
								<td colspan="14" align="center" style="border:none; font-size:14px;">
									Company Name : <?php echo $company_arr[str_replace("'","",$cbo_company_name)]; ?>                                
								</td>
						</tr>
				   </table>
				   <br />
				<table width="1330" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_header_1" align="left"> 
					<thead>
						<tr>
							<th width="30" >SL</th>
							<th width="70" >Prod. Id</th>
							<th width="80" >Trans. Date</th>
							<th width="130" >Trans. Ref.</th>
                            <th width="100" >Yarn Lot</th>
                            <th width="100" >Yarn Count</th>
							<th width="120">Composition</th>
							<th width="100">Yarn Type</th>
							<th width="100">Color</th>
                            <th width="100">Basis</th>
                            <th width="100">Wo/Pi Num</th>
                            <th width="100">Purpus</th>
							<th width="100">Receive Qty</th>
							<th >Issue Qty</th>
						</tr>
					</thead>
			   </table> 
			  <div style="width:1350px; overflow-y: scroll; max-height:250px;" id="scroll_body">
				<table width="1330" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body" align="left">
					<tbody>
					<?php
					$i=1;$total_receive="";$total_issue="";
					foreach($sql_result as $val)
					{
						if ($i%2==0)
						$bgcolor="#E9F3FF";
						else
						$bgcolor="#FFFFFF";
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
							<td width="30"><p><?php echo $i; ?>&nbsp;</p></td>
							<td width="70" align="center"><p><?php echo $val[csf("prod_id")]; ?>&nbsp;</p></td>
							<td width="80" align="center"><p><?php if($val[csf("transaction_date")]!="0000-00-00") echo change_date_format($val[csf("transaction_date")]); else echo ""; ?>&nbsp;</p></td>
							<td width="130"  align="center"><p>
							<?php
								if($val[csf("transaction_type")]==1 || $val[csf("transaction_type")]==4) 
								{
									echo $receive_num_arr[$val[csf('rec_issue_id')]];
								}
								else if($val[csf("transaction_type")]==2  || $val[csf("transaction_type")]==3)
								{
									echo $issue_num_arr[$val[csf('rec_issue_id')]];
								}

							?>&nbsp;
							</p></td>
                            <td width="100" align="center"><p><?php echo $val[csf("lot")]; ?>&nbsp;</p></td>
                            <td width="100" align="center"><p><?php echo $yarn_count_arr[$val[csf("yarn_count_id")]]; ?>&nbsp;</p></td>
                            <td width="120"><p><?php echo $val[csf("composition")]; ?>&nbsp;</p></td>
                            <td width="100"><p><?php echo $yarn_type[$val[csf("yarn_type")]]; ?>&nbsp;</p></td>
                            <td width="100"><p><?php echo $color_arr[$val[csf("color")]]; ?>&nbsp;</p></td>
							<?php
							if($val[csf("transaction_type")]==1 || $val[csf("transaction_type")]==4) 
							{
								?>
								<td width="100"><p><?php echo $receive_basis_arr[$receive_data_arr[$val[csf("rec_issue_id")]]['receive_basis']]; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo $receive_data_arr[$val[csf("rec_issue_id")]]['booking_no']; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo$yarn_issue_purpose[$receive_data_arr[$val[csf("rec_issue_id")]]['receive_purpose']]; ?>&nbsp;</p></td>
								<?php
							}
							else if($val[csf("transaction_type")]==2 || $val[csf("transaction_type")]==3) 
							{
								?>
								<td width="100"><p><?php echo $receive_basis_arr[$issue_data_arr[$val[csf("rec_issue_id")]]['issue_basis']]; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo $issue_data_arr[$val[csf("rec_issue_id")]]['booking_no']; ?>&nbsp;</p></td>
								<td width="100"><p><?php echo $yarn_issue_purpose[$issue_data_arr[$val[csf("rec_issue_id")]]['issue_purpose']]; ?>&nbsp;</p></td>
								<?php
							}
							?>
							
							<td width="100" align="right"><p><?php echo number_format($val[csf("receive_qty")],2); $total_receive +=$val[csf("receive_qty")]; ?>&nbsp;</p></td>
							<td align="right"><p><?php echo number_format($val[csf("issue_qty")],2); $total_issue +=$val[csf("issue_qty")]; ?>&nbsp;</p></td>
						</tr>
						<?php
						$i++;
					}
					?>   
					</tbody>
				</table>
			 </div>
                <table width="1330" border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" id="table_footer" align="left">
                    <tfoot>
                        <th colspan="12">Total:</th>
                        <th width="100" id="value_total_receive"><?php echo number_format($total_receive,2); ?></th>
                        <th width="100" id="value_total_issue"><?php echo number_format($total_issue,2); ?></th>
                    </tfoot> 
                </table>
        </fieldset>
		</div>    
			<?php	 
	
	}
	
	
			
	foreach (glob("*.xls") as $filename) {
	if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc,ob_get_contents());
	echo "$html**$filename**$cbo_item_cat"; 
	exit();
	
}
disconnect($con);
?>

