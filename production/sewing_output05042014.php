<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create sewing output
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	09-03-2013
Updated by 		: 	Kausar (Creating Print Report )		
Update date		: 	09-01-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Sewing Out Info","../", 1, 1, $unicode,'','');

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";
 

function openmypage(page_link,title)
{
	if ( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	else
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1040px,height=370px,center=1,resize=0,scrolling=0','')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var po_id=this.contentDoc.getElementById("hidden_mst_id").value;//po id
			var item_id=this.contentDoc.getElementById("hidden_grmtItem_id").value;
			var po_qnty=this.contentDoc.getElementById("hidden_po_qnty").value;
			var country_id=this.contentDoc.getElementById("hidden_country_id").value; 	
			if (po_id!="")
			{
				//freeze_window(5);
				$("#txt_order_qty").val(po_qnty);
				$("#cbo_item_name").val(item_id);
				$("#cbo_country_name").val(country_id);
				
				childFormReset();//child from reset
				get_php_form_data(po_id+'**'+item_id+'**'+country_id, "populate_data_from_search_popup", "requires/sewing_output_controller" );
 				
				var variableSettings=$('#sewing_production_variable').val();
				var styleOrOrderWisw=$('#styleOrOrderWisw').val();
				
				var prod_reso_allo=$('#prod_reso_allo').val();
				
				show_list_view(po_id+'**'+item_id+'**'+country_id+'**'+prod_reso_allo,'show_dtls_listview','list_view_container','requires/sewing_output_controller','');
				set_button_status(0, permission, 'fnc_sewing_output_entry',1,0);
				release_freezing();
			}
		}
	}//end else
}//end function



function fnc_sewing_output_entry(operation)
{
	if(operation==4)
	{
		 var report_title=$( "div.form_caption" ).html();
		 print_report( $('#cbo_company_name').val()+'*'+$('#txt_mst_id').val()+'*'+report_title, "sewing_output_print", "requires/sewing_output_controller" ) 
		 return;
	}
	else if(operation==0 || operation==1 || operation==2)
	{
 		if ( form_validation('cbo_company_name*txt_order_no*cbo_sewing_company*txt_sewing_date*txt_sewing_qty*txt_reporting_hour','Company Name*Order No*Sewing Company*Sewing Date*Sewing Quantity*Reporting Hour')==false )
		{
			return;
		}		
	else
		{
			if($("#cbo_source").val()==1 && ($("#cbo_sewing_line").val()==0 || $("#cbo_sewing_line").val()=="") )
			{
				alert("Please Select Sewing Line");return;
			}
			//freeze_window(operation);			
			var sewing_production_variable = $("#sewing_production_variable").val();
			var colorList = ($('#hidden_colorSizeID').val()).split(",");
			
			var i=0;var colorIDvalue='';
			if(sewing_production_variable==2)//color level
			{
 				$("input[name=txt_color]").each(function(index, element) {
 					if( $(this).val()!='' )
					{
						if(i==0)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val();
						}
						else
						{
							colorIDvalue += "**"+colorList[i]+"*"+$(this).val();
						}
					}
					i++;
				});
			}
			else if(sewing_production_variable==3)//color and size level
			{
 				$("input[name=colorSize]").each(function(index, element) {
					if( $(this).val()!='' )
					{
						if(i==0)
						{
							colorIDvalue = colorList[i]+"*"+$(this).val();
						}
						else
						{
							colorIDvalue += "***"+colorList[i]+"*"+$(this).val();
						}
					}
 					i++;
				});
			}
			
			var data="action=save_update_delete&operation="+operation+"&colorIDvalue="+colorIDvalue+get_submitted_data_string('garments_nature*cbo_company_name*cbo_country_name*sewing_production_variable*hidden_po_break_down_id*hidden_colorSizeID*cbo_buyer_name*txt_style_no*cbo_item_name*txt_order_qty*cbo_source*cbo_sewing_company*cbo_location*cbo_floor*txt_sewing_date*cbo_sewing_line*txt_reporting_hour*cbo_time*txt_super_visor*txt_sewing_qty*txt_reject_qnty*txt_alter_qnty*txt_challan*txt_remark*txt_input_quantity*txt_cumul_sewing_qty*txt_yet_to_sewing*hidden_break_down_html*txt_mst_id*prod_reso_allo*txt_spot_qnty',"../");
 			 
 			http.open("POST","requires/sewing_output_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_sewing_output_entry_Reply_info;
		}
	}
}
  
function fnc_sewing_output_entry_Reply_info()
{
 	if(http.readyState == 4) 
	{
		// alert(http.responseText);
		var variableSettings=$('#sewing_production_variable').val();
		var styleOrOrderWisw=$('#styleOrOrderWisw').val();
		var item_id=$('#cbo_item_name').val();
		var country_id = $("#cbo_country_name").val();
		var prod_reso_allo=$('#prod_reso_allo').val();
		
		var reponse=http.responseText.split('**');		 
		if(reponse[0]==15) 
		{ 
			 setTimeout('fnc_fabric_cost_dtls('+ reponse[1]+')',8000); 
		}
		if(reponse[0]==0)
		{
			var po_id = reponse[1];
			show_msg(reponse[0]);
 			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id+'**'+prod_reso_allo,'show_dtls_listview','list_view_container','requires/sewing_output_controller','');
			reset_form('','','txt_sewing_date*cbo_sewing_line*txt_reporting_hour*cbo_time*txt_super_visor*txt_sewing_qty*txt_reject_qnty*txt_alter_qnty*txt_challan*txt_remark*txt_input_quantity*txt_cumul_sewing_qty*txt_yet_to_sewing*hidden_break_down_html*txt_mst_id*txt_spot_qnty','','');
			get_php_form_data(po_id+'**'+$('#cbo_item_name').val()+'**'+country_id, "populate_data_from_search_popup", "requires/sewing_output_controller" );
 			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/sewing_output_controller" ); 
			}
			else
			{
				$("#txt_sewing_qty").removeAttr("readonly");
			}
			release_freezing();
		}
		if(reponse[0]==1)
		{
			var po_id = reponse[1];
			show_msg(reponse[0]);
			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id+'**'+prod_reso_allo,'show_dtls_listview','list_view_container','requires/sewing_output_controller','');
			reset_form('','','txt_sewing_date*cbo_sewing_line*txt_reporting_hour*cbo_time*txt_super_visor*txt_sewing_qty*txt_reject_qnty*txt_alter_qnty*txt_challan*txt_remark*txt_input_quantity*txt_cumul_sewing_qty*txt_yet_to_sewing*hidden_break_down_html*txt_mst_id*txt_spot_qnty','','');
			get_php_form_data(po_id+'**'+$('#cbo_item_name').val()+'**'+country_id, "populate_data_from_search_popup", "requires/sewing_output_controller" );
			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/sewing_output_controller" ); 
			}
			else
			{
				$("#txt_sewing_qty").removeAttr("readonly");
			}
			set_button_status(0, permission, 'fnc_sewing_output_entry',1,0);
			release_freezing();
		}
		if(reponse[0]==2)
		{
			var po_id = reponse[1];
			show_msg(reponse[0]);
			show_list_view(po_id+'**'+$("#cbo_item_name").val()+'**'+country_id+'**'+prod_reso_allo,'show_dtls_listview','list_view_container','requires/sewing_output_controller','');
			reset_form('','','txt_sewing_date*cbo_sewing_line*txt_reporting_hour*cbo_time*txt_super_visor*txt_sewing_qty*txt_reject_qnty*txt_alter_qnty*txt_challan*txt_remark*txt_input_quantity*txt_cumul_sewing_qty*txt_yet_to_sewing*hidden_break_down_html*txt_mst_id*txt_spot_qnty','','');
			get_php_form_data(po_id+'**'+$('#cbo_item_name').val()+'**'+country_id, "populate_data_from_search_popup", "requires/sewing_output_controller" );
			if(variableSettings!=1) { 
				get_php_form_data(po_id+'**'+item_id+'**'+variableSettings+'**'+styleOrOrderWisw+'**'+country_id, "color_and_size_level", "requires/sewing_output_controller" ); 
			}
			else
			{
				$("#txt_sewing_qty").removeAttr("readonly");
			}
			set_button_status(0, permission, 'fnc_sewing_output_entry',1,0);
			release_freezing();
		}
 	}
} 

function childFormReset()
{
	reset_form('','','txt_sewing_date*cbo_sewing_line*txt_reporting_hour*cbo_time*txt_super_visor*txt_sewing_qty*txt_reject_qnty*txt_alter_qnty*txt_challan*txt_remark*txt_input_quantity*txt_cumul_sewing_qty*txt_yet_to_sewing*hidden_break_down_html*txt_mst_id*txt_spot_qnty','','');
 	$('#txt_input_quantity').attr('placeholder','');//placeholder value initilize
	$('#txt_cumul_sewing_qty').attr('placeholder','');//placeholder value initilize
	$('#txt_yet_to_sewing').attr('placeholder','');//placeholder value initilize
	$('#list_view_container').html('');//listview container
	$("#breakdown_td_id").html('');

}  

function fn_hour_check(val)
{
	if(val*1>12)
	{
		alert("You Cross 12!!This is 12 Hours.");
		$("#txt_reporting_hour").val('');
	}
}

function fn_total(tableName,index) // for color and size level
{
    var filed_value = $("#colSize_"+tableName+index).val();
	var placeholder_value = $("#colSize_"+tableName+index).attr('placeholder');
	if(filed_value*1 > placeholder_value*1)
	{
		if( confirm("Qnty Excceded by"+(placeholder_value-filed_value)) )	
			void(0);
		else
		{
			$("#colSize_"+tableName+index).val('');
 		}
	}
	
	var totalRow = $("#table_"+tableName+" tr").length;
	//alert(tableName);
	math_operation( "total_"+tableName, "colSize_"+tableName, "+", totalRow);
	if($("#total_"+tableName).val()*1!=0)
	{
		$("#total_"+tableName).html($("#total_"+tableName).val());
	}
	var totalVal = 0;
	$("input[name=colorSize]").each(function(index, element) {
        totalVal += ( $(this).val() )*1;
    });
	$("#txt_sewing_qty").val(totalVal);
}

function fn_colorlevel_total(index) //for color level
{
	
	var filed_value = $("#colSize_"+index).val();
	var placeholder_value = $("#colSize_"+index).attr('placeholder');
	if(filed_value*1 > placeholder_value*1)
	{
		if( confirm("Qnty Excceded by"+(placeholder_value-filed_value)) )	
			void(0);
		else
		{
			$("#colSize_"+index).val('');
 		}
	}
	
    var totalRow = $("#table_color tbody tr").length;
	//alert(totalRow);
	math_operation( "total_color", "colSize_", "+", totalRow);
	$("#txt_sewing_qty").val( $("#total_color").val() );
} 

    
</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">

<?php echo load_freeze_divs ("../",$permission);  ?>

<fieldset style="width:950px;">
<legend>Production Module</legend>  
			<form name="sewingoutput_1" id="sewingoutput_1" autocomplete="off" >
                <fieldset>
                    <table width="100%" border="0">
                        <tr>
                            <td width="102" class="must_entry_caption">Company</td>
                            <td>                                
								<?php 
                                 echo create_drop_down( "cbo_company_name", 170, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select --", $selected, "load_drop_down( 'requires/sewing_output_controller', this.value, 'load_drop_down_location', 'location_td' );get_php_form_data(this.value,'load_variable_settings','requires/sewing_output_controller');" );
                                ?>
                                <input type="hidden" id="sewing_production_variable" />	 
                                <input type="hidden" id="styleOrOrderWisw" />
                                <input type="hidden" id="prod_reso_allo" />
							</td>
                            <td width="102" class="must_entry_caption">Order No</td>
                            <td width="170">
								<input name="txt_order_no" placeholder="Double Click to Search" id="txt_order_no" onDblClick="openmypage('requires/sewing_output_controller.php?action=order_popup&company='+document.getElementById('cbo_company_name').value+'&garments_nature='+document.getElementById('garments_nature').value,'Order Search')"  class="text_boxes" style="width:155px " readonly>
                                <input type="hidden" id="hidden_po_break_down_id" value="" />
							</td>
                            <td width="130" >Country</td>
                            <td width="170">
                                <?php
                                    echo create_drop_down( "cbo_country_name", 170, "select id,country_name from lib_country","id,country_name", 1, "-- Select Country --", $selected, "",1 );
                                ?> 
                            </td>
                        </tr>
                        <tr>    
                            <td width="102">Buyer</td>
                            <td width="170">
                            <?php
                            echo create_drop_down( "cbo_buyer_name", 170, "select id,buyer_name from lib_buyer where is_deleted=0 and status_active=1 order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",1,0 );
							?>
							</td>
                             <td width="102">Job No</td>
                            <td width="170">
                            <input name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:155px " disabled readonly>	
							</td>
                            <td width="102">Style</td>
                            <td width="170">
							<input name="txt_style_no" id="txt_style_no" class="text_boxes"  style="width:158px " disabled readonly>
							</td>
                           
                        </tr>
                        <tr>  
                        	 <td width="102"> Item </td>
                             <td width="170">
							 <?php
                             echo create_drop_down( "cbo_item_name", 170, $garments_item,"", 1, "-- Select Item --", $selected, "",1,0 );	
                             ?>
							 </td>  
                             <td width="102">Order Qnty</td>
                             <td width="170">
							 <input name="txt_order_qty" id="txt_order_qty" class="text_boxes_numeric" style="width:155px " disabled readonly>
							 </td>
                             <td width="102">Source</td>
                             <td width="170">
                             <?php
                             echo create_drop_down( "cbo_source", 170, $knitting_source,"", 1, "-- Select Source --", $selected, "load_drop_down( 'requires/sewing_output_controller', this.value+'**'+$('#cbo_company_name').val(), 'load_drop_down_sewing_output', 'sew_company_td' );get_php_form_data(this.value,'line_disable_enable','requires/sewing_output_controller');get_php_form_data($('#cbo_company_name').val()+'**'+this.value+'**'+$('#hidden_po_break_down_id').val()+'**'+$('#cbo_item_name').val()+'**'+$('#cbo_country_name').val(), 'display_bl_qnty', 'requires/sewing_output_controller');", 0, '1,3' );
							 ?>
                             </td>
                            
                        </tr>
                        <tr>
                         	 <td width="102" class="must_entry_caption">Sewing Company</td>
                             <td width="170" id="sew_company_td" >
                             <?php
                             echo create_drop_down( "cbo_sewing_company", 170, $blank_array,"", 1, "--- Select ---", $selected, "" );
							 ?>
						     </td>
                             <td width="102">Location</td>
                             <td width="170" id="location_td">
                             <?php
                             echo create_drop_down( "cbo_location", 167,$blank_array,"", 1, "-- Select Location --", $selected, "" );
							 ?>
							 </td>
                             <td width="102">Floor</td>
                              <td width="170" id="floor_td">
                              <?php 
                              echo create_drop_down( "cbo_floor", 170, $blank_array,"", 1, "-- Select Floor --", $selected, "" );
							  ?>
                              </td>
                        </tr>
                    </table>
                </fieldset>
                
                <table><tr><td colspan="6" height="5"></td></tr></table>
                <table cellpadding="0" cellspacing="1" width="100%">
                    <tr>
                    	<td width="30%" valign="top">
                            <fieldset>
                            <legend>New Entry</legend>
                                <table  cellpadding="0" cellspacing="1" width="100%">
                                    <tr>
                                    	<td width="120" class="must_entry_caption">Sewing Date</td>
                                         <td width=""> 
                                         <input name="txt_sewing_date" id="txt_sewing_date" class="datepicker" type="text" value="<?php echo date("d-m-Y")?>" style="width:100px;"  onChange="load_drop_down( 'requires/sewing_output_controller', document.getElementById('cbo_floor').value+'_'+document.getElementById('cbo_location').value+'_'+document.getElementById('prod_reso_allo').value+'_'+this.value, 'load_drop_down_sewing_line_floor', 'sewing_line_td' );" />
                                        </td>
                                     </tr>
                                     <tr>
                                        <td width="120">Sewing Line No</td> 
                                        <td id="sewing_line_td">            
                              			<?php
                              			echo create_drop_down( "cbo_sewing_line", 110, $blank_array,"", 1, "Select Line", $selected, "",1,0 );		
							  			?>	
                              			</td> 
                                   </tr>
                                     <tr>
                                       <td class="must_entry_caption">Reporting Hour</td> 
                                       <td ><input name="txt_reporting_hour" id="txt_reporting_hour" class="text_boxes_numeric"  style="width:45px" onKeyUp="fn_hour_check(this.value)" />
                                       <?php
                                         echo create_drop_down( "cbo_time", 50, $time_source, 0, "", 1, "" );
                                         ?></td>
                                     </tr>
                                   <tr>
                                         <td width="120">Supervisor</td> 
                                         <td width=""> 
                                         <input type="text" name="txt_super_visor" id="txt_super_visor" class="text_boxes"  style="width:100px">
                                         </td>
                                   </tr>
                                   <tr>
                                        <td width="120" valign="top" class="must_entry_caption">QC Pass Qty</td> 
                                        <td width="" valign="top"><input name="txt_sewing_qty" id="txt_sewing_qty" class="text_boxes_numeric"  style="width:100px" readonly >
                                            <input type="hidden" id="hidden_break_down_html"  value="" readonly disabled />
                                            <input type="hidden" id="hidden_colorSizeID"  value="" readonly disabled />
                                        </td>
                                   </tr>
                                   <tr>
                                     <td>Alter Qty </td>
                                     <td><input type="text" name="txt_alter_qnty" id="txt_alter_qnty" class="text_boxes_numeric" style="width:100px; text-align:right" /></td>
                                   </tr>
                                   <tr>
                                     <td >Spot Qty </td>
                                     <td><input type="text" name="txt_spot_qnty" id="txt_spot_qnty" class="text_boxes_numeric" style="width:100px; text-align:right" /></td>
                                   </tr>
                                   <tr>
                                     <td valign="top">Reject Qty</td>
                                     <td valign="top"><input type="text" name="txt_reject_qnty" id="txt_reject_qnty" class="text_boxes_numeric" style="width:100px; " /></td>
                                   </tr>
                                    <tr>
                                         <td width="">Challan No</td> 
                                         <td>
                                           <input type="text" name="txt_challan" id="txt_challan" class="text_boxes" style="width:100px" />
                                         </td>
                                    </tr>
                                   <tr>
                                     <td valign="top">Remarks</td>
                                     <td valign="top"><input type="text" name="txt_remark" id="txt_remark" class="text_boxes" style="width:150px;" /></td>
                                   </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td width="1%" valign="top">
                        </td>
                         <td width="25%" valign="top">
                            <fieldset>
                            <legend>Display</legend>
                                <table  cellpadding="0" cellspacing="2" width="100%" >
                                    <tr>
                                        <td width="120">Input Quantity</td>
                                        <td>
                                            <input type="text" name="txt_input_quantity" id="txt_input_quantity" class="text_boxes_numeric" style="width:80px" readonly disabled  />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120">Cumul. Sew. Qty</td>
                                        <td>
                                            <input type="text" name="txt_cumul_sewing_qty" id="txt_cumul_sewing_qty" class="text_boxes_numeric" style="width:80px" readonly disabled />
                                        </td>
                                    </tr>
                                     <tr>
                                        <td width="120">Yet to Sewing</td>
                                        <td>
                                            <input type="text" name="txt_yet_to_sewing" id="txt_yet_to_sewing" class="text_boxes_numeric" style="width:80px" / readonly disabled >
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>	
                        </td>
                        <td width="40%" valign="top" >
                            <div style="max-height:350px; overflow-y:scroll" id="breakdown_td_id" align="center"></div>
                        </td>                         
                     </tr>
                     <tr>
		   				<td align="center" colspan="9" valign="middle" class="button_container">
							<?php
                            echo load_submit_buttons( $permission, "fnc_sewing_output_entry", 0,1,"reset_form('sewingoutput_1','','','','childFormReset()')",1); 
                            ?>
                            <input type="hidden" name="txt_mst_id" id="txt_mst_id" readonly />
           				</td>
           				<td>&nbsp;</td>					
		  			</tr>
                </table>
                <div style="width:940px; margin-top:5px;"  id="list_view_container" align="center"></div>
            </form>
        </fieldset>
    </div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>