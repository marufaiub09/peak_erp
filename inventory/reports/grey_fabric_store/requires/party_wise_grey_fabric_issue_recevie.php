﻿<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Party Wise Grey Fabric Issue Recevie Report
				
Functionality	:	
JS Functions	:
Created by		:	Aziz
Creation date 	: 	22-08-2015
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Party Wise Grey Fabric  Issue Recevie Report","../../../", 1, 1, $unicode,1,0); 

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

	function generate_report(type)
	{
		
		
		var report_basis = $("#cbo_report_basis").val();
		if(report_basis!=1)
		{
		
			if(type==1)
			{
				if( form_validation('cbo_company_name*cbo_dyeing_source*txt_date_from*txt_date_to','Company Name*Source*From Date*To Date')==false)
				{
					return;
				}
			}
		}
		
		var report_title=$( "div.form_caption" ).html();
		var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_dyeing_source*txt_dyeing_com_id*txt_job_no*txt_date_from*txt_date_to*cbo_report_basis*cbo_buyer_name*txt_book_no*txt_book_id',"../../../")+'&report_title='+report_title+'&type='+type;
		freeze_window(3);
		http.open("POST","requires/party_wise_grey_fabric_issue_recevie_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = generate_report_reponse;  
	}

	function generate_report_job()
	{
		if( form_validation('cbo_company_name*cbo_dyeing_source','Company Name*Source')==false)//*txt_date_from*txt_date_to *From Date*To Date
		{
			return;
		}
		
		var report_title=$( "div.form_caption" ).html();
		var data="action=report_generate_job"+get_submitted_data_string('cbo_company_name*cbo_dyeing_source*txt_dyeing_com_id*cbo_order_type*txt_job_no*txt_job_id*txt_date_from*txt_date_to',"../../../")+'&report_title='+report_title;//+'&type='+type
		freeze_window(3);
		http.open("POST","requires/party_wise_grey_fabric_issue_recevie_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = generate_report_reponse;  
	}

	function generate_report_reponse()
	{	
		if(http.readyState == 4) 
		{	 
			var reponse=trim(http.responseText).split("####");
			$("#report_container2").html(reponse[0]);  
			document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
			//setFilterGrid("table_body",-1);
			
			show_msg('3');
			release_freezing();
		}
	} 

	function new_window()
	{
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');
		d.close(); 
	
	}

	function openmypage_job()
	{
		if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}
		var companyID = $("#cbo_company_name").val();
		var cbo_dyeing_source = $("#cbo_dyeing_source").val();
		var txt_dyeing_com_id = $("#txt_dyeing_com_id").val();
		var page_link='requires/party_wise_grey_fabric_issue_recevie_controller.php?action=job_no_popup&companyID='+companyID+'&cbo_dyeing_source='+cbo_dyeing_source+'&txt_dyeing_com_id='+txt_dyeing_com_id;
		var title='Job No Search';
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=630px,height=370px,center=1,resize=1,scrolling=0','../../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var job_no=this.contentDoc.getElementById("hide_job_no").value;
			var job_id=this.contentDoc.getElementById("hide_job_id").value;
			
			$('#txt_job_no').val(job_no);
			$('#hide_job_id').val(job_id);	 
		}
	}
	
	function openmypage_party()
	{
		if( form_validation('cbo_company_name*cbo_dyeing_source','Company Name*Source')==false )
		{
			return;
		}
		var companyID = $("#cbo_company_name").val();
		var cbo_dyeing_source = $("#cbo_dyeing_source").val();
		var txt_knit_comp_id = $("#txt_knit_comp_id").val();
		var page_link='requires/party_wise_grey_fabric_issue_recevie_controller.php?action=party_popup&companyID='+companyID+'&cbo_dyeing_source='+cbo_dyeing_source+'&txt_knit_comp_id='+txt_knit_comp_id;
		var title='Party Search';
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=430px,height=370px,center=1,resize=1,scrolling=0','../../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var party_name=this.contentDoc.getElementById("hide_party_name").value;
			var party_id=this.contentDoc.getElementById("hide_party_id").value;
			
			$('#txt_dyeing_company').val(party_name);
			$('#txt_dyeing_com_id').val(party_id);	 
		}
	}
	
	function openmypage_booking()
	{
			//var job_no = $("#txt_job_no").val();
		if( form_validation('cbo_company_name','Company Name')==false )
		{
			return;
		}
		var data=document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_buyer_name').value;
		//alert (data);
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/party_wise_grey_fabric_issue_recevie_controller.php?action=booking_no_popup&data='+data,'Booking No Popup', 'width=1050px,height=420px,center=1,resize=0','../../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var theemail=this.contentDoc.getElementById("selected_booking");
			//var response=theemail.value.split('_');
			if (theemail.value!="")
			{
				freeze_window(5);
				//document.getElementById("txt_booking_id").value=theemail.value;
			    document.getElementById("txt_book_no").value=theemail.value;
				release_freezing();
			}
		}
	}
	
	function dyeing_company_val()
	{
		$('#txt_dyeing_company').val('');
		$('#txt_dyeing_com_id').val('');	 
	}
	
	
	function report_basis_color(type)
	{
		if(type==1)
		{
			$('#txt_date_from').attr('disabled','disabled'); 
			$('#txt_date_to').attr('disabled','disabled'); 
		}
		else
		{
			$('#txt_date_from').removeAttr('disabled','disabled');
			$('#txt_date_to').removeAttr('disabled','disabled');
		}
	}
</script>
</head>

<body onLoad="set_hotkey();report_basis_color(document.getElementById('cbo_report_basis').value);">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../../../",$permission); ?><br />    		 
    <form name="PartyWiseGreyReconciliation_1" id="PartyWiseGreyReconciliation_1" autocomplete="off" > 
        <div style="width:100%;" align="center">
            <fieldset style="width:1010px;">
            <legend>Search Panel</legend> 
                <table class="rpt_table" width="1000" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <thead>
                        <tr> 	 	
                            <th width="140" class="must_entry_caption">Company</th>                                
                            <th width="130">Source</th>
                            <th width="140">Dyeing Company</th>
                            <th width="80">Report Basis</th>
                            <th width="80">Buyer</th>
                            <th width="80">Job</th>
                            <th width="80">F.Booking</th>
                            <th>Issue Date</th>
                        </tr>
                    </thead>
                    <tr align="center">
                        <td>
                            <?php 
                               echo create_drop_down( "cbo_company_name", 140, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/party_wise_grey_fabric_issue_recevie_controller',this.value, 'load_drop_down_buyer', 'buyer_td' )" );
                            ?>                            
                        </td>
                        <td>
							<?php
                                echo create_drop_down("cbo_dyeing_source",130,$knitting_source,"", 1, "-- Select Source --", 0,"dyeing_company_val();",0,'1,3');
                            ?>
                        </td>
                        <td id="dyeing_com">
                            <input type="text" id="txt_dyeing_company" name="txt_dyeing_company" class="text_boxes" style="width:100px" onDblClick="openmypage_party();" placeholder="Browse Party" />           <input type="hidden" id="txt_dyeing_com_id" name="txt_dyeing_com_id" class="text_boxes" style="width:60px" />
                        </td>
                          <td>
                          <?php
						  $order_type=array(1=>"Color Wise",2=>"Transaction Wise");
                                echo create_drop_down("cbo_report_basis",100,$order_type,"", 1, "-- Select Type --", 1,"report_basis_color(this.value);",0,'')
                            ?>
                        </td>
                        <td  id="buyer_td">
							 <?php 
                                echo create_drop_down( "cbo_buyer_name", 100, $blank_array,"", 1, "-- Select Buyer --", $selected, "",1,"" );
                            ?>
                        </td>
                       
                        <td>
                            <input type="text" id="txt_job_no" name="txt_job_no" class="text_boxes" style="width:80px" onDblClick="openmypage_job();" placeholder="Browse Job" />
                            <input type="hidden" id="txt_job_id" name="txt_job_id" class="text_boxes" style="width:60px" />
                        </td>
                         <td>
                            <input type="text" id="txt_book_no" name="txt_book_no" class="text_boxes" style="width:80px" onDblClick="openmypage_booking();" placeholder="Write/Browse" />
                            <input type="hidden" id="txt_book_id" name="txt_book_id" class="text_boxes" style="width:60px" />
                        </td>
                       
                        <td align="center">
                        		<?php //echo date("01-m-Y"); ?>
                             <input type="text" name="txt_date_from" id="txt_date_from"   class="datepicker" style="width:60px" placeholder="From Date"/>
                             To
                             <input type="text" name="txt_date_to" id="txt_date_to"   class="datepicker" style="width:60px" placeholder="To Date"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" align="center" width="95%"><?php echo load_month_buttons(1); ?></td>
                    </tr>
                </table>
                <div style="margin-top:10px">  
                    <input type="button" name="search" id="search" value="Show" onClick="generate_report(1)" style="width:100px" class="formbutton" />&nbsp;
                    <input type="reset" name="res" id="res" value="Reset" style="width:100px" class="formbutton" onClick="reset_form('PartyWiseGreyReconciliation_1', 'report_container*report_container2','','','','');" />
                </div> 
            </fieldset>  
            
            <div id="report_container" align="center"></div>
        	<div id="report_container2"></div>
        </div>
    </form>    
</div>    
</body>  
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
