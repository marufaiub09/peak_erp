﻿<?
/*-------------------------------------------- Comments
Version                  :  V1
Purpose			         : 	This form will create Sample Fabric Booking (Without Order)
Functionality	         :	
JS Functions	         :
Created by		         :	Monzu 
Creation date 	         : 	27-12-2012
Requirment Client        : 
Requirment By            : 
Requirment type          : 
Requirment               : 
Affected page            : 
Affected Code            :                   
DB Script                : 
Updated by 		         : 		
Update date		         : 		   
QC Performed BY	         :		
QC Date			         :	
Comments		         :From this version oracle conversion is start
*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Sample Booking Non Order", "../../", 1, 1,$unicode,'','');
?>	
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
var permission='<? echo $permission; ?>';


function openmypage_booking(page_link,title)
{
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var theemail=this.contentDoc.getElementById("selected_booking");
		if (theemail.value!="")
		{
			freeze_window(5);
			reset_form('fabricbooking_1','','booking_list_view','cbo_pay_mode,3*cbo_currency,2*cbo_ready_to_approved,2');
			get_php_form_data( theemail.value, "populate_data_from_search_popup", "requires/sample_booking_non_order_controller" );
			reset_form('orderdetailsentry_2','booking_list_view','','','')
			show_list_view(theemail.value,'show_fabric_booking','booking_list_view','requires/sample_booking_non_order_controller','setFilterGrid(\'list_view\',-1)');
			set_button_status(1, permission, 'fnc_fabric_booking',1);
			release_freezing();
		}
	}
}

function color_from_library(company_id)
{
	var color_from_library=return_global_ajax_value(company_id, 'color_from_library', '', 'requires/sample_booking_non_order_controller');
	if(color_from_library==1)
	{
		$('#txt_gmt_color').attr('readonly',true);
		$('#txt_gmt_color').attr('placeholder','Click');
		$('#txt_gmt_color').attr('onClick',"color_select_popup(document.getElementById('cbo_buyer_name').value,this.id);");
		
		$('#txt_color').attr('readonly',true);
		$('#txt_color').attr('placeholder','Click');
		$('#txt_color').attr('onClick',"color_select_popup(document.getElementById('cbo_buyer_name').value,this.id);");
		
	}
	else
	{
		$('#txt_gmt_color').attr('readonly',false);
		$('#txt_gmt_color').removeAttr('placeholder','Click');
		$('#txt_gmt_color').removeAttr('onClick',"color_select_popup(document.getElementById('cbo_buyer_name').value,this.id);");
		
		$('#txt_color').attr('readonly',false);
		$('#txt_color').removeAttr('placeholder','Click');
		$('#txt_color').removeAttr('onClick',"color_select_popup(document.getElementById('cbo_buyer_name').value,this.id);");
	}
}

function color_select_popup(buyer_name,text_box)
{
	//var page_link='requires/sample_booking_non_order_controller.php?action=color_popup'
	//alert(page_link)
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/sample_booking_non_order_controller.php?action=color_popup&buyer_name='+buyer_name, 'Color Select Pop Up', 'width=250px,height=450px,center=1,resize=1,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var color_name=this.contentDoc.getElementById("color_name");
		if (color_name.value!="")
		{
			$('#'+text_box).val(color_name.value);
		}
	}
}

function calculate_requirement()
{
	var cbo_company_name= document.getElementById('cbo_company_name').value;
	if(cbo_company_name=="")
	{
		alert("Select Company");
		return;
	}
	
	var cbo_fabric_natu= document.getElementById('cbo_fabric_natu').value;
	if(cbo_fabric_natu=="")
	{
		alert("Select Fabric Nature");
		return;
	}
	var process_loss_method_id=return_global_ajax_value(cbo_company_name+'_'+cbo_fabric_natu, 'process_loss_method_id', '', 'requires/sample_booking_non_order_controller');
	var txt_finish_qnty=(document.getElementById('txt_finish_qnty').value)*1;
	var processloss=(document.getElementById('txt_process_loss').value)*1;
	    var WastageQty='';
		if(process_loss_method_id==1)
		{
			WastageQty=txt_finish_qnty+txt_finish_qnty*(processloss/100);
		}
		else if(process_loss_method_id==2)
		{
			var devided_val = 1-(processloss/100);
			var WastageQty=parseFloat(txt_finish_qnty/devided_val);
		}
		else
		{
			WastageQty=0;
		}
		WastageQty= number_format_common( WastageQty, 5, 0) ;	
		document.getElementById('txt_grey_qnty').value= WastageQty;
		document.getElementById('process_loss_method').value= process_loss_method_id;
		document.getElementById('txt_amount').value=number_format_common((document.getElementById('txt_rate').value)*1*WastageQty,5,0)
		
		
}


	


function fnc_fabric_booking( operation )
{
	if(operation==2)
	{
		alert("Delete Restricted")
		return;
	}
	if(document.getElementById('id_approved_id').value==1)
	{
		alert("This booking is approved")
		return;
	}
	

 if (form_validation('cbo_company_name*cbo_buyer_name*cbo_fabric_natu*cbo_fabric_source*txt_booking_date','Company Name*Buyer Name*Fabric Nature*Fabric Source*Booking Date')==false)
	{
		return;
	}	
	else
	{
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name*cbo_buyer_name*txt_booking_no*cbo_fabric_natu*cbo_fabric_source*cbo_currency*txt_exchange_rate*cbo_pay_mode*txt_booking_date*cbo_supplier_name*txt_attention*txt_delivery_date*cbo_source*cbo_ready_to_approved*cbo_team_leader*cbo_dealing_merchant',"../../");
		freeze_window(operation);
		http.open("POST","requires/sample_booking_non_order_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_booking_reponse;
	}
}
	 
function fnc_fabric_booking_reponse()
{
	
	if(http.readyState == 4) 
	{
		 var reponse=trim(http.responseText).split('**');
		 show_msg(trim(reponse[0]));
		 document.getElementById('txt_booking_no').value=reponse[1];
		 set_button_status(1, permission, 'fnc_fabric_booking',1);
		 release_freezing();
	}
}



function open_fabric_decription_popup()
{
	var cbofabricnature=document.getElementById('cbo_fabric_natu').value;
	var page_link='requires/sample_booking_non_order_controller.php?action=fabric_description_popup&fabric_nature='+cbofabricnature
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Fabric Description', 'width=1060px,height=450px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
		{
			var fab_des_id=this.contentDoc.getElementById("fab_des_id");
			var fab_nature_id=this.contentDoc.getElementById("fab_nature_id");
			var fab_desctiption=this.contentDoc.getElementById("fab_desctiption");
			var fab_gsm=this.contentDoc.getElementById("fab_gsm");
			var yarn_desctiption=this.contentDoc.getElementById("yarn_desctiption");
			var construction=this.contentDoc.getElementById("construction");
			var composition=this.contentDoc.getElementById("composition");
			document.getElementById('libyarncountdeterminationid').value=fab_des_id.value;
			document.getElementById('txt_fabricdescription').value=fab_desctiption.value;
			document.getElementById('txt_gsm').value=fab_gsm.value;
			document.getElementById('yarnbreackdown').value=yarn_desctiption.value;
			document.getElementById('construction').value=construction.value;
			document.getElementById('composition').value=composition.value;
		}
}

function open_sample_popup()
{
	var cbo_company_name=document.getElementById('cbo_company_name').value;
	var cbo_buyer_name=document.getElementById('cbo_buyer_name').value;
	if(cbo_company_name==0)
	{
		alert("Select Company")
		return;
	}
	if(cbo_buyer_name==0)
	{
		alert("Select Buyer")
		return;
	}
	var page_link='requires/sample_booking_non_order_controller.php?action=sample_description_popup&cbo_company_name='+cbo_company_name+'&cbo_buyer_name='+cbo_buyer_name;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Sample Description', 'width=1060px,height=450px,center=1,resize=1,scrolling=0','../');
	emailwindow.onclose=function()
		{
			var style_id=this.contentDoc.getElementById("style_id");
			var style_no=this.contentDoc.getElementById("style_no");
			var sample_id=this.contentDoc.getElementById("sample_id");
			var article_no=this.contentDoc.getElementById("article_no");

			//alert(sample_id.value)
			document.getElementById('txt_style').value=style_id.value;
			document.getElementById('txt_style_no').value=style_no.value;
			document.getElementById('cbo_sample_type').value=sample_id.value;
			document.getElementById('txt_article_no').value=article_no.value;

		}
	
}


function fnc_fabric_booking_dtls( operation )
{
	/*if(operation==2)
	{
		alert("Delete Restricted")
		return;
	}*/
	if(document.getElementById('id_approved_id').value==1)
	{
		alert("This booking is approved")
		return;
	}
	if (form_validation('cbo_body_part*txt_finish_qnty*cbo_sample_type*txt_fabricdescription','Body Part*Finish Fabric*Sample type*Fabric Description')==false)
	{
		return;
	}
	/*if(document.getElementById('libyarncountdeterminationid').value==0 || document.getElementById('libyarncountdeterminationid').value=="")
	{
		alert("You may have copied Fabric Description,Please Browse it")
		return;
	}*/
	var data="action=save_update_delete_dtls&operation="+operation+get_submitted_data_string('txt_booking_no*cbo_fabric_natu*cbo_fabric_source*cbo_body_part*cbo_color_type*txt_style*txt_style_des*cbo_sample_type*libyarncountdeterminationid*construction*composition*yarnbreackdown*txt_fabricdescription*txt_gsm*txt_gmt_color*txt_color*txt_gmts_size*txt_size*txt_dia_width*txt_finish_qnty*txt_process_loss*txt_grey_qnty*txt_rate*txt_amount*update_id_details*process_loss_method*txt_article_no*txt_yarn_details*txt_remarks',"../../");
	//alert(data);
	//return;
	freeze_window(operation);
	http.open("POST","requires/sample_booking_non_order_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_fabric_booking_dtls_reponse;
}
	 
function fnc_fabric_booking_dtls_reponse()
{
	if(http.readyState == 4) 
	{
		 var reponse=http.responseText.split('**');
		 show_msg(trim(reponse[0]));
		 reset_form('','','txt_gmt_color*txt_color','','')
		 set_button_status(0, permission, 'fnc_fabric_booking_dtls',2);
		 show_list_view(reponse[1],'show_fabric_booking','booking_list_view','requires/sample_booking_non_order_controller','setFilterGrid(\'list_view\',-1)');
		 release_freezing();
	}
}


function open_terms_condition_popup(page_link,title)
{
	var txt_booking_no=document.getElementById('txt_booking_no').value;
	if (txt_booking_no=="")
	{
		alert("Save The Booking First")
		return;
	}	
	else
	{
	    page_link=page_link+get_submitted_data_string('txt_booking_no','../../');
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=720px,height=470px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
		}
	}
}

function enable_disable(value)
{
	if(value==2){
		document.getElementById('txt_rate').disabled=false;
	}
	else
	{
		document.getElementById('txt_rate').disabled=true;
	}
}

function generate_fabric_report()
{
if (form_validation('txt_booking_no','Booking No')==false)
	{
		return;
	}
	else
	{
		$report_title=$( "div.form_caption" ).html();
		var data="action=show_fabric_booking_report"+get_submitted_data_string('txt_booking_no*cbo_company_name*id_approved_id',"../../")+'&report_title='+$report_title;
		//freeze_window(5);
		http.open("POST","requires/sample_booking_non_order_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = generate_fabric_report_reponse;
	}	
}


function generate_fabric_report_reponse()
{
	if(http.readyState == 4) 
	{
		$('#data_panel').html( http.responseText );
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel').innerHTML+'</body</html>');
		d.close();
	}
}
</script>
 
</head>
 
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
     <? echo load_freeze_divs ("../../",$permission);  ?>
            	<form name="fabricbooking_1"  autocomplete="off" id="fabricbooking_1">
            	<fieldset style="width:950px;">
                <legend>Sample Booking (Without Order) </legend>
               
            		<table  width="900" cellspacing="2" cellpadding="0" border="0">
                    
                    
                    <tr>
                            
                                <td  width="130" height="" align="right" > Booking No </td>              <!-- 11-00030  -->
                                <td  width="170" >
                                	<input class="text_boxes" type="text" style="width:160px" onDblClick="openmypage_booking('requires/sample_booking_non_order_controller.php?action=fabric_booking_popup','fabric Booking Search')" readonly placeholder="Double Click for Booking" name="txt_booking_no" id="txt_booking_no"/>
                                
                                 
                                </td>
                              <td  align="right" class="must_entry_caption">Company Name</td>
                              <td>
                              <? 
							  	echo create_drop_down( "cbo_company_name", 172, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name", "id,company_name",1, "-- Select Company --", $selected, "load_drop_down( 'requires/sample_booking_non_order_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );color_from_library( this.value )","","" );
								?>	  
                              </td>
                         <td align="right" class="must_entry_caption">Buyer Name</td>   
   						 <td id="buyer_td"> 
                             <?  
							  	echo create_drop_down( "cbo_buyer_name", 172, $blank_array,"", 1, "-- Select Buyer --", $selected, "","","" );
								?>
                         </td>
							
                        </tr>
                       <tr>
                       <td align="right" class="must_entry_caption">Fabric Nature</td>
                        	<td>
                            	  <? 
									echo create_drop_down( "cbo_fabric_natu", 172, $item_category,"", 1, "-- Select --", 1,$onchange_func, $is_disabled, "2,3");		
								  ?>	
                            </td>
                               <td align="right" width="130" class="must_entry_caption">
                               Fabric Source
                                </td>
                                <td>	
                                    <? 
									echo create_drop_down( "cbo_fabric_source", 172, $fabric_source,"", 1, "-- Select --", "","enable_disable(this.value);", "", "");		
								  ?>
                                </td>
                       <td  width="130" align="right" class="must_entry_caption">Booking Date</td>
                                <td width="170">
                                    <input class="datepicker" type="text" style="width:160px" name="txt_booking_date" id="txt_booking_date"/>	
                                </td>
                                                          
                        </tr>
                      
                        
                        <tr>
                        <td align="right">Currency</td>
                              <td>
                              <? 
							  	echo create_drop_down( "cbo_currency", 172, $currency,"", 1, "-- Select --", 2, "",0 );		
							  ?>	
                               
                              </td>
                        	<td align="right">Exchange Rate</td>
                              <td>
                             <input style="width:160px;" type="text" class="text_boxes"  name="txt_exchange_rate" id="txt_exchange_rate"  />  
                              </td>
                       
                        	<td  align="right">Supplier Name</td>
                                <td>
                               <?
							   		echo create_drop_down( "cbo_supplier_name", 172, "select id,supplier_name from lib_supplier where status_active =1 and is_deleted=0 order by supplier_name","id,supplier_name", 1, "-- Select Supplier --", $selected, "",0 );
							   ?> 
                                 </td> 
                              </tr>
                            
                             
                        
                        
                 
                        <tr>
                            <td  width="130" align="right">Delivery Date</td>
                                <td width="170">
                                    <input class="datepicker" type="text" style="width:160px" name="txt_delivery_date" id="txt_delivery_date"/>	
                                </td>
                                 <td  align="right">Pay Mode</td>
                                <td>
                               <?
							   		echo create_drop_down( "cbo_pay_mode", 172, $pay_mode,"", 1, "-- Select Pay Mode --", 3, "","" );
							   ?> 
                                 </td>
                             <td  width="130" height="" align="right"> Source </td>              <!-- 11-00030  -->
                                <td  width="170" >
                                	<?
							   		echo create_drop_down( "cbo_source", 172, $source,"", 1, "-- Select Source --", "", "","" );
							   ?>
                                
                                 
                                </td>
                                
                                                             
                        </tr>
                         
                        <tr>
                        	<td align="right">Attention</td>   
                        	<td align="left" height="10" colspan="3">
                            	<input class="text_boxes" type="text" style="width:97%;"  name="txt_attention" id="txt_attention"/>
                            	<input type="hidden" class="image_uploader" style="width:162px" value="Lab DIP No" onClick="openmypage('requires/sample_booking_non_order_controller.php?action=lapdip_no_popup','Lapdip No','lapdip')">
                                <input type="hidden" id="id_approved_id">
                             <td align="right">Ready To Approved</td>  
                        	<td align="center" height="10">
                              <?
							   		echo create_drop_down( "cbo_ready_to_approved", 172, $yes_no,"", 1, "-- Select--", 2, "","","" );
							  ?>
                            </td>
                            </td>
                             
                        </tr>
                        
                        <tr>
                        	<td align="right" class="">Team Leader</td>   
    						<td>
                             <?  
							  	echo create_drop_down( "cbo_team_leader", 172, "select id,team_leader_name from lib_marketing_team where status_active =1 and is_deleted=0 order by team_leader_name","id,team_leader_name", 1, "-- Select Team --", $selected, "load_drop_down( 'requires/sample_booking_non_order_controller', this.value, 'cbo_dealing_merchant', 'div_marchant' ) " );
								?>		
                            </td>
							<td align="right" class="">Dealing Merchant</td>   
    						<td id="div_marchant" > 
                            <? 
							  	echo create_drop_down( "cbo_dealing_merchant", 172, $blank_array,"", 1, "-- Select Team Member --", $selected, "" );
								?>	
                           </td>
                           
                            <td align="right"></td>
                        	<td>
                            	<input type="button" id="set_button" class="image_uploader" style="width:160px;" value="Terms & Condition/Notes" onClick="open_terms_condition_popup('requires/sample_booking_non_order_controller.php?action=terms_condition_popup','Terms Condition')" />
                            </td>
                        </tr>
                        
                        
                        
                        
                        <tr>
                        	<td align="center" colspan="6" valign="top" id="app_sms2" style="font-size:18px; color:#F00">
                            	
                            </td>
                        </tr>
                        <tr>
                        	<td align="center" colspan="6" valign="middle" class="button_container">
                              <? echo load_submit_buttons( $permission, "fnc_fabric_booking", 0,0 ,"reset_form('fabricbooking_1','','booking_list_view','cbo_pay_mode,3*cbo_currency,2*cbo_ready_to_approved,2')",1) ; ?>
                            </td>
                        </tr>
                        <tr>
                        	<td align="center" colspan="6" height="10">
                           
                           </td>
                        </tr>
                    </table>
                 
              </fieldset>
              </form>
              
              <form name="orderdetailsentry_2"  autocomplete="off" id="orderdetailsentry_2">
            	<fieldset style="width:950px;">
                <legend>Sample Booking (Without Order) </legend>
               
            		<table  width="900" cellspacing="2" cellpadding="0" border="0">
                    
                    
                    <tr>
                            
                                <td  width="130" height="" align="right" > Style ref </td>              <!-- 11-00030  -->
                                <td  width="170" >
                                	<input type="text" id="txt_style_no"    name="txt_style_no"  class="text_boxes" style="width:160px" onDblClick="open_sample_popup()" placeholder="Double Click to Search" readonly/>
                                  <input type="hidden" id="txt_style"    name="txt_style"  class="text_boxes" style="width:100px" onDblClick="open_sample_popup()" placeholder="Double Click to Search" readonly/>
                                
                                 
                                </td>
                              <td  align="right">Style Des.</td>
                              <td>
                             <input type="text" id="txt_style_des"    name="txt_style_des"  class="text_boxes" style="width:160px"/> 
                              </td>
                         <td align="right" class="must_entry_caption">Sample type</td>   
   						 <td> 
                             <?
													echo create_drop_down( "cbo_sample_type", 172, "select sample_name,id from lib_sample where is_deleted=0 and status_active=1 order by sample_name","id,sample_name", '1', "--Select--", '', "","",'' );
												?>
                         </td>
							
                        </tr>
                       <tr>
                       <td align="right" class="must_entry_caption">Body Part</td>
                        	<td>
                            	   <?  echo create_drop_down( "cbo_body_part", 172, $body_part,"", 1, "--Select--", $selected, "" ); ?>	
                            </td>
                               <td align="right" width="130" >
                               Color Type
                                </td>
                                <td>	
                                     <?  echo create_drop_down( "cbo_color_type", 172, $color_type,"", 1, "--Select--", $selected, "" ); ?>
                                </td>
                       <td  width="130" align="right" class="must_entry_caption">Fabric Description</td>
                                <td  width="170" id="fabricdescription_id_td"> 
                              <input type="hidden" id="libyarncountdeterminationid"  name="libyarncountdeterminationid" class="text_boxes" style="width:10px" />
                                    <input type="hidden" id="construction"  name="construction" class="text_boxes" style="width:10px" />
                                    <input type="hidden" id="composition"  name="composition" class="text_boxes" style="width:10px"  />
                                    <input type="hidden" id="yarnbreackdown" name="yarnbreackdown"  class="text_boxes" style="width:90px"/> 
                                    <input type="hidden" id="process_loss_method" name="process_loss_method"  class="text_boxes" style="width:90px"/> 
							  <input type="text" id="txt_fabricdescription"    name="txt_fabricdescription"  class="text_boxes" style="width:160px" onDblClick="open_fabric_decription_popup()" readonly/>
                              </td>
                                                          
                        </tr>
                      
                        
                        <tr>
                        <td align="right">GSM</td>
                              <td>
                               <input name="txt_gsm" id="txt_gsm" class="text_boxes" type="text" value=""  style="width:160px "/>	
                               
                              </td>
                              <td align="right">Gmts Color</td>
                              <td>
                               <input name="txt_gmt_color" id="txt_gmt_color" class="text_boxes" type="text" value=""  style="width:160px "/>
                              </td>
                        	<td align="right">Fabric Color</td>
                              <td id="fabriccolor_id_id_td" >
                               <input name="txt_color" id="txt_color" class="text_boxes" type="text" value=""  style="width:160px "/>
                              </td>
                              </tr>
                            
                             
                        
                        
                 
                        <tr>
                        <td  align="right">Gmts size</td>
                                <td>
                              <input name="txt_gmts_size" id="txt_gmts_size" class="text_boxes" type="text" value=""  style="width:160px "/>
                              </td>
                        <td  align="right">Item size</td>
                                <td id="itemsize_id_td">
                              <input name="txt_size" id="txt_size" class="text_boxes" type="text" value=""  style="width:160px "/>
                              </td>
                              
                            <td  width="130" align="right">Dia/ Width</td>
                            <td>
                              <input name="txt_dia_width" id="txt_dia_width" class="text_boxes" type="text" value=""  style="width:160px "/>
                              </td>
                                 
                             
                                
                                                             
                        </tr>
                         
                        <tr>
                        <td  align="right" class="must_entry_caption">Finish Fabric</td>
                                <td>
                               <input name="txt_finish_qnty" id="txt_finish_qnty" class="text_boxes_numeric" type="text" onChange="calculate_requirement()" value=""  style="width:160px "/>
                                 </td>
                        <td  width="130" height="" align="right"> Process loss </td>              <!-- 11-00030  -->
                                <td  width="170" >
                                	<input name="txt_process_loss" id="txt_process_loss" class="text_boxes_numeric" type="text" value="" onChange="calculate_requirement()"   style="width:160px "/>
                                
                                 
                                </td>
                        	<td align="right">Gray Fabric</td>   
                        	<td align="left" height="10">
                            	<input name="txt_grey_qnty" id="txt_grey_qnty" class="text_boxes_numeric" type="text" value=""  style="width:160px " readonly/>
                             
                            
                           
                             
                        </tr>
                        <tr>
                         <td align="right">Article Number</td>  
                        	<td align="center" height="10">
                            <input name="txt_article_no" class="text_boxes" ID="txt_article_no" style="width:160px" maxlength="50" title="Maximum 50 Character">
                            </td>
                        <td align="right">Rate</td>  
                        	<td align="center" height="10">
                             <input name="txt_rate" id="txt_rate" class="text_boxes_numeric" type="text" value=""  style="width:160px " onChange="calculate_requirement()" />
                            </td>
                        <td align="right">Amount</td>  
                        	<td align="center" height="10">
                             <input name="txt_amount" id="txt_amount" class="text_boxes_numeric" type="text" value=""  style="width:160px " readonly/>
                              
                            </td>
                        	
                            
                           
                             
                        </tr>
                        <tr>
                         <td align="right">Yarn Details</td>  
                        	<td align="center" height="10">
                            <input name="txt_yarn_details" class="text_boxes" ID="txt_yarn_details" style="width:160px" maxlength="200" title="Maximum 200 Character">
                            </td>
                        <td align="right">Remarks</td>  
                        	<td align="center" height="10">
                             <input name="txt_remarks" id="txt_remarks" class="text_boxes" type="text" value=""  style="width:160px" maxlength="200" title="Maximum 200 Character"  />
                             <input type="hidden" id="update_id_details">
                            </td>
                        <td align="right">&nbsp;</td>  
                        	<td align="center" height="10">&nbsp;
                            
                            </td>
                        	
                            
                           
                             
                        </tr>
                        
                        
                        
                        
                        
                        <tr>
                        	<td align="center" colspan="6" valign="middle" class="button_container">
                              <?
									
									echo load_submit_buttons( $permission, "fnc_fabric_booking_dtls", 0,0 ,"reset_form('orderdetailsentry_2','','','','')",2) ; 
									?>
                                    <input type="button" value="Print Booking" onClick="generate_fabric_report()"  style="width:100px" name="print" id="print" class="formbutton" />
                            </td>
                        </tr>
                        
                    </table>
                 
              </fieldset>
              </form>
              
              <fieldset style="width:1670px;">
                <legend>Booking Entry</legend>
            	
                    <table style="border:none" cellpadding="0" cellspacing="2" border="0">
                             
                            <tr align="center">
                                <td colspan="12" id="booking_list_view">
                                </td>	
                        	</tr>
                       </table>
            	
                </fieldset>
                
	</div>
   <div style="display:none" id="data_panel"></div>
   

</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>