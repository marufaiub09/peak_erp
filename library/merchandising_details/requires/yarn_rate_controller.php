<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$permission=$_SESSION['page_permission'];
if ($action=="search_list_view")
{
	$lib_sup=return_library_array("select supplier_name,id from lib_supplier", "id", "supplier_name");
	$lib_yarn_count=return_library_array( "select yarn_count,id from lib_yarn_count", "id", "yarn_count");
	$sql="select id,supplier_id,yarn_count,composition,percent,yarn_type,rate,effective_date from lib_yarn_rate where status_active=1 and is_deleted=0 order by id";
	$arr=array (0=>$lib_sup,1=>$lib_yarn_count,2=>$composition,4=>$yarn_type);
	echo  create_list_view ( "list_view", "Supplier Name,Yarn Count,Composition,Percent,Type,Rate/KG,Effective Date", "250,100,300,40,110,50,70","1080","350",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form'",1, "supplier_id,yarn_count,composition,0,yarn_type,0,0", $arr , "supplier_id,yarn_count,composition,percent,yarn_type,rate,effective_date", "../merchandising_details/requires/yarn_rate_controller",'setFilterGrid("list_view",-1);','0,0,0,1,0,2,3') ;
}
    
if ($action=="load_php_data_to_form")
{
	$nameArray=sql_select( "select id,supplier_id,yarn_count,composition,percent,yarn_type,rate,effective_date from lib_yarn_rate where id='$data'" );
	foreach ($nameArray as $inf)
	{
		echo "document.getElementById('cbo_supplier').value  = '".$inf[csf("supplier_id")]."';\n";
		echo "document.getElementById('cbocountcotton').value = '".$inf[csf("yarn_count")]."';\n";    
		echo "document.getElementById('cbocompone').value = '".$inf[csf("composition")]."';\n";
		echo "document.getElementById('percentone').value = '".$inf[csf("percent")]."';\n";   
		echo "document.getElementById('cbotypecotton').value  = '".$inf[csf("yarn_type")]."';\n";
		echo "document.getElementById('txt_rate').value  = '".$inf[csf("rate")]."';\n";
		echo "document.getElementById('txt_date').value  = '".change_date_format($inf[csf("effective_date")],'dd-mm-yyyy','-')."';\n";
		echo "document.getElementById('update_id').value  = '".$inf[csf("id")]."';\n";
	    echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_yarn_rate',1);\n";  
	}
}
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if ($operation==0)  // Insert Here
	{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$id=return_next_id( "id", "lib_yarn_rate", 1 ) ;
			$field_array1= "id,supplier_id,yarn_count,composition,percent,yarn_type,rate,effective_date,inserted_by,insert_date,status_active,is_deleted";
			$data_array1="(".$id.",".$cbo_supplier.",".$cbocountcotton.",".$cbocompone.",".$percentone.",".$cbotypecotton.",".$txt_rate.",".$txt_date.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."','1','0')";
			$rID=sql_insert("lib_yarn_rate",$field_array1,$data_array1,0);
			if($db_type==0)
			{
				if($rID){
					mysql_query("COMMIT");  
					echo "0**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			
			if($db_type==2 || $db_type==1 )
			{
				if($rID)
				{
					oci_commit($con);  
					echo "0**".$rID;
				}
			else{
					oci_rollback($con); 
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
	}
	
	else if ($operation==1)   // Update Here
	{
		
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$field_array1= "supplier_id*yarn_count*composition*percent*yarn_type*rate*effective_date*updated_by*update_date*status_active*is_deleted";
			$data_array1="".$cbo_supplier."*".$cbocountcotton."*".$cbocompone."*".$percentone."*".$cbotypecotton."*".$txt_rate."*".$txt_date."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'1'*'0'";
			 $rID=sql_update("lib_yarn_rate",$field_array1,$data_array1,"id","".$update_id."",0);
			
			if($db_type==0)
			{
				if($rID){
					mysql_query("COMMIT");  
					echo "1**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
				
				if($rID)
				{
					oci_commit($con);  
					echo "1**".$rID;
				}
				else{
					oci_rollback($con); 
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
	}
	
	else if ($operation==2) // Delete Here
	{
		
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$field_array1="updated_by*update_date*status_active*is_deleted";
			$data_array1="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
			$rID=sql_delete("lib_yarn_rate",$field_array1,$data_array1,"id","".$update_id."",1);
			
			if($db_type==0)
			{
				if($rID){
					mysql_query("COMMIT");  
					echo "2**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID)
				{
					oci_commit($con);   
					echo "2**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
	    }
	}
?>