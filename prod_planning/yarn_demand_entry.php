<?php
/*-------------------------------------------- Comments

Purpose			: 	This form will create Yarn Demand Entry
					
Functionality	:	
				

JS Functions	:

Created by		:	Fuad Shahriar 
Creation date 	: 	19-08-2013
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

session_start(); 
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Yarn Requisition Entry", "../", 1, 1,'','','');

?>	
<script>

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";  
var permission = '<?php echo $permission; ?>';
 
function fnc_yarn_demand_entry(operation)
{
	if(operation==4)
	{ 
		print_report( $('#update_id').val()+"**"+$('#cbo_company_id').val(), "print", "requires/yarn_demand_entry_controller" ) 
		return;
	}
	
	if (form_validation('cbo_company_id*txt_demand_date*txt_requisition_no','Comapny Name*Demand Date*Requisition No')==false)
	{
		return;
	}
	
	var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_demand_no*cbo_company_id*cbo_location*txt_demand_date*txt_requisition_no*txt_demand_qnty*save_data*update_id*update_dtls_id',"../");
	
	freeze_window(operation);
	
	http.open("POST","requires/yarn_demand_entry_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange=fnc_yarn_demand_entry_Reply_info;
}

function fnc_yarn_demand_entry_Reply_info()
{
	if(http.readyState == 4) 
	{
		 //alert(http.responseText);return;
		var reponse=trim(http.responseText).split('**');	
		
		show_msg(reponse[0]);
		
		if((reponse[0]==0 || reponse[0]==1))
		{
			reset_form('','demand_items_list_view','txt_requisition_no*txt_demand_qnty*save_data*update_dtls_id','','disable_enable_fields(\'cbo_company_id\',1)');
			var cbo_company_id = $('#cbo_company_id').val();
			$('#txt_demand_no').val(reponse[1]);
			$('#update_id').val(reponse[2]);
			show_list_view(reponse[2]+"**"+cbo_company_id,'show_demand_listview','demand_items_list_view','requires/yarn_demand_entry_controller','');
		}
		else if(reponse[0]==2)
		{
			reset_form('demandEntry_1','demand_items_list_view','','','disable_enable_fields(\'cbo_company_id\',0)');
		}
		else if(reponse[0]==17)	
		{
			alert("Demand Qnty Exceeds Requisition Qnty");
		}
	
		set_button_status(reponse[3], permission, 'fnc_yarn_demand_entry',1,reponse[4]);
		release_freezing();	
	}
}
	
function openmypage_reqsn()
{
	var companyID = $('#cbo_company_id').val();
	var txt_requisition_no = $('#txt_requisition_no').val();
	var save_data = $('#save_data').val();

	if (form_validation('cbo_company_id','Company')==false)
	{
		return;
	}
	
	var page_link='requires/yarn_demand_entry_controller.php?action=yarn_reqsn_popup&txt_requisition_no='+txt_requisition_no+'&companyID='+companyID+'&save_data='+save_data;
	var title='Yarn Requisition Info';
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px,height=390px,center=1,resize=1,scrolling=0','');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var reqsn_no=this.contentDoc.getElementById("reqsn_no").value;
		var save_data=this.contentDoc.getElementById("save_data").value;
		var tot_demand_qnty=this.contentDoc.getElementById("tot_demand_qnty").value;
		
		$('#txt_requisition_no').val(reqsn_no);
		$('#save_data').val(save_data);
		$('#txt_demand_qnty').val(tot_demand_qnty);
	}
}

function openmypage_demandNo()
{
	if (form_validation('cbo_company_id','Company')==false)
	{
		return;
	}
	else
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var title = 'Demand No Info';
		var page_link = 'requires/yarn_demand_entry_controller.php?cbo_company_id='+cbo_company_id+'&action=systemId_popup';

		emailwindow=dhtmlmodal.open('EmailBox', 'iframe',  page_link, title, 'width=650px,height=350px,center=1,resize=0,scrolling=0','')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var demand_id=this.contentDoc.getElementById("hidden_sys_id").value;
			
			reset_form('demandEntry_1','demand_items_list_view','','','','');
			get_php_form_data(demand_id, "populate_data_from_demand_update", "requires/yarn_demand_entry_controller" );
			show_list_view(demand_id+"**"+cbo_company_id,'show_demand_listview','demand_items_list_view','requires/yarn_demand_entry_controller','');
		}
	}
}

</script>
</head>

<body onLoad="set_hotkey();">
    <div style="width:100%;" align="center">
        <?php echo load_freeze_divs ("../",$permission); ?>
        <fieldset style="width:850px;"><br>
        <legend>Demand Entry</legend> 
            <form name="demandEntry_1" id="demandEntry_1"> 
                <fieldset style="width:820px;">
                    <table width="810" align="center" border="0">
                        <tr>
                            <td colspan="3" align="right"><strong>Demand No</strong></td>
                            <td colspan="3" align="left">
                                <input type="text" name="txt_demand_no" id="txt_demand_no" class="text_boxes" style="width:150px;" placeholder="Double click to search" onDblClick="openmypage_demandNo();"  readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Company Name</td>
                            <td>
                                <?php
                                    echo create_drop_down( "cbo_company_id", 152, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", 0, "load_drop_down('requires/yarn_demand_entry_controller', this.value, 'load_drop_down_location', 'location_td' );" );
                                ?>
                            </td>
                            <td>Location</td>
                            <td id="location_td">
                                <?php
                                    echo create_drop_down("cbo_location", 152, $blank_array,"", 1,"-- Select Location --", 0,"");
                                ?>
                            </td>
                            <td class="must_entry_caption">Demand Date</td>
                            <td>
                                <input type="date" name="txt_demand_date" id="txt_demand_date" class="datepicker" style="width:140px;" tabindex="6" />
                            </td>
                        </tr>
                     </table>
                </fieldset>                 
                <fieldset style="width:810px; margin-top:10px">
                <legend>Requisition Details</legend>
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td width="110" class="must_entry_caption">Requisition No</td>
                            <td width="183" align="left">						 
                                <input type="text" name="txt_requisition_no" id="txt_requisition_no" style="width:140px" class="text_boxes" readonly placeholder="Single Click" onClick="openmypage_reqsn()"/>
                            </td> 
                            <td width="110">Demand Qnty</td>
                            <td align="left">						 
                                <input type="text" name="txt_demand_qnty" id="txt_demand_qnty" style="width:90px" class="text_boxes_numeric" placeholder="Display" disabled/>
                            </td>                 	</tr>
                    </table>
                </fieldset> 
                <table width="810">
                    <tr>
                        <td colspan="4" align="center" class="button_container">
                            <?php 
                                echo load_submit_buttons($permission, "fnc_yarn_demand_entry", 0,1,"reset_form('demandEntry_1','demand_items_list_view','','','disable_enable_fields(\'cbo_company_id\',0)');",1);
                            ?> 
                            <input type="hidden" name="save_data" id="save_data"/>
                            <input type="hidden" name="update_id" id="update_id"/>
                            <input type="hidden" name="update_dtls_id" id="update_dtls_id"/>
                        </td>	  
                    </tr>
                </table>
            </form>
            <div id="demand_items_list_view" style="margin-top:10px"></div>
        </fieldset>
    </div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>