<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$batch_arr = return_library_array("select id, batch_no from pro_batch_create_mst","id","batch_no");
$machine_name=return_library_array( "select machine_no,id from  lib_machine_name where is_deleted=0", "id", "machine_no"  );
$product_array=return_library_array( "select id, product_name_details from product_details_master where item_category_id=13",'id','product_name_details');
if ($action=="load_drop_floor")
{
	$data=explode('_',$data);
	$loca=$data[0];
	$com=$data[1];
	//echo "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]'  order by floor_name";die;
	echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]' and production_process=4 order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected,"load_drop_down( 'requires/singeing_controller', document.getElementById('cbo_company_id').value+'**'+this.value, 'load_drop_machine', 'machine_td' );" );     	 
	exit();
}
if ($action=="load_drop_machine")
{
	$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
	$data=explode('**',$data);
	$com=$data[0];
	$floor=$data[1];
	if($db_type==2)
	{
	echo create_drop_down( "cbo_machine_name", 135, "select id,machine_no || '-' || brand as machine_name from lib_machine_name where category_id=4 and company_id=$com and floor_id=$floor and status_active=1 and is_deleted=0 and is_locked=0 order by machine_name","id,machine_name", 1, "-- Select Machine --", $selected, "get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+this.value, 'populate_data_from_machine', 'requires/singeing_controller' );","" );
	}
	else if($db_type==0)
	{
	echo create_drop_down( "cbo_machine_name", 135, "select id,concat(machine_no, '-', brand) as machine_name from lib_machine_name where category_id=4 and company_id=$com and floor_id=$floor and status_active=1 and is_deleted=0 and is_locked=0 order by machine_name","id,machine_name", 1, "-- Select Machine --", $selected, "get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+this.value, 'populate_data_from_machine', 'requires/singeing_controller' );","" );
	}
	exit();
}
if ($action=="populate_data_from_machine")
{ 
	$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
	$ex_data=explode('**',$data);
	 $sql_res="select id, floor_id, machine_group from lib_machine_name where id=$ex_data[2] and category_id=4 and company_id=$ex_data[0] and  floor_id=$ex_data[1] and status_active=1 and is_deleted=0 ";
	$nameArray=sql_select($sql_res);
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_machine_no').value 			= '".$floor_arr[$row[csf("floor_id")]]."';\n";
		echo "document.getElementById('txt_mc_group').value 			= '".$row[csf("machine_group")]."';\n";
	}
	exit();
}
if ($action=="batch_number_popup")
{
	echo load_html_head_contents("Batch Number Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id)
		{ 
			$('#hidden_batch_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
<div align="center" style="width:800px;">
    <form name="searchbatchnofrm"  id="searchbatchnofrm">
        <fieldset style="width:790px;">
        <legend>Enter search words</legend>
           <table cellpadding="0" cellspacing="0" width="770" border="1" rules="all" class="rpt_table">
                <thead>
                    <tr>
                        <th colspan="4">
                          <?php
							  echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" );
                          ?>
                        </th>
                    </tr>                	
                    <tr>
                        <th width="150px">Batch Type</th>
                        <th width="150px">Batch No</th>
                        <th width="220px">Batch Date Range</th>
                        <th>
                            <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                            <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                            <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" value="">
                        </th>
                    </tr>
                </thead>
                <tr>
                    <td align="center">	
                        <?php
                            echo create_drop_down( "cbo_batch_type", 150, $order_source,"",1, "--Select--", 0,0,0 );
                        ?>
                    </td>
                     <td align="center">				
                        <input type="text" style="width:140px" class="text_boxes"  name="txt_search_batch" id="txt_search_batch" />	
                    </td> 
                    <td align="center">
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px;">To<input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px;">
                    </td>
                   
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view (document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value+'_'+document.getElementById('cbo_batch_type').value+'_'+document.getElementById('txt_search_batch').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_batch_search_list_view', 'search_div', 'singeing_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </table>
            <table width="100%" style="margin-top:5px;">
                <tr>
                    <td colspan="4">
                        <div style="width:100%; margin-left:3px;" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}
if($action=="create_batch_search_list_view")
{
	$data = explode("_",$data);
	$start_date =$data[0];
	$end_date =$data[1];
	$company_id =$data[2];
	$batch_type =$data[3];
	$batch_no =$data[4];
	$search_type =$data[5];
 	
	if($search_type==1)
	{
		if ($batch_no!='') $batch_cond=" and batch_no='$batch_no'"; else $batch_cond="";
	}
	else if($search_type==4 || $search_type==0)
	{
		if ($batch_no!='') $batch_cond=" and batch_no like '%$batch_no%'"; else $batch_cond="";
	}
	else if($search_type==2)
	{
		if ($batch_no!='') $batch_cond=" and batch_no like '$batch_no%'"; else $batch_cond="";
	}
	else if($search_type==3)
	{
		if ($batch_no!='') $batch_cond=" and batch_no like '%$batch_no'"; else $batch_cond="";
	}	
	if($batch_type==0)
		$search_field_cond_batch="and entry_form in (0,36)";
	else if($batch_type==1)
		$search_field_cond_batch="and entry_form=0";
	else if($batch_type==2)
		$search_field_cond_batch="and entry_form=36";
	//echo $search_field_cond_batch;die;
	if($db_type==2)
		{	
		if ($start_date!="" &&  $end_date!="") $batch_date_con = " and batch_date between '".change_date_format($start_date, "mm-dd-yyyy", "-",1)."' and '".change_date_format($end_date, "mm-dd-yyyy", "-",1)."'"; else $batch_date_con ="";
		
		if($batch_type==0 || $batch_type==2)
			{
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,listagg(cast(a.order_no as varchar2(4000)),',') within group (order by a.order_no) as po_no from  subcon_ord_dtls a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
			else
			{
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,listagg(cast(a.po_number as varchar2(4000)),',') within group (order by a.po_number) as po_no from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
		}
		if($db_type==0)
		{	
		if ($start_date!="" &&  $end_date!="") $batch_date_con = " and batch_date between '".change_date_format($start_date, "yyyy-mm-dd", "-",1)."' and '".change_date_format($end_date, "yyyy-mm-dd", "-")."'"; else $batch_date_con ="";
		
		if($batch_type==0 || $batch_type==2)
			{
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,group_concat( distinct a.order_no )  as po_no from  subcon_ord_dtls a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
			else
			{
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,group_concat(distinct a.po_number)  as po_no from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
		}
	$po_num=array();
	
	foreach($sql_po as $row_po_no)
	{
	$po_num[$row_po_no[csf('mst_id')]]['po_no']=$row_po_no[csf('po_no')];
	$po_num[$row_po_no[csf('mst_id')]]['job_no_mst']=$row_po_no[csf('job_no_mst')];
		
	} 	//and company_id=$company_id
	$sql = "select id, batch_no, batch_date, batch_weight, booking_no, extention_no, color_id, batch_against, re_dyeing_from from pro_batch_create_mst where batch_for in(0,1) and batch_against<>4 and status_active=1 and is_deleted=0 $search_field_cond_batch $batch_date_con $batch_cond order by id desc"; 
	//echo $sql;//die; 
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Batch No</th>
                <th width="80">Extention No</th>
                <th width="80">Batch Date</th>
                <th width="90">Batch Qnty</th>
                <th width="115">Job No</th>
                <th width="80">Color</th>
                <th>Po No</th>
            </thead>
        </table>
        <div style="width:770px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					$po_no='';
					if($selectResult[csf('re_dyeing_from')]==0)
					{	
						$po_no=implode(",",array_unique(explode(",",$po_num[$selectResult[csf('id')]]['po_no'])));
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
							<td width="40" align="center"><?php echo $i; ?></td>	
							<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
                            <td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?></p></td>
							<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
							<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
                            <td width="115"><p><?php echo $po_num[$selectResult[csf('id')]]['job_no_mst']; ?></p></td>
							<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
							<td><?php echo $po_no; ?></td>	
						</tr>
						<?php
						$i++;
					}
					else
					{
						$sql_re= "select id, batch_no, batch_date, batch_weight, booking_no, MAX(extention_no) as extention_no, color_id, batch_against, re_dyeing_from from pro_batch_create_mst where  batch_for in(0,1) and entry_form in(0,36) and batch_against<>4 and status_active=1 and is_deleted=0 and id=".$selectResult[csf('re_dyeing_from')]." group by id, batch_no, batch_date, batch_weight, booking_no,color_id, batch_against,re_dyeing_from ";
						$dataArray=sql_select( $sql_re );
						foreach($dataArray as $row)
						{
							if($row[csf('re_dyeing_from')]==0)
							{
								$po_no=implode(",",array_unique(explode(",",$po_num[$selectResult[csf('id')]]['po_no'])));
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
									<td width="40" align="center"><?php echo $i; ?></td>	
									<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
									<td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?></p></td>
									<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
									<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
                                    <td width="115"><p><?php echo $po_num[$selectResult[csf('id')]]['job_no_mst']; ?></p></td>
									<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
									<td><?php echo $po_no; ?></td>	
								</tr>
								<?php
								$i++;
							}
						}
					}
				}
			?>
            </table>
        </div>
	</div>           
<?php
exit();
}
if($action=='populate_data_from_batch')
{ 	
	$ex_data=explode('_',$data);
	//$load_unload=$ex_data[0];
	//echo $batch_id;die;
	$batch_id_ex=$ex_data[0]; 
	$batch_no=$ex_data[1]; 
	if($db_type==0) $select_group_row="  order by a.id desc limit 0,1"; 
	else if($db_type==2) $select_group_row="and  rownum<=1 group by a.id,a.batch_no,a.batch_weight,a.color_id, 
	a.booking_without_order,a.batch_date,a.color_range_id,a.insert_date,a.batch_for,a.company_id,a.process_id,a.entry_form ";
	if($db_type==0) $pop_batch="order by a.id";
	else if($db_type==2) $pop_batch=" group by a.id,a.batch_no, a.batch_weight,batch_date,a.color_id,a.color_range_id,a.insert_date,a.batch_for,a.process_id,a.company_id, a.booking_without_order ,a.entry_form 	order by a.id";
	if($db_type==0) $select_list=" group_concat(distinct(b.po_id)) as po_id"; 
	else if($db_type==2) $select_list="listagg(b.po_id,',') within group (order by b.po_id) as po_id";
	if($batch_no!='')
		{ 
	$data_array=sql_select("select MAX(a.id) as id,a.batch_no,a.entry_form,a.company_id,a.batch_for,a.batch_date,a.process_id as process_id_batch, a.batch_weight,Max(a.extention_no) as extention_no, 
	a.color_id,a.color_range_id,a.insert_date, a.booking_without_order, sum(b.batch_qnty) as batch_qnty, $select_list from pro_batch_create_mst a,pro_batch_create_dtls b where a.id=b.mst_id  and a.entry_form in(0,36) and a.id='$batch_id_ex' $select_group_row");
		}
	else
	{
	$data_array=sql_select("select a.id,a.batch_no,a.entry_form,a.batch_for,a.company_id,a.process_id as process_id_batch, a.batch_weight,Max(a.extention_no) as extention_no,a.batch_date, 
	a.color_id,a.color_range_id,a.insert_date, a.booking_without_order, sum(b.batch_qnty) as batch_qnty, $select_list from pro_batch_create_mst a,pro_batch_create_dtls b where a.id='$batch_id_ex' and a.entry_form in(0,36)  and a.id=b.mst_id  $pop_batch");	
	}
	if($db_type==0) $select_f_group=""; 
	else if($db_type==2) $select_f_group="group by a.job_no_mst, b.buyer_name";
	if($db_type==0) $select_listagg="group_concat(distinct(a.po_number)) as po_no"; 
	else if($db_type==2) $select_listagg="listagg(cast(a.po_number as varchar2(4000)),',') within group (order by a.po_number) as po_no";
	if($db_type==0) $select_listagg_subcon="group_concat(distinct(a.order_no)) as po_no"; 
	else if($db_type==2) $select_listagg_subcon="listagg(cast(a.order_no as varchar2(4000)),',') within group (order by a.order_no) as po_no";
	foreach ($data_array as $row)
	{  	//if($row[csf('batch_against')])
		$pro_id=implode(",",array_unique(explode(",",$row[csf('po_id')])));
		echo "load_drop_down( 'requires/singeing_controller', '".$row[csf("company_id")]."', 'load_drop_floor', 'floor_td' );\n";
		echo "document.getElementById('txt_batch_no').value 		= '".$row[csf("batch_no")]."';\n";
		echo "document.getElementById('hidden_batch_id').value 		= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_batch_ID').value 		= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_color').value 			= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('txt_ext_id').value 			= '".$row[csf("extention_no")]."';\n";
		echo "document.getElementById('txt_batch_against').value 	= '".$batch_for[$row[csf("batch_for")]]."';\n";
		echo "document.getElementById('txt_color_range').value 		= '".$color_range[$row[csf("color_range_id")]]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		if($row[csf("entry_form")]==36)
		{
			
		$batch_type="<b> SUBCONTRACT ORDER BATCH</b>";
		$result_job=sql_select("select $select_listagg_subcon, b.subcon_job as job_no_mst, b.party_id as buyer_name from  subcon_ord_dtls a, 
		 subcon_ord_mst b where a.job_no_mst=b.subcon_job and a.id in(".$pro_id.") and b.status_active=1 and b.is_deleted=0 and a.status_active=1 
		and a.is_deleted=0 group by b.subcon_job, b.party_id");
		}
		else
		{
		$batch_type="<b> SELF ORDER BATCH </b>";
		$result_job=sql_select("select $select_listagg, a.job_no_mst, b.buyer_name from wo_po_break_down a, 
		wo_po_details_master b where a.job_no_mst=b.job_no and a.id in(".$pro_id.") and b.status_active=1 and b.is_deleted=0 and a.status_active=1 
		and a.is_deleted=0 $select_f_group");
		}
		echo "document.getElementById('batch_type').innerHTML 			= '".$batch_type."';\n";
		
		$insert_t=explode(' ',$row[csf("insert_date")]);
		//print_r($insert_t);
		echo "document.getElementById('txt_batch_time').value 		= '".change_date_format($insert_t[0]).'  '. $insert_t[1]."';\n";
		echo "document.getElementById('txt_batch_date').value 		= '".change_date_format($row[csf("batch_date")])."';\n";
	
		$pro_id2=implode(",",array_unique(explode(",",$result_job[0][csf("po_no")])));
		echo "document.getElementById('txt_buyer').value 			= '".$buyer_arr[$result_job[0][csf("buyer_name")]]."';\n";
		echo "document.getElementById('txt_job_no').value 			= '".$result_job[0][csf("job_no_mst")]."';\n";
		echo "document.getElementById('txt_order_no').value 		= '".$pro_id2."';\n";
		$sql_batch=sql_select("select id,batch_no,batch_id,company_id,process_end_date,temparature,stretch,over_feed,feed_in,pinning,speed_min,process_start_date,start_hours,start_minutes,end_hours,end_minutes,machine_id,floor_id,process_id,production_date,shift_name,remarks from pro_fab_subprocess where entry_form=47 and batch_id='".$row[csf("id")]."' ");
		if(count($sql_batch)>0)
			{
				echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',1);\n"; 	
			}
			else
			{
				echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',0);\n"; 	
			}
		foreach($sql_batch as $r_batch)
		{
			echo "document.getElementById('txt_batch_no').value 			= '".$r_batch[csf("batch_no")]."';\n";
			echo "document.getElementById('txt_update_id').value 			= '".$r_batch[csf("id")]."';\n";
			//echo "document.getElementById('txt_update_dtls_id').value 			= '".$r_batch[csf("dtls_id")]."';\n";
			echo "document.getElementById('txt_process_end_date').value 	= '".change_date_format($r_batch[csf("process_end_date")])."';\n";
			echo "document.getElementById('txt_process_date').value 	= '".change_date_format($r_batch[csf("production_date")])."';\n";
			echo "document.getElementById('txt_process_start_date').value 	= '".change_date_format($r_batch[csf("process_start_date")])."';\n";
			/*if($r_batch[csf("id")]!="")
			{
				$process_name='';
				$process_id_array=explode(",",$r_batch[csf("process_id")]);
				foreach($process_id_array as $val)
				{
					if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=",".$conversion_cost_head_array[$val];
				}*/
			echo "document.getElementById('cbo_sub_process').value 			= '".$r_batch[csf("process_id")]."';\n";
			//echo "document.getElementById('txt_process_name').value 			= '".$process_name."';\n";	
			//}
			/*else
			{
			$process_name_batch='';
			$process_id_array=explode(",",$row[csf("process_id_batch")]);
			foreach($process_id_array as $val)
			{
				if($process_name_batch=="") $process_name_batch=$conversion_cost_head_array[$val]; else $process_name_batch.=",".$conversion_cost_head_array[$val];
			}
			echo "document.getElementById('txt_process_id').value 			= '".$row[csf("process_id_batch")]."';\n";
			echo "document.getElementById('txt_process_name').value 			= '".$process_name_batch."';\n";	
			}*/
			//echo "set_multiselect('cbo_sub_process','0','1','".$r_batch[csf('process_id')]."','0');\n";
			$hour=str_pad($r_batch[csf("end_hours")],2,'0',STR_PAD_LEFT);
			$minute=str_pad($r_batch[csf("end_minutes")],2,'0',STR_PAD_LEFT);
			echo "document.getElementById('txt_end_hours').value	= '".$hour."';\n";
			echo "document.getElementById('txt_end_minutes').value	= '".$minute."';\n";
			$start_hour=str_pad($r_batch[csf("start_hours")],2,'0',STR_PAD_LEFT);
			$start_minute=str_pad($r_batch[csf("start_minutes")],2,'0',STR_PAD_LEFT);
			echo "document.getElementById('txt_start_hours').value	= '".$start_hour."';\n";
			echo "document.getElementById('txt_start_minutes').value = '".$start_minute."';\n";
			echo "document.getElementById('txt_temparature').value	= '".$r_batch[csf("temparature")]."';\n";
			echo "document.getElementById('txt_stretch').value	= '".$r_batch[csf("stretch")]."';\n";
			echo "document.getElementById('txt_feed').value	= '".$r_batch[csf("over_feed")]."';\n";
			echo "document.getElementById('txt_feed_in').value	= '".$r_batch[csf("feed_in")]."';\n";
			echo "document.getElementById('txt_pinning').value	= '".$r_batch[csf("pinning")]."';\n";
			echo "document.getElementById('txt_speed').value	= '".$r_batch[csf("speed_min")]."';\n";
			echo "load_drop_down( 'requires/singeing_controller', document.getElementById('cbo_company_id').value+'**'+".$r_batch[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
			echo "document.getElementById('cbo_floor').value = '".$r_batch[csf("floor_id")]."';\n";
			echo "document.getElementById('cbo_machine_name').value = '".$r_batch[csf("machine_id")]."';\n";
			echo "document.getElementById('cbo_shift_name').value	= '".$r_batch[csf("shift_name")]."';\n";
			echo "document.getElementById('txt_remarks').value	= '".$r_batch[csf("remarks")]."';\n";
		}
		exit();
	}
}
if($action=='show_fabric_desc_listview')
{
	//print($data);
	$ex_data=explode('_',$data);
	$batch_id=$ex_data[0];
	//$update_id=$ex_data[1];
	$update_id=$ex_data[2];
	if($db_type==0) $select_group=" group by b.id,b.item_description"; 
	else if($db_type==2) $select_group="group by b.id,b.item_description,b.width_dia_type,b.prod_id";//order by id desc limit 0,1
	$i=1;	
	$sql_result=sql_select("select b.id, b.gsm,b.width_dia_type,b.dia_width,b.const_composition,b.batch_qty,b.prod_id from pro_fab_subprocess_dtls b,pro_fab_subprocess a where a.id=b.mst_id and a.batch_id='$batch_id' and b.entry_page=47 and a.status_active=1 and a.is_deleted=0");
	if(count($sql_result)>0)
	{
		foreach($sql_result as $row)
		{
			//$desc=explode(",",$row[csf('item_description')]);
		?>
			<tr class="general" id="row_<?php echo $i; ?>">
				<td title="<?php echo $desc[1]; ?>"><input type="text" name="txtconscomp_<?php echo $i; ?>" id="txtconscomp_<?php echo $i; ?>" class="text_boxes" style="width:250px;" value="<?php echo $row[csf('const_composition')] ; ?>" disabled/></td>
				<td><input type="text" name="txtgsm_<?php echo $i; ?>" id="txtgsm_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px;" value="<?php echo $row[csf('gsm')]; ?>" /></td>
				<td><input type="text" name="txtbodypart_<?php echo $i; ?>" id="txtbodypart_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo  $row[csf('dia_width')]; //$row[csf('width_dia_type')]; ?>" disabled/></td> 
				<td title="<?php echo $row[csf('width_dia_type')];?>"><input type="text" name="txtdiawidth_<?php echo $i; ?>" id="txtdiawidth_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo $fabric_typee[$row[csf('width_dia_type')]];?>" disabled/>
                 <input type="hidden" name="txtdiawidthID_<?php echo $i; ?>" id="txtdiawidthID_<?php echo $i; ?>" readonly />
                </td>
				<td><input type="text" name="txtbatchqnty_<?php echo $i; ?>" id="txtbatchqnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:90px;" value="<?php echo $row[csf('batch_qty')]; ?>" disabled/>
				 <input type="hidden" name="txtprodid_<?php echo $i; ?>" id="txtprodid_<?php echo $i; ?>"  value="<?php echo $row[csf('prod_id')];?>" />
				  <input type="hidden" name="updateiddtls_<?php echo $i; ?>" id="updateiddtls_<?php echo $i; ?>" class="text_boxes" value="<?php echo $row[csf('id')];?>" readonly />
                </td>
			</tr>
		<?php
			$b_qty+=$row[csf('batch_qty')];
			$i++; 
		}
	}
	else
	{
		$result=sql_select("select b.width_dia_type,b.item_description,b.width_dia_type,b.prod_id, sum(b.batch_qnty) as batch_qnty from pro_batch_create_dtls b,pro_batch_create_mst a  where mst_id='$batch_id' and a.id=b.mst_id and  a.entry_form in(0,36) and b.status_active=1 and b.is_deleted=0  $select_group");
		foreach($result as $row)
		{
			$desc=explode(",",$row[csf('item_description')]);
		?>
			<tr class="general" id="row_<?php echo $i; ?>">
				<td title="<?php echo $desc[1]; ?>"><input type="text" name="txtconscomp_<?php echo $i; ?>" id="txtconscomp_<?php echo $i; ?>" class="text_boxes" style="width:250px;" value="<?php echo $desc[1]; ?>" disabled/></td>
				<td><input type="text" name="txtgsm_<?php echo $i; ?>" id="txtgsm_<?php echo $i; ?>" class="text_boxes_numeric" style="width:60px;" value="<?php echo $desc[2]; ?>" /></td>
				<td><input type="text" name="txtbodypart_<?php echo $i; ?>" id="txtbodypart_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo  $desc[3]; //$row[csf('width_dia_type')]; ?>" disabled/></td> 
				<td title="<?php echo $fabric_typee[$row[csf('width_dia_type')]];?>"><input type="text" name="txtdiawidth_<?php echo $i; ?>" id="txtdiawidth_<?php echo $i; ?>" class="text_boxes" style="width:120px;" value="<?php echo $fabric_typee[$row[csf('width_dia_type')]];?>" disabled/>
                 <input type="hidden" name="txtdiawidthID_<?php echo $i; ?>" id="txtdiawidthID_<?php echo $i; ?>" value="<?php echo $row[csf('width_dia_type')];?>" readonly />
                </td>
				<td><input type="text" name="txtbatchqnty_<?php echo $i; ?>" id="txtbatchqnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:90px;" value="<?php echo $row[csf('batch_qnty')]; ?>" disabled/>
				 <input type="hidden" name="txtprodid_<?php echo $i; ?>" id="txtprodid_<?php echo $i; ?>"  value="<?php echo $row[csf('prod_id')];?>" />
				  <input type="hidden" name="updateiddtls_<?php echo $i; ?>" id="updateiddtls_<?php echo $i; ?>" class="text_boxes" value="<?php //echo $row[csf('id')];?>" readonly />
				</td>
			</tr>
		<?php
			$b_qty+= $row[csf('batch_qnty')];
			$i++;
		}
	}?>
	 <tr>
        <td colspan="4" align="right"><b>Sum:</b> <?php //echo $b_qty; ?> </td>
         <td align="right"><b><?php echo $b_qty; ?> </b></td>
     </tr>
	<?php
	exit();
}
if($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if(str_replace("'","",$update_id)=="")
		$field_array="id,company_id,batch_no,batch_id,batch_ext_no,process_id,process_end_date,production_date,temparature,stretch,over_feed,feed_in,pinning,speed_min,process_start_date,start_hours,start_minutes,end_hours,end_minutes,machine_id,floor_id,entry_form,shift_name,remarks,inserted_by,insert_date";
		$id=return_next_id( "id", "pro_fab_subprocess", 1 ) ;
		$data_array="(".$id.",".$cbo_company_id.",".$txt_batch_no.",".$txt_batch_ID.",".$txt_ext_id.",".$cbo_sub_process.",".$txt_process_end_date.",".$txt_process_date.",".$txt_temparature.",".$txt_stretch.",".$txt_feed.",".$txt_feed_in.",".$txt_pinning.",".$txt_speed.",".$txt_process_start_date.",".$txt_start_hours.",".$txt_start_minutes.",".$txt_end_hours.",".$txt_end_minutes.",".$cbo_machine_name.",".$cbo_floor.",47,".$cbo_shift_name.",".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		//print_r($data_array);die;
		$mst_update_id=str_replace("'","",$id);
		$field_array_dtls="id, mst_id, entry_page, prod_id, const_composition, gsm, dia_width, width_dia_type, batch_qty, inserted_by, insert_date";
		$id_dtls=return_next_id( "id", "pro_fab_subprocess_dtls", 1 ) ;
		//echo $total_row;die;
		for($i=1;$i<=$total_row;$i++)
		{
			$prod_id="txtprodid_".$i;
			$txtconscomp="txtconscomp_".$i;
			$txtgsm="txtgsm_".$i;
			$txtbodypart="txtbodypart_".$i;
			$txtdiawidth="txtdiawidth_".$i;
			$txtbatchqnty="txtbatchqnty_".$i;
			$txtdiawidthID="txtdiawidthID_".$i;
			$Itemprod_id=str_replace("'","",$$prod_id);
			if($data_array_dtls!="") $data_array_dtls.=","; 
			$data_array_dtls.="(".$id_dtls.",".$mst_update_id.",47,".$Itemprod_id.",".$$txtconscomp.",".$$txtgsm.",".$$txtbodypart.",".$$txtdiawidthID.",".$$txtbatchqnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			$id_dtls=$id_dtls+1;
			//print_r($data_array_dtls);die;
		}
		$rID=sql_insert("pro_fab_subprocess",$field_array,$data_array,0);
		$rID2=sql_insert("pro_fab_subprocess_dtls",$field_array_dtls,$data_array_dtls,0);
		//echo "insert into pro_fab_subprocess_dtls (".$field_array_dtls.") values ".$data_array_dtls;die;
		//check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  

				echo "0**".$id."**".$id_dtls;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id."**".$id_dtls;
			}
		}
		if($db_type==2 || $db_type==1 )
		{
		if($rID && $rID2)
			{
				oci_commit($con);
				echo "0**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$update_id=str_replace("'",'',$txt_update_id);
		$field_array_update="company_id*batch_no*batch_id*batch_ext_no*process_id*process_end_date*production_date*temparature*stretch*over_feed*feed_in*pinning*speed_min*process_start_date*start_hours*start_minutes*end_hours*end_minutes*machine_id*floor_id*shift_name*remarks*updated_by*update_date";
		$data_array_update="".$cbo_company_id."*".$txt_batch_no."*".$txt_batch_ID."*".$txt_ext_id."*".$cbo_sub_process."*".$txt_process_end_date."*".$txt_process_date."*".$txt_temparature."*".$txt_stretch."*".$txt_feed."*".$txt_feed_in."*".$txt_pinning."*".$txt_speed."*".$txt_process_start_date."*".$txt_start_hours."*".$txt_start_minutes."*".$txt_end_hours."*".$txt_end_minutes."*".$cbo_machine_name."*".$cbo_floor."*".$cbo_shift_name."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		//echo "insert into subcon_inbound_bill_dtls (".$field_array.") values ".$data_array;die;
//print_r($data_array);die;
		$rID=sql_update("pro_fab_subprocess",$field_array_update,$data_array_update,"id",$update_id,0);
		$field_array_up="mst_id*gsm*updated_by*update_date";
		$field_array_dtls="id, mst_id, entry_page, prod_id, const_composition, gsm, dia_width, width_dia_type, batch_qty, inserted_by, insert_date";
		$add_comma=0;
	   for($i=1; $i<=$total_row; $i++)
		{
			$prod_id="txtprodid_".$i;
			$txtconscomp="txtconscomp_".$i;
			$txtgsm="txtgsm_".$i;
			$txtbodypart="txtbodypart_".$i;
			$txtdiawidth="txtdiawidth_".$i;
			$txtbatchqnty="txtbatchqnty_".$i;
			$updateiddtls="updateiddtls_".$i;
			$Itemprod_id=str_replace("'","",$$prod_id);
			$id_arr[]=str_replace("'",'',$$updateiddtls);
			$data_array_up[str_replace("'",'',$$updateiddtls)] =explode("*",("".$update_id."*".$$txtgsm."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
		}
		 $rID3=execute_query(bulk_update_sql_statement("pro_fab_subprocess_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
		//check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			 if($rID && $rID3)
				  {
					  mysql_query("COMMIT");  
					  echo "1**".$update_id;
				  }
				  else
				  {
					  mysql_query("ROLLBACK"); 
					  echo "10**".$update_id;
				  }
		}
		else if($db_type==2)
		{
			 if($rID && $rID3)
				  {
					 oci_commit($con); 
					  echo "1**".$update_id;
				  }
				  else
				  {
					 oci_rollback($con);
					  echo "10**".$update_id;
				  }
		}
		disconnect($con);
		die;
	}
}
if($action=="check_batch_no")
{
	$data=explode("**",$data);
	$sql="select id, batch_no,company_id from pro_batch_create_mst where batch_no='".trim($data[1])."' and entry_form in(0,36) and is_deleted=0 and status_active=1";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('id')]."_".$data_array[0][csf('company_id')];;
	}
	else
	{
		echo "0_";
	}
	
	exit();	
}
if($action=="check_batch_no_scan")
{
	$data=explode("**",$data);
	$batch_id=(int) $data[1];
	$sql="select id, batch_no,company_id from pro_batch_create_mst where id='".$batch_id."'  and entry_form in(0,36) and is_deleted=0 and status_active=1 order by id desc";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('id')]."_".$data_array[0][csf('company_id')];
	}
	else
	{
		echo "0_";
	}
	exit();	
}
if($action=="process_name_popup")
{
	echo load_html_head_contents("Batch Number Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		$(document).ready(function(e) {
			setFilterGrid('tbl_list_search',-1);
		});
		var selected_id = new Array(); var selected_name = new Array(); var buyer_id=''; var style_ref_array= new Array();
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function set_all()
		{
			var old=document.getElementById('txt_process_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		function js_set_value( str ) 
		{
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hidden_process_id').val(id);
			$('#hidden_process_name').val(name);
		}
    </script>
</head>
<body>
<div align="center">
	<fieldset style="width:370px;margin-left:10px">
    	<input type="hidden" name="hidden_process_id" id="hidden_process_id" class="text_boxes" value="">
        <input type="hidden" name="hidden_process_name" id="hidden_process_name" class="text_boxes" value="">
        <form name="searchprocessfrm_1"  id="searchprocessfrm_1" autocomplete="off">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="350" class="rpt_table" >
                <thead>
                    <th width="50">SL</th>
                    <th>Process Name</th>
                </thead>
            </table>
            <div style="width:350px; overflow-y:scroll; max-height:280px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="332" class="rpt_table" id="tbl_list_search" >
                <?php
                    $i=1; $process_row_id=''; $not_process_id_print_array=array(1,2,3,4,101,120,121,122,123,124);
					$hidden_process_id=explode(",",$txt_process_id);
                    foreach($conversion_cost_head_array as $id=>$name)
                    {
						if(!in_array($id,$not_process_id_print_array))
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							 
							if(in_array($id,$hidden_process_id)) 
							{ 
								if($process_row_id=="") $process_row_id=$i; else $process_row_id.=",".$i;
							}
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
								<td width="50" align="center"><?php echo "$i"; ?>
									<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $id; ?>"/>	
									<input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $name; ?>"/>
								</td>	
								<td><p><?php echo $name; ?></p></td>
							</tr>
							<?php
							$i++;
						}
                    }
                ?>
                    <input type="hidden" name="txt_process_row_id" id="txt_process_row_id" value="<?php echo $process_row_id; ?>"/>
                </table>
            </div>
             <table width="350" cellspacing="0" cellpadding="0" style="border:none" align="center">
                <tr>
                    <td align="center" height="30" valign="bottom">
                        <div style="width:100%"> 
                            <div style="width:50%; float:left" align="left">
                                <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                            </div>
                            <div style="width:50%; float:left" align="left">
                                <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	set_all();
</script>
</html>
<?php
exit();
}

?>