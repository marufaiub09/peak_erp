-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inv_item_transfer_mst`
--

DROP TABLE IF EXISTS `inv_item_transfer_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_item_transfer_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_prefix` varchar(20) NOT NULL,
  `transfer_prefix_number` int(11) NOT NULL DEFAULT '0',
  `transfer_system_id` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Company to company transfer=From Company ',
  `challan_no` varchar(20) NOT NULL,
  `entry_form` int(11) NOT NULL,
  `transfer_date` date NOT NULL,
  `transfer_criteria` tinyint(1) NOT NULL DEFAULT '0',
  `to_company` int(11) NOT NULL DEFAULT '0',
  `from_order_id` int(11) NOT NULL DEFAULT '0',
  `to_order_id` int(11) NOT NULL DEFAULT '0',
  `item_category` int(11) NOT NULL DEFAULT '0' COMMENT 'Except Order to Order Transfer',
  `is_posted_account` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transfer_prefix` (`transfer_prefix`),
  KEY `transfer_prefix_number` (`transfer_prefix_number`),
  KEY `transfer_system_id` (`transfer_system_id`),
  KEY `company_id` (`company_id`),
  KEY `challan_no` (`challan_no`),
  KEY `transfer_date` (`transfer_date`),
  KEY `transfer_criteria` (`transfer_criteria`),
  KEY `to_company` (`to_company`),
  KEY `from_order_id` (`from_order_id`),
  KEY `to_order_id` (`to_order_id`),
  KEY `item_category` (`item_category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_item_transfer_mst`
--

LOCK TABLES `inv_item_transfer_mst` WRITE;
/*!40000 ALTER TABLE `inv_item_transfer_mst` DISABLE KEYS */;
INSERT INTO `inv_item_transfer_mst` VALUES (1,'PAL-GFTE-15-',1,'PAL-GFTE-15-00001',1,'5589',0,'2015-09-15',2,0,0,0,13,0,31,'2015-09-15 16:04:28',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `inv_item_transfer_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
