<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_unit")
{
	echo create_drop_down( "cbo_unit_id", 150, "Select id, floor_name from  lib_prod_floor where company_id=$data and status_active=1 and is_deleted=0 order by floor_name","id,floor_name", 1, "-- Select --", $selected, "",0 );     	 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	//echo $datediff;
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_unit=str_replace("'","",$cbo_unit_id);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	$type=str_replace("'","",$cbo_type);
	
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$batch_quantity_arr=return_library_array( "select a.id,sum(b.batch_qnty) as  batch_qnty from  pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id", "id", "batch_qnty");
	$chem_dye_cost_array=array(); $chem_dye_cost_kg_array=array();
	$chem_dye_cost_sql="select b.batch_id, SUM(a.cons_amount) as cons_amount from inv_transaction a, dyes_chem_issue_dtls b where a.id=b.trans_id AND b.item_category in (5,6,7) AND b.is_deleted=0 AND b.status_active=1   group by b.batch_id";
	//echo $chem_dye_cost_sql;die;
	$chem_dye_cost_result=sql_select($chem_dye_cost_sql);
	//var_dump($chem_dye_cost_result);die;
	foreach($chem_dye_cost_result as $row)
	{
		$batch_id_arr=array_unique(explode(",",$row[csf('batch_id')]));
		$total_batch_qnty=0;
		foreach($batch_id_arr as $batch_id)
		{
			$total_batch_qnty+=$batch_quantity_arr[$batch_id];
			//echo $total_batch_qnty;die;
		}
		
		foreach($batch_id_arr as $batch_id)
		{
			$chem_dye_cost_array[$batch_id]=(($row[csf('cons_amount')]/$total_batch_qnty)*$batch_quantity_arr[$batch_id]);
			$chem_dye_cost_kg_array[$batch_id]=($row[csf('cons_amount')]/$total_batch_qnty);
		}
		
	}
	//var_dump($chem_dye_cost_array[896]);die;
	
	$machine_cap_array=array();
	$mach_cap_sql=sql_select("select id, machine_no, sum(prod_capacity) as prod_capacity from lib_machine_name where status_active=1 and is_deleted=0 group by id, machine_no");
	foreach($mach_cap_sql as $row)
	{
		$machine_cap_array[$row[csf('id')]]["machine_no"]=$row[csf('machine_no')];
		$machine_cap_array[$row[csf('id')]]["prod_capacity"]=$row[csf('prod_capacity')];
	}
	

	
function convertMinutes2Hours($Minutes)
{
    if ($Minutes < 0)
    {
        $Min = Abs($Minutes);
    }
    else
    {
        $Min = $Minutes;
    }
    $iHours = Floor($Min / 60);
    $Minutes = ($Min - ($iHours * 60)) / 100;
    $tHours = $iHours + $Minutes;
    if ($Minutes < 0)
    {
        $tHours = $tHours * (-1);
    }
    $aHours = explode(".", $tHours);
    $iHours = $aHours[0];
    if (empty($aHours[1]))
    {
        $aHours[1] = "00";
    }
    $Minutes = $aHours[1];
    if (strlen($Minutes) < 2)
    {
        $Minutes = $Minutes ."0";
    }
    $tHours = $iHours .":". $Minutes;
    return $tHours;
}
	
ob_start();	
	//$table_width=90+($datediff*160);
	
?>
	<div>
        <table width="2250px" cellpadding="0" cellspacing="0" id="caption" align="center">
            <tr>
               <td align="center" width="100%" colspan="24" class="form_caption" ><strong style="font-size:18px"><?php echo $company_library[$cbo_company];?></strong></td>
            </tr> 
            <tr>  
               <td align="center" width="100%" colspan="24" class="form_caption" ><strong style="font-size:14px">Daily Dyeing Produciton Analysis Report</strong></td>
            </tr>
            <tr>  
               <td align="center" width="100%" colspan="24" class="form_caption" ><strong style="font-size:14px"> <?php echo "From : ".change_date_format(str_replace("'","",$txt_date_from))." To : ".change_date_format(str_replace("'","",$txt_date_to))."" ;?></strong></td>
            </tr>  
        </table>
        <?php
		if($type==0 || $type==1)
		{
			
			?>
			<div align="left" style="background-color:#E1E1E1; color:#000; width:250px; font-size:14px; font-family:Georgia, 'Times New Roman', Times, serif;"><strong><u><i>Self Dyeing Production</i></u></strong></div>
                <table width="2250" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" >
                    <thead>
                        <th width="40">SL</th>
                        <th width="110">WO/Booking No</th>
                        <th width="90">Job No</th>
                        <th width="120">Buyer Name</th>
                        <th width="130">Style Ref.</th>
                        <th width="140">Order No</th>
                        <th width="100">Order Qty.</th>
                        <th width="60">GSM</th>
                        <th width="100">Fabric Color</th>
                        <th width="100">Batch No/ Lot No.</th>
                        <th width="100">MC No.</th>
                        <th width="90">MC Capacity</th>
                        <th width="100">Dyeing Qty</th>
                        <th width="60">UL %</th>
                        <th width="60">Hour Req.</th>
                        <th width="80">Loading Time</th>
                        <th width="80">Unloading Time</th>
                        <th width="70">Hour Used</th>
                        <th width="60">Hour Devi.</th>                
                        <th width="90">Process/ Color Range</th>
                        <th width="110">Total Dye & Chem Cost</th>
                        <th width="90">Dye & Chem Cost/Kg</th>
                        <th width="130">Dyeing Company</th>
                        <th>Remarks</th>
                    </thead>
                </table>
			<div style="width:2270px; overflow-y:scroll; max-height:275px;" id="scroll_body" >
                <table width="2250" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" id="table_body">
                <?php	
                $buyer_library=return_library_array( "select id,short_name from lib_buyer", "id", "short_name");
                $color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name");
                $gsm_library=return_library_array( "select id,gsm from  product_details_master", "id", "gsm");
				
                
                $order_arr=array();
                $sql_order="Select b.id, a.job_no, a.buyer_name, a.style_ref_no, b.po_number, b.po_quantity from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1";
                $result_sql_order=sql_select($sql_order);
                foreach($result_sql_order as $row)
                {
					$order_arr[$row[csf('id')]]['job_no']=$row[csf('job_no')];
					$order_arr[$row[csf('id')]]['buyer_name']=$row[csf('buyer_name')];
					$order_arr[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
					$order_arr[$row[csf('id')]]['po_number']=$row[csf('po_number')];
					$order_arr[$row[csf('id')]]['po_quantity']=$row[csf('po_quantity')];
                }
                
                $load_data=array();
                $load_time_data=sql_select("select id,batch_id,batch_no,load_unload_id,process_end_date,end_hours,end_minutes from pro_fab_subprocess where load_unload_id=1 and entry_form=35 and company_id=$cbo_company and status_active=1  and is_deleted=0");
                
                foreach($load_time_data as $row_time)// for Loading time
                {
					$load_data[$row_time[csf('batch_id')]]['process_end_date']=$row_time[csf('process_end_date')];
					$load_data[$row_time[csf('batch_id')]]['end_hours']=$row_time[csf('end_hours')];
					$load_data[$row_time[csf('batch_id')]]['end_minutes']=$row_time[csf('end_minutes')];
                }
                
                
                //print_r($chem_dye_cost_array[905]);die;
                
               
                if($db_type==0)
				{
					$sql_dtls="select b.id, a.batch_id, a.company_id as dyeing_company, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.booking_no, b.color_range_id, b.dur_req_hr, b.dur_req_min, min(c.prod_id) as prod_id, group_concat(c.po_id) as po_id, sum(c.batch_qnty) as qnty
                from pro_fab_subprocess a, pro_batch_create_mst b,  pro_batch_create_dtls c 
                where a.batch_id=b.id and a.entry_form=35 and a.load_unload_id=2 and b.id=c.mst_id and a.company_id='$cbo_company' and a.process_end_date between '$date_from' and '$date_to' and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 
                group by b.id, a.batch_id, a.company_id, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.booking_no, b.color_range_id, b.dur_req_hr, b.dur_req_min order by b.id";
				}
				else if($db_type==2)
				{
					$sql_dtls="select b.id, a.batch_id, a.company_id as dyeing_company, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.booking_no, b.color_range_id, b.dur_req_hr, b.dur_req_min, min(c.prod_id) as prod_id, LISTAGG(cast(c.po_id as varchar2(4000)), ',') WITHIN GROUP (ORDER BY c.po_id) as po_id, sum(c.batch_qnty) as qnty
                from pro_fab_subprocess a, pro_batch_create_mst b,  pro_batch_create_dtls c 
                where a.batch_id=b.id and a.entry_form=35 and a.load_unload_id=2 and b.id=c.mst_id and a.company_id='$cbo_company' and a.process_end_date between '$date_from' and '$date_to' and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 
                group by b.id, a.batch_id, a.company_id, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.booking_no, b.color_range_id, b.dur_req_hr, b.dur_req_min order by b.id";
				}
                //echo $sql_dtls;
                $sql_result=sql_select($sql_dtls);
                $tot_rows=count($sql_result);
                $i=1;  $ul_percent=0; $tot_dye_qnty=0;$total_dye_chem_cost=0;
                foreach ( $sql_result as $row )
                {
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$po_id_arr=array_unique(explode(",",$row[csf('po_id')])); 
                    ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                        <td width="40"><?php echo $i; ?></td>
                        <td width="110"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                        <td width="90" align="center"><p>
						<?php
						$job_chech=array();$job_all=$buyer_all=$style_all=$po_all="";$po_qnty=0;
						foreach($po_id_arr as $po_id)
						{
							if(!in_array($order_arr[$po_id]['job_no'],$job_chech))
							{
								$job_chech[]=$order_arr[$po_id]['job_no'];
								if($job_all=="") $job_all=$order_arr[$po_id]['job_no']; else $job_all .=", ".$order_arr[$po_id]['job_no'];
								if($buyer_all=="") $buyer_all=$buyer_library[$order_arr[$po_id]['buyer_name']]; else $buyer_all .=", ".$buyer_library[$order_arr[$po_id]['buyer_name']];
								if($style_all=="") $style_all=$order_arr[$po_id]['style_ref_no']; else $style_all .=", ".$order_arr[$po_id]['style_ref_no'];
							}
							if($po_all=="") $po_all=$order_arr[$po_id]['po_number']; else $po_all .=", ".$order_arr[$po_id]['po_number'];
							$po_qnty +=$order_arr[$po_id]['po_quantity'];
						}
						echo $job_all; 
						?></p></td>
                        <td width="120"><p><?php echo $buyer_all; ?></p></td>
                        <td width="130"><p><?php echo $style_all;  ?></p></td>
                        <td width="140"><p><?php echo $po_all;?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($po_qnty,2,'.','');  ?></p></td>
                        <td width="60" align="center"><p><?php echo $gsm_library[$row[csf('prod_id')]]; ?></p></td>
                        <td width="100"><p><?php echo $color_library[$row[csf('color_id')]]; ?></p></td>
                        <td width="100"><p><?php echo $row[csf('batch_no')]; ?></p></td>
                        <td width="100" align="center"><p><?php echo $machine_cap_array[$row[csf('machine_id')]]["machine_no"]; ?></p></td>
                        <td width="90" align="right"><p><?php $machine_capacity=$machine_cap_array[$row[csf('machine_id')]]["prod_capacity"]; echo number_format($machine_capacity,2,'.',''); ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($row[csf('qnty')],2,'.',''); $total_dyeing_qnty+=$dye_qnty; ?></p></td>
                        <td width="60" align="right"><p><?php $ul_percent=$row[csf('qnty')]/$machine_capacity*100; echo number_format($ul_percent,2,'.',''); ?></p></td>
                        <td width="60" align="center"><p><?php $req_hour=""; $req_hour=$row[csf('dur_req_hr')].":".$row[csf('dur_req_min')]; echo $req_hour; ?></p></td>
                        <td width="80" align="center"><p><?php $start_time=''; $start_time=$load_data[$row[csf('id')]]['end_hours'].':'.$load_data[$row[csf('id')]]['end_minutes']; echo  $start_time; ?></p></td>
                        <td width="80" align="center"><p><?php $end_time=''; $end_time=$row[csf('end_hours')].':'.$row[csf('end_minutes')]; echo $end_time; ?></p></td>
                        <td width="70" align="right"><p><?php $start_time=strtotime($load_data[$row[csf('id')]]['process_end_date']." ".$start_time); $end_time=strtotime($row[csf('process_end_date')]." ".$end_time);  $timeDiffin=($start_time-$end_time)/60; $time_used=convertMinutes2Hours($timeDiffin); echo $time_used; // ?></p></td>
                        <?php
						
                        $req_time=strtotime($row[csf('dur_req_hr')].":".$row[csf('dur_req_min')].":" . 00);
						$hour_dev_cal=0;$tot_deviation="";
						if($req_time!="")
						{
							$used_time=strtotime($time_used.':'. 00);
							$hour_dev_cal=($used_time-$req_time)/60;
							$tot_deviation=convertMinutes2Hours($hour_dev_cal); 
						}
						if($hour_dev_cal>0)
						{
							?>
							<td width="60" align="center" bgcolor="#FF0000"><p><?php echo $tot_deviation;// date('H:i', $diff);//floor($hours). ':' . ( ($hours-floor($hours)) * 60 ); ?></p></td>
							<?php
						}
						else
						{
							?>
							<td width="60" align="center" ><p><?php echo $tot_deviation;// date('H:i', $diff);//floor($hours). ':' . ( ($hours-floor($hours)) * 60 ); ?></p></td>
							<?php
						}
						?>
                        <td width="90"><p><?php echo $color_range[$row[csf('color_range_id')]]; ?></p></td>
                        <td width="110" align="right"><p><?php $chem_dye_cost =$chem_dye_cost_array[$row[csf('batch_id')]]; echo number_format($chem_dye_cost,4,'.','');?></p></td>
                        <td width="90" align="right"><p><?php echo number_format($chem_dye_cost_kg_array[$row[csf('batch_id')]],2,'.',''); $total_dye_chem_cost+=$chem_dye_cost_kg_array[$row[csf('batch_id')]];?> </p></td>
                        <td width="130" align="center"><p><?php echo $company_library[$row[csf('dyeing_company')]]; ?></p></td>
                        <td><p><?php echo $row[csf('remarks')]; ?></p></td>
                    </tr>
                    <?php
                    $i++;
                } 
                ?>
                </table>
                <table width="2250px" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0">
                    <tfoot>
                        <th width="40">&nbsp;</th>
                        <th width="110">&nbsp;</th>
                        <th width="90">&nbsp;</th>
                        <th width="120">&nbsp;</th>
                        <th width="130">&nbsp;</th>
                        <th width="140">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="60">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="90"><strong>Total</strong></th>
                        <th width="100" id="total_dyeing_qnty" align="right"><?php echo number_format($total_dyeing_qnty,2,'.',''); ?></th>
                        <th width="60">&nbsp;</th>
                        <th width="60">&nbsp;</th>
                        <th width="80">&nbsp;</th>
                        <th width="80">&nbsp;</th>
                        <th width="70">&nbsp;</th>
                        <th width="60">&nbsp;</th>
                        <th width="90">&nbsp;</th>
                        <th width="110">&nbsp;</th>
                        <th width="90" id="total_dye_chem_cost" align="right"><?php echo number_format($total_dye_chem_cost,4,'.',''); ?></th>
                        <th width="130">&nbsp;</th>
                    	<th>&nbsp;</th>
                    </tfoot>
                </table>
			</div> 
			<?php
		}
		if($type==0 || $type==2)
		{
			?>
			<br />
			<div>
			<div align="left" style="background-color:#E1E1E1; color:#000; width:320px; font-size:14px; font-family:Georgia, 'Times New Roman', Times, serif;"><strong><u><i>In-bound Subcontract Dyeing Production</i></u></strong></div>
                <table width="2050" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" >
                    <thead>
                    <th width="40">SL</th>
                    <th width="90">Job No</th>
                    <th width="120">Party Name</th>
                    <th width="130">Style Ref.</th>
                    <th width="140">Order No</th>
                    <th width="100">Order Qty.</th>
                    <th width="100">Batch No</th>
                    <th width="100">Batch Color</th>
                    <th width="100">MC No.</th>
                    <th width="90">MC Capacity</th>
                    <th width="100">Dyeing Qty</th>
                    <th width="60">UL %</th>
                    <th width="60">Hour Req.</th>
                    <th width="80">Loading Time</th>
                    <th width="80">Unloading Time</th>
                    <th width="70">Hour Used</th>
                    <th width="60">Hour Devi.</th>                
                    <th width="90">Process/ Color Range</th>
                    <th width="110">Total Dye & Chem Cost</th>
                    <th width="90">Dye & Chem Cost/Kg</th>
                    <th width="130">Dyeing Company</th>
                    <th>Remarks</th>
                    </thead>
                </table>
			<div style="width:2070px; overflow-y:scroll; max-height:275px;" id="scroll_body" >
                <table width="2050" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0" id="table_body1">
                <?php	
                $buyer_library=return_library_array( "select a.id,a.short_name from lib_buyer a, lib_buyer_party_type b where a.id=b.buyer_id and b.party_type in(2,3)", "id", "short_name");
                $color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name");
                $machine_library=return_library_array( "select id,machine_no from lib_machine_name", "id", "machine_no");
                
                $job_arr=array();
                $sql_job="Select b.id, a.subcon_job, a.party_id, b.cust_style_ref, b.order_no, b.order_quantity from  subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1";
                $result_sql_job=sql_select($sql_job);
                foreach($result_sql_job as $row)
                {
					$job_arr[$row[csf('id')]]['job_no']=$row[csf('subcon_job')];
					$job_arr[$row[csf('id')]]['party_id']=$row[csf('party_id')];
					$job_arr[$row[csf('id')]]['style_ref_no']=$row[csf('cust_style_ref')];
					$job_arr[$row[csf('id')]]['order_no']=$row[csf('order_no')];
					$job_arr[$row[csf('id')]]['order_quantity']=$row[csf('order_quantity')];
                }
                
                $load_data=array();
                $load_time_data=sql_select("select id, batch_id, batch_no, process_end_date, load_unload_id, end_hours, end_minutes from pro_fab_subprocess where load_unload_id=1 and entry_form=38 and company_id=$cbo_company and status_active=1  and is_deleted=0");
                
                foreach($load_time_data as $row_time)// for Loading time
                {
					$load_data[$row_time[csf('batch_id')]]['process_end_date']=$row_time[csf('process_end_date')];
					$load_data[$row_time[csf('batch_id')]]['end_hours']=$row_time[csf('end_hours')];
					$load_data[$row_time[csf('batch_id')]]['end_minutes']=$row_time[csf('end_minutes')];
                }
                
                
                
               /* $machine_cap_array=array();
                $mach_cap_sql=sql_select("select id, sum(prod_capacity) as prod_capacity from lib_machine_name where status_active=1 and is_deleted=0 group by id");
                foreach($mach_cap_sql as $row)
                {
                	$machine_cap_array[$row[csf('id')]]=$row[csf('prod_capacity')];
                }*/
				//echo $db_type."jahid";die;
                if($db_type==0)
				{
                	$sql_sub_dtls="select b.id, a.batch_id, a.company_id as dyeing_company, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.color_range_id, b.dur_req_hr, b.dur_req_min, min(c.prod_id), group_concat(c.po_id) as po_id, sum(c.batch_qnty) as qnty
                from pro_fab_subprocess a, pro_batch_create_mst b,  pro_batch_create_dtls c where a.batch_id=b.id and a.entry_form=38 and a.load_unload_id=2 and b.id=c.mst_id and a.company_id='$cbo_company' and a.process_end_date between '$date_from' and '$date_to' and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 
                group by b.id, a.batch_id, a.company_id, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.color_range_id, b.dur_req_hr, b.dur_req_min order by b.id";
				}
				else if($db_type==2)
				{
					 $sql_sub_dtls="select b.id, a.batch_id, a.company_id as dyeing_company, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.color_range_id, b.dur_req_hr, b.dur_req_min, min(c.prod_id), LISTAGG(cast(c.po_id as varchar2(4000)), ',') WITHIN GROUP (ORDER BY c.po_id) as po_id, sum(c.batch_qnty) as qnty
                from pro_fab_subprocess a, pro_batch_create_mst b,  pro_batch_create_dtls c where a.batch_id=b.id and a.entry_form=38 and a.load_unload_id=2 and b.id=c.mst_id and a.company_id='$cbo_company' and a.process_end_date between '$date_from' and '$date_to' and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0 
                group by b.id, a.batch_id, a.company_id, a.process_end_date, a.machine_id, a.end_hours, a.end_minutes, a.remarks, b.batch_no, b.color_id, b.color_range_id, b.dur_req_hr, b.dur_req_min order by b.id";
				}
				//echo $sql_sub_dtls;die;
                $sql_sub_result=sql_select($sql_sub_dtls);
                $tot_rows=count($sql_sub_result);
                $k=1;  $ul_percent=0; $tot_dye_qnty=0;$total_dye_chem_cost=0;
                foreach ( $sql_sub_result as $row )
                {
					if ($k%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$po_id_arr=array_unique(explode(",",$row[csf('po_id')])); 
                    ?>
					<tr bgcolor="<?php echo $bgcolor; ?>" onclick="change_color_sub('row_<?php echo $k; ?>','<?php echo $bgcolor; ?>')" id="row_<?php echo $k; ?>">
                        <td width="40"><?php echo $k; ?></td>
                        <td width="90" align="center"><p>
						<?php
						$job_chech=array();$job_all=$buyer_all=$style_all=$po_all="";$po_qnty=0;
						foreach($po_id_arr as $po_id)
						{
							if(!in_array($job_arr[$po_id]['job_no'],$job_chech))
							{
								$job_chech[]=$job_arr[$po_id]['job_no'];
								if($job_all=="") $job_all=$job_arr[$po_id]['job_no']; else $job_all .=", ".$job_arr[$po_id]['job_no'];
								if($buyer_all=="") $buyer_all=$job_arr[$po_id]['party_id']; else $buyer_all .=", ".$job_arr[$po_id]['party_id'];
								if($style_all=="") $style_all=$job_arr[$po_id]['style_ref_no']; else $style_all .=", ".$job_arr[$po_id]['style_ref_no'];
							}
							if($po_all=="") $po_all=$job_arr[$po_id]['order_no']; else $po_all .=", ".$job_arr[$po_id]['order_no'];
							$po_qnty +=$job_arr[$po_id]['order_quantity'];
						}
						echo $job_all; 
						?></p></td>
                        <td width="120"><p><?php echo $buyer_library[$buyer_all]; ?></p></td>
                        <td width="130"><p><?php echo $style_all;  ?></p></td>
                        <td width="140"><p><?php echo $po_all;?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($po_qnty,2,'.','');  ?></p></td>
                        <td width="100"><p><?php echo $row[csf('batch_no')]; ?></p></td>
                        <td width="100"><p><?php echo $color_library[$row[csf('color_id')]]; ?></p></td>
                        <td width="100" align="center"><p><?php echo $machine_cap_array[$row[csf('machine_id')]]["machine_no"]; ?></p></td>
                        <td width="90" align="right"><p><?php $machine_capacity=$machine_cap_array[$row[csf('machine_id')]]["prod_capacity"]; echo number_format($machine_capacity,2,'.',''); ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($row[csf('qnty')],2,'.',''); $total_sub_dyeing_qnty+=$dye_qnty; ?></p></td>
                        <td width="60" align="right"><p><?php $ul_percent=$row[csf('qnty')]/$machine_capacity*100; echo number_format($ul_percent,2,'.',''); ?></p></td>
                        <td width="60" align="center"><p><?php $req_hour=""; $req_hour=$row[csf('dur_req_hr')].":".$row[csf('dur_req_min')]; echo $req_hour; ?></p></td>
                        <td width="80" align="center"><p><?php $start_time=''; $start_time=$load_data[$row[csf('id')]]['end_hours'].':'.$load_data[$row[csf('id')]]['end_minutes']; echo  $start_time; ?></p></td>
                        <td width="80" align="center"><p><?php $end_time=''; $end_time=$row[csf('end_hours')].':'.$row[csf('end_minutes')]; echo $end_time; ?></p></td>
                        <td width="70" align="right"><p><?php $start_time=strtotime($load_data[$row[csf('id')]]['process_end_date']." ".$start_time); $end_time=strtotime($row[csf('process_end_date')]." ".$end_time);  $timeDiffin=($end_time-$start_time)/60; $time_used=convertMinutes2Hours($timeDiffin); echo $time_used ?></p></td>
                        <?php
						
                        $req_time=strtotime($row[csf('dur_req_hr')].":".$row[csf('dur_req_min')].":" . 00);
						$hour_dev_cal=0;$tot_deviation="";
						if($req_time!="")
						{
							$used_time=strtotime($time_used.':'. 00);
							$hour_dev_cal=($used_time-$req_time)/60;
							$tot_deviation=convertMinutes2Hours($hour_dev_cal); 
						}
						if($hour_dev_cal>0)
						{
							?>
							<td width="60" align="center" bgcolor="#FF0000"><p><?php echo $tot_deviation;// date('H:i', $diff);//floor($hours). ':' . ( ($hours-floor($hours)) * 60 ); ?></p></td>
							<?php
						}
						else
						{
							?>
							<td width="60" align="center" ><p><?php echo $tot_deviation;// date('H:i', $diff);//floor($hours). ':' . ( ($hours-floor($hours)) * 60 ); ?></p></td>
							<?php
						}
						?>
                        <td width="90"><p><?php echo $color_range[$row[csf('color_range_id')]]; ?></p></td>
                        <td width="110" align="right"><p><?php $chem_dye_cost =$chem_dye_cost_array[$row[csf('batch_id')]]; echo number_format($chem_dye_cost,4,'.',''); ?></p></td>
                        <td width="90" align="right"><p><?php echo number_format($chem_dye_cost_kg_array[$row[csf('batch_id')]],2,'.',''); $total_sub_dye_chem_cost+=$chem_dye_cost_kg_array[$row[csf('batch_id')]]; ?></p></td>
                        <td width="130" align="center"><p><?php echo $company_library[$row[csf('dyeing_company')]]; ?></p></td>
                        <td><p><?php echo $row[csf('remarks')]; ?></p></td>
					</tr>
					<?php
					$k++;
                } 
				?>
                </table>
                <table width="2050px" border="1" rules="all" class="rpt_table" cellpadding="0" cellspacing="0">
                    <tfoot>
                        <th width="40">&nbsp;</th>
                        <th width="90">&nbsp;</th>
                        <th width="120">&nbsp;</th>
                        <th width="130">&nbsp;</th>
                        <th width="140">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="100">&nbsp;</th>
                        <th width="90"><strong>Total</strong></th>
                        <th width="100" id="total_sub_dyeing_qnty" align="right"><?php echo number_format($total_sub_dyeing_qnty,2,'.',''); ?></th>
                        <th width="60">&nbsp;</th>
                        <th width="60">&nbsp;</th>
                        <th width="80">&nbsp;</th>
                        <th width="80">&nbsp;</th>
                        <th width="70">&nbsp;</th>
                        <th width="60">&nbsp;</th>
                        <th width="90">&nbsp;</th>
                        <th width="110">&nbsp;</th>
                        <th width="90" id="total_sub_dye_chem_cost" align="right"><?php echo number_format($total_sub_dye_chem_cost,4,'.',''); ?></th>
                        <th width="130">&nbsp;</th>
                        <th>&nbsp;</th>
                    </tfoot>
                </table>
			</div>
			</div>     
			<?php
		}
		?>    
  </div>
  
	<?php
	echo "$total_data****requires/$filename****$tot_rows";
    exit();
}
?>
