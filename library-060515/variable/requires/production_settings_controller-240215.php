<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="on_change_data")
{
	
	$explode_data = explode("_",$data);
	$type = $explode_data[0];
	$company_id = $explode_data[1];
	
	if ($type==1)   //Production_Production Update Areas
	{
	
			$nameArray= sql_select("select id, company_name,variable_list,cutting_update_hcode,cutting_update, printing_emb_production_hcode, printing_emb_production, sewing_production_hcode, sewing_production, iron_update_hcode, iron_update, finishing_update_hcode, finishing_update, ex_factory, fabric_roll_level_hcode, fabric_roll_level, fabric_machine_level_hcode,fabric_machine_level, batch_maintained_hcode,batch_maintained, iron_input,production_entry from variable_settings_production where company_name='$company_id' and variable_list=1 order by id");
 			if(count($nameArray)>0)$is_update=1;else $is_update=0;
 			?>			 
            <fieldset>
                <legend>Production Update Areas</legend>
                <div style="width:700px; min-height:20px; max-height:250px;" id="variable_list_cont1" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr>
                            <td width="130" align="left" id="cutting_update">Cutting Update</td>
                            <td width="190">
                                  	<? 
										echo create_drop_down( "cbo_cutting_update", 170, $production_update_areas,'', 1, '---- Select ----', $nameArray[0][csf('cutting_update')], "" );
									?>
                            </td>
                            <td width="160" align="left" id="printing_emb_production">Printing & Embrd. Prodiction</td>
                            <td width="180">
                                 	<? 
										echo create_drop_down( "cbo_printing_emb_production", 170, $production_update_areas,'', 1, '---- Select ----', $nameArray[0][csf('printing_emb_production')], "" );
									?>
                            </td>
                        </tr>
                        <tr> 
                            <td width="130" align="left" id="sewing_production">Sewing Production</td>
                            <td width="190">
                                <? 
										echo create_drop_down( "cbo_sewing_production", 170, $production_update_areas,'', 1, '---- Select ----', $nameArray[0][csf('sewing_production')], "" );
									?>
                             </td>
                            <td width="130" align="left" id="finishing_update">Finishing Entry</td>
                            <td width="190">
                                <? 
										echo create_drop_down( "cbo_finishing_update", 170, $production_update_areas,'', 1, '---- Select ----', $nameArray[0][csf('finishing_update')], "" );
									?>
                             </td>
                        </tr>
                        <tr>
                        	<td width="160" align="left" id="iron_update">Iron Output</td>
                            <td width="190">
                                <? 
										echo create_drop_down( "cbo_iron_update", 170, $production_update_areas,'', 1, '---- Select ----', $nameArray[0][csf('iron_update')], "" );
									?>
                            </td>  
                            <td width="160" align="left" id="ex_factory">Ex-Factory</td>
                            <td width="190">
                                <? 
										echo create_drop_down( "cbo_ex_factory", 170, $production_update_areas,'', 1, '---- Select ----', $nameArray[0][csf('ex_factory')], "" );
									?>
                             </td>
                        </tr>
                        <tr> 
                             <td width="130" align="left" id="production_entry">Production Entry</td>
                             <td width="190">
                                 <? 
										$production_entry = array(1=>'Style Wise',2=>'Order Wise');
										echo create_drop_down( "cbo_production_entry", 170, $production_entry,'', 1, '---- Select ----', $nameArray[0][csf('production_entry')], "" );
									?>
                             </td>
                             <td width="130" id="cutting_delevery_entry">Cutting Deloevary to Input</td>
                             <td width="190">
                                  <? 
										echo create_drop_down( "cbo_cutting_update_to_input", 170, $production_update_areas,'', 1, '---- Select ----', $nameArray[0][csf('cutting_update')], "" );
									?>
                             
                             </td>
                        </tr>
                         
                    </table>
                </div>
                <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td align="center" width="320">&nbsp;</td>						
 						</tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
									echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                ?>
                            </td>					
                        </tr>
					</table>
				</div>               
            </fieldset>
 
<?    				
			
	}
    
    
	
	if ($type==2){ //Production_Excess Cutting Slab
  	?>
					<fieldset>
						<legend>Excess Cutting Slab</legend>
						 <div style="width:500px;" align="left">
							<table cellspacing="0" width="100%" >
								<tr>
								   <td width="150" align="center" id="order_quantityStart" colspan="2">Slab Range Start</td>
									<td width="150" align="center" id="order_quantityEnd" colspan="2">Slab Range End</td>
									<td width="150" align="center" id="excess_percent">Excess %</td>
								</tr>
							</table>
						</div>	
						<div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
							<table cellspacing="0" width="100%" id="tbl_slab" >
								<?
									$i=0;
									$sub_nameArray= sql_select("select id,company_name,variable_list,slab_rang_start,slab_rang_end,excess_percent from variable_prod_excess_slab where company_name='$company_id' and variable_list=2 order by id");
 									foreach($sub_nameArray as $rows)
									{
										$i++;
								?>
								
								 <tr style="text-decoration:none">
									<td width="150"><input type="text" name="txt_slab_rang_start<? echo $i; ?>" id="txt_slab_rang_start<? echo $i; ?>" onchange="next_number(<? echo $i; ?>)" value="<? echo $rows[csf("slab_rang_start")]; ?>" class="text_boxes_numeric" style="width:150px;"/></td>
									<td width="150"><input type="text" name="txt_slab_rang_end<? echo $i; ?>" id="txt_slab_rang_end<? echo $i; ?>" value="<? echo $rows[csf("slab_rang_end")]; ?>" class="text_boxes_numeric" style="width:150px;"/></td>
									<td width="150"><input type="text" name="txt_excess_percent<? echo $i; ?>" id="txt_excess_percent<? echo $i; ?>" value="<? echo $rows[csf("excess_percent")]; ?>" class="text_boxes_numeric" onfocus="add_variable_row(<? echo $i; ?>)"  style="width:150px;"/></td>
								</tr>
								<? } 
								
								if($i==0)
								{	
									$i++;							
								?>
 								<tr style="text-decoration:none">
									<td width="150"><input type="text" name="txt_slab_rang_start<? echo $i; ?>" id="txt_slab_rang_start<? echo $i; ?>" onchange="next_number(<? echo $i; ?>)" class="text_boxes_numeric"  style="width:150px;" value="" /></td>
									<td width="150"><input type="text" name="txt_slab_rang_end<? echo $i; ?>" id="txt_slab_rang_end<? echo $i; ?>" class="text_boxes_numeric" style="width:150px;"  value="" /></td>
									<td width="150"><input type="text" name="txt_excess_percent<? echo $i; ?>" id="txt_excess_percent<? echo $i; ?>" class="text_boxes_numeric" onfocus="add_variable_row(<? echo $i; ?>)"   style="width:150px;"  value="" /></td>
								</tr>
								<? } ?>
							</table>
						</div>	
						 <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
							<table cellspacing="0" width="100%" >
                                <tr> 
                                    <td align="center" width="320">&nbsp;</td>						
                                </tr>						 
                                <tr>
                                   <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                        <input  type="hidden"name="update_id" id="update_id" value="">
                                        <? 
                                            echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                        ?>
                                    </td>					
                                </tr>
                            </table>
						</div>
					</fieldset>
				
		<?
	}
	


	if ($type==3){//Production_Fabric in Roll Level
	
 			$nameArray= sql_select("select id, company_name,variable_list,cutting_update_hcode,cutting_update, printing_emb_production_hcode, printing_emb_production, sewing_production_hcode, sewing_production, iron_update_hcode, iron_update, finishing_update_hcode, finishing_update, ex_factory, fabric_roll_level_hcode, fabric_roll_level, fabric_machine_level_hcode,fabric_machine_level, batch_maintained_hcode,batch_maintained, iron_input,production_entry from variable_settings_production where company_name='$company_id' and variable_list=3 order by id");
 			if(count($nameArray)>0)$is_update=1;else $is_update=0;
     	?>
            <fieldset>
                <legend>Fabric in Roll Level</legend>
                <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                    <table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="130" align="left" id="fabric_roll_level" cbo_fabric_roll_level>Fabric in Roll Level</td>
                            <td width="190"> 
                                 <? 
									echo create_drop_down( "cbo_fabric_roll_level", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('fabric_roll_level')], "",'','' );
								?>
                            </td>
                        </tr>
                    </table>
                </div>
                 <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                    <table cellspacing="0" width="100%" >
                                <tr> 
                                    <td align="center" width="320">&nbsp;</td>						
                                </tr>						 
                                <tr>
                                   <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                        <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
                                        <? 
                                            echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                        ?>
                                    </td>					
                                </tr>
                     </table>
                </div>
            </fieldset>
 				
		<?
	}
	
	
	if ($type==4){//Production_Fabric in Machine Level
		 	
			$nameArray= sql_select("select id, company_name,variable_list,cutting_update_hcode,cutting_update, printing_emb_production_hcode, printing_emb_production, sewing_production_hcode, sewing_production, iron_update_hcode, iron_update, finishing_update_hcode, finishing_update, ex_factory, fabric_roll_level_hcode, fabric_roll_level, fabric_machine_level_hcode,fabric_machine_level, batch_maintained_hcode,batch_maintained, iron_input,production_entry from variable_settings_production where company_name='$company_id' and variable_list=4 order by id");
 			if(count($nameArray)>0)$is_update=1;else $is_update=0;
 		?>
 
		<fieldset>
			<legend>Fabric in Machine Level</legend>
			<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
				<table cellspacing="0" width="100%" >
					<tr> 
						<td width="130" align="left" id="fabric_machine_level">Fabric in Machine Level</td>
						<td width="190">
 							<? 
								echo create_drop_down( "cbo_fabric_machine_level", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('fabric_machine_level')], "",'','' );
							?>
						</td>
					</tr>
				</table>
			</div>
			<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
				<table cellspacing="0" width="100%" >
                            <tr> 
                                <td align="center" width="320">&nbsp;</td>						
                            </tr>						 
                            <tr>
                               <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                    <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
                                    <? 
                                        echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                    ?>
                                </td>					
                            </tr>
                 </table>
			</div>
		</fieldset>
	
	<?
	}

 
  
if ($type==13){//Production_Batch Maintained
		
			$nameArray= sql_select("select id, company_name,variable_list,cutting_update_hcode,cutting_update, printing_emb_production_hcode, printing_emb_production, sewing_production_hcode, sewing_production, iron_update_hcode, iron_update, finishing_update_hcode, finishing_update, ex_factory, fabric_roll_level_hcode, fabric_roll_level, fabric_machine_level_hcode,fabric_machine_level, batch_maintained_hcode,batch_maintained, iron_input,production_entry from variable_settings_production where company_name='$company_id' and variable_list=13 order by id");
 			if(count($nameArray)>0)$is_update=1;else $is_update=0;

		?>
		
 			<fieldset>
				<legend>Batch Maintained</legend>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td width="130" align="left" id="batch_maintained">Batch Maintained</td>
							<td width="190">
                                 <? 
									echo create_drop_down( "cbo_batch_maintained", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('batch_maintained')], "",'','' );
								?>
							</td>
						</tr>
					</table>
				</div>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
					<table cellspacing="0" width="100%" >
                            <tr> 
                                <td align="center" width="320">&nbsp;</td>						
                            </tr>						 
                            <tr>
                               <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                    <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
                                    <? 
                                        echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                    ?>
                                </td>					
                            </tr>
                	 </table>
				</div>
			</fieldset>
 		<?
	}
	
	if ($type==15){//Production Auto Faric Store Update
		
			$category_wise_array=array();
			
			$nameArray= sql_select("select id, company_name,variable_list,item_category_id,auto_update from variable_settings_production where company_name='$company_id' and variable_list=15 order by id");
			
			if(count($nameArray)>0) $is_update=1; else $is_update=0;
			
			foreach($nameArray as $row)
			{
				$category_wise_array[$row[csf('item_category_id')]]['auto']=$row[csf('auto_update')];
				$category_wise_array[$row[csf('item_category_id')]]['update_id']=$row[csf('id')];
			}
		?>
		
 			<fieldset>
				<legend>Auto Faric Store Update</legend>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
					<table cellspacing="0" width="100%" class="rpt_table">
                    	<thead>
                        	<th>Item Category</th>
                            <th>Auto Update</th>
                        </thead>
						<tr align="center"> 
							<td>
                                <? 
									echo create_drop_down( "cbo_item_category_1", 150, $item_category,'', 0, '', '2', "",'','2' );
								?>
							</td>
							<td>
                                 <? 
									echo create_drop_down( "cbo_auto_update_1", 150, $yes_no,'', 1, '---- Select ----', $category_wise_array[2]['auto'], "",'','' );
								?>
							</td>
                            <input  type="hidden"name="update_id_1" id="update_id_1" value="<? echo $category_wise_array[2]['update_id']; ?>">
						</tr>
                        <tr align="center"> 
							<td>
                                 <? 
									echo create_drop_down( "cbo_item_category_2", 150, $item_category,'', 0, '','13', "",'','13' );
								?>
							</td>
							<td>
                                 <? 
									echo create_drop_down( "cbo_auto_update_2", 150, $yes_no,'', 1, '---- Select ----',$category_wise_array[13]['auto'], "",'','' );
								?>
							</td>
                            <input type="hidden"name="update_id_2" id="update_id_2" value="<? echo $category_wise_array[13]['update_id']; ?>">
						</tr>
					</table>
				</div>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
					<table cellspacing="0" width="100%" >
                            <tr> 
                                <td align="center" width="320">&nbsp;</td>						
                            </tr>						 
                            <tr>
                               <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                    <? 
                                        echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                    ?>
                                </td>					
                            </tr>
                	 </table>
				</div>
			</fieldset>
 		<?
	}
	
	if($type==23){//Production Resource Allocation
		
			$nameArray= sql_select("select id, auto_update from variable_settings_production where company_name='$company_id' and variable_list=23 and status_active=1 and is_deleted=0");
 			if(count($nameArray)>0)$is_update=1;else $is_update=0;

		?>
		
 			<fieldset>
				<legend>Production Resource Allocation</legend>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td align="left" id="ProductionResourceAllocation">Production Resource Allocation</td>
							<td width="190">
                                 <? 
									echo create_drop_down( "cbo_prod_resource", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('auto_update')], "",'','' );
								?>
							</td>
						</tr>
					</table>
				</div>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
					<table cellspacing="0" width="100%" >
                            <tr> 
                                <td align="center" width="320">&nbsp;</td>						
                            </tr>						 
                            <tr>
                               <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                    <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
                                    <? 
                                        echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                    ?>
                                </td>					
                            </tr>
                	 </table>
				</div>
			</fieldset>
 		<?
		exit();
	}
	
	if($type==24)
	{//Production Resource Allocation
		
		$nameArray=sql_select("select id, batch_no_creation from variable_settings_production where company_name='$company_id' and variable_list=24 and status_active=1 and is_deleted=0");
		if(count($nameArray)>0) $is_update=1; else $is_update=0;
	?>
        <fieldset>
            <legend>Batch No</legend>
            <div style="width:300px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                <table cellspacing="0" width="100%" >
                    <tr> 
                        <td width="120" align="left" id="batch_no">Batch No</td>
                        <td>
                             <? 
                                echo create_drop_down( "cbo_batch_no", 170, $yes_no,'', 1, '---- Select ----', $nameArray[0][csf('batch_no_creation')], "",'','' );
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:350px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>						
                        </tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
                                <? 
                                    echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                ?>
                            </td>					
                        </tr>
                 </table>
            </div>
        </fieldset>
 	<?
		exit();
	}
	
	if($type==25)
	{//Production Resource Allocation
		
		$nameArray=sql_select("select id, smv_source from variable_settings_production where company_name='$company_id' and variable_list=25 and status_active=1 and is_deleted=0");
		if(count($nameArray)>0) $is_update=1; else $is_update=0;
		
		$smv_source_arr=array(1=>"From Order Entry",2=>"From Pre-Costing",3=>"From GSD Entry");
	?>
        <fieldset>
            <legend>Batch No</legend>
            <div style="width:300px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                <table cellspacing="0" width="100%" >
                    <tr> 
                        <td width="120" align="left" id="batch_no">SMV Source</td>
                        <td>
                             <? 
                                echo create_drop_down( "cbo_source", 170, $smv_source_arr,'', 0, '---- Select ----', $nameArray[0][csf('smv_source')], "",'','' );
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:350px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>						
                        </tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
                                <? 
                                    echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                ?>
                            </td>					
                        </tr>
                 </table>
            </div>
        </fieldset>
 	<?
		exit();
	}
	if($type==26)
	{//Production Resource Allocation
		
		$dataArray=array();
		if($db_type==0)
		{
			$nameArray=sql_select("select id,shift_id,prod_start_time,lunch_start_time from variable_settings_production where company_name='$company_id' and variable_list=26 and status_active=1 and is_deleted=0");
		}
		else
		{
			$nameArray=sql_select("select id, shift_id, TO_CHAR(prod_start_time,'HH24:MI') as prod_start_time, TO_CHAR(lunch_start_time,'HH24:MI') as lunch_start_time from variable_settings_production where company_name='$company_id' and variable_list=26 and status_active=1 and is_deleted=0");	
		}
		foreach($nameArray as $row)
		{
			$dataArray[$row[csf('shift_id')]]=$row[csf('id')]."**".$row[csf('prod_start_time')]."**".$row[csf('lunch_start_time')];
		}
		
		if(count($nameArray)>0) $is_update=1; else $is_update=0;
	?>
        <fieldset>
            <legend>Sewing Production </legend>
            <div style="width:420px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                <table cellspacing="0" width="100%" cellpadding="0" class="rpt_table" rules="all" border="1" id="tbl_list_search">
                	<thead>
                    	<th width="100">Shift Name</th>
                        <th width="160">Production Start Time</th>
                        <th>Lunch Start Time</th>
                    </thead>
                    <?
						$i=1;
						foreach($shift_name  as $id=>$name)
						{
							if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							
							$data=explode("**",$dataArray[$id]);
							$update_dtls_id=$data[0];
							$prod_start_time=$data[1];
							$lunch_start_time=$data[2];
							
							?>
                            <tr bgcolor="<? echo $bgcolor; ?>" id="tr_<? echo $i; ?>" align="center" valign="middle"> 
                                <td><b><? echo $name; ?></b></td>
                                <td>
                                	<input class="timepicker text_boxes" type="text" style="width:140px" name="txt_prod_start_time[]" id="txt_prod_start_time<? echo $i; ?>" value="<? echo $prod_start_time; ?>" onblur="fnc_valid_time(this.value,'txt_prod_start_time<? echo $i; ?>');"/>
                                </td>
                                <td>
                                  	<input class="timepicker text_boxes" type="text" style="width:140px" name="txt_lunch_start_time[]" id="txt_lunch_start_time<? echo $i; ?>" value="<? echo $lunch_start_time; ?>" onblur="fnc_valid_time(this.value,'txt_lunch_start_time<? echo $i; ?>');"/>
                                  	<input type="hidden"name="shift_id[]" id="shift_id<? echo $i; ?>" value="<? echo $id; ?>">
                                   	<input type="hidden"name="update_id[]" id="update_id<? echo $i; ?>" value="<? echo $update_dtls_id; ?>">
                                </td>
                            </tr>
						<? 
						$i++;
						}
					?>
                </table>
            </div>
            <div style="width:350px; min-height:20px; max-height:250px;" id="variable_list_cont3" align="center">
                <table cellspacing="0" width="100%" >
                        <tr> 
                            <td align="center" width="320">&nbsp;</td>						
                        </tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <? 
                                    echo load_submit_buttons( $permission, "fnc_production_variable_settings", $is_update,0 ,"reset_form('productionVariableSettings','','')",1);
                                ?>
                            </td>					
                        </tr>
                 </table>
            </div>
        </fieldset>
 	<?
		exit();
	}

}//end change on data condition




if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
					 
		if(str_replace("'","",$cbo_variable_list_production)==2)
		{
			
			sql_select("delete from variable_prod_excess_slab where company_name=$cbo_company_name_production and variable_list=$cbo_variable_list_production");
			
			$slab_id = return_next_id( "id", "variable_prod_excess_slab", 1);
			$field_array="id, company_name,variable_list, slab_rang_start, slab_rang_end, excess_percent, inserted_by,insert_date,status_active"; 
			 
			for($i=1;$i<=$counter;$i++)
			{
				
				$txt_slab_rang_start = 'txt_slab_rang_start'.$i;
				$txt_slab_rang_end = 'txt_slab_rang_end'.$i;
				$txt_excess_percent = 'txt_excess_percent'.$i;
				if($i==1)
					$data_array.="(".$slab_id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$txt_slab_rang_start.",".$$txt_slab_rang_end.",".$$txt_excess_percent.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				else
					$data_array.=",(".$slab_id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$txt_slab_rang_start.",".$$txt_slab_rang_end.",".$$txt_excess_percent.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				$slab_id++;
				
			}	
			 $rID=sql_insert("variable_prod_excess_slab",$field_array,$data_array,1); 
		}
		else if(str_replace("'","",$cbo_variable_list_production)==15)
		{
			
			$id = return_next_id( "id", "variable_settings_production", 1);
			$field_array="id, company_name, variable_list, item_category_id, auto_update, inserted_by, insert_date"; 
			
			$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$cbo_item_category_1.",".$cbo_auto_update_1.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			$id=$id+1;
			$data_array.=",(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$cbo_item_category_2.",".$cbo_auto_update_2.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
			//echo "insert into variable_settings_production ".$field_array." values ".$data_array;
			$rID=sql_insert("variable_settings_production",$field_array,$data_array,1); 
		}
		else
		{
			if (is_duplicate_field( "company_name", "variable_settings_production", "company_name=$cbo_company_name_production and variable_list=$cbo_variable_list_production" ) == 1)
			{
				echo 11; die;
			}
			else
			{
				$id=return_next_id( "id", "variable_settings_production", 1 ) ;
				if(str_replace("'","",$cbo_variable_list_production)==1)
				{	
					$field_array="id, company_name,variable_list,cutting_update_hcode,cutting_update,cutting_input_hcode,cutting_input,printing_emb_production_hcode, printing_emb_production, sewing_production_hcode, sewing_production, iron_update_hcode, iron_update, finishing_update_hcode, finishing_update, ex_factory, production_entry_hcode, production_entry, inserted_by,insert_date,status_active"; 
					$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",'".$cutting_update_html."',".$cbo_cutting_update.",'".$cutting_delevary_input_html."',".$cbo_cutting_update_to_input.",'".$printing_emb_production_html."',".$cbo_printing_emb_production.",'".$sewing_production_html."',".$cbo_sewing_production.",'".$iron_update_html."',".$cbo_iron_update.",'".$finishing_update_html."',".$cbo_finishing_update.",".$cbo_ex_factory.",'".$production_entry_html."',".$cbo_production_entry.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				}
				else if(str_replace("'","",$cbo_variable_list_production)==3)
				{
					$field_array="id, company_name,variable_list, fabric_roll_level_hcode, fabric_roll_level, inserted_by,insert_date,status_active"; 
					$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",'".$fabric_roll_level_html."',".$cbo_fabric_roll_level.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				}
				else if(str_replace("'","",$cbo_variable_list_production)==4)
				{
					$field_array="id, company_name,variable_list, fabric_machine_level_hcode, fabric_machine_level, inserted_by,insert_date,status_active"; 
					$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",'".$fabric_machine_level_html."',".$cbo_fabric_machine_level.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				}
				else if(str_replace("'","",$cbo_variable_list_production)==13)
				{
					$field_array="id, company_name,variable_list, batch_maintained_hcode, batch_maintained, inserted_by,insert_date,status_active"; 
					$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",'".$batch_maintained_html."',".$cbo_batch_maintained.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				}
				
				else if(str_replace("'","",$cbo_variable_list_production)==23)
				{
					$field_array="id, company_name,variable_list, auto_update, inserted_by,insert_date,status_active"; 
					$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$cbo_prod_resource.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				}
				else if(str_replace("'","",$cbo_variable_list_production)==24)
				{
					$field_array="id, company_name,variable_list, batch_no_creation, inserted_by,insert_date,status_active"; 
					$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$cbo_batch_no.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				}
				else if(str_replace("'","",$cbo_variable_list_production)==25)
				{
					$field_array="id, company_name, variable_list, smv_source, inserted_by,insert_date,status_active"; 
					$data_array="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$cbo_source.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				}
				else if(str_replace("'","",$cbo_variable_list_production)==26)
				{
					$field_array="id, company_name, variable_list, shift_id, prod_start_time, lunch_start_time, inserted_by,insert_date"; 
					$data_array='';
					for($i=1;$i<=$total_row;$i++)
					{
						$shift_id="shift_id".$i;
						$update_id="update_id".$i;
						$txt_prod_start_time="txt_prod_start_time".$i;
						$txt_lunch_start_time="txt_lunch_start_time".$i;
						
						if($db_type==0)
						{
							if($data_array!="") $data_array.=",";
							$data_array.="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$shift_id.",".$$txt_prod_start_time.",".$$txt_lunch_start_time.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
						}
						else if($db_type==2)
						{
							
							$prod_start_time="to_date(".$$txt_prod_start_time.",'HH24:MI:SS')";
							$lunch_start_time="to_date(".$$txt_lunch_start_time.",'HH24:MI:SS')";
							
							$data_array.="INTO variable_settings_production (".$field_array.") VALUES(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$shift_id.",".$prod_start_time.",".$lunch_start_time.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";	
						}
						
						$id=$id+1;
					}
					$query="INSERT ALL ".$data_array." SELECT * FROM dual";
				}
				
				//print_r($data_array);
				//echo "insert into variable_settings_production ($field_array) values($data_array)";die;
				if(str_replace("'","",$cbo_variable_list_production)==26 && $db_type==2)
				{
					//echo $query;
					$rID=execute_query($query);
				}
				else
				{
					$rID=sql_insert("variable_settings_production",$field_array,$data_array,1);
				}
				
				//echo "10*******".$rID;die;
				
			}
		}
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo 0;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo 10;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		}
		disconnect($con);
		die;
		 
	}
	
	else if ($operation==1)   // Update Here
	{
		
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
					
		if(str_replace("'","",$cbo_variable_list_production)==1)
		{	
			$field_array="company_name*variable_list*cutting_update_hcode*cutting_update*cutting_input_hcode*cutting_input*printing_emb_production_hcode*printing_emb_production*sewing_production_hcode*sewing_production*iron_update_hcode*iron_update*finishing_update_hcode*finishing_update*ex_factory*production_entry_hcode*production_entry*updated_by*update_date*status_active"; 
			$data_array="".$cbo_company_name_production."*".$cbo_variable_list_production."*'".$cutting_update_html."'*".$cbo_cutting_update."*'".$cutting_delevary_input_html."'*".$cbo_cutting_update_to_input."*'".$printing_emb_production_html."'*".$cbo_printing_emb_production."*'".$sewing_production_html."'*".$cbo_sewing_production."*'".$iron_update_html."'*".$cbo_iron_update."*'".$finishing_update_html."'*".$cbo_finishing_update."*".$cbo_ex_factory."*'".$production_entry_html."'*".$cbo_production_entry."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
			$rID=sql_update("variable_settings_production",$field_array,$data_array,"id","".$update_id."",1);
		}
		else if(str_replace("'","",$cbo_variable_list_production)==2)
		{
			
			sql_select("delete from variable_prod_excess_slab where company_name=$cbo_company_name_production and variable_list=$cbo_variable_list_production");
			
			$slab_id = return_next_id( "id", "variable_prod_excess_slab", 1);
			$field_array="id, company_name,variable_list, slab_rang_start, slab_rang_end, excess_percent, inserted_by,insert_date,status_active"; 
			 
			for($i=1;$i<=$counter;$i++)
			{
				
				$txt_slab_rang_start = 'txt_slab_rang_start'.$i;
				$txt_slab_rang_end = 'txt_slab_rang_end'.$i;
				$txt_excess_percent = 'txt_excess_percent'.$i;
				if($i==1)
					$data_array.="(".$slab_id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$txt_slab_rang_start.",".$$txt_slab_rang_end.",".$$txt_excess_percent.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				else
					$data_array.=",(".$slab_id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$txt_slab_rang_start.",".$$txt_slab_rang_end.",".$$txt_excess_percent.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
				$slab_id++;
				
			}	
			 $rID=sql_insert("variable_prod_excess_slab",$field_array,$data_array,1); 
		}
		else if(str_replace("'","",$cbo_variable_list_production)==3)
		{
			$field_array="company_name*variable_list*fabric_roll_level_hcode*fabric_roll_level*updated_by*update_date*status_active"; 
			$data_array="".$cbo_company_name_production."*".$cbo_variable_list_production."*'".$fabric_roll_level_html."'*".$cbo_fabric_roll_level."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
			$rID=sql_update("variable_settings_production",$field_array,$data_array,"id","".$update_id."",1);
		} 
		else if(str_replace("'","",$cbo_variable_list_production)==4)
		{
			$field_array="company_name*variable_list*fabric_machine_level_hcode*fabric_machine_level*updated_by*update_date*status_active"; 
			$data_array="".$cbo_company_name_production."*".$cbo_variable_list_production."*'".$fabric_machine_level_html."'*".$cbo_fabric_machine_level."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
			$rID=sql_update("variable_settings_production",$field_array,$data_array,"id","".$update_id."",1);
		}
		else if(str_replace("'","",$cbo_variable_list_production)==13)
		{
			$field_array="company_name*variable_list*batch_maintained_hcode*batch_maintained*updated_by*update_date*status_active"; 
			$data_array="".$cbo_company_name_production."*".$cbo_variable_list_production."*'".$batch_maintained_html."'*".$cbo_batch_maintained."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
			$rID=sql_update("variable_settings_production",$field_array,$data_array,"id","".$update_id."",1);
		}
		else if(str_replace("'","",$cbo_variable_list_production)==15)
		{
			$field_array="company_name*variable_list*item_category_id*auto_update*updated_by*update_date"; 
			
			$updateID_array[]=str_replace("'","",$update_id_1);
			$updateID_array[]=str_replace("'","",$update_id_2);
			
			$data_array[str_replace("'","",$update_id_1)]=explode("*",("".$cbo_company_name_production."*".$cbo_variable_list_production."*".$cbo_item_category_1."*".$cbo_auto_update_1."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));
			
			$data_array[str_replace("'","",$update_id_2)]=explode("*",("".$cbo_company_name_production."*".$cbo_variable_list_production."*".$cbo_item_category_2."*".$cbo_auto_update_2."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));
			
			$rID=execute_query(bulk_update_sql_statement("variable_settings_production","id",$field_array,$data_array,$updateID_array),1);
		
		}
		else if(str_replace("'","",$cbo_variable_list_production)==23)
		{
			$field_array="company_name*variable_list*auto_update*updated_by*update_date*status_active"; 
			$data_array="".$cbo_company_name_production."*".$cbo_variable_list_production."*".$cbo_prod_resource."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
			$rID=sql_update("variable_settings_production",$field_array,$data_array,"id","".$update_id."",1);
		}
		else if(str_replace("'","",$cbo_variable_list_production)==24)
		{
			$field_array="company_name*variable_list*batch_no_creation*updated_by*update_date*status_active"; 
			$data_array="".$cbo_company_name_production."*".$cbo_variable_list_production."*".$cbo_batch_no."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
			$rID=sql_update("variable_settings_production",$field_array,$data_array,"id","".$update_id."",1);
		}
		else if(str_replace("'","",$cbo_variable_list_production)==25)
		{
			$field_array="company_name*variable_list*smv_source*updated_by*update_date*status_active"; 
			$data_array="".$cbo_company_name_production."*".$cbo_variable_list_production."*".$cbo_source."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1";
			$rID=sql_update("variable_settings_production",$field_array,$data_array,"id","".$update_id."",1);
		}
		else if(str_replace("'","",$cbo_variable_list_production)==26)//sewing production
		{
			$field_array="shift_id*prod_start_time*lunch_start_time*updated_by*update_date"; 
			for($i=1;$i<=$total_row;$i++)
			{
				$shift_id="shift_id".$i;
				$update_id="update_id".$i;
				$txt_prod_start_time="txt_prod_start_time".$i;
				$txt_lunch_start_time="txt_lunch_start_time".$i;
				
				if($db_type==0)
				{
					$prod_start_time=$$txt_prod_start_time;
					$lunch_start_time=$$txt_lunch_start_time;
				}
				else
				{
					$prod_start_time="to_date(".$$txt_prod_start_time.",'HH24:MI:SS')";
					$lunch_start_time="to_date(".$$txt_lunch_start_time.",'HH24:MI:SS')";
				}
				
				$id_arr[]=str_replace("'",'',$$update_id);
				
				$data_array_update[str_replace("'",'',$$update_id)] = explode("*",($$shift_id."*".$prod_start_time."*".$lunch_start_time."*".$user_id."*'".$pc_date_time."'"));
			}
			//echo bulk_update_sql_statement( "variable_settings_production", "id", $field_array, $data_array_update, $id_arr );
			$rID=execute_query(bulk_update_sql_statement( "variable_settings_production", "id", $field_array, $data_array_update, $id_arr ));
			
			/*$field_array="id, company_name, variable_list, shift_id, prod_start_time, lunch_start_time, inserted_by,insert_date"; 
			$data_array='';
			$id=return_next_id( "id", "variable_settings_production", 1 ) ;
			for($i=1;$i<=$total_row;$i++)
			{
				$shift_id="shift_id".$i;
				$update_id="update_id".$i;
				$txt_prod_start_time="txt_prod_start_time".$i;
				$txt_lunch_start_time="txt_lunch_start_time".$i;
				
				if($db_type==0)
				{
					if($data_array!="") $data_array.=",";
					$data_array.="(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$shift_id.",".$$txt_prod_start_time.",".$$txt_lunch_start_time.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				}
				else if($db_type==2)
				{
					$prod_start_time="to_date(".$$txt_prod_start_time.",'HH24:MI:SS')";
					$lunch_start_time="to_date(".$$txt_lunch_start_time.",'HH24:MI:SS')";
					
					$data_array.="INTO variable_settings_production (".$field_array.") VALUES(".$id.",".$cbo_company_name_production.",".$cbo_variable_list_production.",".$$shift_id.",".$prod_start_time.",".$lunch_start_time.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";	
				}
				
				$id=$id+1;
			}
			$query="INSERT ALL ".$data_array." SELECT * FROM dual";
			
			$delete_roll=execute_query( "delete from variable_settings_production where company_name=$cbo_company_name_production and variable_list=26",0);
			if($db_type==2)
			{
				//echo $query;
				$rID=execute_query($query);
			}
			else
			{
				$rID=sql_insert("variable_settings_production",$field_array,$data_array,1);
			}*/
		}
		 
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo 1;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo 10;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		}
		
		disconnect($con);
		die;
	}	
	
}

?>