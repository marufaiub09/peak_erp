<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../includes/common.php');
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 150, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "--Select Location--", $selected, "","","","","","",3 );
	exit();	 
}

if ($action=="load_drop_down_supplier_name")
{
	echo create_drop_down( "cbo_supplier_company", 150, "select sup.id, sup.supplier_name from lib_supplier sup,lib_supplier_tag_company b where sup.status_active=1 and sup.is_deleted=0 and b.supplier_id=sup.id and b.tag_company='$data' $supplier_cond and sup.id in (select  supplier_id from  lib_supplier_party_type where party_type=21) order by supplier_name", "id,supplier_name", 1, "-- Select supplier --", $selected, "show_list_view(document.getElementById('cbo_company_id').value+'_'+this.value,'outside_finishing_info_list','outside_finishing_info_list','requires/outside_finishing_bill_entry_controller','setFilterGrid(\'list_view_issue\',-1)');","","","","","",5 );
	exit();
}
if ($action=="load_drop_down_supplier_name_pop")
{
	echo create_drop_down( "cbo_supplier_company", 150, "select sup.id, sup.supplier_name from lib_supplier sup,lib_supplier_tag_company b where sup.status_active=1 and sup.is_deleted=0 and b.supplier_id=sup.id and b.tag_company='$data' $supplier_cond and sup.id in (select  supplier_id from  lib_supplier_party_type where party_type=21) order by supplier_name", "id,supplier_name", 1, "-- Select supplier --", $selected, "","","","","","",5 );
	exit();
}

if ($action=="outside_finishing_info_list")
{
	echo load_html_head_contents("Popup Info","../", 1, 1, $unicode,1,'');
	$from=1;
	$data=explode('_',$data);
	?>
	<script>
	
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >	
            <div style="width:100%;">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="827px" class="rpt_table">
                    <thead>
                        <th width="30">SL</th>
                        <th width="70">Challan No</th>
                        <th width="70">Recive Date</th>
                        <th width="110">Recive No</th> 
                        <th width="180">Fabric Description</th>
                        <th width="60">Receive Qty</th>
                        <th width="80">Order No</th>
                        <th width="100">Style Ref.</th>
                        <th width="">Buyer</th>
                    </thead>
                </table>
            </div>
        <div style="width:830px;max-height:180px; overflow-y:scroll" id="kintt_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="810px" class="rpt_table" id="list_view_issue">
            <?php 
			$po_arr=return_library_array( "select id,po_number from  wo_po_break_down",'id','po_number');
			$buyer_arr=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
            $item_id_arr=return_library_array( "select id,product_name_details from  product_details_master",'id','product_name_details');
			
			$order_array=array();
			$sql_order="Select a.id, a.po_number, b.style_ref_no, b.buyer_name from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1";
			$sql_order_result=sql_select($sql_order);
			foreach ($sql_order_result as $row)
			{
				$order_array[$row[csf("id")]]['po_number']=$row[csf("po_number")];
				$order_array[$row[csf("id")]]['style_ref_no']=$row[csf("style_ref_no")];
				$order_array[$row[csf("id")]]['buyer_name']=$row[csf("buyer_name")];
			}
 
            $i=1;
			if($db_type==0)
			{
				$group_cond= " group by a.challan_no, c.po_breakdown_id";
			}
			else if($db_type==2)
			{
				$group_cond= " group by c.id, a.currency_id, a.challan_no, a.receive_date, a.recv_number, a.buyer_id, b.prod_id, c.po_breakdown_id, c.dtls_id";
			}
			
			if($db_type==0)
			{
				if($data[2]=="") // Insert
				{
				   $sql="SELECT c.id, a.currency_id, a.challan_no, a.receive_date, a.recv_number, a.buyer_id, b.prod_id, sum(c.quantity) as order_qnty, c.po_breakdown_id, c.dtls_id, 0 as biid FROM inv_receive_master a,  pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 and a.company_id =$data[0] AND a.knitting_company=$data[1] and a.id not in (select id from inv_receive_master where knitting_source=3 and a.company_id =$data[0] AND entry_form=37 AND knitting_company=$data[1] and receive_basis in (1,6,9)) and a.item_category=2 and a.status_active=1 AND a.id NOT IN (SELECT receive_id FROM subcon_outbound_bill_dtls) $group_cond ";
				}
				else
				{
				   $sql="SELECT c.id, a.currency_id, a.challan_no, a.receive_date, a.recv_number, a.buyer_id, b.prod_id, sum(c.quantity) as order_qnty, c.po_breakdown_id, c.dtls_id, 0 as biid FROM inv_receive_master a,  pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 and a.company_id =$data[0] AND a.knitting_company=$data[1] and a.id not in (select id from inv_receive_master where knitting_source=3 and company_id =$data[0] AND entry_form=37 AND knitting_company=$data[1] and receive_basis in (1,6,9)) and a.item_category=2 and a.status_active=1 AND a.id NOT IN (SELECT receive_id FROM subcon_outbound_bill_dtls) $group_cond 
		UNION 
		SELECT c.id, a.currency_id, a.challan_no, a.receive_date, a.recv_number, a.buyer_id, b.prod_id, sum(c.quantity) as order_qnty, c.po_breakdown_id, c.dtls_id, 1 as biid FROM inv_receive_master a,  pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 and a.company_id =$data[0] AND a.knitting_company=$data[1] and a.id not in (select id from inv_receive_master where knitting_source=3 and company_id =$data[0] AND entry_form=37 AND knitting_company=$data[1] and receive_basis in (1,6,9)) and a.item_category=2 and a.status_active=1 AND a.id 
		IN (SELECT receive_id FROM subcon_outbound_bill_dtls WHERE mst_id=$data[2]) $group_cond";
				}
			}
			elseif($db_type==2)
			{
				if($data[2]=="") // Insert
				{
				   $sql="SELECT c.id, a.currency_id, a.challan_no, a.receive_date, a.recv_number, a.buyer_id, b.prod_id, sum(c.quantity) as order_qnty, c.po_breakdown_id, c.dtls_id, 0 as biid FROM inv_receive_master a,  pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 and a.company_id =$data[0] AND a.knitting_company=$data[1] and a.id not in (select id from inv_receive_master where knitting_source=3 and a.company_id =$data[0] AND entry_form=37 AND knitting_company=$data[1] and receive_basis in (1,6,9)) and a.item_category=2 and a.status_active=1 AND a.id NOT IN (SELECT receive_id FROM subcon_outbound_bill_dtls) $group_cond ";
				}
				else
				{
				   $sql="SELECT c.id, a.currency_id, a.challan_no, a.receive_date, a.recv_number, a.buyer_id, b.prod_id, sum(c.quantity) as order_qnty, c.po_breakdown_id, c.dtls_id, 0 as biid FROM inv_receive_master a,  pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 and a.company_id =$data[0] AND a.knitting_company=$data[1] and a.id not in (select id from inv_receive_master where knitting_source=3 and company_id =$data[0] AND entry_form=37 AND knitting_company=$data[1] and receive_basis in (1,6,9)) and a.item_category=2 and a.status_active=1 AND a.id NOT IN (SELECT receive_id FROM subcon_outbound_bill_dtls) $group_cond 
		UNION 
		SELECT c.id, a.currency_id, a.challan_no, a.receive_date, a.recv_number, a.buyer_id, b.prod_id, sum(c.quantity) as order_qnty, c.po_breakdown_id, c.dtls_id, 1 as biid FROM inv_receive_master a,  pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 and a.company_id =$data[0] AND a.knitting_company=$data[1] and a.id not in (select id from inv_receive_master where knitting_source=3 and company_id =$data[0] AND entry_form=37 AND knitting_company=$data[1] and receive_basis in (1,6,9)) and a.item_category=2 and a.status_active=1 AND a.id 
		IN (SELECT receive_id FROM subcon_outbound_bill_dtls WHERE mst_id=$data[2]) $group_cond";
				}
			}
			
			$sql_result=sql_select($sql);
			
			foreach($sql_result as $row)
			{
				if ($i%2==0)
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";
			
				if($row[csf('biid')]==1) $bgcolor="#FFCC00";
				?>
				<tr id="tr_<?php echo $row[csf('id')]; ?>" bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]; ?>');" >
                    <td width="30" align="center"><?php echo $i; ?></td>
                    <td width="70" align="center"><p><?php echo $row[csf('challan_no')]; ?></p></td>
                    <td width="70" align="center"><?php echo change_date_format($row[csf('receive_date')]); ?></td>
                    <td width="110" align="center"><?php echo $row[csf('recv_number')]; ?></td>
                    <td width="180" align="center"><?php echo $item_id_arr[$row[csf('prod_id')]]; ?></td>
                    <td width="60" align="center"><?php echo $row[csf('order_qnty')]; ?></td>
                    <td width="80" align="center"><?php echo $order_array[$row[csf("po_breakdown_id")]]['po_number']; ?></td>
                    <td width="100" align="center"><?php echo $order_array[$row[csf("po_breakdown_id")]]['style_ref_no']; ?></td>
                    <td align="center"><?php echo $buyer_arr[$order_array[$row[csf("po_breakdown_id")]]['buyer_name']]; ?>
                    <input type="hidden" id="currid<?php echo $row[csf('id')]; ?>" value="<?php echo $row[csf('id')]; ?>"></td>
                    </tr>
                    <?php
                    $i++;
				}
				?>
                </table>
                </div>
                 <table width="810px" >
				<tr>
                    <td colspan="9" align="center">
                    <input type="button" id="show_button" class="formbutton" style="width:100px" value="Close" onClick="window_close()" />
                    </td>
                </tr>
            </table>
        </div>
	</div>
	</body>           
	<script src="../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if ($action=="load_php_dtls_form")
{
	$data = explode("_",$data);
	//echo $data[0].'=='.$data[1];
	$del_id=array_diff(explode(",",$data[0]), explode(",",$data[1]));
	$bill_id=array_intersect(explode(",",$data[0]), explode(",",$data[1]));
	$delete_id=array_diff(explode(",",$data[1]), explode(",",$data[0]));
	$delete_id=implode(",",$delete_id); $del_id=implode(",",$del_id); $bill_id=implode(",",$bill_id);
	//echo $delete_id.'=='.$del_id.'=='.$bill_id;
	$order_array=array();
	$sql_order="Select a.id, a.po_number, b.style_ref_no, b.buyer_name from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1";
	$sql_order_result=sql_select($sql_order);
	foreach ($sql_order_result as $row)
	{
		$order_array[$row[csf("id")]]['po_number']=$row[csf("po_number")];
		$order_array[$row[csf("id")]]['style_ref_no']=$row[csf("style_ref_no")];
		$order_array[$row[csf("id")]]['buyer_name']=$row[csf("buyer_name")];
	}
	//var_dump($order_array);
    $buyer_arr=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name'); 
	$roll_no_arr=return_library_array( "select id, no_of_roll from  pro_grey_prod_entry_dtls",'id','no_of_roll');
    $item_id_arr=return_library_array( "select id,product_name_details from  product_details_master",'id','product_name_details');
	
	if( $data[2]!="" )//update===========
	{
			$sql="SELECT id as upd_id, receive_id, receive_date, challan_no, order_id, item_id as prod_id, roll_no as no_of_roll, receive_qty as order_qnty, uom, 0 as dtls_id, rate, amount, remarks FROM subcon_outbound_bill_dtls  WHERE mst_id=$data[2] and process_id=4";
	}
	else //insert=================order_wise_pro_details
	{
		if($db_type==0)
		{
			if($bill_id!="" && $del_id!="")
				$sql="(SELECT id as upd_id, receive_id, receive_date, challan_no, order_id, item_id as prod_id, roll_no as no_of_roll, receive_qty as order_qnty, uom, 0 as dtls_id, rate, amount, remarks  FROM subcon_outbound_bill_dtls  WHERE receive_id in ($bill_id) and process_id='4' )
				 union 
				 (SELECT 0 as upd_id, c.id as receive_id, a.receive_date, a.challan_no, c.po_breakdown_id as order_id, b.prod_id, b.no_of_roll, sum(c.quantity) as order_qnty, 0 as uom, c.dtls_id, 0 as rate, 0 as amount, null as remarks FROM inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 AND c.id in ($del_id) and a.item_category=2 and a.id not in (select id from inv_receive_master where knitting_source=3 AND entry_form=37 AND receive_basis in (1,6,9)) group by c.id, a.receive_date, a.challan_no, c.po_breakdown_id, b.prod_id, b.no_of_roll, c.dtls_id)";
			else if($bill_id!="" && $del_id=="")
				$sql="SELECT id as upd_id, receive_id, receive_date, challan_no, order_id, item_id as prod_id, roll_no as no_of_roll, receive_qty as order_qnty, uom, 0 as dtls_id, rate, amount, remarks  FROM subcon_outbound_bill_dtls  WHERE receive_id in ($bill_id) and process_id='4' ";
			else if($bill_id=="" && $del_id!="")
				$sql="SELECT 0 as upd_id, c.id as receive_id, a.receive_date, a.challan_no, c.po_breakdown_id as order_id, b.prod_id, b.no_of_roll, sum(c.quantity) as order_qnty, 0 as uom, c.dtls_id, 0 as rate, 0 as amount, null as remarks FROM inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 AND c.id in ($del_id) and a.item_category=2 and a.id not in (select id from inv_receive_master where knitting_source=3 AND entry_form=37 AND receive_basis in (1,6,9)) group by c.id, a.receive_date, a.challan_no, c.po_breakdown_id, b.prod_id, b.no_of_roll, c.dtls_id";
		}
		elseif($db_type==2)
		{
			if($bill_id!="" && $del_id!="")
				$sql="(SELECT id as upd_id, receive_id, receive_date, challan_no, order_id, item_id as prod_id, roll_no as no_of_roll, receive_qty as order_qnty, uom, 0 as dtls_id, rate, amount, remarks  FROM subcon_outbound_bill_dtls  WHERE receive_id in ($bill_id) and process_id='4' )
				 union 
				 (SELECT 0 as upd_id, c.id as receive_id, a.receive_date, a.challan_no, c.po_breakdown_id as order_id, b.prod_id, b.no_of_roll, sum(c.quantity) as order_qnty, 0 as uom, c.dtls_id, 0 as rate, 0 as amount, null as remarks FROM inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 AND c.id in ($del_id) and a.item_category=2 and a.id not in (select id from inv_receive_master where knitting_source=3 AND entry_form=37 AND receive_basis in (1,6,9)) group by c.id, a.receive_date, a.challan_no, c.po_breakdown_id, b.prod_id, b.no_of_roll, c.dtls_id)";
			else if($bill_id!="" && $del_id=="")
				$sql="SELECT id as upd_id, receive_id, receive_date, challan_no, order_id, item_id as prod_id, roll_no as no_of_roll, receive_qty as order_qnty, uom, 0 as dtls_id, rate, amount, remarks  FROM subcon_outbound_bill_dtls  WHERE receive_id in ($bill_id) and process_id='4' ";
			else if($bill_id=="" && $del_id!="")
				$sql="SELECT 0 as upd_id, c.id as receive_id, a.receive_date, a.challan_no, c.po_breakdown_id as order_id, b.prod_id, b.no_of_roll, sum(c.quantity) as order_qnty, 0 as uom, c.dtls_id, 0 as rate, 0 as amount, null as remarks FROM inv_receive_master a, pro_finish_fabric_rcv_dtls b, order_wise_pro_details c WHERE a.id = b.mst_id and b.id=c.dtls_id and c.trans_type=1 and c.entry_form in (7,37) AND a.knitting_source=3 AND c.id in ($del_id) and a.item_category=2 and a.id not in (select id from inv_receive_master where knitting_source=3 AND entry_form=37 AND receive_basis in (1,6,9)) group by c.id, a.receive_date, a.challan_no, c.po_breakdown_id, b.prod_id, b.no_of_roll, c.dtls_id";
		}
	}
	
	$sql_result=sql_select($sql);
	$k=0;
	$num_rowss=count($sql_result);
	foreach ($sql_result as $row)
	{
		 $k++;
		 if( $data[2]!="" )
		 {
			 if($data[1]=="") $data[1]=$row[csf("receive_id")]; else $data[1].=",".$row[csf("receive_id")];
		 }
	?>
         <tr align="center">				
            <td>
				 <?php if ($k==$num_rowss) { ?>
                    <input type="hidden" name="issue_id_all" id="issue_id_all"  style="width:80px" value="<?php echo $data[1]; ?>" />
                    <input type="hidden" name="delete_id" id="delete_id"  style="width:80px" value="<?php echo $delete_id; ?>" />
                 <?php } ?>
                <input type="hidden" name="updateiddtls_<?php echo $k; ?>" id="updateiddtls_<?php echo $k; ?>" value="<?php echo ($row[csf("upd_id")] != 0 ? $row[csf("upd_id")] : "") ?>">
                <input type="hidden" name="reciveid_<?php echo $k; ?>" id="reciveid_<?php echo $k; ?>" value="<?php echo $row[csf("receive_id")]; ?>"> 
                <input type="date" name="txtreceivedate_<?php echo $k; ?>" id="txtreceivedate_<?php echo $k; ?>"  class="datepicker" style="width:80px" value="<?php echo change_date_format($row[csf("receive_date")]); ?>" readonly />									
            </td>
            <td>
                <input type="text" name="txtchallenno_<?php echo $k; ?>" id="txtchallenno_<?php echo $k; ?>"  class="text_boxes" style="width:70px" value="<?php echo $row[csf("challan_no")]; ?>" readonly />							 
            </td>
            <td>
                <input type="hidden" name="ordernoid_<?php echo $k; ?>" id="ordernoid_<?php echo $k; ?>" value="<?php echo $row[csf("order_id")]; ?>" style="width:60px" readonly /> 
                <input type="text" name="txtorderno_<?php echo $k; ?>" id="txtorderno_<?php echo $k; ?>"  class="text_boxes" style="width:60px" value="<?php echo $order_array[$row[csf("order_id")]]['po_number']; ?>" readonly />										
            </td>
            <td>
                <input type="text" name="txtstylename_<?php echo $k; ?>" id="txtstylename_<?php echo $k; ?>"  class="text_boxes" style="width:80px;" value="<?php echo $order_array[$row[csf("order_id")]]['style_ref_no']; ?>" readonly />
            </td>
            <td>
                <input type="text" name="txtpartyname_<?php echo $k; ?>" id="txtpartyname_<?php echo $k; ?>"  class="text_boxes" style="width:60px" value="<?php echo $buyer_arr[$row[csf("buyer_id")]]; ?>" readonly />								
            </td>
            <td>			
                <input name="txtnumberroll_<?php echo $k; ?>" id="txtnumberroll_<?php echo $k; ?>" type="text" class="text_boxes" style="width:40px" value="<?php echo $row[csf("no_of_roll")]; ?>" readonly />							
            </td> 
            <td>
                <input type="hidden" name="itemid_<?php echo $k; ?>" id="itemid_<?php echo $k; ?>" value="<?php echo $row[csf("prod_id")]; ?>">
                <input type="text" name="textfebricdesc_<?php echo $k; ?>" id="textfebricdesc_<?php echo $k; ?>"  class="text_boxes" style="width:100px" value="<?php echo $item_id_arr[$row[csf("prod_id")]]; ?>" readonly />
            </td>
            <td>
            	<input type="hidden" name="textwonumid_<?php echo $k; ?>" id="textwonumid_<?php echo $k; ?>" value="<?php echo $row[csf("wo_num_id")]; ?>">
                <input type="text" name="textwonum_<?php echo $k; ?>" id="textwonum_<?php echo $k; ?>"  class="text_boxes" style="width:60px" value="<?php echo $booking_arr[$row[csf("wo_num_id")]]; ?>" placeholder="Browse" onDblClick="openmypage_wonum();" readonly />
            </td>
            <td>
                <input type="text" name="txtfabqnty_<?php echo $k; ?>" id="txtfabqnty_<?php echo $k; ?>"  class="text_boxes_numeric" style="width:60px" value="<?php echo $row[csf("order_qnty")]; ?>" readonly />
            </td>
            <td>
				<?php echo create_drop_down( "cbouom_$k", 55, $unit_of_measurement,"", 0, "--Select UOM--",12,"",1,$row[csf("uom")],"" );?>
            </td>
            <td>
                <input type="text" name="txtrate_<?php echo $k; ?>" id="txtrate_<?php echo $k; ?>"  class="text_boxes_numeric" style="width:40px" value="<?php echo $row[csf("rate")]; ?>" onBlur="amount_caculation(<?php echo $k; ?>);" />
            </td>
            <td>
                <input type="text" name="txtamount_<?php echo $k; ?>" id="txtamount_<?php echo $k; ?>" style="width:60px"  class="text_boxes_numeric"  value="<?php echo $row[csf("amount")]; ?>" readonly />
            </td>
            <td>
                <input type="text" name="txtremarks_<?php echo $k; ?>" id="txtremarks_<?php echo $k; ?>"  class="text_boxes" style="width:80px" value="<?php echo $row[csf("remarks")]; ?>" />
            </td>
        </tr>
	<?php	
	}
	exit();
}

if ($action=="wonum_popup")
{
	echo load_html_head_contents("Popup Info", "../../", 1, 1,'',1,'');
	$data=explode('_',$data);
	?>	
    <script>
		  function js_set_value(id)
		  { 
			  document.getElementById('hidd_item_id').value=id;
			  parent.emailwindow.hide();
		  }
	</script>
    </head>
    <body>
        <form name="searchpofrm"  id="searchpofrm">
        <input type="hidden" id="hidd_item_id" />
        <div style="width:100%;">
        <table cellspacing="0" width="100%" class="rpt_table">
            <thead>
                <th width="50">SL</th>
                <th width="150" align="center">Wo No</th>
                <th width="150" align="center">Supplier id </th>
                <th width="100" align="center">Color</th>
                <th width="100" align="center">Construction</th>
                <th width="100" align="center">Copmposition</th>
                <th width="50" align="center">GSM</th>
                <th width="50" align="center">Dia</th>
                <th width="50" align="center">Rate</th>                    
                <th width="" align="center">Uom</th>
            </thead>
        </table>
        </div>
        <div style="width:100%;max-height:180px; overflow:y-scroll" id="sewing_production_list_view" align="left">
        <table cellspacing="0" width="100%" class="rpt_table">
			<?php  
                $supplier_library_arr=return_library_array( "select id,supplier_name from lib_supplier", "id","supplier_name"  );
				$color_library_arr=return_library_array( "select id,color_name from lib_color", "id","color_name"  );
			    $i=1;
				$sql_result=sql_select("select a.id,a.booking_no,a.supplier_id,b.color_size_table_id,b.rate,b.construction,b.copmposition,b.gsm_weight,b.dia_width,b.uom,b.process from wo_booking_mst a,wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$data[0] and a.supplier_id=$data[1] and b.process in (31,32,33,34,35,36,37,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88)");
                foreach($sql_result as $row)
                {
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]."_".$row[csf('booking_no')]."_".$row[csf('rate')]; ?>');" > 
                        <td width="50" align="center"><?php echo $i; ?></td>
                        <td width="150" align="center"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                        <td width="150" align="center"><?php echo $supplier_library_arr[$row[csf('supplier_id')]]; ?></td>
                        <td width="100" align="center"><?php echo $row[csf('color_size_table_id')]; ?></td>
                        <td width="100" align="center"><?php echo $row[csf('construction')]; ?></td>
                        <td width="100" align="center"><?php echo $row[csf('copmposition')]; ?></td>
                        <td width="50" align="center"><?php echo $row[csf('gsm_weight')]; ?></td>
                        <td width="50" align="center"><?php echo $row[csf('dia_width')]; ?></td>
                        <td width="50" align="center"><?php echo $row[csf('rate')]; ?></td>
                        <td width="" align="center"><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></td>
					</tr>
					<?php
					$i++;
                }
                ?>
            </table>
        </div>
    </form>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
	<?php
	exit();
}

if ($action=="load_php_data_to_form_wonum")
{
}

if ($action=="outside_bill_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	$ex_data=explode('_',$data);
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('outside_bill_id').value=id;
			parent.emailwindow.hide();
		}
    </script>
  </head>
  <body>
  <div align="center" style="width:100%;" >
  <form name="finishingbill_1"  id="finishingbill_1" autocomplete="off">
	  <table width="650" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
          <thead>                	 
              <th width="150">Company Name</th>
              <th width="150">Supplier Name</th>
              <th width="80">Bill ID</th>
              <th width="170">Date Range</th>
              <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>           
          </thead>
          <tbody>
                <tr>
                <td> <input type="hidden" id="outside_bill_id">  
					<?php   
						echo create_drop_down( "cbo_company_id", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $ex_data[0],"load_drop_down( 'outside_finishing_bill_entry_controller', this.value, 'load_drop_down_supplier_name_pop', 'supplier_td' );",0 );
                    ?>
                </td>
                <td width="140" id="supplier_td">
					<?php
						echo create_drop_down( "cbo_supplier_company", 150, "select sup.id, sup.supplier_name from lib_supplier sup,lib_supplier_tag_company b where sup.status_active=1 and sup.is_deleted=0 and b.supplier_id=sup.id and b.tag_company='$ex_data[0]' $supplier_cond and sup.id in (select  supplier_id from  lib_supplier_party_type where party_type=21) order by supplier_name", "id,supplier_name", 1, "-- Select supplier --", $ex_data[1], "","","","","","",5 );
                    ?> 
                </td>
                <td>
                    <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes_numeric" style="width:75px" />
                </td>
                <td align="center">
                    <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                    <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                </td> 
                <td align="center">
                    <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_supplier_company').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value, 'outside_finishing_bill_list_view', 'search_div', 'outside_finishing_bill_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" />
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center" height="40" valign="middle">
					<?php echo load_month_buttons(1);  ?>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="center" valign="top" id=""><div id="search_div"></div></td>
            </tr>
	  </table>    
	  </form>
    </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?php
	exit();
}

if ($action=="outside_finishing_bill_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company_name=" and company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $supplier_name=" and supplier_id='$data[1]'"; 
	if ($data[2]!="" &&  $data[3]!="") $return_date = "and bill_date between '".change_date_format($data[2], "mm-dd-yyyy", "/",1)."' and '".change_date_format($data[3], "mm-dd-yyyy", "/",1)."'"; else $return_date="";
	if ($data[4]!='') $bill_id_cond=" and prefix_no_num='$data[4]'"; else $bill_id_cond="";
	
	$location=return_library_array( "select id,location_name from lib_location",'id','location_name');
	$supplier_library_arr=return_library_array( "select id,supplier_name from lib_supplier", "id","supplier_name"  );
	
	$arr=array (2=>$location,4=>$supplier_library_arr,5=>$bill_for,6=>$production_process);
	
	if($db_type==0)
	{
		$year_cond= "year(insert_date)as year";
	}
	else if($db_type==2)
	{
		$year_cond= "TO_CHAR(insert_date,'YYYY') as year";
	}
	
	$sql= "select id, bill_no, prefix_no_num, $year_cond, location_id, bill_date, supplier_id, bill_for from subcon_outbound_bill_mst where process_id=4 and status_active=1 $company_name $party_name $return_date";
	
	echo  create_list_view("list_view", "Bill No,Year,Location Name,Bill Date,Party Name,Bill For", "70,70,100,100,120,100","600","250",0, $sql , "js_set_value", "id", "", 1, "0,0,location_id,0,supplier_id,bill_for", $arr , "prefix_no_num,year,location_id,bill_date,supplier_id,bill_for", "outside_finishing_bill_entry_controller","",'0,0,0,3,0,0') ;
	exit();	
}

if ($action=="load_php_data_to_form_outside_bill")
{
	$nameArray= sql_select("select id, bill_no, company_id, location_id, bill_date, supplier_id, bill_for from subcon_outbound_bill_mst where id='$data'");
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_bill_no').value 					= '".$row[csf("bill_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "load_drop_down( 'requires/outside_finishing_bill_entry_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_location', 'location_td' );\n";
		echo "document.getElementById('cbo_location_name').value			= '".$row[csf("location_id")]."';\n"; 
		echo "document.getElementById('txt_bill_date').value 				= '".change_date_format($row[csf("bill_date")])."';\n";   
		echo "load_drop_down( 'requires/outside_finishing_bill_entry_controller', document.getElementById('cbo_company_id').value, 'load_drop_down_supplier_name', 'supplier_td' );\n";
		echo "document.getElementById('cbo_supplier_company').value			= '".$row[csf("supplier_id")]."';\n"; 
		echo "document.getElementById('cbo_bill_for').value					= '".$row[csf("bill_for")]."';\n"; 
	    echo "document.getElementById('update_id').value            		= '".$row[csf("id")]."';\n";
	}
	exit();
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$bill_process_id="4";
	if ($operation==0)   // Insert Here========================================================================================delivery_id
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)";	
		}
		else if($db_type==2)
		{
			$year_cond=" and TO_CHAR(insert_date,'YYYY')";	
		}

		$new_bill_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'FIN', date("Y",time()), 5, "select prefix_no, prefix_no_num from  subcon_outbound_bill_mst where company_id=$cbo_company_id and process_id=$bill_process_id $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
		
		if(str_replace("'",'',$update_id)=="")
		{
			$id=return_next_id( "id", "subcon_outbound_bill_mst", 1 ) ; 	
			$field_array="id, prefix_no, prefix_no_num, bill_no, company_id, location_id, bill_date, supplier_id, bill_for, process_id, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_bill_no[1]."','".$new_bill_no[2]."','".$new_bill_no[0]."',".$cbo_company_id.",".$cbo_location_name.",".$txt_bill_date.",".$cbo_supplier_company.",".$cbo_bill_for.",".$bill_process_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
			//echo "INSERT INTO subcon_inbound_bill_mst (".$field_array.") VALUES ".$data_array; 
			$rID=sql_insert("subcon_outbound_bill_mst",$field_array,$data_array,0);
			$return_no=$new_bill_no[0]; 
		}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="location_id*bill_date*supplier_id*bill_for*updated_by*update_date";
			$data_array="".$cbo_location_name."*".$txt_bill_date."*".$cbo_supplier_company."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			$rID=sql_update("subcon_outbound_bill_mst",$field_array,$data_array,"id",$update_id,0);
			$return_no=str_replace("'",'',$txt_bill_no);
		}
			
		$id1=return_next_id( "id","subcon_outbound_bill_dtls",1);
		$field_array1 ="id, mst_id, receive_id, receive_date ,challan_no, order_id, item_id, roll_no, wo_num_id, receive_qty, uom, rate, amount, remarks, process_id, inserted_by, insert_date";
		  
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
		{
			$receive_date="txtreceivedate_".$i;
			$challen_no="txtchallenno_".$i;
			$orderid="ordernoid_".$i;
			$style_name="txtstylename_".$i;
			$party_name="txtpartyname_".$i;
			$number_roll="txtnumberroll_".$i;
			$item_id="itemid_".$i;
			$wo_number="textwonumid_".$i;
			$feb_qnty="txtfabqnty_".$i;
			$cbo_uom="cbouom_".$i;
			$rate="txtrate_".$i;
			$amount="txtamount_".$i;
			$remarks="txtremarks_".$i;
			$recive_id="reciveid_".$i;
			$updateid_dtls="updateiddtls_".$i;
			
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{
				if ($add_comma!=0) $data_array1 .=",";
				$data_array1 .="(".$id1.",".$id.",".$$recive_id.",".$$receive_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$number_roll.",".$$wo_number.",".$$feb_qnty.",".$$cbo_uom.",".$$rate.",".$$amount.",".$$remarks.",'".$bill_process_id."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id1=$id1+1;
				$add_comma++;
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$delivery_id."*".$$delevery_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$number_roll."*".$$quantity."*".$$rate."*".$$amount."*".$$remarks."*".$$curanci."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
				$id_arr_delivery[]=str_replace("'",'',$$delivery_id);
				$data_array_delivery[str_replace("'",'',$$delivery_id)] =explode("*",("1"));
			}
		}
		if($data_array1!="")
		{
			//echo "insert into subcon_outbound_bill_dtls (".$field_array1.") values ".$data_array1;die;
			$rID1=sql_insert("subcon_outbound_bill_dtls",$field_array1,$data_array1,1);
		}
		if($db_type==0)
		{
			if($rID && $rID1)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID1)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}	
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here=============================================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
	
		$id=str_replace("'",'',$update_id);
		$field_array="location_id*bill_date*supplier_id*bill_for*updated_by*update_date";
		$data_array="".$cbo_location_name."*".$txt_bill_date."*".$cbo_supplier_company."*".$cbo_bill_for."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		$rID=sql_update("subcon_outbound_bill_mst",$field_array,$data_array,"id",$update_id,0);
		$return_no=str_replace("'",'',$txt_bill_no);
		
		$id1=return_next_id( "id","subcon_outbound_bill_dtls",1);
		$field_array1="id, mst_id, receive_id, receive_date, challan_no, order_id, item_id, roll_no, wo_num_id, receive_qty, uom,rate, amount, remarks, process_id, inserted_by, insert_date";
		$field_array_up ="receive_id*receive_date*challan_no*order_id*item_id*roll_no*wo_num_id*receive_qty*uom*rate*amount*remarks*updated_by*update_date";
		$add_comma=0;
		for($i=1; $i<=$tot_row; $i++)
	  	{
			$receive_date="txtreceivedate_".$i;
			$challen_no="txtchallenno_".$i;
			$orderid="ordernoid_".$i;
			$style_name="txtstylename_".$i;
			$party_name="txtpartyname_".$i;
			$number_roll="txtnumberroll_".$i;
			$item_id="itemid_".$i;
			$wo_number="textwonumid_".$i;
			$feb_qnty="txtfabqnty_".$i;
			$cbo_uom="cbouom_".$i;
			$rate="txtrate_".$i;
			$amount="txtamount_".$i;
			$remarks="txtremarks_".$i;
			$recive_id="reciveid_".$i;
			$updateid_dtls="updateiddtls_".$i;
				
			if(str_replace("'",'',$$updateid_dtls)=="")  
			{ 
				if ($add_comma!=0) $data_array1 .=",";
				$data_array1 .="(".$id1.",".$id.",".$$recive_id.",".$$receive_date.",".$$challen_no.",".$$orderid.",".$$item_id.",".$$number_roll.",".$$wo_number.",".$$feb_qnty.",".$$cbo_uom.",".$$rate.",".$$amount.",".$$remarks.",".$bill_process_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id1=$id1+1;
				$add_comma++;
			}
			else
			{
				$id_arr[]=str_replace("'",'',$$updateid_dtls);
				$data_array_up[str_replace("'",'',$$updateid_dtls)] =explode("*",("".$$recive_id."*".$$receive_date."*".$$challen_no."*".$$orderid."*".$$item_id."*".$$number_roll."*".$$wo_number."*".$$feb_qnty."*".$$cbo_uom."*".$$rate."*".$$amount."*".$$remarks."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'"));
			}
		}
			  
		$rID1=execute_query(bulk_update_sql_statement("subcon_outbound_bill_dtls", "id",$field_array_up,$data_array_up,$id_arr ));
		if($data_array1!="")
		{
			//echo "insert into subcon_outbound_bill_dtls (".$field_array1.") values ".$data_array1;
			$rID1=sql_insert("subcon_outbound_bill_dtls",$field_array1,$data_array1,1);
		}
		
		if(str_replace("'",'',$delete_id)!="")
		{
			$delete_id=str_replace("'",'',$delete_id);
			$rID3=execute_query( "delete from subcon_outbound_bill_dtls where receive_id in ($delete_id)",0);
			$delete_id=explode(",",str_replace("'",'',$delete_id));
			for ($i=0;$i<count($delete_id);$i++)
			{
				$id_delivery[]=$delete_id[$i];
				$data_delivery[str_replace("'",'',$delete_id[$i])] =explode(",",("0"));
			}
		}
		
		if($db_type==0)
		{
			if($rID && $rID1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID1)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$return_no);
			}
		}		
		disconnect($con);
		die;
	}
}
?>