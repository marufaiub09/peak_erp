<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=$_SESSION['page_permission'];

include('../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$menu_id=$_SESSION['menu_id'];

$company_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
$kniting_company_arr=return_library_array("select id,company_name from lib_company where status_active=1 and is_deleted=0 $company_cod order by company_name","id","company_name");
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
$user_arr=return_library_array( "select id, user_name from user_passwd", "id", "user_name"  );


if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 130, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
} 


if ($action=="requisition_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
	//echo $company;die;
	?>
<script>
	function js_set_value(str)
	{
		$("#hidden_tbl_id").val(str); // wo/pi id
		parent.emailwindow.hide();
	}
	
	
	function open_job(title)
	{
		
		var company = '<?php echo $company; ?>';
		//alert(company);
		page_link='yarn_delivery_approval_controller.php?action=job_search_popup&company='+company;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=600px,height=330px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var data=this.contentDoc.getElementById("hidden_tbl_id").value.split("_");
			//alert(data[1]);
			document.getElementById('job_id').value=data[0];
			document.getElementById('txt_job').value=data[1];
	
		}
	}
	
	
	function open_order(title)
	{
		
		var company = '<?php echo $company; ?>';
		//alert(company);
		page_link='yarn_delivery_approval_controller.php?action=order_search_popup&company='+company;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=600px,height=330px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var data=this.contentDoc.getElementById("hidden_tbl_id").value.split("_");
			//alert(data[1]);
			document.getElementById('order_id').value=data[0];
			document.getElementById('txt_order').value=data[1];
	
		}
	}
	
	
	function open_progrum(title)
	{
		
		var company = '<?php echo $company; ?>';
		//alert(company);
		page_link='yarn_delivery_approval_controller.php?action=progrum_search_popup&company='+company;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=600px,height=330px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var data=this.contentDoc.getElementById("hidden_tbl_id").value.split("_");
			//alert(data[1]);
			document.getElementById('order_id').value=data[0];
			document.getElementById('txt_order').value=data[1];
	
		}
	}
	
</script>
 
<div align="center" style="width:880px;" >
<form name="searchrequisition"  id="searchrequisition" autocomplete="off">
	<table width="700" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="150">Year</th>
                <th width="150">Job No</th>
                <th width="150">Order No</th>
                <th width="150">Progrum No</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:90px" class="formbutton" onClick="reset_form('searchjob','search_div','')"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
					<?php 
						$current_year=date("Y");
                    	echo create_drop_down( "cbo_year_name", 140,$year,"id,year", 1, "-- Select Year --",$current_year,"" );
                    ?>
                    </td>
                    <td align="center" id="buyer_td">				
					<input type="text" id="txt_job" name="txt_job_no" class="text_boxes" style="width:140px;" ondblclick="open_job('Job Search')" placeholder="Browse or Write"  /><input type="hidden" id="job_id" name="job_id" />
                    </td>    
                    <td align="center">
                        <input type="text" name="txt_order" id="txt_order" class="text_boxes" style="width:140px;" ondblclick="open_order('Order Search')" placeholder="Browse or Write" /><input type="hidden" id="order_id" name="order_id" />
                     </td>
                     <td align="center">
                        <input type="text" name="txt_prog_no" id="txt_prog_no" class="text_boxes" style="width:140px;" ondblclick="open_progrum('Progrum Search')" placeholder="Browse or Write" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_job_no').value+'_'+<?php echo $company; ?>, 'create_job_search_list_view', 'search_div', 'yarn_delivery_approval_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:90px;" />				
                    </td>
            	</tr>
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>

<?php
}


if ($action=="job_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
	//echo $company;
	?>
<script>
	function js_set_value(str)
	{
		$("#hidden_tbl_id").val(str); // wo/pi id
		parent.emailwindow.hide();
	}
</script>
 
<div align="center" style="width:560px;" >
<form name="searchjob"  id="searchjob" autocomplete="off">
	<table width="470" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="130">Company</th>
                <th width="130">Buyer</th>
                <th width="100">Job No</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:80px" class="formbutton" onClick="reset_form('searchjob','search_div','')"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
					<?php 
                    	echo create_drop_down( "cbo_company_name", 130, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", str_replace("'","",$company)/*$selected */, "load_drop_down( 'yarn_delivery_approval_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );");
                    ?>&nbsp;
                    </td>
                    <td align="center" id="buyer_td">				
					<?php
                    	$blank_array="select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name";
						//echo $blank_array;
						
						
						//echo create_drop_down( "cbo_buyer_name", 130, $blank_array, 1, "-- Select Buyer --", str_replace("'","",$cbo_buyer_name), "" );
						echo create_drop_down( "cbo_buyer_name", 130, $blank_array,"id,buyer_name", 1, "-- Select Buyer --",0);
                    ?>	
                    </td>    
                    <td align="center">
                        <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:90px" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_job_no').value+'_'+<?php echo $company; ?>, 'create_job_search_list_view', 'search_div', 'yarn_delivery_approval_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:80px;" />				
                    </td>
            	</tr>
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>

<?php
}

if ($action=="create_job_search_list_view")
{
	$data=explode("_",$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_buyer_name=str_replace("'","",$data[1]);
	$txt_job_no=str_replace("'","",$data[2]);
	
	//echo $cbo_company_name."**".$cbo_buyer_name."**".$txt_job_no;
	
	
	if($cbo_company_name!=0) $cbo_company_name="and a.company_name='$cbo_company_name'"; else $cbo_company_name="";
	if($cbo_buyer_name!=0) $cbo_buyer_name="and a.buyer_name='$cbo_buyer_name'"; else $cbo_buyer_name="";
	if($txt_job_no!="") $txt_job_no="and a.job_no='$txt_job_no'"; else $txt_job_no="";
	
	$sql="select a.id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, group_concat(distinct b.po_number) as po_number from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst $cbo_company_name $cbo_buyer_name $txt_job_no group by a.job_no";
	//echo $sql;
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$arr=array(2=>$buyer_arr);
	echo  create_list_view("list_view", "Job No, Year ,Buyer, Style Ref.NO, Order No.","110,70,110,120,130","590","220",0, $sql , "js_set_value", "id,job_no", "", 1, "0,0,buyer_name,0,0", $arr, "job_no,company_name,buyer_name,style_ref_no,po_number", "",'','0,0,0,0,0,0') ;	
	echo '<input type="hidden" id="hidden_tbl_id">';
}


if ($action=="order_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
	//echo $company;
	?>
<script>
	function js_set_value(str)
	{
		$("#hidden_tbl_id").val(str); // wo/pi id
		parent.emailwindow.hide();
	}
</script>
 
<div align="center" style="width:560px;" >
<form name="searchjob"  id="searchjob" autocomplete="off">
	<table width="470" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="130">Company</th>
                <th width="130">Buyer</th>
                <th width="100">Order No</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:80px" class="formbutton" onClick="reset_form('searchjob','search_div','')"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
					<?php 
                    	echo create_drop_down( "cbo_company_name", 130, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", str_replace("'","",$company)/*$selected */, "load_drop_down( 'yarn_delivery_approval_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );");
                    ?>&nbsp;
                    </td>
                    <td align="center" id="buyer_td">				
					<?php
                    	$blank_array="select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name";
						//echo $blank_array;
						
						
						//echo create_drop_down( "cbo_buyer_name", 130, $blank_array, 1, "-- Select Buyer --", str_replace("'","",$cbo_buyer_name), "" );
						echo create_drop_down( "cbo_buyer_name", 130, $blank_array,"id,buyer_name", 1, "-- Select Buyer --",0);
                    ?>	
                    </td>    
                    <td align="center">
                        <input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:90px" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_order_no').value+'_'+<?php echo $company; ?>, 'create_order_search_list_view', 'search_div', 'yarn_delivery_approval_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:80px;" />				
                    </td>
            	</tr>
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>

<?php
}

if ($action=="create_order_search_list_view")
{
	$data=explode("_",$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_buyer_name=str_replace("'","",$data[1]);
	$txt_order_no=str_replace("'","",$data[2]);
	
	//echo $cbo_company_name."**".$cbo_buyer_name."**".$txt_job_no;
	
	
	if($cbo_company_name!=0) $cbo_company_name="and a.company_name='$cbo_company_name'"; else $cbo_company_name="";
	if($cbo_buyer_name!=0) $cbo_buyer_name="and a.buyer_name='$cbo_buyer_name'"; else $cbo_buyer_name="";
	if($txt_order_no!="") $txt_order_no="and b.po_number='$txt_order_no'"; else $txt_order_no="";
	
	$sql="select b.id,b.po_number,a.job_no,a.company_name,a.buyer_name,a.style_ref_no from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst $cbo_company_name $cbo_buyer_name $txt_order_no group by b.po_number";
	//echo $sql;
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$arr=array(3=>$buyer_arr);
	echo  create_list_view("list_view", "Job No,Order No.,Year,Buyer,Style Ref.NO","110,130,70,110,120","590","220",0, $sql , "js_set_value", "id,po_number", "", 1, "0,0,0,buyer_name,0", $arr, "job_no,po_number,company_name,buyer_name,style_ref_no", "",'','0,0,0,0,0,0') ;	
	echo '<input type="hidden" id="hidden_tbl_id">';
}


/*if ($action=="create_job_search_list_view")
{
	$data=explode("_",$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_buyer_name=str_replace("'","",$data[1]);
	$txt_job_no=str_replace("'","",$data[2]);
	
	//echo $cbo_company_name."**".$cbo_buyer_name."**".$txt_job_no;
	
	
	if($cbo_company_name!=0) $cbo_company_name="and a.company_name='$cbo_company_name'"; else $cbo_company_name="";
	if($cbo_buyer_name!=0) $cbo_buyer_name="and a.buyer_name='$cbo_buyer_name'"; else $cbo_buyer_name="";
	if($txt_job_no!="") $txt_job_no="and a.job_no='$txt_job_no'"; else $txt_job_no="";
	
	$sql="select a.id, a.job_no, a.company_name, a.buyer_name, a.style_ref_no, group_concat(distinct b.po_number) as po_number from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst $cbo_company_name $cbo_buyer_name $txt_job_no group by a.job_no";
	//echo $sql;
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$arr=array(2=>$buyer_arr);
	echo  create_list_view("list_view", "Job No, Year ,Buyer, Style Ref.NO, Order No.","110,70,110,120,130","590","220",0, $sql , "js_set_value", "id,job_no", "", 1, "0,0,buyer_name,0,0", $arr, "job_no,company_name,buyer_name,style_ref_no,po_number", "",'','0,0,0,0,0,0') ;	
	echo '<input type="hidden" id="hidden_tbl_id">';
}*/


if ($action=="progrum_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);  
	//echo $company;
	?>
<script>
	function js_set_value(str)
	{
		$("#hidden_tbl_id").val(str); // wo/pi id
		parent.emailwindow.hide();
	}
</script>
 
<div align="center" style="width:560px;" >
<form name="searchjob"  id="searchjob" autocomplete="off">
	<table width="470" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <th width="130">Company</th>
                <th width="130">Buyer</th>
                <th width="100">Program No</th>
                <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:80px" class="formbutton" onClick="reset_form('searchjob','search_div','')"  /></th>           
            </thead>
            <tbody>
                <tr>
                    <td>
					<?php 
                    	echo create_drop_down( "cbo_company_name", 130, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", str_replace("'","",$company)/*$selected */, "load_drop_down( 'yarn_delivery_approval_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );");
                    ?>&nbsp;
                    </td>
                    <td align="center" id="buyer_td">				
					<?php
                    	$blank_array="select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name";
						//echo $blank_array;
						
						
						//echo create_drop_down( "cbo_buyer_name", 130, $blank_array, 1, "-- Select Buyer --", str_replace("'","",$cbo_buyer_name), "" );
						echo create_drop_down( "cbo_buyer_name", 130, $blank_array,"id,buyer_name", 1, "-- Select Buyer --",0);
                    ?>	
                    </td>    
                    <td align="center">
                        <input type="text" name="txt_program_no" id="txt_program_no" class="text_boxes" style="width:90px" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_order_no').value+'_'+<?php echo $company; ?>, 'create_program_search_list_view', 'search_div', 'yarn_delivery_approval_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:80px;" />				
                    </td>
            	</tr>
            </tbody>
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>

<?php
}

if ($action=="create_program_search_list_view")
{
	/*$data=explode("_",$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_buyer_name=str_replace("'","",$data[1]);
	$txt_order_no=str_replace("'","",$data[2]);
	
	//echo $cbo_company_name."**".$cbo_buyer_name."**".$txt_job_no;
	
	
	if($cbo_company_name!=0) $cbo_company_name="and a.company_name='$cbo_company_name'"; else $cbo_company_name="";
	if($cbo_buyer_name!=0) $cbo_buyer_name="and a.buyer_name='$cbo_buyer_name'"; else $cbo_buyer_name="";
	if($txt_order_no!="") $txt_order_no="and b.po_number='$txt_order_no'"; else $txt_order_no="";
	
	$sql="select b.id,b.po_number,a.job_no,a.company_name,a.buyer_name,a.style_ref_no from  wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst $cbo_company_name $cbo_buyer_name $txt_order_no group by b.po_number";
	//echo $sql;
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$arr=array(3=>$buyer_arr);
	echo  create_list_view("list_view", "Job No,Order No.,Year,Buyer,Style Ref.NO","110,130,70,110,120","590","220",0, $sql , "js_set_value", "id,po_number", "", 1, "0,0,0,buyer_name,0", $arr, "job_no,po_number,company_name,buyer_name,style_ref_no", "",'','0,0,0,0,0,0') ;	
	echo '<input type="hidden" id="hidden_tbl_id">';*/
}


if($action=="report_generate")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$sequence_no='';
	$company_name=str_replace("'","",$cbo_company_name);
	
	if(str_replace("'","",$txt_req_no)!="")
	{
		if (str_replace("'","",$txt_req_id)!="")
		{
			 $txt_req_no=" and b.id in ($txt_req_id)"; 
		}
		else
		{
			$txt_req_no=" and b.requisition_no in ($txt_req_no)"; 
		}
	}
	else
	{
		$txt_req_no="";
	}
	
	
	
	$date_cond='';
	if(str_replace("'","",$txt_date)!="")
	{
		if(str_replace("'","",$cbo_get_upto)==1) $date_cond=" and a.issue_date>$txt_date";
		else if(str_replace("'","",$cbo_get_upto)==2) $date_cond=" and a.issue_date<=$txt_date";
		else if(str_replace("'","",$cbo_get_upto)==3) $date_cond=" and a.issue_date=$txt_date";
		else $date_cond='';
	}

	$approval_type=str_replace("'","",$cbo_approval_type);
	//$user_id=3;
	//$dealing_merchant_array = return_library_array("select id, team_member_name from lib_mkt_team_member_info","id","team_member_name");
	//$job_dealing_merchant_array = return_library_array("select job_no, dealing_marchant from wo_po_details_master","job_no","dealing_marchant");

	$user_sequence_no=return_field_value("sequence_no","electronic_approval_setup","page_id=$menu_id and user_id=$user_id and is_deleted=0");
	$min_sequence_no=return_field_value("min(sequence_no)","electronic_approval_setup","page_id=$menu_id and is_deleted=0");
	
	if($user_sequence_no=="")
	{
		echo "<font style='color:#F00; font-size:14px; font-weight:bold'>You Have No Authority To Sign Fabric Booking.</font>";
		die;
	}
	
	if($approval_type==0)
	{
		$sequence_no=return_field_value("max(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no<$user_sequence_no and bypass=2 and is_deleted=0");
		
		if($user_sequence_no==$min_sequence_no)
		{
			//$sql="select a.id, a.booking_no, a.item_category, a.fabric_source, a.company_id, a.booking_type, a.is_short, a.buyer_id, a.supplier_id, a.delivery_date, a.booking_date, '0' as approval_id, a.job_no, a.is_approved, a.po_break_down_id from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$company_name and a.item_category in(2,13) and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.is_approved=$approval_type $buyer_id_cond $date_cond group by a.id";
			
			//$sql="select a.id, a.booking_no, a.item_category, a.fabric_source, a.company_id, a.booking_type, a.is_short, a.buyer_id, a.supplier_id, a.delivery_date, a.booking_date, '0' as approval_id, a.job_no, a.is_approved, a.po_break_down_id, a.is_apply_last_update from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$company_name and a.item_category in(2,13) and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.ready_to_approved=1 and a.is_approved=$approval_type $buyer_id_cond $date_cond group by a.id order by a.insert_date desc";
			
						$sql="select a.id,  a.issue_number_prefix_num, year(a.insert_date) as year,  a.issue_purpose, a.issue_basis, a.issue_date, a.knit_dye_company, '0' as approval_id,  group_concat(b.requisition_no) as requisition_no  from  inv_issue_master a, inv_transaction b where a.id=b.mst_id and a.company_id=$company_name  and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.entry_form=3 $txt_req_no $date_cond  group by a.id";

			
		}
		else if($sequence_no=="")
		{
			$sequence_no_by=return_field_value("group_concat(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no<$user_sequence_no and bypass=1 and is_deleted=0");
			//$booking_id==$quotation_id
			$quotation_id=return_field_value("group_concat(distinct(mst_id)) as quotation_id","inv_issue_master a, approval_history b","a.id=b.mst_id and a.company_id=$company_name  and b.sequence_no in ($sequence_no_by) and b.entry_form=3 and b.current_approval_status=1","quotation_id");//booking_id
			
			$quotation_id_app_byuser=return_field_value("group_concat(distinct(mst_id)) as quotation_id","inv_issue_master a, approval_history b","a.id=b.mst_id and a.company_id=$company_name  and b.sequence_no=$user_sequence_no and b.entry_form=3 and b.current_approval_status=1","quotation_id");
			//$booking_id_app_byuser==$quotation_id_app_byuser
			//$booking_id_cond==$quotation_id_cond
			if($quotation_id_app_byuser!="") $quotation_id_cond=" and a.id not in($quotation_id_app_byuser)";
			else if($quotation_id!="") $quotation_id_cond.=" or (a.id in($quotation_id))";
			else $quotation_id_cond="";
		
			//$sql="select a.id,  a.company_id,  a.buyer_id, a.style_ref, a.style_desc, a.quot_date,a.est_ship_date, '0' as approval_id,  a.approved , a.inserted_by from wo_price_quotation a, wo_price_quotation_costing_mst b where a.id=b.quotation_id and a.company_id=$company_name  and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.approved=$approval_type $buyer_id_cond  $quotation_id_cond group by a.id";
			
			//$sql="select a.id, a.booking_no, a.item_category, a.fabric_source, a.company_id, a.booking_type, a.is_short, a.buyer_id, a.supplier_id, a.delivery_date, a.booking_date, '0' as approval_id, a.job_no, a.is_approved, a.po_break_down_id, a.is_apply_last_update from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$company_name and a.item_category in(2,13) and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.ready_to_approved=1 and a.is_approved=$approval_type $booking_id_cond $buyer_id_cond $date_cond group by a.id order by a.insert_date desc";
			
			$sql="select a.id,  a.issue_number_prefix_num, year(a.insert_date) as year,  a.issue_purpose, a.issue_basis, a.issue_date, a.knit_dye_company, '0' as approval_id,  group_concat(b.requisition_no) as requisition_no  from  inv_issue_master a, inv_transaction b where a.id=b.mst_id and a.company_id=$company_name  and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.entry_form=3 and a.is_approved=$approval_type $txt_req_no $date_cond $quotation_id_cond  group by a.id";

		}
		else
		{
			$user_sequence_no=$user_sequence_no-1;
			
			if($sequence_no==$user_sequence_no) $sequence_no_by_pass='';
			else
			{
				$sequence_no_by_pass=return_field_value("group_concat(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no between $sequence_no and $user_sequence_no and bypass=1 and is_deleted=0");
			}

			/*$seq_dataArray=sql_select("select max(case when bypass=2 then sequence_no end) as by_pass_no, group_concat(case when bypass=1 then sequence_no end) as by_pass_yes from electronic_approval_setup where page_id=$menu_id and sequence_no<$user_sequence_no");
			
			$sequence_no=$seq_dataArray[0][csf('by_pass_no')];
			$sequence_no_by_pass=$seq_dataArray[0][csf('by_pass_yes')];*/
			
			if($sequence_no_by_pass=="") $sequence_no_cond=" and c.sequence_no='$sequence_no'";
			else $sequence_no_cond=" and (c.sequence_no='$sequence_no' or c.sequence_no in ($sequence_no_by_pass))";
			
			//$sequence_no=return_field_value("max(sequence_no)","electronic_approval_setup","page_id=$menu_id and sequence_no<$user_sequence_no and bypass=2");
			//$sequence_no_cond=" and b.sequence_no='$sequence_no'";
			
			/*$sql="select a.id, a.booking_no, a.item_category, a.fabric_source, a.company_id, a.booking_type, a.is_short, a.buyer_id, a.supplier_id, a.delivery_date, a.booking_date, a.job_no, a.po_break_down_id, a.is_approved, b.id as approval_id, b.sequence_no, b.approved_by, a.is_apply_last_update from wo_booking_mst a, approval_history b where a.id=b.mst_id and b.entry_form=7 and a.company_id=$company_name and a.item_category in(2,13) and a.status_active=1 and a.is_deleted=0 and b.current_approval_status=1 and a.ready_to_approved=1 and a.is_approved=1 $buyer_id_cond $sequence_no_cond $date_cond order by a.insert_date desc";
			
			$sql="select a.id,  a.company_id,  a.buyer_id,  a.style_ref, a.style_desc, a.quot_date,a.est_ship_date, a.approved,a.inserted_by, b.id as approval_id from wo_price_quotation a, approval_history b where a.id=b.mst_id and b.entry_form=10 and a.company_id=$company_name  and a.status_active=1 and a.is_deleted=0 and current_approval_status=1 and a.approved=1 $buyer_id_cond $sequence_no_cond $date_cond";*/
			
			$sql="select a.id,  a.issue_number_prefix_num, year(a.insert_date) as year,  a.issue_purpose, a.issue_basis, a.issue_date, a.knit_dye_company, '0' as approval_id,  group_concat(b.requisition_no) as requisition_no  from  inv_issue_master a, inv_transaction b,  approval_history c  where a.id=b.mst_id and a.id=c.mst_id and a.company_id=$company_name  and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.entry_form=3 and a.approved=1 $txt_req_no $date_cond $sequence_no_cond  group by a.id";
		}
	}
	else
	{
		$sequence_no_cond=" and c.sequence_no='$user_sequence_no'";
		//$sql="select a.id, a.booking_no, a.item_category, a.fabric_source, a.company_id, a.booking_type, a.is_short, a.buyer_id, a.supplier_id, a.delivery_date, a.booking_date, a.job_no, a.po_break_down_id, a.is_approved, b.id as approval_id from wo_booking_mst a, approval_history b where a.id=b.mst_id and b.entry_form=7 and a.company_id=$company_name and a.item_category in(2,13) and a.status_active=1 and a.is_deleted=0 and b.current_approval_status=1 and a.is_approved=1 $buyer_id_cond $sequence_no_cond $date_cond";
		
		$sql="select a.id,  a.issue_number_prefix_num, year(a.insert_date) as year,  a.issue_purpose, a.issue_basis, a.issue_date, a.knit_dye_company, '0' as approval_id,  group_concat(b.requisition_no) as requisition_no  from  inv_issue_master a, inv_transaction b,  approval_history c  where a.id=b.mst_id and a.id=c.mst_id and a.company_id=$company_name  and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and  and a.is_approved=1 and a.entry_form=3 $txt_req_no $date_cond $sequence_no_cond  group by a.id";
	}

	//echo $sql;die;
	
	?>
    <form name="requisitionApproval_2" id="requisitionApproval_2">
        <fieldset style="width:940px; margin-top:10px">
        <legend>Fabric Booking Approval</legend>	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="920" class="rpt_table" >
                <thead>
                	<th width="50"></th>
                    <th width="40">SL</th>
                    <th width="100">System No</th>
                    <th width="80">Year</th>
                    <th width="160">Issue purpuse.</th>
                    <th width="100">Issue Date</th>
                    <th width="100">Basis</th>
                    <th width="120">Issue to</th>
                    <th width="">Requisition No</th>
                </thead>
            </table>
            <div style="width:920px; overflow-y:scroll; max-height:330px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="902" class="rpt_table" id="tbl_list_search">
                    <tbody>
                        <?php
						
                            $i=1;
                            $nameArray=sql_select( $sql );
                            foreach ($nameArray as $row)
                            {
								if ($i%2==0)  
									$bgcolor="#E9F3FF";
								else
									$bgcolor="#FFFFFF";
								
								$value='';
								if($approval_type==0)
								{
									$value=$row[csf('id')];
								}
								else
								{
									$app_id=return_field_value("id","approval_history","mst_id ='".$row[csf('id')]."' and entry_form='3' order by id desc limit 0,1");
									$value=$row[csf('id')]."**".$app_id;
								}
								
								/*if($row[csf('booking_type')]==4) 
								{
									$booking_type="Sample";
									$type=3;
								}
								else
								{
									if($row[csf('is_short')]==1) $booking_type="Short"; else $booking_type="Main"; 
									$type=$row[csf('is_short')];
								}
								
								$dealing_merchant=$dealing_merchant_array[$job_dealing_merchant_array[$row[csf('job_no')]]];*/
								
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>" align="center"> 
                                	<td width="50" align="center" valign="middle">
                                        <input type="checkbox" id="tbl_<?php echo $i;?>" />
                                        <input id="booking_id_<?php echo $i;?>" name="booking_id[]" type="hidden" value="<?php echo $value; ?>" />
                                        <input id="booking_no_<?php echo $i;?>" name="booking_no]" type="hidden" value="<?php echo $row[csf('id')]; ?>" />
                                        <input id="approval_id_<?php echo $i;?>" name="approval_id[]" type="hidden" value="<?php echo $row[csf('approval_id')]; ?>" />
                                    </td>   
									<td width="40" align="center"><?php echo $i; ?></td>
									<td width="100">
                                    	<p>
                                        <!--<a href='##' style='color:#000' onclick="generate_worder_report('<?php //echo "preCostRpt2"; ?>',<?php //echo $row[csf('id')]; ?>,<?php //echo $row[csf('company_id')]; ?>,<?php //echo $row[csf('buyer_id')]; ?>,'<?php //echo $row[csf('style_ref')];?>','<?php //echo $row[csf('quot_date')]; ?>')"><?php //echo $row[csf('id')]; ?></a>-->
                                        <?php
                                        echo $row[csf('issue_number_prefix_num')];
										?>
                                        </p>
                                    </td>
                                    
									
                                    <td width="80"><p><?php  echo $row[csf('year')]; ?>&nbsp;</p></td>
									<td width="160" align="left"><p><?php echo $yarn_issue_purpose[$row[csf('issue_purpose')]]; ?>&nbsp;</p></td>
                                    
                                  
                                   
                                    <td width="100" align="center"><?php if($row[csf('issue_date')]!="0000-00-00") echo change_date_format($row[csf('issue_date')]); ?>&nbsp;</td>
									<td align="center" width="100"><?php echo $issue_basis[$row[csf('issue_basis')]]; ?>&nbsp;</td>
                                    <td width="120" align="center"><?php echo $kniting_company_arr[$row[csf('knit_dye_company')]]; ?></td>
                                    <td><p><?php echo $row[csf('requisition_no')]; ?>&nbsp;</p></td>
								</tr>
								<?php
								$i++;
							}
                        ?>
                    </tbody>
                </table>
            </div>
            <table cellspacing="0" cellpadding="0" border="0" rules="all" width="920" class="rpt_table">
				<tfoot>
                    <td width="50" align="center" ><input type="checkbox" id="all_check" onclick="check_all('all_check')" /></td>
                    <td colspan="2" align="left"><input type="button" value="<?php if($approval_type==1) echo "Un-Approve"; else echo "Approve"; ?>" class="formbutton" style="width:100px" onclick="submit_approved(<?php echo $i; ?>,<?php echo $approval_type; ?>)"/></td>
				</tfoot>
			</table>
        </fieldset>
    </form>         
<?php
	exit();	
}


if ($action=="approve")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	//$user_id=23;
	//echo $booking_nos;die;
	$con = connect();
	if($db_type==0)
	{
		mysql_query("BEGIN");
	}
	
	$msg=''; $flag=''; $response='';
	
	$user_sequence_no=return_field_value("sequence_no","electronic_approval_setup","page_id=$menu_id and user_id=$user_id and is_deleted=0");
	$min_sequence_no=return_field_value("min(sequence_no)","electronic_approval_setup","page_id=$menu_id and is_deleted=0");
	
	if($approval_type==0)
	{
		$response=$booking_ids;
		$field_array="id, entry_form, mst_id, approved_no, sequence_no, current_approval_status, approved_by, approved_date"; 
		$id=return_next_id( "id","approval_history", 1 ) ;
		
		$max_approved_no_arr = return_library_array("select mst_id, max(approved_no) as approved_no from approval_history where mst_id in($booking_ids) and entry_form=3 group by mst_id","mst_id","approved_no");
		
		$approved_status_arr = return_library_array("select id, is_approved from inv_issue_master where id in($booking_ids)","id","is_approved");

		$rID=sql_multirow_update("inv_issue_master","is_approved",1,"id",$booking_ids,0);
		if($rID) $flag=1; else $flag=0;
		
		if($approval_ids!="")
		{
			$rIDapp=sql_multirow_update("approval_history","current_approval_status",0,"id",$approval_ids,0);
			if($flag==1) 
			{
				if($rIDapp) $flag=1; else $flag=0; 
			} 
		}
		
		/*if($user_sequence_no==$min_sequence_no)
		{
			$rID=sql_multirow_update("wo_booking_mst","is_approved",1,"id",$booking_ids,0);
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$rID=sql_multirow_update("approval_history","current_approval_status",0,"id",$approval_ids,0);
			if($rID) $flag=1; else $flag=0;
		}*/
		
		$approved_no_array=array();
		$booking_ids_all=explode(",",$booking_ids);
		$booking_nos_all=explode(",",$booking_nos);
		$book_nos='';
		
		for($i=0;$i<count($booking_nos_all);$i++)
		{
			$val=$booking_nos_all[$i];
			$booking_id=$booking_ids_all[$i];

			$approved_no=$max_approved_no_arr[$booking_id];
			$approved_status=$approved_status_arr[$booking_id];
			
			if($approved_status==0)
			{
				$approved_no=$approved_no+1;
				$approved_no_array[$val]=$approved_no;
				if($book_nos=="") $book_nos=$val; else $book_nos.=",".$val;
			}
			
			if($data_array!="") $data_array.=",";
			
			$data_array.="(".$id.",11,".$booking_id.",".$approved_no.",'".$user_sequence_no."',1,".$user_id.",'".$pc_date_time."')"; 
				
			$id=$id+1;
			
		}
		//echo $book_nos;die;
		//echo "insert into approval_history (".$field_array.") Values ".$data_array."**".$book_nos."**".$booking_nos;die;
		$rID2=sql_insert("approval_history",$field_array,$data_array,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		
		if(count($approved_no_array)>0)
		{
			$approved_string="";
			
			foreach($approved_no_array as $key=>$value)
			{
				$approved_string.=" WHEN $key THEN $value";
			}
			
			$approved_string_mst="CASE id ".$approved_string." END";
			$approved_string_dtls="CASE mst_id ".$approved_string." END";
			//$approved_string_dtls_ppropor="CASE mst_id ".$approved_string." END";
			
			$sql_insert="insert into  inv_issue_master_history(id,approve_no,issue_id,issue_number_prefix,issue_number_prefix_num,issue_number,issue_basis,issue_purpose,entry_form,item_category,company_id,location_id,supplier_id,store_id,buyer_id,buyer_job_no,style_ref,booking_id,booking_no,req_no,batch_no,issue_date,sample_type,knit_dye_source,knit_dye_company,challan_no,loan_party,lap_dip_no,gate_pass_no,item_color,color_range,remarks,received_id,received_mrr_no,other_party,order_id,is_approved,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted) 
				select	
				'', $approved_string_mst, id,issue_number_prefix,issue_number_prefix_num,issue_number,issue_basis,issue_purpose,entry_form,item_category,company_id,location_id,supplier_id,store_id,buyer_id,buyer_job_no,style_ref,booking_id,booking_no,req_no,batch_no,issue_date,sample_type,knit_dye_source,knit_dye_company,challan_no,loan_party,lap_dip_no,gate_pass_no,item_color,color_range,remarks,received_id,received_mrr_no,other_party,order_id,is_approved,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted from  inv_issue_master where id in ($book_nos)";
					
			$rID3=execute_query($sql_insert,0);
			if($flag==1) 
			{
				if($rID3) $flag=1; else $flag=0; 
			} 
			
			$sql_insert_dtls="insert into  inv_transaction_history (id, approve_no, transaction_id, mst_id, requisition_no, receive_basis,pi_wo_batch_no,company_id,supplier_id,prod_id,product_code,item_category,
transaction_type,transaction_date,store_id,order_id,brand_id,order_uom,order_qnty,order_rate,order_ile,order_ile_cost,
order_amount,cons_uom,cons_quantity,return_qnty,cons_reject_qnty,cons_rate,cons_ile,cons_ile_cost,cons_amount,balance_qnty,
balance_amount,floor_id,machine_id,machine_category,no_of_bags,cone_per_bag,weight_per_bag,weight_per_cone,room,rack,self,bin_box,
expire_date,dyeing_sub_process,issue_challan_no,remarks,batch_lot,location_id,department_id,section_id,job_no,dyeing_color_id,inserted_by,
insert_date,updated_by,update_date,status_active,is_deleted) 
				select	
				'', $approved_string_dtls, id, mst_id, requisition_no, receive_basis,pi_wo_batch_no,company_id,supplier_id,prod_id,product_code,item_category,transaction_type,transaction_date,store_id,
order_id,brand_id,order_uom,order_qnty,order_rate,order_ile,order_ile_cost,order_amount,cons_uom,cons_quantity,return_qnty,cons_reject_qnty,cons_rate,
cons_ile,cons_ile_cost,cons_amount,balance_qnty,balance_amount,floor_id,machine_id,machine_category,no_of_bags,cone_per_bag,weight_per_bag,weight_per_cone,
room,rack,self,bin_box,expire_date,dyeing_sub_process,issue_challan_no,remarks,batch_lot,location_id,department_id,section_id,job_no,dyeing_color_id,
inserted_by,insert_date,updated_by,update_date,status_active,is_deleted   from inv_transaction where mst_id in ($book_nos) and transaction_type=2  and status_active=1";
					
			$rID4=execute_query($sql_insert_dtls,1);
			if($flag==1) 
			{
				if($rID4) $flag=1; else $flag=0; 
			}
			
			
			$trans_id=return_field_value("group_concat(distinct id) as id"," inv_transaction","mst_id in($book_nos) and transaction_type=2 and status_active=1","id");

			
			$sql_insert_dtls_propor="insert into  order_wise_pro_detail_history (id,approve_no, proportionate_id,trans_id,trans_type,entry_form,dtls_id,po_breakdown_id,prod_id,color_id,quantity,issue_purpose,returnable_qnty,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted) 
				select	
				'', 1, id,trans_id,trans_type,entry_form,dtls_id,po_breakdown_id,prod_id,color_id,quantity,issue_purpose,returnable_qnty,inserted_by,insert_date,updated_by,update_date,status_active,is_deleted  from order_wise_pro_details where trans_id in ($trans_id)";
					
			$rID5=execute_query($sql_insert_dtls,1);
			if($flag==1) 
			{
				if($rID5) $flag=1; else $flag=0; 
			}  
		}
			//echo $sql_insert_dtls_propor;die;
		//echo $flag;die;
		if($flag==1) $msg='19'; else $msg='21';
	}
	else
	{
		$booking_ids_all=explode(",",$booking_ids);
		
		$booking_ids=''; $app_ids='';
		
		foreach($booking_ids_all as $value)
		{
			$data = explode('**',$value);
			$booking_id=$data[0];
			$app_id=$data[1];
			
			if($booking_ids=='') $booking_ids=$booking_id; else $booking_ids.=",".$booking_id;
			if($app_ids=='') $app_ids=$app_id; else $app_ids.=",".$app_id;
		}
		$rID=sql_multirow_update("inv_issue_master","is_approved",0,"id",$booking_ids,0);
		if($rID) $flag=1; else $flag=0;
		
		$rID2=sql_multirow_update("approval_history","current_approval_status",0,"id",$approval_ids,0);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
			
		$data=$user_id."*'".$pc_date_time."'";
		$rID3=sql_multirow_update("approval_history","un_approved_by*un_approved_date",$data,"id",$app_ids,1);
		if($flag==1) 
		{
			if($rID3) $flag=1; else $flag=0; 
		} 
		
		$response=$booking_ids;
		
		if($flag==1) $msg='20'; else $msg='22';
	}
	
	if($db_type==0)
	{ 
		if($flag==1)
		{
			mysql_query("COMMIT");  
			echo $msg."**".$response;
		}
		else
		{
			mysql_query("ROLLBACK"); 
			echo $msg."**".$response;
		}
	}
	
	if($db_type==2 || $db_type==1 )
	{
		if($flag==1)
		{
			echo $msg."**".$response;
		}
		else
		{
			echo $msg."**".$response;
		}
	}
	disconnect($con);
	die;
	
}

if($action=="img")
{
	echo load_html_head_contents("Image View", "../../", 1, 1,'','','');
	extract($_REQUEST);
	
?>
	<fieldset style="width:600px; margin-left:5px">
		<div style="width:100%; word-wrap:break-word" id="scroll_body">
             <table border="0" rules="all" width="100%" cellpadding="2" cellspacing="2">
             	<tr>
					<?php
					$i=0;
                    $sql="select image_location from common_photo_library where master_tble_id='$id' and form_name='quotation_entry' and file_type=1";
                    $result=sql_select($sql);
                    foreach($result as $row)
                    {
						$i++;
                    ?>
                    	<td align="center"><img width="300px" height="180px" src="../../<?php echo $row[csf('image_location')];?>" /></td>
                    <?php
						if($i%2==0) echo "</tr><tr>";
                    }
                    ?>
                </tr>
            </table>
        </div>	
	</fieldset>     
<?php
exit();
}

if($action=="file")
{
	echo load_html_head_contents("File View", "../../", 1, 1,'','','');
	extract($_REQUEST);
	
?>
	<fieldset style="width:600px; margin-left:5px">
		<div style="width:100%; word-wrap:break-word" id="scroll_body">
             <table border="0" rules="all" width="100%" cellpadding="2" cellspacing="2">
             	<tr>
					<?php
					$i=0;
                    $sql="select image_location from common_photo_library where master_tble_id='$id' and form_name='quotation_entry' and file_type=2";
                    $result=sql_select($sql);
                    foreach($result as $row)
                    {
						$i++;
                    ?>
                    	<td width="100" align="center"><a href="../../<?php echo $row[csf('image_location')]; ?>"><img width="89" height="97" src="../../file_upload/blank_file.png"><br>File-<?php echo $i; ?></a></td>
                    <?php
						if($i%6==0) echo "</tr><tr>";
                    }
                    ?>
                </tr>
            </table>
        </div>	
	</fieldset>     
<?php
exit();
}

?>