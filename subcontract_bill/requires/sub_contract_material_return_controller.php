<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$trans_Type="3";

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 140, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", $selected, "" );
	exit();	 
}

if($action=="load_drop_down_company_supplier")
{
	echo create_drop_down( "cbo_company_supplier", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $selected, "","","","","","",5 );
	
	exit();
}

if ($action=="return_id_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	$ex_data=explode('_',$data);
	?>
	<script>
		function js_set_value(id)
		{ 
			document.getElementById('return_id').value=id;
			parent.emailwindow.hide();
		}
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="searchjobfrm_1"  id="searchjobfrm_1" autocomplete="off">
                <table width="750" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="150">Company Name</th>
                        <th width="110">Return ID</th>
                        <th width="200">Date Range</th>
                        <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:100px;" /></th>             
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="return_id">  
								<?php   
									echo create_drop_down( "cbo_company_name", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $ex_data[0],"",0 );
                                ?>
                            </td>
                            <td>
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:95px" />
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px">
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_search_common').value, 'return_id_search_list_view', 'search_div', 'sub_contract_material_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center" height="40" valign="middle">
								<?php echo load_month_buttons(1);  ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center" valign="top" id=""><div id="search_div"></div></td>
                        </tr>
                    </tbody>
                </table>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if ($action=="return_id_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company_name=" and company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!="" &&  $data[2]!="") $return_date = "and subcon_date between '".change_date_format($data[1], "mm-dd-yyyy", "/",1)."' and '".change_date_format($data[2], "mm-dd-yyyy", "/",1)."'"; else $return_date="";
	if ($data[3]!="") $return_id_cond=" and prefix_no_num='$data[3]'"; else $return_id_cond="";
	$company_id=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$location=return_library_array( "select id,location_name from lib_location",'id','location_name');
	$return_to=return_library_array( "select id,buyer_name from lib_buyer",'id','buyer_name');
	$arr=array (2=>$company_id,3=>$location,4=>$return_to);
	if($db_type==0)
	{
		$sql= "select id, sys_no, prefix_no_num, YEAR(insert_date) as year, company_id,location_id,party_id,subcon_date,chalan_no,remarks,status_active from sub_material_mst where trans_type=3 and status_active=1 $company_name $return_date $return_id_cond order by id DESC";
	}
	else if($db_type==2)
	{
		$sql= "select id, sys_no, prefix_no_num, TO_CHAR(insert_date,'YYYY') as year, company_id,location_id,party_id,subcon_date,chalan_no,remarks,status_active from sub_material_mst where trans_type=3 and status_active=1 $company_name $return_date $return_id_cond order by id DESC";
	}
		 
	echo  create_list_view("list_view", "Return ID,Year,Company Name,Location,Return To,Return Date,Return Challan,Remarks", "70,80,100,100,100,70,80,100","750","250",0, $sql , "js_set_value", "id", "", 1, "0,0,company_id,location_id,party_id,0,0,0", $arr , "prefix_no_num,year,company_id,location_id,party_id,subcon_date,chalan_no,remarks", "sub_contract_material_return_controller","",'0,0,0,0,0,3,0,0') ;
	exit();
}

if ($action=="load_php_data_to_form_mst")
{
	$nameArray=sql_select( "select id, sys_no, company_id, location_id, party_id, subcon_date, chalan_no, forwarder, tran_company, remarks from sub_material_mst where id='$data'" ); 
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_return_no').value 			= '".$row[csf("sys_no")]."';\n";
		echo "document.getElementById('cbo_company_name').value 		= '".$row[csf("company_id")]."';\n";
		echo "load_drop_down( 'requires/sub_contract_material_return_controller', document.getElementById('cbo_company_name').value, 'load_drop_down_location', 'location_td' );\n";
		echo "document.getElementById('cbo_location_name').value		= '".$row[csf("location_id")]."';\n"; 
		echo "load_drop_down( 'requires/sub_contract_material_return_controller', document.getElementById('cbo_company_name').value, 'load_drop_down_company_supplier', 'return_to_td' );\n"; 
		echo "document.getElementById('cbo_company_supplier').value		= '".$row[csf("party_id")]."';\n"; 
		echo "document.getElementById('txt_return_date').value 			= '".change_date_format($row[csf("subcon_date")])."';\n";   
		echo "document.getElementById('txt_return_challan').value		= '".$row[csf("chalan_no")]."';\n";
		echo "document.getElementById('cbo_forwarder').value 			= '".$row[csf("forwarder")]."';\n";   
		echo "document.getElementById('txt_transport_company').value		= '".$row[csf("tran_company")]."';\n"; 
		echo "document.getElementById('txt_remarks').value				= '".$row[csf("remarks")]."';\n"; 
	    echo "document.getElementById('update_id').value            	= '".$row[csf("id")]."';\n";
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_material_return',1,1);\n";
	}
	exit();
}

if ($action=="job_popup")
{
	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode,'','');
	?>
	<script>
		function js_set_value(id)
		{
			var spl_data=id.split('_');
			//alert (spl_data[1])
			document.getElementById('order_id').value=spl_data[0];
			document.getElementById('order_no').value=spl_data[1];
			parent.emailwindow.hide();
		}
		
		function set_caption(id)
		{
			if(id==1)  document.getElementById('search_by_td_up').innerHTML='Enter Order No';
			if(id==2)  document.getElementById('search_by_td_up').innerHTML='Enter Job No';
		}			
	</script>
	</head>
	<body>
        <div align="center" style="width:100%;" >
            <form name="searchjobfrm_1"  id="searchjobfrm_1" autocomplete="off">
                <table width="740" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="140">Company Name</th>
                        <th width="130">Party Name</th>
                        <th width="150">Date Range</th>
                        <th width="70">Search By</th>
                        <th width="90" id="search_by_td_up">Select Job/Order</th>
                        <th><input type="reset" name="reset" id="reset" class="formbutton" value="Reset" style="width:70px;" /></th>             
                    </thead>
                    <tbody>
                        <tr>
                            <td> <input type="hidden" id="order_id">  <input type="hidden" id="order_no">
								<?php   
									$data=explode("_",$data);
									echo create_drop_down( "cbo_company_name", 140, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", $data[0],"",1 );//load_drop_down( 'sub_contract_material_return_controller', this.value, 'load_drop_down_company_supplier','return_to_td' );
                                ?>
                            </td>
                            <td>
								<?php 
									echo create_drop_down( "cbo_company_supplier", 130, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id  and b.tag_company='$data[0]' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "-- Select Party --", $data[1], "",1 ); 
                                ?>
                            </td>
                            <td align="center">
                                <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:60px">
                                <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:60px">
                            </td>
                            <td >
							<?php
								$sarch_by_arr=array(1=>"Order No",2=>"Job No"); 
								echo create_drop_down( "cbo_search_by", 70,$sarch_by_arr,"", 1, "-- Select Search --", 0,"set_caption(this.value)");
                            ?>
                            </td>
                            <td align="center" id="search_by_td">
                                <input type="text" name="txt_search_common" id="txt_search_common" class="text_boxes" style="width:85px" autocomplete=off />
                            </td> 
                            <td align="center">
                                <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_name').value+'_'+document.getElementById('cbo_company_supplier').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value, 'job_search_list_view', 'search_div', 'sub_contract_material_return_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:70px;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center" height="40" valign="middle">
								<?php echo load_month_buttons(1);  ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div id="search_div"></div>    
            </form>
        </div>
	</body>           
	<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
	</html>
	<?php
	exit();
}

if ($action=="job_search_list_view")
{
	$data=explode('_',$data);
	$sarch_by=str_replace("'","",$data[4]);
	$txt_search_common=trim(str_replace("'","",$data[5]));
	if($sarch_by==1) $search=" and b.order_no='$txt_search_common'"; else if($sarch_by==2) $search="and a.job_no_prefix_num='$txt_search_common'"; else $search="";
	if ($data[0]!=0) $company_con=" and a.company_id='$data[0]'"; else  $company_con="";
	if ($data[1]!=0) $buyer_con=" and a.party_id='$data[1]'"; else $buyer_con="";
	if ($data[2]!="" &&  $data[3]!="") $order_rcv_date = "and b.order_rcv_date between '".change_date_format($data[2], "mm-dd-yyyy", "/",1)."' and '".change_date_format($data[3], "mm-dd-yyyy", "/",1)."'"; else $order_rcv_date ="";
	
	$party_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	//$arr=array ();
	if($db_type==0)
	{
		$sql="select a.subcon_job, a.job_no_prefix_num, YEAR(a.insert_date) as year, b.id, b.order_no, b.cust_style_ref, b.order_rcv_date, b.delivery_date, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $company_con $buyer_con $order_rcv_date $search order by a.id DESC";
	}
	else if($db_type==2)
	{
		$sql="select a.subcon_job, a.job_no_prefix_num, TO_CHAR(a.insert_date,'YYYY') as year, b.id, b.order_no, b.cust_style_ref, b.order_rcv_date, b.delivery_date, b.status_active from subcon_ord_mst a, subcon_ord_dtls b where a.subcon_job=b.job_no_mst and a.status_active=1 and b.status_active=1 $company_con $buyer_con $order_rcv_date $search order by a.id DESC";
	}
		 
	echo  create_list_view("list_view", "Job No,Year,Order No,Style,Order Date,Delivery Date", "70,80,100,100,100,100","650","250",0, $sql , "js_set_value", "id,order_no", "", 1, "0,0,0,0,0,0", $arr , "job_no_prefix_num,year,order_no,cust_style_ref,order_rcv_date,delivery_date","", "",'0,0,0,0,3,3') ;
	exit();
}

if ($action=="material_description_return_popup")
{
	echo load_html_head_contents("Material Description Form", "../../", 1, 1,'',1,'');
	extract($_REQUEST);
	$ex_data=explode('_',$data);
	?>
    <script>
		  function js_set_value(id,val)
		  {
			  //alert (val)
		  	  $("#description_id").val(id);
			  $("#material_description").val(val);
			  parent.emailwindow.hide();
		  }
	</script>
    </head>
    <body>
            <input type="hidden" name="description_id" id="description_id">
        	<input type="hidden" name="material_description" id="material_description">

    <div style="width:650px;">
     	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table">
            <thead>
                <tr>
                    <th width="30" >SL</th>
                    <th width="220" >Material Description</th>
                    <th width="60" >UOM</th>
                    <th width="60" >Dia</th>
                    <th width="60" >Rec. Qty</th>
                    <th width="60" >Roll/Bag</th>
                    <th width="60" >Cone</th>
                    <th width="60" >Iss. Qty</th>
                    <th width="80" >Balance</th>
                </tr>
            </thead>
     	</table>
     </div>
     <div style="width:667px; max-height:300px;overflow-y:scroll;" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="100%" class="rpt_table" id="tbl_po_list">
			<?php
			if($ex_data[3]!=0){ $category_id=" and a.item_category_id=$ex_data[3]";}else{echo "Please Select item category First."; die; }
			if($db_type==0)
			{
				$group_cond=" group by a.material_description, a.subcon_uom, a.grey_dia";
				$id="a.id as id";
			}
			else if($db_type==2)
			{
				$group_cond=" group by a.material_description, a.subcon_uom, a.grey_dia";
				$id="log_concat(a.id) as id";
			}
			$issue_balance_array=array();
			$sql_issue="select $id, a.material_description, a.subcon_uom, sum(a.subcon_roll) as subcon_roll, sum(a.quantity) as quantity, sum(a.rec_cone) as rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.material_description<>' ' and b.trans_type in (2,3) and b.status_active=1 and b.is_deleted=0 and a.order_id='$ex_data[2]' $category_id $group_cond";
			$sql_issue_result=sql_select($sql_issue);
			foreach( $sql_issue_result as $row )
			{
				$issue_balance_array[$row[csf("material_description")]][$row[csf("subcon_uom")]][$row[csf("grey_dia")]]=$row[csf("quantity")];
			}
			
			$sql="select $id, a.material_description, a.subcon_uom, sum(a.subcon_roll) as subcon_roll, sum(a.quantity) as quantity, sum(a.rec_cone) as rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.material_description<>' ' and b.trans_type=1 and a.status_active=2 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.order_id='$ex_data[2]' $category_id $group_cond";
			$i=1;
			$nameArray=sql_select($sql);
            foreach( $nameArray as $row )
            {
                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="js_set_value('<?php echo $row[csf("id")]; ?>','<?php echo $row[csf("material_description")]; ?>');" > 
						<td width="30" align="center"><?php echo $i; ?></td>
						<td width="220" align="center"><?php echo $row[csf("material_description")]; ?></td>		
						<td width="60" align="center"><?php echo $unit_of_measurement[$row[csf("subcon_uom")]]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf("grey_dia")]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf("quantity")]; ?></td>	
                        <td width="60" align="center"><?php echo $row[csf("subcon_roll")]; ?></td>	
                        <td width="60" align="center"><?php echo $row[csf("rec_cone")]; ?></td>
                        <td width="60" align="center"><?php echo $issue_balance_array[$row[csf("material_description")]][$row[csf("subcon_uom")]][$row[csf("grey_dia")]]; ?></td>	
                        <td width="80" align="center"><?php echo $row[csf("quantity")]-$issue_balance_array[$row[csf("material_description")]][$row[csf("subcon_uom")]][$row[csf("grey_dia")]]; ?></td>	
					</tr>
				<?php 
				$i++;
            }
   		?>
			</table>
        </div>
    </body>           
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script>
    </html>
    <?php
  exit();
}

if ($action=="material_return_list_view")
{
	$order_arr=return_library_array( "select id, order_no from subcon_ord_dtls",'id','order_no');
	$sql = "select id, item_category_id, material_description, quantity, subcon_uom, subcon_roll, grey_dia, status_active, order_id from sub_material_dtls where status_active=1 and is_deleted=0 and mst_id='$data'"; 
	$arr=array(0=>$order_arr,1=>$item_category,4=>$unit_of_measurement,7=>$row_status);
	echo  create_list_view("list_view", "Order No,Item Category,Material Description,Return Qty,UOM,Roll,Dia,Status", "100,100,150,80,50,60,60,80","730","250",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form_dtls'", 1, "order_id,item_category_id,0,0,subcon_uom,0,0,status_active",$arr,"order_id,item_category_id,material_description,quantity,subcon_uom,subcon_roll,grey_dia,status_active", "requires/sub_contract_material_return_controller","","0,0,0,0,0,0,0,0");
	exit();
}

if ($action=="load_php_data_to_form_dtls")
{
	$nameArray=sql_select( "select a.id, a.mst_id, a.item_category_id, a.material_description, a.quantity, a.subcon_uom, a.subcon_roll, a.rec_cone, a.grey_dia, a.status_active, b.id as order_tbl_id, b.order_no from sub_material_dtls a,subcon_ord_dtls b where a.order_id=b.id and a.id='$data'" );
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_orderno').value		 		= '".$row[csf("order_no")]."';\n";
		echo "document.getElementById('order_no_id').value		 		= '".$row[csf("order_tbl_id")]."';\n"; 
		echo "document.getElementById('cbo_itemcategory').value			= '".$row[csf("item_category_id")]."';\n"; 
		echo "document.getElementById('txt_description').value			= '".$row[csf("material_description")]."';\n";  
		echo "document.getElementById('txt_quantity').value				= '".$row[csf("quantity")]."';\n";  
		echo "document.getElementById('cbo_uom').value		 			= '".$row[csf("subcon_uom")]."';\n";
		echo "document.getElementById('txt_roll').value		 			= '".$row[csf("subcon_roll")]."';\n";
		echo "document.getElementById('txt_dia').value		 			= '".$row[csf("grey_dia")]."';\n";
		echo "document.getElementById('txt_cone').value		 			= '".$row[csf("rec_cone")]."';\n";
		echo "document.getElementById('update_id').value            	= '".$row[csf("mst_id")]."';\n";
		echo "document.getElementById('updateid_1').value            	= '".$row[csf("id")]."';\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_material_return',1,1);\n";
		echo "change_uom('".$row[csf("item_category_id")]."');\n";
	}
	exit();	
}

/*if ($action=="load_php_data_for_dtls")
{
	$ex_data=explode('_',$data);
	$sql_rec="Select a.quantity, a.subcon_roll, a.rec_cone, a.grey_dia from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.order_id=$ex_data[0] and a.id=$ex_data[1] and a.status_active=1 and b.trans_type=1 and a.is_deleted=0";
	$sql_result_rec = sql_select($sql_rec);
	$sql_iss="Select sum(a.quantity) as quantity, sum (a.subcon_roll) as subcon_roll, sum (a.rec_cone) as rec_cone from sub_material_dtls a, sub_material_mst b where b.id=a.mst_id and a.order_id=$ex_data[0] and b.trans_type=3 and a.status_active=1 and a.is_deleted=0";
	$sql_result_iss = sql_select($sql_iss);
	$tot_roll=0;
	$tot_dia=$sql_result_rec[0][csf('grey_dia')]*1;//-($sql_result_iss[0][csf('grey_dia')]*1)
	$tot_qty=$sql_result_rec[0][csf('quantity')]*1-($sql_result_iss[0][csf('quantity')]*1);
	$tot_roll=$sql_result_rec[0][csf('subcon_roll')]*1-($sql_result_iss[0][csf('subcon_roll')]*1);
	$tot_cone=$sql_result_rec[0][csf('rec_cone')]*1-($sql_result_iss[0][csf('rec_cone')]*1);
	echo "$('#txt_dia').val('".$tot_dia."');\n";
	echo "$('#txt_quantity').attr('placeholder','".$tot_qty."');\n";
	echo "$('#txt_roll').attr('placeholder','".$tot_roll."');\n";
	echo "$('#txt_cone').attr('placeholder','".$tot_cone."');\n";
	exit();
}*/

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$trans_Type="3";
	if ($operation==0)   // Insert Here==============================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		if($db_type==0)
		{
			$year_cond=" and YEAR(insert_date)";	
		}
		else if($db_type==2)
		{
			$year_cond=" and TO_CHAR(insert_date,'YYYY')";	
		}
		$new_return_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'RTN', date("Y",time()), 5, "select id, prefix_no, prefix_no_num from sub_material_mst where company_id=$cbo_company_name and trans_type=$trans_Type $year_cond=".date('Y',time())." order by id desc ", "prefix_no", "prefix_no_num" ));
		if(str_replace("'",'',$update_id)=="")
		{	
			$id=return_next_id("id", "sub_material_mst",1);// die;
			$field_array="id, prefix_no, prefix_no_num, sys_no, trans_type, company_id, location_id, party_id, subcon_date, chalan_no, forwarder, tran_company, remarks, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_return_no[1]."','".$new_return_no[2]."','".$new_return_no[0]."','".$trans_Type."',".$cbo_company_name.",".$cbo_location_name.",".$cbo_company_supplier.",".$txt_return_date.",".$txt_return_challan.",".$cbo_forwarder.",".$txt_transport_company.",".$txt_remarks.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			//echo "INSERT INTO sub_material_mst (".$field_array.") VALUES ".$data_array;  die;  
			//$rID=sql_insert("sub_material_mst",$field_array,$data_array,1);
			//$return_no=$new_return_no[0];
		}
		else
		{
			$id=str_replace("'",'',$update_id);
			$field_array="location_id*party_id*subcon_date*chalan_no*forwarder*tran_company*remarks*updated_by*update_date";
			$data_array="".$cbo_location_name."*".$cbo_company_supplier."*".$txt_return_date."*".$txt_return_challan."*".$cbo_forwarder."*".$txt_transport_company."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		}
		$id1=return_next_id("id","sub_material_dtls", 1); 
		$field_array2="id,mst_id,order_id,item_category_id,material_description,quantity,subcon_uom,subcon_roll,rec_cone,grey_dia,inserted_by,insert_date";
		$data_array2="(".$id1.",".$id.",".$order_no_id.",".$cbo_itemcategory.",".$txt_description.",".$txt_quantity.",".$cbo_uom.",".$txt_roll.",".$txt_cone.",".$txt_dia.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
		
		if(str_replace("'",'',$update_id)=="")
		{
			//echo "INSERT INTO sub_material_mst(".$field_array.") VALUES ".$data_array;
			$rID=sql_insert("sub_material_mst",$field_array,$data_array,0);
			$return_no=$new_return_no[0];
		}
		else
		{
			$rID=sql_update("sub_material_mst",$field_array,$data_array,"id",$update_id,0); 
			$return_no=str_replace("'",'',$txt_return_no);
		}
		//echo "INSERT INTO sub_material_dtls(".$field_array2.") VALUES ".$data_array2; die;
		$rID2=sql_insert("sub_material_dtls",$field_array2,$data_array2,1);
		//echo $rID."===".$rID2; die;
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$id1)."**".str_replace("'",'',$return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$id1)."**".str_replace("'",'',$return_no);
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$id1)."**".str_replace("'",'',$return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'",'',$id)."**".str_replace("'",'',$id1)."**".str_replace("'",'',$return_no);
			}
		}	
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here============================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		//echo $txt_return_no;die;
		$field_array="location_id*party_id*subcon_date*chalan_no*forwarder*tran_company*remarks*updated_by*update_date";
		$data_array="".$cbo_location_name."*".$cbo_company_supplier."*".$txt_return_date."*".$txt_return_challan."*".$cbo_forwarder."*".$txt_transport_company."*".$txt_remarks."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
		
		$field_array2="order_id*item_category_id*material_description*quantity*subcon_uom*subcon_roll*rec_cone*grey_dia*updated_by*update_date";
		$data_array2="".$order_no_id."*".$cbo_itemcategory."*".$txt_description."*".$txt_quantity."*".$cbo_uom."*".$txt_roll."*".$txt_cone."*".$txt_dia."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"; 
			//echo $data_array; die;	
		$rID=sql_update("sub_material_mst",$field_array,$data_array,"id",$update_id,0); 
			
		$rID2=sql_update("sub_material_dtls",$field_array2,$data_array2,"id",$updateid_1,1); //  die;
		
		if($db_type==0)
		{
			if($rID && $rID2)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'",'',$update_id)."**".str_replace("'",'',$updateid_1)."**".str_replace("'",'',$txt_return_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**";
			}
		}
		if($db_type==2)
		{
			if($rID && $rID2)
			{
				oci_commit($con);
				echo "1**".str_replace("'",'',$update_id)."**".str_replace("'",'',$updateid_1)."**".str_replace("'",'',$txt_return_no);
			}
			else
			{
				oci_rollback($con);
				echo "10**";
			}
		}	
		disconnect($con);
 		die;
	}
	else if ($operation==2)   // Delete Here ============================================================
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="updated_by*update_date*status_active*is_deleted";
		$data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*'0'*'1'";
		$rID=sql_delete("sub_material_dtls",$field_array,$data_array,"id","".$updateid_1."",1);
			
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'",'',$update_id)."**".str_replace("'",'',$updateid_1);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$update_id)."**".str_replace("'",'',$updateid_1);
			}
		}
		if($db_type==2)
		{
			if($rID==1)
			{
				echo "2**".str_replace("'",'',$update_id)."**".str_replace("'",'',$updateid_1);
			}
			else
			{
				echo "10**";
			}
		}	
		disconnect($con);
		die;
	}
}

if($action=="inventory_return_print")
{
	extract($_REQUEST);
	$ex_data=explode('*',$data);
	$company=$ex_data[0];
	$update_id=$ex_data[1];
	$sys_id=$ex_data[2];
	//print_r ($data);
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from   lib_buyer", "id", "buyer_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$color_arr=return_library_array( "select id, color_name from  lib_color", "id", "color_name"  );//die;
	$recChallan_arr=return_library_array( "select id, chalan_no from  sub_material_mst where trans_type=1", "id", "chalan_no"  );
	$recChallan_arr=array();
	if($db_type==0)
	{
		$sql_rec="select b.order_id, group_concat(distinct(a.chalan_no)) as chalan_no from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 group by b.order_id";
	}
	else if($db_type==2)
	{
		$sql_rec="select b.order_id, log_concat(distinct(cast(a.chalan_no as varchar2(500)))) as chalan_no from sub_material_mst a, sub_material_dtls b where a.id=b.mst_id and a.trans_type=1 group by b.order_id";
	}
	$result_sql_rec=sql_select($sql_rec);
	foreach($result_sql_rec as $row)
	{
		$recChallan_arr[$row[csf('order_id')]]['chalan_no']=$row[csf('chalan_no')];
	}
	//var_dump($recChallan_arr);
	$sql_job_po="select a.id, a.order_no, a.main_process_id, a.cust_buyer, a.cust_style_ref, b.party_id, b.subcon_job, a.process_id from  subcon_ord_dtls a, subcon_ord_mst b where a.job_no_mst=b.subcon_job and b.is_deleted=0 and b.status_active=1 and a.status_active=1 and a.is_deleted=0";
	$job_po_array=array();
	$result_job_po=sql_select($sql_job_po);
	foreach($result_job_po as $row)
	{
		$job_po_array[$row[csf('id')]]['order_no']=$row[csf('order_no')];
		$job_po_array[$row[csf('id')]]['style']=$row[csf('cust_style_ref')];
		$job_po_array[$row[csf('id')]]['buyer']=$row[csf('cust_buyer')];
	}
	
	$sql="select party_id, subcon_date, chalan_no, forwarder, tran_company, remarks from  sub_material_mst where id='$update_id' and status_active=1 and status_active=1";
	
	$dataArray=sql_select($sql);
?>
    <div style="width:930px;">
    <table width="930" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:22px"><strong><?php echo $company_library[$company]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:12px">  
				<?php
					$nameArray=sql_select( "select plot_no, level_no, road_no, block_no, country_id, province, city, zip_code, email, website, vat_number from lib_company where id=$company and status_active=1 and is_deleted=0"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];?> <br>
                        <b> Vat No : <?php echo $result[csf('vat_number')]; ?></b> <?php
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:18px"><strong><u>Material Return Challan</u></strong></td>
        </tr>
        <tr>
			<?php 
                $party_add=$dataArray[0][csf('party_id')];
                $nameArray=sql_select( "select address_1, web_site, buyer_email, country_id from  lib_buyer where id=$party_add"); 
                foreach ($nameArray as $result)
                { 
                    $address="";
                    if($result!="") $address=$result['address_1'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
                }
            ?> 
        	<td width="300" rowspan="2" valign="top" colspan="2" style="font-size:12px"><strong>Party : <?php echo $buyer_library[$party_add].'<br>'.$address;  ?></strong></td>
            <td width="125" style="font-size:12px"><strong>Challan No :</strong></td><td width="170px"><?php echo $dataArray[0][csf('chalan_no')]; ?></td>
            <td width="125" style="font-size:12px"><strong>Delivery Date :</strong></td><td width="170px"><?php echo change_date_format($dataArray[0][csf('subcon_date')]); ?></td>
        </tr>
        <tr>
            <td style="font-size:12px"><strong>Transport Com.:</strong></td><td><?php echo $dataArray[0][csf('tran_company')]; ?></td>
            <td style="font-size:12px"><strong>Forwarder:</strong></td><td><?php echo $supplier_library[$dataArray[0][csf('forwarder')]]; ?></td>
        </tr>
        <tr>
            <td style="font-size:12px" colspan="6"><strong>Remarks:</strong>&nbsp;&nbsp;&nbsp;<?php echo $dataArray[0][csf('remarks')]; ?></td>
        </tr>
    </table>
    <br>
    <div style="width:100%;">
    <table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
        <thead style="font-size:12px">
            <th width="30">SL</th>
            <th width="150">Description</th>
            <th width="100">Order</th>
            <th width="80">Rec. Challan</th>
            <th width="80">Cust. Style</th>
            <th width="80">Cust. Buyer</th>
            <th width="60">Roll/Bag</th>
            <th width="90">Return Qty</th>
        </thead>
        <?php
		
			$mst_id=$dataArray[0][csf('id')];
			$sql_dtls="select order_id, material_description, subcon_roll, quantity from sub_material_dtls where mst_id='$update_id'";
			
			$i=1;
			
			$dtls_value=sql_select($sql_dtls);
			foreach($dtls_value as $row)
			{
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				
				$recChallan_no='';
				$rec_challan=explode(',',$recChallan_arr[$row[csf('order_id')]]['chalan_no']);
				foreach($rec_challan as $val)
				{
					if ($recChallan_no=='') $recChallan_no=$val; else $recChallan_no.=', '.$val;
				}
				
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>" style="font-size:12px">
                <td width="30"><?php echo $i; ?></td>
                <td width="150"><p><?php echo $row[csf('material_description')]; ?></p></td>
                <td width="100"><p><?php echo $job_po_array[$row[csf('order_id')]]['order_no']; ?></p></td>
                <td width="80"><p><?php echo $recChallan_no; ?>&nbsp;</p></td>
                <td width="80"><p><?php echo $job_po_array[$row[csf('order_id')]]['style']; ?>&nbsp;</p></td>
                <td width="80"><p><?php echo $job_po_array[$row[csf('order_id')]]['buyer']; ?>&nbsp;</p></td>
                <td width="60" align="right"><p><?php echo $row[csf('subcon_roll')]; ?>&nbsp;</p></td>
                <td width="90" align="right"><?php echo $row[csf('quantity')]; ?>&nbsp;</td>
			</tr>
			<?php
			$tot_roll+=$row[csf('subcon_roll')];
			$tot_qty+=$row[csf('quantity')];
			$i++;
			}
		?>
            <tfoot style="font-size:12px">
                <th colspan="6" align="right"><strong>Total</strong></th>
                <th align="right"><?php echo $tot_roll; ?>&nbsp;</th>
                <th align="right"><?php echo $tot_qty; ?>&nbsp;</th>
            </tfoot>
        </table> <br>
		 <?php
            echo signature_table(51, $ex_data[0], "930px");
			if( $ex_data[4]==1)
			{
         ?>
    	<table width="900" cellspacing="0" >
        	<tr><td colspan="6">
            </td></tr>
        	<tr><td colspan="6" align="center">...........................................................................................................................................................................</td></tr>
            <tr><td colspan="6" align="center">,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,</td></tr>
            <tr>
                <td colspan="6" align="center" style="font-size:22px"><strong><?php echo $company_library[$company]; ?></strong></td>
            </tr>
            <tr class="form_caption">
                <td colspan="6" align="center" style="font-size:12px">
                    <?php
                        $nameArray=sql_select( "select plot_no, level_no, road_no, block_no, country_id, province, city, zip_code, email, website, vat_number from lib_company where id=$company and status_active=1 and is_deleted=0"); 
                        foreach ($nameArray as $result)
                        { 
                        ?>
                            Plot No: <?php echo $result['plot_no']; ?> 
                            Level No: <?php echo $result['level_no']?>
                            Road No: <?php echo $result['road_no']; ?> 
                            Block No: <?php echo $result['block_no'];?> 
                            City No: <?php echo $result['city'];?> 
                            Zip Code: <?php echo $result['zip_code']; ?> 
                            Province No: <?php echo $result['province'];?> 
                            Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
                            Email Address: <?php echo $result['email'];?> 
                            Website No: <?php echo $result['website']; ?> <br>
                            <b> Vat No : <?php echo $result[csf('vat_number')]; ?></b> <?php
                        }
                    ?> 
                </td>  
            </tr>
            <tr>
                <td colspan="6" align="center" style="font-size:18px"><strong>Gate Pass</strong></td>
            </tr>
            <tr>
                <?php 
                    $party_add=$dataArray[0][csf('party_id')];
                    $nameArray=sql_select( "select address_1, web_site, buyer_email, country_id from  lib_buyer where id=$party_add"); 
                    foreach ($nameArray as $result)
                    { 
                        $address="";
                        if($result!="") $address=$result['address_1'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
                    }
                ?> 
                <td width="300" rowspan="4" valign="top" colspan="2" style="font-size:12px"><strong>Party : <?php echo $buyer_library[$party_add].'<br>'.$address;  ?></strong></td>
                <td width="125" style="font-size:12px"><strong>Challan No :</strong></td><td width="170px"><?php echo $dataArray[0][csf('chalan_no')]; ?></td>
                <td width="125" style="font-size:12px"><strong>Delivery Date :</strong></td><td width="170px"><?php echo change_date_format($dataArray[0][csf('subcon_date')]); ?></td>
            </tr>
            <tr>
                <td style="font-size:12px"><strong>Transport Com.:</strong></td><td><?php echo $dataArray[0][csf('tran_company')]; ?></td>
                <td style="font-size:12px"><strong>Forwarder:</strong></td><td><?php echo $supplier_library[$dataArray[0][csf('forwarder')]]; ?></td>
            </tr>
            <tr><td colspan="5">&nbsp;</td></tr>
            <tr>
                <td colspan="6" align="right" style="font-size:14px">
                    <table cellspacing="0" width="350"  border="1" rules="all" class="rpt_table" >
                        <thead bgcolor="#dddddd" align="center">
                            <th width="150">Roll/Bag</th>
                            <th width="150">Weight</th>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td align="center"><?php echo $tot_roll; ?></td>
                               <td align="center"><?php echo $tot_qty; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
		 <?php
            echo signature_table(46, $ex_data[0], "900px");
         ?>
          </div> 
	</div>
<?php
	}
exit();
}
?>