-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_pri_quo_fabric_cost_dtls`
--

DROP TABLE IF EXISTS `wo_pri_quo_fabric_cost_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_pri_quo_fabric_cost_dtls` (
  `id` int(11) NOT NULL DEFAULT '0',
  `quotation_id` int(11) NOT NULL DEFAULT '0',
  `item_number_id` int(11) NOT NULL DEFAULT '0',
  `body_part_id` int(11) NOT NULL DEFAULT '0',
  `fab_nature_id` int(11) NOT NULL DEFAULT '0',
  `color_type_id` int(11) NOT NULL DEFAULT '0',
  `lib_yarn_count_deter_id` int(11) NOT NULL DEFAULT '0',
  `construction` varchar(150) NOT NULL,
  `composition` varchar(150) NOT NULL,
  `fabric_description` varchar(150) NOT NULL,
  `gsm_weight` int(11) NOT NULL DEFAULT '0',
  `avg_cons` double NOT NULL DEFAULT '0',
  `fabric_source` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `avg_finish_cons` double NOT NULL,
  `avg_process_loss` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime DEFAULT NULL,
  `status_active` int(1) NOT NULL DEFAULT '1',
  `is_deleted` int(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `costing_per` int(11) NOT NULL DEFAULT '0',
  `fab_cons_in_quotat_varia` int(11) NOT NULL DEFAULT '0',
  `process_loss_method` int(11) NOT NULL DEFAULT '0',
  `cons_breack_down` text NOT NULL,
  `msmnt_break_down` text NOT NULL,
  `yarn_breack_down` varchar(150) NOT NULL,
  `marker_break_down` text NOT NULL,
  `width_dia_type` int(11) NOT NULL DEFAULT '0',
  KEY `quotation_id` (`quotation_id`),
  KEY `item_number_id` (`item_number_id`),
  KEY `body_part_id` (`body_part_id`),
  KEY `fab_nature_id` (`fab_nature_id`),
  KEY `color_type_id` (`color_type_id`),
  KEY `fabric_source` (`fabric_source`),
  KEY `is_deleted` (`is_deleted`),
  KEY `status_active` (`status_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_pri_quo_fabric_cost_dtls`
--

LOCK TABLES `wo_pri_quo_fabric_cost_dtls` WRITE;
/*!40000 ALTER TABLE `wo_pri_quo_fabric_cost_dtls` DISABLE KEYS */;
INSERT INTO `wo_pri_quo_fabric_cost_dtls` VALUES (1,1,1,1,2,1,108,'Single Jersey','Cotton 100%','Single Jersey Cotton 100%',140,2.87,1,0,0,2.53,12,2,'2015-11-01 10:59:36',2,'2015-11-22 12:03:42',1,0,1,1,1,0,'S_71 INCH_2.53_12_2.87_12__M_71 INCH_2.53_12_2.87_12__L_71 INCH_2.53_12_2.87_12__XL_71 INCH_2.53_12_2.87_12__XXL_71 INCH_2.53_12_2.87_12','','1_1_100_1_100','',1),(2,1,1,4,2,1,359,'1X1 RIB','Cotton 100%','1X1 RIB Cotton 100%',220,0.11,1,0,0,0.1,10,2,'2015-11-01 10:59:36',2,'2015-11-22 12:03:42',1,0,1,1,1,0,'S_28 TUBE_0.096_10_0.11_12__M_28 TUBE_0.096_10_0.11_12__L_28 TUBE_0.096_10_0.11_12__XL_28 TUBE_0.096_10_0.11_12__XXL_28 TUBE_0.096_10_0.11_12','','1_1_100_1_100','',1);
/*!40000 ALTER TABLE `wo_pri_quo_fabric_cost_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:46
