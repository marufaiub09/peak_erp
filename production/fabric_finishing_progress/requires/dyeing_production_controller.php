<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$batch_arr = return_library_array("select id, batch_no from pro_batch_create_mst","id","batch_no");
$machine_name=return_library_array( "select machine_no,id from  lib_machine_name where is_deleted=0", "id", "machine_no"  );
if ($action=="load_drop_floor")
{
	$data=explode('_',$data);
	$loca=$data[0];
	$com=$data[1];
	//echo "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]'  order by floor_name";die;
	echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 and company_id='$data[0]'  and production_process=3  order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected,"load_drop_down( 'requires/dyeing_production_controller', document.getElementById('cbo_company_id').value+'**'+this.value, 'load_drop_machine', 'machine_td' );" );     	 
	exit();
}
if($action=="load_drop_sub_process")
{
	echo create_drop_down( "cbo_sub_process", 135, $conversion_cost_head_array,"", 0, "", $selected, "","",$data );
}
if ($action=="load_drop_machine")
{
$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
$data=explode('**',$data);
	$com=$data[0];
	$floor=$data[1];
	if($db_type==2)
	{
	echo create_drop_down( "cbo_machine_name", 135, "select id,machine_no || '-' || brand as machine_name from lib_machine_name where category_id=2 and 				
	company_id=$com and floor_id=$floor and status_active=1 and is_deleted=0 and is_locked=0 order by machine_name","id,machine_name", 1,"-- Select Machine --", $selected, "get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+this.value, 
	'populate_data_from_machine', 'requires/dyeing_production_controller' );","" );
	}
	else if($db_type==0)
	{
	echo create_drop_down( "cbo_machine_name", 135, "select id,concat(machine_no,'-',brand) as machine_name from lib_machine_name where category_id=2 and company_id=$com and floor_id=$floor and status_active=1 and is_deleted=0 and is_locked=0 order by machine_name","id,machine_name", 1, "-- Select Machine --", $selected, "get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+this.value, 'populate_data_from_machine', 'requires/dyeing_production_controller' );","" );
	}
	exit();
}
if ($action=="populate_data_from_machine")
{ 
	$floor_arr=return_library_array( "select id,floor_name from  lib_prod_floor",'id','floor_name');
	$ex_data=explode('**',$data);
	$sql_res="select id, floor_id, machine_group from lib_machine_name where id=$ex_data[2] and category_id=2 and company_id=$ex_data[0] and  floor_id=$ex_data[1] and status_active=1 and is_deleted=0";
	$nameArray=sql_select($sql_res);
	foreach ($nameArray as $row)
	{	
		echo "document.getElementById('txt_machine_no').value 			= '".$floor_arr[$row[csf("floor_id")]]."';\n";
		echo "document.getElementById('txt_mc_group').value 			= '".$row[csf("machine_group")]."';\n";
	}
	exit();
}
if ($action=="batch_number_popup")
{
	echo load_html_head_contents("Batch Number Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id)
		{ 
			$('#hidden_batch_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
<div align="center" style="width:800px;">
    <form name="searchbatchnofrm"  id="searchbatchnofrm">
        <fieldset style="width:790px;">
        <legend>Enter search words</legend>
             <table cellpadding="0" cellspacing="0" width="770" border="1" rules="all" class="rpt_table">
                <thead>
                    <tr>
                        <th colspan="4">
                          <?php
							  echo create_drop_down( "cbo_string_search_type", 130, $string_search_type,'', 1, "-- Searching Type --" );
                          ?>
                        </th>
                    </tr>                	
                    <tr>
                        <th width="150px">Batch Type</th>
                        <th width="150px">Batch No</th>
                        <th width="220px">Batch Date Range</th>
                        <th>
                            <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                            <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                            <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" value="">
                        </th>
                    </tr>
                </thead>
                <tr>
                    <td align="center">	
                        <?php
                            echo create_drop_down( "cbo_batch_type", 150, $order_source,"",1, "--Select--", 0,0,0 );
                        ?>
                    </td>
                     <td align="center">				
                        <input type="text" style="width:140px" class="text_boxes"  name="txt_search_batch" id="txt_search_batch" />	
                    </td> 
                    <td align="center">
                        <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:80px;">To<input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:80px;">
                    </td>
                   
                    <td align="center">
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view (document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_company_id').value+'_'+document.getElementById('cbo_batch_type').value+'_'+document.getElementById('txt_search_batch').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_batch_search_list_view', 'search_div', 'dyeing_production_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center" height="40" valign="middle"><?php echo load_month_buttons(1); ?></td>
                </tr>
            </table>
            <table width="100%" style="margin-top:5px;">
                <tr>
                    <td colspan="4">
                        <div style="width:100%; margin-left:3px;" id="search_div" align="left"></div>
                    </td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>
</body>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}
if($action=="create_batch_search_list_view")
{
	$data = explode("_",$data);
	$start_date =$data[0];
	$end_date =$data[1];
	$company_id =$data[2];
	$batch_type =$data[3];
	$batch_no =$data[4];
	$search_type =$data[5];
 	
	if($search_type==1)
	{
		if ($batch_no!='') $batch_cond=" and batch_no='$batch_no'"; else $batch_cond="";
	}
	else if($search_type==4 || $search_type==0)
	{
		if ($batch_no!='') $batch_cond=" and batch_no like '%$batch_no%'"; else $batch_cond="";
	}
	else if($search_type==2)
	{
		if ($batch_no!='') $batch_cond=" and batch_no like '$batch_no%'"; else $batch_cond="";
	}
	else if($search_type==3)
	{
		if ($batch_no!='') $batch_cond=" and batch_no like '%$batch_no'"; else $batch_cond="";
	}	
	if($batch_type==0)
		$search_field_cond_batch="and entry_form in (0,36)";
	else if($batch_type==1)
		$search_field_cond_batch="and entry_form=0";
	else if($batch_type==2)
		$search_field_cond_batch="and entry_form=36";
	//echo $search_field_cond_batch;die;
	if($db_type==2)
		{	
		if ($start_date!="" &&  $end_date!="") $batch_date_con = " and batch_date between '".change_date_format($start_date, "mm-dd-yyyy", "-",1)."' and '".change_date_format($end_date, "mm-dd-yyyy", "-",1)."'"; else $batch_date_con ="";
		
		if($batch_type==0 || $batch_type==2)
			{
		
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,listagg(cast(a.order_no as varchar2(4000)),',') within group (order by a.order_no) as po_no from  subcon_ord_dtls a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
			else
			{
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,listagg(cast(a.po_number as varchar2(4000)),',') within group (order by a.po_number) as po_no from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
		}
		if($db_type==0)
		{	
		if ($start_date!="" &&  $end_date!="") $batch_date_con = " and batch_date between '".change_date_format($start_date, "yyyy-mm-dd", "-",1)."' and '".change_date_format($end_date, "yyyy-mm-dd", "-")."'"; else $batch_date_con ="";
		
		if($batch_type==0 || $batch_type==2)
			{
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,group_concat( distinct a.order_no )  as po_no from  subcon_ord_dtls a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
			else
			{
			$sql_po=sql_select("select b.mst_id, a.job_no_mst,group_concat(distinct a.po_number)  as po_no from wo_po_break_down a, pro_batch_create_dtls b where a.id=b.po_id and  b.status_active=1 and b.is_deleted=0 group by b.mst_id,a.job_no_mst");
			}
		}
	$po_num=array();
	
	foreach($sql_po as $row_po_no)
	{
	$po_num[$row_po_no[csf('mst_id')]]['po_no']=$row_po_no[csf('po_no')];
	$po_num[$row_po_no[csf('mst_id')]]['job_no_mst']=$row_po_no[csf('job_no_mst')];
		
	} 	//and company_id=$company_id
	$sql = "select id, batch_no, batch_date, batch_weight, booking_no, extention_no, color_id, batch_against, re_dyeing_from from pro_batch_create_mst where batch_for in(0,1) and batch_against<>4 and status_active=1 and is_deleted=0 $search_field_cond_batch $batch_date_con $batch_cond order by id desc"; 
	//echo $sql;//die; 
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Batch No</th>
                <th width="80">Extention No</th>
                <th width="80">Batch Date</th>
                <th width="90">Batch Qnty</th>
                <th width="115">Job No</th>
                <th width="80">Color</th>
                <th>Po No</th>
            </thead>
        </table>
        <div style="width:770px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="tbl_list_search" >
            <?php
				$i=1;
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					$po_no='';
					//echo $selectResult['re_dyeing_from'];die;
					if($selectResult[csf('re_dyeing_from')]==0)
					{	
						$po_no=implode(",",array_unique(explode(",",$po_num[$selectResult[csf('id')]]['po_no'])));
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
							<td width="40" align="center"><?php echo $i; ?></td>	
							<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
                            <td width="80"><p><?php /*if($selectResult[csf('extention_no')]!=0)*/ echo $selectResult[csf('extention_no')]; ?></p></td>
							<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
							<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
                            <td width="115"><p><?php echo $po_num[$selectResult[csf('id')]]['job_no_mst']; ?></p></td>
							<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
							<td><?php echo $po_no; ?></td>	
						</tr>
						<?php
						$i++;
					}
					else
					{ 
						 $sql_re= "select id, batch_no, batch_date, batch_weight, booking_no, MAX(extention_no) as extention_no, color_id, batch_against, 
						 re_dyeing_from from pro_batch_create_mst where  batch_for in(0,1) and entry_form in(0,36) and batch_against<>4 and status_active=1 and 
						 is_deleted=0 and id='".$selectResult[csf('re_dyeing_from')]."' group by id, batch_no, batch_date, batch_weight, booking_no,color_id, batch_against, 
						 re_dyeing_from  ";
						$dataArray=sql_select( $sql_re );
							
						foreach($dataArray as $row)
						{
							if($row[csf('re_dyeing_from')]==0)
							{
								$po_no=implode(",",array_unique(explode(",",$po_num[$selectResult[csf('id')]]['po_no'])));
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value(<?php echo $selectResult[csf('id')]; ?>)"> 
									<td width="40" align="center"><?php echo $i; ?></td>	
									<td width="100"><p><?php echo $selectResult[csf('batch_no')]; ?></p></td>
									<td width="80"><p><?php if($selectResult[csf('extention_no')]!=0) echo $selectResult[csf('extention_no')]; ?></p></td>
									<td width="80"><?php echo change_date_format($selectResult[csf('batch_date')]); ?></td>
									<td width="90" align="right"><?php echo $selectResult[csf('batch_weight')]; ?></td> 
								
                                    <td width="115"><p><?php echo $po_num[$selectResult[csf('id')]]['job_no_mst']; ?></p></td>
									<td width="80"><p><?php echo $color_arr[$selectResult[csf('color_id')]]; ?></p></td>
									<td><?php echo $po_no; ?></td>	
								</tr>
								<?php
								$i++;
							}
						}
					}
				}
			?>
            </table>
        </div>
	</div>           
<?php
exit();
}
if($action=='populate_data_from_batch')
{ 
	$ex_data=explode('_',$data);
	$load_unload=$ex_data[0];
	$batch_id=$ex_data[1]; 
	$batch_no=$ex_data[2]; 
	$company=$ex_data[3]; 
	$ltb_btb=array(1=>'BTB',2=>'LTB');
	if($db_type==0) $select_field1="order by a.id"; 
	else if($db_type==2) $select_field1=" group by a.id,a.batch_no,a.batch_weight,a.color_id,a.company_id,a.process_id, a.booking_without_order,a.entry_form order by a.id";
	if($db_type==0) $select_list="group_concat(distinct(b.po_id)) as po_id"; 
	else if($db_type==2) $select_list=" listagg(b.po_id,',') within group (order by b.po_id) as po_id";
	//$booking_id=implode(",",array_unique(explode(",",$booking_id)));
		if($batch_no!='')
		{ 
		$data_array=sql_select("select a.id as id,a.batch_no,a.entry_form,a.company_id, a.batch_weight,Max(a.extention_no) as extention_no,a.process_id as process_id_batch, a.color_id, a.booking_without_order, 
		sum(b.batch_qnty) as batch_qnty,  $select_list from pro_batch_create_mst a,pro_batch_create_dtls b where 
		a.id='$batch_id'  and a.entry_form in(0,36) and a.id=b.mst_id $select_field1");
		}
		else
		{
		$data_array=sql_select("select a.id as id,a.batch_no,a.entry_form,a.company_id, a.batch_weight,Max(a.extention_no) as extention_no,a.process_id as process_id_batch, a.color_id, a.booking_without_order, 
		sum(b.batch_qnty) as batch_qnty, $select_list  from pro_batch_create_mst a,pro_batch_create_dtls b where 
		a.id='$batch_id' and a.entry_form in(0,36) and a.id=b.mst_id  $select_field1");
		}
	if($db_type==0) $select_f_group=""; 
	else if($db_type==2) $select_f_group="group by a.job_no_mst, b.buyer_name";
	if($db_type==0) $select_listagg="group_concat(distinct(a.po_number)) as po_no"; 
	else if($db_type==2) $select_listagg="listagg(cast(a.po_number as varchar(500)),',') within group (order by a.po_number) as po_no";
	
	if($db_type==0) $select_listagg_subcon="group_concat(distinct(a.order_no)) as po_no"; 
	else if($db_type==2) $select_listagg_subcon="listagg(cast(a.order_no as varchar2(4000)),',') within group (order by a.order_no) as po_no";
	if($db_type==0) $select_listagg_subcon="group_concat(distinct(a.order_no)) as po_no"; 
	else if($db_type==2) $select_listagg_subcon="listagg(cast(a.order_no as varchar2(4000)),',') within group (order by a.order_no) as po_no";

	foreach ($data_array as $row)
	{ 
		$pro_id=implode(",",array_unique(explode(",",$row[csf('po_id')])));
	    echo "load_drop_down( 'requires/dyeing_production_controller', '".$row[csf("company_id")]."', 'load_drop_floor', 'floor_td' );\n";
		echo "document.getElementById('txt_batch_no').value 			= '".$row[csf("batch_no")]."';\n";
		echo "document.getElementById('hidden_batch_id').value 			= '".$row[csf("id")]."';\n";	
		echo "document.getElementById('txt_batch_ID').value 			= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_color').value 				= '".$color_arr[$row[csf("color_id")]]."';\n";
		echo "document.getElementById('txt_ext_id').value 				= '".$row[csf("extention_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		if($row[csf("entry_form")]==36)
		{
			
		$batch_type="<b> SUBCONTRACT ORDER BATCH</b>";
		$result_job=sql_select("select $select_listagg_subcon, b.subcon_job as job_no_mst, b.party_id as buyer_name from  subcon_ord_dtls a, 
		 subcon_ord_mst b where a.job_no_mst=b.subcon_job and a.id in(".$pro_id.") and b.status_active=1 and b.is_deleted=0 and a.status_active=1 
		and a.is_deleted=0 group by b.subcon_job, b.party_id");
		}
		else
		{
		$batch_type="<b> SELF ORDER BATCH </b>";
		$result_job=sql_select("select $select_listagg, a.job_no_mst, b.buyer_name from wo_po_break_down a, 	
		wo_po_details_master b where a.job_no_mst=b.job_no and a.id in(".$pro_id.") and b.status_active=1 and b.is_deleted=0 and a.status_active=1 
		and a.is_deleted=0 $select_f_group");
		}
		echo "document.getElementById('batch_type').innerHTML 			= '".$batch_type."';\n";
		
		$process_name_batch='';
		$process_id_array=explode(",",$row[csf("process_id_batch")]);
		foreach($process_id_array as $val)
		{
			if($process_name_batch=="") $process_name_batch=$conversion_cost_head_array[$val]; else $process_name_batch.=",".$conversion_cost_head_array[$val];
		}
		echo "document.getElementById('txt_process_id').value 			= '".$row[csf("process_id_batch")]."';\n";
		echo "document.getElementById('txt_process_name').value 			= '".$process_name_batch."';\n";
		
		$pro_id2=implode(",",array_unique(explode(",",$result_job[0][csf("po_no")])));
		echo "document.getElementById('txt_buyer').value 				= '".$buyer_arr[$result_job[0][csf("buyer_name")]]."';\n";
		echo "document.getElementById('txt_job_no').value 				= '".$result_job[0][csf("job_no_mst")]."';\n";
		echo "document.getElementById('txt_order_no').value 			= '".$pro_id2."';\n";
		$sql_batch_d=sql_select("select id,company_id,batch_id,batch_no,process_end_date,end_hours,end_minutes,machine_id,floor_id,process_id,ltb_btb_id,remarks from 
		pro_fab_subprocess where batch_id='$batch_id' and entry_form=35 and load_unload_id=1");
		foreach($sql_batch_d as $dyeing_d)
		{
		echo "document.getElementById('txt_dying_started').value = '".change_date_format($dyeing_d[csf("process_end_date")])."';\n";
		echo "document.getElementById('txt_dying_end_load').value = '".$dyeing_d[csf("end_hours")].':'.$dyeing_d[csf("end_minutes")]."';\n";
		echo "document.getElementById('txt_ltb_btb').value	= '".$ltb_btb[$dyeing_d[csf("ltb_btb_id")]]."';\n";
		}
		//exit();
	}
	if($db_type==0) $select_group_row1=" order by id desc limit 0,1"; 
	else if($db_type==2) $select_group_row1=" and  rownum>=1 order by id desc";//order by id desc limit 0,1
		if($load_unload==1)
		{ 
	$sql_batch=sql_select(
	"select id,batch_no,batch_id,process_end_date,load_unload_id,end_hours,end_minutes,machine_id,floor_id,process_id,
	ltb_btb_id,water_flow_meter,result,remarks,multi_batch_load_id	from pro_fab_subprocess where batch_id='$batch_id' and entry_form=35 and load_unload_id in(1) ");
		}
		else if($load_unload==2)
		{ 
		$sql_batch=sql_select("select id,batch_no,batch_id,process_end_date,load_unload_id,end_hours,end_minutes,machine_id,floor_id,process_id,ltb_btb_id,water_flow_meter,result,remarks,shift_name,production_date from pro_fab_subprocess where batch_id='$batch_id' and entry_form=35 and load_unload_id in(2,1) $select_group_row1");
		}
		foreach($sql_batch as $r_batch)
		{
			if($load_unload==1) //Load
				{
				if($r_batch[csf('load_unload_id')]==1)
				{
				echo "document.getElementById('txt_update_id').value 				= '".$r_batch[csf("id")]."';\n";	
				}
				else
				{
				echo "document.getElementById('txt_update_id').value 				= '';\n";
				}
				$process_name='';
				$process_id_array=explode(",",$r_batch[csf("process_id")]);
				foreach($process_id_array as $val)
				{
					if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=",".$conversion_cost_head_array[$val];
				}	
				echo "document.getElementById('txt_process_start_date').value 		= '".change_date_format($r_batch[csf("process_end_date")])."';\n";
				//echo "document.getElementById('cbo_sub_process').value 				= '".$r_batch[csf("process_id")]."';\n";
				echo "document.getElementById('txt_process_id').value 				= '".$r_batch[csf("process_id")]."';\n";
				echo "document.getElementById('txt_process_name').value 			= '".$process_name."';\n";
				//echo "set_multiselect('cbo_sub_process','0','1','".$r_batch[csf('process_id')]."','0');\n";
				echo "document.getElementById('cbo_ltb_btb').value	= '".$r_batch[csf("ltb_btb_id")]."';\n";
				echo "document.getElementById('txt_water_flow').value	= '".$r_batch[csf("water_flow_meter")]."';\n";
				echo "document.getElementById('cbo_yesno').value	= '".$r_batch[csf("multi_batch_load_id")]."';\n";
				$minute=str_pad($r_batch[csf("end_minutes")],2,'0',STR_PAD_LEFT);
				$hour=str_pad($r_batch[csf("end_hours")],2,'0',STR_PAD_LEFT);
				echo "document.getElementById('txt_start_minutes').value	= '".$minute."';\n";
				echo "document.getElementById('txt_start_hours').value	= '".$hour."';\n";
				echo "load_drop_down( 'requires/dyeing_production_controller', document.getElementById('cbo_company_id').value+'**'+".$r_batch[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
				echo "document.getElementById('cbo_floor').value = '".$r_batch[csf("floor_id")]."';\n";
				echo "document.getElementById('cbo_machine_name').value = '".$r_batch[csf("machine_id")]."';\n";
				echo "document.getElementById('txt_remarks').value	= '".$r_batch[csf("remarks")]."';\n";
				if($r_batch[csf("id")]!=0)
				{
					echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',1);\n"; 	
				}
				else
				{
					echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',0);\n"; 		
				}
				}
				else if($load_unload==2) //Unload
				{ 
					if($r_batch[csf("load_unload_id")]==2)
					{
					echo "document.getElementById('txt_update_id').value 		= '".$r_batch[csf("id")]."';\n";
					echo "document.getElementById('txt_process_end_date').value = '".($r_batch[csf("load_unload_id")] == 1 ? "" : change_date_format($r_batch[csf("process_end_date")]))."';\n";
					echo "document.getElementById('txt_process_date').value = '".($r_batch[csf("load_unload_id")] == 1 ? "" : change_date_format($r_batch[csf("production_date")]))."';\n";
					echo "document.getElementById('cbo_shift_name').value	= '".$r_batch[csf("shift_name")]."';\n";
					echo "document.getElementById('txt_water_flow').value	= '".($r_batch[csf("load_unload_id")] == 1 ? "" : $r_batch[csf("water_flow_meter")])."';\n";
					echo "document.getElementById('cbo_ltb_btb').value	= '".$r_batch[csf("ltb_btb_id")]."';\n";
					}
					//$process_name=$r_batch[csf('process_id')];
					//echo "document.getElementById('cbo_sub_process').value 		= '".$r_batch[csf("process_id")]."';\n";
					//echo "set_multiselect('cbo_sub_process','0','1','".$r_batch[csf("process_id")]."','0');\n";
					$process_name='';
					$process_id_array=explode(",",$r_batch[csf("process_id")]);
					foreach($process_id_array as $val)
					{
						if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=",".$conversion_cost_head_array[$val];
					}	
					//echo "load_drop_down( 'requires/dyeing_production_controller', '".$r_batch[csf("process_id")]."', 'load_drop_sub_process', 'sub_process_td' );\n";
                   // echo "set_multiselect('cbo_sub_process','0','0','0','0');\n";
					//echo "set_multiselect('cbo_sub_process','0','1','".$r_batch[csf('process_id')]."','0');\n";
					//echo "document.getElementById('txt_end_minutes').value	= '".($r_batch[csf("load_unload_id")] == 1 ? "" : $r_batch[csf("end_minutes")])."';\n";
					//echo "document.getElementById('txt_end_hours').value	= '".($r_batch[csf("load_unload_id")] == 1 ? "" : $r_batch[csf("end_hours")])."';\n";
					echo "document.getElementById('txt_process_id').value 				= '".$r_batch[csf("process_id")]."';\n";
					echo "document.getElementById('txt_process_name').value 				= '".$process_name."';\n";
					if($r_batch[csf("load_unload_id")]==2)
					{
					$minute=str_pad($r_batch[csf("end_minutes")],2,'0',STR_PAD_LEFT);
					$hour=str_pad($r_batch[csf("end_hours")],2,'0',STR_PAD_LEFT);
					echo "document.getElementById('txt_end_minutes').value	= '".$minute."';\n";
					echo "document.getElementById('txt_end_hours').value	= '".$hour."';\n";
					}
					echo "document.getElementById('cbo_floor').value = '".$r_batch[csf("floor_id")]."';\n";
					echo "load_drop_down( 'requires/dyeing_production_controller', document.getElementById('cbo_company_id').value+'**'+".$r_batch[csf("floor_id")].", 'load_drop_machine', 'machine_td' );\n";
					echo "document.getElementById('cbo_machine_name').value = '".$r_batch[csf("machine_id")]."';\n";
					//echo "document.getElementById('cbo_ltb_btb').value	= '".$r_batch[csf("ltb_btb_id")]."';\n";
					if($r_batch[csf("load_unload_id")]==2)
					{
					echo "document.getElementById('cbo_result_name').value	= '".$r_batch[csf("result")]."';\n";
					echo "document.getElementById('txt_remarks').value	= '".($r_batch[csf("load_unload_id")] == 1 ? "" : $r_batch[csf("remarks")])."';\n";
					}
					//echo "document.getElementById('txt_remarks').value	= '".$r_batch[csf("remarks")]."';\n";
					echo "$('#cbo_machine_name').attr('disabled',true);\n";
					echo "$('#cbo_floor').attr('disabled',true);\n";
					if($r_batch[csf("load_unload_id")]==2)
					{
						echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',1);\n"; 	
					}
					else
					{
						echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_pro_fab_subprocess',0);\n";	
					}
				}
		}
		exit();	
}
if($action=='show_fabric_desc_listview')
{
			if($db_type==0) $select_group=" group by b.id,b.item_description"; 
			else if($db_type==2) $select_group="group by b.id,b.item_description,b.width_dia_type";//order by id desc limit 0,1
			$result=sql_select("select b.width_dia_type,b.item_description,b.width_dia_type, sum(b.batch_qnty) as batch_qnty from pro_batch_create_dtls b,pro_batch_create_mst a  where b.mst_id=$data and a.id=b.mst_id and  a.entry_form in(0,36) and b.status_active=1 and b.is_deleted=0 $select_group");
	$i=1;
	$b_qty=0;
	foreach($result as $row)
	{
		$desc=explode(",",$row[csf('item_description')]);
	?>
    	<tr class="general" id="row_<?php echo $i; ?>">
            <td title="<?php echo $desc[1]; ?>"><input type="text" name="txt_cons_comp_<?php echo $i; ?>" id="txt_cons_comp_<?php echo $i; ?>" class="text_boxes" style=
            "width:170px;" value="<?php echo $desc[1]; ?>" disabled/></td>
            <td><input type="text" name="txt_gsm_<?php echo $i; ?>" id="txt_gsm_<?php echo $i; ?>" class="text_boxes" style="width:60px;" value="<?php echo $desc[2]; ?>
            " disabled/></td>
            <td><input type="text" name="txt_body_part_<?php echo $i; ?>" id="txt_body_part_<?php echo $i; ?>" class="text_boxes" style="width:110px;" value="<?php echo		
			 $desc[3];  //$row[csf('width_dia_type')] ?>" disabled/></td>
            <td title="<?php echo  $fabric_typee[$row[csf('width_dia_type')]]; ?>"><input type="text" name="txt_dia_width_<?php echo $i; ?>" id="txt_dia_width_<?php 
			echo $i; ?>" class="text_boxes" style="width:70px;" value="<?php echo  $fabric_typee[$row[csf('width_dia_type')]]; ?>" disabled/></td>
            <td><input type="text" name="txt_batch_qnty_<?php echo $i; ?>" id="txt_batch_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px;" value
            ="<?php echo number_format($row[csf('batch_qnty')]); ?>" disabled/></td>
        </tr>
    <?php
	$b_qty+= $row[csf('batch_qnty')];
		$i++;
	}
	?>
	 <tr>
        <td colspan="4" align="right"><b>Sum:</b> <?php //echo $b_qty; ?> </td>
         <td align="right"><?php echo number_format($b_qty); ?> </td>
        </tr>
        <?php
	exit();
}
if($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="";
		$data_array="";
		$id=return_next_id( "id", "pro_fab_subprocess", 1 ) ;
		//echo $cbo_load_unload;die;multi_batch_load_id
		if($cbo_load_unload=="'1'")
		{
			$field_array="id,company_id,batch_no,batch_id,batch_ext_no,process_id,ltb_btb_id,water_flow_meter,process_end_date,end_hours,end_minutes,machine_id,floor_id,load_unload_id,entry_form,remarks,multi_batch_load_id,inserted_by,insert_date";			
			$data_array="(".$id.",".$cbo_company_id.",".$txt_batch_no.",".$txt_batch_ID.",".$txt_ext_id.",".$txt_process_id.",".$cbo_ltb_btb.",".$txt_water_flow.",".$txt_process_start_date.",".$txt_start_hours.",".$txt_start_minutes.",".$cbo_machine_name.",".$cbo_floor.",".$cbo_load_unload.",35,".$txt_remarks.",".$cbo_yesno.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		}
		if($cbo_load_unload=="'2'")
		{
			$field_array="id,company_id,batch_no,batch_id,batch_ext_no,process_id,ltb_btb_id,water_flow_meter,process_end_date,end_hours,end_minutes,machine_id,floor_id,load_unload_id,result,entry_form,remarks,shift_name,production_date,inserted_by,insert_date";			
			$data_array="(".$id.",".$cbo_company_id.",".$txt_batch_no.",".$hidden_batch_id.",".$txt_ext_id.",".$txt_process_id.",".$cbo_ltb_btb.",".$txt_water_flow.",".$txt_process_end_date.",".$txt_end_hours.",".$txt_end_minutes.",".$cbo_machine_name.",".$cbo_floor.",".$cbo_load_unload.",".$cbo_result_name.",35,".$txt_remarks.",".$cbo_shift_name.",".$txt_process_date.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		//print_r($data_array);
		}
		$rID=sql_insert("pro_fab_subprocess",$field_array,$data_array,0);
			//echo "insert into pro_fab_subprocess (".$field_array.") values ".$data_array;die;
		//check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "0**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$id;
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID)
			{
				oci_commit($con);
				echo "0**".$id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$id;
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$update_id=str_replace("'","",$txt_update_id);
		$field_array="";
		$data_array="";
		$id=return_next_id( "id", "pro_fab_subprocess", 1 ) ;
		//echo $cbo_load_unload;die;
		if($cbo_load_unload=="'1'")
		{
			$field_array_update="company_id*batch_no*batch_id*batch_ext_no*process_id*ltb_btb_id*water_flow_meter*process_end_date*end_hours*end_minutes*machine_id*floor_id*load_unload_id*entry_form*remarks*multi_batch_load_id*updated_by*update_date";
			$data_array_update="".$cbo_company_id."*".$txt_batch_no."*".$txt_batch_ID."*".$txt_ext_id."*".$txt_process_id."*".$cbo_ltb_btb."*".$txt_water_flow."*".$txt_process_start_date."*".$txt_start_hours."*".$txt_start_minutes."*".$cbo_machine_name."*".$cbo_floor."*".$cbo_load_unload."*35*".$txt_remarks."*".$cbo_yesno."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		}
		if($cbo_load_unload=="'2'")
		{
			$field_array_update="company_id*batch_no*batch_id*batch_ext_no*process_id*ltb_btb_id*water_flow_meter*process_end_date*end_hours*end_minutes*machine_id*floor_id*load_unload_id*result*entry_form*remarks*shift_name*production_date*updated_by*update_date";
			$data_array_update="".$cbo_company_id."*".$txt_batch_no."*".$hidden_batch_id."*".$txt_ext_id."*".$txt_process_id."*".$cbo_ltb_btb."*".$txt_water_flow."*".$txt_process_end_date."*".$txt_end_hours."*".$txt_end_minutes."*".$cbo_machine_name."*".$cbo_floor."*".$cbo_load_unload."*".$cbo_result_name."*35*".$txt_remarks."*".$cbo_shift_name."*".$txt_process_date."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
		}
		$rID=sql_update("pro_fab_subprocess",$field_array_update,$data_array_update,"id",$update_id,1);
		//echo "insert into pro_fab_subprocess values $field_array_update,$field_array_update,'id',$update_id,0)";die;
		//check_table_status( $_SESSION['menu_id'],0);	
		if($db_type==0)
		{
			if($rID)
			{
				mysql_query("COMMIT");  
				echo "1**".$update_id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$update_id;
			}
		}
		if($db_type==2 || $db_type==1 )
		{
		if($rID)
			{
				oci_commit($con);
				echo "1**".$update_id;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$update_id;
			}
		}
		disconnect($con);
		die;
	}
}
if($action=="check_batch_no")
{
	$data=explode("**",$data);
	$sql="select id, batch_no,company_id from pro_batch_create_mst where batch_no='".trim($data[1])."' and entry_form in(0,36) and is_deleted=0 and status_active=1 order by id desc";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('id')]."_".$data_array[0][csf('company_id')];
	}
	else
	{
		echo "0_";
	}
	exit();	
}
if($action=="check_batch_no_scan")
{
	$data=explode("**",$data);
	$batch_id=(int) $data[1];
	$sql="select id, batch_no,company_id from pro_batch_create_mst where id='".$batch_id."' and entry_form in(0,36) and is_deleted=0 and status_active=1 order by id desc";
	$data_array=sql_select($sql,1);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('id')]."_".$data_array[0][csf('company_id')];
	}
	else
	{
		echo "0_";
	}
	exit();	
}
if($action=="check_batch_no_load")
{
	$data=explode("**",$data);
	$sql="select id, batch_id from pro_fab_subprocess where  company_id='".trim($data[0])."' and  batch_id='".trim($data[2])."' and load_unload_id=1 and entry_form=35 and is_deleted=0 and status_active=1";
	$data_array=sql_select($sql);
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('batch_id')];
	}
	else
	{
		echo "0_";
	}
	exit();	
}
if($action=="check_batch_no_for_machine")
{
	$data=explode("**",$data);
	$sql="select  batch_no as batch_no from pro_fab_subprocess where  company_id='".trim($data[0])."'  and machine_id='$data[3]' and load_unload_id=1 and entry_form=35 and is_deleted=0 and status_active=1 and batch_id not in(select batch_id from pro_fab_subprocess where  company_id='".trim($data[0])."'  and machine_id='$data[3]' and load_unload_id=2 and entry_form=35 and is_deleted=0 and status_active=1 ) ";
	$data_array=sql_select($sql,1);
	//echo count($data_array);die;
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('batch_no')];
	}
	else
	{
		echo "0_";
	}
	exit();	
}
if($action=="check_for_shade_matched")
{
	$data=explode("**",$data);
	//listagg(batch_id,',') within group (order by batch_id) as batch_id
	$sql_unload="select  batch_no from pro_fab_subprocess where  company_id='".trim($data[0])."' and  batch_no='".trim($data[1])."' and load_unload_id=2 and entry_form=35 and is_deleted=0 and result=1 and status_active=1";
	$data_array=sql_select($sql_unload,1);
	//echo count($data_array);die;
	if(count($data_array)>0)
	{
		echo "1"."_".$data_array[0][csf('batch_no')];
	}
	else
	{
		echo "0_";
	}
	exit();	
}
if ($action=="on_change_data")
{	
	extract($_REQUEST);
	$explode_data = explode("_",$data);
	$type = $explode_data[0];
	$company_id = $explode_data[1];
	if($data=="1") // Loading
	{
			?>
              <div>
			<fieldset>
					<table cellpadding="0" cellspacing="2" width="100%" id="main_tbl">
                        <tr> 
                            <td width="" class="must_entry_caption">Batch No.</td>
                            <td>
                                <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:122px;" placeholder="Write/Browse" onDblClick="openmypage_batchnum();" onChange="check_batch();"   />
                                <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" readonly />
                                <!--<input type="hidden" name="txt_ext_id" id="txt_ext_id" style="width:100px;" class="text_boxes" readonly />-->
                            </td>
                        </tr>
                        <tr>
                         <td class="must_entry_caption" width="130">Company</td>
                            <td>
                                <?php
                                    echo create_drop_down( "cbo_company_id", 135, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down('requires/dyeing_production_controller', this.value, 'load_drop_floor', 'floor_td' );","","","","","" );
                                ?>
                                <input type="hidden" name="txt_update_id" id="txt_update_id" style="width:100px;" class="text_boxes"  readonly />
                            </td>
                        </tr>
                        <tr>
                            <td>Process </td>
                            <td>
                               <?php
								   //echo create_drop_down( "cbo_sub_process", 135, $conversion_cost_head_array,"", 0, "", 0, "","","34,60,61,62,82,83,84,85,86,87,89,100","","","");
                               ?>
                                <input type="text" name="txt_process_name" id="txt_process_name" class="text_boxes" style="width:122px;" placeholder="Double Click To Search" onDblClick="openmypage_process();" readonly />
                            <input type="hidden" name="txt_process_id" id="txt_process_id" />
                            </td>
                        </tr>
                        <tr>
                            <td class="">LTB/BTB </td>
                            <td>
                               <?php $ltb_btb=array(1=>'BTB',2=>'LTB');
								   echo create_drop_down( "cbo_ltb_btb", 135, $ltb_btb,"", 1, "-- Select --", 1, "","","","","","");
                               ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="">Water Flow Reading </td>
                            <td>
                                <input type="text" name="txt_water_flow" id="txt_water_flow" style="width:122px;" class="text_boxes"   />
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process Start Date</td>
                            <td>
                                <input type="text" name="txt_process_start_date" id="txt_process_start_date" class="datepicker" style="width:122px;"  readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process Start Time</td>
                            <td>
                                 <input type="text" name="txt_start_hours" id="txt_start_hours" class="text_boxes_numeric" placeholder="Hours" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_hours','txt_end_date',2,23)" value="<?php echo date('H');?>"  />
                                <input type="text" name="txt_start_minutes" id="txt_start_minutes" class="text_boxes_numeric" placeholder="Minutes" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_minutes','txt_end_date',2,59)" value="<?php echo date('i');?>"  />
                            </td>
                        </tr>
                         <tr>
                            <td class="must_entry_caption">Floor</td>
                            <td id="floor_td">
								<?php
									 echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected, "",0,"","","","",4 );
                                ?>
                            </td>
                        <tr>
                        <tr>
                            <td class="must_entry_caption">Machine Name</td>
                            <td id="machine_td">
								<?php
									echo create_drop_down("cbo_machine_name", 135, $blank_array,"", 1, "-- Select Machine --", 0, "",0,"","","",""); 
                                ?>
                            </td>
                        </tr>
                         <tr>
                            <td>Multi Batch Loading</td>
                            <td>
								<?php
									echo create_drop_down("cbo_yesno", 80, $yes_no,"", 1, "-- Select--", 2, "",0,"","","",""); 
                                ?>
                            </td>
                        </tr>
                    </table>
			</fieldset>
  <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
  </div>
<?php    				
	}
	if($data=="2") // Un-loading
	{
			?>
			<fieldset>
					<table cellpadding="0" cellspacing="2" width="100%" id="main_tbl">
                        <tr> 
                            <td width="" class="must_entry_caption">Batch No.</td>
                            <td>
                                <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:122px;" placeholder="Write/Browse"  onDblClick="openmypage_batchnum();" onChange="check_batch();" />
                                <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" readonly />
                                <!--<input type="hidden" name="txt_ext_id" id="txt_ext_id" style="width:100px;" class="text_boxes" readonly />-->
                            </td>
                        </tr>
                        <tr>
                         <td class="must_entry_caption" width="130">Company</td>
                            <td>
                                <?php
                                    echo create_drop_down( "cbo_company_id", 135, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down('requires/dyeing_production_controller', this.value, 'load_drop_floor', 'floor_td' );" );
                                ?>
                                <input type="hidden" name="txt_update_id" id="txt_update_id" style="width:100px;" class="text_boxes" readonly />
                            </td>
                        </tr>
                        <tr>
                            <td>Process </td>
                            <td id="sub_process_td">
                               <?php
								   //echo create_drop_down( "cbo_sub_process", 135, $conversion_cost_head_array,"", 0, "", $selected, "","","32,63,83,84" );
                               ?>
                                <input type="text" name="txt_process_name" id="txt_process_name" class="text_boxes" style="width:122px;" placeholder="Double Click To Search" onDblClick="openmypage_process();" readonly />
                            <input type="hidden" name="txt_process_id" id="txt_process_id" />
                            </td>
                        </tr>
                        <tr>
                            <td class="">LTB/BTB </td>
                            <td>
                               <?php $ltb_btb=array(1=>'BTB',2=>'LTB');
								   echo create_drop_down( "cbo_ltb_btb", 135, $ltb_btb,"", 1, "-- Select --", 0, "","","","","","");
                               ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="">Water Flow Reading </td>
                            <td>
                                <input type="text" name="txt_water_flow" id="txt_water_flow" style="width:122px;" class="text_boxes"  />
                            </td>
                        </tr>
                         <tr>
                            <td class="must_entry_caption">Production Date</td>
                            <td>
                                <input type="text" name="txt_process_end_date" id="txt_process_end_date" class="datepicker" style="width:122px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process End Date</td>
                            <td>
                                <input type="text" name="txt_process_date" id="txt_process_date" class="datepicker" style="width:122px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process End Time</td>
                            <td>
                                 <input type="text" name="txt_end_hours" id="txt_end_hours" class="text_boxes_numeric" placeholder="Hours" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_hours','txt_end_date',2,23)" value="<?php echo date('H');?>" />
                                <input type="text" name="txt_end_minutes" id="txt_end_minutes" class="text_boxes_numeric" placeholder="Minutes" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_minutes','txt_end_date',2,59)" value="<?php echo date('i');?>" />
                            </td>
                        </tr>
                         <td class="must_entry_caption">Floor</td>
                            <td id="floor_td">
								<?php
									 echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active =1 and is_deleted=0 order by floor_name","id,floor_name", 1, "-- Select Floor --", $selected, "",0,"","","","",4 );
                                ?>
                            </td>
                        <tr>
                            <td class="must_entry_caption">Machine Name</td>
                            <td id="machine_td">
								<?php
									echo create_drop_down("cbo_machine_name", 135, $blank_array,"", 1, "-- Select Machine --", 0, "",1 ); 
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Result</td>
                            <td>
								<?php
									echo create_drop_down("cbo_result_name", 135, $dyeing_result,"", 1, "-- Select Result --", 0, "",0 ,"","","","","txt_remarks"); 
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Shift Name</td>
                            <td>
								<?php
									echo create_drop_down("cbo_shift_name", 135, $shift_name,"", 1, "-- Select Shift --", 0, "",0 ,"","","","",""); 
                                ?>
                            </td>
                        </tr>
                    </table>
				<input type="hidden" name="txt_process_start_date" id="txt_process_start_date" />
                <input type="hidden" name="txt_start_minutes" id="txt_start_minutes" class="text_boxes_numeric"  />
                <input type="hidden" name="cbo_ltb_btb" id="cbo_ltb_btb" class="text_boxes_numeric"  />
			</fieldset>
<?php    				
	} ?>
    <script>	
	//set_multiselect('cbo_sub_process','0','0','','0');
</script>
     <script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
    <?php
	exit();
}
if($action=="process_name_popup")
{
	echo load_html_head_contents("Batch Number Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		$(document).ready(function(e) {
			setFilterGrid('tbl_list_search',-1);
		});
		var selected_id = new Array(); var selected_name = new Array(); var buyer_id=''; var style_ref_array= new Array();
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function set_all()
		{
			var old=document.getElementById('txt_process_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		function js_set_value( str ) 
		{
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hidden_process_id').val(id);
			$('#hidden_process_name').val(name);
		}
    </script>
</head>
<body>
<div align="center">
	<fieldset style="width:370px;margin-left:10px">
    	<input type="hidden" name="hidden_process_id" id="hidden_process_id" class="text_boxes" value="">
        <input type="hidden" name="hidden_process_name" id="hidden_process_name" class="text_boxes" value="">
        <form name="searchprocessfrm_1"  id="searchprocessfrm_1" autocomplete="off">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="350" class="rpt_table" >
                <thead>
                    <th width="50">SL</th>
                    <th>Process Name</th>
                </thead>
            </table>
            <div style="width:350px; overflow-y:scroll; max-height:280px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="332" class="rpt_table" id="tbl_list_search" >
                <?php
                    $i=1; $process_row_id=''; $not_process_id_print_array=array(1,2,3,4,101,120,121,122,123,124);
					$hidden_process_id=explode(",",$txt_process_id);
                    foreach($conversion_cost_head_array as $id=>$name)
                    {
						if(!in_array($id,$not_process_id_print_array))
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							 
							if(in_array($id,$hidden_process_id)) 
							{ 
								if($process_row_id=="") $process_row_id=$i; else $process_row_id.=",".$i;
							}
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
								<td width="50" align="center"><?php echo "$i"; ?>
									<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $id; ?>"/>	
									<input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $name; ?>"/>
								</td>	
								<td><p><?php echo $name; ?></p></td>
							</tr>
							<?php
							$i++;
						}
                    }
                ?>
                    <input type="hidden" name="txt_process_row_id" id="txt_process_row_id" value="<?php echo $process_row_id; ?>"/>
                </table>
            </div>
             <table width="350" cellspacing="0" cellpadding="0" style="border:none" align="center">
                <tr>
                    <td align="center" height="30" valign="bottom">
                        <div style="width:100%"> 
                            <div style="width:50%; float:left" align="left">
                                <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                            </div>
                            <div style="width:50%; float:left" align="left">
                                <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	set_all();
</script>
</html>
<?php
exit();
}
if($action=="process_name_popup_unload")
{
	echo load_html_head_contents("Batch Number Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		$(document).ready(function(e) {
			setFilterGrid('tbl_list_search',-1);
		});
		var selected_id = new Array(); var selected_name = new Array(); var buyer_id=''; var style_ref_array= new Array();
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function set_all()
		{
			var old=document.getElementById('txt_process_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		function js_set_value( str ) 
		{
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hidden_process_id').val(id);
			$('#hidden_process_name').val(name);
		}
    </script>
</head>
<body>
<div align="center">
	<fieldset style="width:370px;margin-left:10px">
    	<input type="hidden" name="hidden_process_id" id="hidden_process_id" class="text_boxes" value="">
        <input type="hidden" name="hidden_process_name" id="hidden_process_name" class="text_boxes" value="">
        <form name="searchprocessfrm_1"  id="searchprocessfrm_1" autocomplete="off">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="350" class="rpt_table" >
                <thead>
                    <th width="50">SL</th>
                    <th>Process Name</th>
                </thead>
            </table>
            <div style="width:350px; overflow-y:scroll; max-height:280px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="332" class="rpt_table" id="tbl_list_search" >
                <?php
                    $i=1; $process_row_id=''; $not_process_id_print_array=array();
					//echo $txt_process_id;die;
					$hidden_process_id=explode(",",$txt_process_id);
					/*$process_name='';
					$process_id_array=explode(",",$r_batch[csf("process_id")]);
					foreach($process_id_array as $val)
					{
						if($process_name=="") $process_name=$conversion_cost_head_array[$val]; else $process_name.=",".$conversion_cost_head_array[$val];
					}*/
                    foreach($conversion_cost_head_array as $id=>$name)
                    {
						if(in_array($id,$hidden_process_id))
						{
							if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							 
							if(in_array($id,$hidden_process_id)) 
							{ 
								if($process_row_id=="") $process_row_id=$i; else $process_row_id.=",".$i;
							}
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
								<td width="50" align="center"><?php echo "$i"; ?>
									<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $id; ?>"/>	
									<input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $name; ?>"/>
								</td>	
								<td><p><?php echo $name; ?></p></td>
							</tr>
							<?php
							$i++;
						}
                    }
                ?>
                    <input type="hidden" name="txt_process_row_id" id="txt_process_row_id" value="<?php echo $process_row_id; ?>"/>
                </table>
            </div>
             <table width="350" cellspacing="0" cellpadding="0" style="border:none" align="center">
                <tr>
                    <td align="center" height="30" valign="bottom">
                        <div style="width:100%"> 
                            <div style="width:50%; float:left" align="left">
                                <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                            </div>
                            <div style="width:50%; float:left" align="left">
                                <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </fieldset>
</div>    
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
	set_all();
</script>
</html>
<?php
exit();
}
?>