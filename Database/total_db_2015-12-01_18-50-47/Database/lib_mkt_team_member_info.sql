-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lib_mkt_team_member_info`
--

DROP TABLE IF EXISTS `lib_mkt_team_member_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lib_mkt_team_member_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL DEFAULT '0',
  `designation` varchar(50) DEFAULT NULL,
  `team_member_name` varchar(50) DEFAULT NULL,
  `team_member_email` varchar(50) DEFAULT NULL,
  `capacity_smv_member` int(11) NOT NULL DEFAULT '0',
  `capacity_basic_member` double NOT NULL DEFAULT '0',
  `member_contact_no` varchar(64) NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'employee code will be add here',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'employee code will be add here',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0:No 1:Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0:No 1:Yes',
  PRIMARY KEY (`id`),
  KEY `team_id` (`team_id`),
  KEY `team_member_name` (`team_member_name`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lib_mkt_team_member_info`
--

LOCK TABLES `lib_mkt_team_member_info` WRITE;
/*!40000 ALTER TABLE `lib_mkt_team_member_info` DISABLE KEYS */;
INSERT INTO `lib_mkt_team_member_info` VALUES (1,1,'GM','Md.Zaheed','',0,0,'',2,'2015-01-04 00:21:25',2,'2015-11-12 09:37:16',1,0),(2,1,'Merchandiser','Mr. Ariful Islam Jony','',0,0,'',2,'2015-01-04 03:45:06',1,'2015-01-31 05:17:36',1,0),(3,1,'S.R Merchandiser','Sarker Al Mamun','',0,0,'',2,'2015-01-08 21:30:01',NULL,NULL,1,0),(4,1,'Merchandiser','Mr.Mostafa','',0,0,'',2,'2015-01-08 21:30:53',NULL,NULL,1,0),(5,1,'Asst.Merchandiser','Mouli Rubaiat','',0,0,'',2,'2015-01-08 21:32:50',2,'2015-05-12 13:50:29',2,0),(6,1,'Asst.Merchandiser','Md.kaysar','',0,0,'',2,'2015-01-08 21:33:13',2,'2015-05-12 13:50:47',2,0),(7,1,'Merchandiser','Md.Nazrul Islam','',0,0,'',2,'2015-01-10 21:39:56',NULL,NULL,1,0),(8,1,'Sr. Merchandiser','Md. Kamal Hossain','',0,0,'',20,'2015-04-15 14:29:11',NULL,NULL,1,0),(9,1,'Sr. Merchandiser','Mr.Rafiqul Islam','',0,0,'',2,'2015-05-12 12:59:42',NULL,NULL,1,0),(10,1,'Sr. Merchandiser','Md. Raqibul Hasan','',0,0,'',20,'2015-06-01 17:11:58',NULL,NULL,1,0),(11,1,'Sr. Merchandisisng','Md. Alamgir kabir','',0,0,'',2,'2015-10-04 17:05:40',NULL,NULL,1,0),(12,1,'GM','Md.Zaheed','',0,0,'',2,'2015-11-12 09:29:48',2,'2015-11-12 09:31:32',0,1),(13,2,'AGM','Md.abu naser sobuz','',0,0,'',2,'2015-11-12 09:33:16',2,'2015-11-12 09:36:54',3,0),(14,1,'Merchandiser','Md.Samsul Alam','',0,0,'',2,'2015-11-24 15:45:03',NULL,NULL,1,0);
/*!40000 ALTER TABLE `lib_mkt_team_member_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:42
