-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_gmts_cutting_qc_mst`
--

DROP TABLE IF EXISTS `pro_gmts_cutting_qc_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_gmts_cutting_qc_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cut_qc_prefix` varchar(20) NOT NULL,
  `cut_qc_prefix_no` int(11) NOT NULL,
  `cutting_qc_no` varchar(20) NOT NULL,
  `cutting_no` varchar(25) NOT NULL,
  `location_id` int(11) NOT NULL,
  `floor_id` int(11) NOT NULL,
  `table_no` int(11) NOT NULL DEFAULT '0',
  `job_no` varchar(25) NOT NULL,
  `job_year` int(11) NOT NULL,
  `batch_id` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `entry_date` date NOT NULL,
  `start_time` varchar(10) NOT NULL DEFAULT '0:00',
  `end_date` date NOT NULL,
  `end_time` varchar(10) NOT NULL,
  `marker_length` varchar(25) NOT NULL,
  `marker_width` int(11) NOT NULL,
  `fabric_width` int(11) NOT NULL,
  `gsm` int(11) NOT NULL,
  `width_dia` int(3) NOT NULL,
  `cutting_qc_date` date DEFAULT NULL,
  `cutting_qc_time` time DEFAULT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` int(11) NOT NULL DEFAULT '1',
  `is_deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_gmts_cutting_qc_mst`
--

LOCK TABLES `pro_gmts_cutting_qc_mst` WRITE;
/*!40000 ALTER TABLE `pro_gmts_cutting_qc_mst` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_gmts_cutting_qc_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
